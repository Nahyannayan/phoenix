<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeaveApprovalEmail.aspx.vb" Inherits="LeaveApprovalEmail" Title="PHOENIXBETA" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<html lang="en">
<head runat="server">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PHOENIXBETA</title>
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">

    <style type="text/css">
        .card-login {
            box-shadow: 2px 0px 12px #333333;
            max-width: 100%;
        }

        .Absolute-Center.is-Responsive {
            max-width: 600px !important;
            width: 90% !important;
            min-width: 293px !important;
        }

        .btn-danger {
            color: #fff;
            background-color: #dc3545 !important;
            border-color: #dc3545;
        }

        .redButton {
            border-radius: 6px;
            font-family: 'Nunito', sans-serif !important;
            background-color: inherit !important;
            font-size: inherit !important;
            width: auto !important;
            min-width: 10%;
            padding: 8px !important;
            background-image: none !important;
            height: auto !important;
            margin: 4px;
            cursor: pointer;
            color: #fff;
            font-weight: bold;
            border-style: none;
            background: rgb(165,224,103) !important;
            background: -moz-linear-gradient(top, #e52d27 0%, #b31217 51%, #e52d27 100%) !important;
            background: -webkit-linear-gradient(top, #e52d27 0%, #b31217 51%, #e52d27 100%) !important;
            background: linear-gradient(to bottom, #e52d27 0%, #b31217 51%, #e52d27 100%) !important;
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e40a0a', endColorstr='#9f0202',GradientType=0 ) !important;
        }

        .title {
            /* background-image: url(/Images/bggreen.gif); */
            color: #fff;
            background: rgb(144,193,79);
            /* background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 52%,rgba(131,193,50,1) 100%); */
            /* background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 52%,rgba(131,193,50,1) 100%); */
            /* font-weight: bold; */
            font-size: 130%;
            font-weight: 700;
            border-radius: 6px;
            padding: 6px 12px;
        }

        .ribbonActive {
            position: absolute;
            right: -5px;
            top: -5px;
            z-index: 1;
            overflow: hidden;
            width: 75px;
            height: 75px;
            text-align: right;
        }

            .ribbonActive span {
                font-size: 13px;
                font-weight: bold;
                color: #FFF;
                text-transform: uppercase;
                text-align: center;
                line-height: 20px;
                transform: rotate(45deg);
                -webkit-transform: rotate(45deg);
                width: 100px;
                display: block;
                background: #79A70A;
                background: linear-gradient(#9BC90D 0%, #79A70A 100%);
                box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
                position: absolute;
                top: 19px;
                right: -21px;
            }

                .ribbonActive span::before {
                    content: "";
                    position: absolute;
                    left: 0px;
                    top: 100%;
                    z-index: -1;
                    border-left: 3px solid #79A70A;
                    border-right: 3px solid transparent;
                    border-bottom: 3px solid transparent;
                    border-top: 3px solid #79A70A;
                }

                .ribbonActive span::after {
                    content: "";
                    position: absolute;
                    right: 0px;
                    top: 100%;
                    z-index: -1;
                    border-left: 3px solid transparent;
                    border-right: 3px solid #79A70A;
                    border-bottom: 3px solid transparent;
                    border-top: 3px solid #79A70A;
                }

        .ribbonInActive {
            position: absolute;
            right: -5px;
            top: -5px;
            z-index: 1;
            overflow: hidden;
            width: 75px;
            height: 75px;
            text-align: right;
        }

            .ribbonInActive span {
                font-size: 13px;
                font-weight: bold;
                color: #FFF;
                text-transform: uppercase;
                text-align: center;
                line-height: 20px;
                transform: rotate(45deg);
                -webkit-transform: rotate(45deg);
                width: 100px;
                display: block;
                background: #79A70A;
                background: linear-gradient(#F70505 0%, #8F0808 100%);
                box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
                position: absolute;
                top: 19px;
                right: -21px;
            }

                .ribbonInActive span::before {
                    content: "";
                    position: absolute;
                    left: 0px;
                    top: 100%;
                    z-index: -1;
                    border-left: 3px solid #8F0808;
                    border-right: 3px solid transparent;
                    border-bottom: 3px solid transparent;
                    border-top: 3px solid #8F0808;
                }

                .ribbonInActive span::after {
                    content: "";
                    position: absolute;
                    right: 0px;
                    top: 100%;
                    z-index: -1;
                    border-left: 3px solid transparent;
                    border-right: 3px solid #8F0808;
                    border-bottom: 3px solid transparent;
                    border-top: 3px solid #8F0808;
                }
    </style>
</head>
<body>

    <form id="form1" defaultfocus="imgBtnLogin" runat="server" class="form-horizontal">
        <%--Boootstrap html goes here--%>
        <div class="container">
            <div class="row" style="margin-right:0px !important;margin-left:0px!important;">
                <div class="is-Responsive">
                    <div class="card  mx-auto   mb-5  ">
                        <div class="card-header-pills text-center">
                            <img src="/images/loginImage/oasias-logo-login.png">
                        </div>
                        <div class="card-body">

                            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager2" runat="Server">
                            </ajaxToolkit:ToolkitScriptManager>
                            <table width="100%">
                                <tr>
                                    <td colspan="4" class="title">Leave Approval
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4"><span class="field-label">Employee Name</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblEmpName" runat="server" CssClass="field-value" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><span class="field-label">Leave Type</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblLeaveType" runat="server" CssClass="field-value" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><span class="field-label">Leave Period</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblPeriod" runat="server" CssClass="field-value" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><span class="field-label">Leave Days</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblDays" runat="server" CssClass="field-value" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><span class="field-label">Leave Details</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:TextBox ID="txtLeaveDetail" runat="server" CssClass="field-value" TextMode="MultiLine" Enabled="false" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="title">Leave Eligibility
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%"><span class="field-label">Availed</span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblAvailed" runat="server" CssClass="field-value"  Width="100%"></asp:Label></td>
                                    <td width="20%"><span class="field-label">Balance</span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblBalance" runat="server" CssClass="field-value" Width="100%" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Comments</span></td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtComments" runat="server" Width="100%" TextMode="MultiLine" Placeholder="Line Manager's Comment..."></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btn1" runat="server" Text="Approve" CssClass="button" data-toggle="modal" data-target="#ModalAccept" OnClientClick="return false;" />
                                        <asp:Button ID="btn2" runat="server" Text="  Reject  " CssClass="redButton" data-toggle="modal" data-target="#ModalReject" OnClientClick="return false;" />
                                    </td>
                                </tr>
                            </table>
                            <div id="RibbonApproved" class="ribbonActive" runat="server" style="display: none">
                                <span>Approved</span>
                            </div>
                            <div id="RibbonRejected" class="ribbonInActive" runat="server" style="display: none">
                                <span>Rejected</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ModalAccept" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="title" width="100%">Approve Leave</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to approve the leave ? </p>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="button" OnClick="btnApprove_Click" />
                        <input type="button" class="button" data-dismiss="modal" value="Cancel" />
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="ModalReject" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="title" width="100%">Reject Leave</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to reject the leave ? </p>
                    </div>
                    <div class="modal-footer" style="text-align: center">
                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="redButton" OnClick="btnReject_Click" />
                        <input type="button" class="button" data-dismiss="modal" value="Cancel" />
                    </div>
                </div>

            </div>
        </div>
    </form>

    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>
</html>





