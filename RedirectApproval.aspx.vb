﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class RedirectApproval
    Inherits System.Web.UI.Page
    Dim encr As New SHA256EncrDecr

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim url As String
        'url = String.Format("Modulelogin.aspx")
        url = String.Format(encr.Decrypt_SHA256(Request.QueryString("Rpage")))
        'Session("sModule") = encr.Decrypt_SHA256(Request.QueryString("Module"))
        Session("sModule") = Request.QueryString("Module")
        Session("Modulename") = encr.Decrypt_SHA256(Request.QueryString("Modulename"))
        StoreSettings()
        Response.Redirect(url)
    End Sub
    Sub StoreSettings()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC update_module '" + Session("sUsr_name") + "','" + Session("sModule") + "'"
        SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Session("modPage") = "1"
    End Sub
End Class


