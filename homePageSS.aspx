<%@ Page Language="VB" MasterPageFile="~/mainMasterPageSS.master"   AutoEventWireup="false" CodeFile="homePageSS.aspx.vb" Inherits="homePageSS" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ MasterType  VirtualPath="~/mainMasterPageSS.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%--<%@ Register src="ConsultantDatabase/Usercontrol/CD_homeDisplay.ascx" tagname="CD_homeDisplay" tagprefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  
<table border="0" cellpadding="0" cellspacing="0" align="center"  style="width: 100%"> 
<tr valign="top">
    <td width="75%" style="height: 259px" >
        <table border="0" cellpadding="0" cellspacing="0" align="center"  
            style="width: 100%; padding-right: 25px; padding-left: 15px; height: 0px;"> 
        <tr valign="top">
            <td width="100%" align="left" >
                <table align="right" style="width: 17%; height: 4px;">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="matters">
                            <asp:RadioButton ID="rbGridView" runat="server" Checked="True" GroupName="View" 
                                Text="Grid" AutoPostBack="True" Visible="False" />
                        </td>
                        <td align="center" class="matters">
                            <asp:RadioButton ID="rbListView" runat="server" GroupName="View" Text="List" 
                                AutoPostBack="True" Visible="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top" id="trTransaction" runat="server">
            <td width="100%" style="border-bottom: 1px solid #1B80B6; height: 22px" 
                align="left" >
            <table width="100%"  >
            <tr>
            <td align="left" width ="80%">
                <asp:Image ID="Image4" runat="server" Height="18px" 
                    ImageUrl="~/Images/ButtonImages/Transaction.png" Width="16px" />
             <asp:Label ID="lblTranHead" runat="server" 
                    style="left: 2px; position: relative; top: 0px" EnableViewState="False" 
                             CssClass="matters" Font-Size="Small" Font-Italic="True">Transactions</asp:Label>
            </td>
            <td align="right" width ="20%">
                            <asp:LinkButton ID="lnkTransactionMore" runat="server" Font-Italic="True">more..</asp:LinkButton>
            </td>
            </tr>
            </table>
            </td>
        </tr>
         <tr valign="top" onmouseover="this.className='normalActive'" onmouseout="this.className='normal'">
            <td width="100%" style="height: 10px" align="left" >
                <asp:DataList ID="dlTransaction" runat="server" RepeatColumns="5" 
                    RepeatDirection="Horizontal" Width="100%">
                    <ItemTemplate>
                        <asp:table id ="tblItem"    runat="server" style="width:100%; height: 51px;" onmouseover="this.className='normalItemActive'" onmouseout="this.className='normalItemInActive'" >
                            <asp:tablerow ID="Tablerow1" runat="server">
                                <asp:tablecell ID="Tablecell1" align="center" style="width: 48px; height: 26px" runat="server">
                                    <asp:ImageButton ID="imgMenuImage" runat="server" Height="48px" 
                                        ImageUrl='<%# Bind("MNU_IMAGE") %>' Width="48px" />
                                </asp:tablecell>
                                <asp:tablecell ID="Tablecell2" style="height: 26px; width: 558px;" runat="server">
                                    <asp:LinkButton ID="lblMenuText" runat="server" Font-Italic="True" 
                                        Font-Size="X-Small" Font-Underline="False" Text='<%# Bind("MNU_TEXT") %>'></asp:LinkButton>
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                            <asp:tablerow ID="Tablerow2" runat="server">
                                <asp:tablecell ID="Tablecell3" style="width: 48px; height: 12px" runat="server">
                                </asp:tablecell>
                                <asp:tablecell ID="Tablecell4" style="height: 12px; width: 558px;" runat="server">
                                    <asp:HiddenField ID="hdnMenuLink" runat="server" Value='<%# Bind("MNU_NAME") %>'  /> 
                                    <asp:HiddenField ID="hdnMenuCode" runat="server" Value='<%# Bind("MNU_CODE") %>' /> 
                                    <asp:HiddenField ID="hdnMenuDescr" runat="server" Value='<%# Bind("MNU_DESCR") %>' /> 
                                </asp:tablecell>
                                <asp:tablecell ID="Tablecell5" style="height: 12px" runat="server">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                        </asp:table>
                    </ItemTemplate>
                </asp:DataList>
                </td>
        </tr>
        <tr valign="top" id="trApproval" runat="server">
            <td width="100%" style="border-bottom: 1px solid #1B80B6; height: 22px" 
                align="left"  >
            <table width="100%"  >
            <tr>
            <td align="left" width ="80%">
                <asp:Image ID="Image3" runat="server" Height="18px" 
                    ImageUrl="~/Images/ButtonImages/Approve.png" Width="16px" />
             <asp:Label ID="lblApprHead" runat="server" 
                    style="left: 2px; position: relative; top: 0px" EnableViewState="False" 
                             CssClass="matters" Font-Size="Small" Font-Italic="True">Approval</asp:Label>

            </td>
              <td align="right" width ="20%">
                            <asp:LinkButton ID="lnkApproveMore" runat="server" Font-Italic="True">more..</asp:LinkButton>
              </td>
            </tr>
            </table>
            </td>
        </tr>
        <tr valign="top" onmouseover="this.className='normalActive'" onmouseout="this.className='normal'">
            <td width="100%" style="height: 10px" align="left" >
                <asp:DataList ID="dlApproval" runat="server" RepeatColumns="5" 
                    RepeatDirection="Horizontal" Width="100%">
                    <ItemTemplate>
                        <asp:table id ="tblItem"    runat="server" style="width:100%; height: 51px;" onmouseover="this.className='normalItemActive'" onmouseout="this.className='normalItemInActive'" >
                            <asp:tablerow runat="server">
                                <asp:tablecell align="center" style="width: 48px; height: 26px" runat="server">
                                    <asp:ImageButton ID="imgMenuImage" runat="server" Height="48px" 
                                        ImageUrl='<%# Bind("MNU_IMAGE") %>' Width="48px" />
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px; width: 558px;" runat="server">
                                    <asp:LinkButton ID="lblMenuText" runat="server" Font-Italic="True" 
                                        Font-Size="X-Small" Font-Underline="False" Text='<%# Bind("MNU_TEXT") %>'></asp:LinkButton>
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                            <asp:tablerow runat="server">
                                <asp:tablecell style="width: 48px; height: 12px" runat="server">
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px; width: 558px;" runat="server">
                                    <asp:HiddenField ID="hdnMenuLink" runat="server" Value='<%# Bind("MNU_NAME") %>'  /> 
                                    <asp:HiddenField ID="hdnMenuCode" runat="server" Value='<%# Bind("MNU_CODE") %>' /> 
                                    <asp:HiddenField ID="hdnMenuDescr" runat="server" Value='<%# Bind("MNU_DESCR") %>' /> 
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px" runat="server">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                        </asp:table>
                    </ItemTemplate>
                </asp:DataList>
                </td>
        </tr>
        <tr valign="top" id="trPersonal" runat="server">
            <td width="100%" style="border-bottom: 1px solid #1B80B6; height: 21px" 
                align="left"  >
            <table width="100%"  >
            <tr valign="top">
                <td width="100%" align="left" >
            
                <asp:Image ID="Image5" runat="server" Height="18px" 
                    ImageUrl="~/Images/ButtonImages/Personal.png" Width="16px" />
             <asp:Label ID="lblPersHead" runat="server" 
                    style="left: 2px; position: relative; top: 0px" EnableViewState="False" 
                             CssClass="matters" Font-Size="Small" Font-Italic="True">Personal</asp:Label>

            </td>
              <td align="right" width ="20%">
                            <asp:LinkButton ID="lnkPersonalMore" runat="server" Font-Italic="True">more..</asp:LinkButton>
              </td>
            </tr>
            </table>
            </td>
            </tr>
        <tr valign="top" onmouseover="this.className='normalActive'" onmouseout="this.className='normal'">
            <td width="100%" align="left" >
                <asp:DataList ID="dlPersonal" runat="server" RepeatColumns="5" 
                    RepeatDirection="Horizontal" Width="100%">
                    <ItemTemplate>
                        <asp:table id ="tblItem"    runat="server" style="width:100%; height: 51px;" onmouseover="this.className='normalItemActive'" onmouseout="this.className='normalItemInActive'" >
                            <asp:tablerow runat="server">
                                <asp:tablecell align="center" style="width: 48px; height: 26px" runat="server">
                                    <asp:ImageButton ID="imgMenuImage" runat="server" Height="48px" 
                                        ImageUrl='<%# Bind("MNU_IMAGE") %>' Width="48px" />
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px; width: 558px;" runat="server">
                                    <asp:LinkButton ID="lblMenuText" runat="server" Font-Italic="True" 
                                        Font-Size="X-Small" Font-Underline="False" Text='<%# Bind("MNU_TEXT") %>'></asp:LinkButton>
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                            <asp:tablerow runat="server">
                                <asp:tablecell style="width: 48px; height: 12px" runat="server">
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px; width: 558px;" runat="server">
                                    <asp:HiddenField ID="hdnMenuLink" runat="server" Value='<%# Bind("MNU_NAME") %>'  /> 
                                    <asp:HiddenField ID="hdnMenuCode" runat="server" Value='<%# Bind("MNU_CODE") %>' /> 
                                    <asp:HiddenField ID="hdnMenuDescr" runat="server" Value='<%# Bind("MNU_DESCR") %>' /> 
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px" runat="server">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                        </asp:table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr valign="top" id="trReports" runat="server">
            <td width="100%"  style="border-bottom-color: #1B80B6; border-bottom-width: 1px; border-bottom-style: solid;" 
                align="left" >
            <table width="100%"  >
            <tr>
            <td align="left" width ="80%">
                <asp:Image ID="Image6" runat="server" Height="18px" 
                    ImageUrl="~/Images/ButtonImages/Reports.png" Width="16px" />
             <asp:Label ID="lblReportsHead" runat="server" 
                    style="left: 2px; position: relative; top: 0px" EnableViewState="False" 
                             CssClass="matters" Font-Size="Small" Font-Italic="True">Reports</asp:Label>
            </td>
              <td align="right" width ="20%">
                            <asp:LinkButton ID="lnkReportsMore" runat="server" Font-Italic="True">more..</asp:LinkButton>
              </td>
            </tr>
            </table>
            </td>
        </tr>
        <tr valign="top" onmouseover="this.className='normalActive'" onmouseout="this.className='normal'">
            <td width="100%" align="left" >
                <asp:DataList ID="dlReports" runat="server" RepeatColumns="5" 
                    RepeatDirection="Horizontal" Width="100%" Height="16px">
                    <ItemTemplate>
                        <asp:table id ="tblItem"    runat="server" style="width:100%; height: 51px;" onmouseover="this.className='normalItemActive'" onmouseout="this.className='normalItemInActive'" >
                            <asp:tablerow runat="server">
                                <asp:tablecell align="center" style="width: 48px; height: 26px" runat="server">
                                    <asp:ImageButton ID="imgMenuImage" runat="server" Height="48px" 
                                        ImageUrl='<%# Bind("MNU_IMAGE") %>' Width="48px" />
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px; width: 558px;" runat="server">
                                    <asp:LinkButton ID="lblMenuText" runat="server" Font-Italic="True" 
                                        Font-Size="X-Small" Font-Underline="False" Text='<%# Bind("MNU_TEXT") %>'></asp:LinkButton>
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                            <asp:tablerow runat="server">
                                <asp:tablecell style="width: 48px; height: 12px" runat="server">
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px; width: 558px;" runat="server">
                                    <asp:HiddenField ID="hdnMenuLink" runat="server" Value='<%# Bind("MNU_NAME") %>'  /> 
                                    <asp:HiddenField ID="hdnMenuCode" runat="server" Value='<%# Bind("MNU_CODE") %>' /> 
                                    <asp:HiddenField ID="hdnMenuDescr" runat="server" Value='<%# Bind("MNU_DESCR") %>' /> 
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px" runat="server">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                        </asp:table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        
        
        
          <tr valign="top" id="trHelp" runat="server">
            <td width="100%"  style="border-bottom-color: #1B80B6; border-bottom-width: 1px; border-bottom-style: solid;" 
                align="left" >
            <table width="100%"  >
            <tr>
            <td align="left" width ="80%">
                <asp:Image ID="Image7" runat="server" Height="18px" 
                    ImageUrl="~/Images/ButtonImages/Help.png" Width="16px" />
             <asp:Label ID="lblHelpHead" runat="server" 
                    style="left: 2px; position: relative; top: 0px" EnableViewState="False" 
                             CssClass="matters" Font-Size="Small" Font-Italic="True">Helpdesk</asp:Label>
            </td>
              <td align="right" width ="20%">
                            <asp:LinkButton ID="lnkHelpMore" runat="server" Font-Italic="True">more..</asp:LinkButton>
              </td>
            </tr>
            </table>
            </td>
        </tr>
        <tr valign="top" onmouseover="this.className='normalActive'" onmouseout="this.className='normal'">
            <td width="100%" align="left" >
                <asp:DataList ID="dlHelp" runat="server" RepeatColumns="5" 
                    RepeatDirection="Horizontal" Width="100%" Height="16px" OnItemDataBound="dlTransaction_ItemDataBound">
                    <ItemTemplate>
                        <asp:table id ="tblItem"    runat="server" style="width:100%; height: 51px;" onmouseover="this.className='normalItemActive'" onmouseout="this.className='normalItemInActive'" >
                            <asp:tablerow runat="server">
                                <asp:tablecell align="center" style="width: 48px; height: 26px" runat="server">
                                    <asp:ImageButton ID="imgMenuImage" runat="server" Height="48px" 
                                        ImageUrl='<%# Bind("MNU_IMAGE") %>' Width="48px" />
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px; width: 558px;" runat="server">
                                    <asp:LinkButton ID="lblMenuText" runat="server" Font-Italic="True" 
                                        Font-Size="X-Small" Font-Underline="False" Text='<%# Bind("MNU_TEXT") %>'></asp:LinkButton>
                                </asp:tablecell>
                                <asp:tablecell style="height: 26px">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                            <asp:tablerow runat="server">
                                <asp:tablecell style="width: 48px; height: 12px" runat="server">
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px; width: 558px;" runat="server">
                                    <asp:HiddenField ID="hdnMenuLink" runat="server" Value='<%# Bind("MNU_NAME") %>'  /> 
                                    <asp:HiddenField ID="hdnMenuCode" runat="server" Value='<%# Bind("MNU_CODE") %>' /> 
                                    <asp:HiddenField ID="hdnMenuDescr" runat="server" Value='<%# Bind("MNU_DESCR") %>' /> 
                                </asp:tablecell>
                                <asp:tablecell style="height: 12px" runat="server">
                                    &nbsp;</asp:tablecell>
                            </asp:tablerow>
                        </asp:table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        
        <tr id="trPassword" valign="top" runat="server">
            <td width="100%" align="left" >
                <asp:LinkButton ID="lbExpiry" runat="server">Your Password will expire in 20 Days</asp:LinkButton>
                </td>
        </tr>
        <tr valign="top">
            <td width="100%" style="height: 259px" align="left" >
                &nbsp;</td>
        </tr>
        </table>
    
    </td>
</tr>
</table>
</asp:Content> 