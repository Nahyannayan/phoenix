﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ErrorInfo.aspx.vb" Inherits="ErrorInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
     <div class="card card-login mx-auto text-center m-3">
                    <div class="card-header-pills badge-danger text-capitalize cover-top-radius font-weight-bold">Oops! something has happened</div>
                    <div class="card-body">
                      <p> send an email to phoenixsupport@gemseducation.com </p>
                                <p>
                                    You will be redirected after 5 second to access page.</p>
                                <p>
                                   <asp:HyperLink ID="HyperLink2" runat="server" Font-Italic="True" Font-Names="Verdana"
                                         NavigateUrl="/homepage.aspx">Click here to  return to the home page</asp:HyperLink>&nbsp;</p>
                                
    

                    </div>
                </div>

    </form>
</body>
</html>
