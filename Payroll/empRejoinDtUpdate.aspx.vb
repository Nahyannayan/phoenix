﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empRejoinDtUpdate
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    'Version        Author          Date            Change
    '1.1            Swapna          22May2011       Added screen for re-joining date updates


    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ext = ".xls" Then
                '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                ' getdataExcel(strFileName)
                UpLoadDBF()
            Else
                lblError.Text = "Please upload .xls files only."
                Exit Sub
            End If

        Else
            lblError.Text = "File not uploaded"
            Exit Sub
        End If
        'btnSave.Visible = True
        'Dim DS As New DataSet
        'MyCommand.Fill(DS)
        'Dim Dt As DataTable = DS.Tables(0)
        'gvNewdata.DataSource = Dt
        'gvNewdata.DataBind()

    End Sub
    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 4)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(0).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(1).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next

            Dim strEarnDescr As String = ""
            ColName = xltable.Columns(3).ColumnName
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEarnDescr &= IIf(strEarnDescr <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                End If
            Next

            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If

            Dim ValidStudent As New DataTable
            Dim ValidCode As New DataTable

            Dim mTable As New DataTable
            mTable = CreateDataTableRejoinDt()

            Dim rowId As Integer = 0

            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                ValidStudent = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in (" & strEmpNos & ") and emp_bsu_id='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)


                mRow = mTable.NewRow
                mRow("Id") = xlRow(0)
                mRow("EMPNO") = xlRow(1)
                mRow("EMP_Name") = xlRow(2)
                mRow("EMP_LRejoinDt") = Format(CDate(xlRow(3)), OASISConstants.DateFormat)
                mRow("EMP_BSU_ID") = Session("sBsuid")

                mRow("Delete_flag") = ""
                'mRow("EDD_EMP_ID")=
                If ValidStudent.Select("Empno='" & xlRow(1) & "'", "").Length = 0 Then
                    'mRow("NotValid") = "1"
                    'mRow("ErrorText") = "Invalid Employee No"
                    lblError.Text = "Invalid Employee No:" & xlRow(1) & ", Sl No. :" & xlRow(0) & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidStudent.Select("Empno='" & xlRow(1) & "'", "")(0)
                    mRow("EMP_ID") = drEmp("Emp_id")
                    mRow("EMP_Name") = drEmp("EMPNAME")
                    'mRow("EDD_EMP_ID") = ValidStudent.Rows(rowId).Item("Emp_id")
                    'mRow("EDD_EMP_Name") = ValidStudent.Rows(rowId).Item("EMPNAME")
                End If


                rowId = rowId + 1
                mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()
          
            gvMonthE_D.DataSource = mTable
            Session("dtDt") = mTable
            gvMonthE_D.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub
    Sub GetEmployeeNo()
        Dim cmd As New SqlCommand
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection()
        cmd.Connection = conn
        If h_EDD_EMP_ID.Value <> "" Then
            cmd.CommandText = "SELECT EMPNO,Isnull(emp_lastRejoinDT,'') emp_lastRejoinDT FROM EMPLOYEE_M WHERE EMP_bActive=1 AND EMP_BSU_ID='" & Session("sBsuid") & "' AND EMP_ID='" & h_EDD_EMP_ID.Value & "'"
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            While (dr.Read())
                txtEmpID.Text = dr("EMPNO").ToString()
                txtLRdate.Text = Format(CDate(dr("emp_lastRejoinDT").ToString), OASISConstants.DateFormat)
                'txtLRdate.Text = String.Format("{0:dd/MMM/yyyy}")
            End While
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnLoad)
        Try
            If Page.IsPostBack = False Then
                Session("dtDt") = ""
                Session("dtDt") = CreateDataTableRejoinDt()
                ViewState("datamode") = "add"
                ViewState("idTr") = 0
                Session("gDtlDataMode") = "ADD"
                gridbind()
                gvMonthE_D.Attributes.Add("bordercolor", "#1b80b6")

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ' ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
               


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153066") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    txtEmpName.Attributes.Add("readonly", "readonly")
                    ' txtType.Attributes.Add("readonly", "readonly")

                    txtEmpID.Attributes.Add("readonly", "readonly")
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            End If
          

            '----------create a list item to bind the years-------------

        Catch ex As Exception
            Errorlog(ex.Message)

        End Try

      

        'If Deduction

        'If earning type





    End Sub
    Public Function gridbind() As String


        Try
            'Dim dtInfo As DataTable = CreateDataTable()

            Dim i As Integer
            Dim dtDtTemp As New DataTable
            dtDtTemp = CreateDataTableRejoinDt()


            If Session("dtDt").Rows.Count > 0 Then
                For i = 0 To Session("dtDt").Rows.Count - 1
                    If (Session("dtDt").Rows(i)("Delete_flag") <> "DELETED") Then
                        Dim ldrTempNew As DataRow
                        ldrTempNew = dtDtTemp.NewRow
                        For j As Integer = 0 To Session("dtDt").Columns.Count - 1
                            ldrTempNew.Item(j) = Session("dtDt").Rows(i)(j)
                        Next
                        dtDtTemp.Rows.Add(ldrTempNew)
                    End If
                Next

            End If



            gvMonthE_D.DataSource = dtDtTemp
            ' gvMonthE_D.Columns(1).Visible = False
            ' gvMonthE_D.Columns(2).Visible = False
            ' gvMonthE_D.Columns(4).Visible = False
            gvMonthE_D.DataBind()
            Return "1"
        Catch ex As Exception
            Return "1"
        End Try
    End Function

    Public Function CreateDataTableRejoinDt() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.Int64"))
            Dim EMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim EMPNO As New DataColumn("EMPNO", System.Type.GetType("System.String"))
            Dim EMP_Name As New DataColumn("EMP_Name", System.Type.GetType("System.String"))
            Dim EMP_LRejoinDt As New DataColumn("EMP_LRejoinDt", System.Type.GetType("System.String"))

            Dim EMP_BSU_ID As New DataColumn("EMP_BSU_ID", System.Type.GetType("System.String"))
            Dim Delete_flag As New DataColumn("Delete_flag", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(EMP_ID)
            dtDt.Columns.Add(EMPNO)
            dtDt.Columns.Add(EMP_Name)
            dtDt.Columns.Add(EMP_LRejoinDt)
          
            dtDt.Columns.Add(EMP_BSU_ID)
            dtDt.Columns.Add(Delete_flag)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Protected Sub txtEmpName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpName.TextChanged
        GetEmployeeNo()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_connOasis)
        objConn.Open()
        Dim ret As Integer = 0
        Dim count As Integer = 0
        Dim iIndex As Integer

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If Session("dtDt").Rows.Count > 0 Then
                For iIndex = 0 To Session("dtDt").Rows.Count - 1
                    Dim dr As DataRow = Session("dtDt").Rows(iIndex)
                    count = count + 1
                    Dim SqlCmd As New SqlCommand("UpdateEmpLastRejoinDt", objConn, stTrans)
                    SqlCmd.CommandType = CommandType.StoredProcedure

                    SqlCmd.Parameters.AddWithValue("@emp_id", dr("EMP_ID")) '("EMP_ID")
                    SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
                    SqlCmd.Parameters.AddWithValue("@empNo", dr("EMPNO"))
                    SqlCmd.Parameters.AddWithValue("@RejDate", dr("EMP_LRejoinDt"))

                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    ret = SqlCmd.Parameters("@ReturnValue").Value
                    If ret <> 0 Then
                        stTrans.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(ret)
                        Exit Sub
                    End If
                    UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), dr("EMP_ID"), "Update", Page.User.Identity.Name.ToString, Me.Page, "-Last Rejoin Date Updated directly.")

                Next
            Else

                If (h_EDD_EMP_ID.Value <> "" And txtEmpID.Text <> "" And txtLRdate.Text <> "") Then
                    count = count + 1
                    Dim SqlCmd As New SqlCommand("UpdateEmpLastRejoinDt", objConn, stTrans)
                    SqlCmd.CommandType = CommandType.StoredProcedure

                    SqlCmd.Parameters.AddWithValue("@emp_id", h_EDD_EMP_ID.Value) '("EMP_ID")
                    SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
                    SqlCmd.Parameters.AddWithValue("@empNo", txtEmpID.Text)
                    SqlCmd.Parameters.AddWithValue("@RejDate", txtLRdate.Text)

                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    ret = SqlCmd.Parameters("@ReturnValue").Value
                    If ret <> 0 Then
                        stTrans.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(ret)
                        Exit Sub
                    End If
                    UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_EDD_EMP_ID.Value, "Update", Page.User.Identity.Name.ToString, Me.Page, "-Last Rejoin Date Updated directly.")
                End If
            End If

            If count = 0 Then
                stTrans.Rollback()
                lblError.Text = "Please enter required details to save."
                ret = 1000
            End If

            If ret = 0 Then
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(ret)
                'Session("dtDt") = ""
                ViewState("datamode") = "none"
                clear_details()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage("1000")
            objConn.Close()
        End Try

    End Sub
    Private Sub clear_details()
        txtEmpID.Text = ""
        txtEmpName.Text = ""
        txtLRdate.Text = ""
        h_EDD_EMP_ID.Value = ""
        Session("dtDt") = ""
        gvMonthE_D.DataSource = Nothing
        gvMonthE_D.DataBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call clear_details()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        clear_details()
        Session("dtDt") = CreateDataTableRejoinDt()
        gvMonthE_D.DataSource = Nothing
        gvMonthE_D.DataBind()

    End Sub
End Class
