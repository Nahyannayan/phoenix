﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"  CodeFile="empStaffListDownload.aspx.vb" Inherits="EmpStaffListDownload" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Staff Downloading
        </div>
        <div class="card-body">
            <div class="table-responsive">

        <div align="center">
            <img src="../images/loginimage/oasias-logo-login.png" style="height: 92px;" />
            
            <table width="100%">
                <tr>
                    <td align="left" width="20%">
                        <span class="field-label"> Business Unit </span>
                    </td>
                    <td align="left" width="30%">
                         <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
                    </td>
                    <td align="left" width="20%">
                        <asp:Button ID="btnDownload" runat="server" Text="Download Staff List" CssClass="button" OnClick="btnupload_Click" />
                    </td>
                    <td align="left" width="30%"><asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblStatus" runat="server" CssClass="error"></asp:Label><br />
                       <asp:CheckBox ID="CheckError" runat="server" AutoPostBack="True" Text="Show Error List" Visible="False" />
                              </td>
                </tr>
            </table>
            
           
           <table width="100%">
               <tr>
                   <td>

                
            <asp:GridView ID="GridDownload" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                PageSize="15" EnableModelValidation="True" Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="Image1" ImageUrl='<%# Eval("Emd_Photo") %>' runat="server" Height="32px" Width="32px" />
                        </ItemTemplate>
                        <ItemStyle Width="7%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Emp No">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# bind("[Emp No]") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Emp Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# bind("[Emp Name]") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bsu Name" Visible="False">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# bind("[Bsu Name]") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" />
            </asp:GridView>

                    
                   </td>
               </tr>
           </table> 

        </div>
    
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select a Business Unit"
        ControlToValidate="ddbsu" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <%# Eval("File_Name") %>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    <asp:HiddenField ID="HiddenPostBack" Value="0" runat="server" />

            </div>
        </div>
    </div>

</asp:Content> 
