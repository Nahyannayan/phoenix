<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="empLeaveApplicationListPopup.aspx.vb" Inherits="Payroll_empLeaveApplicationListPopup"
    Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html>
<script language="javascript" type="text/javascript">


    function ShowLeaveDetail(id) {
        //var sFeatures;
        //sFeatures="dialogWidth: 700px; ";
        //sFeatures+="dialogHeight: 450px; ";
        //sFeatures+="help: no; ";
        //sFeatures+="resizable: no; ";
        //sFeatures+="scroll: yes; ";
        //sFeatures+="status: no; ";
        //sFeatures+="unadorned: no; ";
        //var NameandCode;
        //var result;
        //result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)

        //return false;
        Popup("ShowEmpLeave.aspx?ela_id=" + id)
    }
</script>
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<script>
    function Popup(url) {
        $.fancybox({
            'width': '80%',
            'height': '40%',
            'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': url
        });
    };
    function ClosePopup(a) {
        parent.$.fancybox.close()
        parent.AcceptValues(a)

    }


</script>

<body>
    <form id="form1" runat="server">


        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>

        <div class="table-responsive">
            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr valign="top">
                    <td class="matters" valign="top" width="50%" align="left">
                        <asp:HyperLink ID="hlAddNew" runat="server" Visible="false">Add New</asp:HyperLink>
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    <td align="right" width="50%"></td>
                </tr>
            </table>
            <div class="container" style="width: 100%">
                <div class="col-sm-12">
                    <div class="form-group">

                        <div class="row">
                            <div class="col-sm-12">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="20%">
                                            <span class="field-label">Select Calendar Year&nbsp;&nbsp;</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnGO" runat="server" CssClass="button" Text="View" Visible="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <telerik:RadHtmlChart runat="server" ID="RadHtmlChart2" Skin="Metro" Width="510px">
                                    <PlotArea>
                                        <Series>
                                            <telerik:PieSeries DataFieldY="Y" NameField="X">
                                                <LabelsAppearance DataFormatString="{0}">
                                                </LabelsAppearance>

                                                <TooltipsAppearance DataFormatString="{0}">
                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                </TooltipsAppearance>
                                            </telerik:PieSeries>
                                        </Series>

                                    </PlotArea>
                                    <Legend>

                                        <Appearance Position="Left" Visible="true" />
                                    </Legend>
                                    <ChartTitle Text="Annual Leave">
                                    </ChartTitle>
                                </telerik:RadHtmlChart>
                            </div>
                            <div class="col-sm-4" style="display: none;">
                                <telerik:RadHtmlChart runat="server" ID="RadHtmlChart3" Height="250">
                                    <PlotArea>
                                        <Series>
                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                <LabelsAppearance DataFormatString="{0}">
                                                </LabelsAppearance>

                                            </telerik:DonutSeries>
                                        </Series>
                                        <YAxis>
                                        </YAxis>
                                    </PlotArea>
                                    <Legend>
                                        <Appearance Position="Right" Visible="true" />
                                    </Legend>
                                    <ChartTitle Text="Summary by Leave Type(As On Today)">
                                    </ChartTitle>
                                </telerik:RadHtmlChart>
                            </div>
                            
                             <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card">
                        <div class="card-body">
                        <telerik:RadHtmlChart runat="server" ID="ColumnChart" Height="250px" Width="500px">
                            <PlotArea>
                                <Series>
                                    <telerik:ColumnSeries DataFieldY="Annual_Leave" Stacked="true" Name="Annual_Leave">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                        <Appearance FillStyle-BackgroundColor="#00a3e0"></Appearance>
                                        <TooltipsAppearance>
                                            <ClientTemplate>Annual_Leave</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                    <telerik:ColumnSeries DataFieldY="Medical_Leave" Stacked="true" Name="Medical_Leave">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                        <Appearance FillStyle-BackgroundColor="#041e42"></Appearance>
                                        <TooltipsAppearance>
                                            <ClientTemplate>Medical_Leave</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                    <telerik:ColumnSeries DataFieldY="Emergency_Leave" Stacked="true" Name="Emergency_Leave">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                         <Appearance FillStyle-BackgroundColor="#ff6900"></Appearance>
                                        <TooltipsAppearance>
                                            <ClientTemplate>Emergency_Leave</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                    <telerik:ColumnSeries DataFieldY="Others" Stacked="true" Name="Others">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                         <Appearance FillStyle-BackgroundColor="#210124"></Appearance>
                                        <TooltipsAppearance>
                                            <ClientTemplate>Others</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                </Series>
                                <XAxis DataLabelsField="X">
                                    <MajorGridLines Visible="false"/>
                                    <MinorGridLines Visible="false" />
                                    <LabelsAppearance Visible="true" DataFormatString="MMMyy"></LabelsAppearance>
                                </XAxis>
                                <YAxis>
                                     <MajorGridLines Visible="false"/>
                                    <MinorGridLines Visible="false" />
                                </YAxis>
                            </PlotArea>
                            <Legend>
                                <Appearance Position="Bottom" Visible="true" />
                            </Legend>
                            
                            <ChartTitle Text="Leave Summary"></ChartTitle>
                        </telerik:RadHtmlChart>
                        </div>
                        </div>
                    </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div>
                                    <br />
                                </div>
                                <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" AllowPaging="false" CellSpacing="0"
                                    PageSize="5" GridLines="Both" CssClass="table table-bordered table-row" Width="100%">
                                    <MasterTableView>
                                    </MasterTableView>
                                    <HeaderStyle BackColor="#F5F5F5" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <AlternatingItemStyle HorizontalAlign="Center" />
                                    <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                                </telerik:RadGrid>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <asp:GridView ID="gvLegend" runat="server" AutoGenerateColumns="true" CssClass="table table-bordered table-row" ShowHeader="false" Style="font-size: small;">
                                </asp:GridView>
                                <asp:Label ID="lblLegend" runat="server" Text=""></asp:Label>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <table width="100%">
                <tr>
                    <td align="center" valign="top" class="matters" width="100%">
                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                            CssClass="table table-row table-bordered" Width="100%" AllowPaging="True" PageSize="30">
                            <Columns>
                                <asp:BoundField DataField="EMP_NAME" HeaderText="Name" ReadOnly="True"
                                    SortExpression="EMP_NAME"></asp:BoundField>
                                <asp:BoundField DataField="EMPNO" HeaderText="Staff ID" ReadOnly="True"  ItemStyle-HorizontalAlign="Center"
                                    SortExpression="EMPNO"></asp:BoundField>
                                <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="LEAVEDAYS" HeaderText="Leave Days" ReadOnly="True" SortExpression="LEAVEDAYS" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date From"
                                    HtmlEncode="False" SortExpression="ELA_DTFROM" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date To"
                                    HtmlEncode="False" SortExpression="ELA_DTTO" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="ELA_APPRSTATUS" HeaderText="Status" ReadOnly="True" SortExpression="ELA_APPRSTATUS" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="ELA_APPRDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Approval Date"
                                    HtmlEncode="False" SortExpression="ELA_APPRDATE" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="<%# &quot;ShowLeaveDetail('&quot; & Container.DataItem(&quot;ELA_ID&quot;) & &quot;');return false;&quot; %>"
                                            Text='<%# Bind("ELA_REMARKS") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <%-- <asp:HyperLink ID="hlEdit" runat="server" >View</asp:HyperLink>--%>
                                        <asp:LinkButton ID="lbView" runat="server" Text="View" OnClick="lbView_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Print">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="hlPrint" runat="server" Text="Print" OnClick="btnPrint_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="APPRSTATUS" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAPPRSTATUS" runat="server" Text='<%# Bind("APPRSTATUS") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
