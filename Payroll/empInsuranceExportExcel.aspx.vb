﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Math

Imports UtilityObj
Partial Class Payroll_empInsuranceExportExcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim Catid As String = ""


        If Not Request.QueryString("Catid") Is Nothing Then
            Catid = Request.QueryString("Catid")
        End If

        Dim ABCCat As String = ""
        If Not Request.QueryString("ABCCat") Is Nothing Then
            ABCCat = Request.QueryString("ABCCat")
        End If
        Dim DesID As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        ' Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "exec  rptGetDependanceInsuranceDetails  '" & Session("sBsuid") & "'")
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "exec  rptGetDependanceInsuranceDetails  '" & Session("InsuBSUIds") & "','" & Session("InsuEmpIds") & "','" & Catid & "','" & ABCCat & "','" & Session("InsuDesIds") & "'")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "exec  rptGetDependanceInsuranceDetails  '" & Session("InsuBSUIds") & "','" & Session("InsuEmpIds") & "','" & Catid & "','" & ABCCat & "','" & Session("InsuDesIds") & "'," & Session("InsuCategory") & "," & Session("Status"))


        ' Dim GridView1 As New GridView
        'GridView1.t()
        gvExport.DataSource = ds
        gvExport.DataBind()
        'gvExport.Visible = False
        ' exportnew()
        'Excel_Export()
        'ExporttoExcel(ds, "swapna")


        'gvExport = gvExport
        'Response.ClearContent()
        'Response.AddHeader("content-disposition", "attachment; filename=test.xls")
        'Response.ContentType = "application/excel"
        'Dim sw As New System.IO.StringWriter()
        'Dim htw As New HtmlTextWriter(sw)
        'gvExport.RenderControl(htw)
        'Response.Write(sw.ToString())
        'Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        Return
    End Sub


    Protected Sub gvExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExport.RowDataBound
        'For Each drow As GridViewRow In gvExport.Rows
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblNum As Label = CType(e.Row.FindControl("lblEmpDepID"), Label)
                Dim imgEmployee As Image = CType(e.Row.FindControl("imgEMployee"), Image)
                Dim RbDep As Telerik.Web.UI.RadBinaryImage = CType(e.Row.FindControl("RbDep"), Telerik.Web.UI.RadBinaryImage)
                Dim imgFileName As String = "\" + Session("susr_name") + "_Insurance_" + Session("sBsuid") + "_" + lblNum.Text + ".jpeg"
                'Dim imgFilePath As String = Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName
                If imgEmployee.AlternateText <> "" Then
                    Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "/" & imgEmployee.AlternateText
                    'imgEmployee.ImageUrl = 
                    'strImagePath = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                    Dim strPath As String = strImagePath ' GetPhotoPath()
                    Try
                        If strPath <> "" Then
                            ' System.IO.File.Copy(strPath, imgFilePath)
                        End If
                    Catch ex As Exception
                    End Try

                    'imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName

                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New  _
                      System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Try
                        Dim fullSizeImg As System.Drawing.Image
                        fullSizeImg = System.Drawing.Image.FromFile(strPath)

                        Dim thumbNailImg As System.Drawing.Image
                        thumbNailImg = fullSizeImg.GetThumbnailImage(60, 60, _
                                                               dummyCallBack, IntPtr.Zero)
                        'If File.Exists(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName) Then
                        '    lblerror.Text = "file exists - " + imgFileName
                        '    Exit Sub
                        'End If

                        thumbNailImg.Save(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName, System.Drawing.Imaging.ImageFormat.Jpeg)
                        'System.IO.File.Copy("~/Payroll/InsurancePhotos" + imgFileName, imgEmployee.ImageUrl)
                        'imgEmployee = thumbNailImg.
                        imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                        'Dim imgBytes As Byte()
                        'imgBytes = ConvertImageFiletoBytes(strImagePath)
                        RbDep.DataValue = Nothing
                        'RbDep.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                        imgEmployee.Visible = True
                        thumbNailImg.Dispose()
                    Catch ex As Exception
                        UtilityObj.Errorlog("empIsurerowdatabound", ex.Message)
                    End Try

                    'System.IO.File.Delete(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName)
                ElseIf RbDep.DataValue.Length > 0 Then
                    Try
                        Dim OC As System.Drawing.Image '= EOS_MainClass.ConvertBytesToImage(PhotoBytes)
                        OC = EOS_MainClass.ConvertBytesToImage(RbDep.DataValue)

                        Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                        dummyCallBack = New  _
                          System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)

                        Dim fullSizeImg As System.Drawing.Image
                        fullSizeImg = OC

                        Dim thumbNailImg As System.Drawing.Image
                        thumbNailImg = fullSizeImg.GetThumbnailImage(60, 60, _
                                                               dummyCallBack, IntPtr.Zero)

                        thumbNailImg.Save(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName, System.Drawing.Imaging.ImageFormat.Jpeg)


                        imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                        imgEmployee.Visible = True
                    Catch ex As Exception
                        UtilityObj.Errorlog("ins rowdatabound", ex.Message)
                    End Try
                  
                Else
                    imgEmployee.Visible = False
                End If
                ' imgEmployee.Visible = False
                'RbDep.Visible = False

            End If


        Catch ex As Exception
            UtilityObj.Errorlog("empIsurerowdatabound", ex.Message)
        End Try
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    'Create the delegate
    'Function GetPhotoPath() As String
    '    Dim lblStuName As Label = gvStudent.Rows(0).FindControl("lblStuName")
    '    Dim lblStuPhotoPath As Label = gvStudent.Rows(0).FindControl("lblStuPhotoPath")
    '    If lblStuPhotoPath.Text <> "" Then
    '        Dim strImagePath As String = WebConfigurationManager.AppSettings("UploadPhotoPath") + lblStuPhotoPath.Text
    '        Return strImagePath
    '    Else
    '        Return ""
    '    End If
    '    'imgStud.AlternateText = "No Image found"
    'End Function


    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub Excel_Export()

        Response.Clear()

        Response.Buffer = True

        Response.AddHeader("content-disposition", _
    "attachment;filename=GridViewExport.xls")

        Response.Charset = ""

        Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()

        Dim hw As New HtmlTextWriter(sw)

        gvExport.AllowPaging = False

        gvExport.DataBind()

        For i As Integer = 0 To gvExport.Rows.Count - 1

            Dim row As GridViewRow = gvExport.Rows(i)

            'Apply text style to each Row

            row.Attributes.Add("class", "textmode")

        Next

        gvExport.RenderControl(hw)



        'style to format numbers to string

        Dim style As String = "<style> .textmode " & "{ mso-number-format:\@; } </style>"

        Response.Write(style)

        Response.Output.Write(sw.ToString())

        Response.Flush()

        Response.End()

    End Sub


    'Private Sub exportnew()
    '    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    '    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    '    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    '    Dim misValue As Object = System.Reflection.Missing.Value

    '    xlApp = New Microsoft.Office.Interop.Excel.ApplicationClass()
    '    xlWorkBook = xlApp.Workbooks.Add(misValue)
    '    xlWorkSheet = DirectCast(xlWorkBook.Worksheets.get_Item(1), Microsoft.Office.Interop.Excel.Worksheet)
    '    Dim i As Integer = 0
    '    Dim j As Integer = 0

    '    For i = 0 To dataGridView1.RowCount - 1
    '        For j = 0 To dataGridView1.ColumnCount - 1

    '            Dim cell As DataGridViewCell = dataGridView1(j, i)
    '            xlWorkSheet.Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous
    '            xlWorkSheet.Columns.AutoFit()
    '            If cell.Value.[GetType]() = GetType(Bitmap) Then
    '                xlWorkSheet.Cells(i + 1, j + 1) = ReadFile(DirectCast(cell.Value, Bitmap))
    '            Else
    '                xlWorkSheet.Cells(i + 1, j + 1) = cell.Value

    '            End If
    '        Next
    '    Next

    '    xlWorkBook.SaveAs(saveFileDialog1.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, _
    '     Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
    '    xlWorkBook.Close(True, misValue, misValue)
    '    xlApp.Quit()

    '    releaseObject(xlWorkSheet)
    '    releaseObject(xlWorkBook)
    '    releaseObject(xlApp)

    'End Sub

    Protected Sub lnkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExport.Click
        Excel_Export()
    End Sub
End Class
