Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empAirfare
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'ts

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            gvAttendance.Attributes.Add("bordercolor", "#1b80b6")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130080" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If

            If Request.QueryString("viewid") <> "" Then
                btnAdddetails.Visible = False
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If


            ViewState("iID") = 0
            Session("AirFare") = CreateDataTableAirFare()

            gvAttendance.DataBind()
        End If
    End Sub

 
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT EAF_ID, EAF_CIT_ID," _
            & " EAF_DTFROM, EAF_DTTO, EAF_CLASS," _
            & " EAF_AGEGROUP, EAF_AMOUNT" _
            & " FROM EMPAIRFARE_S AS EF" _
            & " WHERE     (EAF_BSU_ID = '" & Session("sBSUID") & "')" _
            & " AND EF.EAF_ID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddCity.DataBind()
                'h_Emp_No.Value = ds.Tables(0).Rows(0)("ELV_EMP_ID")
                ddCity.SelectedIndex = -1
                ddAgegroup.SelectedIndex = -1
                ddAirfareclass.SelectedIndex = -1
                ddCity.Items.FindByValue(ds.Tables(0).Rows(0)("EAF_CIT_ID").ToString).Selected = True
                ddAgegroup.Items.FindByValue(ds.Tables(0).Rows(0)("EAF_AGEGROUP").ToString).Selected = True
                ddAirfareclass.Items.FindByValue(ds.Tables(0).Rows(0)("EAF_CLASS").ToString).Selected = True
                txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("EAF_DTFROM")), "dd/MMM/yyyy")
                txtAmount.Text = ds.Tables(0).Rows(0)("EAF_AMOUNT")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
 

    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        btnAdddetails.Enabled = False
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
        ddCity.Enabled = False
        ddAgegroup.Enabled = False
        ddAirfareclass.Enabled = False
    End Sub
 

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        btnAdddetails.Enabled = True
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        ddCity.Enabled = True
        ddAgegroup.Enabled = True
        ddAirfareclass.Enabled = True
    End Sub

 
    Sub clear_All()
        txtFrom.Text = ""
        txtAmount.Text = ""
        Session("AirFare").Rows.Clear()
    End Sub
 

    Sub gridbind()
        gvAttendance.DataSource = Session("AirFare")
        gvAttendance.DataBind()
    End Sub

  
    Sub gridbind_add()
        Try
            Dim dtFrom As Date
            ' "Id","Cityid",  "FDate", "FareClass" "AgeGroup", "Amount",FareClassname,Cityname
            dtFrom = CDate(txtFrom.Text)
            Dim dr As DataRow
            dr = Session("AirFare").NewRow
            dr("Id") = ViewState("iID")
            ViewState("iID") = ViewState("iID") + 1
            dr("Cityid") = ddCity.SelectedItem.Value
            dr("FDate") = dtFrom
            dr("FareClass") = ddAirfareclass.SelectedItem.Value
            dr("AgeGroup") = ddAgegroup.SelectedItem.Value

            dr("FareClassname") = ddAirfareclass.SelectedItem.Text
            dr("Cityname") = ddCity.SelectedItem.Text

            If IsNumeric(txtAmount.Text) Then
                dr("Amount") = txtAmount.Text
            Else
                lblError.Text = "Please Check Amount"
                Exit Sub
            End If
            Session("AirFare").Rows.Add(dr)
            gvAttendance.DataSource = Session("AirFare")
            gvAttendance.DataBind()

        Catch ex As Exception
            lblError.Text = "Check Date"
        End Try

    End Sub

 
    Public Shared Function CreateDataTableAirFare() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            ' EAF_CIT_ID, EAF_DTFROM , ,EAF_CLASS ,EAF_AGEGROUP,EAF_AMOUNT 
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cCityid As New DataColumn("Cityid", System.Type.GetType("System.String"))
            Dim cCityname As New DataColumn("Cityname", System.Type.GetType("System.String"))
            Dim FDate As New DataColumn("FDate", System.Type.GetType("System.DateTime"))
            Dim cFareClassname As New DataColumn("FareClassname", System.Type.GetType("System.String"))

            Dim cFareClass As New DataColumn("FareClass", System.Type.GetType("System.String"))
            Dim cAgeGroup As New DataColumn("AgeGroup", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            ' Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            'Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            'System.DateTime"Id", "Remarks" "EMP_ID",  "EMP_NAME" "Leavetype", "FDate", "TDate",    "Status"  
            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cCityid)
            dtDt.Columns.Add(cCityname)
            dtDt.Columns.Add(FDate)
            dtDt.Columns.Add(cFareClass)
            dtDt.Columns.Add(cAgeGroup)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cFareClassname)
            'dtDt.Columns.Add(cLeavetype_name)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Session("AirFare").Rows.Count = 0 And ViewState("datamode") <> "edit" Then
            lblError.Text = "There is no data to save"
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim eaf_id As String
            Dim edit_bool As Boolean

            If ViewState("datamode") = "edit" Then
                edit_bool = True
                Dim dtFrom As Date
                ' "Id","Cityid",  "FDate", "FareClass" "AgeGroup", "Amount",FareClassname,Cityname
                dtFrom = CDate(txtFrom.Text)

                eaf_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))


                retval = PayrollFunctions.SaveEMPAIRFARE_S(Session("sbsuid"), eaf_id, ddCity.SelectedItem.Value, _
                                            ddAirfareclass.SelectedItem.Value, txtFrom.Text, _
                                            ddAgegroup.SelectedItem.Value, _
                                            txtAmount.Text, edit_bool, stTrans)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "EAF ID -" & eaf_id, _
                     "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

            Else
                edit_bool = False
                eaf_id = 0

            End If
            ' "Id","Cityid",  "FDate", "FareClass" "AgeGroup", "Amount",FareClassname,Cityname

            For i As Integer = 0 To Session("AirFare").Rows.Count - 1
                retval = PayrollFunctions.SaveEMPAIRFARE_S(Session("sbsuid"), eaf_id, Session("AirFare").Rows(i)("Cityid"), _
                             Session("AirFare").Rows(i)("FareClass"), Session("AirFare").Rows(i)("FDate"), _
                             Session("AirFare").Rows(i)("AgeGroup"), _
                             Session("AirFare").Rows(i)("Amount"), _
                             edit_bool, stTrans)
                ''''''''



                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                Session("AirFare").Rows(i)("Cityid") & " - " & Session("AirFare").Rows(i)("FareClass") & " - " _
                & Session("AirFare").Rows(i)("FDate") & " - " & Session("AirFare").Rows(i)("AgeGroup") & " - " _
                & Session("AirFare").Rows(i)("Amount"), "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                '  
                '''''''
                If retval <> "0" Then
                    lblError.Text = getErrorMessage(retval)
                    Exit Sub
                End If
            Next

            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
                gridbind()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
 

    Protected Sub gvAttendance_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAttendance.RowDataBound
        Try
            Dim ddMonthstatusgrd As New DropDownList
            Dim lblId As New Label
            ddMonthstatusgrd = e.Row.FindControl("ddMonthstatusgrd")
            lblId = e.Row.FindControl("lblLeave")
            ddMonthstatusgrd.Items.FindByValue(lblId.Text).Selected = True
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdddetails.Click

        gridbind_add()
    End Sub

 
    Protected Sub gvAttendance_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvAttendance.RowDeleting
        Try
            Dim row As GridViewRow = gvAttendance.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("AirFare").Rows.Count - 1
                If iIndex = Session("AirFare").Rows(iRemove)(0) Then
                    Session("AirFare").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Record cannot be Deleted"
        End Try
    End Sub
 

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        btnAdddetails.Enabled = True
        'tr_Deatailhead.Visible = True
        'tr_Deatails.Visible = True 
        imgFrom.Enabled = True
        ddCity.Enabled = True
        ddAgegroup.Enabled = True
        ddAirfareclass.Enabled = True
    End Sub

 
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

 
    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub
 

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

  
    Protected Sub gvAttendance_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvAttendance.RowEditing
        Try
            gvAttendance.EditIndex = e.NewEditIndex
            Dim row As GridViewRow = gvAttendance.Rows(e.NewEditIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("AirFare").Rows.Count - 1
                If iIndex = Session("AirFare").Rows(iRemove)(0) Then
                    Session("AirFare").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Record cannot be Deleted"
        End Try
    End Sub


End Class
