Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Payroll_empMasterDetail_DocumentsView
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or _
            (ViewState("MainMnu_code") <> "P050130") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, _
                ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
                gvEMPDETAILS.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
                If Request.QueryString("deleted") <> "" Then
                    lblError.Text = getErrorMessage("520")
                End If
                ViewState("datamode") = Encr_decrData.Encrypt("add")
            End If
        End If
    End Sub

    Public Function returnpath(ByVal p_bPhotos As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_bPhotos)
            If p_bPhotos Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvEMPDETAILS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvEMPDETAILS.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvEMPDETAILS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPDETAILS.PageIndexChanging
        gvEMPDETAILS.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvEMPDETAILS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPDETAILS.RowDataBound
        Try
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            Dim cmdCol As Integer = gvEMPDETAILS.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                If Session("sBSUId") <> "800017" Then
                    hlview.NavigateUrl = "empMasterDetail_Documents.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                Else
                    hlview.NavigateUrl = "empMasterDetail_Documents_Aqaba.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End If

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrEmpDOJ As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrPassportNo As String = String.Empty
        Dim lstrDesignation As String = String.Empty
        Dim lstrVisaID As String = String.Empty

        Dim lstrFiltDOJ As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltPassportNo As String = String.Empty
        Dim lstrFiltDesignation As String = String.Empty
        Dim lstrFiltVisaID As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        If gvEMPDETAILS.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDOJ")
            lstrEmpDOJ = Trim(txtSearch.Text)
            If (lstrEmpDOJ <> "") Then lstrFiltDOJ = SetCondn(lstrOpr, "EMP_JOINDT", lstrEmpDOJ)
            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EMPNO", lstrEMPNo)
            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)
            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPassportNo")
            lstrPassportNo = txtSearch.Text
            If (lstrPassportNo <> "") Then lstrFiltPassportNo = SetCondn(lstrOpr, "EMP_PASSPORT", lstrPassportNo)
            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDesignation")
            lstrDesignation = txtSearch.Text
            If (lstrDesignation <> "") Then lstrFiltDesignation = SetCondn(lstrOpr, "CATEGORY_DESC", lstrDesignation)
            larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtVisaID")
            lstrVisaID = txtSearch.Text
            If (lstrVisaID <> "") Then lstrFiltVisaID = SetCondn(lstrOpr, "EMP_VISA_ID", lstrVisaID)
        End If
        str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT, EMP_VISA_ID, " & _
        " EMP_PASSPORT, EMP_STATUS_DESCR, CATEGORY_DESC as EMP_DES_DESCR," & _
        " CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 0 ELSE 1 END bPhotoExists  " & _
        " FROM vw_OSO_EMPLOYEEMASTER" & _
        " WHERE (EMP_VISA_BSU_ID = '" & Session("sBsuid") & "' " & _
        " or EMP_VISA_BSU_ID in (SELECT UBL_BSU_ID FROM USR_BSULIST where UBL_MNU_ID='" & ViewState("MainMnu_code") & "' and UBL_USR_NAME='" & Session("sUsr_name") & "'))" & _
        str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName & _
        lstrFiltDesignation & lstrFiltPassportNo & lstrFiltVisaID & " order by EMP_STATUS_DESCR, EMPNO, EMPNAME, CATEGORY_DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        gvEMPDETAILS.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEMPDETAILS.DataBind()
            Dim columnCount As Integer = gvEMPDETAILS.Rows(0).Cells.Count

            gvEMPDETAILS.Rows(0).Cells.Clear()
            gvEMPDETAILS.Rows(0).Cells.Add(New TableCell)
            gvEMPDETAILS.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEMPDETAILS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEMPDETAILS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvEMPDETAILS.DataBind()
        End If

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDOJ")
        txtSearch.Text = lstrEmpDOJ

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPassportNo")
        txtSearch.Text = lstrPassportNo

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtVisaID")
        txtSearch.Text = lstrVisaID

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDesignation")
        txtSearch.Text = lstrDesignation

        gvEMPDETAILS.SelectedIndex = p_selected_id
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
