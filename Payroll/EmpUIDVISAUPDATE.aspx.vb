﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data

Imports System.Collections.Generic
Partial Class Payroll_EmpUIDVISAUPDATE
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "U000069") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    bindEmirate()
                    FillEMPDETAILS()


                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If
    End Sub

    Protected Sub ddlEmirate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmirate.SelectedIndexChanged
        Try
            If ddlEmirate.SelectedValue <> "0" Then
                bindArea()
            End If
        Catch ex As Exception

        End Try

    End Sub
    Sub FillEMPDETAILS()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param(2) As SqlClient.SqlParameter
       
        param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))


        Dim ds As DataSet
        Dim dt As DataTable

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_EMP_VISA_EID_DETAILS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            dt = ds.Tables(0)

            If dt.Rows.Count > 0 Then
                lblEmployeeName.Text = dt.Rows(0)("EMP_DISPLAYNAME").ToString
                lblEmpNo.Text = dt.Rows(0)("EMPNo").ToString
                txtDepUidno.Text = Convert.ToString(dt.Rows(0)("EMP_UIDNO"))

                ''by nahyan on 24feb2019 to eable eid for non uae schools
                If Convert.ToString(Session("BSU_COUNTRY_ID")) <> "172" Then
                    txtEIDNo.Text = Convert.ToString(dt.Rows(0)("EIDNO"))
                    txtEIDNo.Visible = True

                    txtEIDNo1.Visible = False
                    txtEIDNo2.Visible = False
                    txtEIDNo3.Visible = False
                    txtEIDNo4.Visible = False
                Else

                    txtEIDNo.Visible = False

                    If Convert.ToString(dt.Rows(0)("EIDNO")) <> "" AndAlso Convert.ToString(dt.Rows(0)("EIDNO")).Length >= 15 Then
                        txtEIDNo1.Text = Convert.ToString(dt.Rows(0)("EIDNO")).Substring(0, 3)
                        txtEIDNo2.Text = Convert.ToString(dt.Rows(0)("EIDNO")).Substring(3, 4)
                        txtEIDNo3.Text = Convert.ToString(dt.Rows(0)("EIDNO")).Substring(7, 7)
                        txtEIDNo4.Text = Convert.ToString(dt.Rows(0)("EIDNO")).Substring(14, 1)
                        ''by 
                        'ElseIf Convert.ToString(Session("sBsuid")) = "800444" Or Convert.ToString(Session("sBsuid")) = "800111" Then
                        '    txtEIDNo.Visible = True
                        '    txtEIDNo.Text = Convert.ToString(dt.Rows(0)("EIDNO"))
                        '    txtEIDNo1.Visible = False
                        '    txtEIDNo2.Visible = False
                        '    txtEIDNo3.Visible = False
                        '    txtEIDNo4.Visible = False
                    End If

                End If

                txtEIDExpryDT.Text = Convert.ToString(dt.Rows(0)("EIDEXPDATE"))
                txtDepVissueplace.Text = Convert.ToString(dt.Rows(0)("VISAISSUEPLACE"))
                txtDepVissuedate.Text = Convert.ToString(dt.Rows(0)("VISAISUEDATE"))
                txtDepVexpdate.Text = Convert.ToString(dt.Rows(0)("VISAEXPDATE"))
                txtAlternateEmail.Text = dt.Rows(0)("EMD_Personal_EmailID").ToString
                txtPhoneNumber.Text = dt.Rows(0)("EMD_CUR_MOBILE").ToString
                txtPassportNo.Text = dt.Rows(0)("PASSPORTNO").ToString
                txtPPIssuedPlace.Text = dt.Rows(0)("PASSPORTISSUEPLACE").ToString
                txtPPIssueDate.Text = dt.Rows(0)("PASSPORTISUEDATE").ToString
                txtPPEXpiry.Text = dt.Rows(0)("PASSPORTEXPDATE").ToString


                txtInsCardNum.Text = dt.Rows(0)("emp_insurance_number").ToString
                If dt.Rows(0)("EMP_VISA_SPONSOR").ToString <> "" Then
                    ddlSponsor.ClearSelection()
                    If dt.Rows(0)("EMP_VISATYPE").ToString = "0" Then
                        ddlSponsor.Items.FindByValue("1").Selected = True
                        ddlSponsor.Enabled = False
                    Else
                        ddlSponsor.Enabled = True
                        ddlSponsor.Items.FindByValue(dt.Rows(0)("EMP_VISA_SPONSOR").ToString).Selected = True
                    End If
                Else
                    ddlSponsor.ClearSelection()
                    If dt.Rows(0)("EMP_VISATYPE").ToString = "0" Then
                        ddlSponsor.Items.FindByValue("1").Selected = True
                        ddlSponsor.Enabled = False
                    End If
                End If

                If dt.Rows(0)("EMP_EMR_ID").ToString <> "" Then
                    ddlEmirate.ClearSelection()
                    ddlEmirate.Items.FindByValue(dt.Rows(0)("EMP_EMR_ID").ToString).Selected = True
                End If

                If dt.Rows(0)("EMP_EMA_ID").ToString <> "" Then
                    bindArea()
                    ddlArea.ClearSelection()
                    ddlArea.Items.FindByValue(dt.Rows(0)("EMP_EMA_ID").ToString).Selected = True
                End If
                hdnCTYId.Value = dt.Rows(0)("emp_Cty_id").ToString
                ''enable visa details if VISA is not GEMS
                If dt.Rows(0)("EMP_VISATYPE").ToString = "0" AndAlso dt.Rows(0)("EMP_VISASTATUS").ToString = "2" Then
                    txtDepVissuedate.Enabled = False
                    txtDepVexpdate.Enabled = False
                    txtDepVissueplace.Enabled = False
                Else
                    txtDepVissuedate.Enabled = True
                    txtDepVexpdate.Enabled = True
                    txtDepVissueplace.Enabled = True
                End If

            End If

            End If


    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs)
        Dim errormsg As String = String.Empty
        divDrill.Visible = False
        If callTransaction(errormsg) <> 0 Then
            '' lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:green;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

            lblError.Attributes.Add("class", "alert alert-error")
            lblError.InnerHtml = "<div style='font-weight: bold;'>" & errormsg & "</div>"

        Else

            '' Response.Redirect("/PHOENIXBETA/ESSDashboard.aspx", False)
            '' lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"

            lblError.Attributes.Add("class", "alert alert-success")
            lblError.InnerHtml = "<div style='font-weight: bold;'>Record updated successfully.</div>"
            'divDrill.Visible = True
            'lbluerror.InnerHtml = "<div style='padding:5pt;font-weight:bold;'>Record updated successfully.</div>"
        End If
    End Sub
    Protected Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        divDrill.Visible = False
        Response.Redirect("/PHOENIXBETA/ESSDashboard.aspx", False)
    End Sub

    Protected Sub btClose_Click(sender As Object, e As EventArgs) Handles btClose.Click
        divDrill.Visible = False
        ''Response.Redirect("/PHOENIXBETA/ESSDashboard.aspx", False)
    End Sub
    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction
        If txtEIDNo1.Text <> "" Then

        Else
            ''by nahyan on 24feb2019 to eable eid for non uae schools
            If Convert.ToString(Session("BSU_COUNTRY_ID")) = "172" Then
                ' If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                errormsg = "Please enter National Id"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtEIDNo2.Text <> "" Then

        Else
            'If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
            ''by nahyan on 24feb2019 to eable eid for non uae schools
            If Convert.ToString(Session("BSU_COUNTRY_ID")) = "172" Then
                errormsg = "Please enter National Id"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtEIDNo3.Text <> "" Then

        Else
            ' If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
            ''by nahyan on 24feb2019 to eable eid for non uae schools
            If Convert.ToString(Session("BSU_COUNTRY_ID")) = "172" Then
                errormsg = "Please enter National Id"
                callTransaction = -1
                Exit Function
            End If
        End If
        If txtEIDNo4.Text <> "" Then

        Else
            ' If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
            ''by nahyan on 24feb2019 to eable eid for non uae schools
            If Convert.ToString(Session("BSU_COUNTRY_ID")) = "172" Then
                errormsg = "Please enter National Id"
                callTransaction = -1
                Exit Function
            End If
        End If
        If txtEIDExpryDT.Text = "" Then
            ' If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
            ''by nahyan on 24feb2019 to eable eid for non uae schools
            If Convert.ToString(Session("BSU_COUNTRY_ID")) = "172" Then
                errormsg = "Please enter National id expiry date !!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtPassportNo.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter passport number !!!!"
                callTransaction = -1
                Exit Function
            End If

        End If

        If txtPPIssuedPlace.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter passport issued place !!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtPPIssueDate.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter passport issued date !!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtPPEXpiry.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter passport expiry date !!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtDepUidno.Text = "" Then
            If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" AndAlso hdnCTYId.Value <> "172" Then
                errormsg = "Please enter UID number!!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtDepVissuedate.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter VISA issued date!!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtDepVexpdate.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter VISA expiry date!!!!"
                callTransaction = -1
                Exit Function
            End If
        End If
        If txtDepVissueplace.Text = "" Then
            If hdnCTYId.Value <> "172" Then
                errormsg = "Please enter VISA issued place!!!!"
                callTransaction = -1
                Exit Function
            End If
        End If

        If txtAlternateEmail.Text = "" Then
            errormsg = "Please enter alternate email!!!!"
            callTransaction = -1
            Exit Function
        End If

        If txtPhoneNumber.Text = "" Then
            errormsg = "Please enter phone number!!!!"
            callTransaction = -1
            Exit Function
        End If

        If errormsg = "" Then

            Try
                Using CONN As SqlConnection = ConnectionManger.GetOASISConnection



                    tran = CONN.BeginTransaction("SampleTransaction")
                    Try
                        Dim eIDNo As String = String.Empty
                        ''emirats ifd not required for QATAR bsu
                        ''commented and added by nahyan on 24feb2019 to enable eid for non uae 

                        ''by nahyan on 24feb2019 to eable eid for non uae schools
                        If Convert.ToString(Session("BSU_COUNTRY_ID")) = "172" Then


                            ' If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                            If txtEIDNo1.Text <> "" Then
                                eIDNo = txtEIDNo1.Text
                            End If
                            If txtEIDNo2.Text <> "" Then
                                eIDNo = eIDNo + txtEIDNo2.Text
                            End If
                            If txtEIDNo3.Text <> "" Then
                                eIDNo = eIDNo + txtEIDNo3.Text
                            End If
                            If txtEIDNo4.Text <> "" Then
                                eIDNo = eIDNo + txtEIDNo4.Text
                            End If
                            'Else ''by nahyan on12 mar2017 for qatar 
                            '    eIDNo = txtEIDNo.Text
                        Else
                            eIDNo = txtEIDNo.Text
                        End If


                        Dim param(18) As SqlParameter
                        param(0) = New SqlParameter("@EMP_ID", Session("EmployeeId"))
                        param(1) = New SqlParameter("@UID_NO", txtDepUidno.Text)
                        param(2) = New SqlParameter("@EID_NO", eIDNo)
                        param(3) = New SqlParameter("@EID_EXP_DATE", txtEIDExpryDT.Text)
                        param(4) = New SqlParameter("@VISA_ISSU_PLACE", txtDepVissueplace.Text)
                        param(5) = New SqlParameter("@VISA_ISSUE_DATE", txtDepVissuedate.Text)
                        param(6) = New SqlParameter("@VIS_EXP_DATE", txtDepVexpdate.Text)
                        param(7) = New SqlParameter("@Email", txtAlternateEmail.Text)
                        param(8) = New SqlParameter("@cur_mobile", txtPhoneNumber.Text)
                        param(9) = New SqlParameter("@PassportNo", txtPassportNo.Text)
                        param(10) = New SqlParameter("@PPIssuedPlace", txtPPIssuedPlace.Text)
                        param(11) = New SqlParameter("@PPIssueDate", txtPPIssueDate.Text)
                        param(12) = New SqlParameter("@PPEXpiry", txtPPEXpiry.Text)
                        param(13) = New SqlParameter("@VISASPONSOR", Convert.ToInt32(ddlSponsor.SelectedValue))
                        param(14) = New SqlParameter("@EMPEMIRATE", Convert.ToInt32(ddlEmirate.SelectedValue))
                        param(15) = New SqlParameter("@EMPAREA", Convert.ToInt32(ddlArea.SelectedValue))
                        param(16) = New SqlParameter("@INSUR_NO", txtInsCardNum.Text)


                        param(17) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        param(17).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.Update_EMP_VISA_EID_INFO", param)
                        Dim ReturnFlag As Integer = param(17).Value



                        If ReturnFlag = -1 Then
                            callTransaction = "-1"
                            errormsg = "Record cannot be saved. !!!"
                        ElseIf ReturnFlag <> 0 Then
                            callTransaction = "1"
                            errormsg = "Error occured while processing info !!!"
                        Else
                            ViewState("GCM_ID") = "0"
                            ViewState("datamode") = "none"

                            callTransaction = "0"
                            'resetall()
                        End If

                    Catch ex As Exception
                        callTransaction = "1"
                        errormsg = ex.Message
                    Finally
                        If callTransaction = "-1" Then
                            errormsg = "Record cannot be saved. !!!"
                            UtilityObj.Errorlog(errormsg)
                            tran.Rollback()
                        ElseIf callTransaction <> "0" Then
                            errormsg = "Error occured while saving !!!"
                            UtilityObj.Errorlog(errormsg)
                            tran.Rollback()
                        Else
                            errormsg = ""
                            tran.Commit()
                        End If

                    End Try

                End Using
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "save info")
            End Try
        Else
            callTransaction = -1
        End If


    End Function

    Sub bindEmirate()
        ddlEmirate.Items.Clear()



        Dim ds As New DataSet
        Try
            Dim param(3) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", 0)
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "EMIRATE")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            CommandType.StoredProcedure, "dbo.GET_EMIRATE_CITY", param)

            ddlEmirate.DataSource = ds.Tables(0)
            ddlEmirate.DataTextField = "EMR_DESCR"
            ddlEmirate.DataValueField = "EMR_ID"
            ddlEmirate.DataBind()
            ddlEmirate.SelectedIndex = -1
            ddlEmirate.Items.Insert(0, New ListItem("Select", "0"))
            ddlEmirate_SelectedIndexChanged(ddlEmirate, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind emirate")
        End Try
    End Sub

    Sub bindArea()
        ddlArea.Items.Clear()



        Dim ds As New DataSet
        Try
            Dim param(3) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", Convert.ToInt32(ddlEmirate.SelectedValue))
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "AREA")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            CommandType.StoredProcedure, "GET_EMIRATE_CITY", param)


            ddlArea.DataSource = ds.Tables(0)
            ddlArea.DataTextField = "EMA_DESCR"
            ddlArea.DataValueField = "EMA_ID"
            ddlArea.DataBind()
            ddlArea.SelectedIndex = -1
            ddlArea.Items.Insert(0, New ListItem("Select", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind area")
        End Try
    End Sub
    Protected Sub lnkbackbtn_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("../ESSDashboard.aspx")
        Response.Redirect(URL)
    End Sub
End Class
