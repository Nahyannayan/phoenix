Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_accccViewCreditcardReceipt
    Inherits System.Web.UI.Page
     
    Dim Encr_decrData As New Encryption64
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            h_Grid.Value = "top"
            lblError.Text = ""
            ''''' 
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

            Page.Title = OASISConstants.Gemstitle
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'Or _
            '            (ViewState("MainMnu_code") <> "P050090")
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                gridbind()
            End If
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDocExpiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvDocExpiry.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Protected Sub gvEMPDETAILS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDocExpiry.PageIndexChanging
        gvDocExpiry.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrDocType As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrDocNo As String = String.Empty
        Dim lstrIssuePlace As String = String.Empty
        Dim lstrIssueDate As String = String.Empty
        Dim lstrExpDate As String = String.Empty

        Dim lstrFiltDocType As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltIssuePlace As String = String.Empty
        Dim lstrFiltIssueDate As String = String.Empty
        Dim lstrFiltExpDate As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        If gvDocExpiry.Rows.Count > 0 Then
            ' --- Initialize The Variables


            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtDocType")
            lstrDocType = Trim(txtSearch.Text)
            If (lstrDocType <> "") Then lstrFiltDocType = SetCondn(lstrOpr, "ESD_DESCR", lstrDocType)

            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EMPNO", lstrEMPNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMP_NAME", lstrEmpName)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtDocNo")
            lstrDocNo = txtSearch.Text
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "EMD_NO", lstrDocNo)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtIssPlace")
            lstrIssuePlace = txtSearch.Text
            If (lstrIssuePlace <> "") Then lstrFiltIssuePlace = SetCondn(lstrOpr, "EMP_ISSUEPLACE", lstrIssuePlace)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtIssueDate")
            lstrIssueDate = txtSearch.Text
            If (lstrIssueDate <> "") Then lstrFiltIssueDate = SetCondn(lstrOpr, "EMD_ISSUEDT", lstrIssueDate)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDocExpiry.HeaderRow.FindControl("txtExpDate")
            lstrExpDate = txtSearch.Text
            If (lstrExpDate <> "") Then lstrFiltExpDate = SetCondn(lstrOpr, "EMD_EXPDT", lstrExpDate)

        End If
        Dim docID As String = IIf(Request.QueryString("docid") <> Nothing, Request.QueryString("docid").ToString(), String.Empty)
        If ddlFilter.SelectedItem.Value = "1" Then
            str_Sql = "SELECT * from vw_OSA_EMPDOCUMENTS WHERE EMD_bACTIVE = 1 AND EMP_STATUS NOT IN (8,4,5,7) AND EMP_BSU_ID ='" & Session("sBsuid") & _
            "' AND EMD_ESD_ID = " & docID & " AND EMD_EXPDT <= '" & Date.Now.AddDays(Session("DocExpDays")) & "' " & _
            lstrFiltDocType & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDocNo & lstrFiltIssuePlace & lstrFiltIssueDate & lstrFiltExpDate
        ElseIf ddlFilter.SelectedItem.Value = "2" Then
            str_Sql = "SELECT * from vw_OSA_EMPDOCUMENTS WHERE EMD_bACTIVE = 1 AND EMP_STATUS NOT IN (8,4,5,7) AND EMP_VISA_BSU_ID ='" & Session("sBsuid") & _
            "' AND EMD_ESD_ID = " & docID & " AND EMD_EXPDT <= '" & Date.Now.AddDays(Session("DocExpDays")) & "' " & _
            lstrFiltDocType & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDocNo & lstrFiltIssuePlace & lstrFiltIssueDate & lstrFiltExpDate
        Else
            str_Sql = "SELECT * from vw_OSA_EMPDOCUMENTS WHERE EMD_bACTIVE = 1 AND EMP_STATUS NOT IN (8,4,5,7) AND (EMP_BSU_ID ='" & Session("sBsuid") & _
                       "' OR EMP_VISA_BSU_ID='" & Session("sBSUid") & "' ) AND EMD_ESD_ID = " & docID & " AND EMD_EXPDT <= '" & Date.Now.AddDays(Session("DocExpDays")) & "' " & _
                       lstrFiltDocType & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDocNo & lstrFiltIssuePlace & lstrFiltIssueDate & lstrFiltExpDate
        End If


        str_Sql = str_Sql & " Order BY BSU_NAME,VisaStatus DESC,EMD_EXPDT"

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT," & _
        '" EMP_PASSPORT, EMP_STATUS_DESCR, EMP_DES_DESCR FROM vw_OSO_EMPLOYEEMASTER" & _
        '" WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' " _
        '& str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDesignation & lstrFiltPassportNo

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDocExpiry.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvDocExpiry.DataBind()
            Dim columnCount As Integer = gvDocExpiry.Rows(0).Cells.Count

            gvDocExpiry.Rows(0).Cells.Clear()
            gvDocExpiry.Rows(0).Cells.Add(New TableCell)
            gvDocExpiry.Rows(0).Cells(0).ColumnSpan = columnCount
            gvDocExpiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvDocExpiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvDocExpiry.DataBind()
        End If

        'gvJournal.DataBind()
        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtDocType")
        txtSearch.Text = lstrDocType

        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtDocNo")
        txtSearch.Text = lstrDocNo

        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtIssPlace")
        txtSearch.Text = lstrIssuePlace

        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtIssueDate")
        txtSearch.Text = lstrIssueDate

        txtSearch = gvDocExpiry.HeaderRow.FindControl("txtExpDate")
        txtSearch.Text = lstrExpDate

        gvDocExpiry.SelectedIndex = p_selected_id
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvDocExpiry_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            Dim cmdCol As Integer = gvDocExpiry.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = "empMasterDetail_Documents.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=gKYXjSd/HcE=" & "&datamode=" & ViewState("datamode")
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        gridbind()
    End Sub
End Class
