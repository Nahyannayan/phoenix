<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupSalaryCompareDetails.aspx.vb" Inherits="Payroll_PopupSalaryCompareDetails" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location ="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Salary Details</title>
         <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
     <base target="_self" />
</head>

    <script type="text/javascript">
        function closeparent() {
            parent.closepopup();
        }
    </script>
<body  >
    <form id="form1" runat="server">
        <table   width="100%">
            <tr>
                <td align="center" valign="top">
                    <asp:GridView ID="gvEmpSalary" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass=" table table-row table-bordered"
                        PageSize="5" Width="100%" EnableModelValidation="True">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <Columns>
                            <asp:TemplateField HeaderText="Earning">
                                <ItemTemplate>
                                    <asp:Label ID="lblERN_DESCR" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Schedule">
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblSchedule" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Eligible Amount">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountEligible" runat="server" Text='<%#AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Actual Amount">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Revised Eligible Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblRevElib" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY_NEW")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Revised Actual Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblRevAct" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount_NEW")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PAYTERM" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblPayTerm" runat="server" Text='<%# Bind("PAYTERM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_new" Height="25px" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle">
                    <asp:GridView ID="gvEmpSalaryUpload" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass=" table table-row table-bordered"
                        PageSize="5" Width="100%" EnableModelValidation="True" Visible="False">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <Columns>
                            <asp:TemplateField HeaderText="Earning">
                                <ItemTemplate>
                                    <asp:Label ID="lblEarning" runat="server" Text='<%# bind("Earning") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# bind("Amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_new" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
            <tr id="trGross" runat = "server">
                <td align="center" valign="middle">
                    <span class="field-label"> Gross Salary (Current)</span>
                    <asp:TextBox ID="txtGrossSalCur" runat="server"  ></asp:TextBox><br />
                     <span class="field-label">Gross Salary (Revised)</span>
                    <asp:TextBox ID="txtGrossSalRev" runat="server"  ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="closeparent()" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
