<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="UploadEMPAirFareDetails.aspx.vb" Inherits="Payroll_UploadEmpAirFareDetails" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../Asset/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">


        function CheckAllRows(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox' && (document.forms[0].elements[i].name.search(/chkPay/) > 0)) {
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }

        function CheckAllRowsOutside(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox' && (document.forms[0].elements[i].name.search(/chkOutside/) > 0)) {
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }


        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
            }

            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'none') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'none') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
            }
            return false;
        }



        function ToggleSearchDepartment() {
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'display') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchCategory() {
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchDesignation() {
            if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'display') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDesignation.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=hfDesignation.ClientID %>').value = 'display';
            }
            return false;
        }



        function HideAll() {


            document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
            document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
            document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
            document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
        }


        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=BSU", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=BSU", "pop_bsu");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function GetDeptName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=DEPT", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=DEPT", "pop_dept");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_DEPTID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function GetCATName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=CAT", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=CAT", "pop_cat");
           <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_CATID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function GetDESGName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=DESG", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=DESG", "pop_desg");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_DESGID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //            result = window.showModalDialog("reports\/aspx\/SelIDDESC.aspx?ID=EMP","", sFeatures)            
            //result = window.showModalDialog("../Common/PopupFormThreeIDHidden.aspx?ID=EMP&multiSelect=true", "", sFeatures)
            //alert(1);
            var oWnd = radopen("../Common/PopupFormThreeIDHidden.aspx?ID=EMP&multiSelect=true", "pop_emp2");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function GetEMPNAME2() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //alert(document.getElementById('<%=h_BSUID.ClientID %>').value)
            //result = window.showModalDialog("SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "pop_emp3");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_BSUID.ClientID%>').value = NameandCode;
                __doPostBack('<%=h_BSUID.ClientID%>', "");
                //document.forms[0].submit();
            }
        }
        function OnClientDeptClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_DEPTID.ClientID%>').value = NameandCode;
                __doPostBack('<%=h_DEPTID.ClientID%>', "");
                //document.forms[0].submit();
            }
        }
        function OnClientCatClose(oWnd, args) {

            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_CATID.ClientID%>').value = NameandCode;
                __doPostBack('<%=h_CATID.ClientID%>', "");
                //document.forms[0].submit();
            }
        }

        function OnClientDesgClose(oWnd, args) {
            //alert(1);
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_DESGID.ClientID%>').value = NameandCode;
                __doPostBack('<%=h_DESGID.ClientID%>', "");
                //document.forms[0].submit();
            }
        }
        function OnClientEmp3Close(oWnd, args) {
            //alert(1);
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                __doPostBack('<%=h_EMPID.ClientID%>', "");
                //document.forms[0].submit();
            }
        }
        function OnClientEmp2Close(oWnd, args) {
            //alert(1);
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                __doPostBack('<%=h_EMPID.ClientID%>', "");
                //document.forms[0].submit();
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_dept" runat="server" Behaviors="Close,Move" OnClientClose="OnClientDeptClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cat" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCatClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_desg" runat="server" Behaviors="Close,Move" OnClientClose="OnClientDesgClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_emp3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEmp3Close"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_emp2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEmp2Close"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server"
                Text="Process Employee Air Fare Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" runat="server" id="tblMain" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 1px" valign="middle">
               &nbsp;
            </td>
        </tr>--%>
                    <tr id="trMonth_Year">
                        <td align="left" width="20%"><span class="field-label">Pay Month</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlPayMonth" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Pay Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlPayYear" runat="server"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trProcessingDate" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Processing Date</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlProcessingDate" runat="server">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkExcludeProcessDate" runat="server" AutoPostBack="True" OnCheckedChanged="chkExcludeProcessDate_CheckedChanged"
                                Text="Exclude Process Date"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr id="trAsOnDate" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">As On Date</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True"> </asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgAsOn" TargetControlID="txtAsOnDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgAsOn" TargetControlID="txtAsOnDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgAsOn" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvAsOnDate" ControlToValidate="txtAsOnDate"
                                ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator></td>
                    </tr>

                    <tr id="treeCat" visible="true" runat="server">
                        <td align="left" width="20%"><span class="field-label">Category</span>
                        </td>
                        <td align="left" width="30%">
                            <div class="checkbox-list">
                                <uc3:usrTreeView ID="UsrTreeView1" runat="server" />
                            </div>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Employee Category</span>
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" />
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" />
                        </td>
                    </tr>


                    <tr id="trSelDepartment">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusDepartment" runat="server" ImageUrl="../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchDepartment();return false;"
                                CausesValidation="False" />
                            <span class="field-label">Select Department Filter</span>
                        </td>
                    </tr>
                    <tr id="trDepartment" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusDepartment" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" /><span class="field-label">Department</span>
                        </td>
                        <td align="left" colspan="3">(Enter the Department ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDeptName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDEPTID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return GetDeptName()" /><br />
                            <asp:GridView ID="gvDept" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="DEPT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDEPTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DEPT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDeptDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelcategory">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                            <span class="field-label">Select Category Filter</span>
                        </td>
                    </tr>
                    <tr id="trCategory" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Category</span>
                        </td>
                        <td align="left" colspan="3">(Enter the Category ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtCatName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return GetCATName()" /><br />
                            <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="CAT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelDesignation">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusDesignation" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" />
                            <span class="field-label">Select Designation Filter</span>
                        </td>
                    </tr>
                    <tr id="trDesignation" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusDesignation" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" /><span class="field-label">Designation</span>
                        </td>
                        <td align="left" colspan="3">(Enter the Designation ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDesgName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDESGID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return GetDESGName()" /><br />
                            <asp:GridView ID="gvDesg" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="DESG ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDESGID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DESG NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDESGDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelEMPName" runat="server" visible="false">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../Images/PLUS.jpg"
                                OnClientClick="ToggleSearchEMPNames();return false;"
                                CausesValidation="False" />
                            <span class="field-label">Select Employee Name Filter</span>
                        </td>
                    </tr>
                    <tr id="trEMPName">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../Images/MINUS.jpg"
                                OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" /><span class="field-label">Employee Name</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtEMPNAME" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick=" GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblemp_id" runat="server" Text='<%# Bind("EMp_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" nowrap="nowrap" width="100%">
                            <asp:CheckBox ID="chkSaved" runat="server" BackColor="#FFAAAA"
                                Text="Already Processed" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           <asp:CheckBox ID="chkTiktPerYear" runat="server" BackColor="Wheat"
                               Text="0.5 Tickets/yr  " />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                           <asp:CheckBox ID="chkNotProcessed" runat="server"
                               Text="Not Processed" />&nbsp;
                           &nbsp;
                           <asp:CheckBox ID="chkJoinMonth" runat="server"
                               Text="Anniversary Month  " />&nbsp; &nbsp;<asp:Button ID="btnGetAirFareDetails" runat="server" CssClass="button" Text="Get Air Fare Details" /></td>
                    </tr>
                    <tr id="trgrid" visible="false">
                        <td align="left" colspan="4">
                            <div id="div_Scrollgridholder">

                                <asp:GridView ID="gvAirFareDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                    EnableModelValidation="True" Style="text-align: center" UseAccessibleHeader="True" CssClass="table table-bordered table-row">
                                    <HeaderStyle VerticalAlign="Top" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Emp No">
                                            <HeaderTemplate>
                                                EmpNo 

                                            </HeaderTemplate>

                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EAF_EMPNO") %>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emp Name">
                                            <HeaderTemplate>
                                                Name
                                            </HeaderTemplate>

                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EAF_EMPNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From City">
                                            <HeaderTemplate>
                                                From<br />
                                                City
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblFromCity" runat="server" Text='<%# Bind("EAF_FROMCITY") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To City">
                                            <HeaderTemplate>
                                                To<br />
                                                City
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblToCity" runat="server" Text='<%# Bind("EAF_TOCITY") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dt From" Visible="False">

                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpid" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                                <asp:Label ID="lblEarID" runat="server" Text='<%# Bind("EAR_ID") %>'></asp:Label>
                                                <asp:Label ID="lblDtFrom" runat="server" CssClass="matters3" Text='<%# Bind("ELS_DTFROM") %>'></asp:Label>
                                                <asp:Label ID="lblTotalTiktsPerYear" runat="server" Text='<%# Bind("EAF_TotalTICKETSPERYEAR") %>'></asp:Label>
                                                <asp:Label ID="lblActualTiktNo" runat="server" Text='<%# Bind("EAF_TICKETSACTUALNo") %>'></asp:Label>
                                                <asp:Label ID="lblTocityID" runat="server" Text='<%# Bind("ToCityId") %>'></asp:Label>
                                                <asp:Label ID="lblFromcityid" runat="server" Text='<%# Bind("FromCityID") %>'></asp:Label>
                                                <asp:Label ID="lblTktClass" runat="server" Text='<%# Bind("TktClass") %>'></asp:Label>
                                                <asp:Label ID="lblTotalAmount" runat="server" Width="75px" Text='<%# Bind("EAF_Actual_totalAmt") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dt To" Visible="False">

                                            <ItemTemplate>
                                                <asp:Label ID="lblDtTo" runat="server" Text='<%# Bind("ELS_DTTo") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tickets Actual">
                                            <HeaderTemplate>
                                                Tickets<br />
                                                Actual
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketsActual" runat="server"
                                                    Text='<%# Bind("EAF_TICKETSACTUAL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tickets Eligible">
                                            <HeaderTemplate>
                                                Tickets<br />
                                                Eligible
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketsEligible" runat="server"
                                                    Text='<%# Bind("EAF_TICKETSELIGIBLE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket Class">
                                            <HeaderTemplate>
                                                Ticket<br />
                                                Class
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketsClass" runat="server"
                                                    Text='<%# Bind("EAF_TICKETCLASS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tickets/Yr">
                                            <HeaderTemplate>
                                                Tickets/<br />
                                                year
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketsPerYear" runat="server" Text='<%# Bind("EAF_TICKETSPERYEAR") %>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Per Ticket Amt">
                                            <HeaderTemplate>
                                                Amount/<br />
                                                Ticket
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblperTiktAmount" runat="server" Width="75px" Text='<%# Bind("PerTicketAmt") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Amt">
                                            <HeaderTemplate>
                                                Total<br />
                                                Amount
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTotalAmount" runat="server" Width="75px" Text='<%# Bind("EAF_TOTALAMOUNT") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <HeaderTemplate>
                                                Remarks
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Bind("EAR_Remarks") %>'
                                                    TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Last Paid On" DataField="LastPaidOn"></asp:BoundField>

                                        <asp:TemplateField HeaderText="Is Paid">
                                            <HeaderTemplate>
                                                Is Paid
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsPaid" runat="server" Text='<%# Bind("EAR_BPaid") %>' TextMode="MultiLine"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Paid&nbsp;
                                    <br />
                                                Outside
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkOutside" runat="server" Checked='<%# Eval("EAR_bOutsideSystem") %>' />
                                                <asp:CheckBox ID="chkBlockEdit" Visible="false" runat="server" Checked='<%# Eval("EAR_BlockEditing") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkPay_Header" runat="server"
                                                    OnClick="CheckAllRows(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkPay" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="DivFooterRow" style="overflow: hidden;" align="center">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Process"
                                    ValidationGroup="dayBook" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="Remove Processing"
        ValidationGroup="dayBook" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                            </div>



                        </td>
                    </tr>


                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_DEPTID" runat="server" />
                <asp:HiddenField ID="h_CATID" runat="server" />
                <asp:HiddenField ID="h_DESGID" runat="server" />
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfDepartment" runat="server" type="hidden" />
                <input id="hfCategory" runat="server" type="hidden" />
                <input id="hfDesignation" runat="server" type="hidden" />
                <input id="hfEmpName" runat="server" type="hidden" />


            </div>
        </div>
    </div>
</asp:Content>
