<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empScalesGrade.aspx.vb" Inherits="Payroll_empScalesGrade" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language = "javascript">
       function GetEMPGRADE()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            <%--var result;
            result = window.showModalDialog("SelIDDesc.aspx?ID=EMPGRADE","", sFeatures)
            if(result != '' && result != undefined)
            {
                NameandCode = result.split('___');
                document.getElementById('<%=h_EMP_EGD_ID.ClientID %>').value = NameandCode[0]; 
                document.getElementById('<%=txtEMPGrade.ClientID %>').value=NameandCode[1]; 
             }
             return false;--%>
           var url = "SelIDDesc.aspx?ID=EMPGRADE";
           var oWnd = radopen(url, "pop_empgrade");


       }

       function OnClientClose1(oWnd, args) {
           //get the transferred arguments

           var arg = args.get_argument();

           if (arg) {

               NameandCode = arg.NameandCode.split('||');
               document.getElementById('<%=h_EMP_EGD_ID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtEMPGrade.ClientID%>').value = NameandCode[1];
                __doPostBack('<%= txtEMPGrade.ClientID%>', 'TextChanged');
            }
        }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
         <Windows>
            <telerik:RadWindow ID="pop_empgrade" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblFormCaption" runat="server" Text="Employee Scales Grade"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="EMPQUACAT" />
            </td>
        </tr>
        <tr valign="bottom">
            <td align="left" valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    ></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <table align="center" width="100%">
                    
                    <tr id="trMainDescr" runat="server">
                        <td align="left" width="20%">
                            <asp:Label ID="lblMainDescription" CssClass="field-label" runat="server" Text="Employee Grade" ></asp:Label></td>
                       
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEMPGrade" runat="server" CssClass="inputbox" MaxLength="100"
                               ></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server"
                                    ImageUrl="~/Images/forum_search.gif" OnClientClick = "GetEMPGRADE(); return false;"/>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEMPGrade"
                                ErrorMessage="Enter qualification Description" Height="5px" ValidationGroup="EMPQUACAT"
                                >*</asp:RequiredFieldValidator></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr id="trSubDescr" runat="server">
                        <td align="left">
                            <asp:Label ID="lblSubDescription" CssClass="field-label" runat="server" Text="Description"></asp:Label></td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtDescr" runat="server"  MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescr"
                                ErrorMessage="Enter Short Description of Qualification" ValidationGroup="EMPQUACAT">*</asp:RequiredFieldValidator></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="EMPQUACAT" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" /></td>
        </tr>
        
    </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="h_EMP_EGD_ID" runat="server" />
</asp:Content>

