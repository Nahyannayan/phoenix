Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empLoanApplication

    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version            Date            Author              Change
    '1.1                11-May-2011     Swapna              COL_ID being sent as 0 instead of NULL value
    '1.2                18 Dec 2011     Swapna              Received by -bug fix and added parameter VHH_bNoEdit

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            txtEmpNo.Attributes.Add("readonly", "readonly")
            tr_update.Visible = False
            tr_add.Visible = False
            tr_RemarksEdit.Visible = False
            Set_CashorBank()
            txtDocdate.Text = GetDiplayDate()
            gvLoan.Columns(3).Visible = False
            gvLoan.Columns(5).Visible = False
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtChqBook.Attributes.Add("readonly", "readonly")
            txtChqNo.Attributes.Add("readonly", "readonly")
            txtCashDescr.Attributes.Add("readonly", "readonly")
            txtBankDescr.Attributes.Add("readonly", "readonly")
            btnPrint.Visible = False

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            gvLoan.Attributes.Add("bordercolor", "#1b80b6")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130050" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Session("dtLoan") = DataTables.CreateDataTable_Loan()
            gridbind()

            If Request.QueryString("viewid") <> "" Then
                setViewData()
                gvLoan.Columns(3).Visible = True
                gvLoan.Columns(4).Visible = False
                gvLoan.Columns(5).Visible = False
                tr_add.Visible = True
                btnAddInst.Enabled = False
                btnAddedit.Enabled = False 'V1.1
                txtInst.Attributes.Add("readonly", "readonly")
                rbBank.Enabled = False
                rbCash.Enabled = False
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If
        End If
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit ' V1.2
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String  'added EMPLOAN_H.ELH_ERN_ID & VHH_RECEIVEDBY in below query   V1.1
            str_Sql = "SELECT EMPLOYEE_M.EMP_id,EMPLOYEE_M.EMPNO,EMPLOAN_H.ELH_ERN_ID, EMPLOAN_H.ELH_PURPOSE, " _
                       & " EMPLOAN_H.ELH_DATE, EMPLOAN_H.ELH_INSTNO, EMPLOAN_H.ELH_CUR_ID," _
                       & " EMPLOAN_H.ELH_AMOUNT,EMPLOAN_H.ELH_BALANCE, EMPLOAN_H.ELH_BALANCE, EMPLOAN_H.ELH_bPERSONAL, " _
                       & " EMPLOAN_H.ELH_bPosted, EMPLOAN_H.ELH_ID, " _
                       & " ISNULL(EMPLOYEE_M.EMP_FNAME,'')+' '+ ISNULL(EMPLOYEE_M.EMP_MNAME,'')+' '+ " _
                       & " ISNULL(EMPLOYEE_M.EMP_LNAME,'') AS EMP_NAME," _
                       & " EMPSALCOMPO_M.ERN_DESCR,  EMPLOAN_H.ELH_DOCTYPE, EMPLOAN_H.ELH_PAYREFNO,VHH_RECEIVEDBY" _
                       & " FROM EMPLOAN_H INNER JOIN" _
                       & " EMPLOYEE_M ON EMPLOAN_H.ELH_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN" _
                       & " EMPSALCOMPO_M ON EMPLOAN_H.ELH_ERN_ID = EMPSALCOMPO_M.ERN_ID" _
                       & " LEFT JOIN OASISFIN..VOUCHER_H on VOUCHER_H.VHH_DOCNO=EMPLOAN_H.ELH_PAYREFNO and elh_bsu_id=vhh_bsu_id" _
                       & " WHERE ELH_BSU_ID='" & Session("SBSUID") & "'" _
                       & " AND ELH_ID ='" & p_Modifyid & "'"
            Dim ds As New DataSet
            Lockheader()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddEarnings.SelectedValue = ds.Tables(0).Rows(0)("ELH_ERN_ID")  'V1.1 -bug fix
                txtReceived.Text = ds.Tables(0).Rows(0)("VHH_RECEIVEDBY")  'V1.1 -new entry
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                txtAmount.Text = ds.Tables(0).Rows(0)("ELH_AMOUNT").ToString
                If ds.Tables(0).Rows(0)("ELH_AMOUNT") <> ds.Tables(0).Rows(0)("ELH_BALANCE") Then
                    ViewState("canedit") = "no"

                End If
                txtRemarks.Text = ds.Tables(0).Rows(0)("ELH_PURPOSE").ToString
                txtInst.Text = ds.Tables(0).Rows(0)("ELH_INSTNO").ToString
                txtDocdate.Text = Format(CDate(ds.Tables(0).Rows(0)("ELH_DATE")), "dd/MMM/yyyy")
                chkPersonal.Checked = ds.Tables(0).Rows(0)("ELH_bPERSONAL")
                If ds.Tables(0).Rows(0)("ELH_DOCTYPE") <> "" Then
                    Dim str_conn_OASIS As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
                    ViewState("lstrNewDocNo") = ds.Tables(0).Rows(0)("ELH_PAYREFNO")
                    ViewState("doctype") = ds.Tables(0).Rows(0)("ELH_DOCTYPE")
                    Select Case ds.Tables(0).Rows(0)("ELH_DOCTYPE").ToString
                        Case "BP"

                            Dim id_descr As String = GetDataFromSQL("SELECT ACT.ACT_ID+'|'+ACT.ACT_NAME  FROM VOUCHER_H AS VHH INNER JOIN" _
                             & " ACCOUNTS_M AS ACT ON VHH.VHH_ACT_ID = ACT.ACT_ID" _
                             & " WHERE (VHH.VHH_DOCNO = '" & ds.Tables(0).Rows(0)("ELH_PAYREFNO") & "') AND (VHH.VHH_DOCTYPE = 'bp') " _
                             & " AND (VHH.VHH_BSU_ID = '" & Session("SBSUID") & "')", str_conn_OASIS)
                            txtBankCode.Text = id_descr.Split("|")(0)
                            txtBankDescr.Text = id_descr.Split("|")(1)
                            rbBank.Checked = True
                            rbCash.Checked = False
                            Dim NO_DT_ID As String = GetDataFromSQL("SELECT     RTRIM(VHD_CHQNO) + '|' + REPLACE(CONVERT(VARCHAR(11), VHD_CHQDT, 113), ' ', '/') + '|' + " _
                            & " RTRIM(VHD_CHQID) AS NO_DT_ID  FROM VOUCHER_D AS VHD" _
                            & " WHERE     (VHD.VHD_DOCNO = '" & ds.Tables(0).Rows(0)("ELH_PAYREFNO") & "') AND (VHD.VHD_DOCTYPE = 'BP') " _
                            & " AND (VHD.VHD_BSU_ID = '" & Session("SBSUID") & "')", str_conn_OASIS)
                            If NO_DT_ID <> "" Then
                                txtChqNo.Text = NO_DT_ID.Split("|")(0)
                                txtChequedate.Text = NO_DT_ID.Split("|")(1)
                                txtChqBook.Text = NO_DT_ID.Split("|")(2)
                            End If

                        Case "CP"
                            Dim id_descr As String
                            id_descr = GetDataFromSQL("SELECT ACT.ACT_ID+'|'+ACT.ACT_NAME FROM VOUCHER_H AS VHH INNER JOIN" _
                            & " ACCOUNTS_M AS ACT ON VHH.VHH_ACT_ID = ACT.ACT_ID" _
                            & " WHERE (VHH.VHH_DOCNO = '" & ds.Tables(0).Rows(0)("ELH_PAYREFNO") & "') AND (VHH.VHH_DOCTYPE = 'CP') " _
                            & " AND (VHH.VHH_BSU_ID = '" & Session("SBSUID") & "')", str_conn_OASIS)

                            txtCashAcc.Text = id_descr.Split("|")(0)
                            txtCashDescr.Text = id_descr.Split("|")(1)
                            rbCash.Checked = True
                            rbBank.Checked = False
                    End Select
                    Set_CashorBank()
                End If
            Else
                ViewState("canedit") = "no"
            End If

            str_Sql = "SELECT ELD_ID,  ELH_INSTNO AS ID, ELD_DATE AS DATES , " _
            & " ELD_AMOUNT AS AMOUNT, ISNULL(ELD_PAIDAMT,0)  as PAMOUNT,'' AS Status" _
            & " FROM EMPLOAN_D" _
            & " WHERE ELD_ELH_ID='" & p_Modifyid & "'"
            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Session("dtLoan") = ds.Tables(0)
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub Lockheader()
        imgFrom.Enabled = False
        imgEmployee.Enabled = False
        txtDocdate.Attributes.Add("readonly", "readonly")
        chkPersonal.Enabled = False
        txtAmount.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        txtInst.Attributes.Add("readonly", "readonly")
        imgCash.Enabled = False
        imgBank.Enabled = False
        ImageButton4.Enabled = False
        IMG1.Visible = False
    End Sub

    Private Sub setViewData() 'setting controls on view/edit

        imgFrom.Enabled = False
        imgEmployee.Enabled = False
        txtDocdate.Attributes.Add("readonly", "readonly")
        chkPersonal.Enabled = False
        txtAmount.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        txtInst.Attributes.Add("readonly", "readonly")
        ddEarnings.Enabled = False
    End Sub

    Private Sub ResetViewData() 'resetting controls on view/edit
        txtRemarks.Attributes.Remove("readonly")
        txtInst.Attributes.Remove("readonly")

    End Sub

    Function validate_controls() As String

        Dim str_error As String = ""
        txtAlloted.Text = getTotalAlloted()

        Dim strfDate As String = txtDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            str_error = str_error & str_err & "<br>"
        Else
            txtDocdate.Text = strfDate
        End If

        If CDbl(txtAlloted.Text) <> CDbl(txtAmount.Text) Then
            str_error = str_error & "Check the amount Allocation<br>"
        End If
  
        
        If rbBank.Checked Then
            If txtBankCode.Text = "" Then
                str_error = str_error & "Please Select Bank <br>"
            End If
            txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
            If txtBankDescr.Text = "" Then
                str_error = str_error & "Invalid bank selected <br>"
            End If
            If txtChqBook.Text = "" Then
                str_error = str_error & "Please Select Cheque <br>"
            End If
            If txtChequedate.Text = "" Then
                str_error = str_error & "Please Enter Cheque Date <br>"
            Else
                If IsDate(txtChequedate.Text) = False Then
                    str_error = str_error & "Please Enter Valid Cheque Date <br>"
                End If
            End If
        Else
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Please Select Cash Account <br>"
            End If
            txtCashDescr.Text = AccountFunctions.Validate_Account(txtCashAcc.Text, Session("sbsuid"), "CASHONLY")
            If txtCashDescr.Text = "" Then
                str_error = str_error & "Invalid Cash Account Selected <br>"
            End If
        End If 

        If txtRemarks.Text = "" Then
            str_error = str_error & "Please Enter Remarks <br>"
        End If


        If ViewState("datamode") = "edit" Then
            If txtEditRemarks.Text = "" Then
                str_error = str_error & "Please Enter Remarks(Edit) <br>"
            End If
        End If

        validate_controls = str_error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If IsNumeric(txtAmount.Text) = False Then
            lblError.Text = "Please enter valid Amount"
            Exit Sub
        End If
        Dim str_error As String = validate_controls()
        If txtReceived.Text = "" Then
            lblError.Text = "Please enter 'Received by' details."
            Exit Sub
        End If
        If str_error <> "" Then
            lblError.Text = str_error
            Exit Sub
        End If
        If txtReceived.Text.Contains("cash") Then
            lblError.Text = "Text 'Cash' not allowed in Received By details."
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim elh_id As String
            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                elh_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                elh_id = 0
            End If
            Dim retval As String = "1000"
            Dim new_elh_id As Integer = 0
            Dim STR_DOCTYPE, STR_DOCNO As String
            If rbBank.Checked Then
                STR_DOCTYPE = "BP"
            Else
                STR_DOCTYPE = "CP"
                If AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtDocdate.Text.Trim) < CDbl(txtAmount.Text) Then
                    lblError.Text = "There is not Enough Balance in the Account"
                    Exit Sub
                End If
            End If
            AccountFunctions.GetNextDocId(STR_DOCTYPE, Session("sBsuid"), CType(txtDocdate.Text, Date).Month, CType(txtDocdate.Text, Date).Year)
            ViewState("doctype") = STR_DOCTYPE
            STR_DOCNO = ""
            If ViewState("datamode") <> "edit" Then

                'retval = Save_Voucher(objConn, txtDocdate.Text, "Loan for : " & txtEmpNo.Text & " " & txtRemarks.Text, txtAmount.Text, STR_DOCTYPE, _ 'V1.1 comment
                retval = Save_Voucher(objConn, txtDocdate.Text, "(" & txtEmpNo.Text & ") " & txtRemarks.Text & "- No. of Installments:" & txtInst.Text, txtAmount.Text, STR_DOCTYPE, _
             STR_DOCNO, stTrans)
                ' swapna added No. of installments in narration
            Else
                retval = "0"
            End If
            If retval = "0" Then
                retval = PayrollFunctions.SaveEMPLOAN_H(elh_id, h_Emp_No.Value, txtRemarks.Text.ToString, Session("sbsuid"), _
                           ddEarnings.SelectedItem.Value, txtDocdate.Text, txtInst.Text, Session("BSU_CURRENCY"), _
                           txtAmount.Text, txtAmount.Text, chkPersonal.Checked, _
                           edit_bool, 0, new_elh_id, STR_DOCTYPE, STR_DOCNO, txtEditRemarks.Text, stTrans)
            End If

            If retval = "0" Then
                '''''
                Dim dtPrevdate As Date = CDate(txtDocdate.Text)
                Dim iDeletecount As Integer = 0
                For i As Integer = 0 To Session("dtLoan").Rows.Count - 1
                    If dtPrevdate > Session("dtLoan").Rows(i)("Dates") Then
                        stTrans.Rollback()
                        lblError.Text = "Check the dates are in ascending order "
                        Exit Sub
                    End If
                    If Session("dtLoan").Rows(i)("Status") & "" <> "Deleted" Then
                        retval = PayrollFunctions.SaveEMPLOAN_D(Session("dtLoan").Rows(i)("ELD_ID"), new_elh_id, i - iDeletecount + 1, Session("dtLoan").Rows(i)("Dates"), _
                                           Session("dtLoan").Rows(i)("Amount"), 0, 0, edit_bool, stTrans)
                        dtPrevdate = Session("dtLoan").Rows(i)("Dates")

                    Else
                        retval = PayrollFunctions.DeleteEMPLOAN_D(Session("dtLoan").Rows(i)("ELD_ID"), stTrans)
                        iDeletecount = iDeletecount + 1
                    End If
                    If retval <> "0" Then
                        stTrans.Rollback()
                        lblError.Text = "There is some error"
                        Exit Sub
                    End If
                Next
                Dim str_Sql As String
                Dim ds As New DataSet
                str_Sql = "SELECT  ISNULL(ELH_AMOUNT-(SELECT SUM(ELD_AMOUNT)  " _
                            & " FROM " & OASISConstants.dbPayroll & ".DBO.EMPLOAN_D WHERE EH.ELH_ID=ELD_ELH_ID),0) AS DIFF" _
                            & " FROM " & OASISConstants.dbPayroll & ".DBO.EMPLOAN_H AS  EH" _
                            & " WHERE ELH_ID='" & new_elh_id & "'"
                ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then 'check total tallying
                    If ds.Tables(0).Rows(0)(0) = 0 Then
                        stTrans.Commit()
                        ViewState("lstrNewDocNo") = STR_DOCNO
                        btnPrint.Visible = True
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, new_elh_id, _
                    "Edit", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = getErrorMessage("0")
                        clear_All()
                    Else
                        stTrans.Rollback()
                        lblError.Text = " Totals not tallying"
                    End If
                Else
                    lblError.Text = " Totals not tallying"
                End If
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        'Try
        '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        '    Dim str_Sql As String

        '    str_Sql = "SELECT     EMP_ID ," _
        '       & " isnull( EMP_LASTREJOINDT,'01-jan-1900')  " _
        '       & " as EMP_LASTREJOINDT " _
        '       & " FROM EMPLOYEE_M" _
        '       & " where emp_id='" & h_Emp_No.Value & "'" _
        '       & "and EMP_BSU_ID ='" & Session("sBsuid") & "'"


        '    str_Sql = "SELECT     ED.EMD_PERM_ADDRESS, " _
        '        & " ED.EMD_PERM_POBOX, ED.EMD_PERM_CITY," _
        '        & " ED.EMD_PERM_CTY_ID, ED.EMD_PERM_PHONE, " _
        '        & " ED.EMD_PERM_MOBILE," _
        '        & " isnull( EMP_LASTREJOINDT, EM.EMP_JOINDT) AS EMP_LASTREJOINDT" _
        '        & " FROM EMPLOYEE_M AS EM LEFT OUTER JOIN" _
        '        & " EMPLOYEE_D AS ED ON EM.EMP_ID = ED.EMD_EMP_ID" _
        '        & " where EM.EMP_ID='" & h_Emp_No.Value & "'" _
        '        & " and EM.EMP_BSU_ID ='" & Session("sBsuid") & "'"



        '    '& " order by gm.GPM_DESCR "
        '    Dim ds As New DataSet
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        txtLRejoindt.Text = Format(ds.Tables(0).Rows(0)("EMP_LASTREJOINDT"), "dd/MMM/yyyy")
        '        txtPhone.Text = ds.Tables(0).Rows(0)("EMD_PERM_PHONE").ToString
        '        txtInst.Text = ds.Tables(0).Rows(0)("EMD_PERM_POBOX").ToString
        '        txtAddress.Text = ds.Tables(0).Rows(0)("EMD_PERM_ADDRESS").ToString
        '        txtAmount.Text = ds.Tables(0).Rows(0)("EMD_PERM_MOBILE").ToString
        '        If txtLRejoindt.Text = "01/Jan/1900" Then
        '            txtLRejoindt.Text = ""
        '        End If
        '    Else
        '        txtLRejoindt.Text = ""
        '    End If
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    txtLRejoindt.Text = ""
        'End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            gvLoan.Columns(3).Visible = False
            gvLoan.Columns(5).Visible = False
            setViewData()
            'clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        gvLoan.Columns(3).Visible = False
        gvLoan.Columns(5).Visible = False
        btnPrint.Visible = False
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
        rbBank.Enabled = True
        rbCash.Enabled = True

        ''''''
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
        txtDocdate.Attributes.Remove("readonly")
        chkPersonal.Enabled = True
        txtAmount.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        txtInst.Attributes.Remove("readonly")
        imgCash.Enabled = True
        imgBank.Enabled = True
        ImageButton4.Enabled = True
        IMG1.Visible = True
    
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
        btnAddInst.Enabled = True
       
        ddEarnings.Enabled = True
        '''''

    End Sub

    Sub clear_All()

        h_Emp_No.Value = ""
        txtEmpNo.Text = ""
        txtAmount.Text = ""
        txtRemarks.Text = ""
        txtInst.Text = ""

        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtCashAcc.Text = ""
        txtCashDescr.Text = ""
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtChequedate.Text = ""
        txtDDateAdd.Text = ""
        txtDDate.Text = ""
        tr_RemarksEdit.Visible = False
        txtReceived.Text = "" 'V1.1
        txtDocdate.Text = GetDiplayDate()
        Session("dtLoan").Rows.Clear()
        gridbind()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ResetViewData()
        ViewState("datamode") = "edit"
        tr_RemarksEdit.Visible = True
        btnAddedit.Enabled = True 'V1.1
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        txtInst.Attributes.Add("readonly", "readonly")
        If ViewState("canedit") = "no" Then
            Lockheader()
        End If
        gvLoan.Columns(4).Visible = True
        gvLoan.Columns(5).Visible = True
    End Sub

    Protected Sub btnAddInst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddInst.Click
        Dim iNoinst As Integer
        If IsNumeric(txtInst.Text) = False Then
            lblError.Text = "Please enter valid Installment"
            Exit Sub
        End If
        If IsNumeric(txtAmount.Text) = False Then
            lblError.Text = "Please enter valid Amount"
            Exit Sub
        End If
        If txtAmount.Text = "" Then
            lblError.Text = "Please enter amount."
            Exit Sub
        End If
        iNoinst = CInt(txtInst.Text)
        Dim dtInstDate As Date = CDate(txtDocdate.Text)
        If (dtInstDate < CDate(txtDocdate.Text) Or dtInstDate < CDate(txtDocdate.Text)) Then
            lblError.Text = "Select a greater Installment Date."
            Exit Sub
        End If
        Dim decTotalAmount, decMinst, decCarryDecimal As Decimal
        decTotalAmount = Convert.ToDecimal(txtAmount.Text)
        decMinst = Math.Ceiling(decTotalAmount / iNoinst)
        decCarryDecimal = decTotalAmount - decMinst * (iNoinst - 1)
        Session("dtLoan").Rows.Clear()
        Try
            Dim rDt As DataRow
            For i As Integer = 1 To iNoinst
                If decTotalAmount > 0 Then
                    rDt = Session("dtLoan").NewRow
                    rDt("Id") = i

                    rDt("Dates") = dtInstDate.AddMonths(i)
                    rDt("PAmount") = 0
                    rDt("ELD_ID") = 0
                    rDt("Amount") = decMinst
                    '''' "Id",  "Date", "Status" "Amount" ELD_ID
                    If i = iNoinst Then
                        rDt("Amount") = decCarryDecimal
                    End If
                    Session("dtLoan").Rows.Add(rDt)
                Else
                    lblError.Text = "Enter valid number"
                    Exit Sub
                End If
            Next
            txtAlloted.Text = txtAmount.Text
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = "Check data entered"
        End Try
        '''''

    End Sub

    Function getTotalAlloted() As Decimal
        Dim dceToatlalloted As Decimal = 0
        For i As Integer = 0 To Session("dtLoan").Rows.Count - 1
            If Session("dtLoan").Rows(i)("Status") & "" <> "Deleted" Then
                dceToatlalloted = Session("dtLoan").Rows(i)("Amount") + dceToatlalloted
            End If
        Next
        Return dceToatlalloted
    End Function

    Sub gridbind()
        Try
            Dim i As Integer
            Dim dtTempLoan As New DataTable
            dtTempLoan = DataTables.CreateDataTable_Loan()

            Dim dAllocate As Double = 0
            If Session("dtLoan").Rows.Count > 0 Then
                For i = 0 To Session("dtLoan").Rows.Count - 1
                    If Session("dtLoan").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempLoan.NewRow
                        For j As Integer = 0 To Session("dtLoan").Columns.Count - 1
                            rDt.Item(j) = Session("dtLoan").Rows(i)(j)
                        Next
                        'dAllocate = dAllocate + Session("dtLoan").Rows(i)("Amount")
                        dtTempLoan.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            txtAlloted.Text = getTotalAlloted()
            gvLoan.DataSource = dtTempLoan
            gvLoan.DataBind()
            txtInst.Text = gvLoan.Rows.Count
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub gvLoan_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvLoan.RowDeleting
        If Convert.ToDecimal(gvLoan.Rows(e.RowIndex).Cells(3).Text) > 0 Then
            lblError.Text = "Cannot Edit this Installment since it is posted."
            Exit Sub
        End If
        Dim iTodateIndex As Integer
        iTodateIndex = CInt(gvLoan.Rows(e.RowIndex).Cells(0).Text)
        For i As Integer = 0 To Session("dtLoan").Rows.Count - 1
            If Session("dtLoan").Rows(i)("id") = iTodateIndex Then
                Session("dtLoan").Rows(i)("Status") = "Deleted"
            End If
        Next
        gridbind()
    End Sub

    'Protected Sub gvLoan_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvLoan.RowEditing
    '    If ViewState("datamode") = "edit" Then
    '        If Convert.ToDecimal(gvLoan.Rows(e.NewEditIndex).Cells(3).Text) > 0 Then
    '            lblError.Text = "Cannot Edit this Installment since it is posted."
    '            Exit Sub
    '        End If
    '    End If


    '    btnSave.Enabled = False
    '    gvLoan.SelectedIndex = e.NewEditIndex
    '    txtDDate.Text = gvLoan.Rows(gvLoan.SelectedIndex).Cells(1).Text
    '    txtDAmount.Text = gvLoan.Rows(gvLoan.SelectedIndex).Cells(2).Text
    '    gvLoan.Columns(4).Visible = False
    '    tr_update.Visible = True
    '    tr_add.Visible = False
    '    gridbind()
    'End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim iTodateIndex As Integer

        If IsNumeric(txtDAmount.Text) = False Then
            lblError.Text = "Please enter valid Installment"
            Exit Sub
        End If
        If IsNumeric(txtAmount.Text) = False Then
            lblError.Text = "Please enter valid Amount"
            Exit Sub
        End If
        Dim dtInstDate As Date = CDate(txtDDate.Text)
        'SWAPNA
        If dtInstDate < CDate(txtDocdate.Text) Then
            txtDDate.Text = txtDocdate.Text
            dtInstDate = txtDocdate.Text
        End If
        If (dtInstDate < CDate(txtDocdate.Text) Or dtInstDate < CDate(txtDocdate.Text)) Then
            lblError.Text = "Select a greater Installment Date."
            Exit Sub
        End If

        iTodateIndex = CInt(gvLoan.Rows(gvLoan.SelectedIndex).Cells(0).Text)
        For i As Integer = 0 To Session("dtLoan").Rows.Count - 1

            If Session("dtLoan").Rows(i)("id") = iTodateIndex Then
                Session("dtLoan").Rows(i)("Dates") = dtInstDate
                Session("dtLoan").Rows(i)("Amount") = txtDAmount.Text
            End If
        Next
        Dim Counter As Integer = 1
        For j As Integer = gvLoan.SelectedIndex + 1 To Session("dtLoan").Rows.Count - 1
            Session("dtLoan").Rows(j)("Dates") = dtInstDate.AddMonths(Counter)
            Counter += 1
        Next
        gvLoan.SelectedIndex = -1
        gvLoan.Columns(4).Visible = True
        tr_update.Visible = False
        If ViewState("datamode") = "edit" Then
            tr_add.Visible = True
        End If
        btnSave.Enabled = True
        gridbind()
    End Sub

    Protected Sub btnInstCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInstCancel.Click
        gvLoan.SelectedIndex = -1
        gvLoan.Columns(4).Visible = True
        tr_update.Visible = False
        tr_add.Visible = True
        btnSave.Enabled = True
        gridbind()
    End Sub

    Protected Sub gvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvLoan.SelectedIndexChanged

    End Sub

    Protected Sub btnAddedit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddedit.Click
        'V1.1
        If txtDAmountAdd.Text = "" Or txtDDateAdd.Text = "" Then
            lblError.Text = "Please enter required details."
            Exit Sub
        End If
        If IsNumeric(txtDAmountAdd.Text) = False Then
            lblError.Text = "Please enter valid Installment"
            Exit Sub
        End If
        Dim dtInstDate As Date = CDate(txtDDateAdd.Text)
       

        If (dtInstDate < CDate(txtDocdate.Text) Or dtInstDate < CDate(txtDocdate.Text)) Then
            lblError.Text = "Select a greater Installment Date."
            Exit Sub
        End If
        'V1.1 ends
        Dim decAmount As Decimal
        decAmount = Convert.ToDecimal(txtDAmountAdd.Text)
        Try
            Dim rDt As DataRow

            If decAmount > 0 Then
                rDt = Session("dtLoan").NewRow
                rDt("Id") = getmaxid()

                rDt("Dates") = dtInstDate
                rDt("PAmount") = 0
                rDt("ELD_ID") = 0
                rDt("Amount") = decAmount
                '''' "Id",  "Date", "Status" "Amount" ELD_ID

                Session("dtLoan").Rows.Add(rDt)
            Else
                lblError.Text = "Enter valid number"
                Exit Sub
            End If

            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = "Check data entered"
        End Try
    End Sub

    Function getmaxid() As Integer
        Dim iMaxid As Integer = 0
        For i As Integer = 0 To Session("dtLoan").Rows.Count - 1
            If iMaxid < Session("dtLoan").Rows(i)("id") Then
                iMaxid = Session("dtLoan").Rows(i)("id")
            End If
        Next
        iMaxid = iMaxid + 1
        Return iMaxid
    End Function

    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged
        Set_CashorBank()
    End Sub

    Protected Sub rbBank_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBank.CheckedChanged
        Set_CashorBank()
    End Sub

    Sub Set_CashorBank()
        If rbCash.Checked Then
            tr_Bank.Visible = False
            tr_Cheque.Visible = False
            tr_Cash.Visible = True
        Else
            tr_Bank.Visible = True
            tr_Cheque.Visible = True
            tr_Cash.Visible = False
        End If
    End Sub

    Protected Function Save_Voucher(ByVal objConn As SqlConnection, ByVal p_Date As Date, ByVal p_Narration As String, _
    ByVal p_Amount As String, ByVal p_Doctype As String, ByRef STR_DOCNO As String, ByVal stTrans As SqlTransaction) As String
        Dim lintRetVal As Integer = "1000"
        Dim lstrNewDocNo As String
        Dim str_connOasisFin As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim gdsSysInfo As New DataSet

        Dim str_Provacc As String = ""
        Dim str_Salaryacc_D As String = ""
        Dim str_Cashflow As String = ""
        Dim str_Exgrate As Decimal
        Dim str_CorCurid As String = ""
        Dim str_Sql As String
        str_Sql = " SELECT SYS_SALARYPDCPROVACC, SYS_ADVCASHFLOW,SYS_CUR_ID,SYS_SALARYPDCPROVACC From vw_OSF_SYSINFO_S"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasis, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            If gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPDCPROVACC").ToString = "" Or gdsSysInfo.Tables(0).Rows(0)("SYS_ADVCASHFLOW").ToString = "" Then
                lblError.Text = "Provision code not set"
                Return "714"
            End If
            str_Provacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPDCPROVACC").ToString
            str_Cashflow = gdsSysInfo.Tables(0).Rows(0)("SYS_ADVCASHFLOW").ToString
            str_CorCurid = gdsSysInfo.Tables(0).Rows(0)("SYS_CUR_ID")
        Else
            lblError.Text = "Provision code not set"
            Return "714"
        End If
        gdsSysInfo.Tables.Clear() 'GET EEXGRATE WRT CORP.OFFICE

        str_Sql = "SELECT EXG_RATE " _
        & " FROM EXGRATE_S WHERE EXG_BSU_ID='" & Session("sBsuid") & "'" _
        & " AND EXG_CUR_ID='" & str_CorCurid & "' AND '" & p_Date & "' " _
        & " BETWEEN EXG_FDATE AND ISNULL(EXG_TDATE,GETDATE())"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasisFin, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Exgrate = gdsSysInfo.Tables(0).Rows(0)("EXG_RATE")
        Else
            lblError.Text = "Exchange rate not set"
            Return "713"
        End If
        ''''
        gdsSysInfo.Tables.Clear() 'GET DETAIL ACC
        str_Sql = "SELECT   ERD.ERD_ACC_ID " _
            & " FROM EMPLOYEE_M AS EM INNER JOIN EMPSALCOMPO_D AS ERD " _
            & " ON EM.EMP_ECT_ID = ERD.ERD_ECT_ID" _
            & " WHERE EM.EMP_ID='" & h_Emp_No.Value & "' AND ERD.ERD_ERN_ID='" & ddEarnings.SelectedItem.Value & "'"

        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasis, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Salaryacc_D = gdsSysInfo.Tables(0).Rows(0)("ERD_ACC_ID")
        Else
            lblError.Text = "Credit Account not set"
            Return "715"
        End If
        Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
        SqlCmd.CommandType = CommandType.StoredProcedure
        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
        sqlpGUID.Value = System.DBNull.Value
        SqlCmd.Parameters.Add(sqlpGUID)
        SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
        SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
        SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
        SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", p_Doctype)
        SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", "")
        SqlCmd.Parameters.AddWithValue("@VHH_REFNO", "")
        SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "P")
        SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", p_Date)

        If rbCash.Checked Then
            SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtCashAcc.Text)
        Else
            SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", Convert.ToInt32(hCheqBook.Value))
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChequedate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
        End If

        SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 0)
        SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
        SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
        SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
        SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
        SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
        SqlCmd.Parameters.AddWithValue("@VHH_bAuto", True)
        SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

        SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
        SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
        SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
        SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", str_Provacc)
        SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

        SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
        SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
        SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", str_Exgrate)
        SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", p_Narration)
        'SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", System.DBNull.Value) 'V1.1
        SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", 0)
        SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
        SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
        SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
        SqlCmd.Parameters.AddWithValue("@VHH_bAdvance", False)

        SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
        SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txtReceived.Text)  'V1.1
        SqlCmd.Parameters.AddWithValue("@VHH_bNoEDit", True) 'V1.2

        Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
        sqlpJHD_TIMESTAMP.Value = System.DBNull.Value

        SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
        SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
        SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
        SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
        SqlCmd.Parameters.AddWithValue("@bEdit", False)
        SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
        SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
        SqlCmd.ExecuteNonQuery()
        lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)

        lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
        STR_DOCNO = lstrNewDocNo
        SqlCmd.Parameters.Clear()
        If (lintRetVal = 0) Then
            lintRetVal = DoTransactions(objConn, stTrans, lstrNewDocNo, 1, _
            str_Exgrate, 1, p_Amount, str_Cashflow, _
              str_Salaryacc_D, p_Date, p_Narration, p_Doctype)
            If lintRetVal <> 0 Then
                Return lintRetVal
            End If
            ' lintRetVal = post_voucher(lstrNewDocNo, p_Doctype, stTrans, objConn)  V1.1 - No posting required
            Return lintRetVal
        Else
            Return lintRetVal
        End If

    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
           ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
           ByVal p_Exgrate1 As String, ByVal p_Exgrate2 As String, _
           ByVal p_Lineid As String, ByVal p_Amount As Decimal, ByVal p_Cashflow As String, _
           ByVal str_Salaryacc As String, ByVal p_Date As String, ByVal p_Narrn As String, _
           ByVal p_Doctype As String) As String

        Dim iReturnvalue As Integer

        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim dTotal As Double = 0
        Dim iEbtID As Integer = 0
        cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        Dim str_crdb As String = "DR"
        iReturnvalue = DoTransactions_Sub_Table(objConn, stTrans, p_docno, _
        str_crdb, p_Lineid, str_Salaryacc, p_Amount, p_Date, h_Emp_No.Value, txtEmpNo.Text, p_Doctype)
        If iReturnvalue <> "0" Then
            Return iReturnvalue
        End If
        '' '' 
        cmd.Parameters.AddWithValue("@GUID", System.DBNull.Value)
        cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
        cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
        cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
        cmd.Parameters.AddWithValue("@VHD_DOCTYPE", p_Doctype)
        cmd.Parameters.AddWithValue("@VHD_DOCNO", p_docno)
        cmd.Parameters.AddWithValue("@VHD_LINEID", p_Lineid)
        cmd.Parameters.AddWithValue("@VHD_ACT_ID", str_Salaryacc)
        cmd.Parameters.AddWithValue("@VHD_AMOUNT", p_Amount)
        cmd.Parameters.AddWithValue("@VHD_NARRATION", p_Narrn)
        If rbCash.Checked Then
            cmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_CHQNO", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_CHQDT", "  ")
            cmd.Parameters.AddWithValue("@VHD_bCheque", False)
        Else
            cmd.Parameters.AddWithValue("@VHD_CHQID", Convert.ToInt32(hCheqBook.Value))
            cmd.Parameters.AddWithValue("@VHD_CHQNO", txtChqNo.Text)
            cmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtChequedate.Text))
            cmd.Parameters.AddWithValue("@VHD_bCheque", True)
        End If

        cmd.Parameters.AddWithValue("@VHD_RSS_ID", p_Cashflow)
        cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
        cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
        cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
        cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
        cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)

        'cmd.Parameters.AddWithValue("@VHD_COL_ID", System.DBNull.Value)
        cmd.Parameters.AddWithValue("@VHD_COL_ID", 0)   'V1.1
        cmd.Parameters.AddWithValue("@bEdit", False)
        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
        cmd.ExecuteNonQuery()
        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

        cmd.Parameters.Clear()
        Return iReturnvalue
    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
          ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
          ByVal p_crdr As String, ByVal p_slno As Integer, ByVal p_accountid As String, _
          ByVal p_Amount As String, ByVal p_date As String, _
          ByVal p_Code As String, ByVal p_CodeDescr As String, ByVal p_Doctype As String) As String

        Dim iReturnvalue As Integer
      
        Dim cmd As New SqlCommand
        Dim dTotal As Double = 0
        cmd.Dispose()
        cmd = New SqlCommand("SaveVOUCHER_D_S", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
        sqlpGUID.Value = System.DBNull.Value
        cmd.Parameters.Add(sqlpGUID)

        Dim sqlpJDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
        sqlpJDS_ID.Value = p_slno
        cmd.Parameters.Add(sqlpJDS_ID)

        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = p_Doctype
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim sqlpJDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
        sqlpJDS_DOCDT.Value = p_date
        cmd.Parameters.Add(sqlpJDS_DOCDT)

        Dim sqlpJDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
        sqlpJDS_ACT_ID.Value = p_accountid
        cmd.Parameters.Add(sqlpJDS_ACT_ID)

        Dim sqlpbJDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
        sqlpbJDS_SLNO.Value = p_slno
        cmd.Parameters.Add(sqlpbJDS_SLNO)

        Dim sqlpJDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
        sqlpJDS_AMOUNT.Value = p_Amount
        cmd.Parameters.Add(sqlpJDS_AMOUNT)

        Dim sqlpJDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
        sqlpJDS_CCS_ID.Value = "0001"
        cmd.Parameters.Add(sqlpJDS_CCS_ID)

        Dim sqlpJDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
        sqlpJDS_CODE.Value = p_Code
        cmd.Parameters.Add(sqlpJDS_CODE)

        Dim sqlpJDS_Descr As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
        sqlpJDS_Descr.Value = p_CodeDescr
        cmd.Parameters.Add(sqlpJDS_Descr)

        Dim sqlpbJDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
        sqlpbJDS_DRCR.Value = p_crdr
        cmd.Parameters.Add(sqlpbJDS_DRCR)

        Dim sqlpJDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
        sqlpJDS_bPOSTED.Value = False
        cmd.Parameters.Add(sqlpJDS_bPOSTED)

        Dim sqlpbJDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
        sqlpbJDS_BDELETED.Value = False
        cmd.Parameters.Add(sqlpbJDS_BDELETED)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = False
        cmd.Parameters.Add(sqlpbEdit)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        cmd.Parameters.Clear()
        Return iReturnvalue
    End Function

    Function post_voucher(ByVal p_Docno As String, ByVal p_DocTYPE As String, ByVal stTrans As SqlTransaction, ByVal objConn As SqlConnection) As Integer

        Try
            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim cmd As New SqlCommand("POSTVOUCHER", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid")
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = Session("F_YEAR")
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpJHD_DOCTYPE.Value = p_DocTYPE
                cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                sqlpJHD_DOCNO.Value = p_Docno
                cmd.Parameters.Add(sqlpJHD_DOCNO)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                Return iReturnvalue
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                Errorlog(ex.Message)
                Return 1000
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        'If ViewState("doctype") = "BP" Then
        'Session("ReportSource") = AccountsReports.BankPayment(ViewState("lstrNewDocNo"), CDate(txtDocdate.Text), Session("sBSUID"), Session("F_YEAR"))
        Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", ViewState("lstrNewDocNo"), False, Session("HideCC"))
        ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
        ReportLoadSelection()
        'Else
        '    Session("ReportSource") = AccountsReports.CashPayments(ViewState("lstrNewDocNo"), String.Format("{0:MM-dd-yyyy}", CDate(txtDocdate.Text)), Session("sBSUID"), Session("F_YEAR"))
        '    Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
        'End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub lnk_gvLoan_edit_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim gvr As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        If ViewState("datamode") = "edit" Then
            If Convert.ToDecimal(gvr.Cells(3).Text) > 0 Then
                lblError.Text = "Cannot Edit this Installment since it is posted."

                Exit Sub
            End If
        End If

        btnSave.Enabled = False
        gvLoan.SelectedIndex = gvr.RowIndex
        txtDDate.Text = gvr.Cells(1).Text
        txtDAmount.Text = gvr.Cells(2).Text
        For i As Integer = gvr.RowIndex + 1 To gvLoan.Rows.Count - 1

        Next

        gvLoan.Columns(4).Visible = False
        tr_update.Visible = True
        tr_add.Visible = False
        gridbind()
    End Sub

End Class
