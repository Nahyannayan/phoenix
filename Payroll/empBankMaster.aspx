<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empBankMaster.aspx.vb" Inherits="Payroll_empBankMaster" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Bank Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Bank Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBankDesc" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBankDesc" ValidationGroup="vg"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Bank name can not be left empty."
                                            ForeColor="">*</asp:RequiredFieldValidator></td>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Salary transfer description</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTransferdesc" runat="server" CssClass="inputbox_multi" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTransferdesc" ValidationGroup="vg"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Description can not be left empty."
                                            ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr class="matters">
                                    <td align="left"><span class="field-label">Bank Code</span></td>
                                    <td align="left">&nbsp;<asp:TextBox ID="txtBankCode" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBankCode" ValidationGroup="vg"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Bank code required." ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"><span class="field-label">Short Name</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBankShort" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtBankShort" ValidationGroup="vg"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Short name required." ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Head Office</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHeadoffice" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtHeadoffice" ValidationGroup="vg"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Head office cannot be left empty."
                                            ForeColor="">*</asp:RequiredFieldValidator></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" />
                                        &nbsp;<asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vg" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

