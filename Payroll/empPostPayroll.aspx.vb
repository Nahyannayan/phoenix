Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
''' <summary>
''' Version                   Date                 Author                 Change
''' 1.1                     08 Oct 2012            Swapna               To add option to close bsu pay month
''' </summary>
''' <remarks></remarks>
Partial Class Payroll_empPostPayroll
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            bind_BusinessUnit()
            Session("EMP_SEL_COND") = " AND (EMP_bACTIVE = 1) and EMP_STATUS=1 and EMP_BSU_ID='" & Session("sBsuid") & "' "
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            gvMonthyear.Attributes.Add("bordercolor", "#1b80b6")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P170010" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            set_Comboanddate()
            GetPayMonth() 'V1.1
        Else
        End If
    End Sub

    Sub bind_BusinessUnit()
        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else

            Try
                'if user is super admin give access to all the Business Unit
                If tblbUSuper = True Then
                    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        'create a list item to bind records from reader to dropdownlist ddlBunit
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        'check if it return rows or not
                        If BUnitreaderSuper.HasRows = True Then
                            While BUnitreaderSuper.Read
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBUnit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                Else
                    'if user is not super admin get access to the selected  Business Unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        If BUnitreader.HasRows = True Then
                            While BUnitreader.Read
                                di = New ListItem(BUnitreader(2), BUnitreader(0))
                                ddlBUnit.Items.Add(di)
                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim streMPIDS As String = UtilityObj.GenerateXML(h_EMPID.Value, XMLType.EMPName)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim objConnNew As New SqlConnection(str_conn) ' V1.1
       
        Try
            Dim ela_id As String
            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                ela_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                ela_id = 0
            End If
            lblError.Text = ""
            Dim retval As String = "0"
            Dim count As Integer = 0
            For Each gvr As GridViewRow In gvMonthyear.Rows
                Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkMONTH_YEAR"), HtmlInputCheckBox)
                If cb.Checked = True Then
                    count = count + 1
                End If
            Next

            If count > 0 Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try

                
                    For Each gvr As GridViewRow In gvMonthyear.Rows
                        Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkMONTH_YEAR"), HtmlInputCheckBox)
                        Dim txtDate As TextBox = CType(gvr.FindControl("txtDate"), TextBox)
                        Dim lblIsFFS As Label = CType(gvr.FindControl("lblIsFFS"), Label)
                        Dim IsFFS As Boolean = False
                        If lblIsFFS.Text <> "" Then
                            IsFFS = True
                        End If
                        If IsDate(txtDate.Text) = False Then
                            retval = "606"
                            Exit For
                        End If
                        'Dim lblmonth_year As Label = CType(gvr.FindControl("lblmonth_year"), Label)
                        'Dim lblESD_EMP_ID As Label = CType(gvr.FindControl("lblESD_EMP_ID"), Label)
                        If cb.Checked = True Then
                            retval = PayrollFunctions.PostPayroll(Session("sBsuid"), cb.Value.Split("_")(0), _
                                                 cb.Value.Split("_")(1), txtDate.Text, stTrans, IsFFS)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If

                    Next
                    If retval = "0" Then
                        stTrans.Commit()
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value, _
                             "Insert", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = "Payroll is posted Successfully ..."
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(retval)
                        Exit Try
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                End Try
            End If
            If retval = "0" Then 'V1.1

                If chkClose.Checked = True Then
                    objConnNew.Open()
                    Dim stTransNew As SqlTransaction = objConnNew.BeginTransaction
                    Try

                        retval = CloseBsuPayMonth(objConn, stTransNew)
                        If retval = "0" Then
                            stTransNew.Commit()
                            lblError.Text = lblError.Text & "Pay month closed successfully"
                        Else
                            stTransNew.Rollback()
                            lblError.Text = getErrorMessage(retval)
                        End If
                    Catch ex As Exception
                        stTransNew.Rollback()
                        Errorlog(ex.Message)
                    End Try
                End If
            End If


        Catch ex As Exception

            'stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If objConnNew.State = ConnectionState.Open Then
                objConnNew.Close()
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub clear_All()
        h_EMPID.Value = ""
        GetPayMonth()
    End Sub

    Sub set_Comboanddate()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "getDataforPayrollPost '" & ddlBUnit.SelectedItem.Value & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvMonthyear.DataSource = ds.Tables(0)

            gvMonthyear.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        txtPayMonth.Text = "" 'V1.1
        set_Comboanddate()
        GetPayMonth() 'V1.1

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Public Sub GetPayMonth() 'V1.1
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  Isnull(BSU_PAYMONTH,0) BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & ddlBUnit.SelectedValue & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("BSU_PAYMONTH") <> 0 Then
                    txtPayMonth.Text = MonthName(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))) & "-" & ds.Tables(0).Rows(0)("BSU_PAYYEAR")
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub chkClose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkClose.CheckedChanged 'V1.1
        If chkClose.Checked = True Then

            btnSave.Attributes.Add("OnClick", "return confirm_Disable();")
        Else
            btnSave.Attributes.Remove("OnClick")
        End If
    End Sub
    Protected Function CloseBsuPayMonth(ByVal str_conn As SqlConnection, ByVal stTrans As SqlTransaction) As String 'V1.1
        'Dim sqlParam(1) As SqlParameter
        'sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBUnit.SelectedValue, SqlDbType.VarChar)
        'sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnVal", "", SqlDbType.VarChar, True)
        'sqlParam(1).Direction = ParameterDirection.ReturnValue

        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = ddlBUnit.SelectedValue
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim Retval As String
        Retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "CloseBSUPayMonth", pParms)
        Retval = pParms(1).Value
        Return Retval
    End Function
End Class
