<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empLoanApplication.aspx.vb" Inherits="Payroll_empLoanApplication" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetEMPName() {

            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up")
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtReceived.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');

            }
        }

        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }

        function get_Bank() {

            var NameandCode;
            var result;

            result = radopen("..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "pop_up2");


        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtChqBook.ClientId %>').value = '';
                document.getElementById('<%=hCheqBook.ClientId %>').value = '';
                document.getElementById('<%=txtChqNo.ClientId %>').value = '';
                __doPostBack('<%= txtBankCode.ClientID%>', 'TextChanged');

            }
        }

        function get_Cash() {

            var NameandCode;
            var result;

            result = radopen("..\/accounts\/ShowAccount.aspx?ShowType=CASHONLY&codeorname=" + document.getElementById('<%=txtCashAcc.ClientID %>').value, "pop_up3");


        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtCashAcc.ClientID %>').value = NameandCode[0];
          document.getElementById('<%=txtCashDescr.ClientID %>').value = NameandCode[1];
          __doPostBack('<%= txtCashAcc.ClientID%>', 'TextChanged');

      }
  }

  function get_Cheque() {
      
      var NameandCode;
      var result;

      if (document.getElementById('<%= txtBankCode.ClientId %>').value == "") {
          alert("Please Select The Bank");
          return false;
      }

      result =radopen("..\/accounts\/ShowChqs.aspx?ShowType=CHQBOOK_PDC&BankCode=" + document.getElementById('<%= txtBankCode.ClientId %>').value + "&docno=0", "pop_up4");
     
  }

         function OnClientClose4(oWnd, args) {
         //get the transferred arguments

       var arg = args.get_argument();
       if (arg) {
          NameandCode = arg.NameandCode.split('||');
          document.getElementById('<%= txtChqBook.ClientId %>').value = NameandCode[1];
          document.getElementById('<%= hCheqBook.ClientId %>').value = NameandCode[0];
          document.getElementById('<%= txtChqNo.ClientId %>').value = NameandCode[2];
                __doPostBack('<%= txtChqNo.ClientID%>', 'TextChanged');

            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Loans/Advances
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table cellpadding="5" cellspacing="0" width="100%" align="center">
                    <%--<tr class="subheader_img">
                <td colspan="4" style="height: 19px" align="left">Loans/Advances</td>                
            </tr>--%>
                    <tr width="100%">
                        <td align="left" width="20%"><span class="field-label">Select Employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox><asp:ImageButton
                                ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;" /><br />
                            <asp:RadioButton ID="rbCash" runat="server" Checked="True" GroupName="pay" CssClass="field-label"
                                Text="Cash Payment" AutoPostBack="True" />
                            <asp:RadioButton ID="rbBank" runat="server" GroupName="pay" Text="Bank Payment" CssClass="field-label" AutoPostBack="True" /><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo" 
                                CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocdate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left" width="20%"><span class="field-label">Deduction Ref</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddEarnings" runat="server" DataSourceID="SqlDataSource1"
                                DataTextField="ERN_DESCR" DataValueField="ERN_ID">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="tr_Cash" runat="server">
                        <td align="left" width="20%"><span class="field-label">Cash A/C</span> </td>
                        <td align="left" colspan="2">
                             <table width="100%">
                                <tr>
                                    <td width="40%" align="left" class="p-0">
                            <asp:TextBox ID="txtCashAcc" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgCash" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Cash(); return false;" />
                                    </td>
                                    <td width="60%" align="left" class="p-0">
                            <asp:TextBox ID="txtCashDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                        <td width="30%"></td>
                    </tr>
                    <tr id="tr_Bank" runat="server">
                        <td align="left" width="20%"><span class="field-label">Bank A/C</span></td>
                        <td align="left" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="40%" align="left" class="p-0">
                            <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank(); return false;" />
                                    </td>
                                    <td width="60%" align="left" class="p-0">
                            <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                                    </td>
                         <td width="30%"></td>
                        
                    </tr>
                    <tr id="tr_Cheque" runat="server">
                        <td align="left" width="20%"><span class="field-label">Cheque Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtChequedate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        

                        <td align="left" width="20%"><span class="field-label">Cheque Lot</span></td>
                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td width="40%" align="left" class="p-0">
                                        <asp:TextBox ID="txtChqBook" runat="server"></asp:TextBox>
                            <a href="#" onclick="get_Cheque(); return false;">
                                <img alt="Cheque" id="IMG1" language="javascript" src="../Images/cal.gif" runat="server" /></a>
                                    </td>
                                    <td width="60%" align="left" class="p-0">
                                        <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                       </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td width="30%" align="left" valign="top">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <br />
                            <asp:CheckBox ID="chkPersonal" runat="server" Text="Personal" Checked="True" CssClass="field-label" />
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT ERN_ID, ERN_DESCR FROM EMPSALCOMPO_M WHERE (ERN_TYP = 0) order by IsNull(ERN_ORDER,100) asc"></asp:SqlDataSource>

                        </td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Received By</span></td>
                        <td align="left" width="30%" valign="top">
                            <asp:TextBox ID="txtReceived" runat="server" TextMode="SingleLine"></asp:TextBox>

                        </td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">No of Installments</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtInst" runat="server"></asp:TextBox>
                            <asp:Button ID="btnAddInst" runat="server" Text="Add" cssclass="button"/></td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvLoan" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="Installment">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dates" DataFormatString="{0:MMM/yyyy}" HeaderText="Date"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AMOUNT" HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAMOUNT" HeaderText="Paid Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnk_gvLoan_edit" runat="server" CausesValidation="False" OnClick="lnk_gvLoan_edit_Click"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Total Allocated</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAlloted" runat="server" CssClass="inputbox" Enabled="False"></asp:TextBox></td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr id="tr_add" runat="server">
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDDateAdd" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDAmountAdd" runat="server" CssClass="inputbox" ></asp:TextBox>
                            <asp:Button ID="btnAddedit" runat="server" Text="Add" cssclass="button"/></td>
                    </tr>
                    <tr id="tr_RemarksEdit" runat="server">
                        <td align="left" width="20%"><span class="field-label">Remarks (Edit)</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEditRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                TextMode="MultiLine" ></asp:TextBox></td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr id="tr_update" runat="server">
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDAmount" runat="server" CssClass="inputbox" Width="50%"></asp:TextBox>
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" cssclass="button"/>
                            <asp:Button ID="btnInstCancel" runat="server" Text="Cancel" cssclass="button" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Font-Bold="True" Text="Print" CausesValidation="False" /></td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" />
                <asp:HiddenField ID="hCheqBook" runat="server" />

                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtDocdate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton4" TargetControlID="txtChequedate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton2" TargetControlID="txtDDateAdd">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtDDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
