<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empLeaveAdj_View.aspx.vb" Inherits="Payroll_empLeaveAdj_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Employee Leave Adjustment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left"  valign="middle">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        <td align="right" valign="middle">
                            <asp:RadioButton ID="rbPositive" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="Adjustment" Text="Addition" />
                            <asp:RadioButton ID="rbNegative" runat="server" AutoPostBack="True" CssClass="field-label" 
                                GroupName="Adjustment" Text="Deduction" />
                            <asp:RadioButton ID="rbAll" runat="server"
                                AutoPostBack="True" CssClass="field-label"  GroupName="Adjustment" Text="All" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" valign="top">
                            <table align="left" style="border-collapse: collapse" cellpadding="5" cellspacing="0" width="100%">
                                <%--  <tr class="title-bg">
                        <td align="left" valign="middle"></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:GridView ID="gvPrePayment" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                           EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20">
                                            <Columns>
                                                <asp:TemplateField HeaderText="EAH_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("EAH_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEAH_ID" runat="server" Text='<%# Bind("EAH_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee <br/> No.">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEmpNoH" runat="server" Text='<%# Bind("Empno") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEmpNo" runat="server" CssClass="gridheader_text" Text="Employee No."></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchEmpNo" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchEmpName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("Empno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Emp Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EName") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEmpNameH" runat="server" CssClass="gridheader_text" Text="Employee Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchEmpName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Leave Type">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblLeaveTypeH" runat="server" CssClass="gridheader_text" Text="Leave Type"></asp:Label><br />
                                                        <asp:TextBox ID="txtLeaveType" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchLeaveType" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchLeaveType_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaveType" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblFromDateH" runat="server" CssClass="gridheader_text" Text="From Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchFromDate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchFromDate_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFromdate" runat="server" Text='<%# Bind("DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date ">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("DTFROM") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label1" runat="server" CssClass="gridheader_text" Text="To Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchToDate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchFromDate_Click" />
                                                    </HeaderTemplate>

                                                    <ControlStyle />
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDate" runat="server" Text='<%# Bind("DTTO", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Month">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("DTTO") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblMonthH" runat="server" CssClass="gridheader_text" Text="Pay Month"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtMonth" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchMonth" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchMonth_Click" />
                                                    </HeaderTemplate>

                                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMonth" runat="server" Text='<%# Bind("PAYMONTH") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("PAYMONTH") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblYearH" runat="server" CssClass="gridheader_text" Text="Pay Year"></asp:Label><br />
                                                        <asp:TextBox ID="txtYear" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchYear" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchYear_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblYear" runat="server" Text='<%# Bind("PAYYEAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("PAYYEAR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblRemarkH" runat="server" CssClass="gridheader_text" Text="Remark"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchRemark" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchRemark_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemark" runat="server" Text='<%# Bind("REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_9" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_10" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

