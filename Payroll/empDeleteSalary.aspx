<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDeleteSalary.aspx.vb" Inherits="Payroll_empDeleteSalary" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
<script language="javascript" type="text/javascript">
 function funSearch(e){
 
      var evt = e ? e : window.event;
       var bt = document.getElementById("<%=btnEnterSearch.ClientID %>");
       
              if (bt){
          if (evt.keyCode == 13){
                bt.click();
               return false;
          }
       }
 }
function showhide_leavetype(id)
 { 
    if  (document.getElementById(id).className+''=='display_none')
    {
    document.getElementById(id).className='';
    }
    else
    {
    document.getElementById(id).className='display_none';
    }   
 }
 function change_chk_state(src)
         {
               var chk_state=(src.checked);

               for(i=0; i<document.forms[0].elements.length; i++)
               {
                if (document.forms[0].elements[i].type=='checkbox' )
                    {
                    document.forms[0].elements[i].checked=chk_state;
                    }
               }
           }     
 
     
    </script>
   
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-usd mr-3"></i> Remove Processed Salary
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table  width="100%" align="center" >
    <tr>
    <td  colspan="4" align="left">
        <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
    </td>    
    </tr>
 </table>
 <table width="100%" align="center" >            
            
     <tr>
         <td align="center">
             <table width="100%">
                 <tr>
                     <td align="left" width="20%">
                         <span class="field-label">Select Month</span></td>
                    
                     <td align="left" width="20%">
                         <asp:DropDownList id="ddMonth" runat="server" Width="100%" AutoPostBack="True">
                         </asp:DropDownList></td>
                     <td align="left" width="30%"></td>
                      <td align="left" width="30%"></td>
                 </tr>
             </table>
         </td>
     </tr>
            <tr id="Trbank"> 
          
            <td align="center" colspan="4"  valign ="top">
                <asp:GridView ID="gvSalary" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" 
                    Width="100%" AllowPaging="True" 
                    PageSize="15" >
                    <Columns>
                    <asp:TemplateField>
                    <HeaderTemplate>
                    EMPLOYEE No.<br />
                    <asp:TextBox ID="txtEmpNo" runat="server" Width="75%" onkeypress="return funSearch(event)"></asp:TextBox>
                    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                             </HeaderTemplate>
                                <ItemStyle />
                                <ItemTemplate>
                                    <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                </ItemTemplate>
                          </asp:TemplateField>
                      
                        <asp:TemplateField HeaderText="EMP NAME">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                   EMPLOYEE NAME<br />
                                   <asp:TextBox ID="txtEmpName" runat="server" Width="75%" onkeypress="return funSearch(event)"></asp:TextBox>
                                   <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                        <asp:BoundField DataField="MONTHYEAR" HeaderText="Month" ReadOnly="True" SortExpression="MONTHYEAR" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BANKCASH" HeaderText="Pay Mode" ReadOnly="True" SortExpression="BANKCASH" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESD_EARN_NET" HeaderText="Amount" SortExpression="ESD_EARN_NET" DataFormatString="{0:0.00}" >
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Remove">
                            <ItemTemplate>
                                <input id="chkESD_ID" runat="server" value='<%# Bind("ESD_ID") %>' type="checkbox" />
                            </ItemTemplate>
                             <HeaderTemplate>
                                <input id="chkAll" onclick="change_chk_state(this);" type="checkbox" runat="server" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ESD_EMP_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblESD_EMP_ID" runat="server" Text='<%# Bind("ESD_EMP_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
            <tr>
                <td  colspan="4" align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Remove Salary" />&nbsp;
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
        </table>
         <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                     <asp:Button id="btnEnterSearch" runat="server" style="display:none" /> 


            </div>
        </div>
    </div>
</asp:Content>

