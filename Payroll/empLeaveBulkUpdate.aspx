<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empLeaveBulkUpdate.aspx.vb"
    Inherits="Payroll_empLeaveBulkUpdate" Theme="General" Title="Employee Leave Bulk Update" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/sb-admin.css" rel="Stylesheet"  type="text/css"/>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../cssfiles/RadStyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">   
      
    </script>
    <style>

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 15px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .RadComboBox_Default {
            width: 100% !important;
        }
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image:url(../Images/calender.gif)!important;
            width: 30px;
            height: 30px;
        }
        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active{
            background-image:url(../Images/calender.gif)!important;
            width: 30px;
            height: 30px;
            background-position: 0;
        }

        .RadPicker { width:80% !important;
        }
        table.RadCalendar_Default {
            background:#ffffff !important;
        }
        .caption {
            padding-bottom :0rem !important;
            padding-top : 0rem !important;
        }
       .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
           background-position : 0 0 !important;
       }
       .RadPicker .rcCalPopup, .RadPicker .rcTimePopup {
           width:30px !important;
           height:30px !important;
       }
        .RadGrid_Default {
            border: 0px !important;
        }
        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border: 1px solid rgba(0,0,0,0.16);
        }
    </style>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" style="table-layout: fixed">
    <form id="form1" runat="server" style="table-layout: fixed; overflow: hidden;">
    <!--1st drop down menu -->
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600"
        EnablePageMethods="true">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center" width="100%">
                <tr>
                    <td  width="100%" align="center" class="title-bg-lite">
                        Employee Leave Update
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table align="center" width="100%">
                            <tr>
                                <td width="20%" align="left">
                                    <span class="field-label">Employee No.</span>
                                </td>
                                
                                <td width="30%" align="left">
                                    <asp:Label ID="lblEmpNo" runat="server" 
                                        CssClass="field-value"></asp:Label>
                                </td>
                                <td width="20%" align="left">
                                    <span class="field-label">Employee Name</span>
                                </td>
                                
                                <td width="30%" align="left">
                                    <asp:Label ID="lblEMPName" runat="server"  
                                        CssClass="field-value"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                   <span class="field-label"> From Date<span class="field-label">
                                </td>
                               
                                <td align="left">
                                    <telerik:RadDatePicker ID="RadFromDate" runat="server" DateInput-EmptyMessage="MinDate"
                                        CssClass="RadLabel" AutoPostBack="True">
                                        <DateInput EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" AutoPostBack="True">
                                        </DateInput>
                                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                    </telerik:RadDatePicker>
                                </td>
                                <td width="10%" align="left">
                                   <span class="field-label"> To Date</span>
                                </td>
                                
                                <td width="65%" align="left">
                                    <telerik:RadDatePicker ID="RadToDate" runat="server" DateInput-EmptyMessage="MinDate"
                                        CssClass="RadLabel" AutoPostBack="True">
                                        <DateInput EmptyMessage="MinDate"  DateFormat="dd/MMM/yyyy" AutoPostBack="True">
                                        </DateInput>
                                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                    </telerik:RadDatePicker>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnView" runat="server" CssClass="button" Text="View"  Visible="False" />
                                    <asp:Button ID="BtnSave" runat="server" CssClass="button" Text="Save"  />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <telerik:RadGrid ID="RadEmpAttendance" runat="server" AllowSorting="True" enableajax="True"
                            ShowStatusBar="True" AutoGenerateColumns="False" GroupingEnabled="False" ShowGroupPanel="True"
                            OnItemDataBound="RadEmpAttendance_ItemDataBound" Width="100%" GridLines="None"
                            HorizontalAlign="Left">
                            <%--<AlternatingItemStyle CssClass="RadGridAltRow" />--%>
                            <ItemStyle CssClass="RadGridRow" />
                            <MasterTableView GridLines="Both">
                                <ExpandCollapseColumn Visible="False">
                                    <HeaderStyle Width="19px" />
                                </ExpandCollapseColumn>
                                <RowIndicatorColumn Visible="False">
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <DetailTables>
                                </DetailTables>
                                <ExpandCollapseColumn Visible="True">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn DataField="EMP_ID" HeaderText="EMPID" UniqueName="colEMPID"
                                        Visible="False">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnEMPID" runat="server" Value='<%# Eval("EMP_ID") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="ATT_DATE" HeaderText="DATE" UniqueName="colInTime" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="ATT_DATELabel" runat="server" Text='<%# Eval("ATT_DATE","{0:D}") %>'
                                                Style="text-align: left"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="ATT_TIME" HeaderText="IN TIME" UniqueName="colInTime">
                                        <ItemTemplate>
                                            <asp:Label ID="ATT_TIMELabel" runat="server" Text='<%# Eval("ATT_TIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="ATT_STATUS" UniqueName="ColDDStatus" HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnStatus" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ATT_STATUS")%>' />
                                            <telerik:RadComboBox ID="StatusRadComboBox" runat="server" EmptyMessage="Type here to search..."
                                                EnableScreenBoundaryDetection="False">
                                            </telerik:RadComboBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="REMARKS" UniqueName="colTXTRemarks" HeaderText="REMARKS">
                                        <ItemTemplate>
                                            <telerik:RadTextBox ID="RemarksRadTextBox" runat="server" EnableScreenBoundaryDetection="True"
                                                TextMode="MultiLine" Rows="1" Text='<%# Eval("REMARKS") %>' ShouldResetWidthInPixels="False"
                                                >
                                            </telerik:RadTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="bDocChecked" HeaderText="DOC CHECK" UniqueName="colDocCheck">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDocChecked" runat="server" Checked='<%# Bind("bDocChecked") %>'
                                                Enabled="true" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="bLastUpdateDate" HeaderText="LOG DATE" UniqueName="column" ItemStyle-Wrap="false" >
                                        <ItemTemplate>
                                            <asp:Label ID="bLastUpdateDateLabel" runat="server" Text='<%# Eval("bLastUpdateDate") %>'></asp:Label>
                                            <asp:HiddenField ID="hdnLeaveAvailable" runat="server" Value='<%# Eval("LeaveAvailable") %>' />
                                            <asp:HiddenField ID="hdnLeaveStatus" runat="server" Value='<%# Eval("LeaveStatus") %>' />
                                            <asp:HiddenField ID="hdnProcessed" runat="server" Value='<%# Eval("Processed") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn ButtonType="ImageButton">
                                    </EditColumn>
                                </EditFormSettings>
                                <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                            </MasterTableView>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                            <ClientSettings EnableRowHoverStyle="false">
                            </ClientSettings>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="h_Emp_ID" runat="server" />
            <asp:HiddenField ID="h_ATT_DATE" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
