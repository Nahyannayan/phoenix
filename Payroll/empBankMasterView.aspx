<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empBankMasterView.aspx.vb" Inherits="Payroll_empBankMasterView" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>



    <script language="javascript" type="text/javascript">

        function test(val) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID%>").value = val + '__' + path;
        }
        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1() %>").src = path;
                 document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
             }

             function test2(val) {
                 var path;
                 if (val == 'LI') {
                     path = '../Images/operations/like.gif';
                 } else if (val == 'NLI') {
                     path = '../Images/operations/notlike.gif';
                 } else if (val == 'SW') {
                     path = '../Images/operations/startswith.gif';
                 } else if (val == 'NSW') {
                     path = '../Images/operations/notstartwith.gif';
                 } else if (val == 'EW') {
                     path = '../Images/operations/endswith.gif';
                 } else if (val == 'NEW') {
                     path = '../Images/operations/notendswith.gif';
                 }
                 document.getElementById("<%=getid2() %>").src = path;
              document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
          }
          function test3(val) {
              var path;
              if (val == 'LI') {
                  path = '../Images/operations/like.gif';
              } else if (val == 'NLI') {
                  path = '../Images/operations/notlike.gif';
              } else if (val == 'SW') {
                  path = '../Images/operations/startswith.gif';
              } else if (val == 'NSW') {
                  path = '../Images/operations/notstartwith.gif';
              } else if (val == 'EW') {
                  path = '../Images/operations/endswith.gif';
              } else if (val == 'NEW') {
                  path = '../Images/operations/notendswith.gif';
              }
              document.getElementById("<%=getid3() %>").src = path;
                     document.getElementById("<%=h_selected_menu_4.ClientID %>").value = val + '__' + path;
                 }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">

                    <tr>
                        <td>
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="center" class="matters" valign="top">
                                                    <asp:GridView ID="gvCommonEmp" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        HeaderStyle-Height="30" PageSize="20" Width="100%" CellPadding="2">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Code">
                                                                <HeaderTemplate>
                                                                    Code<br />
                                                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnCodeSearch_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcode" runat="server" Text='<%# Bind("BNK_SHORT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Description" SortExpression="DESCR">
                                                                <EditItemTemplate>
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    Description<br />
                                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnSearchpar_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("BNK_DESCRIPTION") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Transfer Desc.">
                                                                <EditItemTemplate>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccN" runat="server" Text='<%# Bind("BNK_HEADOFFICE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Head Office<br />
                                                                    <asp:TextBox ID="txtAmtName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchAcc" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnSearchAcc_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Shortname">
                                                                <EditItemTemplate>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAc" runat="server" Text='<%# Bind("BNK_SHORTNAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Short name<br />
                                                                    <asp:TextBox ID="txtAmtNamec" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchAccc" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnSearchAcc_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:TemplateField HeaderText="BNK_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBNK_ID" runat="server" Text='<%# Bind("BNK_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_4" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
