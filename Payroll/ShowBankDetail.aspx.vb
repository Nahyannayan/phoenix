Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_ShowBankDetail
    Inherits System.Web.UI.Page

    'Version        Date        Author      Change
    '1.1            9-may-2011  Swapna      To check ABC category
    '1.2            6-Jun-2012  Swapna      To add employee filter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GRIDBIND()
    End Sub



    Sub GRIDBIND()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_filter As String = ""
            If Request.QueryString("filter") <> "" Then
                str_filter = "AND IsNull(EM.EMP_ABC,'0') in (" & Request.QueryString("filter") & ")"
            End If
            str_Sql = "SELECT DISTINCT LEFT(DATENAME(MONTH, CAST(CAST(ESD.ESD_MONTH AS varchar(4)) +" _
              & " '/22' + '/' + CAST(ESD.ESD_YEAR AS varchar(4)) AS DATETIME)), 3) + " _
              & " '/' + CAST(ESD.ESD_YEAR AS varchar(4)) AS PayMonth," _
              & " BANK_M.BNK_DESCRIPTION,  ESD.ESD_EARN_NET ,ESD.ESD_BANK, " _
              & " EM.EMPNO, ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+ ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
              & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
              & " BANK_M ON ESD.ESD_BANK = BANK_M.BNK_ID" _
              & " INNER JOIN EMPLOYEE_M AS EM ON ESD.ESD_EMP_ID = EM.EMP_ID " _
              & " WHERE (ESD.ESD_MODE = 1) AND (ESD.ESD_PAID = 0)" _
              & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "')" _
              & " AND ESD.ESD_YEAR in (" & Session("yearselected") & ") and ESD.ESD_MONTH in (" & Session("monthselected") & ")" _
              & " AND ESD.ESD_BANK='" & Request.QueryString("ela_id") & "'" _
              & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE (EHD_bHold = 1) )" _
              & str_filter & ""
            'V1.2
            If Request.QueryString("EmpId") <> "" Then
                str_Sql = str_Sql + " AND ESD_EMP_ID= " & Request.QueryString("EmpId")
            End If
           
            If Request.QueryString("WPS") = 0 Then
                str_Sql = str_Sql + "AND ESD.ESD_BProcessWPS=0"
            End If
            If Request.QueryString("WPS") = 1 Then
                str_Sql = str_Sql + "AND ESD.ESD_BProcessWPS=1"
            End If

            Dim strFFS As String = " AND isnull(ESD.ESD_ERD_ID,0)=0 "
            If Request.QueryString("IsFFS") = True Then
                strFFS = " AND isnull(ESD.ESD_ERD_ID,0)>0 "
            End If
            str_Sql = str_Sql + strFFS

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            

            gvApprovals.DataSource = ds.Tables(0)
            gvApprovals.DataBind()
            str_Sql = " select BANK_M.BNK_DESCRIPTION, SUM(ESD.ESD_EARN_NET) AS amount,ESD.ESD_BANK " _
                      & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
                      & " BANK_M ON ESD.ESD_BANK = BANK_M.BNK_ID" _
                      & " INNER JOIN EMPLOYEE_M EM ON EM.EMP_ID = ESD.ESD_EMP_ID" _
                      & " WHERE (ESD.ESD_MODE = 1) AND (ESD.ESD_PAID = 0) " _
                      & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "') and BANK_M.BNK_ID='" & Request.QueryString("ela_id") & "'" _
                      & " AND ESD.ESD_YEAR in (" & Session("yearselected") & ") and ESD.ESD_MONTH in (" & Session("monthselected") & ")" _
                      & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE (EHD_bHold = 1) )" _
                      & " AND ESD.ESD_BProcessWPS=" & Request.QueryString("WPS") & ""
            'V1.2
            If Request.QueryString("EmpId") <> "" Then
            str_Sql = str_Sql + " AND ESD_EMP_ID= " & Request.QueryString("EmpId")
            End If

            str_Sql = str_Sql + strFFS + " GROUP BY  BANK_M.BNK_DESCRIPTION,ESD.ESD_BANK"


            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                'PayMonth, BNK_DESCRIPTION, amount, ESD_BANK
                lblHead.Text = "Details for Bank : " & ds.Tables(0).Rows(0)("BNK_DESCRIPTION")
                lblTotal.Text = " Total : " & AccountFunctions.Round(ds.Tables(0).Rows(0)("amount"))

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


End Class
