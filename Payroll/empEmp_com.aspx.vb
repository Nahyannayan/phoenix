Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Category_Emp
    Inherits System.Web.UI.Page
    Public _SelectedCatID As String
    
    Dim splitquery() As String

    Dim Encr_decrData As New Encryption64


    Public Property SelectedCatID() As Integer
        Get
            Return _SelectedCatID
        End Get
        Set(ByVal value As Integer)
            _SelectedCatID = value
        End Set
    End Property
    
    
    Protected Sub gvCommonEmp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCommonEmp.SelectedIndexChanged
        'Dim CatID As Integer = gvCommonEmp.SelectedDataKey.Value
        'SelectedCatID = CatID

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = String.Format("~\payroll\empEmp_com_Edit.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P050010" And ViewState("MainMnu_code") <> "P050015" And ViewState("MainMnu_code") <> "P050020" And ViewState("MainMnu_code") <> "P050025" And _
                ViewState("MainMnu_code") <> "P050030" And ViewState("MainMnu_code") <> "P050035" And ViewState("MainMnu_code") <> "P050045" And ViewState("MainMnu_code") = "P050022" And ViewState("MainMnu_code") = "P050024" And _
                ViewState("MainMnu_code") = "P050050" And ViewState("MainMnu_code") = "P050065") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("MainMnu_code") = "P050010" Then
                        lblHead.Text = "Category"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select * from (select ECT_ID as id,ECT_DESCR as descr,0 as paid,0 as eob  from EMPCATEGORY_M)a where id<>'' "


                    ElseIf ViewState("MainMnu_code") = "P050015" Then
                        lblHead.Text = "Grade"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select * from (SELECT EGD_ID AS ID, EGD_DESCR AS DESCR ,0 as paid,0 as eob FROM EMPGRADES_M )a where id<>'' "

                    ElseIf ViewState("MainMnu_code") = "P050020" Then
                        lblHead.Text = "School Designation"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select * from (SELECT DES_ID AS ID, DES_DESCR AS DESCR, 0 as paid,0 as eob  FROM EMPDESIGNATION_M where des_flag='SD')a where id<>''"

                    ElseIf ViewState("MainMnu_code") = "P050022" Then
                        lblHead.Text = "MOE Designation"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select * from (SELECT DES_ID AS ID, DES_DESCR AS DESCR, 0 as paid,0 as eob  FROM EMPDESIGNATION_M  where des_flag='ME')a where id<>'' "
                    ElseIf ViewState("MainMnu_code") = "P050024" Then
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False
                        lblHead.Text = "MOL Designation"
                        ViewState("str_Sql") = "Select * from (SELECT DES_ID AS ID, DES_DESCR AS DESCR, 0 as paid,0 as eob  FROM EMPDESIGNATION_M where des_flag='ML')a where id<>'' "

                    ElseIf ViewState("MainMnu_code") = "P050025" Then
                        lblHead.Text = "Teaching Grade"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select * from (SELECT TGD_ID AS ID, TGD_DESCR AS DESCR,0 as paid,0 as eob  FROM EMPTEACHINGGRADES_M )a where id<>'' "

                    ElseIf ViewState("MainMnu_code") = "P050030" Then
                        lblHead.Text = "Job Responsibility"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select * from (SELECT Res_ID AS ID, Res_DESCR AS DESCR ,0 as paid,0 as eob FROM EMPJOBRESPONSILITY_M )a where id<>''"

                    ElseIf ViewState("MainMnu_code") = "P050035" Then
                        lblHead.Text = "Leave Type"
                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ' gvCommonEmp.Columns(2).Visible = False
                        ' gvCommonEmp.Columns(3).Visible = False
                        ViewState("str_Sql") = "Select * from (SELECT ELT_ID AS ID, ELT_DESCR AS DESCR,ELT_bPaid as paid,ELT_eob as eob FROM EMPLEAVETYPE_M )a where id<>''"

                    ElseIf ViewState("MainMnu_code") = "P050045" Then
                        lblHead.Text = "Document Master"

                        ' gvCommonEmp.Columns(2).Visible = False

                        gvCommonEmp.Columns(3).Visible = False
                        ViewState("str_Sql") = "Select * from (SELECT ESD_ID AS ID,ESD_DESCR AS DESCR,ESD_bCHKEXPIRY as paid,0 as eob FROM EMPSTATDOCUMENTS_M)a where id<>''"

                    ElseIf ViewState("MainMnu_code") = "P050050" Then
                        lblHead.Text = "Religion"

                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False
                        ViewState("str_Sql") = "Select * from (SELECT RLG_ID AS ID, RLG_DESCR AS DESCR,0 as paid,0 as eob FROM Religion_M )a where id<>''"
                    ElseIf ViewState("MainMnu_code") = "P050065" Then
                        lblHead.Text = "Department"

                        gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False
                        ViewState("str_Sql") = "Select * from (SELECT DPT_ID AS ID, DPT_DESCR AS DESCR,0 as paid,0 as eob FROM DEPARTMENT_M )a where id<>''"


                    End If
                    gridbind()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception

                lblError.Text = "Request could not be processed "
            End Try

        End If
        
        set_Menu_Img()
    End Sub


    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            
            Dim ds As New DataSet

            Dim str_filter_code, str_filter_name, str_txtCode, str_txtName As String
            ''''''''
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            Dim txtSearch As New TextBox

            '''''''''
            If gvCommonEmp.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("a.ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("a.DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, ViewState("str_Sql") & str_filter_code & str_filter_name & " Order by a.DESCR")

            If ds.Tables(0).Rows.Count > 0 Then

                gvCommonEmp.DataSource = ds.Tables(0)
                gvCommonEmp.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                ds.Tables(0).Rows(0)(2) = True
                ds.Tables(0).Rows(0)(3) = True
                gvCommonEmp.DataSource = ds.Tables(0)
                Try
                    gvCommonEmp.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCommonEmp.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCommonEmp.Rows(0).Cells.Clear()
                gvCommonEmp.Rows(0).Cells.Add(New TableCell)
                gvCommonEmp.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCommonEmp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCommonEmp.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            If ViewState("MainMnu_code") = "P050045" Then
                Dim lblHeader As New Label
                lblHeader = gvCommonEmp.HeaderRow.FindControl("lblPaidH")
                lblHeader.Text = "Expiry"
            End If
            set_Menu_Img()
            txtSearch = gvCommonEmp.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvCommonEmp.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub
    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    


    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchpar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()

    End Sub

    Protected Sub gvCommonEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCommonEmp.PageIndexChanging
        gvCommonEmp.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

   
    Protected Sub gvCommonEmp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCommonEmp.RowCommand
        If e.CommandName = "View" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvCommonEmp.Rows(index), GridViewRow)

            Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            Dim Eid As String = UserIDLabel.Text
            Dim url As String

            Eid = Encr_decrData.Encrypt(Eid)
            ViewState("datamode") = "view"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\payroll\empEmp_com_Edit.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", ViewState("MainMnu_code"), ViewState("datamode"), Eid)
            Response.Redirect(url)

            'SelectedUserID = UserIDLabel.Text

        End If
    End Sub

     
End Class
