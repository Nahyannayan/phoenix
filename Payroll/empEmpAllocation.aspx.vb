Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic

Partial Class empEmpAllocation
    Inherits System.Web.UI.Page
    
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'collect the url of the file to be redirected in view state
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P050040" And ViewState("MainMnu_code") <> "A100065") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                Dim menu_rights As Integer

                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))





                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                ' btnSave.Attributes.Add("onClick", "return CheckErrors()")

                Try


                    Dim bunit_id As String = String.Empty
                    Dim bunit_name As String = String.Empty
                    Using readerUserDetail As SqlDataReader = AccessRoleUser.GetBusinessUnitsByUser(Session("sBsuid"))
                        If readerUserDetail.HasRows = True Then
                            While readerUserDetail.Read()
                                bunit_id = Convert.ToString(readerUserDetail("id"))
                                bunit_name = Convert.ToString(readerUserDetail("e_name"))
                                rdBus_unit.Items.Add(New ListItem(bunit_name, bunit_id))

                            End While
                            rdBus_unit.SelectedIndex = 0
                        End If

                    End Using


                    If ViewState("MainMnu_code") = "P050040" Then


                        lblTitle.Text = "Employee Allocation"

                        lblEmp.Text = "Select Employee Name"

                        btnEmpName.Text = "Employee Name"
                        chkEmp.Items.Clear()

                        Using readerEmpName As SqlDataReader = AccessRoleUser.GetEmployee_BsuUnit(Session("sBsuid"))

                            If readerEmpName.HasRows = True Then
                                While readerEmpName.Read()
                                    bunit_id = Convert.ToString(readerEmpName("ID"))
                                    bunit_name = Convert.ToString(readerEmpName("E_Name"))
                                    chkEmp.Items.Add(New ListItem(bunit_name, bunit_id))

                                End While
                            End If

                        End Using



                    ElseIf ViewState("MainMnu_code") = "A100065" Then
                        lblTitle.Text = "Fixed Asset Allocation"

                        lblEmp.Text = "Select Asset Description"

                        btnEmpName.Text = "Fixed Asset"




                        chkEmp.Items.Clear()

                        Using readerEmpName As SqlDataReader = AccessRoleUser.GetFixedAsset_BsuUnit(Session("sBsuid"))

                            If readerEmpName.HasRows = True Then
                                While readerEmpName.Read()
                                    bunit_id = Convert.ToString(readerEmpName("ID"))
                                    bunit_name = Convert.ToString(readerEmpName("E_Name"))
                                    chkEmp.Items.Add(New ListItem(bunit_name, bunit_id))

                                End While
                            End If

                        End Using


                    End If



                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, "EmpAlloaction page")
                    lblError.Text = UtilityObj.getErrorMessage("1000")

                End Try


            End If

            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        Try
            If p_imgsrc <> "" Then
                'mnu_1_img.Src = p_imgsrc
            End If
            'Return mnu_1_img.ClientID

        Catch ex As Exception
            Return ""
        End Try


    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
       

        If Page.IsValid Then
            If ViewState("MainMnu_code") = "P050040" Then

                Call saveEmpAllocation()
            ElseIf ViewState("MainMnu_code") = "A100065" Then
                Call saveFixedAssetAllocation()

            End If



        End If
    End Sub
    Sub saveFixedAssetAllocation()
        Dim status As Integer
        Dim flagAudit As Integer
        Dim FAL_FAS_CODE As String

        Dim transaction As SqlTransaction

        'update  the new user
        Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim FAL_BSU_ID As String = rdBus_unit.SelectedItem.Value

                Dim FAL_REMARKS As String = txtRemark.Text
                Dim FAL_DTFROM As String = txtFromDate.Text
                'loop for each employee id from checkboxlist selected
                For Each item As ListItem In chkEmp.Items
                    If (item.Selected) Then
                        FAL_FAS_CODE = item.Value

                        status = AccessRoleUser.FIX_CODE_Alloc_insert(FAL_FAS_CODE, FAL_BSU_ID, Session("sBsuid"), FAL_REMARKS, FAL_DTFROM, transaction)

                        If status <> 0 Then

                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))

                        End If

                        'call the Utility class to track the Edit change on the control

                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), FAL_FAS_CODE, "Insert", Page.User.Identity.Name.ToString, Me.Page)


                        If flagAudit <> 0 Then

                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))

                        End If


                    End If
                Next

                transaction.Commit()

                lblError.Text = "Record Updated Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message

                UtilityObj.Errorlog(myex.Message, "Fixed Asset Allocation")

            Catch ex As Exception

                transaction.Rollback()
                lblError.Text = "Record can not be Updated"
                UtilityObj.Errorlog(ex.Message, Page.Title)


            End Try

        End Using
    End Sub
    Sub saveEmpAllocation()
        Dim status As Integer
        Dim flagAudit As Integer
        Dim EAL_EMP_ID As String

        Dim transaction As SqlTransaction

        'update  the new user
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim EAL_BSU_ID As String = rdBus_unit.SelectedItem.Value

                Dim EAL_REMARKS As String = txtRemark.Text
                Dim EAL_FROMDT As String = txtFromDate.Text
                'loop for each employee id from checkboxlist selected
                For Each item As ListItem In chkEmp.Items
                    If (item.Selected) Then
                        EAL_EMP_ID = item.Value

                        status = AccessRoleUser.EMPID_Alloc_insert(EAL_EMP_ID, EAL_BSU_ID, Session("sBsuid"), EAL_REMARKS, EAL_FROMDT, transaction)

                        If status <> 0 Then

                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))

                        End If

                        'call the Utility class to track the Edit change on the control

                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EAL_EMP_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page)


                        If flagAudit <> 0 Then

                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))

                        End If


                    End If
                Next

                transaction.Commit()

                lblError.Text = "Record Updated Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message

                UtilityObj.Errorlog(myex.Message, "Employee Allocation")

            Catch ex As Exception

                transaction.Rollback()
                lblError.Text = "Record can not be Updated"
                UtilityObj.Errorlog(ex.Message, Page.Title)


            End Try

        End Using

    End Sub
    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvEmpNameValidate.ServerValidate
        Dim sflag As Boolean

        For Each item As ListItem In chkEmp.Items
            If (item.Selected) Then
                sflag = True
            End If
        Next

        If sflag Then
            args.IsValid = True
        Else
            If ViewState("MainMnu_code") = "P050040" Then
                cvEmpNameValidate.ErrorMessage = "Select the Employee to be  alloacted"
            ElseIf ViewState("MainMnu_code") = "A100065" Then
                cvEmpNameValidate.ErrorMessage = "Select the  Asset Description to be  alloacted"
            End If


            args.IsValid = False
        End If
    End Sub
    Protected Sub cvBsuValidate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvBsuValidate.ServerValidate
        Dim sflag As Boolean

        For Each item As ListItem In rdBus_unit.Items
            If (item.Selected) Then
                sflag = True
            End If
        Next

        If sflag Then
            args.IsValid = True
        Else
            'CustomValidator1.Text = ""
            args.IsValid = False
        End If
    End Sub

    'Protected Sub btnBsu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBsu.Click, btnEmpName.Click
    '    Dim b As Button
    '    b = CType(sender, Button)

    '    Select Case b.ID
    '        Case "btnBsu"
    '            callSearch("B")
    '            Label4.Text = "You clicked the first button"
    '        Case "btnEmpName"
    '            callSearch("E")
    '            Label4.Text = "You clicked the second button"

    '    End Select


    'End Sub

   
   
    Protected Sub btnBsu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBsu.Click
        Try

        

            Dim str_filter_name As String
            Dim str_search As String

            str_filter_name = ""

            Dim str_Sid_search() As String

            str_Sid_search = h_Selected_menu_1.Value.Split("__")
            str_search = str_Sid_search(0)

            If str_search = "LI" Then
                str_filter_name = " AND E_Name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "NLI" Then
                str_filter_name = "  AND  NOT E_Name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "SW" Then
                str_filter_name = " AND E_Name  LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "NSW" Then
                str_filter_name = " AND E_Name  NOT LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "EW" Then
                str_filter_name = " AND E_Name LIKE  '%" & txtSearch.Text & "'"
            ElseIf str_search = "NEW" Then
                str_filter_name = " AND E_Name NOT LIKE '%" & txtSearch.Text & "'"
            End If

            rdBus_unit.Items.Clear()

            Dim bunit_id As String = String.Empty
            Dim bunit_name As String = String.Empty
            Using readerUserDetail As SqlDataReader = AccessRoleUser.GetBusinessUnitsByUser_Filter(Session("sBsuid"), str_filter_name)
                If readerUserDetail.HasRows = True Then
                    While readerUserDetail.Read()
                        bunit_id = Convert.ToString(readerUserDetail("id"))
                        bunit_name = Convert.ToString(readerUserDetail("e_name"))
                        rdBus_unit.Items.Add(New ListItem(bunit_name, bunit_id))

                    End While
                    rdBus_unit.SelectedIndex = 0


                End If

            End Using

        Catch ex As Exception
            UtilityObj.Errorlog("Error in Business unit search")
            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try
    End Sub

    Protected Sub btnEmpName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpName.Click

        Try

            Dim str_filter_name, bunit_id, bunit_name As String
            Dim str_search As String

            str_filter_name = ""

            Dim str_Sid_search() As String

            str_Sid_search = h_Selected_menu_1.Value.Split("__")
            str_search = str_Sid_search(0)

            If str_search = "LI" Then
                str_filter_name = " AND E_Name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "NLI" Then
                str_filter_name = "  AND  NOT E_Name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "SW" Then
                str_filter_name = " AND E_Name  LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "NSW" Then
                str_filter_name = " AND E_Name  NOT LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "EW" Then
                str_filter_name = " AND E_Name LIKE  '%" & txtSearch.Text & "'"
            ElseIf str_search = "NEW" Then
                str_filter_name = " AND E_Name NOT LIKE '%" & txtSearch.Text & "'"
            End If

            chkEmp.Items.Clear()

            If ViewState("MainMnu_code") = "P050040" Then
                Using readerEmpName As SqlDataReader = AccessRoleUser.GetEmployee_BsuUnit_Filter(Session("sBsuid"), txtFromDate.Text, str_filter_name)

                    If readerEmpName.HasRows = True Then
                        While readerEmpName.Read()
                            bunit_id = Convert.ToString(readerEmpName("ID"))
                            bunit_name = Convert.ToString(readerEmpName("E_Name"))
                            chkEmp.Items.Add(New ListItem(bunit_name, bunit_id))

                        End While
                    End If

                End Using
            ElseIf ViewState("MainMnu_code") = "A100065" Then
                Using readerEmpName As SqlDataReader = AccessRoleUser.GetFixedAsset_BsuUnit_Filter(Session("sBsuid"), txtFromDate.Text, str_filter_name)

                    If readerEmpName.HasRows = True Then
                        While readerEmpName.Read()
                            bunit_id = Convert.ToString(readerEmpName("ID"))
                            bunit_name = Convert.ToString(readerEmpName("E_Name"))
                            chkEmp.Items.Add(New ListItem(bunit_name, bunit_id))

                        End While
                    End If

                End Using

            End If

        Catch ex As Exception
            UtilityObj.Errorlog("Error in Employee/Fixed Asset unit search")
            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

    End Sub

    Protected Sub txtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDate.TextChanged
        Dim datetime1 As Date
        Try
            chkEmp.Items.Clear()
            'convert the date into the required format so that it can be validate by Isdate function
            datetime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date

            If IsDate(datetime1) Then



                Dim str_filter_name As String = String.Empty
                Dim bunit_id, bunit_name As String
                If ViewState("MainMnu_code") = "P050040" Then
                    Using readerEmpName As SqlDataReader = AccessRoleUser.GetEmployee_BsuUnit_Filter(Session("sBsuid"), txtFromDate.Text, str_filter_name)

                        If readerEmpName.HasRows = True Then
                            While readerEmpName.Read()
                                bunit_id = Convert.ToString(readerEmpName("ID"))
                                bunit_name = Convert.ToString(readerEmpName("E_Name"))
                                chkEmp.Items.Add(New ListItem(bunit_name, bunit_id))

                            End While
                        End If

                    End Using
                ElseIf ViewState("MainMnu_code") = "A100065" Then
                    Using readerEmpName As SqlDataReader = AccessRoleUser.GetFixedAsset_BsuUnit_Filter(Session("sBsuid"), txtFromDate.Text, str_filter_name)

                        If readerEmpName.HasRows = True Then
                            While readerEmpName.Read()
                                bunit_id = Convert.ToString(readerEmpName("ID"))
                                bunit_name = Convert.ToString(readerEmpName("E_Name"))
                                chkEmp.Items.Add(New ListItem(bunit_name, bunit_id))

                            End While
                        End If

                    End Using
                End If
            End If

        Catch ex As Exception
            ValidationSummary1.HeaderText = "Enter valid date"


        End Try

    End Sub

    
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

    End Sub
End Class
#Region "only comments"
'    lblError.Text = "your custom error message"
'    If ValidationSummary1.Visible = True Then
'        ValidationSummary1.Visible = False
'    End If

'Else
'    If ValidationSummary1.Visible = False Then
'        ValidationSummary1.Visible = True
'        lblError.Text = ""
'    End If
#End Region