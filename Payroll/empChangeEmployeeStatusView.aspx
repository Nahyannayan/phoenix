<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empChangeEmployeeStatusView.aspx.vb" Inherits="Payroll_empChangeEmployeeStatusView" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
 <script language="javascript" type="text/javascript">
  
 
 function ShowLeaveDetail(id)
       {    
            var sFeatures;
            sFeatures="dialogWidth: 559px; ";
            sFeatures+="dialogHeight: 405px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)
         
            return false;
        }
    
    </script>                
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Employee Status
        </div>
        <div class="card-body">
            <div class="table-responsive">           
 
    <table align="center" width="100%">
            <tr valign="top" >
          
                <td valign="top" width="50%" align="left">
                <asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink>
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
                    <td align="right">
                        &nbsp;<input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        &nbsp;&nbsp;
                    </td>               
            </tr>
        </table>  <a id='top'></a>
    <table id="tbl_test" runat="server" align="center" width="100%">
            
            <tr>
                <td align="center" valign="top">
                    <asp:GridView ID="gvJournal" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                        EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <Columns>
<asp:TemplateField SortExpression="EMPNO" HeaderText="Emp No"><HeaderTemplate>
                                   Emp No<br />
                                       <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                       <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                
</HeaderTemplate>
<ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField SortExpression="EMP_NAME" HeaderText="Emp Name"><HeaderTemplate>
                                    Emp Name<br />
                                    <asp:TextBox ID="txtEmpname" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                
</HeaderTemplate>
<ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="From Date"><HeaderTemplate>
<asp:Label id="lblFromdate" runat="server" Text="From Date" __designer:wfdid="w140"></asp:Label><br />
<asp:TextBox id="txtFrom" runat="server" Width="75%" SkinID="Gridtxt" __designer:wfdid="w144"></asp:TextBox>
<asp:ImageButton id="btnBankACSearch" onclick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w145"></asp:ImageButton>
</HeaderTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
<ItemTemplate>
<asp:Label id="Label3" runat="server" Text='<%# Bind("EST_DTFROM", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w143"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="To Date"><HeaderTemplate>
<asp:Label id="lblTodate" runat="server" Text="To Date" __designer:wfdid="w140"></asp:Label><br />
<asp:TextBox id="txtTDate" runat="server" Width="75%" __designer:wfdid="w141"></asp:TextBox>
<asp:ImageButton id="btnAmtSearcha" onclick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w142"></asp:ImageButton>
</HeaderTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
<ItemTemplate>
<asp:Label id="Label4" runat="server" Text='<%# Bind("EST_DTTO", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w139"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Status"><HeaderTemplate>
Status
<asp:DropDownList id="ddlStatus" runat="server" __designer:wfdid="w127" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="Label6" runat="server" Text='<%# Bind("EST_DESCR") %>' __designer:wfdid="w126"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Remarks"><HeaderTemplate>
Remarks<br />
<asp:TextBox id="txtRemarks" runat="server" Width="75%" __designer:wfdid="w116" SkinID="Gridtxt"></asp:TextBox>
<asp:ImageButton id="btnNarration" onclick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w117"></asp:ImageButton>
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="Label5" runat="server" Text='<%# Bind("EST_REMARKS") %>' __designer:wfdid="w115"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="View">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
<ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="EAT_ID"><EditItemTemplate>
                                    &nbsp;
                                
</EditItemTemplate>
<ItemTemplate>
                                    <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("EST_ID") %>'></asp:Label>
                                
</ItemTemplate>
</asp:TemplateField>
</Columns>
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView> 
                </td>
            </tr>
        <tr style="display:none">
            <td align="left" colspan="9" valign="top">
            <table width="100%" class="table table-bordered table-row">
            <tr>
            <td style="width: 99px; color: red; height: 14px;">
                Status</td>
            <td style="width: 99px; color: red; height: 14px">
                Date1</td>
                <td style="width: 99px; color: red; height: 14px">
                Date2</td>
            </tr>
                <tr>
                    <td style="width: 59px">
                        Missing</td>
                    <td style="width: 99px">
                        Missing Date</td>
                    <td>
                        Last Working Date</td>
                </tr>
                <tr>
                    <td style="width: 59px">
                        Resigned</td>
                    <td style="width: 99px">
                        Resign Date</td>
                    <td>
                        Last Working Date</td>
                </tr>
                <tr>
                    <td style="width: 59px; height: 14px">
                        Terminated</td>
                    <td style="width: 99px; height: 14px">
                        Terminated Date</td>
                    <td style="height: 14px">
                        Last Working Date</td>
                </tr>
                <tr>
                    <td style="width: 59px; height: 14px">
                        Suspended</td>
                    <td style="width: 99px; height: 14px">
                        Suspended Date</td>
                    <td style="height: 14px">
                        Last Working Date</td>
                </tr>
                <tr>
                    <td style="width: 59px; height: 14px">
                        Absconding</td>
                    <td style="width: 99px; height: 14px">
                        Absconding Date</td>
                    <td style="height: 14px">
                        Last Working Date</td>
                </tr>
                <tr>
                    <td style="width: 59px; height: 14px">
                        Transfer</td>
                    <td style="width: 99px; height: 14px">
                        Transfer Date</td>
                    <td style="height: 14px">
                        Last Working Date</td>
                </tr>
                <tr>
                    <td style="width: 59px; height: 14px">
                        On Leave</td>
                    <td style="width: 99px; height: 14px">
                        From Date</td>
                    <td style="height: 14px">
                        To Date</td>
                </tr>
            </table>
            </td>
        </tr>
        </table>

            </div>
        </div>
    </div>

</asp:Content>
