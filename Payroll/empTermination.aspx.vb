Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Xml

Partial Class Payroll_empTermination
  Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version        Author               Date            Purpose
    ''1.1            swapna              27/Feb/2013     To display earning types bsu wise.
    ''1.2           rajesh              28/apr/2016         REF Task ID 97060
    ''2.0           Vikranth            18th/June/2019      checking to call KSA related procedure or another in FillGratuityDetails() function.

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            ViewState("ChkDeduction") = ""
            txtEmpNo.Attributes.Add("readonly", "readonly")
            txtEarning.Attributes.Add("readonly", "readonly")
            txtDeductions.Attributes.Add("readonly", "readonly")
            txtTotal.Attributes.Add("readonly", "readonly")

            GridViewArrears.Attributes.Add("bordercolor", "#1b80b6")
            gvEmpSalary.Attributes.Add("bordercolor", "#1b80b6")
            gvAssetAllocated.Attributes.Add("bordercolor", "#1b80b6")
            GridViewDeductions.Attributes.Add("bordercolor", "#1b80b6")
            FormViewGratuity.Attributes.Add("bordercolor", "#1b80b6")
            FormViewVacation.Attributes.Add("bordercolor", "#1b80b6")

            FormViewGratuity.DataBind()
            FormViewVacation.DataBind()
            GridViewArrears.DataBind()
            gvEmpSalary.DataBind()
            gvAssetAllocated.DataBind()
            GridViewDeductions.DataBind()
            BindENR()
            bindDeductionCombo()

            Session("Deductions") = CreateDataTableDeductions()
            ViewState("iID") = 0
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            btnUpdate.Visible = False
            btnChildCancel.Visible = False
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            '            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153070" And ViewState("MainMnu_code") <> "H000082" And ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If (ViewState("MainMnu_code") = "P153070" Or ViewState("MainMnu_code") = "H000082" Or ViewState("MainMnu_code") = "P153072" Or ViewState("MainMnu_code") = "P153073" Or ViewState("MainMnu_code") = "P153074") And (ViewState("datamode") = "edit" Or ViewState("datamode") = "view") Then
                ViewState("EREG_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                FillResignationDetails(ViewState("EREG_ID"))
            End If

            If ViewState("MainMnu_code") = "P153072" Then
                TBLSettlementDetails.Visible = False
                TBLVISACANCEL.Visible = True
                btnAdd.Visible = False
                btnSave.Text = "Save"
            ElseIf ViewState("MainMnu_code") = "P153073" Then
                TBLSettlementDetails.Visible = False
                TBLLCCANCEL.Visible = True
                btnAdd.Visible = False
                btnSave.Text = "Save"
            ElseIf ViewState("MainMnu_code") = "P153074" Then
                TBLSettlementDetails.Visible = False
                TBLEMPEXIT.Visible = True
                btnAdd.Visible = False
                btnSave.Text = "Save"
            End If
            If Request.QueryString("viewid") <> "" Then
                LockControls(True)
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                LockControls(False)
            End If
        End If
        If ViewState("MainMnu_code") = "P153072" Then
            lblTitle.Text = "Employee Visa Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153073" Then
            lblTitle.Text = "Employee Labour Card Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153074" Then
            lblTitle.Text = "Employee Exit"
        ElseIf ViewState("MainMnu_code") = "P153070" Then
            lblTitle.Text = "Final Settlement"
            btnAdd.Visible = False
            imgEmployee.Visible = False
        ElseIf ViewState("MainMnu_code") = "H000082" Then
            lblTitle.Text = "Approve Final Settlement"
        End If
        'PopupControlExtender1.DynamicContextKey = ViewState("EREG_ID")
    End Sub

    Sub FillResignationDetails(ByVal RegId As String)
        Try
            Dim Param(1) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@EREG_ID", RegId)

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetEmpFinalSettlementDetails", Param)
            If (ds.Tables(0).Rows.Count > 0) Then
                'lnkLog.Style.Add("display", "block")


                txtEmpNo.Text = ds.Tables(0).Rows(0).Item("EMPNO").ToString
                lbl_Empname.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
                lbldesg.Text = ds.Tables(0).Rows(0).Item("EMP_DES_DESCR").ToString
                lbldpt.Text = ds.Tables(0).Rows(0).Item("EMP_DEPT_DESCR").ToString
                lblLastWorkDate.Text = Format(ds.Tables(0).Rows(0).Item("EREG_LASTWORKINGDATE"), OASISConstants.DateFormat)
                lblResgDate.Text = Format(ds.Tables(0).Rows(0).Item("EREG_REGDATE"), OASISConstants.DateFormat)
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("EREG_REMARKS1").ToString        'V1.1
                DropDownListResignTerm.SelectedValue = ds.Tables(0).Rows(0).Item("EREG_REGTYPE").ToString
                h_Emp_No.Value = ds.Tables(0).Rows(0).Item("EREG_EMP_ID").ToString
                lblVisaBSU.Text = ds.Tables(0).Rows(0).Item("VISA_BSU_NAME").ToString
                lblWorkingBSU.Text = ds.Tables(0).Rows(0).Item("WORK_BSU_NAME").ToString
                lblDOJBSU.Text = Format(ds.Tables(0).Rows(0).Item("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                lblDOJGroup.Text = Format(ds.Tables(0).Rows(0).Item("EMP_JOINDT"), OASISConstants.DateFormat)
                txtFrom.Text = Format(ds.Tables(0).Rows(0).Item("EREG_LASTWORKINGDATE"), OASISConstants.DateFormat)
                txtRemarks_ACC.Text = ds.Tables(0).Rows(0).Item("EREG_REMARKS_ACC").ToString
                ViewState("ACCPOSTED") = ds.Tables(0).Rows(0).Item("EREG_ApprStatus_ACC").ToString
                If IsDate(ds.Tables(0).Rows(0).Item("EREG_VISACANCEL_DT")) Then txtVisaCancelDt.Text = Format(ds.Tables(0).Rows(0).Item("EREG_VISACANCEL_DT"), OASISConstants.DateFormat)
                If IsDate(ds.Tables(0).Rows(0).Item("EREG_LC_DT")) Then txtLCCancelDt.Text = Format(ds.Tables(0).Rows(0).Item("EREG_LC_DT"), OASISConstants.DateFormat)
                If IsDate(ds.Tables(0).Rows(0).Item("EREG_EXIT_DT")) Then txtExitDt.Text = Format(ds.Tables(0).Rows(0).Item("EREG_EXIT_DT"), OASISConstants.DateFormat)
                If txtVisaCancelDt.Text <> "" Then rbVisaYes.Checked = True Else rbVisaNo.Checked = True
                If txtLCCancelDt.Text <> "" Then rbLCYes.Checked = True Else rbLCNo.Checked = True
                If txtExitDt.Text <> "" Then rbExitYes.Checked = True Else rbExitNo.Checked = True

                If ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074" Then
                    Dim GRATUITYDETAIL As String
                    GRATUITYDETAIL = ds.Tables(0).Rows(0).Item("EREG_GRATUITYDETAIL")
                    If GRATUITYDETAIL = "" Then
                        FillGratuityDetails(True)
                    Else
                        Dim dtGratuity As New DataTable
                        Dim mRowArr(), mRowStr, mColArr() As String
                        mRowArr = GRATUITYDETAIL.Split("|")
                        For Each mRowStr In mRowArr
                            mColArr = mRowStr.Split("=")
                            If mColArr.Length > 0 Then
                                dtGratuity.Columns.Add(mColArr(0), System.Type.GetType("System.String"))
                            End If
                        Next
                        If dtGratuity.Columns.Count > 0 Then
                            Dim mRow As DataRow
                            mRow = dtGratuity.NewRow
                            For Each mRowStr In mRowArr
                                mColArr = mRowStr.Split("=")
                                If mColArr.Length > 1 Then
                                    mRow(mColArr(0)) = mColArr(1)
                                End If
                            Next
                            dtGratuity.Rows.Add(mRow)
                        End If
                        Try
                            ViewState("GratuityDetails") = dtGratuity
                            FormViewGratuity.DataSource = ViewState("GratuityDetails")
                            FormViewGratuity.DataBind()
                        Catch ex As Exception
                        End Try
                    End If

                    FillArrears()
                    If ds.Tables.Count > 1 Then
                        If ds.Tables(1).Rows.Count > 0 Then
                            Session("Deductions") = ds.Tables(1)
                            GridViewDeductions.DataSource = Session("Deductions")
                            GridViewDeductions.DataBind()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim vGSD_GSM_SLAB_ID As String = contextKey
        Dim drReader As SqlDataReader
        Try
            Dim str_Sql As String = "SELECT REPLACE(CONVERT(VARCHAR(11), AUD_LOG_DATE, 106), ' ', '/') +' '+STUFF(RIGHT( CONVERT(VARCHAR,AUD_LOG_DATE,100 ) ,7), 6, 0, ' ') as AUD_LOG_DATE,AUD_USR_NAME,AUD_TYPE,REPLACE(CONVERT(VARCHAR(11), AUD_PRE_DATE, 106), ' ', '/')  as AUD_PRE_DATE FROM AUDIT_UPDATE_VISA_LC_CANCEL WHERE  " & _
            " aud_regid = " & vGSD_GSM_SLAB_ID & _
            " order by AUD_LOG_DATE desc "

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='popdetails'>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=4 class='title-bg-lite'><b>Details</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            sTemp.Append("<td><b>Log Date</b></td>")
            sTemp.Append("<td><b>User Name</b></td>")
            sTemp.Append("<td><b>Type</b></td>")
            sTemp.Append("<td><b>Pre Date</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drReader("AUD_LOG_DATE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AUD_USR_NAME").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AUD_TYPE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AUD_PRE_DATE").ToString & "</td>")
                sTemp.Append("</tr>")
            End While

        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function
    Private Sub LockControls(ByVal mDisable As Boolean)
        'imgTo.Enabled = True
        If ViewState("datamode") = "add" Then
            imgEmployee.Enabled = Not mDisable
        Else
            imgEmployee.Enabled = False
        End If
        txtFrom.ReadOnly = mDisable
        txtRemarkdeduction.ReadOnly = mDisable
        txtRemarks.ReadOnly = mDisable
        btnFill.Enabled = Not mDisable
        btnUpdate.Enabled = Not mDisable
        btnChildCancel.Enabled = Not mDisable
        imgFrom.Enabled = Not mDisable

        btnSave.Visible = Not mDisable
        btnViewArrears.Enabled = Not mDisable
        btnRefreshGratuity.Enabled = Not mDisable
        rbVisaYes.Enabled = Not mDisable
        rbVisaNo.Enabled = Not mDisable
        rbLCYes.Enabled = Not mDisable
        rbLCNo.Enabled = Not mDisable
        rbExitNo.Enabled = Not mDisable
        rbExitYes.Enabled = Not mDisable

        imgVisaCancelDt.Enabled = rbVisaYes.Checked And Not mDisable
        imgLCCancelDt.Enabled = rbLCYes.Checked And Not mDisable
        imgExitDt.Enabled = rbExitYes.Checked And Not mDisable
        txtVisaCancelDt.ReadOnly = Not rbVisaYes.Checked Or mDisable
        txtLCCancelDt.ReadOnly = Not rbLCYes.Checked Or mDisable
        txtExitDt.ReadOnly = Not rbExitYes.Checked Or mDisable


        If ViewState("ACCPOSTED") And ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074" Then
            btnApprove.Visible = False
            btnSave.Visible = False
            btnEdit.Visible = False
        Else
            btnSave.Visible = Not mDisable
            btnEdit.Visible = mDisable
            If ViewState("MainMnu_code") = "H000082" Then
                btnApprove.Visible = True
            Else
                btnApprove.Visible = False
            End If
        End If
    End Sub
    Private Sub SaveData(ByVal AlsoApprove As Boolean)
        If ViewState("ACCPOSTED") = True Then
            lblError.Text = "You cannot save or edit approved records!!!"
            Exit Sub
        End If
        Dim strfDate As String = txtFrom.Text.Trim
        'Dim strtDate As String = txtTo.Text.Trim
        Dim str_err As String = "" '= DateFunctions.checkdates_canfuture(strfDate, strtDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        End If
        txtFrom.Text = strfDate
        'txtTo.Text = strtDate
        If txtRemarks.Text.Trim = "" Then
            lblError.Text = "Ramarks Cannot be empty"
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim new_erd_id As Integer = 0
            Dim bTermination As Boolean = True
            If DropDownListResignTerm.SelectedItem.Value = 4 Then
                bTermination = False
            End If
            Dim retval As String = "0"

            Dim ldblDedAmount As Decimal = 0
            Dim ldbleErnAmount As Decimal = 0
            Dim iIndex As Integer

            'loop through the data table row  for the selected rowindex item in the grid view
            If Session("Deductions").Rows.Count > 0 Then
                For iIndex = 0 To Session("Deductions").Rows.Count - 1
                    If Session("Deductions").Rows(iIndex)("bDeduction") = True Then
                        ldblDedAmount = ldblDedAmount + Session("Deductions").Rows(iIndex)("Amount")
                    Else
                        ldbleErnAmount = ldbleErnAmount + Session("Deductions").Rows(iIndex)("Amount")
                    End If
                Next
            End If

            Dim sqlstr As String
            Dim dtGratruity As New DataTable
            dtGratruity = ViewState("GratuityDetails")
            Dim mGratuityDetail As String = ""
            Dim dtcol As DataColumn
            If dtGratruity.Rows.Count > 0 Then


                If dtGratruity.Columns.Count > 0 Then

                    For Each dtcol In dtGratruity.Columns
                        If mGratuityDetail <> "" Then mGratuityDetail = mGratuityDetail & "|"
                        mGratuityDetail = mGratuityDetail & dtcol.ColumnName & "=" & dtGratruity.Rows(0).Item(dtcol)
                    Next
                End If
            End If
            'swapna changed 
            sqlstr = "delete from EMPTERMINATION_D_s where EDD_EREG_ID=" & Val(ViewState("EREG_ID")).ToString
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlstr)
            For iIndex = 0 To Session("Deductions").Rows.Count - 1
                '  "Remarks", "bDeduction" "DeductionID", "DeductionDESCR",  "Amount"
                retval = PayrollFunctions.SaveEMPTERMINATION_D_s(0, 1, Session("Deductions").Rows(iIndex)("DeductionID"), _
             Session("Deductions").Rows(iIndex)("Remarks"), Session("Deductions").Rows(iIndex)("bDeduction"), _
             Session("Deductions").Rows(iIndex)("Amount"), Val(ViewState("EREG_ID")), stTrans)
                If retval <> 0 Then
                    Exit For
                End If
            Next
            If retval <> 0 Then
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
                Exit Sub
            End If

            retval = PayrollFunctions.EmployeeTermination(Session("sBsuid"), txtFrom.Text, h_Emp_No.Value, txtRemarks.Text, "", ldblDedAmount, True, True, bTermination, new_erd_id, ldbleErnAmount, Val(ViewState("EREG_ID")), mGratuityDetail, AlsoApprove, stTrans)
            'HR tran lock changes
            'If retval <> 0 Then
            '    stTrans.Rollback()
            '    lblError.Text = getErrorMessage(retval)
            '    Exit Sub
            'End If
            'swapna commented
            'sqlstr = "delete from EMPTERMINATION_D_s where EDD_EREG_ID=" & Val(ViewState("EREG_ID")).ToString
            'SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlstr)
            'For iIndex = 0 To Session("Deductions").Rows.Count - 1
            '    '  "Remarks", "bDeduction" "DeductionID", "DeductionDESCR",  "Amount"
            '    retval = PayrollFunctions.SaveEMPTERMINATION_D_s(0, new_erd_id, Session("Deductions").Rows(iIndex)("DeductionID"), _
            ' Session("Deductions").Rows(iIndex)("Remarks"), Session("Deductions").Rows(iIndex)("bDeduction"), _
            ' Session("Deductions").Rows(iIndex)("Amount"), Val(ViewState("EREG_ID")), stTrans)
            '    If retval <> 0 Then
            '        Exit For
            '    End If
            'Next
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                h_Emp_No.Value & "--" & _
                txtFrom.Text.Trim & "- -" & txtRemarks.Text.Trim, _
                "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = getErrorMessage("0")
                Session("Deductions").Rows.clear()

                If AlsoApprove Then
                    ViewState("ACCPOSTED") = True
                End If
                LockControls(True)
                If ViewState("MainMnu_code") = "P153070" Then
                    btnAdd.Visible = False
                End If

            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            lblError.Text = "Error occured while processing the record."
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ViewState("MainMnu_code") = "P153072" Or ViewState("MainMnu_code") = "P153073" Or ViewState("MainMnu_code") = "P153074" Then
                Dim mCancelDt As String = ""
                Dim CancelType As String = ""
                If ViewState("MainMnu_code") = "P153072" Then
                    If rbVisaYes.Checked Then
                        If Not IsDate(txtVisaCancelDt.Text) Then
                            lblError.Text = "Invalid Date !!!"
                            Exit Sub
                        End If

                        mCancelDt = CDate(txtVisaCancelDt.Text).ToString("dd/MMM/yyyy")


                    Else
                        mCancelDt = ""
                    End If
                    CancelType = "VISA"
                ElseIf ViewState("MainMnu_code") = "P153073" Then
                    If rbLCYes.Checked Then
                        If Not IsDate(txtLCCancelDt.Text) Then
                            lblError.Text = "Invalid Date !!!"
                            Exit Sub
                        End If
                        mCancelDt = CDate(txtLCCancelDt.Text).ToString("dd/MMM/yyyy")
                    Else
                        mCancelDt = ""
                    End If
                    CancelType = "LC"
                ElseIf ViewState("MainMnu_code") = "P153074" Then
                    If rbExitYes.Checked Then
                        If Not IsDate(txtExitDt.Text) Then
                            lblError.Text = "Invalid Date !!!"
                            Exit Sub
                        End If
                        mCancelDt = CDate(txtExitDt.Text).ToString("dd/MMM/yyyy")
                    Else
                        mCancelDt = ""
                    End If
                    CancelType = "EXIT"
                End If

                If CDate(mCancelDt) > CDate(Now).ToString("dd/MMM/yyyy") Then
                    lblError.Text = "Invalid Date !!! Date entry should not allow future dates"
                    Exit Sub
                End If

                Dim sqlParam(4) As SqlParameter
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                sqlParam(0) = Mainclass.CreateSqlParameter("@EREG_ID", Val(ViewState("EREG_ID")), SqlDbType.Int)
                sqlParam(1) = Mainclass.CreateSqlParameter("@CANCEL_DT", mCancelDt, SqlDbType.VarChar)
                sqlParam(2) = Mainclass.CreateSqlParameter("@CANCEL_BY", Session("sUsr_name"), SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@CANCEL_TYPE", CancelType, SqlDbType.VarChar)
                sqlParam(4) = Mainclass.CreateSqlParameter("@v_ReturnMsg", "", SqlDbType.VarChar, True)

                Dim str_success As String
                str_success = Mainclass.ExecuteParamQRY(str_conn, "UPDATE_VISA_LC_CANCEL", sqlParam)
                str_success = sqlParam(4).Value
                If str_success = 0 Then
                    lblError.Text = getErrorMessage("0")
                    LockControls(True)
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            Else
                SaveData(True)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Error occured while processing the record."
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            LockControls(True)
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ViewState("ACCPOSTED") = False
        LockControls(False)
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub clear_All()
        h_Emp_No.Value = ""
        txtEmpNo.Text = ""
        txtRemarks.Text = ""
        txtFrom.Text = ""
        Clear_Deductions()
        FormViewGratuity.DataBind()
        FormViewVacation.DataBind()
        GridViewArrears.DataBind()
        Gridbind_GridViewDeductions()
        txtEmpNo.Text = ""
        lbl_Empname.Text = ""
        lbldesg.Text = ""
        lbldpt.Text = ""
        lblLastWorkDate.Text = ""
        lblResgDate.Text = ""
        txtRemarks.Text = ""
        DropDownListResignTerm.SelectedIndex = -1
        h_Emp_No.Value = ""
        lblVisaBSU.Text = ""
        lblWorkingBSU.Text = ""
        lblDOJBSU.Text = ""
        lblDOJGroup.Text = ""
        txtFrom.Text = ""
        GridViewDeductions.DataSource = Nothing
    End Sub
    Private Sub FillGratuityDetails(ByVal Refresh As Boolean)
        Dim ds As New DataSet
        Dim str_sql As String
        Dim ldblGratuity As Double
        Dim dtTermination As Date = CDate(txtFrom.Text)
        If Refresh Then
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            ds.Tables.Clear()
            Dim bTermination As Boolean = False
            If DropDownListResignTerm.SelectedValue = "5" Then
                bTermination = True
            End If

            'Below lines commented and added by vikranth 16th June 2019 for checking to call KSA related procedure or another.....

            'str_sql = "SELECT * FROM [dbo].[fn_GetGratuvityScheduleTermResignByEmployee]  ('" & Session("sBsuid") & "','" & dtTermination.ToString("dd/MMM/yyyy") & "','" & bTermination & "','" & h_Emp_No.Value & "')" _
            '& " where EMP_ID='" & h_Emp_No.Value & "'"
            Dim Param(1) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "CheckGratuityfunctionForKSA", Param)
            If ds.Tables(0).Rows.Count > 0 Then
                If Convert.ToInt32(ds.Tables(0).Rows(0)("KSAcheck").ToString()) = 1 Then
                    str_sql = "SELECT * FROM [dbo].[fn_GetGratuvityScheduleTermResignByEmployee_KSA]  ('" & Session("sBsuid") & "','" & dtTermination.ToString("dd/MMM/yyyy") & "','" & bTermination & "','" & h_Emp_No.Value & "')" _
            & " where EMP_ID='" & h_Emp_No.Value & "'"
                Else str_sql = "SELECT * FROM [dbo].[fn_GetGratuvityScheduleTermResignByEmployee]  ('" & Session("sBsuid") & "','" & dtTermination.ToString("dd/MMM/yyyy") & "','" & bTermination & "','" & h_Emp_No.Value & "')" _
            & " where EMP_ID='" & h_Emp_No.Value & "'"
                End If
            Else
                str_sql = "SELECT * FROM [dbo].[fn_GetGratuvityScheduleTermResignByEmployee]  ('" & Session("sBsuid") & "','" & dtTermination.ToString("dd/MMM/yyyy") & "','" & bTermination & "','" & h_Emp_No.Value & "')" _
            & " where EMP_ID='" & h_Emp_No.Value & "'"
            End If

            'Ended by vikranth on 16th june 2019

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If IsNumeric(ds.Tables(0).Rows(0)("ACCRUED_AMOUNT").ToString) Then
                    ldblGratuity = ds.Tables(0).Rows(0)("ACCRUED_AMOUNT")
                Else
                    ldblGratuity = 0
                End If
            End If
            ViewState("GratuityDetails") = ds.Tables(0)
            FormViewGratuity.DataSource = ViewState("GratuityDetails")
            FormViewGratuity.DataBind()
        End If

    End Sub
    Private Sub FillArrears()
        Try
            If IsDate(txtFrom.Text) Then
                'Dim dtTermination As Date = CDate(txtFrom.Text)
                Dim dtTermination As Date = CDate(lblLastWorkDate.Text)  ' as discussed with Arun
                If txtEmpNo.Text = "" Then
                    lblError.Text = "Name Cannot be empty"
                    Exit Sub
                End If

                Dim ldblLeaveSalary, ldblAirfare As Decimal
                Dim dtJoindt As DateTime
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim ds As New DataSet
                Dim str_sql As String
                ds.Tables.Clear()

                ds.Tables.Clear()
                'str_sql = "select * from [fn_GetLeaveScheduleReport]('" & Session("sBsuid") & "','" & dtTermination.ToString("dd/MMM/yyyy") & "' )" _
                '& " where EMP_ID='" & h_Emp_No.Value & "'"
                str_sql = "select * from dbo.[fn_GetLeaveSchedule_Finalsettlement]('" & Session("sBsuid") & "','" & dtTermination.ToString("dd/MMM/yyyy") & _
                "','" & h_Emp_No.Value & "')"

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

                FormViewVacation.DataSource = ds.Tables(0)
                If ds.Tables(0).Rows.Count > 0 Then
                    If IsNumeric(ds.Tables(0).Rows(0)("LEAVE_SALARY").ToString) Then
                        ldblLeaveSalary = ds.Tables(0).Rows(0)("LEAVE_SALARY")
                        ldblAirfare = ds.Tables(0).Rows(0)("TKTAMOUNT")
                        dtJoindt = ds.Tables(0).Rows(0)("EMP_JOINDT")
                    Else
                        ldblLeaveSalary = 0
                        ldblAirfare = 0
                    End If
                End If
                ' txtJoinDt.Text = Format(dtJoindt, "dd/MMM/yyyy")

                FormViewVacation.DataBind()
                FillFixedAssetTab(h_Emp_No.Value)
                FillSalaryTab(h_Emp_No.Value)
                Dim objConn As New SqlConnection(str_conn) '
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim retval As String = "1000"
                    retval = PayrollFunctions.ProcessSalaryForTermination(Session("sBsuid"), _
                    dtTermination.Month, dtTermination.Year, h_Emp_No.Value, True, ldblLeaveSalary, 0, _
                    dtTermination, -1, stTrans)
                    If retval = "0" Then
                        stTrans.Commit()
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(retval)
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try
                ds.Tables.Clear()
                str_sql = "SELECT   ESD_EMP_ID,  DBO.fn_GetMonthFirst3(ESD_MONTH)+'/'+ltrim(ESD_YEAR) AS MONTHYEAR, " _
                    & " ESD_YEAR, sum(ESD_EARN_NET) AS AMOUNT ,dbo.fn_ReturnDate(1,ESD_MONTH,ESD_YEAR) ORDERDT" _
                    & " FROM EMPSALARYDATA_D WHERE (ESD_PAID = 0)  and ESD_EMP_ID ='" & h_Emp_No.Value & "'" _
                    & " and convert(datetime,(convert(varchar(10),ESD_MONTH) + '-01-' + convert(varchar(10),ESD_YEAR))) < [dbo].[fN_GetFirstDayOFmonth] ('" & CDate(lblLastWorkDate.Text) & "')" _
                    & " GROUP BY ESD_EMP_ID,  DBO.fn_GetMonthFirst3(ESD_MONTH)+'/'+ltrim(ESD_YEAR), " _
                    & " ESD_YEAR, ESD_EARN_NET ,dbo.fn_ReturnDate(1,ESD_MONTH,ESD_YEAR)" _
                    & " order by  ESD_EMP_ID,dbo.fn_ReturnDate(1,ESD_MONTH,ESD_YEAR)"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

                GridViewArrears.DataSource = ds.Tables(0)
                GridViewArrears.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnViewArrears_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewArrears.Click
        FillArrears()
    End Sub

    Private Sub FillFixedAssetTab(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_sql As String = "SELECT EMPASSETS_D.EAD_ISSUE_DT, EMPASSETS_D.EAD_VALUE, EMPASSETS_D.EAD_REMARKS, " & _
        "CASE WHEN EMPASSETS_D.EAD_STATUS=0 THEN 'Issue' WHEN EMPASSETS_D.EAD_STATUS=1 then 'Return' ELSE 'Lost' END AS  EAD_STATUS" & _
        ", EMPASSETS_D.EAD_bACTIVE, OASISFIN.dbo.FIXEDASSET_M.FAS_DESCRIPTION AS ASSETNAME, EMPASSETS_D.EAD_EMP_ID" & _
        " FROM OASISFIN.dbo.FIXEDASSET_M RIGHT OUTER JOIN EMPASSETS_D ON OASISFIN.dbo.FIXEDASSET_M.FAS_CODE = EMPASSETS_D.EAD_FAS_ID " & _
        "WHERE EMPASSETS_D.EAD_RET_DT Is NULL AND EAD_EMP_ID =" & empID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        gvAssetAllocated.DataSource = ds
        gvAssetAllocated.DataBind()
    End Sub

    Function CreateDataTableDeductions() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try '@EDD_ERN_ID ,@EDD_DESCRIPTION,@EDD_BDEDUCTION,@EDD_AMOUNT 
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cRemarks As New DataColumn("Remarks", System.Type.GetType("System.String"))
            Dim cbDeduction As New DataColumn("bDeduction", System.Type.GetType("System.Boolean"))
            Dim cDeductionID As New DataColumn("DeductionID", System.Type.GetType("System.String"))
            Dim cDeductionDESCR As New DataColumn("DeductionDESCR", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cRemarks)
            dtDt.Columns.Add(cbDeduction)

            dtDt.Columns.Add(cDeductionID)
            dtDt.Columns.Add(cDeductionDESCR)
            dtDt.Columns.Add(cAmount)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Protected Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        '"Id", "Remarks", "bDeduction" "DeductionID", "DeductionDESCR",  "Amount"
        Try

            Dim dr As DataRow
            dr = Session("Deductions").NewRow
            dr("Id") = ViewState("iID")
            ViewState("iID") = ViewState("iID") + 1
            dr("Remarks") = txtRemarkdeduction.Text
            dr("bDeduction") = chkDeduction.Checked
            If chkDeduction.Checked Then
                dr("DeductionID") = ddSalDedCode.SelectedItem.Value
                dr("DeductionDESCR") = ddSalDedCode.SelectedItem.Text
                ViewState("ChkDeduction") = True
            Else
                dr("DeductionID") = ddSalEarnCode.SelectedItem.Value
                dr("DeductionDESCR") = ddSalEarnCode.SelectedItem.Text
                ViewState("ChkDeduction") = False
            End If


            If IsNumeric(txtAmount.Text) Then
                dr("Amount") = txtAmount.Text
            Else
                lblError.Text = "Please Check Amount"
                Exit Sub
            End If
            Session("Deductions").Rows.Add(dr)
            Gridbind_GridViewDeductions()
            Clear_Deductions()
            chkDeduction.Checked = ViewState("ChkDeduction")
            bindDeductionCombo()
        Catch ex As Exception
            lblError.Text = "Check Date"
        End Try
    End Sub

    Sub Clear_Deductions()
        txtRemarkdeduction.Text = ""
        chkDeduction.Checked = True
        txtAmount.Text = ""
    End Sub

    Protected Sub GridViewDeductions_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewDeductions.RowDeleting
        Try
            Dim row As GridViewRow = GridViewDeductions.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("Deductions").Rows.Count - 1
                If iIndex = Session("Deductions").Rows(iRemove)(0) Then
                    Session("Deductions").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            Gridbind_GridViewDeductions()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Record cannot be Deleted"
        End Try
    End Sub

    Protected Sub GridViewDeductions_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridViewDeductions.RowEditing
        btnSave.Enabled = False
        GridViewDeductions.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = GridViewDeductions.Rows(e.NewEditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("Deductions").Rows.Count - 1
            '"Id", "Remarks", "bDeduction" "DeductionID", "DeductionDESCR",  "Amount"
            If iIndex = Session("Deductions").Rows(iEdit)(0) Then
                txtAmount.Text = Session("Deductions").Rows(iEdit)("Amount")
                If chkDeduction.Checked Then
                    ddSalDedCode.SelectedIndex = -1
                    If Not ddSalDedCode.Items.FindByValue(Session("Deductions").Rows(iEdit)("DeductionID")) Is Nothing Then
                        ddSalDedCode.Items.FindByValue(Session("Deductions").Rows(iEdit)("DeductionID")).Selected = True
                    End If
                    ' hfTypeID.Value = Session("Deductions").Rows(iEdit)("DeductionID")
                Else
                    ddSalEarnCode.SelectedIndex = -1
                    If Not ddSalEarnCode.Items.FindByValue(Session("Deductions").Rows(iEdit)("DeductionID")) Is Nothing Then
                        ddSalEarnCode.Items.FindByValue(Session("Deductions").Rows(iEdit)("DeductionID")).Selected = True
                    End If
                    'txtType.Text = Session("Deductions").Rows(iEdit)("DeductionDESCR")
                    'hfTypeID.Value = Session("Deductions").Rows(iEdit)("DeductionID")
                End If

                txtRemarkdeduction.Text = Session("Deductions").Rows(iEdit)("Remarks")
                chkDeduction.Checked = Session("Deductions").Rows(iEdit)("bDeduction")
                Exit For
            End If
        Next
        btnFill.Visible = False
        btnUpdate.Visible = True
        btnChildCancel.Visible = True
        GridViewDeductions.Columns(5).Visible = False
        GridViewDeductions.Columns(6).Visible = False
        Gridbind_GridViewDeductions()
    End Sub

    Sub Gridbind_GridViewDeductions()
        Dim decEarning As Decimal = 0
        Dim decDeduction As Decimal = 0

        For i As Integer = 0 To Session("Deductions").Rows.Count - 1
            '"Id", "Remarks", "bDeduction" "DeductionID", "DeductionDESCR",  "Amount"
            If Session("Deductions").Rows(i)("bDeduction") = True Then
                decDeduction = decDeduction + Session("Deductions").Rows(i)("Amount")
            Else
                decEarning = decEarning + Session("Deductions").Rows(i)("Amount")
            End If
            txtDeductions.Text = decDeduction
            txtEarning.Text = decEarning
            txtTotal.Text = decEarning - decDeduction
        Next

        GridViewDeductions.DataSource = Session("Deductions")
        GridViewDeductions.DataBind()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        btnSave.Enabled = True
        Dim row As GridViewRow = GridViewDeductions.Rows(GridViewDeductions.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("Deductions").Rows.Count - 1
            '"Id", "Remarks", "bDeduction" "DeductionID", "DeductionDESCR",  "Amount"
            If iIndex = Session("Deductions").Rows(iEdit)(0) Then
                Session("Deductions").Rows(iEdit)("Amount") = txtAmount.Text
                If chkDeduction.Checked Then
                    Session("Deductions").Rows(iEdit)("DeductionDESCR") = ddSalDedCode.SelectedItem.Text
                    Session("Deductions").Rows(iEdit)("DeductionID") = ddSalDedCode.SelectedItem.Value
                Else
                    Session("Deductions").Rows(iEdit)("DeductionDESCR") = ddSalEarnCode.SelectedItem.Text
                    Session("Deductions").Rows(iEdit)("DeductionID") = ddSalEarnCode.SelectedItem.Value
                End If


                Session("Deductions").Rows(iEdit)("Remarks") = txtRemarkdeduction.Text
                Session("Deductions").Rows(iEdit)("bDeduction") = chkDeduction.Checked
                Exit For
            End If
        Next
        btnFill.Visible = True
        btnUpdate.Visible = False
        btnChildCancel.Visible = False
        GridViewDeductions.Columns(5).Visible = True
        GridViewDeductions.SelectedIndex = -1
        GridViewDeductions.Columns(6).Visible = True
        Gridbind_GridViewDeductions()
    End Sub

    Protected Sub btnChildCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChildCancel.Click
        btnFill.Visible = True
        btnUpdate.Visible = False
        btnChildCancel.Visible = False
        btnSave.Enabled = True
        GridViewDeductions.Columns(5).Visible = True
        GridViewDeductions.SelectedIndex = -1
        GridViewDeductions.Columns(6).Visible = True
        Gridbind_GridViewDeductions()
    End Sub

    Private Sub FillSalaryTab(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal = 0
        str_Sql = "SELECT   EMPSALCOMPO_M.ERN_DESCR, EMPSALARYHS_s.ESH_AMOUNT FROM EMPSALARYHS_s LEFT OUTER JOIN EMPSALCOMPO_M" & _
        " ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID where ESH_EMP_ID = " & empID & " and ESH_TODT is null  and EMPSALARYHS_s.ESH_bsu_id='" & Session("sBsuid") & "' ORDER BY ERN_ORDER "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim datarow As DataRow
        datarow = ds.Tables(0).NewRow
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            grossSal = grossSal + ds.Tables(0).Rows(i)(1)
        Next
        'Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        datarow(0) = "Gross Salary"
        datarow(1) = grossSal
        ds.Tables(0).Rows.Add(datarow)
        gvEmpSalary.DataSource = ds.Tables(0)
        gvEmpSalary.DataBind()
        gvEmpSalary.Rows(gvEmpSalary.Rows.Count - 1).BackColor = Drawing.Color.LightPink
    End Sub

    Private Sub BindENR()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 1 order by ERN_ORDER "
        Dim str_Sql As String = "exec GetEarnTypesBSUWise '" & Session("sBSUID") & "',1"   'V1.1

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataBind()
        dr.Close()
        'str_Sql = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 0 order by ERN_ORDER "
        str_Sql = "exec GetEarnTypesBSUWise '" & Session("sBSUID") & "',0"    'V1.1
        dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalDedCode.DataSource = dr
        ddSalDedCode.DataTextField = "DESCR"
        ddSalDedCode.DataValueField = "ERN_ID"
        ddSalDedCode.DataBind()
        dr.Close()
    End Sub

    Public Sub bindDeductionCombo()
        If chkDeduction.Checked Then
            ddSalDedCode.Visible = True
            ddSalEarnCode.Visible = False
        Else
            ddSalDedCode.Visible = False
            ddSalEarnCode.Visible = True
        End If
    End Sub

    Protected Sub chkDeduction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDeduction.CheckedChanged
        bindDeductionCombo()
    End Sub
    Protected Sub txtEmpNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNo.TextChanged
        bindEmployeeDetails()
    End Sub
    Sub bindEmployeeDetails()
        Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
        Using EmpReader As SqlDataReader = SqlHelper.ExecuteReader(sqlcon, CommandType.Text, "SELECT O.*,D.DPT_DESCR DPT_NAME,S.DES_DESCR DES_NAME  FROM vw_OSO_EMPLOYEEMASTERDETAILS O,DEPARTMENT_M D ,EMPDESIGNATION_M S where  O.EMP_DPT_ID=D.DPT_ID AND O.EMP_DES_ID=S.DES_ID AND EMPNO='" & txtEmpNo.Text & "'")

            If EmpReader.HasRows = True Then
                While EmpReader.Read

                    h_Emp_No.Value = Convert.ToString(EmpReader("EMP_ID"))
                    lbl_Empname.Text = Convert.ToString(EmpReader("EMPNAME"))
                    lbldpt.Text = Convert.ToString(EmpReader("DPT_NAME"))
                    lbldesg.Text = Convert.ToString(EmpReader("DES_NAME"))
                    lblVisaBSU.Text = Convert.ToString(EmpReader("VISA_BSU_NAME"))
                    lblWorkingBSU.Text = Convert.ToString(EmpReader("WORK_BSU_NAME"))
                    lblDOJBSU.Text = Format(EmpReader("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                    lblDOJGroup.Text = Format(EmpReader("EMP_JOINDT"), OASISConstants.DateFormat)
                    Dim dt As New DataTable
                    Dim sqlStr As String
                    sqlStr = "SELECT EREG_LASTWORKINGDATE,EREG_REGDATE FROM EMPRESIGNATION_D  WHERE EREG_EMP_ID='" & Convert.ToString(EmpReader("EMP_ID")) & "' AND EREG_BSU_ID='" & Session("sBsuid") & "'"
                    dt = Mainclass.getDataTable(sqlStr, sqlcon)
                    If dt.Rows.Count > 0 Then
                        lblLastWorkDate.Text = Format(dt.Rows(0).Item("EREG_LASTWORKINGDATE"), OASISConstants.DateFormat)
                        lblResgDate.Text = Format(dt.Rows(0).Item("EREG_REGDATE"), OASISConstants.DateFormat)
                        txtFrom.Text = Format(dt.Rows(0).Item("EREG_LASTWORKINGDATE"), OASISConstants.DateFormat)
                    End If
                End While
            Else
                h_Emp_No.Value = ""
                lbl_Empname.Text = ""
                lbldpt.Text = ""
                lbldesg.Text = ""
            End If
        End Using
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If ViewState("ACCPOSTED") = True And ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074" Then
            lblError.Text = "You cannot edit approved records"
            Exit Sub
        End If
        LockControls(False)
        btnSave.Visible = True
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        SaveData(True)
    End Sub

    Protected Sub btnRefreshGratuity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshGratuity.Click
        FillGratuityDetails(True)
    End Sub

    Protected Sub RdVisaStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbVisaYes.CheckedChanged
        If rbVisaYes.Checked Then
            txtVisaCancelDt.Enabled = True
            imgVisaCancelDt.Enabled = True
        Else
            txtVisaCancelDt.Enabled = False
            imgVisaCancelDt.Enabled = False
        End If
    End Sub
    Protected Sub RdLCStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLCYes.CheckedChanged
        If rbLCYes.Checked Then
            txtLCCancelDt.Enabled = True
            imgLCCancelDt.Enabled = True
        Else
            txtLCCancelDt.Enabled = False
            imgLCCancelDt.Enabled = False
        End If
    End Sub
    Protected Sub RdExitStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbExitYes.CheckedChanged
        If rbExitYes.Checked Then
            txtExitDt.Enabled = True
            imgExitDt.Enabled = True
        Else
            txtExitDt.Enabled = False
            imgExitDt.Enabled = False
        End If
    End Sub

    Protected Sub rbVisaNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbVisaNo.CheckedChanged
        If rbVisaNo.Checked Then
            txtVisaCancelDt.Enabled = False
            imgVisaCancelDt.Enabled = False
        Else
            txtVisaCancelDt.Enabled = True
            imgVisaCancelDt.Enabled = True
        End If
    End Sub

    Protected Sub rbLCNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbLCNo.CheckedChanged
        If rbLCNo.Checked Then
            txtLCCancelDt.Enabled = False
            imgLCCancelDt.Enabled = False
        Else
            txtLCCancelDt.Enabled = True
            imgLCCancelDt.Enabled = True
        End If
    End Sub

    Protected Sub rbExitNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbExitNo.CheckedChanged
        If rbExitNo.Checked Then
            txtExitDt.Enabled = False
            imgExitDt.Enabled = False
        Else
            txtExitDt.Enabled = True
            imgExitDt.Enabled = True
        End If
    End Sub


End Class
