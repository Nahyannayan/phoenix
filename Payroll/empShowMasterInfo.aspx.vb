﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Payroll_empShowMasterInfo
    Inherits System.Web.UI.Page
    Dim SearchMode As String
    'Version            Author          Date            Purpose
    ' 1.1               Swapna          3/Jan/2011      To add WPS agent search
    '1.2                Swapna          29/Mar/2012     To add cost centre search

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try

                If h_SelectedId.Value <> "Close" Then
                    Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
                    'Response.Write(" alert('uuu');")
                    Response.Write("} </script>" & vbCrLf)
                End If


                SearchMode = Request.QueryString("id")
                If SearchMode = "BK" Then
                    Page.Title = "Bank Info"
                ElseIf SearchMode = "S" Then
                    Page.Title = "Employee Status Info"
                ElseIf SearchMode = "C" Or SearchMode = "DC" Or SearchMode = "CC" Or SearchMode = "PC" Then
                    Page.Title = "Country Info"
                ElseIf SearchMode = "D" Then
                    Page.Title = "Department Info"
                ElseIf SearchMode = "T" Then
                    Page.Title = "Teacher Grade Info"
                ElseIf SearchMode = "G" Then
                    Page.Title = "Grade Info"
                ElseIf SearchMode = "CT" Then
                    Page.Title = "Category Info"
                ElseIf SearchMode = "ML" Then
                    Page.Title = "Minister of Labour Info"
                ElseIf SearchMode = "ME" Then
                    Page.Title = "Minister of Education Info"
                ElseIf SearchMode = "SD" Then
                    Page.Title = "School Designation Info"
                ElseIf SearchMode = "IU" Then
                    Page.Title = "Visa Issued Unit Info"
                ElseIf SearchMode = "WU" Then
                    Page.Title = "Work Unit Info"
                ElseIf SearchMode = "AP" Then
                    Page.Title = "Approval Policy"
                ElseIf SearchMode = "APP" Then
                    Page.Title = "Applicant Details"
                ElseIf SearchMode = "WPS" Then                  'V1.1
                    Page.Title = "WPS Agent Details"
                ElseIf SearchMode = "COC" Then                  'V1.2
                    Page.Title = "Cost Center Info"
                ElseIf SearchMode = "JOB" Then                  'V1.2
                    Page.Title = "Job Master Info"

                End If
                Page.DataBind()
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
    End Sub


    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            SearchMode = Request.QueryString("id")
            Dim str_filter_code As String
            Dim str_filter_name As String
            If SearchMode = "BK" Then
                str_Sql = "Select ID,E_Name from(SELECT BNK_ID as ID,BNK_DESCRIPTION as E_Name  FROM BANK_M inner join businessunit_m on isnull([BANK_CTY_ID],BSU_COUNTRY_ID) =BSU_COUNTRY_ID where bsu_id= '" & Session("sBsuid") & "' )a where a.id<>'' " 'swapna added
            ElseIf SearchMode = "S" Then
                str_Sql = "Select ID, E_Name from(SELECT EST_ID as ID,EST_DESCR as E_Name  FROM EMPSTATUS_M)a where a.ID<>'' "
            ElseIf SearchMode = "C" Or SearchMode = "DC" Then
                str_Sql = "Select ID, E_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as E_Name FROM COUNTRY_M)a where a.ID<>'' and E_Name <> '-' and a.ID not in (0,5,252) "
            ElseIf SearchMode = "CC" Or SearchMode = "PC" Then
                str_Sql = "Select ID, E_Name from(SELECT  CTY_ID as ID,CTY_DESCR as E_Name FROM COUNTRY_M)a where a.ID<>''"
            ElseIf SearchMode = "D" Then
                str_Sql = "Select ID, E_Name from(SELECT DPT_ID as ID,DPT_DESCR as E_Name  FROM DEPARTMENT_M)a where a.ID<>'' "
            ElseIf SearchMode = "T" Then
                str_Sql = "Select ID, E_Name from(SELECT TGD_ID as ID,TGD_DESCR as E_Name  FROM EMPTEACHINGGRADES_M)a where a.ID<>''"
            ElseIf SearchMode = "G" Then
                str_Sql = "Select ID, E_Name from(SELECT EGD_ID as ID,EGD_DESCR as E_Name  FROM EMPGRADES_M)a where a.ID<>'' "
            ElseIf SearchMode = "CT" Then
                str_Sql = "Select ID, E_Name from(SELECT ECT_ID as ID,ECT_DESCR as E_Name FROM EMPCATEGORY_M)a where a.ID<>''"
            ElseIf SearchMode = "ML" Then
                str_Sql = "Select ID, E_Name from(SELECT DES_ID as ID,DES_DESCR as E_Name,DES_FLAG as dF FROM EMPDESIGNATION_M)a where a.df='ML' and a.ID<>''"
            ElseIf SearchMode = "ME" Then
                str_Sql = "Select ID, E_Name from(SELECT DES_ID as ID,DES_DESCR as E_Name,DES_FLAG as dF FROM EMPDESIGNATION_M)a where a.df='ME' and a.ID<>''"
            ElseIf SearchMode = "SD" Then
                str_Sql = "Select ID, E_Name from(SELECT DES_ID as ID,DES_DESCR as E_Name,DES_FLAG as dF FROM EMPDESIGNATION_M)a where a.df='SD' and a.ID<>''"
            ElseIf SearchMode = "EP" Then
                str_Sql = "Select ID,E_Name from(Select emp_id as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name, EMP_bACTIVE from Employee_M)a where a.id<>''AND EMP_bACTIVE =1 "
            ElseIf SearchMode = "EPBD" Then
                Dim BSUID, DESID As String
                BSUID = Request.QueryString("BSUID")
                DESID = Request.QueryString("DESID")
                str_Sql = "Select ID,E_Name from(Select emp_id as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name, EMP_bACTIVE from Employee_M  where (EMP_BSU_ID='" & BSUID & "' or '" & BSUID & "'='') AND (EMP_DES_ID='" & DESID & "' or '" & DESID & "'='') )a where a.id<>''AND EMP_bACTIVE =1 "
            ElseIf SearchMode = "REFEP" Then
                Dim STUD_ID As String = Request.QueryString("STU_ID")
                str_Sql = "Select ID,E_Name from(SELECT EMPLOYEE_M.EMP_ID AS ID, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name" & _
                " FROM STUDENT_D INNER JOIN EMPLOYEE_M ON STUDENT_D.STS_EMP_ID = EMPLOYEE_M.EMP_ID " & _
                " where STS_bFGEMSEMP = 1 AND EMP_bACTIVE = 1 AND STUDENT_D.STS_STU_ID =" & STUD_ID & _
                " )a where a.id<>'' "
            ElseIf SearchMode = "IU" Then
                str_Sql = "Select ID, E_Name from(Select BSU_ID as ID,BSU_NAME as E_Name from businessunit_m)a where a.ID<>'' "
            ElseIf SearchMode = "WU" Then
                str_Sql = "Select ID, E_Name from(Select BSU_ID as ID,BSU_NAME as E_Name from businessunit_m)a where a.ID<>'' "
                ''added bynahyan on4feb for dependantadd in ESS
            ElseIf SearchMode = "DEPENDADD" Then
                ''   str_Sql = "SELECT ID,A.E_Name FROM (SELECT BSU_ID as ID,BSU_NAME as E_Name FROM OASIS_FIM.FIM.BSU_M UNION SELECT BSU_ID as ID,BSU_NAME as E_Name  FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN ('999998'    ,'900007'    ,'800075'    ,'900024'    ,'900020'    ,'900001'    ,'800030'    ,'900010'    ,'888889'    ))A where a.ID<>'' "
                str_Sql = "SELECT ID,A.E_Name FROM (SELECT BSU_ID as ID,BSU_NAME as E_Name FROM [OASIS].[dbo].[vw_CORP_REP_BSU] UNION SELECT BSU_ID as ID,BSU_NAME as E_Name  FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN ('999998'    ,'900007'    ,'800075'    ,'900024'    ,'900020'    ,'900001'    ,'800030'    ,'900010'    ,'888889'    ))A where a.ID<>''"
            ElseIf SearchMode = "AP" Then
                str_Sql = "Select ID, E_Name from(SELECT LPS_ID as ID, LPS_DESCRIPTION as E_Name FROM APPROVALPOLICY_H WHERE LPS_bActive = 1 AND LPS_DOCTYPE = 'LEAVE' AND LPS_BSU_ID = '" & Session("sBSUID") & "')a where a.ID<>''"
            ElseIf SearchMode = "APP" Then
                str_Sql = "Select ID, E_Name from(SELECT  APPLICATION_NO AS ID ,ISNULL(FIRST_NAME,'') + ' '+ ISNULL(MIDDLE_NAME,'')+ ' '+ ISNULL(SUR_NAME,'') AS E_Name " & _
                " FROM OASIS_HR . dbo . APPLICATION_MASTER WHERE CURRENT_STAGE=410 AND ACTIVE='True') a where a.ID<>''"
            ElseIf SearchMode = "WPS" Then   'V1.1
                str_Sql = "SELECT ID,E_Name FROM(SELECT  WPA_ID as ID,WPA_DESCR  as E_Name FROM WPS_AGENT_M)a where a.ID<>'' "

            ElseIf SearchMode = "COC" Then  'V1.2
                str_Sql = "SELECT ID,E_Name,[CCT_ORDER] FROM(SELECT  [CCT_ID] ID  ,[CCT_DESCR] E_Name,[CCT_ORDER] FROM [OASISFIN].[dbo].[COSTCENTER_M]" _
            & " where isnull([CCT_BSU_ID] ,'" & Session("sBsuid") & "') like '%" & Session("sBsuid") & "%' and [CCT_BActive]=1" _
            & " )a where a.ID<>'' "
            ElseIf SearchMode = "JOB" Then
                str_Sql = "SELECT ID,E_Name FROM(SELECT  JM_ID as ID,JM_NAME  as E_Name FROM JOB_MASTER)a where a.ID<>'' "
            End If

            Dim ds As New DataSet
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtName As String
            Dim str_search As String
            str_txtCode = ""
            str_txtName = ""
            str_filter_code = ""
            str_filter_name = ""
            If gvEmpInfo.Rows.Count > 0 Then
                Dim str_Sid_search() As String

                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                str_txtCode = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If
            If SearchMode = "COC" Then  ' V1.2
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name & "order by a.CCT_ORDER")
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name & " order by a.E_Name")

            gvEmpInfo.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpInfo.DataBind()
                Dim columnCount As Integer = gvEmpInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvEmpInfo.Rows(0).Cells.Clear()
                gvEmpInfo.Rows(0).Cells.Add(New TableCell)
                gvEmpInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvEmpInfo.DataBind()
            End If

            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New LinkButton
        Dim lbClose As New LinkButton
        lbClose = sender
        lblcode = sender.Parent.FindControl("LinkButton2")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        If (Not lblcode Is Nothing) Then
            ''   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"
            h_SelectedId.Value = l_Str_Msg
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.Students = document.getElementById('h_SelectedId').value;")
            Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value);")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            'Response.Write("<script language='javascript'> function listen_window(){")
            ''Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"


        End If
    End Sub


    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpInfo.PageIndexChanging
        gvEmpInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New LinkButton
        Dim lbClose As New LinkButton
        lbClose = sender.Parent.FindControl("LinkButton1")
        lblcode = sender
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            h_SelectedId.Value = l_Str_Msg
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.Students = document.getElementById('h_SelectedId').value;")
            Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value);")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
        End If
    End Sub


End Class
