Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports PayrollFunctions

Partial Class Payroll_empMasterDetail_Documents
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    Public Shared Function GetDate(ByVal vDate As String) As String
        If vDate = "" OrElse CDate(vDate) = New Date(1900, 1, 1) Then
            Return "-"
        Else
            Return Format(CDate(vDate), OASISConstants.DateFormat)
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Dim CurBsUnit As String = Session("sBsuid")
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ClearAllSession()
            'ViewState("datamode") = "add"
            txtML.Attributes.Add("ReadOnly", "ReadOnly")
            txtME.Attributes.Add("ReadOnly", "ReadOnly")

            txtWU.Attributes.Add("ReadOnly", "ReadOnly")
            txtIU.Attributes.Add("ReadOnly", "ReadOnly")
            txtDocDocument.Attributes.Add("ReadOnly", "ReadOnly")
            'Contact Details
            chkDocActive.Checked = True
            txtIU.Text = Session("BSU_Name")
            hfIU.Value = Session("sBsuid")
            txtWU.Text = Session("BSU_Name")
            hfWU.Value = Session("sBsuid")

            txtEmpno1.Attributes.Add("ReadOnly", "ReadOnly")
            txtFname1.Attributes.Add("ReadOnly", "ReadOnly")
            txtMname1.Attributes.Add("ReadOnly", "ReadOnly")
            txtLname1.Attributes.Add("ReadOnly", "ReadOnly")

            pnlMain.Enabled = False
            Page.Title = OASISConstants.Gemstitle
            Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnitsByCurrency(CurBsUnit)
                If BUnitreaderSuper.HasRows = True Then
                    While BUnitreaderSuper.Read
                        BCur_ID = Convert.ToString(BUnitreaderSuper("BSU_CURRENCY"))
                    End While
                End If
            End Using
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), CurBsUnit, ViewState("MainMnu_code"))
        End If

        If (Request.QueryString("datamode") Is Nothing) Or (Request.QueryString("MainMnu_code") Is Nothing) Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
        Else
            'get the menucode to confirm the user is accessing the valid page
            'if query string returns Eid  if datamode is view state

            'Setting tab rights for user

            Dim Bsu_ID As String = Session("sBsuid")
            Dim MNu_code As String = ViewState("MainMnu_code")
            Dim RoleID As String = String.Empty
            Using reader_Rol_ID As SqlDataReader = AccessRoleUser.GetRoleID_user(Session("sUsr_name"))
                If reader_Rol_ID.HasRows = True Then
                    While reader_Rol_ID.Read()
                        RoleID = Convert.ToString(reader_Rol_ID("USR_ROL_ID"))
                    End While
                End If
            End Using
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            If ViewState("datamode") = "view" Then
                Try
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    FillEmployeeMasterValues(ViewState("viewid"))
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)
                    lblError.Text = "Some Error occured while retrieving the data ... " & ex.Message
                    btnSave.Visible = False
                    Exit Sub
                Finally
                End Try
            End If
        End If
    End Sub

    Private Sub ClearAllSession()
        Session("EMPDEPENDANCEDETAILS") = Nothing
        Session("EMPQUALDETAILS") = Nothing
        Session("EMPSALDETAILS") = Nothing
        Session("EMPDOCDETAILS") = Nothing
        Session("EMPQUALDETAILS") = Nothing
        Session("EMPEXPDETAILS") = Nothing
        Session("EMPSALGrossSalary") = Nothing
        Session("EMPSALSCHEDULE") = Nothing
    End Sub


#Region "Clear Details"

    Private Sub clearme()
        Try
            txtPassportname.Text = ""
            txtEmpno1.Text = ""
            txtMname1.Text = ""
            txtLname1.Text = ""
            txtFname1.Text = ""
            hfWU.Value = ""
            txtWU.Text = ""

            ddlVtype.SelectedIndex = 0
            ddlVstatus.SelectedIndex = 0

            txtME.Text = ""
            hfME_ID.Value = ""

            hfML_ID.Value = ""
            txtIU.Text = ""
            txtML.Text = ""
            hfIU.Value = ""
            txtVisaPrgID.Text = ""
            ClearEMPDocDetails()
            ClearSession_GridDetails()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
#End Region

#Region "Fill Tabs"

    Private Sub FillEmployeeMasterValues(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select * from vw_OSO_EMPLOYEEMASTER where EMP_ID = " & empID
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)



        ddlVtype.DataBind()
        While dr.Read()
            txtPassportname.Text = dr("EMP_PASSPORTNAME").ToString
            txtEmpno1.Text = dr("EMPNO").ToString

            txtFname1.Text = dr("EMP_FNAME").ToString
            txtMname1.Text = dr("EMP_MNAME").ToString
            txtLname1.Text = dr("EMP_LNAME").ToString


            hfWU.Value = dr("EMP_BSU_ID").ToString
            txtWU.Text = dr("WORK_BSU_NAME").ToString


            DoNullCheck(ddlEmpContractType, dr("EMP_CONTRACTTYPE"))
            'ddlVstatus.SelectedValue = dr("EMP_VISASTATUS").ToString
            DoNullCheck(ddlVstatus, dr("EMP_VISASTATUS"))
            callVisa_Bind()
            DoNullCheck(ddlVtype, dr("EMP_VISATYPE"))
            'DoNullCheck(ddlEmpSalute, dr("EMP_SALUTE"))
            ddlEmpSalute.Text = dr("EMP_SALUTE")

            txtME.Text = dr("EMPMOE_DESCR").ToString
            hfME_ID.Value = dr("EMP_MOE_DES_ID").ToString
            hfML_ID.Value = dr("EMP_VISA_DES_ID").ToString  'Visa DestinationID
            txtIU.Text = dr("VISA_BSU_NAME").ToString()
            txtML.Text = dr("EMPVISA_DESCR").ToString()
            hfIU.Value = dr("EMP_VISA_BSU_ID").ToString
            txtVisaPrgID.Text = dr("EMP_VISA_ID").ToString
            txtPersonID.Text = dr("EMP_PERSON_ID").ToString
            DoNullCheck(ddlCategory, dr("EMP_ABC"))
            '' added by nahyan to check if abc is changed while update
            hdnABC.Value = Convert.ToString(dr("EMP_ABC"))
            hf_InsuID.Value = IIf(TypeOf (dr("EMP_INSU_ID")) Is DBNull, 0, dr("EMP_INSU_ID"))
            hfAttBy.Value = IIf(TypeOf (dr("EMP_ATT_Approv_EMP_ID")) Is DBNull, 0, dr("EMP_ATT_Approv_EMP_ID"))
            hf_KnownAs.Value = IIf(TypeOf (dr("EMP_DISPLAYNAME")) Is DBNull, "", dr("EMP_DISPLAYNAME"))

            ''added bynahyan on 16june2016 TASK id 98843

            txtUIDNo.Text = IIf(TypeOf (dr("EMP_UIDNO")) Is DBNull, "", dr("EMP_UIDNO"))

        End While

        dr.Close()
        '' to display fromdate of abc category added on 24may2016
        Dim str_Sql2 As String = "SELECT top 1 REPLACE(CONVERT(VARCHAR(11), EST_DTFROM, 106), ' ', '/') EST_DTFROM,EST_CODE FROM OASIS.dbo.EMPTRANTYPE_TRN  WHERE  EST_TTP_ID=18 and EST_EMP_ID=" & empID & " order by EST_ID DESC"
        Dim dr2 As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql2)

        While dr2.Read()
            txtFromAbc.Text = dr2("EST_DTFROM").ToString
        End While
        dr2.Close()
        FillDocumentTab(empID)
    End Sub

    Private Sub DoNullCheck(ByVal ctrl As WebControl, ByVal input As Object)
        If TypeOf (ctrl) Is DropDownList Then
            Dim ddlist As DropDownList = ctrl
            If (Not input Is Nothing) AndAlso (Not input Is DBNull.Value) Then
                If ddlist.Items.FindByValue(input) Is Nothing Then
                    ddlist.SelectedIndex = 0
                Else
                    ddlist.SelectedIndex = -1
                    ddlist.Items.FindByValue(input).Selected = True
                    'ddlist.SelectedValue = input
                End If
            Else
                ddlist.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub FillDocumentTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateDocDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "SELECT ISNULL(EMPDOCUMENTS_S.EMD_ESD_ID,'') EMD_ESD_ID, ISNULL(EMPDOCUMENTS_S.EMD_ID,'') EMD_ID," & _
        " ISNULL(EMPDOCUMENTS_S.EMD_NO,'') EMD_NO, ISNULL(EMPDOCUMENTS_S.EMD_bACTIVE,0) EMD_bACTIVE," & _
        " ISNULL(EMPDOCUMENTS_S.EMP_ISSUEPLACE,'') EMP_ISSUEPLACE, ISNULL(EMPDOCUMENTS_S.EMD_EXPDT, '') EMD_EXPDT " & _
        ", ISNULL(EMPDOCUMENTS_S.EMD_ISSUEDT,'') EMD_ISSUEDT , ISNULL(EMPSTATDOCUMENTS_M.ESD_DESCR,'') ESD_DESCR " & _
        "FROM EMPDOCUMENTS_S LEFT OUTER JOIN EMPSTATDOCUMENTS_M ON EMPDOCUMENTS_S.EMD_ESD_ID = EMPSTATDOCUMENTS_M.ESD_ID "
        str_Sql += " where EMD_EMP_ID = " & empID
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EMD_ID")
            ldrTempNew.Item("Document") = dr("ESD_DESCR")
            ldrTempNew.Item("DocID") = dr("EMD_ESD_ID")
            ldrTempNew.Item("Doc_No") = dr("EMD_NO")
            ldrTempNew.Item("Doc_bActive") = dr("EMD_bACTIVE")
            ldrTempNew.Item("Doc_IssuePlace") = dr("EMP_ISSUEPLACE")
            If dr("EMD_ISSUEDT").ToString <> "" OrElse dr("EMD_ISSUEDT") <> DateTime.MinValue Then
                ldrTempNew.Item("Doc_IssueDate") = Format(dr("EMD_ISSUEDT"), OASISConstants.DateFormat)
            Else
                ldrTempNew.Item("Doc_IssueDate") = ""
            End If
            If dr("EMD_EXPDT").ToString <> "" OrElse dr("EMD_EXPDT") <> DateTime.MinValue Then
                ldrTempNew.Item("Doc_ExpDate") = Format(dr("EMD_EXPDT"), OASISConstants.DateFormat)
            Else
                ldrTempNew.Item("Doc_ExpDate") = ""
            End If
            ldrTempNew.Item("Status") = "LOADED"

            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPDOCDETAILS") = dtable
        GridBindDocDetails()
    End Sub

#End Region
    Private Sub ValidateDocumentsinDB()
        Try


            Dim lstrVisaStatus As String
            'Dim iIndex As Integer
            'Dim lintPassprt As Boolean
            'Dim lintVisa As Boolean
            'Dim lintLabCard As Boolean
            Dim lstrVisaType As String
            Dim Documentlist As String = ""
            Dim lstrVisaCategory As String = ""
            'lintPassprt = 0
            'lintVisa = 0
            'lintLabCard = 0
            lstrVisaStatus = ddlVstatus.SelectedItem.Value
            lstrVisaType = ddlVtype.SelectedItem.Value
            lstrVisaCategory = ddlCategory.selecteditem.value

            If Not Session("EMPDOCDETAILS") Is Nothing Then

                For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPDOCDETAILS").Rows(iIndex)
                    'If (Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "DELETED") And _
                    'Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "LOADED" Then
                    'If dr("DocID") = "1" Then lintPassprt = 1
                    'If dr("DocID") = "2" Then lintVisa = 1
                    'If dr("DocID") = "3" Then lintLabCard = 1
                    'End If
                    Documentlist = Documentlist + dr("DocID").ToString & ","
                Next

            End If
            Dim objConn As New SqlConnection
            objConn = ConnectionManger.GetOASISConnection

            Dim iReturnvalue As Integer
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("CheckVisaValidations", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEMD_VisaStatus As New SqlParameter("@VisaStatus", SqlDbType.Int)
            sqlpEMD_VisaStatus.Value = lstrVisaStatus
            cmd.Parameters.Add(sqlpEMD_VisaStatus)

            Dim sqlpEMD_VisaType As New SqlParameter("@VisaType", SqlDbType.Int)
            sqlpEMD_VisaType.Value = lstrVisaType
            cmd.Parameters.Add(sqlpEMD_VisaType)

            Dim sqlpEMD_DOcumentList As New SqlParameter("@Documentlist", SqlDbType.VarChar)
            sqlpEMD_DOcumentList.Value = Documentlist
            cmd.Parameters.Add(sqlpEMD_DOcumentList)

            Dim sqlpEMD_BSUID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            sqlpEMD_BSUID.Value = Session("sBsuid").ToString
            cmd.Parameters.Add(sqlpEMD_BSUID)

            Dim sqlpEMD_visaCategory As New SqlParameter("@visaCategory", SqlDbType.VarChar, 1)
            sqlpEMD_visaCategory.Value = lstrVisaCategory
            cmd.Parameters.Add(sqlpEMD_visaCategory)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
            Else
                lblError.Text = ""
            End If
        Catch ex As Exception
            lblError.Text = "Error while validating documents"
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lstrErrMsg = ""
        ServerValidate()
        ValidateDocumentsinDB()
        lstrErrMsg = lblError.Text
        '   --- Proceed To Save
        If (lstrErrMsg = "") Then
            If Page.IsValid = True Then
                Dim Status As Integer
                Dim bEdit As Boolean
                Dim newNo As String = String.Empty
                Dim reportTo As String = String.Empty
                'check the data mode to do the required operation
                Dim strEmpNo As String = String.Empty
                Dim EmpID As Integer = IIf(ViewState("viewid") IsNot Nothing, ViewState("viewid"), 0)

                If ViewState("datamode") = "edit" Then
                    bEdit = True
                    Dim transaction As SqlTransaction
                    'update  the new user
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try
                            If ddlCategory.SelectedItem.Value <> "N" And ddlCategory.SelectedItem.Value <> hdnABC.Value.Trim Then
                                If txtFromAbc.Text = "" Then
                                    lblError.Text = "Please enter ABC category change date"
                                    ''Throw New Exception("pls enter ABC category change date")
                                    Exit Sub
                                End If
                            End If

                            Status = UpdateEmployeeMaster(conn, transaction, EmpID, hfWU.Value, _
                            txtPassportname.Text, ddlVtype.SelectedItem.Value, ddlVstatus.SelectedItem.Value, _
                            hfML_ID.Value, hfME_ID.Value, hfIU.Value, txtVisaPrgID.Text, ddlEmpContractType.SelectedItem.Value)

                            Dim retVal As Integer = SaveEMPDocumentDetails(conn, transaction, EmpID)

                            ''added by nahyan to audit abc category 
                            If ddlCategory.SelectedItem.Value <> "N" And ddlCategory.SelectedItem.Value <> hdnABC.Value.Trim Then
                                Status = SaveEMPTRANTYPE_TRN(Session("sbsuid"), EmpID, "18", ddlCategory.SelectedItem.Value, _
                                  txtFromAbc.Text, Nothing, "", hdnABC.Value.Trim, False, transaction)
                            End If

                            'If error occured during the process  throw exception and rollback the process
                            If Status = -1 Then
                                Throw New ArgumentException("Record does not exist for updating")
                            ElseIf Status <> 0 Then
                                Throw New ArgumentException("Error while updating the current record")
                                UtilityObj.Errorlog(UtilityObj.getErrorMessage(Status))
                            Else
                                'Store the required information into the Audit Trial table when Edited
                                Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EmpID & "-" & txtEmpno1.Text, "edit", Page.User.Identity.Name.ToString, Page.Master.FindControl("cphMasterpage"))
                                If Status <> 0 Then
                                    UtilityObj.Errorlog(UtilityObj.getErrorMessage(Status))
                                    Throw New ArgumentException("Could not complete your request")
                                End If


                                If retVal = 0 Then
                                    'if everything is saved properly then Clear the Sessions
                                    ClearSession_GridDetails()
                                Else
                                    lblError.Text = UtilityObj.getErrorMessage(retVal)
                                    Throw New ArgumentException("Could not complete your request")
                                End If
                                ViewState("datamode") = "none"
                                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                transaction.Commit()
                                Call clearme()
                                lblError.Text = "Record Updated Successfully"
                                Response.Redirect(ViewState("ReferrerUrl"))

                            End If
                        Catch myex As ArgumentException
                            If Status <> 0 Then
                                lblError.Text = UtilityObj.getErrorMessage(Status)
                            Else
                                lblError.Text = myex.Message
                            End If
                            transaction.Rollback()
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message)
                            'transaction.Rollback()
                            lblError.Text = "Record could not be Updated"
                        End Try
                    End Using
                End If
            End If
        End If
    End Sub

    Private Function UpdateEmployeeMaster(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, _
    ByVal EMP_ID As String, ByVal BSU_ID As String, ByVal EMP_PASSPORTNAME As String, _
    ByVal EMP_VISATYPE As String, ByVal EMP_VISASTATUS As String, ByVal EMP_VISA_DES_ID As String, _
    ByVal EMP_MOE_DES_ID As String, ByVal EMP_VISA_BSU_ID As String, ByVal EMP_VISA_ID As String, _
    ByVal EMP_CONTRACTTYPE As String) As Integer
        Dim status As Integer
        Dim newNo As String = String.Empty
        Dim dt As New DataTable
        'Dim drSalScale As SqlDataReader
        Dim drSalScale As DataRow
        Dim str_sql As String = String.Empty
        str_sql = "select * from EMPLOYEE_M WHERE EMP_ID = '" & _
        EMP_ID & "' AND EMP_BSU_ID = '" & BSU_ID & "'"

        Dim cmd1 As SqlCommand = New SqlCommand(str_sql, conn, trans)
        cmd1.CommandType = CommandType.Text
        Dim sqlAdpt As New SqlDataAdapter(cmd1)
        sqlAdpt.Fill(dt)
        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                drSalScale = dt.Rows(0)

                status = AccessRoleUser.SaveEmployee_DocumentsData(EMP_ID, EMP_VISATYPE, EMP_VISASTATUS, EMP_VISA_DES_ID, EMP_MOE_DES_ID, EMP_VISA_BSU_ID,
                                          ddlCategory.SelectedItem.Value, EMP_CONTRACTTYPE, EMP_VISA_ID, txtPersonID.Text, txtPassportname.Text,
                                          GetVal(drSalScale("EMP_BSU_ID")),
                                            GetVal(drSalScale("EMP_JOINDT")), GetVal(drSalScale("EMP_BSU_JOINDT")), Session("sUsr_name"), txtUIDNo.Text, conn, trans)

                Return status

            End If
        End If
        Return -1
    End Function

    Private Function GetVal(ByVal obj As Object, Optional ByVal datatype As SqlDbType = SqlDbType.VarChar) As Object
        If obj Is DBNull.Value Then
            Select Case datatype
                Case SqlDbType.Decimal
                    Return 0
                Case SqlDbType.Int
                    Return 0
                Case SqlDbType.Bit
                    Return False
                Case SqlDbType.VarChar
                    Return ""
            End Select
        Else
            Return obj
        End If
        Return ""
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        pnlMain.Enabled = True
        UtilityObj.beforeLoopingControls(Page.Master.FindControl("cphMasterpage"))
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Public Function GetBSUEmpFilePath() As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select BSU_EMP_FILEPATH FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString
    End Function

    Private Sub ClearSession_GridDetails()
        'if everything is saved properly then Clear the Sessions
        Session.Remove("EMPDOCDETAILS")
        Session.Remove("EMPEXPDETAILS")
        Session.Remove("EMPQUALDETAILS")
        Session.Remove("EMPSALDETAILS")
        Session.Remove("EMPDEPENDANCEDETAILS")

        Session.Remove("EMPDocEditID")
        Session.Remove("EMPEXPEditID")
        Session.Remove("EMPQUALEditID")
        Session.Remove("EMPSALEditID")
        Session.Remove("EMPDEPENDANCEEditID")
        GridBindDocDetails()
    End Sub

#Region "Document Details"

    Protected Sub btnDocAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocAdd.Click
        If Session("EMPDOCDETAILS") Is Nothing Then
            Session("EMPDOCDETAILS") = CreateDocDetailsTable()
        End If
        Dim dtDocDetails As DataTable = Session("EMPDOCDETAILS")
        Dim status As String = String.Empty
        If CDate(txtDocIssueDate.Text) > CDate(txtDocExpDate.Text) Then
            lblError.Text = "Exp. Date should be less than Issue Date"
            GridBindDocDetails()
            Exit Sub
        End If

        Dim i As Integer
        For i = 0 To dtDocDetails.Rows.Count - 1
            If Session("EMPDocEditID") IsNot Nothing And _
            Session("EMPDocEditID") <> dtDocDetails.Rows(i)("UniqueID") Then
                If dtDocDetails.Rows(i)("Document") = txtDocDocument.Text And _
                    dtDocDetails.Rows(i)("DocID") = h_DOC_DOCID.Value And _
                     dtDocDetails.Rows(i)("Doc_No") = txtDocDocNo.Text And _
                     dtDocDetails.Rows(i)("Doc_IssuePlace") = txtDocIssuePlace.Text And _
                     dtDocDetails.Rows(i)("Doc_IssueDate") = CDate(txtDocIssueDate.Text) And _
                     dtDocDetails.Rows(i)("Doc_ExpDate") = CDate(txtDocExpDate.Text) Then
                    lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                    GridBindDocDetails()
                    Exit Sub
                End If
            End If
        Next

        If btnDocAdd.Text = "Add" Then
            Dim newDR As DataRow = dtDocDetails.NewRow()
            newDR("UniqueID") = gvEMPDocDetails.Rows.Count()
            newDR("Document") = txtDocDocument.Text
            newDR("DocID") = h_DOC_DOCID.Value
            newDR("Doc_No") = txtDocDocNo.Text
            newDR("Doc_bActive") = chkDocActive.Checked
            newDR("Doc_IssuePlace") = txtDocIssuePlace.Text
            newDR("Doc_IssueDate") = CDate(txtDocIssueDate.Text)
            newDR("Doc_ExpDate") = CDate(txtDocExpDate.Text)
            newDR("Status") = "Insert"
            status = "Insert"
            lblError.Text = ""
            dtDocDetails.Rows.Add(newDR)
            btnDocAdd.Text = "Add"
        ElseIf btnDocAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPDocEditID")
            For iIndex = 0 To dtDocDetails.Rows.Count - 1
                If str_Search = dtDocDetails.Rows(iIndex)("UniqueID") And dtDocDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtDocDetails.Rows(iIndex)("DOC_No") = txtDocDocNo.Text
                    dtDocDetails.Rows(iIndex)("Document") = txtDocDocument.Text
                    dtDocDetails.Rows(iIndex)("DOC_IssuePlace") = txtDocIssuePlace.Text
                    dtDocDetails.Rows(iIndex)("DOC_ExpDate") = txtDocExpDate.Text
                    dtDocDetails.Rows(iIndex)("DocID") = h_DOC_DOCID.Value
                    dtDocDetails.Rows(iIndex)("DOC_IssueDate") = txtDocIssueDate.Text
                    dtDocDetails.Rows(iIndex)("Doc_bActive") = chkDocActive.Checked
                    If dtDocDetails.Rows(iIndex)("Status").ToString.ToUpper <> "INSERT" Then
                        dtDocDetails.Rows(iIndex)("Status") = "Edit"
                    End If
                    Exit For
                End If
            Next
            btnDocAdd.Text = "Add"
            ClearEMPDocDetails()
        End If

        If ViewState("datamode") = "EDIT" Then
            If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, Page.Master.FindControl("cphMasterpage")) = 0 Then
                ClearEMPDocDetails()
            Else
                UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
            End If
        End If
        Session("EMPDOCDETAILS") = dtDocDetails
        GridBindDocDetails()
    End Sub

    Private Function CreateDocDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cDocument As New DataColumn("Document", System.Type.GetType("System.String"))
            Dim cDocID As New DataColumn("DocID", System.Type.GetType("System.String"))
            Dim cDoc_No As New DataColumn("Doc_No", System.Type.GetType("System.String"))
            Dim cDoc_bActive As New DataColumn("Doc_bActive", System.Type.GetType("System.Boolean"))
            Dim cDoc_IssuePlace As New DataColumn("Doc_IssuePlace", System.Type.GetType("System.String"))
            Dim cDoc_IssueDate As New DataColumn("Doc_IssueDate", System.Type.GetType("System.DateTime"))
            Dim cDoc_ExpDate As New DataColumn("Doc_ExpDate", System.Type.GetType("System.DateTime"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cDocument)
            dtDt.Columns.Add(cDocID)
            dtDt.Columns.Add(cDoc_No)
            dtDt.Columns.Add(cDoc_bActive)
            dtDt.Columns.Add(cDoc_IssuePlace)
            dtDt.Columns.Add(cDoc_IssueDate)
            dtDt.Columns.Add(cDoc_ExpDate)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnDocCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocCancel.Click
        ClearEMPDocDetails()
    End Sub

    Sub ClearEMPDocDetails()
        txtDocDocNo.Text = ""
        h_DOC_DOCID.Value = ""
        txtDocDocument.Text = ""
        txtDocExpDate.Text = ""
        txtDocIssueDate.Text = ""
        txtDocIssuePlace.Text = ""
        chkDocActive.Checked = True
        btnDocAdd.Text = "Add"
        lblError.Text = ""
        Session.Remove("EMPDocEditID")
    End Sub

    Protected Sub lnkDocEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If str_Search = Session("EMPDOCDETAILS").Rows(iIndex)("UniqueID") And Session("EMPDOCDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDocDocNo.Text = Session("EMPDOCDETAILS").Rows(iIndex)("DOC_No")
                h_DOC_DOCID.Value = Session("EMPDOCDETAILS").Rows(iIndex)("DOCID")
                txtDocDocument.Text = Session("EMPDOCDETAILS").Rows(iIndex)("Document")
                txtDocIssuePlace.Text = Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssuePlace")
                If Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate") Is Nothing OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate").ToString = "" OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate") = New Date(1900, 1, 1) Then
                    txtDocExpDate.Text = ""
                Else
                    txtDocExpDate.Text = Format(Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate"), OASISConstants.DateFormat)
                End If
                If Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate") Is Nothing OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate").ToString = "" OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate") = New Date(1900, 1, 1) Then
                    txtDocIssueDate.Text = ""
                Else
                    txtDocIssueDate.Text = Format(Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate"), OASISConstants.DateFormat)
                End If
                chkDocActive.Checked = Session("EMPDOCDETAILS").Rows(iIndex)("Doc_bActive")
                gvEMPDocDetails.SelectedIndex = iIndex
                btnDocAdd.Text = "Save"
                Session("EMPDocEditID") = str_Search
                UtilityObj.beforeLoopingControls(Page.Master.FindControl("cphMasterpage"))
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvEMPDocDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEMPDocDetails.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowDocDetails(categoryID)
    End Sub

    Private Sub DeleteRowDocDetails(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim lblTid As New Label
        lblTid = TryCast(gvEMPDocDetails.Rows(pId).FindControl("lblId"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        For iRemove = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If (Session("EMPDOCDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPDOCDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, Page.Master.FindControl("cphMasterpage")) = 0 Then
                    GridBindDocDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Private Sub GridBindDocDetails()
        If Session("EMPDOCDETAILS") Is Nothing Then
            gvEMPDocDetails.DataSource = Nothing
            gvEMPDocDetails.DataBind()
            Return
        End If
        Dim strColumnName As String = String.Empty
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateDocDetailsTable()
        If Session("EMPDOCDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                If (Session("EMPDOCDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPDOCDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPDOCDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvEMPDocDetails.DataSource = dtTempDtl
        gvEMPDocDetails.DataBind()
    End Sub

    Private Function SaveEMPDocumentDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPDOCDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPDOCDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPDOCDETAILS").Rows(iIndex)
                    If (Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "DELETED") And _
                    Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "LOADED" Then
                        cmd = New SqlCommand("SaveEMPDOCUMENTS_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_EMP_ID As New SqlParameter("@EMD_EMP_ID", SqlDbType.Int)
                        sqlpEMD_EMP_ID.Value = EmpID
                        cmd.Parameters.Add(sqlpEMD_EMP_ID)

                        Dim sqlpEMD_ESD_ID As New SqlParameter("@EMD_ESD_ID", SqlDbType.Int)
                        sqlpEMD_ESD_ID.Value = dr("DocID")
                        cmd.Parameters.Add(sqlpEMD_ESD_ID)

                        'JNL_SUB_ID
                        Dim sqlpEMD_NO As New SqlParameter("@EMD_NO", SqlDbType.VarChar, 50)
                        sqlpEMD_NO.Value = dr("Doc_No")
                        cmd.Parameters.Add(sqlpEMD_NO)
                        If Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "EDIT" Then
                            Dim sqlpEMD_ID As New SqlParameter("@EMD_ID", SqlDbType.Int)
                            sqlpEMD_ID.Value = dr("UniqueID")
                            cmd.Parameters.Add(sqlpEMD_ID)
                        End If

                        Dim sqlpEMP_ISSUEPLACE As New SqlParameter("@EMP_ISSUEPLACE", SqlDbType.VarChar, 100)
                        sqlpEMP_ISSUEPLACE.Value = dr("DOC_IssuePlace")
                        cmd.Parameters.Add(sqlpEMP_ISSUEPLACE)

                        Dim sqlpEMD_ISSUEDT As New SqlParameter("@EMD_ISSUEDT", SqlDbType.DateTime)
                        sqlpEMD_ISSUEDT.Value = dr("DOC_IssueDate")
                        cmd.Parameters.Add(sqlpEMD_ISSUEDT)

                        Dim sqlpEMD_EXPDT As New SqlParameter("@EMD_EXPDT", SqlDbType.DateTime)
                        sqlpEMD_EXPDT.Value = dr("DOC_ExpDate")
                        cmd.Parameters.Add(sqlpEMD_EXPDT)

                        Dim sqlpEMD_bACTIVE As New SqlParameter("@EMD_bACTIVE", SqlDbType.Bit)
                        sqlpEMD_bACTIVE.Value = dr("Doc_bActive")
                        cmd.Parameters.Add(sqlpEMD_bACTIVE)

                        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                        If Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "INSERT" Then
                            sqlpbEdit.Value = False
                        ElseIf Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "EDIT" Then
                            sqlpbEdit.Value = True
                        End If
                        cmd.Parameters.Add(sqlpbEdit)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    ElseIf Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "DELETED" Then
                        'Delete Code will Come here
                        cmd = New SqlCommand("DELETEEMPDOCUMENTS_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_ID As New SqlParameter("@EMD_ID", SqlDbType.Int)
                        sqlpEMD_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpEMD_ID)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

#End Region

#Region "Server Validations"

    Protected Sub ServerValidate()
        lstrErrMsg = ""
        If Trim(txtFname1.Text) = "" Then
            lblError.Text = "Employee First Name should not be blank"
        End If
        lstrErrMsg = lblError.Text
    End Sub

    Private Function ValidateValue(ByVal val As String, ByVal isNumeric As Boolean) As Object
        If val = "" Or val = String.Empty Then
            Return DBNull.Value
        ElseIf isNumeric Then
            Return CInt(val)
        End If
        Return DBNull.Value
    End Function

#End Region

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            pnlMain.Enabled = False
            Call clearme()
            'clear the textbox and set the default settings
            ClearAllSessions()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub ClearAllSessions()
        Session.Remove("EMPDEPENDANCEDETAILS")
        Session.Remove("EMPQUALDETAILS")
        Session.Remove("EMPEXPDETAILS")
        Session.Remove("EMPSALSCHEDULE")
        Session.Remove("EMPSALDETAILS")
        Session.Remove("EMPDOCDETAILS")
        Session.Remove("EMPSALDETAILS")
        Session.Remove("EMPSALDETAILS")
    End Sub
    Protected Sub ddlVstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    If ddlVstatus.SelectedItem.Text = "Visa Holder" Then
        '        ddlVtype.SelectedValue = 0
        '    ElseIf ddlVstatus.SelectedItem.Text = "LC Holder" Then
        '        ddlVtype.SelectedValue = 1
        '    End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message)
        'End Try
        Call callVisa_Bind()

    End Sub
    Protected Sub ddlVtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVtype.SelectedIndexChanged
        If ((ddlVstatus.SelectedItem.Text = "LC Holder") And ((ddlVtype.SelectedItem.Text = "Temporary Permit") Or (ddlVtype.SelectedItem.Text = "Part-time Permit"))) Then
            ' ddlCategory.SelectedValue = "B"
            txtIU.Text = "NON-GEMS(VISA Unit)"
            hfIU.Value = "800015"
        Else
            'ddlCategory.SelectedValue = "C"
            txtIU.Text = Session("BSU_Name")
            hfIU.Value = Session("sBsuid")
        End If
    End Sub
    Public Sub callVisa_Bind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Try
            Dim Status_ID As String = String.Empty
            If ddlVstatus.SelectedIndex = -1 Then
                Status_ID = ""
            Else
                Status_ID = ddlVstatus.SelectedItem.Value
            End If
            Dim di As ListItem
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@VISA_STATUS_ID", Status_ID)

            Using VST_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetVISA_Type", pParms)
                ddlVtype.Items.Clear()
                If VST_reader.HasRows = True Then
                    While VST_reader.Read
                        di = New ListItem(VST_reader("EVM_DESCR"), VST_reader("EVM_ID"))
                        ddlVtype.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class

