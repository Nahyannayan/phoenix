Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Web.Configuration
Imports UtilityObj

'Version   Done By        On                       For

'  1.1     Swapana      14-Dec-2010     Addition of Approval screen to approve transfer by transferring BSU
' 1.2       16-Jan-2011     Swapna          Changed menu options in DB as follows:
'                                           "H000074" instead of "P013190"
'                                           "H000075" instead of "P050040"
'1.3        Swapna          23-Feb-2012   empno added to mail text
'1.4        SwapnA          2-Jul-2012          Future dated transfer changes
'            Hashmi                          Multiple email sending

Partial Class EmpStaffTransfer
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            ViewState("LastUnitJoinDate") = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If ViewState("datamode") = "view" Then
                ViewState("EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                FillEmployeeDatas(ViewState("EMP_ID"))
                FillSalaryDetails(ViewState("EMP_ID"))

            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("transID") = Encr_decrData.Decrypt(Request.QueryString("transID").Replace(" ", "+")) 'V1.4
            'collect the url of the file to be redirected in view state
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "H000075" Then     'V1.2
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim menu_rights As Integer
                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            txtSalAmtEligible.Attributes.Add("onblur", "javascript:GetPayableFromEligible();")

            MVEMPTRANSFER.ActiveViewIndex = 0
            BindENR()
            BindSalaryPayCurrency()
            UtilityObj.beforeLoopingControls(Me.Page)
            GetPayMonth_YearDetails()
       

        End If
        SetReadOnlyFields() 'V1.1
        If ddlPayMode.SelectedValue = "0" Then
            txtBname.Text = ""
            txtBname.Enabled = False
            btnBank_name.Enabled = False
            txtAccCode.Text = ""
            txtAccCode.Enabled = False
        Else
            txtBname.Enabled = True
            btnBank_name.Enabled = True
            txtAccCode.Enabled = True

        End If

    End Sub
    Private Sub FillSalaryDetails(ByVal empID As Integer)
        Dim dtable As DataTable = CreateSalaryDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal = 0
        Dim IsBasicEntered As Boolean = False  'V1.1
        str_Sql = "SELECT     EMPSALARYHS_s.*, EMPSALCOMPO_M.ERN_DESCR FROM EMPSALARYHS_s LEFT OUTER JOIN EMPSALCOMPO_M" & _
        " ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID where ESH_EMP_ID = " & empID & " and ESH_TODT is null ORDER BY ERN_ORDER "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            grossSal += dr("ESH_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = dr("ESH_ID")
            ldrTempNew.Item("ENR_ID") = dr("ESH_ERN_ID")
            If (dr("ESH_ERN_ID") = "BASIC") Then
                IsBasicEntered = True
            End If
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = dr("ESH_bMonthly")
            ldrTempNew.Item("PAYTERM") = dr("ESL_PAYTERM")
            ldrTempNew.Item("PAY_SCHEDULE") = GETPAY_SCHEDULE(dr("ESL_PAYTERM"))
            ldrTempNew.Item("Amount") = dr("ESH_AMOUNT")
            If dr("ESH_ELIGIBILITY") Is DBNull.Value Then
                ldrTempNew.Item("Amount_ELIGIBILITY") = dr("ESH_AMOUNT")
            Else
                ldrTempNew.Item("Amount_ELIGIBILITY") = dr("ESH_ELIGIBILITY")
            End If
            ldrTempNew.Item("Status") = "LOADED"
            dtable.Rows.Add(ldrTempNew)
            'Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
            'FillAllocationPopup(dr("ESH_ERN_ID"), dr("ESH_ID"))
        End While
        dr.Close()
        'V1.1 starts
        'In case no salary records are available add a default row with BASIC salary = AED 1
        If (dtable.Rows.Count = 0 Or IsBasicEntered = 0) Then
            Dim ldrTempNew As DataRow
            grossSal = grossSal + 1
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dtable.Rows.Count
            ldrTempNew.Item("ENR_ID") = "BASIC"
            ldrTempNew.Item("ERN_DESCR") = "Basic"
            ldrTempNew.Item("bMonthly") = True
            ldrTempNew.Item("PAYTERM") = 0
            ldrTempNew.Item("PAY_SCHEDULE") = "Monthly"
            ldrTempNew.Item("Amount") = 1
            ldrTempNew.Item("Amount_ELIGIBILITY") = 1
            ldrTempNew.Item("Status") = "INSERT"
            dtable.Rows.Add(ldrTempNew)
        End If
        'V1.1 ends
        Session("EMPSALDETAILS") = dtable
        GridBindSalaryDetails()
    End Sub
    'V1.1
    Private Function GETPAY_SCHEDULE(ByVal vPAYTERM As Integer) As String
        Select Case vPAYTERM
            Case 0
                Return "Monthly"
            Case 1
                Return "Bimonthly"
            Case 2
                Return "Quarterly"
            Case 3
                Return "Half-yearly"
            Case 4
                Return "Yearly"
        End Select
        Return ""
    End Function
    Protected Sub ClearAllDetails()
        txtJdate.Text = ""
        txtEmpName.Text = ""
        txtML.Text = ""
        hfML_ID.Value = ""
        txtME.Text = ""
        hfME_ID.Value = ""
        txtGrade.Text = ""
        hfGrade_ID.Value = ""
        txtReport.Text = ""
        hfReport.Value = ""
        txtVacationPolicy.Text = ""
        hf_VacationPolicy.Value = ""
        txtSD.Text = ""
        hfSD_ID.Value = ""
        txtCat.Text = ""
        hfCat_ID.Value = ""
        txtDept.Text = ""
        hfDept_ID.Value = ""
        txtEmpApprovalPol.Text = ""
        h_ApprovalPolicy.Value = ""
        hfSalGrade.Value = ""
        txtSalGrade.Text = ""
        txtBname.Text = ""
        hfBank_ID.Value = ""
        txtAccCode.Text = ""
        txtSwiftcode.Text = ""
        txtFTE.Text = ""
        gvEmpSalary.DataSource = ""
        gvEmpSalary.DataBind()
    End Sub
    Protected Sub SetReadOnlyFields()
        'Made readonly property of textbox as false and added attribute readonly to read the values
        'updated in the textboxes
        txtEmpName.Attributes.Add("ReadOnly", "ReadOnly")
        txtJdate.Attributes.Add("ReadOnly", "ReadOnly")

        txtML.Attributes.Add("ReadOnly", "ReadOnly")
        txtME.Attributes.Add("ReadOnly", "ReadOnly")
        txtGrade.Attributes.Add("ReadOnly", "ReadOnly")
        txtReport.Attributes.Add("ReadOnly", "ReadOnly")
        txtVacationPolicy.Attributes.Add("ReadOnly", "ReadOnly")
        txtSD.Attributes.Add("ReadOnly", "ReadOnly")
        txtCat.Attributes.Add("ReadOnly", "ReadOnly")
        txtDept.Attributes.Add("ReadOnly", "ReadOnly")
        txtEmpApprovalPol.Attributes.Add("ReadOnly", "ReadOnly")
        txtSalGrade.Attributes.Add("ReadOnly", "ReadOnly")
        txtBname.Attributes.Add("ReadOnly", "ReadOnly")

    End Sub
    'V1.1 ends

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Dim isBasicAdded As Boolean = False
            'If CDate(txtJdate.Text) < Today.Date Then
            If (txtJdate.Text) <> "" Then
                If (ViewState("EMP_ID") <> "") Then

                    If ViewState("MainMnu_code") = "H000075" Then     'V1.2
                        If (txtGsalary.Text = "" Or Val(txtGsalary.Text) = 0) Then
                            lblError.Text = getErrorMessage("0")
                            Return
                        Else
                            lblError.Text = ""
                            For Each gvr As GridViewRow In gvEmpSalary.Rows
                                If gvr.RowType = DataControlRowType.DataRow Then
                                    If (TryCast(gvr.FindControl("lblENRID"), Label).Text = "BASIC") Then   'This is ID for Basic salary which is must to be added
                                        isBasicAdded = True

                                    End If
                                End If
                            Next
                            If (isBasicAdded = True) Then
                                Call SaveEmployeeTransfer(ViewState("EMP_ID"), ViewState("transID"))
                            Else
                                lblError.Text = getErrorMessage("960")
                            End If

                        End If


                    End If
                Else
                    lblError.Text = getErrorMessage("963")

                End If
            Else
                lblError.Text = getErrorMessage("965")
            End If
        End If
    End Sub

    Private Function SaveEmployeeTransfer(ByVal EMP_ID As String, ByVal TransferID As String) As Integer
        Dim errorno As Integer = 0
        Dim emp_newNo As String = String.Empty
        Dim NEW_EMP_STAFF_NO As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim trans As SqlTransaction = conn.BeginTransaction("SampleTransaction")
            Try
                If (CDate(txtJdate.Text) > Today.Date And ViewState("LastUnitJoinDate") <= Today.Date) Then  'V1.4  -- swapna added for new employees joining in future/transferred
                    If errorno = 0 Then errorno = UPDATETODATETRANTYPE(conn, trans, EMP_ID, CDate(txtJdate.Text), ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = UpdateTransactionTypeTable(conn, trans, EMP_ID)
                    If errorno = 0 Then errorno = SaveEmployeeAllocation(conn, trans, EMP_ID, emp_newNo, NEW_EMP_STAFF_NO)
                    ' If errorno = 0 Then errorno = ResetSalaryDetails(conn, trans, EMP_ID, ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = SaveEMPTransferSalaryDetails(conn, trans, EMP_ID, ViewState("transID"))
                    If errorno = 0 Then errorno = UPDATETransferTable(conn, trans, EMP_ID, ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = UpdateEmployeeTransferMaster(conn, trans, EMP_ID, Session("sBSUID"), CDbl(Val(txtGsalary.Text)), emp_newNo, NEW_EMP_STAFF_NO, TransferID)
                    'V1.1  --- To make employee user name NULL after transfer
                    '  If errorno = 0 Then errorno = MakeEMPUSRName_Null(conn, trans, EMP_ID, Session("sbsuid"), ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = SaveEmpProvisions(conn, trans, EMP_ID, ViewState("FROM_BSU_ID"), ViewState("transID"))

                Else
                    If errorno = 0 Then errorno = UPDATETODATETRANTYPE(conn, trans, EMP_ID, CDate(txtJdate.Text), ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = UpdateTransactionTypeTable(conn, trans, EMP_ID)
                    If errorno = 0 Then errorno = SaveEmployeeAllocation(conn, trans, EMP_ID, emp_newNo, NEW_EMP_STAFF_NO)
                    If errorno = 0 Then errorno = ResetSalaryDetails(conn, trans, EMP_ID, ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = SaveEMPSalaryDetails(conn, trans, EMP_ID)
                    If errorno = 0 Then errorno = UPDATETransferTable(conn, trans, EMP_ID, ViewState("FROM_BSU_ID"))
                    If errorno = 0 Then errorno = UpdateEmployeeMaster(conn, trans, EMP_ID, Session("sBSUID"), CDbl(Val(txtGsalary.Text)), emp_newNo, NEW_EMP_STAFF_NO)
                    If errorno = 0 Then errorno = SaveEmpProvisions(conn, trans, EMP_ID, ViewState("FROM_BSU_ID"), ViewState("transID")) 'V1.4
                    'V1.1  --- To make employee user name NULL after transfer
                    'If errorno = 0 Then errorno = MakeEMPUSRName_Null(conn, trans, EMP_ID, Session("sbsuid"), ViewState("FROM_BSU_ID"))
                End If

                If errorno = 0 Then
                    trans.Commit()
                    'SendEmailToNextApprover()
                    SendEmailToNextApprover_Alternate()
                    ClearEMPSalDetails()
                    ClearAllDetails()
                    ViewState("EMP_ID") = ""
                    'V1.1
                    ' lblError.Text = "Employee Transfer Completed successfully"
                    lblError.Text = getErrorMessage("956")

                    ViewState("datamode") = "view"

                Else
                    trans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(errorno)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = getErrorMessage("957")
                Return 1000
                'V1.1

                'lblError.Text = "Error occured while Trnasfering Employee"
            End Try
        End Using
    End Function
    Protected Sub SendEmailToNextApprover()
        Dim Mailstatus As String = ""
        Try


            'Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            'Dim selectId = Encr_decrData.Decrypt(Request.QueryString("transID").Replace(" ", "+"))
            'Dim ds As New DataSet()
            'Dim pParms(4) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            'pParms(0).Value = Session("sBsuid").ToString

            'pParms(1) = New SqlClient.SqlParameter("@USER_ID", SqlDbType.VarChar, 100)
            'pParms(1).Value = Session("sUsr_name").ToString


            'pParms(2) = New SqlClient.SqlParameter("@PCH_ID", SqlDbType.Int)
            'pParms(2).Value = selectId

            'Dim strStatus As String = "A"


            'pParms(3) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar, 10)
            'pParms(3).Value = strStatus

            'ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getPettyExpMailDetails", pParms)
            ''lblError.Text = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            Dim ToEmailId As String = ""
            'If ds.Tables(0).Rows.Count > 0 Then
            '    ToEmailId = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            'End If
            Dim pParms(0) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = Session("sBsuid")
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[getTransferMailIDs]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
            End Using

            If ToEmailId = "" Then
                ToEmailId = "robbie@gemseducation.com"
            End If
            Dim fromemailid = "system@gemseducation.com"
            Dim sb As New StringBuilder
            Dim MainText As String = ""
            Dim Subject As String = ""
            'If ds.Tables(1).Rows.Count > 0 Then

            Subject = "Staff Transfer"

            MainText = "Kindly note that following employee has been transferred to your unit."
            ' End If

            sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
            sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Hi,</td></tr>")
            sb.Append("<tr><td ><br /><br /></td></tr>")
            sb.Append("<tr><td>" & MainText & "</td></tr>")
            sb.Append("<tr><td>Employee No. : " & hfEmployeeNum.Value & "</td></tr>")
            sb.Append("<tr><td>Employee Name : " & txtEmpName.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Transferred From : " & txtFromBSUName.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Designation : " & txtSD.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Transfer Date: " & txtJdate.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Department : " & txtDept.Text.ToString & "</td></tr>")
            sb.Append("<tr><td ><br /><br /></td></tr>")


            sb.Append("<tr><td ><br /><br />Regards,</td></tr>")
            sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
            sb.Append("<tr></tr><tr></tr>")
            sb.Append("</table>")

            Dim ds2 As New DataSet
            ds2 = GetCommunicationSettings()

            Dim username = ""
            Dim password = ""
            Dim port = 0
            Dim host = ""


            If ds2.Tables(0).Rows.Count > 0 Then
                username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            End If
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    For Each drow As DataRow In ds.Tables(0).Rows
                        ToEmailId = drow.Item("MailID")
                        Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb.ToString, username, password, host, port, 0, False)
                    Next
                End If
            End If


        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
            'UtilityObj.Errorlog(Mailstatus)

        End Try
    End Sub
    Protected Sub SendEmailToNextApprover_Alternate() 'V1.4
        Dim Mailstatus As String = ""
        Try
            Dim dsApprovers As DataSet
            Dim ToEmailId As String = ""
            dsApprovers = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, "exec GetTransferEmailIdsNew '" & Session("sBsuId") & "'," & ViewState("transID"))
            If Not dsApprovers Is Nothing Then
                For Each rowApprover As DataRow In dsApprovers.Tables(0).Rows
                    Dim pParms(8) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@TO_EMP_ID", SqlDbType.VarChar)
                    pParms(0).Value = rowApprover.Item("emd_email")
                    pParms(1) = New SqlClient.SqlParameter("@TRANS_EMP_NO", SqlDbType.VarChar)
                    pParms(1).Value = hfEmployeeNum.Value
                    pParms(2) = New SqlClient.SqlParameter("@TRANS_EMP_NAME", SqlDbType.VarChar)
                    pParms(2).Value = txtEmpName.Text.ToString
                    pParms(3) = New SqlClient.SqlParameter("@TRANS_BSU_NAME", SqlDbType.VarChar)
                    pParms(3).Value = txtFromBSUName.Text.ToString
                    pParms(4) = New SqlClient.SqlParameter("@DESIGNATION", SqlDbType.VarChar)
                    pParms(4).Value = txtSD.Text.ToString
                    pParms(5) = New SqlClient.SqlParameter("@TRANSFER_DATE", SqlDbType.VarChar)
                    pParms(5).Value = txtJdate.Text.ToString
                    pParms(6) = New SqlClient.SqlParameter("@DEPARTMENT", SqlDbType.VarChar)
                    pParms(6).Value = txtDept.Text.ToString
                    pParms(7) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar)
                    pParms(7).Value = "T"
                    pParms(8) = New SqlClient.SqlParameter("@ETF_ID", SqlDbType.Int)
                    pParms(8).Value = ViewState("transID")
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, "SEND_EMPLOYEE_TRANSFER_EMAIL", pParms)
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'UtilityObj.Errorlog(Mailstatus)
        End Try
    End Sub

    Public Function GetCommunicationSettings() As DataSet


        Dim ds As DataSet

        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)

        Return ds

    End Function
    Private Function UPDATETransferTable(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByVal fromBSUID As String) As Integer

        Dim iReturnvalue As Integer
        Try
            Dim strUserName As String = ""  'V1.1-  TO UPDATE BSU 2 APPROVER NAME IN DB
            If Not (Session("sUsr_name") Is Nothing) Then
                strUserName = Session("sUsr_name")
            End If

            Dim cmd As New SqlCommand
            Dim str_sql As String = String.Empty
            cmd = New SqlCommand("", objConn, stTrans)
            cmd.CommandType = CommandType.Text
            str_sql = "Update EMPTRANSFER set ETF_TO_bAccept = 1,ETF_ToBSUApprover='" & strUserName & "',ETF_STATUS=8 WHERE ETF_TO_bAccept = 0 AND ETF_EMP_ID =" & EmpID _
            & " AND ETF_FROM_BSU_ID = '" & fromBSUID & "' and isnull(etf_bisdeleted,0)=0 AND ETF_TO_BSU_ID= '" & Session("sbsuid") & "'"
            cmd.CommandText = str_sql
            iReturnvalue = cmd.ExecuteNonQuery()
            If iReturnvalue >= 1 Then
                Return 0
            Else
                Return iReturnvalue
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    'Function added for V1.1 to update username as null
    Private Function MakeEMPUSRName_Null(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByVal TOBSUID As String, ByVal OLDBSUID As String) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("EMP_NULL_EMPUSERNAME", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 15)
            sqlpEMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = TOBSUID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpOldBSU_ID As New SqlParameter("@OLDBSU_ID", SqlDbType.VarChar, 15)
            sqlpOldBSU_ID.Value = OLDBSUID
            cmd.Parameters.Add(sqlpOldBSU_ID)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then

                'iReturnvalue = cmd.ExecuteNonQuery()
                Return 1000
            End If

            Return iReturnvalue

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function

    Private Function UPDATETODATETRANTYPE(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByVal frmDt As DateTime, _
    ByVal BSU_ID As String) As Integer

        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            Dim str_sql As String = String.Empty
            cmd = New SqlCommand("", objConn, stTrans)
            cmd.CommandType = CommandType.Text
            'For bug fix of date format
            frmDt = Convert.ToDateTime(frmDt).ToString("dd/MMM/yyyy")
            str_sql = "update  EMPTRANTYPE_TRN set EST_DTTO = '" & frmDt.AddDays(-1) & _
            "' where EST_BSU_ID='" & BSU_ID & "' and EST_EMP_ID=" & EmpID & " and EST_DTTO is null"
            'str_sql = "Update EMPTRANSFER set ETF_TO_bAccept = 1 WHERE ETF_EMP_ID =" & EmpID
            cmd.CommandText = str_sql
            iReturnvalue = cmd.ExecuteNonQuery()
            If iReturnvalue >= 1 Then
                Return 0
            Else
                Return iReturnvalue
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function

    Private Function UpdateTransactionTypeTable(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim ht As New Hashtable
        ht(OASISConstants.TRANS_TYPE_Designation) = hfSD_ID.Value
        ht(OASISConstants.TRANS_TYPE_Grade) = hfGrade_ID.Value
        ht(OASISConstants.TRANS_TYPE_Category) = hfCat_ID.Value
        ht(OASISConstants.TRANS_TYPE_Mode_of_pay) = ddlPayMode.SelectedValue
        ht(OASISConstants.TRANS_TYPE_Company_Transportation) = IIf(ddlEmpTransportation.SelectedValue = 0, False, True)
        ht(OASISConstants.TRANS_TYPE_Department) = hfDept_ID.Value
        ht(OASISConstants.TRANS_TYPE_Department) = hfDept_ID.Value
        ht(OASISConstants.TRANS_TYPE_Employee_Account) = txtAccCode.Text
        ht(OASISConstants.TRANS_TYPE_Employee_Bank) = hfBank_ID.Value

        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            Dim ienum As IDictionaryEnumerator = ht.GetEnumerator()
            While (ienum.MoveNext())
                cmd = New SqlCommand("SaveEMPTRANTYPE_TRN", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpEST_TTP_ID As New SqlParameter("@EST_TTP_ID", SqlDbType.Int)
                sqlpEST_TTP_ID.Value = ienum.Key
                cmd.Parameters.Add(sqlpEST_TTP_ID)

                Dim sqlpEST_BSU_ID As New SqlParameter("@EST_BSU_ID", SqlDbType.VarChar, 10)
                sqlpEST_BSU_ID.Value = Session("sbsuid")
                cmd.Parameters.Add(sqlpEST_BSU_ID)

                Dim sqlpEST_EMP_ID As New SqlParameter("@EST_EMP_ID", SqlDbType.VarChar, 15)
                sqlpEST_EMP_ID.Value = EmpID
                cmd.Parameters.Add(sqlpEST_EMP_ID)

                Dim sqlpEST_CODE As New SqlParameter("@EST_CODE", SqlDbType.VarChar, 10)
                sqlpEST_CODE.Value = ienum.Value
                cmd.Parameters.Add(sqlpEST_CODE)

                Dim sqlpEST_DTFROM As New SqlParameter("@EST_DTFROM", SqlDbType.DateTime)
                sqlpEST_DTFROM.Value = txtJdate.Text
                cmd.Parameters.Add(sqlpEST_DTFROM)

                Dim sqlpEST_DTTO As New SqlParameter("@EST_DTTO", SqlDbType.DateTime)
                sqlpEST_DTTO.Value = DBNull.Value
                cmd.Parameters.Add(sqlpEST_DTTO)

                Dim sqlpEST_REMARKS As New SqlParameter("@EST_REMARKS", SqlDbType.VarChar, 100)
                sqlpEST_REMARKS.Value = "Employee Transfered "
                cmd.Parameters.Add(sqlpEST_REMARKS)

                Dim sqlpEST_OLDCODE As New SqlParameter("@EST_OLDCODE", SqlDbType.VarChar, 10)
                sqlpEST_OLDCODE.Value = DBNull.Value
                cmd.Parameters.Add(sqlpEST_OLDCODE)

                Dim sqlpbFromMAsterForm As New SqlParameter("@bFromMAsterForm", SqlDbType.Bit)
                sqlpbFromMAsterForm.Value = True
                cmd.Parameters.Add(sqlpbFromMAsterForm)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue > 0 Then
                    Exit While
                End If
            End While

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Private Function ResetSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByVal OLD_BSU_ID As String) As Integer

        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("ResetEMPSALARY", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 15)
            sqlpEMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpuser As New SqlParameter("@user", SqlDbType.VarChar, 20)
            sqlpuser.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpuser)

            Dim sqlpOLD_BSU_ID As New SqlParameter("@OLD_BSU_ID", SqlDbType.VarChar, 10)
            sqlpOLD_BSU_ID.Value = OLD_BSU_ID
            cmd.Parameters.Add(sqlpOLD_BSU_ID)

            Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
            sqlpESH_TODT.Value = txtJdate.Text
            cmd.Parameters.Add(sqlpESH_TODT)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    'V1.4 changes
    Private Function SaveEmpProvisions(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByRef OldBsu_ID As String, ByRef TransferID As String) As Integer
        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("[GLG].[PROV_EMP_TFR]", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpEAL_EMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 15)
            sqlpEAL_EMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEAL_EMP_ID)

            Dim sqlpEAL_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 10)
            sqlpEAL_BSU_ID.Value = Session("sbsuid")
            cmd.Parameters.Add(sqlpEAL_BSU_ID)

            Dim sqlpEAL_OldBsuID As New SqlParameter("@OLDBSU_ID", SqlDbType.VarChar, 10)
            sqlpEAL_OldBsuID.Value = OldBsu_ID
            cmd.Parameters.Add(sqlpEAL_OldBsuID)

            Dim sqlpFROM_ETF_ID As New SqlParameter("@ETF_ID", SqlDbType.Int)
            sqlpFROM_ETF_ID.Value = CInt(TransferID)
            cmd.Parameters.Add(sqlpFROM_ETF_ID)

            Dim sqlpTrans_ID As New SqlParameter("@TRAN_TYPE", SqlDbType.VarChar, 10)
            If CDate(txtJdate.Text) <= Today.Date Then
                sqlpTrans_ID.Value = "NORMAL"
            Else
                sqlpTrans_ID.Value = "FUTURE_INS"
            End If

            cmd.Parameters.Add(sqlpTrans_ID)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    Private Function SaveEmployeeAllocation(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByRef NewEMPNo As String, ByRef EMP_STAFF_NO As String) As Integer

        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            'cmd = New SqlCommand("", objConn, stTrans)
            'cmd.CommandType = CommandType.Text
            'cmd.CommandText = "delete empallocation where  EAL_EMP_ID=" & EmpID & " and EAL_FROMDT='" & ViewState("OLD_J_DATE") & "'"
            'cmd.ExecuteNonQuery()

            cmd = New SqlCommand("SaveEmpTransfer", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpEAL_EMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 15)
            sqlpEAL_EMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEAL_EMP_ID)

            Dim sqlpEAL_BSU_ID As New SqlParameter("@TO_BSU_ID", SqlDbType.VarChar, 10)
            sqlpEAL_BSU_ID.Value = Session("sbsuid")
            cmd.Parameters.Add(sqlpEAL_BSU_ID)

            Dim sqlpEAL_FROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
            sqlpEAL_FROMDT.Value = txtJdate.Text
            cmd.Parameters.Add(sqlpEAL_FROMDT)

            Dim sqlpFROM_BSU_ID As New SqlParameter("@FROM_BSU_ID", SqlDbType.VarChar, 10)
            sqlpFROM_BSU_ID.Value = ViewState("FROM_BSU_ID")
            cmd.Parameters.Add(sqlpFROM_BSU_ID)

            Dim sqlpbNEw As New SqlParameter("@bNEw", SqlDbType.Bit)
            sqlpbNEw.Value = 0
            cmd.Parameters.Add(sqlpbNEw)

            Dim sqlpEAL_REMARKS As New SqlParameter("@REMARKS", SqlDbType.VarChar, 100)
            sqlpEAL_REMARKS.Value = "Employee Transfered to new business Unit"
            cmd.Parameters.Add(sqlpEAL_REMARKS)

            Dim sqlpEMPNO As New SqlParameter("@Emp_New_No", SqlDbType.VarChar, 20)
            sqlpEMPNO.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpEMPNO)

            Dim sqlpEMP_NEW_STAFFNO As New SqlParameter("@EMP_NEW_STAFFNO", SqlDbType.VarChar, 20)
            sqlpEMP_NEW_STAFFNO.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpEMP_NEW_STAFFNO)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            NewEMPNo = sqlpEMPNO.Value
            EMP_STAFF_NO = sqlpEMP_NEW_STAFFNO.Value

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    'V1.4
    Private Function UpdateEmployeeTransferMaster(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal EMP_ID As String, ByVal BSU_ID As String, ByVal GrossSalary As Double, ByVal newEMP_NO As String, ByVal NEW_EMP_STAFF_NO As String, ByVal TransferID As String) As Integer
        Dim status As Integer
        If hfAttApprov.Value = "" Then
            hfAttApprov.Value = "0"
        End If
        status = AccessRoleUser.SAVEEMPLOYEE_M_Transfer(GetVal(Session("sUsr_name")), _
        hfSD_ID.Value, hfCat_ID.Value, hfGrade_ID.Value, GetVal(Session("sBSUID")), GetVal(hfGrade_ID.Value), _
         GetVal(ddlSalC.SelectedValue), GetVal(ddlPayC.SelectedValue), GetVal(ddlPayMode.SelectedValue), _
        GetVal(hfBank_ID.Value), GetVal(txtSwiftcode.Text), GetVal(txtAccCode.Text), _
        GetVal(hfME_ID.Value), GetVal(hfML_ID.Value), _
        GetVal(GrossSalary), GetVal(hfSalGrade.Value), GetVal(hfDept_ID.Value), _
         GetVal(hfReport.Value), GetVal(hf_VacationPolicy.Value), EMP_ID, _
        GetVal(ddlEmpTransportation.SelectedValue), _
        GetVal(h_ApprovalPolicy.Value), GetVal(txtFTE.Text), txtJdate.Text, ViewState("FROM_BSU_ID").ToString, TransferID, conn, trans, hfAttApprov.Value)
        Return status


    End Function
    Private Function UpdateEmployeeMaster(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal EMP_ID As String, ByVal BSU_ID As String, ByVal GrossSalary As Double, ByVal newEMP_NO As String, ByVal NEW_EMP_STAFF_NO As String) As Integer
        Dim status As Integer
        Dim dt As New DataTable
        'Dim drSalScale As SqlDataReader
        Dim drSalScale As DataRow
        Dim str_sql As String = String.Empty
        str_sql = "select * from EMPLOYEE_M WHERE EMP_ID = '" & _
        EMP_ID & "'" '" AND EMP_BSU_ID = '" & BSU_ID & "'"
        Dim cmd1 As SqlCommand = New SqlCommand(str_sql, conn, trans)
        cmd1.CommandType = CommandType.Text
        Dim sqlAdpt As New SqlDataAdapter(cmd1)
        sqlAdpt.Fill(dt)
        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                drSalScale = dt.Rows(0)
                'Staff no. changed from int to varcharfor V1.1 
                'Bug fixing for designation Id -pass hfSD_ID instead of hfDept_ID in 3rd line below
                If drSalScale("EMP_STATUS").ToString = "8" And drSalScale("EMP_bACTIVE") = True Then
                    drSalScale("EMP_STATUS") = 1
                End If
                status = AccessRoleUser.SAVEEMPLOYEE_M(GetVal(Session("sUsr_name")), GetVal(newEMP_NO), GetVal(drSalScale("EMP_APPLNO")), _
                GetVal(drSalScale("EMP_SALUTE")), GetVal(drSalScale("EMP_FNAME")), GetVal(drSalScale("EMP_MNAME")), _
                GetVal(drSalScale("EMP_LNAME")), hfSD_ID.Value, hfCat_ID.Value, _
                hfGrade_ID.Value, GetVal(Session("sBSUID")), GetVal(hfGrade_ID.Value), _
                GetVal(drSalScale("EMP_VISATYPE")), GetVal(drSalScale("EMP_VISASTATUS")), _
                GetVal(drSalScale("EMP_JOINDT")), GetVal(CDate(txtJdate.Text)), GetVal(drSalScale("EMP_LASTVACFROM")), _
                GetVal(drSalScale("EMP_LASTVACTO")), GetVal(drSalScale("EMP_LASTREJOINDT")), GetVal(drSalScale("EMP_bOVERSEAS")), _
                GetVal(drSalScale("EMP_STATUS")), GetVal(drSalScale("EMP_RESGDT")), GetVal(drSalScale("EMP_LASTATTDT")), _
                GetVal(drSalScale("EMP_PROBTILL")), GetVal(drSalScale("EMP_ACCOMODATION")), GetVal(drSalScale("EMP_bACTIVE")), _
                GetVal(ddlSalC.SelectedValue), GetVal(ddlPayC.SelectedValue), GetVal(ddlPayMode.SelectedValue), _
                GetVal(hfBank_ID.Value), GetVal(txtAccCode.Text), GetVal(txtSwiftcode.Text), _
                GetVal(hfME_ID.Value), GetVal(hfML_ID.Value), GetVal(drSalScale("EMP_VISA_BSU_ID")), GetVal(drSalScale("EMP_MARITALSTATUS")), _
                GetVal(GrossSalary), GetVal(drSalScale("EMP_PASSPORT")), GetVal(drSalScale("EMP_REMARKS")), GetVal(drSalScale("EMP_CTY_ID")), _
                GetVal(drSalScale("EMP_SEX_bMALE")), GetVal(drSalScale("EMP_QLF_ID")), GetVal(hfSalGrade.Value), GetVal(hfDept_ID.Value), _
                GetVal(drSalScale("EMP_BLOODGRP")), GetVal(drSalScale("EMP_FATHER")), GetVal(drSalScale("EMP_MOTHER")), _
                GetVal(hfReport.Value), GetVal(hf_VacationPolicy.Value), GetVal(NEW_EMP_STAFF_NO, SqlDbType.VarChar), _
                GetVal(drSalScale("EMP_ABC")), GetVal(drSalScale("EMP_bOT"), SqlDbType.Bit), EMP_ID, _
                GetVal(drSalScale("EMp_bTEmp"), SqlDbType.Bit), GetVal(drSalScale("EMP_bCompanyTransport"), SqlDbType.Bit), GetVal(CDate(Replace(drSalScale("EMP_DOB"), "#", "'")), SqlDbType.DateTime), _
                GetVal(drSalScale("EMP_RLG_ID")), GetVal(h_ApprovalPolicy.Value), GetVal(drSalScale("EMP_CONTRACTTYPE"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_MAXCHILDCONESSION"), SqlDbType.Int), GetVal(drSalScale("EMP_LOP_OPN"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_FTE"), SqlDbType.Decimal), GetVal(drSalScale("EMP_bReqPunching"), SqlDbType.Bit), _
                GetVal(drSalScale("EMP_TICKETFLAG"), SqlDbType.Int), GetVal(drSalScale("EMP_TICKETCLASS")), GetVal(drSalScale("EMP_TICKETCOUNT"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_TICKET_CIT_ID")), GetVal(drSalScale("EMP_VISA_ID")), GetVal(drSalScale("EMP_PERSON_ID")), True, newEMP_NO, EMP_ID, GetVal(drSalScale("EMP_PASSPORTNAME")), conn, trans)
                Return status
            End If
        End If
        Return -1
    End Function

    Private Function GetVal(ByVal obj As Object, Optional ByVal datatype As SqlDbType = SqlDbType.VarChar) As Object
        If obj Is DBNull.Value Then
            Select Case datatype
                Case SqlDbType.Decimal
                    Return 0
                Case SqlDbType.Int
                    Return 0
                Case SqlDbType.Bit
                    Return False
                Case SqlDbType.VarChar
                    Return ""
            End Select
        Else
            Return obj
        End If
        Return ""
    End Function

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalSet.Click
        MVEMPTRANSFER.ActiveViewIndex = 1
        'vwOtherDetails.Visible = False
        'vwSalarydetails.Visible = True
    End Sub
    Protected Sub btnSalBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalBack.Click
        lblError.Text = ""
        MVEMPTRANSFER.ActiveViewIndex = 0

    End Sub

    Private Function FillEmployeeDatas(ByVal EMP_ID As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "select * from dbo.vw_OSO_EMPLOYEEMASTER WHERE EMP_ID ='" & _
            EMP_ID & "'" ' AND EMP_BSU_ID = '" & BSU_ID & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (dr.Read())
                txtEmpName.Text = dr("EMPNAME").ToString
                txtML.Text = dr("EMPVISA_DESCR").ToString
                hfML_ID.Value = dr("EMP_VISA_DES_ID").ToString
                txtME.Text = dr("EMPMOE_DESCR").ToString
                hfME_ID.Value = dr("EMP_MOE_DES_ID").ToString
                txtGrade.Text = dr("TEACH_GRADE_DESCR").ToString
                hfGrade_ID.Value = dr("EMP_TGD_ID").ToString
                txtReport.Text = dr("EMP_REP_TO_Name").ToString
                hfReport.Value = dr("EMP_REPORTTO_EMP_ID").ToString
                txtVacationPolicy.Text = dr("BLS_DESCRIPTION").ToString
                hf_VacationPolicy.Value = dr("EMP_BLS_ID").ToString
                txtSD.Text = dr("EMP_DES_DESCR").ToString
                hfSD_ID.Value = dr("EMP_DES_ID").ToString
                txtCat.Text = dr("CATEGORY_DESC").ToString
                hfCat_ID.Value = dr("EMP_ECT_ID").ToString
                txtDept.Text = dr("EMP_DEPT_DESCR").ToString
                'hfDept_ID.Value = dr("EMP_DES_ID").ToString
                hfDept_ID.Value = dr("EMP_DPT_ID").ToString  'Correction in V1.1
                txtEmpApprovalPol.Text = dr("APPROVAL_POLICY").ToString
                h_ApprovalPolicy.Value = dr("EMP_LPS_ID").ToString
                hfEmployeeNum.Value = dr("EMPNO").ToString 'V1.3
                'Salary Details
                ' hfSalGrade.Value = dr("EMP_SCALE_GRADE_DESCR").ToString hfSalGrade    V1.1- To pass Sclae Grade ID and not description
                hfSalGrade.Value = dr("EMP_SGD_ID").ToString                           'V1.1
                txtSalGrade.Text = dr("EMP_SCALE_GRADE_DESCR").ToString
                ddlSalC.SelectedValue = dr("EMP_CUR_ID").ToString
                ddlPayMode.SelectedValue = dr("EMP_MODE").ToString
                txtBname.Text = dr("BANK_DESCR").ToString
                hfBank_ID.Value = dr("EMP_BANK").ToString
                txtAccCode.Text = dr("EMP_ACCOUNT").ToString
                txtSwiftcode.Text = dr("EMP_SWIFTCODE").ToString
                ddlEmpTransportation.SelectedValue = dr("EMP_bCompanyTransport").ToString
                ddlPayC.SelectedValue = dr("EMP_PAY_CUR_ID").ToString
                txtFTE.Text = dr("EMP_FTE").ToString
                hfAttApprov.Value = dr("EMP_ATT_APPROV_EMP_ID").ToString   ' swapna added
                txtAttApprov.Text = dr("EMPattendanceapprover").ToString
                ViewState("LastUnitJoinDate") = Format(dr("EMP_BSU_JOINDT"), OASISConstants.DateFormat).ToString
            End While
            dr.Close()
            str_Sql = "SELECT EMPTRANSFER.ETF_FROM_BSU_ID, EMPTRANSFER.ETF_FROMDT, " & _
            "BUSINESSUNIT_M.BSU_NAME FROM EMPTRANSFER LEFT OUTER JOIN " & _
            "BUSINESSUNIT_M ON EMPTRANSFER.ETF_FROM_BSU_ID = BUSINESSUNIT_M.BSU_ID " & _
            "WHERE ETF_TO_bAccept = 0  and ETF_EMP_ID ='" & EMP_ID & "'"
            Dim drTrans As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (drTrans.Read())
                txtJdate.Text = Format(drTrans("ETF_FROMDT"), OASISConstants.DateFormat)
                txtFromBSUName.Text = drTrans("BSU_NAME").ToString
                ViewState("FROM_BSU_ID") = drTrans("ETF_FROM_BSU_ID").ToString
            End While
            drTrans.Close()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Function


#Region "Salary Details"

    Private Sub BindENR()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 1 order by ERN_ORDER "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataBind()
        dr.Close()
    End Sub

    Private Sub BindSalaryPayCurrency()

        Dim itemTypeCounter As Integer
        Dim BCur_ID As String = String.Empty
        Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnitsByCurrency(Session("sBSUID"))
            If BUnitreaderSuper.HasRows = True Then
                While BUnitreaderSuper.Read
                    BCur_ID = Convert.ToString(BUnitreaderSuper("BSU_CURRENCY"))
                End While
            End If
        End Using

        Using CUnitreaderSuper As SqlDataReader = AccessRoleUser.GetCurrency()
            'create a list item to bind records from reader to dropdownlist ddlBunit
            Dim di As ListItem
            ddlSalC.Items.Clear()
            'check if it return rows or not
            If CUnitreaderSuper.HasRows = True Then
                While CUnitreaderSuper.Read
                    di = New ListItem(CUnitreaderSuper("CUR_DESCR"), CUnitreaderSuper("CUR_ID"))
                    'adding listitems into the dropdownlist
                    ddlSalC.Items.Add(di)
                    ddlPayC.Items.Add(di)

                    For itemTypeCounter = 0 To ddlSalC.Items.Count - 1
                        'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                        If ddlSalC.Items(itemTypeCounter).Value = BCur_ID Then
                            ddlSalC.SelectedIndex = itemTypeCounter
                            ddlPayC.SelectedIndex = itemTypeCounter
                        End If
                    Next
                End While
            End If
        End Using
    End Sub

    Private Function CreateSalaryDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cERN_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY As New DataColumn("Amount_ELIGIBILITY", System.Type.GetType("System.Decimal"))
            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cPAYTERM As New DataColumn("PAYTERM", System.Type.GetType("System.Int16"))
            Dim cPAY_SCHEDULE As New DataColumn("PAY_SCHEDULE", System.Type.GetType("System.String")) 'V1.1
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cERN_DESCR)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cAmount_ELIGIBILITY)
            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cPAYTERM)
            dtDt.Columns.Add(cPAY_SCHEDULE) 'V1.1
            dtDt.Columns.Add(cStatus)

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnSalAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalAdd.Click
        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryDetailsTable()
        End If
        If Not IsNumeric(txtSalAmount.Text) Then
            lblError.Text = "Amount should be valid"
            Exit Sub
        End If
        Dim GrossSal As Double = 0
        'If txtGsalary.Text <> "" Then
        '    If Not Session("EMPSALGrossSalary") Is Nothing Then
        '        GrossSal = CDbl(Session("EMPSALGrossSalary"))
        '    End If
        '    If GrossSal + CDbl(txtSalAmount.Text) > CDbl(txtGsalary.Text) Then
        '        lblError.Text = "Total Amount should be less than Gross Salary"
        '        Exit Sub
        '    End If
        'End If
        Dim status As String = String.Empty
        Dim dtSalDetails As DataTable = Session("EMPSALDETAILS")
        Dim i As Integer
        'Dim gvRow As GridViewRow = gvEmpSalary.SelectedRow()

        For i = 0 To dtSalDetails.Rows.Count - 1
            If Session("EMPSalEditID") Is Nothing Or _
            Session("EMPSalEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                'If Session("EMPSalEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                If dtSalDetails.Rows(i)("STATUS").ToString.ToUpper <> "DELETED" _
                And dtSalDetails.Rows(i)("ENR_ID") = ddSalEarnCode.SelectedItem.Value Then
                    lblError.Text = "Cannot add transaction details.The Earn Code details are repeating."
                    GridBindSalaryDetails()
                    Exit Sub
                End If
            End If
        Next
        If btnSalAdd.Text = "Add" Then
            Dim newDR As DataRow = dtSalDetails.NewRow()
            newDR("UniqueID") = dtSalDetails.Rows.Count()
            newDR("ENR_ID") = ddSalEarnCode.SelectedValue
            newDR("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
            newDR("Amount") = AccountFunctions.Round(txtSalAmount.Text)
            newDR("Amount_ELIGIBILITY") = AccountFunctions.Round(txtSalAmtEligible.Text)
            newDR("bMonthly") = chkSalPayMonthly.Checked
            newDR("PAYTERM") = CInt(ddlayInstallment.SelectedValue)
            'newDR("SCHEDULE") = ddlayInstallment.SelectedItem.Text.ToString
            newDR("Status") = "INSERT"
            status = "Insert"
            dtSalDetails.Rows.Add(newDR)
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            Dim amt As Double = CDbl(IIf(chkSalPayMonthly.Checked, txtSalAmount.Text * 12, txtSalAmount.Text))
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), amt)
        ElseIf btnSalAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPSalEditID")
            For iIndex = 0 To dtSalDetails.Rows.Count - 1
                If str_Search = dtSalDetails.Rows(iIndex)("UniqueID") And dtSalDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtSalDetails.Rows(iIndex)("ENR_ID") = ddSalEarnCode.SelectedValue
                    dtSalDetails.Rows(iIndex)("ERN_DESCR") = ddSalEarnCode.Text
                    dtSalDetails.Rows(iIndex)("Amount") = txtSalAmount.Text
                    dtSalDetails.Rows(iIndex)("Amount_ELIGIBILITY") = txtSalAmtEligible.Text
                    dtSalDetails.Rows(iIndex)("bMonthly") = chkSalPayMonthly.Checked
                    dtSalDetails.Rows(iIndex)("PAYTERM") = CInt(ddlayInstallment.SelectedValue)
                    dtSalDetails.Rows(iIndex)("status") = "edited"
                    status = "Edit/Update"
                    Exit For
                End If
            Next
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            Dim amt As Double = CDbl(IIf(chkSalPayMonthly.Checked, txtSalAmount.Text * 12, txtSalAmount.Text))
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), amt)
        End If
        If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpName.Text, status, Page.User.Identity.Name.ToString, vwSalarydetails) = 0 Then
            ClearEMPSalDetails()
        Else
            UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        End If

        Session("EMPSALDETAILS") = dtSalDetails
        GridBindSalaryDetails()
    End Sub

    Private Sub GridBindSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPSALDETAILS") Is Nothing Then Return
        If Session("EMPSALDETAILS") Is Nothing Then
            gvEmpSalary.DataSource = Nothing
            gvEmpSalary.DataBind()
            Return
        End If

        Dim i As Integer
        Dim grossSal As Double
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateSalaryDetailsTable()
        If Session("EMPSALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPSALDETAILS").Rows.Count - 1
                If (Session("EMPSALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPSALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("EMPSALDETAILS").Rows(i)(strColumnName)
                            If Not Session("EMPSALDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("EMPSALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        Session("EMPSALGrossSalary") = grossSal
        gvEmpSalary.DataSource = dtTempDtl
        gvEmpSalary.DataBind()
        txtGsalary.Text = Session("EMPSALGrossSalary")
    End Sub

    Private Sub DeleteRowSalDetails(ByVal pId As String)

        Dim lblTid As New Label
        lblTid = TryCast(gvEmpSalary.Rows(pId).FindControl("lblId"), Label)

        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub

        Dim iRemove As Integer = 0
        For iRemove = 0 To Session("EMPSALDETAILS").Rows.Count - 1
            If (Session("EMPSALDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPSALDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpName.Text, "Delete", Page.User.Identity.Name.ToString, vwSalarydetails) = 0 Then
                    GridBindSalaryDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Salary Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnSalCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalCancel.Click
        ClearEMPSalDetails()
    End Sub

    Sub ClearEMPSalDetails()
        ddSalEarnCode.SelectedIndex = 0
        txtSalAmount.Text = ""
        txtSalAmtEligible.Text = ""
        btnSalAdd.Text = "Add"
        lblError.Text = ""
        chkSalPayMonthly.Checked = True
        ddlayInstallment.SelectedValue = 0
        Session.Remove("EMPSalEditID")
    End Sub

    Private Sub GetPayMonth_YearDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT BSU_PAYMONTH, BSU_PAYYEAR ,BSU_CURRENCY FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            ViewState("BSU_PAYMONTH") = dr("BSU_PAYMONTH")
            ViewState("BSU_PAYYEAR") = dr("BSU_PAYYEAR")
            ViewState("PAY_CUR_ID") = dr("BSU_CURRENCY")

        End While
        dr.Close()
    End Sub

    Protected Sub lnkSalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1
            If str_Search = Session("EMPSALDETAILS").Rows(iIndex)("UniqueID") And Session("EMPSALDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                ddSalEarnCode.SelectedIndex = -1
                ddSalEarnCode.Items.FindByValue(Session("EMPSALDETAILS").Rows(iIndex)("ENR_ID")).Selected = True
                txtSalAmount.Text = Session("EMPSALDETAILS").Rows(iIndex)("Amount")
                txtSalAmtEligible.Text = Session("EMPSALDETAILS").Rows(iIndex)("Amount_ELIGIBILITY")
                chkSalPayMonthly.Checked = Session("EMPSALDETAILS").Rows(iIndex)("bMonthly")
                ddlayInstallment.SelectedIndex = -1
                ddlayInstallment.Items.FindByValue(Session("EMPSALDETAILS").Rows(iIndex)("PAYTERM")).Selected = True

                Session("EMPSALGrossSalary") -= CDbl(txtSalAmount.Text)
                gvEmpSalary.SelectedIndex = iIndex
                btnSalAdd.Text = "Save"
                Session("EMPSalEditID") = str_Search
                UtilityObj.beforeLoopingControls(vwSalarydetails)
                Exit For
            End If
        Next
    End Sub

    Private Function SaveEMPSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPSALDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            Dim IsBasicAdded As Boolean = 0
            If Session("EMPSALDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1

                    Dim dr As DataRow = Session("EMPSALDETAILS").Rows(iIndex)

                    'If (Session("EMPSALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                    ' If dr("status") <> "LOADED" Then
                    cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                    sqlpESH_EMP_ID.Value = EmpID 'EmployeeID
                    cmd.Parameters.Add(sqlpESH_EMP_ID)

                    If dr("status") = "edited" Or dr("status") = "DELETED" Then
                        Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.Int)
                        sqlpESH_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpESH_ID)
                    End If

                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                    sqlpUser.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpUser)

                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                    sqlpESH_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpESH_BSU_ID)

                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                    sqlpESH_ERN_ID.Value = dr("ENR_ID")
                    cmd.Parameters.Add(sqlpESH_ERN_ID)



                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                    sqlpESH_AMOUNT.Value = dr("Amount")
                    cmd.Parameters.Add(sqlpESH_AMOUNT)

                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                    sqlpESH_ELIGIBILITY.Value = dr("Amount_ELIGIBILITY")
                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                    sqlpESH_bMonthly.Value = dr("bMonthly")
                    cmd.Parameters.Add(sqlpESH_bMonthly)

                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                    sqlpESH_PAYTERM.Value = dr("PAYTERM")
                    cmd.Parameters.Add(sqlpESH_PAYTERM)

                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_CUR_ID.Value = ViewState("PAY_CUR_ID")
                    cmd.Parameters.Add(sqlpESH_CUR_ID)

                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                    sqlpESH_PAYMONTH.Value = ViewState("BSU_PAYMONTH")
                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                    sqlpESH_PAYYEAR.Value = ViewState("BSU_PAYYEAR")
                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_PAY_CUR_ID.Value = ddlPayC.SelectedItem.Value
                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                    sqlpESH_FROMDT.Value = CDate(txtJdate.Text)
                    cmd.Parameters.Add(sqlpESH_FROMDT)

                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                    sqlpESH_TODT.Value = DBNull.Value
                    cmd.Parameters.Add(sqlpESH_TODT)

                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)

                    If (Session("EMPSALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                        sqlpbDeleted.Value = 0
                    Else
                        sqlpbDeleted.Value = 1
                    End If
                    cmd.Parameters.Add(sqlpbDeleted)

                    Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                    retNewESH_ID.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(retNewESH_ID)

                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retSValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retSValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retSValParam.Value
                    If iReturnvalue <> 0 Then
                        Exit For
                    Else
                        'Set the UniqueID to the ESH_ID that is returned....
                        If Not Session("EMPSALSCHEDULE") Is Nothing Then
                            If Session("EMPSALSCHEDULE").Rows.Count > 0 Then
                                For index As Integer = 0 To Session("EMPSALSCHEDULE").Rows.Count - 1
                                    Dim drow As DataRow = Session("EMPSALSCHEDULE").Rows(index)
                                    If drow("ENR_ID") = dr("ENR_ID") Then
                                        drow("ESH_ID") = retNewESH_ID.Value
                                    End If
                                Next
                            End If
                        End If

                    End If
                    'End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function
    'V1.4
    Private Function SaveEMPTransferSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByVal TransferID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPSALDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            Dim IsBasicAdded As Boolean = 0
            If Session("EMPSALDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1

                    Dim dr As DataRow = Session("EMPSALDETAILS").Rows(iIndex)

                    'If (Session("EMPSALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                    ' If dr("status") <> "LOADED" Then
                    '   cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                    cmd = New SqlCommand("SaveEMPSALARYHS_S_transfer", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                    sqlpESH_EMP_ID.Value = EmpID 'EmployeeID
                    cmd.Parameters.Add(sqlpESH_EMP_ID)

                    If dr("status") = "edited" Or dr("status") = "DELETED" Then
                        Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.Int)
                        sqlpESH_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpESH_ID)
                    End If

                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                    sqlpUser.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpUser)

                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                    sqlpESH_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpESH_BSU_ID)

                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                    sqlpESH_ERN_ID.Value = dr("ENR_ID")
                    cmd.Parameters.Add(sqlpESH_ERN_ID)



                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                    sqlpESH_AMOUNT.Value = dr("Amount")
                    cmd.Parameters.Add(sqlpESH_AMOUNT)

                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                    sqlpESH_ELIGIBILITY.Value = dr("Amount_ELIGIBILITY")
                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                    sqlpESH_bMonthly.Value = dr("bMonthly")
                    cmd.Parameters.Add(sqlpESH_bMonthly)

                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                    sqlpESH_PAYTERM.Value = dr("PAYTERM")
                    cmd.Parameters.Add(sqlpESH_PAYTERM)

                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_CUR_ID.Value = ViewState("PAY_CUR_ID")
                    cmd.Parameters.Add(sqlpESH_CUR_ID)

                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                    sqlpESH_PAYMONTH.Value = ViewState("BSU_PAYMONTH")
                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                    sqlpESH_PAYYEAR.Value = ViewState("BSU_PAYYEAR")
                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_PAY_CUR_ID.Value = ddlPayC.SelectedItem.Value
                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                    sqlpESH_FROMDT.Value = CDate(txtJdate.Text)
                    cmd.Parameters.Add(sqlpESH_FROMDT)

                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                    sqlpESH_TODT.Value = DBNull.Value
                    cmd.Parameters.Add(sqlpESH_TODT)

                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)

                    If (Session("EMPSALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                        sqlpbDeleted.Value = 0
                    Else
                        sqlpbDeleted.Value = 1
                    End If
                    cmd.Parameters.Add(sqlpbDeleted)

                    Dim retTRF_ID As New SqlParameter("@ESHT_TRF_ID", SqlDbType.Int)
                    retTRF_ID.Value = CInt(TransferID)
                    cmd.Parameters.Add(retTRF_ID)

                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retSValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retSValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retSValParam.Value
                    If iReturnvalue <> 0 Then
                        Exit For
                    Else
                        'Set the UniqueID to the ESH_ID that is returned....
                        If Not Session("EMPSALSCHEDULE") Is Nothing Then
                            If Session("EMPSALSCHEDULE").Rows.Count > 0 Then
                                For index As Integer = 0 To Session("EMPSALSCHEDULE").Rows.Count - 1
                                    Dim drow As DataRow = Session("EMPSALSCHEDULE").Rows(index)
                                    If drow("ENR_ID") = dr("ENR_ID") Then
                                        drow("ESH_ID") = 0
                                    End If
                                Next
                            End If
                        End If

                    End If
                    'End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function
    Protected Sub gvEmpSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpSalary.RowDataBound
        Try
            Dim cmdCol As Integer = gvEmpSalary.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblid As New Label
            Dim lblamt As New Label
            Dim lblENRID As New Label
            Dim lblSchedule As New Label
            Dim lblPayTerm As New Label
            Dim lblBMonthly As New Label
            lblid = e.Row.FindControl("lblId")
            lblBMonthly = e.Row.FindControl("lblBMonthly")
            lblSchedule = e.Row.FindControl("lblSchedule")

            ' V1.1 starts

            'lblamt = AccountFunctions.Round(e.Row.FindControl("lblAmt"))
            lblamt = e.Row.FindControl("lblAmt")
            'V1.1 ends
            lblENRID = e.Row.FindControl("lblENRID")
            lblPayTerm = e.Row.FindControl("lblPayTerm")
            If lblSchedule IsNot Nothing Then
                lblSchedule.Text = GETPAY_SCHEDULE(lblPayTerm.Text)
                'V1.1 starts
                'Select Case lblPayTerm.Text
                '    Case 0
                '        lblSchedule.Text = "Monthly"
                '    Case 1
                '        lblSchedule.Text = "Bimonthly"
                '    Case 2
                '        lblSchedule.Text = "Quarterly"
                '    Case 3
                '        lblSchedule.Text = "Half-yearly"
                '    Case 4
                '        lblSchedule.Text = "Yearly"
                'End Select
                'V1.1 ends

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvEmpSalary_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEmpSalary.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowSalDetails(categoryID)
    End Sub

    Protected Sub btnSalScale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalScale.Click
        'Dim dtable As DataTable = CreateSalaryDetailsTable()
        If hfSalGrade.Value = "" Then Return
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal
        Dim fteVal As Double = 1
        If txtFTE.Text = "" Or Not IsNumeric(txtFTE.Text) Then
            fteVal = 1
        ElseIf CDbl(txtFTE.Text) > 1 Or CDbl(txtFTE.Text) < 0 Then
            fteVal = 1
        Else
            fteVal = CDbl(txtFTE.Text)
        End If
        str_Sql = "Select EMPGRADES_M.EGD_ID FROM EMPGRADES_M RIGHT OUTER JOIN" & _
        " EMPSCALES_GRADES_M ON EMPGRADES_M.EGD_ID = EMPSCALES_GRADES_M.SGD_EGD_ID " & _
        " WHERE EMPSCALES_GRADES_M.SGD_ID =" & hfSalGrade.Value
        Dim objGrade As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not objGrade Is DBNull.Value Then
            hfGrade_ID.Value = objGrade.ToString
        End If
        str_Sql = "select EMPGRADE_SALARY_S.*, ERN_DESCR from EMPGRADE_SALARY_S " & _
        " EMPGRADE_SALARY_S LEFT OUTER JOIN EMPSALCOMPO_M ON EMPGRADE_SALARY_S.GSS_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        "WHERE GSS_SGD_ID =" & hfSalGrade.Value & " and GSS_DTTO is null "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'If Session("EMPSALSCHEDULE") Is Nothing Then
        '    Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        'End If
        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryDetailsTable()
        End If

        Dim dTemptable As DataTable = Session("EMPSALDETAILS")
        For index As Integer = 0 To dTemptable.Rows.Count - 1
            dTemptable.Rows(index)("Status") = "DELETED"
        Next
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            grossSal += dr("GSS_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("ENR_ID") = dr("GSS_ERN_ID")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = True
            ldrTempNew.Item("PAYTERM") = 0
            ldrTempNew.Item("Amount") = dr("GSS_AMOUNT") * fteVal
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("GSS_AMOUNT")
            ldrTempNew.Item("Status") = "EDITED"
            dTemptable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPSALDETAILS") = dTemptable
        GridBindSalaryDetails()
    End Sub

    Private Function GetNextESHID() As Integer
        If ViewState("ESH_ID") Is Nothing Then
            Dim cmd As New SqlCommand
            cmd.Connection = ConnectionManger.GetOASISConnection()
            cmd.CommandText = "select isnull(max(ESH_ID),0) from EMPSALARYHS_s "
            Dim GSS_ID As Integer = CInt(cmd.ExecuteScalar())
            ViewState("ESH_ID") = GSS_ID
        Else
            ViewState("ESH_ID") += 1
        End If
        Return ViewState("ESH_ID")
    End Function

#End Region

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel1.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub
    Protected Sub ddlPayMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayMode.SelectedIndexChanged
        If ddlPayMode.SelectedValue = "0" Then
            txtBname.Text = ""
            txtBname.Enabled = False
            btnBank_name.Enabled = False
            txtAccCode.Text = ""
            txtAccCode.Enabled = False
        Else
            txtBname.Enabled = True
            btnBank_name.Enabled = True
            txtAccCode.Enabled = True

        End If
    End Sub
End Class
