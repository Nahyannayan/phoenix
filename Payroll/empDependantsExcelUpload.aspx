﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empDependantsExcelUpload.aspx.vb"
    MasterPageFile="~/mainMasterPage.master" Inherits="Payroll_empDependantsExcelUpload" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="cphMasterpage" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function CheckAllRows(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox' && (document.forms[0].elements[i].name.search(/chkPay/) > 0)) {
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }

        function CheckAllRowsOutside(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox' && (document.forms[0].elements[i].name.search(/chkOutside/) > 0)) {
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Dependent Details Upload
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Upload File</span> </td>
                        <td align="left" width="30%">
                            <asp:FileUpload ID="flUpExcel" runat="server" />
                        </td>
                        <td align="left" width="50%">
                            <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvDependancedetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Transaction details added yet." Width="100%" AllowPaging="True" DataKeyNames="EDD_RELATION,EDD_GEMS_TYPE,EDD_BSU_ID,EDD_GEMS_ID,EDD_Insu_id,EMP_ID,EDD_CTY_ID">
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkPay_Header" runat="server"
                                                OnClick="CheckAllRows(this);" />Sl No. 
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:CheckBox ID="chkPay" runat="server" />
                                            <br />
                                            <asp:Label ID="lblSlno" runat="server" Text='<%# bind("UniqueID") %>'> </asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpname" runat="server" Text='<%# bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="EMPNO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNO" runat="server" Text='<%# bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Dependent Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_NAME" runat="server" Text='<%# bind("EDD_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Relation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_RELATIONDESCR" runat="server" Text='<%# bind("EDD_RELATIONDESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Dep. DOB">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_DOB" runat="server" Text='<%# Eval("EDD_DOB","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gender">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_bMale" runat="server" Text='<%# bind("EDD_bMale") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nationality">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_Nationality" runat="server" Text='<%# bind("EDD_Nationality") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Marital Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsMarried" runat="server" Text='<%# IIF(Eval("EDD_bMaritalStatus")="M","Married","Single") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="With GEMS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_GEMS_TYPE_DESCR" runat="server" Text='<%# bind("EDD_GEMS_TYPE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="GEMS Unit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_BSU_NAME" runat="server" Text='<%# bind("EDD_BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gems ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_GEMS_DisplayID" runat="server" Text='<%# bind("EDD_GEMS_DisplayID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Insurance Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsuranceCategoryDESC" runat="server" Text='<%# bind("InsuranceCategoryDESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Comp. Paid Insu">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_bCompanyInsurance" runat="server" Text='<%# bind("EDD_bCompanyInsurance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Dependant Photo">
                                        <ItemTemplate>
                                            <asp:FileUpload ID="flDepImage" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Emirates ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_EMIRATES_ID" runat="server" Text='<%# bind("EDD_EMIRATES_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Emirates ID Exp.Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_EID_EXPDT" runat="server" Text='<%# Eval("EDD_EID_EXPDT","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:Button runat="server" ID="btnSaveToDB" CssClass="button" Text="Save" Visible="False" />

                            <br />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
