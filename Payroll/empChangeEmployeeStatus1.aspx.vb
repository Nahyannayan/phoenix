Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports PayrollFunctions
Partial Class Payroll_empChangeEmployeeStatus

    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
           
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P450025" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            Using SUnitreaderSuper As SqlDataReader = AccessRoleUser.GetempStatus()

                Dim di As ListItem
                ddEstCode.Items.Clear()

                If SUnitreaderSuper.HasRows = True Then
                    While SUnitreaderSuper.Read
                        If SUnitreaderSuper("EST_ID") <> "8" Then
                            di = New ListItem(SUnitreaderSuper("EST_DESCR"), SUnitreaderSuper("EST_ID"))
                            ddEstCode.Items.Add(di)
                        End If

                    End While
                End If
            End Using

            If Request.QueryString("viewid") <> "" Then
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
                txtTodate.Text = ""
                txtTodate.Enabled = False
                imgToDate.Enabled = False
                ' lblToDate.Visible = False
                lblperiod.Text = "From date"
                lblToDate.Text = "To Date"
            End If
        End If
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String


            str_Sql = "SELECT EST.EST_DTFROM, " _
                & " EST.EST_EMP_ID, EST.EST_DTTO, ES.EST_DESCR, " _
                & " EST.EST_CODE, EST.EST_REMARKS, EM.EMPNO, " _
                & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+ " _
                & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
                & " FROM EMPTRANTYPE_TRN AS EST INNER JOIN" _
                & " EMPSTATUS_M AS ES ON EST.EST_CODE = ES.EST_ID INNER JOIN" _
                & " EMPLOYEE_M AS EM ON EST.EST_EMP_ID = EM.EMP_ID LEFT OUTER JOIN" _
                & " EMPTRANTYPE_M AS TTM ON EST.EST_TTP_ID = TTM.TTP_ID" _
                & " WHERE (EST.EST_TTP_ID = 4) AND (EST.EST_BSU_ID = '" & Session("sBsuid") & "')" _
                & "  and est.est_id='" & p_Modifyid & "'"
            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'ddLeavetypeMonthly.DataBind()
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EST_EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                ddEstCode.SelectedIndex = -1
                ddEstCode.Items.FindByValue(ds.Tables(0).Rows(0)("EST_CODE").ToString).Selected = True
                txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("EST_DTFROM")), "dd/MMM/yyyy")
                If IsDBNull(ds.Tables(0).Rows(0)("EST_DTTO")) = False Then
                    txtTodate.Visible = True
                    imgToDate.Visible = True
                    lblToDate.Visible = True
                    txtTodate.Text = Format(CDate(ds.Tables(0).Rows(0)("EST_DTTO")), "dd/MMM/yyyy")
                End If
                txtRemarks.Text = ds.Tables(0).Rows(0)("EST_REMARKS").ToString
                SetVisible(ddEstCode.SelectedItem.Text)
            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        'txtDpt.Attributes.Add("readonly", "readonly")
        'txtCat.Attributes.Add("readonly", "readonly")
        'txtGrade.Attributes.Add("readonly", "readonly")
        'txtNDpt.Attributes.Add("readonly", "readonly")
        'txtNCat.Attributes.Add("readonly", "readonly")

        txtRemarks.Attributes.Add("readonly", "readonly")
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
        imgEmployee.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
    End Sub

    Sub clear_All()
        txtFrom.Text = ""
        txtRemarks.Text = ""
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        h_Cat.Value = ""
        h_Dpt.Value = ""
        'txtDpt.Text = ""
        'txtCat.Text = ""
        'txtGrade.Text = ""
        'txtNGrade.Text = ""
        'txtNDpt.Text = ""
        'txtNCat.Text = ""

        bindgrid()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            'ViewState("EDVISA_ID") 
            'ViewState("EDMOE_ID")  
            '    ViewState("EDEMP_ID") 
            Dim retval As String = "1000"
            Dim str_update_master As String = ""

           
            retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "4", ddEstCode.SelectedItem.Value, _
              txtFrom.Text, txtTodate.Text, txtRemarks.Text.ToString, "", False, stTrans)

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value & "-" & _
            ddEstCode.SelectedItem.Value & "-" & txtFrom.Text & "-" & txtRemarks.Text.ToString, _
                    "Edit", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

            If str_update_master = "" Then
                'str_update_master = " EMP_ECT_ID='" & ddEstCode.SelectedItem.Value & "'" 'h_Cat.Value & "' "
                str_update_master = " EMP_STATUS='" & ddEstCode.SelectedItem.Value & "'" 'h_Cat.Value & "' "
            Else
                str_update_master = str_update_master & ", EMP_STATUS='" & ddEstCode.SelectedItem.Text & "' "
            End If

            'End If
            If retval = "0" Then
                'Dim myCommand As New SqlCommand("UPDATE  EMPLOYEE_M SET " & str_update_master & "WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                'Dim myCommand As New SqlCommand("UPDATE  EMPLOYEE_M SET " & str_update_master & "WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                'myCommand.ExecuteNonQuery()
            End If
            'swapna added for EMP-isFINALchecked flag setting on resignation/termination
            If retval = "0" And (ddEstCode.SelectedValue = 4 Or ddEstCode.SelectedValue = 5) Then
                If chkFinalSettled.Visible = True And chkFinalSettled.Checked = True Then
                    Dim myCommand As New SqlCommand("UPDATE  EMPLOYEE_M SET EMP_IsFinalSettled=1 WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                    myCommand.ExecuteNonQuery()
                End If

            End If

            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Sub setEditdata()
        'txtCategory.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")

        'imgCalendar.Enabled = True

        imgEmployee.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Protected Sub imgEmployee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        bindgrid()
    End Sub

    Sub bindgrid()
        Dim ds As New DataSet
        Dim str_Sql As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        str_Sql = "GetDepCatGrade  '" & h_Emp_No.Value & "','" & Session("sBsuid") & "'"
        str_Sql = "SELECT  EST.EST_DTFROM, " _
        & " EST.EST_DTTO, ES.EST_DESCR," _
        & " EST.EST_CODE, EST.EST_REMARKS, EST.EST_OLDCODE" _
        & " FROM EMPTRANTYPE_TRN AS EST INNER JOIN" _
        & " EMPSTATUS_M AS ES ON EST.EST_CODE = ES.EST_ID" _
        & " LEFT OUTER JOIN EMPTRANTYPE_M AS TTM " _
        & " ON EST.EST_TTP_ID = TTM.TTP_ID" _
        & " WHERE (EST.EST_TTP_ID = 4) AND (EST.EST_BSU_ID = '" & Session("sBsuid") & "') and EST.EST_EMP_ID='" & h_Emp_No.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDetails.DataSource = ds
        gvDetails.DataBind()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ddEstCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            chkFinalSettled.Visible = False
            divChkMsg.Visible = False
            Select Case ddEstCode.SelectedItem.Text.Trim
                Case "Missing"
                    lblperiod.Text = "Missing Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Resigned"
                    lblperiod.Text = "Resigned Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                    chkFinalSettled.Visible = True
                    divChkMsg.Visible = True
                Case "Terminated" '
                    lblperiod.Text = "Terminated Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                    chkFinalSettled.Visible = True
                    divChkMsg.Visible = True
                Case "Suspended"
                    lblperiod.Text = "From date"
                    lblToDate.Text = "To Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Absconding"
                    lblperiod.Text = "Absconding Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "On Leave"
                    lblperiod.Text = "From Date"
                    lblToDate.Text = "To Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Transfer"
                    lblperiod.Text = "Transfer Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case Else
                    lblperiod.Text = "From Date"
                    lblToDate.Text = "To Date"
                    txtTodate.Text = ""
                    txtTodate.Enabled = False
                    imgToDate.Enabled = False
            End Select


        Catch ex As Exception

        End Try
    End Sub

    Private Function SetVisible(ByVal Result As String)
        Try
            Select Case Result
                Case "Missing"
                    lblperiod.Text = "Missing Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Resigned"
                    lblperiod.Text = "Resigned Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Terminated" '
                    lblperiod.Text = "Terminated Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Suspended"
                    lblperiod.Text = "Suspended date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Absconding"
                    lblperiod.Text = "Absconding Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "Transfer"
                    lblperiod.Text = "Transfer Date"
                    lblToDate.Text = "Last Working Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case "On Leave"
                    lblperiod.Text = "From Date"
                    lblToDate.Text = "To Date"
                    txtTodate.Enabled = True
                    imgToDate.Enabled = True
                Case Else
                    lblperiod.Text = "From Date"
                    lblToDate.Text = "To Date"
                    txtTodate.Enabled = False
                    imgToDate.Enabled = False
            End Select


        Catch ex As Exception

        End Try
    End Function

End Class
