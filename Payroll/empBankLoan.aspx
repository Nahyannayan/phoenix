<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empBankLoan.aspx.vb" Inherits="Payroll_empBankLoan" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

  <%--  <script language="javascript" type="text/javascript">

        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;

            url = 'empShowMasterEmp.aspx?id=' + mode;

            if (mode == 'BK') {
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtBName.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBnk_ID.ClientID %>").value = NameandCode[1];
            }

        }
        function getEmpID(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'EN') {
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }

                NameandCode = result.split('___');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
                 document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];
             }

         }
    </script>--%>


    <script>
        function getPageCode(mode) {
            var url;
            url = 'empShowMasterEmp.aspx?id=' + mode;
            var oWnd = radopen(url, "pop_bank");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtBName.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBnk_ID.ClientID %>").value = NameandCode[1];
            }
        }
        function getEmpID(mode) {
            var url;
            url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientClose2(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%=txtEmpName.ClientID %>', '');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Bank Loans By Employee"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Employee Name</span></td>
                                    <td align="left" class="matters" colspan="3">
                                        <asp:TextBox ID="txtEmpName" runat="server" OnTextChanged="txtEmpName_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getEmpID('EN'); return false;" />
                                        <asp:RequiredFieldValidator ID="rfvEmpName" runat="server" ControlToValidate="txtEmpName"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Employee name required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Bank Name</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtBName" runat="server"></asp:TextBox>

                                        <asp:ImageButton ID="btnBankName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getPageCode('BK');return false;" />
                                        <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBName"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Bank name required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters"><span class="field-label">Branch Name</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtBranchName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">IBAN No</span></td>
                                    <td align="left" class="matters" colspan="1">
                                        <asp:TextBox ID="txtAccno" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAccno"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Account No. Required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>

                                    <td align="left" class="matters"><span class="field-label">Amount</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                        <asp:CompareValidator ID="cvAmount" runat="server" ControlToValidate="txtAmount"
                                            ErrorMessage="Enter valid numerical value" Type="Double" CssClass="error" ForeColor="" Operator="DataTypeCheck" ValidationGroup="groupM1">*</asp:CompareValidator></td>
                                </tr>

                                <tr>
                                    <td align="left" class="matters"><span class="field-label">From Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtFrom_date" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter From  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="From Date entered is not a valid date" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvFrom_Date" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters"><span class="field-label">To Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtTo_date" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnTo_date" runat="server" ImageUrl="~/Images/calendar.gif" /><span style="color: #c00000"> </span>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTo_date"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter To  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvTodate" runat="server" CssClass="error" Display="Dynamic"
                                            EnableViewState="False" ErrorMessage="To Date entered is not a valid date and must be greater than or equal to From date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Active</span></td>
                                    <td align="left" class="matters">
                                        <asp:CheckBox ID="chkLActive" runat="server" Checked="true" /></td>
                                    <td align="left" class="matters"><span class="field-label">Salary Transfer Required</span></td>
                                    <td align="left" class="matters">
                                        <asp:CheckBox ID="chkTransfer" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Remark</span></td>
                                    <td align="left" class="matters" colspan="3">
                                        <asp:TextBox ID="txtRemark" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                            TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ControlToValidate="txtRemark"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Remark can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom"></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfBNK_ID" runat="server" />
                <asp:HiddenField ID="hfEmp_ID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnFrom_date" TargetControlID="txtFrom_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFrom_date" TargetControlID="txtFrom_date">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgbtnTo_date" TargetControlID="txtTo_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtTo_date" TargetControlID="txtTo_date">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

