<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empViewEmpMaster.aspx.vb" Inherits="Payroll_empViewEmpMaster" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }
        window.onload = scroll_page;
    </script>
    <!--1st drop down menu -->

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Employee Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top"  align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><br />
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>                             
                        </td>                       
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">
                    <%--<tr class="subheader_BlueTableView" valign="top" >
                <th align="left" colspan="9" valign="middle"></th>
            </tr>--%>

                    <tr>
                        <td align="left" width="20%"> <span  class="field-label">Filter </span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="0">ALL</asp:ListItem>
                                <asp:ListItem Value="1">Working Unit</asp:ListItem>
                                <asp:ListItem Value="2">Visa Unit</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"> <span  class="field-label"> Status  </span>  </td>
                        <td align="left" width="30%">

                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                <asp:ListItem Value="0" Selected="True">ALL</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="2">On Leave</asp:ListItem>
                                <asp:ListItem Value="3">Missing</asp:ListItem>
                                <asp:ListItem Value="4">Resigned</asp:ListItem>
                                <asp:ListItem Value="5">Terminated</asp:ListItem>
                                <asp:ListItem Value="6">Suspended</asp:ListItem>
                                <asp:ListItem Value="7">Absconding</asp:ListItem>
                                <asp:ListItem Value="8">Transfered</asp:ListItem>
                                <asp:ListItem Value="9">Pending</asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4" width="100%">
                            <asp:GridView ID="gvEMPDETAILS" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row" SkinID="GridViewView" PageSize="50">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <HeaderTemplate>
                                            EMPLOYEE No.
                                            <br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt" > </asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>

                                       
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prev EMP NO">
                                        <HeaderTemplate>
                                            PREV. EMP No.
                                            <br />
                                            <asp:TextBox ID="txtPrevEmpNo" runat="server" SkinID="Gridtxt" ></asp:TextBox>
                                            <asp:ImageButton ID="btnPrevDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrevEMPNo" runat="server" Text='<%# Bind("EMP_STAFFNo") %>'></asp:Label>
                                        </ItemTemplate>                                      
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            EMPLOYEE NAME
                                            <br />
                                            <asp:TextBox ID="txtEmpName" runat="server"  SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP DOJ">
                                        <HeaderTemplate>
                                            D.O. Join
                                            <br />
                                            <asp:TextBox ID="txtDOJ" runat="server"  SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Bind("EMP_JOINDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP PASSPORT NO" SortExpression="VHH_DOCDT">
                                        <HeaderTemplate>
                                            Passport No
                                            <br />
                                            <asp:TextBox ID="txtPassportNo" runat="server"  SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="btnSearchName_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("EMP_PASSPORT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VISA ID" Visible="False">
                                        <HeaderTemplate>
                                            VISAID
                                            <br />
                                            <asp:TextBox ID="txtVisaID" runat="server"  SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnVisaID" OnClick="btnSearchName_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVisaID" runat="server" Text='<%# bind("EMP_VISA_ID") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP DESIGNATION">
                                        <HeaderTemplate>
                                            DESIGNATION
                                            <br />
                                            <asp:TextBox ID="txtDesignation" runat="server" SkinID="Gridtxt" ></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("EMP_DES_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderTemplate>
                                            Status
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label61" runat="server" Text='<%# Bind("EMP_STATUS_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Photos" SortExpression="bPhotoExists">
                                        <ItemTemplate>
                                            <asp:Image ID="imgPosted" runat="server" ImageUrl='<%# returnpath(Container.DataItem("bPhotoExists")) %>' />
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <a id='detail'></a>
                            <br />
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_11" runat="server" type="hidden" value="=" />
                            <a id='child'></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

