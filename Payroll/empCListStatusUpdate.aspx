<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empCListStatusUpdate.aspx.vb" Inherits="Payroll_empCListStatusUpdate" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style type="text/css">
        .RadComboBox .rcbInputCell {
    width: 100%;
    height: 31px !important;
background-color: transparent !important; 
border-radius: 6px !important;
background-image: none !important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
}

/*.RadComboBox .rcbArrowCellRight a {
    background-position: -18px -176px;
    border: solid black;
    border-width: 0 1px 1px 0;
    transform: rotate(360deg);
    -webkit-transform: rotate(45deg) !important;
    color: black;
    width: 7px !important;
    height: 7px !important;
    overflow: hidden;
        margin-top: -1px;
    margin-left: -15px;
}*/
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 31px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
    padding:0px !important;
    background-color: transparent !important;
}
    table.rcbDisabled {
    width: 100% !important;
}
    .RadComboBox_Default .rcbDisabled .rcbInput, .RadComboBoxWithLabel table {
        width:100% !important;
        background-color: transparent !important;
        border:0px !important;
        border-radius:0px !important;
    }
    .RadComboBox_Default .rcbDisabled {
        background-color: rgba(0,0,0,0.01)!important;
    }
        .RadComboBox_Default {
    /* color: #333; */
    font: inherit;
    width: 80% !important;
}
        .RadComboBox_Default .rcbFocused .rcbInput, .RadComboBox .rcbInput {
    color: black;
    padding: 10px !important;
    border: 0px !important;
    box-shadow: none !important;
}

           .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
              .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }

              .RadComboBox table td.rcbInputCell {
    padding: 0px !important;
    border-width: 0;
}
    .RadComboBoxDropDown.rcbAutoWidthResizer {
        overflow: inherit!important;
        overflow-x: visible!important;
        overflow-y: scroll !important;
        width: 520px!important;
        height: 300px !important;
    }
    .RadComboBox_Default .rcbInner {
        border-radius:6px !important;
    }
    .RadComboBox .rcbInner {
        border: 1px solid #dee2da!important;
        box-shadow: 1px 2px 5px rgba(0,0,0,0.1) !important;
        
    }
    </style>

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        function funSearch(e) {

            var evt = e ? e : window.event;
            var bt = document.getElementById("<%=btnEnterSearch.ClientID %>");

            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }

        function change_chk_state_(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0 || document.forms[0].elements[i].name.search(/chkSelall/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }

        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            C List Status Update
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">

                            <asp:GridView ID="gvEMPDETAILS" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                AllowPaging="True" SkinID="GridViewView" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText="Log">
                                        <ItemTemplate>
                                            <asp:Image ID="imgLog" runat="server" ImageUrl="~/Images/master/Remark.png" />
                                            <ajaxToolkit:PopupControlExtender ID="popStatusHistory" runat="server" PopupControlID="pnlPopup"
                                                TargetControlID="imgLog" DynamicContextKey='<%# Eval("EMP_ID") %>' DynamicControlID="pnlPopup"
                                                DynamicServicePath="NewClistPopUpService.asmx"
                                                DynamicServiceMethod="GetDynamicContent" Position="Right">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <HeaderTemplate>
                                            EMPNo.<br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt" onkeypress="return funSearch(event)"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <HeaderStyle Width="10%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            NAME<br />
                                            <asp:TextBox ID="txtEmpName" runat="server" SkinID="Gridtxt" onkeypress="return funSearch(event)"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP DOJ">
                                        <HeaderTemplate>
                                            D.O. Join
                                            <br />
                                            <asp:TextBox ID="txtDOJ" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <HeaderStyle Width="8%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Bind("EMP_JOINDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP PASSPORT NO" SortExpression="VHH_DOCDT" Visible="False">
                                        <HeaderTemplate>
                                            Passport No
                                            <br />
                                            <asp:TextBox ID="txtPassportNo" runat="server" SkinID="Gridtxt" onkeypress="return funSearch(event)"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="btnSearchName_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("EMP_PASSPORT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP DESIGNATION">
                                        <HeaderTemplate>
                                            DESIGNATION
                                            <br />
                                            <asp:TextBox ID="txtDesignation" runat="server" onkeypress="return funSearch(event)"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("EMP_DES_DESCR") %>' onkeypress="return funSearch(event)"></asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle />
                                        <FooterStyle />
                                        <HeaderStyle />
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            ABC
                                            <br />
                                            <asp:TextBox ID="txtABC" runat="server" SkinID="Gridtxt" onkeypress="return funSearch(event)"></asp:TextBox>
                                            <asp:ImageButton ID="btnABCSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <HeaderStyle Width="6%" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblABC" runat="server" Text='<%# Bind("EMP_ABC") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>
                                            <%--<asp:DropDownList ID="ddlReason" runat="server"  DataSourceID="sdsReasonList" DataTextField="CList_Status_DESCR" DataValueField="CList_Status_ID" ></asp:DropDownList>--%>
                                            <telerik:RadComboBox runat="server" ID="ddlReason" DropDownAutoWidth="Enabled" Filter="Contains" Label="Filter Contains" RenderMode="Lightweight" DataSourceID="sdsReasonList" DataTextField="CList_Status_DESCR" DataValueField="CList_Status_ID" DropDownWidth="100%"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sdsReasonList" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                                SelectCommand="exec GetCListStatusDropDown"></asp:SqlDataSource>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action to be taken">
                                        <ItemTemplate>
                                            <div style="float: left">
                                                <asp:TextBox ID="txtStatusRemarks" runat="server" MaxLength="500" TextMode="MultiLine"></asp:TextBox>

                                            </div>
                                        </ItemTemplate>


                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Completion Date">


                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDategv" runat="server" Width="60%"></asp:TextBox>
                                            <asp:ImageButton ID="imgCalendargv" runat="server" ImageUrl="~/Images/calendar.gif"
                                                            ImageAlign="Middle" CausesValidation="False" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                            PopupButtonID="txtDategv" TargetControlID="txtDategv" PopupPosition="BottomRight">
                                                        </ajaxToolkit:CalendarExtender>
                                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                            PopupButtonID="imgCalendargv" TargetControlID="txtDategv">
                                                        </ajaxToolkit:CalendarExtender>
                                                    

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Save One by One">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnSaveRow" runat="server" ToolTip="Save current record only"
                                                ImageUrl="~/Images/ButtonImages/SAVE_APPROVE.png" OnClick="btnSaveRow_Click" />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP_ID" SortExpression="EMP_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>

                                            <input id="chkSelall" type="checkbox" onclick="javascript: change_chk_state_(this)" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkESD_BANK" runat="server" value='<%# Bind("EMP_ID") %>' type="checkbox" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save All" ValidationGroup="Datas" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>

                <asp:Panel ID="pnlPopup" runat="server" CssClass="panel-cover">
                    <div id="InnDiv" align="left">
                        <table id="MainTable" style="cursor: hand;">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <input id="h_Grid" runat="server" type="hidden" value="top" />
                <input id="h_SelectedId"
                    runat="server" type="hidden" value="-1" />
                <input id="h_selected_menu_1" runat="server"
                    type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden"
                    value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input
                    id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server"
                    type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden"
                    value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_11" runat="server" type="hidden" value="=" />
                <asp:Button ID="btnEnterSearch" runat="server" Style="display: none" />
            </div>
        </div>
    </div>

</asp:Content>
