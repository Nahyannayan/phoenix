Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empAirfareView


    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    'ts


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle

            hlAddNew.NavigateUrl = "empAirfare.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_FromCity.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_TicketType.Value = "LI__../Images/operations/like.gif"

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "P130080" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub






    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function







    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrcondn6, lstrcondn7 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrcondn7 = ""

            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables


                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)


                '   --- FILTER CONDITIONS ---
                '   -- 1   from dt
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFDate")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EAF_DTFROM", lstrCondn1)

                '   -- 1  to dt
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EAF_DTTO", lstrCondn2)

                '   -- 2  class
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFClass")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EAF_CLASS", lstrCondn3)

                '   -- 3   age

                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAgegroup")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, " EAF_AGEGROUP", lstrCondn4)

                '   -- 5  To City

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCity")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "CIT_DESCR", lstrCondn5)

                '   -- 6  From City

                larrSearchOpr = h_Selected_menu_FromCity.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFromCity")
                lstrcondn6 = txtSearch.Text
                If (lstrcondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FROM_CITY", lstrcondn6)

                '   -- 7  From City

                larrSearchOpr = h_Selected_menu_TicketType.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTicketType")
                lstrcondn7 = txtSearch.Text
                If (lstrcondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EAF_TYPE", lstrcondn7)


            End If 

            str_Sql = "SELECT * FROM(SELECT EF.EAF_ID,EF.EAF_BSU_ID, " _
            & " EF.EAF_DTFROM, EAF_DTTO ," _
            & " CASE EF.EAF_CLASS WHEN 'E' THEN 'Economy' " _
            & " WHEN 'F' THEN 'First Class'" _
            & " WHEN 'B' THEN 'Business Class'end " _
            & " as EAF_CLASS , case EF.EAF_AGEGROUP when 'A' then 'Adult' when 'C' then 'Child' when 'I' then 'Infant' end as EAF_AGEGROUP ," _
            & " EF.EAF_AMOUNT, CM.CIT_DESCR, CM1.CIT_DESCR AS FROM_CITY , CASE WHEN EF.EAF_TYPE = 'R' THEN 'Return' WHEN EF.EAF_TYPE = 'O' THEN 'One Way' ELSE '' END AS EAF_TYPE" _
            & " FROM EMPAIRFARE_S AS EF INNER JOIN" _
            & " CITY_M AS CM ON EF.EAF_CIT_ID = CM.CIT_ID inner JOIN dbo.CITY_M AS CM1 ON EF.EAF_FROM_CIT_ID = CM1.CIT_ID " _
            & " )AS  DB WHERE  EAF_BSU_ID='" & Session("sBSUID") & "'" & str_Filter & " order by EAF_DTFROM desc,CIT_DESCR , FROM_CITY "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtFDate")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtFClass")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtAgegroup")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtCity")
            txtSearch.Text = lstrCondn5
 
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
 



    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub







    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empAirfare.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub






    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub






End Class
