﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empRecurringDed
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            ViewState("RowId") = "0"
            ViewState("ERDID") = "0"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If ViewState("datamode") = "view" Then
                ViewState("viewid") = Encr_decrData.Decrypt(IIf(Request.QueryString("viewid") IsNot Nothing, Request.QueryString("viewid").Replace(" ", "+"), ""))
                ViewState("ERDID") = Encr_decrData.Decrypt(IIf(Request.QueryString("ERDID") IsNot Nothing, Request.QueryString("ERDID").Replace(" ", "+"), ""))
                ViewState("RowId") = "0"

                h_Emp_No.Value = ViewState("viewid")
                GetDeductionHistory()

                For Each gvrow As GridViewRow In gvEmpSalHistory.Rows
                    Dim lnkEdit As LinkButton
                    If gvrow.Cells(8).Text = ViewState("ERDID") Then
                        ViewState("RowId") = gvrow.RowIndex
                        lnkEdit = TryCast(gvrow.FindControl("lnkEdit"), LinkButton)
                        lnkEdit_Click(lnkEdit, Nothing)
                    End If

                Next


            Else
                ViewState("viewid") = ""
            End If

            If ViewState("datamode") = Nothing Then
                ViewState("datamode") = "add"
                ViewState("RowId") = "0"

            End If
           

            'If Request.QueryString("datamode") <> "" Then
            '    'Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130185" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                GridBindCodes()
            End If
        End If

    End Sub
 
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

   
    Protected Sub GridBindCodes()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "GetDedEarnCodes"
        Dim SqlParam(2) As SqlParameter
        SqlParam(0) = New SqlClient.SqlParameter("@ERNType", SqlDbType.VarChar, 2)
        SqlParam(0).Value = ddlType.SelectedValue
        SqlParam(1) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 10)
        SqlParam(1).Value = Session("sBsuid")

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, str_Sql, SqlParam)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataBind()
        dr.Close()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        GridBindCodes()
    End Sub


    Protected Function SaveRecurringDeduction(ByVal strRowId As String) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim cmd As New SqlCommand
        cmd.CommandText = "SaveRecurringDeductions"
        cmd.CommandType = CommandType.StoredProcedure
        Dim objConn As New SqlConnection(str_conn)
        cmd.Connection = objConn

        Try


            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            cmd.Transaction = stTrans
            cmd.Parameters.AddWithValue("@ERD_EMP_ID", h_Emp_No.Value)
            cmd.Parameters.AddWithValue("@ERD_BSU_ID", Session("sBSUID"))
            cmd.Parameters.AddWithValue("@ERD_STARTDT", CDate(txtFromDate.Text))
            cmd.Parameters.AddWithValue("@ERD_ENDDT", txtToDate.Text)
            cmd.Parameters.AddWithValue("@ERD_bActive", 1)
            cmd.Parameters.AddWithValue("@ERD_CUR_ID", Session("BSU_CURRENCY"))
            cmd.Parameters.AddWithValue("@ERD_ERNCODE", ddSalEarnCode.SelectedValue)
            cmd.Parameters.AddWithValue("@ERD_TYPE", ddlType.SelectedValue)
            cmd.Parameters.AddWithValue("@ERD_AMOUNT", txtSalAmount.Text)
            cmd.Parameters.AddWithValue("@ERD_REMARKS", txtRemarks.Text)
            cmd.Parameters.AddWithValue("@bEdit", strRowId)
            cmd.Parameters.Add("@RetVal", SqlDbType.Int)
            cmd.Parameters("@RetVal").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            If cmd.Parameters("@RetVal").Value = 0 Then
                lblError.Text = UtilityObj.getErrorMessage(cmd.Parameters("@RetVal").Value)
                stTrans.Commit()
                ClearAll()
                GetDeductionHistory()
                ViewState("RowId") = "0"
                ViewState("datamode") = "add"

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                lblError.Text = UtilityObj.getErrorMessage(cmd.Parameters("@RetVal").Value)
                stTrans.Rollback()
            End If
            'EMPRECURRING_EARN_DED()
        Catch ex As Exception
            lblError.Text = ex.Message
        Finally
            objConn.Close()
        End Try
    End Function

    Protected Sub txtEmpNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNo.TextChanged
        GetDeductionHistory()
    End Sub

    Protected Sub GetDeductionHistory()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "exec GetRecurringDeductionsHistory " & h_Emp_No.Value & ",'" & Session("sBSUID") & "',0"
        Dim ds As New DataSet
        gvEmpSalHistory.Columns(0).Visible = True
        gvEmpSalHistory.Columns(8).Visible = True
        gvEmpSalHistory.Columns(9).Visible = True

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            gvEmpSalHistory.DataSource = ds.Tables(0)
            gvEmpSalHistory.DataBind()
            If ViewState("viewid") <> "" Then
                txtEmpNo.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
                ViewState("datamode") = "add"

            End If
        Else
            gvEmpSalHistory.DataSource = Nothing
            gvEmpSalHistory.DataBind()
        End If
        gvEmpSalHistory.Columns(0).Visible = False
        gvEmpSalHistory.Columns(8).Visible = False
        gvEmpSalHistory.Columns(9).Visible = False
        For Each gvRow As GridViewRow In gvEmpSalHistory.Rows
            gvRow.BackColor = Nothing
        Next
       
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ViewState("datamode") = "Edit"
        txtRemarks.Text = sender.Parent.parent.cells(5).Text
        Dim lblFromDate As Label
        Dim lblToDate As Label
        Dim lnkEdit As LinkButton
        Dim lblAmount As Label
        lblFromDate = TryCast(sender.Parent.parent.FindControl("lblFromDate"), Label)
        lblToDate = TryCast(sender.Parent.parent.FindControl("lblToDate"), Label)
        lnkEdit = TryCast(sender.Parent.parent.FindControl("lnkEdit"), LinkButton)
        lblAmount = TryCast(sender.Parent.parent.FindControl("lblAmount"), Label)
        For Each gvRow As GridViewRow In gvEmpSalHistory.Rows
            gvRow.BackColor = Nothing
        Next
        gvEmpSalHistory.Rows(sender.parent.parent.rowIndex).BackColor = Drawing.Color.Wheat
      
        'txtSalAmount.Text = sender.Parent.parent.cells(5).Text
        txtSalAmount.Text = lblAmount.Text
        txtFromDate.Text = lblFromDate.Text
        txtToDate.Text = lblToDate.Text
        txtRemarks.Text = sender.Parent.parent.cells(6).Text
        ddlType.SelectedValue = sender.Parent.parent.cells(0).Text
        ddlType.Enabled = False
        GridBindCodes()
        ddSalEarnCode.Items.FindByText(sender.Parent.parent.cells(2).Text).Selected = True
        setViewData()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnDelete.Visible = True
        btnSave.Visible = True
        btnAdd.Visible = True
        ViewState("RowId") = sender.Parent.parent.cells(8).Text
        'ddlType.
        'Dim lblRowId As New Label
        ' lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        'Dim gvrow As GridViewRow
        'gvrow = gvEmpSalHistory.Rows(e.parent.RowIndex).Cells(0).Value.ToString()

        'Dim i As Integer
        'i = gvEmpSalHistory.
        'setDeductionDetails(i)
        ' gvEmpSalHistory_SelectedIndexChanged(Nothing, Nothing)
    End Sub
    Protected Sub setViewData()
        ddSalEarnCode.Enabled = False
        txtFromDate.ReadOnly = True
        txtFromDate.Enabled = False
        ImgFrom.Visible = False
    End Sub
    Protected Sub setDeductionDetails(ByVal RowId As Integer)
        txtRemarks.Text = gvEmpSalHistory.Rows(RowId).Cells(4).Text
    End Sub
    Protected Sub ClearAll()
        ViewState("RowId") = "0"
        ViewState("ERDID") = "0"
        txtRemarks.Text = ""
        txtSalAmount.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        ddlType.SelectedIndex = -1
        GridBindCodes()
        ddlType.Enabled = True
        ddSalEarnCode.Enabled = True
        txtFromDate.ReadOnly = False
        ImgFrom.Visible = True
        txtFromDate.Enabled = True
    End Sub
    Protected Sub gvEmpSalHistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpSalHistory.PageIndexChanging
        gvEmpSalHistory.PageIndex = e.NewPageIndex
        GetDeductionHistory()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If h_Emp_No.Value = "" Then
            lblError.Text = "Please select an employee."
            Exit Sub
        End If
        If IsNumeric(txtSalAmount.Text) = False Then
            lblError.Text = "Invalid amount"
            Exit Sub
        End If
        If txtToDate.Text <> "" Then
            If CDate(txtFromDate.Text) > CDate(txtToDate.Text) Then
                lblError.Text = "Invalid Dates."
                Exit Sub
            End If
        End If
        If ViewState("RowId") = "0" Or ViewState("RowId") = Nothing Then
            SaveRecurringDeduction(0)
        Else
            SaveRecurringDeduction(ViewState("RowId"))
        End If
        ViewState("ERDID") = "0"
    End Sub

    'Protected Sub gvEmpSalHistory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvEmpSalHistory.SelectedIndexChanged
    '    Dim RecordId As String = gvEmpSalHistory.SelectedIndex
    '    setDeductionDetails(RecordId)
    'End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ViewState("datamode") = "add"
        ClearAll()
        ViewState("RowId") = "0"
        ViewState("ERDID") = "0"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        For Each gvRow As GridViewRow In gvEmpSalHistory.Rows
            gvRow.BackColor = Nothing
        Next

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            ClearAll()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        DeActivateRecDeduction()

    End Sub
    Protected Sub DeActivateRecDeduction()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim cmd As New SqlCommand
        cmd.CommandText = "DeleteRecurringDeductions"
        cmd.CommandType = CommandType.StoredProcedure
        Dim objConn As New SqlConnection(str_conn)
        cmd.Connection = objConn
        If ViewState("RowId") = Nothing Or ViewState("RowId") = "" Or ViewState("RowId") = "0" Then
            lblError.Text = "No record to delete."
            Exit Sub
        End If
        Try


            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            cmd.Transaction = stTrans
            cmd.Parameters.AddWithValue("@ERD_EMP_ID", h_Emp_No.Value)
            cmd.Parameters.AddWithValue("@ERD_BSU_ID", Session("sBSUID"))
            cmd.Parameters.AddWithValue("@ERD_STARTDT", CDate(txtFromDate.Text))
            cmd.Parameters.AddWithValue("@ERD_bActive", 0)
            cmd.Parameters.AddWithValue("@ERD_ID", ViewState("RowId"))
            cmd.Parameters.AddWithValue("@ERD_ERNCODE", ddSalEarnCode.SelectedValue)
            'cmd.Parameters.AddWithValue("@ERD_TYPE", ddlType.SelectedValue)
            'cmd.Parameters.AddWithValue("@ERD_AMOUNT", txtSalAmount.Text)
            'cmd.Parameters.AddWithValue("@ERD_REMARKS", txtRemarks.Text)
            'cmd.Parameters.AddWithValue("@bEdit", strRowId)
            cmd.Parameters.Add("@RetVal", SqlDbType.Int)
            cmd.Parameters("@RetVal").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            If cmd.Parameters("@RetVal").Value = 0 Then
                lblError.Text = UtilityObj.getErrorMessage(cmd.Parameters("@RetVal").Value)
                stTrans.Commit()
                ClearAll()
                GetDeductionHistory()
                ViewState("RowId") = "0"
                ViewState("datamode") = "add"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                lblError.Text = UtilityObj.getErrorMessage(cmd.Parameters("@RetVal").Value)
                stTrans.Rollback()
            End If
            'EMPRECURRING_EARN_DED()
        Catch ex As Exception
            lblError.Text = ex.Message
        Finally
            objConn.Close()
        End Try
    End Sub
End Class
