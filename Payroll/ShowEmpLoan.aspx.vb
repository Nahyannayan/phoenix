Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_ShowEmpLoan
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GRIDBIND()
    End Sub
    Sub GRIDBIND()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
          
            str_Sql = "SELECT EMPLOYEE_M.EMPNO, EMPLOAN_H.ELH_PURPOSE, " _
                           & " EMPLOAN_H.ELH_DATE, EMPLOAN_H.ELH_INSTNO, EMPLOAN_H.ELH_CUR_ID," _
                           & " EMPLOAN_H.ELH_AMOUNT, EMPLOAN_H.ELH_BALANCE, EMPLOAN_H.ELH_bPERSONAL, " _
                           & " EMPLOAN_H.ELH_bPosted, EMPLOAN_H.ELH_ID, " _
                           & " ISNULL(EMPLOYEE_M.EMP_FNAME,'')+' '+ISNULL(EMPLOYEE_M.EMP_MNAME,'')+' '+ " _
                           & " ISNULL(EMPLOYEE_M.EMP_LNAME,'') AS EMP_NAME," _
                           & " EMPSALCOMPO_M.ERN_DESCR " _
                           & " FROM EMPLOAN_H INNER JOIN" _
                           & " EMPLOYEE_M ON EMPLOAN_H.ELH_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN" _
                           & " EMPSALCOMPO_M ON EMPLOAN_H.ELH_ERN_ID = EMPSALCOMPO_M.ERN_ID" _
                           & " WHERE ELH_BSU_ID='" & Session("SBSUID") & "'" _
                           & " AND ELH_ID ='" & Request.QueryString("ela_id") & "'"


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            fvLeave.DataSource = ds.Tables(0)
            fvLeave.DataBind()
            

            str_Sql = "SELECT ELD_ELH_ID, " _
            & " ELH_INSTNO, ELD_DATE, ELD_AMOUNT, " _
            & " ELD_PAIDAMT FROM EMPLOAN_D " _
            & " WHERE ELD_ELH_ID='" & Request.QueryString("ela_id") & "'" _
            & " ORDER BY ELH_INSTNO"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            
            gvApprovals.DataSource = ds.Tables(0)
            gvApprovals.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
