<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empEarn_Ded_Edit.aspx.vb" Inherits="Payroll_empEarn_Ded_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        //function getRoleID(ctrl1, ctrl2, id) {
        //    var sFeatures;
        //    sFeatures = "dialogWidth: 460px; ";
        //    sFeatures += "dialogHeight: 400px; ";
        //    sFeatures += "help: no; ";
        //    sFeatures += "resizable: no; ";
        //    sFeatures += "scroll: yes; ";
        //    sFeatures += "status: no; ";
        //    sFeatures += "unadorned: no; ";
        //    var NameandCode;
        //    var result;
        //    var url;

        //    url = '../Accounts/accShowEmpPP.aspx?id=D';


        //    result = window.showModalDialog(url, "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }

        //    NameandCode = result.split('___');
        //    document.getElementById(ctrl1).value = NameandCode[0];
        //    document.getElementById(ctrl2).value = NameandCode[1];
        //    UpdateAccounts(id);
        //}
        function UpdateAccounts(id) {

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (id == '1') {
                    if (document.forms[0].elements[i].type == 'text')//txtDesc txtCode
                    {
                        if (document.forms[0].elements[i].name.search(/txtDesc/) > 0)
                            if (document.forms[0].elements[i].value == '')
                            { document.forms[0].elements[i].value = document.getElementById('<%=txtAmtName.ClientID %>').value }
                if (document.forms[0].elements[i].name.search(/txtCode/) > 0)
                    if (document.forms[0].elements[i].value == '')
                    { document.forms[0].elements[i].value = document.getElementById('<%=txtAmtCode.ClientID %>').value }
            }
        }
        else {
            if (document.forms[0].elements[i].name.search(/txtCDesc/) > 0)
                if (document.forms[0].elements[i].value == '')
                { document.forms[0].elements[i].value = document.getElementById('<%=txtCAmtName.ClientID %>').value }
                if (document.forms[0].elements[i].name.search(/txtCCode/) > 0)
                    if (document.forms[0].elements[i].value == '')
                    { document.forms[0].elements[i].value = document.getElementById('<%=txtCAmtCode.ClientID %>').value }
            }
        }

    }
    </script>

    
 <script>
     function getRoleID(ctrl1, ctrl2, id) {
        var  url = '../Accounts/accShowEmpPP.aspx?id=D';

        document.getElementById("<%=hf_id.ClientID%>").value = id;
         document.getElementById("<%=hf_ctrl1.ClientID%>").value = ctrl1;
         document.getElementById("<%=hf_ctrl2.ClientID%>").value = ctrl2;

            var oWnd = radopen(url, "pop_items");



        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_ctrl2.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_ctrl1.ClientID%>").value).value = NameandCode[0];
                UpdateAccounts(document.getElementById("<%=hf_id.ClientID%>").value)
            }
        }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_items" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Employee Category"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False" ValidationGroup="vg1"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr class="matters">
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblID" runat="server" Text="ID" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtID" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtID" ValidationGroup="vg1"
                                            CssClass="error" Display="Dynamic" ErrorMessage="ID required." ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td class="matters" align="left" width="20%">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDesc" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDesc" ValidationGroup="vg1"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Description can not be left empty."
                                            ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblBpaid" runat="server" Text="Debit Account Name" CssClass="field-label"></asp:Label></td>
                                    <td valign="middle" align="left">
                                        <asp:TextBox ID="txtAmtCode" runat="server" CssClass="inputbox"></asp:TextBox><asp:ImageButton
                                            ID="btnPartyAcc" runat="server" ImageUrl="~/Images/cal.gif" CausesValidation="False" />
                                        <asp:TextBox ID="txtAmtName" runat="server" CssClass="inputbox"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAmtName" ValidationGroup="vg1"
                                CssClass="error" Display="Dynamic" ErrorMessage="Account Name required." ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Credit Account Name" CssClass="field-label"></asp:Label></td>
                                    <td valign="middle" align="left">
                                        <asp:TextBox ID="txtCAmtCode" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="btnCPartyAcc" runat="server" ImageUrl="~/Images/cal.gif" CausesValidation="False" />
                                        <asp:TextBox ID="txtCAmtName" runat="server" CssClass="inputbox"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCAmtName" ValidationGroup="vg1"
                                CssClass="error" Display="Dynamic" ErrorMessage="Account Name required." ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="ro4" runat="server" visible="False">
                                    <td id="cl4Include" runat="server" align="left">
                                        <asp:Label ID="lblEob" runat="server" Text="Apply Rule" CssClass="field-label"></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlRules" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr runat="server" id="cl6DeductLOP">
                                    <td runat="server" colspan="4" align="left" id="Td1">
                                        <asp:CheckBox ID="chkDeductLOP" runat="server" />
                                        <asp:Label ID="lblDeductLOP" runat="server" Text="Deduct if LOP" CssClass="field-label"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="TBCPaywithLeaveSal" runat="server">
                                    <td runat="server" colspan="4" align="left" id="Td2">
                                        <asp:CheckBox ID="chkPaywithLeavesal" runat="server" />
                                        <asp:Label ID="lblPaywithLeaveSal" runat="server" Text="Pay with Leave Salary" CssClass="field-label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvCategory" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="False" DataKeyNames="ECT_ID" Width="100%" CellPadding="4">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Code" InsertVisible="False" SortExpression="ECT_ID"
                                                    Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblECT_ID" runat="server" Text='<%# Bind("ECT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ECT_DESCR" HeaderText="Category" SortExpression="ECT_DESCR">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Debit Account">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCode" runat="server" OnPreRender="txtCode_PreRender" Text='<%# BIND("ERD_ACC_ID") %>'></asp:TextBox><asp:ImageButton
                                                            ID="btnPartyAcc" runat="server" ImageUrl="~/Images/cal.gif" CausesValidation="False" /><asp:TextBox ID="txtDesc" runat="server" OnPreRender="txtDesc_PreRender" Text='<%# BIND("ACT_NAME") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Credit Account">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCCode" runat="server" OnPreRender="txtCode_PreRender" Text='<%# BIND("ERD_PAYABLE_ACT_ID") %>'></asp:TextBox><asp:ImageButton
                                                            ID="btnCPartyAcc" runat="server" ImageUrl="~/Images/cal.gif" CausesValidation="False" /><asp:TextBox ID="txtCDesc" runat="server" OnPreRender="txtDesc_PreRender"
                                                                Text='<%# BIND("ERD_PAYABLE_ACT_DESCR") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pro Rata">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkProrata" runat="server" Checked='<%# Bind("ERD_CALCTYP") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vg1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hf_id" runat="server" />
    <asp:HiddenField ID="hf_ctrl1" runat="server" />
    <asp:HiddenField ID="hf_ctrl2" runat="server" />
</asp:Content>

