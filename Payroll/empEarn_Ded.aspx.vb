Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Payroll_empEarn_Ded
    Inherits System.Web.UI.Page
    Dim splitquery() As String

    Dim Encr_decrData As New Encryption64

    'ts
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function returnpath(ByVal p_posted) As String
        Try
            If p_posted = 1 Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = String.Format("~\payroll\empEarn_Ded_Edit.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                gvCommonEmp.Attributes.Add("bordercolor", "#1b80b6")

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P050055" And ViewState("MainMnu_code") <> "P050060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("MainMnu_code") = "P050055" Then
                        lblHead.Text = "Earnings"
                        ' gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = True

                        'ViewState("str_Sql") = "Select id,descr,paid,eob,EMR_DESC from (SELECT EMPSALCOMPO_M.ERN_ID AS id, EMPSALCOMPO_M.ERN_DESCR AS descr, vw_OSF_ACCOUNTS_M.ACT_NAME AS paid,EMPSALCOMPO_M.ERN_Flag AS eob, EMPSALCOMPO_M.ERN_TYP, EMPEARNRULES_M.EMR_DESCRIPTION AS EMR_DESC FROM vw_OSF_ACCOUNTS_M INNER JOIN  EMPSALCOMPO_M ON vw_OSF_ACCOUNTS_M.ACT_ID = EMPSALCOMPO_M.ERN_ACC_ID INNER JOIN  EMPEARNRULES_M ON EMPSALCOMPO_M.ERN_Flag = EMPEARNRULES_M.EMR_ID)a where a.ERN_TYP=1 and a.id<>'' "
                        ViewState("str_Sql") = "Select ID,DESCR, EOB,  INCLUDESAL, bLOP,CREDIT,DEBIT,EMR_DESC from (" _
                            & " SELECT ERN.ERN_ID AS ID, ERN.ERN_DESCR AS DESCR, ERN.ERN_BIncludeInLeaveSal AS INCLUDESAL, " _
                            & " ERN.ERN_bLOP AS bLOP, ERN.ERN_Flag AS EOB, ERN.ERN_TYP, ERN.ERN_ACC_ID + ' - ' +" _
                            & " AMD.ACT_NAME AS DEBIT, ERN.ERN_PAYABLE_ACT_ID + ' - ' + AMC.ACT_NAME AS CREDIT, " _
                            & " EMR.EMR_DESCRIPTION  AS EMR_DESC FROM EMPEARNRULES_M AS EMR INNER JOIN" _
                            & " EMPSALCOMPO_M AS ERN ON EMR.EMR_ID = ERN.ERN_TYP LEFT OUTER JOIN" _
                            & " vw_OSF_ACCOUNTS_M AS AMC ON ERN.ERN_PAYABLE_ACT_ID = AMC.ACT_ID LEFT OUTER JOIN" _
                            & " vw_OSF_ACCOUNTS_M AS AMD ON ERN.ERN_ACC_ID = AMD.ACT_ID" _
                            & " )a where a.ERN_TYP=1 and  a.ID<>''"
                    ElseIf ViewState("MainMnu_code") = "P050060" Then
                        'gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(4).Visible = False
                        lblHead.Text = "Deductions"
                        ' ViewState("str_Sql") = "Select id,descr,paid,eob,EMR_DESC from (SELECT EMPSALCOMPO_M.ERN_ID as id, EMPSALCOMPO_M.ERN_DESCR as descr,  vw_OSF_ACCOUNTS_M.ACT_NAME as paid, EMPSALCOMPO_M.ERN_Flag as eob,EMPSALCOMPO_M.ERN_TYP as ERN_TYP ,'' as EMR_DESC FROM  vw_OSF_ACCOUNTS_M INNER JOIN  EMPSALCOMPO_M ON vw_OSF_ACCOUNTS_M.ACT_ID = EMPSALCOMPO_M.ERN_ACC_ID)a where a.ERN_TYP=0 and a.id<>'' "
                        ViewState("str_Sql") = "Select ID,DESCR,EOB,CREDIT,DEBIT,EMR_DESC from (" _
                            & " SELECT     ERN.ERN_ID AS ID, ERN.ERN_DESCR AS DESCR, " _
                            & " ERN.ERN_Flag AS EOB, ERN.ERN_TYP, '' AS EMR_DESC, " _
                            & " ERN.ERN_ACC_ID + ' - ' + AMD.ACT_NAME AS DEBIT, ISNULL(ERN.ERN_PAYABLE_ACT_ID,'') + ' - ' + ISNULL(AMC.ACT_NAME,'') AS CREDIT" _
                            & " FROM EMPSALCOMPO_M AS ERN LEFT OUTER JOIN" _
                            & " vw_OSF_ACCOUNTS_M AS AMC ON ERN.ERN_PAYABLE_ACT_ID = AMC.ACT_ID LEFT OUTER JOIN" _
                            & " vw_OSF_ACCOUNTS_M AS AMD ON ERN.ERN_ACC_ID = AMD.ACT_ID" _
                            & " )a where a.ERN_TYP=0 and a.ID<>''"
                    End If
                    gridbind()
                    callDllHeaderBind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
        set_Menu_Img()
    End Sub


    Public Sub callDllHeaderBind(Optional ByVal p_selected As Integer = 0)
        Try
            Dim ddlHeader As New DropDownList
            ddlHeader = gvCommonEmp.HeaderRow.FindControl("ddlEmpEarnH")
            '  Dim i_ddlHeader As Integer = ddlHeader.SelectedIndex
            'ddlHeader.Items.FindByValue(gvCommonEmp.Rows(iIndex)("Collectioncode")).Selected = True
            Dim di As ListItem
            Using EmpEarnreader As SqlDataReader = AccessRoleUser.GetEmpEarnRules()
                ddlHeader.Items.Clear()
                di = New ListItem("All", 0)
                ddlHeader.Items.Add(di)
                If EmpEarnreader.HasRows = True Then
                    While EmpEarnreader.Read
                        di = New ListItem(Convert.ToString(EmpEarnreader("EMR_Description")), Convert.ToInt64(EmpEarnreader("EMR_ID")))
                        ddlHeader.Items.Add(di)
                    End While
                End If
            End Using
            If p_selected <> 0 Then
                ViewState("str_filter_type") = " AND a.eob = '" & p_selected & "'"
            Else
                ViewState("str_filter_type") = " AND a.eob <>-1"
            End If
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlHeader.Items.Count - 1
                'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                If Not ViewState("a1") Is Nothing Then
                    If ddlHeader.Items(ItemTypeCounter).Value = Convert.ToInt64(ViewState("a1")) Then
                        ddlHeader.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Dim ds As New DataSet
            Dim str_filter_code, str_filter_name, str_txtCode, str_txtAccName, str_txtAccNamec, _
            str_txtName, str_filter_Accname, str_filter_Accnamec As String
            '  Dim i_ddlHeader As Integer
            ''''''''
            str_filter_code = ""
            str_filter_name = ""
            str_filter_Accname = ""
            str_txtCode = ""
            str_txtName = ""
            str_filter_Accnamec = ""
            str_txtAccName = ""
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            Dim txtSearch As New TextBox

            '''''''''
            If gvCommonEmp.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("a.ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("a.DESCR", str_Sid_search(0), str_txtName)
                ''column1

                str_Sid_search = h_selected_menu_3.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtAmtName")
                str_txtAccName = txtSearch.Text.Trim
                str_filter_Accname = set_search_filter("a.DEBIT", str_Sid_search(0), str_txtAccName)

                str_Sid_search = h_selected_menu_3.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtAmtNamec")
                str_txtAccNamec = txtSearch.Text.Trim
                str_filter_Accnamec = set_search_filter("a.CREDIT", str_Sid_search(0), str_txtAccNamec)

            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, ViewState("str_Sql") & str_filter_code _
            & str_filter_name & str_filter_Accname & str_filter_Accnamec _
            & ViewState("str_filter_type") & " Order by a.DESCR")

            If ds.Tables(0).Rows.Count > 0 Then

                gvCommonEmp.DataSource = ds.Tables(0)
                gvCommonEmp.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                ds.Tables(0).Rows(0)(2) = True
                ds.Tables(0).Rows(0)(3) = True
                gvCommonEmp.DataSource = ds.Tables(0)
                Try
                    gvCommonEmp.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCommonEmp.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCommonEmp.Rows(0).Cells.Clear()
                gvCommonEmp.Rows(0).Cells.Add(New TableCell)
                gvCommonEmp.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCommonEmp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCommonEmp.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            set_Menu_Img()
            'txtSearch = gvCommonEmp.HeaderRow.FindControl("txtCode")
            'txtSearch.Text = str_txtCode
            'txtSearch = gvCommonEmp.HeaderRow.FindControl("txtName")
            'txtSearch.Text = str_txtName
            'txtSearch = gvCommonEmp.HeaderRow.FindControl("txtAccName")
            'txtSearch.Text = str_txtAccName

            'ddlHeader = gvCommonEmp.HeaderRow.FindControl("ddlEmpEarnH")
            'i_ddlHeader = ddlHeader.SelectedIndex

            ''ddlHeader = gvCommonEmp.HeaderRow.FindControl("DropDownListType")
            ''ddlHeader.SelectedIndex = i_ddlHeader

            callDllHeaderBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub


    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub


    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchpar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()

    End Sub


    Protected Sub gvCommonEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCommonEmp.PageIndexChanging
        gvCommonEmp.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub DropDownListType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim a1 As Integer = sender.SelectedItem.Value
        ViewState("a1") = sender.SelectedItem.Value
        callDllHeaderBind(ViewState("a1"))
        gridbind()
    End Sub


    Protected Sub gvCommonEmp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCommonEmp.RowCommand
        If e.CommandName = "View" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvCommonEmp.Rows(index), GridViewRow)

            Dim UserIDLabel As Label = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            Dim Eid As String = UserIDLabel.Text
            Dim url As String

            Eid = Encr_decrData.Encrypt(Eid)
            ViewState("datamode") = "view"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\payroll\empEarn_Ded_Edit.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", ViewState("MainMnu_code"), ViewState("datamode"), Eid)
            Response.Redirect(url)

             
        End If
    End Sub


     


    Protected Sub btnSearchAcc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

   
End Class
