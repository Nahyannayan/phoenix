﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" CodeFile="LeaveHistoryDashboard.aspx.vb" Inherits="LeaveHistoryDashboard" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="CssStyles/bootstrap.css" rel="stylesheet" />

   

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Leave History
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <div class="container-fluid">
        <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
               
                <div class="row">
                    <div class="col-sm-12"><table width="100%"><tr><td align="left" width="20%">
                      <span class="field-label" >    Select Calendar Year&nbsp;&nbsp;</span>
                </td>
                     <td align="left" width="25%">   <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true">
                </asp:DropDownList>
                  </td>
                        <td align="left" width="30%" style="display:none;">
                <asp:Button ID="btnGO" runat="server" CssClass="button" Text="View"  />
                    </td>

                        <td align="left" width="25%"></td>
                                                               </tr></table></div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">

                        <div class="card">
                        <div class="card-body">
                        <telerik:RadHtmlChart runat="server" ID="RadHtmlChart2" Height="250" Skin="Metro">
                            <PlotArea>
                                <Series>
                                    <telerik:PieSeries DataFieldY="Y" NameField="X">
                                        <LabelsAppearance DataFormatString="{0}" >
                                        </LabelsAppearance>

                                        <TooltipsAppearance DataFormatString="{0}">
                                            <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:PieSeries>
                                </Series>
                                <YAxis>
                                </YAxis>
                            </PlotArea>
                            <Legend>

                                <Appearance Position="Bottom" Visible="true" />
                            </Legend>
                            <ChartTitle Text="Annual Leave">
                            </ChartTitle>
                        </telerik:RadHtmlChart>
                        </div>
                        </div>
                        </div>

                   
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card">
                        <div class="card-body">
                        <telerik:RadHtmlChart runat="server" ID="ColumnChart" Height="250px">
                            <PlotArea>
                                <Series>
                                    <telerik:ColumnSeries DataFieldY="Annual_Leave" Stacked="true" Name="Annual_Leave">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                        <Appearance FillStyle-BackgroundColor="#00a3e0"></Appearance>
                                        <TooltipsAppearance Visible="true">
                                            <ClientTemplate>#=dataItem.Annual_Leave#</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                    <telerik:ColumnSeries DataFieldY="Medical_Leave" Stacked="true" Name="Medical_Leave">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                        <Appearance FillStyle-BackgroundColor="#041e42"></Appearance>
                                        <TooltipsAppearance Visible="true">
                                            <ClientTemplate>#=dataItem.Medical_Leave#</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                    <telerik:ColumnSeries DataFieldY="Emergency_Leave" Stacked="true" Name="Emergency_Leave">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                         <Appearance FillStyle-BackgroundColor="#ff6900"></Appearance>
                                        <TooltipsAppearance Visible="true">
                                            <ClientTemplate>#=dataItem.Emergency_Leave#</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                    <telerik:ColumnSeries DataFieldY="Others" Stacked="true" Name="Others">
                                        <LabelsAppearance Visible="false"></LabelsAppearance>
                                         <Appearance FillStyle-BackgroundColor="#210124"></Appearance>
                                        <TooltipsAppearance Visible="true">
                                            <ClientTemplate>#=dataItem.Others#</ClientTemplate>
                                        </TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                </Series>
                                <XAxis DataLabelsField="X">
                                    <LabelsAppearance Visible="true" DataFormatString="MMMyy"></LabelsAppearance>
                                    <MajorGridLines Visible="false" Width="0" />
                                                        <MinorGridLines Visible="false" />
                                </XAxis>
                                <YAxis>
                                    <MajorGridLines Visible="false" Width="0" />
                                                        <MinorGridLines Visible="false" />
                                </YAxis>
                            </PlotArea>
                            <Legend>
                                <Appearance Position="Bottom" Visible="true" />
                            </Legend>
                            
                            <ChartTitle Text="Leave Summary"></ChartTitle>
                        </telerik:RadHtmlChart>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <br />
                        </div>
                        <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" AllowPaging="false" CellSpacing="0"
                            PageSize="5" GridLines="Both" CssClass="table table-bordered table-row" Width="100%" >
                            <MasterTableView>
                            </MasterTableView>
                            <HeaderStyle BackColor="#F5F5F5"   HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <AlternatingItemStyle  HorizontalAlign="Center" />
                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                        </telerik:RadGrid>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <br />
                         <asp:GridView ID="gvLegend" runat="server" AutoGenerateColumns="true" CssClass="table table-bordered table-row" ShowHeader="false" style="font-size:small;">
                           
                        </asp:GridView>
                        <asp:Label ID="lblLegend" runat="server" Text=""></asp:Label>
                    </div>

                </div>

                <div class="text-center">
        <asp:Button ID="lnkbackbtn" CssClass="button" runat="server" OnClick="lnkbackbtn_Click" Text="Back"></asp:Button>
    </div>
            </div>
        </div>
        </div>
    </div>
</div></div></div>
</asp:Content>

