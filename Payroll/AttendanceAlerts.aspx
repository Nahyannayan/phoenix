<%@ Page Language="VB" AutoEventWireup="true" CodeFile="AttendanceAlerts.aspx.vb"
    Inherits="AttendanceAlerts" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>

</head>
<body class="matter"  bottommargin="0" leftmargin="0" rightmargin="0"
    topmargin="0">
    <form id="form1" runat="server">
        <!--1st drop down menu -->
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True"></asp:Label>
                    <br />
                    <telerik:RadGrid ID="RadGridReport" runat="server" AllowSorting="True" ShowStatusBar="True"
                        Width="100%" GridLines="Vertical" AllowPaging="True" PageSize="100"
                        EnableTheming="False" CellSpacing="0">
                        <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="True">
                        </ClientSettings>
                        <AlternatingItemStyle CssClass="input-RadGridAltRow" HorizontalAlign="Left" />
                        <MasterTableView GridLines="Both">
                            <EditFormSettings>
                            </EditFormSettings>
                            <ItemStyle CssClass="input-RadGridRow" HorizontalAlign="Left" />
                            <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Left" />
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
