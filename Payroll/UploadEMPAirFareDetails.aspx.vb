Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Version            Date            Author          Change
'1.1                06-sep-2011     Swapna          Bug fix when no date selection available
'1.2                21-dec-2011     Swapna          New report to view employee leave balance details
'1.3                17-May-2012     Swapna          New report to view Airfare details
'1.4                29-May-2012     Swapna          New report to view Salary slips emailed.
'1.5                03-Jun-2012     Swapna          To add Final Settlement filter for salary reports
Partial Class Payroll_UploadEmpAirFareDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String
    Public dsAirFareDetails As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       

        Try
            ClientScript.RegisterStartupScript(Me.GetType(), "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" + gvAirFareDetails.ClientID + "', 400, 1000 , 40,false); </script>", False)

            If Not Page.IsPostBack Then

                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                ViewState("datamode") = "view"
                Session("datamode") = "view"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P450047" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle
                If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then h_BSUID.Value = Session("sBsuid")

                trCategory.Visible = False
                trSelcategory.Visible = False
                trDepartment.Visible = False
                trSelDepartment.Visible = False
                trDesignation.Visible = False
                trSelDesignation.Visible = False
                trEMPName.Visible = True
                trSelEMPName.Visible = True
                BindCategory()
                chkEMPABC_A.Checked = True
                chkEMPABC_B.Checked = True
                'trEMPABCCAT.Visible = False
                trgrid.Visible = False

                imgBankSel.OnClientClick = "GetEMPNAME(); return false;"
                StoreEMPFilter()

                'If MainMnu_code = "P159020" Then
                '    trAsOnDate.Visible = True
                '    txtAsOnDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                'End If

                'If MainMnu_code = "P159005" Or MainMnu_code = "P159020" Then

                '    trFinalSettlement.Visible = True 'V1.5
                'End If
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If


            FillEmpNames(h_EMPID.Value)

            If Not IsPostBack Then

                FillPayYearPayMonth()
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'If MainMnu_code = "P159034" Or MainMnu_code = "P150023" Or MainMnu_code = "P130176" Then
                '    trProcessingDate.Visible = False
                '    trEMPABCCAT.Visible = True

                '    trEMPName.Visible = True
                '    trSelEMPName.Visible = True
                'End If
                Me.trProcessingDate.Visible = False

                Me.trEMPName.Visible = True
                Me.trSelEMPName.Visible = False
                ' GetAirFareDetailsReport()
            End If
            'Me.gvAirFareDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub h_BSUID_ValueChanged(sender As Object, e As EventArgs) Handles h_BSUID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        'FillBSUNames(h_BSUID.Value)
        'h_BSUID.Value = ""
    End Sub

    Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDeptNames(h_DEPTID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub

    Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDESGNames(h_DESGID.Value)
        'h_DESGID.Value = ""
    End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub
    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub
  
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal connStr As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connStr)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        'If addValue.Equals(True) Then
        '    drpObj.Items.Insert(0, " ")
        '    drpObj.Items(0).Value = "0"
        '    drpObj.SelectedValue = "0"
        'End If
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        For i As Integer = iyear - 1 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

 
    Private Sub GetAirFareDetailsReport()
        'Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_connOasis)
        Dim params(9) As SqlParameter
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim cmd As New SqlCommand("[spGetEmployeeAirFareDetailsForUpload]", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        Try

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("sBsuid").ToString
            params(0) = sqlpBSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)


            Dim sqlpPAYMONTH As New SqlParameter("@MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = Convert.ToInt32(ddlPayMonth.SelectedValue)
            params(1) = sqlpPAYMONTH
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = Convert.ToInt32(ddlPayYear.SelectedValue)
            params(2) = sqlpPAYYEAR
            cmd.Parameters.Add(sqlpPAYYEAR)



            Dim sqlpCAT_ID As New SqlParameter("@Category", SqlDbType.VarChar, 20)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            params(3) = sqlpCAT_ID
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABCCategory", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            params(4) = sqlpABC_CAT
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpEmpNames As New SqlParameter("@EmployeeName", SqlDbType.VarChar, 20)
            sqlpEmpNames.Value = h_EMPID.Value
            params(5) = sqlpEmpNames
            cmd.Parameters.Add(sqlpEmpNames)

            Dim sqlpSaved As New SqlParameter("@IsSaved", SqlDbType.Bit)
            sqlpSaved.Value = chkSaved.Checked
            params(6) = sqlpSaved
            cmd.Parameters.Add(sqlpSaved)

            Dim sqlpTiktsYr As New SqlParameter("@IsHalfTicket", SqlDbType.Bit)
            sqlpTiktsYr.Value = chkTiktPerYear.Checked
            params(7) = sqlpTiktsYr
            cmd.Parameters.Add(sqlpTiktsYr)

            Dim sqlpJoinMonth As New SqlParameter("@IsJoinMonth", SqlDbType.Bit)
            sqlpJoinMonth.Value = chkJoinMonth.Checked
            params(8) = sqlpJoinMonth
            cmd.Parameters.Add(sqlpJoinMonth)

            Dim sqlpNotProcessed As New SqlParameter("@bNotProcessed", SqlDbType.Bit)
            sqlpNotProcessed.Value = chkNotProcessed.Checked
            params(9) = sqlpNotProcessed
            cmd.Parameters.Add(sqlpNotProcessed)


            cmd.CommandTimeout = 0

            Dim Ad As SqlDataAdapter = New SqlDataAdapter(cmd)
            objConn.Close()
            Dim dsAirfare As DataSet = New DataSet

            Ad.Fill(dsAirfare)
            'Me.dsAirFareDetails = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "[spGetEmployeeAirFareDetailsForUpload]", params)
            If dsAirfare.Tables(0).Rows.Count > 0 Then
                Me.gvAirFareDetails.DataSource = dsAirfare.Tables(0)
                Me.gvAirFareDetails.DataBind()
                'ClientScript.RegisterStartupScript(Me.GetType(), "CreateFixGridHeader", "<script>CreateFixGridHeader('DivGridViewData', 'gvAirFareDetails', 'FixHeaderDiv');</script>")



            Else
                Me.gvAirFareDetails.DataSource = Nothing
                Me.gvAirFareDetails.DataBind()
            End If
            'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" + gvAirFareDetails.ClientID + "', 400, 1000 , 40,false); </script>", False)

            trgrid.Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Me.lblError.Text = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try
    End Sub
    

    

    

    

    Public Function GetABCCategory() As String

        Dim strABC As String = ""

        If chkEMPABC_A.Checked Then strABC += "A|"
        If chkEMPABC_B.Checked Then strABC += "B|"
        If chkEMPABC_C.Checked Then strABC += "C|"

        Return strABC
    End Function

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMPNO as ID,EMp_id, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

 


    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex

    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblemp_id"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub chkExcludeProcessDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProcessingDate.Enabled = Not chkExcludeProcessDate.Checked
    End Sub

    Protected Sub imgBankSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBankSel.Click

    End Sub

    Protected Sub btnGetAirFareDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetAirFareDetails.Click
        Session("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), Session("datamode"))
        Me.GetAirFareDetailsReport()

        btnRemove.Visible = True

    End Sub

    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ReturnFlag As Integer = 0

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim trans As SqlTransaction = connection.BeginTransaction
            Dim rowCount As Integer = 0
            For Each dtrow1 As GridViewRow In gvAirFareDetails.Rows
                Dim chkChecked As CheckBox = CType(dtrow1.FindControl("chkPay"), CheckBox)
                If chkChecked.Checked = True Then
                    rowCount = rowCount + 1
                End If
            Next
            If rowCount = 0 Then
                lblError.Text = "Please select at least 1 record to save."
                Exit Sub
            End If


            For Each dtrow As GridViewRow In gvAirFareDetails.Rows
                Dim chkChecked As CheckBox = CType(dtrow.FindControl("chkPay"), CheckBox)
                Dim chkOutside As CheckBox = CType(dtrow.FindControl("chkOutside"), CheckBox)
                Dim lblempid As Label = CType(dtrow.FindControl("lblempid"), Label)
                Dim lbltodt As Label = CType(dtrow.FindControl("lblDtTo"), Label)
                Dim lblfromdt As Label = CType(dtrow.FindControl("lblDtFrom"), Label)
                Dim lblTicketsClass As Label = CType(dtrow.FindControl("lblTktClass"), Label)
                Dim lbltiktflag As Label = CType(dtrow.FindControl("lblActualTiktNo"), Label)
                Dim lblticktcitid As Label = CType(dtrow.FindControl("lblTocityID"), Label)
                Dim lblFromCityID As Label = CType(dtrow.FindControl("lblFromcityid"), Label)
                Dim lblperticketamt As Label = CType(dtrow.FindControl("lblperTiktAmount"), Label)
                Dim txttotalamt As TextBox = CType(dtrow.FindControl("txtTotalAmount"), TextBox)
                Dim lblearid As Label = CType(dtrow.FindControl("lblEarID"), Label)
                Dim lblTicketsPerYear As Label = CType(dtrow.FindControl("lblTicketsPerYear"), Label)
                Dim lblTotalTiktsPerYear As Label = CType(dtrow.FindControl("lblTotalTiktsPerYear"), Label)
                Dim txtRemarks As TextBox = CType(dtrow.FindControl("txtRemarks"), TextBox)
                Dim lblTotalAmount As Label = CType(dtrow.FindControl("lblTotalAmount"), Label)
                If chkChecked.Checked = True Then

                    Dim pParms(18) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EAR_BSU_ID", Session("sBsuid"))
                    pParms(1) = New SqlClient.SqlParameter("@EAR_FROMDATE", lblfromdt.Text)
                    pParms(2) = New SqlClient.SqlParameter("@EAR_TODT", lbltodt.Text)
                    pParms(3) = New SqlClient.SqlParameter("@EAR_EMP_ID", lblempid.Text)
                    pParms(4) = New SqlClient.SqlParameter("@EAR_TICKETFLAG", lbltiktflag.Text)
                    pParms(5) = New SqlClient.SqlParameter("@EAR_TICKETCLASS", lblTicketsClass.Text)
                    pParms(6) = New SqlClient.SqlParameter("@EAR_TICKETCOUNT", lblTicketsPerYear.Text)

                    pParms(7) = New SqlClient.SqlParameter("@EAR_TOTALTICKETS", lblTotalTiktsPerYear.Text)
                    pParms(8) = New SqlClient.SqlParameter("@EAR_TICKET_CIT_ID", lblticktcitid.Text)
                    pParms(9) = New SqlClient.SqlParameter("@EAR_PERTICKETAMT", lblperticketamt.Text)
                    pParms(10) = New SqlClient.SqlParameter("@EAR_TOTALAMT", txttotalamt.Text)
                    pParms(11) = New SqlClient.SqlParameter("@EAR_BPaid", False)
                    pParms(12) = New SqlClient.SqlParameter("@EAR_PAYMONTH", ddlPayMonth.SelectedValue)
                    pParms(13) = New SqlClient.SqlParameter("@EAR_ID", lblearid.Text)

                    pParms(14) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(14).Direction = ParameterDirection.ReturnValue
                    pParms(15) = New SqlClient.SqlParameter("@EAR_PAYYEAR", ddlPayYear.SelectedItem.Text)

                    pParms(16) = New SqlClient.SqlParameter("@EAR_REMARKS", txtRemarks.Text)
                    pParms(17) = New SqlClient.SqlParameter("@EAR_ACTUAL_TOTALAMT", lblTotalAmount.Text)
                    pParms(18) = New SqlClient.SqlParameter("@EAR_PaidOutside", chkOutside.Checked)

                    SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spSaveEmp_Airfare", pParms)
                    ReturnFlag = pParms(14).Value
                    If ReturnFlag <> 0 Then
                        Exit For
                    End If
                End If
            Next
            If ReturnFlag <> 0 Then
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(ReturnFlag)
            Else
                trans.Commit()
                Session("datamode") = "view"
                gvAirFareDetails.DataSource = Nothing
                gvAirFareDetails.DataBind()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), Session("datamode"))
                btnRemove.Visible = False
            End If
            lblError.Text = UtilityObj.getErrorMessage(ReturnFlag)


        End Using
    End Sub

    Protected Sub gvAirFareDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAirFareDetails.RowDataBound
        For Each grow As GridViewRow In gvAirFareDetails.Rows
            Dim chkChecked As CheckBox = CType(grow.FindControl("chkPay"), CheckBox)
            Dim lblearid As Label = CType(grow.FindControl("lblEarID"), Label)
            Dim lblTicketsPerYear As Label = CType(grow.FindControl("lblTicketsPerYear"), Label)
            Dim lblIsPaid As Label = CType(grow.FindControl("lblIsPaid"), Label)
            Dim txtRemarks As TextBox = CType(grow.FindControl("txtRemarks"), TextBox)
            Dim txttotalamt As TextBox = CType(grow.FindControl("txtTotalAmount"), TextBox)
            Dim chkOutside As CheckBox = CType(grow.FindControl("chkOutside"), CheckBox)
            Dim chkBlockEdit As CheckBox = CType(grow.FindControl("chkBlockEdit"), CheckBox)
            If CDbl(lblTicketsPerYear.Text) = 0.5 Then
                grow.BackColor = Drawing.Color.Wheat
            End If


            If CInt(lblearid.Text) > 0 Then
                grow.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFAAAA")
            End If

            If lblIsPaid.Text = "Yes" And chkOutside.Checked = False Then
                chkChecked.Enabled = False
                txtRemarks.Enabled = False
                txttotalamt.Enabled = False
            End If

            If lblIsPaid.Text = "No" Then
                txtRemarks.Enabled = True
                txttotalamt.Enabled = True
                If CInt(lblearid.Text) = 0 Then
                    txtRemarks.Text = "Airfare processing- " & ddlPayMonth.SelectedItem.Text & "/" & ddlPayYear.SelectedItem.Text
                End If

            End If
            If chkBlockEdit.Checked = True Then
                chkChecked.Enabled = False
                txtRemarks.Enabled = False
                txttotalamt.Enabled = False
            End If

        Next
    End Sub

    Protected Sub ddlPayMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayMonth.SelectedIndexChanged
        For Each grow As GridViewRow In gvAirFareDetails.Rows
            Dim lblIsPaid As Label = CType(grow.FindControl("lblIsPaid"), Label)
            Dim txtRemarks As TextBox = CType(grow.FindControl("txtRemarks"), TextBox)
            Dim lblearid As Label = CType(grow.FindControl("lblEarID"), Label)
            If lblIsPaid.Text = "No" And CInt(lblearid.Text) = 0 Then
                txtRemarks.Text = "Airfare processing- " & ddlPayMonth.SelectedItem.Text & "/" & ddlPayYear.SelectedItem.Text
            End If

        Next
    End Sub

    Protected Sub ddlPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayYear.SelectedIndexChanged
        For Each grow As GridViewRow In gvAirFareDetails.Rows
            Dim lblIsPaid As Label = CType(grow.FindControl("lblIsPaid"), Label)
            Dim txtRemarks As TextBox = CType(grow.FindControl("txtRemarks"), TextBox)
            Dim lblearid As Label = CType(grow.FindControl("lblEarID"), Label)
            If lblIsPaid.Text = "No" And CInt(lblearid.Text) = 0 Then
                txtRemarks.Text = "Airfare processing- " & ddlPayMonth.SelectedItem.Text & "/" & ddlPayYear.SelectedItem.Text
            End If

        Next
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim rowCount As Integer = 0
        For Each dtrow1 As GridViewRow In gvAirFareDetails.Rows
            Dim chkChecked As CheckBox = CType(dtrow1.FindControl("chkPay"), CheckBox)
            If chkChecked.Checked = True Then
                rowCount = rowCount + 1
            End If
        Next
        If rowCount = 0 Then
            lblError.Text = "Please select at least 1 record to Remove."
            Exit Sub
        End If

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim trans As SqlTransaction = connection.BeginTransaction
            Dim ret As Integer = 0
            For Each grow As GridViewRow In gvAirFareDetails.Rows
                Dim chkChecked As CheckBox = CType(grow.FindControl("chkPay"), CheckBox)
                Dim lblearid As Label = CType(grow.FindControl("lblEarID"), Label)
                Dim lblIsPaid As Label = CType(grow.FindControl("lblIsPaid"), Label)

                If chkChecked.Checked = True And CInt(lblearid.Text) <> 0 Then
                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@bsu_id", Session("sBsuid"))
                    pParms(1) = New SqlClient.SqlParameter("@EAR_ID", CInt(lblearid.Text))
                    pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(2).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spRemoveAirfareProcessed", pParms)
                    If pParms(2).Value <> 0 Then
                        ret = pParms(2).Value
                        lblError.Text = UtilityObj.getErrorMessage(pParms(2).Value)
                        trans.Rollback()
                        Exit For
                    End If
                End If
            Next
            If ret = 0 Then
                trans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(ret)
                GetAirFareDetailsReport()
            End If
        End Using
    End Sub

    Protected Sub gvAirFareDetails_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" + gvAirFareDetails.ClientID + "', 400, 1000 , 40,false); </script>", False)
        If gvAirFareDetails.Rows.Count > 0 Then
            'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" + gvAirFareDetails.ClientID + "', 400, 1000 , 40,false); </script>", False)

        End If
    End Sub
End Class