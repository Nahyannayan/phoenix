<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empLeaveSlab.aspx.vb" Inherits="Payroll_empLeaveSlab" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <script language="javascript" type="text/javascript">
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
        function getPageCode(mode) {
           
            var NameandCode;
            var result;
            var url;

            url = 'empShowMasterEmp.aspx?id=' + mode;
            if (mode == 'CT') {
                result = radopen(url, "pop_up");

                //if (result == '' || result == undefined) {
                //    return false;
                //}
               <%-- NameandCode = result.split('___');
                document.getElementById("<%=txtCategory.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_catid.ClientID %>").value = NameandCode[1];--%>
            }
        }
         function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtCategory.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_catid.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtCategory.ClientID%>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Leave Rule
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <asp:Label ID="lblError" runat="server"  EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="main" />
                        </td>
                    </tr>
                </table>
                <table cellpadding="5"  style="border-collapse: collapse" cellspacing="0" align="center" width="100%">
                    <%--   <tr class="subheader_img">
            <td colspan="4" style="height: 19px" align="left">Leave Rule</td>
        </tr>--%>
                    <tr>
                        <td align="left"><span class="field-label">Select Category</span></td>
                        <td align="left" >
                            <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgCategory" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('CT');return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCategory"
                                CssClass="error" ErrorMessage="Please Select an Category" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Leave Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddMonthstatusPeriodically" runat="server" DataSourceID="SqlDataSource1" DataTextField="ELT_DESCR" DataValueField="ELT_ID" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Min. Months for Eligibility</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtEligMinMonths" runat="server" ></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtEligMinMonths"
                                CssClass="error" ErrorMessage="Please Enter Min. Months for Eligibility"
                                ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Period for Which the Leave Rule is Applicable</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFrom"
                                CssClass="error" ErrorMessage="Please Select From date" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left"><span class="field-label">To</span> </td>
                        <td>
            <asp:TextBox ID="txtTo" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                      <br />
                            <asp:CheckBox ID="chkPaid" runat="server" Text="Paid" CssClass="field-label" /></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Allowed Days Per Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDaysperYear" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDaysperYear"
                                CssClass="error" ErrorMessage="Please Enter Allowed Days Per Year" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left" width="20%"><span class="field-label">Maximum Consecutive Days</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDayspermonth" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDayspermonth"
                                CssClass="error" ErrorMessage="Please Enter From Allowed Leave Days Per Month"
                                ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Full Paid Days Per Year</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFullpaidDays" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtHalfpaidDays"
                                CssClass="error" ErrorMessage="Please Enter Full Paid Days Per Year" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left"><span class="field-label">Half Paid Days Per Year</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtHalfpaidDays" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDaysperYear"
                                CssClass="error" ErrorMessage="Please Enter Half Paid Days Per Year " ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Max Allowed Days W/O Break of Service</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtMaxwoBos" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtMaxwoBos"
                                CssClass="error" ErrorMessage="Please Enter Max Allowed W/O Break of Service"
                                ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left"><span class="field-label">Minimum Leave Days</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtMinLeavedays" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkLeaveperiod" runat="server" Text="Enforce Leave (Annual leave)" CssClass="field-label" />
                        </td>
                        <td align="left"><span class="field-label">Salary Split Months For Vacation(Separated by Commas) eg:-5,6</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtSalsplitup" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select the Vacation Period</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtVDateFrom" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgVfrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left"><span class="field-label">To</span></td> 
                        <td align="left"><asp:TextBox ID="txtVdateto" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgVto" runat="server" ImageUrl="~/Images/calendar.gif" /><br />
                       &nbsp;<asp:CheckBox ID="chkEosCalc" runat="server" Text="Include in EOS Calculation" CssClass="field-label" /></td>

                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Max Carry Forward</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtMaxcarryforward" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtMaxcarryforward"
                                CssClass="error" ErrorMessage="Please Enter Max Carry Forward " ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left"><span class="field-label">Max Encash Days</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtMAxencashdays" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtMAxencashdays"
                                CssClass="error" ErrorMessage="Please Enter Max Encash Days" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>

                    <tr>

                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkHolidaysareleave" runat="server" Text="Holidays in the Leave Period Counted With Leave" CssClass="field-label" /></td>
                        <td align="left">
                            <asp:CheckBox ID="chkAccumulated" runat="server" Text="Leave By Accrual" CssClass="field-label" />
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="chkLeaveSalary" runat="server" Text="Leave Salary" CssClass="field-label" />
                        </td>

                    </tr>
                     <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg"><span class="field-label">Daily Leave Accrual</span></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Leave Slab 1</span></td>
                        <td align="left"><span class="field-label"> Days From</span>
              <asp:TextBox ID="txtRateFrom1" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtRateFrom1"
                                CssClass="error" ErrorMessage="Please Enter Rate From 1" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left"><span class="field-label">To</span>
              <asp:TextBox ID="txtRateTo1" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtRateTo1"
                                CssClass="error" ErrorMessage="Please Enter Rate To 1" ValidationGroup="main">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left"><span class="field-label">Rate</span> <span style="color: Red">*</span><asp:TextBox ID="txtRate1" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtRate1"
                                CssClass="error" ErrorMessage="Please Enter Rate 1" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">LeaveSlab 2</span></td>
                        <td align="left"><span class="field-label">Days From</span>
              <asp:TextBox ID="txtRateFrom2" runat="server"></asp:TextBox></td>
                        <td align="left"><span class="field-label">To</span>
              <asp:TextBox ID="txtRateTo2" runat="server"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">Rate</span>
             <asp:TextBox ID="txtRate2" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">LeaveSlab 3</span></td>
                        <td align="left"><span class="field-label">Days From</span>
              <asp:TextBox ID="txtRateFrom3" runat="server" ></asp:TextBox></td>
                        <td align="left"><span class="field-label">To</span>
              <asp:TextBox ID="txtRateTo3" runat="server"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">Rate</span>
             <asp:TextBox ID="txtRate3" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr class="title-bg" runat="server" id="tr_AnnualHd">
                        <td colspan="4" align="left">Select Salary Comopnents</td>
                    </tr>
                    <tr runat="server" id="tr_AnnualDt">
                        <td align="left" colspan="4">
                           <div class="checkbox-list-full"> <asp:CheckBoxList ID="cblSplitup" runat="server" DataSourceID="listSplit" DataTextField="ERN_DESCR"
                                DataValueField="ERN_ID" RepeatColumns="4" RepeatDirection="Horizontal" >
                            </asp:CheckBoxList> </div>
                            <asp:SqlDataSource ID="listSplit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT ERN_ID, ERN_DESCR FROM EMPSALCOMPO_M WHERE (ERN_TYP = 1)"></asp:SqlDataSource>
                        </td>


                    </tr>
                     <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Remarks</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtRemarks"
                                CssClass="error" ErrorMessage="Please Select From date" ValidationGroup="main">*</asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT [ELT_ID], [ELT_DESCR] FROM [EMPLEAVETYPE_M]"></asp:SqlDataSource>
                          
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" UseSubmitBehavior="False" ValidationGroup="main" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_catid" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgVfrom" TargetControlID="txtVDateFrom">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgVto" TargetControlID="txtVdateto">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>
