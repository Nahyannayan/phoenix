﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GemBox.Spreadsheet
Imports UtilityObj
Imports Encryption64
'Version        Date             Author          Change
'1.1            20-Feb-2011      Swapna         To display all employees leaves
'1.2            27-Jul-2011      Swapna         To display year end date
Partial Class Payroll_empLeaveDetailsView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As ScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(Me.btnExcelto)
        If Page.IsPostBack = False Then
            ViewState("ExportExcelFlag") = 0
            Page.Title = OASISConstants.Gemstitle
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                rblOptions.SelectedValue = "All"
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                txtFromDate.Text = Format(Today.Date, "dd/MMM/yyyy")
                Dim newDate As DateTime = DateTime.Now.AddMonths(1)
                txtToDate.Text = Format(newDate, "dd/MMM/yyyy")
                'txtToDate.Text = Format(DateAdd(DateInterval.Month, 1, Today.Date), "dd/MMM/yyyy")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P130076" And ViewState("MainMnu_code") <> "P130077") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    set_Menu_Img()

                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    'Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    gridbind()
    'End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub gridbind()
        Try

            If txtFromDate.Text = "" Or txtToDate.Text = "" Then
                lblError.Text = "Enter dates correctly"
                Exit Sub
            End If
            If CDate(txtFromDate.Text) > CDate(txtToDate.Text) Then
                lblError.Text = "Invalid dates"
                Exit Sub
            End If

            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl, str_txtdept, str_filter_Leave, str_filter_Dept As String

            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            'str_filter_bankcash = ""

            'str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim txtSearch As New TextBox
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_filter_Leave = ""
            str_filter_Dept = ""
            str_txtdept = ""
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_txtCode <> "" Then
                    str_filter_code = set_search_filter("EMPNO", str_search, txtSearch.Text)
                End If

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtEmpName")
                str_txtName = txtSearch.Text
                If str_txtName <> "" Then
                    str_filter_name = set_search_filter("EMPNAME", str_search, txtSearch.Text)
                End If

                ''leave type

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtLeaveType")
                str_txtControl = txtSearch.Text
                If str_txtControl <> "" Then
                    str_filter_Leave = set_search_filter("LeaveType", str_search, txtSearch.Text)
                End If

                ''Department

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup.HeaderRow.FindControl("txtDept")
                str_txtdept = txtSearch.Text
                If str_txtdept <> "" Then
                    str_filter_Dept = set_search_filter("EMP_DEPT_DESCR", str_search, txtSearch.Text)
                End If

            End If

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = str_filter_code & str_filter_name & str_filter_Leave & str_filter_Dept
            If rblOptions.SelectedValue <> "All" Then
                str_Sql = str_Sql & "and Status= '" & rblOptions.SelectedValue & "'"
            End If

            '= " SELECT RSS_ID ,RSS_LNE_TYP ,RSS_DESCR ,RSS_TYP" _
            '    & " FROM RPTSETUP_S WHERE" _
            '    & " RSS_TYP='C' AND RSS_LNE_TYP='N'"
            'Dim str_emp_id As String = ""
            'If Request.QueryString("id") <> "" Then
            '    str_emp_id = "and ED.EMD_EMP_ID='" & Request.QueryString("id") & "'"
            'End If



            'Dim objConn As New SqlConnection(str_conn)
            Dim param(6) As SqlClient.SqlParameter
            'Dim SqlCmd As New SqlCommand("rptEmployeeLeaves")
            'SqlCmd.CommandType = CommandType.StoredProcedure
            'SqlCmd.Parameters.AddWithValue("@DTFROM", txtFromDate.Text)
            'SqlCmd.Parameters.AddWithValue("@DTTO", txtToDate.Text)

            param(0) = New SqlClient.SqlParameter("@DTFROM", txtFromDate.Text)
            param(1) = New SqlClient.SqlParameter("@DTTO", txtToDate.Text)
            param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            param(3) = New SqlClient.SqlParameter("@usr_ID", Session("sUsr_name"))
            param(4) = New SqlClient.SqlParameter("@Filter", str_Sql)
            param(5) = New SqlClient.SqlParameter("@Menu_ID", ViewState("MainMnu_code").ToString)
            Dim ds As New DataSet
            If ViewState("ExportExcelFlag") = 1 Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rptEmployeeLeaves_ExportToExcel", param)
                If ds.Tables(0).Rows.Count = 0 Then
                    lblError.Text = " No Data To Export"
                Else
                    ExportExcel(ds)
                End If
            Else
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rptEmployeeLeaves", param)
                gvGroup.DataSource = ds
                If ds.Tables(0).Rows.Count = 0 Then
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    gvGroup.DataBind()
                    Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                    gvGroup.Rows(0).Cells.Clear()
                    gvGroup.Rows(0).Cells.Add(New TableCell)
                    gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                    ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                    'gvGroup.HeaderRow.Visible = True
                    If ViewState("ExportExcelFlag") = 1 Then
                        lblError.Text = " No Data To Export"
                    End If
                Else
                    gvGroup.DataBind()
                    sp_message.InnerHtml = ""
                    If ViewState("ExportExcelFlag") = 1 Then
                        ExportExcel(ds)
                    End If
                End If
                'If Request.QueryString("bankcash") <> "" Then
                '    gvGroup.Columns(4).Visible = False
                'End If
                ''  gvGroup.Columns(0).Visible = False

                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                txtSearch.Text = str_txtCode
                txtSearch = gvGroup.HeaderRow.FindControl("txtEmpName")
                txtSearch.Text = str_txtName
                txtSearch = gvGroup.HeaderRow.FindControl("txtLeaveType")
                txtSearch.Text = str_txtControl
                txtSearch = gvGroup.HeaderRow.FindControl("txtDept")
                txtSearch.Text = str_txtdept


                set_Menu_Img()
                'below code written by vikranth on 30th Oct 2019
                Dim imgDoc As ImageButton
                For Each grow As GridViewRow In gvGroup.Rows
                    imgDoc = DirectCast(grow.FindControl("imgDoc"), ImageButton)
                    Dim btnImgDoc As HyperLink = DirectCast(grow.FindControl("btnImgDoc"), HyperLink)

                    btnImgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(imgDoc.AlternateText)

                    If imgDoc.AlternateText <> "" Then
                        If imgDoc.AlternateText <> 0 Then
                            imgDoc.Visible = False
                            btnImgDoc.Visible = True
                        Else imgDoc.Visible = False
                            btnImgDoc.Visible = False
                        End If
                    Else
                        imgDoc.Visible = False
                        btnImgDoc.Visible = False
                    End If
                Next
                'end by vikranth
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ExportExcel(ByVal ds As DataSet)

        If (ds.Tables.Count > 0) Then
            If (ds.Tables(0).Rows.Count > 0) Then
                Dim dt As DataTable = ds.Tables(0)
                Dim photopath As String = WebConfigurationManager.AppSettings("UploadExcelFile").ToString
                Dim tempFileName As String = photopath + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile

                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                'ws.InsertDataTable(dt, "A1", True)

                'ef.SaveXls(tempFileName)
                ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                ef.Save(tempFileName)

                Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

                'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(tempFileName)
                'HttpContext.Current.Response.Flush()
                'HttpContext.Current.Response.Close()

                System.IO.File.Delete(tempFileName)
            Else
                lblError.Text = " No Data To Export"
            End If
        End If

    End Sub
    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        gridbind()
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        Try
            If e.Row.RowType <> DataControlRowType.Header Then
                Dim lblELAID As New Label
                Dim lblRemarks As New Label
                Dim lnkRemarks As New LinkButton
                lnkRemarks = TryCast(e.Row.FindControl("lnkRemarks"), LinkButton)
                lblELAID = TryCast(e.Row.FindControl("lblELAID"), Label)
                lblRemarks = TryCast(e.Row.FindControl("lblRemarks"), Label)

                If lblELAID.Text <> "0" Then
                    lnkRemarks.Visible = True
                    lblRemarks.Visible = False
                Else
                    lnkRemarks.Visible = False
                    lblRemarks.Visible = True
                End If
                lblELAID.Visible = False
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnExcelto_Click(sender As Object, e As EventArgs)
        ViewState("ExportExcelFlag") = 1
        gridbind()
    End Sub
    Protected Sub imgDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim imgdoc As ImageButton = sender.parent.findcontrol("imgdoc")

        ' ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Document", "window.showModelessDialog ('" + url + "','popup','dialogWidth:785px;dialogHeight:420px;status=0;resizable=1')", True)
        'OpenFileFromDB(imgdoc.AlternateText)
    End Sub
    Private Sub OpenFileFromDB(ByVal DocId As String)
        Try


            Dim conn As String = ""

            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("OpenDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)

            Dim myReader As SqlDataReader = cmd.ExecuteReader()

            If myReader.Read Then
                Response.ClearHeaders()
                Response.ContentType = myReader("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(myReader("DOC_DOCUMENT"), Byte())
                'Response.Buffer = True
                'Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                'Response.AddHeader("Cache-control", "no-cache")
                Response.AddHeader("content-disposition", "attachment;filename=" & myReader("DOC_DOCNO").ToString())
                Response.Clear()
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.Close()
                Response.End()

            End If
            myReader.Close()

        Catch ex As Exception
            'lblError.Text = ex.Message
            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try

    End Sub
End Class
