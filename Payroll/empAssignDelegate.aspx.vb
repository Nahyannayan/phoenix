Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Payroll_empAssignDelegate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    Private Property DelegateDetail() As DataRow
        Get
            If Not ViewState("DelegateDetail") Is Nothing And ViewState("DelegateDetail").rows.count > 0 Then
                Return ViewState("DelegateDetail").rows(0)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataRow)
            ViewState("DelegateDetail") = value.Table
        End Set
    End Property
    Private Property RecordForwarded() As Boolean
        Get
            Return ViewState("RecordForwarded")
        End Get
        Set(ByVal value As Boolean)
            ViewState("RecordForwarded") = value
        End Set
    End Property
    Private Property RecordDisabled() As Boolean
        Get
            Return ViewState("RecordDisabled")
        End Get
        Set(ByVal value As Boolean)
            ViewState("RecordDisabled") = value
        End Set
    End Property
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "U000036" And ViewState("MainMnu_code") <> "P130019") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                filldrp()
                If Request.QueryString("viewid") <> "" Then
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    SetDataMode("view")
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                    h_BSU_ID.Value = Session("sBsuid")
                    txtBSUName.Text = Mainclass.GetBSUName(h_BSU_ID.Value)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)
        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub
    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT EJM_ID,EJM_DESCR FROM EMPJOB_M   ORDER BY EJM_DESCR "
            fillDropdown(ddJobType, Query, "EJM_DESCR", "EJM_ID", "OASISConnectionString", False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            DelegateDetail = GetDelegateDetail(p_Modifyid)
            If DelegateDetail Is Nothing Then Exit Sub
            h_EJD_ID.Value = DelegateDetail("EJD_ID").ToString
            txtBSUName.Text = DelegateDetail("BSU_NAME").ToString
            ddJobType.SelectedValue = DelegateDetail("EJD_EJM_ID").ToString
            h_BSU_ID.Value = DelegateDetail("EJD_BSU_ID").ToString
            h_EmpID.Value = DelegateDetail("EJD_EMP_ID").ToString
            h_DelegateEmpID.Value = DelegateDetail("EJD_DEL_EMP_ID").ToString
            txtEmployee.Text = DelegateDetail("EMP_NAME").ToString
            txtDelegateEmp.Text = DelegateDetail("DEL_EMP_NAME").ToString
            h_BSU_ID.Value = DelegateDetail("EJD_BSU_ID").ToString
            If IsDate(DelegateDetail("EJD_DTFROM")) Then
                txtFromDate.Text = Format(DelegateDetail("EJD_DTFROM"), "dd/MMM/yyyy")
            Else
                txtFromDate.Text = ""
            End If
            If IsDate(DelegateDetail("EJD_DTTO")) Then
                txtToDate.Text = Format(DelegateDetail("EJD_DTTO"), "dd/MMM/yyyy")
            Else
                txtToDate.Text = ""
            End If
            txtRemarks.Text = DelegateDetail("EJD_REMARKS").ToString
            txtLoggedInuser.Text = DelegateDetail("EJD_ENTEREDBY").ToString
            chkDisable.Checked = DelegateDetail("EJD_bDisable")
            chkForward.Checked = DelegateDetail("EJD_bForward")
            RecordForwarded = DelegateDetail("EJD_bForward")
            RecordDisabled = DelegateDetail("EJD_bDisable")
            If RecordDisabled Then
                trDisabledDate.Visible = True
                If IsDate(DelegateDetail("EJD_DisableDate")) Then
                    txtDisabledDate.Text = Format(DelegateDetail("EJD_DisableDate"), "dd/MMM/yyyy")
                Else
                    txtDisabledDate.Text = ""
                End If
            Else
                trDisabledDate.Visible = False
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        chkDisable.Enabled = Not mDisable And Not RecordDisabled
        chkDisable.Visible = RecordForwarded
        btnSave.Enabled = Not mDisable And Not RecordDisabled
        mDisable = mDisable Or RecordForwarded
        btnAdd.Enabled = mDisable
        btnEdit.Enabled = mDisable And Not RecordDisabled And txtLoggedInuser.Text.ToLower = Session("sUsr_name").ToLower
        btnDelete.Enabled = mDisable And Not RecordForwarded And txtLoggedInuser.Text.ToLower = Session("sUsr_name").ToLower
        ddJobType.Enabled = Not mDisable
        txtFromDate.Enabled = Not mDisable
        txtToDate.Enabled = Not mDisable
        imgEmployee.Enabled = Not mDisable And ViewState("MainMnu_code") <> "U000036"
        imgDelegateEmp.Enabled = Not mDisable
        txtEmployee.Enabled = Not mDisable And ViewState("MainMnu_code") <> "U000036"
        txtDelegateEmp.Enabled = Not mDisable
        imgFromDate.Enabled = Not mDisable
        imgToDate.Enabled = Not mDisable
        txtRemarks.Enabled = Not mDisable
        chkForward.Enabled = Not mDisable
    End Sub
    Sub clear_All()
        h_EJD_ID.Value = ""
        txtBSUName.Text = ""
        ddJobType.SelectedValue = Nothing
        h_BSU_ID.Value = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtRemarks.Text = ""
        h_EmpID.Value = ""
        h_DelegateEmpID.Value = ""
        txtEmployee.Text = ""
        txtDelegateEmp.Text = ""
        chkDisable.Checked = False
        chkForward.Checked = False
        RecordDisabled = False
        RecordForwarded = False
        h_EmpID.Value = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
        txtEmployee.Text = EOS_MainClass.GetEmployeeNameFromID(h_EmpID.Value)
        txtLoggedInuser.Text = Session("sUsr_name")
        txtDisabledDate.Text = ""
        trDisabledDate.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        h_BSU_ID.Value = Session("sBsuid")
        txtBSUName.Text = Mainclass.GetBSUName(h_BSU_ID.Value)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            retval = DeleteDelegateDetail(h_EJD_ID.Value)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Deleted Successfully !!!!"
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = retval
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim FromDate As String = txtFromDate.Text.Trim
            If Not IsDate(FromDate) Then
                lblError.Text = "Invalid From Date !!!!"
                Exit Sub
            End If
            Dim ToDate As String = txtToDate.Text.Trim
            If Not IsDate(ToDate) Then
                lblError.Text = "Invalid To Date !!!!"
                Exit Sub
            End If
            If CDate(FromDate) > CDate(ToDate) Then
                lblError.Text = "From Date is greater than To Date !!!!"
                Exit Sub
            End If
            If h_EmpID.Value = "" Or txtEmployee.Text = "" Then
                lblError.Text = "Select the Employee !!!!"
                Exit Sub
            End If
            If h_DelegateEmpID.Value = "" Or txtDelegateEmp.Text = "" Then
                lblError.Text = "Select the Delegate Employee !!!!"
                Exit Sub
            End If
            If DelegateDetail Is Nothing Then Exit Sub
            If ViewState("datamode") = "add" Then
                DelegateDetail("EJD_ID") = 0
                h_EJD_ID.Value = 0
            Else
                DelegateDetail("EJD_ID") = h_EJD_ID.Value
            End If
            DelegateDetail("EJD_BSU_ID") = h_BSU_ID.Value
            DelegateDetail("EJD_EMP_ID") = h_EmpID.Value
            DelegateDetail("EJD_DEL_EMP_ID") = h_DelegateEmpID.Value
            DelegateDetail("EJD_ENTEREDBY") = Session("sUsr_name")
            DelegateDetail("EJD_EJM_ID") = ddJobType.SelectedValue
            DelegateDetail("EJD_REMARKS") = txtRemarks.Text
            DelegateDetail("EJD_DTFROM") = FromDate
            DelegateDetail("EJD_DTTO") = ToDate
            DelegateDetail("EJD_bDisable") = chkDisable.Checked
            DelegateDetail("EJD_bForward") = chkForward.Checked

            Dim retval As String
            Dim EJD_ID As String = ""
            retval = SaveDelegateDetail(DelegateDetail, EJD_ID)
            If Not RecordForwarded And chkForward.Checked Then SendEmailNotification()
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, h_EJD_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Saved Successfully !!!"
                h_EJD_ID.Value = EJD_ID
                setModifyHeader(EJD_ID)
                SetDataMode("view")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub SendEmailNotification()
        Try
            Dim sql As String
            sql = "select EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' '  + EMP_LNAME [EMP_NAME],isnull(EMD_EMAIL  ,'')EMP_EMAIL"
            sql &= " FROM EMPLOYEE_M LEFT OUTER JOIN EMPLOYEE_D on EMP_ID=EMD_EMP_ID "
            sql &= " where EMP_ID =" & h_DelegateEmpID.Value & " OR EMP_ID IN (SELECT EMP_REPORTTO_EMP_ID  FROM EMPLOYEE_M where EMP_ID=" & h_EmpID.Value & ")"
            Dim dtEmail As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            dtEmail = Mainclass.getDataTable(sql, str_conn)
            Dim FromDesg, ToDesig As String
            FromDesg = Mainclass.getDataValue("Select DES_DESCR FROM EMPLOYEE_M LEFT OUTER JOIN EMPDESIGNATION_M on DES_ID=EMP_DES_ID  WHERE EMP_ID=" & h_EmpID.Value, "OASISConnectionString")
            ToDesig = Mainclass.getDataValue("Select DES_DESCR FROM EMPLOYEE_M LEFT OUTER JOIN EMPDESIGNATION_M on DES_ID=EMP_DES_ID  WHERE EMP_ID=" & h_DelegateEmpID.Value, "OASISConnectionString")
            Dim FromEmailID, ToEmailID As String
            FromEmailID = "communication@gemsedu.com"
            ToEmailID = ""
            Dim MainText As String = ""
            Dim Subject As String = ""
            Dim mRow As DataRow
            Dim dtComm As New DataTable
            dtComm = GetCommunicationSettings()
            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""
            If dtComm.Rows.Count > 0 Then
                username = dtComm.Rows(0).Item("BSC_USERNAME").ToString()
                password = dtComm.Rows(0).Item("BSC_PASSWORD").ToString()
                port = dtComm.Rows(0).Item("BSC_PORT").ToString()
                host = dtComm.Rows(0).Item("BSC_HOST").ToString()
            End If
            Dim Mailstatus As String = ""
            For Each mRow In dtEmail.Rows
                Dim sb As New StringBuilder
                ToEmailID = mRow("EMP_EMAIL")
                If ToEmailID = "" Then
                    Continue For
                End If
                Subject = "Delegation of OASIS Approval Rights  (ALERT) "
                sb.Append("<table border='0' width='100%' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & mRow("EMP_NAME") & " ,</td></tr>")
                sb.Append("<tr><td ><br /><br /></td></tr>")
                sb.Append("<tr><td><br />Subject :  " & Subject & " </td></tr>")
                sb.Append("<tr><td><br />Job Type :  " & ddJobType.SelectedItem.Text & " </td></tr>")
                sb.Append("<tr><td><br />Business Unit :  " & txtBSUName.Text & "</td></tr>")
                sb.Append("<tr><td><br />Applicable Period :  " & txtFromDate.Text & " to " & txtToDate.Text & " </td></tr>")
                sb.Append("<tr><td><br />Delegated From :  " & txtEmployee.Text & " (" & FromDesg & ")</td></tr>")
                sb.Append("<tr><td><br />Delegated To :  " & txtDelegateEmp.Text & " (" & ToDesig & ")</td></tr>")
                sb.Append("<tr><td><br />Remarks :  " & txtRemarks.Text & " </td></tr>")
                sb.Append("<tr><td><br />Entered By :  " & EOS_MainClass.GetEmployeeNameFromID(EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))) & " </td></tr>")
                sb.Append("<tr><td ><br /><br />Regards,</td></tr>")
                sb.Append("<tr><td><b>Team OASIS.</b></td></tr>")
                sb.Append("<tr><td></td></tr>")
                sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
                sb.Append("<tr></tr><tr></tr>")
                sb.Append("</table>")
                Try
                    '  ToEmailID = "shakeel.shakkeer@gemseducation.com"
                    Mailstatus = EmailService.email.SendPlainTextEmails(FromEmailID, ToEmailID, Subject, sb.ToString, username, password, host, port, 0, False)
                Catch ex As Exception
                Finally
                    Mainclass.SaveEmailSendStatus(Session("sBsuid").ToString, "Job Delegation", "", ToEmailID, Subject, sb.ToString, IIf(LCase(Mailstatus).StartsWith("error"), 0, 1), Mailstatus)
                End Try
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Public Function GetCommunicationSettings() As DataTable
        Dim ds As DataSet
        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
        Return ds.Tables(0)
    End Function
    Public Shared Function GetDelegateDetail(ByVal EJD_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EJD_ID", EJD_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("GetDelegateDetail", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveDelegateDetail(ByVal DelegateDetail As DataRow, ByRef EJD_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(11) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EJD_ID", DelegateDetail("EJD_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@EJD_BSU_ID", DelegateDetail("EJD_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@EJD_EJM_ID", DelegateDetail("EJD_EJM_ID"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@EJD_DTFROM", DelegateDetail("EJD_DTFROM"), SqlDbType.DateTime)
            sqlParam(4) = Mainclass.CreateSqlParameter("@EJD_DTTO", DelegateDetail("EJD_DTTO"), SqlDbType.DateTime)
            sqlParam(5) = Mainclass.CreateSqlParameter("@EJD_EMP_ID", DelegateDetail("EJD_EMP_ID"), SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@EJD_DEL_EMP_ID", DelegateDetail("EJD_DEL_EMP_ID"), SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@EJD_REMARKS", DelegateDetail("EJD_REMARKS"), SqlDbType.VarChar)
            sqlParam(8) = Mainclass.CreateSqlParameter("@EJD_ENTEREDBY", DelegateDetail("EJD_ENTEREDBY"), SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            sqlParam(10) = Mainclass.CreateSqlParameter("@EJD_bDisable", DelegateDetail("EJD_bDisable"), SqlDbType.Bit)
            sqlParam(11) = Mainclass.CreateSqlParameter("@EJD_bForward", DelegateDetail("EJD_bForward"), SqlDbType.Bit)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "SaveDelegateDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(9).Value = "" Then
                SaveDelegateDetail = ""
                EJD_ID = sqlParam(0).Value
            Else
                SaveDelegateDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(9).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteDelegateDetail(ByVal EJD_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EJD_ID", EJD_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "DeleteDelegateDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteDelegateDetail = ""
            Else
                DeleteDelegateDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
End Class
