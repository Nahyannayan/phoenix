<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empMonthlyE_D.aspx.vb" Inherits="Payroll_empMonthlyE_D" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <%--    <script language="javascript" type="text/javascript">
        function getEmpID(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 495px; ";
            sFeatures += "dialogHeight: 410px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var payYear, payMonth;
            url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'EN') {
                payMonth = document.getElementById("<%=ddlPayMonth.ClientID %>").value;
                payYear = document.getElementById("<%=ddlPayYear.ClientID %>").value;
                url = "../Accounts/accShowEmpDetail.aspx?id=EN"; //&payMonth=" + payMonth + "&PAYYEAR=" + payYear;
                result = window.showModalDialog(url, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                NameandCode = result.split('___');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_EDD_EMP_ID.ClientID %>").value = NameandCode[1];

            }
            if (mode == 'DT') {
                if (document.getElementById("<%=h_Deduction.ClientID %>").value == "DEDUCTION") {
                    url = '../Accounts/accShowEmpDetail.aspx?id=DT';
                }
                else {
                    url = '../Accounts/accShowEmpDetail.aspx?id=ET';
                }
                result = window.showModalDialog(url, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                NameandCode = result.split('___');
                document.getElementById("<%=txtType.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfTypeID.ClientID %>").value = NameandCode[1];
            }
        }
    </script>--%>

    <script>
        function getEmpID(mode) {
            var url;
            document.getElementById("<%=hf_mode.ClientID%>").value = mode

            if (mode == 'EN') {
                url = "../Accounts/accShowEmpDetail.aspx?id=EN"

            }
            if (mode == 'DT') {
                if (document.getElementById("<%=h_Deduction.ClientID %>").value == "DEDUCTION") {
                    url = '../Accounts/accShowEmpDetail.aspx?id=DT';
                }
                else {
                    url = '../Accounts/accShowEmpDetail.aspx?id=ET';
                }
            }
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (document.getElementById("<%=hf_mode.ClientID%>").value == "EN") {
                            document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_EDD_EMP_ID.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');
                        }
                        else {
                            document.getElementById("<%=txtType.ClientID %>").value = NameandCode[0];
                            document.getElementById("<%=hfTypeID.ClientID %>").value = NameandCode[1];
                        }
                    }
                }
                function autoSizeWithCalendar(oWindow) {
                    var iframe = oWindow.get_contentFrame();
                    var body = iframe.contentWindow.document.body;

                    var height = body.scrollHeight;
                    var width = body.scrollWidth;

                    var iframeBounds = $telerik.getBounds(iframe);
                    var heightDelta = height - iframeBounds.height;
                    var widthDelta = width - iframeBounds.width;

                    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                    oWindow.center();
                }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblMonthly" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblRowNum" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table width="100%">
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Month</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlPayMonth" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left"  width="20%"><span class="field-label">Year</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlPayYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Employee Name</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtEmpName" runat="server" AutoPostBack="true" OnTextChanged="txtEmpName_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp_Name" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                            OnClientClick="getEmpID('EN');return false;" />
                                        <asp:RequiredFieldValidator ID="rfvEmpName" runat="server" ControlToValidate="txtEmpName"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Employee name required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" ><span class="field-label">Employee ID</span></td>
                                    <td align="left" >

                                        <asp:TextBox ID="txtEmpID" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="lblDedType" runat="server" Text="Deduction  Type" CssClass="field-label"></asp:Label></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtType" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnFixAsset" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                            OnClientClick="getEmpID('DT');return false;" />


                                        <asp:RequiredFieldValidator ID="rfvType" runat="server"
                                            ControlToValidate="txtType" CssClass="error" Display="Dynamic" ErrorMessage="Deduction Type required"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" ><span class="field-label">Amount</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Amount entry required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvAmount" runat="server" ControlToValidate="txtAmount"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Enter valid amount" ForeColor=""
                                            Operator="DataTypeCheck" Type="Double" ValidationGroup="groupM1">*</asp:CompareValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Remark</span></td>
                                    <td align="left"  colspan="2">
                                        <asp:TextBox ID="txtRemark" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                            TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ControlToValidate="txtRemark"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Remark can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="2">
                                        <asp:Button ID="btnFill" runat="server" CssClass="button" Text="Add" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnChildCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                </tr>
                                <tr id="trExcelUpload" runat="server">
                                    <td align="left" ><span class="field-label">Bulk Upload</span></td>
                                    <td align="left"  colspan="3">
                                        <asp:FileUpload ID="flUpExcel" runat="server" />

                                        <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="4">
                                        <asp:GridView ID="gvMonthE_D" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                            Width="100%" CssClass="table table-row table-bordered">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EDD_EMP_ID" HeaderText="Emp ID" Visible="False">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_EMPNO" HeaderText="EmpNO">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_EMP_Name" HeaderText="Employee Name">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_ERNCODE" HeaderText="EDD_ Code" Visible="False">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode_Descr" runat="server" Text='<%# Bind("Code_Descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EDD_Paymonth" HeaderText="EDD_Month" Visible="False">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Desc_month" HeaderText="Month" />
                                                <asp:BoundField DataField="EDD_PayYear" HeaderText="Year">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_Amount" HeaderText="Amount">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_REMARKS" HeaderText="Remark">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_Type" HeaderText="TYPE" Visible="False">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_ID" HeaderText="EDD_ID" Visible="False">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_CUR_ID" HeaderText="Currency" Visible="False" />
                                                <asp:BoundField DataField="EDD_BSU_ID" HeaderText="BSU" Visible="False" />
                                                <asp:TemplateField HeaderText="Edit">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="EditBtn" runat="server" OnClick="EditBtn_Click">  Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="DeleteBtn" runat="server" CommandName="Delete" OnClick="DeleteBtn_Click">
         Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">
                                        <asp:Label ID="lblMonthlysub" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="4">

                                        <asp:GridView ID="gvOldData" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                            DataKeyNames="id" Width="100%" EmptyDataText="No Details Added">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                    <ItemStyle Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EDD_EMP_ID" HeaderText="Emp ID" Visible="False">
                                                    <HeaderStyle Width="15%" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_EMP_Name" HeaderText="Employee Name">
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_ERNCODE" HeaderText="EDD_ Code" Visible="False">
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode_Descr" runat="server" Text='<%# Bind("Code_Descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EDD_Paymonth" HeaderText="EDD_Month" Visible="False">
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Desc_month" HeaderText="Month" />
                                                <asp:BoundField DataField="EDD_PayYear" HeaderText="Year">
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_Amount" HeaderText="Amount">
                                                    <HeaderStyle Width="15%" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_REMARKS" HeaderText="Remark">
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_Type" HeaderText="TYPE" Visible="False">
                                                    <HeaderStyle Width="15%" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_ID" HeaderText="EDD_ID" Visible="False">
                                                    <HeaderStyle Width="15%" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EDD_CUR_ID" HeaderText="Currency" Visible="False" />
                                                <asp:BoundField DataField="EDD_BSU_ID" HeaderText="BSU" Visible="False" />
                                                <asp:TemplateField HeaderText="Edit" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="EditBtn" runat="server" OnClick="EditBtn_Click">  Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                    <ItemStyle Width="15%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="DeleteBtn" runat="server" CommandName="Delete" OnClick="DeleteBtn_Click">
         Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfEmp_ID" runat="server" />
                <asp:HiddenField ID="hfTypeID" runat="server" />
                <asp:HiddenField ID="hfEDD_ID" runat="server" />
                <asp:HiddenField ID="h_Deduction" runat="server" />
                <asp:HiddenField ID="h_EDD_EMP_ID" runat="server" />
                <asp:HiddenField ID="hffileName" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
