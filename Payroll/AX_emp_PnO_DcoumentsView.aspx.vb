﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
'Version        Date             Author          Change
'1.1            11-Oct-2018     Swapna          To filter view screen based on logged in user's id
Partial Class Payroll_AX_emp_PnO_DcoumentsView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "AX_emp_PnODocumentRequests.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'gvDocuments.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "U000025" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    GetLoggedInEmployeeDetails()
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub
    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            'str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "'"     'V1.1 comment
            str_Sql = " exec DAX.GetEmployeePnO_DocumentsRequestList '" & Session("SbSUID") & "','" & h_Emp_No.Value & "',0 "   ''" & Session("SbSUID") & "','" & Session("sUsr_name") & "'"    'V1.1
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvDocuments.DataSource = ds.Tables(0)
            gvDocuments.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Public Sub GetLoggedInEmployeeDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT " _
         & " EM.EMP_ID, ISNULL(EM.EMP_FNAME,'')+' '+" _
         & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
         & " AS EMP_NAME,EMP_BSU_ID  FROM  EMPLOYEE_M EM INNER JOIN USERS_M" _
         & " ON EM.EMP_ID=USERS_M.USR_EMP_ID" _
         & " WHERE USERS_M.USR_NAME ='" & Session("sUsr_name") & "' and em.emp_bsu_id ='" & Session("sBsuid") & "'"
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then

            h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
            '  h_emp_bsu.Value = ds.Tables(0).Rows(0)("EMP_BSU_ID")
        Else
            lblError.Text = getErrorMessage("3000") '"Please log in to your business unit to make P&O document requests."
            hlAddNew.Visible = False
            Exit Sub

        End If

    End Sub

    Protected Sub gvDocuments_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvDocuments.RowDataBound
        Try
            Dim lblRequestID As New Label
            lblRequestID = TryCast(e.Row.FindControl("lblRequestID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblRequestID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "AX_emp_PnODocumentRequests.aspx?viewid=" & Encr_decrData.Encrypt(lblRequestID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
           

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
