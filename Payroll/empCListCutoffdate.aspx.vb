﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Payroll_empCListCutoffdate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P050225") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    bind_academicyear()
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub bind_academicyear()
        ddlacademicyear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ACY_ID,ACY_DESCR FROM DBO.ACADEMICYEAR_M WHERE ACY_ID>=27 "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlacademicyear.DataSource = ds
        ddlacademicyear.DataTextField = "ACY_DESCR"
        ddlacademicyear.DataValueField = "ACY_ID"
        ddlacademicyear.DataBind()
        

    End Sub
    Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACY_ID", ddlacademicyear.SelectedValue)
           

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_CListCutoffDate", pParms)
            If ds.Tables(0).Rows.Count > 0 Then
                gvCLIST.DataSource = ds.Tables(0)
                gvCLIST.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCLIST.DataSource = ds.Tables(0)
                Try
                    gvCLIST.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvCLIST.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvCLIST.Rows(0).Cells.Clear()
                gvCLIST.Rows(0).Cells.Add(New TableCell)
                gvCLIST.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCLIST.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCLIST.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlacademicyear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlacademicyear.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        lblError.Text = ""
      
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim i As Integer
        i = 0
        For Each row As GridViewRow In gvCLIST.Rows
            Dim lblid As New Label
            Dim lblmnth As New Label
            Dim lblyear As New Label
            Dim txtCut As New TextBox

            lblid = row.FindControl("lblid")
            lblmnth = row.FindControl("lblmnth")
            lblyear = row.FindControl("lblyear")
            txtCut = row.FindControl("txtCut")

            If ((txtCut.Text = "")) Then
                i = i + 1
            End If
            Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@CCD_MONTH", lblmnth.Text)
            pParms(1) = New SqlClient.SqlParameter("@CCD_YEAR", lblyear.Text)
            pParms(2) = New SqlClient.SqlParameter("@CCD_DATE", txtCut.Text)
            pParms(3) = New SqlClient.SqlParameter("@CCD_ACY_ID", ddlacademicyear.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_CLISTCUTOFFDATE", pParms)


        Next
        If gvCLIST.Rows.Count = i Then
            lblError.Text = "Please enter Value"
        Else
            lblError.Text = "Record Saved sucessfully"
            gridbind()
        End If
    End Sub
End Class
