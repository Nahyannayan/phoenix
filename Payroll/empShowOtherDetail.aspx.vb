Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Payroll_empShowOtherDetail
    Inherits System.Web.UI.Page
    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then
            Try
                SearchMode = Request.QueryString("id")
                If SearchMode = "Q" Then
                    Page.Title = "Qualification Info"

                ElseIf SearchMode = "SG" Then

                    ' gvEmpInfo.Columns(3).Visible = False

                    Page.Title = "Salary Scale Info"
                End If
                Page.DataBind()

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                If SearchMode = "Q" Then

                End If

                gridbind()
            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
        'If SearchMode = "Q" Then
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid3(str_Sid_img(2))
        ' End If
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            SearchMode = Request.QueryString("id")
            Dim str_filter_code As String
            Dim str_filter_name As String


            If SearchMode = "Q" Then
                'Page.Title = "Bank Info"
                str_Sql = "Select ID,E_Name,Cat,Subject from(SELECT QLF_ID as ID,QLF_DESCR as E_Name,Category as Cat,Subject as Subject FROM VW_OSO_QUALIFICATION)a where a.id<>'' "

            ElseIf SearchMode = "SG" Then

                Page.Title = "Salary Scale"
                ' gvEmpInfo.Columns(3).Visible = False
                str_Sql = "Select ID,E_Name,Cat,Subject from(SELECT    EMPSCALES_GRADES_M.SGD_ID as ID, 0 as Subject, EMPSCALES_GRADES_M.SGD_DESCR as E_name, " & _
                      " EMPGRADES_M.EGD_DESCR as Cat FROM  EMPSCALES_GRADES_M INNER JOIN " & _
                      " EMPGRADES_M ON EMPSCALES_GRADES_M.SGD_EGD_ID = EMPGRADES_M.EGD_ID)a where a.id<>'' "

            End If

            Dim ds As New DataSet
            Dim i As Integer
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtID As New TextBox
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtName, str_txtCat As String
            Dim str_search, str_filter_Cat, str_filter_Sub, str_txtSub As String
            str_txtCode = ""
            str_txtName = ""
            str_txtCat = ""
            str_txtSub = ""
            str_filter_Sub = ""
            str_filter_Cat = ""
            str_filter_code = ""
            str_filter_name = ""

            If gvEmpInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                str_txtCode = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcat")
                str_txtCat = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Cat = " AND a.Cat LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Cat = "  AND  NOT a.Cat LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Cat = " AND a.Cat  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Cat = " AND a.Cat  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Cat = " AND a.Cat LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Cat = " AND a.Cat NOT LIKE '%" & txtSearch.Text & "'"
                End If

                '  If SearchMode = "Q" Then



                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtSub")
                str_txtSub = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Sub = " AND a.Subject LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Sub = "  AND  NOT  a.Subject LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Sub = " AND  a.Subject  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Sub = " AND  a.Subject  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Sub = " AND  a.Subject LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Sub = " AND  a.Subject NOT LIKE '%" & txtSearch.Text & "'"
                End If

                '  End If



            End If





            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name & str_filter_Cat & str_filter_Sub & "order by a.E_Name")



            gvEmpInfo.DataSource = ds.Tables(0)
            ' gvEmpInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpInfo.DataBind()
                Dim columnCount As Integer = gvEmpInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvEmpInfo.Rows(0).Cells.Clear()
                gvEmpInfo.Rows(0).Cells.Add(New TableCell)
                gvEmpInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvEmpInfo.HeaderRow.Visible = True
            Else


                gvEmpInfo.DataBind()
               

            End If
            


            'If SearchMode = "E" Then
            '    lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
            '    lblID.Text = "Employee ID"
            '    lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
            '    lblName.Text = "Employee Name"
            '    Page.Title = "Employee Info"


            'ElseIf SearchMode = "B" Then

            '    lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
            '    lblID.Text = "Business ID"
            '    lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
            '    lblName.Text = "Business Unit Name"
            '    Page.Title = "Business Unit Info"

            'ElseIf SearchMode = "C" Then

            '    lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
            '    lblID.Text = "Category ID"
            '    lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
            '    lblName.Text = "Category Name"
            '    Page.Title = "Category Info"
            'End If



            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try





    End Sub


    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchCat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New LinkButton

        Dim lbClose As New LinkButton


        lbClose = sender

        lblcode = sender.Parent.FindControl("LinkButton2")

        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "||" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & l_Str_Msg & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub


    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpInfo.PageIndexChanging
        gvEmpInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New LinkButton

        Dim lbClose As New LinkButton


        lbClose = sender.Parent.FindControl("LinkButton1")


        lblcode = sender
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "||" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & l_Str_Msg & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub gvEmpInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpInfo.RowDataBound
        If SearchMode = "SG" Then
            e.Row.Cells(0).Style.Value = "display:none"
        End If
    End Sub
End Class
