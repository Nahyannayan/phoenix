Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class SelIDDesc
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
            If str_ID <> String.Empty Then
                Select Case (str_ID.ToUpper)
                    Case "EMPGRADE"
                        GridBindEMPGRADE()
                    Case "EMPSGD"
                        GridBindEMPSGD()
                    Case "DOCUMENT"
                        GridBindDocument()
                    Case "QUALIFICATION"
                        GridBindEMPQUALIFICATION()
                    Case "QUALIFICATIONCATEGORY"
                        GridBindEMPQUALIFICATIONCATEGORY()
                    Case "CITY"
                        GridBindCity()
                    Case "JOBRES"
                        GridBindJobResponsibility()
                    Case "EMP"
                        GridBindEMPLOPYEE()
                    Case "PUN"
                        GridBindEMPLOPYEEPUNCHING()
                    Case "SALCOMP"
                        GridBindSalComp()
                    Case "VP"
                        Dim CATID As String = IIf(Request.QueryString("CATID") IsNot Nothing, Request.QueryString("CATID"), String.Empty)
                        GridBindVacationPolicy(CATID)
                End Select
            End If
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBindEMPLOPYEEPUNCHING()
        Try
            Dim lblheader As New Label
            Page.Title = "Employee"
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ESD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ESD_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            Dim payYear As String = IIf(Request.QueryString("PAYYEAR") IsNot Nothing, Request.QueryString("PAYYEAR"), String.Empty)
            Dim payMonth As String = IIf(Request.QueryString("PAYMONTH") IsNot Nothing, Request.QueryString("PAYMONTH"), String.Empty)
            str_Sql = "Select ID,DESCR from(Select emp_ID as ID,isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'')  " & _
            " as DESCR,emp_bsu_ID as BSU,EMP_bACTIVE as bActive, EMP_PAYYEAR, EMP_PAYMONTH from Employee_M WHERE EMP_bReqPunching = 1)a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>''"

            'str_Sql = "SELECT ESD_ID as ID, ESD_DESCR as DESCR FROM EMPSTATDOCUMENTS_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindEMPLOPYEE()
        Try
            Dim lblheader As New Label
            Page.Title = "Employee"
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ESD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ESD_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            Dim payYear As String = IIf(Request.QueryString("PAYYEAR") IsNot Nothing, Request.QueryString("PAYYEAR"), String.Empty)
            Dim payMonth As String = IIf(Request.QueryString("PAYMONTH") IsNot Nothing, Request.QueryString("PAYMONTH"), String.Empty)
            str_Sql = "Select ID,DESCR from(Select cast(Replace(empno,'" & Session("sBSUID") & _
            "','') as numeric) as ID ,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  " & _
            " as DESCR,emp_bsu_ID as BSU,EMP_bACTIVE as bActive, EMP_PAYYEAR, EMP_PAYMONTH from Employee_M)a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
            If payYear <> "" And payMonth <> "" Then
                str_Sql += " AND EMP_PAYYEAR =" & payYear & " AND EMP_PAYMONTH = " & payMonth
            End If

            'str_Sql = "SELECT ESD_ID as ID, ESD_DESCR as DESCR FROM EMPSTATDOCUMENTS_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindJobResponsibility()
        Try
            Page.Title = "JOB RESPONSIBILITY"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("RES_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("RES_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = "SELECT RES_ID as ID, RES_DESCR as DESCR FROM EMPJOBRESPONSILITY_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindVacationPolicy(ByVal CATID As String)
        Try
            Page.Title = "Vecation Policy"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BLS_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BLS_DESCRIPTION", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                'str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = "select BLS_ID as ID, BLS_DESCRIPTION as DESCR from BSU_LEaVESLAB_S where BLS_ECT_ID = '" & CATID & _
            "' AND BLS_bActive = 1 and BLS_BSU_iD = '" & Session("sBSUID") & "' AND BLS_ELT_ID = 'AL' " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindDocument()
        Try
            Page.Title = "DOCUMENTS"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            gvGroup.PageSize = 25

            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ESD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ESD_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            str_filter.Append(" WHERE 1 = 1 ")
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            'str_filter.Append(" WHERE 1 = 1 ")

            Dim BSU_ID As String = IIf(Request.QueryString("BSU_ID") IsNot Nothing, Request.QueryString("BSU_ID"), String.Empty)
            If Not String.IsNullOrEmpty(BSU_ID) Then
                str_Sql = "SELECT ESD_ID as ID,ESD_DESCR as DESCR FROM  dbo.EMPDOC_BSU WITH (NOLOCK) INNER JOIN EMPSTATDOCUMENTS_M WITH (NOLOCK)  ON EDB_ESD_ID=ESD_ID" & str_filter.ToString()
                str_Sql = str_Sql + " AND EDB_BSU_ID =" + BSU_ID
                If Not String.IsNullOrEmpty(Request.QueryString("bMAINDOC")) Then
                    str_Sql = str_Sql + " AND ESD_bMAINDOC =" + Request.QueryString("bMAINDOC")
                End If
            Else
                str_Sql = "SELECT ESD_ID as ID, ESD_DESCR as DESCR FROM EMPSTATDOCUMENTS_M " & str_filter.ToString()
                str_Sql = str_Sql & " order  by isnull(esd_order,100) "
            End If
            str_filter.Append(" order  by esd_order")
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindEMPGRADE()
        Try
            Page.Title = "EMPLOYEE GRADE"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EGD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("EGD_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = "SELECT EGD_ID as ID, EGD_DESCR as DESCR FROM EMPGRADES_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindEMPQUALIFICATION()
        Try
            Page.Title = "EMPLOYEE QUALIFICATION"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("QLF_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("QLF_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                'str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            Dim cat_id As String = Request.QueryString("CAT_ID")
            str_Sql = "SELECT QLF_ID as ID, QLF_DESCR as DESCR FROM EMPQUALIFICATION_M WHERE QLF_QCT_ID = " & cat_id & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindEMPQUALIFICATIONCATEGORY()
        Try
            Page.Title = "QUALIFICATION CATEGORY"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("QCT_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("QCT_Descr", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = "SELECT  QCT_ID as ID, QCT_Descr as DESCR FROM   EMPQUALIFICATION_CAT_M" & str_filter.ToString()
            'str_Sql = "SELECT QLF_ID, QLF_DESCR as DESCR FROM EMPQUALIFICATION_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindEMPSGD()
        Try
            Page.Title = "EMPLOYEE SALARY SCALE GRADE"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("SGD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("SGD_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = "SELECT     SGD_ID AS ID, SGD_DESCR + '___' + EMPGRADES_M.EGD_DESCR AS DESCR " & _
            " FROM EMPSCALES_GRADES_M LEFT OUTER JOIN EMPGRADES_M ON " & _
            "EMPSCALES_GRADES_M.SGD_EGD_ID = EMPGRADES_M.EGD_ID " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindCity()
        Try
            Page.Title = "CITY"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("CIT_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("CIT_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = "SELECT CIT_ID as ID, CIT_DESCR as DESCR FROM CITY_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "CITY ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindSalComp()
        Try
            Page.Title = "SALARY COMPONENT"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ERN_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ERN_DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If
            If str_filter_code <> String.Empty And str_filter_name <> String.Empty Then
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
            End If
            str_Sql = Session("sSALCOMP_Q") & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "DESCRIPTION"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
        If str_ID <> String.Empty Then
            Select Case (str_ID.ToUpper)
                Case "EMPGRADE"
                    GridBindEMPGRADE()
                Case "EMPSGD"
                    GridBindEMPSGD()
                Case "QUALIFICATION"
                    GridBindEMPQUALIFICATION()
                Case "DOCUMENT"
                    GridBindDocument()
                Case "PUN"
                    GridBindEMPLOPYEEPUNCHING()
                Case "CITY"
                    GridBindCity()
                Case "JOBRES"
                    GridBindJobResponsibility()
                Case "PUN"
                    GridBindEMPLOPYEEPUNCHING()
                Case "SALCOMP"
                    GridBindSalComp()
                Case "VP"
                    Dim CATID As String = IIf(Request.QueryString("CATID") IsNot Nothing, Request.QueryString("CATID"), String.Empty)
                    GridBindVacationPolicy(CATID)
            End Select
        End If
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
        If str_ID <> String.Empty Then
            Select Case (str_ID.ToUpper)
                Case "EMPGRADE"
                    GridBindEMPGRADE()
                Case "EMPSGD"
                    GridBindEMPSGD()
                Case "QUALIFICATION"
                    GridBindEMPQUALIFICATION()
                Case "DOCUMENT"
                    GridBindDocument()
                Case "PUN"
                    GridBindEMPLOPYEEPUNCHING()
                Case "CITY"
                    GridBindCity()
                Case "JOBRES"
                    GridBindJobResponsibility()
                Case "EMP"
                    GridBindEMPLOPYEE()
                Case "PUN"
                    GridBindEMPLOPYEEPUNCHING()
                Case "SALCOMP"
                    GridBindSalComp()
                Case "VP"
                    Dim CATID As String = IIf(Request.QueryString("CATID") IsNot Nothing, Request.QueryString("CATID"), String.Empty)
                    GridBindVacationPolicy(CATID)
            End Select
        End If
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
        If str_ID <> String.Empty Then
            Select Case (str_ID.ToUpper)
                Case "EMPGRADE"
                    GridBindEMPGRADE()
                Case "EMPSGD"
                    GridBindEMPSGD()
                Case "QUALIFICATION"
                    GridBindEMPQUALIFICATION()
                Case "DOCUMENT"
                    GridBindDocument()
                Case "PUN"
                    GridBindEMPLOPYEEPUNCHING()
                Case "CITY"
                    GridBindCity()
                Case "JOBRES"
                    GridBindJobResponsibility()
                Case "EMP"
                    GridBindEMPLOPYEE()
                Case "PUN"
                    GridBindEMPLOPYEEPUNCHING()
                Case "SALCOMP"
                    GridBindSalComp()
                Case "VP"
                    Dim CATID As String = IIf(Request.QueryString("CATID") IsNot Nothing, Request.QueryString("CATID"), String.Empty)
                    GridBindVacationPolicy(CATID)
            End Select
        End If
    End Sub

    Protected Sub gvGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup.Load
        'gvGroup.Columns(2).Visible = False
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")       
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
        End If
    End Sub
End Class

