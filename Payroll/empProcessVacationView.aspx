<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empProcessVacationView.aspx.vb" Inherits="Payroll_empProcessVacationView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">



        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
                }
                else if (pId == 3) {
                    document.getElementById("<%=getid("mnu_3_img") %>").src = path;
               }
               else if (pId == 4) {
                   document.getElementById("<%=getid("mnu_4_img") %>").src = path;
               }
               else if (pId == 5) {
                   document.getElementById("<%=getid("mnu_5_img") %>").src = path;
             }
             else if (pId == 6) {
                 document.getElementById("<%=getid("mnu_6_img") %>").src = path;
               }
               else if (pId == 7) {
                   document.getElementById("<%=getid("mnu_7_img") %>").src = path;
               }

    document.getElementById(ctrl1).value = val + '__' + path;
}

function change_chk_state(src) {
    var chk_state = (src.checked);

    for (i = 0; i < document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].type == 'checkbox') {
            document.forms[0].elements[i].checked = chk_state;
        }
    }
}
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Vacation Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" border="0" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="98%">
                    <tr valign="top">

                        <td class="matters" valign="top" width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="center" valign="top" class="matters">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO" CssClass="table table-row table-bordered"
                                Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="Emp No" SortExpression="EMPNO">
                                        <HeaderTemplate>
                                            Emp No<br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp Name" SortExpression="EMP_NAME">
                                        <HeaderTemplate>
                                            Emp Name<br />
                                            <asp:TextBox ID="txtEmpname" runat="server" SkinID="Gridtxt" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Dt.">
                                        <HeaderTemplate>
                                            From Date<br />
                                            <asp:TextBox ID="txtLType" runat="server" SkinID="Gridtxt" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ELV_DTFROM") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="ELV_DTTO" HeaderText="To Dt."
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ELV_ACTUALDAYS" HeaderText="A. Days">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELV_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ELV_AIRTICKET" HeaderText="Air Ticket" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ELV_TKTCOUNT" HeaderText="Tkt Count">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("ELV_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Remarks<br />
                                            <asp:TextBox ID="txtRemarks" runat="server" SkinID="Gridtxt" Width="72px"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remove" Visible="false">
                                        <ItemTemplate>
                                            <input id="chkESD_ID" runat="server" value='<%# Bind("ELV_ELA_ID") %>' type="checkbox" />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <input id="chkAll" onclick="change_chk_state(this);" type="checkbox" runat="server" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center">
                            <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="Remove Vacation Processing" OnClick="btnRemove_Click" Visible="False" />
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

