<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empAirfareView.aspx.vb" Inherits="Payroll_empAirfareView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function ShowLeaveDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowEmpLeave.aspx?ela_id=" + id, "", sFeatures)

            return false;
        }
        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
                }
                else if (pId == 4) {
                    document.getElementById("<%=getid("mnu_4_img") %>").src = path;
               }
               else if (pId == 5) {
                   document.getElementById("<%=getid("mnu_5_img") %>").src = path;
               }
               else if (pId == 6) {
                   document.getElementById("<%=getid("mnu_6_img") %>").src = path;
             }
             else if (pId == 7) {
                 document.getElementById("<%=getid("mnu_7_img") %>").src = path;
               }

    document.getElementById(ctrl1).value = val + '__' + path;
}
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Air Fare Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%">
                    <tr valign="top">

                        <td class="matters" valign="top" width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_FromCity" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_TicketType" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table width="100%">
                    <tr>
                        <td align="center" valign="top" class="matters" colspan="9">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-row table-bordered" Width="100%" AllowPaging="True" PageSize="30"
                                EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="From Date">
                                        <HeaderTemplate>
                                            From Date<br />
                                            <asp:TextBox ID="txtFDate" runat="server" SkinID="Gridtxt" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("EAF_DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <HeaderTemplate>
                                            To Date<br />
                                            <asp:TextBox ID="txtTDate" runat="server" SkinID="Gridtxt" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# IIF(Eval("EAF_DTTO") Is DBNull.Value,"Till Date", Eval("EAF_DTTO", "{0:dd/MMM/yyyy}")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fare Class">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("EAF_CLASS") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Fare Class<br />
                                            <asp:TextBox ID="txtFClass" runat="server" SkinID="Gridtxt" Width="52px"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EAF_CLASS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Age Group">
                                        <HeaderTemplate>
                                            Age Group<br />
                                            <asp:TextBox ID="txtAgegroup" runat="server" SkinID="Gridtxt" Width="46px"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("EAF_AGEGROUP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="From City">
                                        <HeaderTemplate>
                                            From City<br />
                                            <asp:TextBox ID="txtFromCity" runat="server" SkinID="Gridtxt" Width="72px"></asp:TextBox>
                                            <asp:ImageButton ID="btnFromCity" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFromCity" runat="server" Text='<%# Bind("FROM_CITY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="To City">
                                        <HeaderTemplate>
                                            To City<br />
                                            <asp:TextBox ID="txtCity" runat="server" SkinID="Gridtxt" Width="72px"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("CIT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ticket Type">
                                        <HeaderTemplate>
                                            Ticket Type<br />
                                            <asp:TextBox ID="txtTicketType" runat="server" SkinID="Gridtxt" Width="72px"></asp:TextBox>
                                            <asp:ImageButton ID="btnTicketType" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTicketType" runat="server" Text='<%# Bind("EAF_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("EAF_AMOUNT") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("EAF_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EAF_ID" Visible="False">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("EAF_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


