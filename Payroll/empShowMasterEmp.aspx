<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empShowMasterEmp.aspx.vb" Inherits="Payroll_ShowMasterEmp" Theme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>       
    <base target="_self" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
     
   
    <script type="text/javascript">
       function GetRadWindow() {
           var oWindow = null;
           if (window.radWindow) oWindow = window.radWindow;
           else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
           return oWindow;
       }
    </script>
</head>
<body onload="listen_window();" topmargin=0 bottommargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server">   
       
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td align="center" valign="top">
    <asp:GridView ID="gvEmpInfo" CssClass="table table-bordered table-row" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ID" Width="100%"  PageSize="15">
    <Columns>
        <asp:TemplateField HeaderText=" ID" SortExpression="EMP_ID">
        <EditItemTemplate> 
        </EditItemTemplate>
        <ItemTemplate>
        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("ID") %>'></asp:LinkButton>&nbsp;
        </ItemTemplate>
        <HeaderTemplate>
        
            <asp:Label ID="lblID" runat="server" EnableViewState="False"
            Text="ID"></asp:Label><br />
            <asp:TextBox ID="txtcode" runat="server" Width="75%"></asp:TextBox>
            <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
            OnClick="btnSearchEmpId_Click" />
         </HeaderTemplate>
         <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
        <HeaderTemplate>
          
                <asp:Label ID="lblName" runat="server" Text="Description"
                ></asp:Label>
                    <br />
                <asp:TextBox ID="txtName" runat="server" Width="75%"></asp:TextBox>
                <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                OnClick="btnSearchEmpName_Click" />
        </HeaderTemplate>
        <ItemTemplate>
        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
        OnClick="LinkButton1_Click" Text='<%# Eval("E_Name") %>'></asp:LinkButton>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        </Columns> 
    </asp:GridView> 
    </td>
    </tr>
    
</table>
    

<input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
<input id="h_SelectedId" runat="server" type="hidden" value="0" />   
</form>
</body>
</html>
