﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empInsuranceExportExcel.aspx.vb" Inherits="Payroll_empInsuranceExportExcel" EnableEventValidation="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">                       
                    <div>
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblerror" runat="server"></asp:Label>
                                    <asp:Label ID="lblHead" runat="server" Text="Employee Insurance Details" Font-Bold="True" Font-Underline="True"></asp:Label>
                                </td>
                            </tr>
                            <%--<tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblHead" runat="server" Text="Employee Insurance Details" Font-Bold="True" Font-Underline="True"></asp:Label>
                    </td>
                </tr>--%>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvExport" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" CellPadding="1">
                                        <RowStyle />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sl No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNum" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                                    <asp:Label ID="lblEmpDepID" runat="server" Text='<%# bind("EmpDepID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Flag" HeaderText="Data Updation Status"></asp:BoundField>
                                            <asp:BoundField DataField="School Name/Sub-Group" HeaderText="School Name/Sub-Group"></asp:BoundField>
                                            <asp:BoundField DataField="Emirates ID" HeaderText="Emirates ID" />
                                            <asp:BoundField DataField="Staff ID" HeaderText="Staff ID" />
                                            <asp:BoundField DataField="Member Name" HeaderText="Member Name" />
                                            <asp:BoundField DataField="Principal Member Name" HeaderText="Principal Member Name" />
                                            <asp:BoundField DataField="Relation" HeaderText="Relation" />
                                            <asp:BoundField DataField="DOB" HeaderText="DOB" />
                                            <asp:BoundField DataField="Gender" HeaderText="Gender" />
                                            <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                                            <asp:BoundField DataField="Marital Status" HeaderText="Marital Status" />
                                            <asp:BoundField DataField="Emirate/Location" HeaderText="Emirate/Location" />
                                            <asp:BoundField DataField="Category" HeaderText="Category" />
                                            <asp:BoundField DataField="Date of Joining" HeaderText="Date of Joining" />
                                            <asp:BoundField DataField="With Gems" HeaderText="With Gems" />
                                            <asp:TemplateField HeaderText="Photo">
                                                <ItemTemplate>
                                                    <telerik:RadBinaryImage runat="server" ID="RbDep" DataValue='<%# IIf(Eval("EDD_Photo") IsNot DBNull.Value, Eval("EDD_Photo"),New System.Byte(-1) {})%>'
                                                        AutoAdjustImageControlSize="false" Visible="False"
                                                        AlternateText="Photo" />
                                                    <asp:Image runat="server" ID="imgEMployee" AlternateText='<%# Bind("EMP_PHOTO") %>'
                                                        Width="60" Height="60" />

                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PassportNo" HeaderText="PassportNo" />

                                            <asp:BoundField DataField="PassportIssPalce" HeaderText="Passport Issue Place" />
                                            <asp:BoundField DataField="PassportIssDate" HeaderText="Passport Issue Date" />
                                            <asp:BoundField DataField="PassportExpDate" HeaderText="Passport Expiry Date" />
                                            <asp:BoundField DataField="UIDNo" HeaderText="UID No" />
                                            <asp:BoundField DataField="Visa_Issue_date" HeaderText="Visa_Issue_Date" />
                                            <asp:BoundField DataField="Visa_exp_date" HeaderText="Visa_Exp_Date" />
                                            <asp:BoundField DataField="visa_issue_place" HeaderText="Visa_Issue_Place" />
                                        </Columns>
                                        <RowStyle  />
                                        <FooterStyle  />
                                        <PagerStyle  HorizontalAlign="Left" />
                                        <SelectedRowStyle  Font-Bold="True"  />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>                    
                    <asp:LinkButton ID="lnkExport" runat="server" Visible="False">Export to Excel</asp:LinkButton>                       
    </form>
</body>
</html>
