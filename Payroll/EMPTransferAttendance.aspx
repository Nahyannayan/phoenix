<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="EMPTransferAttendance.aspx.vb" Inherits="Payroll_EMPTransferAttendance"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Attendance Transfer
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                   <%-- <tr class="subheader_img">
                        <td align="left" colspan="4">
                        </td>
                    </tr>--%>
                    <tr >
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" width="80%"  colspan="3">
                            <asp:DropDownList ID="ddlBSUinit" runat="server"  AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">To Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  colspan="4" >
                            <asp:Button ID="btnTransferAttendance" runat="server" CssClass="button" Text="Transfer Attendance"
                                ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" >
                            <asp:Label ID="lblSalSummary" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trSalSummary">
                        <td colspan="4" width="100%" >
                            <asp:GridView ID="gvSalarySummary" runat="server" AutoGenerateColumns="False" Width="100%"
                                CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="CategoryID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblECTID" runat="server" Text='<%# Bind("ESD_ECT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Month" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMonth" runat="server" Text='<%# Bind("ESD_MONTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Year" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblYear" runat="server" Text='<%# Bind("ESD_YEAR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ECT_DESCR" HeaderText="Category Name">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TOTALCOUNT" HeaderText="Count">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" DataFormatString="{0:n2}" HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAIDCASH" DataFormatString="{0:n2}" HeaderText="Cash">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAIDBANK" DataFormatString="{0:n2}" HeaderText="Bank">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="trRemSalary">
                        <td colspan="4" width="100%">
                            <table width="100%">
                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="lblRemSalaryMsg" runat="server" EnableViewState="False" CssClass="field-label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <asp:GridView ID="gvSalary" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CellPadding="1" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="EMPNO" HeaderText="Employee NO" SortExpression="EMPNO" />
                                                <asp:BoundField DataField="EMP_NAME" HeaderText="Name" ReadOnly="True" SortExpression="EMP_NAME" />
                                                <asp:BoundField DataField="MONTHYEAR" HeaderText="Month" ReadOnly="True" SortExpression="MONTHYEAR">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BANKCASH" HeaderText="Pay Mode" ReadOnly="True" SortExpression="BANKCASH">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ESD_EARN_NET" HeaderText="Amount" SortExpression="ESD_EARN_NET"
                                                    DataFormatString="{0:0.00}">
                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Remove">
                                                    <ItemTemplate>
                                                        <input id="chkESD_ID" runat="server" type="checkbox" />
                                                        <asp:HiddenField ID="hdnESD_ID" runat="server" Value='<%# Eval("ESD_ID") %>' />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <input id="chkAll" onclick="change_chk_state(this);" type="checkbox" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ESD_EMP_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblESD_EMP_ID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Remove Salary" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>
