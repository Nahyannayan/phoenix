Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empDocumentMovement
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            txtDocName.Attributes.Add("readonly", "readonly")


            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130065" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") <> "" Then
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If
        End If
    End Sub



    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
      

            str_Sql = "SELECT     EDM.EDM_ID, " _
                     & " EDM.EDM_OUTDT, EDM.EDM_REMARKS," _
                     & " EDM.EDM_RETURNDT, EDM.EDM_APPRBY, EDM.EDM_ISS_EMP_ID, " _
                     & " ISNULL(EM.EMP_FNAME, '') +' '+ ISNULL(EM.EMP_MNAME, '') +' '+ " _
                     & " ISNULL(EM.EMP_LNAME, '') AS EMP_NAME, ESD.ESD_DESCR," _
                     & " EM.EMPNO, EMD.EMD_NO, ISNULL(EMA.EMP_FNAME, '')+" _
                     & " ISNULL(EMA.EMP_MNAME, '')+ ISNULL(EMA.EMP_LNAME, '') AS EMP_ANAME," _
                     & " EDM.EDM_EDC_ID ,EDM.EDM_EMP_ID, EDM_TENT_RET_DT " _
                     & " FROM EMPDOCMOVEMENT AS EDM " _
                     & " INNER JOIN EMPLOYEE_M AS EM ON " _
                     & " EDM.EDM_EMP_ID = EM.EMP_ID INNER JOIN" _
                     & " EMPSTATDOCUMENTS_M AS ESD ON " _
                     & " EDM.EDM_ESD_ID = ESD.ESD_ID INNER JOIN" _
                     & " EMPDOCUMENTS_S AS EMD ON " _
                     & " EDM.EDM_EDC_ID = EMD.EMD_ID INNER JOIN" _
                     & " EMPLOYEE_M AS EMA ON EDM.EDM_ISS_EMP_ID = EMA.EMP_ID" _
                     & " WHERE     (EM.EMP_BSU_ID = '" & Session("sBSUID") & "')" _
                     & " AND  EDM.EDM_ID='" & p_Modifyid & "'"
            Dim ds As New DataSet


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EDM_EDC_ID")
                txtDocName.Text = ds.Tables(0).Rows(0)("EMD_NO").ToString
                h_Docid.Value = ds.Tables(0).Rows(0)("EDM_EDC_ID").ToString 'EDM_EDC_ID
                txtRemarks.Text = ds.Tables(0).Rows(0)("EDM_REMARKS").ToString
                txtApprovedcomment.Text = ds.Tables(0).Rows(0)("EDM_APPRBY").ToString
                txtDate.Text = Format(CDate(ds.Tables(0).Rows(0)("EDM_OUTDT")), "dd/MMM/yyyy")
                If Not ds.Tables(0).Rows(0)("EDM_TENT_RET_DT") Is Nothing AndAlso Not ds.Tables(0).Rows(0)("EDM_TENT_RET_DT") Is DBNull.Value Then
                    txtRetDate.Text = Format(CDate(ds.Tables(0).Rows(0)("EDM_TENT_RET_DT")), "dd/MMM/yyyy")
                End If
                txtApprovedby.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString

            Else
                ViewState("canedit") = "no"
            End If


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    

    Private Sub setViewData() 'setting controls on view/edit

        imgFrom.Enabled = False
        imgDoc.Enabled = False
        txtDate.Attributes.Add("readonly", "readonly")
        txtRetDate.Attributes.Add("readonly", "readonly")


    End Sub




    Private Sub ResetViewData() 'resetting controls on view/edit
        imgFrom.Enabled = True
        imgDoc.Enabled = True
        txtDate.Attributes.Remove("readonly")
        txtRetDate.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
    End Sub



    Sub bindevents1()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT ELT_ID, ELT_DESCR FROM EMPLEAVETYPE_M"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub




    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click


        Dim strfDate As String = txtDate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        End If
        txtDate.Text = strfDate

        If txtRemarks.Text.Trim = "" Then
            lblError.Text = "Remarks Cannot be empty"
            Exit Sub
        End If

        If txtRetDate.Text <> "" Then
            If CDate(txtDate.Text) > CDate(txtRetDate.Text) Then
                lblError.Text = "Return date should be greater than Issue Date"
                Exit Sub
            End If
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim elh_id As String
            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                elh_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                elh_id = 0
            End If
            Dim retval As String = "1000"
            Dim new_elh_id As Integer = 0
            retval = SaveEMPDOCMOVEMENT(elh_id, CInt(h_Docid.Value), _
            txtDate.Text, txtRetDate.Text, txtRemarks.Text, "", txtApprovedcomment.Text, _
            edit_bool, 0, Session("sUsr_name"), stTrans)

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            CInt(h_Docid.Value) & "-" & txtDate.Text & "-" & txtRemarks.Text & "-" & txtApprovedcomment.Text, _
            "Insert", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub




    Private Function SaveEMPDOCMOVEMENT(ByVal p_EDM_ID As Integer, ByVal p_EDM_EDC_ID As Integer, _
      ByVal p_EDM_OUTDT As Date, ByVal p_EDM_TENT_RET_DT As Date, ByVal p_EDM_REMARKS As String, _
    ByVal p_EDM_RETURNDT As String, ByVal p_EDM_APPRBY As String, ByVal p_bEdit As Boolean, _
     ByVal p_EDM_REC_EMP_ID As Integer, ByVal p_User As String, ByVal p_stTrans As SqlTransaction) As String
        'EXEC	@return_value = [dbo].[SaveEMPDOCMOVEMENT]
        '@EDM_ID = 0,
        '@EDM_EDC_ID = 14,
        '@EDM_EMP_ID = 1001,@EDM_APPRBY
        '@EDM_OUTDT = N'12/DEC/2007',
        '@EDM_REMARKS = N'XF6MT',
        '@EDM_RETURNDT = NULL,
        '@USER = 'guru',
        '@EDM_REC_EMP_ID = 1000,
        '@bEdit = 0
        Try
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EDM_ID", SqlDbType.Int)
            pParms(0).Value = p_EDM_ID
            pParms(1) = New SqlClient.SqlParameter("@EDM_EDC_ID", SqlDbType.Int)
            pParms(1).Value = p_EDM_EDC_ID
            pParms(2) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
            pParms(2).Value = p_bEdit
            pParms(3) = New SqlClient.SqlParameter("@EDM_OUTDT", SqlDbType.DateTime)
            pParms(3).Value = p_EDM_OUTDT
            pParms(4) = New SqlClient.SqlParameter("@EDM_TENT_RET_DT", SqlDbType.DateTime)
            pParms(4).Value = p_EDM_TENT_RET_DT
            pParms(5) = New SqlClient.SqlParameter("@EDM_REMARKS", SqlDbType.VarChar, 100)
            pParms(5).Value = p_EDM_REMARKS
            pParms(6) = New SqlClient.SqlParameter("@EDM_RETURNDT", SqlDbType.DateTime)
            If p_EDM_RETURNDT = "" Then
                pParms(6).Value = System.DBNull.Value
            Else
                pParms(6).Value = p_EDM_RETURNDT
            End If
            pParms(7) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 20)
            pParms(7).Value = p_User
            pParms(8) = New SqlClient.SqlParameter("@EDM_REC_EMP_ID", SqlDbType.Int)
            If p_EDM_REC_EMP_ID = 0 Then
                pParms(8).Value = System.DBNull.Value
            Else
                pParms(8).Value = p_EDM_REC_EMP_ID
            End If

            pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue

            pParms(10) = New SqlClient.SqlParameter("@EDM_APPRBY", SqlDbType.VarChar, 20)
            pParms(10).Value = p_EDM_APPRBY
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPDOCMOVEMENT", pParms)

            SaveEMPDOCMOVEMENT = pParms(9).Value

        Catch ex As Exception
            Errorlog(ex.Message)
            SaveEMPDOCMOVEMENT = "1000"
        End Try

    End Function



    Private Function DeleteEMPLOAN_D(ByVal p_ELD_ID As Integer, ByVal p_stTrans As SqlTransaction) As String

        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELD_IDSS", SqlDbType.Int)
            pParms(0).Value = p_ELD_ID
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue

            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteEMPLOAN_D", pParms)

            DeleteEMPLOAN_D = pParms(1).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            DeleteEMPLOAN_D = "1000"
        End Try

    End Function

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDoc.Click

    End Sub




    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            setViewData()
            'clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub




    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()

    End Sub




    Sub clear_All()
        txtApprovedby.Text = ""
        h_Emp_No.Value = ""
        txtDocName.Text = ""
        txtRemarks.Text = ""
        txtDate.Text = ""
        txtRetDate.Text = ""
    End Sub




    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ResetViewData()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

       
    End Sub


 
 
  
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        
 
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim new_elh_id As Integer = 0
            retval = DeleteEMPDOCMOVEMENT(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")), stTrans)
            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")

            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Private Function DeleteEMPDOCMOVEMENT(ByVal p_EDM_ID As Integer,   ByVal p_stTrans As SqlTransaction) As String
        '@return_value = [dbo].[ReturnEMPDOCMOVEMENT]
        '@EDM_ID = 1 ,
        '@USER = N'guru',
        '@EDM_RETURNDT = N'11-12-2007'
        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EDM_ID", SqlDbType.Int)
            pParms(0).Value = p_EDM_ID

            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue


            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteEMPDOCMOVEMENT", pParms)

            DeleteEMPDOCMOVEMENT = pParms(1).Value

        Catch ex As Exception
            Errorlog(ex.Message)
            DeleteEMPDOCMOVEMENT = "1000"
        End Try

    End Function
End Class
