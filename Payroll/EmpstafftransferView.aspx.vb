Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
'Version   Done By        On                       For

'  1.1     Swapana      14-Dec-2010     Addition of Approval screen to approve transfer by transferring BSU
'  1.2      Swapana     16-jan-2011     Changed menu option value in DB
'                                       "H000074" instead of "P013190"
'                                       "H000075" instead of "P050040"
'                                       "H000079" instead of "P050115" 
' 1.3       Swapana     28-feb-2011     Changing query for checking null conditions

'
'
Partial Class EmpstafftransferView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Select Case ViewState("MainMnu_code").ToString
                ' Case "P050040"   V1.2
                Case "H000075"
                    hlAddNew.NavigateUrl = "Empstafftransfer.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    ' Case "P050115"      V1.2
                Case "H000079"
                    hlAddNew.NavigateUrl = "EMPBookStaffTransfer.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    'Case "P013190" 'V1.1 --Commented for V1.2
                Case "H000074" 'V1.1   -- added in V1.2
                    hlAddNew.NavigateUrl = "EMPBookStaffTransfer.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            End Select

            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                ' "P050040" changed to "H000075"  -- for V1.2 
                ' "P013190" changed to "H000075"  -- for V1.2 
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000075" _
                                    And ViewState("MainMnu_code") <> "H000079" And ViewState("MainMnu_code") <> "H000074") Then  'V1.1
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Select Case ViewState("MainMnu_code").ToString
                        'Case "P050115" V1.2
                        Case "H000079"
                            lblHeader.Text = "Employee Transfer Request"
                            GridBind()
                            gvJournal.Columns(6).Visible = True
                            btnApprove.Visible = False    'V1.1

                        Case "H000075"    '"P050040" V1.2
                            lblHeader.Text = "Employee Transfer Acceptance"
                            GridBind()
                            gvJournal.Columns(6).Visible = False
                            hlAddNew.Visible = False

                            'V1.1 starts
                        Case "H000074"               '"P013190" V1.2
                            lblHeader.Text = "Employee Transfer Approval"
                            GridBind()
                            hlAddNew.Visible = False
                            gvJournal.Columns(6).Visible = True
                            btnApprove.Visible = True
                            'V1.1 ends

                    End Select
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrOpr As String = String.Empty

            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                'larrSearchOpr = h_selected_menu_1.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                'lstrCondn1 = Trim(txtSearch.Text)
                'If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtBSUName")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSU_NAME", lstrCondn3)

                '   -- 3   txtTDate

                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ETF_FROMDT", lstrCondn4)

                '   -- 5  city

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_REMARKS", lstrCondn5)
            End If

            Dim str_cond As String = String.Empty
            Select Case ViewState("MainMnu_code").ToString
                Case "H000075"                    'To view employees who have been transferred to current BSU
                    ''"P050040" changed in V1.2  
                    str_cond = "IsNull(ETF_TO_bForward,0) = 1 AND IsNull(ETF_TO_bAccept,0) <> 1 AND IsNUll(ETF_bApproved_FromBSU,0) =1 AND IsNull(ETF_bIsDeleted,0)=0 and ETF_TO_BSU_ID = '" & Session("sBSUID") & "'"
                    str_Sql = "SELECT  EMPTRANSFER.ETF_EMP_ID, BUSINESSUNIT_M.BSU_NAME, " & _
                   "ISNULL(vw_OSO_EMPLOYEEMASTER.EMP_SALUTE,'') + ' ' +vw_OSO_EMPLOYEEMASTER.EMPNAME EMP_NAME," & _
                    "EMPTRANSFER.ETF_REMARKS, EMPTRANSFER.ETF_FROMDT, isnull(EMPTRANSFER.ETF_ID,0) as ETF_ID,isnull(EMPTRANSFER.ETF_bApproved_FromBSU,0) ETF_bApproved_FromBSU FROM " & _
                    "EMPTRANSFER LEFT OUTER JOIN BUSINESSUNIT_M ON  " & _
                    "EMPTRANSFER.ETF_FROM_BSU_ID = BUSINESSUNIT_M.BSU_ID LEFT OUTER JOIN " & _
                    "EMPLOYEE_M ON EMPTRANSFER.ETF_EMP_ID = EMPLOYEE_M.EMP_ID " & _
                    " LEFT OUTER JOIN vw_OSO_EMPLOYEEMASTER ON vw_OSO_EMPLOYEEMASTER.EMP_ID=EMPLOYEE_M.EMP_ID " & _
                    "WHERE " & str_cond & str_Filter
                    '----V1.3  comment----
                    ' "ISNULL(EMPLOYEE_M.EMP_SALUTE,'') + ' ' + EMPLOYEE_M.EMP_FNAME + ' ' + " & _
                    '"EMPLOYEE_M.EMP_MNAME + ' ' + EMPLOYEE_M.EMP_LNAME AS EMP_NAME, " & _

                    'Case "P050115"                  'To view employees transferred from current BSU
                Case "H000079"
                    str_cond = " IsNull(ETF_TO_bAccept,0) <> 1 AND IsNull(ETF_bIsDeleted,0)=0 AND  ETF_FROM_BSU_ID = '" & Session("sBSUID") & "'"
                    str_Sql = "SELECT EMPTRANSFER.ETF_EMP_ID, BUSINESSUNIT_M.BSU_NAME, " & _
                            "ISNULL(vw_OSO_EMPLOYEEMASTER.EMP_SALUTE,'') + ' ' +vw_OSO_EMPLOYEEMASTER.EMPNAME EMP_NAME," & _
                            "EMPTRANSFER.ETF_REMARKS, EMPTRANSFER.ETF_FROMDT,isnull(EMPTRANSFER.ETF_ID,0) as ETF_ID,isnull(EMPTRANSFER.ETF_bApproved_FromBSU,0) ETF_bApproved_FromBSU FROM " & _
                            "EMPTRANSFER LEFT OUTER JOIN BUSINESSUNIT_M ON  " & _
                            "EMPTRANSFER.ETF_TO_BSU_ID = BUSINESSUNIT_M.BSU_ID LEFT OUTER JOIN " & _
                            "EMPLOYEE_M ON EMPTRANSFER.ETF_EMP_ID = EMPLOYEE_M.EMP_ID " & _
                            " LEFT OUTER JOIN vw_OSO_EMPLOYEEMASTER ON vw_OSO_EMPLOYEEMASTER.EMP_ID=EMPLOYEE_M.EMP_ID " & _
                            "WHERE " & str_cond & str_Filter
                    '---V1.3-----
                    '"ISNULL(EMPLOYEE_M.EMP_SALUTE,'') + ' ' + EMPLOYEE_M.EMP_FNAME + ' ' + " & _
                    '        "EMPLOYEE_M.EMP_MNAME + ' ' + EMPLOYEE_M.EMP_LNAME AS EMP_NAME, " & _
                Case "H000074"             'To view  empoyees list waiting for transfer approval by "FROM" Unit   V1.1
                    '"P013190"   changed in V1.2
                    str_cond = "isnull(ETF_TO_bAccept,0) <> 1 AND IsNull(ETF_bIsDeleted,0)=0 AND isnull(ETF_bApproved_FromBSU,0) <>1 AND ETF_FROM_BSU_ID = '" & Session("sBSUID") & "'"
                    str_Sql = "SELECT EMPTRANSFER.ETF_EMP_ID, BUSINESSUNIT_M.BSU_NAME, " & _
                             "ISNULL(vw_OSO_EMPLOYEEMASTER.EMP_SALUTE,'') + ' ' +vw_OSO_EMPLOYEEMASTER.EMPNAME EMP_NAME," & _
                            "EMPTRANSFER.ETF_REMARKS, EMPTRANSFER.ETF_FROMDT,isnull(EMPTRANSFER.ETF_ID,0)as ETF_ID,isnull(EMPTRANSFER.ETF_bApproved_FromBSU,0) ETF_bApproved_FromBSU FROM " & _
                            "EMPTRANSFER LEFT OUTER JOIN BUSINESSUNIT_M ON  " & _
                            "EMPTRANSFER.ETF_TO_BSU_ID = BUSINESSUNIT_M.BSU_ID LEFT OUTER JOIN " & _
                            "EMPLOYEE_M ON EMPTRANSFER.ETF_EMP_ID = EMPLOYEE_M.EMP_ID " & _
                            " LEFT OUTER JOIN vw_OSO_EMPLOYEEMASTER ON vw_OSO_EMPLOYEEMASTER.EMP_ID=EMPLOYEE_M.EMP_ID " & _
                            "WHERE " & str_cond & str_Filter

                    '---V1.3----
                    '"ISNULL(EMPLOYEE_M.EMP_SALUTE,'') + ' ' + EMPLOYEE_M.EMP_FNAME + ' ' + " & _
                    '        "EMPLOYEE_M.EMP_MNAME + ' ' + EMPLOYEE_M.EMP_LNAME AS EMP_NAME, " & _

            End Select

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)("ETF_bApproved_FromBSU") = False   'To handle error on checkbox bind when no rows are returned from DB.
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

            Else
                gvJournal.DataBind()
                If (ViewState("MainMnu_code").ToString = "H000079") Then   'V1.1,V1.2

                    For Each gvr As GridViewRow In gvJournal.Rows

                        If gvr.RowType = DataControlRowType.DataRow Then


                            Dim chkApproval As New CheckBox
                            chkApproval = TryCast(gvr.FindControl("chkApproval"), CheckBox)
                            chkApproval.Enabled = False

                        End If

                    Next
                End If
            End If
            'ts
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn3

            'txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
            'txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblEMP_ID As New Label
            lblEMP_ID = TryCast(e.Row.FindControl("lblEMP_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            Dim TransID As New Label
            TransID = TryCast(e.Row.FindControl("lblETFID"), Label)
            Dim chkApproval As New CheckBox
            chkApproval = TryCast(e.Row.FindControl("chkApproval"), CheckBox)
            Dim rbtApproval As New RadioButton
            rbtApproval = TryCast(e.Row.FindControl("rbtApproval"), RadioButton)
            'If TransID.Text = "" Then
            '    TransID.Text = 0
            'End If
            If hlEdit IsNot Nothing And lblEMP_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
                    Case "H000075"   'V1.2
                        hlEdit.NavigateUrl = "Empstafftransfer.aspx?viewid=" & Encr_decrData.Encrypt(lblEMP_ID.Text) & "&transID=" & Encr_decrData.Encrypt(TransID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        ' Case "P050115"
                    Case "H000079"  'V1.2
                        hlEdit.Text = "View/Edit"
                        hlEdit.NavigateUrl = "EMPBookStaffTransfer.aspx?viewid=" & Encr_decrData.Encrypt(lblEMP_ID.Text) & "&transID=" & Encr_decrData.Encrypt(TransID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case "H000074"  'For approval from Current BSU  V1.1 , 'V1.2
                        chkApproval.Style.Add("display", "none")
                        rbtApproval.Visible = True
                        hlEdit.NavigateUrl = "EMPBookStaffTransfer.aspx?viewid=" & Encr_decrData.Encrypt(lblEMP_ID.Text) & "&transID=" & Encr_decrData.Encrypt(TransID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select


            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If gvJournal.Rows.Count > 0 Then
            UpdateTransferApprovals()

        Else
            lblError.Text = "No requests selected for approval"
        End If
    End Sub
    'To update transfer details in DB when approvals are made by transferring Unit or transferred unit
    'V1.1 starts
    Sub UpdateTransferApprovals()
        Try
            Dim strUserName As String = ""
            Dim ApprovalIDs As String = ""
            If Not (Session("sUsr_name") Is Nothing) Then
                strUserName = Session("sUsr_name")
            End If
            For Each gvr As GridViewRow In gvJournal.Rows

                If gvr.RowType = DataControlRowType.DataRow Then

                    'If DirectCast(gvr.FindControl("chkApproval"), CheckBox).Checked = True Then
                    If DirectCast(gvr.FindControl("rbtApproval"), RadioButton).Checked = True Then
                        Dim TransID As Label = DirectCast(gvr.FindControl("lblETFID"), Label)
                        ApprovalIDs = ApprovalIDs + TransID.Text & ","

                    End If

                End If

            Next
            If ApprovalIDs <> "" Then


                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim objConn As New SqlConnection(str_conn)
                Dim returnVal As Integer
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction

                Dim SqlCmd As New SqlCommand("EMP_UPDATE_EMPTRANSFER_APPROVAL", objConn, stTrans)
                SqlCmd.CommandType = CommandType.StoredProcedure

                SqlCmd.Parameters.AddWithValue("@ApprovalIDs", ApprovalIDs)
                SqlCmd.Parameters.AddWithValue("@Approver", strUserName)

                SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
                SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

                SqlCmd.ExecuteNonQuery()
                returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

                returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

                If returnVal <> 0 Then
                    'lblError.Text = UtilityObj.getErrorMessage("957")
                    lblError.Text = UtilityObj.getErrorMessage(returnVal) ' swapna added for HR tran lock
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage("962")
                    GridBind()
                End If

                objConn.Close()
            Else
                lblError.Text = "No records selected for approval"
                GridBind()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message.ToString

        End Try


    End Sub
    'V1.1 ends

    Protected Sub rbtApproval_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rbtApproval As RadioButton = TryCast(sender, RadioButton)
        Dim parentRow As GridViewRow = TryCast(rbtApproval.NamingContainer, GridViewRow)
        Dim strName As String = rbtApproval.Text
        Dim TransID As Label = DirectCast(parentRow.FindControl("lblETFID"), Label)
        Dim ret As String = ""
        If rbtApproval.Checked = True Then

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
           
            Dim SqlCmd As New SqlCommand("GetTransferBetweenTheMonthMessage", objConn)
            SqlCmd.CommandType = CommandType.StoredProcedure

            SqlCmd.Parameters.AddWithValue("@ETF_ID", TransID.Text)
            SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.Add("@returnMsg", SqlDbType.VarChar, 5000)
            SqlCmd.Parameters("@returnMsg").Direction = ParameterDirection.Output
            lblError.Text = ""
            SqlCmd.ExecuteNonQuery()
            ret = SqlCmd.Parameters("@returnMsg").Value
            If ret <> "" Then
                'lblError.Text = ret
                btnApprove.Attributes.Add("OnClick", "return window.confirm('" & ret & "');")
            Else
                btnApprove.Attributes.Remove("OnClick")

            End If
        End If
        'Mutually exclusive RadioButtons
        'For Each row As GridViewRow In gvJournal.Rows
        '    Dim rbtApproval As RadioButton = TryCast(row.FindControl("rbtApproval"), RadioButton)
        '    If rbtApproval != rbName Then
        '        rbrbtApproval.Checked = False
        '    End If
        'Next
    End Sub
End Class
