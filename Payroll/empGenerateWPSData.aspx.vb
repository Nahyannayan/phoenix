﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj
'-------------------------------------------------------------------------
'Author:Swapana.T.V
'Creation Date:6/Jan/2011
'Purpose:To generate employee WPS details in .sif file for selected visa issuing units
'-------------------------------------------------------------------------

Partial Class Payroll_empGenerateWPSData
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                'Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'If Not Request.UrlReferrer Is Nothing Then
                '    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                'End If
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "P153061") 

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    lblError.Text = ""
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(GetMinMonthofSalary()))
                    BindVisaUnits()
                    BindYear()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Protected Sub BindVisaUnits()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim ds As New DataSet
            Dim pParam(2) As SqlClient.SqlParameter
            pParam(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 50)
            pParam(0).Value = Session("sBsuid")

            pParam(1) = New SqlClient.SqlParameter("@month", SqlDbType.VarChar, 2)
            pParam(1).Value = ddlMonth.SelectedValue

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetVisaIssueUnits", pParam)
            ddlUnits.DataSource = ds
            ddlUnits.DataTextField = ds.Tables(0).Columns("UnitName").ToString
            ddlUnits.DataValueField = ds.Tables(0).Columns("UnitID").ToString
            ddlUnits.DataBind()
            objConn.Close()
            If ddlUnits.Items.Count = 0 Then
                ddlUnits.Enabled = False
                lblError.Text = UtilityObj.getErrorMessage("979")
            Else
                lblError.Text = ""
                ddlUnits.Enabled = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        If txtBankCode.Text = "" Or txtBankDescr.Text = "" Then
            lblError.Text = "Select Bank"
            Exit Sub
        End If
        lblError.Text = ""
        If ddlUnits.Enabled = False Then
            lblError.Text = UtilityObj.getErrorMessage("980")
        Else
            GenerateSIFfile()
            'BindVisaUnits()
        End If

    End Sub

    Protected Sub GenerateSIFfile()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try

            objConn.Open()

            Dim trans As SqlTransaction = objConn.BeginTransaction("SampleTransaction")

            Dim errorno As Integer = 0
            'If errorno = 0 Then errorno = CreateSIFfile(objConn, trans)
            If errorno = 0 Then errorno = CreateSIFfile_Alternative(objConn, trans)
            If errorno = 0 Then errorno = UpdateExportToDB(objConn, trans)
            If errorno = 0 Then
                trans.Commit()
                lblError.Text = ""

            Else
                trans.Rollback()
                'lblError.Text = "Error Occured"

            End If
            ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(GetMinMonthofSalary()))


        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            objConn.Close()

        End Try

    End Sub

    Protected Function DisplayFileDialog(ByVal objFile As MemoryStream)
        Response.Clear()
        'Add Headers to enable dialog display
        Response.AppendHeader("Content-Disposition", "attachment; filename=WPS.sif")
        Response.AddHeader("Content-Length", objFile.Length.ToString())
        Response.ContentType = "application/octet-stream"
        Response.WriteFile("WPS_DATA")
        Response.Flush()
        'Response.End()
        'Instead use below code
        HttpContext.Current.ApplicationInstance.CompleteRequest()
        Return 0
    End Function

    Protected Function DisplayDownloadDialog(ByVal PathVirtual As String, ByVal DownloadFileName As String) As Integer
        'DownloadFileName name is already appended with .sif
        Dim strPhysicalPath As String
        Dim objFileInfo As System.IO.FileInfo
        Try
            'strPhysicalPath = Server.MapPath(PathVirtual)
            strPhysicalPath = PathVirtual
            'exit if file does not exist
            If strPhysicalPath <> "" Then
                'If Not System.IO.File.Exists(strPhysicalPath) Then
                '    lblError.Text = "File could not be generated"
                '    Return 1000
                '    'Exit Function
                'Else
                'objFileInfo = New System.IO.FileInfo(strPhysicalPath)
                '' ''Response.Clear()
                ' '' ''Add Headers to enable dialog display
                '' ''Response.AppendHeader("Content-Disposition", "attachment; filename=" + DownloadFileName)
                ' '' ''Response.AddHeader("Content-Length", objFileInfo.Length.ToString())
                '' ''Response.ContentType = "application/octet-stream"
                '' ''Response.WriteFile(strPhysicalPath)
                '' ''Response.Flush()
                ' '' ''System.IO.File.Delete(strPhysicalPath) 'To delete the file generated in the server
                ' '' ''Response.End()
                ' '' ''Instead use below code
                '' ''HttpContext.Current.ApplicationInstance.CompleteRequest()

                Dim bytes() As Byte = File.ReadAllBytes(strPhysicalPath)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(strPhysicalPath))
                Response.BinaryWrite(bytes)

                Response.Flush()

                Response.End()

                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(strPhysicalPath))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(strPhysicalPath)
                'HttpContext.Current.Response.End()
                Return 0

                'End If
            Else
                lblError.Text = "File could not be generated"
                Return 1000
            End If


        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return 1000
        End Try
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub BindYear()
        '----------create a list item to bind the years-------------
        Dim totyear, i As Integer
        ' Session("BSU_PAYMONTH")
        'Session("BSU_PAYYEAR")

        Dim startYear As Integer
        totyear = Session("BSU_PAYYEAR") 'AccessPayrollClass.GetMax_Min_Year(minyear) 'class for getting the total years
        startYear = totyear - 20
        Dim liYear As ListItem
        ddlYear.Items.Clear()
        For i = 0 To 40

            liYear = New ListItem(startYear + i, startYear + i)
            ddlYear.Items.Add(liYear)

        Next
        ddlYear.SelectedValue = ddlYear.Items.FindByValue(totyear).ToString
    End Sub

    Protected Function GetMinMonthofSalary() As String
        Dim strMonth As String = "1"
        Dim iReturnvalue As Integer = 0
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim con As New SqlConnection(str_conn)
            con.Open()
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("GetMinMonthofSalary", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim outMinMonth As New SqlParameter("@MinMonth", SqlDbType.VarChar, 2)
            outMinMonth.Direction = ParameterDirection.Output
            cmd.Parameters.Add(outMinMonth)

            cmd.ExecuteNonQuery()
            strMonth = outMinMonth.Value
            con.Close()
            If strMonth <> "" Then
                Return strMonth
            Else
                Return "1"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return "1"
        End Try
    End Function

    Private Function CreateSIFfile(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        'Funtion to create a file in server by getting data from DB based on user selection and then download it from server to client system
        'After downloading the file is deleted from the server.
        Dim fileName As String = ""
        Try

            Dim i As Integer
            Dim strLine As String = ""
            'Dim filePath As String = ""

            Dim fileExcel As String = ""
            Dim link As String = ""

            Dim objFileStream As FileStream
            Dim objStreamWriter As StreamWriter
            Dim nRandom As Random = New Random(DateTime.Now.Millisecond)



            'Create a pseudo-random file name.
            fileExcel = "t" & nRandom.Next().ToString() & ".sif"

            'Set a virtual folder to save the file.
            'If Not Directory.Exists("~\Payroll") Then
            '    Directory.CreateDirectory("~\Payroll")
            'End If
            'filePath = Server.MapPath("~\Payroll")

            Dim filePath As String = Web.Configuration.WebConfigurationManager.AppSettings("UploadCVPath").ToString()

            'Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString

            If Not Directory.Exists(filePath & "\temporary") Then
                Directory.CreateDirectory(filePath & "\temporary")
            End If
            'filePath = "C:\WPSPAYROLL\"
            fileName = filePath & "\temporary\" & fileExcel

            'Use FileStream to create the .xls file.

            objFileStream = New FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write)
            objStreamWriter = New StreamWriter(objFileStream)

            'objConn.Open()


            Dim pParam(6) As SqlClient.SqlParameter
            pParam(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 50)
            pParam(0).Value = Session("sBsuid")

            pParam(1) = New SqlClient.SqlParameter("@VisaBSU_ID", SqlDbType.VarChar, 50)
            pParam(1).Value = ddlUnits.SelectedValue

            pParam(2) = New SqlClient.SqlParameter("@Month", SqlDbType.VarChar, 2)
            pParam(2).Value = ddlMonth.SelectedValue

            pParam(3) = New SqlClient.SqlParameter("@Year", SqlDbType.VarChar, 50)
            pParam(3).Value = ddlYear.SelectedValue

            pParam(4) = New SqlClient.SqlParameter("@BSU_WPSFileName", SqlDbType.VarChar, 100)
            pParam(4).Direction = ParameterDirection.Output

            pParam(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            pParam(5).Direction = ParameterDirection.ReturnValue

            pParam(6) = New SqlClient.SqlParameter("@ReturnMsg", SqlDbType.VarChar, 200)
            pParam(6).Direction = ParameterDirection.Output


            'pParam(5).Value = "BSU_WPSID"

            Dim iReturnvalue As String = ""

            'Use a DataReader to connect to  database.
            Dim dr As SqlDataReader

            dr = SqlHelper.ExecuteReader(stTrans, CommandType.StoredProcedure, "GET_empWPSDataForExport", pParam)

            'Enumerate the field names and records that are used to build the file.
            'For i = 0 To dr.FieldCount - 1
            '    strLine = strLine & dr.GetName(i).ToString & Chr(9)
            'Next

            'Write the field name information to file.
            'objStreamWriter.WriteLine(strLine)

            'Reinitialize the string for data.
            strLine = ""
            Dim iCount As Integer = 0
            'Enumerate the database that is used to populate the file.
            If dr.HasRows() Then
                While dr.Read()
                    For i = 0 To dr.FieldCount - 1
                        strLine = strLine & dr.GetValue(i)
                        iCount = iCount + 1
                        If i <> (dr.FieldCount - 1) Then
                            strLine = strLine & Chr(44)
                        End If
                    Next
                    objStreamWriter.WriteLine(strLine)
                    'Append last row as shown below
                    strLine = ""
                End While
            Else
                objStreamWriter.WriteLine("No data available")

            End If
            'lblError.Text = "File saved in " & fileName
            'Clean up.
            dr.Close()
            objStreamWriter.Close()
            objFileStream.Close()

            'Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            Dim str_imgvirtual As String = Web.Configuration.WebConfigurationManager.AppSettings("UploadCVPath").ToString()


            Dim BSUWPSID As String = pParam(4).Value
            iReturnvalue = pParam(5).Value
            If iReturnvalue = 975 Then

                lblError.Text = UtilityObj.getErrorMessage("975")
                objStreamWriter.Close()
                objFileStream.Close()
                System.IO.File.Delete(fileName)
                Return 1000

            End If
            If iReturnvalue <> 0 Then
                If pParam(6).Value.ToString <> "" Then
                    lblError.Text = pParam(6).Value.ToString
                End If
                objStreamWriter.Close()
                objFileStream.Close()
                System.IO.File.Delete(fileName)
                Return 1000
            End If
            'objConn.Close()

            ''''''''To download '''''''''''''''''''''''
            If (iCount > 0 And iReturnvalue = 0) Then
                Dim iFileDone As Integer = DisplayDownloadDialog(str_imgvirtual & "temporary\" & fileExcel, BSUWPSID)
                If iFileDone = 0 Then
                    lblError.Text = UtilityObj.getErrorMessage("978")
                    Return 0
                Else
                    objStreamWriter.Close()
                    objFileStream.Close()
                    System.IO.File.Delete(fileName)
                    lblError.Text = "File could not be generated"
                    Return 1000
                End If
            Else
                objStreamWriter.Close()
                objFileStream.Close()
                System.IO.File.Delete(fileName)
                lblError.Text = UtilityObj.getErrorMessage("976")
                Return 1000

            End If

            objStreamWriter.Close()
            objFileStream.Close()

            Return 0
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return 1000

        End Try
    End Function

    Private Function CreateSIFfile_Alternative(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        'Funtion to create a file in server by getting data from DB based on user selection and then download it from server to client system
        'After downloading the file is deleted from the server.
        Dim objFileStream As MemoryStream
        Dim objStreamWriter As StreamWriter

        objFileStream = New MemoryStream()
        objStreamWriter = New StreamWriter(objFileStream)
        Try
            Dim i As Integer
            Dim strLine As String = ""
            Dim link As String = ""

            Dim pParam(7) As SqlClient.SqlParameter
            pParam(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 50)
            pParam(0).Value = Session("sBsuid")

            pParam(1) = New SqlClient.SqlParameter("@VisaBSU_ID", SqlDbType.VarChar, 50)
            pParam(1).Value = ddlUnits.SelectedValue

            pParam(2) = New SqlClient.SqlParameter("@Month", SqlDbType.VarChar, 2)
            pParam(2).Value = ddlMonth.SelectedValue

            pParam(3) = New SqlClient.SqlParameter("@Year", SqlDbType.VarChar, 50)
            pParam(3).Value = ddlYear.SelectedValue

            pParam(4) = New SqlClient.SqlParameter("@BSU_WPSFileName", SqlDbType.VarChar, 100)
            pParam(4).Direction = ParameterDirection.Output

            pParam(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            pParam(5).Direction = ParameterDirection.ReturnValue

            pParam(6) = New SqlClient.SqlParameter("@ReturnMsg", SqlDbType.VarChar, 200)
            pParam(6).Direction = ParameterDirection.Output

            pParam(7) = New SqlClient.SqlParameter("@Bank_ACT_ID", SqlDbType.VarChar, 30)
            pParam(7).Value = txtBankCode.Text

            Dim iReturnvalue As String = ""

            ' swapna changes----------
            Dim cmd As New SqlCommand
            cmd.Parameters.Add(pParam(0))
            cmd.Parameters.Add(pParam(1))
            cmd.Parameters.Add(pParam(2))
            cmd.Parameters.Add(pParam(3))
            cmd.Parameters.Add(pParam(4))
            cmd.Parameters.Add(pParam(5))
            cmd.Parameters.Add(pParam(6))
            cmd.Parameters.Add(pParam(7))
            cmd.Connection = objConn
            cmd.Transaction = stTrans
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "GET_empWPSDataForExport"
            cmd.CommandTimeout = 0

            'Use a DataReader to connect to  database.
            Dim dr As SqlDataReader


            'dr = SqlHelper.ExecuteReader(stTrans, CommandType.StoredProcedure, "GET_empWPSDataForExport", pParam)
            dr = cmd.ExecuteReader()
            'Reinitialize the string for data.
            strLine = ""
            'Enumerate the database that is used to populate the file.
            If dr.HasRows() Then
                While dr.Read()
                    For i = 0 To dr.FieldCount - 1
                        strLine = strLine & dr.GetValue(i)
                        If i <> (dr.FieldCount - 1) Then
                            strLine = strLine & Chr(44)
                        End If
                    Next
                    objStreamWriter.WriteLine(strLine)
                    'Append last row as shown below
                    strLine = ""
                End While
            Else
                dr.Close()
                lblError.Text = Convert.ToString(pParam(6).Value)
                'lblError.Text = "No data available"
                Return 1000
            End If
            'lblError.Text = "File saved in " & fileName
            'Clean up.
            dr.Close()
            Dim BSUWPSID As String = Convert.ToString(pParam(4).Value)
            iReturnvalue = pParam(5).Value

            objStreamWriter.Close()
            objFileStream.Close()

            If iReturnvalue = 975 Then
                lblError.Text = UtilityObj.getErrorMessage("975")
                Return 1000
            End If
            If iReturnvalue <> 0 Then
                If pParam(6).Value.ToString <> "" Then
                    lblError.Text = pParam(6).Value.ToString
                End If
                Return 1000
            End If

            Dim bytearray() As Byte
            bytearray = objFileStream.ToArray
            SaveSifFiletoDB(bytearray, objConn, stTrans)
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=" & BSUWPSID)
            'Response.AddHeader("Cache-control", "no-cache")
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.BinaryWrite(bytearray)

            Response.Flush()
            Response.Close()
            'Response.End()
            Return 0

        Catch ex As Exception
            lblError.Text = ex.Message
            Return 1000
        Finally
            objStreamWriter.Close()
            objFileStream.Close()
        End Try

    End Function
    Private Function SaveSifFiletoDB(ByVal bytearray() As Byte, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            'Dim bytes() As Byte
            'bytes = System.Text.Encoding.ASCII.GetBytes(BitArray)

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("InsertWPSSifFile", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpSifData As New SqlParameter("@SifContent", SqlDbType.VarBinary)
            sqlpSifData.Value = bytearray ' bytearray
            cmd.Parameters.Add(sqlpSifData)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then

                Return 1000

            Else

                Return iReturnvalue
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try

    End Function

    Private Function UpdateExportToDB(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        'Function to update the database after file has been exported for a particular BSU with selected visa issuing unit
        Dim iReturnvalue As Integer = 0
        Try

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("UpdateWPSExport", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpVisaBSU_ID As New SqlParameter("@VisaBSU_ID", SqlDbType.VarChar, 15)
            sqlpVisaBSU_ID.Value = ddlUnits.SelectedValue
            cmd.Parameters.Add(sqlpVisaBSU_ID)

            Dim sqlpMonth As New SqlParameter("@Month", SqlDbType.VarChar, 15)
            sqlpMonth.Value = ddlMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpYear As New SqlParameter("@Year", SqlDbType.VarChar, 15)
            sqlpYear.Value = ddlYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)

            Dim sqlpuser As New SqlParameter("@user", SqlDbType.VarChar, 100)
            sqlpuser.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpuser)


            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then

                Return 1000

            Else
                BindVisaUnits()
                Return iReturnvalue
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function

    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        BindVisaUnits()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
