<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empLeaveApplicationListCompare.aspx.vb" Inherits="Payroll_empLeaveApplicationListCompare" Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html>
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<script>
    function Popup(url) {
        $.fancybox({
            'width': '1200',
            'height': '600',
            'autoScale': false,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': url
        });
    };
    function ClosePopup(a) {
        parent.$.fancybox.close()
        parent.AcceptValues(a)

    }
</script>
<%--<script>
    function GetWidth(){
        document.getElementById('<%=hf_width.ClientID%>').val = $(document).width();
        document.getElementById('<%=hf_height.ClientID%>').val = $(document).height();
        //alert($(".fancybox-wrap").width() + 'x' + $(document).height());
        alert($(document).width())
    }
</script>--%>
<style>
    .RadPivotGrid th, .RadPivotGrid td, .RadPivotGrid .rpgOuterTableWrapper td{
        
    text-align: center !important;
    vertical-align: middle !important;
    }
    .RadPivotGrid .rpgColumnHeader{
            background-image: none !important;
    }



</style>

<body width="100%">
    <form runat="server">
        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
        <div>


                        <telerik:RadPivotGrid ID="RadPivotGrid1" runat="server" Width="1200" Height="600" ShowColumnHeaderZone="false" ShowDataHeaderZone="false" ShowRowHeaderZone="false" 
                            AllowPaging="true" AllowFiltering="true" ShowFilterHeaderZone="false" EnableZoneContextMenu="true" OnCellDataBound="RadPivotGrid1_CellDataBound" PageSize="100">
                            <ClientSettings Scrolling-AllowVerticalScroll="true">
                            </ClientSettings>
                            <DataCellStyle Width="31px" Height="31px" CssClass="round-column" />
                            <ColumnHeaderCellStyle  BackColor="#8BB1A1" />
                            <Fields>
                                <telerik:PivotGridColumnField DataField="X" CellStyle-Font-Size="X-Small">
                                </telerik:PivotGridColumnField>
                                <telerik:PivotGridRowField ZoneIndex="1" DataField="Photo"  >
                                    <CellTemplate>
                                        <asp:Image ID="img_Photo" runat="server" CssClass="user-profile" ImageUrl='<%# Container.DataItem%>' Width="50px" Height="50px" Style="border-radius: 50%;" />
                                    </CellTemplate>
                                </telerik:PivotGridRowField>
                                <telerik:PivotGridRowField ZoneIndex="0" DataField="Names" Caption="Select Staff"  >
                                    <CellTemplate>
                                        <asp:Label ID="lbl1" runat="server" Text='<%# Container.DataItem%>' Width="100%"></asp:Label>
                                    </CellTemplate>
                                </telerik:PivotGridRowField>
                                <telerik:PivotGridAggregateField DataField="Y" Aggregate="Sum">
                                </telerik:PivotGridAggregateField>
                            </Fields>
                            <ClientSettings EnableFieldsDragDrop="false">
                                <ClientMessages DragToReorder="Drag the field to change its order"></ClientMessages>
                            </ClientSettings>
                            <TotalsSettings GrandTotalsVisibility="None" ColumnsSubTotalsPosition="None" RowsSubTotalsPosition="None" />
                        </telerik:RadPivotGrid>
                        <asp:Literal ID="lt_Lengend" runat="server"></asp:Literal>
        </div>

        <div>
            <telerik:RadHtmlChart runat="server" ID="RadHtmlChart1" Width="1200" Height="500" Skin="Metro" Transitions="true" Visible="false">
                <PlotArea>
                    <XAxis DataLabelsField="days">
                        <LabelsAppearance DataFormatString="dd MMM yy" RotationAngle="270"></LabelsAppearance>
                        <MajorGridLines Visible="false" />
                        <MinorGridLines Visible="false" />
                    </XAxis>
                    <YAxis Visible="false">
                        <LabelsAppearance Visible="false"></LabelsAppearance>
                        <MajorGridLines Visible="false" />
                        <MinorGridLines Visible="false" />
                    </YAxis>
                    <Series>
                        <telerik:ColumnSeries DataFieldY="Y" GroupName="Type">
                            <TooltipsAppearance Color="White" DataFormatString="{0}">
                                <ClientTemplate>
                                Staff: <span style="color:red">#=dataItem.Names# </span></br>
                                Leave Date: <span style="color:red">#=dataItem.X# </span></br>
                                Leave Type: <span style="color:red">Annual Leave </span>
                                </ClientTemplate>
                            </TooltipsAppearance>
                            <LabelsAppearance Position="Center">
                                <TextStyle Color="White" />
                            </LabelsAppearance>
                        </telerik:ColumnSeries>
                    </Series>
                </PlotArea>
                <Appearance FillStyle-BackgroundColor="White"></Appearance>
                <ChartTitle Text="Leave Comparision">
                </ChartTitle>
            </telerik:RadHtmlChart>
        </div>
        <asp:HiddenField ID="hf_width" runat="server" />
        <asp:HiddenField ID="hf_height" runat="server" />
    </form>
</body>
</html>
