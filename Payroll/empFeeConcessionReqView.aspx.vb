Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empFeeConcessionReqView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Property bTRANSPORT() As Boolean
        Get
            Return ViewState("bTRANSPORT")
        End Get
        Set(value As Boolean)
            ViewState("bTRANSPORT") = value
        End Set
    End Property
    Public Property bMatchNames() As Boolean
        Get
            Return ViewState("bMatchNames")
        End Get
        Set(value As Boolean)
            ViewState("bMatchNames") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000085" And ViewState("MainMnu_code") <> "F733034") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "H000085"
                            lblHead.Text = "Approve Fee Concession Request"
                            bTRANSPORT = False
                            rblFilter.Items(0).Text = ""
                            rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")
                        Case "F733034"
                            lblHead.Text = "Approve Staff Fee Concession Request"
                            bTRANSPORT = True
                    End Select
                    Me.btnApprove.Enabled = False
                    Me.btnReject.Enabled = False
                    bMatchNames = True
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Private Sub SetNameMismatchAlert()
        If gvJournal.Rows.Count <= 0 Then
            Me.lblAlert.Text = ""
        ElseIf bMatchNames = False Then
            Me.lblAlert.Text = "Highlighted are Concession(s) having mismatch in Employee name and Parent name of Child, Please verify before approval"
        Else
            Me.lblAlert.Text = ""
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub rblFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            lstrCondn8 = ""
            str_Filter = ""
            Select ViewState("MainMnu_code").ToString
                Case "H000085"
                    rblFilter.Items(0).Text = ""
                    rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")
            End Select

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 2  txtEmpName
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpName")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNAME", lstrCondn2)

                '   -- 3  txtDept
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDept")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_DEPT_DESCR", lstrCondn3)

                '   -- 4   txtEmpDesign
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpDesign")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_DES_DESCR", lstrCondn4)

                '   -- 5  txtStudName
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)

                '   -- 6  txtGrade
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn6)

                '   -- 7  txtStuno
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn7)
                '   -- 8  txtEmpBsu
                larrSearchOpr = h_selected_menu_8.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpBsu")
                lstrCondn8 = txtSearch.Text
                If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_BSU_SHORTNAME", lstrCondn8)
            End If
            Dim STR_RECEIPT As String = "FEES.vw_STUDENT_CONCESSION_REQUEST"

            Me.gvJournal.Columns(0).Visible = False
            Me.gvJournal.Columns(15).Visible = False
            Me.btnApprove.Enabled = False
            Me.btnReject.Enabled = False

            Dim fldApproval As String = IIf(bTRANSPORT = True, "SCD_APPR_STATUS='A' AND SCD_APPR_PROVIDER_STATUS", "SCD_APPR_STATUS")
            Dim rblStatusFilter As String = Me.rblFilter.SelectedValue

            If rblStatusFilter = "APD" Then
                str_Filter = str_Filter & " AND " & fldApproval & "='A'"
            ElseIf rblStatusFilter = "PND" Then
                str_Filter = str_Filter & " AND " & fldApproval & "='P'"
                Me.gvJournal.Columns(0).Visible = True
                Me.gvJournal.Columns(15).Visible = True
                Me.btnApprove.Enabled = True
                Me.btnReject.Enabled = True
            ElseIf rblStatusFilter = "RJD" Then
                str_Filter = str_Filter & " AND " & fldApproval & "='R'"
            ElseIf rblStatusFilter = "PWU" Then
                str_Filter = str_Filter & " AND SCD_APPR_STATUS='P' AND SCD_APPR_PROVIDER_STATUS='P'"
            Else
                str_Filter = str_Filter & " AND " & fldApproval & "<>'X'" ' Show All
            End If

            Dim str_topFilter As String = ""
            If ddlTopFilter.SelectedItem.Value <> "All" Then
                str_topFilter = " TOP " & ddlTopFilter.SelectedItem.Value
            End If

            Dim USR_EMP_DEPT_ID As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(EMP_DPT_ID,0)EMP_DPT_ID FROM dbo.EMPLOYEE_M WITH ( NOLOCK ) WHERE  EMP_ID = " & Session("EmployeeId") & "")

            If Not bTRANSPORT Then ' SChool Approval
                str_Sql = "SELECT " & str_topFilter & " * FROM " & STR_RECEIPT & " WHERE SCR_EMP_BSU_ID='" & Session("sBSUID") & "'" & str_Filter
                '& " AND EMP_DPT_ID = CASE WHEN SCR_EMP_BSU_ID = '999998' THEN " & USR_EMP_DEPT_ID & " ELSE EMP_DPT_ID END"
                str_Sql += " ORDER BY SCR_DATE DESC, STU_NAME"
            Else 'Transport Approval
                str_Sql = "SELECT " & str_topFilter & " * FROM " & STR_RECEIPT & " WHERE SCD_PROVIDER_BSU_ID='" & Session("sBSUID") & "'" _
                             & str_Filter & " ORDER BY SCR_DATE DESC, STU_NAME"
            End If


            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case "H000085"
                    gvJournal.Columns(15).Visible = False
                    gvJournal.Columns(16).Visible = False
                Case "F733034"
                    gvJournal.Columns(15).Visible = True
                    gvJournal.Columns(16).Visible = True
            End Select
            bMatchNames = True

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtDept")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpDesign")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn7

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpBsu")
            txtSearch.Text = lstrCondn8

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopFilter.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbDetail As LinkButton = DirectCast(e.Row.FindControl("lbDetail"), LinkButton)
            Dim lblSCR_DATE As Label = DirectCast(e.Row.FindControl("lblSCR_DATE"), Label)
            Dim hf_EMPID As HiddenField = DirectCast(e.Row.FindControl("hf_EMPID"), HiddenField)
            Dim hf_STUID As HiddenField = DirectCast(e.Row.FindControl("hf_STUID"), HiddenField)
            Dim hf_SCRID As HiddenField = DirectCast(e.Row.FindControl("hf_SCRID"), HiddenField)
            Dim hf_SCDID As HiddenField = DirectCast(e.Row.FindControl("hf_SCDID"), HiddenField)
            Dim hf_STATUS As HiddenField = DirectCast(e.Row.FindControl("hf_STATUS"), HiddenField)
            Dim hf_SSVID As HiddenField = DirectCast(e.Row.FindControl("hf_SSVID"), HiddenField)
            Dim txtCnFrom As TextBox = DirectCast(e.Row.FindControl("txtCnFrom"), TextBox)
            Dim txtCnTo As TextBox = DirectCast(e.Row.FindControl("txtCnTo"), TextBox)
            Dim lblErrorMessage As Label = DirectCast(e.Row.FindControl("lblErrorMessage"), Label)
            If Not lblErrorMessage Is Nothing AndAlso lblErrorMessage.Text <> "" Then
                gvJournal.Columns(20).Visible = True
                lblErrorMessage.ForeColor = Drawing.Color.DarkRed
            End If
            lbDetail.Attributes.Clear()
            If Not lbDetail Is Nothing AndAlso hf_EMPID.Value <> "" AndAlso hf_STUID.Value <> "" AndAlso hf_STATUS.Value <> "" AndAlso hf_SCDID.Value <> "" AndAlso hf_SCRID.Value <> "" Then
                'lbDetail.Attributes.Add("onClick", "return ShowSubWindow('empFeeConcessionReqDetail.aspx?EMP_ID=" & Encr_decrData.Encrypt(hf_EMPID.Value) & "&STU_ID=" & Encr_decrData.Encrypt(hf_STUID.Value) & "&SCR_ID=" & Encr_decrData.Encrypt(hf_SCRID.Value) & "&SCD_ID=" & Encr_decrData.Encrypt(hf_SCDID.Value) & "&STATUS=" & Encr_decrData.Encrypt(hf_STATUS.Value) & "&bTRANSPORT=" & Encr_decrData.Encrypt(bTRANSPORT) & "', '', '60%', '80%');")
                lbDetail.Attributes.Add("onClick", "ViewDetail('empFeeConcessionReqDetail.aspx?EMP_ID=" & Encr_decrData.Encrypt(hf_EMPID.Value) & "&STU_ID=" & Encr_decrData.Encrypt(hf_STUID.Value) & "&SCR_ID=" & Encr_decrData.Encrypt(hf_SCRID.Value) & "&SCD_ID=" & Encr_decrData.Encrypt(hf_SCDID.Value) & "&STATUS=" & Encr_decrData.Encrypt(hf_STATUS.Value) & "&bTRANSPORT=" & Encr_decrData.Encrypt(bTRANSPORT) & "', '', '60%', '80%'); return false;")
                If Not CompareEmpParent(hf_EMPID.Value, hf_STUID.Value) And (Me.rblFilter.SelectedValue = "ALL" Or Me.rblFilter.SelectedValue = "PND") Then 'Returns False if Mismatch and shows message only if pending or All
                    e.Row.ForeColor = Drawing.Color.Red
                    bMatchNames = False
                End If
                'lbDetail.Attributes.Add("onClick", "alert('hi');")
            Else
                lbDetail.Attributes.Add("onClick", "return false;")
            End If
            If Not txtCnFrom Is Nothing AndAlso Not txtCnTo Is Nothing AndAlso Not hf_SSVID Is Nothing Then
                If txtCnFrom.Text.Trim = "" And txtCnTo.Text.Trim = "" Then
                    SetConcessionDates(txtCnFrom, txtCnTo, hf_EMPID.Value, hf_SSVID.Value, lblSCR_DATE.Text)
                End If
            End If
        End If
    End Sub

    Private Sub SetConcessionDates(ByRef txtF As TextBox, ByRef txtT As TextBox, ByVal EMPID As Long, ByVal SSVID As Integer, ByVal EntryDt As String)
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("[FEES].[SP_GET_PERIOD_FOR_STAFF_CONCESSION_AVAILING]", ConnectionManger.GetOASISTransportConnection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(5) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@ENTRYDT", SqlDbType.DateTime)
            pParms(0).Value = EntryDt
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@REF_ID", SqlDbType.Int)
            pParms(1).Value = EMPID
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@SSV_ID", SqlDbType.Int)
            pParms(2).Value = SSVID
            cmd.Parameters.Add(pParms(2))

            pParms(3) = New SqlClient.SqlParameter("@DT_FROM", SqlDbType.DateTime)
            pParms(3).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(3))

            pParms(4) = New SqlClient.SqlParameter("@DT_TO", SqlDbType.DateTime)
            pParms(4).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(4))

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            txtF.Text = IIf(txtF.Text = "", pParms(3).Value, txtF.Text)
            txtT.Text = IIf(txtT.Text = "", pParms(4).Value, txtT.Text)
        Catch
            'txtF.Text = String.Format("dd/MMM/yyyy", DateTime.Now.ToShortDateString)
            'txtT.Text = ""
        Finally

        End Try
    End Sub

    Private Function CompareEmpParent(ByVal EMPID As Int64, ByVal STUID As Int64) As Boolean
        CompareEmpParent = True

        Try
            Dim bMatch As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT dbo.fnCompEmployeeAndParent(" & EMPID & "," & STUID & ")")
            CompareEmpParent = bMatch
        Catch ex As Exception
            CompareEmpParent = False
        End Try


    End Function

    Private Function GetSelectedRows() As String
        GetSelectedRows = ""
        For Each gvr As GridViewRow In gvJournal.Rows
            Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
            Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
            If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                GetSelectedRows &= "|" & hf_SCDID.Value
            End If
        Next
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        'If GetSelectedRows() <> "" Then
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim bRolledBack As Boolean = False
        Dim bCommitted As Boolean = False
        Dim Trans As SqlTransaction '= objConn.BeginTransaction("APRVE")
        Try
            Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
            For Each gvr As GridViewRow In gvJournal.Rows

                Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
                Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
                Dim txtCnFrom As TextBox = DirectCast(gvr.FindControl("txtCnFrom"), TextBox)
                Dim txtCnTo As TextBox = DirectCast(gvr.FindControl("txtCnTo"), TextBox)
                Dim lblErrorMessage As Label = DirectCast(gvr.FindControl("lblErrorMessage"), Label)
                If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                    Dim retval As String = "1"
                    If objcls.VALIDATE_CONCESSION_REQUEST(hf_SCDID.Value, lblErrorMessage.Text) = True Then
                        Trans = objConn.BeginTransaction("APVE")
                        If bTRANSPORT AndAlso (Not IsDate(txtCnFrom.Text) Or Not IsDate(txtCnTo.Text)) Then
                            retval = "1"
                        Else
                            objcls.PROCESS = "A"
                            objcls.User = Session("sUsr_name")
                            objcls.SCR_SCD_IDs = hf_SCDID.Value 'GetSelectedRows()
                            objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
                            objcls.bTRANSPORT = bTRANSPORT
                            objcls.SCD_CONCESSION_FROMDT = txtCnFrom.Text
                            objcls.SCD_CONCESSION_TODT = txtCnTo.Text
                            retval = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
                            'retval = 10
                        End If
                        If retval <> "0" Then
                            bRolledBack = True
                            Trans.Rollback()
                        Else
                            Trans.Commit()
                            bCommitted = True
                        End If
                    Else
                        bRolledBack = True
                        gvJournal.Columns(20).Visible = True
                    End If
                End If
            Next
        Catch ex As Exception
            'Me.lblError.Text = "Unable to approve Request(s)"
            Trans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If bRolledBack = True And bCommitted = True Then
                'Me.lblError.Text = "Not all request(s) have been approved, check the 'Error' column for details"
                usrMessageBar.ShowNotification("Not all request(s) have been approved, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
            ElseIf bCommitted Then
                'Me.lblError.Text = "Selected Request(s) have been approved"
                usrMessageBar.ShowNotification("Selected Request(s) have been approved", UserControls_usrMessageBar.WarningType.Success)
            ElseIf bRolledBack Then
                'Me.lblError.Text = "Unable to approve Request(s)"
                usrMessageBar.ShowNotification("Unable to approve Request(s)", UserControls_usrMessageBar.WarningType.Danger)
            End If
            gridbind()
        End Try
        'Else
        'Me.lblError.Text = "Please select one or more concession(s) to be approved"
        'End If
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If GetSelectedRows() <> "" Then
            Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim Trans As SqlTransaction = objConn.BeginTransaction("REJECT")
            Try
                objcls.PROCESS = "R"
                objcls.User = Session("sUsr_name")
                objcls.SCR_SCD_IDs = GetSelectedRows()
                objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
                objcls.bTRANSPORT = bTRANSPORT
                Dim retval As String = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
                If retval <> "0" Then
                    'Me.lblError.Text = UtilityObj.getErrorMessage(retval)
                    usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                    Trans.Rollback()
                Else
                    Trans.Commit()
                    'Me.lblError.Text = "Selected Request(s) have been rejected"
                    usrMessageBar.ShowNotification("Selected Request(s) have been rejected", UserControls_usrMessageBar.WarningType.Success)
                    gridbind()
                End If
            Catch ex As Exception
                'Me.lblError.Text = "Unable to Reject"
                usrMessageBar.ShowNotification("Unable to Reject", UserControls_usrMessageBar.WarningType.Danger)
                Trans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try


        Else
            'Me.lblError.Text = "Please select one or more concession(s) to be rejected"
            usrMessageBar.ShowNotification("Please select one or more concession(s) to be rejected", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Protected Sub gvJournal_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvJournal.DataBound
        SetNameMismatchAlert()
    End Sub


End Class
