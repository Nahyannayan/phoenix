<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empViewDetails.aspx.vb"
    Inherits="Payroll_empViewDetails" Theme="General" Title="Employee Leave Details" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/sb-admin.css" rel="Stylesheet"  type="text/css"/>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../cssfiles/RadStyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">   
      
    </script>

    <style type="text/css">
        .RadGrid_Default {
            border: 0px !important;
        }
        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border: 1px solid rgba(0,0,0,0.16);
        }
    </style>
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server">
    <!--1st drop down menu -->
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600"
        EnablePageMethods="true">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                      

            <table align="center" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td width="15%">
                                    <span class="field-label">Employee No.</span> 
                                </td>
                                
                                <td width="25%">
                                    <asp:Label ID="lblEmpNo" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                                <td width="15%">
                                    <span class="field-label">Employee Name</span>
                                </td>
                                
                                <td width="45%">
                                    <asp:Label ID="lblEMPName" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ajaxToolkit:TabContainer ID="TCMainContainer" runat="server" ActiveTabIndex="2"
                            Width="100%" ForeColor="Maroon">
                            <ajaxToolkit:TabPanel ID="TPEmpDetail" runat="server" HeaderText="qwerty">
                                <HeaderTemplate>
                                    Employee Information
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table align="left" width="100%" style="vertical-align: top">
                                        <tr>
                                            <td width="80%" valign="top">
                                                <table align="left" width="100%">
                                                    <tr>
                                                        <td width="40%">
                                                           <span class="field-label"> Date Of Join</span>
                                                        </td>
                                                        
                                                        <td width="45%">
                                                            <asp:Label ID="lblDOJ" runat="server"  CssClass="field-value"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">
                                                           <span class="field-label"> Reporting To</span>
                                                        </td>
                                                        
                                                        <td width="45%">
                                                            <asp:Label ID="lblReportingTo" runat="server"  CssClass="field-value"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">
                                                            <span class="field-label">Attendance Reporting To</span>
                                                        </td>
                                                        
                                                        <td width="45%">
                                                            <asp:Label ID="lblATTReportingTo" runat="server"  CssClass="field-value"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">
                                                            <span class="field-label">Attendance Binding</span>
                                                        </td>
                                                       
                                                        <td width="45%">
                                                            <asp:Label ID="lblATTBinding" runat="server"  CssClass="field-value"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20%" align="center">
                                                <asp:ImageMap ID="imgEmployee" runat="server" Height="150px" Width="130px">
                                                </asp:ImageMap>
                                            </td>
                                    </table>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel runat="server" ID="TPLeaveDetail">
                                <HeaderTemplate>
                                    Leave Detail
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <ajaxToolkit:TabContainer ID="TabSubLeaveDetail" runat="server" ActiveTabIndex="0"
                                        Width="100%" >
                                        <ajaxToolkit:TabPanel ID="TPLeaveSummary" runat="server">
                                            <HeaderTemplate>
                                                Leave Summary
                                            </HeaderTemplate>
                                            <ContentTemplate>
                                                <table align="center" width="100%">
                                                    <tr>
                                                        <td width="50%" style="vertical-align: top; text-align: left">
                                                            <telerik:RadGrid ID="RadLeaveSummary" CssClass="table table-bordered table-row" runat="server" ShowStatusBar="True" AutoGenerateColumns="False"
                                                                AllowSorting="True" AllowAutomaticDeletes="True" AllowAutomaticInserts="True"
                                                                AllowAutomaticUpdates="True" GridLines="None">
                                                                <AlternatingItemStyle HorizontalAlign="Left" BorderStyle="Outset"></AlternatingItemStyle>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView DataKeyNames="ELS_ELT_ID" AllowMultiColumnSorting="True" Width="100%"
                                                                    CommandItemDisplay="Top" Name="VW_LEAVE_SUMMARY" EditMode="InPlace" PageSize="8"
                                                                    NoDetailRecordsText="">
                                                                    <DetailTables>
                                                                        <telerik:GridTableView DataKeyNames="ELT_DATE" Width="100%" runat="server" CommandItemDisplay="Top"
                                                                            Name="VW_LEAVE_DETAIL" EditMode="InPlace" PageSize="5" GridLines="Both">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn DataField="ELT_DATE" HeaderText="DATE" UniqueName="colDate">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="DATELabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ELT_DATE", "{0 :dd/MMM/yyyy}")%>'></asp:Label></ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn DataField="ELT_REMARKS" HeaderText="REMARKS" UniqueName="colREMARKS">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="REMARKSLabel" runat="server" Text='<%# Eval("ELT_REMARKS") %>'></asp:Label></ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </telerik:GridTableView>
                                                                    </DetailTables>
                                                                    <ExpandCollapseColumn Visible="True">
                                                                    </ExpandCollapseColumn>
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn DataField="ELS_EMP_ID" HeaderText="EMPID" UniqueName="colEMPID"
                                                                            Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hdnEMPID" runat="server" Value='<%# Eval("ELS_EMP_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn DataField="ELS_ELT_ID" HeaderText="ELTID" UniqueName="colELTID"
                                                                            Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hdnELTID" runat="server" Value='<%# Eval("ELS_ELT_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn DataField="ELT_DESCR" HeaderText="LEAVE TYPE" UniqueName="colEligible">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LeaveTypeLabel" runat="server" Text='<%# Eval("ELT_DESCR") %>'></asp:Label></ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn DataField="ELIGIBLE_LEAVE" HeaderText="ELIGIBLE" UniqueName="colEligible">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="ELIGIBLELabel" runat="server" Text='<%# Eval("ELIGIBLE_LEAVE") %>'></asp:Label></ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn DataField="LEAVE_TAKEN" HeaderText="LEAVE TAKEN" UniqueName="colLEAVETAKEN">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LeaveTakenLabel" runat="server" Text='<%# Eval("LEAVE_TAKEN") %>'></asp:Label></ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn DataField="BAL_LEAVE" HeaderText="BALANCE" UniqueName="colBAL_LEAVE">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="BAL_LEAVELabel" runat="server" Text='<%# Eval("BAL_LEAVE") %>'></asp:Label></ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <HeaderStyle BackColor="ActiveCaption"></HeaderStyle>
                                                                </MasterTableView>
                                                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                <GroupHeaderItemStyle  />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                            <HeaderTemplate>
                                                Leave Summary(Chart)
                                            </HeaderTemplate>
                                            <ContentTemplate>
                                                <table align="center" width="100%">
                                                    <tr>
                                                        <td width="50%" style="vertical-align: top; text-align: left">
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Literal ID="FCPieLiteral" runat="server"></asp:Literal>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Literal ID="FCBarLiteral" runat="server"></asp:Literal>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="error"></asp:Label>
                                                                </td>
                                                            </tr>
                                                </table>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                    </ajaxToolkit:TabContainer>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                <HeaderTemplate>
                                    Attendance History
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table align="center" width="100%">
                                        <tr>
                                            <td width="50%" style="vertical-align: top; text-align: left">
                                                <asp:Label ID="lblHistoryText" runat="server" CssClass="RadLabelTextHeader"></asp:Label>
                                                <telerik:RadGrid ID="RadLeaveHistory" cssclass="table table-bordered table-row" runat="server" AllowSorting="True" enableajax="True"
                                                    ShowStatusBar="True" AutoGenerateColumns="False" GroupingEnabled="False" ShowGroupPanel="True"
                                                    Width="100%" GridLines="None">
                                                    <AlternatingItemStyle CssClass="RadGridAltRow" />
                                                    <ItemStyle CssClass="RadGridRow" />
                                                    <MasterTableView GridLines="Both">
                                                        <Columns>
                                                            <telerik:GridTemplateColumn DataField="ELT_DESCR" HeaderText="Leave Type" UniqueName="colLeaveType">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LeaveTypeLabel" runat="server" Text='<%# Eval("ELT_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn DataField="AUDIT_USR" HeaderText="User" UniqueName="colAuditUsr">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="UserLabel" runat="server" Text='<%# Eval("AUDIT_USR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn DataField="AUDIT_TIME" HeaderText="LOG DT" UniqueName="colLogDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LogDTLabel" runat="server" Text='<%# Eval("AUDIT_TIME") %>'>></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                        <EditFormSettings>
                                                            <EditColumn ButtonType="ImageButton">
                                                            </EditColumn>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="h_SDate" runat="server" />
            <asp:HiddenField ID="h_Emp_ID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
