<%@ Page Language="VB"   AutoEventWireup="false"
    CodeFile="empLeaveApplicationList.aspx.vb" Inherits="Payroll_empLeaveApplicationList"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html>
    <script language="javascript" type="text/javascript">
  
 
 function ShowLeaveDetail(id)
       {    
            //var sFeatures;
            //sFeatures="dialogWidth: 700px; ";
            //sFeatures+="dialogHeight: 450px; ";
            //sFeatures+="help: no; ";
            //sFeatures+="resizable: no; ";
            //sFeatures+="scroll: yes; ";
            //sFeatures+="status: no; ";
            //sFeatures+="unadorned: no; ";
            //var NameandCode;
            //var result;
            //result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)
         
     //return false;
     Popup("ShowEmpLeave.aspx?ela_id="+id)
        }
    </script>
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
        <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <script>
        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
        function ClosePopup(a) {
            parent.$.fancybox.close()
            parent.AcceptValues(a)

        }
    </script>

    <body>
        <form id="form1" runat="server">



    <table align="center" width="100%" >
        <tr valign="top">
            <td width="50%" align="left">
                <asp:HyperLink id="hlAddNew" runat="server" Visible="false">Add New</asp:HyperLink>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
            <td align="right" width="50%">
                 
            </td>
        </tr>
    </table>
    <table  width="100%" >
        <tr>
            <td align="center" width="100%">
              <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                    CssClass="table table-row table-bordered" Width="100%" AllowPaging="True" PageSize="30" >
                    <Columns>
                        <asp:BoundField DataField="EMP_NAME" HeaderText="Name" ReadOnly="True" 
                            SortExpression="EMP_NAME">
                        </asp:BoundField>
                        <asp:BoundField DataField="EMPNO" HeaderText="Staff ID" ReadOnly="True" 
                            SortExpression="EMPNO">
                        </asp:BoundField>
                        <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR">
                        </asp:BoundField>
                        <asp:BoundField DataField="LEAVEDAYS" HeaderText="Leave Days" ReadOnly="True" SortExpression="LEAVEDAYS">
                        </asp:BoundField>
                        <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date From"
                            HtmlEncode="False" SortExpression="ELA_DTFROM"></asp:BoundField>
                        <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date To"
                            HtmlEncode="False" SortExpression="ELA_DTTO"></asp:BoundField>
                        <asp:BoundField DataField="ELA_APPRSTATUS" HeaderText="Status" ReadOnly="True" SortExpression="ELA_APPRSTATUS">
                        </asp:BoundField>
                        <asp:BoundField DataField="ELA_APPRDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Approval Date"
                            HtmlEncode="False" SortExpression="ELA_APPRDATE"></asp:BoundField>
                        <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="<%# &quot;ShowLeaveDetail('&quot; & Container.DataItem(&quot;ELA_ID&quot;) & &quot;');return false;&quot; %>"
                                    Text='<%# Bind("ELA_REMARKS") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                               <%-- <asp:HyperLink ID="hlEdit" runat="server" >View</asp:HyperLink>--%>
                                <asp:LinkButton ID="lbView" runat="server" Text="View" OnClick="lbView_Click" ></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Print">
                            <ItemTemplate>
                                <asp:LinkButton ID="hlPrint" runat="server" Text="Print" OnClick="btnPrint_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                            <EditItemTemplate>
                                
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="APPRSTATUS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAPPRSTATUS" runat="server" Text='<%# Bind("APPRSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>

        </form>
    </body>
</html>
