﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj
Partial Class Payroll_empGenerateGIROFile
    Inherits System.Web.UI.Page
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                'Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'If Not Request.UrlReferrer Is Nothing Then
                '    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                'End If
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "P153061") 

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    lblError.Text = ""
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(GetMinMonthofSalary()))

                    BindYear()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub
    Protected Function GetMinMonthofSalary() As String
        Dim strMonth As String = "1"
        Dim iReturnvalue As Integer = 0
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim con As New SqlConnection(str_conn)
            con.Open()
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("GetMinMonthofSalary", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim outMinMonth As New SqlParameter("@MinMonth", SqlDbType.VarChar, 2)
            outMinMonth.Direction = ParameterDirection.Output
            cmd.Parameters.Add(outMinMonth)

            cmd.ExecuteNonQuery()
            strMonth = outMinMonth.Value
            con.Close()
            If strMonth <> "" Then
                Return strMonth
            Else
                Return "1"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return "1"
        End Try
    End Function
    Protected Sub BindYear()
        '----------create a list item to bind the years-------------
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim ds As New DataSet
        Dim cmd As String
        cmd = " select distinct esd_year from empsalarydata_d where esd_bsu_id='" & Session("sBsuid") & "' order by esd_year"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, cmd)
        ddlYear.DataSource = ds
        ddlYear.DataTextField = ds.Tables(0).Columns("esd_year").ToString
        ddlYear.DataValueField = ds.Tables(0).Columns("esd_year").ToString
        ddlYear.DataBind()

        ddlYear.SelectedIndex = ddlYear.Items.Count - 1
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try

            If txtValueDate.Text = "" Or IsDate(txtValueDate.Text) = False Then
                lblError.Text = "Please enter value date correctly."
                Exit Sub
            End If
            If txtBankCode.Text = "" Or txtBankDescr.Text = "" Then
                lblError.Text = "Select Bank"
                Exit Sub
            End If
            If CDate(txtValueDate.Text) < Today.Date Then
                lblError.Text = "Value date cannot be less than today's date."
                Exit Sub
            End If
            lblError.Text = ""
            GenerateGIROfile()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub GenerateGIROfile()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try

            objConn.Open()

            Dim trans As SqlTransaction = objConn.BeginTransaction("SampleTransaction")

            Dim errorno As Integer = 0
            'If errorno = 0 Then errorno = CreateSIFfile(objConn, trans)
            If errorno = 0 Then errorno = CreateGIROfile(objConn, trans)
            If errorno = 0 Then errorno = UpdateExportToDB(objConn, trans)
            If errorno = 0 Then
                trans.Commit()
                lblError.Text = ""

            Else
                trans.Rollback()
                'lblError.Text = "Error Occured"

            End If
            ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(GetMinMonthofSalary()))


        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            objConn.Close()

        End Try

    End Sub
    Private Function CreateGIROfile(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        'Funtion to create a file in server by getting data from DB based on user selection and then download it from server to client system
        'After downloading the file is deleted from the server.
        Dim objFileStream As MemoryStream
        Dim objStreamWriter As StreamWriter

        objFileStream = New MemoryStream()
        objStreamWriter = New StreamWriter(objFileStream)
        Try
            Dim i As Integer
            Dim strLine As String = ""
            Dim link As String = ""

            Dim pParam(8) As SqlClient.SqlParameter
            pParam(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 50)
            pParam(0).Value = Session("sBsuid")

            pParam(1) = New SqlClient.SqlParameter("@VisaBSU_ID", SqlDbType.VarChar, 50)
            pParam(1).Value = Session("sBsuid")

            pParam(2) = New SqlClient.SqlParameter("@Month", SqlDbType.VarChar, 2)
            pParam(2).Value = ddlMonth.SelectedValue

            pParam(3) = New SqlClient.SqlParameter("@Year", SqlDbType.VarChar, 50)
            pParam(3).Value = ddlYear.SelectedValue

            pParam(4) = New SqlClient.SqlParameter("@BSU_WPSFileName", SqlDbType.VarChar, 100)
            pParam(4).Direction = ParameterDirection.Output

            pParam(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            pParam(5).Direction = ParameterDirection.ReturnValue

            pParam(6) = New SqlClient.SqlParameter("@ReturnMsg", SqlDbType.VarChar, 200)
            pParam(6).Direction = ParameterDirection.Output

            pParam(7) = New SqlClient.SqlParameter("@Bank_ACT_ID", SqlDbType.VarChar, 30)
            pParam(7).Value = txtBankCode.Text


            pParam(8) = New SqlClient.SqlParameter("@valueDate", SqlDbType.DateTime)
            pParam(8).Value = txtValueDate.Text

            Dim iReturnvalue As String = ""

            ' swapna changes----------
            Dim cmd As New SqlCommand
            cmd.Parameters.Add(pParam(0))
            cmd.Parameters.Add(pParam(1))
            cmd.Parameters.Add(pParam(2))
            cmd.Parameters.Add(pParam(3))
            cmd.Parameters.Add(pParam(4))
            cmd.Parameters.Add(pParam(5))
            cmd.Parameters.Add(pParam(6))
            cmd.Parameters.Add(pParam(7))
            cmd.Parameters.Add(pParam(8))
            cmd.Connection = objConn
            cmd.Transaction = stTrans
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "GenerateGiroPaymentFile"
            cmd.CommandTimeout = 0

            'Use a DataReader to connect to  database.
            Dim dr As SqlDataReader


            'dr = SqlHelper.ExecuteReader(stTrans, CommandType.StoredProcedure, "GET_empWPSDataForExport", pParam)
            dr = cmd.ExecuteReader()
            'Reinitialize the string for data.
            strLine = ""
            'Enumerate the database that is used to populate the file.
            If dr.HasRows() Then
                While dr.Read()
                    For i = 0 To dr.FieldCount - 1
                        strLine = strLine & dr.GetValue(i)
                        'If i <> (dr.FieldCount - 1) Then
                        '    strLine = strLine '& Chr(44)
                        'End If
                    Next
                    objStreamWriter.WriteLine(strLine)
                    'Append last row as shown below
                    strLine = ""
                End While
            Else
                dr.Close()
                lblError.Text = Convert.ToString(pParam(6).Value)
                'lblError.Text = "No data available"
                Return 1000
            End If
            'lblError.Text = "File saved in " & fileName
            'Clean up.
            dr.Close()
            'Dim BSUWPSID As String = Convert.ToString(pParam(4).Value)
            'iReturnvalue = pParam(5).Value

            objStreamWriter.Close()
            objFileStream.Close()
            iReturnvalue = 0
            If iReturnvalue = 975 Then
                lblError.Text = UtilityObj.getErrorMessage("975")
                Return 1000
            End If
            If iReturnvalue <> 0 Then
                'If pParam(6).Value.ToString <> "" Then
                '    lblError.Text = pParam(6).Value.ToString
                'End If
                Return 1000
            End If

            Dim bytearray() As Byte
            bytearray = objFileStream.ToArray
            SaveSifFiletoDB(bytearray, objConn, stTrans)
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=Giro.txt")
            'Response.AddHeader("Cache-control", "no-cache")
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.BinaryWrite(bytearray)

            Response.Flush()
            Response.Close()
            'Response.End()
            Return 0

        Catch ex As Exception
            lblError.Text = ex.Message
            Return 1000
        Finally
            objStreamWriter.Close()
            objFileStream.Close()
        End Try

    End Function
    Private Function UpdateExportToDB(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        'Function to update the database after file has been exported for a particular BSU with selected visa issuing unit
        Dim iReturnvalue As Integer = 0
        Try

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("UpdateGIROExport", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpVisaBSU_ID As New SqlParameter("@VisaBSU_ID", SqlDbType.VarChar, 15)
            sqlpVisaBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpVisaBSU_ID)

            Dim sqlpMonth As New SqlParameter("@Month", SqlDbType.VarChar, 15)
            sqlpMonth.Value = ddlMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpYear As New SqlParameter("@Year", SqlDbType.VarChar, 15)
            sqlpYear.Value = ddlYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)

            Dim sqlpuser As New SqlParameter("@user", SqlDbType.VarChar, 100)
            sqlpuser.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpuser)


            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then

                Return 1000

            Else

                Return iReturnvalue
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub
    Private Function SaveSifFiletoDB(ByVal bytearray() As Byte, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            'Dim bytes() As Byte
            'bytes = System.Text.Encoding.ASCII.GetBytes(BitArray)

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("InsertWPSSifFile", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 15)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpSifData As New SqlParameter("@SifContent", SqlDbType.VarBinary)
            sqlpSifData.Value = bytearray ' bytearray
            cmd.Parameters.Add(sqlpSifData)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then

                Return 1000

            Else

                Return iReturnvalue
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
