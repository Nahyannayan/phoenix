Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empLeaveApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version            date            author              change
    '1.1                initial release Swapna              
    '1.2                15-mar-2012     swapna/shakeel      leave editing changes
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("MyLeaveType") Is Nothing Then
            Session("MyLeaveType") = "AL"
        End If
        If Page.IsPostBack = False Then
            Session("MyLeaveType") = "AL"
            'ViewState("IsAmendedFilterON") = "no"
            ViewState("IdDocEnabled") = "0"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            tblApprovalDetails.Visible = False
            Page.Title = OASISConstants.Gemstitle
            ' hlLeavePlans.Attributes.Add("onclick", "javascript:shwPopUp()")
            txtFrom.Text = Today.Date.ToString("dd/MMM/yyyy")
            'txtTo.Text = Today.AddMonths(12).Date.ToString("dd/MMM/yyyy")
            txtTo.Text = Today.AddMonths(1).Date.ToString("dd/MMM/yyyy")
            GRIDBIND()
            gvLeaveApproval.Attributes.Add("bordercolor", "#1b80b6")

            If ViewState("MainMnu_code") = "P170007" Then
                'trChangeStatus.Visible = False
                'btnSave.Text = "Approve"
                tblDPTLeave.Visible = False
                If Not rblOptions.Items.FindByValue("H") Is Nothing Then
                    rblOptions.Items.Remove(rblOptions.Items.FindByValue("H"))
                End If
                If Not rblOptions.Items.FindByValue("M") Is Nothing Then
                    rblOptions.Items.Remove(rblOptions.Items.FindByValue("M"))
                End If
                lblTitle.Text = "Leave Approval (HR)"
                gvLeaveApproval.Columns(11).HeaderText = "Modified Date"
            Else
                lblTitle.Text = "Leave Approval"
                gvLeaveApproval.Columns(11).HeaderText = "Submit Date"
            End If


            ''nahyan to hide comapre all checkbox 27102018

            Dim isAccessTocomapreAll = IsAccesstoCompareAllStaff()

            If (isAccessTocomapreAll) Then
                chkAll.Visible = True
            Else
                chkAll.Visible = False

            End If

            ''ends here nahyan 27102108
            'SetPopUp()
        End If
        'If hfEmpId.Value <> "" Then
        '    GetEmpLeaveCounts(hfEmpId.Value, hfElt_ID.Value)

        'End If
    End Sub
    Sub SetPopUp()
        For Each grow As GridViewRow In gvLeaveApproval.Rows
            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(grow.FindControl("PopupControlExtender2"), AjaxControlToolkit.PopupControlExtender)
            Dim imgViewPopupExtender As AjaxControlToolkit.PopupControlExtender = TryCast(grow.FindControl("imgViewPopupExtender"), AjaxControlToolkit.PopupControlExtender)

            ' Set the BehaviorID 
            Dim behaviorID As String = String.Concat("pce", grow.RowIndex)
            pce.BehaviorID = behaviorID

            imgViewPopupExtender.BehaviorID = behaviorID

            ' Programmatically reference the Image control 
            Dim i As LinkButton = DirectCast(grow.FindControl("lnkEmpLeaves"), LinkButton)
            Dim imgView As ImageButton = DirectCast(grow.FindControl("imgView"), ImageButton)

            ' Add the clie nt-side attributes (onmouseover & onmouseout) 
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();return false;", behaviorID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();return false;", behaviorID)
            imgView.Attributes.Add("onclick", OnMouseOverScript)
            imgView.Attributes.Add("onmouseout", OnMouseOutScript)
            i.Attributes.Add("onclick", OnMouseOverScript)
            i.Attributes.Add("onmouseout", OnMouseOutScript)
        Next
    End Sub
    Sub GRIDBIND()
        Try
            'Dim IsAmended As Boolean = False
            If rblOptions.SelectedValue = "M" Then
                lblTitle.Text = "Leave Approval - Amendments"
            Else
                lblTitle.Text = "Leave Approval"
            End If
            tblApprovalDetails.Visible = False
            Dim str_Sql As String
            Dim ds As New DataSet
            If ViewState("MainMnu_code") <> "P170007" Then
                'str_Sql = "exec GetDataForApproval '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','M',''"
                'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
                'If ds.Tables(0).Rows.Count > 0 Then
                '    'imgAmended.Visible = True
                '    lblViewAmend.Visible = True
                'Else
                '    'imgAmended.Visible = False
                '    lblViewAmend.Visible = False
                'End If
                gvLeaveApproval.Columns(11).HeaderText = IIf(rblOptions.SelectedValue = "M", "Modified Date", IIf(rblOptions.SelectedValue = "All", "Submit/Modified Date", "Submit Date"))
                'If IsAmended Then
                '    gvLeaveApproval.Columns(11).HeaderText = "Modified Date"
                'Else
                '    gvLeaveApproval.Columns(11).HeaderText = "Submit Date"
                'End If
            Else
                'imgAmended.Visible = False
            End If

            If ViewState("MainMnu_code") = "P170007" Then
                str_Sql = "exec GetDataForApprovalHR 'leave','" & Session("sbsuid") & "','" & rblOptions.SelectedValue & "','" & txtEmpSearch.Text & "'"
            Else
                str_Sql = "exec GetDataForApproval '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','" & rblOptions.SelectedValue & "','" & txtEmpSearch.Text & "'"
            End If
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)

            gvLeaveApproval.DataSource = ds.Tables(0)
            gvLeaveApproval.DataBind()
            If rblOptions.SelectedItem.Value = "Open" Or rblOptions.SelectedValue = "M" Then
                gvLeaveApproval.Columns(12).Visible = True
            Else
                gvLeaveApproval.Columns(12).Visible = False
            End If
            Dim lblCanEdit As Label
            Dim lnkApprove As LinkButton
            Dim btnApproveHold As Button
            Dim imgDoc As ImageButton
            For Each grow As GridViewRow In gvLeaveApproval.Rows
                lblCanEdit = DirectCast(grow.FindControl("lblCanEdit"), Label)
                imgDoc = DirectCast(grow.FindControl("imgDoc"), ImageButton)
                Dim btnImgDoc As HyperLink = DirectCast(grow.FindControl("btnImgDoc"), HyperLink)

                btnImgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(imgDoc.AlternateText)

                If lblCanEdit.Text = False Then
                    lnkApprove = DirectCast(grow.FindControl("lbApprove"), LinkButton)
                    btnApproveHold = DirectCast(grow.FindControl("btnApproveHold"), Button)
                    lnkApprove.Enabled = False
                    btnApproveHold.Enabled = False
                End If
                If imgDoc.AlternateText <> "" And imgDoc.AlternateText <> 0 Then
                    imgDoc.Visible = False
                    btnImgDoc.Visible = True
                Else
                    imgDoc.Visible = False
                    btnImgDoc.Visible = False
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function get_Isholiday(ByVal dtDate As Date) As Boolean
        Dim isHoliday As Boolean = False
        ' SUNDAY, SATURDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
        Select Case dtDate.DayOfWeek
            Case DayOfWeek.Sunday
                If ViewState("strHoliday1") = "SUNDAY" Or ViewState("strHoliday2") = "SUNDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Monday
                If ViewState("strHoliday1") = "MONDAY" Or ViewState("strHoliday2") = "MONDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Tuesday
                If ViewState("strHoliday1") = "TUESDAY" Or ViewState("strHoliday2") = "TUESDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Wednesday
                If ViewState("strHoliday1") = "WEDNESDAY" Or ViewState("strHoliday2") = "WEDNESDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Thursday
                If ViewState("strHoliday1") = "THURSDAY" Or ViewState("strHoliday2") = "THURSDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Friday
                If ViewState("strHoliday1") = "FRIDAY" Or ViewState("strHoliday2") = "FRIDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Saturday
                If ViewState("strHoliday1") = "SATURDAY" Or ViewState("strHoliday2") = "SATURDAY" Then
                    isHoliday = True
                End If
        End Select
        Return isHoliday
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        SAVE_LEAVE_APPROVAL("A")
    End Sub

    Private Sub SAVE_LEAVE_APPROVAL(ByVal ApprovalMode As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim lblEMP_ID As Label
            Dim lblAPS_ID, lblActualELA_ID As Label
            lblEMP_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblEMP_ID")
            lblAPS_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblAPS_ID")
            lblActualELA_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblActualELA_ID")
            'GetEmpLeaveCounts(hfEmpId.Value, hfElt_ID.Value)
            GetEmpLeaveCounts(lblActualELA_ID.Text, hfElt_ID.Value, ApprovalMode)
            Dim isHRApproval As Boolean
            Dim ApprStatus As String = ApprovalMode
            If ViewState("MainMnu_code") = "P170007" Then
                isHRApproval = True
                ApprStatus = "A"
            Else
                isHRApproval = False
            End If
            Dim bitDocCheck As Boolean = 0
            If chkDocverify.Visible = True Then
                bitDocCheck = chkDocverify.Checked
            End If
            retval = SaveLEAVEAPPROVAL(Session("sBsuId"), lblEMP_ID.Text, txtRemarks.Text.Trim, lblAPS_ID.Text, ApprStatus, Session("sUsr_name"), isHRApproval, stTrans, bitDocCheck)
            If retval = "0" Then
                stTrans.Commit()
                'stTrans.Rollback()
                'lblError.Text = getErrorMessage("0")
                'usrMessageBar2.ShowMessage(getErrorMessage("0"), False)
                If ApprStatus = "A" Then
                    usrMessageBar2.ShowMessage("Selected request has been Approved", False)
                ElseIf ApprStatus = "R" Then
                    usrMessageBar2.ShowMessage("Selected request has been Rejected", False)
                ElseIf ApprStatus = "H" Then
                    usrMessageBar2.ShowMessage("Selected request has been set to Hold", False)
                End If
                btnApprove.Visible = False
                btnReject.Visible = False
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, lblEMP_ID.Text & "-" & txtRemarks.Text.Trim & "-" & lblAPS_ID.Text & "-" & _
                ApprovalMode, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                gvLeaveApproval.SelectedIndex = -1
                'GRIDBIND()
                'tblApprovalDetails.Visible = False
                'pnlLeave.Visible = False
                'lblError.Text = "Selected request has been Approved / Rejected."
                hfElt_ID.Value = ""
                hfEmpId.Value = ""
                lblErrMsg.Text = ""
                'ddLeaveStatus.SelectedValue = "A"
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar2.ShowMessage(getErrorMessage("1000"))
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub SendEmailToNextApprover()
        Dim Mailstatus As String = ""
        Try
            Dim lblAPS_ID, lblELA_ID, lblActualELA_ID As Label
            lblAPS_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblAPS_ID")
            lblELA_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblELA_ID")
            lblActualELA_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblActualELA_ID")
            Dim strStatus As String = "A"
            'strStatus = ddLeaveStatus.SelectedValue.ToString

            Dim strStatusDesc = "Approved"
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection

            Dim ds As New DataSet()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = Session("sBsuid").ToString

            pParms(1) = New SqlClient.SqlParameter("@APS_ID", SqlDbType.Int)
            pParms(1).Value = lblAPS_ID.Text

            If strStatus = "R" Then
                pParms(2) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.Int)
                pParms(2).Value = lblELA_ID.Text
            Else
                pParms(2) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.Int)
                pParms(2).Value = lblELA_ID.Text
            End If

            pParms(2) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.Int)
            pParms(2).Value = lblELA_ID.Text

            pParms(3) = New SqlClient.SqlParameter("@status", SqlDbType.VarChar, 2)
            pParms(3).Value = strStatus

            pParms(4) = New SqlClient.SqlParameter("@bFromLeave", SqlDbType.Bit)
            pParms(4).Value = True

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getLeaveApproverMailDetails", pParms)
            'lblError.Text = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            Dim ToEmailId As String = ""
            Dim MainText As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                'Approved----- send mail to next approver
                If (strStatus = "A" And ds.Tables(0).Rows(0).Item("IsFinalApproval") = False) Then
                    ToEmailId = ds.Tables(0).Rows(0).Item("ApproverMailID").ToString
                    MainText = "An application for leave submitted by " & ds.Tables(0).Rows(0).Item("EMPName").ToString() & _
                    " (" & ds.Tables(0).Rows(0).Item("DPT_DESCR").ToString() & ") is pending for approval at your desk "

                    'Final Approval----- send mail to employee
                ElseIf (strStatus = "A" And ds.Tables(0).Rows(0).Item("IsFinalApproval") = True) Then
                    ToEmailId = ds.Tables(0).Rows(0).Item("EmpMailID").ToString
                    MainText = "Please be informed that the " & ds.Tables(0).Rows(0).Item("ELT_DESCR").ToString() & "  application  submitted " & _
              "by " & ds.Tables(0).Rows(0).Item("EMPName").ToString() & " from " & Format(ds.Tables(0).Rows(0).Item("ELA_DTFROM"), "dd/MMM/yyyy") & " has been APPROVED by " & ds.Tables(0).Rows(0).Item("ApproverName").ToString() & "."

                    ''Rejected----- send mail to employee
                ElseIf strStatus = "R" Then
                    MainText = "Please be informed that the " & ds.Tables(0).Rows(0).Item("ELT_DESCR").ToString() & "  application you have submitted " & _
               "from " & Format(ds.Tables(0).Rows(0).Item("ELA_DTFROM"), "dd/MMM/yyyy") & " is <b> REJECTED </b> by " & ds.Tables(0).Rows(0).Item("ApproverName").ToString() & "."
                    ToEmailId = ds.Tables(0).Rows(0).Item("EmpMailID").ToString
                End If
            End If
            If ToEmailId = "" Then
                ToEmailId = "swapna.tv@gemseducation.com"
            End If
            Dim fromemailid = "system@gemseducation.com"
            Dim sb As New StringBuilder
            Dim sb2 As New StringBuilder
            Dim sb3 As New StringBuilder

            Dim Subject As String = ""
            If ds.Tables(0).Rows.Count > 0 Then

                Subject = "ONLINE LEAVE APPLICATION"
                sb2.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                sb3.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")

                sb.Append("<tr><td ><br /><br /></td></tr>")
                sb.Append("<tr><td><b>SUB:</b> ONLINE LEAVE APPLICATION APPROVAL</td></tr>")
                sb.Append("<tr><td>" & MainText & "</td></tr>")
                If (strStatus = "A" And ds.Tables(0).Rows(0).Item("IsFinalApproval") = False) Then
                    Dim LoginURL As String
                    LoginURL = WebConfigurationManager.AppSettings.Item("webvirtualURL") & "/login.aspx?ss=1"
                    sb.Append("<tr><td>To view and approve the details, please click on the following link: <a href='" & LoginURL & "'><br />Employee Self Service</a></td></tr>")
                ElseIf strStatus = "R" Then
                    'sb.Append("<tr><td><b>REMARKS:</b> " & ds.Tables(0).Rows(0).Item("UsrRemark").ToString() & ")</td></tr>")
                    sb.Append("<tr><td><b>REMARKS:</b> " & txtRemarks.Text & ")</td></tr>")
                End If
                sb.Append("<tr><td ><br /><br />Thanks,</td></tr>")
                sb.Append("<tr><td><b>Team OASIS.</b> </td></tr>")
                sb.Append("<tr><td></td></tr>")
                sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
                sb.Append("<tr></tr><tr></tr>")
                Dim ds2 As New DataSet
                ds2 = GetCommunicationSettings()

                Dim username = ""
                Dim password = ""
                Dim port = ""
                Dim host = ""

                If ds2.Tables(0).Rows.Count > 0 Then
                    username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                    password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                    port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                    host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                End If

                If ds.Tables(1).Rows.Count > 0 Then
                    For Each drow As DataRow In ds.Tables(1).Rows
                        sb2.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & drow.Item("Empname").ToString() & " </td></tr>")
                        sb2.Append(sb)
                        ToEmailId = drow.Item("EmpMailID").ToString()
                        Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb2.ToString, username, password, host, port, 0, False)
                        sb2.Remove(0, sb2.Length)
                        sb2.Append(sb3)
                    Next
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'UtilityObj.Errorlog(Mailstatus)

        End Try
    End Sub
    Public Function GetCommunicationSettings() As DataSet
        Dim ds As DataSet
        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
        Return ds
    End Function
    Private Function SaveLEAVEAPPROVAL(ByVal p_BSU_ID As String, ByVal p_EMP_ID As String, _
     ByVal p_REMARKS As String, ByVal p_APS_ID As Integer, ByVal p_Status As String, _
     ByVal p_USER As String, ByVal isHRApproval As Boolean, ByVal p_stTrans As SqlTransaction, Optional ByVal p_DocCheck As Boolean = 0) As String
        '@return_value = [dbo].[LEAVEAPPROVAL]
        '@BSU_ID = N'XXXXXX',
        '@EMP_ID = N'1003', 
        '@REMARKS = N'VERUTHE 1-3 APP',
        '@APS_ID = 95,
        '@STATUS = N'H',
        '@USER = N'LEVEL 1-3'
        Try
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_EMP_ID
            pParms(2) = New SqlClient.SqlParameter("@REMARKS", SqlDbType.VarChar, 200)
            pParms(2).Value = p_REMARKS
            pParms(3) = New SqlClient.SqlParameter("@APS_ID", SqlDbType.Int)
            pParms(3).Value = p_APS_ID
            pParms(4) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar, 1)
            pParms(4).Value = p_Status
            pParms(5) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 20)
            pParms(5).Value = p_USER
            pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            pParms(7) = New SqlClient.SqlParameter("@IsHRApproval", SqlDbType.Bit)
            pParms(7).Value = isHRApproval
            pParms(8) = New SqlClient.SqlParameter("@IsDocVerified", SqlDbType.Bit)
            pParms(8).Value = p_DocCheck
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "LEAVEAPPROVAL", pParms)
            If pParms(6).Value = "0" Then
                SaveLEAVEAPPROVAL = pParms(6).Value
            Else
                SaveLEAVEAPPROVAL = "1000"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            SaveLEAVEAPPROVAL = "1000"
        End Try
    End Function

    Protected Sub gvLeaveApproval_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvLeaveApproval.RowEditing
        Try
            gvLeaveApproval.SelectedIndex = e.NewEditIndex
            'GRIDBIND()
            Dim strLeaveFrom, strLeaveTo As String
            Dim lblEMP_ID, lblID, lblELT_ID, lblDocChk, lblActualELA_ID As Label
            Dim btnApproveHold As Button = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("btnApproveHold")
            lblDocChk = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblDocChk") 'V1.2
            lblELT_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblELT_ID")
            lblActualELA_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblActualELA_ID")
            lblEMP_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblEMP_ID")
            lblID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblID")
            hfEmpId.Value = lblEMP_ID.Text
            hfElt_ID.Value = lblELT_ID.Text
            strLeaveFrom = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(6).Text
            strLeaveTo = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(7).Text
            txtFrom.Text = CDate(strLeaveFrom).ToString("dd/MMM/yyyy")
            ' txtTo.Text = CDate(strLeaveFrom).AddMonths(12).ToString("dd/MMM/yyyy")
            txtTo.Text = CDate(strLeaveTo).ToString("dd/MMM/yyyy")
            lblEmpName.Text = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(3).Text
            Dim FromDate As String = String.Empty, ToDate As String = String.Empty
            If strLeaveFrom <> "" AndAlso IsDate(strLeaveFrom) Then
                FromDate = Format(CDate(strLeaveFrom), "dd/MMM/yyyy")
            End If
            If strLeaveTo <> "" AndAlso IsDate(strLeaveTo) Then
                ToDate = Format(CDate(strLeaveTo), "dd/MMM/yyyy")
            End If
            lblLeavePeriod.Text = "From  " & FromDate & "  To  " & ToDate
            lblLeavedays.Text = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(8).Text
            lblLeaveType.Text = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(4).Text
            lblDetails.Text = TryCast(gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(10).FindControl("LinkButton1"), LinkButton).Text
            'hfElt_ID.Value = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(17).Text
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr As String
            lblPrevLeavePeriod.Text = ""
            trPrevLeavePeriod.Visible = False
            If lblID.Text.StartsWith("M") Then
                trPrevLeavePeriod.Visible = True
            End If
            If lblID.Text.StartsWith("M") Or ViewState("MainMnu_code") = "P170007" Then
                Dim myDT As New DataTable
                sqlStr = "GetOriginalLeavePlanString '" & lblID.Text & "'"
                myDT = Mainclass.getDataTable(sqlStr, str_conn)
                If myDT.Rows.Count > 0 Then
                    lblPrevLeavePeriod.Text = myDT.Rows(0).Item("LeavePlan")
                End If
            End If
            sqlStr = "SELECT EMD_PHOTO FROM EMPLOYEE_M WITH(NOLOCK) ,EMPLOYEE_D WITH(NOLOCK) WHERE EMP_ID=EMD_EMP_ID AND EMP_ID='" & hfEmpId.Value & "'"
            Dim EMPTable As New DataTable
            EMPTable = Mainclass.getDataTable(sqlStr, str_conn)
            If EMPTable.Rows.Count > 0 Then
                Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & EMPTable.Rows(0).Item("EMD_PHOTO").ToString
                imgProfile.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
            End If
            tblApprovalDetails.Visible = True
            pnlLeave.Visible = True
            btnApprove.Visible = True
            btnReject.Visible = True
            usrMessageBar2.ShowMessage("")
            'GetEmpLeaveCounts(hfEmpId.Value, hfElt_ID.Value)
            GetEmpLeaveCounts(lblActualELA_ID.Text, hfElt_ID.Value, "A")
            getDocumentEnabledFlag(hfElt_ID.Value, lblDocChk.Text)
            Session("MyLeaveType") = hfElt_ID.Value
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        gvLeaveApproval.SelectedIndex = -1
        GRIDBIND()
        tblApprovalDetails.Visible = False
        pnlLeave.Visible = False
        txtFrom.Text = Today.Date.ToString("dd/MMM/yyyy")
        'txtTo.Text = Today.Date.AddMonths(12).ToString("dd/MMM/yyyy")
        txtTo.Text = Today.Date.AddMonths(1).ToString("dd/MMM/yyyy")
    End Sub
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim b As New StringBuilder()
        Dim ds As DataSet = New DataSet()

        'IRmtHR.IEmpTraining(iEmpTrain = New RmtHR.EmpTraining())
        'DataTable(Table = iEmpTrain.SelectedEmployees(Convert.ToInt16(contextKey)))
        'Dim MyLeaveClass As Payroll_empLeaveApproval = New Payroll_empLeaveApproval

        'ds = MyLeaveClass.GetEmpLeaveCounts(Convert.ToInt16(contextKey))
        Try
            Dim rowId As String = ""
            Dim dtgrid As New DataTable
            'Dim lblELT_ID As Label
            'lblELT_ID = DirectCast(gvLeaveApproval.Rows(rowId).FindControl("lblELT_ID"), Label)
            ''If h_Emp_No.Value <> "" And txtFrom.Text <> "" Then
            'Dim MyClas As Payroll_empLeaveApproval = New Payroll_empLeaveApproval
            'For Each grow As GridViewRow In MyClas.gvLeaveApproval.Rows
            '    Dim lblAPSID As Label
            '    lblAPSID = grow.FindControl("lblAPS_ID")
            '    If lblAPSID.Text = APSID Then
            '        rowId = grow.RowIndex
            '    End If
            'Next
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = Convert.ToInt16(contextKey) 'for EmpID

            pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(1).Value = Today.Date()

            pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
            pParms(2).Value = "AL"

            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = HttpContext.Current.Session("sBsuid")


            pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
            pParms(4).Value = False
            pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
            pParms(5).Value = True

            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                For Each dt As DataTable In ds.Tables
                    If dt.Columns.Count > 2 Then
                        dtgrid = dt
                    End If
                Next
            End Using


            b.Append("<table style='background-color:#f3f3f3; border: #336699 3px solid; ")
            b.Append("width:350px; font-size:10pt; font-family:Verdana;' cellspacing='0' cellpadding='3'>")

            b.Append("<tr><td colspan='4' style='background-color:#336699; color:white;'>")
            b.Append("<b>Leave Status</b>")
            b.Append("</td></tr>")
            b.Append("<tr><td style='width:80px;' class='msg_header'>Leave Type</td>")
            b.Append("<td style='width:80px;' class='msg_header'>Eligible</td>")
            b.Append("<td style='width:80px;' class='msg_header'>Taken</td>")
            b.Append("<td style='width:80px;' class='msg_header'>Balance</td>")

            For tt As Integer = 0 To dtgrid.Rows.Count - 1
                'for ( tt= 0; tt < table.Rows.Count; tt++)

                'For Each dRow As DataRow In ds.Tables(0).Rows
                b.Append("<tr>")
                b.Append("<td class='matters'>" & dtgrid.Rows(tt)("Leave Type").ToString() & "</td>")
                b.Append("<td class='matters'>" & dtgrid.Rows(tt)("Eligible").ToString() & "</td>")
                b.Append("<td class='matters'>" & dtgrid.Rows(tt)("Taken").ToString() & "</td>")
                b.Append("<td class='matters'>" & dtgrid.Rows(tt)("Balance").ToString() & "</td>")
                b.Append("</tr>")
            Next

            b.Append("</table>")

            Return b.ToString()
        Catch
        End Try

    End Function


    Protected Sub lnkLeavePlans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeavePlans.Click
        Try

            Dim BSUID As String = Session("sbsuid").ToString
            Dim strlblAPS_ID As String

            Dim strLeaveFrom As String = ""
            Dim strLeaveTo As String = ""
            If (gvLeaveApproval.SelectedIndex > -1) Then
                Dim lblAPS_ID As Label
                strLeaveFrom = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(6).Text
                strLeaveTo = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(7).Text
                lblAPS_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblAPS_ID")
                strlblAPS_ID = lblAPS_ID.Text
                If (CDate(txtFrom.Text) > CDate(strLeaveFrom)) Then
                    'lblError.Text = "Invalid period Start Date.Check employee's leave dates"
                    usrMessageBar.ShowMessage("Invalid period Start Date.Check employee's leave dates")
                    Exit Sub
                End If
                If (CDate(txtTo.Text) < CDate(strLeaveTo)) Then
                    'lblError.Text = "Invalid period End Date.Check employee's leave dates"
                    usrMessageBar.ShowMessage("Invalid period End Date.Check employee's leave dates")
                    Exit Sub
                End If
            Else
                strLeaveFrom = Today.Date().ToString("dd/MMM/yyyy")
                strLeaveTo = Today.Date().AddMonths(12).ToString("dd/MMM/yyyy")
                strlblAPS_ID = 0
                If (CDate(txtFrom.Text) > CDate(txtTo.Text)) Then
                    'lblError.Text = "Invalid dates."
                    usrMessageBar.ShowMessage("Invalid dates.")
                End If
            End If


            Dim params As New Hashtable
            params.Add("@BSU_ID", BSUID)
            params.Add("@startDate", CDate(txtFrom.Text))
            params.Add("@endDate", CDate(txtTo.Text))
            params.Add("@APS_ID", 0)
            params.Add("@SubAPS_ID", strlblAPS_ID)
            params.Add("@username", HttpContext.Current.Session("sUsr_name").ToString)
            If (chkAll.Checked) Then
                params.Add("@ShowAll", 1)
            Else
                params.Add("@ShowAll", 0)
            End If

            params("reportHeading") = "Leave Schedule for the period " + CDate(txtFrom.Text).ToString("dd/MMM/yyyy") + " To " + CDate(txtTo.Text).ToString("dd/MMM/yyyy")
            params("@SubHeading") = "Leave planned by Employee from " + CDate(strLeaveFrom).ToString("dd/MMM/yyyy") + " To " + CDate(strLeaveTo).ToString("dd/MMM/yyyy")

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../Payroll/Reports/Rpt/rptEmpLeavePlan.rpt")

                'End If
            End With
            Session("rptClass") = rptClass

            'Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "LeavePlan", "<script language=javascript>window.open('empLeavePlan.aspx','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=785,height=400,left = 350,top = 200');</script>")
            'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "LeavePlan", "window.open('empLeavePlan.aspx','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=785,height=400,left = 350,top = 200')", True)
            'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "LeavePlan", "window.showModalDialog('empLeavePlan.aspx','popup','dialogWidth:785px;dialogHeight:400px;status=0;resizable=1;maximize:yes;minimize:yes;')", True)
            '' ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "NewLeavePlan", "window.showModelessDialog('empLeavePlan.aspx','popupNew','dialogWidth:785px;dialogHeight:400px;status=0;resizable=1;')", True)

            'Dim url As String = "empLeavePlan.aspx"
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "LeavePlan", "Popup('" + url + "');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "Popup ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
            GET_LEAVE_PLAN()
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Payroll/empLeaveApplicationListCompare.aspx');", True)
            '   ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowMessage(ex.Message)
        End Try
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Master.DisableScriptManager()
    'End Sub
    Private Sub IsPlannerEnabled()
        Try
            Dim lblELT_ID As Label
            Dim lblECT_ID As Label
            lblELT_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblELT_ID")
            lblECT_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblECT_ID")
            Dim strDate = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(6).Text

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim lblELT_ID As Label
            'lblELT_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblEMP_ID")

            Dim pParms(5) As SqlClient.SqlParameter 'V1.1
            pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")

            pParms(1) = New SqlClient.SqlParameter("@ELTID", SqlDbType.VarChar, 10)
            pParms(1).Value = lblELT_ID.Text ' ddMonthstatusPeriodically.SelectedValue

            pParms(2) = New SqlClient.SqlParameter("@ECTID", SqlDbType.Int)
            pParms(2).Value = lblECT_ID.Text

            pParms(3) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
            pParms(3).Value = Convert.ToDateTime(strDate)

            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "CheckPlannerEnabled", pParms)

            Dim count = pParms(4).Value
            If count > 0 Then
                ViewState("IsPlannerEnabled") = 1
            Else
                ViewState("IsPlannerEnabled") = 0
            End If
        Catch ex As Exception
            'lblError.Text = "Error in fetching leaves."
            usrMessageBar.ShowMessage("Error in fetching leave data.")
        End Try
    End Sub

    Protected Sub lnkPlanner_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            IsPlannerEnabled()
            If CDate(txtTo.Text) < CDate(txtFrom.Text) Then
                'lblError.Text = "Invalid Dates"
                usrMessageBar2.ShowMessage("Invalid Dates")
                Exit Sub
            End If
            Dim lblEMP_ID, lblELT_ID As Label
            Dim strFromDate = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(6).Text
            Dim strToDate = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).Cells(7).Text
            lblEMP_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblEMP_ID")
            lblELT_ID = gvLeaveApproval.Rows(gvLeaveApproval.SelectedIndex).FindControl("lblELT_ID")
            'Dim strFromDate = txtFrom.Text
            'Dim strToDate = txtTo.Text
            'Dim days As String = DateDiff(DateInterval.Day, CDate(strFromDate), CDate(strToDate)) + 1
            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(strFromDate) & "','" & CDate(strToDate) & "','" & Session("sBsuid") & "'," & lblEMP_ID.Text & ",'" & lblELT_ID.Text & "') as days"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
            ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
            Dim days As String = ds.Tables(0).Rows(0).Item("days")
            Dim url As String = "../Common/PopupShowData.aspx?id=LEAVEPLANNER&StartDate=" + CDate(strFromDate) + "&Days=" + days + "&BSUID=" + Session("sBsuid") + "&IsPlannerEnabled=" + ViewState("IsPlannerEnabled").ToString
            ' Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "LeavePlan", "<script language=javascript>window.open('" + url + "','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=785,height=420,left = 350,top = 200');</script>")
            'using ajax and window.focus on pop-up page
            'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "LeavePlanner", "window.open('" + url + "','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=785,height=420,left = 350,top = 200')", True)
            'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "LeavePlan", "window.showModalDialog('" + url + "','popup','dialogWidth:785px;dialogHeight:420px;status=0;resizable=1;')", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "LeavePlanner", "Popup('" + url + "');", True)
        Catch ex As Exception
            'lblError.Text = "Error Occured"
            usrMessageBar2.ShowMessage("Error Occured")
        End Try
    End Sub
    Public Sub GetEmpLeaveCounts(ByVal EmPID As Integer, ByVal Leavetype As String, ByVal ApprovalMode As String)
        Try
            Dim rowId As String = ""

            'Dim lblELT_ID As Label
            'lblELT_ID = DirectCast(gvLeaveApproval.Rows(rowId).FindControl("lblELT_ID"), Label)
            ''If h_Emp_No.Value <> "" And txtFrom.Text <> "" Then
            'Dim MyClas As Payroll_empLeaveApproval = New Payroll_empLeaveApproval
            'For Each grow As GridViewRow In MyClas.gvLeaveApproval.Rows
            '    Dim lblAPSID As Label
            '    lblAPSID = grow.FindControl("lblAPS_ID")
            '    If lblAPSID.Text = APSID Then
            '        rowId = grow.RowIndex
            '    End If
            'Next
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = EmPID

            pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(1).Value = Today.Date()

            pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
            pParms(2).Value = Leavetype '"AL" 'lblELT_ID.Text

            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = Session("sBsuid")

            pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
            pParms(4).Value = False

            pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
            pParms(5).Value = True

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)

            End Using

            Dim intLeaveBalnce, days As Integer
            If ds.Tables(0).Rows.Count > 0 Then
                intLeaveBalnce = ds.Tables(0).Rows(0).Item(3)
            End If
            '---------- check leave balance---------------------
            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(txtFrom.Text) & "','" & CDate(txtTo.Text) & "','" & Session("sBsuid") & "'," & hfEmpId.Value & ",'" & hfElt_ID.Value & "') as days "
            Dim ds2 As New DataSet
            ds2 = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
            ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
            days = ds2.Tables(0).Rows(0).Item("days")
            If ApprovalMode = "A" Then
                If (days > intLeaveBalnce) Then
                    'chkSaveAnyway.Visible = True
                    'lblError.Text = "Note: You have exceeded your leave balance for current year.Check ""Continue Anyway"" to save details or Cancel to re-apply"
                    trMsg.Visible = True
                    lblErrMsg.Text = getErrorMessage("1123")
                End If

            Else
                trMsg.Visible = False
                lblErrMsg.Text = ""
                'chkSaveAnyway.Checked = False
                'chkSaveAnyway.Visible = False
            End If

            'Else
            'lblError.Text = "Please enter required details."
            'End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowMessage(ex.Message)
        End Try
    End Sub
    Protected Sub imgDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim imgdoc As ImageButton = sender.parent.findcontrol("imgdoc")

        ' ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Document", "window.showModelessDialog ('" + url + "','popup','dialogWidth:785px;dialogHeight:420px;status=0;resizable=1')", True)
        ' OpenFileFromDB(imgdoc.AlternateText)
    End Sub
    Private Sub OpenFileFromDB(ByVal DocId As String)
        Try


            Dim conn As String = ""

            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("OpenDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)

            Dim myReader As SqlDataReader = cmd.ExecuteReader()

            If myReader.Read Then
                Response.ClearHeaders()
                Response.ContentType = myReader("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(myReader("DOC_DOCUMENT"), Byte())
                'Response.Buffer = True
                'Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                'Response.AddHeader("Cache-control", "no-cache")
                Response.AddHeader("content-disposition", "attachment;filename=" & myReader("DOC_DOCNO").ToString())
                Response.Clear()
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.Close()
                Response.End()

            End If
            myReader.Close()

        Catch ex As Exception
            'lblError.Text = ex.Message
            'lblError.Text = UtilityObj.getErrorMessage("1000")
            usrMessageBar.ShowMessage(getErrorMessage("1000"))
        End Try

    End Sub

    Protected Sub getDocumentEnabledFlag(ByVal strLeaveType As String, ByVal chkStatus As Boolean)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select Isnull(EAS_BDocCheck,0) EAS_BDocCheck from dbo.EMPATTENDANCE_STATUS where eas_elt_id= '" & strLeaveType & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows(0).Item("EAS_BDocCheck") = True Then
            ViewState("IdDocEnabled") = "1"
            chkDocverify.Visible = True

            If chkStatus = True Then
                chkDocverify.Checked = True
                chkDocverify.Enabled = False
            Else
                chkDocverify.Checked = False
                chkDocverify.Enabled = True
            End If
        Else
            ViewState("IdDocEnabled") = "0"
            chkDocverify.Visible = False
        End If



    End Sub
    'Protected Sub gvLeaveApproval_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLeaveApproval.RowCreated
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        ' Programmatically reference the PopupControlExtender 
    '        'GetEmpLeaveCounts(e.Row.RowIndex)
    '        Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender2"), AjaxControlToolkit.PopupControlExtender)

    '        ' Set the BehaviorID 
    '        Dim behaviorID As String = String.Concat("pce", e.Row.RowIndex)
    '        pce.BehaviorID = behaviorID


    '        ' Programmatically reference the Image control 
    '        Dim i As LinkButton = DirectCast(e.Row.FindControl("lnkEmpLeaves"), LinkButton)

    '        ' Add the clie nt-side attributes (onmouseover & onmouseout) 
    '        Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
    '        Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)

    '        i.Attributes.Add("onclick", OnMouseOverScript)
    '        i.Attributes.Add("onmouseout", OnMouseOutScript)
    '    End If
    'End Sub


    'Protected Sub gvLeaveApproval_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLeaveApproval.RowCommand
    '    Dim grow As GridViewRow
    '    Dim lnkEdit As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '    grow = DirectCast(lnkEdit.Parent.Parent, GridViewRow)
    '    Select Case e.CommandName
    '        Case "Clicked"
    '            gvLeaveApproval.EditIndex = grow.RowIndex
    '            ViewState("RowID") = grow.RowIndex
    '            'Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(grow.FindControl("PopupControlExtender2"), AjaxControlToolkit.PopupControlExtender)
    '            'GetEmpLeaveCounts(grow.RowIndex)

    '    End Select

    'End Sub

    'Protected Sub gvLeaveApproval_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLeaveApproval.RowDataBound
    '    For Each gvRow As GridViewRow In gvLeaveApproval.Rows
    '        If gvRow.RowType = DataControlRowType.DataRow Then
    '            ' Programmatically reference the PopupControlExtender 
    '            'GetEmpLeaveCounts(e.Row.RowIndex)
    '            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(gvRow.FindControl("PopupControlExtender2"), AjaxControlToolkit.PopupControlExtender)

    '            ' Set the BehaviorID 
    '            Dim behaviorID As String = String.Concat("pce", gvRow.RowIndex)
    '            pce.BehaviorID = behaviorID


    '            ' Programmatically reference the Image control 
    '            Dim i As LinkButton = DirectCast(gvRow.FindControl("lnkEmpLeaves"), LinkButton)

    '            ' Add the clie nt-side attributes (onmouseover & onmouseout) 
    '            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
    '            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)

    '            i.Attributes.Add("onclick", OnMouseOverScript)
    '            i.Attributes.Add("onmouseout", OnMouseOutScript)
    '            GetEmpLeaveCounts(gvRow.RowIndex)
    '        End If
    '    Next
    'End Sub


    Protected Sub rblOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblOptions.SelectedIndexChanged
        'ViewState("IsAmendedFilterON") = "no"
        If (rblOptions.SelectedValue = "A") Then
            tblApprovalDetails.Visible = False
            'ElseIf (rblOptions.SelectedValue = "M") Then
            '    ViewState("IsAmendedFilterON") = "yes"
        End If
        GRIDBIND()
    End Sub

    Protected Sub gvLeaveApproval_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLeaveApproval.PageIndexChanging
        gvLeaveApproval.PageIndex = e.NewPageIndex
        GRIDBIND()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Dim smScriptManager As New ScriptManager
        smScriptManager = Page.Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True

    End Sub
    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        pnlLeave.Visible = False
        If btnApprove.Visible = False Or btnReject.Visible = False Then ' if approved or rejected, reload the main page after pop window closed, added by Jacob on 19/DEC/2018.
            GRIDBIND()
        End If
    End Sub
    Protected Sub imgAmended_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'ViewState("IsAmendedFilterON") = "yes"
        txtEmpSearch.Text = ""
        rblOptions.SelectedValue = Nothing
        GRIDBIND()
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        GRIDBIND()
    End Sub

    Protected Sub lblViewAmend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'ViewState("IsAmendedFilterON") = "yes"
        txtEmpSearch.Text = ""
        rblOptions.SelectedValue = Nothing
        GRIDBIND()
    End Sub

    Protected Sub lnkbackbtn_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("../ESSDashboard.aspx")
        Response.Redirect(URL)
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub lnkCompareRpt_Click(sender As Object, e As EventArgs)
        Dim lnkCompare As LinkButton = CType(sender, LinkButton)
        Dim gridrow As GridViewRow = CType(lnkCompare.NamingContainer, GridViewRow)

        Dim strLeaveFrom As String = ""
        Dim strLeaveTo As String = ""
        Dim lblAPS_ID As Label
        strLeaveFrom = gridrow.Cells(6).Text
        strLeaveTo = gridrow.Cells(7).Text
        Dim str_Sql As String
        Dim USR_NAME As String
        Dim ds As New DataSet
        Dim str_conn = ConnectionManger.GetOASISConnectionString

        If Request.QueryString("id") <> "" Then
            str_Sql = "select USR_NAME from USERS_M where USR_EMP_ID=" + Request.QueryString("id")
            USR_NAME = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        Else
            USR_NAME = Session("sUsr_name")
        End If
        Try
            Dim PARAM(6) As SqlParameter
            Dim EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(USR_NAME)
            Dim BSUID As String = Session("sbsuid").ToString
            PARAM(0) = New SqlParameter("@BSU_ID", BSUID)
            PARAM(1) = New SqlParameter("@startDate", CDate(strLeaveFrom))
            PARAM(2) = New SqlParameter("@endDate", CDate(strLeaveTo))
            PARAM(3) = New SqlParameter("@APS_ID", 0)
            PARAM(4) = New SqlParameter("@username", HttpContext.Current.Session("sUsr_name").ToString)
            PARAM(5) = New SqlParameter("@ShowAll", 0)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.[GetLeavePlanNew]", PARAM)

            If dsDetails.Tables(0).Rows.Count > 0 Then
                Session("Plan_Chart") = dsDetails.Tables(0)
            Else
                Session("Plan_Chart") = Nothing
            End If

            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Payroll/empLeaveApplicationListCompare.aspx');", True)

        Catch ex As Exception

        End Try

    End Sub

    Sub GET_LEAVE_PLAN()
        Dim str_Sql As String
        Dim USR_NAME As String
        Dim ds As New DataSet
        Dim str_conn = ConnectionManger.GetOASISConnectionString

        If Request.QueryString("id") <> "" Then
            str_Sql = "select USR_NAME from USERS_M where USR_EMP_ID=" + Request.QueryString("id")
            USR_NAME = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        Else
            USR_NAME = Session("sUsr_name")
        End If
        Try
            Dim PARAM(7) As SqlParameter
            Dim EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(USR_NAME)
            Dim BSUID As String = Session("sbsuid").ToString
            PARAM(0) = New SqlParameter("@BSU_ID", BSUID)
            PARAM(1) = New SqlParameter("@startDate", CDate(txtFrom.Text))
            PARAM(2) = New SqlParameter("@endDate", CDate(txtTo.Text))
            PARAM(3) = New SqlParameter("@APS_ID", 0)
            PARAM(4) = New SqlParameter("@username", HttpContext.Current.Session("sUsr_name").ToString)
            If (chkAll.Checked) Then
                PARAM(5) = New SqlParameter("@ShowAll", 1)
            Else
                PARAM(5) = New SqlParameter("@ShowAll", 0)
            End If



            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.[GetLeavePlanNew]", PARAM)

            If dsDetails.Tables(0).Rows.Count > 0 Then
                Session("Plan_Chart") = dsDetails.Tables(0)
            Else
                Session("Plan_Chart") = Nothing
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Function IsAccesstoCompareAllStaff() As Boolean
        Dim IsPhoenIsAccessToCompareAll As Boolean = False

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        Try
            param(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar)
            param(0).Value = HttpContext.Current.Session("sUsr_name").ToString
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.IsAccesstoLeaveCompare", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim Dt As DataTable = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    IsPhoenIsAccessToCompareAll = True
                End If

            End If

            Return IsPhoenIsAccessToCompareAll
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        SAVE_LEAVE_APPROVAL("R")
    End Sub
End Class
