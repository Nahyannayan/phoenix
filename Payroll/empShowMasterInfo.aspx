﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empShowMasterInfo.aspx.vb" Inherits="Payroll_empShowMasterInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>       
    <base target="_self" />
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
     <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script> 
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function test(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
             }
             function test1(val) {
                 var path;
                 if (val == 'LI') {
                     path = '../Images/operations/like.gif';
                 } else if (val == 'NLI') {
                     path = '../Images/operations/notlike.gif';
                 } else if (val == 'SW') {
                     path = '../Images/operations/startswith.gif';
                 } else if (val == 'NSW') {
                     path = '../Images/operations/notstartwith.gif';
                 } else if (val == 'EW') {
                     path = '../Images/operations/endswith.gif';
                 } else if (val == 'NEW') {
                     path = '../Images/operations/notendswith.gif';
                 }
                 document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
          }
    </script>
</head>
<body onload="listen_window();" topmargin=0 bottommargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server">   
        <!--1st drop down menu -->
<div id="dropmenu" class="dropmenudiv" style="left: 153px; width: 110px; top: 1px;display:none;">
    <a href="javascript:test('LI');"> <img alt="Any where" class="img_left" src="../Images/operations/like.gif" style="text-decoration: underline" />
    Any Where</a> 
    <a href="javascript:test('NLI');"> <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" style="text-decoration: underline" />
    Not In</a> 
    <a href="javascript:test('SW');"> <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" style="text-decoration: underline" />
    Starts With</a> 
    <a href="javascript:test('NSW');"> <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" style="text-decoration: underline" />
    Not Start With</a> 
    <a href="javascript:test('EW');"> <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" style="text-decoration: underline" />
    Ends With</a> 
    <a href="javascript:test('NEW');">
    <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" style="text-decoration: underline" />
    Not Ends With</a>
</div>
<!--2nd drop down menu -->
<div id="dropmenu1" class="dropmenudiv" style="left: 0px; width: 110px; top: -1px;display:none;" >
    <a href="javascript:test1('LI');">
    <img alt="Like" class="img_left" src="../Images/operations/like.gif" style="text-decoration: underline" />
    Any Where</a> <a href="javascript:test1('NLI');">
    <img alt="Like" class="img_left" src="../Images/operations/notlike.gif" style="text-decoration: underline" />
    Not In</a> <a href="javascript:test1('SW');">
    <img alt="Like" class="img_left" src="../Images/operations/startswith.gif" style="text-decoration: underline" />
    Starts With</a> <a href="javascript:test1('NSW');">
    <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" style="text-decoration: underline" />
    Not Start With</a> <a href="javascript:test1('EW');">
    <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" style="text-decoration: underline" />
    Ends With</a> <a href="javascript:test1('NEW');">
    <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" style="text-decoration: underline" />
    Not Ends With</a>
</div>  
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td align="center" class="matters" valign="top">
    <asp:GridView ID="gvEmpInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ID" Width="100%" CssClass="table table-bordered table-row" PageSize="15">
    <Columns>
        <asp:TemplateField HeaderText=" ID" SortExpression="EMP_ID">
        <EditItemTemplate> 
        </EditItemTemplate>
        <ItemTemplate>
        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("ID") %>'></asp:LinkButton>&nbsp;
        </ItemTemplate>
        <HeaderTemplate>
       
            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text" EnableViewState="False"
            Text="ID"></asp:Label>
          
            <div id="Div1" class="chromestyle" style="display:none;">
            <ul>
            <li><a href="#" rel="dropmenu">
            <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
            style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
            </li>
            </ul>
            </div>
            <br />
            <asp:TextBox ID="txtcode" runat="server" Width="92px"></asp:TextBox>
            <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
            OnClick="btnSearchEmpId_Click" />
           
         </HeaderTemplate>
         <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
        <HeaderTemplate>
          
                <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Description"
                Width="214px"></asp:Label>
                <div id="Div2" class="chromestyle" style="display:none;">
                <ul>
                <li><a href="#" rel="dropmenu1">
                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
                </li>
                </ul>
                </div>
                
                <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox>
                <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                OnClick="btnSearchEmpName_Click" />&nbsp;</td>
               
        </HeaderTemplate>
        <ItemTemplate>
        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
        OnClick="LinkButton1_Click" Text='<%# Eval("E_Name") %>'></asp:LinkButton>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        </Columns> 
    </asp:GridView> 
    </td>
    </tr>
    <tr>
    <td>
    </td>
    </tr>
</table>
    
<script type="text/javascript">
    cssdropdown.startchrome("Div1");
    cssdropdown.startchrome("Div2");
</script> 
<input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
<input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
<input id="h_SelectedId" runat="server" type="hidden" value="0" />   
</form>
</body>
</html>
