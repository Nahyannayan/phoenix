﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empStaffListWithPhoto.aspx.vb" Inherits="Payroll_empStaffListWithPhoto" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>Employee Details</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>

                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblerror" runat="server"></asp:Label>
                        <asp:Label ID="lblHead" runat="server" Text="Employee Details" Font-Bold="True">Employee Details</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkExport" runat="server" Visible="False">Export to Excel</asp:LinkButton></td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvExport" runat="server" AutoGenerateColumns="False" CellPadding="1" CssClass="table table-bordered table-row">
                            <RowStyle />
                            <Columns>
                                <asp:TemplateField HeaderText="Sl No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNum" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                        <asp:Label ID="lblEmpDepID" runat="server" Text='<%# bind("Emp_ID") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="EmpNo" HeaderText="Employee Number" />
                                <asp:BoundField DataField="Empname" HeaderText="Employee Name" />
                                <asp:BoundField DataField="WORK_BSU_NAME" HeaderText="Working Unit"></asp:BoundField>
                                <asp:BoundField DataField="CATEGORY_DESC" HeaderText="Category" />
                                <asp:BoundField DataField="EMP_JOINDT" HeaderText="Date of Joining" />
                                <asp:BoundField DataField="EMP_DES_DESCR" HeaderText="Designation" />
                                <asp:BoundField DataField="EMP_DEPT_DESCR" HeaderText="Department" />
                                <asp:TemplateField HeaderText="Photo">
                                    <ItemTemplate>
                                        <asp:Image runat="server" ID="imgEMployee" AlternateText='<%# Bind("EMD_PHOTO") %>' Width="60" Height="60" />
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle />
                            <FooterStyle />
                            <PagerStyle HorizontalAlign="Left" />
                            <SelectedRowStyle Font-Bold="True" />
                            <HeaderStyle Font-Bold="True" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
