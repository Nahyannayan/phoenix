﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Imports GemBox.Spreadsheet

Partial Class EmpStaffListDownload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session.Timeout = 5
            HiddenPostBack.Value = 1
            Session("Data") = Nothing

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") = Nothing Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If

            BindBsu()

            'Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
            '    While DefReader.Read
            '        Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))


            '    End While
            'End Using

        End If
    End Sub

    'Public Sub BindBsu()
    '    Try
    '        Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()


    '        Dim Sql_Query As String = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
    '        Dim ds As DataSet
    '        ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
    '        ddbsu.DataSource = ds.Tables(0)
    '        ddbsu.DataTextField = "BSU_NAME"
    '        'ddbsu.DataValueField = "BSU_ID"
    '        ddbsu.DataValueField = "BSU_ShortName"
    '        ddbsu.DataBind()
    '        Dim list As New ListItem
    '        list.Text = "Select a Business Unit"
    '        list.Value = "-1"
    '        ddbsu.items.insert(0, list)

    '        ddbsu.SelectedValue = Session("sBsuid")

    '    Catch ex As Exception

    '    End Try

    'End Sub

    Public Sub BindBsu()
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.StoredProcedure, "[dbo].[GetUserBusinessUnits]", pParms)
        ddbsu.DataTextField = "Bsu_Name"
        ddbsu.DataValueField = "BSU_SHORTNAME"
        ddbsu.DataSource = ds.Tables(0)
        ddbsu.DataBind()
        Dim item As New ListItem("[Select]", "-1")
        ddbsu.Items.Insert(0, item)
        ddbsu.SelectedIndex = 0
    End Sub


    Private Sub UploadFile()
        Me.lblmessage.Text = Nothing

        'Dim SourcePhysicalPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto"
        Dim SourcePhysicalPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        Dim DestinationPhysicalPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue
        Dim DestinationPhotoPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue & "\Photos"

        'First delete the Bsu_shortname folder if it already exists. Then create the Bsu_shortname folder
        If IO.Directory.Exists(DestinationPhysicalPath) Then
            IO.Directory.Delete(DestinationPhysicalPath, True)
        End If
        IO.Directory.CreateDirectory(DestinationPhysicalPath)

        If IO.Directory.Exists(DestinationPhysicalPath) Then
            If IO.Directory.Exists(DestinationPhotoPath) Then
                IO.Directory.Delete(DestinationPhotoPath, True)
            End If
            IO.Directory.CreateDirectory(DestinationPhotoPath)
        End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_Code", Me.ddbsu.SelectedValue)
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Staff_Download", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("Emd_Photo") = SourcePhysicalPath & row.Item("EMD_PHOTO").ToString.Replace("/", "\")
                Next
                ds.AcceptChanges()
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                ef.Worksheets.Add("EMP LIST")
                ef.Worksheets(0).InsertDataTable(ds.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ef.Worksheets(0).HeadersFooters.AlignWithMargins = True
                For i As Integer = 0 To ef.Worksheets(0).Rows.Count - 1
                    ef.Worksheets(0).Cells(i, 3).Value = ""
                Next
                ef.Save(DestinationPhysicalPath & "\EMP_LIST.xlsx")

                'Now copy each employee photo to the destination photo path
                For Each row As DataRow In ds.Tables(0).Rows
                    If File.Exists(row.Item("Emd_Photo")) Then
                        File.Copy(row.Item("Emd_Photo"), DestinationPhotoPath & "\" & row.Item("Emp no") & ".jpg", True)
                    End If
                Next

                'Now Zip the entire Bsu_shortname folder and download it
                If File.Exists(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip") Then
                    File.Delete(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip")
                End If
                Dim zip As New Zip.FastZip
                zip.CreateZip(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip", DestinationPhysicalPath, True, Nothing)

                Dim Content() As Byte = File.ReadAllBytes(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip")
                Response.ContentType = "application/zip"
                Response.AddHeader("content-disposition", "attachment; filename=" + ddbsu.SelectedValue & ".zip")
                Response.BufferOutput = True
                Response.OutputStream.Write(Content, 0, Content.Length)
                Response.Flush()
                Response.End()


                ''Begin downloading zip file
                'Response.Clear()
                'Response.Buffer = True
                'Response.Charset = ""
                'Response.ContentType = "application/zip"
                'Response.AddHeader("content-disposition", "attachment; filename=" + ddbsu.SelectedValue & ".zip")
                'Using ZipStream As New MemoryStream()
                '    zip.CreateZip(ZipStream, DestinationPhysicalPath, True, Nothing, Nothing)
                '    ZipStream.WriteTo(Response.OutputStream)
                '    Response.Flush()
                '    Response.End()
                'End Using
                ''End downloading zip file

                ''Begin downloading excel file
                'Response.Clear()
                'Response.Buffer = True
                'Response.Charset = ""
                'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                'Response.AddHeader("content-disposition", "attachment;filename=EMP_LIST.xls")
                'Using MyMemoryStream As New MemoryStream()
                '    'ef.SaveAs(MyMemoryStream)
                '    ef.SaveXls(MyMemoryStream)
                '    MyMemoryStream.WriteTo(Response.OutputStream)
                '    Response.Flush()
                '    Response.End()
                'End Using
                ''End downloading excel file
                ef = Nothing
            End If
        Catch ex As Exception
            Me.lblmessage.Text = "Error downloading Staff list"
        End Try
        Exit Sub

        ' Dim formatarray As String() = Filter.Split(";")
        'Dim ExtractPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO"
        Dim photoPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString()
        Dim ExtractPath As String = photoPath & "\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO"

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetEMP_Photo_Download")
            While reader.Read
                Dim lstrEMPNO = Convert.ToString(reader("EMPNO")) + "_" + Convert.ToString(reader("EMPNAME"))
                Dim lstrPathDB = Convert.ToString(reader("EMD_PHOTO"))

                If lstrPathDB <> "" Then
                    'Dim ExtractPath2 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO" & "\" & di.Name
                    ' Dim ExtractPath2 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO" & "\" & di.Name
                    Dim ExtractPath2 As String = photoPath & "\OASIS_HR\ApplicantPhoto & lstrPathDB"
                    Dim UploadPath As String = photoPath & "\OASIS_HR\DownloadPath\125018\" & lstrEMPNO & ".jpg"
                    'fl = Directory.GetFiles(ExtractPath2)
                    'For Each f In fl
                    '    fi = New FileInfo(f)
                    '    'Dim ExtractPath3 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO" & "\" & di.Name & "\" & fi.Name
                    '    'Dim ExtractPath3 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO" & "\" & di.Name & "\" & fi.Name
                    Dim ExtractPath3 As String = photoPath & "\OASIS_HR\ApplicantPhoto " & lstrPathDB
                    File.Copy(ExtractPath3, UploadPath, True)
                End If

                'Next
            End While
        End Using
        'Next


    End Sub

    Private Sub GridBind(ByVal Bsu_Code As String)
        Dim VirtualPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_Code", Bsu_Code)
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Staff_Download", pParms)
            If Not ds Is Nothing Then
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("Emd_Photo") = VirtualPath & row.Item("EMD_PHOTO")
                Next
                ds.AcceptChanges()
                Me.GridDownload.DataSource = ds
                Me.GridDownload.DataBind()
            End If
        Catch ex As Exception
            Me.lblmessage.Text = "Error getting Staff list"
        End Try
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddbsu.SelectedIndexChanged
        Me.GridBind(ddbsu.SelectedValue)
    End Sub

    Protected Sub GridDownload_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridDownload.PageIndexChanging
        GridDownload.PageIndex = e.NewPageIndex
        Me.GridBind(ddbsu.SelectedValue)
    End Sub

    Public Sub DownloadStaffList()

        Me.lblmessage.Text = Nothing

        'Dim SourcePhysicalPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto"
        Dim SourcePhysicalPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        Dim DestinationPhysicalPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue
        Dim DestinationPhotoPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue & "\Photos"

        'First delete the Bsu_shortname folder if it already exists. Then create the Bsu_shortname folder
        If IO.Directory.Exists(DestinationPhysicalPath) Then
            IO.Directory.Delete(DestinationPhysicalPath, True)
        End If
        IO.Directory.CreateDirectory(DestinationPhysicalPath)

        If IO.Directory.Exists(DestinationPhysicalPath) Then
            If IO.Directory.Exists(DestinationPhotoPath) Then
                IO.Directory.Delete(DestinationPhotoPath, True)
            End If
            IO.Directory.CreateDirectory(DestinationPhotoPath)
        End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_Code", Me.ddbsu.SelectedValue)
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Staff_Download", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("Emd_Photo") = SourcePhysicalPath & row.Item("EMD_PHOTO").ToString.Replace("/", "\")
                Next
                ds.AcceptChanges()
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet
                ef.Worksheets.Add("EMP LIST")
                ws = ef.Worksheets(0)
                ws.InsertDataTable(ds.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                'AutoFit column widths
                Dim columnCount = ws.CalculateMaxUsedColumns()
                For i As Integer = 0 To columnCount - 1
                    'ws.Columns(i).AutoFitAdvanced(1, ws.Rows(1), ws.Rows(ws.Rows.Count - 1))
                    ws.Columns(i).AutoFit(1, ws.Rows(1), ws.Rows(ws.Rows.Count - 1))
                Next
                'Set header rows style
                Dim ExcelHeaderCellStyle As New GemBox.Spreadsheet.CellStyle()
                ExcelHeaderCellStyle.WrapText = False
                ExcelHeaderCellStyle.ShrinkToFit = False
                ExcelHeaderCellStyle.Font.Weight = ExcelFont.BoldWeight
                ExcelHeaderCellStyle.Font.Color = Drawing.Color.Blue
                ws.Cells(0, 0).Style = ExcelHeaderCellStyle
                ws.Cells(0, 1).Style = ExcelHeaderCellStyle
                ws.Cells(0, 2).Style = ExcelHeaderCellStyle
                'Delete the 4th column having to EMD_Photo values as it is not required in the excel file
                For i As Integer = 0 To ef.Worksheets(0).Rows.Count - 1
                    ws.Cells(i, 3).Value = ""
                Next
                ef.Save(DestinationPhysicalPath & "\EMP_LIST.xlsx")
                ef = Nothing

                'Now copy each employee photo to the destination photo path
                For Each row As DataRow In ds.Tables(0).Rows
                    If File.Exists(row.Item("Emd_Photo")) Then
                        File.Copy(row.Item("Emd_Photo"), DestinationPhotoPath & "\" & row.Item("Emp no") & ".jpg", True)
                    End If
                Next

                'Now Zip the entire Bsu_shortname folder
                If File.Exists(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip") Then
                    File.Delete(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip")
                End If
                Dim zip As New Zip.FastZip
                zip.CreateZip(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip", DestinationPhysicalPath, True, Nothing)

                'Download the created zip file
                Dim Content() As Byte = File.ReadAllBytes(SourcePhysicalPath & "\" & ddbsu.SelectedValue & ".zip")
                Response.ContentType = "application/zip"
                Response.AddHeader("content-disposition", "attachment; filename=" + ddbsu.SelectedValue & ".zip")
                Response.BufferOutput = True
                Response.OutputStream.Write(Content, 0, Content.Length)
                Response.Flush()
                Response.End()

            End If
        Catch ex As Exception
            Me.lblmessage.Text = "Error downloading Staff list"
        End Try
        Exit Sub

    End Sub

    Protected Sub btnupload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        If Me.ddbsu.SelectedValue = "-1" Then
            Me.lblmessage.Text = "Please select a Business Unit"
            Exit Sub
        End If
        If Me.GridDownload.Rows.Count <= 0 Then
            Me.lblmessage.Text = "There is no data to download"
            Exit Sub
        End If
        Me.DownloadStaffList()
    End Sub


    'Private Sub BindBsu()
    '    Dim query As String = "SELECT bsu_shortname, BSU_NAME FROM dbo.BUSINESSUNIT_M WHERE ISNULL(BSU_bSHOW,1) = 1 ORDER BY bsu_name"
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, query)
    '    If Not ds Is Nothing Then
    '        Me.ddbsu.DataValueField = "bsu_shortname"
    '        Me.ddbsu.DataTextField = "bsu_name"
    '        Me.ddbsu.DataSource = ds
    '        Me.ddbsu.DataBind()


    '    End If
    'End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
