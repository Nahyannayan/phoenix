Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj
Imports Telerik.Web.UI
'------------------
Partial Class Payroll_empWPSExportView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version                Date                Author              Change
    '1.2                    9/feb/2012          Swapna              link to view wps generated report
    '1.3                    23/apr/2012         Swapna              new WPS changes
    '1.4                    17/Apr/2014         Swapna              WPS new format changes
    '2.0                    13/Aug/2014         Swapna              GIRO file download- Singapore

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")


                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153061" And ViewState("MainMnu_code") <> "P153062") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If ViewState("MainMnu_code") = "P153061" Then
                        ViewState("datamode") = "add"
                        hlAddNew.NavigateUrl = "empGenerateWPSData.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    ElseIf ViewState("MainMnu_code") = "P153062" Then
                        ViewState("datamode") = "add"
                        hlAddNew.NavigateUrl = "empGenerateGIROFile.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If ViewState("MainMnu_code") = "P153061" Then
                        rcbSignatory_ItemsRequested() 'V1.4  
                        rcbSignatory2_ItemsRequested()
                        gridbind()
                    ElseIf ViewState("MainMnu_code") = "P153062" Then
                        rcbSignatory.Visible = False
                        rcbSignatory2.Visible = False
                        lblSignHead1.Visible = False
                        lblHeading2.Visible = False
                        lblTitle.Text = "GIRO File Details"
                        gridbindGIRODetails()
                    End If


                    End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim ds As New DataSet
            Dim pParam(3) As SqlClient.SqlParameter
            pParam(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 50)
            pParam(0).Value = Session("sBsuid")
            Dim strSearchMon, strSearchUnit As String
            Dim txtSearchMon, txtSearchUnit As TextBox
            strSearchMon = String.Empty
            strSearchUnit = String.Empty
            If gvSalary.Rows.Count > 0 Then




                'For Each gvr As GridViewRow In gvSalary.Rows


                txtSearchMon = gvSalary.HeaderRow.FindControl("txtMonyr")
                strSearchMon = txtSearchMon.Text
                txtSearchUnit = gvSalary.HeaderRow.FindControl("txtVisaUnit")
                strSearchUnit = txtSearchUnit.Text


                ' Next
            End If
            pParam(1) = New SqlClient.SqlParameter("@MonYr", SqlDbType.VarChar, 2)
            pParam(1).Value = strSearchMon

            pParam(2) = New SqlClient.SqlParameter("@VisaBSU", SqlDbType.VarChar, 2)
            pParam(2).Value = strSearchUnit

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "getWPSViewData", pParam)
            If ds.Tables(0).Rows.Count > 0 Then
                gvSalary.DataSource = ds
                gvSalary.DataBind()
                txtSearchMon = gvSalary.HeaderRow.FindControl("txtMonyr")
                txtSearchMon.Text = strSearchMon
                txtSearchUnit = gvSalary.HeaderRow.FindControl("txtVisaUnit")
                txtSearchUnit.Text = strSearchUnit
            Else
                gvSalary.DataSource = Nothing
                gvSalary.DataBind()
            End If

            objConn.Close()
            
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed "
        End Try

    End Sub
    Private Sub gridbindGIRODetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim ds As New DataSet
            Dim pParam(3) As SqlClient.SqlParameter
            pParam(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 50)
            pParam(0).Value = Session("sBsuid")
            Dim strSearchMon, strSearchUnit As String
            Dim txtSearchMon, txtSearchUnit As TextBox
            strSearchMon = String.Empty
            strSearchUnit = String.Empty
            If gvSalary.Rows.Count > 0 Then




                'For Each gvr As GridViewRow In gvSalary.Rows


                txtSearchMon = gvSalary.HeaderRow.FindControl("txtMonyr")
                strSearchMon = txtSearchMon.Text
                txtSearchUnit = gvSalary.HeaderRow.FindControl("txtVisaUnit")
                strSearchUnit = txtSearchUnit.Text


                ' Next
            End If
            pParam(1) = New SqlClient.SqlParameter("@MonYr", SqlDbType.VarChar, 2)
            pParam(1).Value = strSearchMon

            pParam(2) = New SqlClient.SqlParameter("@VisaBSU", SqlDbType.VarChar, 2)
            pParam(2).Value = strSearchUnit

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "getGIROViewData", pParam)
            If ds.Tables(0).Rows.Count > 0 Then
                gvSalary.DataSource = ds
                gvSalary.DataBind()
                txtSearchMon = gvSalary.HeaderRow.FindControl("txtMonyr")
                txtSearchMon.Text = strSearchMon
                txtSearchUnit = gvSalary.HeaderRow.FindControl("txtVisaUnit")
                txtSearchUnit.Text = strSearchUnit
                gvSalary.HeaderRow.Cells(6).Text = "GIRO file"
                gvSalary.HeaderRow.Cells(8).Text = "Remove GIRO file"
                gvSalary.Columns(0).Visible = False
            Else
                gvSalary.DataSource = Nothing
                gvSalary.DataBind()
            End If



            objConn.Close()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvSalary.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvSalary.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ViewState("MainMnu_code") = "P153061" Then
            gridbind()
        ElseIf ViewState("MainMnu_code") = "P153062" Then
            gridbindGIRODetails()
        End If
    End Sub
    Protected Sub lbTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim txtEWH_ID As TextBox
        'txtEWH_ID = TryCast(sender.Parent.FindControl("txtEWH_ID"), TextBox)
        'Dim strQuery As String = txtEWH_ID.Text
        'If ViewState("MainMnu_code") = "P153061" Then
        '    ' If Session("BSU_IsOnDAX") = 1 Then
        '    Response.Redirect("AXempSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=WPS")
        '    '[  Else
        '    '  Response.Redirect("empSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=WPS")
        '    'End If


        'ElseIf ViewState("MainMnu_code") = "P153062" Then
        '    Response.Redirect("empSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=GIRO")
        'End If

        Dim txtEWH_ID As TextBox
        txtEWH_ID = TryCast(sender.Parent.FindControl("txtEWH_ID"), TextBox)
        Dim strQuery As String = txtEWH_ID.Text
        If ViewState("MainMnu_code") = "P153061" Then
            If Session("BSU_IsOnDAX") = 1 Then
                Response.Redirect("AXempSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=WPS")
            Else
                Response.Redirect("empSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=WPS")
            End If


        ElseIf ViewState("MainMnu_code") = "P153062" Then
            If Session("BSU_IsOnDAX") = 1 Then  '--change for GWS
                Response.Redirect("AXempSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=GIRO")
            Else
                Response.Redirect("empSalaryTransfer.aspx?EWH_ID=" & strQuery & "&MainMnu_code=P130171&datamode=add&Filetype=GIRO")
            End If

        End If

    End Sub
    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim strdocno As String = sender.Text
            If strdocno = "" Then
                Return
            End If

            Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", strdocno, False, True)
            'Session("ReportSource") = AccountsReports.BankPaymentVoucher(strdocno, "", Session("sBSUID"), Session("F_YEAR"))
            ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
            ReportLoadSelection()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim lblVUnit, lblMonthYear, lblExpDate, lblEWH_ID As Label
            Dim lbldocNo As LinkButton
            lblVUnit = TryCast(sender.Parent.FindControl("lblVUnit"), Label)
            lblExpDate = TryCast(sender.Parent.FindControl("lblExpDate"), Label)
            lblMonthYear = TryCast(sender.Parent.FindControl("lblMonthYear"), Label)
            lbldocNo = TryCast(sender.Parent.FindControl("lbVoucher"), LinkButton)
            lblEWH_ID = TryCast(sender.Parent.FindControl("lblEWH_ID"), Label)

            '    Dim str_Sql As String = "exec getWPSPrintData '" & Session("sBsuid") & "','" & lblMonthYear.Text & "','" & lblVUnit.Text & "'" & ",'" & lblExpDate.Text & "'"
            'V1.3
            If lbldocNo.Text = "" Then
                lblError.Text = "Please transfer salary first."
                Exit Sub
            End If
            Dim str_Sql As String = "exec getWPSPrintData_NewFormat '" & Session("sBsuid") & "','" & lblMonthYear.Text & "','" & lblVUnit.Text & "'" & ",'" & lblExpDate.Text & "'," & lblEWH_ID.Text & ",'" & lbldocNo.Text & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("UserName") = Session("sUsr_name")
                If rcbSignatory.Items.Count > 0 Then
                    params("SignatoryName") = rcbSignatory.SelectedItem.Text 'V1.4   
                    params("SignatoryDesg") = rcbSignatory.SelectedItem.Value.Split(",")(0) 'V1.4 
                Else
                    params("SignatoryName") = "" 'V1.4   
                    params("SignatoryDesg") = "" 'V1.4   
                End If
                If rcbSignatory2.Items.Count > 0 Then
                    params("SignatoryName2") = rcbSignatory2.SelectedItem.Text 'V1.4   
                    params("SignatoryDesg2") = rcbSignatory2.SelectedItem.Value.Split(",")(0)
                Else
                    params("SignatoryName2") = "" 'V1.4   
                    params("SignatoryDesg2") = "" 'V1.4   
                End If
                If rcbSignatory2.SelectedItem.Value.Split(",")(0) = "--" Then
                    params("SignatoryName2") = "" 'V1.4   
                    params("SignatoryDesg2") = "" 'V1.4   
                End If
              
                repSource.Parameter = params
                repSource.Command = cmd
                ' repSource.ResourceName = "../../payroll/Reports/RPT/rptWPSPrint.rpt" 
                repSource.ResourceName = "../../payroll/Reports/RPT/WPSPrintNewFormatLatest.rpt" 'V1.4   
                Session("ReportSource") = repSource
                'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvSalary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSalary.RowCommand
        Dim strDocID As String = ""
        strDocID = e.CommandArgument.ToString
        If e.CommandName = "View" Then
            OpenFileFromDB(strDocID)
        End If



    End Sub
    Protected Sub OpenFileFromDB(ByVal DocId As Integer)
        Try

            Dim conn As String = ""

            conn = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("OpenSifFile", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@EWH_ID", DocId)

            Dim myReader As SqlDataReader = cmd.ExecuteReader()

            If myReader.Read Then

                Dim SiffileName As String = myReader("EWH_FileName").ToString()
                Dim bytes() As Byte = CType(myReader("EWH_PROCESSFILE"), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.AddHeader("content-disposition", "attachment;filename=" & SiffileName)
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End If
            myReader.Close()

        Catch ex As Exception
            lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try

    End Sub

    Protected Sub gvSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSalary.RowDataBound
        Dim i As Integer = 0
        For Each grow As GridViewRow In gvSalary.Rows

            'Dim txtFileID As TextBox = DirectCast(grow.Cells(7).FindControl("txtEWH_ID"), TextBox)


            Dim imgDoc As Image = DirectCast(grow.Cells(6).FindControl("imgDoc"), Image)

            If imgDoc.AlternateText <> 0 Then
                imgDoc.Visible = True
            Else
                imgDoc.Visible = False
            End If


            Dim lnkTran As LinkButton = DirectCast(grow.Cells(7).FindControl("lbTransfer"), LinkButton)
            Dim lnkDelete As LinkButton = DirectCast(grow.Cells(7).FindControl("lnkDelete"), LinkButton)
            ' code changed by swapna
            'If lnkTran.CommandArgument <> "" Then
            '    lnkTran.Visible = False
            '    lnkDelete.Visible = False
            'Else
            '    lnkTran.Visible = True
            '    lnkDelete.Visible = True
            'End If
            Dim lbVoucher As LinkButton = DirectCast(grow.Cells(7).FindControl("lbVoucher"), LinkButton)

            If lnkTran.CommandArgument <> "" Then
                lnkTran.Visible = False
                lnkDelete.Visible = False
                If lbVoucher.Text = "" Or lbVoucher.Text = "0" Then
                    lbVoucher.Visible = False
                Else
                    lbVoucher.Visible = True
                End If

            Else
                lnkTran.Visible = True
                lnkDelete.Visible = True
                lbVoucher.Visible = False
            End If
        Next

    End Sub


    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlTran As SqlTransaction = objConn.BeginTransaction


        Dim cmd As New SqlCommand("RemoveWPSFileAndData", objConn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim txtEWH_ID As TextBox
        txtEWH_ID = TryCast(sender.Parent.FindControl("txtEWH_ID"), TextBox)

        Dim sqlpEWHIDAs As New SqlParameter("@EWH_ID", SqlDbType.Int)
        sqlpEWHIDAs.Value = txtEWH_ID.Text
        cmd.Parameters.Add(sqlpEWHIDAs)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlRetVlaue As New SqlParameter("@sqlRetVlaue", SqlDbType.Int)
        sqlRetVlaue.Direction = ParameterDirection.ReturnValue

        cmd.Transaction = sqlTran
        Dim retVal As Integer = 0
        retVal = cmd.ExecuteNonQuery()
        retVal = sqlRetVlaue.Value
        If retVal <> 0 Then
            sqlTran.Rollback()
        End If
        If retVal = "0" Then

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            sqlpEWHIDAs.Value & "--" & _
            sqlpBSU_ID.Value, _
            "Delete WPS", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            sqlTran.Commit()

            If ViewState("MainMnu_code") = "P153061" Then
                gridbind()
            ElseIf ViewState("MainMnu_code") = "P153062" Then
                gridbindGIRODetails()
            End If
        End If
    End Sub
    Protected Sub lnkDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim txtEWH_ID As TextBox
            Dim lblVUnit, lblMonthYear, lblExpDate, lblVUnitDescr As Label
            lblVUnit = TryCast(sender.Parent.FindControl("lblVUnit"), Label)
            lblVUnitDescr = TryCast(sender.Parent.FindControl("lblVisaBsu"), Label)
            lblExpDate = TryCast(sender.Parent.FindControl("lblExpDate"), Label)
            lblMonthYear = TryCast(sender.Parent.FindControl("lblMonthYear"), Label)


            txtEWH_ID = TryCast(sender.Parent.FindControl("txtEWH_ID"), TextBox)
            Dim strQuery As String = txtEWH_ID.Text

            Dim str_Sql As String = "exec rptWPSGeneratedDetails '" & Session("sBsuid") & "','" & lblMonthYear.Text & "','" & lblVUnit.Text & "'" & ",'" & lblExpDate.Text & "'," & strQuery
            'Dim str_Sql As String = "exec getWPSPrintData '" & Session("sBsuid") & "','" & lblMonthYear.Text & "','" & lblVUnit.Text & "'" & ",'" & lblExpDate.Text & "'"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("UserName") = Session("sUsr_name")
                params("Bsu_ID") = Session("sBsuid")
                params("ImgType") = "Logo"
                params("monyear") = lblMonthYear.Text
                params("VisaDescr") = lblVUnitDescr.Text


                repSource.HeaderBSUID = Session("sBsuid")

                repSource.IncludeBSUImage = True
                repSource.Parameter = params

                repSource.Command = cmd
                repSource.ResourceName = "../../payroll/Reports/RPT/RptWPSGeneratedDetails.rpt"
                Session("ReportSource") = repSource
                'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            lblError.Text = ex.Message

        End Try
    End Sub
    Protected Sub rcbSignatory_ItemsRequested() 'V1.4   
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "exec dbo.GetLetterSignatory '" & Session("SBsuid") & "','WPS'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        rcbSignatory.DataSource = ds
        rcbSignatory.DataBind()
        rcbSignatory.SelectedIndex = 0


    End Sub
    Protected Sub rcbSignatory2_ItemsRequested() 'V1.6 
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "exec dbo.GetLetterSignatory '" & Session("SBsuid") & "','WPS','2'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        rcbSignatory2.DataSource = ds
        rcbSignatory2.DataBind()
        rcbSignatory2.SelectedIndex = 0


    End Sub
    Protected Sub rcbSignatory_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs) 'V1.4   

        'set the Text and Value property of every item

        'here you can set any other properties like Enabled, ToolTip, Visible, etc.

        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()

        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("EMP_DES_DESCR").ToString() & "," & (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()


    End Sub
    Protected Sub rcbSignatory2_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs) 'V1.4   

        'set the Text and Value property of every item

        'here you can set any other properties like Enabled, ToolTip, Visible, etc.

        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()

        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("EMP_DES_DESCR").ToString() & "," & (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()


    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
