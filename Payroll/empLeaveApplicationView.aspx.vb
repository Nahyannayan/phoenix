Imports System.IO
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports Telerik.Web.UI
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj


Partial Class Payroll_empLeaveApplicationView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Enum UserMessageType As Integer
        Success
        Failure
        None
    End Enum
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Sub HandleUserMessage(ByVal Usr_Msg As String, ByVal Msg_Type As UserMessageType)
        If Msg_Type = UserMessageType.Success Then
            lblmsg.CssClass = "divValidMsg"
            lblmsg.Visible = True
            lblmsg.Text = Usr_Msg
        ElseIf Msg_Type = UserMessageType.Failure Then
            lblmsg.CssClass = "divErrorMsg"
            lblmsg.Visible = True
            lblmsg.Text = Usr_Msg
        ElseIf Msg_Type = UserMessageType.None Then
            lblmsg.Visible = False
            lblmsg.Text = Nothing
        End If
    End Sub

    Private Sub HandlePopUpUserMessage(ByVal Usr_Msg As String, ByVal Msg_Type As UserMessageType)
        'If Msg_Type = UserMessageType.Success Then
        '    lblPopupMsg.Visible = True
        '    lblPopupMsg.Text = Usr_Msg
        'ElseIf Msg_Type = UserMessageType.Failure Then
        '    lblPopupMsg.Visible = True
        '    lblPopupMsg.Text = Usr_Msg
        'ElseIf Msg_Type = UserMessageType.None Then
        '    lblPopupMsg.Visible = False
        '    lblPopupMsg.Text = Nothing
        'End If
    End Sub

    Private Function ValidateData() As Boolean
        ValidateData = False
        If Me.txtFrom.Text = Nothing Then
            HandleUserMessage("Please enter a valid from date value", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Not DateTime.TryParse(Me.txtFrom.Text, Nothing) Then
            HandleUserMessage("Please enter a valid From Date value", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtTo.Text = Nothing OrElse Not DateTime.TryParse(Me.txtTo.Text, Nothing) Then
            HandleUserMessage("Please enter a valid To Date value", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtRemarks.Text = Nothing OrElse Me.txtRemarks.Text.Length > 500 Then
            HandleUserMessage("Please enter leave details (Max 300 characters)", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtHandover.Text = Nothing OrElse Me.txtHandover.Text.Length > 100 Then
            HandleUserMessage("Please enter name of the contact person in your absence (Max 50 characters)", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtAddress.Text = Nothing OrElse Me.txtAddress.Text.Length > 500 Then
            HandleUserMessage("Please enter address details (Max 300 characters)", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtRemarks.Text = Nothing OrElse Me.txtRemarks.Text.Length > 500 Then
            HandleUserMessage("Please enter leave details (Max 300 characters)", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtPhone.MaxLength > 50 Then
            HandleUserMessage("Phone number should not exceed 50 characters", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        If Me.txtMobile.MaxLength > 50 Then
            HandleUserMessage("Mobile number should not exceed 50 characters", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Return False
        End If
        ' ''If Me.txtPhone.Text = Nothing Then
        ' ''    HandleUserMessage("Please enter a valid phone no", UserMessageType.Failure)
        ' ''    Me.divPopup.Visible = False
        ' ''    Return False
        ' ''End If
        ' ''If Me.txtMobile.Text = Nothing Then
        ' ''    HandleUserMessage("Please enter a valid mobile no", UserMessageType.Failure)
        ' ''    Me.divPopup.Visible = False
        ' ''    Return False
        ' ''End If

        HandleUserMessage(Nothing, UserMessageType.None)

        Return True
    End Function

    Private Sub ResetViewData() 'resetting controls on view/edit

        imgEmployee.Enabled = True
        txtPhone.Attributes.Remove("readonly")
        txtFrom.Attributes.Remove("readonly")
        txtTo.Attributes.Remove("readonly")
        txtMobile.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        '''txtPobox.Attributes.Remove("readonly")
        txtAddress.Attributes.Remove("readonly")
        txtHandover.Attributes.Remove("readonly")
        chkForward.Enabled = True
        ddMonthstatusPeriodically.Enabled = True

        'CalendarExtender1.Enabled = True
        'CalendarExtender2.Enabled = True
        'CalendarExtender3.Enabled = True
        'DocDate.Enabled = True
        cxT_FrmDt.Enabled = True
        cxT_FrmDtC.Enabled = True
        cxT_ToDt.Enabled = True
        cxT_ToDtC.Enabled = True
        imgBtnT_FrmDt.Enabled = True
        imgBtnT_ToDt.Enabled = True


        If ViewState("IsForwarded") = "yes" Then
            btnCancelLeave.Visible = True
        Else
            btnCancelLeave.Visible = False
        End If
        chkForward.Attributes.Remove("readonly")
        'V1.4
    End Sub

    Private Sub IsPlannerEnabled(Optional ByVal ForCurrentMonth As Boolean = False)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter 'V1.1
            pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
            pParms(0).Value = h_Bsu_Id.Value

            pParms(1) = New SqlClient.SqlParameter("@ELTID", SqlDbType.VarChar, 10)
            pParms(1).Value = ddMonthstatusPeriodically.SelectedValue

            pParms(2) = New SqlClient.SqlParameter("@ECTID", SqlDbType.Int)
            pParms(2).Value = ViewState("BLS_ECT_ID")

            pParms(3) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
            If ForCurrentMonth = True Then
                pParms(3).Value = New Date(Now.Year, Now.Month, Now.Day)
            Else
                pParms(3).Value = Convert.ToDateTime(txtFrom.Text)
            End If

            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "CheckPlannerEnabled", pParms)

            Dim count = pParms(4).Value
            If count > 0 Then
                ViewState("IsPlannerEnabled") = 1
            Else
                ViewState("IsPlannerEnabled") = 0
            End If

        Catch ex As Exception
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = "Error in fetching leaves."
        End Try
    End Sub

    Private Function GetActualLeaveDays(Optional ByVal ForCurrentMonth As Boolean = False, Optional ByVal FromDate As Date = Nothing, Optional ByVal ToDate As Date = Nothing) As Decimal
        GetActualLeaveDays = 0
        Dim StartDate, EndDate As Date
        Try
            IsPlannerEnabled(ForCurrentMonth)
            If Not ForCurrentMonth Then
                'If txtFrom.Text = "" Or txtTo.Text = "" Then
                If FromDate = Nothing Or ToDate = Nothing Then
                    lblmsg.CssClass = "divInValidMsg"
                    lblmsg.Text = "Enter leave dates"
                    Exit Function
                End If
            End If
            If ForCurrentMonth Then
                StartDate = New Date(Now.Year, Now.Month, 1)
                EndDate = New Date(Now.Year, Now.Month, Date.DaysInMonth(Now.Year, Now.Month))
            Else
                'StartDate = CDate(txtFrom.Text)
                StartDate = FromDate
                'EndDate = CDate(txtTo.Text)
                EndDate = ToDate
            End If

            If EndDate < StartDate Then
                lblmsg.CssClass = "divInValidMsg"
                lblmsg.Text = "Invalid Dates"
                Exit Function
            End If
            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & StartDate & "','" & EndDate & "','" & h_Bsu_Id.Value & "'," & h_Emp_No.Value & ",'" & ddMonthstatusPeriodically.SelectedValue & "') as days"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
            Dim days As String = ds.Tables(0).Rows(0).Item("days")
            Return days
        Catch ex As Exception
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = "Error Occured"
        End Try
    End Function

    Private Function GetLeaveMatrix(ByVal StartDate As Date, ByVal Days As Decimal, ByVal BsuId As String, ByVal IsPlannerEnabled As Integer) As DataSet
        GetLeaveMatrix = Nothing
        'If Request.QueryString("StartDate") <> "" And Request.QueryString("Days") <> "" And Request.QueryString("BSUID") <> "" And Request.QueryString("IsPlannerEnabled") = 1 Then
        'End If
        If IsPlannerEnabled = 1 Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(0).Value = StartDate

            pParms(1) = New SqlClient.SqlParameter("@Days", SqlDbType.Int)
            pParms(1).Value = Days

            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = BsuId


            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[GetMyLeaveDays]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                Return ds
            End Using
        End If
    End Function

    Private Function GetLeaveMatrixNew(ByVal StartDate As Date, ByVal Days As Decimal, ByVal BsuId As String, ByVal IsPlannerEnabled As Integer) As DataSet
        GetLeaveMatrixNew = Nothing
        'If Request.QueryString("StartDate") <> "" And Request.QueryString("Days") <> "" And Request.QueryString("BSUID") <> "" And Request.QueryString("IsPlannerEnabled") = 1 Then
        'End If
        If IsPlannerEnabled = 1 Then
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(0).Value = StartDate

            pParms(1) = New SqlClient.SqlParameter("@Days", SqlDbType.Int)
            pParms(1).Value = Days

            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = BsuId

            pParms(3) = New SqlClient.SqlParameter("@EMPID", SqlDbType.Int)
            pParms(3).Value = Me.h_Emp_No.Value


            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[GetMyLeaveDaysV2]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                Return ds
            End Using
        End If
    End Function

    Public Sub ShowLeaveCalculation(Optional ByVal ForCurrentMonth As Boolean = False, Optional ByVal FromDate As Date = Nothing, Optional ByVal ToDate As Date = Nothing, Optional ByVal ForRange As Boolean = False, Optional ByVal MonthStartDate As Date = Nothing, Optional ByVal MonthEndDate As Date = Nothing)

        Dim StartDate, EndDate, LeaveStartMonthStartDate, LeaveEndMonthEndDate, ReturnDate As Date
        Dim dsLeaveMatrix As DataSet = Nothing
        If Not ForCurrentMonth Then
            'If txtFrom.Text = "" Or txtTo.Text = "" And ForRange = False Then
            If FromDate = Nothing Or ToDate = Nothing Then
                lblmsg.CssClass = "divInValidMsg"
                lblmsg.Text = "Enter leave dates"
                Exit Sub
            End If
        End If
        If ForCurrentMonth Then
            StartDate = New Date(Now.Year, Now.Month, 1)
            EndDate = New Date(Now.Year, Now.Month, Date.DaysInMonth(Now.Year, Now.Month))
        Else
            'StartDate = CDate(txtFrom.Text)
            StartDate = FromDate
            'EndDate = CDate(txtTo.Text)
            EndDate = ToDate
        End If

        LeaveStartMonthStartDate = New Date(StartDate.Year, StartDate.Month, 1)
        LeaveEndMonthEndDate = New Date(EndDate.Year, EndDate.Month, Date.DaysInMonth(EndDate.Year, EndDate.Month))

        ' ''If MonthStartDate.Year = 1 Then
        ' ''    LeaveStartMonthStartDate = StartDate
        ' ''Else
        ' ''    LeaveMonthStartDate = MonthStartDate
        ' ''End If

        ' ''If MonthEndDate.Year = 1 Then
        ' ''    LeaveMonthEndDate = EndDate
        ' ''Else
        ' ''    LeaveMonthEndDate = MonthEndDate
        ' ''End If

        Dim ActualLeaveDays, TotalLeaveDays, RemainingDaysBeforeApplying, RemainingDays As Decimal
        Dim ShowBalance As Boolean
        Dim TextToRender As String = ""
        ReturnDate = DateAdd(DateInterval.Day, 1, EndDate)
        TotalLeaveDays = DateDiff(DateInterval.Day, StartDate, EndDate) + 1
        If TotalLeaveDays = 0 Then Exit Sub
        ActualLeaveDays = GetActualLeaveDays(ForCurrentMonth, StartDate, EndDate)
        '''If ActualLeaveDays = 0 Then Exit Sub

        'dsLeaveMatrix = Me.GetLeaveMatrix(StartDate, ActualLeaveDays, h_Bsu_Id.Value, ViewState("IsPlannerEnabled"))
        'commented Above added By Mahesh on 15-Dec-2019

        dsLeaveMatrix = Me.GetLeaveMatrixNew(StartDate, ActualLeaveDays, h_Bsu_Id.Value, ViewState("IsPlannerEnabled"))


        If Not dsLeaveMatrix Is Nothing Then
            TotalLeaveDays = dsLeaveMatrix.Tables(0).Rows.Count

            ReturnDate = DateAdd(DateInterval.Day, 1, CDate(dsLeaveMatrix.Tables(0).Rows(dsLeaveMatrix.Tables(0).Rows.Count - 1).Item("Date")))
        End If

        If GetEmpLeaveCounts(StartDate, Me.ddMonthstatusPeriodically.SelectedValue, , , RemainingDaysBeforeApplying, ShowBalance) Is Nothing Then
            Exit Sub
        End If

        Dim ActualTakenLeaves As Integer = 0
        'demo
        ' ''Dim dsActualLeaves As New DataSet
        ' ''Me.GetEmpActualLeaveCountsWhileApproving(Me.h_Emp_No.Value, Me.ddMonthstatusPeriodically.SelectedValue, dsActualLeaves)
        ' ''If Not dsActualLeaves Is Nothing AndAlso dsActualLeaves.Tables.Count > 0 AndAlso dsActualLeaves.Tables(0).Rows.Count > 0 AndAlso dsActualLeaves.Tables(0).Rows(0).Item("elt_id") = Me.ddMonthstatusPeriodically.SelectedValue Then
        ' ''    ActualTakenLeaves = dsActualLeaves.Tables(0).Rows(0).Item("Taken")
        ' ''End If
        'end demo

        RemainingDays = RemainingDaysBeforeApplying - (ActualLeaveDays + ActualTakenLeaves)

        '<tr class="trLeaveCal_Row">
        '            <td>Annual Leave</td><td>30/Dec/2013 - 02/Jan/2014</td><td>03/Jan/2014</td>
        '            <td >4</td><td >3</td><td >1</td>
        '</tr>

        'Reset dsLeaveMatrix and re-populate it including the start date of the leave start month and end date of the leave end month
        Dim tmpDays As Integer = 0
        tmpDays = DateDiff(DateInterval.Day, LeaveStartMonthStartDate, LeaveEndMonthEndDate) + 1
        dsLeaveMatrix = Me.GetLeaveMatrix(LeaveStartMonthStartDate, tmpDays, h_Bsu_Id.Value, ViewState("IsPlannerEnabled"))

        If Not dsLeaveMatrix Is Nothing Then
            For i = dsLeaveMatrix.Tables(0).Rows.Count - 1 To 0 Step -1
                If dsLeaveMatrix.Tables(0).Rows(i).Item("Date") > LeaveEndMonthEndDate Then
                    dsLeaveMatrix.Tables(0).Rows.RemoveAt(i)
                End If
            Next
            dsLeaveMatrix.AcceptChanges()

            'Add DayType, ColorCode, Status column to dsLeaveMatrixTable
            dsLeaveMatrix.Tables(0).Columns.Add("DayType", GetType(String))
            dsLeaveMatrix.Tables(0).Columns.Add("Back_Color", GetType(String))
            dsLeaveMatrix.Tables(0).Columns.Add("Fore_Color", GetType(String))
            dsLeaveMatrix.Tables(0).Columns.Add("Status", GetType(String))
            dsLeaveMatrix.AcceptChanges()
        End If

        '' ''for demo purpose only
        ' ''If Not dsLeaveMatrix Is Nothing Then
        ' ''    For Each row As DataRow In dsLeaveMatrix.Tables(0).Rows
        ' ''        If Left(row.Item("Date"), 6) = "01/Jan" Then
        ' ''            row.Item("Remarks") = "New Year"
        ' ''        End If
        ' ''    Next
        ' ''    dsLeaveMatrix.AcceptChanges()
        ' ''End If       

        'Get already applied leaves for the selected period and add it to dsLeaveMatrix table
        ''Dim query As String = "SELECT  LA.ELA_DTFROM, LA.ELA_DTTO, LA.ELA_ELT_ID, DATEDIFF(DAY,la.ELA_DTFROM, la.ELA_DTTO) AS Days, ISNULL(LA.ELA_APPRSTATUS,'N') ELA_APPRSTATUS FROM dbo.EMPLEAVEAPP LA WHERE LA.ELA_BSU_ID = '" & h_Bsu_Id.Value & "' AND LA.ELA_EMP_ID = " & Me.h_Emp_No.Value & " AND LA.ELA_ELT_ID = '" & Me.ddMonthstatusPeriodically.SelectedValue & "' AND LA.ELA_DTFROM > = CONVERT(DATETIME,'" & Format(StartDate, "dd/MMM/yy") & "') AND LA.ELA_DTTO < = CONVERT(DATETIME,'" & Format(EndDate, "dd/MMM/yy") & "')"
        '''Dim query As String = "SELECT  LA.ELA_DTFROM, LA.ELA_DTTO, LA.ELA_ELT_ID, DATEDIFF(DAY,la.ELA_DTFROM, la.ELA_DTTO) AS Days, ISNULL(LA.ELA_APPRSTATUS,'N') ELA_APPRSTATUS FROM dbo.EMPLEAVEAPP LA WHERE LA.ELA_BSU_ID = '" & h_Bsu_Id.Value & "' AND LA.ELA_EMP_ID = " & Me.h_Emp_No.Value & " AND LA.ELA_ELT_ID = '" & Me.ddMonthstatusPeriodically.SelectedValue & "' AND LA.ELA_DTFROM > = CONVERT(DATETIME,'" & Format(LeaveStartMonthStartDate, "dd/MMM/yy") & "') AND LA.ELA_DTTO < = CONVERT(DATETIME,'" & Format(LeaveEndMonthEndDate, "dd/MMM/yy") & "')"

        Dim params(4) As SqlParameter
        params(0) = New SqlClient.SqlParameter("@Bsu_Id", SqlDbType.VarChar)
        params(0).Value = h_Bsu_Id.Value
        params(1) = New SqlClient.SqlParameter("@Emp_Id", SqlDbType.Int)
        params(1).Value = h_Emp_No.Value
        params(2) = New SqlClient.SqlParameter("@Elt_Id", SqlDbType.VarChar)
        '''params(2).Value = Me.ddMonthstatusPeriodically.SelectedValue
        params(2).Value = "" 'get all leave types
        params(3) = New SqlClient.SqlParameter("@LeaveStartMonthStartDate", SqlDbType.VarChar)
        params(3).Value = Format(LeaveStartMonthStartDate, "dd/MMM/yy")
        params(4) = New SqlClient.SqlParameter("@LeaveEndMonthEndDate", SqlDbType.VarChar)
        params(4).Value = Format(LeaveEndMonthEndDate, "dd/MMM/yy")

        '''ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)

        '''With SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString(), CommandType.Text, query)
        With SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString(), CommandType.StoredProcedure, "[USR_SS].[GetAlreadyAppliedLeaves]", params)
            Dim tmpStartDate, tmpEndDate As Date
            Dim Days As Integer = 0
            Dim tmpRows() As DataRow
            ''below  if by nahyan on 24dec2018
            If Not dsLeaveMatrix Is Nothing Then
                Do While .Read
                    'Days = DateDiff(DateInterval.Day, .Item("Ela_DTTo"), .Item("Ela_DtFrom")) + 1
                    'Days = DateDiff(DateInterval.Day, .Item("Ela_DTFrom"), .Item("Ela_DTTo")) + 1
                    Days = .Item("Days") + 1
                    For i As Integer = 1 To Days
                        Dim row As DataRow = dsLeaveMatrix.Tables(0).NewRow
                        row.Item("Id") = dsLeaveMatrix.Tables(0).Rows.Count + 1
                        row.Item("Date") = Format(DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")), "dd/MMM/yy")
                        'row.Item("Day") = DateAdd(DateInterval.Day, i , DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")).DayOfWeek
                        row.Item("Day") = DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")).DayOfWeek
                        row.Item("Remarks") = ""
                        row.Item("DayType") = .Item("ELA_ELT_Id")
                        tmpRows = dsLeaveMatrix.Tables(0).Select("Date = '" & Format(DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")), "dd/MMM/yy") & "'")
                        If tmpRows.Length > 0 Then
                            For Each tmprow In tmpRows
                                If Not IsDBNull(tmprow.Item("Remarks")) AndAlso Not tmprow.Item("Remarks") = "" Then
                                    If tmprow.Item("Remarks") = Session("BsuWeekEnd1") Or tmprow.Item("Remarks") = Session("BsuWeekEnd2") Then
                                        row.Item("DayType") = "WK"
                                    Else
                                        row.Item("DayType") = "HOL_STAFF"
                                    End If
                                End If
                            Next
                        End If
                        row.Item("Back_Color") = ""
                        row.Item("Fore_Color") = ""
                        row.Item("Status") = .Item("ELA_APPRSTATUS")
                        dsLeaveMatrix.Tables(0).Rows.Add(row)
                    Next
                Loop
            End If
            ''above end if by nahyan on 24dec2018
            If Not dsLeaveMatrix Is Nothing Then
                dsLeaveMatrix.AcceptChanges()
            End If
        End With

        ''Now update DayType column in dsLeaveMatrix dataset where DayType <> ""
        If Not dsLeaveMatrix Is Nothing Then
            For Each row As DataRow In dsLeaveMatrix.Tables(0).Rows
                If IsDBNull(row.Item("DayType")) OrElse row.Item("DayType") = "" Then
                    If Not row.Item("Remarks") Is Nothing Then
                        If row.Item("Remarks") = Session("BsuWeekEnd1") Or row.Item("Remarks") = Session("BsuWeekEnd2") Then
                            row.Item("DayType") = "WK" 'weekend
                        ElseIf row.Item("Remarks") <> "" Then
                            row.Item("DayType") = "HOL_STAFF" 'holiday
                        Else
                            If ForRange = True Then
                                'row.Item("DayType") = Me.ddMonthstatusPeriodically.SelectedValue
                                If row.Item("Date") < StartDate Or row.Item("Date") > EndDate Then
                                Else
                                    row.Item("DayType") = "AD" 'corresponding to AD value in EMPLeaveType_Color table
                                End If
                                'row.Item("Back_Color") = "E4FC72"
                                'row.Item("Back_Color") = "#000000"
                            End If
                        End If
                    End If
                End If
            Next
            dsLeaveMatrix.AcceptChanges()
        End If

        Dim dsColor As DataSet
        If Not Session("LeaveTypeColorCodes") Is Nothing Then
            dsColor = CType(Session("LeaveTypeColorCodes"), DataSet)
        End If

        Dim ColorIndex, TotalColors As Integer
        Dim ColorRows() As DataRow

        If Not dsColor Is Nothing And Not dsLeaveMatrix Is Nothing Then
            ColorRows = dsColor.Tables(0).Select("ELT_ID='HOL_STAFF'")
            TotalColors = ColorRows.Length

            For Each row As DataRow In dsLeaveMatrix.Tables(0).Rows
                If Not IsDBNull(row.Item("DayType")) AndAlso Not row.Item("DayType") Is Nothing Then
                    If row.Item("DayType") = "HOL_STAFF" Then
                        row.Item("Back_Color") = ColorRows(ColorIndex).Item("Back_Color")
                        row.Item("Fore_Color") = ColorRows(ColorIndex).Item("Fore_Color")
                        'ColorIndex += 1
                        'If ColorIndex >= TotalColors Then
                        '    ColorIndex = 1
                        'End If

                    Else
                        Dim tmpRows() As DataRow = dsColor.Tables(0).Select("ELT_ID = '" & row.Item("DayType") & "'")
                        If tmpRows.Length > 0 Then
                            row.Item("Back_Color") = tmpRows(0).Item("Back_Color")
                            row.Item("Fore_Color") = tmpRows(0).Item("Fore_Color")
                        End If
                    End If
                End If
            Next
            dsLeaveMatrix.AcceptChanges()
        End If

        Session("LeaveMatrix") = dsLeaveMatrix

        Dim LeaveColor, LeaveColorRenderText As String
        If Not Session("LeaveTypeColorCodes") Is Nothing Then
            Dim tmpRows() As DataRow
            tmpRows = CType(Session("LeaveTypeColorCodes"), DataSet).Tables(0).Select("ELT_ID='" & Me.ddMonthstatusPeriodically.SelectedValue & "'")
            If tmpRows.Length > 0 Then
                LeaveColor = tmpRows(0).Item("Back_Color")
            End If
        End If
        If ForRange = True Then
            If Not LeaveColor = "" Then
                LeaveColorRenderText &= "<div style=" & Chr(34) & "background-color:" & LeaveColor & ";" & Chr(34) & " class=" & Chr(34) & "calEventColor" & Chr(34) & "></div>"
            End If
            'TextToRender &= "<tr class=" & Chr(34) & "trLeaveCal_Row" & Chr(34) & IIf(LeaveColor = Nothing, Nothing, Chr(34) & "background-color:" & Chr(34) & LeaveColor) & ">" & ""
            TextToRender &= "<tr class=" & Chr(34) & "trLeaveCal_Row" & Chr(34) & IIf(LeaveColor = Nothing, Nothing, Nothing) & ">" & ""
            TextToRender &= "<td>" & IIf(LeaveColor = Nothing, Nothing, LeaveColorRenderText) & Me.ddMonthstatusPeriodically.SelectedItem.Text & "</td><td>"
            TextToRender &= txtFrom.Text & " - " & IIf(txtTo.Text = "dd/MMM/yyyy", Format(EndDate, "dd/MMM/yyyy"), txtTo.Text) & "</td><td>"
            TextToRender &= Format(ReturnDate, "dd/MMM/yyyy") & "</td><td>"
            TextToRender &= Convert.ToInt32(TotalLeaveDays) & "</td><td>"
            TextToRender &= Convert.ToInt32(ActualLeaveDays) & "</td><td>"
            If ShowBalance = True Then
                TextToRender &= Convert.ToInt32(RemainingDays) & "</td>"
            End If
            TextToRender &= "</tr>"
            Me.LiteralLeaveCalculation.Text = TextToRender
        End If
        If Me.ddMonthstatusPeriodically.SelectedValue <> "AL" Then
            Me.LiteralLeaveCalculation.Text = ""
        End If

        'Me.calDatepicker.VisibleDate = StartDate
        Me.LoadCalendar(False, StartDate, True, True)

        '''Me.ShowCalendarEvents(StartDate, EndDate)
        Me.ShowCalendarEvents(LeaveStartMonthStartDate, LeaveEndMonthEndDate)

    End Sub

    Private Sub ShowLeaveStatus(ByVal AsofDate As Date)
        Dim dsLeaveStatus As New DataSet
        Dim dsLeaveColors As DataSet = Session("LeaveTypeColorCodes")
        Try
            dsLeaveStatus.Tables.Add()
            dsLeaveStatus.Tables(0).Columns.Add("elt_id", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("elt_descr", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("bShow_Details", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("Tot_Taken", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("Tot_Balance", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("bPrev_Record", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("elt_Order", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("Elt_BackColor", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("Elt_ForeColor", GetType(String))
            dsLeaveStatus.AcceptChanges()

            Dim Eligible, Taken, Balance As Integer
            Dim Showbalance As Boolean
            Dim tmpDs, dsExistingLeaves As DataSet, row, colorrow As DataRow
            '''Dim query As String = "SELECT  ELA_ELT_ID , COUNT(ELA_ELT_ID) AS LeaveCount FROM dbo.EMPLEAVEAPP WHERE ELA_ELT_ID IN ( SELECT  [ELT_ID] FROM [EMPLEAVETYPE_M] WHERE ELT_bLEAVE = 1 ) GROUP BY ELA_ELT_ID"

            'first get the no of existing leaves for each leave type
            '''dsExistingLeaves = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, query)

            '''Dim params(0) As SqlParameter
            dsExistingLeaves = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.StoredProcedure, "[USR_SS].[GetAppliedLeaveTypeCount]", Nothing)

            For i As Integer = 0 To Me.ddMonthstatusPeriodically.Items.Count - 1
                tmpDs = Me.GetEmpLeaveCounts(AsofDate, Me.ddMonthstatusPeriodically.Items(i).Value, Eligible, Taken, Balance, True)
                '' ''demo
                Dim ActualTakenLeaves As Integer = 0
                ' ''Dim dsActualLeaves As New DataSet
                ' ''Me.GetEmpActualLeaveCountsWhileApproving(Me.h_Emp_No.Value, Me.ddMonthstatusPeriodically.Items(i).Value, dsActualLeaves)
                ' ''If Not dsActualLeaves Is Nothing AndAlso dsActualLeaves.Tables.Count > 0 AndAlso dsActualLeaves.Tables(0).Rows.Count > 0 AndAlso dsActualLeaves.Tables(0).Rows(0).Item("elt_id") = Me.ddMonthstatusPeriodically.Items(i).Value Then
                ' ''    ActualTakenLeaves = dsActualLeaves.Tables(0).Rows(0).Item("Taken")
                ' ''End If
                '' ''end demo
                If Not tmpDs Is Nothing Then
                    For Each tmpRow As DataRow In tmpDs.Tables(0).Rows
                        row = dsLeaveStatus.Tables(0).NewRow
                        row.Item("elt_id") = Me.ddMonthstatusPeriodically.Items(i).Value
                        row.Item("elt_descr") = Me.ddMonthstatusPeriodically.Items(i).Text
                        If Me.ddMonthstatusPeriodically.Items(i).Value = "AL" Then
                            row.Item("bShow_Details") = tmpRow.Item("bShowLeaveBalance")
                        Else
                            row.Item("bShow_Details") = 0 'tmpRow.Item("bShowLeaveBalance")
                        End If
                        row.Item("Tot_Taken") = tmpRow.Item("Taken") + ActualTakenLeaves
                        row.Item("Tot_Balance") = tmpRow.Item("Balance") - ActualTakenLeaves
                        If Me.ddMonthstatusPeriodically.Items(i).Value = "AL" Then
                            row.Item("bPrev_Record") = 1
                        Else
                            row.Item("bPrev_Record") = 0
                        End If
                        row.Item("elt_order") = i + 1
                        If Not dsLeaveColors Is Nothing Then
                            colorrow = dsLeaveColors.Tables(0).Select("Elt_Id='" & Me.ddMonthstatusPeriodically.Items(i).Value & "'")(0)
                            row.Item("Elt_BackColor") = colorrow.Item("Back_Color")
                            row.Item("Elt_ForeColor") = colorrow.Item("Fore_Color")
                        End If
                        dsLeaveStatus.Tables(0).Rows.Add(row)
                    Next
                End If
                dsLeaveStatus.AcceptChanges()
            Next

            If Not dsExistingLeaves Is Nothing AndAlso dsExistingLeaves.Tables(0).Rows.Count > 0 Then
                Dim tmpRows() As DataRow
                For Each row In dsLeaveStatus.Tables(0).Rows
                    tmpRows = dsExistingLeaves.Tables(0).Select("Ela_Elt_Id = '" & row.Item("Elt_Id") & "'")
                    If tmpRows.Length > 0 Then
                        row.Item("bPrev_Record") = 1
                    Else
                        row.Item("bPrev_Record") = 0
                    End If
                Next
            End If
            dsExistingLeaves.Dispose()

            If dsLeaveStatus.Tables(0).Rows.Count > 0 Then
                'If Not dsLeaveStatus Is Nothing Then
                dlLeaveStatus.DataSource = dsLeaveStatus.Tables(0)
                dlLeaveStatus.DataBind()
            Else 'fill empty data
                For i As Integer = 0 To Me.ddMonthstatusPeriodically.Items.Count - 1
                    row = dsLeaveStatus.Tables(0).NewRow
                    row.Item("elt_id") = Me.ddMonthstatusPeriodically.Items(i).Value
                    row.Item("elt_descr") = Me.ddMonthstatusPeriodically.Items(i).Text
                    If Me.ddMonthstatusPeriodically.Items(i).Value = "AL" Then
                        row.Item("bShow_Details") = 1
                    Else
                        row.Item("bShow_Details") = 0 'tmpRow.Item("bShowLeaveBalance")
                    End If
                    row.Item("Tot_Taken") = 0
                    row.Item("Tot_Balance") = 0
                    row.Item("bPrev_Record") = 1
                    row.Item("elt_order") = i + 1
                    If Not dsLeaveColors Is Nothing Then
                        colorrow = dsLeaveColors.Tables(0).Select("Elt_Id='" & Me.ddMonthstatusPeriodically.Items(i).Value & "'")(0)
                        row.Item("Elt_BackColor") = colorrow.Item("Back_Color")
                        row.Item("Elt_ForeColor") = colorrow.Item("Fore_Color")
                    End If
                    dsLeaveStatus.Tables(0).Rows.Add(row)
                Next
                dsLeaveStatus.AcceptChanges()
                dlLeaveStatus.DataSource = dsLeaveStatus.Tables(0)
                dlLeaveStatus.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = "Error getting calendar events"
        End Try
    End Sub

    Private Sub GetBsuWeekEnd()
        Dim Wk1, Wk2 As String
        Dim dr As SqlDataReader
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "BsuWeekEnd"
            params(1) = New SqlClient.SqlParameter("@Bsu_Id", SqlDbType.VarChar)
            params(1).Value = h_Bsu_Id.Value

            dr = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
            Do While dr.Read
                Wk1 = dr.Item("WeekEnd1")
                Wk2 = dr.Item("WeekEnd2")
            Loop
            If Not dr.IsClosed Then dr.Close()
            Session("BsuWeekEnd1") = Wk1
            Session("BsuWeekEnd2") = Wk2
        Catch ex As Exception
            Me.lblmsg.CssClass = "divInValidMsg"
            Me.lblmsg.Text = "Error getting week ends for the business unit."
        End Try

    End Sub

    Private Sub GetLeaveTypeColorCodes()
        Dim ds As DataSet
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim params(0) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "LeaveTypeColorCodes"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
            If Not ds Is Nothing Then
                Session("LeaveTypeColorCodes") = ds
            End If
        Catch ex As Exception
            Me.lblmsg.CssClass = "divInValidMsg"
            Me.lblmsg.Text = "Error getting week ends for the business unit."
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' ''Dim smScriptManager As New ScriptManager
        ' ''smScriptManager = Page.Master.FindControl("ScriptManager1")
        ' ''smScriptManager.RegisterPostBackControl(btnSave)

        'Me.divPopup.Visible = True

        If Page.IsPostBack = False Then

            clear_All()

            btnCancel.Attributes.Add("OnClick", "return window.parent.ResetDashBoard();")
            btnCancel.Attributes.Add("OnClick", "return window.parent.RefreshLeaves();")

            ' ''If h_Bsu_Id.Value = Nothing Then
            ' ''    h_Bsu_Id.Value = "999998"
            ' ''End If
            ' ''If Session("sUsr_name") = Nothing Then
            ' ''    Session("sUsr_name") = "Lijo"
            ' ''End If
            ' ''If Session("sUsr_id") = Nothing Then
            ' ''    Session("sUsr_id") = "lj001"
            ' ''End If

            '''trUplaod.Attributes.Add("style", "display:none")

            '''filupload.Attributes.Add("style", "display:none")
            ViewState("IdDocEnabled") = "0"

            GetLoggedInEmployee()

            Me.GetBsuWeekEnd()
            Me.GetLeaveTypeColorCodes()

            Me.LoadMonthAndYear()
            Me.LoadCalendar(True, , True, True)

            Page.Title = OASISConstants.Gemstitle
            '''txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = h_Bsu_Id.Value
            Dim USR_NAME As String = Session("sUsr_name")

            ViewState("PassElaID") = False

            'for editing demo purpose
            Dim ViewId As String
            'If Not Session("empApplyLeave_EditId") Is Nothing Then
            If Request.QueryString("Ela_Id") <> "" Then
                'ViewId = Session("empApplyLeave_EditId")
                'ViewId = Encr_decrData.Decrypt(Request.QueryString("Ela_Id").Replace(" ", "+"))
                ViewId = Replace(Request.QueryString("Ela_Id"), "'", "")
                Session.Remove("empApplyLeave_EditId")
            End If
            'ViewId = "4287"
            If Request.QueryString("viewid") <> "" Then
                ViewId = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            'ViewId = Request.QueryString("viewid").Replace(" ", "+")
            'If Request.QueryString("viewid") <> "" Then

            If ViewState("datamode") = "view" Then
                lblView.Attributes.Add("style", "display:block;")
                imgDoc.Attributes.Add("style", "display:block;")
                imgDelete.Attributes.Add("style", "display:block;")
            Else
                lblView.Attributes.Add("style", "display:none;")
                imgDoc.Attributes.Add("style", "display:none;")
                imgDelete.Attributes.Add("style", "display:none;")
            End If

            If ViewId <> "" Then
                Me.btnSavePopUp.Visible = False
                'GetLoggedInEmployee()
                Me.bindLeaveTypes()
                setViewData()
                'IsPlannerEnabled()
                'setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                setModifyHeader(ViewId)
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                'Me.ddMonthstatusPeriodically.Items(3).Selected = True
                ResetViewData()
                'GetLoggedInEmployee()
                GetEmployeeDetails()
                Me.bindLeaveTypes()
                'GetEmpLeaveCounts(Now.Date, Me.ddMonthstatusPeriodically.SelectedValue)
                ddMonthstatusPeriodically.SelectedIndex = 0
                Me.ShowLeaveCalculation(True, , , False)
                Me.ShowLeaveStatus(Now.Date)
                gridbind()
            End If

            'rddlMth.ClearSelection()
            'rddlMth.FindItemByText("Dec").Selected = True
            'rddlYear.ClearSelection()
            'rddlYear.FindItemByText("2013").Selected = True
            ''bindLeave_status()
        Else

        End If
    End Sub

    Private Sub bindLeaveTypes()
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim dr As SqlDataReader
        Dim gender As String = ""
        Dim ds As DataSet

        'Dim query As String = "Select isnull(EMP_SEX_bMALE,0) as emp_sex_bmale From Employee_M Where Emp_Id = (Select Usr_emp_id From Users_m where usr_id ='" & Session("sUsr_Id") & "')"
        Try
            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "LeaveTypes"
            params(1) = New SqlClient.SqlParameter("@Usr_Id", SqlDbType.VarChar)
            params(1).Value = Session("sUsr_Id")

            'gender = SqlHelper.ExecuteScalar(connStr, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)

            'params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            'params(0).Value = "LeaveTypes"
            'params(1) = New SqlClient.SqlParameter("@Gender", SqlDbType.VarChar)
            'params(1).Value = gender

            ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.ddMonthstatusPeriodically.DataTextField = "ELT_DESCR"
                Me.ddMonthstatusPeriodically.DataValueField = "ELT_ID"
                Me.ddMonthstatusPeriodically.DataSource = ds.Tables(0)
                Me.ddMonthstatusPeriodically.DataBind()
                ' Me.ddMonthstatusPeriodically.Items.Insert(0, New ListItem("--Please Select--", "0"))

                'Me.ddMonthstatusPeriodically.DataTextField = "ELT_DESCR"
                'Me.ddMonthstatusPeriodically.DataValueField = "ELT_ID"
                'Me.ddMonthstatusPeriodically.DataSource = ds.Tables(0)
                'Me.ddMonthstatusPeriodically.DataBind()
            End If
        Catch ex As Exception
            Me.lblmsg.Text = "There was an issue loading leave types"
            Me.lblmsg.CssClass = "divInValidMsg"
        End Try
    End Sub

    Private Sub bindLeave_status()
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim DS As New DataSet
        Dim param(8) As SqlParameter
        'param(0) = New SqlParameter("@USR_ID", "lj001") ' Session("sUsr_id")
        param(0) = New SqlParameter("@USR_ID", Session("sUsr_id"))
        param(1) = New SqlParameter("@YEAR", rddlYear.SelectedValue)
        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "USR_SS.EmpLeave_status", param)
        If DS.Tables(0).Rows.Count > 0 Then
            dlLeaveStatus.DataSource = DS.Tables(0)
            dlLeaveStatus.DataBind()
        End If
    End Sub

    Protected Sub lbtnPreMth_Click(sender As Object, e As EventArgs) Handles lbtnPreMth.Click
        Me.lbtnNxtMth.Visible = True
        Me.LoadCalendar(False, DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate), True)
        Me.lbtnPreMth.Text = Left(MonthName(DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate).Month), 3)
        Me.lbtnNxtMth.Text = Left(MonthName(DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate).Month), 3)
        If Me.CalDatepicker.VisibleDate.Year = Date.Now.Year - 1 And Me.CalDatepicker.VisibleDate.Month = 1 Then
            Me.lbtnPreMth.Visible = False
        End If
        ' Dim StartDate As Date = New Date(Me.rddlYear.SelectedValue, Me.rddlMth.SelectedValue, 1)
        Dim StartDate As Date = DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate)
        Dim EndDate As Date = New Date(CDate(StartDate).Year, CDate(StartDate).Month, Date.DaysInMonth(CDate(StartDate).Year, CDate(StartDate).Month))
        Me.ShowLeaveCalculation(False, CDate(StartDate), EndDate, False)
        Me.ShowLeaveStatus(Now.Date)
    End Sub
    Protected Sub lbtnNxtMth_Click(sender As Object, e As EventArgs) Handles lbtnNxtMth.Click
        Me.lbtnPreMth.Visible = True
        Me.LoadCalendar(False, DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate), True)
        Me.lbtnPreMth.Text = Left(MonthName(DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate).Month), 3)
        Me.lbtnNxtMth.Text = Left(MonthName(DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate).Month), 3)
        If Me.CalDatepicker.VisibleDate.Year = Date.Now.Year + 1 And Me.CalDatepicker.VisibleDate.Month = 12 Then
            Me.lbtnNxtMth.Visible = False
        End If
        Dim StartDate As Date = DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate)
        Dim EndDate As Date = New Date(CDate(StartDate).Year, CDate(StartDate).Month, Date.DaysInMonth(CDate(StartDate).Year, CDate(StartDate).Month))
        Me.ShowLeaveCalculation(False, CDate(StartDate), EndDate, False)
        Me.ShowLeaveStatus(Now.Date)
    End Sub

    'Protected Sub IsLeaveExceeded()
    '    Try
    '        If h_Emp_No.Value <> "" Then
    '            Dim pParms(6) As SqlClient.SqlParameter
    '            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
    '            pParms(0).Value = h_Emp_No.Value

    '            pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
    '            pParms(1).Value = Today.Date

    '            pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
    '            pParms(2).Value = ddMonthstatusPeriodically.SelectedValue

    '            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
    '            pParms(3).Value = h_Bsu_Id.Value

    '            pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
    '            pParms(4).Value = False
    '            pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
    '            pParms(5).Value = False

    '            pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
    '            If ViewState("PassElaID") = True Then
    '                pParms(6).Value = h_ELA_ID.Value
    '            Else
    '                pParms(6).Value = DBNull.Value
    '            End If


    '            Dim intLeaveBalnce, days As Integer
    '            Dim ds As New DataSet
    '            Dim dtgrid As New DataTable
    '            Dim adpt As New SqlDataAdapter
    '            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
    '                Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
    '                cmd.CommandType = CommandType.StoredProcedure
    '                cmd.Parameters.AddRange(pParms)
    '                adpt.SelectCommand = cmd
    '                adpt.Fill(ds)
    '            End Using
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                intLeaveBalnce = ds.Tables(0).Rows(0).Item(3)
    '            End If

    '            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
    '            Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(txtFrom.Text) & "','" & CDate(txtTo.Text) & "','" & h_Bsu_Id.Value & "'," & h_Emp_No.Value & ",'" & ddMonthstatusPeriodically.SelectedValue & "') as days"
    '            Dim ds2 As New DataSet
    '            ds2 = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
    '            ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
    '            days = ds2.Tables(0).Rows(0).Item("days")

    '            If (days > intLeaveBalnce) Then
    '                chkSaveAnyway.Visible = True
    '                'lblError.Text = "Note: You have exceeded your leave balance for current year.Check ""Continue Anyway"" to save details or Cancel to re-apply"
    '                'HandleUserMessage("Note: You have exceeded your leave balance for current year. Check ""Continue Anyway"" to save details or Close to re-apply", UserMessageType.Failure)

    '                HandlePopUpUserMessage("Note: You have exceeded your leave balance for current year. Check ""Continue Anyway"" to save details or Close to re-apply", UserMessageType.Failure)

    '                'lblmsg.Text = getErrorMessage("1122")
    '            Else
    '                HandlePopUpUserMessage("", UserMessageType.None)
    '            End If

    '        End If

    '    Catch
    '    End Try

    'End Sub

    Public Shared Function SaveEMPLEAVEAPP(ByRef p_ELA_ID As String, ByVal p_ELA_BSU_ID As String, ByVal p_ELA_EMP_ID As String,
      ByVal p_ELA_ELT_ID As String, ByVal p_ELA_DTFROM As Date, ByVal p_ELA_DTTO As Date,
      ByVal p_ELA_REMARKS As String, ByVal p_ELA_APPRSTATUS As String, ByVal p_ELA_STATUS As Integer,
      ByVal p_ELA_ADDRESS As String, ByVal p_ELA_PHONE As String, ByVal p_ELA_MOBILE As String, ByVal p_ELA_Pobox As String,
      ByVal p_APS_bFORWARD As Boolean, ByVal p_Edit As Boolean, ByVal p_stTrans As SqlTransaction, ByVal p_user As String, ByVal p_handovertxt As String) As String
        Try
            Dim pParms(19) As SqlClient.SqlParameter 'V1.2 ,V1.3
            pParms(0) = Mainclass.CreateSqlParameter("@ELA_BSU_ID", p_ELA_BSU_ID, SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@ELA_EMP_ID", p_ELA_EMP_ID, SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@ELA_ELT_ID", p_ELA_ELT_ID, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@ELA_DTFROM", p_ELA_DTFROM, SqlDbType.DateTime)
            pParms(4) = Mainclass.CreateSqlParameter("@ELA_DTTO", p_ELA_DTTO, SqlDbType.DateTime)
            pParms(5) = Mainclass.CreateSqlParameter("@ELA_APPRSTATUS", p_ELA_APPRSTATUS, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@bEdit", p_Edit, SqlDbType.Bit)
            pParms(7) = Mainclass.CreateSqlParameter("@ELA_STATUS", p_ELA_STATUS, SqlDbType.Int)
            'pParms(8) = Mainclass.CreateSqlParameter("@return_value", "0", SqlDbType.Int, True)
            pParms(9) = Mainclass.CreateSqlParameter("@ELA_REMARKS", p_ELA_REMARKS, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@ELA_ID", p_ELA_ID, SqlDbType.Int, True)
            pParms(11) = Mainclass.CreateSqlParameter("@ELA_ADDRESS", p_ELA_ADDRESS, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@ELA_PHONE", p_ELA_PHONE, SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@ELA_MOBILE", p_ELA_MOBILE, SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@ELA_POBOX", p_ELA_Pobox, SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@APS_bFORWARD", p_APS_bFORWARD, SqlDbType.Bit)

            pParms(16) = Mainclass.CreateSqlParameter("@UserName", p_user, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@Handovertxt", p_handovertxt, SqlDbType.VarChar)
            pParms(18) = Mainclass.CreateSqlParameter("@RetMsg", "", SqlDbType.VarChar, True, 100)
            pParms(19) = Mainclass.CreateSqlParameter("@RetVal", "", SqlDbType.VarChar, True, 100)
            Dim retval As String
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPLEAVEAPP_V2", pParms)
            p_ELA_ID = pParms(10).Value.ToString
            SaveEMPLEAVEAPP = pParms(19).Value.ToString & "||" & pParms(18).Value.ToString
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Protected Sub getDocumentEnabledFlag(ByVal strLeaveType As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        If Not ddMonthstatusPeriodically.SelectedIndex = 0 Then
            Dim Sql_Query = "select Isnull(EAS_BDocCheck,0) EAS_BDocCheck from dbo.EMPATTENDANCE_STATUS where eas_elt_id= '" & strLeaveType & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows(0).Item("EAS_BDocCheck") = True Then
                ViewState("IdDocEnabled") = "1"
            Else
                ViewState("IdDocEnabled") = "0"
            End If
        Else
            ViewState("IdDocEnabled") = "0"
        End If
    End Sub

    Public Function InsertUploadedFile(ByVal cmd As SqlCommand, ByVal con As SqlConnection, ByVal tran As SqlTransaction) As Boolean

        'Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("conString").ConnectionString()
        'Dim strConnString As String = "Data Source=GEMSPROJECT;Initial Catalog=OASIS_DOCS;Persist Security Info=True;User ID=sa;Password=xf6mt"
        ' Dim con As New SqlConnection(strConnString)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        cmd.Transaction = tran

        Try
            'con.Open()
            cmd.ExecuteNonQuery()
            Return True

        Catch ex As Exception
            Response.Write(ex.Message)
            Return False

        Finally

            'con.Close()
            'con.Dispose()

        End Try

    End Function

    Private Function SaveDocument(ByVal fuUpload As FileUpload, ByVal con As SqlConnection, ByVal tran As SqlTransaction, ByVal PCD_ID As String) As Boolean
        Try


            Dim filePath As String = fuUpload.PostedFile.FileName
            'Dim fuUpload As FileUpload
            'filePath = System.IO.Path.GetExtension(fuUpload.FileName)

            Dim filename As String = Path.GetFileName(filePath)

            Dim ext As String = Path.GetExtension(filename)

            Dim contenttype As String = String.Empty


            Select Case ext.ToLower()
                Case ".doc"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".docx"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".xls"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".xlsx"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".jpg"

                    contenttype = "image/jpg"
                    Exit Select

                Case ".jpeg"

                    contenttype = "image/jpeg"
                    Exit Select

                Case ".tif"

                    contenttype = "image/tiff"
                    Exit Select

                Case ".png"

                    contenttype = "image/png"
                    Exit Select

                Case ".gif"

                    contenttype = "image/gif"
                    Exit Select

                Case ".pdf"

                    contenttype = "application/pdf"
                    Exit Select



            End Select

            If contenttype <> String.Empty Then

                Dim fs As Stream = fuUpload.PostedFile.InputStream
                Dim br As New BinaryReader(fs)
                Dim bytes As Byte() = br.ReadBytes(fs.Length)


                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", "LEAVE")
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bytes)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", h_Bsu_Id.Value)
                Dim bUploadfile As Boolean = InsertUploadedFile(cmd, con, tran)
                If bUploadfile Then
                    lblmsg.CssClass = "divValidMsg"
                    lblmsg.Text = "File Uploaded Successfully"
                    Return True
                Else
                    lblmsg.CssClass = "divInValidMsg"
                    lblmsg.Text = UtilityObj.getErrorMessage("991")
                    Return False
                End If

            Else
                lblmsg.CssClass = "divInValidMsg"
                lblmsg.Text = "File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Private Sub setViewData() 'setting controls on view/edit

        '''imgEmployee.Enabled = False
        txtFrom.Attributes.Add("readonly", "readonly")
        txtTo.Attributes.Add("readonly", "readonly")
        txtPhone.Attributes.Add("readonly", "readonly")
        txtMobile.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        '''txtPobox.Attributes.Add("readonly", "readonly")
        txtAddress.Attributes.Add("readonly", "readonly")
        txtHandover.Attributes.Add("readonly", "readonly")
        chkForward.Enabled = False
        ddMonthstatusPeriodically.Enabled = False

        cxT_FrmDt.Enabled = False
        cxT_FrmDtC.Enabled = False
        cxT_ToDt.Enabled = False
        cxT_ToDtC.Enabled = False
        imgBtnT_FrmDt.Enabled = False
        imgBtnT_ToDt.Enabled = False

        'CalendarExtender2.Enabled = False
        'CalendarExtender3.Enabled = False
        'DocDate.Enabled = False
        btnCancelLeave.Visible = False
        chkForward.Attributes.Add("readonly", "readonly")
        'V1.4
    End Sub

    Private Sub LoadMonthAndYear()
        Dim item As Telerik.Web.UI.DropDownListItem
        'first add months
        item = New Telerik.Web.UI.DropDownListItem("Jan", "1")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Feb", "2")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Mar", "3")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Apr", "4")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("May", "5")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Jun", "6")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Jul", "7")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Aug", "8")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Sep", "9")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Oct", "10")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Nov", "11")
        Me.rddlMth.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem("Dec", "12")
        Me.rddlMth.Items.Add(item)
        ''''now add years

        item = New Telerik.Web.UI.DropDownListItem(Now.Year - 6, Now.Year - 6)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year - 5, Now.Year - 5)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year - 4, Now.Year - 4)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year - 3, Now.Year - 3)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year - 2, Now.Year - 2)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year - 1, Now.Year - 1)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year, Now.Year)
        Me.rddlYear.Items.Add(item)
        item = New Telerik.Web.UI.DropDownListItem(Now.Year + 1, Now.Year + 1)
        Me.rddlYear.Items.Add(item)

        ''for demo purpose
        'Dim item As Telerik.Web.UI.DropDownListItem
        ''first add months
        'item = New Telerik.Web.UI.DropDownListItem("Jan", 1)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Feb", 2)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Mar", 3)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Apr", 4)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("May", 5)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Jun", 6)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Jul", 7)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Aug", 8)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Sep", 9)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Oct", 10)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Nov", 11)
        'Me.rddlMth.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem("Dec", 12)
        'Me.rddlMth.Items.Add(item)
        ''now add years
        'item = New Telerik.Web.UI.DropDownListItem(Now.Year - 2, Now.Year - 2)
        'Me.rddlYear.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem(Now.Year - 1, Now.Year - 1)
        'Me.rddlYear.Items.Add(item)
        'item = New Telerik.Web.UI.DropDownListItem(Now.Year, Now.Year)
        'Me.rddlYear.Items.Add(item)
    End Sub

    Private Sub LoadCalendar(ByVal CurrentDateAsStartDate As Boolean, Optional ByVal StartDate As Date = Nothing, Optional ByVal UpdateMonthYearDropDown As Boolean = True, Optional ByVal UpdatePreNxtMthLabels As Boolean = False)
        If CurrentDateAsStartDate = True Or StartDate = Nothing Then
            Me.CalDatepicker.VisibleDate = Now.Date
            Me.CalDatepicker.SelectedDate = Now.Date
        Else
            Me.CalDatepicker.VisibleDate = StartDate

        End If

        If UpdateMonthYearDropDown = True Then
            Me.rddlMth.SelectedValue = Me.CalDatepicker.VisibleDate.Month
            Me.rddlYear.SelectedValue = Me.CalDatepicker.VisibleDate.Year
        End If

        If UpdatePreNxtMthLabels Then
            Me.lbtnPreMth.Text = Left(MonthName(DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate).Month), 3)
            Me.lbtnNxtMth.Text = Left(MonthName(DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate).Month), 3)
        End If

    End Sub

    Public Function GetEmpLeaveCounts(ByVal AsofDate As Date, ByVal LeaveType As String, Optional ByRef EligibleDays As Decimal = Nothing, Optional ByRef TakenDays As Decimal = Nothing, Optional ByRef BalanceDays As Decimal = Nothing, Optional ByRef ShowBalance As Boolean = True) As DataSet
        Try
            If h_Emp_No.Value <> "" Then
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                pParms(0).Value = h_Emp_No.Value

                pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                pParms(1).Value = Today.Date

                '''pParms(1).Value = AsofDate

                ''' 'for demo purpose
                '''Dim dat As New Date(2013, 12, 1)
                '''pParms(1).Value = dat.Date

                pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
                '''pParms(2).Value = ddMonthstatusPeriodically.SelectedValue
                pParms(2).Value = LeaveType

                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = h_Bsu_Id.Value

                pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
                pParms(4).Value = False

                pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
                If LeaveType = "AL" Then
                    pParms(5).Value = False
                Else
                    pParms(5).Value = True
                End If



                pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                If ViewState("PassElaID") = True Then
                    'Added below If condition by vikranth on 19th Aug 2019
                    If Not h_ELA_ID.Value Is Nothing AndAlso h_ELA_ID.Value <> "" Then
                        pParms(6).Value = h_ELA_ID.Value
                    Else
                        pParms(6).Value = DBNull.Value
                    End If
                Else
                    pParms(6).Value = DBNull.Value
                End If

                Dim ds As New DataSet
                Dim dtgrid As New DataTable
                Dim adpt As New SqlDataAdapter
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddRange(pParms)
                    adpt.SelectCommand = cmd
                    adpt.Fill(ds)
                    For Each dt As DataTable In ds.Tables
                        If dt.Columns.Count > 2 Then
                            dtgrid = dt
                        End If
                    Next
                    '''rptGridViewLeave.DataSource = dtgrid
                    '''rptGridViewLeave.DataBind()
                    '''HttpContext.Current.Session("divDetail") = divDetail
                    ''' 

                    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        EligibleDays = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("Eligible"))
                        TakenDays = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("Taken"))
                        BalanceDays = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("Balance"))
                        ShowBalance = ds.Tables(0).Rows(0).Item("BShowLeaveBalance")
                    End If

                    Return ds
                    'GridViewShowDetails.Columns(2).Visible = False
                End Using
            Else
                If h_Emp_No.Value = "" Then
                    lblmsg.CssClass = "divInValidMsg"
                    lblmsg.Text = "Select an employee."
                End If
            End If
        Catch ex As Exception
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = ex.Message
        End Try
    End Function

    'Don't use this method anywhere...its just used for demo purpose. Delete it later on when updating to LIVE server
    Public Sub GetEmpActualLeaveCountsWhileApproving(ByVal EmPID As Integer, ByVal Leavetype As String, Optional ByRef ds_status As DataSet = Nothing)
        Try
            Dim rowId As String = ""

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = EmPID

            pParms(1) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
            pParms(1).Value = Leavetype

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[EmpActualLeaveStatusWhileApplying]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
            End Using

            If Not ds_status Is Nothing Then ds_status = ds.Copy

        Catch ex As Exception
            '''lblError.Text = ex.Message
            'HandleUserMessage(ex.Message, UserMessageType.Failure)
            HandleUserMessage(getErrorMessage("1000"), UserMessageType.Failure)
        End Try
    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String, Optional ByVal IsELA As Boolean = False) 'setting header data on view/edit
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            '  V1 0.2   'IsNull(EM.EMP_LASTREJOINDT, EM.EMP_JOINDT) changed to EMP_BSU_JOINDT
            str_Sql = "SELECT   ELA.ELA_ID, ELA.ID, ELA.ELA_EMP_ID," _
             & " ELA.ELA_ELT_ID,ELA.ELA_HANDOVERTXT, ELA.ELA_DTFROM, ELA.ELA_DTTO, " _
             & " ELA.ELA_REMARKS, ELA.ELA_ADDRESS, ELA.ELA_PHONE, " _
             & " ELA.ELA_MOBILE, ELA.ELA_POBOX,ELA.ELA_APPRSTATUS,isnull(EM.emp_fname,'')+'  '+isnull(EM.emp_mname,'')+'  '+isnull(EM.emp_lname,'') as EMP_NAME,IsNull(EM.EMP_BSU_JOINDT, EM.EMP_JOINDT) EMP_LASTREJOINDT,ELA_bFORWARD,IsNull(EM.EMP_ECT_ID,0) EMP_ECT_ID, CASE ELA_bFORWARD WHEN 0 THEN 'yes' else 'no' end as canedit,Isnull(FD.DOC_ID,0) DOC_ID ," _
             & " isnull(ELA_bReqCancellation,0) ReqCanc,case when isnull(ELA_APPRSTATUS,'')='C' then 1 else 0 end as IsCancelled   " _
             & " FROM VW_EmployeeLeaveApp AS ELA INNER JOIN" _
             & " EMPLOYEE_M AS EM ON ELA.ELA_EMP_ID = EM.EMP_ID left outer join OASIS_DOCS.FIN.FDOCUMENTS FD ON " _
             & "ELA.ELA_ID=FD.DOC_PCD_ID AND FD.DOC_TYPE='LEAVE' "

            'Dim tmp As Integer
            'Dim params(1) As SqlParameter
            'params(0) = New SqlClient.SqlParameter("@Modify_Id", SqlDbType.VarChar)
            'params(0).Value = p_Modifyid
            'params(1) = New SqlClient.SqlParameter("@Is_Ela", SqlDbType.Int)
            'If IsELA = True Then
            '    tmp = 1
            'Else
            '    tmp = 0
            'End If
            'params(1).Value = tmp

            If IsELA Then
                str_Sql &= " WHERE ELA.ELA_ID='" & p_Modifyid & "' "
            Else
                str_Sql &= " WHERE ELA.ID='" & p_Modifyid & "' "
            End If

            ' str_Sql &= " WHERE ELA.ELA_ID='" & p_Modifyid & "' "

            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ViewState("ReqCanc") = "no"
            ViewState("IsCancelled") = "no"
            ViewState("IsForwarded") = "no"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[USR_SS].[GetLeaveDetail]", params)
            If ds.Tables(0).Rows.Count > 0 Then
                'h_ID.Value = ds.Tables(0).Rows(0)("ID").ToString
                h_ELA_ID.Value = ds.Tables(0).Rows(0)("ELA_ID")
                h_DOC_PCD_ID.Value = ds.Tables(0).Rows(0)("DOC_ID") 'V1.4
                h_Emp_No.Value = ds.Tables(0).Rows(0)("ELA_EMP_ID")
                Me.ddMonthstatusPeriodically.SelectedValue = ds.Tables(0).Rows(0)("ELA_ELT_ID")
                'txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                txtMobile.Text = ds.Tables(0).Rows(0)("ELA_MOBILE").ToString
                txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                txtRemarks.Text = ds.Tables(0).Rows(0)("ELA_REMARKS").ToString
                txtJoinDate.Text = Format(CDate(ds.Tables(0).Rows(0)("EMP_LASTREJOINDT")), "dd/MMM/yyyy").ToString   'V1.1
                ' txtPobox.Text = ds.Tables(0).Rows(0)("ELA_POBOX").ToString
                txtAddress.Text = ds.Tables(0).Rows(0)("ELA_ADDRESS").ToString
                txtHandover.Text = ds.Tables(0).Rows(0)("ELA_HANDOVERTXT").ToString 'V1.1
                txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("ELA_DTFROM")), "dd/MMM/yyyy")
                txtTo.Text = Format(CDate(ds.Tables(0).Rows(0)("ELA_DTTO")), "dd/MMM/yyyy")
                ViewState("canedit") = ds.Tables(0).Rows(0)("canedit")
                ViewState("BLS_ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID") 'V1.1
                ddMonthstatusPeriodically.SelectedValue = ds.Tables(0).Rows(0)("ELA_ELT_ID")
                ViewState("canedit") = ds.Tables(0).Rows(0)("canedit")
                'V1.4
                ViewState("ReqCanc") = IIf(ds.Tables(0).Rows(0)("ReqCanc"), "yes", "no")
                ViewState("IsCancelled") = IIf(ds.Tables(0).Rows(0)("IsCancelled"), "yes", "no")
                ViewState("IsForwarded") = IIf(ds.Tables(0).Rows(0)("ELA_bFORWARD"), "yes", "no")
                If (txtAddress.Text = "" And txtMobile.Text = "" And txtPhone.Text = "") Then
                    'ImageButton3_Click(Nothing, Nothing)
                    Me.GetEmployeeDetails()
                End If
                If ViewState("canedit") = "yes" Then
                    chkForward.Checked = False
                Else
                    chkForward.Checked = True
                End If
                If ViewState("IsForwarded") = "yes" Then
                    btnEdit.Attributes.Add("OnClick", "return confirm_Edit();")
                    'btnEdit.Attributes.Add("OnClick", "return test();")
                Else
                    btnEdit.Attributes.Remove("OnClick")
                End If
                If ds.Tables(0).Rows(0)("ELA_APPRSTATUS").ToString = "A" Then
                    btnEdit.Visible = True
                    btnDelete.Visible = True
                    btnPrintLeave.Visible = True
                Else
                    btnPrintLeave.Visible = False




                    If (ViewState("ReqCanc") = "yes" Or ViewState("IsCancelled") = "yes") Then
                        ' ''btnEdit.Enabled = False
                        ' ''btnCancelLeave.Enabled = False
                        ' ''btnDelete.Enabled = False
                        ' ''btnSavePopUp.Enabled = False
                        btnEdit.Visible = False
                        btnCancelLeave.Visible = False
                        btnDelete.Visible = False
                        btnSavePopUp.Visible = False
                    Else
                        ' ''btnEdit.Enabled = True
                        ' ''btnCancelLeave.Enabled = True
                        ' ''btnDelete.Enabled = True
                        ' ''btnSavePopUp.Enabled = True
                        btnEdit.Visible = True
                        btnCancelLeave.Visible = True
                        btnDelete.Visible = True
                        'btnSavePopUp.Visible = True
                    End If

                    If ViewState("ReqCanc") = "yes" Then
                        'lblmsg.CssClass = "divErrorMsg"
                        'lblmsg.Text = "Requested for cancellation of leave application."
                        lblCancelMsg.Text = "Requested for cancellation of leave application."
                    End If

                    If ViewState("IsCancelled") = "yes" Then
                        'lblmsg.CssClass = "divValidMsg"
                        'lblmsg.Text = "Leave application cancelled."
                        lblCancelMsg.Text = "Leave application cancelled."
                    End If
                End If
                getDocumentEnabledFlag(ds.Tables(0).Rows(0)("ELA_ELT_ID").ToString)
                If h_DOC_PCD_ID.Value <> "" And h_DOC_PCD_ID.Value <> "0" And ViewState("IdDocEnabled") = "1" Then
                    imgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(h_DOC_PCD_ID.Value)
                    ' ''trUplaod.Attributes.Remove("style")
                    ' ''trUplaod.Attributes.Add("style", "display:inline")
                    trUplaod.Visible = True
                    'filupload.Visible = False
                    ' ''filupload.Attributes.Remove("style")
                    ' ''filupload.Attributes.Add("style", "display:none")
                    ' ''lblView.Visible = True
                    ' ''imgDoc.Visible = True
                ElseIf ViewState("IdDocEnabled") = "1" And h_DOC_PCD_ID.Value = "0" Then
                    ' ''trUplaod.Attributes.Remove("style")
                    ' ''trUplaod.Attributes.Add("style", "display:inline")
                    trUplaod.Visible = True
                    ' filupload.Visible = True
                    ' ''filupload.Attributes.Remove("style")
                    ' ''filupload.Attributes.Add("style", "display:inline")
                    ' ''lblView.Visible = False
                    ' ''imgDoc.Visible = False
                Else
                    ' ''trUplaod.Attributes.Remove("style")
                    ' ''trUplaod.Attributes.Add("style", "display:none")
                    trUplaod.Visible = False
                    ' filupload.Visible = False
                    'lblView.Visible = False
                    'imgDoc.Visible = False

                End If
            Else
                ViewState("canedit") = "no"
            End If

            btnDelete.Attributes.Add("OnClick", "return confirm_Delete();")
            btnCancelLeave.Attributes.Add("OnClick", "return confirm_Cancellation();")

            'GetEmpLeaveCounts(Now.Date, Me.ddMonthstatusPeriodically.SelectedValue)
            Me.ShowLeaveCalculation(False, CDate(Me.txtFrom.Text), CDate(Me.txtTo.Text), True)
            Me.ShowLeaveStatus(CDate(Me.txtFrom.Text))

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GetEmployeeDetails()
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As New DataSet()
            ' ''Dim pParms(2) As SqlClient.SqlParameter
            ' ''pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            ' ''pParms(0).Value = h_Bsu_Id.Value.ToString

            ' ''pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            ' ''pParms(1).Value = h_Emp_No.Value

            Dim params(2) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "EmpLeaveAppDetails"
            params(1) = New SqlClient.SqlParameter("@Emp_Id", SqlDbType.VarChar)
            params(1).Value = h_Emp_No.Value
            params(2) = New SqlClient.SqlParameter("@Bsu_Id", SqlDbType.VarChar)
            params(2).Value = h_Bsu_Id.Value.ToString

            '''ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getEmpLeaveAppDetails", pParms)

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)

            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtJoinDate.Text = Format(ds.Tables(0).Rows(0)("EMP_LASTREJOINDT"), "dd/MMM/yyyy")
                txtPhone.Text = ds.Tables(0).Rows(0)("EMD_PERM_PHONE").ToString
                '''txtPobox.Text = ds.Tables(0).Rows(0)("EMD_PERM_POBOX").ToString
                txtAddress.Text = ds.Tables(0).Rows(0)("EMD_PERM_ADDRESS").ToString
                txtMobile.Text = ds.Tables(0).Rows(0)("EMD_PERM_MOBILE").ToString
                ' ''If txtLRejoindt.Text = "01/Jan/1900" Then
                ' ''    txtLRejoindt.Text = ""
                ' ''End If
                ViewState("BLS_ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID") 'V1.1
            Else
                '''txtLRejoindt.Text = ""
            End If
            'GetEmpLeaveCounts()
        Catch ex As Exception
            Errorlog(ex.Message)
            '''txtLRejoindt.Text = ""
        End Try
    End Sub

    Private Sub ShowCalendarEvents(ByVal FromDate As Date, ByVal ToDate As DateTime)
        Try
            'First render holidays
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As DataSet
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = h_Bsu_Id.Value.ToString

            pParms(1) = New SqlClient.SqlParameter("@ECT_ID", SqlDbType.Int)
            pParms(1).Value = ViewState("BLS_ECT_ID")

            pParms(2) = New SqlClient.SqlParameter("@From_Date", SqlDbType.Date)
            pParms(2).Value = FromDate

            pParms(3) = New SqlClient.SqlParameter("@To_Date", SqlDbType.Date)
            pParms(3).Value = ToDate

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[EmpLeave_CalendarEvents_Holidays]", pParms)

            Dim Hol_Back_Color, Hol_Fore_Color As String
            Dim ColorRows() As DataRow
            Dim ColorIndex, TotalColors As Integer
            If Not ds Is Nothing And Not Session("LeaveTypeColorCodes") Is Nothing Then
                'Dim tmpRows() As DataRow
                With CType(Session("LeaveTypeColorCodes"), DataSet)
                    ColorRows = .Tables(0).Select("ELT_ID = 'HOL_STAFF'")
                    If ColorRows.Length > 0 Then
                        Hol_Back_Color = ColorRows(0).Item("Back_Color")
                        Hol_Fore_Color = ColorRows(0).Item("Fore_Color")
                    End If
                End With
            End If

            ColorIndex = 0
            TotalColors = ColorRows.Length - 1

            '           <div class="calEventBoxMain" >
            '                    <div class="calEventBoxSub" >
            '            <div style="background-color:#ee541e;" class="calEventColor"></div><div 
            '  class="calEventName">U.A.E National Day</div><div  class="calEventDay">1 Day</div></div>
            '<span class="calEventDate">Date : 02/Dec/2013</span>
            '    </div>
            Dim KeepGeneratingRandomColors As Boolean
            If Hol_Back_Color = "RAND" Then
                KeepGeneratingRandomColors = True
            End If
            Dim tmpStr As String = ""
            Randomize()
            For Each row As DataRow In ds.Tables(0).Rows
                If KeepGeneratingRandomColors Then
                    'Hol_Back_Color = "#ee541e"

                    Dim rand As New Random()

                    'Hol_Back_Color = [String].Format("#{0:X6}", rand.[Next](&H1000000))
                    '"#A197B9"
                    Hol_Back_Color = "#A" & rand.Next(50, 254) & "B" & rand.Next(1, 9)
                End If

                Hol_Back_Color = ColorRows(ColorIndex).Item("Back_Color")
                ColorIndex += 1
                If ColorIndex >= TotalColors Then
                    ColorIndex = 0
                End If

                tmpStr &= "<div class=" & Chr(34) & "calEventBoxMain" & Chr(34) & ">"
                tmpStr &= "<div class=" & Chr(34) & "calEventBoxSub" & Chr(34) & ">"
                tmpStr &= "<div style=" & Chr(34) & "background-color:" & Hol_Back_Color & ";" & Chr(34) & " class=" & Chr(34) & "calEventColor" & Chr(34) & "></div> <div "
                tmpStr &= " class=" & Chr(34) & "calEventName" & Chr(34) & ">" & row.Item("Mod_Remarks") & "</div><div  class=" & Chr(34) & "calEventDay" & Chr(34) & ">" & IIf(row.Item("Days") <= 1, "1 Day", Convert.ToInt32(row.Item("Days")) & " Days") & "</div></div>"
                tmpStr &= "<span class=" & Chr(34) & "calEventDate" & Chr(34) & ">Date : " & Format(row.Item("Mod_Date"), "dd/MMM/yy") & "</span>"
                tmpStr &= "</div>"
            Next

            'Now render applied leaves
            'Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            'Dim ds As DataSet
            'Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = h_Bsu_Id.Value.ToString

            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = Me.h_Emp_No.Value

            pParms(2) = New SqlClient.SqlParameter("@From_Date", SqlDbType.Date)
            pParms(2).Value = FromDate

            pParms(3) = New SqlClient.SqlParameter("@To_Date", SqlDbType.Date)
            pParms(3).Value = ToDate

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[EmpLeave_CalendarEvents_AppliedLeaves]", pParms)
            Dim back_color As String = "#FF0000"
            If Not ds Is Nothing And Not Session("LeaveTypeColorCodes") Is Nothing Then
                Dim tmpRows() As DataRow
                For Each row As DataRow In ds.Tables(0).Rows
                    tmpRows = CType(Session("LeaveTypeColorCodes"), DataSet).Tables(0).Select("elt_id='" & row.Item("ela_elt_id") & "'")
                    If tmpRows.Length > 0 Then
                        back_color = tmpRows(0).Item("Back_Color")
                    End If
                    tmpStr &= "<div class=" & Chr(34) & "calEventBoxMain" & Chr(34) & ">"
                    tmpStr &= "<div class=" & Chr(34) & "calEventBoxSub" & Chr(34) & ">"
                    tmpStr &= "<div style=" & Chr(34) & "background-color:" & back_color & ";" & Chr(34) & " class=" & Chr(34) & "calEventColor" & Chr(34) & "></div> <div "
                    tmpStr &= " class=" & Chr(34) & "calEventName" & Chr(34) & ">" & row.Item("elt_descr") & "</div><div  class=" & Chr(34) & "calEventDay" & Chr(34) & ">" & "  (" & IIf(row.Item("ela_leavedays") <= 1, "1 Day", row.Item("ela_leavedays") & " Days") & ")</div></div>"
                    tmpStr &= "<span class=" & Chr(34) & "calEventDate" & Chr(34) & ">Date : " & Format(row.Item("ela_dtfrom"), "dd/MMM/yy") & " to " & Format(row.Item("ela_dtto"), "dd/MMM/yy") & "</span>"
                    tmpStr &= "</div>"
                Next
            End If

            Me.LiteralCalendarEvents.Text = tmpStr
            tmpStr = ""

        Catch ex As Exception
            Errorlog(ex.Message)
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = "Error getting calendar events"
        End Try
    End Sub
    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As New DataSet()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = h_Bsu_Id.Value.ToString

            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = h_Emp_No.Value

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getEmpLeaveAppDetails", pParms)
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                '''txtLRejoindt.Text = Format(ds.Tables(0).Rows(0)("EMP_LASTREJOINDT"), "dd/MMM/yyyy")
                txtPhone.Text = ds.Tables(0).Rows(0)("EMD_PERM_PHONE").ToString
                '''txtPobox.Text = ds.Tables(0).Rows(0)("EMD_PERM_POBOX").ToString
                txtAddress.Text = ds.Tables(0).Rows(0)("EMD_PERM_ADDRESS").ToString
                txtMobile.Text = ds.Tables(0).Rows(0)("EMD_PERM_MOBILE").ToString
                ' ''If txtLRejoindt.Text = "01/Jan/1900" Then
                ' ''    txtLRejoindt.Text = ""
                ' ''End If
                ViewState("BLS_ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID") 'V1.1
            Else
                '''txtLRejoindt.Text = ""
            End If
            'GetEmpLeaveCounts()
        Catch ex As Exception
            Errorlog(ex.Message)
            '''txtLRejoindt.Text = ""
        End Try
    End Sub
    Public Sub GetLoggedInEmployee()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As New DataSet
        'V1.4
        Dim params(1) As SqlParameter
        params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
        params(0).Value = "LoggedInEmployee"
        params(1) = New SqlClient.SqlParameter("@Usr_Name", SqlDbType.VarChar)
        params(1).Value = Session("sUsr_name")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
        If ds.Tables(0).Rows.Count > 0 Then
            h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
            h_Bsu_Id.Value = ds.Tables(0).Rows(0)("EMP_Bsu_ID")
            ImageButton3_Click(Nothing, Nothing)
        End If
    End Sub
    Sub clear_All()
        h_Emp_No.Value = ""
        h_ELA_ID.Value = ""
        '''txtEmpNo.Text = ""
        txtMobile.Text = ""
        txtPhone.Text = ""
        txtRemarks.Text = ""
        '''txtLRejoindt.Text = ""
        '''txtPobox.Text = ""
        txtAddress.Text = ""
        '''txtFrom.Text = GetDiplayDate()
        txtTo.Text = ""
        txtHandover.Text = ""
        chkSaveAnyway.Visible = False
        chkSaveAnyway.Checked = False
        GetLoggedInEmployee()
        ddMonthstatusPeriodically.SelectedIndex = 0
        ' ''trUplaod.Attributes.Remove("style")
        ' ''trUplaod.Attributes.Add("style", "display:none")
        ' ''filupload.Attributes.Remove("style")
        ' ''filupload.Attributes.Add("style", "display:none")
        ' ''imgDoc.Visible = False
        ' ''imgDelete.Visible = False
        h_DOC_PCD_ID.Value = ""
        '''lblView.Visible = False
        Me.LiteralLeaveCalculation.Text = Nothing
        Me.LiteralCalendarEvents.Text = Nothing
        If Not Session("LeaveMatrix") Is Nothing Then
            Session("LeaveMatrix") = Nothing
        End If
    End Sub
    Protected Sub btnSavePopUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePopUp.Click
        If Not ValidateData() Then Exit Sub
        'Me.divPopup.Visible = True
        Me.chkForward.Checked = True
        'Me.DemoSave()
        Me.SaveLeaveApplication()
    End Sub
    Private Sub SaveLeaveApplication()
        ' ''Page.Validate()
        ' ''If Not Page.IsValid Then
        ' ''    Exit Sub
        ' ''End If

        If Not ValidateData() Then Exit Sub

        Dim strfDate As String = txtFrom.Text.Trim
        Dim strtDate As String = txtTo.Text.Trim
        Dim str_err As String = DateFunctions.checkdates_canfuture(strfDate, strtDate)
        If str_err <> "" Then
            HandleUserMessage(str_err, UserMessageType.Failure)
            Me.divPopup.Visible = False
            Exit Sub
        End If
        txtFrom.Text = strfDate
        txtTo.Text = strtDate
        If txtRemarks.Text.Length > 500 Then
            HandleUserMessage("Details cannot be more than 500 characters.", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Exit Sub
        End If
        ' ''If ddMonthstatusPeriodically.SelectedIndex = 0 Then
        ' ''    lblmsg.CssClass = "divInValidMsg"
        ' ''    lblmsg.Text = "Please select a leave type."
        ' ''    Exit Sub
        ' ''End If
        If txtRemarks.Text.Trim = "" Then
            HandleUserMessage("Details Cannot be empty", UserMessageType.Failure)
            Me.divPopup.Visible = False
            Exit Sub
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim ela_id, str_mode As String
            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                str_mode = "Edit"
                ela_id = h_ELA_ID.Value ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                str_mode = "Insert"
                ela_id = 0
            End If
            Dim retval As String = "1000"
            Dim retMsg As String = ""
            Dim bIsLeaveExceeded As Boolean = False

            If ddMonthstatusPeriodically.SelectedItem.Value = "AL" Then
                bIsLeaveExceeded = IsLeaveExceeded()
            Else
                chkSaveAnyway.Visible = False
            End If

            If bIsLeaveExceeded Then
                retval = 2580
                HandleUserMessage(getErrorMessage(retval), UserMessageType.Failure)
            Else
                retval = 0
            End If


            'If chkSaveAnyway.Visible = True And chkSaveAnyway.Checked = False Then
            '    retval = 1000
            'Else
            '    retval = 0
            'End If
            If retval = 0 Then
                retval = SaveEMPLEAVEAPP(ela_id, h_Bsu_Id.Value, h_Emp_No.Value, ddMonthstatusPeriodically.SelectedItem.Value,
                txtFrom.Text.Trim, txtTo.Text.Trim, txtRemarks.Text.Trim, "N", 0, txtAddress.Text.Trim, txtPhone.Text.Trim,
                txtMobile.Text.Trim, "0", chkForward.Checked, edit_bool, stTrans, Session("sUsr_name").ToString, txtHandover.Text)   'V1.1
                Dim separator As String() = New String() {"||"}
                Dim myValarr() As String = retval.Split(separator, StringSplitOptions.None)
                retval = myValarr(0)
                If myValarr.Length > 1 Then
                    retMsg = myValarr(1)
                Else
                    retMsg = ""
                End If
                If retval = "0" Then
                    getDocumentEnabledFlag(ddMonthstatusPeriodically.SelectedValue)

                    If ViewState("IdDocEnabled") = "1" Then

                        If filupload.HasFile Then
                            If Not (SaveDocument(filupload, objConn, stTrans, ela_id)) Then
                                retval = 991
                            End If
                        End If


                    End If

                End If
                If retval = "0" Then
                    stTrans.Commit()

                    'V1.3 starts
                    Dim str_Sql As String
                    Dim ds As New DataSet
                    If chkForward.Checked = True Then
                        If ela_id = 0 And chkForward.Checked = True Then
                            str_Sql = "SELECT min(APS_ID) FROM APPROVAL_S where aps_doc_id in (select max(ela_id) from empleaveapp ) and aps_doctype='leave' "
                        Else
                            str_Sql = "SELECT min(APS_ID) FROM APPROVAL_S where aps_doc_id=" & ela_id & "and aps_doctype='leave' "
                        End If
                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                        If ds.Tables(0).Rows.Count > 0 Then
                            ela_id = ds.Tables(0).Rows(0).Item(0).ToString
                        End If
                    End If

                    'V1.3 ends
                    Dim flagAudit As Integer
                    ' ''Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, _
                    ' ''h_Emp_No.Value & "-" & ddMonthstatusPeriodically.SelectedItem.Value & "-" & _
                    ' ''txtFrom.Text.Trim & "-" & txtTo.Text.Trim & "-" & txtRemarks.Text.Trim, _
                    ' ''str_mode, Page.User.Identity.Name.ToString, Me.Page)
                    If retMsg <> " " Then
                        HandleUserMessage(retMsg + " - " + getErrorMessage("0"), UserMessageType.Success)
                        Me.divPopup.Visible = False
                        retval = 0
                    End If
                    If flagAudit <> 0 Then
                        Me.divPopup.Visible = False
                        Throw New ArgumentException("Could not process your request")
                    End If
                    Me.divPopup.Visible = False
                    HandleUserMessage(getErrorMessage("0"), UserMessageType.Success)
                    chkSaveAnyway.Visible = False
                    setViewData()
                    setModifyHeader(ela_id, True)
                    ViewState("datamode") = "view"
                    '''Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    clear_All()
                    '''btnCancel.Visible = False
                    btnCancelLeave.Visible = False
                    'Response.Redirect(Request.RawUrl)
                Else
                    Me.divPopup.Visible = False
                    stTrans.Rollback()
                    HandleUserMessage(getErrorMessage(retval) + "-" + retMsg, UserMessageType.Failure)
                End If
            End If
        Catch ex As Exception
            Me.divPopup.Visible = False
            stTrans.Rollback()
            Errorlog(ex.Message)
            'HandleUserMessage(ex.Message, UserMessageType.Failure)
            HandleUserMessage(getErrorMessage("1000"), UserMessageType.Failure)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            Me.ShowLeaveCalculation(True, , , False)
            Me.ShowLeaveStatus(Now.Date)
            gridbind()
        End Try
    End Sub
    Protected Function IsLeaveExceeded() As Boolean

        Try
            If h_Emp_No.Value <> "" Then
                Dim pParms(7) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                pParms(0).Value = h_Emp_No.Value

                pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                pParms(1).Value = Today.Date

                pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
                pParms(2).Value = ddMonthstatusPeriodically.SelectedValue

                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = Session("sBsuid")

                pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
                pParms(4).Value = False
                pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
                pParms(5).Value = False

                pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                If ViewState("PassElaID") = True Then
                    'Added below If condition by vikranth on 19th Aug 2019
                    If Not h_ELA_ID.Value Is Nothing AndAlso h_ELA_ID.Value <> "" Then
                        pParms(6).Value = h_ELA_ID.Value
                    Else
                        pParms(6).Value = DBNull.Value
                    End If
                Else
                    pParms(6).Value = DBNull.Value
                End If

                pParms(7) = New SqlClient.SqlParameter("@ELA_FROMDT", SqlDbType.DateTime)
                pParms(7).Value = txtFrom.Text.Trim



                Dim intLeaveBalnce, days As Integer
                Dim ds As New DataSet
                Dim dtgrid As New DataTable
                Dim adpt As New SqlDataAdapter
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddRange(pParms)
                    adpt.SelectCommand = cmd
                    adpt.Fill(ds)
                End Using
                If ds.Tables(0).Rows.Count > 0 Then
                    intLeaveBalnce = ds.Tables(0).Rows(0).Item(3)
                End If

                Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(txtFrom.Text) & "','" & CDate(txtTo.Text) & "','" & Session("sBsuid") & "'," & h_Emp_No.Value & ",'" & ddMonthstatusPeriodically.SelectedValue & "') as days"
                Dim ds2 As New DataSet
                ds2 = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
                ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
                days = ds2.Tables(0).Rows(0).Item("days")

                If (days > intLeaveBalnce) Then
                    IsLeaveExceeded = True
                    ' chkSaveAnyway.Visible = True
                    'lblError.Text = "Note: You have exceeded your leave balance for current year.Check ""Continue Anyway"" to save details or Cancel to re-apply"
                    'lblError.Text = getErrorMessage("1122")
                Else
                    IsLeaveExceeded = False
                End If


            End If

        Catch
        End Try

    End Function
    Private Sub DemoSave()
        Try
            Dim i As Integer = 1
            Dim j As Integer = 0
            Dim k As Integer
            k = 1 / j
        Catch ex As Exception
            Me.divPopup.Visible = False
            HandleUserMessage(ex.Message, UserMessageType.Failure)
        End Try

    End Sub
    Private Sub DeleteFileFromDB(ByVal DocId As String)
        Try

            Dim conn As String = ""

            conn = ConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString

            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("DeleteDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)
            cmd.Parameters.Add("@retVal", SqlDbType.BigInt)
            cmd.Parameters("@retVal").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            objcon.Close()
            Dim ret As Integer = 0
            ret = cmd.Parameters("@retVal").Value

            If ret <> 0 Then
                lblmsg.CssClass = "divInValidMsg"
                lblmsg.Text = UtilityObj.getErrorMessage("1000")
            Else
                lblmsg.CssClass = "divValidMsg"
                lblmsg.Text = "Document removed successfully."
                imgDelete.Visible = False
                imgDoc.Visible = False
                lblView.Visible = False
            End If

        Catch ex As Exception
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = UtilityObj.getErrorMessage("1000")
        End Try

    End Sub
    Protected Sub imgDelete_Click(sender As Object, e As ImageClickEventArgs) Handles imgDelete.Click
        DeleteFileFromDB(h_DOC_PCD_ID.Value)
        'filupload.Visible = True
        filupload.Attributes.Remove("style")
        filupload.Attributes.Add("style", "inline")
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If ViewState("canedit") = "no" Then
            HandleUserMessage("Forwarded applications cannot be deleted.", UserMessageType.Failure)
        Else

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = h_ELA_ID.Value ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

            pParms(1) = New SqlClient.SqlParameter("@retVal", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer = 0

            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim stTrans As SqlTransaction = conn.BeginTransaction

            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[DeleteEmpLeaveApp]", pParms)
            If pParms(1).Value = 0 Then
                '''lblmsg.CssClass = "divValidMsg"
                '''lblmsg.Text = UtilityObj.getErrorMessage("0")
                Me.HandleUserMessage("Leave application successfully deleted.", UserMessageType.Success)

                stTrans.Commit()
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, "Leave Application deleting", _
                ' "Delete", Page.User.Identity.Name.ToString, Me.Page)
                Me.btnSavePopUp.Visible = False
                Me.btnEdit.Visible = False
                Me.btnDelete.Visible = False
                Me.btnDelete.Visible = False
                Me.btnCancelLeave.Visible = False
                Me.btnCancel.Visible = True
                clear_All()
            Else
                'lblmsg.Text = UtilityObj.getErrorMessage("retval")
                HandleUserMessage(UtilityObj.getErrorMessage("retval"), UserMessageType.Failure)
                stTrans.Rollback()
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'btnCancel.Attributes.Add("OnClick", "return window.parent.RefreshLeaves();")
        Else
            ' ''clear_All()
            ' ''System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "CloseLeaveApplicationWindow", "window.parent.ResetDashBoard();", True)
        End If

    End Sub
    Protected Sub btnPrintLeave_Click(sender As Object, e As EventArgs) Handles btnPrintLeave.Click
        Try
            Dim printId As String = ""
            printId = Replace(h_ELA_ID.Value, "A", "")
            Dim params As New Hashtable
            params.Add("@IMG_BSU_ID", h_Bsu_Id.Value)
            params.Add("@IMG_TYPE", "LOGO")
            params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
            params.Add("@ELA_ID", CInt(printId))
            params.Add("@DocType", "LEAVE")
            ' params("VoucherName") = labHead.Text.ToUpper().ToString()   'V1.1
            params("reportHeading") = "LEAVE APPLICATION FORM"
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "Oasis"
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                '''.reportPath = Server.MapPath("../Payroll/Reports/Rpt/rptLeaveAppForm.rpt")
                .reportPath = Server.MapPath("Reports/Rpt/rptLeaveAppForm.rpt")
                'End If
            End With
            Session("rptClass") = rptClass
            ' ''If Session("sModule") = "SS" Then
            ' ''    Response.Redirect("~/Reports/ASPX Report/rptReportViewerSS.aspx")
            ' ''Else
            ' ''    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ' ''End If

            '   Response.Redirect("~/Reports/ASPX Report/rptReportViewerSS.aspx")
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblmsg.CssClass = "divInValidMsg"
            lblmsg.Text = "Request cannot be processed"
        End Try
    End Sub
    Protected Sub rddlMth_SelectedIndexChanged(sender As Object, e As DropDownListEventArgs) Handles rddlMth.SelectedIndexChanged
        Me.LoadCalendar(False, New Date(Me.rddlYear.SelectedValue, Me.rddlMth.SelectedValue, 1), False, True)
        Me.lbtnPreMth.Text = Left(MonthName(DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate).Month), 3)
        Me.lbtnNxtMth.Text = Left(MonthName(DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate).Month), 3)
        If Me.CalDatepicker.VisibleDate.Year = Date.Now.Year - 1 And Me.CalDatepicker.VisibleDate.Month = 1 Then
            Me.lbtnPreMth.Visible = False
        End If
        If Me.CalDatepicker.VisibleDate.Year = Date.Now.Year + 1 And Me.CalDatepicker.VisibleDate.Month = 12 Then
            Me.lbtnNxtMth.Visible = False
        End If
        Dim StartDate As Date = New Date(Me.rddlYear.SelectedValue, Me.rddlMth.SelectedValue, 1)
        Dim EndDate As Date = New Date(CDate(StartDate).Year, CDate(StartDate).Month, Date.DaysInMonth(CDate(StartDate).Year, CDate(StartDate).Month))
        Me.ShowLeaveCalculation(False, CDate(StartDate), EndDate, False)
        Me.ShowLeaveStatus(Now.Date)
    End Sub
    Protected Sub rddlYear_SelectedIndexChanged(sender As Object, e As DropDownListEventArgs) Handles rddlYear.SelectedIndexChanged
        Me.LoadCalendar(False, New Date(Me.rddlYear.SelectedValue, Me.rddlMth.SelectedValue, 1), False, True)
        Me.lbtnPreMth.Text = Left(MonthName(DateAdd(DateInterval.Month, -1, Me.CalDatepicker.VisibleDate).Month), 3)
        Me.lbtnNxtMth.Text = Left(MonthName(DateAdd(DateInterval.Month, 1, Me.CalDatepicker.VisibleDate).Month), 3)
        If Me.CalDatepicker.VisibleDate.Year = Date.Now.Year - 1 And Me.CalDatepicker.VisibleDate.Month = 1 Then
            Me.lbtnPreMth.Visible = False
        End If
        If Me.CalDatepicker.VisibleDate.Year = Date.Now.Year + 1 And Me.CalDatepicker.VisibleDate.Month = 12 Then
            Me.lbtnNxtMth.Visible = False
        End If
        Dim StartDate As Date = New Date(Me.rddlYear.SelectedValue, Me.rddlMth.SelectedValue, 1)
        Dim EndDate As Date = New Date(CDate(StartDate).Year, CDate(StartDate).Month, Date.DaysInMonth(CDate(StartDate).Year, CDate(StartDate).Month))
        Me.ShowLeaveCalculation(False, CDate(StartDate), EndDate, False)
        Me.ShowLeaveStatus(Now.Date)
    End Sub
    Protected Sub calDatepicker_DayRender(sender As Object, e As DayRenderEventArgs) Handles CalDatepicker.DayRender
        Dim ds As DataSet
        If Not Session("LeaveMatrix") Is Nothing Then
            ds = CType(Session("LeaveMatrix"), DataSet)
            'Dim tmpDate As String = e.Day.Date.Day & "/" & Date.e.Day.Date.Month & "/" & e.Day.Date.Year
            Dim tmpDate As String = Format(e.Day.Date, "dd/MMM/yy")
            Dim tmpRows() As DataRow = ds.Tables(0).Select("Date = '" & tmpDate & "'")
            If tmpRows.Length > 0 Then
                For Each row As DataRow In tmpRows
                    If Not IsDBNull(row.Item("Back_Color")) AndAlso Not row.Item("Back_Color") = "" Then
                        e.Cell.BackColor = Drawing.Color.FromName(row.Item("Back_Color"))
                    End If
                    If Not IsDBNull(row.Item("Fore_Color")) AndAlso Not row.Item("Fore_Color") = "" Then
                        e.Cell.ForeColor = Drawing.Color.FromName(row.Item("Fore_Color"))
                    End If
                    If Not IsDBNull(row.Item("DayType")) AndAlso Not row.Item("DayType") = "" AndAlso Not row.Item("DayType") = "AD" Then
                        'e.Cell.Text = e.Day.Date.Day & " " & UCase(row.Item("DayType"))
                        If Not IsDBNull(row.Item("Status")) AndAlso row.Item("Status") = "N" And Not row.Item("DayType") = "WK" And Not row.Item("DayType") = "HOL" Then
                            e.Cell.Text = e.Day.DayNumberText & " (*)"
                            'e.Cell.Font.Size = New FontUnit(6)
                            'e.Cell.ForeColor = Drawing.Color.Red
                        End If
                    End If
                Next
            Else
                If Not Session("BsuWeekEnd1") Is Nothing And Not Session("BsuWeekEnd2") Is Nothing And Not Session("LeaveTypeColorCodes") Is Nothing Then
                    If e.Day.Date.DayOfWeek.ToString = Session("BsuWeekEnd1") Or e.Day.Date.DayOfWeek.ToString = Session("BsuWeekEnd2") Then
                        tmpRows = CType(Session("LeaveTypeColorCodes"), DataSet).Tables(0).Select("ELT_ID = 'WK'")
                        If tmpRows.Length > 0 Then
                            e.Cell.BackColor = Drawing.Color.FromName(tmpRows(0).Item("Back_Color"))
                            e.Cell.ForeColor = Drawing.Color.FromName(tmpRows(0).Item("Fore_Color"))
                        End If
                    End If
                End If

            End If
        End If

    End Sub
    Protected Sub txtFrom_TextChanged(sender As Object, e As EventArgs) Handles txtFrom.TextChanged
        If txtFrom.Text = Nothing Then
            Exit Sub
        End If


        If Not txtFrom.Text = Nothing And Not txtTo.Text = Nothing Then
            If DateTime.TryParse(txtFrom.Text, Nothing) AndAlso DateTime.TryParse(txtFrom.Text, Nothing) Then
                If CDate(txtFrom.Text) > CDate(txtTo.Text) Then
                    Exit Sub
                End If
            End If
        End If

        If DateTime.TryParse(Me.txtFrom.Text, Nothing) Then
            If txtTo.Text = "" Then
                txtTo.Text = txtFrom.Text
            Else
                If Convert.ToDateTime(txtTo.Text) < Convert.ToDateTime(txtFrom.Text) Then
                    txtTo.Text = txtFrom.Text
                End If

            End If
            If Me.txtTo.Text = Nothing OrElse Not DateTime.TryParse(Me.txtTo.Text, Nothing) Then
                Dim EndDate As Date = New Date(CDate(txtFrom.Text).Year, CDate(txtFrom.Text).Month, Date.DaysInMonth(CDate(txtFrom.Text).Year, CDate(txtFrom.Text).Month))
                Me.ShowLeaveCalculation(False, CDate(txtFrom.Text), EndDate, IIf(Me.txtTo.Text = "", False, True))
                Me.ShowLeaveStatus(CDate(txtFrom.Text))
            Else
                Me.ShowLeaveCalculation(False, CDate(txtFrom.Text), CDate(txtTo.Text), True)
                Me.ShowLeaveStatus(CDate(txtFrom.Text))
            End If
        End If
    End Sub

    Protected Sub txtTo_TextChanged(sender As Object, e As EventArgs) Handles txtTo.TextChanged
        If ddMonthstatusPeriodically.SelectedItem.Value = "0" Then
            lblmsg.Visible = True
            lblmsg.Text = "Please Select Leave Type!!!"
        Else
            lblmsg.Visible = False
            If txtFrom.Text = "" Then Exit Sub
            If txtTo.Text = "" Then Exit Sub

            If Not txtFrom.Text = Nothing And Not txtTo.Text = Nothing Then
                If DateTime.TryParse(Me.txtFrom.Text, Nothing) AndAlso DateTime.TryParse(Me.txtTo.Text, Nothing) Then
                    If CDate(txtFrom.Text) > CDate(txtTo.Text) Then
                        Exit Sub
                    End If
                End If
            End If

            If DateTime.TryParse(Me.txtTo.Text, Nothing) Then
                Dim FromDate As Date
                If Not DateTime.TryParse(Me.txtFrom.Text, Nothing) Then
                    FromDate = New Date(Now.Date.Year, Now.Date.Month, 1)
                Else
                    FromDate = CDate(txtFrom.Text)
                End If
                Me.ShowLeaveCalculation(False, FromDate, CDate(txtTo.Text), True)
                Me.ShowLeaveStatus(FromDate)
            End If
        End If
    End Sub
    'Protected Sub lbtnClosePl_Click(sender As Object, e As EventArgs) Handles lbtnClosePl.Click
    '    Me.divbox.Visible = False
    'End Sub

    Protected Sub imgclose_Click(sender As Object, e As ImageClickEventArgs) Handles imgclose.Click
        Me.divPopup.Visible = False
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Me.chkForward.Checked = True
        'Me.DemoSave()
        Me.SaveLeaveApplication()
    End Sub

    Protected Sub btnSaveDraft_Click(sender As Object, e As EventArgs) Handles btnSaveDraft.Click
        Me.chkForward.Checked = False
        Me.SaveLeaveApplication()
    End Sub

    Protected Sub dlLeaveStatus_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlLeaveStatus.ItemCommand
        Me.ddMonthstatusPeriodically.SelectedValue = e.CommandArgument.ToString
        Me.PushDetails(e.CommandArgument.ToString)

    End Sub

    Private Sub PushDetails(ByVal ELT_ID As String)
        Try
            Dim pParms(0) As SqlClient.SqlParameter
            pParms(0) = Mainclass.CreateSqlParameter("@ELT_ID", ELT_ID, SqlDbType.VarChar)
            Dim dr As SqlDataReader
            dr = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnection, CommandType.StoredProcedure, "[USR_SS].[EmpLeaveApplication_GetPrevAppliedLeave]", pParms)
            Do While dr.Read
                Me.txtRemarks.Text = dr.Item("ELA_REMARKS")
                Me.txtHandover.Text = dr.Item("ELA_HANDOVERTXT")
                Me.txtAddress.Text = dr.Item("ELA_ADDRESS")
                Me.txtPhone.Text = dr.Item("ELA_PHONE")
                Me.txtMobile.Text = dr.Item("ELA_MOBILE")
            Loop
        Catch ex As Exception
            HandleUserMessage("Error occured getting last leave record", UserMessageType.Failure)
        End Try
    End Sub

    Protected Sub ddMonthstatusPeriodically_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddMonthstatusPeriodically.SelectedIndexChanged

        If Me.ddMonthstatusPeriodically.SelectedValue = "ML" Then 'medical leave
            Me.trUplaod.Visible = True
        Else
            Me.trUplaod.Visible = False
        End If

        If Me.txtFrom.Text = Nothing Or Me.txtTo.Text = Nothing Then
            Me.ShowLeaveCalculation(True, , , False)
            Me.ShowLeaveStatus(Now.Date)
            Me.LiteralLeaveCalculation.Text = ""
            Exit Sub
        End If
        If Not IsDate(Me.txtFrom.Text) Or Not IsDate(Me.txtTo.Text) Then
            'clear leave calculation
            '1) Set the calendar to show current month and show calendar events and leave status for current month (same as of page_load event)
            '2) Clear leave calculation part
            'clear calendar events
            'clear leave status
            Me.ShowLeaveCalculation(True, , , False)
            Me.ShowLeaveStatus(Now.Date)
            Me.LiteralLeaveCalculation.Text = ""
            Exit Sub
        End If
        If IsDate(Me.txtFrom.Text) Or IsDate(Me.txtTo.Text) Then
            Me.ShowLeaveCalculation(False, CDate(Me.txtFrom.Text), CDate(txtTo.Text), True)
            Me.ShowLeaveStatus(CDate(Me.txtFrom.Text))
        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If ViewState("canedit") = "yes" Or 1 = 1 Then
            ResetViewData()
            '  If ddMonthstatusPeriodically.SelectedValue = "ML" Then
            getDocumentEnabledFlag(ddMonthstatusPeriodically.SelectedValue)
            If ViewState("IdDocEnabled") = "1" Then
                If h_DOC_PCD_ID.Value <> "" Then
                    If ViewState("canedit") = "yes" Or imgDoc.Visible = True Then
                        If imgDoc.Visible = True Then
                            imgDelete.Visible = True
                        Else
                            imgDelete.Visible = False
                        End If
                    Else
                        imgDoc.Visible = False
                        imgDelete.Visible = False
                        ' filupload.Visible = True
                        filupload.Attributes.Remove("style")
                        filupload.Attributes.Add("style", "display:inline")
                    End If
                End If
            End If
            ViewState("PassElaID") = True
            ViewState("datamode") = "edit"
            Me.btnEdit.Visible = False
            Me.btnSavePopUp.Visible = True
            '''GetEmpLeaveCounts(Now.Date, Me.ddMonthstatusPeriodically.SelectedValue)
            '''Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ElseIf ViewState("IsCancelled") = "yes" Then
            lblmsg.CssClass = "divErrorMsg"
            lblmsg.Text = "You can not edit the cancelled application ."
        Else
            ViewState("EditForwarded") = True
        End If
    End Sub

    Protected Sub btnCancelLeave_Click(sender As Object, e As EventArgs) Handles btnCancelLeave.Click
        If ViewState("canedit") = "yes" Then
            '''lblError.Text = "Only Forwarded applications can be Cancelled.Try Delete!!"
            HandleUserMessage("Only Forwarded leave applications can be Cancelled.Try Deleting the leave application!!", UserMessageType.Failure)
        Else

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = h_ELA_ID.Value ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

            pParms(1) = New SqlClient.SqlParameter("@retVal", SqlDbType.VarChar, 200)
            pParms(1).Value = ""

            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer = 0

            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim stTrans As SqlTransaction = conn.BeginTransaction

            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[CancelEmpLeaveApp]", pParms)
            If pParms(1).Value = 0 Then
                'lblError.Text = getErrorMessage("0")
                '''lblError.Text = "Request for Cancellation made successfully."
                Me.HandleUserMessage("Leave cancellation request sent successfully.", UserMessageType.Success)
                stTrans.Commit()
                ' ''Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, "Leave Application Cancelling", _
                ' '' "Cancel", Page.User.Identity.Name.ToString, Me.Page)
                chkSaveAnyway.Visible = False

                setViewData()
                '''setModifyHeader(h_ID.Value)
                setModifyHeader(h_ELA_ID.Value)
                ViewState("datamode") = "add"
                '''Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ''' Me.btnSavePopUp.Visible = False
                Me.btnEdit.Visible = False
                Me.btnDelete.Visible = False
                Me.btnDelete.Visible = False
                Me.btnCancelLeave.Visible = False
                Me.btnCancel.Visible = True
                clear_All()
                btnCancelLeave.Visible = False
                btnCancelLeave.Visible = False
                'clear_All()
            Else
                '''lblError.Text = UtilityObj.getErrorMessage("retval")
                Me.HandleUserMessage(UtilityObj.getErrorMessage("retval"), UserMessageType.Failure)
                stTrans.Rollback()
            End If
        End If
    End Sub

    Protected Sub dlLeaveStatus_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlLeaveStatus.ItemDataBound
        Dim hfBackColor As HiddenField = e.Item.FindControl("hfElt_BackColor")
        Dim hfForeColor As HiddenField = e.Item.FindControl("hfElt_ForeColor")
        Dim div As HtmlGenericControl = CType(e.Item.FindControl("divText"), HtmlGenericControl)
        Dim lbl As Label = CType(e.Item.FindControl("lbltitle"), Label)
        div.Attributes.Add("style", "margin:0px;padding-left:3px;padding-top:4px;background:" & hfBackColor.Value)
        lbl.ForeColor = Drawing.Color.FromName(hfForeColor.Value)
        '''lbl.Attributes.Add("style", "forecolor:" & hfForeColor.Value)
    End Sub

    Protected Sub lnkbackbtn_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("../ESSDashboard.aspx")
        Response.Redirect(URL)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            'str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "'"     'V1.1 comment
            str_Sql = " exec GetLeaveDetailsPending '" & Session("SbSUID") & "','" & Session("sUsr_name") & "'"    'V1.1
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            gvJournal.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

End Class
