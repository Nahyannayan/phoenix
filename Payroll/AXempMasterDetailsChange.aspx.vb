﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports PayrollFunctions

Partial Class Payroll_AXempMasterDetailsChange
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'Dim smScriptManager As New ScriptManager
            'smScriptManager = Master.FindControl("ScriptManager1")
            'smScriptManager.EnablePageMethods = True
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtVisa.Attributes.Add("readonly", "readonly")
            txtDes.Attributes.Add("readonly", "readonly")
            txtMoe.Attributes.Add("readonly", "readonly")
            txtEmpApprovalPol.Attributes.Add("readonly", "readonly")
            txtNVisa.Attributes.Add("readonly", "readonly")
            ' txtNEmp.Attributes.Add("readonly", "readonly")
            txtNMoe.Attributes.Add("readonly", "readonly")
            txtVacationPolicy.Attributes.Add("readonly", "readonly")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "HAX0001" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") <> "" Then

                setViewData()
                '  setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))

                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If
            'Commented and added below method by vikranth on 5th Aug 2019
            'CheckIfBSUisTaxEnabled()
            CheckBusinessUnit_Flags()
            bindgrid()
            'Added by vikranth on 13th Nov 2019
            If (Session("BSU_COUNTRY_ID") = "217") Then
                tdABCCategory.Visible = True
                tdddlABCCategory.Visible = True
            Else tdABCCategory.Visible = False
                tdddlABCCategory.Visible = False
            End If
        End If
    End Sub
    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtVisa.Attributes.Add("readonly", "readonly")
        txtDes.Attributes.Add("readonly", "readonly")
        txtMoe.Attributes.Add("readonly", "readonly")
        txtNVisa.Attributes.Add("readonly", "readonly")

        txtNMoe.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        txtEmpApprovalPol.Attributes.Add("readonly", "readonly")
        txtVacationPolicy.Attributes.Add("readonly", "readonly")
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
        imgEmployee.Enabled = False
    End Sub
    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
    End Sub

    Public Sub setDefaultLeavePolicy()
        Try
            Dim sqlStr As String
            Dim mLeaveTbl As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            sqlStr = "SELECT TOP 1 LPS_ID,LPS_DESCRIPTION FROM APPROVALPOLICY_H H INNER join APPROVALPOLICY_D D ON LPS_ID=LPD_LPS_ID AND LPD_BSU_ID='" & Session("sBsuid") & "' AND LPD_DES_ID ='" & h_Empcat.Value & "' WHERE LPS_bActive = 1 ORDER BY H.LPS_ID desc"
            mLeaveTbl = Mainclass.getDataTable(sqlStr, str_conn)
            If mLeaveTbl.Rows.Count > 0 Then
                txtEmpApprovalPol.Text = mLeaveTbl.Rows(0).Item("LPS_DESCRIPTION")
                h_ApprovalPolicy.Value = mLeaveTbl.Rows(0).Item("LPS_ID")
            End If
        Catch ex As Exception

        End Try
    End Sub



    Sub clear_All()
        txtFrom.Text = ""
        txtRemarks.Text = ""
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        h_Empcat.Value = ""
        h_VisaDES.Value = ""
        txtVisa.Text = ""
        txtDes.Text = ""
        txtMoe.Text = ""
        txtNVisa.Text = ""
        h_MoeDES.Value = ""
        txtNMoe.Text = ""
        txtEmpApprovalPol.Text = ""
        txtVacationPolicy.Text = ""
        h_ApprovalPolicy.Value = ""
        txtABC.Text = ""
        ddABC.SelectedValue = "N"
        txtEmpApprovalPol.Text = ""
        lblVacPolicy.Text = ""
        lblApprovalPolicy.Text = ""
        bindgrid()
        chkbPunching.Checked = False
        txtAttendBy.Text = ""
        hfAttendBy.Value = ""
        chkbEmailPayslipInArabic.Checked = False 'Added by vikranth on 4th July 2019
        chkbEnabledELeave.Checked = False 'Added by vikranth on 5th Aug 2019
    End Sub
    Sub bindgrid()

        Dim ds As New DataSet
        Dim str_Sql As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        str_Sql = "SELECT EST.EST_BSU_ID, TTP.TTP_DESCR," _
        & " EST.EST_EMP_ID, EST.EST_CODE, EST.EST_DTFROM, EST.EST_DTTO, " _
        & " EST.EST_REMARKS, EST.EST_OLDCODE, EDN.DES_DESCR AS NEWDES, " _
        & " EDO.DES_DESCR AS OLDDES FROM EMPTRANTYPE_M AS TTP RIGHT OUTER JOIN" _
        & " EMPTRANTYPE_TRN AS EST INNER JOIN EMPDESIGNATION_M AS EDN " _
        & " ON EST.EST_CODE = EDN.DES_ID INNER JOIN EMPDESIGNATION_M" _
        & " AS EDO ON EST.EST_OLDCODE = EDO.DES_ID ON TTP.TTP_ID = EST.EST_TTP_ID" _
        & " WHERE (EST.EST_EMP_ID = '" & h_Emp_No.Value & "') AND (EST.EST_BSU_ID = '" & Session("sBsuid") & "') " _
        & " AND (EST.EST_TTP_ID IN( 1,22,33)) ORDER BY EST.EST_TTP_ID"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        GridView1.DataSource = ds
        GridView1.DataBind()
    End Sub
    Protected Sub imgEmployee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            'Added new column (EMP_Email_Payslip_In_Arabic) by vikranth on 4th July 2019 in below select query
            'Added new column (EMP_SI_Calculation) by vikranth on 1st Oct 2019 in below select query
            ' Needs to Add these two fields (,EM.EMP_Email_Payslip_In_Arabic,EM.EMP_bOn_Eleave) in below select query while going live E-leave and KSA GOSI changes
            str_Sql = "SELECT EM.EMP_ID,EM.EMPNO, " _
            & " EM.EMP_ECT_ID, ECT.ECT_DESCR, EM.EMP_DPT_ID, " _
            & " DES.DES_DESCR DESDESCR ,EM.EMP_ABC,EDM.DES_DESCR MOEDES_DESCR, EDV.DES_DESCR VISADES_DESCR,LPS.LPS_DESCRIPTION AS LPS_DES, " _
            & " EM.EMP_VISA_DES_ID,EM.EMP_MOE_DES_ID, EM.EMP_DES_ID,EM.EMP_LPS_ID ,EM.EMP_BLS_ID ,EM.BLS_DESCRIPTION,EM.empAttendanceApprover ,EM.EMP_ATT_Approv_EMP_ID, " _
            & " isnull(EM.EMP_bReqPunching,0) EMP_bReqPunching,EM.EMP_bIsTaxable,EM.EMP_SI_Calculation,EM.EMD_WEEKEND1,EM.EMD_WEEKEND2 FROM vw_OSO_EMPLOYEEMASTER AS EM inner join " _
            & " EMPCATEGORY_M AS ECT ON EM.EMP_ECT_ID = ECT.ECT_ID inner join " _
            & " EMPDESIGNATION_M AS DES ON EM.EMP_DES_ID = DES.DES_ID left join " _
            & "  bsu_leaveslab_s on bls_id=emp_bls_id and bls_bsu_id=emp_bsu_id left join " _
            & " EMPDESIGNATION_M AS EDM ON EM.EMP_MOE_DES_ID = EDM.DES_ID left JOIN " _
            & " EMPDESIGNATION_M AS EDV ON EM.EMP_VISA_DES_ID = EDV.DES_ID LEFT OUTER JOIN APPROVALPOLICY_H LPS ON EM.EMP_LPS_ID= LPS.LPS_ID " _
            & " left join employee_m E2 on E2.emp_id=EM.EMP_ATT_Approv_EMP_ID " _
            & " WHERE (EM.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EM.EMP_ID = '" & h_Emp_No.Value & "')"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDes.Text = IIf(ds.Tables(0).Rows(0)("DESDESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("DESDESCR").ToString)
                txtCategory.Text = IIf(ds.Tables(0).Rows(0)("ECT_DESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("ECT_DESCR").ToString)
                txtAttendBy.Text = IIf(ds.Tables(0).Rows(0)("empAttendanceApprover") Is DBNull.Value, "", ds.Tables(0).Rows(0)("empAttendanceApprover").ToString)
                'txtGrade.Text = ds.Tables(0).Rows(0)("EGD_DESCR").ToString
                txtABC.Text = IIf(ds.Tables(0).Rows(0)("EMP_ABC") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ABC").ToString)
                txtMoe.Text = IIf(ds.Tables(0).Rows(0)("MOEDES_DESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("MOEDES_DESCR").ToString)
                txtVisa.Text = IIf(ds.Tables(0).Rows(0)("VISADES_DESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("VISADES_DESCR").ToString)
                lblApprovalPolicy.Text = IIf(ds.Tables(0).Rows(0)("LPS_DES") Is DBNull.Value, "", ds.Tables(0).Rows(0)("LPS_DES").ToString)
                lblVacPolicy.Text = IIf(ds.Tables(0).Rows(0)("BLS_DESCRIPTION") Is DBNull.Value, "", ds.Tables(0).Rows(0)("BLS_DESCRIPTION").ToString)
                hfCat_ID.Value = IIf(ds.Tables(0).Rows(0)("EMP_ECT_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ECT_ID").ToString)
                ViewState("ECT_ID") = IIf(ds.Tables(0).Rows(0)("EMP_ECT_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ECT_ID").ToString)
                ViewState("EDVISA_ID") = IIf(ds.Tables(0).Rows(0)("EMP_VISA_DES_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_VISA_DES_ID").ToString)
                ViewState("EDMOE_ID") = IIf(ds.Tables(0).Rows(0)("EMP_MOE_DES_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_MOE_DES_ID").ToString)
                ViewState("EDDES_ID") = IIf(ds.Tables(0).Rows(0)("EMP_DES_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_DES_ID").ToString)
                ViewState("EDLPS_ID") = IIf(ds.Tables(0).Rows(0)("EMP_LPS_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_LPS_ID").ToString)
                ViewState("EBLS_ID") = IIf(ds.Tables(0).Rows(0)("EMP_BLS_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_BLS_ID").ToString)
                ViewState("ATT_EMP_ID") = IIf(ds.Tables(0).Rows(0)("EMP_ATT_Approv_EMP_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ATT_Approv_EMP_ID").ToString)
                chkbPunching.Checked = IIf(ds.Tables(0).Rows(0)("EMP_bReqPunching") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bReqPunching")) ''IIf(dr("EMP_bReqPunching") Is DBNull.Value, False, dr("EMP_bReqPunching"))
                ViewState("chkPunching") = IIf(ds.Tables(0).Rows(0)("EMP_bReqPunching") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bReqPunching"))
                ViewState("EmpNo") = IIf(ds.Tables(0).Rows(0)("EMPNO") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMPNO").ToString)
                chkTax.Checked = IIf(ds.Tables(0).Rows(0)("EMP_bIsTaxable") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bIsTaxable").ToString)
                ViewState("chkTax") = IIf(ds.Tables(0).Rows(0)("EMP_bIsTaxable") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bIsTaxable").ToString)
                'ViewState("EMP_Email_Payslip_In_Arabic") = IIf(ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic")) ' Added by vikranth on 4th July 2019
                'chkbEmailPayslipInArabic.Checked = IIf(ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic")) ' Added by vikranth on 4th July 2019
                'ViewState("EMP_bOn_Eleave") = IIf(ds.Tables(0).Rows(0)("EMP_bOn_Eleave") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bOn_Eleave")) ' Added by vikranth on 5th Aug 2019
                'chkbEnabledELeave.Checked = IIf(ds.Tables(0).Rows(0)("EMP_bOn_Eleave") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bOn_Eleave")) ' Added by vikranth on 5th Aug 2019
                ViewState("EMP_SI_Calculation") = IIf(ds.Tables(0).Rows(0)("EMP_SI_Calculation") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_SI_Calculation")) ' Added by vikranth on 1st Oct 2019
                chkSICalculation.Checked = IIf(ds.Tables(0).Rows(0)("EMP_SI_Calculation") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_SI_Calculation")) ' Added by vikranth on 1st Oct 2019
                ViewState("EMD_WEEKEND1") = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND1") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                ViewState("EMD_WEEKEND2") = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND2") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                ddlWeekEnd1.SelectedValue = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND1") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                ddlWeekEnd2.SelectedValue = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND2") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                'ViewState("BSU_WEEKEND1") = IIf(ds.Tables(0).Rows(0)("BSU_WEEKEND1") Is DBNull.Value, "", ds.Tables(0).Rows(0)("BSU_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                'ViewState("BSU_WEEKEND2") = IIf(ds.Tables(0).Rows(0)("BSU_WEEKEND2") Is DBNull.Value, "", ds.Tables(0).Rows(0)("BSU_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                'ddlWeekEnd1.SelectedValue = IIf(ViewState("EMD_WEEKEND1") = "", ViewState("BSU_WEEKEND1"), ViewState("EMD_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                'ddlWeekEnd2.SelectedValue = IIf(ViewState("EMD_WEEKEND2") = "", ViewState("BSU_WEEKEND2"), ViewState("EMD_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                If (Session("BSU_COUNTRY_ID") = "217") Then
                    ddlABC.SelectedValue = IIf(ds.Tables(0).Rows(0)("EMP_ABC") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ABC").ToString)
                End If
            Else

                End If
            bindgrid()
        Catch ex As Exception
            lblError.Text = ex.Message
            Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtRemarks.Text = "" Then
            lblError.Text = "Please enter remarks."
            Exit Sub
        End If
        If txtFrom.Text = "" Then
            lblError.Text = "Please select From date."
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            Dim retval As String = "0"
            Dim str_update_master As String = ""
            Dim str_str_code As String = ""
            Dim str_ttp_id As String = ""

            If txtEmpNo.Text = "" Then
                lblError.Text = "Please select an employee."
                Exit Sub
            End If
            Dim vals As Integer = 0

            'If txtNMoe.Text <> "" And h_MoeDES.Value <> "" Then
            '    retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "22", h_MoeDES.Value, _
            '                            txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EDMOE_ID"), False, stTrans)
            '    vals = 1
            '    If retval = "0" Then
            '        Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET  EMP_MOE_DES_ID = " & h_MoeDES.Value & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
            '        myCommand.ExecuteNonQuery()
            '        retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_MOE_DES_ID-" & txtNMoe.Text)
            '    End If

            'End If


            'If txtNVisa.Text <> "" And h_VisaDES.Value <> "" Then
            '    retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "33", h_VisaDES.Value, _
            '                            txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EDVISA_ID"), False, stTrans)
            '    vals = 1
            '    If retval = "0" Then
            '        Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_VISA_DES_ID= " & h_VisaDES.Value & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
            '        myCommand.Transaction = stTrans
            '        myCommand.ExecuteNonQuery()
            '    End If
            '    retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_VISA_DES_ID-" & txtNVisa.Text)
            'End If

            'If txtABC.Text <> ddABC.SelectedItem.Text And ddABC.SelectedValue <> "N" Then
            '    retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "18", ddABC.SelectedValue, _
            '                            txtFrom.Text, Nothing, txtRemarks.Text.ToString, txtABC.Text, False, stTrans)
            '    vals = 1
            '    If retval = "0" Then
            '        Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_ABC = '" & ddABC.SelectedItem.Text & "' WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
            '        myCommand.Transaction = stTrans
            '        myCommand.ExecuteNonQuery()
            '    End If
            '    retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "ABC -" & ddABC.SelectedValue)
            'End If
            If txtVacationPolicy.Text <> "" And hf_VacationPolicy.Value <> "" And ViewState("EBLS_ID") <> hf_VacationPolicy.Value Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET emp_bls_id = " & hf_VacationPolicy.Value & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                vals = 1
                myCommand = New SqlCommand("exec InsertLeaveSchedule'" & Session("sBsuid").ToString() & "','" & txtFrom.Text & "'," & h_Emp_No.Value, objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()

                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "emp_bls_id -" & txtVacationPolicy.Text)
            End If

            If txtEmpApprovalPol.Text <> "" And h_ApprovalPolicy.Value <> "" Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET emp_lps_id = " & h_ApprovalPolicy.Value & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                vals = 1
            End If
            If hfAttendBy.Value <> "" Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_ATT_Approv_EMP_ID = " & hfAttendBy.Value & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()

                myCommand = New SqlCommand("UPDATE empallocation SET EAL_ATT_APPROV_EMP_ID = " & hfAttendBy.Value & " WHERE EAL_EMP_ID='" & h_Emp_No.Value & "' and EAL_TODT is NULL", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()

                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "emp_bls_id -" & txtVacationPolicy.Text)

                vals = 1
            End If
            If chkbPunching.Checked <> ViewState("chkPunching") Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_bReqPunching = " & IIf(chkbPunching.Checked = True, 1, 0) & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_bReqPunching -" & chkbPunching.Checked)
                vals = 1
            End If
            If chkTax.Checked <> ViewState("chkTax") Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_bIsTaxable = " & IIf(chkTax.Checked = True, 1, 0) & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_bReqPunching -" & chkbPunching.Checked)
                vals = 1
            End If
            ''Added below If condition by vikranth on 4th July 2019
            'If chkbEmailPayslipInArabic.Checked <> ViewState("EMP_Email_Payslip_In_Arabic") Then
            '    Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_Email_Payslip_In_Arabic = " & IIf(chkbEmailPayslipInArabic.Checked = True, 1, 0) & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
            '    myCommand.Transaction = stTrans
            '    myCommand.ExecuteNonQuery()
            '    retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_Email_Payslip_In_Arabic -" & chkbEmailPayslipInArabic.Checked)
            '    vals = 1
            'End If
            ''Added below If condition by vikranth on 5th Aug 2019
            'If chkbEnabledELeave.Checked <> ViewState("EMP_bOn_Eleave") Then
            '    Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_bOn_Eleave = " & IIf(chkbEnabledELeave.Checked = True, 1, 0) & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
            '    myCommand.Transaction = stTrans
            '    myCommand.ExecuteNonQuery()
            '    retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_bOn_Eleave -" & chkbEnabledELeave.Checked)
            '    vals = 1
            'End If
            'Added below If condition by vikranth on 1st Oct 2019RegisterStartupScript
            If chkSICalculation.Checked <> ViewState("EMP_SI_Calculation") Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_SI_Calculation = " & IIf(chkSICalculation.Checked = True, 1, 0) & " WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_SI_Calculation -" & chkSICalculation.Checked)
                vals = 1
            End If
            'Added below If condition by vikranth on 15th Oct 2019
            If ddlWeekEnd1.SelectedValue <> ViewState("EMD_WEEKEND1") Or ddlWeekEnd1.SelectedValue = ViewState("EMD_WEEKEND1") Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_D SET EMD_WEEKEND1 = '" & ddlWeekEnd1.SelectedValue & "' WHERE EMD_EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMD_WEEKEND1 -" & ddlWeekEnd1.SelectedValue)
                vals = 1
            End If

            'Added below If condition by vikranth on 15th Oct 2019
            If ddlWeekEnd2.SelectedValue <> ViewState("EMD_WEEKEND2") Or ddlWeekEnd2.SelectedValue = ViewState("EMD_WEEKEND2") Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_D SET EMD_WEEKEND2 = '" & ddlWeekEnd2.SelectedValue & "' WHERE EMD_EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMD_WEEKEND2 -" & ddlWeekEnd2.SelectedValue)
                vals = 1
            End If

            'Added by vikranth on 13th Nov 2019
            If (Session("BSU_COUNTRY_ID") = "217") Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET EMP_ABC = '" & ddlABC.SelectedValue & "' WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.Transaction = stTrans
                myCommand.ExecuteNonQuery()
                retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "EMP_ABC -" & ddlABC.SelectedValue)
                vals = 1
            End If

            If vals = 0 Then
                lblError.Text = "No changes to save."
                Exit Sub
            End If

            If retval = "0" Then

                stTrans.Commit()
                lblError.Text = "Data saved successfully!"
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(retval)
            End If
            retval = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value & "-" & ViewState("EmpNo"), "edit", Page.User.Identity.Name.ToString, Me.Page, "Employee editing")


        Catch ex As Exception
            lblError.Text = ex.Message ' "Error occured."
            stTrans.Rollback()

        Finally

            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try

    End Sub
    Private Sub CheckIfBSUisTaxEnabled()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT isnull(BSU_IsTAXEnabled,0) BSU_IsTAXEnabled FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            ViewState("BSU_IsTaxEnabled") = dr("BSU_IsTAXEnabled")
        End While
        If ViewState("BSU_IsTaxEnabled") = True Then
            trTax.Visible = True
        Else
            trTax.Visible = False
        End If
        dr.Close()
    End Sub
    'Written below method by vikranth on 5th Aug 2019
    Private Sub CheckBusinessUnit_Flags()
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As DataSet

        'Dim query As String = "Select isnull(EMP_SEX_bMALE,0) as emp_sex_bmale From Employee_M Where Emp_Id = (Select Usr_emp_id From Users_m where usr_id ='" & Session("sUsr_Id") & "')"
        Try
            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            params(0).Value = Session("sBSUID")
            params(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            If h_Emp_No.Value = "" Then
                h_Emp_No.Value = 0
            End If
            params(1).Value = h_Emp_No.Value


            ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "SP_CheckBusinessUnit_Flags", params)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("BSU_IsTAXEnabled").ToString().ToLower() = "true" Then
                        trTax.Visible = True
                    Else trTax.Visible = False
                    End If
                    'Added below If condition by vikranth on 1st Oct 2019
                    If ds.Tables(0).Rows(0)("BSU_SI_Calculation").ToString().ToLower() = "true" Then
                        tdSILabel.Visible = True
                        tdSICheckbox.Visible = True
                    Else tdSILabel.Visible = False
                        tdSICheckbox.Visible = False
                    End If
                Else trTax.Visible = False
                    tdSILabel.Visible = False
                    tdSICheckbox.Visible = False
                End If
                'If ds.Tables(1).Rows.Count > 0 Then
                '    If ds.Tables(1).Rows(0)("BUS_ISARABICEntity").ToString().ToLower() = "true" Then
                '        tdArabicLabel.Visible = True
                '       tdChckboxArabicLabel.Visible = True
                '    Else tdArabicLabel.Visible = False
                '           tdChckboxArabicLabel.Visible = False
                '    End If
                'Else tdArabicLabel.Visible = False
                '       tdChckboxArabicLabel.Visible = False
                'End If
                'If ds.Tables(2).Rows.Count > 0 Then
                '    If ds.Tables(2).Rows(0)("BLS_IS_ELEAVEENABLED").ToString().ToLower() = "true" Then
                '        tdEleaveLbl.Visible = True
                '        tdChkboxEleave.Visible = True
                '    Else tdEleaveLbl.Visible = False
                '        tdChkboxEleave.Visible = False
                '    End If
                'Else tdEleaveLbl.Visible = False
                '        tdChkboxEleave.Visible = False
                'End If

            End If
        Catch ex As Exception
            Me.lblError.Text = "There was an issue loading businessunit flags"
        End Try
    End Sub

    Protected Sub h_Emp_No_ValueChanged(sender As Object, e As EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            'Added below method by vikranth on 5th Aug 2019
            CheckBusinessUnit_Flags()
            'Added new column (EMP_Email_Payslip_In_Arabic) by vikranth on 4th July 2019 in below select query
            'Added new column (EMP_SI_Calculation) by vikranth on 1st Oct 2019 in below select query
            ' Needs to Add these two fields (,EM.EMP_Email_Payslip_In_Arabic,EM.EMP_bOn_Eleave) in below select query while going live E-leave and KSA GOSI changes
            str_Sql = "SELECT EM.EMP_ID,EM.EMPNO, " _
            & " EM.EMP_ECT_ID, ECT.ECT_DESCR, EM.EMP_DPT_ID, " _
            & " DES.DES_DESCR DESDESCR ,EM.EMP_ABC,EDM.DES_DESCR MOEDES_DESCR, EDV.DES_DESCR VISADES_DESCR,LPS.LPS_DESCRIPTION AS LPS_DES, " _
            & " EM.EMP_VISA_DES_ID,EM.EMP_MOE_DES_ID, EM.EMP_DES_ID,EM.EMP_LPS_ID ,EM.EMP_BLS_ID ,EM.BLS_DESCRIPTION,EM.empAttendanceApprover ,EM.EMP_ATT_Approv_EMP_ID, " _
            & " isnull(EM.EMP_bReqPunching,0) EMP_bReqPunching,EM.EMP_bIsTaxable,EM.EMP_SI_Calculation,EM.EMD_WEEKEND1,EM.EMD_WEEKEND2 FROM vw_OSO_EMPLOYEEMASTER AS EM inner join " _
            & " EMPCATEGORY_M AS ECT ON EM.EMP_ECT_ID = ECT.ECT_ID inner join " _
            & " EMPDESIGNATION_M AS DES ON EM.EMP_DES_ID = DES.DES_ID left join " _
            & "  bsu_leaveslab_s on bls_id=emp_bls_id and bls_bsu_id=emp_bsu_id left join " _
            & " EMPDESIGNATION_M AS EDM ON EM.EMP_MOE_DES_ID = EDM.DES_ID left JOIN " _
            & " EMPDESIGNATION_M AS EDV ON EM.EMP_VISA_DES_ID = EDV.DES_ID LEFT OUTER JOIN APPROVALPOLICY_H LPS ON EM.EMP_LPS_ID= LPS.LPS_ID " _
            & " left join employee_m E2 on E2.emp_id=EM.EMP_ATT_Approv_EMP_ID " _
            & " WHERE (EM.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EM.EMP_ID = '" & h_Emp_No.Value & "')"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDes.Text = IIf(ds.Tables(0).Rows(0)("DESDESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("DESDESCR").ToString)
                txtCategory.Text = IIf(ds.Tables(0).Rows(0)("ECT_DESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("ECT_DESCR").ToString)
                txtAttendBy.Text = IIf(ds.Tables(0).Rows(0)("empAttendanceApprover") Is DBNull.Value, "", ds.Tables(0).Rows(0)("empAttendanceApprover").ToString)
                'txtGrade.Text = ds.Tables(0).Rows(0)("EGD_DESCR").ToString
                txtABC.Text = IIf(ds.Tables(0).Rows(0)("EMP_ABC") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ABC").ToString)
                txtMoe.Text = IIf(ds.Tables(0).Rows(0)("MOEDES_DESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("MOEDES_DESCR").ToString)
                txtVisa.Text = IIf(ds.Tables(0).Rows(0)("VISADES_DESCR") Is DBNull.Value, "", ds.Tables(0).Rows(0)("VISADES_DESCR").ToString)
                lblApprovalPolicy.Text = IIf(ds.Tables(0).Rows(0)("LPS_DES") Is DBNull.Value, "", ds.Tables(0).Rows(0)("LPS_DES").ToString)
                lblVacPolicy.Text = IIf(ds.Tables(0).Rows(0)("BLS_DESCRIPTION") Is DBNull.Value, "", ds.Tables(0).Rows(0)("BLS_DESCRIPTION").ToString)
                hfCat_ID.Value = IIf(ds.Tables(0).Rows(0)("EMP_ECT_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ECT_ID").ToString)
                ViewState("ECT_ID") = IIf(ds.Tables(0).Rows(0)("EMP_ECT_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ECT_ID").ToString)
                ViewState("EDVISA_ID") = IIf(ds.Tables(0).Rows(0)("EMP_VISA_DES_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_VISA_DES_ID").ToString)
                ViewState("EDMOE_ID") = IIf(ds.Tables(0).Rows(0)("EMP_MOE_DES_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_MOE_DES_ID").ToString)
                ViewState("EDDES_ID") = IIf(ds.Tables(0).Rows(0)("EMP_DES_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_DES_ID").ToString)
                ViewState("EDLPS_ID") = IIf(ds.Tables(0).Rows(0)("EMP_LPS_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_LPS_ID").ToString)
                ViewState("EBLS_ID") = IIf(ds.Tables(0).Rows(0)("EMP_BLS_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_BLS_ID").ToString)
                ViewState("ATT_EMP_ID") = IIf(ds.Tables(0).Rows(0)("EMP_ATT_Approv_EMP_ID") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ATT_Approv_EMP_ID").ToString)
                chkbPunching.Checked = IIf(ds.Tables(0).Rows(0)("EMP_bReqPunching") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bReqPunching")) ''IIf(dr("EMP_bReqPunching") Is DBNull.Value, False, dr("EMP_bReqPunching"))
                ViewState("chkPunching") = IIf(ds.Tables(0).Rows(0)("EMP_bReqPunching") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bReqPunching"))
                ViewState("EmpNo") = IIf(ds.Tables(0).Rows(0)("EMPNO") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMPNO").ToString)
                chkTax.Checked = IIf(ds.Tables(0).Rows(0)("EMP_bIsTaxable") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bIsTaxable").ToString)
                ViewState("chkTax") = IIf(ds.Tables(0).Rows(0)("EMP_bIsTaxable") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bIsTaxable").ToString)
                'ViewState("EMP_Email_Payslip_In_Arabic") = IIf(ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic")) ' Added by vikranth on 4th July 2019
                'chkbEmailPayslipInArabic.Checked = IIf(ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_Email_Payslip_In_Arabic")) ' Added by vikranth on 4th July 2019
                'ViewState("EMP_bOn_Eleave") = IIf(ds.Tables(0).Rows(0)("EMP_bOn_Eleave") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bOn_Eleave")) ' Added by vikranth on 5th Aug 2019
                'chkbEnabledELeave.Checked = IIf(ds.Tables(0).Rows(0)("EMP_bOn_Eleave") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_bOn_Eleave")) ' Added by vikranth on 5th Aug 2019
                ViewState("EMP_SI_Calculation") = IIf(ds.Tables(0).Rows(0)("EMP_SI_Calculation") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_SI_Calculation")) ' Added by vikranth on 1st Oct 2019
                chkSICalculation.Checked = IIf(ds.Tables(0).Rows(0)("EMP_SI_Calculation") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_SI_Calculation")) ' Added by vikranth on 1st Oct 2019
                ViewState("EMD_WEEKEND1") = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND1") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                ViewState("EMD_WEEKEND2") = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND2") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                ddlWeekEnd1.SelectedValue = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND1") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                ddlWeekEnd2.SelectedValue = IIf(ds.Tables(0).Rows(0)("EMD_WEEKEND2") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMD_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                'ViewState("BSU_WEEKEND1") = IIf(ds.Tables(0).Rows(0)("BSU_WEEKEND1") Is DBNull.Value, "", ds.Tables(0).Rows(0)("BSU_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                'ViewState("BSU_WEEKEND2") = IIf(ds.Tables(0).Rows(0)("BSU_WEEKEND2") Is DBNull.Value, "", ds.Tables(0).Rows(0)("BSU_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                'ddlWeekEnd1.SelectedValue = IIf(ViewState("EMD_WEEKEND1") = "", ViewState("BSU_WEEKEND1"), ViewState("EMD_WEEKEND1")) ' Added by vikranth on 15th Oct 2019
                'ddlWeekEnd2.SelectedValue = IIf(ViewState("EMD_WEEKEND2") = "", ViewState("BSU_WEEKEND2"), ViewState("EMD_WEEKEND2")) ' Added by vikranth on 15th Oct 2019
                If (Session("BSU_COUNTRY_ID") = "217") Then
                    ddlABC.SelectedValue = IIf(ds.Tables(0).Rows(0)("EMP_ABC") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EMP_ABC").ToString)
                End If
            Else

            End If
            bindgrid()
        Catch ex As Exception
            lblError.Text = ex.Message
            Errorlog(ex.Message)

        End Try
    End Sub
End Class
