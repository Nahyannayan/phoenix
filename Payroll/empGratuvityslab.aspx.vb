Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Payroll_empGratuvityslab
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtCategory.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            cblSplitup.DataBind()
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P100050" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If

            If Request.QueryString("viewid") <> "" Then

                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            Else
                ResetViewData()
            End If

        End If
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     BGS.BGS_ID, BGS.BGS_BSU_ID, " _
            & " BGS.BGS_DESCRIPTION, BGS.BGS_DTFROM, " _
            & " BGS.BGS_DTTO, BGS.BGS_CONTRACTTYPE, BGS.BGS_ECT_ID, " _
            & " BGS.BGS_TRANTYPE, BGS.BGS_SLABFROM, BGS.BGS_SLABTO," _
            & " BGS.BGS_PROVISIONDAYS, BGS.BGS_ACTUALDAYS, EC.ECT_DESCR" _
            & " FROM BSU_GRATUITYSLAB_S AS BGS INNER JOIN" _
            & " EMPCATEGORY_M AS EC ON BGS.BGS_ECT_ID = EC.ECT_ID" _
            & " WHERE (BGS.BGS_ID = '" & p_Modifyid & "') AND (BGS.BGS_BSU_ID = '" & Session("sBSUID") & "')"
            Dim ds As New DataSet
            ViewState("canedit") = "no"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                h_catid.Value = ds.Tables(0).Rows(0)("BGS_ECT_ID")
                txtCategory.Text = ds.Tables(0).Rows(0)("ECT_DESCR").ToString
                'ddMonthstatusPeriodically.SelectedIndex = -1
                'ddMonthstatusPeriodically.Items.FindByValue(ds.Tables(0).Rows(0)("BLS_ELT_ID").ToString).Selected = True
                If IsDate(ds.Tables(0).Rows(0)("BGS_DTFROM")) Then
                    txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("BGS_DTFROM")), "dd/MMM/yyyy")

                End If
                If IsDate(ds.Tables(0).Rows(0)("BGS_DTTO")) Then
                    txtTo.Text = Format(CDate(ds.Tables(0).Rows(0)("BGS_DTTO")), "dd/MMM/yyyy")
                End If

                txtRemarks.Text = ds.Tables(0).Rows(0)("BGS_DESCRIPTION")
                txtGrafrom.Text = ds.Tables(0).Rows(0)("BGS_SLABFROM")
                txtGrato.Text = ds.Tables(0).Rows(0)("BGS_SLABTO")
                txtProvdays.Text = ds.Tables(0).Rows(0)("BGS_PROVISIONDAYS")
                txtActualDays.Text = ds.Tables(0).Rows(0)("BGS_ACTUALDAYS")
                ddContracttype.Items.FindByValue(ds.Tables(0).Rows(0)("BGS_CONTRACTTYPE")).Selected = True
                ddTrantype.Items.FindByValue(ds.Tables(0).Rows(0)("BGS_TRANTYPE")).Selected = True
                str_Sql = "SELECT  BLG.BLG_EARNCODE " _
                                    & " FROM BSU_GRATUITYSLAB_S AS BGS INNER JOIN" _
                                    & " BSU_LEAVEnGRATUITYEARN_S AS BLG ON BGS.BGS_ID = BLG.BLG_REF_ID" _
                                    & " WHERE BLG.BLG_REF_ID=" & p_Modifyid & " AND   BLG.BLG_TRANTYPE='G'"
                ds.Tables.Clear() 'BLG.BLG_REF_ID,   BLG.BLG_ID,

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    cblSplitup.Items.FindByValue(ds.Tables(0).Rows(I)(0)).Selected = True
                Next
            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtTo.Attributes.Add("readonly", "readonly")
        'txtOn.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        imgFrom.Enabled = False
        imgTo.Enabled = False
        imgCategory.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtTo.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        imgFrom.Enabled = True
        imgCategory.Enabled = True
        imgTo.Enabled = True
    End Sub

    Sub clear_All()
        txtRemarks.Text = ""
        txtFrom.Text = ""
        txtTo.Text = ""
        h_catid.Value = ""
        txtCategory.Text = ""
        txtGrafrom.Text = ""
        txtGrato.Text = ""
        txtProvdays.Text = ""
        txtActualDays.Text = ""
        For Each item As ListItem In cblSplitup.Items
            item.Selected = False
        Next
    End Sub

    Sub bindMonthstatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("strHoliday1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("strHoliday2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function checkErrors() As String
        Dim Str_Error As String = ""
        If txtRemarks.Text.Trim = "" Then
            Str_Error = "Please Enter Remarks <Br>"
        End If
        If txtFrom.Text.Trim = "" Then
            Str_Error = Str_Error & "Please Enter From Date <Br>"
        Else
            If IsDate(txtFrom.Text) = False Then
                Str_Error = Str_Error & "Please Enter Valid From Date <Br>"
            End If
        End If
        If IsDate(txtFrom.Text) = False Then
            Str_Error = Str_Error & "Please Valid To Date <Br>"
        End If

        If IsNumeric(txtGrafrom.Text) = False Then
            Str_Error = Str_Error & "Please Valid Full Paid Days <Br>"
        End If
        If IsNumeric(txtGrato.Text) = False Then
            Str_Error = Str_Error & "Please Valid Half Paid Days <Br>"
        End If

        If IsNumeric(txtProvdays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Maximum Carry Forward <Br>"
        End If
        If IsNumeric(txtActualDays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Maximum Encash Days <Br>"
        End If
        checkErrors = Str_Error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_error As String = checkErrors()
        If str_error <> "" Then
            lblError.Text = "Please check the Following : <br>" & str_error
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
             
            Dim retval As String = "1000"
            Dim BLS_ID As String
            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                'BLS_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                BLS_ID = 0
                Dim str_earncode As String = ""
                For Each item As ListItem In cblSplitup.Items
                    If (item.Selected) Then
                        If str_earncode <> "" Then
                            str_earncode = str_earncode & "|" & item.Value
                        Else
                            str_earncode = item.Value
                        End If
                    End If
                Next
                If str_earncode = "" Then
                    lblError.Text = "Cannot save. Please Select Atleast One Comopnents"
                    Exit Sub
                End If
                retval = PayrollFunctions.SaveBSU_GRATUITYSLAB_S(BLS_ID, Session("sbsuid"), txtRemarks.Text, _
                  txtFrom.Text, txtTo.Text, h_catid.Value, ddContracttype.SelectedItem.Value, _
                  ddTrantype.SelectedItem.Value, txtGrafrom.Text, txtGrato.Text, txtActualDays.Text, _
                   txtProvdays.Text, str_earncode, edit_bool, stTrans)

            End If
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                txtRemarks.Text & "-" & txtFrom.Text & "-" & txtTo.Text & "-" & _
                h_catid.Value & "-" & ddContracttype.SelectedItem.Value & "-" & ddTrantype.SelectedItem.Value _
                & "-" & txtGrafrom.Text & "-" & txtGrato.Text & "-" & txtActualDays.Text, _
                "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = "Cannot save. There is some errors"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            lblError.Text = "Cannot save. There is some errors"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

End Class
