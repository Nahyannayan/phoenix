Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports PayrollFunctions

Partial Class Payroll_empChangeModeofPay
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version        Author          Date            Change
    '1.1            Swapna          07-April-2011   adding conditions for changing accomodation to company /Transportation availing-Yes
    'ts 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P450030" _
            And ViewState("MainMnu_code") <> "P450035" And ViewState("MainMnu_code") <> "P450040") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Select Case ViewState("MainMnu_code").ToString
                    Case "P450030"
                        ViewState("str_doctype") = "pay"
                        lblHead.Text = "Mode Of Pay"
                        lblDetail.Text = "Mode of Pay History"
                        ddAccomodation.Visible = False
                        ddMode.Visible = True
                        ddTransport.Visible = False
                        tr_Bankaccount.Visible = True
                    Case "P450035"
                        lblHead.Text = "Accommodation Deatils"
                        ViewState("str_doctype") = "acc"
                        lblDetail.Text = "Accommodation History"
                        ddAccomodation.Visible = True
                        ddMode.Visible = False
                        ddTransport.Visible = False
                        tr_Bankaccount.Visible = False
                        lblText.Visible = True
                    Case "P450040"
                        lblHead.Text = "Avail Company Transport"
                        ViewState("str_doctype") = "tra"
                        lblDetail.Text = "Transport History"
                        ddAccomodation.Visible = False
                        ddMode.Visible = False
                        ddTransport.Visible = True
                        tr_Bankaccount.Visible = False
                        lblText.Visible = True
                End Select
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") <> "" Then
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If
        End If
    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_Sql As String
            str_Sql = "SELECT EST.EST_DTFROM, " _
                & " EST.EST_EMP_ID, EST.EST_DTTO, ES.EST_DESCR, " _
                & " EST.EST_CODE, EST.EST_REMARKS, EM.EMPNO, " _
                & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+ " _
                & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME, EM.EMP_MODE" _
                & " FROM EMPTRANTYPE_TRN AS EST INNER JOIN" _
                & " EMPSTATUS_M AS ES ON EST.EST_CODE = ES.EST_ID INNER JOIN" _
                & " EMPLOYEE_M AS EM ON EST.EST_EMP_ID = EM.EMP_ID LEFT OUTER JOIN" _
                & " EMPTRANTYPE_M AS TTM ON EST.EST_TTP_ID = TTM.TTP_ID" _
                & " WHERE (EST.EST_TTP_ID = 4) AND (EST.EST_BSU_ID = '" & Session("sBsuid") & "')" _
                & "  and est.est_id='" & p_Modifyid & "'"
            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then 
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EST_EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                ddMode.SelectedIndex = -1
                ddMode.Items.FindByValue(ds.Tables(0).Rows(0)("EST_CODE").ToString).Selected = True

                txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("EST_DTFROM")), "dd/MMM/yyyy")

                txtRemarks.Text = ds.Tables(0).Rows(0)("EST_REMARKS").ToString
            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
        imgEmployee.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
    End Sub

    Sub clear_All()
        txtFrom.Text = ""
        txtRemarks.Text = ""
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        h_Cat.Value = ""
        h_Dpt.Value = ""
        txtBname.Enabled = True
        btnBank_name.Enabled = True
        txtAccCode.Enabled = True
        bindgrid()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            Dim retval As String = "1000"
            Dim str_update_master As String = ""
            Dim str_str_code As String = ""
            Dim str_ttp_id As String = ""
            Select Case ViewState("MainMnu_code").ToString
                Case "P450030" 'mode of pay 
                    If txtFrom.Text = Nothing Then
                        lblError.Text = "Please select a date."
                        stTrans.Rollback()
                        Exit Sub
                    End If
                    If ddMode.SelectedItem.Value = "1" Then
                        If txtBname.Text = "" Or txtAccCode.Text = "" Then
                            stTrans.Rollback()
                            lblError.Text = "BAnk and Account Cannot be blank"
                            Exit Sub
                        End If
                    End If
                    If ddMode.SelectedItem.Value = "1" Then
                        retval = CheckAccountNumberValid()
                        If retval <> 0 Then
                            stTrans.Rollback()
                            lblError.Text = UtilityObj.getErrorMessage(retval)
                            Exit Sub
                        End If
                    End If

                    If hfBank_ID.Value <> ViewState("EMP_BANK") Then
                        retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "25", hfBank_ID.Value, _
                                    txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EMP_BANK"), False, stTrans)
                    End If

                    If txtAccCode.Text <> ViewState("EMP_ACCOUNT") Then
                        retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "24", txtAccCode.Text, _
                                    txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EMP_ACCOUNT"), False, stTrans)
                    End If
                    str_str_code = ddMode.SelectedItem.Value
                    str_update_master = " EMP_MODE='" & ddMode.SelectedItem.Value & "' ,EMP_ACCOUNT='" & txtAccCode.Text & "', EMP_BANK='" & hfBank_ID.Value & "'"
                    str_ttp_id = "6"
                Case "P450035"
                    'V1.1 starts
                    If CDate(txtFrom.Text) < CDate(hf_JoinDt.Value) Then
                        lblError.Text = "Date entered cannot be less than Employee's Joining date."
                        Exit Sub
                    End If
                    If Day(CDate(txtFrom.Text)) > 1 Then
                        lblError.Text = "Date entered must be first day of the month."
                        Exit Sub
                    End If
                    Dim IsSalProcessed As Boolean = CheckSalaryProcessed()
                    If IsSalProcessed Then
                        lblError.Text = "Request cannot be processed.Salary for selected month is already processed."
                        Exit Sub
                    End If
                    'V1.1 ends
                    str_str_code = ddAccomodation.SelectedItem.Value
                    str_update_master = " EMP_ACCOMODATION='" & ddAccomodation.SelectedItem.Value & "' "
                    str_ttp_id = "7"
                Case "P450040"
                    'V1.1 starts
                    If CDate(txtFrom.Text) < CDate(hf_JoinDt.Value) Then
                        lblError.Text = "Date entered cannot be less than Employee's Joining date."
                        Exit Sub
                    End If
                    If Day(CDate(txtFrom.Text)) > 1 Then
                        lblError.Text = "Date entered must be first day of the month."
                        Exit Sub
                    End If
                    Dim IsSalProcessed As Boolean = CheckSalaryProcessed()
                    If IsSalProcessed Then
                        lblError.Text = "Request cannot be processed.Salary for selected month is already processed."
                        Exit Sub
                    End If
                    'V1.1 ends
                    str_str_code = ddTransport.SelectedItem.Value
                    str_update_master = " EMP_bCompanyTransport='" & ddTransport.SelectedItem.Value & "' "
                    str_ttp_id = "8"
            End Select
            retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, str_ttp_id, str_str_code, _
              txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("mode"), False, stTrans)

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            h_Emp_No.Value & str_ttp_id & "-" & str_str_code & "-" & txtFrom.Text & "-" & _
            txtRemarks.Text.ToString, _
            "Edit", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            If retval = "0" Then
                Dim myCommand As New SqlCommand("UPDATE EMPLOYEE_M SET " & str_update_master & "WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.ExecuteNonQuery()
                'V1.1 addition
                If ((ViewState("str_doctype") = "acc" And ddAccomodation.SelectedValue = 0) Or (ViewState("str_doctype") = "tra" And ddTransport.SelectedValue = True)) Then

                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                    pParms(0).Value = h_Emp_No.Value 'for EmpID

                    pParms(1) = New SqlClient.SqlParameter("@ERN_ID", SqlDbType.VarChar, 10)
                    If (ViewState("str_doctype") = "acc") Then
                        pParms(1).Value = "HRA"

                    ElseIf (ViewState("str_doctype") = "tra") Then
                        pParms(1).Value = "TA"
                    End If
                    pParms(2) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                    pParms(2).Value = CDate(txtFrom.Text)

                    pParms(3) = New SqlClient.SqlParameter("@Returnvalue", SqlDbType.Bit)
                    pParms(3).Direction = ParameterDirection.ReturnValue


                    Dim adpt As New SqlDataAdapter
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        Dim cmd As New SqlCommand("UpdateSalaryERNStatus", conn)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(pParms(0))
                        cmd.Parameters.Add(pParms(1))
                        cmd.Parameters.Add(pParms(2))
                        cmd.Parameters.Add(pParms(3))
                        cmd.ExecuteNonQuery()
                    End Using
                    retval = pParms(3).Value
                End If
            End If

            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        bindgrid()
    End Sub
    'V1.1 addition
    Function CheckSalaryProcessed() As Boolean
        Dim ds As New DataSet
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
        pParms(0).Value = h_Emp_No.Value 'for EmpID

        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = txtFrom.Text

        pParms(2) = New SqlClient.SqlParameter("@Returnvalue", SqlDbType.Bit)
        pParms(2).Direction = ParameterDirection.ReturnValue


        Dim adpt As New SqlDataAdapter
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmd As New SqlCommand("ChkSalaryProcessed", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(pParms(0))
            cmd.Parameters.Add(pParms(1))
            cmd.Parameters.Add(pParms(2))
            cmd.ExecuteNonQuery()
        End Using
        Dim retval As Boolean = pParms(2).Value

        Return retval

    End Function
    Function CheckAccountNumberValid() As Integer
        Dim ds As New DataSet
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(0).Value = Session("sBsuid").ToString

        pParms(1) = New SqlClient.SqlParameter("@AccNumber", SqlDbType.VarChar)
        pParms(1).Value = txtAccCode.Text

        pParms(2) = New SqlClient.SqlParameter("@Returnvalue", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue


        Dim adpt As New SqlDataAdapter
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmd As New SqlCommand("ValidateAccountNumber", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(pParms(0))
            cmd.Parameters.Add(pParms(1))
            cmd.Parameters.Add(pParms(2))
            cmd.ExecuteNonQuery()
        End Using
        Dim retval As Integer = pParms(2).Value

        Return retval

    End Function
    Sub setEditdata()
        txtRemarks.Attributes.Remove("readonly")
        imgEmployee.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Protected Sub imgEmployee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        Try
            Dim str_Sql As String
            str_Sql = "SELECT EMP_ID, EMP_MODE,EMP_ACCOMODATION,EMP_bCompanyTransport,EMP.EMP_JOINDT, from employee_m" _
            & " WHERE ( EMP_BSU_ID = '" & Session("sBsuid") & "') AND ( EMP_ID = '" & h_Emp_No.Value & "')"
            str_Sql = "SELECT     EMP.EMP_ID,EMP.EMP_JOINDT, EMP.EMP_MODE, EMP.EMP_ACCOMODATION," _
                & " EMP.EMP_bCompanyTransport, EMP.EMP_BANK, EMP.EMP_ACCOUNT, " _
                & " BNK.BNK_DESCRIPTION FROM EMPLOYEE_M AS EMP LEFT OUTER JOIN" _
                & " BANK_M AS BNK ON EMP.EMP_BANK = BNK.BNK_ID" _
                & " WHERE (EMP.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EMP.EMP_ID = '" & h_Emp_No.Value & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Select Case ViewState("MainMnu_code").ToString
                    Case "P450030"
                        ViewState("mode") = ds.Tables(0).Rows(0)("EMP_MODE").ToString
                        ViewState("EMP_ACCOUNT") = ds.Tables(0).Rows(0)("EMP_ACCOUNT").ToString
                        ViewState("EMP_BANK") = ds.Tables(0).Rows(0)("EMP_BANK").ToString

                        ddMode.SelectedValue = ds.Tables(0).Rows(0)("EMP_MODE").ToString
                        txtAccCode.Text = ds.Tables(0).Rows(0)("EMP_ACCOUNT").ToString
                        txtBname.Text = ds.Tables(0).Rows(0)("BNK_DESCRIPTION").ToString
                        hfBank_ID.Value = ds.Tables(0).Rows(0)("EMP_BANK").ToString

                        If ddMode.SelectedItem.Text = "Cash" Then
                            txtBname.Enabled = False
                            btnBank_name.Enabled = False
                            txtAccCode.Enabled = False
                        Else
                            txtBname.Enabled = True
                            btnBank_name.Enabled = True
                            txtAccCode.Enabled = True
                        End If
                    Case "P450035"
                        ViewState("mode") = ds.Tables(0).Rows(0)("EMP_ACCOMODATION").ToString
                        hf_JoinDt.Value = ds.Tables(0).Rows(0)("EMP_JOINDT").ToString 'v1.1
                    Case "P450040"
                        ViewState("mode") = ds.Tables(0).Rows(0)("EMP_bCompanyTransport").ToString
                        hf_JoinDt.Value = ds.Tables(0).Rows(0)("EMP_JOINDT").ToString 'v1.1
                End Select
            Else
            End If
            bindgrid()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindgrid()
        Dim ds As New DataSet
        Dim str_Sql As String = ""
        Select Case ViewState("MainMnu_code").ToString
            Case "P450030"
                str_Sql = "SELECT     EST.EST_DTFROM, EST.EST_EMP_ID, EST.EST_DTTO," _
                       & " CASE ISNULL(EST.EST_CODE,0)  WHEN 0 THEN 'CASH'" _
                       & " WHEN 1 THEN 'BANK' END AS NEW ," _
                       & " CASE ISNULL(EST.EST_OLDCODE,EST.EST_CODE) " _
                       & " WHEN 0 THEN 'CASH'  WHEN 1 THEN 'BANK'" _
                       & " END AS OLD , EST.EST_CODE, EST.EST_REMARKS," _
                       & " EST.EST_ID, TTM.TTP_DESCR " _
                       & " FROM EMPTRANTYPE_TRN AS EST LEFT OUTER JOIN" _
                       & " EMPTRANTYPE_M AS TTM ON EST.EST_TTP_ID = TTM.TTP_ID" _
                       & " WHERE (EST.EST_TTP_ID = 6) AND (EST.EST_BSU_ID = '" & Session("sBsuid") & "')" _
                       & " AND EST.EST_EMP_ID='" & h_Emp_No.Value & "' order by EST_DTFROM desc"
            Case "P450035"
                str_Sql = "SELECT     EST_DTFROM," _
                      & " EST_EMP_ID, EST_DTTO, CASE ISNULL( EST_CODE, 0) " _
                      & " WHEN 0 THEN 'Own Accomodation' WHEN 1" _
                      & " THEN 'Company Single Accomodation' WHEN 2 " _
                      & " THEN 'Company Family Accomodation' END AS NEW, " _
                      & " CASE ISNULL( EST_OLDCODE,  EST_CODE) " _
                      & " WHEN 0 THEN 'Own Accomodation' WHEN 1 " _
                      & " THEN 'Company Single Accomodation' WHEN 2 " _
                      & " THEN 'Company Family Accomodation' END AS OLD, " _
                      & " EST_CODE, EST_REMARKS " _
                      & " FROM EMPTRANTYPE_TRN " _
                      & " WHERE (EST_TTP_ID = 7) AND (EST_BSU_ID = '" & Session("sBsuid") & "') " _
                      & " AND (EST_EMP_ID = '" & h_Emp_No.Value & "') order by EST_DTFROM desc"
            Case "P450040"
                str_Sql = "SELECT EST_DTFROM, EST_EMP_ID, EST_DTTO, CASE ISNULL( EST_CODE, '')" _
                    & " WHEN 'true' THEN 'Company Transport'" _
                    & " WHEN 'false' THEN 'Own Transport' end AS NEW," _
                    & " CASE ISNULL( EST_OLDCODE,  EST_CODE)" _
                    & " WHEN 'true' THEN 'Company Transport'" _
                    & " WHEN 'false' THEN 'Own Transport'  END AS OLD," _
                    & " EST_CODE, EST_REMARKS, EST_OLDCODE " _
                    & " FROM EMPTRANTYPE_TRN " _
                    & " WHERE (EST_TTP_ID = 8) AND (EST_BSU_ID = '" & Session("sBsuid") & "')" _
                    & " AND (EST_EMP_ID = '" & h_Emp_No.Value & "' ) order by EST_DTFROM desc"
        End Select
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        gvDetails.DataSource = ds
        gvDetails.DataBind()
    End Sub

    Protected Sub ddMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddMode.SelectedItem.Text = "Bank" Then
            txtBname.Enabled = True
            btnBank_name.Enabled = True
            txtAccCode.Enabled = True
        Else
            txtBname.Enabled = False
            btnBank_name.Enabled = False
            txtAccCode.Enabled = False
        End If
    End Sub

    Protected Sub h_Emp_No_ValueChanged(sender As Object, e As EventArgs)
        Try
            Dim str_Sql As String
            str_Sql = "SELECT EMP_ID, EMP_MODE,EMP_ACCOMODATION,EMP_bCompanyTransport,EMP.EMP_JOINDT, from employee_m" _
            & " WHERE ( EMP_BSU_ID = '" & Session("sBsuid") & "') AND ( EMP_ID = '" & h_Emp_No.Value & "')"
            str_Sql = "SELECT     EMP.EMP_ID,EMP.EMP_JOINDT, EMP.EMP_MODE, EMP.EMP_ACCOMODATION," _
                & " EMP.EMP_bCompanyTransport, EMP.EMP_BANK, EMP.EMP_ACCOUNT, " _
                & " BNK.BNK_DESCRIPTION FROM EMPLOYEE_M AS EMP LEFT OUTER JOIN" _
                & " BANK_M AS BNK ON EMP.EMP_BANK = BNK.BNK_ID" _
                & " WHERE (EMP.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EMP.EMP_ID = '" & h_Emp_No.Value & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Select Case ViewState("MainMnu_code").ToString
                    Case "P450030"
                        ViewState("mode") = ds.Tables(0).Rows(0)("EMP_MODE").ToString
                        ViewState("EMP_ACCOUNT") = ds.Tables(0).Rows(0)("EMP_ACCOUNT").ToString
                        ViewState("EMP_BANK") = ds.Tables(0).Rows(0)("EMP_BANK").ToString

                        ddMode.SelectedValue = ds.Tables(0).Rows(0)("EMP_MODE").ToString
                        txtAccCode.Text = ds.Tables(0).Rows(0)("EMP_ACCOUNT").ToString
                        txtBname.Text = ds.Tables(0).Rows(0)("BNK_DESCRIPTION").ToString
                        hfBank_ID.Value = ds.Tables(0).Rows(0)("EMP_BANK").ToString

                        If ddMode.SelectedItem.Text = "Cash" Then
                            txtBname.Enabled = False
                            btnBank_name.Enabled = False
                            txtAccCode.Enabled = False
                        Else
                            txtBname.Enabled = True
                            btnBank_name.Enabled = True
                            txtAccCode.Enabled = True
                        End If
                    Case "P450035"
                        ViewState("mode") = ds.Tables(0).Rows(0)("EMP_ACCOMODATION").ToString
                        hf_JoinDt.Value = ds.Tables(0).Rows(0)("EMP_JOINDT").ToString 'v1.1
                    Case "P450040"
                        ViewState("mode") = ds.Tables(0).Rows(0)("EMP_bCompanyTransport").ToString
                        hf_JoinDt.Value = ds.Tables(0).Rows(0)("EMP_JOINDT").ToString 'v1.1
                End Select
            Else
            End If
            bindgrid()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
