<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empGratuvityslab.aspx.vb" Inherits="Payroll_empGratuvityslab" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" EnableViewState="false" >
<script language="javascript" type="text/javascript">
function showhide_leavetype(id)
 { 
    if  (document.getElementById(id).className+''=='display_none')
    {
    document.getElementById(id).className='';
    }
    else
    {
    document.getElementById(id).className='display_none';
    }   
 }
function getPageCode(mode) 
 {     
            var sFeatures;
            sFeatures="dialogWidth: 460px; ";
            sFeatures+="dialogHeight: 370px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: no; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
           <%-- var url;
        
            url='empShowMasterEmp.aspx?id='+mode;
          if(mode=='CT')
             {
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {   
             return false; 
                 } 
            NameandCode = result.split('___');  
            document.getElementById("<%=txtCategory.ClientID %>").value=NameandCode[0];
              document.getElementById("<%=h_catid.ClientID %>").value=NameandCode[1];
             }
        }--%>
    if (mode == 'CT') {
        var url = "empShowMasterEmp.aspx?id=" + mode;
        var oWnd = radopen(url, "pop_pagecode");
    }

    
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
               document.getElementById("<%=txtCategory.ClientID %>").value=NameandCode[0];
              document.getElementById("<%=h_catid.ClientID %>").value=NameandCode[1];
                __doPostBack('<%=txtCategory.ClientID%>', 'TextChanged');
            }
        }



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_pagecode" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Gratuity Rule
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table align="center" width="100%">
     <tr><td  colspan="4" align="left" width="100%">
                    <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                    </td></tr></table>
 <table align="center" width="100%">            
            
             <tr>
                <td align="left" width="20%">
                    <span class="field-label">Select Category</span></td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="imgCategory" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('CT');return false;" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCategory"
                        CssClass="error" ErrorMessage="Please Select an Category" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                 <td width="20%"></td>
                 <td width="30%"></td>
            </tr>
       <tr >
         <td align="left">
             <span class="field-label">Select Period for Which the Leave Rule is Applicable</span></td>
         <td align="left">
             <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFrom"
                 CssClass="error" ErrorMessage="Please Select From date" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
         <td align="left"><span class="field-label">To</span>
             </td>
          <td align="left">
              <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" /></td> 
     </tr>
      
     <tr >
         <td align="left"  width="25%"> <span class="field-label">Contract Type</span></td>
          <td align="left"  width="25%">
              <asp:DropDownList ID="ddContracttype" runat="server">
                  <asp:ListItem Value="L">Limited</asp:ListItem>
                  <asp:ListItem Value="U">Unlimited</asp:ListItem>
              </asp:DropDownList></td> 
          <td align="left"  width="25%" > <span class="field-label">Tranasaction Type</span></td>
         <td align="left" width="25%"  >
             <asp:DropDownList ID="ddTrantype" runat="server">
                 <asp:ListItem Value="Terminated">Termination</asp:ListItem>
                 <asp:ListItem Value="Resignation">Resignation </asp:ListItem>
             </asp:DropDownList></td>
     </tr>
     <tr >
         <td align="left"> <span class="field-label">Setting for (eg. 0-364)</span>
         </td>
          <td align="left">
              <asp:TextBox ID="txtGrafrom" runat="server"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtGrafrom"
                  CssClass="error" ErrorMessage="Please Gratuity settings From " ValidationGroup="main">*</asp:RequiredFieldValidator></td>
         <td align="left">
              <span class="field-label">To</span></td>
         <td align="left">
              <asp:TextBox ID="txtGrato" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtGrato"
                 CssClass="error" ErrorMessage="Please Gratuity settings To" ValidationGroup="main">*</asp:RequiredFieldValidator></td> 
     </tr>
     <tr>
         <td align="left"  >
             <span class="field-label">Provision Days</span></td>
         <td align="left"  >
             <asp:TextBox ID="txtProvdays" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtProvdays"
                 CssClass="error" ErrorMessage="Please Enter number of Provision days" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
         <td align="left"  >
             <span class="field-label">Actual Days</span></td>
         <td align="left"  >
             <asp:TextBox ID="txtActualDays" runat="server" Width="112px"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtActualDays"
                 CssClass="error" ErrorMessage="Please Enter Number of Actual Days" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
     </tr>
     <tr><td colspan="4">&nbsp;</td></tr>
      <tr>
                <td colspan="4" class="title-bg" align="left">
                    <span class="field-label">Select Components</span></td>                
            </tr>
     <tr>
     
         <td align="center"  colspan="4">
             <asp:CheckBoxList ID="cblSplitup" runat="server" DataSourceID="listSplit" DataTextField="ERN_DESCR"
                 DataValueField="ERN_ID" RepeatColumns="4" RepeatDirection="Horizontal" Width="100%">
             </asp:CheckBoxList><asp:SqlDataSource ID="listSplit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                 SelectCommand="SELECT ERN_ID, ERN_DESCR FROM EMPSALCOMPO_M WHERE (ERN_TYP = 1)">
             </asp:SqlDataSource>
             </td>         
                      
    
     </tr>
     <tr>
                <td align="left">
                   <span class="field-label">Remarks</span> </td>
                <td align="left">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine" Width="472px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtRemarks"
                        CssClass="error" ErrorMessage="Please Select From date" ValidationGroup="main">*</asp:RequiredFieldValidator>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT [ELT_ID], [ELT_DESCR] FROM [EMPLEAVETYPE_M]"></asp:SqlDataSource>
                    
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="main" />
                </td>
         <td>
         </td>
         <td></td>
     </tr>
            <tr>
                <td colspan="4" align="center">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" UseSubmitBehavior="False" ValidationGroup="main" /><asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
        </table>
  <asp:HiddenField ID="h_catid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgTo" TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
