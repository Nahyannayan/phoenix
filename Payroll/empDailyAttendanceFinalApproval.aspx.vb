Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Imports Telerik.Web.UI

Partial Class Payroll_empDailyAttendanceFinalApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim NotProcessed As Drawing.Color = Drawing.Color.FromArgb(240, 217, 113)
    Private Property EmpAttDates() As DataTable
        Get
            Return ViewState("EmpAttDates")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EmpAttDates") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("datamode") = "add"
                Dim CAT_ID As String = Request.QueryString("CAT_ID")
                Dim vTYPE As String = "0"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'P170020
                btnSave.Enabled = False
                btnSave.Visible = False
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Request.UrlReferrer.PathAndQuery.Contains("empDailyAttendance_FinalApproval") And Session("ATT_APPR_FROMDT") IsNot Nothing Then
                        UsrFromDate.SelectedDate = Format(CDate(Session("ATT_APPR_FROMDT")), OASISConstants.DateFormat)
                        UsrToDate.SelectedDate = Format(CDate(Session("ATT_APPR_TODT")), OASISConstants.DateFormat)
                        CreateDataSetWithDates(UsrFromDate.SelectedDate, UsrToDate.SelectedDate)
                    Else
                        UsrFromDate.SelectedDate = Format(CDate(DateTime.Now.Date).AddMonths(-1), OASISConstants.DateFormat)
                        UsrToDate.SelectedDate = Format(CDate(DateTime.Now.Date), OASISConstants.DateFormat)
                        gridbind()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub gridbind()
        CreateDataSetWithDates(UsrFromDate.SelectedDate, UsrToDate.SelectedDate)
        RadEmpAttendance.DataSource = Nothing
        RadEmpAttendance.DataBind()
        lblViewDetails.Text = "View Details"
    End Sub
    Protected Sub RadEmpAttendance_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs)
        Try
            Dim lblDate As Label
            Dim vDate As DateTime
            Dim RadEmpAttDetail As RadGrid
            lblDate = e.Item.FindControl("lblAttDate")
            RadEmpAttDetail = e.Item.FindControl("RadEmpAttDetail")

            If Not lblDate Is Nothing AndAlso Not RadEmpAttDetail Is Nothing Then
                vDate = CDate(lblDate.Text)
                lblDate.Text = lblDate.Text & " - " + GetDay_From_DateOFWeek(vDate.DayOfWeek)
                BindAttendanceSummary(vDate, vDate, RadEmpAttDetail)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindStatus(ByVal ddlStatus As DropDownList)
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT ISNULL(EAS_ELT_ID, EAS_DESCR) EAS_ELT_ID, EAS_DESCR FROM EMPATTENDANCE_STATUS " & _
        "UNION ALL" & _
        " SELECT 'PRESENT', 'PRESENT' "
        '" UNION ALL SELECT 'ABSENT', 'ABSENT' "
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        ddlStatus.DataTextField = "EAS_DESCR"
        ddlStatus.DataValueField = "EAS_ELT_ID"
        ddlStatus.DataSource = ds.Tables(0)
        ddlStatus.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim transaction As SqlTransaction
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        transaction = conn.BeginTransaction("SampleTransaction")
        Dim str_errMsg As String
        Dim i As Integer = SaveEMPAttendanceApproval(conn, transaction, str_errMsg)
        If i = 0 Then
            transaction.Commit()
            lblError.Text = "Data Saved Successfully"
        Else
            transaction.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(i) & str_errMsg
        End If
    End Sub
    Private Function SaveEMPAttendanceApproval(ByVal conn As SqlConnection, ByVal transaction As SqlTransaction, ByRef vErrorMsg As String) As Integer
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer

        cmd = New SqlCommand("SAVE_EMPATTENDANCE_APPROVAL", conn, transaction)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpEAA_BSU_ID As New SqlParameter("@EAA_BSU_ID", SqlDbType.VarChar)
        sqlpEAA_BSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpEAA_BSU_ID)

        Dim sqlpEAA_DATE As New SqlParameter("@EAA_DATE", SqlDbType.DateTime)
        sqlpEAA_DATE.Value = UsrToDate.SelectedDate
        cmd.Parameters.Add(sqlpEAA_DATE)

        Dim sqlpEAA_FROMDT As New SqlParameter("@EAA_FROMDT", SqlDbType.DateTime)
        sqlpEAA_FROMDT.Value = UsrFromDate.SelectedDate
        cmd.Parameters.Add(sqlpEAA_FROMDT)

        Dim sqlpEAA_TODT As New SqlParameter("@EAA_TODT", SqlDbType.DateTime)
        sqlpEAA_TODT.Value = UsrToDate.SelectedDate
        cmd.Parameters.Add(sqlpEAA_TODT)

        Dim sqlpEAA_APPROVEDBY As New SqlParameter("@EAA_APPROVEDBY", SqlDbType.VarChar)
        sqlpEAA_APPROVEDBY.Value = Session("sUsr_name")
        cmd.Parameters.Add(sqlpEAA_APPROVEDBY)

        Dim sqlpEAA_REMARKS As New SqlParameter("@EAA_REMARKS", SqlDbType.VarChar)
        sqlpEAA_REMARKS.Value = "Attendance Approved by " & Session("sUsr_name")
        cmd.Parameters.Add(sqlpEAA_REMARKS)

        Dim sqlpSPRVSR_PENDING As New SqlParameter("@SPRVSR_PENDING", SqlDbType.VarChar, 5000)
        sqlpSPRVSR_PENDING.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpSPRVSR_PENDING)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        vErrorMsg = sqlpSPRVSR_PENDING.Value
        If iReturnvalue <> 0 Then
            lblError.Text = "Error occured while updating data"
        End If
        Return iReturnvalue
    End Function

    Protected Sub btnListData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListData.Click
        gridbind()
    End Sub

    Sub BindAttendanceSummary(ByVal vFromDT As Date, ByVal vToDT As Date, ByVal RadEmpAttendanceDetails As RadGrid)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(8) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlClient.SqlParameter("@FROMDT", Format(vFromDT.Date, "dd/MMM/yyyy"))
        param(2) = New SqlClient.SqlParameter("@TODT", Format(vToDT.Date, "dd/MMM/yyyy"))
        param(3) = New SqlClient.SqlParameter("@CAT_ID", 0)
        param(4) = New SqlClient.SqlParameter("@TYPE", 0)
        param(5) = New SqlClient.SqlParameter("@REP_EMP_ID", 0)
        param(6) = New SqlClient.SqlParameter("@ELT_ID", "All")
        param(7) = New SqlClient.SqlParameter("@ProcessStatus", 0)
        param(8) = New SqlClient.SqlParameter("@Summary", 1)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_EMP_ATT_DASHBOARD_DETAILS_NEW", param)
        RadEmpAttendanceDetails.DataSource = ds
        RadEmpAttendanceDetails.DataBind()
    End Sub
    Sub CreateDetailDataSetWithDates()
        If EmpAttDates IsNot Nothing Then
            RadEmpAttendance.DataSource = EmpAttDates
            RadEmpAttendance.DataBind()
        End If
    End Sub

    Sub CreateDataSetWithDates(ByVal vFromDT As Date, ByVal vToDT As Date)
        Dim dtDt As New DataTable
        Dim BSU_WEEKEND1 As String = ""
        Dim BSU_WEEKEND2 As String = ""

        Try
            Dim cDT As New DataColumn("DT", System.Type.GetType("System.DateTime"))
            dtDt.Columns.Add(cDT)
            Dim drRow As DataRow
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim str_sql As String = " SELECT BSU_WEEKEND1, BSU_WEEKEND2 FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"

            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            While (drReader.Read())
                BSU_WEEKEND1 = drReader("BSU_WEEKEND1").ToString
                BSU_WEEKEND2 = drReader("BSU_WEEKEND2").ToString
            End While

            While (vFromDT <= vToDT)
                If vFromDT.DayOfWeek() = GetDateOFWeek(BSU_WEEKEND1) OrElse _
                vFromDT.DayOfWeek() = GetDateOFWeek(BSU_WEEKEND2) Then
                    vFromDT = vFromDT.AddDays(1)
                    Continue While
                End If
                drRow = dtDt.NewRow()
                drRow(0) = vFromDT
                dtDt.Rows.Add(drRow)
                vFromDT = vFromDT.AddDays(1)
            End While
            If dtDt.Rows.Count > 0 Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
            btnSave.Enabled = True
            EmpAttDates = dtDt

            Dim dtSummary As New DataTable
            Dim cDTRANGE As New DataColumn("DT", System.Type.GetType("System.String"))
            dtSummary.Columns.Add(cDTRANGE)

            drRow = dtSummary.NewRow
            drRow(0) = Format(CDate(UsrFromDate.SelectedDate), OASISConstants.DateFormat) & " - " & Format(CDate(UsrToDate.SelectedDate), OASISConstants.DateFormat)
            dtSummary.Rows.Add(drRow)

            RadEmpAttendanceSummary.DataSource = dtSummary
            RadEmpAttendanceSummary.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Private Function GetDateOFWeek(ByVal day As String) As System.DayOfWeek
        Select Case day.ToUpper
            Case "FRIDAY"
                Return DayOfWeek.Friday
            Case "SATURDAY"
                Return DayOfWeek.Saturday
        End Select
    End Function

    Private Function GetDay_From_DateOFWeek(ByVal day As System.DayOfWeek) As String
        Select Case day
            Case DayOfWeek.Sunday
                Return "Sunday"
            Case DayOfWeek.Monday
                Return "Monday"
            Case DayOfWeek.Tuesday
                Return "Tuesday"
            Case DayOfWeek.Wednesday
                Return "Wednesday"
            Case DayOfWeek.Thursday
                Return "Thursday"
            Case DayOfWeek.Friday
                Return "Friday"
            Case DayOfWeek.Saturday
                Return "Saturday"
        End Select
    End Function

    Sub BindEmployeeAttDetail(ByVal vDate As DateTime, ByVal gvEmpAtt As RadGrid)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlClient.SqlParameter("@FROMDT", vDate.Date)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_EMP_ATT_APPROVAL", param)

        gvEmpAtt.DataSource = ds
        gvEmpAtt.DataBind()

    End Sub

    Protected Sub RadEmpAttendanceSummary_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs)
        Dim RadEmpAttendanceDetails As RadGrid
        RadEmpAttendanceDetails = e.Item.FindControl("RadEmpAttendanceDetails")
        If Not RadEmpAttendanceDetails Is Nothing Then
            BindAttendanceSummary(UsrFromDate.SelectedDate, UsrToDate.SelectedDate, RadEmpAttendanceDetails)
        End If
    End Sub

    Protected Sub lbCAT_DESCR_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblAttCAT_ID, lblAttDetDate As New Label
            Dim url As String = String.Empty
            lblAttCAT_ID = TryCast(sender.FindControl("lblAttCAT_ID"), Label)
            lblAttDetDate = TryCast(sender.FindControl("lblAttDetDate"), Label)
            Dim Type As Int16
            Select Case sender.ID
                Case "lblAttNO_LEAVES"
                    Type = 1
                Case "lblAttEMP_Present"
                    Type = 2
                Case Else
                    Type = 0
            End Select
            Session("ATT_APPR_FROMDT") = UsrFromDate.SelectedDate
            Session("ATT_APPR_TODT") = UsrToDate.SelectedDate
            If Not lblAttCAT_ID Is Nothing AndAlso Not lblAttCAT_ID Is Nothing Then
                Dim vDATE As String = encrData.Encrypt(lblAttDetDate.Text)
                url = String.Format("{0}?CAT_ID={1}&DATE={3}&MainMnu_code={2}&Type={4}", "empMarkDailyAttendance.aspx", lblAttCAT_ID.Text, Request.QueryString("MainMnu_code"), vDATE, Type.ToString)
                Response.Redirect(url)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lbCAT_DESCR_NEW_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblCAT_ID, lblAttDetDate As New Label
            Dim url As String = String.Empty
            lblCAT_ID = TryCast(sender.FindControl("lblCAT_ID"), Label)
            lblAttDetDate = TryCast(sender.FindControl("lblDate"), Label)
            Dim Type As Int16
            Select Case sender.ID
                Case "lbNO_LEAVES"
                    Type = 1
                Case "lbEMP_Present"
                    Type = 2
                Case Else
                    Type = 0
            End Select
            Session("ATT_APPR_FROMDT") = UsrFromDate.SelectedDate
            Session("ATT_APPR_TODT") = UsrToDate.SelectedDate
            Dim FromDt, ToDt As DateTime
            If EmpAttDates IsNot Nothing Then
                FromDt = EmpAttDates.Compute("min(DT)", "")
                ToDt = EmpAttDates.Compute("max(DT)", "")
            End If
            If Not lblCAT_ID Is Nothing Then
                Dim FromDtStr As String = encrData.Encrypt(FromDt.ToString("dd/MMM/yyyy"))
                Dim ToDtStr As String = encrData.Encrypt(ToDt.ToString("dd/MMM/yyyy"))
                url = String.Format("{0}?CAT_ID={1}&FROMDATE={3}&TODATE={4}&MainMnu_code={2}&Type={5}", "empDailyAttendanceCorrection.aspx", lblCAT_ID.Text, Request.QueryString("MainMnu_code"), FromDtStr, ToDtStr, Type.ToString)
                Response.Redirect(url)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lblViewDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblViewDetails.Click
        If lblViewDetails.Text = "Hide Details" Then
            RadEmpAttendance.DataSource = Nothing
            RadEmpAttendance.DataBind()
            lblViewDetails.Text = "View Details"
        Else
            CreateDetailDataSetWithDates()
            lblViewDetails.Text = "Hide Details"
        End If

    End Sub
End Class
