<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empEmpCategortnDpt.aspx.vb" Inherits="Payroll_empEmpCategortnDpt" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" EnableViewState="false" >
<script language="javascript" type="text/javascript">
   function GetEMPName()
       {    
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN","", sFeatures)
       <%--if(result != '' && result != undefined)
            {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1]; 
                document.getElementById('<%=txtEmpNo.ClientID %>').value=NameandCode[0]; 
                
                return true;
            }
            return false;--%>

       var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
       var oWnd = radopen(url, "pop_employee");
   }

    function OnClientClose1(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {

            NameandCode = arg.NameandCode.split('||');
            document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
    }

    function getPageCode(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 460px; ";
        sFeatures += "dialogHeight: 370px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: no; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;

        // url='empShowMasterEmp.aspx?id='+mode;

        document.getElementById("<%=hf_SearchMode.ClientID%>").value = mode;
        var url = "empShowMasterEmp.aspx?id=" + mode;
        var oWnd = radopen(url, "pop_pagecode");

    }

            function OnClientClose2(oWnd, args) {
                //get the transferred arguments
                var SearchMode = document.getElementById("<%=hf_SearchMode.ClientID%>").value

                var arg = args.get_argument();

                if (arg) {

                    NameandCode = arg.NameandCode.split('||');
                    if(SearchMode=='D')
                    {
                        //result = window.showModalDialog(url,"", sFeatures);
                                 
                        //if (result=='' || result==undefined)
                        //{   
                        //    return false; 
                        //} 
                        //NameandCode = result.split('||');  
                        document.getElementById("<%=txtNDpt.ClientID %>").value=NameandCode[0];
                        document.getElementById("<%=h_Dpt.ClientID %>").value=NameandCode[1];
                    }
             
                    else if(SearchMode=='G')
                    {
                        //result = window.showModalDialog(url,"", sFeatures);
                                 
                        //if (result=='' || result==undefined)
                        //{   
                        //    return false; 
                        //} 
                        //NameandCode = result.split('||');  
                        document.getElementById("<%=txtNGrade.ClientID %>").value=NameandCode[0];
                        document.getElementById("<%=h_Grade.ClientID %>").value=NameandCode[1];
                    } 
                    else if(SearchMode=='CT')
                    {
                        //result = window.showModalDialog(url,"", sFeatures);
                                 
                        //if (result=='' || result==undefined)
                        //{   
                        //    return false; 
                        //} 
                        //NameandCode = result.split('||');  
                        document.getElementById("<%=txtNCat.ClientID %>").value=NameandCode[0];
                        document.getElementById("<%=h_Cat.ClientID %>").value=NameandCode[1];
                    }  
            
                }
            }

       function clears(id)
       { 
           if (id=='1')
           document.getElementById("<%=txtNDpt.ClientID %>").value='';
           else  if (id=='2')
           document.getElementById("<%=txtNGrade.ClientID %>").value='';
           else  if (id=='3')
           document.getElementById("<%=txtNCat.ClientID %>").value='';
       }





    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }


    </script>


     <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_pagecode" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Change Department/Grade/Category
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table  align="center" width="100%"><tr><td  colspan="4" align="left" width="100%">
                    <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                    </td></tr></table>
 <table width="100%">            
            
             <tr>
                <td align="left" width="25%">
                    <span class="field-label">Select Employee</span></td>
                <td align="left" width="25%">
                    <asp:TextBox ID="txtEmpNo" runat="server" ></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                        CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator></td>
                <td width="25%"></td>
                 <td width="25%"></td>
             </tr>
     <tr>
         <td align="left"  >
             <span class="field-label">Current Department</span></td>
        <td align="left" >
             <asp:TextBox ID="txtDpt" runat="server"></asp:TextBox>
         </td>
         <td align="left">
             <span class="field-label">New Department</span></td>
         <td align="left">
             <asp:TextBox ID="txtNDpt" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgVisa" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('D');return false;" />
             <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="javascript:clears('1');return false;">Clear</asp:LinkButton></td>
     </tr>
     <tr>
         <td align="left" width="25%" ><span class="field-label">Grade</span></td>
         <td align="left" width="25%" >
             <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
         </td>
         <td align="left" width="25%"><span class="field-label">Grade</span></td>
         <td align="left" width="25%">
             <asp:TextBox ID="txtNGrade" runat="server"></asp:TextBox>
             <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('G');return false;" />
             <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="javascript:clears('2');return false;">Clear</asp:LinkButton></td>
     </tr>
     <tr>
         <td align="left" width="25%" >
             <span class="field-label">Current Category</span></td>
         <td align="left" width="25%" >
             <asp:TextBox ID="txtCat" runat="server"></asp:TextBox>
         </td>
         <td align="left" width="25%">
             <span class="field-label">New Category</span></td>
         <td align="left" width="25%">
             <asp:TextBox ID="txtNCat" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgEmp" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('CT');return false;" />
             <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="javascript:clears('3');return false;">Clear</asp:LinkButton>
             </td>
     </tr>
     <tr>
         <td align="left" width="25%">
             <span class="field-label">Current ABC Category</span></td>
         <td align="left" width="25%">
             <asp:TextBox ID="txtABC" runat="server"></asp:TextBox></td>
         <td align="left" width="25%">
             <span class="field-label">New ABC Category</span></td>
         <td align="left" width="25%">
             &nbsp;<asp:DropDownList ID="ddABC" runat="server">
                 <asp:ListItem Value="N">No Change</asp:ListItem>
                 <asp:ListItem>A</asp:ListItem>
                 <asp:ListItem>B</asp:ListItem>
                 <asp:ListItem>C</asp:ListItem>
             </asp:DropDownList></td>
     </tr>
       <tr >
         <td align="left" width="25%">
             <span class="field-label">Select a Period</span></td>
         <td align="left" width="25%">
             <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td> 
           <td width="25%"></td>
           <td width="25%"></td>
     </tr>
     <tr>
                <td align="left">
                    <span class="field-label">Remarks</span></td>
                <td colspan="3" align="left">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine" Width="472px"></asp:TextBox></td>
     </tr>
     <tr class="subheader_img" runat="server" id="tr_Deatailhead"><td colspan="4" class="title-bg" align="left" >
         Employee History</td></tr>                      
            <tr runat="server" id="tr_Deatails"> 
            <td colspan="4" align="center" valign="top" >
                <asp:GridView ID="gvDetails" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="TTP_DESCR" HeaderText="Description" InsertVisible="False" ReadOnly="True" >
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EST_DTFROM" HeaderText="From Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EST_DTTO" HeaderText="To Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EST_REMARKS" HeaderText="Remarks" >
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="OLD" HeaderText="Old Designation" >
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NEW" HeaderText="New Designation" >
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField> 
                    </Columns>
                </asp:GridView>
                </td>
            </tr>     
            <tr>
                <td colspan="5" align="center">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
        </table>
    <asp:HiddenField ID="h_Emp_No" runat="server" />
    <asp:HiddenField ID="h_Cat" runat="server" />
    <asp:HiddenField ID="h_Dpt" runat="server" />
    <asp:HiddenField ID="h_Grade" runat="server" />
    <asp:HiddenField ID="hf_SearchMode" runat="server" />
     <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtFrom" TargetControlID="txtFrom">

    </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>

</asp:Content>
