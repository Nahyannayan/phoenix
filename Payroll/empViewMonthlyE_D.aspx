<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empViewMonthlyE_D.aspx.vb" Inherits="Accounts_accccViewCreditcardReceipt" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }
        window.onload = scroll_page;
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr valign="top">
                        <td class="matters" valign="top" align="left" colspan="2">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td width="100%">
                            <asp:GridView ID="gvEMPMONTHLYDEDUCTIONS" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                Width="100%" AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <HeaderTemplate>
                                            Employee No.<br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Employee Name<br />
                                            <asp:TextBox ID="txtEmpName" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                         <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MONTH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMONTH" runat="server" Text='<%# Bind("EMP_MONTH") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Month<br />
                                            <asp:TextBox ID="txtMonth" runat="server"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="YEAR">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblYEAR" runat="server" Text='<%# Bind("EDD_PAYYEAR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Year<br />
                                            <asp:TextBox ID="txtYEAR" runat="server"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Bind("EDD_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtAmount" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearchs" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                             
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False" HeaderText="View">
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>&nbsp;&nbsp;
                                        </ItemTemplate>
                                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EDD_EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <a id='detail'></a>
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
