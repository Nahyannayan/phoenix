Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Telerik.Web.UI

'Version        Author               Date            Purpose
'1.1            Swapna             4/Jan/2011      To get/save WPS agent details
'1.2            Prem                                changed query
'1.3            Swapna             1/aug/2011   To add additional fields in HR screen
'1.4            Swapna             12/Sep/2011   To add insurance card types
'1.5            Swapna              29/sep/2011   To add default vacation policy and personal email id fields
'1.6            Swapna              29/mar/2012     To add cost center
'2.0            Swapna              3/dec/2012      To add airticket/insurance changes- separated tab personal/benefits 
'2.1            swapna              27/Feb/2013     To display earning types bsu wise.
'2.2            Arun G              10/Mar/2013     Modified Qualification tab
'2.3            Jacob C             04/Sep/2013     Modified Personal tab to include EmiratesID details
'3.0           Swapna              06/Aug/2014     Hiding and setting controls based on if country is UAE or Non- UAE
'3.1           Swapna/Rajesh        22-Nov-2015     Visa/LC documents validation moved to sp - tempporary ,part-time visa types added , Rajesh- photo upload size increased.
'3.2            Rajesh              15-Dec-2015     Added UID no in Visa tab and Modified Personal Tab to include dependance visa details 
'---------------------------------------------------------------------------------------
Partial Class Payroll_empMasterDetail
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    Sub ImgHeightnWidth(ByVal strImgPath As String)
        Dim actWidth, actHeight, imgWith, imgHeight As Integer
        Dim i As System.Drawing.Image = System.Drawing.Image.FromFile(strImgPath)
        imgWith = 202 'imgEmpImage.Width.Value
        imgHeight = 189 'imgEmpImage.Height.Value
        actWidth = i.Width
        actHeight = i.Height
        i.Dispose()
        Dim itempUnit As Integer
        If actWidth < imgWith And actHeight < imgHeight Then
            imgEmpImage.Height = actHeight
            imgEmpImage.Width = actWidth
            Exit Sub
        End If
        If actWidth < actHeight Then
            itempUnit = actWidth * imgHeight / actHeight
            If (imgWith > itempUnit) Then
                imgEmpImage.Width = itempUnit
            End If
        Else
            itempUnit = actHeight * imgWith / actWidth
            If (imgHeight > itempUnit) Then
                imgEmpImage.Height = itempUnit
            End If
        End If
    End Sub

    Private Sub SetPanelEnabled(ByVal index As Integer, ByVal enable As Boolean)
        Select Case index
            Case 0
                pnlMain.Enabled = enable
            Case 1
                'If enable Then
                '    If Session("sBsuid") = hfIU.Value Then
                pnlDocDetails.Enabled = enable
                '    End If
                'Else
                '    pnlDocDetails.Enabled = enable
                'End If
            Case 2
                pnlQualificationDet.Enabled = enable
            Case 3
                pnlExperience.Enabled = enable
            Case 4
                pnlCommunicationDet.Enabled = enable
            Case 5
                pnlBenifits.Enabled = enable
            Case 6
                pnlSalary.Enabled = enable
            Case 7
                pnlNewBenefits.Enabled = enable  'V2.0
            Case 8
                pnlOthers.Enabled = enable  'V2.0
            Case 9
                pnlVisaDetails.Enabled = enable  'V2.0
        End Select
    End Sub

    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuMaster.MenuItemClick
        Dim Iindex As Integer = Int32.Parse(e.Item.Value)
        h_PASSPORTNO.Value = txtPassNo.Text
        Dim i As Integer = -1
        '   setmvMaster_ActiveViewIndex(Iindex)
        Select Case Iindex
            Case 0
                i = 0
            Case 1
                i = 4
            Case 2
                i = 5
            Case 3
                i = 6
            Case 4
                i = 2
            Case 5
                i = 1
            Case 6
                i = 3
            Case 7
                i = 8
            Case 8
                i = 7
            Case 9
                i = 9
        End Select
        mvMaster.ActiveViewIndex = i
        mnuMaster.Items(i).Selectable = True
        Call menuShow()
        FillSelectedData()
    End Sub

    Public Shared Function GetDate(ByVal vDate As String) As String
        If vDate = "" OrElse CDate(vDate) = New Date(1900, 1, 1) Then
            Return "-"
        Else
            Return Format(CDate(vDate), OASISConstants.DateFormat)
        End If
    End Function

    Sub setmvMaster_ActiveViewIndex(ByVal sel_ActiveViewIndex As Integer)
        'mnuMaster.Items(0).ImageUrl = "~/Images/tabmenu/main.jpg"
        'mnuMaster.Items(1).ImageUrl = "~/Images/tabmenu/visa.jpg"
        'mnuMaster.Items(2).ImageUrl = "~/Images/tabmenu/qua.jpg"
        'mnuMaster.Items(3).ImageUrl = "~/Images/tabmenu/exp.jpg"
        'mnuMaster.Items(4).ImageUrl = "~/Images/tabmenu/ct.jpg"
        'mnuMaster.Items(5).ImageUrl = "~/Images/tabmenu/per.jpg" '"~/Images/tabmenu/ad.jpg"
        'mnuMaster.Items(6).ImageUrl = "~/Images/tabmenu/sal.jpg"
        'mnuMaster.Items(7).ImageUrl = "~/Images/tabmenu/ben.jpg"  'V2.0
        'mnuMaster.Items(8).ImageUrl = "~/Images/tabmenu/oth.jpg"  'V2.0
        'mnuMaster.Items(9).ImageUrl = "~/Images/tabmenu/doc.jpg"

        mvMaster.ActiveViewIndex = sel_ActiveViewIndex
        Session("mvMaster_ActiveViewIndex") = mvMaster.ActiveViewIndex
        'Select Case mvMaster.ActiveViewIndex
        '    Case 0
        '        mnuMaster.Items(0).ImageUrl = "~/Images/tabmenu/mainr.jpg"
        '    Case 1
        '        mnuMaster.Items(1).ImageUrl = "~/Images/tabmenu/visar.jpg"
        '    Case 2
        '        mnuMaster.Items(2).ImageUrl = "~/Images/tabmenu/quar.jpg"
        '    Case 3
        '        mnuMaster.Items(3).ImageUrl = "~/Images/tabmenu/expr.jpg"
        '    Case 4
        '        mnuMaster.Items(4).ImageUrl = "~/Images/tabmenu/ctr.jpg"
        '    Case 5
        '        mnuMaster.Items(5).ImageUrl = "~/Images/tabmenu/persr.jpg"
        '    Case 6
        '        mnuMaster.Items(6).ImageUrl = "~/Images/tabmenu/salr.jpg"
        '    Case 7
        '        mnuMaster.Items(7).ImageUrl = "~/Images/tabmenu/benr.jpg"
        '    Case 8
        '        mnuMaster.Items(8).ImageUrl = "~/Images/tabmenu/othr.jpg"
        '    Case 9
        '        mnuMaster.Items(9).ImageUrl = "~/Images/tabmenu/docr.jpg"

        'End Select
    End Sub


    Protected Sub FillSelectedData()
        If txtLRdate.Text = "" Then
            txtLRdate.Text = txtJdate.Text
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")
        'ScriptManager1.RegisterPostBackControl(btnDocAdd)
        'ScriptManager1.RegisterPostBackControl(btnDocAdd1)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")

        'ScriptManager1.RegisterPostBackControl(btnDocAdd)
        ScriptManager1.RegisterPostBackControl(btnDocAdd1)
        ScriptManager1.RegisterPostBackControl(btnQualAdd)
        ScriptManager1.RegisterPostBackControl(Me.btnDependanceAdd)
        Dim CurBsUnit As String = Session("sBsuid")
        UpLoadPhoto()
        UploadDependencePhoto()

        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ClientScript.GetPostBackEventReference(txtBname, "")
            txtBname.Attributes.Add("OnChange", "javascript:return DoPostBack()")

            ClearAllSession()
            'ViewState("datamode") = "add"
            txtDependUnit.Attributes.Add("ReadOnly", "ReadOnly")
            txtAppNo.Attributes.Add("ReadOnly", "ReadOnly")
            txtML.Attributes.Add("ReadOnly", "ReadOnly")
            txtME.Attributes.Add("ReadOnly", "ReadOnly")
            txtTeachGrade.Attributes.Add("ReadOnly", "ReadOnly")
            txtWU.Attributes.Add("ReadOnly", "ReadOnly")
            txtIU.Attributes.Add("ReadOnly", "ReadOnly")
            txtSD.Attributes.Add("ReadOnly", "ReadOnly")
            txtCat.Attributes.Add("ReadOnly", "ReadOnly")
            'txtTGrade.Attributes.Add("ReadOnly", "ReadOnly")
            txtDept.Attributes.Add("ReadOnly", "ReadOnly")
            txtCountry.Attributes.Add("ReadOnly", "ReadOnly")
            'txtQual.Attributes.Add("ReadOnly", "ReadOnly")
            txtBname.Attributes.Add("ReadOnly", "ReadOnly")
            txtReport.Attributes.Add("ReadOnly", "ReadOnly")
            txtSalGrade.Attributes.Add("ReadOnly", "ReadOnly")
            'txtQualGrade.Attributes.Add("ReadOnly", "ReadOnly")
            'txtQualQualification.Attributes.Add("ReadOnly", "ReadOnly")
            txtDocDocument.Attributes.Add("ReadOnly", "ReadOnly")
            'Contact Details
            txtContCostUnit.Attributes.Add("ReadOnly", "ReadOnly")
            txtContCurrCountry.Attributes.Add("ReadOnly", "ReadOnly")
            txtContPermCountry.Attributes.Add("ReadOnly", "ReadOnly")
            txtDependDOB.Attributes.Add("ReadOnly", "ReadOnly")
            'txtEMPDOB.Attributes.Add("ReadOnly", "ReadOnly")
            txtEmpApprovalPol.Attributes.Add("ReadOnly", "ReadOnly")
            txtQual_QualifCategory.Attributes.Add("ReadOnly", "ReadOnly")
            txtQualQualification.Attributes.Add("ReadOnly", "ReadOnly")
            txtWPSDesc.Attributes.Add("ReadOnly", "ReadOnly") 'V1.1
            txtCostCenter.Attributes.Add("ReadOnly", "ReadOnly") 'V1.5

            chkSalPayMonthly.Checked = True
            chkDocActive.Checked = True
            txtIU.Text = Session("BSU_Name")
            hfIU.Value = Session("sBsuid")
            txtWU.Text = Session("BSU_Name")
            hfWU.Value = Session("sBsuid")

            txtEmpno1.Attributes.Add("ReadOnly", "ReadOnly")
            txtFname1.Attributes.Add("onBlur", "javascript:{this.value = this.value.toUpperCase(); }")
            txtFname1.Attributes.Add("onFocus", "javascript:{this.value = this.value.toUpperCase(); }")
            txtMname1.Attributes.Add("onBlur", "javascript:{this.value = this.value.toUpperCase(); }")
            txtLname1.Attributes.Add("onBlur", "javascript:{this.value = this.value.toUpperCase(); }")
            FUUploadEmpPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
            'FUUploadDependance.Attributes.Add("onblur", "javascript:UploadDependencePhoto();")
            txtSalAmtEligible.Attributes.Add("onblur", "javascript:GetPayableFromEligible();")

            'V1.3
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", "EmpPopUp") 'modalEmpPopUp.ClientID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", "EmpPopUp") 'modalEmpPopUp.ClientID)
            ' btnsearch.Attributes.Add("onClientclick", "return clickme(event,'" + imgEmployee.ClientID + "','btnsearch')")
            ' txtFNameS.Attributes.Add("onKeydown", OnMouseOverScript)
            'txtFNameS.Attributes.Add("onKeydown", OnMouseOutScript)
            'txtLNameS.Attributes.Add("onKeydown", OnMouseOutScript)
            'txtMNameS.Attributes.Add("onKeydown", OnMouseOutScript)
            'btnsearch.Attributes.Add("onmouseout", OnMouseOutScript)
            'btnsearch.Attributes.Add("onmouseout", "javascript:{alert('hiii'); }")
            txtFNameS.Attributes.Add("onKeydown", "return clickme(event,'" + imgEmployee.ClientID + "','txtFNameS')")

            txtEmpNoS.Attributes.Add("onKeydown", "return clickme(event,'" + imgEmployee.ClientID + "','txtEmpNoS')")
            ' test.Attributes.Add("OnmouseOver", "return clickme(event,'" + imgEmployee.ClientID + "','btnsearch')")
            ' txtFNameS.Attributes.Add("onmouseout", OnMouseOutScript)
            'V1.3 ends


            If Session("sBSUID") <> "800017" Then
                tr_NatNo.Visible = False
            Else
                tr_NatNo.Visible = True
            End If

            If Session("BSU_COUNTRY_ID") = 6 Then
                id_ind.Visible = True
            Else
                id_ind.Visible = False
            End If

            'If Session("BSU_IsHROnDAX") = 1 Then
            '    tr3.Visible = True
            '    tr7_POS_ID.Visible = True
            'Else
            '    tr3.Visible = False
            '    tr7_POS_ID.Visible = False
            'End If

            Page.Title = OASISConstants.Gemstitle
            BindENR()
            BindReligion()
            callVisa_Bind()
            GetPayMonth_YearDetails()
            BindGradeLevels()
            BindConcessionEligibilityUnit()
            BindStaffPoints() ' -- V2.0

            BindLanguage()
            BindCountry()
            BindEducation()
            BindDim_dep()
            BindDim_stafflevels()
            BindPosType()
            'Bind_JobLinked()
            'Bind_JobPositionType()
            BindHRL_Grade()

            EnableGradeAndLevel()


            BindYearOfQualification() ''V2.2
            BindQualificationCategory()
            BindQualificationLevel()
            BindrblQualCertType()
            If Session("BSU_COUNTRY_ID") = "172" Then 'V3.0
                ddlCategory.SelectedIndex = 0 'V1.1
            Else
                ddlCategory.SelectedIndex = 2
                trWPSAgent.Visible = False

            End If
            hf_jobmaster_id.Value = "0"
            txtNoofDepen.Text = "0"
            chkActive.Checked = True
            imgBtnReg.Visible = False
            txtResig.Text = ""
            txtResig.Enabled = False
            BindInsuranceTypes(ddlInsuCardTypes)
            BindInsuranceTypes(ddlDepInsuCardTypes)
            rblIsGems.SelectedValue = "N"
            rblIsGems_SelectedIndexChanged(Nothing, Nothing)
            ddlAirTicketAvailable.DataBind()
            ddlInsuEligible.DataBind()
            ddlInsuEligible.SelectedValue = "0"
            ddlInsuActual.DataBind()
            ddlInsuActual.SelectedValue = "0"
            ddlTicketActual.DataBind()
            If txtCat.Text <> "" And txtVacationPolicy.Text = "" Then
                GetLeavePolicy()
            End If
            Dim itemTypeCounter As Integer
            Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnitsByCurrency(CurBsUnit)
                If BUnitreaderSuper.HasRows = True Then
                    While BUnitreaderSuper.Read
                        BCur_ID = Convert.ToString(BUnitreaderSuper("BSU_CURRENCY"))
                    End While
                End If
            End Using
            Using CUnitreaderSuper As SqlDataReader = AccessRoleUser.GetCurrency()
                'create a list item to bind records from reader to dropdownlist ddlBunit
                Dim di As ListItem
                ddlSalC.Items.Clear()
                'check if it return rows or not
                If CUnitreaderSuper.HasRows = True Then
                    While CUnitreaderSuper.Read
                        di = New ListItem(CUnitreaderSuper("CUR_DESCR"), CUnitreaderSuper("CUR_ID"))
                        'adding listitems into the dropdownlist
                        ddlSalC.Items.Add(di)
                        ddlPayC.Items.Add(di)

                        For itemTypeCounter = 0 To ddlSalC.Items.Count - 1
                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                            If ddlSalC.Items(itemTypeCounter).Value = BCur_ID Then
                                ddlSalC.SelectedIndex = itemTypeCounter
                                ddlPayC.SelectedIndex = itemTypeCounter
                            End If
                        Next
                    End While
                End If
            End Using

            Using SUnitreaderSuper As SqlDataReader = AccessRoleUser.GetempStatus()
                'create a list item to bind records from reader to dropdownlist ddlBunit
                Dim di As ListItem
                ddlStatus.Items.Clear()
                'check if it return rows or not
                If SUnitreaderSuper.HasRows = True Then
                    While SUnitreaderSuper.Read
                        di = New ListItem(SUnitreaderSuper("EST_DESCR"), SUnitreaderSuper("EST_ID"))
                        'adding listitems into the dropdownlist
                        ddlStatus.Items.Add(di)
                    End While
                End If
            End Using
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), CurBsUnit, ViewState("MainMnu_code"))
            If ViewState("datamode") = "view" Then
                ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                'ViewState("searchEmpQuery") = Request.QueryString("searchQuery").Replace(" ", "+")
                trsearchTable.Visible = True

                ddlStatus.Visible = False
                lblEmpStatus.Visible = True
            End If
        End If

        If (Request.QueryString("datamode") Is Nothing) Or (Request.QueryString("MainMnu_code") Is Nothing) Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
        Else
            'get the menucode to confirm the user is accessing the valid page
            'if query string returns Eid  if datamode is view state

            'Setting tab rights for user

            Dim Bsu_ID As String = Session("sBsuid")
            Dim MNu_code As String = ViewState("MainMnu_code")
            Dim RoleID As String = String.Empty
            Using reader_Rol_ID As SqlDataReader = AccessRoleUser.GetRoleID_user(Session("sUsr_name"))
                If reader_Rol_ID.HasRows = True Then
                    While reader_Rol_ID.Read()
                        RoleID = Convert.ToString(reader_Rol_ID("USR_ROL_ID"))
                    End While
                End If
            End Using
            Dim T_code As String = String.Empty
            Dim T_right As String = String.Empty
            Dim ht_tab As New Hashtable()
            Using readertab_Access As SqlDataReader = AccessRoleUser.GetTabRights(RoleID, Bsu_ID, MNu_code)
                If readertab_Access.HasRows = True Then
                    While readertab_Access.Read()
                        T_code = Convert.ToString(readertab_Access("TAB_CODE"))
                        T_right = Convert.ToString(readertab_Access("TAR_RIGHT"))
                        ht_tab.Add(T_code, T_right)
                    End While
                End If
            End Using
            Session("tab_Right_User") = ht_tab
            Call menuShow()
            'Dim userSuper As Boolean
            'userSuper = Session("sBusper")

            'If userSuper = False Then
            '    Dim i As Integer
            '    Dim FirstFlag As Boolean = True
            '    For i = 0 To mnuMaster.Items.Count - 1
            '        Dim str1 As String = i
            '        If ht_tab.ContainsKey(str1) Then
            '            If FirstFlag And (ht_tab.Item(str1) = "1" Or ht_tab.Item(str1) = "2") Then
            '                'mvMaster.ActiveViewIndex = i
            '                FirstFlag = False
            '            End If
            '            If ht_tab.Item(str1) = "1" Then
            '                mnuMaster.Items(i).Selectable = True
            '                SetPanelEnabled(i, False)

            '            ElseIf ht_tab.Item(str1) = "2" Then
            '                mnuMaster.Items(i).Selectable = True
            '                SetPanelEnabled(i, True)
            '            End If
            '        Else
            '            mnuMaster.Items(i).ImageUrl = ""
            '        End If
            '    Next
            'End If

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            If ViewState("datamode") = "view" Then
                Try
                    chkActive.Enabled = False
                    ddlStatus.Enabled = False
                    'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))    V1.3
                    FillEmployeeMasterValues(ViewState("viewid"))
                    ' BindGradeLevels()
                    BindConcessionEligibilityUnit()
                    For Each li As ListItem In ddlGradelevel.Items
                        If li.Value = hfGrade_ID.Value Then
                            ddlGradelevel.SelectedValue = li.Value
                        End If
                    Next
                    DoNullCheck(ddlConEligibility, hf_FeeConcessionCategory_ID.Value)

                    ' mnuMaster.Items(7).Selectable = True
                    mnuMaster.Items(8).Selectable = True
                    FillFixedAssetTab(ViewState("viewid"))
                    FillConcessionGrid(ViewState("viewid"))
                    ControlEnableEditing(ViewState("viewid"))
                    cpePending.Collapsed = True
                    cpePending.CollapsedText = "(Show search...)"
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)
                    lblError.Text = "Some Error occured while retrieving the data ... " & ex.Message
                    btnSave.Visible = False
                    Exit Sub
                Finally
                    For i As Integer = 0 To 9 ' 7
                        SetPanelEnabled(i, False)
                    Next
                End Try
            ElseIf ViewState("datamode") = "add" Then
                mnuMaster.Items(8).Selectable = True
                If Trim(txtFTE.Text) = "" Then
                    txtFTE.Text = "1"
                    rdMale.Checked = True
                End If
            Else
                mnuMaster.Items(8).Selectable = True
            End If
            If ddlPayMode.SelectedItem.Text = "Bank" Then
                txtBname.Enabled = True
                btnBank_name.Enabled = True
                txtAccCode.Enabled = True
            Else
                txtBname.Enabled = False
                btnBank_name.Enabled = False
                txtAccCode.Enabled = False
            End If

        End If

    End Sub
    'v1.3 --to bind grade levels as per category
    Private Sub BindGradeLevels()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim grdlevelID As String = ""
        If hfCat_ID.Value <> "" Then
            grdlevelID = hfCat_ID.Value
        End If

        Dim str_Sql As String = "exec GetEmpGradeLevels '" & grdlevelID & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlGradelevel.Items.Clear()
        ddlGradelevel.SelectedIndex = -1
        ddlGradelevel.ClearSelection()
        ddlGradelevel.SelectedValue = Nothing
        ddlGradelevel.DataSource = dr
        ddlGradelevel.DataTextField = "EGD_DESCR"
        ddlGradelevel.DataValueField = "EGD_ID"
        ddlGradelevel.DataBind()
        ddlGradelevel.Items.Insert(0, New ListItem("--Select--", "0"))
        dr.Close()
    End Sub
    Private Sub BindStaffPoints()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim grdlevelID As String = ""
        'If hfCat_ID.Value <> "" Then
        '    grdlevelID = hfCat_ID.Value
        'End If

        Dim str_Sql As String = "exec GetStaffPoints '" & hfWU.Value & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlStaffPoint.DataSource = dr
        ddlStaffPoint.DataTextField = "SPT_DESCR"
        ddlStaffPoint.DataValueField = "SPT_ID"
        ddlStaffPoint.DataBind()
        ddlStaffPoint.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindConcessionEligibilityUnit()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim catID As String = ""
        If hfCat_ID.Value <> "" Then
            catID = hfCat_ID.Value
        End If

        Dim str_Sql As String = "exec [GetEmpConcessionEligibilityUnit] '" & catID & "','" & hfWU.Value & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlConEligibility.DataSource = dr
        ddlConEligibility.DataTextField = "FCE_BSG_ID"
        ddlConEligibility.DataValueField = "FCE_ID"
        ddlConEligibility.SelectedIndex = -1
        ddlConEligibility.DataBind()
        dr.Close()
    End Sub
    Private Sub GetLeavePolicy()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim grdlevelID As String = ""
        If hfCat_ID.Value <> "" Then
            grdlevelID = hfCat_ID.Value
        End If

        Dim str_Sql As String = "exec GetEmpVacPolicy '" & grdlevelID & "','" & Session("sBsuid") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

        If dr.HasRows Then
            While dr.Read
                txtVacationPolicy.Text = dr("BLS_DESCRIPTION").ToString
                hf_VacationPolicy.Value = dr("BLS_ID").ToString
            End While

        End If

        dr.Close()
    End Sub
    Private Sub BindInsuranceTypes(ByVal ctrl As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim grdlevelID As String = ""
        If hfCat_ID.Value <> "" Then
            grdlevelID = hfCat_ID.Value
        End If

        Dim str_Sql As String = "exec GetInsuranceCardTypes "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ctrl.DataSource = dr
        ctrl.DataTextField = "INSU_DESCR"
        ctrl.DataValueField = "INSU_ID"
        ctrl.DataBind()
        ' ctrl.Items.Insert(0, "--Select--")
        ctrl.Items.Insert(0, New ListItem("--Select--", "0"))

        dr.Close()
    End Sub
    Private Sub ControlEnableEditing(ByVal vEMP_ID As String)

        Dim bEnableEditing As Boolean = False

        Dim str_sql As String = "SELECT COUNT(ESD_ID) FROM EMPSALARYDATA_D WHERE ESD_EMP_ID = " & vEMP_ID
        Dim vCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        If vCount <= 0 Then
            bEnableEditing = True
        Else
            bEnableEditing = False
        End If

        btnE_Desig.Enabled = bEnableEditing
        btnCat.Enabled = bEnableEditing
        ddlStatus.Enabled = bEnableEditing
        ddlPayMode.Enabled = bEnableEditing
        ddlAccom.Enabled = bEnableEditing
        ddlEmpTransportation.Enabled = bEnableEditing
        btnDept.Enabled = bEnableEditing
        ddlCategory.Enabled = bEnableEditing
        btnMoe_desig.Enabled = bEnableEditing
        txtAccCode.Enabled = bEnableEditing
        btnBank_name.Enabled = bEnableEditing

    End Sub

    Sub menuShow()
        If Session("sBusper") = False Then
            Dim i As Integer
            Dim FirstFlag As Boolean = True
            For i = 0 To mnuMaster.Items.Count - 1
                Dim str1 As String = i
                If Session("tab_Right_User").ContainsKey(str1) Then
                    If FirstFlag And (Session("tab_Right_User").Item(str1) = "1" Or Session("tab_Right_User").Item(str1) = "2") Then
                        'mvMaster.ActiveViewIndex = i
                        FirstFlag = False
                    End If
                    If Session("tab_Right_User").Item(str1) = "1" Then
                        mnuMaster.Items(i).Selectable = True
                        SetPanelEnabled(i, False)

                    ElseIf Session("tab_Right_User").Item(str1) = "2" Then
                        mnuMaster.Items(i).Selectable = True
                        SetPanelEnabled(i, True)
                    End If
                Else
                    mnuMaster.Items(i).ImageUrl = ""
                    mnuMaster.Items(i).Enabled = False
                    mnuMaster.Items(i).Text = ""

                End If
            Next
        End If
    End Sub

    Private Sub ClearAllSession()
        Session("EMPDEPENDANCEDETAILS") = Nothing
        Session("EMPQUALDETAILS") = Nothing
        Session("EMPLANGDETAILS") = Nothing
        Session("EMPSALDETAILS") = Nothing
        Session("EMPDOCDETAILS") = Nothing
        Session("EMPDOCDETAILS1") = Nothing
        Session("EMPQUALDETAILS") = Nothing
        Session("EMPEXPDETAILS") = Nothing
        Session("EMPSALGrossSalary") = Nothing
        Session("EMPSALSCHEDULE") = Nothing
    End Sub

    Private Sub UpLoadPhoto()
        If FUUploadEmpPhoto.FileName <> "" And h_EmpImagePath.Value <> FUUploadEmpPhoto.FileName Then
            If FUUploadEmpPhoto.HasFile = False Then
                lblError.Text = "Select image file to upload !!!"
                Exit Sub
                'ElseIf FUUploadEmpPhoto.PostedFile.ContentLength > 50500 Then '4MB -- commented by Rajesh
            ElseIf FUUploadEmpPhoto.PostedFile.ContentLength > 252500 Then '4MB
                'MsgBox("Photo not to exceed 50 KB in size!!!", MsgBoxStyle.Information, "Photo size!!!")
                lblError.Text = "Photo not to exceed 250 KB in size!!!"
                Exit Sub
            End If
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim fs As New FileInfo(FUUploadEmpPhoto.PostedFile.FileName)
            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If
            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FUUploadEmpPhoto.FileName
            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            If FUUploadEmpPhoto.FileName <> "" Then
                FUUploadEmpPhoto.PostedFile.SaveAs(strFilepath)
                ImgHeightnWidth(strFilepath)
                Try
                    If Not FUUploadEmpPhoto.PostedFile.ContentType.Contains("image") Then
                        Throw New Exception
                    End If
                    imgEmpImage.ImageUrl = str_imgvirtual & "/temp/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                    ViewState("tempimgpath") = strFilepath
                    imgEmpImage.AlternateText = "No Image found"
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    UtilityObj.Errorlog(ex.Message)
                    h_EmpImagePath.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgEmpImage.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgEmpImage.AlternateText = "No Image found"
                End Try
            End If

        End If
    End Sub
    Private Sub UploadDependencePhoto()
        If FUUploadDependance.FileName <> "" And h_EmpDependantImgPath.Value <> FUUploadDependance.FileName Then
            If FUUploadDependance.HasFile = False Then
                lblError.Text = "Select image file to upload !!!"
                Exit Sub
            ElseIf FUUploadDependance.PostedFile.ContentLength > 50500 Then '4MB
                'MsgBox("Photo not to exceed 50 KB in size!!!", MsgBoxStyle.Information, "Photo size!!!")
                lblError.Text = "Photo not to exceed 50 KB in size!!!"
                Exit Sub
            End If
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim fs As New FileInfo(FUUploadDependance.PostedFile.FileName)
            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If
            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FUUploadDependance.FileName
            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            If FUUploadDependance.FileName <> "" Then
                FUUploadDependance.PostedFile.SaveAs(strFilepath)
                ImgHeightnWidth(strFilepath)
                Try
                    If Not FUUploadDependance.PostedFile.ContentType.Contains("image") Then
                        Throw New Exception
                    End If
                    imgEmpImage.ImageUrl = str_imgvirtual & "/temp/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                    ViewState("tempimgpath") = strFilepath
                    imgEmpImage.AlternateText = "No Image found"
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    UtilityObj.Errorlog(ex.Message)
                    h_EmpImagePath.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgEmpImage.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgEmpImage.AlternateText = "No Image found"
                End Try
            End If

        End If
    End Sub

    Private Sub BindReligion()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select RLG_ID,RLG_DESCR from RELIGION_M"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlEmpReligion.DataSource = dr
        ddlEmpReligion.DataTextField = "RLG_DESCR"
        ddlEmpReligion.DataValueField = "RLG_ID"
        ddlEmpReligion.DataBind()
        dr.Close()
    End Sub

    Private Sub BindLanguage()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "exec GET_NativeLanguage"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlNativeLang.DataSource = dr
        ddlNativeLang.DataTextField = "LNG_DESCR"
        ddlNativeLang.DataValueField = "LNG_ID"
        ddlNativeLang.DataBind()
        ddlNativeLang.Items.Add(New ListItem("", "0"))
        ddlNativeLang.ClearSelection()
        ddlNativeLang.Items.FindByText("").Selected = True
        dr.Close()
    End Sub

    Private Sub BindCountry()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT  CTY_ID ,CTY_DESCR  FROM COUNTRY_M where CTY_DESCR<>'NULL' ORDER BY CTY_DESCR"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlCOB.DataSource = dr
        ddlCOB.DataTextField = "CTY_DESCR"
        ddlCOB.DataValueField = "CTY_ID"
        ddlCOB.DataBind()

        dr.Close()
    End Sub

    Private Sub BindEducation()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT  EDU_ID ,EDU_DESCR  FROM EDUCATION_M  ORDER BY EDU_DESCR"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlHighEdu.DataSource = dr
        ddlHighEdu.DataTextField = "EDU_DESCR"
        ddlHighEdu.DataValueField = "EDU_ID"
        ddlHighEdu.DataBind()
        ddlHighEdu.Items.Add(New ListItem("", "0"))
        ddlHighEdu.ClearSelection()
        ddlHighEdu.Items.FindByText("").Selected = True
        dr.Close()
    End Sub
    Private Sub BindDim_dep()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "exec GET_DIM_DEPARTMENT "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlDimDepart.DataSource = dr
        ddlDimDepart.DataTextField = "DIM_DESCR"
        ddlDimDepart.DataValueField = "DIM_ID"
        ddlDimDepart.DataBind()
        ddlDimDepart.Items.Add(New ListItem("", "0"))
        ddlDimDepart.ClearSelection()
        ddlDimDepart.Items.FindByText("").Selected = True
    End Sub
    Private Sub BindDim_stafflevels()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "exec GET_DIM_STAFF_LEVELS "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlDimStaffLvl.DataSource = dr
        ddlDimStaffLvl.DataTextField = "DSL_DESCR"
        ddlDimStaffLvl.DataValueField = "DSL_ID"
        ddlDimStaffLvl.DataBind()
        ddlDimStaffLvl.Items.Add(New ListItem("", "0"))
        ddlDimStaffLvl.ClearSelection()
        ddlDimStaffLvl.Items.FindByText("").Selected = True
    End Sub
    Private Sub BindPosType()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "exec GET_POSTYPE_M "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlPostype.DataSource = dr
        ddlPostype.DataTextField = "POS_TYPE_DESCR"
        ddlPostype.DataValueField = "POS_TYPE_ID"
        ddlPostype.DataBind()
        ddlPostype.Items.Add(New ListItem("", "0"))
        ddlPostype.ClearSelection()
        ddlPostype.Items.FindByText("").Selected = True
    End Sub

    Private Sub BindHRL_Grade()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT * FROM dbo.HAYS_GRADE_M"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlGrade.DataSource = dr
        ddlGrade.DataTextField = "HGRADE_VALUE"
        ddlGrade.DataValueField = "HGRADE_ID"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("--Select--", "-1"))
    End Sub

    Private Sub BindHRL_Grade_Levels(Id As String)
        If Id = "-1" Then
            ddlLevel.Items.Clear()
            ddlLevel.Items.Insert(0, New ListItem("--Select--", "-1"))
        Else
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT * FROM dbo.HAYS_GRADE_LEVELS WHERE HRL_HGRADE_ID=" & Id
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            ddlLevel.DataSource = dr
            ddlLevel.DataTextField = "HRL_BAND"
            ddlLevel.DataValueField = "HRL_ID"
            ddlLevel.DataBind()
            ddlLevel.Items.Insert(0, New ListItem("--Select--", "-1"))
        End If
    End Sub

#Region "Clear Details"

    Private Sub clearme()
        Try
            txtPassportname.Text = ""
            txtKnownas.Text = ""
            txtArabicName.Text = ""
            txtEmpno1.Text = ""
            txtAppNo.Text = ""
            txtEmpno1.Text = ""
            txtMname1.Text = ""
            txtLname1.Text = ""
            txtFname1.Text = ""
            imgEmpImage.ImageUrl = "~/Images/Photos/no_image.gif"
            hfSD_ID.Value = ""
            txtSD.Text = ""
            hfCat_ID.Value = ""
            txtCat.Text = ""
            hfGrade_ID.Value = ""
            txtTeachGrade.Text = ""
            hfWU.Value = ""
            txtWU.Text = ""
            hfTeachGrade_ID.Value = ""
            'txtTGrade.Text = ""
            ddlVtype.SelectedIndex = 0
            ddlVstatus.SelectedIndex = 0

            hf_HRL.Value = ""
            ddlGrade.SelectedValue = "-1"
            ddlLevel.SelectedValue = "-1"

            txtJdate.Text = ""
            txtLastPresentDate.Text = ""
            txtDOJGroup.Text = ""
            txtViss_date.Text = ""
            txtVExp_date.Text = ""
            txtLRdate.Text = ""

            ddlStatus.SelectedIndex = 0
            hfStatus_ID.Value = ""
            txtResig.Text = ""
            'txtProb.Text = dr("EMP_PROBTILL")
            txtProb.Text = ""
            ddlAccom.SelectedIndex = 0
            chkActive.Checked = False
            chkOverSeasRecr.Checked = False
            chkAllowOT.Checked = False
            ddlSalC.SelectedIndex = 0
            ddlPayC.SelectedIndex = 0
            ddlPayMode.SelectedIndex = 0
            hfBank_ID.Value = ""
            txtAccCode.Text = ""
            txtBcode.Text = ""
            txtME.Text = ""
            hfME_ID.Value = ""

            hfML_ID.Value = ""
            txtIU.Text = ""
            txtML.Text = ""
            hfIU.Value = ""
            ddlMstatus.SelectedIndex = 0
            txtGsalary.Text = ""
            txtPassNo.Text = ""
            txtRemark.Text = ""
            hfCountry_ID.Value = ""
            txtCountry.Text = ""
            rdMale.Checked = True
            hfQul.Value = ""
            'txtQual.Text = ""
            hfSalGrade.Value = ""
            hfDept_ID.Value = ""
            txtDept.Text = ""
            ddlBgroup.SelectedIndex = 0
            txtFather.Text = ""
            txtMother.Text = ""
            hfReport.Value = ""
            txtReport.Text = ""
            txtStaffNo.Text = ""
            If Session("BSU_COUNTRY_ID") = "172" Then  'V3.0
                ddlCategory.SelectedIndex = 0
            Else
                ddlCategory.SelectedIndex = 2
                trWPSAgent.Visible = False

            End If

            ddlCOB.SelectedIndex = 0
            ddlNativeLang.SelectedIndex = 0
            ddlHighEdu.SelectedIndex = 0
            txtNoofDepen.Text = ""
            ddlDimDepart.SelectedIndex = 0
            ddlDimStaffLvl.SelectedIndex = 0
            txtJobMaster.Text = ""
            hf_jobmaster_id.Value = "0"

            txtVisaPrgID.Text = ""

            txtEmpApprovalPol.Text = ""
            h_ApprovalPolicy.Value = ""
            txtEMPDOB.Text = ""
            txtAirticketCity.Text = ""
            h_AirTcket_City_ID.Value = ""
            txtAirTicketNo.Text = ""
            ddlAirTicketAvailable.SelectedIndex = 0
            ddlAirTicketClass.SelectedIndex = 0
            ddlEmpTransportation.SelectedIndex = 0

            txtSalGrade.Text = ""
            hfSalGrade.Value = ""
            txtBname.Text = ""
            hfBank_ID.Value = ""
            txtMaxChildConcession.Text = ""
            txtVacationPolicy.Text = ""
            hf_VacationPolicy.Value = ""
            txtLOPOpening.Text = ""
            txtFTE.Text = ""
            txtWPSDesc.Text = "" 'V1.1
            If Session("BSU_COUNTRY_ID") = "172" Then 'V3.0
                ddlCategory.SelectedIndex = 0 'V1.1
            Else
                ddlCategory.SelectedIndex = 2
                trWPSAgent.Visible = False

            End If

            'EMP_ID = dr("EMP_ID")
            chkTempEmp.Checked = False
            ClearEMPDocDetails()
            ClearEMPExpDetails()
            ClearEMPSalDetails()
            ClearSession_GridDetails()
            ClearContactTab()
            ClearEMPLangDetails()
            EnableAllControls()

            'v1.3
            txtFNameS.Text = ""

            txtEmpNoS.Text = ""
            lblEmpStatus.Text = ""
            lblEmpStatus.Visible = False
            ddlStatus.Visible = True
            ddlStatus.SelectedIndex = 0

            ddlInsuEligible.SelectedValue = "0"
            ddlInsuActual.SelectedValue = "0"

            ddlGrade.SelectedIndex = 0
            ddlLevel.SelectedIndex = 0
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub EnableAllControls()
        btnE_Desig.Enabled = True
        btnCat.Enabled = True
        ddlStatus.Enabled = True
        ddlPayMode.Enabled = True
        ddlAccom.Enabled = True
        ddlEmpTransportation.Enabled = True
        btnDept.Enabled = True
        ddlCategory.Enabled = True
        btnMoe_desig.Enabled = True
        txtAccCode.Enabled = True
        btnBank_name.Enabled = True
    End Sub

    Private Sub ClearContactTab()
        txtContAccomLocation.Text = ""
        txtContCostUnit.Text = ""
        txtContCurAddress.Text = ""
        txtContCurrCity.Text = ""
        txtContCurrCountry.Text = ""
        txtContCurrMobNo.Text = ""
        txtContCurrPhoneNo.Text = ""
        txtContCurrPOBox.Text = ""
        txtContEmail.Text = ""
        txtContFlatNo.Text = ""
        txtContHomecont.Text = ""
        txtContLocalContact.Text = ""
        txtContLocalContNo.Text = ""
        txtContHomecontNo.Text = ""
        txtContPermAddress.Text = ""
        txtContPermCity.Text = ""
        txtContPermCountry.Text = ""
        'txtContPermMobNo.Text = ""
        txtContPermPhoneNo.Text = ""
        txtContPermPOBox.Text = ""
        ddlContAccomType.SelectedValue = 0

        hfContCCountry_ID.Value = ""
        hfContPCountry_ID.Value = ""
    End Sub

#End Region

#Region "Fill Tabs"

    Private Sub FillFixedAssetTab(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_sql As String = "SELECT EMPASSETS_D.EAD_ISSUE_DT, EMPASSETS_D.EAD_VALUE, EMPASSETS_D.EAD_REMARKS, " & _
        "CASE WHEN EMPASSETS_D.EAD_STATUS=0 THEN 'Issue' WHEN EMPASSETS_D.EAD_STATUS=1 then 'Return' ELSE 'Lost' END AS  EAD_STATUS" & _
        ", EMPASSETS_D.EAD_bACTIVE, OASISFIN.dbo.FIXEDASSET_M.FAS_DESCRIPTION AS ASSETNAME, EMPASSETS_D.EAD_EMP_ID" & _
        " FROM OASISFIN.dbo.FIXEDASSET_M RIGHT OUTER JOIN EMPASSETS_D ON OASISFIN.dbo.FIXEDASSET_M.FAS_ID = EMPASSETS_D.EAD_FAS_ID " & _
        "WHERE EMPASSETS_D.EAD_RET_DT Is NULL AND EAD_EMP_ID =" & empID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        gvAssetAllocated.DataSource = ds
        gvAssetAllocated.DataBind()
    End Sub
    Private Sub FillConcessionGrid(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MainDB").ConnectionString

        Dim str_sql As String = "exec OASIS_FEES..SP_GET_PROFILE_EMPLOYEE_CONCESSION_DETAIL  '" & Session("sBSUID") & "'," & empID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        gvFeeConcessionDetails.DataSource = ds
        gvFeeConcessionDetails.DataBind()
    End Sub

    Private Sub FillEmployeeMasterValues(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select * from vw_OSO_EMPLOYEEMASTER where EMP_ID = " & empID
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlVtype.Items.Insert(0, "None")
        ddlVtype.DataBind()

        While dr.Read()
            'grade description added
            hfGrade_ID.Value = IIf(TypeOf (dr("EMP_EGD_ID")) Is DBNull, "0", dr("EMP_EGD_ID")) ' dr("EMP_EGD_ID").ToString 'EMP_SGD_ID

            'ddlGradelevel.SelectedValue = IIf(TypeOf (dr("EMP_GRADE_DESCR")) Is DBNull, "", dr("EMP_GRADE_DESCR"))

            'For Each li As ListItem In ddlGradelevel.Items
            '    If li.Value = hfGrade_ID.Value Then
            '        ddlGradelevel.SelectedValue = li.Value
            '    End If
            'Next

            txtKnownas.Text = IIf(TypeOf (dr("EMP_DISPLAYNAME")) Is DBNull, "", dr("EMP_DISPLAYNAME"))
            txtArabicName.Text = IIf(TypeOf (dr("EMP_ARABIC_NAME")) Is DBNull, "", dr("EMP_ARABIC_NAME"))
            txtPassportname.Text = dr("EMP_PASSPORTNAME").ToString
            txtEmpno1.Text = dr("EMPNO").ToString
            txtNat_No.Text = dr("EMP_NAT_NO").ToString
            txtAppNo.Text = IIf(dr("EMP_APPLNO").ToString = "0", String.Empty, dr("EMP_APPLNO").ToString)
            txtFname1.Text = dr("EMP_FNAME").ToString
            txtMname1.Text = dr("EMP_MNAME").ToString
            txtLname1.Text = dr("EMP_LNAME").ToString
            hfSD_ID.Value = dr("EMP_DES_ID").ToString
            txtSD.Text = dr("EMP_DES_DESCR").ToString
            hfCat_ID.Value = dr("EMP_ECT_ID").ToString
            hf_HRL.Value = dr("EMP_HRL_ID").ToString
            txtPosID.Text = dr("EMP_POS_ID").ToString
            txtPosDesc.Text = dr("POS_DESCRIPTION").ToString
            If Not hf_HRL.Value = "" Then
                Dim strSql As String = "SELECT HRL_HGRADE_ID FROM dbo.HAYS_GRADE_LEVELS WHERE HRL_ID = '" & hf_HRL.Value & "'"
                Dim strGrade As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strSql)
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(strGrade).Selected = True
                BindHRL_Grade_Levels(strGrade)
                ddlLevel.ClearSelection()
                ddlLevel.Items.FindByValue(hf_HRL.Value).Selected = True
            End If

            BindGradeLevels()
            If Not ddlGradelevel.Items.FindByValue(hfGrade_ID.Value) Is Nothing Then
                ddlGradelevel.ClearSelection()
                ddlGradelevel.Items.FindByValue(hfGrade_ID.Value).Selected = True
            End If
            txtCat.Text = dr("CATEGORY_DESC").ToString
            txtLOPOpening.Text = IIf(TypeOf (dr("EMP_LOP_OPN")) Is DBNull, 0, dr("EMP_LOP_OPN"))
            chkOverSeasRecr.Checked = IIf(TypeOf (dr("EMP_bOVERSEAS")) Is DBNull, False, dr("EMP_bOVERSEAS")) 'dr("EMP_bOVERSEAS")

            txtTeachGrade.Text = dr("TEACH_GRADE_DESCR").ToString
            hfWU.Value = dr("EMP_BSU_ID").ToString
            txtWU.Text = dr("WORK_BSU_NAME").ToString
            hfTeachGrade_ID.Value = dr("EMP_TGD_ID").ToString

            'txtTGrade.Text =  dr("TEACH_GRADE_DESCR").ToString
            'ddlVtype.SelectedValue = dr("EMP_VISATYPE").ToString

            DoNullCheck(ddlEmpContractType, dr("EMP_CONTRACTTYPE"))
            'ddlVstatus.SelectedValue = dr("EMP_VISASTATUS").ToString
            DoNullCheck(ddlVstatus, dr("EMP_VISASTATUS"))
            callVisa_Bind()
            DoNullCheck(ddlVtype, dr("EMP_VISATYPE"))
            If (dr("EMP_DOB").ToString <> "") Then txtEMPDOB.Text = Format(CDate(dr("EMP_DOB").ToString), OASISConstants.DateFormat) Else txtEMPDOB.Text = ""
            'txtEMPDOB.Text = IIf(dr("EMP_DOB").ToString <> "", Format(CDate(dr("EMP_DOB").ToString), OASISConstants.DateFormat), "")
            DoNullCheck(ddlEmpReligion, dr("EMP_RLG_ID"))
            DoNullCheck(ddlEmpSalute, dr("EMP_SALUTE"))
            'ddlEmpSalute.SelectedValue = IIf(dr("EMP_SALUTE") Is DBNull.Value, 0, dr("EMP_SALUTE"))
            txtEmpApprovalPol.Text = dr("APPROVAL_POLICY").ToString
            h_ApprovalPolicy.Value = IIf(dr("EMP_LPS_ID") Is DBNull.Value, 0, dr("EMP_LPS_ID"))
            DoNullCheck(ddlContAccomType, dr("EMP_CONTRACTTYPE"))
            'ddlContAccomType.SelectedValue = IIf(dr("EMP_CONTRACTTYPE") Is DBNull.Value, 0, dr("EMP_CONTRACTTYPE"))
            DoNullCheck(ddlEmpSalute, dr("EMP_SALUTE"))
            'ddlEmpSalute.SelectedValue = IIf(dr("EMP_SALUTE") Is DBNull.Value, 0, dr("EMP_SALUTE"))
            txtMaxChildConcession.Text = dr("EMP_MAXCHILDCONESSION").ToString
            If (Not dr("EMP_bCompanyTransport") Is DBNull.Value) AndAlso dr("EMP_bCompanyTransport") Then
                ddlEmpTransportation.SelectedValue = 1
            Else
                ddlEmpTransportation.SelectedValue = 0
            End If
            'V1.1---Joining date refers to empoyee's BSU Joining Date
            'If (dr("EMP_JOINDT").ToString <> "") Then
            '    txtJdate.Text = Format(CDate(dr("EMP_JOINDT").ToString), OASISConstants.DateFormat)
            '    ViewState("OLD_J_DATE") = txtJdate.Text
            'Else
            If (dr("EMP_BSU_JOINDT").ToString <> "") Then
                txtJdate.Text = Format(CDate(dr("EMP_BSU_JOINDT").ToString), OASISConstants.DateFormat)
                ViewState("OLD_J_DATE") = txtJdate.Text
            Else
                txtJdate.Text = ""
                ViewState("OLD_J_DATE") = ""
            End If

            'txtJdate.Text = IIf(dr("EMP_JOINDT").ToString <> "", Format(CDate(dr("EMP_JOINDT").ToString), OASISConstants.DateFormat), "")
            'txtViss_date.Text = IIf(dr("EMP_LASTVACFROM").ToString <> "", Format(CDate(dr("EMP_LASTVACFROM").ToString), OASISConstants.DateFormat), "")
            'txtVExp_date.Text = IIf(dr("EMP_LASTVACTO").ToString <> "", Format(CDate(dr("EMP_LASTVACTO").ToString), OASISConstants.DateFormat), "")
            'If (dr("EMP_LASTREJOINDT").ToString <> "") Then txtLRdate.Text = Format(CDate(dr("EMP_LASTREJOINDT").ToString), OASISConstants.DateFormat) Else txtLRdate.Text = ""
            'If ((dr("EMP_BSU_JOINDT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_BSU_JOINDT")), CDate("1/1/1900")) <> 0)) Then txtDOJGroup.Text = Format(CDate(dr("EMP_BSU_JOINDT").ToString), OASISConstants.DateFormat) Else txtDOJGroup.Text = ""

            'V1.1 - Joining Date Group os original joining Date which never changes.So bind with 
            'If ((dr("EMP_BSU_JOINDT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_BSU_JOINDT")), CDate("1/1/1900")) <> 0)) Then txtDOJGroup.Text = Format(CDate(dr("EMP_BSU_JOINDT").ToString), OASISConstants.DateFormat) Else txtDOJGroup.Text = ""
            If ((dr("EMP_BSU_JOINDT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_JOINDT")), CDate("1/1/1900")) <> 0)) Then txtDOJGroup.Text = Format(CDate(dr("EMP_JOINDT").ToString), OASISConstants.DateFormat) Else txtDOJGroup.Text = ""

            If ((dr("EMP_LASTATTDT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_LASTATTDT")), CDate("1/1/1900")) <> 0)) Then txtLastPresentDate.Text = Format(CDate(dr("EMP_LASTATTDT").ToString), OASISConstants.DateFormat) Else txtLastPresentDate.Text = ""

            If ((dr("EMP_LASTVACFROM").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_LASTVACFROM")), CDate("1/1/1900")) <> 0)) Then txtViss_date.Text = Format(CDate(dr("EMP_LASTVACFROM").ToString), OASISConstants.DateFormat) Else txtViss_date.Text = ""
            If ((dr("EMP_LASTVACTO").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_LASTVACTO")), CDate("1/1/1900")) <> 0)) Then txtVExp_date.Text = Format(CDate(dr("EMP_LASTVACTO").ToString), OASISConstants.DateFormat) Else txtVExp_date.Text = ""
            If ((dr("EMP_LASTREJOINDT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_LASTREJOINDT")), CDate("1/1/1900")) <> 0)) Then txtLRdate.Text = Format(CDate(dr("EMP_LASTREJOINDT").ToString), OASISConstants.DateFormat) Else txtLRdate.Text = ""

            DoNullCheck(ddlStatus, dr("EMP_STATUS"))
            lblEmpStatus.Text = ddlStatus.SelectedItem.Text
            'ddlStatus.SelectedValue = IIf(dr("EMP_STATUS") Is DBNull.Value, 0, dr("EMP_STATUS").ToString)
            hfStatus_ID.Value = IIf(dr("EMP_STATUS") Is DBNull.Value, 0, dr("EMP_STATUS").ToString)
            'If (dr("EMP_RESGDT").ToString <> "") Then txtResig.Text = Format(CDate(dr("EMP_RESGDT").ToString), OASISConstants.DateFormat) Else txtResig.Text = ""
            If ((dr("EMP_RESGDT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_RESGDT")), CDate("1/1/1900")) <> 0)) Then txtResig.Text = Format(CDate(dr("EMP_RESGDT").ToString), OASISConstants.DateFormat) Else txtResig.Text = ""
            'txtResig.Text = IIf((dr("EMP_RESGDT").ToString <> "") And (DateTime.Compare(CDate(dr("EMP_RESGDT")), CDate("1/1/1900")) <> 0), Format(CDate(dr("EMP_RESGDT").ToString), OASISConstants.DateFormat), "")
            'txtProb.Text = dr("EMP_PROBTILL")
            If ((dr("EMP_PROBTILL").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("EMP_PROBTILL")), CDate("1/1/1900")) <> 0)) Then txtProb.Text = Format(CDate(dr("EMP_PROBTILL").ToString), OASISConstants.DateFormat) Else txtProb.Text = ""
            'If (dr("EMP_PROBTILL").ToString <> "") Then txtProb.Text = Format(CDate(dr("EMP_PROBTILL").ToString), OASISConstants.DateFormat) Else txtProb.Text = ""
            'txtProb.Text = IIf((dr("EMP_PROBTILL").ToString <> "") And (DateTime.Compare(CDate(dr("EMP_PROBTILL")), CDate("1/1/1900")) <> 0), Format(CDate(dr("EMP_PROBTILL").ToString), OASISConstants.DateFormat), "")
            DoNullCheck(ddlAccom, dr("EMP_ACCOMODATION"))
            'ddlAccom.SelectedValue = IIf(dr("EMP_ACCOMODATION") Is DBNull.Value, 0, dr("EMP_ACCOMODATION").ToString())
            chkActive.Checked = IIf(dr("EMP_bACTIVE") Is DBNull.Value, False, dr("EMP_bACTIVE"))
            chkbPunching.Checked = IIf(dr("EMP_bReqPunching") Is DBNull.Value, False, dr("EMP_bReqPunching"))
            chkAllowOT.Checked = IIf(dr("EMP_bOT") Is DBNull.Value, False, dr("EMP_bOT"))
            DoNullCheck(ddlSalC, dr("EMP_CUR_ID"))
            'ddlSalC.SelectedValue = IIf(dr("EMP_CUR_ID") Is DBNull.Value, 0, dr("EMP_CUR_ID").ToString())
            DoNullCheck(ddlPayC, dr("EMP_PAY_CUR_ID"))
            'ddlPayC.SelectedValue = IIf(dr("EMP_PAY_CUR_ID") Is DBNull.Value, 0, dr("EMP_PAY_CUR_ID").ToString())
            DoNullCheck(ddlPayMode, dr("EMP_MODE"))
            'ddlPayMode.SelectedValue = IIf(dr("EMP_MODE") Is DBNull.Value, 0, dr("EMP_MODE").ToString())
            hfBank_ID.Value = dr("EMP_BANK").ToString

            txtVacationPolicy.Text = dr("BLS_DESCRIPTION").ToString
            hf_VacationPolicy.Value = dr("EMP_BLS_ID").ToString
            txtBname.Text = dr("BANK_DESCR").ToString()
            txtAccCode.Text = dr("EMP_ACCOUNT").ToString()
            txtBcode.Text = dr("EMP_SWIFTCODE").ToString
            txtME.Text = dr("EMPMOE_DESCR").ToString
            hfME_ID.Value = dr("EMP_MOE_DES_ID").ToString
            hfML_ID.Value = dr("EMP_VISA_DES_ID").ToString  'Visa DestinationID
            txtIU.Text = dr("VISA_BSU_NAME").ToString()
            txtML.Text = dr("EMPVISA_DESCR").ToString()
            hfIU.Value = dr("EMP_VISA_BSU_ID").ToString
            DoNullCheck(ddlMstatus, dr("EMP_MARITALSTATUS"))
            'ddlMstatus.SelectedValue = IIf(dr("EMP_MARITALSTATUS") Is DBNull.Value, 0, dr("EMP_MARITALSTATUS"))
            txtGsalary.Text = AccountFunctions.Round(dr("EMP_GROSSSAL").ToString)
            txtPassNo.Text = dr("EMP_PASSPORT").ToString
            txtRemark.Text = dr("EMP_REMARKS").ToString
            txtFTE.Text = IIf(dr("EMP_FTE") Is DBNull.Value, 1, dr("EMP_FTE").ToString)
            hfCountry_ID.Value = dr("EMP_CTY_ID").ToString
            txtCountry.Text = dr("EMP_CNTR_DESCR").ToString
            If (Not dr("EMP_SEX_bMALE") Is DBNull.Value) AndAlso dr("EMP_SEX_bMALE") Then
                rdMale.Checked = True
            Else
                rdFemale.Checked = True
            End If
            hfQul.Value = dr("EMP_QLF_ID").ToString
            'txtQual.Text = dr("EMP_QUAL_DESCR").ToString
            txtSalGrade.Text = dr("EMP_SCALE_GRADE_DESCR").ToString
            hfSalGrade.Value = IIf(TypeOf (dr("EMP_SGD_ID")) Is DBNull, String.Empty, dr("EMP_SGD_ID"))
            hfDept_ID.Value = dr("EMP_DPT_ID").ToString
            txtDept.Text = dr("EMP_DEPT_DESCR").ToString
            DoNullCheck(ddlBgroup, dr("EMP_BLOODGRP"))
            'ddlBgroup.SelectedValue = IIf(dr("EMP_BLOODGRP") Is DBNull.Value, 0, dr("EMP_BLOODGRP").ToString)
            txtFather.Text = dr("EMP_FATHER").ToString
            txtMother.Text = dr("EMP_MOTHER").ToString
            hfReport.Value = dr("EMP_REPORTTO_EMP_ID").ToString
            txtReport.Text = dr("EMP_REP_TO_Name").ToString
            txtStaffNo.Text = dr("EMP_STAFFNO").ToString
            DoNullCheck(ddlCategory, dr("EMP_ABC"))
            txtVisaPrgID.Text = dr("EMP_VISA_ID").ToString
            txtPersonID.Text = dr("EMP_PERSON_ID").ToString
            txtUIDNo.Text = dr("EMP_UIDNO").ToString
            'ddlCategory.SelectedValue = IIf(dr("EMP_ABC") Is DBNull.Value, 0, dr("EMP_ABC"))
            ' DoNullCheck(ddlAirTicketAvailable, dr("EMP_TICKETFLAG"))  got reversed, correcting
            DoNullCheck(ddlAirTicketAvailable, dr("EMP_TICKETFLAG_Eligible"))
            'ddlAirTicketAvailable.SelectedValue = IIf(dr("EMP_TICKETFLAG") Is DBNull.Value, 0, dr("EMP_TICKETFLAG"))
            txtAirticketCity.Text = dr("AIRTICKETCITY_DESCR").ToString
            h_AirTcket_City_ID.Value = dr("EMP_TICKET_CIT_ID").ToString
            txtAirTicketNo.Text = CDbl(IIf(dr("EMP_TICKETCOUNT").ToString = "", 0, dr("EMP_TICKETCOUNT")))
            DoNullCheck(ddlAirTicketClass, dr("EMP_TICKETCLASS"))
            'ddlAirTicketClass.SelectedValue = IIf(dr("EMP_TICKETCLASS") Is DBNull.Value, 0, dr("EMP_TICKETCLASS"))
            'EMP_ID = dr("EMP_ID")
            chkTempEmp.Checked = IIf(dr("EMP_bTemp") Is DBNull.Value, False, dr("EMP_bTemp"))
            DoNullCheck(ddlInsuCardTypes, dr("EMP_INSU_ID"))
            hfAttendBy.Value = dr("EMP_ATT_Approv_EMP_ID").ToString
            txtAttendBy.Text = dr("empAttendanceApprover").ToString
            txtCostCenter.Text = dr("CostCenterDescr").ToString   'V1.6
            hf_CostCenterID.Value = dr("CostCenterID").ToString 'V1.6

            'V2.0 starts
            txtFromAirCity.Text = IIf(dr("FromCity") Is DBNull.Value, "", dr("FromCity").ToString)
            h_FromAirTcket_City_ID.Value = dr("EMP_FROM_TICKET_CIT_ID").ToString
            ddlInsuActual.SelectedValue = IIf(dr("EMP_INSUFLAG") Is DBNull.Value, "0", dr("EMP_INSUFLAG"))
            ddlInsuEligible.SelectedValue = IIf(dr("EMP_INSUFLAG_ELIGIBLE") Is DBNull.Value, "0", dr("EMP_INSUFLAG_ELIGIBLE"))
            ' ddlTicketActual.SelectedValue = IIf(dr("EMP_TICKETFLAG_Eligible") Is DBNull.Value, 0, dr("EMP_TICKETFLAG_Eligible")) -- got reversed,correcting
            ddlTicketActual.SelectedValue = IIf(dr("EMP_TICKETFLAG") Is DBNull.Value, 0, dr("EMP_TICKETFLAG"))
            DoNullCheck(ddlStaffPoint, dr("EMP_STP_ID"))
            'V2.0 ends

            BindCountry()
            ddlCOB.SelectedValue = IIf(dr("EMP_COB") Is DBNull.Value, "5", dr("EMP_COB").ToString)
            ddlNativeLang.SelectedValue = IIf(dr("EMP_NATIVE_LANG") Is DBNull.Value, 0, dr("EMP_NATIVE_LANG").ToString)
            ddlHighEdu.SelectedValue = IIf(dr("EMP_HIGH_EDU") Is DBNull.Value, 0, dr("EMP_HIGH_EDU").ToString)
            txtNoofDepen.Text = IIf(dr("EMP_NO_OF_DEPENDENTS") Is DBNull.Value, 0, dr("EMP_NO_OF_DEPENDENTS").ToString)
            ddlDimDepart.SelectedValue = IIf(dr("EMP_DIM_DEPT") Is DBNull.Value, 0, dr("EMP_DIM_DEPT").ToString)
            ddlDimStaffLvl.SelectedValue = IIf(dr("EMP_DIM_STAFF_LEVEL") Is DBNull.Value, 0, dr("EMP_DIM_STAFF_LEVEL").ToString)
            hf_jobmaster_id.Value = IIf(dr("EMP_JOB_MASTERID") Is DBNull.Value, 0, dr("EMP_JOB_MASTERID").ToString)
            ddlPostype.SelectedValue = IIf(dr("EMP_JOB_POS_TYPE") Is DBNull.Value, 0, dr("EMP_JOB_POS_TYPE").ToString)

            If hf_jobmaster_id.Value <> "" AndAlso hf_jobmaster_id.Value <> "0" Then
                FillJobMasterById(Convert.ToInt32(hf_jobmaster_id.Value))
            End If



        End While
        dr.Close()
        FillContact(empID)
        FillSalaryTab(empID)
        FillDocumentTab(empID)
        FillDocumentTab1(empID)
        FillQualificationTab(empID)
        FillLangTab(empID)
        FillExperienceTab(empID)
        FillDependanceDetailsTab(empID)
        FillWPSDetails(empID) 'V1.1
    End Sub

    Private Sub DoNullCheck(ByVal ctrl As WebControl, ByVal input As Object)
        If TypeOf (ctrl) Is DropDownList Then
            Dim ddlist As DropDownList = ctrl
            If (Not input Is Nothing) AndAlso (Not input Is DBNull.Value) Then
                If ddlist.Items.FindByValue(input) Is Nothing Then
                    ddlist.SelectedIndex = 0
                Else
                    ddlist.SelectedIndex = -1
                    ddlist.Items.FindByValue(input).Selected = True
                    'ddlist.SelectedValue = input
                End If
            Else
                ddlist.SelectedIndex = 0
            End If
        End If
    End Sub
    'To add employee details on search selection

    Private Sub FillJobMasterById(ByVal JobMID As Integer)
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = String.Empty
            str_Sql = "SELECT JM_NAME  FROM JOB_MASTER a where JM_ID= " & JobMID

            Dim JMNAME As String = SqlHelper.ExecuteScalar(CONN, CommandType.Text, str_Sql)

            txtJobMaster.Text = JMNAME.ToString
        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading"
        End Try
    End Sub

    Private Sub FillContact(ByVal empID As Integer)
        Dim dtable As DataTable = CreateSalaryDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "select * from vw_OSO_EMPLOYEECONTACT where EMD_EMP_ID = " & empID
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            txtContCurAddress.Text = dr("EMD_CUR_ADDRESS").ToString
            txtContCurrPOBox.Text = dr("EMD_CUR_POBOX").ToString
            txtContCurrCity.Text = dr("EMD_CUR_CITY").ToString
            hfContCCountry_ID.Value = dr("EMD_CUR_CTY_ID").ToString
            txtContCurrCountry.Text = dr("CUR_CTY_DESCR").ToString
            txtContEmail.Text = dr("EMD_EMAIL").ToString
            txtContCurrPhoneNo.Text = dr("EMD_CUR_PHONE").ToString
            txtContCurrMobNo.Text = dr("EMD_CUR_MOBILE").ToString
            txtContPermAddress.Text = dr("EMD_PERM_ADDRESS").ToString
            txtContPermPOBox.Text = dr("EMD_PERM_POBOX").ToString
            txtContPermCity.Text = dr("EMD_PERM_CITY").ToString
            hfContPCountry_ID.Value = dr("EMD_PERM_CTY_ID").ToString
            txtContPermCountry.Text = dr("PERM_CTY_DESCR").ToString
            txtContPermPhoneNo.Text = dr("EMD_PERM_PHONE").ToString
            'txtContPermMobNo.Text = dr("EMD_PERM_MOBILE").ToString
            txtContAccomLocation.Text = dr("EMD_ACCOM_LOCATION").ToString
            'ddlContAccomType.SelectedValue = 
            DoNullCheck(ddlContAccomType, dr("EMD_ACCOM_TYPE"))
            txtContFlatNo.Text = dr("EMD_ACCOM_FLAT_NO").ToString
            chkContBSharing.Checked = IIf(TypeOf (dr("EMD_ACCOM_BSHARING")) Is DBNull, False, dr("EMD_ACCOM_BSHARING"))
            txtContCostUnit.Text = dr("EMD_CUT_DESCR").ToString
            h_ContCostUnitID.Value = dr("EMD_CUT_ID").ToString
            txtContLocalContact.Text = dr("EMD_CONTACT_LOCAL").ToString
            txtContLocalContNo.Text = dr("EMD_CONTACT_LOCAL_NUM").ToString
            txtContHomecont.Text = dr("EMD_CONTACT_HOME").ToString
            txtContHomecontNo.Text = dr("EMD_CONTACT_HOME_NUM").ToString
            txtPersonalMail.Text = dr("EMD_Personal_EmailID").ToString
            'Dim strImagePath As String = dr("EMD_PHOTO")
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & dr("EMD_PHOTO")
            imgEmpImage.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
            ViewState("EMP_IMAGE_PATH") = dr("EMD_PHOTO")
            txtFeeActual.Text = IIf(TypeOf (dr("emd_FeeConcessionActual")) Is DBNull, "", dr("emd_FeeConcessionActual").ToString)
            txtFeeEligible.Text = IIf(TypeOf (dr("emd_FeeConcessionEligible")) Is DBNull, "", dr("emd_FeeConcessionEligible").ToString)
            DoNullCheck(ddlConEligibility, dr("emd_FCE_ID"))
            hf_FeeConcessionCategory_ID.Value = IIf(TypeOf (dr("emd_FCE_ID")) Is DBNull, "", dr("emd_FCE_ID").ToString)

            If Session("BSU_COUNTRY_ID") = 6 Then
                txt_AadharCardNo.Text = IIf(TypeOf (dr("EMD_AADHAR_CARD_NO")) Is DBNull, "", dr("EMD_AADHAR_CARD_NO").ToString)
                txt_PanCardNo.Text = IIf(TypeOf (dr("EMD_PAN_CARD_NO")) Is DBNull, "", dr("EMD_PAN_CARD_NO").ToString)
            End If
        End While
        dr.Close()
    End Sub

    Private Sub FillDependanceDetailsTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateDependanceDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        'str_Sql = "select EMPDEPENDANTS_D.*,INSU_DESCR from EMPDEPENDANTS_D left join INSURANCETYPE_M on INSU_ID=EDD_Insu_id "
        'str_Sql += " where EDD_EMP_ID = " & empID
        str_Sql = "exec GetDependantData " & empID
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EDD_ID")
            ldrTempNew.Item("EDD_NAME") = dr("EDD_NAME")
            ldrTempNew.Item("EDD_RELATION") = dr("EDD_RELATION")
            ldrTempNew.Item("EDD_DOB") = Format(dr("EDD_DOB"), OASISConstants.DateFormat)
            ldrTempNew.Item("EDD_NoofTicket") = dr("EDD_NoofTicket")
            ldrTempNew.Item("bConcession") = dr("EDD_bConcession")
            ldrTempNew.Item("Status") = "SELECT"
            ldrTempNew.Item("EDD_Insu_id") = dr("EDD_Insu_id")
            ldrTempNew.Item("InsuranceCategoryDESC") = dr("INSU_DESCR")
            ldrTempNew.Item("EDD_bCompanyInsurance") = dr("EDD_bCompanyInsurance")
            ldrTempNew.Item("EDD_GEMS_TYPE") = dr("EDD_GEMS_TYPE")
            ldrTempNew.Item("EDD_BSU_ID") = dr("EDD_BSU_ID")
            ldrTempNew.Item("EDD_GEMS_TYPE_DESCR") = dr("EDD_GEMS_TYPE_DESCR")
            ldrTempNew.Item("EDD_BSU_NAME") = dr("EDD_BSU_NAME")
            ldrTempNew.Item("EDD_GEMS_ID") = dr("EDD_GEMS_ID")

            ldrTempNew.Item("EDC_DOCUMENT_NO") = dr("EDC_DOCUMENT_NO")
            If IsDate(dr("EDC_EXP_DT")) Then
                ldrTempNew.Item("EDC_EXP_DT") = Format(dr("EDC_EXP_DT"), OASISConstants.DateFormat)
            End If

            ldrTempNew.Item("EDD_bMALE") = dr("EDD_bMALE")
            ldrTempNew.Item("EDC_Gender") = dr("EDC_Gender")
            ldrTempNew.Item("EDD_MSTATUS") = dr("EDD_MSTATUS")
            ldrTempNew.Item("EDD_MARITALSTATUS") = dr("EDD_MARITALSTATUS")
            ldrTempNew.Item("EDD_Photo") = dr("EDD_Photo")
            ldrTempNew.Item("EDD_bPhoto") = dr("EDD_bPhoto")
            ldrTempNew.Item("EDD_CTY_ID") = dr("EDD_CTY_ID")
            ldrTempNew.Item("CTY_NATIONALITY") = dr("CTY_NATIONALITY")

            ldrTempNew.Item("EDD_PASSPRTNO") = dr("EDD_PASSPRTNO")
            ldrTempNew.Item("EDD_UIDNO") = dr("EDD_UIDNO")

            If IsDate(dr("EDD_VISAISSUEDATE")) Then
                ldrTempNew.Item("EDD_VISAISSUEDATE") = Format(dr("EDD_VISAISSUEDATE"), OASISConstants.DateFormat)
            End If
            If IsDate(dr("EDD_VISAEXPDATE")) Then
                ldrTempNew.Item("EDD_VISAEXPDATE") = Format(dr("EDD_VISAEXPDATE"), OASISConstants.DateFormat)
            End If


            ldrTempNew.Item("EDD_VISAISSUEPLACE") = dr("EDD_VISAISSUEPLACE")

            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPDEPENDANCEDETAILS") = dtable
        GridBindDependanceDetails()
    End Sub

    Private Sub FillQualificationTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateQualificationDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "SELECT * from VW_OSO_EMPQUALIFICATION "
        str_Sql += " where EQS_EMP_ID = " & empID

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EQS_ID")
            ldrTempNew.Item("Qualification") = dr("QLF_DESCR")
            ldrTempNew.Item("QualificationOtherText") = dr("EQS_CERT_TEXT")
            ldrTempNew.Item("Qualification_ID") = dr("EMP_QLF_ID")
            ldrTempNew.Item("Qualif_Category") = dr("QCT_Descr")
            ldrTempNew.Item("Institute") = dr("EMP_INSTITUTE")
            ldrTempNew.Item("VStatus") = dr("EMP_STATUS")
            ldrTempNew.Item("Grade") = dr("EQS_GRADE")
            ldrTempNew.Item("Grade_ID") = 0

            ldrTempNew.Item("Qualif_Category_ID") = dr("EQS_QCT_ID")
            ldrTempNew.Item("Qualification_Level") = dr("EQS_QCL_ID")
            ldrTempNew.Item("cSpecialization") = dr("EQS_SPECIALIZATION")
            ldrTempNew.Item("cExpiryDate") = dr("EQS_EXPIRYDATE")
            ldrTempNew.Item("cAwardingbody") = dr("EQS_AWARDINGBODY")
            ldrTempNew.Item("cLocation") = dr("EQS_LOCATION")
            ldrTempNew.Item("cYearOfQual") = dr("EQS_YEAROFQUAL")
            ldrTempNew.Item("cRegno") = dr("EQS_REGNO")
            ldrTempNew.Item("cRemarks") = dr("EQS_REMARKS")
            ldrTempNew.Item("DocFile") = dr("EMD_DOCFILE")
            ldrTempNew.Item("DocFileType") = dr("EMD_DOCFILE_TYPE")
            ldrTempNew.Item("DocFileMIME") = dr("EMD_DOCFILE_MIME")
            ldrTempNew.Item("cIssueDate") = dr("EQS_STARTDATE")



            ldrTempNew.Item("Status") = "INSERT"
            'EQS_EMP_ID
            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPQUALDETAILS") = dtable
        GridBindQualificationDetails()
    End Sub
    Private Sub FillLangTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateLangTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "SELECT * from VW_OSO_EMPLANG "
        str_Sql += " where EQL_EMP_ID = " & empID

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EQL_ID")
            ldrTempNew.Item("Language") = dr("LANG_DESCRIPTION")
            ldrTempNew.Item("Language_ID") = dr("EQL_LANG_ID")
            ldrTempNew.Item("Read") = dr("Read_Descr")
            ldrTempNew.Item("Write") = dr("Write_Descr")
            ldrTempNew.Item("Speak") = dr("Speak_Descr")
            ldrTempNew.Item("Status") = "INSERT"
            'EQS_EMP_ID
            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPLANGDETAILS") = dtable
        GridBindLangDetails()
    End Sub
    Private Sub FillExperienceTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateExperienceDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "select * from EMPEXPERIANCE_D where EMX_EMP_ID = " & empID
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EMX_ID")
            ldrTempNew.Item("Organisation") = dr("EMX_ORGANISATION")
            ldrTempNew.Item("Address") = dr("EMX_ADDRESS")
            ldrTempNew.Item("Designation") = dr("EMX_POSITION")
            ldrTempNew.Item("Remarks") = dr("EMX_REMARKS")
            ldrTempNew.Item("Reference1") = dr("EMX_REF1")
            ldrTempNew.Item("ReferenceNo1") = dr("EMX_REF1_NUM")
            ldrTempNew.Item("Reference2") = dr("EMX_REF2")
            ldrTempNew.Item("ReferenceNo2") = dr("EMX_REF2_NUM")
            If dr("EMX_DTFROM") IsNot Nothing AndAlso dr("EMX_DTFROM") IsNot DBNull.Value Then
                ldrTempNew.Item("FromDate") = Format(CDate(dr("EMX_DTFROM")), OASISConstants.DateFormat)
            End If
            If dr("EMX_DTTO") IsNot Nothing AndAlso dr("EMX_DTTO") IsNot DBNull.Value Then
                ldrTempNew.Item("ToDate") = Format(CDate(dr("EMX_DTTO")), OASISConstants.DateFormat)
            End If
            ldrTempNew.Item("Status") = "INSERT"
            'EMX_EMP_ID
            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPEXPDETAILS") = dtable
        GridBindExperienceDetails()
    End Sub

    Private Sub FillSalaryTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateSalaryDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal
        str_Sql = "SELECT     EMPSALARYHS_s.*, EMPSALCOMPO_M.ERN_DESCR FROM EMPSALARYHS_s LEFT OUTER JOIN EMPSALCOMPO_M" & _
        " ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID where ESH_EMP_ID = " & empID & " and ESH_TODT is null ORDER BY ERN_ORDER "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            grossSal += dr("ESH_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = dr("ESH_ID")
            ldrTempNew.Item("ENR_ID") = dr("ESH_ERN_ID")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = dr("ESH_bMonthly")
            ldrTempNew.Item("PAYTERM") = dr("ESL_PAYTERM")
            ldrTempNew.Item("PAY_SCHEDULE") = GETPAY_SCHEDULE(dr("ESL_PAYTERM"))
            ldrTempNew.Item("Amount") = dr("ESH_AMOUNT")
            If dr("ESH_ELIGIBILITY") Is DBNull.Value Then
                ldrTempNew.Item("Amount_ELIGIBILITY") = dr("ESH_AMOUNT")
            Else
                ldrTempNew.Item("Amount_ELIGIBILITY") = dr("ESH_ELIGIBILITY")
            End If
            ldrTempNew.Item("Status") = "LOADED"
            dtable.Rows.Add(ldrTempNew)
            'Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
            'FillAllocationPopup(dr("ESH_ERN_ID"), dr("ESH_ID"))
        End While
        dr.Close()
        Session("EMPSALDETAILS") = dtable
        GridBindSalaryDetails()
    End Sub

    Private Function GETPAY_SCHEDULE(ByVal vPAYTERM As Integer) As String
        Select Case vPAYTERM
            Case 0
                Return "Monthly"
            Case 1
                Return "Bimonthly"
            Case 2
                Return "Quarterly"
            Case 3
                Return "Half-yearly"
            Case 4
                Return "Yearly"
        End Select
        Return ""
    End Function
    Private Sub FillDocumentTab(ByVal empID As Integer)
        Dim dtable As DataTable = CreateDocDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "SELECT ISNULL(EMPDOCUMENTS_S.EMD_ESD_ID,'') EMD_ESD_ID, ISNULL(EMPDOCUMENTS_S.EMD_ID,'') EMD_ID," & _
        " ISNULL(EMPDOCUMENTS_S.EMD_NO,'') EMD_NO, ISNULL(EMPDOCUMENTS_S.EMD_bACTIVE,0) EMD_bACTIVE," & _
        " ISNULL(EMPDOCUMENTS_S.EMP_ISSUEPLACE,'') EMP_ISSUEPLACE, ISNULL(EMPDOCUMENTS_S.EMD_EXPDT, '') EMD_EXPDT " & _
        ",ISNULL(EMPDOCUMENTS_S.EMD_ISSUEDT,'') EMD_ISSUEDT , ISNULL(EMPSTATDOCUMENTS_M.ESD_DESCR,'') ESD_DESCR,EMD_DOCFILE,EMD_DOCFILE_TYPE,EMD_DOCFILE_MIME " & _
        "FROM EMPDOCUMENTS_S LEFT OUTER JOIN EMPSTATDOCUMENTS_M ON EMPDOCUMENTS_S.EMD_ESD_ID = EMPSTATDOCUMENTS_M.ESD_ID "
        str_Sql += " where EMD_EMP_ID = " & empID & "and EMPSTATDOCUMENTS_M.ESD_bMAINDOC =1 Order By ESD_ORDER"   'Added for V1.2
        'str_Sql += " where EMD_EMP_ID = " & empID                          'Commented for V1.2

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EMD_ID")
            ldrTempNew.Item("Document") = dr("ESD_DESCR")
            ldrTempNew.Item("DocID") = dr("EMD_ESD_ID")
            ldrTempNew.Item("Doc_No") = dr("EMD_NO")
            ldrTempNew.Item("Doc_bActive") = dr("EMD_bACTIVE")
            ldrTempNew.Item("Doc_IssuePlace") = dr("EMP_ISSUEPLACE")
            If dr("EMD_ISSUEDT").ToString <> "" OrElse dr("EMD_ISSUEDT") <> DateTime.MinValue Then
                ldrTempNew.Item("Doc_IssueDate") = Format(dr("EMD_ISSUEDT"), OASISConstants.DateFormat)
            Else
                ldrTempNew.Item("Doc_IssueDate") = ""
            End If
            If dr("EMD_EXPDT").ToString <> "" OrElse dr("EMD_EXPDT") <> DateTime.MinValue Then
                ldrTempNew.Item("Doc_ExpDate") = Format(dr("EMD_EXPDT"), OASISConstants.DateFormat)
            Else
                ldrTempNew.Item("Doc_ExpDate") = ""
            End If
            ldrTempNew.Item("Status") = "LOADED"

            ldrTempNew.Item("DocFile") = dr("EMD_DOCFILE")
            ldrTempNew.Item("DocFileType") = dr("EMD_DOCFILE_TYPE")
            ldrTempNew.Item("DocFileMIME") = dr("EMD_DOCFILE_MIME")


            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPDOCDETAILS") = dtable
        GridBindDocDetails()
    End Sub

    Private Sub FillDocumentTab1(ByVal empID As Integer) ''New Modification
        Dim dtable As DataTable = CreateDocDetailsTable()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "SELECT ISNULL(EMPDOCUMENTS_S.EMD_ESD_ID,'') EMD_ESD_ID, ISNULL(EMPDOCUMENTS_S.EMD_ID,'') EMD_ID," & _
        " ISNULL(EMPDOCUMENTS_S.EMD_NO,'') EMD_NO, ISNULL(EMPDOCUMENTS_S.EMD_bACTIVE,0) EMD_bACTIVE," & _
        " ISNULL(EMPDOCUMENTS_S.EMP_ISSUEPLACE,'') EMP_ISSUEPLACE, ISNULL(EMPDOCUMENTS_S.EMD_EXPDT, '') EMD_EXPDT " & _
        ",ISNULL(EMPDOCUMENTS_S.EMD_ISSUEDT,'') EMD_ISSUEDT , ISNULL(EMPSTATDOCUMENTS_M.ESD_DESCR,'') ESD_DESCR,EMD_DOCFILE,EMD_DOCFILE_TYPE,EMD_DOCFILE_MIME " & _
        "FROM EMPDOCUMENTS_S LEFT OUTER JOIN EMPSTATDOCUMENTS_M ON EMPDOCUMENTS_S.EMD_ESD_ID = EMPSTATDOCUMENTS_M.ESD_ID "
        str_Sql += " where EMD_EMP_ID = " & empID & " and EMPSTATDOCUMENTS_M.ESD_bMAINDOC =0 Order By ESD_ORDER"   'Added for V1.2
        'str_Sql += " where EMD_EMP_ID = " & empID                          'Commented for V1.2

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dtable.NewRow
            ldrTempNew.Item("UniqueID") = dr("EMD_ID")
            ldrTempNew.Item("Document") = dr("ESD_DESCR")
            ldrTempNew.Item("DocID") = dr("EMD_ESD_ID")
            ldrTempNew.Item("Doc_No") = dr("EMD_NO")
            ldrTempNew.Item("Doc_bActive") = dr("EMD_bACTIVE")
            ldrTempNew.Item("Doc_IssuePlace") = dr("EMP_ISSUEPLACE")
            If dr("EMD_ISSUEDT").ToString <> "" OrElse dr("EMD_ISSUEDT") <> DateTime.MinValue Then
                ldrTempNew.Item("Doc_IssueDate") = Format(dr("EMD_ISSUEDT"), OASISConstants.DateFormat)
            Else
                ldrTempNew.Item("Doc_IssueDate") = ""
            End If
            If dr("EMD_EXPDT").ToString <> "" OrElse dr("EMD_EXPDT") <> DateTime.MinValue Then
                ldrTempNew.Item("Doc_ExpDate") = Format(dr("EMD_EXPDT"), OASISConstants.DateFormat)
            Else
                ldrTempNew.Item("Doc_ExpDate") = ""
            End If
            ldrTempNew.Item("Status") = "LOADED"

            ldrTempNew.Item("DocFile") = dr("EMD_DOCFILE")
            ldrTempNew.Item("DocFileType") = dr("EMD_DOCFILE_TYPE")
            ldrTempNew.Item("DocFileMIME") = dr("EMD_DOCFILE_MIME")


            dtable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPDOCDETAILS1") = dtable
        GridBindDocDetails1()
    End Sub
    'V1.1 starts
    'To get WPS agent details of employee
    Private Sub FillWPSDetails(ByVal empID As Integer)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Try

            objConn.Open()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@empID", empID)
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_EMP_WPSDetails", pParms)

            If dr.HasRows Then
                While dr.Read()
                    hf_WPSID.Value = dr("EMP_WPSID")
                    txtWPSDesc.Text = dr("EMP_WPA_DESCR")

                End While


            End If

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        Finally
            objConn.Close()
        End Try


    End Sub
#End Region

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        If ddlStatus.SelectedValue = "4" Or ddlStatus.SelectedValue = "5" Then
            txtResig.Enabled = False
            imgBtnReg.Visible = False
            txtResig.Text = ""
            chkActive.Checked = False
        Else
            txtResig.Enabled = True
            imgBtnReg.Visible = True
            chkActive.Checked = True
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtFTE.Text = "" Then
            txtFTE.Text = "1"
        End If
        If txtNoofDepen.Text = "" Then
            txtNoofDepen.Text = "0"
        End If
        ViewState("Status") = 0
        ServerValidate()


        '   --- Proceed To Save
        If (lstrErrMsg = "") Then

            If Page.IsValid = True Then
                Dim Status As Integer
                Dim bEdit As Boolean
                Dim bitBActive As Boolean
                Dim bitBmale As Boolean
                Dim newNo As String = String.Empty
                Dim reportTo As String = String.Empty

                If chkActive.Checked = True Then
                    bitBActive = True
                Else
                    bitBActive = False
                End If

                If rdMale.Checked = True Then
                    bitBmale = True
                Else
                    bitBmale = False
                End If

                'check the data mode to do the required operation
                Dim strEmpNo As String = String.Empty
                Dim EmpID As Integer = IIf(ViewState("viewid") IsNot Nothing, ViewState("viewid"), 0)

                If ViewState("datamode") = "edit" Then
                    bEdit = True
                    Dim transaction As SqlTransaction

                    'update  the new user
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try
                            Dim MaxChildConcession, LOPOpening As Integer
                            Dim airTicketNo As Double
                            If IsNumeric(txtAirTicketNo.Text) Then
                                airTicketNo = CDbl(txtAirTicketNo.Text) 'Convert.ToInt32(txtAirTicketNo.Text)
                            Else
                                airTicketNo = 0
                            End If
                            If IsNumeric(txtMaxChildConcession.Text) Then
                                MaxChildConcession = Convert.ToInt32(txtMaxChildConcession.Text)
                            Else
                                MaxChildConcession = 0
                            End If

                            If IsNumeric(txtLOPOpening.Text) Then
                                LOPOpening = Convert.ToInt32(txtLOPOpening.Text)
                            Else
                                LOPOpening = 0
                            End If

                            Dim availCompanyTransportation As Boolean = False
                            Select Case ddlEmpTransportation.SelectedValue
                                Case 0
                                    availCompanyTransportation = False
                                Case 1
                                    availCompanyTransportation = True
                            End Select
                            Dim userName As String = Session("sUsr_name")
                            If hfME_ID.Value = "" Then
                                hfME_ID.Value = hfSD_ID.Value
                            End If
                            If hfML_ID.Value = "" Then
                                hfML_ID.Value = hfSD_ID.Value
                            End If
                            hfGrade_ID.Value = ddlGradelevel.SelectedValue
                            'If hfGrade_ID.Value = "" Or hfGrade_ID.Value = "0" Then
                            '    lblError.Text = "Please select a Grade Level."
                            '    Exit Sub
                            'End If
                            If hf_VacationPolicy.Value = "" Then
                                lblError.Text = "Please select a Vacation Policy."
                                Exit Sub
                            End If
                            If hf_CostCenterID.Value = "" Then
                                lblError.Text = "Please select a Cost Center."
                                Exit Sub
                            End If
                            Dim InsuType As Integer = 0
                            If ddlInsuCardTypes.SelectedIndex <> 0 Then
                                InsuType = ddlInsuCardTypes.SelectedValue
                            End If
                            If txtAttendBy.Text = "" Or hfAttendBy.Value = "" Then
                                hfAttendBy.Value = 0
                            End If

                            'V1.1--Interchanged txtDOJGroup.Text, txtJdate.Text as DOJ is always same and txtJdate is BSU joining date.
                            Status = AccessRoleUser.SAVEEMPLOYEE_M(userName, txtEmpno1.Text, txtAppNo.Text, ddlEmpSalute.SelectedValue, txtFname1.Text, txtMname1.Text, txtLname1.Text, hfSD_ID.Value, hfCat_ID.Value, _
                            hfGrade_ID.Value, hfWU.Value, hfTeachGrade_ID.Value, ddlVtype.SelectedValue, ddlVstatus.SelectedValue, txtDOJGroup.Text, txtJdate.Text, txtViss_date.Text, txtVExp_date.Text, _
                            txtLRdate.Text, chkOverSeasRecr.Checked, ddlStatus.SelectedValue, txtResig.Text, txtLastPresentDate.Text, txtProb.Text, ddlAccom.SelectedValue, bitBActive, ddlSalC.SelectedValue, ddlPayC.SelectedValue, ddlPayMode.SelectedValue, _
                            hfBank_ID.Value, txtAccCode.Text, txtBcode.Text, hfME_ID.Value, hfML_ID.Value, hfIU.Value, ddlMstatus.SelectedValue, txtGsalary.Text, txtPassNo.Text, txtRemark.Text, hfCountry_ID.Value, _
                            bitBmale, hfQul.Value, hfSalGrade.Value, hfDept_ID.Value, ddlBgroup.SelectedValue, txtFather.Text, _
                            txtMother.Text, hfReport.Value, hf_VacationPolicy.Value, txtStaffNo.Text, ddlCategory.SelectedValue, chkAllowOT.Checked, EmpID, chkTempEmp.Checked, availCompanyTransportation, _
                            CDate(txtEMPDOB.Text), ddlEmpReligion.SelectedValue, h_ApprovalPolicy.Value, ddlEmpContractType.SelectedValue, MaxChildConcession, LOPOpening, txtFTE.Text, chkbPunching.Checked, _
                            ddlTicketActual.SelectedValue, ddlAirTicketClass.SelectedValue, airTicketNo, h_AirTcket_City_ID.Value, txtVisaPrgID.Text, txtPersonID.Text, bEdit, newNo, EmpID, txtPassportname.Text, conn, transaction, txtKnownas.Text, InsuType, hfAttendBy.Value, hf_CostCenterID.Value, _
                            ddlAirTicketAvailable.SelectedValue, ddlInsuActual.SelectedValue, ddlInsuEligible.SelectedValue, h_FromAirTcket_City_ID.Value, ddlStaffPoint.SelectedValue, txtArabicName.Text, _
                            txtUIDNo.Text, ddlCOB.SelectedValue, ddlNativeLang.SelectedValue, ddlHighEdu.SelectedValue, txtNoofDepen.Text, ddlDimDepart.SelectedValue, ddlDimStaffLvl.SelectedValue, hf_jobmaster_id.Value, ddlPostype.SelectedValue)
                            ' interchanged ddlAirTicketAvailable.SelectedValue and  ddlTicketActual.SelectedValue
                            'If error occured during the process  throw exception and rollback the process

                            If Status = -1 Then
                                Throw New ArgumentException("Record does not exist for updating")
                            ElseIf Status <> 0 Then

                                Throw New ArgumentException("Error while updating the current record")
                                UtilityObj.Errorlog(UtilityObj.getErrorMessage(Status))
                            Else
                                'Store the required information into the Audit Trial table when Edited
                                Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EmpID & "-" & txtEmpno1.Text, "edit", Page.User.Identity.Name.ToString, Me.Page)
                                If Status <> 0 Then
                                    UtilityObj.Errorlog(UtilityObj.getErrorMessage(Status))
                                    Throw New ArgumentException("Could not complete your request")
                                End If
                                Dim retVal As Integer = SaveEMPDocumentDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveDependanceDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveContactDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEMPWPSDetails(conn, transaction, EmpID) 'V1.1
                                If retVal = 0 Then retVal = SaveEMPExperienceDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEMPSalaryDetails(conn, transaction, True, EmpID)
                                'If retVal = 0 Then retVal = SaveEMPSalarySchedule(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEMPQualificationDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEMPLangDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = UpdateTransactionTypeTable(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEmployeeAllocation(conn, transaction, EmpID, newNo)

                                If retVal = 0 Then
                                    'if everything is saved properly then Clear the Sessions
                                    ClearSession_GridDetails()
                                Else
                                    ViewState("Status") = 0
                                    lblError.Text = UtilityObj.getErrorMessage(retVal)
                                    Status = retVal
                                    Throw New ArgumentException("Could not complete your request")
                                End If
                                ViewState("datamode") = "none"
                                trsearchTable.Visible = True
                                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                transaction.Commit()
                                Call Update_Nat_No(EmpID)
                                ddlStatus.Visible = True
                                lblEmpStatus.Visible = False
                                ViewState("Status") = 1
                                Call clearme()
                                lblError.Text = "Record Updated Successfully"
                                '  Response.Redirect(ViewState("ReferrerUrl"))
                            End If
                        Catch myex As ArgumentException
                            If Status <> 0 Then
                                lblError.Text = UtilityObj.getErrorMessage(Status)
                            Else
                                lblError.Text = myex.Message
                            End If
                            ViewState("Status") = 0
                            transaction.Rollback()
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message)
                            ViewState("Status") = 0
                            transaction.Rollback()
                            lblError.Text = ex.Message '"Record could not be Updated"
                        End Try
                    End Using

                ElseIf ViewState("datamode") = "add" Then
                    bEdit = False
                    Dim transaction As SqlTransaction

                    'insert the new user
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try
                            Dim MaxChildConcession, LOPOpening As Integer
                            Dim airTicketNo As Double
                            If IsNumeric(txtAirTicketNo.Text) Then
                                airTicketNo = CDbl(txtAirTicketNo.Text)
                            Else
                                airTicketNo = 0
                            End If
                            If IsNumeric(txtMaxChildConcession.Text) Then
                                MaxChildConcession = Convert.ToInt32(txtMaxChildConcession.Text)
                            Else
                                MaxChildConcession = 0
                            End If
                            If IsNumeric(txtLOPOpening.Text) Then
                                LOPOpening = Convert.ToInt32(txtLOPOpening.Text)
                            Else
                                LOPOpening = 0
                            End If

                            Dim availCompanyTransportation As Boolean = False
                            Select Case ddlEmpTransportation.SelectedValue
                                Case 0
                                    availCompanyTransportation = False
                                Case 1
                                    availCompanyTransportation = True
                            End Select
                            Dim userName As String = Session("sUsr_name")
                            If hfME_ID.Value = "" Then
                                hfME_ID.Value = hfSD_ID.Value
                            End If
                            If hfML_ID.Value = "" Then
                                hfML_ID.Value = hfSD_ID.Value
                            End If
                            If hf_CostCenterID.Value = "" Then
                                lblError.Text = "Please select a Cost Center."
                                Exit Sub
                            End If
                            hfGrade_ID.Value = ddlGradelevel.SelectedValue
                            'If hfGrade_ID.Value = "" Or hfGrade_ID.Value = "0" Then
                            '    lblError.Text = "Please select a Grade Level."
                            '    Exit Sub
                            'End If

                            'V1.1 ---Interchanging txtJdate.Text and  txtDOJGroup.Text below as txtJdate is BSU joining date
                            Dim InsuType As Integer = 0
                            If ddlInsuCardTypes.SelectedIndex Then
                                InsuType = ddlInsuCardTypes.SelectedValue
                            End If
                            If hf_CostCenterID.Value = "" Then
                                hf_CostCenterID.Value = "0"
                            End If




                            Status = AccessRoleUser.SAVEEMPLOYEE_M(userName, txtEmpno1.Text, txtAppNo.Text, ddlEmpSalute.SelectedItem.Text, txtFname1.Text, txtMname1.Text, txtLname1.Text, hfSD_ID.Value, hfCat_ID.Value, _
                            hfGrade_ID.Value, hfWU.Value, hfTeachGrade_ID.Value, ddlVtype.SelectedValue, ddlVstatus.SelectedValue, txtDOJGroup.Text, txtJdate.Text, txtViss_date.Text, txtVExp_date.Text, _
                            txtLRdate.Text, chkOverSeasRecr.Checked, ddlStatus.SelectedValue, txtResig.Text, txtLastPresentDate.Text, txtProb.Text, ddlAccom.SelectedValue, bitBActive, ddlSalC.SelectedValue, ddlPayC.SelectedValue, ddlPayMode.SelectedValue, _
                            hfBank_ID.Value, txtAccCode.Text, txtBcode.Text, hfME_ID.Value, hfML_ID.Value, hfIU.Value, ddlMstatus.SelectedValue, txtGsalary.Text, txtPassNo.Text, txtRemark.Text, hfCountry_ID.Value, _
                            bitBmale, hfQul.Value, hfSalGrade.Value, hfDept_ID.Value, ddlBgroup.SelectedValue, txtFather.Text, _
                            txtMother.Text, hfReport.Value, hf_VacationPolicy.Value, txtStaffNo.Text, ddlCategory.SelectedValue, chkAllowOT.Checked, 0, chkTempEmp.Checked, availCompanyTransportation, _
                            CDate(txtEMPDOB.Text), ddlEmpReligion.SelectedValue, h_ApprovalPolicy.Value, ddlEmpContractType.SelectedValue, MaxChildConcession, LOPOpening, txtFTE.Text, chkbPunching.Checked, _
                            ddlTicketActual.SelectedValue, ddlAirTicketClass.SelectedValue, airTicketNo, h_AirTcket_City_ID.Value, txtVisaPrgID.Text, txtPersonID.Text, _
                            bEdit, newNo, EmpID, txtPassportname.Text, conn, transaction, txtKnownas.Text, InsuType, hfAttendBy.Value, hf_CostCenterID.Value, _
                             ddlAirTicketAvailable.SelectedValue, ddlInsuActual.SelectedValue, ddlInsuEligible.SelectedValue, h_FromAirTcket_City_ID.Value, ddlStaffPoint.SelectedValue, txtArabicName.Text, _
                            txtUIDNo.Text, ddlCOB.SelectedValue, ddlNativeLang.SelectedValue, ddlHighEdu.SelectedValue, txtNoofDepen.Text, ddlDimDepart.SelectedValue, ddlDimStaffLvl.SelectedValue, hf_jobmaster_id.Value, ddlPostype.SelectedValue)

                            ' interchanged ddlAirTicketAvailable.SelectedValue and  ddlTicketActual.SelectedValue

                            If Status = -1 Then
                                Throw New ArgumentException("Record with same ID already exist")
                            ElseIf Status <> 0 Then
                                Throw New ArgumentException("Error while inserting new record")
                                UtilityObj.Errorlog(UtilityObj.getErrorMessage(Status))
                            Else
                                'Store the required information into the Audit Trial table when inserted
                                Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EmpID & "-" & newNo, "insert", Page.User.Identity.Name.ToString, Me.Page)
                                If Status <> 0 Then
                                    Throw New ArgumentException("Could not complete your request")
                                End If
                                Dim retVal As Integer = SaveEMPDocumentDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveDependanceDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveContactDetails(conn, transaction, EmpID)
                                '-- add WPS agent details
                                If retVal = 0 Then retVal = SaveEMPWPSDetails(conn, transaction, EmpID) 'V1.1
                                If retVal = 0 Then retVal = SaveEMPExperienceDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEMPSalaryDetails(conn, transaction, True, EmpID)
                                If retVal = 0 Then retVal = SaveEMPQualificationDetails(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = UpdateTransactionTypeTable(conn, transaction, EmpID)
                                If retVal = 0 Then retVal = SaveEmployeeAllocation(conn, transaction, EmpID, newNo)
                                If retVal = 0 Then
                                    'if everything is saved properly then Clear the Sessions
                                    ClearSession_GridDetails()
                                    ViewState("datamode") = "none"
                                    trsearchTable.Visible = True
                                    ddlStatus.Visible = True
                                    lblEmpStatus.Visible = False
                                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                                    transaction.Commit()
                                    ViewState("Status") = 1
                                    txtEmpno1.Text = newNo
                                    Call Update_Nat_No(EmpID)
                                    Call clearme()
                                    lblError.Text = "Record Inserted Successfully"
                                    'Response.Redirect(ViewState("ReferrerUrl"))
                                Else
                                    'RollBack the Transaction
                                    ViewState("Status") = 0
                                    transaction.Rollback()
                                    lblError.Text = UtilityObj.getErrorMessage(retVal)
                                End If
                            End If
                        Catch myex As ArgumentException
                            UtilityObj.Errorlog(myex.Message)
                            ViewState("Status") = 0
                            transaction.Rollback()
                            If Status <> 0 Then
                                lblError.Text = UtilityObj.getErrorMessage(Status)
                            Else
                                lblError.Text = myex.Message
                            End If
                        Catch ex As Exception
                            ViewState("Status") = 0
                            transaction.Rollback()
                            lblError.Text = "Record could not be Inserted"
                            UtilityObj.Errorlog(ex.Message)
                        End Try
                    End Using
                End If
            End If
        End If
        If ViewState("Status") = 1 Then
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Private Sub Update_Nat_No(ByVal EMPID As Integer)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", EMPID)
            pParms(1) = New SqlClient.SqlParameter("@EMP_NAT_NO", txtNat_No.Text)
            If Not ddlGrade.SelectedValue = "-1" Then
                pParms(2) = New SqlClient.SqlParameter("@EMP_HRL_GRADE_ID", ddlGrade.SelectedValue)
                If Not ddlLevel.SelectedValue = "-1" Then
                    pParms(3) = New SqlClient.SqlParameter("@EMP_HRL_BAND", ddlLevel.SelectedItem.Text)
                End If
            End If

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SaveEmployee_NatNo", pParms)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub EnableGradeAndLevel()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid").ToString())
        Dim Status As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "CHECK_BSU_TO_SHOW_GRADE", pParms)
        If Status Then
            trGrade.Visible = True
            tr3.Visible = True
            tr4.Visible = True
        Else
            trGrade.Visible = False
            tr3.Visible = False
            tr4.Visible = False
        End If
    End Sub

    Private Function UpdateTransactionTypeTable(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim ht As New Hashtable
        ht(OASISConstants.TRANS_TYPE_Designation) = hfSD_ID.Value
        ht(OASISConstants.TRANS_TYPE_Grade) = hfGrade_ID.Value
        ht(OASISConstants.TRANS_TYPE_Category) = hfCat_ID.Value
        ht(OASISConstants.TRANS_TYPE_Mode_of_pay) = ddlPayMode.SelectedValue
        ht(OASISConstants.TRANS_TYPE_Accomodation) = ddlAccom.SelectedValue
        ht(OASISConstants.TRANS_TYPE_Company_Transportation) = IIf(ddlEmpTransportation.SelectedValue = 0, False, True)
        ht(OASISConstants.TRANS_TYPE_Department) = hfDept_ID.Value
        ht(OASISConstants.TRANS_TYPE_Department) = hfDept_ID.Value
        ht(OASISConstants.TRANS_TYPE_Employee_Account) = txtBcode.Text
        ht(OASISConstants.TRANS_TYPE_Employee_Bank) = hfBank_ID.Value

        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            Dim ienum As IDictionaryEnumerator = ht.GetEnumerator()
            While (ienum.MoveNext())
                cmd = New SqlCommand("SaveEMPTRANTYPE_TRN", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpEST_TTP_ID As New SqlParameter("@EST_TTP_ID", SqlDbType.Int)
                sqlpEST_TTP_ID.Value = ienum.Key
                cmd.Parameters.Add(sqlpEST_TTP_ID)

                Dim sqlpEST_BSU_ID As New SqlParameter("@EST_BSU_ID", SqlDbType.VarChar, 10)
                sqlpEST_BSU_ID.Value = hfWU.Value
                cmd.Parameters.Add(sqlpEST_BSU_ID)

                Dim sqlpEST_EMP_ID As New SqlParameter("@EST_EMP_ID", SqlDbType.VarChar, 15)
                sqlpEST_EMP_ID.Value = EmpID
                cmd.Parameters.Add(sqlpEST_EMP_ID)

                Dim sqlpEST_CODE As New SqlParameter("@EST_CODE", SqlDbType.VarChar, 10)
                sqlpEST_CODE.Value = ienum.Value
                cmd.Parameters.Add(sqlpEST_CODE)

                Dim sqlpEST_DTFROM As New SqlParameter("@EST_DTFROM", SqlDbType.DateTime)
                sqlpEST_DTFROM.Value = txtJdate.Text
                cmd.Parameters.Add(sqlpEST_DTFROM)

                Dim sqlpEST_DTTO As New SqlParameter("@EST_DTTO", SqlDbType.DateTime)
                sqlpEST_DTTO.Value = DBNull.Value
                cmd.Parameters.Add(sqlpEST_DTTO)

                Dim sqlpEST_REMARKS As New SqlParameter("@EST_REMARKS", SqlDbType.VarChar, 100)
                sqlpEST_REMARKS.Value = "New Value Inserted"
                cmd.Parameters.Add(sqlpEST_REMARKS)

                Dim sqlpEST_OLDCODE As New SqlParameter("@EST_OLDCODE", SqlDbType.VarChar, 10)
                sqlpEST_OLDCODE.Value = DBNull.Value
                cmd.Parameters.Add(sqlpEST_OLDCODE)

                Dim sqlpbFromMAsterForm As New SqlParameter("@bFromMAsterForm", SqlDbType.Bit)
                sqlpbFromMAsterForm.Value = True
                cmd.Parameters.Add(sqlpbFromMAsterForm)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue > 0 Then
                    Exit While
                End If
            End While

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Private Function SaveEmployeeAllocation(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer, ByVal empNo As String) As Integer
        Dim iReturnvalue As Integer
        Try
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("", objConn, stTrans)
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "delete empallocation where  EAL_EMP_ID=" & EmpID & " and EAL_FROMDT='" & ViewState("OLD_J_DATE") & "'"
            cmd.ExecuteNonQuery()

            cmd = New SqlCommand("SaveEmpAllocation", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEAL_EMP_ID As New SqlParameter("@EAL_EMP_ID", SqlDbType.VarChar, 15)
            sqlpEAL_EMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEAL_EMP_ID)

            Dim sqlpEAL_BSU_ID As New SqlParameter("@EAL_BSU_ID", SqlDbType.VarChar, 10)
            sqlpEAL_BSU_ID.Value = hfWU.Value
            cmd.Parameters.Add(sqlpEAL_BSU_ID)

            Dim sqlpEAL_FROMDT As New SqlParameter("@EAL_FROMDT", SqlDbType.DateTime)
            sqlpEAL_FROMDT.Value = txtJdate.Text
            cmd.Parameters.Add(sqlpEAL_FROMDT)

            Dim sqlpSess_EAL_BSU_ID As New SqlParameter("@Sess_EAL_BSU_ID", SqlDbType.VarChar, 10)
            sqlpSess_EAL_BSU_ID.Value = DBNull.Value
            cmd.Parameters.Add(sqlpSess_EAL_BSU_ID)

            Dim sqlpbNEw As New SqlParameter("@bNEw", SqlDbType.Bit)
            sqlpbNEw.Value = 1
            cmd.Parameters.Add(sqlpbNEw)

            Dim sqlpEAL_REMARKS As New SqlParameter("@EAL_REMARKS", SqlDbType.VarChar, 100)
            sqlpEAL_REMARKS.Value = "New Value Inserted"
            cmd.Parameters.Add(sqlpEAL_REMARKS)

            Dim sqlpEMPNO As New SqlParameter("@EMPNO", SqlDbType.VarChar, 20)
            sqlpEMPNO.Value = empNo
            cmd.Parameters.Add(sqlpEMPNO)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    'V1.1 -- To save WPS details in EMPLOYEE_M master table
    Private Function SaveEMPWPSDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer

        Try
            Dim cmd As New SqlCommand

            cmd = New SqlCommand("Save_EmpWPSDetails", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEAL_EMP_ID As New SqlParameter("@empID", SqlDbType.Int)
            sqlpEAL_EMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEAL_EMP_ID)

            Dim sqlpEAL_WPS_ID As New SqlParameter("@WPSID", SqlDbType.VarChar, 50)
            sqlpEAL_WPS_ID.Value = hf_WPSID.Value
            cmd.Parameters.Add(sqlpEAL_WPS_ID)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try

    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        gvEmpSearch.Visible = False
        trsearchTable.Visible = False
        ddlStatus.Enabled = False
        ddlStatus.Visible = False
        lblEmpStatus.Visible = True
        For i As Integer = 0 To 9 '7
            SetPanelEnabled(i, True)
        Next
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub txtJdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJdate.TextChanged
        Dim strfDate As String = txtJdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        Else
            txtJdate.Text = strfDate
        End If
    End Sub

    Protected Sub txtViss_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtViss_date.TextChanged
        Dim strfDate As String = txtViss_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        Else
            txtViss_date.Text = strfDate
        End If
    End Sub

    Protected Sub txtVExp_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtVExp_date.TextChanged
        Dim strfDate As String = txtVExp_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        Else
            txtVExp_date.Text = strfDate
        End If
    End Sub

    Protected Sub txtProb_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProb.TextChanged
        Dim strfDate As String = txtProb.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        Else
            txtProb.Text = strfDate
        End If
    End Sub

    Protected Sub txtLRdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLRdate.TextChanged
        Dim strfDate As String = txtLRdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        Else
            txtLRdate.Text = strfDate
        End If
    End Sub

    Private Function SaveContactDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Try
            Dim cmd As New SqlCommand
            Dim iReturnvalue As Integer

            cmd = New SqlCommand("SaveEMPLOYEE_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEMD_EMP_ID As New SqlParameter("@EMD_EMP_ID", SqlDbType.Int)
            sqlpEMD_EMP_ID.Value = EmpID
            cmd.Parameters.Add(sqlpEMD_EMP_ID)

            Dim sqlpEMD_CUR_ADDRESS As New SqlParameter("@EMD_CUR_ADDRESS", SqlDbType.VarChar, 100)
            sqlpEMD_CUR_ADDRESS.Value = txtContCurAddress.Text
            cmd.Parameters.Add(sqlpEMD_CUR_ADDRESS)

            Dim sqlpEMD_CUR_POBOX As New SqlParameter("@EMD_CUR_POBOX", SqlDbType.VarChar, 20)
            sqlpEMD_CUR_POBOX.Value = txtContCurrPOBox.Text
            cmd.Parameters.Add(sqlpEMD_CUR_POBOX)

            Dim sqlpEMD_CUR_CITY As New SqlParameter("@EMD_CUR_CITY", SqlDbType.VarChar, 100)
            sqlpEMD_CUR_CITY.Value = txtContCurrCity.Text
            cmd.Parameters.Add(sqlpEMD_CUR_CITY)

            Dim sqlpEMD_CUR_CTY_ID As New SqlParameter("@EMD_CUR_CTY_ID", SqlDbType.VarChar, 3)
            sqlpEMD_CUR_CTY_ID.Value = hfContCCountry_ID.Value
            cmd.Parameters.Add(sqlpEMD_CUR_CTY_ID)

            Dim sqlpEMD_EMAIL As New SqlParameter("@EMD_EMAIL", SqlDbType.VarChar, 100)
            sqlpEMD_EMAIL.Value = txtContEmail.Text
            cmd.Parameters.Add(sqlpEMD_EMAIL)

            Dim sqlpEMD_CUR_PHONE As New SqlParameter("@EMD_CUR_PHONE", SqlDbType.VarChar, 20)
            sqlpEMD_CUR_PHONE.Value = txtContCurrPhoneNo.Text
            cmd.Parameters.Add(sqlpEMD_CUR_PHONE)

            Dim sqlpEMD_CUR_MOBILE As New SqlParameter("@EMD_CUR_MOBILE", SqlDbType.VarChar, 20)
            sqlpEMD_CUR_MOBILE.Value = txtContCurrMobNo.Text
            cmd.Parameters.Add(sqlpEMD_CUR_MOBILE)

            Dim sqlpEMD_PERM_ADDRESS As New SqlParameter("@EMD_PERM_ADDRESS", SqlDbType.VarChar, 100)
            sqlpEMD_PERM_ADDRESS.Value = txtContPermAddress.Text
            cmd.Parameters.Add(sqlpEMD_PERM_ADDRESS)

            Dim sqlpEMD_PERM_POBOX As New SqlParameter("@EMD_PERM_POBOX", SqlDbType.VarChar, 20)
            sqlpEMD_PERM_POBOX.Value = txtContPermPOBox.Text
            cmd.Parameters.Add(sqlpEMD_PERM_POBOX)

            Dim sqlpEMD_PERM_CITY As New SqlParameter("@EMD_PERM_CITY", SqlDbType.VarChar, 100)
            sqlpEMD_PERM_CITY.Value = txtContPermCity.Text
            cmd.Parameters.Add(sqlpEMD_PERM_CITY)

            Dim sqlpEMD_PERM_CTY_ID As New SqlParameter("@EMD_PERM_CTY_ID", SqlDbType.VarChar, 3)
            sqlpEMD_PERM_CTY_ID.Value = hfContPCountry_ID.Value
            cmd.Parameters.Add(sqlpEMD_PERM_CTY_ID)

            Dim sqlpEMD_PERM_PHONE As New SqlParameter("@EMD_PERM_PHONE", SqlDbType.VarChar, 20)
            sqlpEMD_PERM_PHONE.Value = txtContPermPhoneNo.Text
            cmd.Parameters.Add(sqlpEMD_PERM_PHONE)

            Dim sqlpEMD_PERM_MOBILE As New SqlParameter("@EMD_PERM_MOBILE", SqlDbType.VarChar, 20)
            sqlpEMD_PERM_MOBILE.Value = "0" 'txtContPermMobNo.Text
            cmd.Parameters.Add(sqlpEMD_PERM_MOBILE)

            Dim sqlpEMD_ACCOM_LOCATION As New SqlParameter("@EMD_ACCOM_LOCATION", SqlDbType.VarChar, 100)
            sqlpEMD_ACCOM_LOCATION.Value = txtContAccomLocation.Text
            cmd.Parameters.Add(sqlpEMD_ACCOM_LOCATION)

            Dim sqlpEMD_ACCOM_TYPE As New SqlParameter("@EMD_ACCOM_TYPE", SqlDbType.VarChar, 50)
            sqlpEMD_ACCOM_TYPE.Value = ddlContAccomType.SelectedValue
            cmd.Parameters.Add(sqlpEMD_ACCOM_TYPE)

            Dim sqlpEMD_ACCOM_FLAT_NO As New SqlParameter("@EMD_ACCOM_FLAT_NO", SqlDbType.VarChar, 20)
            sqlpEMD_ACCOM_FLAT_NO.Value = txtContFlatNo.Text
            cmd.Parameters.Add(sqlpEMD_ACCOM_FLAT_NO)

            Dim sqlpEMD_ACCOM_BSHARING As New SqlParameter("@EMD_ACCOM_BSHARING", SqlDbType.Bit)
            sqlpEMD_ACCOM_BSHARING.Value = chkContBSharing.Checked
            cmd.Parameters.Add(sqlpEMD_ACCOM_BSHARING)

            Dim sqlpEMD_CUT_ID As New SqlParameter("@EMD_CUT_ID", SqlDbType.Int)
            If h_ContCostUnitID.Value = "" Then
                sqlpEMD_CUT_ID.Value = DBNull.Value
            Else
                sqlpEMD_CUT_ID.Value = h_ContCostUnitID.Value  'Need to be added
            End If
            cmd.Parameters.Add(sqlpEMD_CUT_ID)

            Dim sqlpEMD_CONTACT_LOCAL As New SqlParameter("@EMD_CONTACT_LOCAL", SqlDbType.VarChar, 100)
            sqlpEMD_CONTACT_LOCAL.Value = txtContLocalContact.Text
            cmd.Parameters.Add(sqlpEMD_CONTACT_LOCAL)

            Dim sqlpEMD_CONTACT_LOCAL_NUM As New SqlParameter("@EMD_CONTACT_LOCAL_NUM", SqlDbType.VarChar, 20)
            sqlpEMD_CONTACT_LOCAL_NUM.Value = txtContLocalContNo.Text
            cmd.Parameters.Add(sqlpEMD_CONTACT_LOCAL_NUM)

            Dim sqlpEMD_CONTACT_HOME As New SqlParameter("@EMD_CONTACT_HOME", SqlDbType.VarChar, 100)
            sqlpEMD_CONTACT_HOME.Value = txtContHomecont.Text
            cmd.Parameters.Add(sqlpEMD_CONTACT_HOME)

            Dim sqlpEMD_PHOTO As New SqlParameter("@EMD_PHOTO", SqlDbType.VarChar, 200)
            If ViewState("EMP_IMAGE_PATH") Is Nothing Then
                sqlpEMD_PHOTO.Value = ""
            Else
                sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")
            End If
            cmd.Parameters.Add(sqlpEMD_PHOTO)



            '''''''''''EmpFilepathvirtual
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

            ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
            ViewState("EMPPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & "\EMPPHOTO"
            If h_EmpImagePath.Value <> "" Then
                ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
                ViewState("EMPPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & "\EMPPHOTO"
                Dim dirInfoBSUID As DirectoryInfo = Nothing
                Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing
                If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
                    dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH"))
                End If
                If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & EmpID) Then
                    dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH") & "\" & EmpID)
                End If

                Dim fs As New FileInfo(h_EmpImagePath.Value)
                Dim strFilePath As String = ViewState("EMPPHOTOFILEPATH") & "\" & EmpID & "\EMPPHOTO" & fs.Extension
                File.Copy(ViewState("tempimgpath"), strFilePath, True)

                Dim strFileNameDB As String = "/" & Session("sbsuid") & "/empphoto/" & EmpID & "/EMPPHOTO" & fs.Extension
                imgEmpImage.ImageUrl = str_imgvirtual & strFileNameDB & "?" & DateTime.Now.Ticks.ToString()
                imgEmpImage.AlternateText = "No Image found"
                sqlpEMD_PHOTO.Value = strFileNameDB
            End If

            Dim sqlpEMD_CONTACT_HOME_NUM As New SqlParameter("@EMD_CONTACT_HOME_NUM", SqlDbType.VarChar, 20)
            sqlpEMD_CONTACT_HOME_NUM.Value = txtContHomecontNo.Text
            cmd.Parameters.Add(sqlpEMD_CONTACT_HOME_NUM)

            'V1.5 
            Dim sqlpEMD_PeronalEmail As New SqlParameter("@EMD_Personal_Email", SqlDbType.VarChar, 100)
            sqlpEMD_PeronalEmail.Value = txtPersonalMail.Text
            cmd.Parameters.Add(sqlpEMD_PeronalEmail)

            'V2.0 params added
            Dim sqlpemd_FeeConcessionActual As New SqlParameter("@emd_FeeConcessionActual", SqlDbType.TinyInt)
            sqlpemd_FeeConcessionActual.Value = IIf(txtFeeActual.Text = "", DBNull.Value, txtFeeActual.Text)
            cmd.Parameters.Add(sqlpemd_FeeConcessionActual)

            Dim sqlpemd_FeeConcessionEligible As New SqlParameter("@emd_FeeConcessionEligible", SqlDbType.TinyInt)
            sqlpemd_FeeConcessionEligible.Value = IIf(txtFeeEligible.Text = "", DBNull.Value, txtFeeEligible.Text)
            cmd.Parameters.Add(sqlpemd_FeeConcessionEligible)

            Dim sqlpemd_FCE_ID As New SqlParameter("@emd_FCE_ID", SqlDbType.Int)

            If ddlConEligibility.SelectedValue = 0 Then
                sqlpemd_FCE_ID.Value = DBNull.Value
            Else
                sqlpemd_FCE_ID.Value = ddlConEligibility.SelectedValue
            End If
            cmd.Parameters.Add(sqlpemd_FCE_ID)

            Dim sqlpemd_AadharCardNo As New SqlParameter("@EMD_AADHAR_CARD_NO", SqlDbType.VarChar, 100)
            sqlpemd_AadharCardNo.Value = IIf(txt_AadharCardNo.Text = "", DBNull.Value, txt_AadharCardNo.Text)
            cmd.Parameters.Add(sqlpemd_AadharCardNo)

            Dim sqlpemd_PanCardNo As New SqlParameter("@EMD_PAN_CARD_NO", SqlDbType.VarChar, 100)
            sqlpemd_PanCardNo.Value = IIf(txt_PanCardNo.Text = "", DBNull.Value, txt_PanCardNo.Text)
            cmd.Parameters.Add(sqlpemd_PanCardNo)

            'V2.0 ends

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            'File.Delete(strOriginFilepath)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try

    End Function

    Public Function GetBSUEmpFilePath() As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select BSU_EMP_FILEPATH FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString
    End Function

    Private Sub ClearSession_GridDetails()
        'if everything is saved properly then Clear the Sessions
        Session.Remove("EMPDOCDETAILS")
        Session.Remove("EMPEXPDETAILS")
        Session.Remove("EMPQUALDETAILS")
        Session.Remove("EMPSALDETAILS")
        Session.Remove("EMPDEPENDANCEDETAILS")

        Session.Remove("EMPDocEditID")
        Session.Remove("EMPDocEditID1")
        Session.Remove("EMPEXPEditID")
        Session.Remove("EMPQUALEditID")
        Session.Remove("EMPSALEditID")
        Session.Remove("EMPDEPENDANCEEditID")

        GridBindDocDetails()
        GridBindExperienceDetails()
        GridBindQualificationDetails()
        GridBindSalaryDetails()
        GridBindDependanceDetails()
    End Sub

#Region "Document Details"

    Protected Sub btnDocAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocAdd.Click
        If Session("EMPDOCDETAILS") Is Nothing Then
            Session("EMPDOCDETAILS") = CreateDocDetailsTable()
        End If
        Dim dtDocDetails As DataTable = Session("EMPDOCDETAILS")
        Dim status As String = String.Empty
        If CDate(txtDocIssueDate.Text) > CDate(txtDocExpDate.Text) Then
            lblError.Text = "Exp. Date should be less than Issue Date"
            GridBindDocDetails()
            Exit Sub
        End If



        Dim i As Integer
        For i = 0 To dtDocDetails.Rows.Count - 1
            If Session("EMPDocEditID") IsNot Nothing And _
            Session("EMPDocEditID") <> dtDocDetails.Rows(i)("UniqueID") Then
                If dtDocDetails.Rows(i)("Document") = txtDocDocument.Text And _
                    dtDocDetails.Rows(i)("DocID") = h_DOC_DOCID.Value And _
                     dtDocDetails.Rows(i)("Doc_No") = txtDocDocNo.Text And _
                     dtDocDetails.Rows(i)("Doc_IssuePlace") = txtDocIssuePlace.Text And _
                     dtDocDetails.Rows(i)("Doc_IssueDate") = CDate(txtDocIssueDate.Text) And _
                     dtDocDetails.Rows(i)("Doc_ExpDate") = CDate(txtDocExpDate.Text) Then
                    lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                    GridBindDocDetails()
                    Exit Sub
                End If
            End If
        Next

        If btnDocAdd.Text = "Add" Then
            Dim newDR As DataRow = dtDocDetails.NewRow()
            newDR("UniqueID") = gvEMPDocDetails.Rows.Count()
            newDR("Document") = txtDocDocument.Text
            newDR("DocID") = h_DOC_DOCID.Value
            newDR("Doc_No") = txtDocDocNo.Text
            newDR("Doc_bActive") = chkDocActive.Checked
            newDR("Doc_IssuePlace") = txtDocIssuePlace.Text
            newDR("Doc_IssueDate") = CDate(txtDocIssueDate.Text)
            newDR("Doc_ExpDate") = CDate(txtDocExpDate.Text)
            newDR("Status") = "Insert"

            If FileUploadDoc.HasFile Then


                Dim File As HttpPostedFile
                File = FileUploadDoc.PostedFile
                Dim imgByte As Byte() = Nothing
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
                newDR("DocFile") = imgByte
                newDR("DocFileType") = System.IO.Path.GetExtension(FileUploadDoc.FileName)
                newDR("DocFileMIME") = FileUploadDoc.PostedFile.ContentType
            End If

            status = "Insert"
            lblError.Text = ""
            dtDocDetails.Rows.Add(newDR)
            btnDocAdd.Text = "Add"
        ElseIf btnDocAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPDocEditID")
            For iIndex = 0 To dtDocDetails.Rows.Count - 1
                If str_Search = dtDocDetails.Rows(iIndex)("UniqueID") And dtDocDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtDocDetails.Rows(iIndex)("DOC_No") = txtDocDocNo.Text
                    dtDocDetails.Rows(iIndex)("Document") = txtDocDocument.Text
                    dtDocDetails.Rows(iIndex)("DOC_IssuePlace") = txtDocIssuePlace.Text
                    dtDocDetails.Rows(iIndex)("DOC_ExpDate") = txtDocExpDate.Text
                    dtDocDetails.Rows(iIndex)("DocID") = h_DOC_DOCID.Value
                    dtDocDetails.Rows(iIndex)("DOC_IssueDate") = txtDocIssueDate.Text
                    dtDocDetails.Rows(iIndex)("Doc_bActive") = chkDocActive.Checked

                    If FileUploadDoc.HasFile Then


                        Dim File As HttpPostedFile
                        File = FileUploadDoc.PostedFile
                        Dim imgByte As Byte() = Nothing
                        imgByte = New Byte(File.ContentLength - 1) {}
                        File.InputStream.Read(imgByte, 0, File.ContentLength)
                        dtDocDetails.Rows(iIndex)("DocFile") = imgByte
                        dtDocDetails.Rows(iIndex)("DocFileType") = System.IO.Path.GetExtension(FileUploadDoc.FileName)
                        dtDocDetails.Rows(iIndex)("DocFileMIME") = FileUploadDoc.PostedFile.ContentType
                    End If


                    If dtDocDetails.Rows(iIndex)("Status").ToString.ToUpper <> "INSERT" Then
                        dtDocDetails.Rows(iIndex)("Status") = "Edit"
                    End If
                    Exit For
                End If
            Next
            btnDocAdd.Text = "Add"
            ClearEMPDocDetails()
        End If

        If ViewState("datamode") = "EDIT" Then
            If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
                ClearEMPDocDetails()
            Else
                UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
            End If
        End If
        Session("EMPDOCDETAILS") = dtDocDetails
        GridBindDocDetails()

    End Sub

    Private Function CreateDocDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cDocument As New DataColumn("Document", System.Type.GetType("System.String"))
            Dim cDocID As New DataColumn("DocID", System.Type.GetType("System.String"))
            Dim cDoc_No As New DataColumn("Doc_No", System.Type.GetType("System.String"))
            Dim cDoc_bActive As New DataColumn("Doc_bActive", System.Type.GetType("System.Boolean"))
            Dim cDoc_IssuePlace As New DataColumn("Doc_IssuePlace", System.Type.GetType("System.String"))
            Dim cDoc_IssueDate As New DataColumn("Doc_IssueDate", System.Type.GetType("System.DateTime"))
            Dim cDoc_ExpDate As New DataColumn("Doc_ExpDate", System.Type.GetType("System.DateTime"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cDocFile As New DataColumn("DocFile", System.Type.GetType("System.Object"))
            Dim cDocFileType As New DataColumn("DocFileType", System.Type.GetType("System.String"))
            Dim cDocFileMIME As New DataColumn("DocFileMIME", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cDocument)
            dtDt.Columns.Add(cDocID)
            dtDt.Columns.Add(cDoc_No)
            dtDt.Columns.Add(cDoc_bActive)
            dtDt.Columns.Add(cDoc_IssuePlace)
            dtDt.Columns.Add(cDoc_IssueDate)
            dtDt.Columns.Add(cDoc_ExpDate)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cDocFile)
            dtDt.Columns.Add(cDocFileType)
            dtDt.Columns.Add(cDocFileMIME)


            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnDocCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocCancel.Click
        ClearEMPDocDetails()
    End Sub

    Sub ClearEMPDocDetails()
        txtDocDocNo.Text = ""
        h_DOC_DOCID.Value = ""
        txtDocDocument.Text = ""
        txtDocExpDate.Text = ""
        txtDocIssueDate.Text = ""
        txtDocIssuePlace.Text = ""
        chkDocActive.Checked = True
        btnDocAdd.Text = "Add"
        lblError.Text = ""
        Session.Remove("EMPDocEditID")
    End Sub
    Protected Sub lnkDocSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        For iIndex As Integer = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If Session("EMPDOCDETAILS").Rows(iIndex)("UniqueID") = lblTid.Text Then

                If Not String.IsNullOrEmpty(Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType")) Then
                    Dim imgByte As Byte() = CType(Session("EMPDOCDETAILS").Rows(iIndex)("DocFile"), Byte())
                    'Response.ContentType = Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    'Response.BinaryWrite(imgByte)

                    Dim Filename As String = Session("EMPDOCDETAILS").Rows(iIndex)("Document").ToString() + Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    ' Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = Session("EMPDOCDETAILS").Rows(iIndex)("DocFileMIME").ToString()
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Filename)
                    Response.BinaryWrite(imgByte)
                    Response.Flush()
                End If

            End If
        Next
    End Sub
    Protected Sub lnkDocEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If str_Search = Session("EMPDOCDETAILS").Rows(iIndex)("UniqueID") And Session("EMPDOCDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDocDocNo.Text = Session("EMPDOCDETAILS").Rows(iIndex)("DOC_No")
                h_DOC_DOCID.Value = Session("EMPDOCDETAILS").Rows(iIndex)("DOCID")
                txtDocDocument.Text = Session("EMPDOCDETAILS").Rows(iIndex)("Document")
                txtDocIssuePlace.Text = Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssuePlace")
                If Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate") Is Nothing OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate").ToString = "" OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate") = New Date(1900, 1, 1) Then
                    txtDocExpDate.Text = ""
                Else
                    txtDocExpDate.Text = Format(Session("EMPDOCDETAILS").Rows(iIndex)("DOC_ExpDate"), OASISConstants.DateFormat)
                End If
                If Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate") Is Nothing OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate").ToString = "" OrElse _
                Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate") = New Date(1900, 1, 1) Then
                    txtDocIssueDate.Text = ""
                Else
                    txtDocIssueDate.Text = Format(Session("EMPDOCDETAILS").Rows(iIndex)("DOC_IssueDate"), OASISConstants.DateFormat)
                End If
                chkDocActive.Checked = Session("EMPDOCDETAILS").Rows(iIndex)("Doc_bActive")
                gvEMPDocDetails.SelectedIndex = iIndex
                btnDocAdd.Text = "Save"
                Session("EMPDocEditID") = str_Search
                UtilityObj.beforeLoopingControls(vwDocuments)
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvEMPDocDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPDocDetails.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkDocSelect As LinkButton = DirectCast(e.Row.FindControl("lnkDocSelect"), LinkButton)
            Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")
            ScriptManager1.RegisterPostBackControl(lnkDocSelect)

            Dim HF_DocFileMIME As HiddenField = DirectCast(e.Row.FindControl("HF_DocFileMIME"), HiddenField)
            If String.IsNullOrEmpty(HF_DocFileMIME.Value) Then
                lnkDocSelect.Enabled = False
            End If
        End If
    End Sub
    Protected Sub gvEMPDocDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEMPDocDetails.RowDeleting
        'Dim categoryID As Integer = CInt(gvEMPDocDetails.DataKeys(e.RowIndex).Value)
        Dim categoryID As Integer = e.RowIndex
        DeleteRowDocDetails(categoryID)
    End Sub

    Private Sub DeleteRowDocDetails(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim lblTid As New Label
        lblTid = TryCast(gvEMPDocDetails.Rows(pId).FindControl("lblId"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        For iRemove = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If (Session("EMPDOCDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPDOCDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
                    GridBindDocDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Private Sub GridBindDocDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("EMPDOCDETAILS") Is Nothing Then
            gvEMPDocDetails.DataSource = Nothing
            gvEMPDocDetails.DataBind()
            Return
        End If
        Dim strColumnName As String = String.Empty
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateDocDetailsTable()
        If Session("EMPDOCDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                If (Session("EMPDOCDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPDOCDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPDOCDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvEMPDocDetails.DataSource = dtTempDtl
        gvEMPDocDetails.DataBind()
    End Sub
    Private Sub ValidateDocuments()
        Dim lstrVisaStatus
        Dim iIndex As Integer
        Dim lintPassprt As Integer
        Dim lintVisa As Integer
        Dim lintLabCard As Integer


        lintPassprt = 0
        lintVisa = 0
        lintLabCard = 0
        lstrVisaStatus = ddlVstatus.SelectedItem.Value

        If Session("EMPDOCDETAILS") Is Nothing Then
            If lstrVisaStatus = "2" Then
                If lintPassprt = 0 Or lintVisa = 0 Or lintLabCard = 0 Then
                    lblError.Text = "Passport, Visa, Labour Card details mandatory for Visa Holders"
                End If
            ElseIf lstrVisaStatus = "3" Then
                If lintLabCard = 0 Then
                    lblError.Text = "Labour Card details mandatory for Visa Holders"
                End If
            End If
        Else

            For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                Dim dr As DataRow = Session("EMPDOCDETAILS").Rows(iIndex)
                'If (Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "DELETED") And _
                'Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "LOADED" Then
                If dr("DocID") = "1" Then lintPassprt = 1
                If dr("DocID") = "2" Then lintVisa = 1
                If dr("DocID") = "3" Then lintLabCard = 1
                'End If
            Next
            If lstrVisaStatus = "2" Then
                If lintPassprt = 0 Or lintVisa = 0 Or lintLabCard = 0 Then
                    lblError.Text = "Passport, Visa, Labour Card details mandatory for Visa Holders"
                End If
            ElseIf lstrVisaStatus = "3" Then
                If lintLabCard = 0 Then
                    lblError.Text = "Labour Card details mandatory for Visa Holders"
                End If
            End If

            lstrErrMsg = lblError.Text
        End If

    End Sub
    ' V3.1 starts
    Private Sub ValidateDocumentsinDB()
        Try


            Dim lstrVisaStatus As String
            'Dim iIndex As Integer
            'Dim lintPassprt As Boolean
            'Dim lintVisa As Boolean
            'Dim lintLabCard As Boolean
            Dim lstrVisaType As String
            Dim Documentlist As String = ""
            Dim lstrVisaCategory As String = ""
            'lintPassprt = 0
            'lintVisa = 0
            'lintLabCard = 0
            lstrVisaStatus = ddlVstatus.SelectedItem.Value
            lstrVisaType = ddlVtype.SelectedItem.Value
            lstrVisaCategory = ddlCategory.SelectedItem.Value

            If Not Session("EMPDOCDETAILS") Is Nothing Then

                For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPDOCDETAILS").Rows(iIndex)
                    'If (Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "DELETED") And _
                    'Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "LOADED" Then
                    'If dr("DocID") = "1" Then lintPassprt = 1
                    'If dr("DocID") = "2" Then lintVisa = 1
                    'If dr("DocID") = "3" Then lintLabCard = 1
                    'End If
                    Documentlist = Documentlist + dr("DocID").ToString & ","
                Next

            End If
            Dim objConn As New SqlConnection
            objConn = ConnectionManger.GetOASISConnection

            Dim iReturnvalue As Integer
            Dim cmd As New SqlCommand
            cmd = New SqlCommand("CheckVisaValidations", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpEMD_VisaStatus As New SqlParameter("@VisaStatus", SqlDbType.Int)
            sqlpEMD_VisaStatus.Value = lstrVisaStatus
            cmd.Parameters.Add(sqlpEMD_VisaStatus)

            Dim sqlpEMD_VisaType As New SqlParameter("@VisaType", SqlDbType.Int)
            sqlpEMD_VisaType.Value = lstrVisaType
            cmd.Parameters.Add(sqlpEMD_VisaType)

            Dim sqlpEMD_DOcumentList As New SqlParameter("@Documentlist", SqlDbType.VarChar)
            sqlpEMD_DOcumentList.Value = Documentlist
            cmd.Parameters.Add(sqlpEMD_DOcumentList)

            Dim sqlpEMD_BSUID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            sqlpEMD_BSUID.Value = Session("sBsuid").ToString
            cmd.Parameters.Add(sqlpEMD_BSUID)

            Dim sqlpEMD_visaCategory As New SqlParameter("@visaCategory", SqlDbType.VarChar, 1)
            sqlpEMD_visaCategory.Value = lstrVisaCategory
            cmd.Parameters.Add(sqlpEMD_visaCategory)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            If iReturnvalue <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
            Else
                lblError.Text = ""
            End If
        Catch ex As Exception
            lblError.Text = "Error while validating documents"
        End Try
    End Sub
    ' V 3.1 ends
    Private Function SaveEMPDocumentDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPDOCDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPDOCDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPDOCDETAILS").Rows(iIndex)
                    If (Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "DELETED") And _
                    Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper <> "LOADED" Then
                        cmd = New SqlCommand("SaveEMPDOCUMENTS_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_EMP_ID As New SqlParameter("@EMD_EMP_ID", SqlDbType.Int)
                        sqlpEMD_EMP_ID.Value = EmpID
                        cmd.Parameters.Add(sqlpEMD_EMP_ID)

                        Dim sqlpEMD_ESD_ID As New SqlParameter("@EMD_ESD_ID", SqlDbType.Int)
                        sqlpEMD_ESD_ID.Value = dr("DocID")
                        cmd.Parameters.Add(sqlpEMD_ESD_ID)

                        'JNL_SUB_ID
                        Dim sqlpEMD_NO As New SqlParameter("@EMD_NO", SqlDbType.VarChar, 50)
                        sqlpEMD_NO.Value = dr("Doc_No")
                        cmd.Parameters.Add(sqlpEMD_NO)
                        If Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "EDIT" Then
                            Dim sqlpEMD_ID As New SqlParameter("@EMD_ID", SqlDbType.Int)
                            sqlpEMD_ID.Value = dr("UniqueID")
                            cmd.Parameters.Add(sqlpEMD_ID)
                        End If

                        Dim sqlpEMP_ISSUEPLACE As New SqlParameter("@EMP_ISSUEPLACE", SqlDbType.VarChar, 100)
                        sqlpEMP_ISSUEPLACE.Value = dr("DOC_IssuePlace")
                        cmd.Parameters.Add(sqlpEMP_ISSUEPLACE)

                        Dim sqlpEMD_ISSUEDT As New SqlParameter("@EMD_ISSUEDT", SqlDbType.DateTime)
                        sqlpEMD_ISSUEDT.Value = dr("DOC_IssueDate")
                        cmd.Parameters.Add(sqlpEMD_ISSUEDT)

                        Dim sqlpEMD_EXPDT As New SqlParameter("@EMD_EXPDT", SqlDbType.DateTime)
                        sqlpEMD_EXPDT.Value = dr("DOC_ExpDate")
                        cmd.Parameters.Add(sqlpEMD_EXPDT)

                        Dim sqlpEMD_bACTIVE As New SqlParameter("@EMD_bACTIVE", SqlDbType.Bit)
                        sqlpEMD_bACTIVE.Value = dr("Doc_bActive")
                        cmd.Parameters.Add(sqlpEMD_bACTIVE)

                        Dim sqlpEMD_DOCFILE As New SqlParameter("@EMD_DOCFILE", SqlDbType.VarBinary)
                        sqlpEMD_DOCFILE.Value = dr("DocFile")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE)

                        Dim sqlpEMD_DOCFILE_TYPE As New SqlParameter("@EMD_DOCFILE_TYPE", SqlDbType.VarChar)
                        sqlpEMD_DOCFILE_TYPE.Value = dr("DocFileType")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE_TYPE)

                        Dim sqlpEMD_DOCFILE_MIME As New SqlParameter("@EMD_DOCFILE_MIME", SqlDbType.VarChar)
                        sqlpEMD_DOCFILE_MIME.Value = dr("DocFileMIME")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE_MIME)

                        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                        If Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "INSERT" Then
                            sqlpbEdit.Value = False
                        ElseIf Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "EDIT" Then
                            sqlpbEdit.Value = True
                        End If
                        cmd.Parameters.Add(sqlpbEdit)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    ElseIf Session("EMPDOCDETAILS").Rows(iIndex)("Status").ToString.ToUpper = "DELETED" Then
                        'Delete Code will Come here
                        cmd = New SqlCommand("DELETEEMPDOCUMENTS_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_ID As New SqlParameter("@EMD_ID", SqlDbType.Int)
                        sqlpEMD_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpEMD_ID)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

#End Region

#Region "Document Details1" 'New Modification

    Protected Sub btnDocAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocAdd1.Click
        If Session("EMPDOCDETAILS1") Is Nothing Then
            Session("EMPDOCDETAILS1") = CreateDocDetailsTable1()
        End If
        Dim dtDocDetails As DataTable = Session("EMPDOCDETAILS1")
        Dim status As String = String.Empty

        If Not String.IsNullOrEmpty(txtDocExpDate1.Text) Then
            If CDate(txtDocIssueDate1.Text) > CDate(txtDocExpDate1.Text) Then
                lblError.Text = "Exp. Date should be less than Issue Date"
                GridBindDocDetails1()
                Exit Sub
            End If
        End If

        Dim i As Integer
        For i = 0 To dtDocDetails.Rows.Count - 1
            If Session("EMPDocEditID1") IsNot Nothing And _
            Session("EMPDocEditID1") <> dtDocDetails.Rows(i)("UniqueID") Then
                If dtDocDetails.Rows(i)("Document") = txtDocDocument1.Text And _
                    dtDocDetails.Rows(i)("DocID") = h_DOC_DOCID1.Value And _
                     dtDocDetails.Rows(i)("Doc_No") = txtDocDocNo1.Text And _
                     dtDocDetails.Rows(i)("Doc_IssuePlace") = txtDocIssuePlace1.Text And _
                     dtDocDetails.Rows(i)("Doc_IssueDate") = CDate(txtDocIssueDate1.Text) Then
                    'dtDocDetails.Rows(i)("Doc_ExpDate") = CDate(txtDocExpDate1.Text)

                    lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                    GridBindDocDetails1()
                    Exit Sub
                End If
            End If
        Next

        If btnDocAdd1.Text = "Add" Then
            Dim newDR As DataRow = dtDocDetails.NewRow()
            newDR("UniqueID") = gvEMPDocDetails1.Rows.Count()
            newDR("Document") = txtDocDocument1.Text
            newDR("DocID") = h_DOC_DOCID1.Value
            newDR("Doc_No") = txtDocDocNo1.Text
            newDR("Doc_bActive") = chkDocActive1.Checked
            newDR("Doc_IssuePlace") = txtDocIssuePlace1.Text
            newDR("Doc_IssueDate") = CDate(txtDocIssueDate1.Text)

            If String.IsNullOrEmpty(txtDocExpDate1.Text) Then
                newDR("Doc_ExpDate") = DBNull.Value
            Else
                newDR("Doc_ExpDate") = CDate(txtDocExpDate1.Text)
            End If

            newDR("Status") = "Insert"

            If FileUploadDoc1.HasFile Then

                Dim File As HttpPostedFile
                File = FileUploadDoc1.PostedFile
                Dim imgByte As Byte() = Nothing
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
                newDR("DocFile") = imgByte
                newDR("DocFileType") = System.IO.Path.GetExtension(FileUploadDoc1.FileName)
                newDR("DocFileMIME") = FileUploadDoc1.PostedFile.ContentType
            End If

            status = "Insert"
            lblError.Text = ""
            dtDocDetails.Rows.Add(newDR)
            btnDocAdd1.Text = "Add"
        ElseIf btnDocAdd1.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPDocEditID1")
            For iIndex = 0 To dtDocDetails.Rows.Count - 1
                If str_Search = dtDocDetails.Rows(iIndex)("UniqueID") And dtDocDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtDocDetails.Rows(iIndex)("DOC_No") = txtDocDocNo1.Text
                    dtDocDetails.Rows(iIndex)("Document") = txtDocDocument1.Text
                    dtDocDetails.Rows(iIndex)("DOC_IssuePlace") = txtDocIssuePlace1.Text
                    dtDocDetails.Rows(iIndex)("DOC_ExpDate") = txtDocExpDate1.Text
                    dtDocDetails.Rows(iIndex)("DocID") = h_DOC_DOCID1.Value
                    dtDocDetails.Rows(iIndex)("DOC_IssueDate") = txtDocIssueDate1.Text
                    dtDocDetails.Rows(iIndex)("Doc_bActive") = chkDocActive1.Checked

                    If FileUploadDoc1.HasFile Then

                        Dim File As HttpPostedFile
                        File = FileUploadDoc1.PostedFile
                        Dim imgByte As Byte() = Nothing
                        imgByte = New Byte(File.ContentLength - 1) {}
                        File.InputStream.Read(imgByte, 0, File.ContentLength)
                        dtDocDetails.Rows(iIndex)("DocFile") = imgByte
                        dtDocDetails.Rows(iIndex)("DocFileType") = System.IO.Path.GetExtension(FileUploadDoc1.FileName)
                        dtDocDetails.Rows(iIndex)("DocFileMIME") = FileUploadDoc1.PostedFile.ContentType
                    End If


                    If dtDocDetails.Rows(iIndex)("Status").ToString.ToUpper <> "INSERT" Then
                        dtDocDetails.Rows(iIndex)("Status") = "Edit"
                    End If
                    Exit For
                End If
            Next
            btnDocAdd1.Text = "Add"
            ClearEMPDocDetails1()
        End If

        If ViewState("datamode") = "EDIT" Then
            If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
                ClearEMPDocDetails1()
            Else
                UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
            End If
        End If
        Session("EMPDOCDETAILS1") = dtDocDetails
        GridBindDocDetails1()
    End Sub

    Private Function CreateDocDetailsTable1() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cDocument As New DataColumn("Document", System.Type.GetType("System.String"))
            Dim cDocID As New DataColumn("DocID", System.Type.GetType("System.String"))
            Dim cDoc_No As New DataColumn("Doc_No", System.Type.GetType("System.String"))
            Dim cDoc_bActive As New DataColumn("Doc_bActive", System.Type.GetType("System.Boolean"))
            Dim cDoc_IssuePlace As New DataColumn("Doc_IssuePlace", System.Type.GetType("System.String"))
            Dim cDoc_IssueDate As New DataColumn("Doc_IssueDate", System.Type.GetType("System.DateTime"))
            Dim cDoc_ExpDate As New DataColumn("Doc_ExpDate", System.Type.GetType("System.DateTime"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cDocFile As New DataColumn("DocFile", System.Type.GetType("System.Object"))
            Dim cDocFileType As New DataColumn("DocFileType", System.Type.GetType("System.String"))
            Dim cDocFileMIME As New DataColumn("DocFileMIME", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cDocument)
            dtDt.Columns.Add(cDocID)
            dtDt.Columns.Add(cDoc_No)
            dtDt.Columns.Add(cDoc_bActive)
            dtDt.Columns.Add(cDoc_IssuePlace)
            dtDt.Columns.Add(cDoc_IssueDate)
            dtDt.Columns.Add(cDoc_ExpDate)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cDocFile)
            dtDt.Columns.Add(cDocFileType)
            dtDt.Columns.Add(cDocFileMIME)


            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnDocCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocCancel1.Click
        ClearEMPDocDetails1()
    End Sub

    Sub ClearEMPDocDetails1()
        txtDocDocNo1.Text = ""
        h_DOC_DOCID1.Value = ""
        txtDocDocument1.Text = ""
        txtDocExpDate1.Text = ""
        txtDocIssueDate1.Text = ""
        txtDocIssuePlace1.Text = ""
        chkDocActive1.Checked = True
        btnDocAdd1.Text = "Add"
        lblError.Text = ""
        Session.Remove("EMPDocEditID1")
    End Sub
    Protected Sub lnkDocSelect1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        For iIndex As Integer = 0 To Session("EMPDOCDETAILS1").Rows.Count - 1
            If Session("EMPDOCDETAILS1").Rows(iIndex)("UniqueID") = lblTid.Text Then

                If Not String.IsNullOrEmpty(Session("EMPDOCDETAILS1").Rows(iIndex)("DocFileType")) Then
                    Dim imgByte As Byte() = CType(Session("EMPDOCDETAILS1").Rows(iIndex)("DocFile"), Byte())
                    'Response.ContentType = Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    'Response.BinaryWrite(imgByte)

                    Dim Filename As String = Session("EMPDOCDETAILS1").Rows(iIndex)("Document").ToString() + Session("EMPDOCDETAILS1").Rows(iIndex)("DocFileType").ToString()

                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = Session("EMPDOCDETAILS1").Rows(iIndex)("DocFileMIME").ToString()
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Filename)
                    Response.BinaryWrite(imgByte)
                    Response.Flush()
                End If

            End If
        Next
    End Sub
    Protected Sub lnkDocEdit1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPDOCDETAILS1").Rows.Count - 1
            If str_Search = Session("EMPDOCDETAILS1").Rows(iIndex)("UniqueID") And Session("EMPDOCDETAILS1").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDocDocNo1.Text = Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_No")
                h_DOC_DOCID1.Value = Session("EMPDOCDETAILS1").Rows(iIndex)("DOCID")
                txtDocDocument1.Text = Session("EMPDOCDETAILS1").Rows(iIndex)("Document")
                txtDocIssuePlace1.Text = Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_IssuePlace")
                If Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_ExpDate") Is Nothing OrElse _
                Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_ExpDate").ToString = "" OrElse _
                Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_ExpDate") = New Date(1900, 1, 1) Then
                    txtDocExpDate1.Text = ""
                Else
                    txtDocExpDate1.Text = Format(Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_ExpDate"), OASISConstants.DateFormat)
                End If
                If Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_IssueDate") Is Nothing OrElse _
                Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_IssueDate").ToString = "" OrElse _
                Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_IssueDate") = New Date(1900, 1, 1) Then
                    txtDocIssueDate1.Text = ""
                Else
                    txtDocIssueDate1.Text = Format(Session("EMPDOCDETAILS1").Rows(iIndex)("DOC_IssueDate"), OASISConstants.DateFormat)
                End If
                chkDocActive1.Checked = Session("EMPDOCDETAILS1").Rows(iIndex)("Doc_bActive")
                gvEMPDocDetails1.SelectedIndex = iIndex
                btnDocAdd1.Text = "Save"
                Session("EMPDocEditID1") = str_Search
                UtilityObj.beforeLoopingControls(vwDocuments)
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvEMPDocDetails1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPDocDetails1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkDocSelect As LinkButton = DirectCast(e.Row.FindControl("lnkDocSelect1"), LinkButton)
            Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")
            ScriptManager1.RegisterPostBackControl(lnkDocSelect)

            Dim HF_DocFileMIME As HiddenField = DirectCast(e.Row.FindControl("HF_DocFileMIME"), HiddenField)
            If String.IsNullOrEmpty(HF_DocFileMIME.Value) Then
                lnkDocSelect.Enabled = False
            End If

            Dim Label2 As Label = DirectCast(e.Row.FindControl("Label2"), Label)
            If Not String.IsNullOrEmpty(Label2.Text) Or Label2.Text <> "-" Then
                Label2.Text = Label2.Text.Substring(0, 11)
                If (Label2.Text = "01/Jan/1900") Then
                    Label2.Text = ""
                End If
            End If


        End If
    End Sub
    Protected Sub gvEMPDocDetails1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEMPDocDetails1.RowDeleting
        'Dim categoryID As Integer = CInt(gvEMPDocDetails.DataKeys(e.RowIndex).Value)
        Dim categoryID As Integer = e.RowIndex
        DeleteRowDocDetails1(categoryID)
    End Sub

    Private Sub DeleteRowDocDetails1(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim lblTid As New Label
        lblTid = TryCast(gvEMPDocDetails1.Rows(pId).FindControl("lblId"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        For iRemove = 0 To Session("EMPDOCDETAILS1").Rows.Count - 1
            If (Session("EMPDOCDETAILS1").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPDOCDETAILS1").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
                    GridBindDocDetails1()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Private Sub GridBindDocDetails1()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("EMPDOCDETAILS1") Is Nothing Then
            gvEMPDocDetails1.DataSource = Nothing
            gvEMPDocDetails1.DataBind()
            Return
        End If
        Dim strColumnName As String = String.Empty
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateDocDetailsTable()
        If Session("EMPDOCDETAILS1").Rows.Count > 0 Then
            For i = 0 To Session("EMPDOCDETAILS1").Rows.Count - 1
                If (Session("EMPDOCDETAILS1").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPDOCDETAILS1").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPDOCDETAILS1").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvEMPDocDetails1.DataSource = dtTempDtl
        gvEMPDocDetails1.DataBind()
    End Sub
    Private Sub ValidateDocuments1()

        lstrErrMsg = lblError.Text

    End Sub
    Private Function SaveEMPDocumentDetails1(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPDOCDETAILS1") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPDOCDETAILS1").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPDOCDETAILS1").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPDOCDETAILS1").Rows(iIndex)
                    If (Session("EMPDOCDETAILS1").Rows(iIndex)("Status").ToString.ToUpper <> "DELETED") And _
                    Session("EMPDOCDETAILS1").Rows(iIndex)("Status").ToString.ToUpper <> "LOADED" Then
                        cmd = New SqlCommand("SaveEMPDOCUMENTS_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_EMP_ID As New SqlParameter("@EMD_EMP_ID", SqlDbType.Int)
                        sqlpEMD_EMP_ID.Value = EmpID
                        cmd.Parameters.Add(sqlpEMD_EMP_ID)

                        Dim sqlpEMD_ESD_ID As New SqlParameter("@EMD_ESD_ID", SqlDbType.Int)
                        sqlpEMD_ESD_ID.Value = dr("DocID")
                        cmd.Parameters.Add(sqlpEMD_ESD_ID)

                        'JNL_SUB_ID
                        Dim sqlpEMD_NO As New SqlParameter("@EMD_NO", SqlDbType.VarChar, 50)
                        sqlpEMD_NO.Value = dr("Doc_No")
                        cmd.Parameters.Add(sqlpEMD_NO)
                        If Session("EMPDOCDETAILS1").Rows(iIndex)("Status").ToString.ToUpper = "EDIT" Then
                            Dim sqlpEMD_ID As New SqlParameter("@EMD_ID", SqlDbType.Int)
                            sqlpEMD_ID.Value = dr("UniqueID")
                            cmd.Parameters.Add(sqlpEMD_ID)
                        End If

                        Dim sqlpEMP_ISSUEPLACE As New SqlParameter("@EMP_ISSUEPLACE", SqlDbType.VarChar, 100)
                        sqlpEMP_ISSUEPLACE.Value = dr("DOC_IssuePlace")
                        cmd.Parameters.Add(sqlpEMP_ISSUEPLACE)

                        Dim sqlpEMD_ISSUEDT As New SqlParameter("@EMD_ISSUEDT", SqlDbType.DateTime)
                        sqlpEMD_ISSUEDT.Value = dr("DOC_IssueDate")
                        cmd.Parameters.Add(sqlpEMD_ISSUEDT)

                        Dim sqlpEMD_EXPDT As New SqlParameter("@EMD_EXPDT", SqlDbType.DateTime)
                        sqlpEMD_EXPDT.Value = dr("DOC_ExpDate")
                        cmd.Parameters.Add(sqlpEMD_EXPDT)

                        Dim sqlpEMD_bACTIVE As New SqlParameter("@EMD_bACTIVE", SqlDbType.Bit)
                        sqlpEMD_bACTIVE.Value = dr("Doc_bActive")
                        cmd.Parameters.Add(sqlpEMD_bACTIVE)

                        Dim sqlpEMD_DOCFILE As New SqlParameter("@EMD_DOCFILE", SqlDbType.VarBinary)
                        sqlpEMD_DOCFILE.Value = dr("DocFile")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE)

                        Dim sqlpEMD_DOCFILE_TYPE As New SqlParameter("@EMD_DOCFILE_TYPE", SqlDbType.VarChar)
                        sqlpEMD_DOCFILE_TYPE.Value = dr("DocFileType")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE_TYPE)

                        Dim sqlpEMD_DOCFILE_MIME As New SqlParameter("@EMD_DOCFILE_MIME", SqlDbType.VarChar)
                        sqlpEMD_DOCFILE_MIME.Value = dr("DocFileMIME")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE_MIME)

                        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                        If Session("EMPDOCDETAILS1").Rows(iIndex)("Status").ToString.ToUpper = "INSERT" Then
                            sqlpbEdit.Value = False
                        ElseIf Session("EMPDOCDETAILS1").Rows(iIndex)("Status").ToString.ToUpper = "EDIT" Then
                            sqlpbEdit.Value = True
                        End If
                        cmd.Parameters.Add(sqlpbEdit)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    ElseIf Session("EMPDOCDETAILS1").Rows(iIndex)("Status").ToString.ToUpper = "DELETED" Then
                        'Delete Code will Come here
                        cmd = New SqlCommand("DELETEEMPDOCUMENTS_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_ID As New SqlParameter("@EMD_ID", SqlDbType.Int)
                        sqlpEMD_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpEMD_ID)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

#End Region

#Region "Qualification Details"

    'Protected Sub BindQualifications()
    '    Dim dt As DataTable = New DataTable()

    '    ddlQualDegreeCert.DataSource = dt
    '    ddlQualDegreeCert.DataValueField = ""
    '    ddlQualDegreeCert.DataTextField = ""
    '    ddlQualDegreeCert.Attributes.a()

    'End Sub

    Protected Sub BindQualificationCategory()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm(1) As SqlClient.SqlParameter
        parm(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)

        ddlQualCatagory.DataSource = ds
        ddlQualCatagory.DataTextField = "QCT_Descr"
        ddlQualCatagory.DataValueField = "QCT_ID"
        ddlQualCatagory.DataBind()

        Dim li As ListItem = New ListItem("--Select--", 0)
        ddlQualCatagory.Items.Insert(0, li)
        If ddlQualCatagory.SelectedValue <> "0" Then
            BindQualificationLevel()
        End If

        BindQualificationLevel()
        'BindQualificationDegree()

    End Sub
    Protected Sub ddlQualCatagory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQualCatagory.SelectedIndexChanged
        BindQualificationLevel()
    End Sub

    Protected Sub BindQualificationLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm(2) As SqlClient.SqlParameter
        parm(0) = New SqlClient.SqlParameter("@OPTION", 2)
        parm(1) = New SqlClient.SqlParameter("@QCT_ID", ddlQualCatagory.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)

        ddlQualificationLevel.DataSource = ds
        ddlQualificationLevel.DataTextField = "QLF_DESCR"
        ddlQualificationLevel.DataValueField = "QLF_ID"
        ddlQualificationLevel.DataBind()

        Dim li As ListItem = New ListItem("--Select--", 0)
        ddlQualificationLevel.Items.Insert(0, li)
    End Sub
    Protected Sub ddlQualificationLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQualificationLevel.SelectedIndexChanged

    End Sub
    Protected Sub BindrblQualCertType()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm As SqlParameter() = New SqlParameter(1) {}
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 5)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)
        RbtnDegreeCerti.DataSource = ds
        RbtnDegreeCerti.DataValueField = "QLT_ID"
        RbtnDegreeCerti.DataTextField = "QLT_DESC"
        RbtnDegreeCerti.DataBind()
        RbtnDegreeCerti.SelectedIndex = "0"
        RbtnDegreeCerti_SelectedIndexChanged(RbtnDegreeCerti, Nothing)
    End Sub
    Protected Sub RbtnDegreeCerti_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbtnDegreeCerti.SelectedIndexChanged
        ddlQualDegreeCert.Items.Clear()
        ddlQualDegreeCert.DataBind()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm(1) As SqlClient.SqlParameter
        parm(0) = New SqlClient.SqlParameter("@OPTION", 7)
        parm(1) = New SqlClient.SqlParameter("@DEGREE_CERTIFICATE", RbtnDegreeCerti.SelectedValue)
        Dim li As RadComboBoxItem
        Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)
            While dr.Read()

                If dr("QLF_bGROUPHEADER") Then
                    li = New RadComboBoxItem(dr("QLF_DESCR"))
                    li.IsSeparator = "true"
                Else
                    li = New RadComboBoxItem(dr("QLF_DESCR"), dr("QLF_ID"))
                End If

                ddlQualDegreeCert.Items.Add(li)
            End While
        End Using
        li = New RadComboBoxItem("---Select--", "---Select--")
        ddlQualDegreeCert.Items.Insert(0, li)
        BindCertificateType()
    End Sub
    Protected Sub BindCertificateType()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm As SqlParameter() = New SqlParameter(1) {}
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 8)
        parm(1) = New System.Data.SqlClient.SqlParameter("@QLT_ID", RbtnDegreeCerti.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)
        ddlECertificateType.DataSource = ds
        ddlECertificateType.DataValueField = "QGH_ID"
        ddlECertificateType.DataTextField = "QGH_DESCRIPTION"
        ddlECertificateType.DataBind()
        If RbtnDegreeCerti.SelectedValue = "1" Then
            ddlECertificateType.SelectedValue = "1"
            ddlECertificateType_SelectedIndexChanged(ddlECertificateType, Nothing)
        Else
            Dim li As New ListItem("---Select--", "0")
            ddlECertificateType.Items.Insert(0, li)
        End If
    End Sub
    Protected Sub ddlECertificateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindQualificationDegree(ddlQualDegreeCert)
    End Sub

    Protected Sub BindQualificationDegree(ByVal RCB As RadComboBox)
        Try
            RCB.Items.Clear()
            Dim li As RadComboBoxItem = Nothing
            If RbtnDegreeCerti.SelectedValue <> "" Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim parm As SqlParameter() = New SqlParameter(2) {}
                parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 7)
                parm(1) = New System.Data.SqlClient.SqlParameter("@QLF_QGH_ID", ddlECertificateType.SelectedValue)

                Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)
                    While dr.Read()
                        If Convert.ToBoolean(dr("QLF_bGROUPHEADER")) Then
                            li = New RadComboBoxItem(Convert.ToString(dr("QLF_DESCR")))
                            li.IsSeparator = True
                        Else
                            li = New RadComboBoxItem(Convert.ToString(dr("QLF_DESCR")), Convert.ToString(dr("QLF_ID")))
                        End If
                        RCB.Items.Add(li)
                    End While
                End Using
            End If
            li = New RadComboBoxItem("---Select--", "---Select--")
            RCB.Items.Insert(0, li)
        Catch exx As FormatException
        End Try
    End Sub
    Protected Sub ddlQualDegreeCert_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlQualDegreeCert.SelectedIndexChanged

        Try
            If ddlQualDegreeCert.SelectedItem.Text = "Other" Then

                txtOtherQualification.Visible = True
                'DivEQualDegreeCert.Attributes.Add("style", "display:block")
            Else
                ''txtQualificOther.Text = ""
                txtOtherQualification.Visible = False
                ' DivEQualDegreeCert.Attributes.Add("style", "display:none")
            End If

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Using conn As New SqlConnection(str_conn)
                Dim pParms As System.Data.SqlClient.SqlParameter() = New System.Data.SqlClient.SqlParameter(8) {}
                pParms(0) = New System.Data.SqlClient.SqlParameter("@OPTION", "6")
                pParms(1) = New System.Data.SqlClient.SqlParameter("@QLF_ID", ddlQualDegreeCert.SelectedValue)

                Using dr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", pParms)
                    While dr.Read()
                        RFV_txtQlExpDate.Visible = Convert.ToBoolean(dr("QLF_bEXPDATE"))
                    End While
                End Using
            End Using
        Catch exx As FormatException
            'PanelQualDetails.Visible = false;
            'lblErrEdit.Text = exx.Message
        End Try
        'txtESpecialization.Visible = true; 
    End Sub

    'Protected Sub BindQualificationDegree()
    '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '    Dim parm(1) As SqlClient.SqlParameter
    '    parm(0) = New SqlClient.SqlParameter("@OPTION", 3)
    '    Dim li As RadComboBoxItem
    '    Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", parm)
    '        While dr.Read()

    '            If dr("QLF_bGROUPHEADER") Then
    '                li = New RadComboBoxItem(dr("QLF_DESCR"))
    '                li.IsSeparator = "true"
    '            Else
    '                li = New RadComboBoxItem(dr("QLF_DESCR"), dr("QLF_ID"))
    '            End If

    '            ddlQualDegreeCert.Items.Add(li)
    '        End While
    '    End Using
    '    li = New RadComboBoxItem("---Select--", "---Select--")
    '    ddlQualDegreeCert.Items.Insert(0, li)
    'End Sub
    Protected Sub BindYearOfQualification()
        Dim year As Integer = System.DateTime.Now.Year
        For i As Integer = 0 To 100
            Dim li As ListItem
            If (i = 0) Then
                li = New ListItem("--Select--", "-1")
                ddlQualYear.Items.Insert(i, li)
            End If
            li = New ListItem(year - i, year - i)
            ddlQualYear.Items.Insert(i + 1, li)

        Next
    End Sub
    Private Function CreateQualificationDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cQualification As New DataColumn("Qualification", System.Type.GetType("System.String"))
            ''added by nahyan on 3feb2016 for other
            Dim cQualificationOtherText As New DataColumn("QualificationOtherText", System.Type.GetType("System.String"))
            ''ends here 
            Dim cQualificationID As New DataColumn("Qualification_ID", System.Type.GetType("System.Int32"))
            Dim cQualificationLevel As New DataColumn("Qualification_Level", System.Type.GetType("System.String"))
            Dim cQualificationCat As New DataColumn("Qualif_Category", System.Type.GetType("System.String"))
            Dim cQualificationCatID As New DataColumn("Qualif_Category_ID", System.Type.GetType("System.Int32"))
            Dim cInstitute As New DataColumn("Institute", System.Type.GetType("System.String"))
            Dim cVerificationStatus As New DataColumn("VStatus", System.Type.GetType("System.String"))
            Dim cGrade As New DataColumn("Grade", System.Type.GetType("System.String"))
            Dim cGrade_ID As New DataColumn("Grade_ID", System.Type.GetType("System.Int32"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cQualification)
            ''added by nahyan on 3feb2016 for other
            dtDt.Columns.Add(cQualificationOtherText)

            dtDt.Columns.Add(cQualificationID)
            dtDt.Columns.Add(cQualificationLevel)
            dtDt.Columns.Add(cQualificationCat)
            dtDt.Columns.Add(cQualificationCatID)
            dtDt.Columns.Add(cVerificationStatus)
            dtDt.Columns.Add(cInstitute)
            dtDt.Columns.Add(cGrade)
            dtDt.Columns.Add(cGrade_ID)
            dtDt.Columns.Add(cStatus)


            Dim cSpecialization As New DataColumn("cSpecialization", System.Type.GetType("System.String"))
            Dim cExpiryDate As New DataColumn("cExpiryDate", System.Type.GetType("System.String"))
            Dim cAwardingbody As New DataColumn("cAwardingbody", System.Type.GetType("System.String"))
            Dim cLocation As New DataColumn("cLocation", System.Type.GetType("System.String"))
            Dim cYearOfQual As New DataColumn("cYearOfQual", System.Type.GetType("System.String"))
            Dim cRegno As New DataColumn("cRegno", System.Type.GetType("System.String"))
            Dim cRemarks As New DataColumn("cRemarks", System.Type.GetType("System.String"))
            Dim cDocFile As New DataColumn("DocFile", System.Type.GetType("System.Object"))
            Dim cDocFileType As New DataColumn("DocFileType", System.Type.GetType("System.String"))
            Dim cDocFileMIME As New DataColumn("DocFileMIME", System.Type.GetType("System.String"))
            Dim cIssueDate As New DataColumn("cIssueDate", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cSpecialization)
            dtDt.Columns.Add(cExpiryDate)
            dtDt.Columns.Add(cAwardingbody)
            dtDt.Columns.Add(cLocation)
            dtDt.Columns.Add(cYearOfQual)
            dtDt.Columns.Add(cRegno)
            dtDt.Columns.Add(cRemarks)
            dtDt.Columns.Add(cDocFile)
            dtDt.Columns.Add(cDocFileType)
            dtDt.Columns.Add(cDocFileMIME)
            dtDt.Columns.Add(cIssueDate)

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnQualAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualAdd.Click
        If Session("EMPQUALDETAILS") Is Nothing Then
            Session("EMPQUALDETAILS") = CreateQualificationDetailsTable()
        End If
        h_Qual_Grade.Value = 0
        Dim dtQUALDetails As DataTable = Session("EMPQUALDETAILS")
        Dim Status As String = String.Empty

        Dim i As Integer
        For i = 0 To dtQUALDetails.Rows.Count - 1
            If Session("EMPQualEditID") IsNot Nothing And _
            Session("EMPQualEditID") <> dtQUALDetails.Rows(i)("UniqueID") Then
                If dtQUALDetails.Rows(i)("Qualification") = txtQualQualification.Text And _
                    Convert.ToString(dtQUALDetails.Rows(i)("Qualification_ID")) = h_Qual_QualID.Value And _
                     dtQUALDetails.Rows(i)("Institute") = txtQualInstitute.Text And _
                     dtQUALDetails.Rows(i)("Grade") = txtQualGrade.Text Then
                    'And dtQUALDetails.Rows(i)("Grade_ID") = h_Qual_Grade.Value Then
                    lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                    GridBindQualificationDetails()
                    Exit Sub
                End If
            End If
        Next

        If btnQualAdd.Text = "Add" Then
            Dim newDR As DataRow = dtQUALDetails.NewRow()
            newDR("UniqueID") = gvQualification.Rows.Count()
            newDR("Qualification") = ddlQualDegreeCert.SelectedItem.Text

            ''added by nahyan on 3feb2016 to insert other 
            If ddlQualDegreeCert.SelectedItem.Text = "Other" Then
                newDR("QualificationOtherText") = txtOtherQualification.Text
            Else
                newDR("QualificationOtherText") = ""
            End If
            newDR("Qualification_ID") = ddlQualDegreeCert.SelectedValue
            newDR("Qualif_Category") = ddlQualCatagory.SelectedItem
            newDR("Qualif_Category_ID") = ddlQualCatagory.SelectedValue
            newDR("Qualification_Level") = ddlQualificationLevel.SelectedValue

            newDR("Institute") = txtQualInstitute.Text
            newDR("Grade") = txtQualGrade.Text
            newDR("Grade_ID") = h_Qual_Grade.Value
            newDR("VStatus") = ddQualVStatus.SelectedValue

            newDR("cSpecialization") = txtQualSpecialization.Text
            newDR("cExpiryDate") = txtQualCertExpDate.Text
            newDR("cAwardingbody") = txtQualAwardingBody.Text
            newDR("cLocation") = txtQualLocation.Text
            newDR("cYearOfQual") = ddlQualYear.SelectedValue
            newDR("cRegno") = txtQualRegNo.Text
            newDR("cRemarks") = txtQualRemarks.Text
            newDR("cIssueDate") = txtQualCertStartDate.Text

            If FileUploadQualCertificate.HasFile Then

                Dim File As HttpPostedFile
                File = FileUploadQualCertificate.PostedFile
                Dim imgByte As Byte() = Nothing
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
                newDR("DocFile") = imgByte
                newDR("DocFileType") = System.IO.Path.GetExtension(FileUploadQualCertificate.FileName)
                newDR("DocFileMIME") = FileUploadQualCertificate.PostedFile.ContentType
            End If


            newDR("Status") = "Insert"
            Status = "Insert"
            dtQUALDetails.Rows.Add(newDR)
            btnQualAdd.Text = "Add"
        ElseIf btnQualAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPQualEditID")
            For iIndex = 0 To dtQUALDetails.Rows.Count - 1
                If str_Search = dtQUALDetails.Rows(iIndex)("UniqueID") And dtQUALDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtQUALDetails.Rows(iIndex)("Qualification") = ddlQualDegreeCert.SelectedItem.Text
                    ''added by nahyan on 3feb2016 to insert other 
                    If ddlQualDegreeCert.SelectedItem.Text = "Other" Then
                        dtQUALDetails.Rows(iIndex)("QualificationOtherText") = txtOtherQualification.Text
                    Else
                        dtQUALDetails.Rows(iIndex)("QualificationOtherText") = ""
                    End If

                    dtQUALDetails.Rows(iIndex)("Qualification_ID") = ddlQualDegreeCert.SelectedValue
                    dtQUALDetails.Rows(iIndex)("Qualif_Category") = ddlQualCatagory.SelectedItem
                    dtQUALDetails.Rows(iIndex)("Qualif_Category_ID") = ddlQualCatagory.SelectedValue
                    dtQUALDetails.Rows(iIndex)("Qualification_Level") = ddlQualificationLevel.SelectedValue

                    dtQUALDetails.Rows(iIndex)("Institute") = txtQualInstitute.Text
                    dtQUALDetails.Rows(iIndex)("Grade") = txtQualGrade.Text
                    dtQUALDetails.Rows(iIndex)("VStatus") = ddQualVStatus.SelectedValue
                    dtQUALDetails.Rows(iIndex)("Grade_ID") = h_Qual_Grade.Value

                    dtQUALDetails.Rows(iIndex)("cSpecialization") = txtQualSpecialization.Text
                    dtQUALDetails.Rows(iIndex)("cExpiryDate") = txtQualCertExpDate.Text
                    dtQUALDetails.Rows(iIndex)("cAwardingbody") = txtQualAwardingBody.Text
                    dtQUALDetails.Rows(iIndex)("cLocation") = txtQualLocation.Text
                    dtQUALDetails.Rows(iIndex)("cYearOfQual") = ddlQualYear.SelectedValue
                    dtQUALDetails.Rows(iIndex)("cRegno") = txtQualRegNo.Text
                    dtQUALDetails.Rows(iIndex)("cRemarks") = txtQualRemarks.Text
                    dtQUALDetails.Rows(iIndex)("cIssueDate") = txtQualCertStartDate.Text

                    If FileUploadQualCertificate.HasFile Then

                        Dim File As HttpPostedFile
                        File = FileUploadQualCertificate.PostedFile
                        Dim imgByte As Byte() = Nothing
                        imgByte = New Byte(File.ContentLength - 1) {}
                        File.InputStream.Read(imgByte, 0, File.ContentLength)
                        dtQUALDetails.Rows(iIndex)("DocFile") = imgByte
                        dtQUALDetails.Rows(iIndex)("DocFileType") = System.IO.Path.GetExtension(FileUploadQualCertificate.FileName)
                        dtQUALDetails.Rows(iIndex)("DocFileMIME") = FileUploadQualCertificate.PostedFile.ContentType
                    End If

                    Status = "Edit/Update"
                    Exit For
                End If
            Next
            btnQualAdd.Text = "Add"
        End If

        If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, Status, Page.User.Identity.Name.ToString, vwQualification) = 0 Then
            ClearEMPQualDetails()
        Else
            UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        End If
        Session("EMPQUALDETAILS") = dtQUALDetails
        GridBindQualificationDetails()
    End Sub

    Private Sub GridBindQualificationDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPQUALDETAILS") Is Nothing Then Return
        If Session("EMPQUALDETAILS") Is Nothing Then
            gvQualification.DataSource = Nothing
            gvQualification.DataBind()
            Return
        End If
        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateQualificationDetailsTable()
        If Session("EMPQUALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPQUALDETAILS").Rows.Count - 1
                If (Session("EMPQUALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPQUALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPQUALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvQualification.DataSource = dtTempDtl
        gvQualification.DataBind()
    End Sub

    Private Sub DeleteRowQualDetails(ByVal pId As String)
        Dim lblTid As New Label
        lblTid = TryCast(gvQualification.Rows(pId).FindControl("lblId"), Label)

        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub

        Dim iRemove As Integer = 0
        For iRemove = 0 To Session("EMPQUALDETAILS").Rows.Count - 1
            If (Session("EMPQUALDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPQUALDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwQualification) = 0 Then
                    GridBindQualificationDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnQualCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualCancel.Click
        ClearEMPQualDetails()
    End Sub

    Sub ClearEMPQualDetails()
        txtQualGrade.Text = ""
        h_Qual_Grade.Value = ""
        txtQualInstitute.Text = ""
        txtQualQualification.Text = ""
        h_Qual_QualID.Value = ""

        ddlQualCatagory.SelectedValue = "0"
        ddlQualificationLevel.SelectedValue = "0"
        ddlQualDegreeCert.SelectedIndex = 0
        txtQualSpecialization.Text = ""
        txtQualCertExpDate.Text = ""
        txtQualAwardingBody.Text = ""
        txtQualLocation.Text = ""
        ddlQualYear.SelectedIndex = 0
        txtQualRegNo.Text = ""
        txtQualRemarks.Text = ""
        txtQualCertStartDate.Text = ""

    End Sub

    Protected Sub lnkQualEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BindQualificationCategory()
        BindQualificationLevel()

        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPQUALDETAILS").Rows.Count - 1
            If str_Search = Session("EMPQUALDETAILS").Rows(iIndex)("UniqueID") And Session("EMPQUALDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                'txtQualQualification.Text = Session("EMPQUALDETAILS").Rows(iIndex)("Qualification")
                h_Qual_QualID.Value = Session("EMPQUALDETAILS").Rows(iIndex)("Qualification_ID")
                'txtQual_QualifCategory.Text = Session("EMPQUALDETAILS").Rows(iIndex)("Qualif_Category")
                'h_Qual_category_ID.Value = Session("EMPQUALDETAILS").Rows(iIndex)("Qualif_Category_ID")


                ddlQualCatagory.SelectedValue = Session("EMPQUALDETAILS").Rows(iIndex)("Qualif_Category_ID")
                ddlQualCatagory_SelectedIndexChanged(ddlQualCatagory, Nothing)

                ddlQualificationLevel.SelectedValue = Session("EMPQUALDETAILS").Rows(iIndex)("Qualification_Level")
                txtQualInstitute.Text = Session("EMPQUALDETAILS").Rows(iIndex)("Institute")
                txtQualGrade.Text = Session("EMPQUALDETAILS").Rows(iIndex)("Grade")
                ddQualVStatus.SelectedValue = Session("EMPQUALDETAILS").Rows(iIndex)("VStatus")
                h_Qual_Grade.Value = Session("EMPQUALDETAILS").Rows(iIndex)("Grade_ID")

                txtQualSpecialization.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cSpecialization")
                txtQualCertExpDate.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cExpiryDate")
                txtQualCertStartDate.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cIssueDate")
                txtQualAwardingBody.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cAwardingbody")
                txtQualLocation.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cLocation")
                txtQualRegNo.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cRegno")
                txtQualRemarks.Text = Session("EMPQUALDETAILS").Rows(iIndex)("cRemarks")
                If (Session("EMPQUALDETAILS").Rows(iIndex)("cYearOfQual") <> "") Then
                    ddlQualYear.SelectedValue = Session("EMPQUALDETAILS").Rows(iIndex)("cYearOfQual")
                End If
                ddlQualDegreeCert.ClearSelection()
                ' ddlQualDegreeCert.SelectedValue = Session("EMPQUALDETAILS").Rows(iIndex)("Qualification_ID")
                If String.IsNullOrEmpty(Session("EMPQUALDETAILS").Rows(iIndex)("Qualification_ID")) Then
                    ddlQualDegreeCert.SelectedIndex = 0
                Else
                    Dim QLF_ID As String = Session("EMPQUALDETAILS").Rows(iIndex)("Qualification_ID")
                    ''added by nahyan on 3feb2016 
                    If QLF_ID = "124" Then ''OTHER
                        If String.IsNullOrEmpty(Convert.ToString(Session("EMPQUALDETAILS").Rows(iIndex)("QualificationOtherText"))) Then
                            txtOtherQualification.Text = ""
                        Else
                            txtOtherQualification.Text = Session("EMPQUALDETAILS").Rows(iIndex)("QualificationOtherText")
                        End If

                    End If
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Using conn As New SqlConnection(str_conn)
                        Dim pParms As System.Data.SqlClient.SqlParameter() = New System.Data.SqlClient.SqlParameter(8) {}
                        pParms(0) = New System.Data.SqlClient.SqlParameter("@OPTION", "6")
                        pParms(1) = New System.Data.SqlClient.SqlParameter("@QLF_ID", QLF_ID)
                        Using dr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "GETQUALIFICATION_MASTER_DETAILS", pParms)
                            While dr.Read()
                                '  RbtnDegreeCerti.Items.FindByValue(dr("QLF_QLT_ID").ToString()).Selected = True
                                RbtnDegreeCerti.SelectedValue = dr("QLF_QLT_ID").ToString()
                                RbtnDegreeCerti_SelectedIndexChanged(RbtnDegreeCerti, Nothing)
                                ddlECertificateType.Items.FindByValue(dr("QLF_QGH_ID").ToString()).Selected = True
                                ddlECertificateType_SelectedIndexChanged(ddlECertificateType, Nothing)
                            End While
                        End Using
                    End Using
                    ddlQualDegreeCert.Items.FindItemByValue(QLF_ID).Selected = True
                    ddlQualDegreeCert_SelectedIndexChanged(ddlQualDegreeCert, Nothing)
                End If



                gvQualification.SelectedIndex = iIndex
                btnQualAdd.Text = "Save"
                Session("EMPQualEditID") = str_Search
                Exit For
            End If
        Next
    End Sub

    Private Function SaveEMPQualificationDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPQUALDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPQUALDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPQUALDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPQUALDETAILS").Rows(iIndex)
                    If (Session("EMPQUALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then

                        cmd = New SqlCommand("SaveEMPQUALIFICATION_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_EMP_ID As New SqlParameter("@EQS_EMP_ID", SqlDbType.Int)
                        sqlpEMD_EMP_ID.Value = EmpID 'EmployeeID
                        cmd.Parameters.Add(sqlpEMD_EMP_ID)

                        Dim sqlpEQS_ID As New SqlParameter("@EQS_ID", SqlDbType.Int)
                        'sqlpEQS_ID.Value = DBNull.Value  'EmployeeID
                        If ViewState("datamode") = "add" Then
                            sqlpEQS_ID.Value = 0
                        ElseIf ViewState("datamode") = "edit" Then
                            sqlpEQS_ID.Value = dr("UniqueID")
                        End If
                        cmd.Parameters.Add(sqlpEQS_ID)

                        Dim sqlpEMP_QLF_ID As New SqlParameter("@EMP_QLF_ID", SqlDbType.Int)
                        sqlpEMP_QLF_ID.Value = dr("Qualification_ID")
                        cmd.Parameters.Add(sqlpEMP_QLF_ID)
                        ''added by nahyan on 3feb2016
                        Dim sqlpEMP_QLF_OTHER As New SqlParameter("@EMP_QLF_OTHER", SqlDbType.NVarChar)
                        sqlpEMP_QLF_OTHER.Value = dr("QualificationOtherText")
                        cmd.Parameters.Add(sqlpEMP_QLF_OTHER)

                        Dim sqlpEMP_INSTITUTE As New SqlParameter("@EMP_INSTITUTE", SqlDbType.VarChar, 200)
                        sqlpEMP_INSTITUTE.Value = dr("Institute")
                        cmd.Parameters.Add(sqlpEMP_INSTITUTE)

                        Dim sqlpEMP_STATUS As New SqlParameter("@EMP_STATUS", SqlDbType.VarChar, 50)
                        sqlpEMP_STATUS.Value = dr("VStatus")
                        cmd.Parameters.Add(sqlpEMP_STATUS)

                        Dim sqlpEMP_GRADE As New SqlParameter("@EMP_GRADE", SqlDbType.VarChar, 20)
                        sqlpEMP_GRADE.Value = dr("Grade") 'dr("Grade_ID")
                        cmd.Parameters.Add(sqlpEMP_GRADE)


                        Dim sqlpEQS_QCT_ID As New SqlParameter("@EQS_QCT_ID", SqlDbType.VarChar, 20)
                        sqlpEQS_QCT_ID.Value = dr("Qualif_Category_ID")
                        cmd.Parameters.Add(sqlpEQS_QCT_ID)

                        Dim sqlpEQS_QCL_ID As New SqlParameter("@EQS_QCL_ID", SqlDbType.VarChar, 20)
                        sqlpEQS_QCL_ID.Value = dr("Qualification_Level")
                        cmd.Parameters.Add(sqlpEQS_QCL_ID)

                        Dim sqlpEQS_SPECIALIZATION As New SqlParameter("@EQS_SPECIALIZATION", SqlDbType.VarChar)
                        sqlpEQS_SPECIALIZATION.Value = dr("cSpecialization")
                        cmd.Parameters.Add(sqlpEQS_SPECIALIZATION)

                        Dim sqlpEQS_EXPIRYDATE As New SqlParameter("@EQS_EXPIRYDATE", SqlDbType.VarChar)
                        sqlpEQS_EXPIRYDATE.Value = dr("cExpiryDate")
                        cmd.Parameters.Add(sqlpEQS_EXPIRYDATE)

                        Dim sqlpEQS_AWARDINGBODY As New SqlParameter("@EQS_AWARDINGBODY", SqlDbType.VarChar)
                        sqlpEQS_AWARDINGBODY.Value = dr("cAwardingbody")
                        cmd.Parameters.Add(sqlpEQS_AWARDINGBODY)

                        Dim sqlpEQS_LOCATION As New SqlParameter("@EQS_LOCATION", SqlDbType.VarChar)
                        sqlpEQS_LOCATION.Value = dr("cLocation")
                        cmd.Parameters.Add(sqlpEQS_LOCATION)

                        Dim sqlpEQS_YEAROFQUAL As New SqlParameter("@EQS_YEAROFQUAL", SqlDbType.VarChar)
                        sqlpEQS_YEAROFQUAL.Value = dr("cYearOfQual")
                        cmd.Parameters.Add(sqlpEQS_YEAROFQUAL)

                        Dim sqlpEQS_REGNO As New SqlParameter("@EQS_REGNO", SqlDbType.VarChar)
                        sqlpEQS_REGNO.Value = dr("cRegno")
                        cmd.Parameters.Add(sqlpEQS_REGNO)

                        Dim sqlpEQS_REMARKS As New SqlParameter("@EQS_REMARKS", SqlDbType.VarChar)
                        sqlpEQS_REMARKS.Value = dr("cRemarks")
                        cmd.Parameters.Add(sqlpEQS_REMARKS)

                        Dim sqlpEMD_DOCFILE As New SqlParameter("@EMD_DOCFILE", SqlDbType.VarBinary)
                        sqlpEMD_DOCFILE.Value = dr("DocFile")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE)

                        Dim sqlpEMD_DOCFILE_TYPE As New SqlParameter("@EMD_DOCFILE_TYPE", SqlDbType.VarChar)
                        sqlpEMD_DOCFILE_TYPE.Value = dr("DocFileType")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE_TYPE)

                        Dim sqlpEMD_DOCFILE_MIME As New SqlParameter("@EMD_DOCFILE_MIME", SqlDbType.VarChar)
                        sqlpEMD_DOCFILE_MIME.Value = dr("DocFileMIME")
                        cmd.Parameters.Add(sqlpEMD_DOCFILE_MIME)

                        Dim sqlpEQS_STARTDATE As New SqlParameter("@EQS_STARTDATE", SqlDbType.VarChar)
                        sqlpEQS_STARTDATE.Value = dr("cIssueDate")
                        cmd.Parameters.Add(sqlpEQS_STARTDATE)
                        'Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                        'If ViewState("datamode") = "add" Then
                        '    sqlpbEdit.Value = False
                        'ElseIf ViewState("datamode") = "edit" Then
                        '    sqlpbEdit.Value = True
                        'End If
                        'cmd.Parameters.Add(sqlpbEdit)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    Else
                        'Delete Code will Come here
                        cmd = New SqlCommand("DELETEEMPQUALIFICATION_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEQS_ID As New SqlParameter("@EQS_ID", SqlDbType.Int)
                        sqlpEQS_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpEQS_ID)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Protected Sub gvQualification_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvQualification.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowQualDetails(categoryID)
    End Sub


    Protected Sub gvQualification_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvQualification.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkViewQualDoc As LinkButton = DirectCast(e.Row.FindControl("lnkViewQualDoc"), LinkButton)
            Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")

            ScriptManager1.RegisterPostBackControl(lnkViewQualDoc)
            'Dim UpdatePanel1 As UpdatePanel = Page.Master.FindControl("UpdatePanel1")
            'Dim postbacktri As PostBackTrigger = New PostBackTrigger()
            'postbacktri.ControlID = lnkViewQualDoc.ClientID

            'UpdatePanel1.Triggers.Add(postbacktri)

            Dim HF_QualDocFileMIME As HiddenField = DirectCast(e.Row.FindControl("HF_QualDocFileMIME"), HiddenField)
            If String.IsNullOrEmpty(HF_QualDocFileMIME.Value) Then
                lnkViewQualDoc.Enabled = False
            End If
        End If
    End Sub

    Protected Sub lnkViewQualDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblID"), Label)
        For iIndex As Integer = 0 To Session("EMPQUALDETAILS").Rows.Count - 1
            If Session("EMPQUALDETAILS").Rows(iIndex)("UniqueID") = lblTid.Text Then

                If Not String.IsNullOrEmpty(Session("EMPQUALDETAILS").Rows(iIndex)("DocFileType")) Then
                    Dim imgByte As Byte() = CType(Session("EMPQUALDETAILS").Rows(iIndex)("DocFile"), Byte())
                    'Response.ContentType = Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    'Response.BinaryWrite(imgByte)

                    Dim Filename As String = Session("EMPQUALDETAILS").Rows(iIndex)("Qualification").ToString() + Session("EMPQUALDETAILS").Rows(iIndex)("DocFileType").ToString()

                    'Response.Clear()
                    'Response.Buffer = True
                    'Response.ContentType = Session("EMPQUALDETAILS").Rows(iIndex)("DocFileMIME").ToString()
                    'Response.AddHeader("Content-Disposition", "attachment;filename=" + Filename)
                    'Response.BinaryWrite(imgByte)
                    'Response.Flush()


                    Page.Response.ClearContent()

                    Page.Response.ClearHeaders()

                    Page.Response.ContentType = Session("EMPQUALDETAILS").Rows(iIndex)("DocFileMIME").ToString()

                    Page.Response.AddHeader("Content-Disposition", "attachment;filename=" + Filename)

                    Page.Response.BinaryWrite(imgByte)

                    Page.Response.End()

                End If

            End If
        Next
    End Sub
#End Region

#Region "Experience Details"

    Private Function CreateExperienceDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cOrganisation As New DataColumn("Organisation", System.Type.GetType("System.String"))
            Dim cAddress As New DataColumn("Address", System.Type.GetType("System.String"))
            Dim cDesignation As New DataColumn("Designation", System.Type.GetType("System.String"))
            Dim cRemarks As New DataColumn("Remarks", System.Type.GetType("System.String"))
            Dim cReference1 As New DataColumn("Reference1", System.Type.GetType("System.String"))
            Dim cReferenceNo1 As New DataColumn("ReferenceNo1", System.Type.GetType("System.String"))
            Dim cReference2 As New DataColumn("Reference2", System.Type.GetType("System.String"))
            Dim cReferenceNo2 As New DataColumn("ReferenceNo2", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.DateTime"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.DateTime"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cOrganisation)
            dtDt.Columns.Add(cAddress)
            dtDt.Columns.Add(cDesignation)
            dtDt.Columns.Add(cRemarks)
            dtDt.Columns.Add(cReference1)
            dtDt.Columns.Add(cReferenceNo1)
            dtDt.Columns.Add(cReference2)
            dtDt.Columns.Add(cReferenceNo2)
            dtDt.Columns.Add(cFromDate)
            dtDt.Columns.Add(cToDate)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnExpAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpAdd.Click
        If Session("EMPEXPDETAILS") Is Nothing Then
            Session("EMPEXPDETAILS") = CreateExperienceDetailsTable()
        End If
        Dim dtExpDetails As DataTable = Session("EMPEXPDETAILS")
        Dim status As String = String.Empty
        Dim i As Integer

        If CDate(txtExpFromDate.Text) > CDate(txtExpToDate.Text) Then
            lblError.Text = "To Date should be less than From Date"
            GridBindExperienceDetails()
            Exit Sub
        End If

        For i = 0 To dtExpDetails.Rows.Count - 1
            If Session("EMPExpEditID") IsNot Nothing And _
            Session("EMPExpEditID") <> dtExpDetails.Rows(i)("UniqueID") And _
            dtExpDetails.Rows(i)("STATUS") <> "DELETED" Then
                If dtExpDetails.Rows(i)("Organisation") = txtExpOrganisation.Text And _
                    dtExpDetails.Rows(i)("Address") = txtExpAddress.Text And _
                     dtExpDetails.Rows(i)("Remarks") = txtExpRemarks.Text And _
                     dtExpDetails.Rows(i)("Designation") = txtExpDesignation.Text And _
                     dtExpDetails.Rows(i)("Reference1") = txtExpRefer1.Text And _
                     dtExpDetails.Rows(i)("ReferenceNo1") = txtExpReferPhNo1.Text And _
                     dtExpDetails.Rows(i)("Reference2") = txtExpRefer2.Text And _
                     dtExpDetails.Rows(i)("ReferenceNo2") = txtExpReferPhNo2.Text And _
                     dtExpDetails.Rows(i)("FromDate") = txtExpFromDate.Text And _
                     dtExpDetails.Rows(i)("ToDate") = txtExpToDate.Text Then
                    lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                    GridBindExperienceDetails()
                    Exit Sub
                End If
            End If
        Next

        If btnExpAdd.Text = "Add" Then
            Dim newDR As DataRow = dtExpDetails.NewRow()
            newDR("UniqueID") = gvExperience.Rows.Count()
            newDR("Organisation") = txtExpOrganisation.Text
            newDR("Address") = txtExpAddress.Text
            newDR("Designation") = txtExpDesignation.Text
            newDR("Remarks") = txtExpRemarks.Text
            newDR("Reference1") = txtExpRefer1.Text
            newDR("ReferenceNo1") = txtExpReferPhNo1.Text
            newDR("Reference2") = txtExpRefer2.Text
            newDR("ReferenceNo2") = txtExpReferPhNo2.Text
            newDR("FromDate") = txtExpFromDate.Text
            newDR("ToDate") = txtExpToDate.Text
            newDR("Status") = "Insert"
            status = "Insert"
            dtExpDetails.Rows.Add(newDR)
            btnExpAdd.Text = "Add"
        ElseIf btnExpAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPExpEditID")
            For iIndex = 0 To dtExpDetails.Rows.Count - 1
                If str_Search = dtExpDetails.Rows(iIndex)("UniqueID") And dtExpDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtExpDetails.Rows(iIndex)("Organisation") = txtExpOrganisation.Text
                    dtExpDetails.Rows(iIndex)("Address") = txtExpAddress.Text
                    dtExpDetails.Rows(iIndex)("Designation") = txtExpDesignation.Text
                    dtExpDetails.Rows(iIndex)("Remarks") = txtExpRemarks.Text
                    dtExpDetails.Rows(iIndex)("Reference1") = txtExpRefer1.Text
                    dtExpDetails.Rows(iIndex)("ReferenceNo1") = txtExpReferPhNo1.Text
                    dtExpDetails.Rows(iIndex)("Reference2") = txtExpRefer2.Text
                    dtExpDetails.Rows(iIndex)("ReferenceNo2") = txtExpReferPhNo2.Text
                    dtExpDetails.Rows(iIndex)("FromDate") = txtExpFromDate.Text
                    dtExpDetails.Rows(iIndex)("ToDate") = txtExpToDate.Text
                    status = "Update/Edit"
                    Exit For
                End If
            Next
            btnExpAdd.Text = "Add"
            ClearEMPExpDetails()
        End If
        If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwExperience) <> 0 Then
            ClearEMPExpDetails()
        Else
            UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        End If
        Session("EMPEXPDETAILS") = dtExpDetails
        GridBindExperienceDetails()
    End Sub

    Private Sub GridBindExperienceDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("EMPEXPDETAILS") Is Nothing Then
            gvExperience.DataSource = Nothing
            gvExperience.DataBind()
            Return
        End If

        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateExperienceDetailsTable()
        If Session("EMPEXPDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPEXPDETAILS").Rows.Count - 1
                If (Session("EMPEXPDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPEXPDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPEXPDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvExperience.DataSource = dtTempDtl
        gvExperience.DataBind()

    End Sub

    Private Sub DeleteRowExpDetails(ByVal pId As String)
        Dim lblTid As New Label
        lblTid = TryCast(gvExperience.Rows(pId).FindControl("lblId"), Label)

        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub

        Dim iRemove As Integer = 0
        For iRemove = 0 To Session("EMPEXPDETAILS").Rows.Count - 1
            If (Session("EMPEXPDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPEXPDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwExperience) = 0 Then
                    GridBindExperienceDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnExpCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpCancel.Click
        ClearEMPExpDetails()
    End Sub

    Sub ClearEMPExpDetails()
        txtExpOrganisation.Text = ""
        txtExpAddress.Text = ""
        txtExpDesignation.Text = ""
        txtExpRemarks.Text = ""
        txtExpRefer1.Text = ""
        txtExpReferPhNo1.Text = ""
        txtExpRefer2.Text = ""
        txtExpReferPhNo2.Text = ""
        txtExpFromDate.Text = ""
        txtExpToDate.Text = ""
        btnExpAdd.Text = "Add"
        lblError.Text = ""
    End Sub

    Protected Sub lnkExpEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPEXPDETAILS").Rows.Count - 1
            If str_Search = Session("EMPEXPDETAILS").Rows(iIndex)("UniqueID") And Session("EMPEXPDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then

                txtExpOrganisation.Text = Session("EMPEXPDETAILS").Rows(iIndex)("Organisation")
                txtExpAddress.Text = Session("EMPEXPDETAILS").Rows(iIndex)("Address")
                txtExpDesignation.Text = Session("EMPEXPDETAILS").Rows(iIndex)("Designation")
                txtExpRemarks.Text = Session("EMPEXPDETAILS").Rows(iIndex)("Remarks")
                txtExpRefer1.Text = Session("EMPEXPDETAILS").Rows(iIndex)("Reference1")
                txtExpReferPhNo1.Text = Session("EMPEXPDETAILS").Rows(iIndex)("ReferenceNo1")
                txtExpRefer2.Text = Session("EMPEXPDETAILS").Rows(iIndex)("Reference2")
                txtExpReferPhNo2.Text = Session("EMPEXPDETAILS").Rows(iIndex)("ReferenceNo2")
                If Session("EMPEXPDETAILS").Rows(iIndex)("FromDate") IsNot Nothing AndAlso Session("EMPEXPDETAILS").Rows(iIndex)("FromDate") IsNot DBNull.Value Then
                    txtExpFromDate.Text = Format(Session("EMPEXPDETAILS").Rows(iIndex)("FromDate"), OASISConstants.DateFormat)
                End If
                If Session("EMPEXPDETAILS").Rows(iIndex)("ToDate") IsNot Nothing AndAlso Session("EMPEXPDETAILS").Rows(iIndex)("ToDate") IsNot DBNull.Value Then
                    txtExpToDate.Text = Format(Session("EMPEXPDETAILS").Rows(iIndex)("ToDate"), OASISConstants.DateFormat)
                End If
                gvEMPDocDetails.SelectedIndex = iIndex
                btnExpAdd.Text = "Save"
                Session("EMPExpEditID") = str_Search
                UtilityObj.beforeLoopingControls(vwExperience)
                Exit For
            End If
        Next
    End Sub

    Private Function SaveEMPExperienceDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPEXPDETAILS") Is Nothing Then
                Return 0
            End If

            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPEXPDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPEXPDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPEXPDETAILS").Rows(iIndex)
                    If (Session("EMPEXPDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                        cmd = New SqlCommand("SaveEMPEXPERIANCE_D", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMX_ID As New SqlParameter("@EMX_ID", SqlDbType.Int)
                        If ViewState("datamode") = "add" Then
                            sqlpEMX_ID.Value = 0
                        ElseIf ViewState("datamode") = "edit" Then
                            sqlpEMX_ID.Value = dr("UniqueID")
                        End If
                        'sqlpEMX_ID.Value = 0 'EmployeeID
                        cmd.Parameters.Add(sqlpEMX_ID)

                        Dim sqlpEMX_EMP_ID As New SqlParameter("@EMX_EMP_ID", SqlDbType.Int)
                        sqlpEMX_EMP_ID.Value = EmpID 'EmployeeID
                        cmd.Parameters.Add(sqlpEMX_EMP_ID)

                        Dim sqlpEMX_DTTO As New SqlParameter("@EMX_DTTO", SqlDbType.DateTime)
                        sqlpEMX_DTTO.Value = dr("ToDate")
                        cmd.Parameters.Add(sqlpEMX_DTTO)

                        Dim sqlpEMX_DTFROM As New SqlParameter("@EMX_DTFROM", SqlDbType.DateTime)
                        sqlpEMX_DTFROM.Value = dr("FromDate")
                        cmd.Parameters.Add(sqlpEMX_DTFROM)

                        Dim sqlpEMX_ORGANISATION As New SqlParameter("@EMX_ORGANISATION", SqlDbType.VarChar, 50)
                        sqlpEMX_ORGANISATION.Value = dr("Organisation")
                        cmd.Parameters.Add(sqlpEMX_ORGANISATION)

                        Dim sqlpEMX_ADDRESS As New SqlParameter("@EMX_ADDRESS", SqlDbType.VarChar, 100)
                        sqlpEMX_ADDRESS.Value = dr("Address")
                        cmd.Parameters.Add(sqlpEMX_ADDRESS)

                        Dim sqlpEMX_POSITION As New SqlParameter("@EMX_POSITION", SqlDbType.VarChar, 100)
                        sqlpEMX_POSITION.Value = dr("Designation")
                        cmd.Parameters.Add(sqlpEMX_POSITION)

                        Dim sqlpEMX_REF1 As New SqlParameter("@EMX_REF1", SqlDbType.VarChar, 50)
                        sqlpEMX_REF1.Value = dr("Reference1")
                        cmd.Parameters.Add(sqlpEMX_REF1)

                        Dim sqlpEMX_REF1_NUM As New SqlParameter("@EMX_REF1_NUM", SqlDbType.VarChar, 20)
                        sqlpEMX_REF1_NUM.Value = dr("ReferenceNo1")
                        cmd.Parameters.Add(sqlpEMX_REF1_NUM)

                        Dim sqlpEMX_REF2 As New SqlParameter("@EMX_REF2", SqlDbType.VarChar, 50)
                        sqlpEMX_REF2.Value = dr("Reference2")
                        cmd.Parameters.Add(sqlpEMX_REF2)

                        Dim sqlpEMX_REF2_NUM As New SqlParameter("@EMX_REF2_NUM", SqlDbType.VarChar, 20)
                        sqlpEMX_REF2_NUM.Value = dr("ReferenceNo2")
                        cmd.Parameters.Add(sqlpEMX_REF2_NUM)

                        Dim sqlpEMX_REMARKS As New SqlParameter("@EMX_REMARKS", SqlDbType.VarChar, 1000)
                        sqlpEMX_REMARKS.Value = dr("Remarks")
                        cmd.Parameters.Add(sqlpEMX_REMARKS)

                        'Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                        'If ViewState("datamode") = "add" Then
                        '    sqlpbEdit.Value = False
                        'ElseIf ViewState("datamode") = "edit" Then
                        '    sqlpbEdit.Value = True
                        'End If
                        'cmd.Parameters.Add(sqlpbEdit)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    Else
                        'Delete Code will Come here
                        cmd = New SqlCommand("DELETEEMPEXPERIANCE_D", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMX_ID As New SqlParameter("@EMX_ID", SqlDbType.Int)
                        sqlpEMX_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpEMX_ID)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Protected Sub gvExperience_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvExperience.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowExpDetails(categoryID)
    End Sub

#End Region

#Region "Salary Details"

    Private Sub BindENR()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ' Dim str_Sql As String = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 1 order by Isnull(ERN_ORDER,100) "
        Dim str_Sql As String = "exec GetEarnTypesBSUWise '" & Session("sBSUID") & "',1"   'V2.1
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataBind()
        dr.Close()
    End Sub

    Private Function CreateSalaryDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cERN_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY As New DataColumn("Amount_ELIGIBILITY", System.Type.GetType("System.Decimal"))
            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cPAYTERM As New DataColumn("PAYTERM", System.Type.GetType("System.Int16"))
            Dim cPAY_SCHEDULE As New DataColumn("PAY_SCHEDULE", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cERN_DESCR)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cAmount_ELIGIBILITY)
            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cPAY_SCHEDULE)
            dtDt.Columns.Add(cPAYTERM)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnSalAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalAdd.Click
        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryDetailsTable()
        End If
        If Not IsNumeric(txtSalAmount.Text) Then
            lblError.Text = "Amount should be valid"
            Exit Sub
        End If
        Dim GrossSal As Double = 0
        'If txtGsalary.Text <> "" Then
        '    If Not Session("EMPSALGrossSalary") Is Nothing Then
        '        GrossSal = CDbl(Session("EMPSALGrossSalary"))
        '    End If
        '    If GrossSal + CDbl(txtSalAmount.Text) > CDbl(txtGsalary.Text) Then
        '        lblError.Text = "Total Amount should be less than Gross Salary"
        '        Exit Sub
        '    End If
        'End If
        Dim status As String = String.Empty
        Dim dtSalDetails As DataTable = Session("EMPSALDETAILS")
        Dim i As Integer
        'Dim gvRow As GridViewRow = gvEmpSalary.SelectedRow()

        For i = 0 To dtSalDetails.Rows.Count - 1
            If Session("EMPSalEditID") Is Nothing Or _
            Session("EMPSalEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                'If Session("EMPSalEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                If dtSalDetails.Rows(i)("STATUS").ToString.ToUpper <> "DELETED" _
                And dtSalDetails.Rows(i)("ENR_ID") = ddSalEarnCode.SelectedItem.Value Then
                    lblError.Text = "Cannot add transaction details.The Earn Code details are repeating."
                    GridBindSalaryDetails()
                    Exit Sub
                End If
            End If
        Next
        If btnSalAdd.Text = "Add" Then
            Dim newDR As DataRow = dtSalDetails.NewRow()
            newDR("UniqueID") = dtSalDetails.Rows.Count()
            newDR("ENR_ID") = ddSalEarnCode.SelectedValue
            newDR("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
            newDR("Amount") = AccountFunctions.Round(txtSalAmount.Text)
            newDR("Amount_ELIGIBILITY") = AccountFunctions.Round(txtSalAmtEligible.Text)
            newDR("bMonthly") = chkSalPayMonthly.Checked
            newDR("PAYTERM") = CInt(ddlayInstallment.SelectedValue)
            newDR("PAY_SCHEDULE") = GETPAY_SCHEDULE(CInt(ddlayInstallment.SelectedValue))
            newDR("Status") = "INSERT"
            status = "Insert"
            dtSalDetails.Rows.Add(newDR)
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            Dim amt As Double = CDbl(IIf(chkSalPayMonthly.Checked, txtSalAmount.Text * 12, txtSalAmount.Text))
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), amt)
        ElseIf btnSalAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPSalEditID")
            For iIndex = 0 To dtSalDetails.Rows.Count - 1
                If str_Search = dtSalDetails.Rows(iIndex)("UniqueID") And dtSalDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtSalDetails.Rows(iIndex)("ENR_ID") = ddSalEarnCode.SelectedValue
                    dtSalDetails.Rows(iIndex)("ERN_DESCR") = ddSalEarnCode.Text
                    dtSalDetails.Rows(iIndex)("Amount") = txtSalAmount.Text
                    dtSalDetails.Rows(iIndex)("Amount_ELIGIBILITY") = txtSalAmtEligible.Text
                    dtSalDetails.Rows(iIndex)("bMonthly") = chkSalPayMonthly.Checked
                    dtSalDetails.Rows(iIndex)("PAYTERM") = CInt(ddlayInstallment.SelectedValue)
                    dtSalDetails.Rows(iIndex)("PAY_SCHEDULE") = GETPAY_SCHEDULE(CInt(ddlayInstallment.SelectedValue))
                    dtSalDetails.Rows(iIndex)("status") = "edited"
                    status = "Edit/Update"
                    Exit For
                End If
            Next
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            Dim amt As Double = CDbl(IIf(chkSalPayMonthly.Checked, txtSalAmount.Text * 12, txtSalAmount.Text))
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), amt)
        End If
        If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwSalary) = 0 Then
            ClearEMPSalDetails()
        Else
            UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        End If

        Session("EMPSALDETAILS") = dtSalDetails
        GridBindSalaryDetails()
    End Sub

    Private Sub GridBindSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPSALDETAILS") Is Nothing Then Return
        If Session("EMPSALDETAILS") Is Nothing Then
            gvEmpSalary.DataSource = Nothing
            gvEmpSalary.DataBind()
            Return
        End If
        Dim i As Integer
        Dim grossSal As Double
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateSalaryDetailsTable()
        If Session("EMPSALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPSALDETAILS").Rows.Count - 1
                If (Session("EMPSALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPSALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("EMPSALDETAILS").Rows(i)(strColumnName)
                            If Not Session("EMPSALDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("EMPSALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        Session("EMPSALGrossSalary") = grossSal
        gvEmpSalary.DataSource = dtTempDtl
        gvEmpSalary.DataBind()
        txtGsalary.Text = Session("EMPSALGrossSalary")
    End Sub

    Private Sub DeleteRowSalDetails(ByVal pId As String)
        Dim lblTid As New Label
        lblTid = TryCast(gvEmpSalary.Rows(pId).FindControl("lblId"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        Dim iRemove As Integer = 0
        For iRemove = 0 To Session("EMPSALDETAILS").Rows.Count - 1
            If (Session("EMPSALDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPSALDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwSalary) = 0 Then
                    GridBindSalaryDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Salary Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnSalCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalCancel.Click
        ClearEMPSalDetails()
    End Sub

    Sub ClearEMPSalDetails()
        ddSalEarnCode.SelectedIndex = 0
        txtSalAmount.Text = ""
        txtSalAmtEligible.Text = ""
        btnSalAdd.Text = "Add"
        lblError.Text = ""
        chkSalPayMonthly.Checked = True
        ddlayInstallment.SelectedValue = 0
        Session.Remove("EMPSalEditID")
        txtBname.Enabled = True
        btnBank_name.Enabled = True
        txtAccCode.Enabled = True
    End Sub

    Private Sub GetPayMonth_YearDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT BSU_PAYMONTH, BSU_PAYYEAR FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            ViewState("BSU_PAYMONTH") = dr("BSU_PAYMONTH")
            ViewState("BSU_PAYYEAR") = dr("BSU_PAYYEAR")
        End While
        dr.Close()
    End Sub

    Protected Sub lnkSalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1
            If str_Search = Session("EMPSALDETAILS").Rows(iIndex)("UniqueID") And Session("EMPSALDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                ddSalEarnCode.SelectedIndex = -1
                ddSalEarnCode.Items.FindByValue(Session("EMPSALDETAILS").Rows(iIndex)("ENR_ID")).Selected = True
                txtSalAmount.Text = Session("EMPSALDETAILS").Rows(iIndex)("Amount")
                txtSalAmtEligible.Text = Session("EMPSALDETAILS").Rows(iIndex)("Amount_ELIGIBILITY")
                chkSalPayMonthly.Checked = Session("EMPSALDETAILS").Rows(iIndex)("bMonthly")
                ddlayInstallment.SelectedIndex = -1
                ddlayInstallment.Items.FindByValue(Session("EMPSALDETAILS").Rows(iIndex)("PAYTERM")).Selected = True

                Session("EMPSALGrossSalary") -= CDbl(txtSalAmount.Text)
                gvEmpSalary.SelectedIndex = iIndex
                btnSalAdd.Text = "Save"
                Session("EMPSalEditID") = str_Search
                UtilityObj.beforeLoopingControls(vwSalary)
                Exit For
            End If
        Next
    End Sub

    Private Function SaveEMPSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal addMode As Boolean, ByVal EmpID As Integer) As Integer
        If Not AllowSalaryEdit(EmpID) Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Try
            If Session("EMPSALDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPSALDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1

                    Dim dr As DataRow = Session("EMPSALDETAILS").Rows(iIndex)

                    'If (Session("EMPSALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                    If dr("status") <> "LOADED" Then
                        cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                        sqlpESH_EMP_ID.Value = EmpID 'EmployeeID
                        cmd.Parameters.Add(sqlpESH_EMP_ID)

                        If dr("status") = "edited" Or dr("status") = "DELETED" Then
                            Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.Int)
                            sqlpESH_ID.Value = dr("UniqueID")
                            cmd.Parameters.Add(sqlpESH_ID)
                        End If

                        Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                        sqlpUser.Value = Session("sUsr_name")
                        cmd.Parameters.Add(sqlpUser)

                        Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                        sqlpESH_BSU_ID.Value = Session("sBSUID")
                        cmd.Parameters.Add(sqlpESH_BSU_ID)

                        Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                        sqlpESH_ERN_ID.Value = dr("ENR_ID")
                        cmd.Parameters.Add(sqlpESH_ERN_ID)

                        Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                        sqlpESH_AMOUNT.Value = dr("Amount")
                        cmd.Parameters.Add(sqlpESH_AMOUNT)

                        Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                        sqlpESH_ELIGIBILITY.Value = dr("Amount_ELIGIBILITY")
                        cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                        Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                        sqlpESH_bMonthly.Value = dr("bMonthly")
                        cmd.Parameters.Add(sqlpESH_bMonthly)

                        Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                        sqlpESH_PAYTERM.Value = dr("PAYTERM")
                        cmd.Parameters.Add(sqlpESH_PAYTERM)

                        Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                        sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
                        cmd.Parameters.Add(sqlpESH_CUR_ID)

                        Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                        sqlpESH_PAYMONTH.Value = ViewState("BSU_PAYMONTH")
                        cmd.Parameters.Add(sqlpESH_PAYMONTH)

                        Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                        sqlpESH_PAYYEAR.Value = ViewState("BSU_PAYYEAR")
                        cmd.Parameters.Add(sqlpESH_PAYYEAR)

                        Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                        sqlpESH_PAY_CUR_ID.Value = ddlPayC.SelectedItem.Value
                        cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                        Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                        sqlpESH_FROMDT.Value = CDate(txtJdate.Text)
                        cmd.Parameters.Add(sqlpESH_FROMDT)

                        Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                        sqlpESH_TODT.Value = DBNull.Value
                        cmd.Parameters.Add(sqlpESH_TODT)

                        Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)

                        If (Session("EMPSALDETAILS").Rows(iIndex)("Status") <> "DELETED") Then
                            sqlpbDeleted.Value = 0
                        Else
                            sqlpbDeleted.Value = 1
                        End If
                        cmd.Parameters.Add(sqlpbDeleted)

                        Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                        retNewESH_ID.Direction = ParameterDirection.Output
                        cmd.Parameters.Add(retNewESH_ID)

                        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retSValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retSValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retSValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        Else
                            'Set the UniqueID to the ESH_ID that is returned....
                            If Not Session("EMPSALSCHEDULE") Is Nothing Then
                                If Session("EMPSALSCHEDULE").Rows.Count > 0 Then
                                    For index As Integer = 0 To Session("EMPSALSCHEDULE").Rows.Count - 1
                                        Dim drow As DataRow = Session("EMPSALSCHEDULE").Rows(index)
                                        If drow("ENR_ID") = dr("ENR_ID") Then
                                            drow("ESH_ID") = retNewESH_ID.Value
                                        End If
                                    Next
                                End If
                            End If

                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Private Function AllowSalaryEdit(ByVal EMP_ID As Integer) As Boolean
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Code commented for V1.1--  moving to SP

            ' Dim str_Sql As String = "select count(*) from EMPSALARYDATA_D WHERE ESD_EMP_ID = '" & EMP_ID & _
            '"' AND (ESD_PAID = 1 OR ESD_bPOSTED = 1)"
            ' Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
            ' If count <= 0 Then
            '     Return True
            ' Else
            '     Return False
            ' End If

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@empID", EMP_ID)


            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue


            Dim returnValue As Integer
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "EMP_IsSalaryEditAllowed", pParms)
            returnValue = pParms(1).Value

            If returnValue = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Protected Sub gvEmpSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpSalary.RowDataBound
        Try
            Dim cmdCol As Integer = gvEmpSalary.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblid As New Label
            Dim lblamt As New Label
            Dim lblENRID As New Label
            Dim lblSchedule As New Label
            Dim lblPayTerm As New Label
            Dim lblBMonthly As New Label
            lblid = e.Row.FindControl("lblId")
            lblBMonthly = e.Row.FindControl("lblBMonthly")
            lblSchedule = e.Row.FindControl("lblSchedule")
            'lblamt = AccountFunctions.Round(e.Row.FindControl("lblAmt"))
            lblamt = e.Row.FindControl("lblAmt")
            lblENRID = e.Row.FindControl("lblENRID")
            lblPayTerm = e.Row.FindControl("lblPayTerm")
            If lblSchedule IsNot Nothing Then
                Select Case lblPayTerm.Text
                    Case 0
                        lblSchedule.Text = "Monthly"
                    Case 1
                        lblSchedule.Text = "Bimonthly"
                    Case 2
                        lblSchedule.Text = "Quarterly"
                    Case 3
                        lblSchedule.Text = "Half-yearly"
                    Case 4
                        lblSchedule.Text = "Yearly"
                End Select
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'Return 1000
        End Try
    End Sub

    Protected Sub gvEmpSalary_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEmpSalary.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowSalDetails(categoryID)
    End Sub

#End Region


#Region "Server Validations"

    Protected Sub cvIss_date_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvIss_date.ServerValidate
        Dim sflag As Boolean = False
        Dim datetime2 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            datetime2 = Date.ParseExact(txtViss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(datetime2) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvExp_date_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvExp_date.ServerValidate
        'Dim sflag As Boolean = False
        Dim DateTime2 As Date
        Dim dateTime1 As Date
        Try
            DateTime2 = Date.ParseExact(txtVExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            dateTime1 = Date.ParseExact(txtViss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(DateTime2) Then

                If DateTime2 <= dateTime1 Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvProb_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvProb.ServerValidate
        Dim sflag As Boolean = False
        Dim datetime2 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            datetime2 = Date.ParseExact(txtProb.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(datetime2) Then
                sflag = True
            End If
            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvLrejoining_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvLrejoining.ServerValidate
        Dim sflag As Boolean = False
        Dim datetime2 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            datetime2 = Date.ParseExact(txtLRdate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(datetime2) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try
    End Sub

    Protected Sub ServerValidate()
        lstrErrMsg = ""
        If Trim(txtFname1.Text) = "" Then
            lblError.Text = "Employee First Name should not be blank"
            'ElseIf Trim(txtMname1.Text) = "" Then
            '    lblError.Text = "Employee Middle Name should not be blank"
            'ElseIf Trim(txtLname1.Text) = "" Then
            '    lblError.Text = "Employee Last Name should not be blank"
            'ElseIf Trim(txtFather.Text) = "" Then
            '    lblError.Text = "Employee Father's Name should not be blank"
            'ElseIf Trim(txtMother.Text) = "" Then
            '    lblError.Text = "Employee Mother's Name should not be blank"
        ElseIf Trim(txtPassNo.Text) = "" Then
            lblError.Text = "Employee Passport No should not be blank"
        ElseIf Trim(txtJdate.Text) = "" Then
            lblError.Text = "Employee Joining Date should not be blank"
        ElseIf Trim(txtDOJGroup.Text) = "" Then
            lblError.Text = "Employee Joining Date should not be blank"
            'ElseIf Trim(txtIU.Text) = "" Then
            '    lblError.Text = "Employee Visa Issued Unit should not be blank"
            'ElseIf Trim(txtML.Text) = "" Then
            '    lblError.Text = "Employee MOL Profession should not be blank"
            'ElseIf Trim(txtME.Text) = "" Then
            '    lblError.Text = "Employee MOE Profession should not be blank"
            'ElseIf Trim(txtGrade.Text) = "" Then
            '    lblError.Text = "Employee Grade should not be blank"
            'ElseIf Trim(txtWU.Text) = "" Then
            '    lblError.Text = "Employee work Unit should not be blank"
            'ElseIf Trim(txtSD.Text) = "" Then
            '    lblError.Text = "Employee Designation should not be blank"
        ElseIf Trim(txtCat.Text) = "" Then
            lblError.Text = "Employee Category should not be blank"
            'ElseIf Trim(txtTGrade.Text) = "" Then
            '    lblError.Text = "Employee Teaching Grade should not be blank"
        ElseIf Trim(txtDept.Text) = "" Then
            lblError.Text = "Employee Department should not be blank"
        ElseIf Trim(txtCountry.Text) = "" Then
            lblError.Text = "Nationality should not be blank"
            'ElseIf Trim(txtQual.Text) = "" Then
            '    lblError.Text = "Employee Qualification should not be blank"
            'ElseIf Trim(txtReport.Text) = "" Then
            '    lblError.Text = "'Employee Reporting To' should not be blank"
            'ElseIf Trim(txtSalGrade.Text) = "" Then
            '    lblError.Text = "Employee Salary Grade should not be blank"
            'ElseIf Trim(txtEmpApprovalPol.Text) = "" Then
            '    lblError.Text = "Employee Approval Policy should not be blank"
        ElseIf Trim(txtEMPDOB.Text) = "" Then
            lblError.Text = "Employee DOB should not be blank"
            'ElseIf Trim(txtVacationPolicy.Text) = "" Then
            '    lblError.Text = "Vacation Policy should not be blank"
        ElseIf Trim(txtFTE.Text) = "" Then
            lblError.Text = "FTE should not be blank"
        ElseIf Not IsNumeric(txtFTE.Text) Then
            lblError.Text = "Enter a valid FTE"
        ElseIf CDbl(txtFTE.Text) > 1 Or CDbl(txtFTE.Text) < 0 Then
            lblError.Text = "FTE should be between 0 and 1"
        End If

        If Session("BSU_COUNTRY_ID") = 6 Then
            If Trim(txt_AadharCardNo.Text) = "" Then
                lblError.Text = "Aadhar Card No should not be blank"
            ElseIf Trim(txt_PanCardNo.Text) = "" Then
                lblError.Text = "Pan Card No should not be blank"
            End If
        End If
        ' ValidateAirTicket() -- as per Robbie's request
        '  ValidateDocuments() -- moving validation to Database as per Prem's request
        If lblError.Text = "" Then
            ValidateDocumentsinDB()
        End If

        'ValidateSalaryDetails()
        'If (Trim(txtFname1.Text) = "") Or (Trim(txtMname1.Text) = "") Or (Trim(txtLname1.Text) = "") Or (Trim(txtFather.Text) = "") Or (Trim(txtMother.Text) = "") Or (Trim(txtPassNo.Text) = "") Then

        '    lstrErrMsg = Trim(lstrErrMsg) & "Some of the mandatory fields marked with (*) is empty" & "<br>"
        'ElseIf (Trim(txtProb.Text) = "") Or (Trim(txtVExp_date.Text) = "") Or (Trim(txtViss_date.Text) = "") Or (Trim(txtJdate.Text) = "") Then
        '    lstrErrMsg = Trim(lstrErrMsg) & "Some of the mandatory fields marked with (*) is empty" & "<br>"
        'ElseIf (Trim(txtIU.Text) = "") Or (Trim(txtML.Text) = "") Or (Trim(txtWU.Text) = "") Or (Trim(txtSD.Text) = "") Or (Trim(txtCat.Text) = "") Or (Trim(txtCountry.Text) = "") Or (Trim(txtQual.Text) = "") Or (Trim(txtReport.Text) = "") Then
        '    lstrErrMsg = Trim(lstrErrMsg) & "Some of the mandatory fields marked with (*) is empty" & "<br>"
        'End If

        If txtStaffNo.Text <> "" And (IsNumeric(Trim(txtStaffNo.Text)) = False) Then
            lstrErrMsg = lstrErrMsg & "Staff No Should Be A Numeric Value" & "<br>"
        End If

        'If txtEmpno1.Text <> "" And (IsNumeric(Trim(txtEmpno1.Text)) = False) Then
        '    lstrErrMsg = lstrErrMsg & "Employee No Should Be A Numeric Value" & "<br>"
        'End If

        'If (Trim(txtGsalary.Text) = "") Then
        '    lstrErrMsg = lstrErrMsg & "Gross Salary Should Be A Numeric Value" & "<br>"
        'End If
        'If Not IsNumeric(txtGsalary.Text) Then
        '    lblError.Text = "Please enter Salary details"
        'End If

        If ddlPayMode.SelectedValue = 1 And (txtBname.Text = "" Or txtAccCode.Text = "") Then
            lstrErrMsg += " Bank Name and Account No should not be blank "
        End If
        lstrErrMsg = lblError.Text
    End Sub

    Private Sub ValidateAirTicket()
        If ddlAirTicketAvailable.SelectedIndex <> 0 Then
            If Trim(txtAirticketCity.Text) = "" Then
                lblError.Text = "Air Ticket City should not be blank"
            ElseIf Trim(txtAirTicketNo.Text) = "" Then
                lblError.Text = "Air ticket No should not be blank"
            End If

        End If
    End Sub


    Private Sub ValidateSalaryDetails()
        If ddlPayMode.SelectedValue = 0 Then
            If Trim(txtBname.Text) = "" Then
                lblError.Text = "Bank Name should not be blank"
                Exit Sub
            ElseIf Trim(txtAccCode.Text) = "" Then
                lblError.Text = "Account No should not be blank"
                Exit Sub
            ElseIf Trim(txtAirticketCity.Text) = "" Then
                lblError.Text = "Swift Code should not be blank"
                Exit Sub
            End If
        End If
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        If (Session("EMPSALDETAILS") IsNot Nothing) Then
            If (Session("EMPSALDETAILS").Rows.Count > 0) Then
                For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPSALDETAILS").Rows(iIndex)
                    If (Session("EMPSALDETAILS").Rows(iIndex)("STATUS").ToString.ToUpper() <> "DELETED") And (Session("EMPSALDETAILS").Rows(iIndex)("ENR_ID") = "BASIC") Then
                        Exit Sub
                    End If
                Next
                lblError.Text = "Basic Salary for the Employee is not entered"
            End If
        Else
            lblError.Text = "Please Enter Salary Details"
        End If
    End Sub

    Private Function ValidateValue(ByVal val As String, ByVal isNumeric As Boolean) As Object
        If val = "" Or val = String.Empty Then
            Return DBNull.Value
        ElseIf isNumeric Then
            Return CInt(val)
        End If
        Return DBNull.Value
    End Function

#End Region

#Region "Dependance Details"

    Protected Sub btndependanceCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndependanceCancel.Click
        clearDependanciesDetails()
    End Sub

    Private Sub clearDependanciesDetails()
        txtDependDOB.Text = ""
        txtDependName.Text = ""
        Me.txtEIDExpryDT.Text = ""
        Me.txtEIDNo.Text = ""
        Me.txtDepCountry.Text = ""
        Me.txtDepPassportNo.Text = ""
        Me.txtDepUidno.Text = ""
        Me.txtDepVexpdate.Text = ""
        Me.txtDepVissuedate.Text = ""
        Me.txtDepVissueplace.Text = ""
        hfDepCountry_ID.Value = 0
        ViewState("imgDependant") = Nothing
        'txtDependNoTickets.Text = ""
        ddlDependRelation.SelectedValue = 0
        ChkDependConcession.Checked = False
    End Sub

    Private Function CreateDependanceDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cEDD_NAME As New DataColumn("EDD_NAME", System.Type.GetType("System.String"))
            Dim cEDD_RELATION As New DataColumn("EDD_RELATION", System.Type.GetType("System.String"))
            Dim cEDD_DOB As New DataColumn("EDD_DOB", System.Type.GetType("System.String"))
            Dim cEDD_NoofTicket As New DataColumn("EDD_NoofTicket", System.Type.GetType("System.Double"))
            Dim cbConcession As New DataColumn("bConcession", System.Type.GetType("System.Boolean"))
            Dim cEDD_STU_ID As New DataColumn("EDD_STU_ID", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cInsuranceCategory As New DataColumn("EDD_Insu_id", System.Type.GetType("System.String"))
            Dim cInsuranceCategoryDESC As New DataColumn("InsuranceCategoryDESC", System.Type.GetType("System.String"))
            Dim cCompanyPaidInsurance As New DataColumn("EDD_bCompanyInsurance", System.Type.GetType("System.Boolean"))
            Dim cGemsType As New DataColumn("EDD_GEMS_TYPE", System.Type.GetType("System.String"))
            Dim cGemsTypeDESC As New DataColumn("EDD_GEMS_TYPE_DESCR", System.Type.GetType("System.String"))
            Dim cEDD_GEMS_ID As New DataColumn("EDD_GEMS_ID", System.Type.GetType("System.String"))

            Dim cEDD_BSU_ID As New DataColumn("EDD_BSU_ID", System.Type.GetType("System.String"))
            Dim cEDD_BSU_NAME As New DataColumn("EDD_BSU_NAME", System.Type.GetType("System.String"))

            Dim cEmiratesIDNo As New DataColumn("EDC_DOCUMENT_NO", System.Type.GetType("System.String"))
            Dim cEmiratesIDExpiry As New DataColumn("EDC_EXP_DT", System.Type.GetType("System.String"))
            Dim cSEXbMALE As New DataColumn("EDD_bMALE", System.Type.GetType("System.Boolean"))
            Dim cGender As New DataColumn("EDC_Gender", System.Type.GetType("System.String"))
            Dim cMartialStatus As New DataColumn("EDD_MSTATUS", System.Type.GetType("System.String"))
            Dim cMartialStatusID As New DataColumn("EDD_MARITALSTATUS", System.Type.GetType("System.String")) '
            Dim cNationality As New DataColumn("CTY_NATIONALITY", System.Type.GetType("System.String"))
            Dim cCountryID As New DataColumn("EDD_CTY_ID", System.Type.GetType("System.String"))
            Dim cPhoto As New DataColumn("EDD_bPHOTO", System.Type.GetType("System.Boolean"))
            Dim cImageByte As New DataColumn("EDD_PHOTO", System.Type.GetType("System.Byte[]"))
            Dim cPassportNo As New DataColumn("EDD_PASSPRTNO", System.Type.GetType("System.String"))
            Dim cUIDNo As New DataColumn("EDD_UIDNO", System.Type.GetType("System.String"))
            Dim cVisaissueDate As New DataColumn("EDD_VISAISSUEDATE", System.Type.GetType("System.String"))
            Dim cVisaexpDate As New DataColumn("EDD_VISAEXPDATE", System.Type.GetType("System.String"))
            Dim cVisaissuePlace As New DataColumn("EDD_VISAISSUEPLACE", System.Type.GetType("System.String"))
            cImageByte.AllowDBNull = True

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEDD_NAME)
            dtDt.Columns.Add(cEDD_RELATION)
            dtDt.Columns.Add(cEDD_DOB)
            dtDt.Columns.Add(cEDD_NoofTicket)
            dtDt.Columns.Add(cbConcession)
            dtDt.Columns.Add(cEDD_STU_ID)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cInsuranceCategory)
            dtDt.Columns.Add(cInsuranceCategoryDESC)
            dtDt.Columns.Add(cCompanyPaidInsurance)
            dtDt.Columns.Add(cGemsType)
            dtDt.Columns.Add(cGemsTypeDESC)
            dtDt.Columns.Add(cEDD_GEMS_ID)

            dtDt.Columns.Add(cEDD_BSU_ID)
            dtDt.Columns.Add(cEDD_BSU_NAME)
            dtDt.Columns.Add(cEmiratesIDNo)
            dtDt.Columns.Add(cEmiratesIDExpiry)
            dtDt.Columns.Add(cMartialStatus)
            dtDt.Columns.Add(cGender)
            dtDt.Columns.Add(cMartialStatusID)
            dtDt.Columns.Add(cSEXbMALE)
            dtDt.Columns.Add(cNationality)
            dtDt.Columns.Add(cPhoto)
            dtDt.Columns.Add(cImageByte)
            dtDt.Columns.Add(cCountryID)
            dtDt.Columns.Add(cPassportNo)
            dtDt.Columns.Add(cUIDNo)
            dtDt.Columns.Add(cVisaissueDate)
            dtDt.Columns.Add(cVisaexpDate)
            dtDt.Columns.Add(cVisaissuePlace)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Private Sub GridBindDependanceDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPSALDETAILS") Is Nothing Then Return
        If Session("EMPDEPENDANCEDETAILS") Is Nothing Then
            gvDependancedetails.DataSource = Nothing
            gvDependancedetails.DataBind()
            Return
        End If

        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateDependanceDetailsTable()
        If Session("EMPDEPENDANCEDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPDEPENDANCEDETAILS").Rows.Count - 1
                If (Session("EMPDEPENDANCEDETAILS").Rows(i)("Status").ToString.ToUpper <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPDEPENDANCEDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If strColumnName = "EDD_RELATION" Then
                            ldrTempNew.Item(strColumnName) = ddlDependRelation.Items(Session("EMPDEPENDANCEDETAILS").Rows(i)(strColumnName)).Text
                        Else
                            ldrTempNew.Item(strColumnName) = Session("EMPDEPENDANCEDETAILS").Rows(i)(strColumnName)
                        End If
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvDependancedetails.DataSource = dtTempDtl
        gvDependancedetails.DataBind()
    End Sub

    Private Sub DeleteRowDependanceDetails(ByVal pId As String)

        Dim lblTid As New Label
        lblTid = TryCast(gvDependancedetails.Rows(pId).FindControl("lblId"), Label)

        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub

        Dim iRemove As Integer = 0
        For iRemove = 0 To Session("EMPDEPENDANCEDETAILS").Rows.Count - 1
            If (Session("EMPDEPENDANCEDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPDEPENDANCEDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwSalary) = 0 Then
                    GridBindDependanceDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Salary Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Private Function SaveDependanceDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPDEPENDANCEDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPDEPENDANCEDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPDEPENDANCEDETAILS").Rows.Count - 1

                    Dim dr As DataRow = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)

                    'If (Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("Status") <> "DELETE") Then 'changed EDIT to DELETE
                    cmd = New SqlCommand("SaveEMPDEPENDANTS_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    If (Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("Status") <> "INSERT") Then
                        Dim sqlpEDD_ID As New SqlParameter("@EDD_ID", SqlDbType.Int)
                        sqlpEDD_ID.Value = dr("UniqueID") 'EmployeeID
                        cmd.Parameters.Add(sqlpEDD_ID)
                    End If

                    Dim sqlpESL_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
                    sqlpESL_EMP_ID.Value = EmpID 'EmployeeID
                    cmd.Parameters.Add(sqlpESL_EMP_ID)

                    Dim sqlpEDD_NAME As New SqlParameter("@EDD_NAME", SqlDbType.VarChar, 200)
                    sqlpEDD_NAME.Value = dr("EDD_NAME")
                    cmd.Parameters.Add(sqlpEDD_NAME)

                    Dim sqlpEDD_RELATION As New SqlParameter("@EDD_RELATION", SqlDbType.VarChar, 30)
                    sqlpEDD_RELATION.Value = dr("EDD_RELATION")
                    cmd.Parameters.Add(sqlpEDD_RELATION)

                    Dim sqlpEDD_DOB As New SqlParameter("@EDD_DOB", SqlDbType.VarChar, 20)
                    If (Convert.ToString(dr("EDD_DOB")) = "") Then
                        sqlpEDD_DOB.Value = DBNull.Value
                    Else
                        sqlpEDD_DOB.Value = dr("EDD_DOB")
                    End If
                    'sqlpEDD_DOB.Value = IIf(dr("EDD_DOB") = "", DBNull.Value, dr("EDD_DOB"))
                    cmd.Parameters.Add(sqlpEDD_DOB)

                    Dim sqlpEDD_bCONCESSION As New SqlParameter("@EDD_bCONCESSION", SqlDbType.Bit)
                    sqlpEDD_bCONCESSION.Value = dr("bConcession")
                    cmd.Parameters.Add(sqlpEDD_bCONCESSION)

                    Dim sqlpEDD_NoofTicket As New SqlParameter("@EDD_NoofTicket", SqlDbType.TinyInt)
                    sqlpEDD_NoofTicket.Value = dr("EDD_NoofTicket")
                    cmd.Parameters.Add(sqlpEDD_NoofTicket)

                    'Dim sqlpESL_CUR_ID As New SqlParameter("@ESL_CUR_ID", SqlDbType.VarChar, 6)
                    'sqlpESL_CUR_ID.Value = ddlSalC.SelectedValue
                    'cmd.Parameters.Add(sqlpESL_CUR_ID)

                    Dim sqlpEDD_STU_ID As New SqlParameter("@EDD_STU_ID", SqlDbType.Int)
                    sqlpEDD_STU_ID.Value = dr("EDD_STU_ID")
                    cmd.Parameters.Add(sqlpEDD_STU_ID)

                    Dim sqlpEDD_Insu_id As New SqlParameter("@EDD_Insu_id", SqlDbType.Int)
                    sqlpEDD_Insu_id.Value = dr("EDD_Insu_id")
                    cmd.Parameters.Add(sqlpEDD_Insu_id)

                    Dim sqlpEDD_bCompanyInsurance As New SqlParameter("@EDD_bCompanyInsurance", SqlDbType.Bit)
                    sqlpEDD_bCompanyInsurance.Value = dr("EDD_bCompanyInsurance")
                    cmd.Parameters.Add(sqlpEDD_bCompanyInsurance)

                    Dim sqlpEDD_GEMSTYPE As New SqlParameter("@EDD_GEMS_TYPE", SqlDbType.VarChar)
                    sqlpEDD_GEMSTYPE.Value = dr("EDD_GEMS_TYPE")
                    cmd.Parameters.Add(sqlpEDD_GEMSTYPE)


                    Dim sqlpEDD_GEMS_ID As New SqlParameter("@EDD_GEMS_ID", SqlDbType.VarChar)
                    sqlpEDD_GEMS_ID.Value = dr("EDD_GEMS_ID")
                    cmd.Parameters.Add(sqlpEDD_GEMS_ID)

                    Dim sqlpEDD_BSU_ID As New SqlParameter("@EDD_BSU_ID", SqlDbType.VarChar)
                    sqlpEDD_BSU_ID.Value = dr("EDD_BSU_ID")
                    cmd.Parameters.Add(sqlpEDD_BSU_ID)

                    Dim sqlpEDD_EMIDNO As New SqlParameter("@EDC_DOCUMENT_NO", SqlDbType.VarChar)
                    sqlpEDD_EMIDNO.Value = dr("EDC_DOCUMENT_NO")
                    cmd.Parameters.Add(sqlpEDD_EMIDNO)

                    Dim sqlpEDD_EMIDEXPDT As New SqlParameter("@EDC_EXP_DT", SqlDbType.VarChar)
                    sqlpEDD_EMIDEXPDT.Value = dr("EDC_EXP_DT")
                    cmd.Parameters.Add(sqlpEDD_EMIDEXPDT)

                    Dim sqlpEDD_SEX_bMALE As New SqlParameter("@EDD_bMALE", SqlDbType.Bit)
                    sqlpEDD_SEX_bMALE.Value = dr("EDD_bMALE")
                    cmd.Parameters.Add(sqlpEDD_SEX_bMALE)

                    Dim sqlpEDD_MARTIALSTATUS As New SqlParameter("@EDD_MARITALSTATUS", SqlDbType.Int)
                    sqlpEDD_MARTIALSTATUS.Value = dr("EDD_MARITALSTATUS")
                    cmd.Parameters.Add(sqlpEDD_MARTIALSTATUS)

                    Dim sqlpEDC_PHOTO As New SqlParameter("@EDD_PHOTO", SqlDbType.Image)
                    sqlpEDC_PHOTO.Value = dr("EDD_PHOTO")
                    cmd.Parameters.Add(sqlpEDC_PHOTO)

                    Dim sqlpStatus As New SqlParameter("@Action", SqlDbType.VarChar)
                    sqlpStatus.Value = dr("Status")
                    cmd.Parameters.Add(sqlpStatus)

                    Dim sqlpCITY As New SqlParameter("@EDD_CTY_ID", SqlDbType.Int)
                    sqlpCITY.Value = dr("EDD_CTY_ID")
                    cmd.Parameters.Add(sqlpCITY)

                    Dim sqlpPASSPRTNO As New SqlParameter("@EDD_PASSPRTNO", SqlDbType.VarChar)
                    sqlpPASSPRTNO.Value = dr("EDD_PASSPRTNO")
                    cmd.Parameters.Add(sqlpPASSPRTNO)

                    Dim sqlpUIDNO As New SqlParameter("@EDD_UIDNO", SqlDbType.VarChar)
                    sqlpUIDNO.Value = dr("EDD_UIDNO")
                    cmd.Parameters.Add(sqlpUIDNO)

                    Dim sqlpVISAISSUEDATE As New SqlParameter("@EDD_VISAISSUEDATE", SqlDbType.VarChar)
                    sqlpVISAISSUEDATE.Value = dr("EDD_VISAISSUEDATE")
                    cmd.Parameters.Add(sqlpVISAISSUEDATE)

                    Dim sqlpVISAEXPDATE As New SqlParameter("@EDD_VISAEXPDATE", SqlDbType.VarChar)
                    sqlpVISAEXPDATE.Value = dr("EDD_VISAEXPDATE")
                    cmd.Parameters.Add(sqlpVISAEXPDATE)

                    Dim sqlpVISAISSUEPLACE As New SqlParameter("@EDD_VISAISSUEPLACE", SqlDbType.VarChar)
                    sqlpVISAISSUEPLACE.Value = dr("EDD_VISAISSUEPLACE")
                    cmd.Parameters.Add(sqlpVISAISSUEPLACE)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        Exit For
                        'End If
                        ' Else
                        ''Delete Code will Come here
                        'cmd = New SqlCommand("DELETEEMPSALARY_S", objConn, stTrans)
                        'cmd.CommandType = CommandType.StoredProcedure

                        'Dim sqlpESL_ID As New SqlParameter("@ESL_ID", SqlDbType.Int)
                        'sqlpESL_ID.Value = dr("UniqueID")
                        'cmd.Parameters.Add(sqlpESL_ID)
                        'Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        'retValParam.Direction = ParameterDirection.ReturnValue
                        'cmd.Parameters.Add(retValParam)

                        'cmd.ExecuteNonQuery()
                        'iReturnvalue = retValParam.Value
                        'If iReturnvalue <> 0 Then
                        '    Exit For
                        'End If

                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Protected Sub btnDependanceAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDependanceAdd.Click

        If ddlDependRelation.SelectedItem.Text = "Other" Then
            lblError.Text = "Please Select Dependance Relation"
            Exit Sub

        End If

        If Session("EMPDEPENDANCEDETAILS") Is Nothing Then
            Session("EMPDEPENDANCEDETAILS") = CreateDependanceDetailsTable()
        End If
        Dim status As String = String.Empty
        Dim dtDependanceDetails As DataTable = Session("EMPDEPENDANCEDETAILS")
        Dim i As Integer
        For i = 0 To dtDependanceDetails.Rows.Count - 1
            'If Me.FUUploadDependance.PostedFile.FileName.Trim <> "" Then
            '    ConvertImageFiletoBytes(Me.FUUploadDependance.PostedFile.FileName)
            'Else
            '    'ViewState("imgDependant")
            'End If
            If Session("EMPDEPENDANCEEditID") Is Nothing Or _
            Session("EMPDEPENDANCEEditID") <> dtDependanceDetails.Rows(i)("UniqueID") Then
                'If Session("EMPSalEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                If dtDependanceDetails.Rows(i)("STATUS").ToString.ToUpper <> "DELETED" And _
                dtDependanceDetails.Rows(i)("EDD_NAME").ToString = txtDependName.Text And _
                dtDependanceDetails.Rows(i)("EDD_RELATION").ToString = ddlDependRelation.SelectedItem.Text And _
                dtDependanceDetails.Rows(i)("EDD_DOB").ToString = txtDependDOB.Text And _
                dtDependanceDetails.Rows(i)("EDD_NoofTicket").ToString = txtAirTicketNo.Text And _
                dtDependanceDetails.Rows(i)("EDD_STU_ID").ToString = txtStudID.Text And _
                dtDependanceDetails.Rows(i)("EDC_DOCUMENT_NO").ToString = Me.txtEIDNo.Text And _
                dtDependanceDetails.Rows(i)("EDC_EXP_DT").ToString = Me.txtEIDExpryDT.Text And _
 dtDependanceDetails.Rows(i)("EDD_PASSPRTNO").ToString = Me.txtDepPassportNo.Text And _
  dtDependanceDetails.Rows(i)("EDD_UIDNO").ToString = Me.txtDepUidno.Text And _
  dtDependanceDetails.Rows(i)("EDD_VISAISSUEDATE").ToString = Me.txtDepVissuedate.Text And _
  dtDependanceDetails.Rows(i)("EDD_VISAEXPDATE").ToString = Me.txtDepVexpdate.Text And _
  dtDependanceDetails.Rows(i)("EDD_VISAISSUEPLACE").ToString = Me.txtDepVissueplace.Text And _
                Convert.ToBoolean(dtDependanceDetails.Rows(i)("EDD_bMALE")) = rdDepdMale.Checked And _
                dtDependanceDetails.Rows(i)("EDD_MARITALSTATUS") = Me.ddlDependMstatus.SelectedValue And _
                dtDependanceDetails.Rows(i)("EDD_bPhoto") = Me.FUUploadDependance.HasFile And _
                dtDependanceDetails.Rows(i)("CTY_NATIONALITY") = Me.txtDepCountry.Text And _
                dtDependanceDetails.Rows(i)("bConcession").ToString = ChkDependConcession.Checked Then
                    lblError.Text = "Cannot add details.The details are repeating."
                    GridBindDependanceDetails()
                    Exit Sub
                End If
            End If
        Next
        If btnDependanceAdd.Text = "Add" Then
            Dim newDR As DataRow = dtDependanceDetails.NewRow()
            newDR("UniqueID") = dtDependanceDetails.Rows.Count()
            newDR("EDD_NAME") = txtDependName.Text
            newDR("EDD_RELATION") = ddlDependRelation.SelectedValue
            newDR("EDD_DOB") = txtDependDOB.Text
            newDR("EDD_NoofTicket") = CDbl(txtAirTicketNo.Text) ' CDbl(txtDependNoTickets.Text)
            newDR("EDD_STU_ID") = 0 'CInt(txtDependNoTickets.Text)
            newDR("bConcession") = ChkDependConcession.Checked
            newDR("EDD_GEMS_TYPE") = rblIsGems.SelectedValue
            newDR("EDD_GEMS_TYPE_DESCR") = rblIsGems.SelectedItem.Text

            newDR("EDD_GEMS_ID") = hf_dependant_ID.Value

            newDR("EDD_BSU_ID") = hf_dependant_unit.Value
            newDR("EDD_BSU_NAME") = txtDependUnit.Text

            newDR("EDD_PASSPRTNO") = txtDepPassportNo.Text
            newDR("EDD_UIDNO") = txtDepUidno.Text
            newDR("EDD_VISAISSUEDATE") = txtDepVissuedate.Text
            newDR("EDD_VISAEXPDATE") = txtDepVexpdate.Text
            newDR("EDD_VISAISSUEPLACE") = txtDepVissueplace.Text

            newDR("Status") = "INSERT"
            status = "Insert"

            If ddlDepInsuCardTypes.SelectedItem.Text <> "--Select--" Then
                newDR("EDD_Insu_id") = ddlDepInsuCardTypes.SelectedValue
                newDR("InsuranceCategoryDESC") = ddlDepInsuCardTypes.SelectedItem.Text
            Else
                newDR("EDD_Insu_id") = 0
                newDR("InsuranceCategoryDESC") = ""
            End If

            ' If chkDepCompInsu.Checked Then
            newDR("EDD_bCompanyInsurance") = chkDepCompInsu.Checked
            newDR("EDC_DOCUMENT_NO") = Me.txtEIDNo.Text
            newDR("EDC_EXP_DT") = Me.txtEIDExpryDT.Text
            newDR("EDD_bMALE") = rdDepdMale.Checked
            newDR("EDC_Gender") = IIf(rdDepdMale.Checked = True, "M", "F")
            newDR("EDD_MSTATUS") = Me.ddlDependMstatus.SelectedItem.Text
            newDR("EDD_MARITALSTATUS") = Me.ddlDependMstatus.SelectedValue
            newDR("EDD_bPhoto") = Me.FUUploadDependance.HasFile

            If FUUploadDependance.FileName <> "" Then
                Try
                    Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                    Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                    If Not Directory.Exists(str_img & "\temp") Then
                        Directory.CreateDirectory(str_img & "\temp")
                    End If
                    Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & FUUploadDependance.FileName
                    Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
                    FUUploadDependance.PostedFile.SaveAs(strFilepath)
                    If IO.File.Exists(strFilepath) Then
                        newDR("EDD_Photo") = ConvertImageFiletoBytes(strFilepath)
                        IO.File.Delete(strFilepath)
                    End If
                Catch ex As Exception
                    lblError.Text = "Unable to save the image"
                End Try


            End If


            'If FUUploadDependance.HasFile Then
            '    Dim filePath As String = FUUploadDependance.PostedFile.FileName
            '    newDR("EDD_Photo") = ConvertImageFiletoBytes(filePath)
            'End If
            newDR("CTY_NATIONALITY") = Me.txtDepCountry.Text
            newDR("EDD_CTY_ID") = Me.hfDepCountry_ID.Value
            'Else
            ' newDR("EDD_bCompanyInsurance") = "No"
            'End If

            dtDependanceDetails.Rows.Add(newDR)
            btnDependanceAdd.Text = "Add"
            lblError.Text = ""
            Session("EMPDEPENDANCEEditID") = ""
        ElseIf btnDependanceAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPDEPENDANCEEditID")
            For iIndex = 0 To dtDependanceDetails.Rows.Count - 1
                If str_Search = dtDependanceDetails.Rows(iIndex)("UniqueID") And dtDependanceDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtDependanceDetails.Rows(iIndex)("EDD_NAME") = txtDependName.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_RELATION") = ddlDependRelation.SelectedValue
                    dtDependanceDetails.Rows(iIndex)("EDD_DOB") = txtDependDOB.Text
                    dtDependanceDetails.Rows(iIndex)("bConcession") = ChkDependConcession.Checked
                    dtDependanceDetails.Rows(iIndex)("EDD_STU_ID") = 0 'CInt(txtDependNoTickets.Text)
                    dtDependanceDetails.Rows(iIndex)("EDD_NoofTicket") = CDbl(txtAirTicketNo.Text) 'CDbl(txtDependNoTickets.Text)
                    dtDependanceDetails.Rows(iIndex)("EDD_GEMS_TYPE") = rblIsGems.SelectedValue
                    dtDependanceDetails.Rows(iIndex)("EDD_GEMS_ID") = hf_dependant_ID.Value
                    dtDependanceDetails.Rows(iIndex)("EDD_GEMS_TYPE_DESCR") = rblIsGems.SelectedItem.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_BSU_ID") = hf_dependant_unit.Value
                    dtDependanceDetails.Rows(iIndex)("EDD_BSU_NAME") = txtDependUnit.Text
                    'dtDependanceDetails.Rows(iIndex)("EDD_Insu_id") = ddlDepInsuCardTypes.SelectedValue
                    dtDependanceDetails.Rows(iIndex)("EDD_bCompanyInsurance") = chkDepCompInsu.Checked
                    If ddlDepInsuCardTypes.SelectedItem.Text <> "--Select--" Then
                        dtDependanceDetails.Rows(iIndex)("EDD_Insu_id") = ddlDepInsuCardTypes.SelectedValue
                        dtDependanceDetails.Rows(iIndex)("InsuranceCategoryDESC") = ddlDepInsuCardTypes.SelectedItem.Text
                    Else
                        dtDependanceDetails.Rows(iIndex)("EDD_Insu_id") = 0
                        dtDependanceDetails.Rows(iIndex)("InsuranceCategoryDESC") = ""
                    End If
                    dtDependanceDetails.Rows(iIndex)("EDD_PASSPRTNO") = txtDepPassportNo.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_UIDNO") = txtDepUidno.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_VISAISSUEDATE") = txtDepVissuedate.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_VISAEXPDATE") = txtDepVexpdate.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_VISAISSUEPLACE") = txtDepVissueplace.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_bCompanyInsurance") = chkDepCompInsu.Checked '
                    dtDependanceDetails.Rows(iIndex)("EDC_DOCUMENT_NO") = Me.txtEIDNo.Text.Trim
                    dtDependanceDetails.Rows(iIndex)("EDC_EXP_DT") = Me.txtEIDExpryDT.Text
                    dtDependanceDetails.Rows(iIndex)("EDD_bMALE") = rdDepdMale.Checked
                    dtDependanceDetails.Rows(iIndex)("EDD_MARITALSTATUS") = Me.ddlDependMstatus.SelectedValue
                    dtDependanceDetails.Rows(iIndex)("EDC_Gender") = IIf(rdDepdMale.Checked = True, "M", "F")
                    dtDependanceDetails.Rows(iIndex)("EDD_MSTATUS") = Me.ddlDependMstatus.SelectedItem.Text
                    If FUUploadDependance.HasFile Or Not ViewState("imgDependant") Is Nothing Then
                        dtDependanceDetails.Rows(iIndex)("EDD_bPhoto") = True
                    End If

                    If FUUploadDependance.FileName <> "" Then
                        Try
                            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                            If Not Directory.Exists(str_img & "\temp") Then
                                Directory.CreateDirectory(str_img & "\temp")
                            End If
                            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & FUUploadDependance.FileName
                            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
                            FUUploadDependance.PostedFile.SaveAs(strFilepath)
                            If IO.File.Exists(strFilepath) Then
                                dtDependanceDetails.Rows(iIndex)("EDD_Photo") = ConvertImageFiletoBytes(strFilepath)
                                IO.File.Delete(strFilepath)
                            End If
                        Catch ex As Exception
                            Me.lblError.Text = "Unable to save the Image"
                        End Try


                    End If
                    'If FUUploadDependance.HasFile Then
                    '    Dim filePath As String = FUUploadDependance.PostedFile.FileName
                    '    dtDependanceDetails.Rows(iIndex)("EDD_Photo") = ConvertImageFiletoBytes(filePath)
                    'End If

                    dtDependanceDetails.Rows(iIndex)("EDD_CTY_ID") = Me.hfDepCountry_ID.Value
                    dtDependanceDetails.Rows(iIndex)("CTY_NATIONALITY") = Me.txtDepCountry.Text

                    status = "Edit/Update"
                    Exit For
                End If
            Next
            btnDependanceAdd.Text = "Add"
            lblError.Text = ""
            Session("EMPDEPENDANCEEditID") = ""
        End If
        If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwSalary) = 0 Then
            clearDependanciesDetails()
        Else
            UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        End If

        Session("EMPDEPENDANCEDETAILS") = dtDependanceDetails
        GridBindDependanceDetails()

    End Sub
    Private Sub setImageDisplay(ByVal imgMap As Object, ByVal PhotoBytes As Byte(), Optional ByVal ID As String = "")
        Try
            Dim OC As System.Drawing.Image '= EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            If Not PhotoBytes Is Nothing Then
                ViewState("imgDependant") = PhotoBytes
                OC = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            Else
                OC = System.Drawing.Image.FromFile(WebConfigurationManager.AppSettings.Item("NoImagePath"))
            End If
            If OC Is Nothing Then Exit Sub
            Dim nameOfImage As String = Session.SessionID & Replace(Now.ToString, ":", "") & ID & "CurrentImage"
            If Not Cache.Get(nameOfImage) Is Nothing Then
                Cache.Remove(nameOfImage)
            End If
            Cache.Insert(nameOfImage, OC, Nothing, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Default, Nothing)
            imgMap.ImageUrl = "empOrgChartImage.aspx?ID=" & nameOfImage
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Or ImageFilePath.Trim = "" Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Protected Sub gvDependancedetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDependancedetails.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowDependanceDetails(categoryID)
    End Sub

    Protected Sub lnkDependanceEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPDEPENDANCEDETAILS").Rows.Count - 1
            If str_Search = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("UniqueID") And Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("Status").ToString.ToUpper & "" <> "DELETED" Then

                rblIsGems.SelectedValue = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_GEMS_TYPE").ToString
                rblIsGems_SelectedIndexChanged(Nothing, Nothing)
                txtDependName.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_NAME")
                ddlDependRelation.SelectedValue = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_RELATION")
                txtDependDOB.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_DOB") ' IIf(Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_DOB") = "", "", Format(Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_DOB"), OASISConstants.DateFormat))
                ChkDependConcession.Checked = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("bConcession")
                txtStudID.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_STU_ID").ToString
                '  txtAirTicketNo.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_NoofTicket").ToString
                ChkDependConcession.Checked = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("bConcession").ToString
                hf_dependant_ID.Value = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_GEMS_ID").ToString
                hf_dependant_unit.Value = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_BSU_ID").ToString
                txtDependUnit.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_BSU_NAME").ToString
                DoNullCheck(ddlDepInsuCardTypes, Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_Insu_id").ToString)
                chkDepCompInsu.Checked = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_bCompanyInsurance").ToString

                txtDepPassportNo.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_PASSPRTNO").ToString
                txtDepUidno.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_UIDNO").ToString

                txtDepVissuedate.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_VISAISSUEDATE").ToString


                txtDepVexpdate.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_VISAEXPDATE").ToString
                txtDepVissueplace.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_VISAISSUEPLACE").ToString




                Me.txtEIDNo.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDC_DOCUMENT_NO").ToString
                Me.txtEIDExpryDT.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDC_EXP_DT").ToString
                Me.ddlDependMstatus.SelectedValue = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_MARITALSTATUS").ToString
                If (Convert.ToBoolean(Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_bMALE")) = True) Then
                    Me.rdDepdMale.Checked = True
                Else
                    Me.rdDepdFemale.Checked = True
                End If
                If Not Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_Photo") Is DBNull.Value Then
                    'ViewState("imgDependant") = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_Photo")
                    setImageDisplay(Me.imgDependent, Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_Photo"))
                Else
                    imgDependent.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgDependent.AlternateText = "No Image found"
                End If
                Me.hfDepCountry_ID.Value = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("EDD_CTY_ID").ToString
                Me.txtDepCountry.Text = Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("CTY_NATIONALITY").ToString
                Session("EMPDEPENDANCEDETAILS").Rows(iIndex)("Status") = "EDIT"

                gvDependancedetails.SelectedIndex = iIndex
                btnDependanceAdd.Text = "Save"
                Session("EMPDEPENDANCEEditID") = str_Search
                UtilityObj.beforeLoopingControls(vwConnDet)
                Exit For
            End If
        Next
    End Sub
#End Region

#Region "Language Details"
    Private Function CreateLangTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cLang As New DataColumn("Language", System.Type.GetType("System.String"))
            Dim cLangID As New DataColumn("Language_ID", System.Type.GetType("System.Int32"))
            Dim cLangSpeak As New DataColumn("Speak", System.Type.GetType("System.String"))
            Dim cLangRead As New DataColumn("Read", System.Type.GetType("System.String"))
            Dim cLangWrite As New DataColumn("Write", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cLang)
            dtDt.Columns.Add(cLangID)
            dtDt.Columns.Add(cLangSpeak)
            dtDt.Columns.Add(cLangRead)
            dtDt.Columns.Add(cLangWrite)

            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function


    Protected Sub btnLangAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLangAdd.Click
        If Session("EMPLANGDETAILS") Is Nothing Then
            Session("EMPLANGDETAILS") = CreateLangTable()
        End If
        h_Qual_Grade.Value = 0
        Dim dtLANGDetails As DataTable = Session("EMPLANGDETAILS")
        Dim Status As String = String.Empty

        Dim i As Integer
        For i = 0 To dtLANGDetails.Rows.Count - 1
            If Session("EMPLangEditID") IsNot Nothing And _
            Session("EMPLangEditID") <> dtLANGDetails.Rows(i)("UniqueID") Then
                If dtLANGDetails.Rows(i)("Language") = txtQualQualification.Text Then
                    'And dtQUALDetails.Rows(i)("Grade_ID") = h_Qual_Grade.Value Then
                    lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                    GridBindLangDetails()
                    Exit Sub
                End If
            End If
        Next

        If btnLangAdd.Text = "Add" Then
            Dim newDR As DataRow = dtLANGDetails.NewRow()
            newDR("UniqueID") = gvLang.Rows.Count()
            newDR("Language") = txtLanguage.Text
            newDR("Language_ID") = h_LANG_LANGID.Value
            newDR("Speak") = ddlSpeak.SelectedItem.Text
            newDR("Read") = ddlRead.SelectedItem.Text
            newDR("Write") = ddlWrite.SelectedItem.Text
            newDR("Status") = "Insert"
            Status = "Insert"
            dtLANGDetails.Rows.Add(newDR)
            btnLangAdd.Text = "Add"
        ElseIf btnLangAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPLangEditID")
            For iIndex = 0 To dtLANGDetails.Rows.Count - 1
                If str_Search = dtLANGDetails.Rows(iIndex)("UniqueID") And dtLANGDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtLANGDetails.Rows(iIndex)("Language") = txtLanguage.Text
                    dtLANGDetails.Rows(iIndex)("Language_ID") = h_LANG_LANGID.Value
                    dtLANGDetails.Rows(iIndex)("Speak") = ddlSpeak.SelectedItem.Text
                    dtLANGDetails.Rows(iIndex)("Read") = ddlRead.SelectedItem.Text
                    dtLANGDetails.Rows(iIndex)("Write") = ddlWrite.SelectedItem.Text
                    Status = "Edit/Update"
                    Exit For
                End If
            Next
            btnLangAdd.Text = "Add"
        End If

        If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, Status, Page.User.Identity.Name.ToString, vwAsset) = 0 Then
            ClearEMPLangDetails()
        Else
            UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        End If
        Session("EMPLANGDETAILS") = dtLANGDetails
        GridBindLangDetails()
    End Sub
    Private Sub GridBindLangDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPQUALDETAILS") Is Nothing Then Return
        If Session("EMPLANGDETAILS") Is Nothing Then
            gvLang.DataSource = Nothing
            gvLang.DataBind()
            Return
        End If
        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateLangTable()
        If Session("EMPLANGDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPLANGDETAILS").Rows.Count - 1
                If (Session("EMPLANGDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPLANGDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPLANGDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvLang.DataSource = dtTempDtl
        gvLang.DataBind()
    End Sub



    Private Sub DeleteRowLangDetails(ByVal pId As String)
        Dim lblTid As New Label
        lblTid = TryCast(gvLang.Rows(pId).FindControl("lblId"), Label)

        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub

        Dim iRemove As Integer = 0
        For iRemove = 0 To Session("EMPLANGDETAILS").Rows.Count - 1
            If (Session("EMPLANGDETAILS").Rows(iRemove)("UniqueID") = uID) Then
                Session("EMPLANGDETAILS").Rows(iRemove)("Status") = "DELETED"
                If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwAsset) = 0 Then
                    GridBindLangDetails()
                Else
                    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                End If
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnLangCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLangCancel.Click
        ClearEMPLangDetails()
    End Sub


    Sub ClearEMPLangDetails()
        txtLanguage.Text = ""
        h_LANG_LANGID.Value = ""
    End Sub
    Protected Sub lnkLangEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPLANGDETAILS").Rows.Count - 1
            If str_Search = Session("EMPLANGDETAILS").Rows(iIndex)("UniqueID") And Session("EMPLANGDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtLanguage.Text = Session("EMPLANGDETAILS").Rows(iIndex)("Language")
                h_LANG_LANGID.Value = Session("EMPLANGDETAILS").Rows(iIndex)("Language_ID")
                ddlSpeak.ClearSelection()
                ddlSpeak.Items.FindByText(Session("EMPLANGDETAILS").Rows(iIndex)("Speak")).Selected = True
                ddlRead.ClearSelection()
                ddlRead.Items.FindByText(Session("EMPLANGDETAILS").Rows(iIndex)("Read")).Selected = True
                ddlWrite.ClearSelection()
                ddlWrite.Items.FindByText(Session("EMPLANGDETAILS").Rows(iIndex)("Write")).Selected = True

                gvLang.SelectedIndex = iIndex
                btnLangAdd.Text = "Save"
                Session("EMPLangEditID") = str_Search
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvLang_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvLang.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowLangDetails(categoryID)
    End Sub



    Private Function SaveEMPLangDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal EmpID As Integer) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPLANGDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPLANGDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPLANGDETAILS").Rows.Count - 1
                    Dim dr As DataRow = Session("EMPLANGDETAILS").Rows(iIndex)
                    If (Session("EMPLANGDETAILS").Rows(iIndex)("Status") <> "DELETED") Then

                        cmd = New SqlCommand("SaveEMPLANG_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEMD_EMP_ID As New SqlParameter("@EQL_EMP_ID", SqlDbType.Int)
                        sqlpEMD_EMP_ID.Value = EmpID 'EmployeeID
                        cmd.Parameters.Add(sqlpEMD_EMP_ID)

                        Dim sqlpEQL_ID As New SqlParameter("@EQL_ID", SqlDbType.Int)
                        'sqlpEQS_ID.Value = DBNull.Value  'EmployeeID
                        If ViewState("datamode") = "add" Then
                            sqlpEQL_ID.Value = 0
                        ElseIf ViewState("datamode") = "edit" Then
                            sqlpEQL_ID.Value = dr("UniqueID")
                        End If
                        cmd.Parameters.Add(sqlpEQL_ID)

                        Dim sqlpEQL_LANG_ID As New SqlParameter("@EQL_LANG_ID", SqlDbType.Int)
                        sqlpEQL_LANG_ID.Value = dr("Language_Id")
                        cmd.Parameters.Add(sqlpEQL_LANG_ID)

                        Dim sqlpEQL_Speak As New SqlParameter("@EQL_Speak", SqlDbType.VarChar, 20)
                        sqlpEQL_Speak.Value = dr("Speak")
                        cmd.Parameters.Add(sqlpEQL_Speak)


                        Dim sqlpEQL_READ As New SqlParameter("@EQL_READ", SqlDbType.VarChar, 20)
                        sqlpEQL_READ.Value = dr("Read")
                        cmd.Parameters.Add(sqlpEQL_READ)

                        Dim sqlpEQL_Write As New SqlParameter("@EQL_Write", SqlDbType.VarChar, 20)
                        sqlpEQL_Write.Value = dr("Write")
                        cmd.Parameters.Add(sqlpEQL_Write)



                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    Else
                        'Delete Code will Come here
                        cmd = New SqlCommand("DELETEEMPLANG_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEQL_ID As New SqlParameter("@EQL_ID", SqlDbType.Int)
                        sqlpEQL_ID.Value = dr("UniqueID")
                        cmd.Parameters.Add(sqlpEQL_ID)
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function




#End Region

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        clearme()
        ViewState("datamode") = "add"
        trsearchTable.Visible = False
        chkSalPayMonthly.Checked = True
        txtIU.Text = Session("BSU_Name")
        hfIU.Value = Session("sBsuid")
        txtWU.Text = Session("BSU_Name")
        hfWU.Value = Session("sBsuid")
        chkActive.Enabled = True
        ddlStatus.Enabled = True
        mnuMaster.Items(8).Selectable = True
        ddlStatus.Visible = True
        lblEmpStatus.Visible = False
        If Trim(txtFTE.Text) = "" Then
            txtFTE.Text = "1"
        End If
        For i As Integer = 0 To 9 '7
            SetPanelEnabled(i, True)
        Next
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call clearme()
            'clear the textbox and set the default settings
            ClearAllSessions()
            ViewState("datamode") = "none"
            trsearchTable.Visible = True
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub ClearAllSessions()
        Session.Remove("EMPDEPENDANCEDETAILS")
        Session.Remove("EMPQUALDETAILS")
        Session.Remove("EMPEXPDETAILS")
        Session.Remove("EMPSALSCHEDULE")
        Session.Remove("EMPSALDETAILS")
        Session.Remove("EMPDOCDETAILS")
        Session.Remove("EMPDOCDETAILS1")
        Session.Remove("EMPSALDETAILS")
        Session.Remove("EMPLANGDETAILS")
    End Sub

    Protected Sub btnSalScale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalScale.Click
        'Dim dtable As DataTable = CreateSalaryDetailsTable()
        If hfSalGrade.Value = "" Then Return
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal
        Dim fteVal As Double = 1
        If txtFTE.Text = "" Or Not IsNumeric(txtFTE.Text) Then
            fteVal = 1
        ElseIf CDbl(txtFTE.Text) > 1 Or CDbl(txtFTE.Text) < 0 Then
            fteVal = 1
        Else
            fteVal = CDbl(txtFTE.Text)
        End If
        str_Sql = "Select EMPGRADES_M.EGD_ID FROM EMPGRADES_M RIGHT OUTER JOIN" & _
        " EMPSCALES_GRADES_M ON EMPGRADES_M.EGD_ID = EMPSCALES_GRADES_M.SGD_EGD_ID " & _
        " WHERE EMPSCALES_GRADES_M.SGD_ID =" & hfSalGrade.Value
        Dim objGrade As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)

        'Commenting code to pass EMPGRADES_M.EGD_ID from here as will be based on new drop down provided now.

        'If Not objGrade Is DBNull.Value Then
        '    hfGrade_ID.Value = objGrade.ToString
        'End If

        'comment ends


        str_Sql = "select EMPGRADE_SALARY_S.*, ERN_DESCR from EMPGRADE_SALARY_S " & _
        " EMPGRADE_SALARY_S LEFT OUTER JOIN EMPSALCOMPO_M ON EMPGRADE_SALARY_S.GSS_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        "WHERE GSS_SGD_ID =" & hfSalGrade.Value & " and GSS_DTTO is null "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'If Session("EMPSALSCHEDULE") Is Nothing Then
        '    Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        'End If
        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryDetailsTable()
        End If

        Dim dTemptable As DataTable = Session("EMPSALDETAILS")
        For index As Integer = 0 To dTemptable.Rows.Count - 1
            dTemptable.Rows(index)("Status") = "DELETED"
        Next
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            grossSal += dr("GSS_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("ENR_ID") = dr("GSS_ERN_ID")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = True
            ldrTempNew.Item("PAYTERM") = 0
            ldrTempNew.Item("Amount") = dr("GSS_AMOUNT") * fteVal
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("GSS_AMOUNT")
            ldrTempNew.Item("Status") = "EDITED"
            dTemptable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("EMPSALDETAILS") = dTemptable
        GridBindSalaryDetails()
    End Sub

    Private Function GetNextESHID() As Integer
        If ViewState("ESH_ID") Is Nothing Then
            Dim cmd As New SqlCommand
            cmd.Connection = ConnectionManger.GetOASISConnection()
            cmd.CommandText = "select isnull(max(ESH_ID),0) from EMPSALARYHS_s "
            Dim GSS_ID As Integer = CInt(cmd.ExecuteScalar())
            cmd.Connection.Close()
            ViewState("ESH_ID") = GSS_ID
        Else
            ViewState("ESH_ID") += 1
        End If

        Return ViewState("ESH_ID")

    End Function

    Protected Sub ddlPayMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlPayMode.SelectedItem.Text = "Bank" Then
            txtBname.Enabled = True
            btnBank_name.Enabled = True
            txtAccCode.Enabled = True
        Else
            txtBname.Enabled = False
            btnBank_name.Enabled = False
            txtAccCode.Enabled = False
        End If
    End Sub
    Protected Sub ddlVstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    If ddlVstatus.SelectedItem.Text = "Visa Holder" Then
        '        ddlVtype.SelectedValue = 0
        '    ElseIf ddlVstatus.SelectedItem.Text = "LC Holder" Then
        '        ddlVtype.SelectedValue = 1
        '    End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message)
        'End Try
        Call callVisa_Bind()

    End Sub

    Public Sub callVisa_Bind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Try
            Dim Status_ID As String = String.Empty
            If ddlVstatus.SelectedIndex = -1 Then
                Status_ID = ""
            Else
                Status_ID = ddlVstatus.SelectedItem.Value
            End If
            Dim di As ListItem
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@VISA_STATUS_ID", Status_ID)

            Using VST_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetVISA_Type", pParms)
                ddlVtype.Items.Clear()
                If VST_reader.HasRows = True Then
                    While VST_reader.Read
                        di = New ListItem(VST_reader("EVM_DESCR"), VST_reader("EVM_ID"))
                        ddlVtype.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'v1.3 starts
    Public Sub LoadPopUpEmployee()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strFilter As String = Request.QueryString("FilterMode").Replace(" ", "+")
        Dim strStatus As String = Request.QueryString("Statusid").Replace(" ", "+")
        ' Dim strQuery As String = Session("searchEmpQuery").ToString ' ViewState("searchEmpQuery") 'Session("searchEmpQuery")
        Dim str_sql As String = "SELECT emp_id,empno,emp_fname + ' ' + isnull(Emp_mname,' ') + ' ' +isnull(Emp_lname,' ') as emp_fname from employee_m where "
        If strFilter = 0 Then
            str_sql = str_sql & " (emp_bsu_id='" & Session("sBsuid") & "' OR EMP_VISA_BSU_ID='" & Session("sBsuid") & "')"
        ElseIf strFilter = 1 Then

            str_sql = str_sql & " (EMP_BSU_ID = '" & Session("sBsuid") & "') "

        ElseIf strFilter = 2 Then

            str_sql = str_sql & " (EMP_VISA_BSU_ID = '" & Session("sBsuid") & "') "
        End If

        If strStatus <> "0" Then
            str_sql = str_sql & " and emp_status = " & strStatus
        End If

        If txtFNameS.Text <> "" Then
            Dim strTemp = txtFNameS.Text.Trim()
            strTemp = strTemp.Replace(" ", "%")
            'txtFNameS.Text.Replace(" ", "%")
            ' str_sql = str_sql & " and (emp_fname like '%" & txtFNameS.Text & "%' or emp_mname like '%" & txtFNameS.Text & "%' or emp_lname like '%" & txtFNameS.Text & "%') "
            str_sql = str_sql & " and (emp_fname + ' ' + isnull(Emp_mname,' ') + ' ' +isnull(Emp_lname,' ')) like '%" & strTemp & "%'"
        End If
        'If txtMNameS.Text <> "" Then
        '    str_sql = str_sql & " and IsNull(emp_mname,'') like '%" & txtMNameS.Text & "%'"
        'End If
        'If txtLNameS.Text <> "" Then
        '    str_sql = str_sql & "and IsNull(emp_lname,'') like '%" & txtLNameS.Text & "%'"
        'End If
        If txtEmpNoS.Text <> "" Then
            str_sql = str_sql & " and empno like '%" & txtEmpNoS.Text & "%'"
        End If
        str_sql = str_sql & " order by emp_fname + ' ' + isnull(Emp_mname,' ') + ' ' +isnull(Emp_lname,' ') "

        'emp_fname like '%" & txtFNameS.Text & "%'"   '& ViewState("viewid")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        gvEmpSearch.DataSource = ds
        gvEmpSearch.DataBind()
        If ds.Tables(0).Rows.Count = 1 Then
            Dim lnkTest As New LinkButton
            lnkTest = TryCast(gvEmpSearch.Rows(0).FindControl("lnkEmpFN"), LinkButton)
            lnkEmpFN_Click(lnkTest, Nothing)
            'gvEmpSearch.Visible = False
        End If
        gvEmpSearch.Visible = True
    End Sub


    Protected Sub imgEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgEmployee.Click
        LoadPopUpEmployee()


        'modalEmpPopUp.Commit(txtFNameS.Text)
        'modalEmpPopUp.Commit(txtMNameS.Text)
        'modalEmpPopUp.Commit(txtLNameS.Text)
        'Dim var = btnsearch.OnClientClick()

        'btnsearch_Click(Nothing, Nothing)

        ' modalEmpPopUp.ClientID
        ''modalEmpPopUp.Show()

    End Sub


    'Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
    '    'Dim var = btnsearch.OnClientClick()
    '    'pnlPopup.Visible = True
    'End Sub

    Protected Sub lnkEmpFN_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbname As New LinkButton
        'lbClose = sender.Parent.FindControl("LinkButton2") 'swapna commented -To pass only Names instead of Number
        lbname = sender.Parent.FindControl("lnkEmpFN") 'Swapna added
        lblcode = sender.Parent.FindControl("lblID")
        ViewState("datamode") = "view"
        Dim strid As String = lblcode.Text
        ' Request.QueryString("viewid") = strid
        'FillEmployeeMasterValues(lblcode.Text)
        Try
            chkActive.Enabled = False
            ddlStatus.Enabled = False
            ViewState("viewid") = lblcode.Text
            ' BindGradeLevels()
            FillEmployeeMasterValues(ViewState("viewid"))
            'BindGradeLevels()
            BindConcessionEligibilityUnit()
            For Each li As ListItem In ddlGradelevel.Items
                If li.Value = hfGrade_ID.Value Then
                    ddlGradelevel.SelectedValue = li.Value
                End If
            Next
            mnuMaster.Items(8).Selectable = True
            FillFixedAssetTab(ViewState("viewid"))
            ControlEnableEditing(ViewState("viewid"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Some Error occured while retrieving the data ... " & ex.Message
            btnSave.Visible = False
            Exit Sub
        Finally
            For i As Integer = 0 To 9 '7
                SetPanelEnabled(i, False)
            Next
            'SetPanelEnabled(0, True)
            gvEmpSearch.Visible = False
            'pnlSearchDetail.Visible = False

            cpePending.Collapsed = True
            cpePending.CollapsedText = "(Show search...)"



        End Try
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub GetAgentDetails()
        'to load employee WPS agent same as agent id of selected bank.
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Try
            Dim Status_ID As String = String.Empty
            If txtBname.Text <> "" Then
                Status_ID = hfBank_ID.Value
            End If

            If Status_ID <> "" Then
                Dim pParms(1) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@Bnk_id", Status_ID)

                Using VST_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetBankAgentId", pParms)
                    If VST_reader.HasRows = True Then
                        While VST_reader.Read
                            hf_WPSID.Value = VST_reader.Item("BNK_WPA_ID").ToString
                            txtWPSDesc.Text = VST_reader.Item("WPA_DESCR").ToString
                            txtWPSDesc.Enabled = False
                        End While
                    End If
                End Using
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvEmpSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpSearch.PageIndexChanging
        gvEmpSearch.PageIndex = e.NewPageIndex
        LoadPopUpEmployee()
    End Sub

    Protected Sub txtCat_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCat.TextChanged
        BindGradeLevels()
        GetLeavePolicy()
        BindConcessionEligibilityUnit()

    End Sub

    'V1.3 ends

    Protected Sub txtFNameS_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFNameS.TextChanged
        imgEmployee_Click(Nothing, Nothing)
    End Sub

    Protected Sub txtEmpNoS_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNoS.TextChanged
        imgEmployee_Click(Nothing, Nothing)
    End Sub

    Protected Sub txtBname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBname.TextChanged
        GetAgentDetails()
    End Sub


    Protected Sub btnBank_name_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        GetAgentDetails()
    End Sub

    Protected Sub txtReport_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtAttendBy.Text = "" Or hfAttendBy.Value = "" Then
            txtAttendBy.Text = txtReport.Text
            hfAttendBy.Value = hfReport.Value
        End If
    End Sub

    Protected Sub rblIsGems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblIsGems.SelectedIndexChanged
        If rblIsGems.SelectedValue = "N" Then
            txtDependUnit.Text = ""
            hf_dependant_ID.Value = ""
            hf_dependant_unit.Value = ""
            btnDepUnit.Enabled = False
            imgEmpSel.Visible = False
            txtDependName.Enabled = True
            txtDependName.Text = ""
            imgStudSel.Visible = False
        ElseIf rblIsGems.SelectedValue = "E" Then
            txtDependName.Text = ""
            txtDependUnit.Text = ""
            hf_dependant_unit.Value = ""
            btnDepUnit.Enabled = True
            imgEmpSel.Visible = True
            txtDependName.Enabled = False
            imgStudSel.Visible = False
        Else
            txtDependUnit.Text = ""
            txtDependName.Text = ""
            hf_dependant_unit.Value = ""
            btnDepUnit.Enabled = True
            imgEmpSel.Visible = False
            txtDependName.Enabled = False
            imgStudSel.Visible = True

        End If
    End Sub

    Protected Sub txtDependUnit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDependUnit.TextChanged

        'txtDependName.Text = ""
        'hf_dependant_ID.Value = ""

    End Sub



    Protected Sub ddlVtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVtype.SelectedIndexChanged
        If ((ddlVstatus.SelectedItem.Text = "LC Holder") And ((ddlVtype.SelectedItem.Text = "Temporary Permit") Or (ddlVtype.SelectedItem.Text = "Part-time Permit"))) Then
            ddlCategory.SelectedValue = "B"
            txtIU.Text = "NON-GEMS(VISA Unit)"
            hfIU.Value = "800015"
        Else
            ddlCategory.SelectedValue = "C"
            txtIU.Text = Session("BSU_Name")
            hfIU.Value = Session("sBsuid")
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindHRL_Grade_Levels(ddlGrade.SelectedValue)
    End Sub


    Protected Sub hfBank_ID_ValueChanged(sender As Object, e As EventArgs)
        GetAgentDetails()
    End Sub
End Class


Public Class ERNDetails
    Public ERNID As String
    Public fromDate As DateTime
    Public toDate As DateTime
    Public amount As Double
    Public ESH_ID As String
End Class
