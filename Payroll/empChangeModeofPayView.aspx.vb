Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empChangeModeofPayView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empChangeModeofPay.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or _
            ((ViewState("MainMnu_code") <> "P450035") And (ViewState("MainMnu_code") <> "P450040") And (ViewState("MainMnu_code") <> "P450030")) Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Select Case ViewState("MainMnu_code").ToString
                    Case "P450030"
                        lblHead.Text = "Mode Of Pay Deatils"
                    Case "P450035"
                        lblHead.Text = "Accommodation Deatils"
                    Case "P450040"
                        lblHead.Text = "Transportation Deatils"
                End Select
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
            If Request.QueryString("deleted") <> "" Then
                lblError.Text = "Successfully deleted"
            End If
        End If
    End Sub


    Sub gridbind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)
                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFrom")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_DTFROM", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_DTTO", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_REMARKS", lstrCondn5)
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case "P450030"
                    str_Sql = "SELECT * FROM( SELECT EST.EST_TTP_ID,EST.EST_BSU_ID, EST.EST_DTFROM, " _
                                  & " EST.EST_EMP_ID, EST.EST_DTTO," _
                                  & " CASE ISNULL(EST.EST_CODE, 0) WHEN 0 THEN 'CASH'" _
                                  & " WHEN 1 THEN 'BANK' END AS NEW," _
                                  & " CASE ISNULL(EST.EST_OLDCODE, EST.EST_CODE) WHEN 0" _
                                  & " THEN 'CASH' WHEN 1 THEN 'BANK' END AS OLD," _
                                  & " EST.EST_CODE, EST.EST_REMARKS, EST.EST_ID, " _
                                  & " TTM.TTP_DESCR, EM.EMPNO, ISNULL(EM.EMP_FNAME,'')+' '+ " _
                                  & " ISNULL(EM.EMP_MNAME,'')+' '+ ISNULL(EM.EMP_LNAME,'')AS EMP_NAME" _
                                  & " FROM EMPTRANTYPE_TRN AS EST INNER JOIN" _
                                  & " EMPLOYEE_M AS EM ON EST.EST_EMP_ID = EM.EMP_ID LEFT OUTER JOIN" _
                                  & " EMPTRANTYPE_M AS TTM ON EST.EST_TTP_ID = TTM.TTP_ID ) AS  DB" _
                                  & " WHERE ( EST_TTP_ID = 6) AND ( EST_BSU_ID = '" & Session("sbsuid") & "')"
                Case "P450035"
                    str_Sql = "SELECT * FROM( SELECT   EST.EST_TTP_ID,EST.EST_BSU_ID, EST.EST_DTFROM," _
                                & " EST.EST_EMP_ID, EST.EST_DTTO," _
                                & " CASE ISNULL(EST.EST_CODE, 0) WHEN 0 " _
                                & " THEN 'Own Accommodation' WHEN 1 THEN 'Company Single Accommodation' " _
                                & " WHEN 2 THEN 'Company Family Accommodation' END AS NEW," _
                                & " CASE ISNULL(EST.EST_OLDCODE, EST.EST_CODE) WHEN 0 " _
                                & " THEN 'Own Accommodation' WHEN 1 THEN 'Company Single Accommodation' " _
                                & " WHEN 2 THEN 'Company Family Accommodation' END AS OLD, " _
                                & " EST.EST_CODE, EST.EST_REMARKS, EST.EST_ID," _
                                & " TTM.TTP_DESCR, EM.EMPNO, ISNULL(EM.EMP_FNAME,'')+' '+ " _
                                & " ISNULL(EM.EMP_MNAME,'')+' '+ ISNULL(EM.EMP_LNAME,'')AS EMP_NAME" _
                                & " FROM EMPTRANTYPE_TRN AS EST INNER JOIN" _
                                & " EMPLOYEE_M AS EM ON EST.EST_EMP_ID = EM.EMP_ID LEFT OUTER JOIN" _
                                & " EMPTRANTYPE_M AS TTM ON EST.EST_TTP_ID = TTM.TTP_ID ) AS  DB" _
                                & " WHERE ( EST_TTP_ID = 7) AND ( EST_BSU_ID = '" & Session("sbsuid") & "') "
                Case "P450040"
                    str_Sql = "SELECT * FROM( SELECT  EST.EST_TTP_ID,EST.EST_BSU_ID,EST.EST_ID, EST.EST_DTFROM," _
                                & " EST.EST_EMP_ID, EST.EST_DTTO, CASE ISNULL(EST_CODE, '') " _
                                & " WHEN 'true' THEN 'Company Transport' WHEN 'false' " _
                                & " THEN 'Own Transport' END AS NEW," _
                                & " CASE ISNULL(EST_OLDCODE, EST_CODE)" _
                                & " WHEN 'true' THEN 'Company Transport' WHEN 'false'" _
                                & " THEN 'Own Transport' END AS OLD, EST.EST_CODE, EST.EST_REMARKS," _
                                & " EST.EST_OLDCODE, EM.EMPNO, ISNULL(EM.EMP_FNAME, '') +' '+" _
                                & " ISNULL(EM.EMP_MNAME, '') +' '+ ISNULL(EM.EMP_LNAME, '') AS EMP_NAME" _
                                & " FROM  EMPTRANTYPE_TRN AS EST INNER JOIN" _
                                & " EMPLOYEE_M AS EM ON EST.EST_EMP_ID = EM.EMP_ID ) AS  DB" _
                                & " WHERE     ( EST_TTP_ID = 8)" _
                                & " AND ( EST_BSU_ID = '" & Session("sbsuid") & "')"
            End Select
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & str_Filter)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empChangeModeofPay.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

End Class
