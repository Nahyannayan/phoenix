<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empEarn_Ded.aspx.vb" Inherits="Payroll_empEarn_Ded" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" valign="top">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <asp:GridView ID="gvCommonEmp" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-row table-bordered" DataKeyNames="ID" SkinID="GridViewView" PageSize="20" Width="100%" CellPadding="2">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" SortExpression="GRP_ID">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHCode" runat="server">ID</asp:Label><br />
                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnCodeSearch_Click" />
                                            
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:Label ID="lblcode" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="DESCR">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblName" runat="server">Description</asp:Label><br />
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchpar_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit Account">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblAccNameH" runat="server" Text='<%# Bind("DEBIT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Debit Account<br />
                                            <asp:TextBox ID="txtAmtName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchAcc" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchAcc_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit Account">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblAccNameH" runat="server" Text='<%# Bind("Credit") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Credit Account<br />
                                            <asp:TextBox ID="txtAmtNamec" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchAccc" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchAcc_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End of service benefit" Visible="False">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Rules<br />
                                            <asp:DropDownList ID="ddlEmpEarnH" runat="server" AutoPostBack="True" CssClass="listbox"
                                                OnSelectedIndexChanged="DropDownListType_SelectedIndexChanged">
                                            </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="Label2" runat="server" Text='<%# Bind("EMR_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>

