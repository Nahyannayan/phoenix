﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports UtilityObj
Partial Class Payroll_TrackCovidPositiveCases
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "P050140" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        'set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String



        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))


    End Sub

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvEMPCOVIDTrackDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvEMPCOVIDTrackDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvEMPCOVIDTrackDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrBSU As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrType As String = String.Empty
        Dim lstrCategory As String = String.Empty
        Dim lstrContactPerson As String = String.Empty
        Dim lstrIsoSatrtDT As String = String.Empty
        Dim lstrISoEndDT As String = String.Empty
        Dim lstrDTContactPerson As String = String.Empty
        Dim lstrInfluenza As String = String.Empty

        Dim lstrFiltBSU As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltType As String = String.Empty
        Dim lstrFiltCategory As String = String.Empty
        Dim lstrFiltContactPerson As String = String.Empty
        Dim lstrFiltISoStartDT As String = String.Empty
        Dim lstrFiltIsoEndDT As String = String.Empty
        Dim lstrFiltDTContactPerson As String = String.Empty
        Dim lstrFiltInfluenza As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        If gvEMPCOVIDTrackDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---

            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtBSU")
            lstrBSU = Trim(txtSearch.Text)
            If (lstrBSU <> "") Then lstrFiltBSU = SetCondn(lstrOpr, "BSU_SHORTNAME", lstrBSU)

            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtStaffStudpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "[Staff / Student Ref]", lstrEMPNo)
            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtStaffStudName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "[Ref Name]", lstrEmpName)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtType")
            lstrType = txtSearch.Text
            If (lstrType <> "") Then lstrFiltType = SetCondn(lstrOpr, "[TYPE]", lstrType)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtCategory")
            lstrCategory = txtSearch.Text
            If (lstrCategory <> "") Then lstrFiltCategory = SetCondn(lstrOpr, "[CTD_CATEGORY]", lstrCategory)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtContactPerson")
            lstrContactPerson = txtSearch.Text
            If (lstrContactPerson <> "") Then lstrFiltContactPerson = SetCondn(lstrOpr, "[CTD_PERSON_NAME]", lstrContactPerson)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtIsolationStartDT")
            lstrIsoSatrtDT = txtSearch.Text
            If (lstrIsoSatrtDT <> "") Then lstrFiltISoStartDT = SetCondn(lstrOpr, "[CTD_ISOL_DATE]", lstrIsoSatrtDT)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtIsolationEndDT")
            lstrISoEndDT = txtSearch.Text
            If (lstrISoEndDT <> "") Then lstrFiltIsoEndDT = SetCondn(lstrOpr, "[CTD_DEISOL_DATE]", lstrISoEndDT)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtDTContactPerson")
            lstrDTContactPerson = txtSearch.Text
            If (lstrDTContactPerson <> "") Then lstrFiltDTContactPerson = SetCondn(lstrOpr, "[CONTACT_PERSON_ON]", lstrDTContactPerson)

            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtInfluenza")
            lstrInfluenza = txtSearch.Text
            If (lstrInfluenza <> "") Then lstrFiltInfluenza = SetCondn(lstrOpr, "[CTD_INFLUENZA]", lstrInfluenza)

        End If
        Dim searchStg As String = ""
        searchStg = str_Filter & lstrFiltBSU & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltType & lstrFiltCategory & lstrFiltContactPerson & lstrFiltISoStartDT & lstrFiltIsoEndDT & lstrFiltDTContactPerson & lstrFiltInfluenza
        If searchStg <> "" Then
            searchStg = searchStg.Replace("'%", "''%")
            searchStg = searchStg.Replace("%'", "%''")
        End If
        'lstrFiltDesignation & lstrFiltPassportNo &
        'str_Sql = "exec Get_EMP_COVID_DETAILS '" & Session("sBsuid") & "'"
        str_Sql = "exec COV.GET_POSITIVE_TRACKING_ALL '" & Session("sBsuid") & "' ,'" & searchStg & "'"

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT, EMP_VISA_ID, " &
        '" EMP_PASSPORT, EMP_STATUS_DESCR, CATEGORY_DESC as EMP_DES_DESCR," &
        '" CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 0 ELSE 1 END bPhotoExists  " &
        '" FROM vw_OSO_EMPLOYEEMASTER" &
        '" WHERE (EMP_VISA_BSU_ID = '" & Session("sBsuid") & "' " &
        '" or EMP_VISA_BSU_ID in (SELECT UBL_BSU_ID FROM USR_BSULIST where UBL_MNU_ID='" & ViewState("MainMnu_code") & "' and UBL_USR_NAME='" & Session("sUsr_name") & "'))" &
        'str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName &
        'lstrFiltDesignation & lstrFiltPassportNo & lstrFiltVisaID & " order by EMP_STATUS_DESCR, EMPNO, EMPNAME, CATEGORY_DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        If ds.Tables.Count > 0 Then
            gvEMPCOVIDTrackDetails.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEMPCOVIDTrackDetails.DataBind()
                Dim columnCount As Integer = gvEMPCOVIDTrackDetails.Rows(0).Cells.Count

                gvEMPCOVIDTrackDetails.Rows(0).Cells.Clear()
                gvEMPCOVIDTrackDetails.Rows(0).Cells.Add(New TableCell)
                gvEMPCOVIDTrackDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEMPCOVIDTrackDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEMPCOVIDTrackDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvEMPCOVIDTrackDetails.DataBind()
            End If
        End If

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtBSU")
        txtSearch.Text = lstrBSU

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtStaffStudpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtStaffStudName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtType")
        txtSearch.Text = lstrType

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtCategory")
        txtSearch.Text = lstrCategory

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtContactPerson")
        txtSearch.Text = lstrContactPerson

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtDTContactPerson")
        txtSearch.Text = lstrDTContactPerson

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtIsolationStartDT")
        txtSearch.Text = lstrIsoSatrtDT

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtIsolationEndDT")
        txtSearch.Text = lstrISoEndDT

        txtSearch = gvEMPCOVIDTrackDetails.HeaderRow.FindControl("txtInfluenza")
        txtSearch.Text = lstrInfluenza


        gvEMPCOVIDTrackDetails.SelectedIndex = p_selected_id
    End Sub

    Protected Sub gvEMPCOVIDTrackDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPCOVIDTrackDetails.PageIndexChanging
        gvEMPCOVIDTrackDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblRef_ID As New Label
            Dim lblStaffStudName As New Label
            Dim lblType As New Label
            Dim lblCTH_ID As New Label
            Dim url As String
            Dim viewid As String
            Dim staffStudNo As String
            Dim type As String
            Dim cthid As String

            lblRef_ID = TryCast(sender.FindControl("lblEMPStudID"), Label)
            viewid = lblRef_ID.Text

            lblCTH_ID = TryCast(sender.FindControl("lblCTH_ID"), Label)
            cthid = lblCTH_ID.Text

            lblStaffStudName = TryCast(sender.FindControl("lblStaffStudName"), Label)
            staffStudNo = lblStaffStudName.Text

            lblType = TryCast(sender.FindControl("lblType"), Label)
            type = lblType.Text

            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            staffStudNo = Encr_decrData.Encrypt(staffStudNo)
            type = Encr_decrData.Encrypt(type)
            cthid = Encr_decrData.Encrypt(cthid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Payroll\TrackCovidPositiveCasesEntry.aspx?MainMnu_code={0}&datamode={1}&viewid={2}&refName={3}&reftype={4}&cthid={5}", MainMnu_code, ViewState("datamode"), viewid, staffStudNo, type, cthid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Payroll\TrackCovidPositiveCasesEntry.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbRecovered_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblRef_ID As New Label

            Dim lblCTD_ID As New Label
            Dim lblCTH_ID As New Label

            lblCTH_ID = TryCast(sender.FindControl("lblCTH_ID"), Label)
            'cthid = lblCTH_ID.Text
            lblCTD_ID = TryCast(sender.FindControl("lblCTD_ID"), Label)
            'cthid = lblCTD_ID.Text
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Dim id As Integer = 1000
                Dim cmd As New SqlCommand
                cmd = New SqlCommand("[COV].[RECOVERED_POSITIVE_TRACKING_D]", conn, transaction)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpCTD_ID As New SqlParameter("@CTD_ID", SqlDbType.BigInt)
                sqlpCTD_ID.Value = Convert.ToInt32(lblCTD_ID.Text)
                cmd.Parameters.Add(sqlpCTD_ID)
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                id = retValParam.Value
                If id = 0 Then
                    transaction.Commit()
                    lblError.Text = "Recovery status changed successfully."
                    gridbind()
                Else transaction.Rollback()
                    lblError.Text = "Unable to change the staus to recovered."
                End If
            End Using


        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub gvEMPCOVIDTrackDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPCOVIDTrackDetails.RowDataBound
        Dim lbRecovered As New LinkButton
        lbRecovered = (TryCast(e.Row.FindControl("lbRecovered"), LinkButton))
        Dim lblRecovered As Label = (TryCast(e.Row.FindControl("lblRecovered"), Label))
        Dim lblCTD_INF_CLOSE_CONTACT As Label = (TryCast(e.Row.FindControl("lblCTD_INF_CLOSE_CONTACT"), Label))
        If lblCTD_INF_CLOSE_CONTACT IsNot Nothing Then
            If lblCTD_INF_CLOSE_CONTACT.Text.ToUpper = "INFECTED" Then
                If lblRecovered IsNot Nothing Then
                    If lblRecovered.Text = "Yes" Then
                        lbRecovered.Visible = False
                        lblRecovered.Visible = True
                    Else lblRecovered.Visible = False
                        lbRecovered.Visible = True
                    End If
                End If
            Else lblRecovered.Visible = False
                lbRecovered.Visible = False
            End If
        End If
    End Sub
End Class
