﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="empGenerateGIROFile.aspx.vb" Inherits="Payroll_empGenerateGIROFile" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<script language="javascript" type="text/javascript">
 function get_Bank()
    {     
            var sFeatures;
            sFeatures="dialogWidth: 509px; ";
            sFeatures+="dialogHeight: 434px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
             
            result = window.showModalDialog("..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname="+document.getElementById('<%=txtBankCode.ClientID %>').value,"", sFeatures);                                 

            if (result=='' || result==undefined)
            {
            return false;
            }
            NameandCode = result.split('||');
           document.getElementById('<%=txtBankCode.ClientID %>').value=NameandCode[0]; 
           document.getElementById('<%=txtBankDescr.ClientID %>').value=NameandCode[1]; 
         
           return false;
    }  
 
</script>--%>

    <script>
        function get_Bank() {
            url = "..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value
         var oWnd = radopen(url, "pop_supplier");
     }
     function OnClientClose1(oWnd, args) {
         var NameandCode;
         var arg = args.get_argument();
         if (arg) {

             NameandCode = arg.NameandCode.split('||');
             document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_supplier" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Generate GIRO Format"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server"  width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"  
                                ValidationGroup="groupM1" Style="text-align: left"   />
                          
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory      
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <table id="Table1" runat="server"  width="100%">
                                <tr>
                                    <td align="left" class="matters"  width="20%" ><span class="field-label">Bank A/C</span></td>
                                    <td align="left" class="matters"  width="30%">
                                        <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True"  ></asp:TextBox>
                                        <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank();return false;" />
                                        <asp:TextBox ID="txtBankDescr" runat="server"  ></asp:TextBox>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>

                                    <td align="left" class="matters"  ><span class="field-label">Month<span style="font-size: 8pt; color: #800000">*</span></span></td>
                                    <td align="left" class="matters"  >
                                        <asp:DropDownList ID="ddlMonth" runat="server"  
                                            AutoPostBack="True">
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList></td>

                                    <td align="left" class="matters"><span class="field-label">Year<span style="font-size: 8pt; color: #800000">*</span></span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlYear" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>

                                    <td align="left" class="matters"><span class="field-label">Value Date <span style="font-size: 8pt; color: #800000">*</span></span></td>
                                    <td align="left" class="matters" >
                                        <asp:TextBox ID="txtValueDate" runat="server" ></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server"
                                            CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgValueDate"
                                            PopupPosition="TopRight" TargetControlID="txtValueDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtValueDate_CalendarExtender2"
                                            runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtValueDate" PopupPosition="TopRight"
                                            TargetControlID="txtValueDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgValueDate" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnGenerate" runat="server" CssClass="button"
                                Text="Generate GIRO File"  />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"
                                 />
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</asp:Content>
