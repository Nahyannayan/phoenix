<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empFeeConcessionReqDetail.aspx.vb"
    Inherits="Payroll_empFeeConcessionReqDetail" Title="Untitled Page" %>

<%@ Register src="../UserControls/usrParentChildProfile.ascx" tagname="usrParentChildProfile" tagprefix="uc1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <%--<script type="text/javascript">
        function fancyClose() {
            parent.$.fancybox.close();
            parent.location.reload();
        }

    </script>--%>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Styles.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
   <link href="../cssfiles/RadStyleSheet.css" rel="stylesheet" type="text/css" />--%>
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="width: 100%; margin-top: 0px; vertical-align: middle; background-color: White;">
            <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px;
                vertical-align: top;">
                <asp:ImageButton ID="btnCloseedit" runat="server" Visible="false" ImageUrl="~/Images/Common/Pagebody/close.png" /></span>
        </div>
        <div class="title-bg">
            <div class="left">
                Approve Fee Concession
            </div>
        </div>
        <table id="Table1" runat="server" cellpadding="5" cellspacing="0" style="width: 100%">
            <tr>
                <td>
                    <% If Me.lblPopError.Text <> "" Then%>
                    <div style="width: 100%; text-align: center !important;">
                        <asp:Label ID="lblPopError" Width="80%" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <% End If%>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <uc1:usrParentChildProfile ID="usrParentChildProfile1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left" class="title-bg">
                    Fee Details
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <table  cellpadding="5" style="border-collapse: collapse;" width="100%" cellspacing="0">
                    <tr>
                        <td class="matters" style="width:48%" align="left">
                           <span class="field-label"> Actual Fees</span>
                        </td>
                       
                        <td align="left" style="width:50%">
                            <asp:Label ID="lblGrossFees" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                           <span class="field-label"> Concession </span>

                        </td>
                       
                        <td align="left">
                            <asp:Label ID="lblConcession" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td align='center' id="trRemarks" runat='server' visible='false'>
                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" CssClass="field-value" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center" id="trButtons" runat='server' visible='false'>
                    <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                    <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                    <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" />
                    <asp:HiddenField id="hfPostBack" runat='server' />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
