Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Version            Date            Author          Change
'1.1                06-sep-2011     Swapna          Bug fix when no date selection available
'1.2                21-dec-2011     Swapna          New report to view employee leave balance details
'1.3                9-apr-2012      Swapna          New report to view WPS issues
'1.4                8-Dec-2013      Swapna          New report to get MOL list
Partial Class Reports_ASPX_rptEMPBSUSalaryReports
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle

                UsrBSUnits1.MenuCode = MainMnu_code
                UsrBSUnits1.ExpandFirstNodeOnStart = True


                UsrVisaBSUnits.MenuCode = MainMnu_code

                UsrVisaBSUnits.ExpandFirstNodeOnStart = True
                Select Case MainMnu_code
                    Case "P150062"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                    Case "P150067" 'V1.3
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        trBSUnit.Visible = True
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        trSelcategory.Visible = False
                        trCategory.Visible = False
                        trEMPABCCAT.Visible = False
                        trProcessingDate.Visible = False
                        treeCat.Visible = False

                    Case "P150068" 'V1.3
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        trBSUnit.Visible = True
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        trSelcategory.Visible = False
                        trCategory.Visible = False
                        trEMPABCCAT.Visible = False
                        trProcessingDate.Visible = False
                        treeCat.Visible = False
                        If Session("sBusper") = False Then
                            lblWorkUnit.Text = GetBSUName(Session("sBsuid"))
                            lblWorkUnit.Visible = True
                            UsrBSUnits1.Visible = False
                        Else
                            lblWorkUnit.Visible = False
                            UsrBSUnits1.Visible = True
                        End If
                        


                End Select
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            Select Case MainMnu_code
                Case "P150062", "P150067", "P150068" 'V1.3
                    FillDeptNames(h_DEPTID.Value)
                    FillCATNames(h_CATID.Value)
                    FillDESGNames(h_DESGID.Value)
                    FillEmpNames(h_EMPID.Value)
                    StoreEMPFilter()
            End Select
            If Not IsPostBack Then
                If MainMnu_code = ("P150062") Or MainMnu_code = ("P150067") Or MainMnu_code = ("P150068") Then   'V1.3,V1.4

                    FillPayYearPayMonth()
                    If Request.UrlReferrer <> Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub h_BSUID_ValueChanged(sender As Object, e As EventArgs) Handles h_BSUID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillBSUNames(h_BSUID.Value)
        'h_BSUID.Value = ""
    End Sub

    Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDeptNames(h_DEPTID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub

    Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDESGNames(h_DESGID.Value)
        'h_DESGID.Value = ""
    End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub
    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal connStr As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connStr)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        'swapna changed
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "P150062"
                GenerateVisaBSUSalarySummaryFormat()
            Case "P150067" 'V1.3
                GenerateWPSOnlyIssues()
            Case "P150068"
                GenerateMOLReportforMonth()
        End Select

    End Sub
    Private Sub GenerateWPSOnlyIssues() 'V1.3
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("@PAYmonth") = ddlPayMonth.SelectedValue
        params("@PAYYEAR") = ddlPayYear.SelectedValue
        params("@BSUID") = UsrBSUnits1.GetSelectedNode("|")
        params("@VisaBSU_ID") = UsrVisaBSUnits.GetSelectedNode("|")
        params("@ReportCaption") = "Employees with issues in WPS generation for the Month of: " & ddlPayMonth.SelectedItem.Text & "-" & ddlPayYear.SelectedItem.Text
        params("@empIds") = h_EMPID.Value
        params("@IMG_BSU_ID") = Session("sBsuid")
        params("@IMG_TYPE") = "LOGO"



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = params
            .reportPath = Server.MapPath("../Rpt/rptWPSOnlyIssues.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("../../../Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection_2()

    End Sub
    ''' <summary>
    ''' MOL report for the month - V1.4
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GenerateMOLReportforMonth() 'V1.3
        Try

       
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("@paymonth") = ddlPayMonth.SelectedValue
            params("@payyear") = ddlPayYear.SelectedValue

           
            If Session("sBusper") = False Then
                params("@BSU_ids") = Session("sBsuid")

            Else
                params("@BSU_ids") = UsrBSUnits1.GetSelectedNode("|")

            End If

            params("@visaBSU_ids") = UsrVisaBSUnits.GetSelectedNode("|")

            params("@ReportCaption") = "MOL Checklist Details: " & ddlPayMonth.SelectedItem.Text & "-" & ddlPayYear.SelectedItem.Text
            params("@empIds") = h_EMPID.Value


            params("@IMG_BSU_ID") = Session("sBsuid")
            params("@IMG_TYPE") = "LOGO"



            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = params
                .reportPath = Server.MapPath("../Rpt/rptCheckMOLlistforMonth.rpt")
            End With
            Session("rptClass") = rptClass
            'Response.Redirect("../../../Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection_2()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub GenerateVisaBSUSalarySummaryFormat()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("[EMPSalaryDetails_VisaBSU]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpVisaBSU_ID As New SqlParameter("@VISA_BSU_ID", SqlDbType.VarChar, 20)
            sqlpVisaBSU_ID.Value = UsrVisaBSUnits.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpVisaBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'If ddlProcessingDate.SelectedValue = "" Then V1.1
            If ddlProcessingDate.Items.Count = 0 Then
                params("PROCESS_DATE") = ""
            Else
                params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            End If
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_VisaBSU.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
            'ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Public Function GetABCCategory() As String
        Dim strABC As String = ""
        If chkEMPABC_A.Checked Then strABC += "A|"
        If chkEMPABC_B.Checked Then strABC += "B|"
        If chkEMPABC_C.Checked Then strABC += "C|"
        Return strABC
    End Function

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub ddlPayMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateProcessingDate()
    End Sub

    Protected Sub ddlPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateProcessingDate()
    End Sub

    Private Sub PopulateProcessingDate()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        str_Sql = "SELECT DISTINCT REPLACE(CONVERT(VARCHAR(11), ESD_LOGDT, 106), ' ', '/') AS ESD_DATE " & _
        "FROM EMPSALARYDATA_D where esd_month = '" & ddlPayMonth.SelectedValue & "' AND ESD_YEAR = '" & ddlPayYear.SelectedValue & _
        "' AND ESD_BSU_ID in ('" & UsrBSUnits1.GetSelectedNode("','") & "')"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing OrElse ds.Tables Is Nothing OrElse ds.Tables(0).Rows.Count <= 1 Then
            trProcessingDate.Visible = False
            Exit Sub
        End If
        trProcessingDate.Visible = True
        ddlProcessingDate.DataSource = ds
        ddlProcessingDate.DataTextField = "ESD_DATE"
        ddlProcessingDate.DataValueField = "ESD_DATE"
        ddlProcessingDate.DataBind()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        PopulateProcessingDate()
        If MainMnu_code = "P150067" Or MainMnu_code = "P150068" Then 'V1.3 
            trProcessingDate.Visible = False
        End If

    End Sub

    Protected Sub chkExcludeProcessDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProcessingDate.Enabled = Not chkExcludeProcessDate.Checked
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class