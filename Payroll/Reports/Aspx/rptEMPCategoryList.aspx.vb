﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Payroll_Reports_Aspx_rptEMPCategoryList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            PopulateTree()

        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then

            If tvBusinessunit.CheckedNodes.Count < 1 Then
                lblError.Text = "At least one school needs to be selected"
            ElseIf tvBusinessunit.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvBusinessunit.CheckedNodes
                    If node.Value.Length < 2 Then
                        lblError.Text = "Atleast one school needs to be selected"
                    Else
                        CallReport()
                    End If
                Next
            End If

        End If




    End Sub
    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT  0 AS BSU_ID,'All' AS BSU_NAME,  COUNT (*)  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.CollapseAll()
    End Sub

    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        

        Dim ds As New DataSet


        Dim PARAM(4) As SqlParameter

        PARAM(0) = New SqlParameter("@LOG_USR", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@MNU_CODE", ViewState("MainMnu_code"))
        PARAM(2) = New SqlParameter("@LOG_BSU", Session("sBsuid"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BSU_POPULATE_USM", PARAM)

        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)



    End Sub
    Private Sub CallReport()


        Dim str_bsu_ids As New StringBuilder

        For Each node As TreeNode In tvBusinessunit.CheckedNodes
            If node.Value.Length > 2 Then
                str_bsu_ids.Append(node.Value)
                str_bsu_ids.Append("|")
            End If
        Next

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", "999998") 'Session("sBSUID")
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_XML", str_bsu_ids.ToString)
        param.Add("@LOG_USR", Session("sUsr_name"))
        param.Add("@MNU_CODE", ViewState("MainMnu_code"))


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("../../REPORTS/RPT/rptEmpCategoryList.rpt")


        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
