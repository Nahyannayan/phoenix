Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports GemBox.Spreadsheet
Imports Telerik.Web.UI
Imports System.Drawing

'Version            Date            Author          Change
'1.1                06-sep-2011     Swapna          Bug fix when no date selection available
'1.2                21-dec-2011     Swapna          New report to view employee leave balance details
'1.3                17-May-2012     Swapna          New report to view Airfare details
'1.4                29-May-2012     Swapna          New report to view Salary slips emailed.
'1.5                03-Jun-2012     Swapna          To add Final Settlement filter for salary reports
'1.6                10-Dec-2013     Swapna          To add Visa BSU WPS processed summary report- MOL purpose -P450050
'1.7                6-Apr-2014      Swapna/Hashmi   To add salary certificate report
'1.8                29-Apr-2014     Swapna          New report for Clist staff status
'1.9                27-Aug-2014     Swapna          Staff list with Photo
Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle
                UsrBSUnits1.MenuCode = MainMnu_code
                Select Case MainMnu_code
                    Case "P159005", "P159010", "P159015", "P159020", "P159025", "P159030", "P159035", "P159040", "P159045", "P159034", "P150023", "P130176", "P450050" 'V1.3 , V1.4,V1.6,V1.9 
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employees Salary Details"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        If MainMnu_code = "P130176" Then
                            imgBankSel.OnClientClick = "GetEMPNAME2();"
                        End If
                        trPrintA3.Visible = (MainMnu_code = "P159005")
                        trShowAllMonths.Visible = False
                        If MainMnu_code = "P450050" Then
                            lblBSUtext.Text = "Visa Business Units"
                            lblrptCaption.Text = "Monthly MOL Checklist-Visa unit wise summary"
                            treeCat.Visible = False
                            trCategory.Visible = False
                            trFinalSettlement.Visible = True
                            chkFF.Text = "Exclude C Category"
                        End If
                    Case "P130078"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        imgBankSel.OnClientClick = "GetEMPNAME();"
                        lblrptCaption.Text = "Employees Leave Balance Details"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = True
                        trSelDepartment.Visible = True
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trAsOnDate.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        treeCat.Visible = False
                        tryear.Visible = True
                        trShowAllMonths.Visible = False
                        FillYear()
                    Case "P159062"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Salary Not Processed"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trBSUTree.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trProcessingDate.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trPrintA3.Visible = (MainMnu_code = "P159005")
                        trShowAllMonths.Visible = False
                    Case "P450046"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Air Fare Details (Not Processed)"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trShowAllMonths.Visible = False
                    Case "H000172"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Air Fare Details "
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        trShowAllMonths.Visible = False

                    Case "P450048"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Air Fare Processed Details "
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = True
                        trShowAllMonths.Visible = False
                        trException.Visible = True

                    Case "H200005", "H200006", "H200010" 'V2 
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Insurance Export"
                        If MainMnu_code = "H200006" Then
                            lblrptCaption.Text = "Staff List with Photo"
                        Else
                            trDataUpdation.Visible = True
                            trInsurance.Visible = True
                        End If
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = True
                        trSelDesignation.Visible = True
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        trShowAllMonths.Visible = False
                        trException.Visible = False
                    Case "P450053" 'Salary Certificate Report
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Salary Certificate Report"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = True
                        trSelDesignation.Visible = True
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        BindSignatory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        Me.trProcessingDate.Visible = False
                        trShowAllMonths.Visible = False
                        trException.Visible = False
                        trSignatory.Visible = True
                    Case "H000185"  'V1.8
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "C List Staff Status"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = True
                        trSelDesignation.Visible = True
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        Me.trProcessingDate.Visible = False
                        trShowAllMonths.Visible = False
                        trException.Visible = False
                        trAsOnDate.Visible = True
                        btnExportExcel.Visible = True
                        If txtAsOnDate.Text = "" Then
                            txtAsOnDate.Text = Format(Today.Date, OASISConstants.DateFormat)
                        End If
                End Select


                If MainMnu_code = "P130176" Then
                    imgBankSel.OnClientClick = "GetEMPNAME2();"
                End If
                trPrintA3.Visible = (MainMnu_code = "P159005")
                If MainMnu_code = "P159020" Then
                    trAsOnDate.Visible = True
                    txtAsOnDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                End If

                If MainMnu_code = "P159005" Or MainMnu_code = "P159020" Then

                    trFinalSettlement.Visible = True 'V1.5
                End If
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            Select Case MainMnu_code
                Case "P152001", "P152002", "P152026", "P152025", "P150025", "P150040", "P150035", "P150026", "P130078", "P159034", "P150023", "P130176", "P450048", "H200005", "H000185", "H200006" 'V1.8,V1.9
                    FillDeptNames(h_DEPTID.Value)
                    FillCATNames(h_CATID.Value)
                    FillDESGNames(h_DESGID.Value)
                    FillEmpNames(h_EMPID.Value)
                    StoreEMPFilter()
                Case "P159030"
                    trWPS.Visible = True
                Case "P159010"
                    trSummary.Visible = True
                Case "P450046"
                    FillEmpNames(h_EMPID.Value)
                Case "P450053" 'Salary Certificate Report
                    FillCATNames(h_CATID.Value)
                    FillDESGNames(h_DESGID.Value)
                    FillEmpNames(h_EMPID.Value)
                    StoreEMPFilter()
            End Select

            If Not IsPostBack Then
                If Not MainMnu_code = "P130078" Then
                    FillPayYearPayMonth()
                    If Request.UrlReferrer <> Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                End If
                If MainMnu_code = "P159034" Or MainMnu_code = "P150023" Or MainMnu_code = "P130176" Or MainMnu_code = "P450046" Or MainMnu_code = "H000172" Or MainMnu_code = "P450048" Or _
                MainMnu_code = "H200005" Or MainMnu_code = "H200006" Then 'V1.9

                    trProcessingDate.Visible = False
                    trEMPABCCAT.Visible = True

                    trEMPName.Visible = True
                    trSelEMPName.Visible = True
                End If
                If MainMnu_code = "P450050" Then
                    trProcessingDate.Visible = False
                    trEMPABCCAT.Visible = False

                    trEMPName.Visible = False
                    trSelEMPName.Visible = False
                End If
            End If

            If Not IsPostBack Then
                If MainMnu_code = "P159005" Or MainMnu_code = "P159020" Then
                    Me.trCombined.Visible = True
                Else
                    Me.trCombined.Visible = False
                End If
            End If

            If Not IsPostBack Then
                If MainMnu_code = "P159005" Then
                    If Session("sBsuid") = "900500" Or Session("sBsuid") = "900501" Or Session("sBsuid") = "900510" Then 'only for business units SCHOOL TRANSPORT SERVICES or BRIGHT BUS TRANSPORT or AL KAWAKEB GARAGE
                        trOnLeaveSeparation.Visible = True
                    Else 'for other business units, call normal leavesalary report
                        trOnLeaveSeparation.Visible = False
                    End If
                End If
            End If

            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExportExcel)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ExporttoExcel(ByVal ds As DataSet) 'V1.8

        Dim dt As DataTable

        dt = ds.Tables(0)
        'swapna added for name format
        Dim strmonth As String, strMinute As String
        strmonth = CStr(Today.Date.Month)
        strMinute = CStr(Now.Minute.ToString)

        If strmonth.Length = 1 Then
            strmonth = "0" + strmonth
        End If
        If strMinute.Length = 1 Then
            strMinute = "0" + strMinute
        End If

        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
        'swapna adding for naming format
        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + "CList_" + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls"


        Try

            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '   ws.HeadersFooters.AlignWithMargins = True
            'V1.6  starts
            'ws.Columns(0).Width = 20 * 256
            'ws.Columns(1).Width = 40 * 256
            'ws.Columns(2).Width = 15 * 256
            Dim colTot As Integer = ds.Tables(0).Columns.Count

            For c As Integer = 0 To colTot - 1
                ' ws.Cells(1, c).Style.Font.Color = Drawing.Color.DarkGreen
                ws.Cells(1, c).Style.Font.Weight = ExcelFont.BoldWeight
                ws.Cells(0, c).Style.Font.Weight = ExcelFont.BoldWeight
                ws.Cells(1, c).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center
                ws.Cells(0, c).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center
            Next
            ws.Cells(0, 6).Value = "As on " + txtAsOnDate.Text.ToString
            ws.Cells(0, 6).Style.Borders.SetBorders(MultipleBorders.Outside, Color.Black, GemBox.Spreadsheet.LineStyle.Thin)

            ws.Cells(0, 6).Style.Font.Weight = ExcelFont.BoldWeight
            ws.Cells(0, 6).Style.WrapText = False
            ws.Columns(6).Width = 20 * 256
            ws.Columns(2).Width = 20 * 256
            ws.Columns(1).Width = 20 * 256
            ws.Columns(1).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right
            ws.Columns(2).Style.HorizontalAlignment = HorizontalAlignmentStyle.Left
            ws.Columns(3).Width = 30 * 256
            ws.Columns(4).Width = 30 * 256
            ws.Columns(5).Width = 30 * 256
            ws.Columns(5).Style.WrapText = False
            Dim cr As CellRange = ws.Rows(0).Cells.GetSubrange("H1", "J1")
            cr.Merged = True
            cr.Value = "Aging"
            cr.Style.Borders.SetBorders(MultipleBorders.Outside, Color.Black, GemBox.Spreadsheet.LineStyle.Thin)


            'V1.6  ends

            ef.Save(tempFileName)
            Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)


            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            Response.BinaryWrite(bytes)
            Response.Flush()

            ' HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
            ' HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            ' HttpContext.Current.Response.Clear()
            ' HttpContext.Current.Response.WriteFile(tempFileName)
            '  HttpContext.Current.Response.Flush()
            ' HttpContext.Current.Response.Close()
            '  HttpContext.Current.Response.End()

            System.IO.File.Delete(tempFileName)

        Catch ex As Exception
        Finally
            Response.Close()
        End Try

    End Sub
    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub
    'V1.7 starts
    Private Sub BindSignatory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT E.EMP_ID, E.EMP_FNAME + ' ' + ISNULL(E.EMP_MNAME,'') + ' ' + ISNULL(E.EMP_LNAME,'') AS Emp_Name FROM dbo.EMPLOYEE_M E WHERE E.EMP_BSU_ID = '" & Session("SBsuid") & "' AND E.EMP_bACTIVE = 1 Order By Emp_Name"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)

        Me.ddlSignatory.DataSource = ds
        Me.ddlSignatory.DataTextField = "Emp_Name"
        Me.ddlSignatory.DataValueField = "Emp_ID"
        Me.ddlSignatory.DataBind()
    End Sub
    Protected Sub rcbSignatory_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "exec dbo.GetLetterSignatory '" & Session("SBsuid") & "','SALCERTF'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)

        rcbSignatory.DataSource = ds
        rcbSignatory.DataBind()


    End Sub
    Protected Sub rcbSignatory_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)

        'set the Text and Value property of every item

        'here you can set any other properties like Enabled, ToolTip, Visible, etc.

        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()

        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("EMP_ID").ToString()

    End Sub
    'V1.7 ends
    Private Sub FillYear() 'V1.2
        Try
            Dim strSql As String
            Dim EMP_ID As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            strSql = "SELECT DISTINCT YEAR(ELS_DTFROM) mYear FROM EMPLEAVESCHEDULE_D WHERE ELS_BSU_ID = '" & Session("sBsuid") & "' and ELS_DTFROM is not null order by YEAR(ELS_DTFROM) "
            fillDropdown(ddlYear, strSql, "mYear", "mYear", str_conn, True)
            If ddlYear.Items.Count > 0 Then
                ddlYear.SelectedIndex = ddlYear.Items.Count - 1
            End If

            'If Not Request.QueryString("SelValue") Is Nothing Then
            '    ddlYear.SelectedIndex = ddlYear.Items.Count - 1
            '    'If ddlYear.Items.FindByValue(Request.QueryString("SelValue").ToString) IsNot Nothing Then
            '    '    ddlYear.SelectedIndex = ddlYear.Items.Count - 1
            '    'End If
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal connStr As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connStr)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        'If addValue.Equals(True) Then
        '    drpObj.Items.Insert(0, " ")
        '    drpObj.Items(0).Value = "0"
        '    drpObj.SelectedValue = "0"
        'End If
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        For i As Integer = 2010 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next

        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
        If MainMnu_code = "H000185" Then
            Session("EMP_SEL_COND") = Session("EMP_SEL_COND") + " AND EMP_ABC='C'"
        End If
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "P159005"
                GenerateSalarySummaryFormat1()
            Case "P159010"
                GenerateSalarySummaryFormat2()
            Case "P159015"
                GenerateSalarySummaryFormat3()
            Case "P159020"
                GenerateSalarySummaryFormat4()
            Case "P159025"
                GenerateSalarySummaryFormat5()
            Case "P159030"
                GenerateSalarySummaryFormat6()
            Case "P159035"
                GenerateSalarySummaryFormat7()
            Case "P159040"
                'GenerateSalarySummaryFormat8()
                'GenerateSalarySummaryFormat9()
                GenerateSalarySummaryFormat11()
            Case "P159045"
                GenerateSalarySummaryFormat13()
                'Case "P159045"
                '    GenerateSalarySummaryFormat9()
                'Case "P159045"
                '    GenerateSalarySummaryFormat11()
            Case "P130078"
                GenerateLeaveBalanceDetails()
            Case "P159034" 'v1.3
                GenerateAirfareForMonthDetails()
            Case "P150023"
                GenerateSalarySlipMailedDetails()
            Case "P159062"
                GenerateSalaryNotProcessedReport()
            Case "P130176"
                GenerateHoldTillDateReport()
            Case "P450046", "H000172"
                GenerateAirFareDetailsReport()
            Case "P450048"
                GenerateAirFareProcessedDetailsReport()
            Case "H200005", "H200010"
                OpenInsuranceExportReport(MainMnu_code)
            Case "P450050"
                GenerateMOLReportforMonth() 'V1.6
            Case "P450053" 'Salary Certificate Report
                If rcbSignatory.SelectedValue = Nothing Then
                    lblError.Text = "Please select a Signatory"
                    Exit Sub
                End If
                GenerateSalaryCertificateReport()
            Case "H000185"
                If txtAsOnDate.Text = "" Then
                    lblError.Text = "Please enter date"
                    Exit Sub
                End If
                GenerateCListStatusReport()  'V1.8
            Case "H200006" 'V1.9
                GenerateStaffListWithPhoto()
        End Select

    End Sub

    Protected Sub GenerateStaffListWithPhoto()

        Session("StaffBSUIds") = UsrBSUnits1.GetSelectedNode("|")
        Session("StaffEmpIds") = h_EMPID.Value
        Session("StaffDesIds") = h_DESGID.Value
        Response.Redirect("~/Payroll/empStaffListWithPhoto.aspx?Catid=" & UsrTreeView1.GetSelectedNode("|") & "&ABCCat=" & GetABCCategory())
    End Sub
    Private Sub GenerateCListStatusReport() 'V1.8
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[GetCListStatusAgingExport]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEmp_ID As New SqlParameter("@Empid", SqlDbType.VarChar)
            sqlpEmp_ID.Value = Me.h_EMPID.Value
            cmd.Parameters.Add(sqlpEmp_ID)

            Dim sqlpCAT_ID As New SqlParameter("@Cat_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpDes_ID As New SqlParameter("@Des_ID", SqlDbType.VarChar)
            sqlpDes_ID.Value = Me.h_DESGID.Value
            cmd.Parameters.Add(sqlpDes_ID)

            Dim sqlpAsonDt As New SqlParameter("@AsonDt", SqlDbType.DateTime)
            sqlpAsonDt.Value = txtAsOnDate.Text
            cmd.Parameters.Add(sqlpAsonDt)


            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'repSource.SubReport = repSourceSubRep
            params("userName") = Session("sUsr_name")

            params("AsonDate") = Format(txtAsOnDate.Text, OASISConstants.DateFormat)


            repSource.Parameter = params
            repSource.Command = cmd
            repSource.DisplayGroupTree = False
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptCListStaffStatus.rpt"
            'repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryCertificate_BKP.rpt"

            Session("ReportSource") = repSource

            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GenerateSalaryCertificateReport() 'V1.7
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[getEmpSalaryCertificate]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEmp_ID As New SqlParameter("@Emp_ID", SqlDbType.VarChar, 20)
            sqlpEmp_ID.Value = Me.h_EMPID.Value
            cmd.Parameters.Add(sqlpEmp_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpDes_ID As New SqlParameter("@Des_ID", SqlDbType.VarChar, 20)
            sqlpDes_ID.Value = Me.h_DESGID.Value
            cmd.Parameters.Add(sqlpDes_ID)

            Dim sqlpSignatory_Emp_ID As New SqlParameter("@Signatory_Emp_ID", SqlDbType.Int)
            sqlpSignatory_Emp_ID.Value = rcbSignatory.SelectedValue  'Me.ddlSignatory.SelectedValue
            cmd.Parameters.Add(sqlpSignatory_Emp_ID)




            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'repSource.SubReport = repSourceSubRep
            params("userName") = Session("sUsr_name")



            repSource.Parameter = params
            repSource.Command = cmd
            repSource.DisplayGroupTree = False
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryCertificate.rpt"
            'repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryCertificate_BKP.rpt"

            Session("ReportSource") = repSource

            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GenerateMOLReportforMonth() 'V1.6
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("@paymonth") = ddlPayMonth.SelectedValue
        params("@payyear") = ddlPayYear.SelectedValue

        params("@BSU_ids") = ""


        params("@visaBSU_ids") = UsrBSUnits1.GetSelectedNode("|")
        params("@ReportCaption") = "MOL Checklist Summary: " & ddlPayMonth.SelectedItem.Text & "-" & ddlPayYear.SelectedItem.Text
        params("@empIds") = h_EMPID.Value


        params("@IMG_BSU_ID") = Session("sBsuid")
        params("@IMG_TYPE") = "LOGO"



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = params
            If chkFF.Checked = False Then
                .reportPath = Server.MapPath("../Rpt/rptMOLlistBSUwise.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptMOLlistBSUwise_ExcludeC.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("../../../Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection_2()
    End Sub
    Protected Sub OpenInsuranceExportReport(Optional ByVal mnucode = "")
        Dim InsuCate As Integer = 0

        Session("InsuBSUIds") = UsrBSUnits1.GetSelectedNode("|")
        Session("InsuEmpIds") = h_EMPID.Value
        Session("InsuDesIds") = h_DESGID.Value

        If rbYes.Checked = True Then
            Session("InsuCategory") = 0
        ElseIf rbNo.Checked = True Then
            Session("InsuCategory") = 1
        Else
            Session("InsuCategory") = 2
        End If

        If rbCompleted.Checked = True Then
            Session("Status") = 0
        ElseIf rbPartial.Checked = True Then
            Session("Status") = 1
        Else
            Session("Status") = 2
        End If

        If mnucode = "H200010" Then
            Response.Redirect("~/Payroll/empInsuranceExportExcelNew.aspx?Catid=" & UsrTreeView1.GetSelectedNode("|") & "&ABCCat=" & GetABCCategory())
        Else
            Response.Redirect("~/Payroll/empInsuranceExportExcel.aspx?Catid=" & UsrTreeView1.GetSelectedNode("|") & "&ABCCat=" & GetABCCategory())
        End If



    End Sub

    Protected Sub GenerateHoldTillDateReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("rptHoldTillNowDetails", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@bsu_id", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@year", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)

            Dim sqlpMonth As New SqlParameter("@month", SqlDbType.VarChar, 5)
            sqlpMonth.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptHoldTillNowReport.rpt"

            Session("ReportSource") = repSource
            '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub GenerateSalarySlipMailedDetails()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("rptGetSalaryEmailsSentStatus", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_id", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)


            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@year", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)


            Dim sqlpMonth As New SqlParameter("@month", SqlDbType.VarChar, 5)
            sqlpMonth.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryEmailsSendStatus.rpt"

            Session("ReportSource") = repSource
            '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Protected Sub GenerateAirfareForMonthDetails() 'V1.3
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("RptAirfareAmountForBSU", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@bsu_ID", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)


            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@PayYear", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)


            Dim sqlpMonth As New SqlParameter("@PayMonth", SqlDbType.VarChar, 5)
            sqlpMonth.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)


            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")
            params("rptCapt") = ddlPayMonth.SelectedItem.Text & "-" & ddlPayYear.SelectedValue

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetAirfareDetails.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GenerateLeaveBalanceDetails()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("getRptEmployeeLeaveDetails", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpABC_DPT As New SqlParameter("@DPT_ID", SqlDbType.VarChar, 5000)
            sqlpABC_DPT.Value = h_DEPTID.Value
            cmd.Parameters.Add(sqlpABC_DPT)


            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = "" 'h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@year", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)


            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/RptGetEmployeeLeaveDetails.rpt"

            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try

    End Sub

    Private Sub GenerateSalarySummaryFormat13()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format13]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'V1.1
            '  params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")

            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends

            params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format6_SUB.rpt"

            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat11()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try


            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format11]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAY_MONTH") = ddlPayMonth.SelectedItem.Text
            params("PAY_YEAR") = ddlPayYear.SelectedItem.Text
            'V1.1
            ' params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            'V1.1
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format11.rpt"

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat9()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format9]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format9.rpt"

            Session("ReportSource") = repSource
            '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat7()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format7]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            'V1.1
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass

            Dim objConn1 As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmdSub1 As New SqlCommand("[EMPSalaryDetails_Format7]", objConn1)
            cmdSub1.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTHsub As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTHsub.Value = ddlPayMonth.SelectedValue
            cmdSub1.Parameters.Add(sqlpPAYMONTHsub)

            Dim sqlpPAYYEARsub As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEARsub.Value = ddlPayYear.SelectedValue
            cmdSub1.Parameters.Add(sqlpPAYYEARsub)

            Dim sqlpBSU_IDsub As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_IDsub.Value = UsrBSUnits1.GetSelectedNode("|")
            cmdSub1.Parameters.Add(sqlpBSU_IDsub)

            Dim sqlpCAT_IDsub As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_IDsub.Value = UsrTreeView1.GetSelectedNode()
            cmdSub1.Parameters.Add(sqlpCAT_IDsub)

            Dim sqlpABC_CATsub As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CATsub.Value = GetABCCategory()
            cmdSub1.Parameters.Add(sqlpABC_CATsub)

            Dim sqlpPROCESS_DATEsub As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATEsub.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATEsub.Value = DBNull.Value
            End If
            cmdSub1.Parameters.Add(sqlpPROCESS_DATEsub)


            repSourceSubRep(0).Command = cmdSub1


            'repSourceSubRep(1) = New MyReportClass
            'Dim cmdSub2 As SqlCommand = cmd.Clone()
            'repSourceSubRep(1).Command = cmdSub2

            repSource.SubReport = repSourceSubRep
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format7.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat6()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try


            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format6]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpWPS_DET As New SqlParameter("@WPS_FLAG", SqlDbType.Int)
            sqlpWPS_DET.Value = GetWPSFlag()
            cmd.Parameters.Add(sqlpWPS_DET)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            ' params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            'V1.1
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass

            Dim objConn1 As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmdSub1 As New SqlCommand("[EMPSalaryDetails_Format6_SUB]", objConn1)
            cmdSub1.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTHsub As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTHsub.Value = ddlPayMonth.SelectedValue
            cmdSub1.Parameters.Add(sqlpPAYMONTHsub)

            Dim sqlpPAYYEARsub As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEARsub.Value = ddlPayYear.SelectedValue
            cmdSub1.Parameters.Add(sqlpPAYYEARsub)

            Dim sqlpBSU_IDsub As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_IDsub.Value = UsrBSUnits1.GetSelectedNode("|")
            cmdSub1.Parameters.Add(sqlpBSU_IDsub)

            Dim sqlpCAT_IDsub As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_IDsub.Value = UsrTreeView1.GetSelectedNode()
            cmdSub1.Parameters.Add(sqlpCAT_IDsub)

            Dim sqlpABC_CATsub As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CATsub.Value = GetABCCategory()
            cmdSub1.Parameters.Add(sqlpABC_CATsub)

            Dim sqlpPROCESS_DATEsub As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATEsub.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATEsub.Value = DBNull.Value
            End If
            cmdSub1.Parameters.Add(sqlpPROCESS_DATEsub)


            Dim sqlpWPS_DET_sub As New SqlParameter("@WPS_FLAG", SqlDbType.Int)
            sqlpWPS_DET_sub.Value = GetWPSFlag()
            cmdSub1.Parameters.Add(sqlpWPS_DET_sub)

            repSourceSubRep(0).Command = cmdSub1

            repSource.SubReport = repSourceSubRep

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format6.rpt"

            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Public Function GetWPSFlag() As Integer
        If radAll.Checked Then
            Return 0
        ElseIf radWithWPS.Checked Then
            Return 1
        ElseIf radWithoutWPS.Checked Then
            Return 2
        End If
    End Function

    Private Sub GenerateSalarySummaryFormat5()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format5]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            'V1.1
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format5.rpt"

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat4()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        If Me.chkFF.Checked AndAlso Me.chkCombined.Checked Then
            Me.lblError.Text = "Final Settlement and Combined options cannot be used together"
            Exit Sub
        End If

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format4]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpASONDATE As New SqlParameter("@ASONDATE", SqlDbType.DateTime)
            sqlpASONDATE.Value = txtAsOnDate.Text
            cmd.Parameters.Add(sqlpASONDATE)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            Dim sqlpFF As New SqlParameter("@IsFinalSettlement", SqlDbType.Bit)  'V1.5
            sqlpFF.Value = chkFF.Checked
            cmd.Parameters.Add(sqlpFF)

            Dim sqlpCombined As New SqlParameter("@Combined", SqlDbType.Bit)
            sqlpCombined.Value = Me.chkCombined.Checked
            cmd.Parameters.Add(sqlpCombined)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("EMPABC") = GetABCCategory()
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            params("IsFinalSettlement") = chkFF.Checked
            params("Combined") = Me.chkCombined.Checked
            'V1.1
            'params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends


            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format4.rpt"

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GenerateAirFareProcessedDetailsReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[spGetEmployeeAirFareProcessedDetails]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@MONTH", SqlDbType.Int)
            If MainMnu_code = "H000172" Then
                sqlpPAYMONTH.Value = System.DBNull.Value
            Else
                sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            End If

            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@YEAR", SqlDbType.Int)
            If MainMnu_code = "H000172" Then
                sqlpPAYYEAR.Value = System.DBNull.Value
            Else
                sqlpPAYYEAR.Value = ddlPayYear.SelectedValue

            End If

            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@Category", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABCCategory", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpEmpNames As New SqlParameter("@EmployeeName", SqlDbType.VarChar)
            sqlpEmpNames.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEmpNames)

            Dim sqlpChkMonth As New SqlParameter("@chkAll", SqlDbType.Bit)
            If trShowAllMonths.Visible = False Then
                sqlpChkMonth.Value = True
            Else
                sqlpChkMonth.Value = chkShowAllMonths.Checked
            End If
            cmd.Parameters.Add(sqlpChkMonth)

            Dim sqlHRForm As New SqlParameter("@HRForm", SqlDbType.Bit)
            If MainMnu_code = "H000172" Then
                sqlHRForm.Value = True
            Else
                sqlHRForm.Value = False
            End If
            cmd.Parameters.Add(sqlHRForm)

            Dim sqlpException As New SqlParameter("@IsException", SqlDbType.Bit)
            sqlpException.Value = chkException.Checked
            cmd.Parameters.Add(sqlpException)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("period") = ddlPayMonth.SelectedItem.Text & "," & ddlPayYear.SelectedItem.Text
            params("Isexception") = chkException.Checked


            repSource.Parameter = params
            repSource.Command = cmd

            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeAirFareProcessedDetails.rpt"
            repSource.IncludeBSUImage = True

            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateAirFareDetailsReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[spGetEmployeeTotalAirFareDetails]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@MONTH", SqlDbType.Int)
            If MainMnu_code = "H000172" Then
                sqlpPAYMONTH.Value = System.DBNull.Value
            Else
                sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            End If

            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@YEAR", SqlDbType.Int)
            If MainMnu_code = "H000172" Then
                sqlpPAYYEAR.Value = System.DBNull.Value
            Else
                sqlpPAYYEAR.Value = ddlPayYear.SelectedValue

            End If

            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@Category", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABCCategory", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpEmpNames As New SqlParameter("@EmployeeName", SqlDbType.VarChar)
            sqlpEmpNames.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEmpNames)

            Dim sqlpChkMonth As New SqlParameter("@chkAll", SqlDbType.Bit)
            If trShowAllMonths.Visible = False Then
                sqlpChkMonth.Value = True
            Else
                sqlpChkMonth.Value = chkShowAllMonths.Checked
            End If
            cmd.Parameters.Add(sqlpChkMonth)

            Dim sqlHRForm As New SqlParameter("@HRForm", SqlDbType.Bit)
            If MainMnu_code = "H000172" Then
                sqlHRForm.Value = True
            Else
                sqlHRForm.Value = False
            End If
            cmd.Parameters.Add(sqlHRForm)


            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")

            If MainMnu_code = "P450046" Then
                params("period") = ddlPayMonth.SelectedItem.Text & "," & ddlPayYear.SelectedItem.Text
            End If

            repSource.Parameter = params
            repSource.Command = cmd
            If MainMnu_code = "H000172" Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptAllEmployeeAirFareDetails.rpt"
            Else
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeAirFareNotProcessedDetails.rpt"
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat3()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format2]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)

            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("CurrentDate") = DateTime.Now

            'params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            'V1.1
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends

            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format3.rpt"

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat2()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format2]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("CurrentDate") = DateTime.Now
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            'params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            'V1.1
            If (trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0) Then
                params("PROCESS_DATE") = ddlProcessingDate.SelectedItem.Text
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends

            repSource.Parameter = params
            repSource.Command = cmd
            If chkSummary.Checked Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format2_Summary.rpt"
            Else
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format2.rpt"
            End If
            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat1()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        If Me.chkFF.Checked AndAlso Me.chkCombined.Checked Then
            Me.lblError.Text = "Final Settlement and Combined options cannot be used together"
            Exit Sub
        End If

        Try

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format1]", objConn)

            If Session("sBsuid") = "900500" Or Session("sBsuid") = "900501" Or Session("sBsuid") = "900510" Then 'only for business units SCHOOL TRANSPORT SERVICES or BRIGHT BUS TRANSPORT or AL KAWAKEB GARAGE
                If Me.chkOnLeaveSeparation.Checked = True Then
                    cmd = New SqlCommand("[EMPSalaryDetails_Format1_STS]", objConn)
                Else
                    cmd = New SqlCommand("[EMPSalaryDetails_Format1]", objConn)
                End If
            Else 'for other business units, call normal leavesalary report
                cmd = New SqlCommand("[EMPSalaryDetails_Format1]", objConn)
            End If

            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0 Then
                sqlpPROCESS_DATE.Value = ddlProcessingDate.SelectedValue
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            Dim sqlpFF As New SqlParameter("@IsFinalSettlement", SqlDbType.Bit)  'V1.5
            sqlpFF.Value = chkFF.Checked
            cmd.Parameters.Add(sqlpFF)

            Dim sqlpCombined As New SqlParameter("@Combined", SqlDbType.Bit)
            sqlpCombined.Value = Me.chkCombined.Checked
            cmd.Parameters.Add(sqlpCombined)


            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'If ddlProcessingDate.SelectedValue = "" Then V1.1
            If ddlProcessingDate.Items.Count = 0 Then
                params("PROCESS_DATE") = ""
            Else
                params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            End If
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            params("IsFinalSettlement") = chkFF.Checked
            repSource.Parameter = params
            repSource.Command = cmd

            If chkPrintA3.Checked Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1_A3.rpt"
                If Session("sBsuid") = "900500" Or Session("sBsuid") = "900501" Or Session("sBsuid") = "900510" Then 'only for business units SCHOOL TRANSPORT SERVICES or BRIGHT BUS TRANSPORT or AL KAWAKEB GARAGE
                    If Me.chkOnLeaveSeparation.Checked = True Then
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1_A3_STS.rpt"
                    Else
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1_A3.rpt"
                    End If
                Else 'for other business units, call normal leavesalary report
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1_A3.rpt"
                End If
            Else
                If Session("sBsuid") = "900500" Or Session("sBsuid") = "900501" Or Session("sBsuid") = "900510" Then 'only for business units SCHOOL TRANSPORT SERVICES or BRIGHT BUS TRANSPORT or AL KAWAKEB GARAGE
                    If Me.chkOnLeaveSeparation.Checked = True Then
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1_STS.rpt"
                    Else
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1.rpt"
                    End If
                Else 'for other business units, call normal leavesalary report
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1.rpt"
                End If
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()


            'Session("ReportSource") = repSource
            ''  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            'ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub GenerateSalaryNotProcessedReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("RPT_EmployeeSalaryNotProcessed", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpYear As New SqlParameter("@PAYYEAR", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)


            Dim sqlpMonth As New SqlParameter("@PAYMONTH", SqlDbType.VarChar, 5)
            sqlpMonth.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@CAT_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")
            params("BSU_NAME") = Session("bsu_name")
            params("REPORTCAPTION") = "Employee Salary Not Processed (For the Month " & ddlPayMonth.SelectedItem.Text & " - " & ddlPayYear.SelectedItem.Text & ")"
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeSalaryNotProcessed.rpt"

            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Public Function GetABCCategory() As String

        Dim strABC As String = ""

        If chkEMPABC_A.Checked Then strABC += "A|"
        If chkEMPABC_B.Checked Then strABC += "B|"
        If chkEMPABC_C.Checked Then strABC += "C|"

        Return strABC
    End Function

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub ddlPayMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateProcessingDate()
    End Sub

    Protected Sub ddlPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateProcessingDate()
    End Sub

    Private Sub PopulateProcessingDate()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        str_Sql = "SELECT DISTINCT REPLACE(CONVERT(VARCHAR(11), ESD_LOGDT, 106), ' ', '/') AS ESD_DATE " & _
        "FROM EMPSALARYDATA_D where esd_month = '" & ddlPayMonth.SelectedValue & "' AND ESD_YEAR = '" & ddlPayYear.SelectedValue & _
        "' AND ESD_BSU_ID in ('" & UsrBSUnits1.GetSelectedNode("','") & "')"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing OrElse ds.Tables Is Nothing OrElse ds.Tables(0).Rows.Count <= 1 Then
            trProcessingDate.Visible = False
            Exit Sub
        End If
        trProcessingDate.Visible = True
        ddlProcessingDate.DataSource = ds
        ddlProcessingDate.DataTextField = "ESD_DATE"
        ddlProcessingDate.DataValueField = "ESD_DATE"
        ddlProcessingDate.DataBind()
    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    'If Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")) = "H000185" Then
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    '    ' End If
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        PopulateProcessingDate()
        If MainMnu_code = "P159034" Or MainMnu_code = "P150023" Or MainMnu_code = "P159062" Or MainMnu_code = "P130176" Or MainMnu_code = "P450046" Or MainMnu_code = "H000172" Or MainMnu_code = "P450048" Or _
        MainMnu_code = "H200005" Or MainMnu_code = "P450050" Or MainMnu_code = "P450053" Or MainMnu_code = "H000185" Or MainMnu_code = "H200006" Or MainMnu_code = "H200010" Then 'V1.4,V1.9 
            trProcessingDate.Visible = False
        End If
    End Sub
    Protected Sub chkExcludeProcessDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProcessingDate.Enabled = Not chkExcludeProcessDate.Checked
    End Sub
    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click 'V1.8
        If txtAsOnDate.Text = "" Then
            lblError.Text = "Please enter date"
            Exit Sub
        End If
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("[GetCListStatusAgingExportExcel]", objConn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim sqlparam(4) As SqlParameter  'V1.4
            sqlparam(0) = Mainclass.CreateSqlParameter("@BSU_ID", UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
            sqlparam(1) = Mainclass.CreateSqlParameter("@Empid", h_EMPID.Value, SqlDbType.VarChar)
            sqlparam(2) = Mainclass.CreateSqlParameter("@Cat_ID", UsrTreeView1.GetSelectedNode("|"), SqlDbType.VarChar)
            sqlparam(3) = Mainclass.CreateSqlParameter("@Des_ID", h_DESGID.Value, SqlDbType.VarChar)
            sqlparam(4) = Mainclass.CreateSqlParameter("@AsonDt", txtAsOnDate.Text, SqlDbType.DateTime)

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetCListStatusAgingExportExcel", sqlparam)
            ExporttoExcel(ds)
        Catch ex As Exception
            lblError.Text = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        If Not h_EMPID.Value Is Nothing Then
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub


    Protected Sub txtDesgName_TextChanged(sender As Object, e As EventArgs)
        txtDesgName.Text = ""
        If Not h_DESGID.Value Is Nothing Then
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub
    Protected Sub txtBSUName_TextChanged(sender As Object, e As EventArgs)
        txtBSUName.Text = ""
        FillBSUNames(h_BSUID.Value)
    End Sub
    Protected Sub txtDeptName_TextChanged(sender As Object, e As EventArgs)
        txtDeptName.Text = ""
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub txtCatName_TextChanged(sender As Object, e As EventArgs)
        txtCatName.Text = ""
        FillCATNames(h_CATID.Value)
    End Sub
End Class