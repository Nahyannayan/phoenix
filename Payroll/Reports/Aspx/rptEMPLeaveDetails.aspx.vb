Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String
    'Version            Date            Author          Change
    '1.1                16-Jun-2011     Swapna          added menu and code for Leave salary Details-P150049
    '1.2                26-Jun-2011     Swapna          LOP after leave report link added-Leave Salary Details-Format I-P150051
    '1.3                27-Jul-2011     Swapna          As on Date value det to "31/Dec/YYYY" to vies leave details
    '1.4                10-Jun-2011     Swapna          Leave salary report changes
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value = Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            FillDeptNames(h_DEPTID.Value)
            FillCATNames(h_CATID.Value)
            FillDESGNames(h_DESGID.Value)
            FillEmpNames(h_EMPID.Value)
            StoreEMPFilter()
            'imgBankSel.Attributes.Add("OnClick", "Return GetEMPNAME();")
            'If h_EMPID.Value <> Nothing And h_EMPID.Value <> "" And h_EMPID.Value <> "undefined" Then
            '    FillACTIDs(h_EMPID.Value)
            '    'h_BSUID.Value = ""
            'End If

            If Not IsPostBack Then
                trProcessingDate.Visible = False
                chkViewSummary.Visible = True
                trChkViewSummary.Visible = True
                Select Case MainMnu_code
                    Case "P154005", "P154030"
                        lblrptCaption.Text = "Expense Contribution By Category"
                        chkViewSummary.Text = "Compare Business units"
                        HideShowAllROWS(False)
                    Case "P154010", "P154025"
                        lblrptCaption.Text = "Expense Contribution By Department"
                        chkViewSummary.Text = "Compare Business units"
                        HideShowAllROWS(False)
                    Case "P154015"
                        lblrptCaption.Text = "Expense Contribution By Department(Comparison)"
                        trChkViewSummary.Visible = False
                        chkViewSummary.Visible = False
                        HideShowAllROWS(False)
                    Case "P154020"
                        lblrptCaption.Text = "Expense Contribution By Category(Comparison)"
                        trChkViewSummary.Visible = False
                        chkViewSummary.Visible = False
                        HideShowAllROWS(False)
                    Case "P150005"
                        chkViewSummary.Visible = False
                    Case "P150049", "P150051"   'V1.1  ,V1.2
                        trSelDepartment.Visible = False
                        trSelDesignation.Visible = False
                        trChkViewSummary.Visible = False
                        trEMPABCCat.Visible = False
                    Case "P150007"
                        lblrptCaption.Text = "Employee Leave Details"
                        trChkViewSummary.Visible = False
                        chkViewSummary.Visible = False
                        lblFromDate.Text = "As On Date"
                        lblToDate.Visible = False
                        imgToDate.Visible = False
                        txtToDate.Visible = False
                    Case "H000205"
                        lblrptCaption.Text = "Employee Details"
                        trDateSel.Visible = False
                        lnkExporttoexcel.Visible = True
                    Case "P150017"
                        lblrptCaption.Text = "Employee Leave Details"
                        trChkViewSummary.Visible = False
                        chkViewSummary.Visible = False
                        trEMPABCCat.Visible = False
                        trChkreportName.Visible = True
                    Case "P150027"
                        lblrptCaption.Text = "Employee Monthwise Leave Details"
                        trChkViewSummary.Visible = False
                        chkViewSummary.Visible = False
                        trEMPABCCat.Visible = False
                End Select
                If MainMnu_code = "P150063" Then
                    imgBankSel.Attributes.Remove("OnClick")
                    imgBankSel.Attributes.Add("OnClick", "GetEMPNAME_ALL(); return false;")
                    'Else
                    '    imgBankSel.Attributes.Remove("OnClick")
                    '    imgBankSel.Attributes.Add("OnClick", "Return GetEMPNAME()")
                End If
                If MainMnu_code = "P150049" Then 'V1.4
                    trEMPABCCat.Visible = True
                    trProcessingDate.Visible = True
                    lblrptCaption.Text = "Employee Leave Salary Details"
                End If
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If MainMnu_code = "P150007" Then
                    txtFromDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = "31/Dec/" & Today.Year.ToString    'V1.3
                    txtToDate.Text = txtFromDate.Text

                ElseIf Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                    txtFromDate.Text = Cache("fromDate")
                    txtToDate.Text = Cache("toDate")
                Else
                    txtToDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
                End If

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
                If MainMnu_code = "P150049" Then 'V1.4
                    txtFromDate.AutoPostBack = True
                    txtToDate.AutoPostBack = True
                    PopulateProcessingDate()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub HideShowAllROWS(ByVal val As Boolean)
        trDepartment.Visible = val
        trCategory.Visible = val
        trDesignation.Visible = val
        trEMPName.Visible = val
        trSelDepartment.Visible = val
        trSelcategory.Visible = val
        trSelDesignation.Visible = val
        trSelEMPName.Visible = val
        trEMPABCCat.Visible = val
    End Sub
    Private Sub PopulateProcessingDate()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As DataSet
            str_Sql = "SELECT DISTINCT REPLACE(CONVERT(VARCHAR(11), ESD_LOGDT, 106), ' ', '/') AS ESD_DATE,cast(REPLACE(CONVERT(VARCHAR(11), ESD_LOGDT, 106), ' ', '/')as datetime) LOGDT  " & _
            "FROM EMPSALARYDATA_D where ESD_LOGDT between  '" & txtFromDate.Text & "' and '" & txtToDate.Text & "'" & _
            " AND ESD_BSU_ID in (select * from fnsplitme('" & h_BSUID.Value & "','|')) ORDER BY cast(REPLACE(CONVERT(VARCHAR(11), ESD_LOGDT, 106), ' ', '/')as datetime) "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds Is Nothing OrElse ds.Tables Is Nothing OrElse ds.Tables(0).Rows.Count < 1 Then
                trProcessingDate.Visible = False
                Exit Sub
            End If
            trProcessingDate.Visible = True
            ddlProcessingDate.DataSource = ds
            ddlProcessingDate.DataTextField = "ESD_DATE"
            ddlProcessingDate.DataValueField = "ESD_DATE"
            ddlProcessingDate.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case MainMnu_code
            Case "P154005", "P154030"
                GenerateExpenceContribution("CAT")
            Case "P154010", "P154025"
                GenerateExpenceContribution("DEP")
            Case "P154015"
                GenerateStaffCostAnalysis("DEP")
            Case "P154020"
                GenerateStaffCostAnalysis("CAT")
            Case "P150049"  'V1.1
                GenerateLeaveSalaryDetails()
            Case "P150051"   'V1.2
                GenerateLeaveSalaryDetailsFormat1()
            Case "P150007"
                GenerateEmployeeLeaveDetails()
            Case "H000205"
                GenerateEmployeeDetails()
            Case "P150017", "P150027"
                GenerateAllEmpLeaveDetails()
            Case Else
                'GenerateLeaveDetails()
                GenerateAttendanceLeaveDetails()
        End Select
    End Sub
    Private Sub GenerateEmployeeLeaveDetails()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A|"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "B|"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "C|"
            End If
            Dim strSelectedNodes As String = h_BSUID.Value
            Dim str_bsu_Segment As String = ""
            Dim str_bsu_shortname As String = ""
            Dim str_bsu_CURRENCY As String = ""
            getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, str_bsu_shortname, str_bsu_CURRENCY)

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            Dim objConn As New SqlConnection(str_conn)
            cmd.Connection = objConn
            cmd.CommandText = "RPT_GetEmployeeLeaveDetails "
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", h_BSUID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@DPT_ID", h_DEPTID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@DES_ID", h_DESGID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@CAT_ID", h_CATID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@EMP_ABC", strABC, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_ID", h_EMPID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            sqlParam(6) = Mainclass.CreateSqlParameter("@FromDate", txtFromDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(6))
            cmd.Connection = objConn

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("Month") = "January"
            params("fromDate") = txtFromDate.Text
            params("toDate") = txtToDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            params("Bsu_Segments") = str_bsu_Segment
            repSource.Parameter = params
            repSource.Command = cmd
            params("reportCaption") = "Employee Leave Details"
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeLeaveDetails.rpt"
            'repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeLeaveDetails_old.rpt"

            Session("ReportSource") = repSource
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('../../../Reports/ASPX Report/RptviewerNew.aspx');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "','_blank');", True)
            'Response.Redirect("../../../Reports/ASPX Report/RptviewerNew.aspx", True)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateLeaveSalaryDetails()

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If



            Dim strBsuid As String = " "
            If grdBSU.Rows.Count > 0 Then
                'For Each drow As GridViewRow In grdBSU.Rows
                strBsuid = h_BSUID.Value.ToString 'strBsuid + "," + (TryCast(drow.FindControl("lblBSUID"), Label)).Text
                'Next
            Else
                strBsuid = Session("sBsuid").ToString
            End If

            Dim strCatIds As String = " "
            If gvCat.Rows.Count > 0 Then
                'For Each drow As GridViewRow In gvCat.Rows
                strCatIds = h_CATID.Value.ToString ' strCatIds + "," + (TryCast(drow.FindControl("lblCATID"), Label)).Text
                'Next
            Else
                strCatIds = " "
            End If

            Dim strEmpIDs As String = " "
            If gvEMPName.Rows.Count > 0 Then
                'For Each drow As GridViewRow In gvEMPName.Rows
                strEmpIDs = h_EMPID.Value.ToString  'strEmpIDs + "," + (TryCast(drow.FindControl("lblEMPID"), Label)).Text
                'Next
            Else
                strEmpIDs = " "
            End If

            Dim str_Sql As String
            If trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked Then
                str_Sql = "EXEC Emp_LEaveSalaryReport '" & txtFromDate.Text & "','" & txtToDate.Text & "','" & strBsuid & "' ,'" & strEmpIDs & "','" & strCatIds & "','" & strABC & "','" & CDate(ddlProcessingDate.SelectedValue).ToString("dd/MMM/yyyy") & "'"
            Else
                str_Sql = "EXEC Emp_LEaveSalaryReport '" & txtFromDate.Text & "','" & txtToDate.Text & "','" & strBsuid & "' ,'" & strEmpIDs & "','" & strCatIds & "','" & strABC & "'"
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("@FromDt") = txtFromDate.Text
                'params("@ToDt") = txtToDate.Text
                ''params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                'If chkViewSummary.Checked Then
                '    params("reportCaption") = "Leave Details"
                '    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"
                'Else
                params("reportCaption") = "Leave Salary Details For the period from " + txtFromDate.Text + " to " + txtToDate.Text
                If Session("sBsuid") = "900500" Or Session("sBsuid") = "900501" Or Session("sBsuid") = "900510" Then 'only for business units SCHOOL TRANSPORT SERVICES or BRIGHT BUS TRANSPORT or AL KAWAKEB GARAGE
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptLeaveSalary_STS.rpt"
                Else 'for other business units, call normal leavesalary report
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptLeaveSalary.rpt"
                End If

                'End If
                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection_2()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'V1.2
    Private Sub GenerateLeaveSalaryDetailsFormat1()

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If



            Dim strBsuid As String = " "
            If grdBSU.Rows.Count > 0 Then
                'For Each drow As GridViewRow In grdBSU.Rows
                strBsuid = h_BSUID.Value.ToString 'strBsuid + "," + (TryCast(drow.FindControl("lblBSUID"), Label)).Text
                'Next
            Else
                strBsuid = Session("sBsuid").ToString
            End If

            Dim strCatIds As String = " "
            If gvCat.Rows.Count > 0 Then
                'For Each drow As GridViewRow In gvCat.Rows
                strCatIds = h_CATID.Value.ToString ' strCatIds + "," + (TryCast(drow.FindControl("lblCATID"), Label)).Text
                'Next
            Else
                strCatIds = " "
            End If

            Dim strEmpIDs As String = " "
            If gvEMPName.Rows.Count > 0 Then
                'For Each drow As GridViewRow In gvEMPName.Rows
                strEmpIDs = h_EMPID.Value.ToString  'strEmpIDs + "," + (TryCast(drow.FindControl("lblEMPID"), Label)).Text
                'Next
            Else
                strEmpIDs = " "
            End If


            Dim str_Sql As String = "EXEC Emp_LEaveSalaryReport_Format1 '" & txtFromDate.Text & "','" & txtToDate.Text & "','" & strBsuid & "' ,'" & strEmpIDs & "','" & strCatIds & "'"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("@FromDt") = txtFromDate.Text
                'params("@ToDt") = txtToDate.Text
                ''params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                'If chkViewSummary.Checked Then
                '    params("reportCaption") = "Leave Details"
                '    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"
                'Else
                params("reportCaption") = "For the period from " + txtFromDate.Text + " to " + txtToDate.Text
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptLLeaveSalaryFormat1.rpt"
                'End If
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GenerateStaffCostAnalysis(ByVal type As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim cmd As New SqlCommand("RptStaffcostMovement", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpDTFROm As New SqlParameter("@DTFROm", SqlDbType.DateTime)
            sqlpDTFROm.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpDTFROm)

            Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
            sqlpDTTO.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpDTTO)

            Dim sqlpBSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
            sqlpBSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
            cmd.Parameters.Add(sqlpBSU_IDs)

            Dim sqlpbConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
            sqlpbConsolidation.Value = True
            cmd.Parameters.Add(sqlpbConsolidation)
            Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.VarChar, 4)
            Select Case type
                Case "CAT"
                    sqlpTyp.Value = "CAT"
                Case "DEP"
                    sqlpTyp.Value = "DEP"
            End Select
            cmd.Parameters.Add(sqlpTyp)

            Dim adpt As New SqlDataAdapter
            Dim ds As New DataSet
            adpt.SelectCommand = cmd
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            adpt.Fill(ds)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            If 1 = 1 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("fromDT") = txtFromDate.Text
                params("toDT") = txtToDate.Text
                Select Case type
                    Case "CAT"
                        params("reportCaption") = "Staff Cost Analysis By Category(Comparison)"
                    Case "DEP"
                        params("reportCaption") = "Staff Cost Analysis By Department(Comparison)"
                End Select
                'params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptStaffCostAnalysisTotal.rpt"
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub GenerateExpenceContribution(ByVal type As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim cmd As New SqlCommand("RptEmployeeSalary", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpDTFROm As New SqlParameter("@DTFROm", SqlDbType.DateTime)
            sqlpDTFROm.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpDTFROm)

            Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
            sqlpDTTO.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpDTTO)

            Dim sqlpBSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
            sqlpBSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
            cmd.Parameters.Add(sqlpBSU_IDs)

            Dim sqlpbConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
            sqlpbConsolidation.Value = Not chkViewSummary.Checked
            cmd.Parameters.Add(sqlpbConsolidation)
            Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.VarChar, 4)
            Select Case type
                Case "CAT"
                    sqlpTyp.Value = "CAT"
                Case "DEP"
                    sqlpTyp.Value = "DEP"
            End Select
            cmd.Parameters.Add(sqlpTyp)

            'Dim adpt As New SqlDataAdapter
            'Dim ds As New DataSet
            'adpt.SelectCommand = cmd
            'Dim objConn As New SqlConnection(str_conn)
            'objConn.Open()
            'adpt.Fill(ds)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            If 1 = 1 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                ''params("Month") = "January"
                params("fromDT") = txtFromDate.Text
                params("toDT") = txtToDate.Text
                Select Case type
                    Case "CAT"
                        If chkViewSummary.Checked Then
                            params("reportCaption") = "Staff Cost Analysis By Category(Consolidation)"
                        Else
                            params("reportCaption") = "Staff Cost Analysis By Category"
                        End If
                    Case "DEP"
                        If chkViewSummary.Checked Then
                            params("reportCaption") = "Staff Cost Analysis By Department(Consolidation)"
                        Else
                            params("reportCaption") = "Staff Cost Analysis By Department"
                        End If
                End Select
                'params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                If chkViewSummary.Checked Then
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptExpenseContriCategory.rpt"
                Else
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptExpenseContriCategoryConsol.rpt"
                End If
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub GenerateLeaveDetails()

        Try
            'txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
            'txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            'Cache("fromDate") = txtFromDate.Text
            'Cache("toDate") = txtToDate.Text
            'Dim strXMLBSUNames As String
            ''Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
            'strXMLBSUNames = GenerateXML(h_BSUID.Value, XMLType.BSUName) 'txtBSUNames.Text)

            'Select Case MainMnu_code
            '    Case "A550015" 'Party Ledger
            'End Select
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("EAT_BSU_ID", h_BSUID.Value)
            Dim str_Sql As String = "SELECT * FROM vw_EMPLEAVE_DETAILS WHERE " & _
            strFiltBSUID & GetFilter("CAT_ID", h_CATID.Value, True) & _
            GetFilter("DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EAT_EMP_ID", h_EMPID.Value, True) & _
            " AND EAT_DT between '" & txtFromDate.Text & "' AND '" & txtToDate.Text & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("Month") = "January"
                params("fromDate") = txtFromDate.Text
                params("toDate") = txtToDate.Text
                params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                If chkViewSummary.Checked Then
                    params("reportCaption") = "Leave Details"
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"
                Else
                    params("reportCaption") = "Leave Details"
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDescription.rpt"
                End If
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GenerateAttendanceLeaveDetails()

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("EAT_BSU_ID", h_BSUID.Value)
            'Dim strFilter as String=
            'strFiltBSUID & GetFilter("CAT_ID", h_CATID.Value, True) & _
            'GetFilter("DPT_ID", h_DEPTID.Value, True) & _
            'GetFilter("DES_ID", h_DESGID.Value, True) & _
            'GetFilter("EMP_ABC", strABC, True) & _
            'GetFilter("EAT_EMP_ID", h_EMPID.Value, True) & _
            '" AND EAT_DT between '"  "' AND '"  "'"


            Dim str_Sql As String = "EXEC rptDailyAttSummary '" & txtFromDate.Text & "','" & txtToDate.Text & "',' " & Session("sBsuid").ToString & "' ,'" & Session("sUsr_name") & "','" & _
            GetFilter("EMP_BSU_ID", h_BSUID.Value) & _
            GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EAT_EMP_ID", h_EMPID.Value, True) & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("@FromDt") = txtFromDate.Text
                params("@ToDt") = txtToDate.Text
                'params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                'If chkViewSummary.Checked Then
                '    params("reportCaption") = "Leave Details"
                '    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"
                'Else
                params("reportCaption") = "For the period from " + txtFromDate.Text + " to " + txtToDate.Text
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPApprovedLeaveDetails.rpt"
                'End If
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub GenerateEmployeeDetails()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            Dim objConn As New SqlConnection(str_conn)
            cmd.Connection = objConn
            cmd.CommandText = "[RPT_GetEmployeeDetails]"
            Dim sqlParam(5) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", h_BSUID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@DPT_ID", h_DEPTID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@DES_ID", h_DESGID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@CAT_ID", h_CATID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@EMP_ABC", strABC, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_ID", h_EMPID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("fromDate") = txtFromDate.Text
            params("toDate") = txtToDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            params("reportCaption") = "Employee Details"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeDetails.rpt"
            Session("ReportSource") = repSource
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx", True)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        Select Case MainMnu_code
            Case "H000205"
                GenerateEmployeeDetails()
        End Select
    End Sub


    Private Sub GenerateAllEmpLeaveDetails()
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("Month") = "January"
        params("@FromDt") = txtFromDate.Text
        params("@ToDt") = txtToDate.Text
        params("@Bsu_id") = h_BSUID.Value
        params("@CAT_id") = h_CATID.Value
        params("@DPT_id") = h_DEPTID.Value
        params("@DES_id") = h_DESGID.Value
        params("@EMP_ID") = h_EMPID.Value
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = params
            'If ViewState("MainMnu_code") = "S200055" Then
            'If MainMnu_code = "P150017" Then
            '    .reportPath = Server.MapPath("../Rpt/rptEmpAllLeaveDetails.rpt")
            'Else
            '    .reportPath = Server.MapPath("../Rpt/rptEmpleaves_Monthwise.rpt")
            'End If
            If rblReportName.SelectedValue = "1" Then
                .reportPath = Server.MapPath("../Rpt/rptEmpAllLeaveDetails.rpt")
            ElseIf rblReportName.SelectedValue = "2" Then
                .reportPath = Server.MapPath("../Rpt/rptEmpleaves_Monthwise.rpt")
            Else

                .reportPath = Server.MapPath("../Rpt/rptEmpYearlyLeaves.rpt")

            End If
            'End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("../../../Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Protected Sub chkExcludeProcessDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProcessingDate.Enabled = Not chkExcludeProcessDate.Checked
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If MainMnu_code = "P150049" Then 'V1.4
            trProcessingDate.Visible = True
            PopulateProcessingDate()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" + Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx") + "');", True)
        Else
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx") + "','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "');", True)
        Else
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "','_blank');", True)
        End If
    End Sub
    Protected Sub txtBSUName_TextChanged(sender As Object, e As EventArgs)
        txtBSUName.Text = ""
        FillBSUNames(h_BSUID.Value)
    End Sub
    Protected Sub txtDeptName_TextChanged(sender As Object, e As EventArgs)
        txtDeptName.Text = ""
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub txtCatName_TextChanged(sender As Object, e As EventArgs)
        txtCatName.Text = ""
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub txtDesgName_TextChanged(sender As Object, e As EventArgs)
        txtDesgName.Text = ""
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        FillEmpNames(h_EMPID.Value)
    End Sub
End Class