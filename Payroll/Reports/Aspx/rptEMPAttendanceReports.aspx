<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEMPAttendanceReports.aspx.vb" Inherits="Reports_ASPX_Report_BankBook" title="Untitled Page" %>

<%@ Register Src="../../../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc4" %>

<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../../../Asset/UserControls/usrBSUnitsAsset.ascx" TagName="usrBSUnits"
    TagPrefix="uc2" %>
<%@ Register Src="../../../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">   
<script language="javascript" type="text/javascript">
    function ToggleSearchEMPNames() 
        {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display')
             {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else 
            {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }
        function parseDMY(s) {
            return new Date(s.replace(/^(\d+)\W+(\w+)\W+/, '$2 $1 '));
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            <%--var result;
            var dtFrom = parseDMY(document.getElementById('<%=txtFromDate.ClientID %>').value);
            result = window.showModalDialog("SelIDDESC.aspx?ID=EMP_ALLOC&year=" + dtFrom.getFullYear() + "&month=" + dtFrom.getMonth(), "", sFeatures)

            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
            var dtFrom = parseDMY(document.getElementById('<%=txtFromDate.ClientID %>').value);
            var url = "SelIDDESC.aspx?ID=EMP_ALLOC&year=" + dtFrom.getFullYear() + "&month=" + dtFrom.getMonth();
            var oWnd = radopen(url, "pop_empname");


        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID %>').value = NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }
     
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblrptCaption" runat="server" Text="Employee Daily Attendance Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

   <table align="center" width="100%">
    <tr align =left >
    <td>
    <asp:ValidationSummary   ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
    </td>
    </tr>
       <tr align =left >
           <td>
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
           </td>
       </tr>
    </table>

    <table align="center" runat="server"  id="tblMain" width="100%">
            <tr id="trdateRow" runat="server">
            <td align="left" valign="top">
                Date</td>
            <td align="left" colspan="3" style="text-align: left">
                <div class="checkbox-list">
                <uc4:usrDatePicker ID="UsrDatePicker1" runat="server" />
                </div>
            </td>
        </tr>
         <tr id="trDateSel" runat="server" visible="false">
            <td align="left" width="20%">
                <asp:Label ID="lblFromDate" CssClass="field-label" runat="server" Text="From Date"></asp:Label>
                </td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtFromDate" CssClass="field-label" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                    EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
            <td align="left" wisth="20%">
                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                </td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtToDate" runat="server" Width="101px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span class="field-label">
                Business Unit</span></td>
            <td align="left" colspan="3">
                <div class="checkbox-list">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                </div>
            </td>
        </tr>
         <tr id="trToBsu" runat="server" visible=false>
            <td align="left" valign="top">
               <span class="field-label">Transferred To Business Unit</span></td>
            <td align="left" colspan="3">
                <div class="checkbox-list">
                <uc1:usrBSUnits ID="UsrBSUnits2" runat="server" />
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
               <span class="field-label">Category</span></td>
            <td align="left" colspan="3">
                <div class="checkbox-list">
                <uc3:usrTreeView ID="UsrTreeView1" runat="server" />
                </div>
            </td>
        </tr>
      
        <tr id ="trSelEMPName"  runat="server" visible="false" >
            <td align="left" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                <span class="field-label">Select Employee Name Filter</span></td>
        </tr>
        <tr id ="trEMPName" style="display:none;">
            <td align="left" valign ="top">
                <asp:ImageButton ID="imgMinusEmpName" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                <span class="field-label">Employee Name</span></td>
            <td align="left" colspan="3" valign ="top">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                <asp:TextBox ID="txtEMPNAME" runat="server" OnTextChanged="txtEMPNAME_TextChanged" AutoPostBack="true"
                  ></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNAME(); return false;" /><br />
                <asp:GridView ID="gvEMPName" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" width="100%" AllowPaging="True" PageSize="5">
                        <Columns>
                            <asp:TemplateField HeaderText="EMP ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                             <HeaderStyle CssClass="gridheader_new" />
                    </asp:GridView>
            </td>
        </tr>
         <tr id ="trChkreportName" runat="server" visible="false">
            <td align="left" colspan="4">
                <asp:RadioButtonList ID="rblReportFilter" CssClass="field-label" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected=True>Transfers pending for Approval</asp:ListItem>
                    <asp:ListItem Value="2">Transfers pending for Acceptance</asp:ListItem>
                    <asp:ListItem Value="3">Successful Transfers</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_EMPID" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_DEPTID" runat="server" />
    <asp:HiddenField ID="h_CATID" runat="server" />
    <asp:HiddenField ID="h_DESGID" runat="server" />
    <input id="hfBSU" runat="server" type="hidden" />
    <input id="hfDepartment" runat="server" type="hidden" />
    <input id="hfCategory" runat="server" type="hidden" />
    <input id="hfDesignation" runat="server" type="hidden" />
    <input id="hfEmpName" runat = "server" type="hidden" />
    <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
  <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

