﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCListCompReport.aspx.vb" Inherits="Payroll_Reports_Aspx_rptCListCompReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            C List Comparison Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0"
                                width="100%">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="9" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana"></span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left"><span class="field-label">Report Type</span></td>

                                    <td align="left">
                                        <asp:RadioButtonList ID="rblType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Text="Summary" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Detail" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="left"><span class="field-label">Financial Year</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcyaer" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <%--  <tr>                      
                    </tr>--%>
                                <tr>

                                    <td align="left"><span class="field-label">From Date</span> <span style="color: red">*</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgAtt" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom" Display="Dynamic"
                                            ErrorMessage="Enter the start date in given format mmm/yyyy e.g.  Sep/2007 or 09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator3" runat="server" ControlToValidate="txtFrom" CssClass="error"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="From Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfFrom" runat="server" ControlToValidate="txtFrom"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter the From date"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"><span class="field-label">To Date </span><span style="color: red">*</span>
                                    </td>

                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtTo" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo" Display="Dynamic"
                                            ErrorMessage="Enter the to date in given format mmm/yyyy e.g.  Sep/2007 or 09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator1" runat="server" ControlToValidate="txtTo" CssClass="error"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="To Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter the To date"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trSchool" runat="server">
                                    <td align="left"><span class="field-label">Business Unit</span></td>

                                    <td align="left" >
                                       <div class="checkbox-list">
                                        <asp:TreeView ID="tvBusinessunit" runat="server" onclick="client_OnTreeNodeChecked();"
                                            ShowCheckBoxes="All">
                                            <NodeStyle CssClass="treenode" />
                                        </asp:TreeView></div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export" Style="display: none;" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Report" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>




                    <tr>
                        <td valign="bottom">

                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="MMM/yyyy" PopupButtonID="imgAtt" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                            </ajaxToolkit:CalendarExtender>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

