Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Payroll_Reports_Aspx_rptEmpLeftDuringPeriod
    Inherits System.Web.UI.Page

    ''' version                 Date            author            change
    ''' 1.1                     31/oct/2012     Swapna          New report for hold released details
    ''' 1.2                     19/Aug/2014     Swapna          Final settlement details report
   
    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value = Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            FillDeptNames(h_DEPTID.Value)
            FillCATNames(h_CATID.Value)
            FillDESGNames(h_DESGID.Value)
            FillEmpNames(h_EMPID.Value)
            StoreEMPFilter()
            'If h_EMPID.Value <> Nothing And h_EMPID.Value <> "" And h_EMPID.Value <> "undefined" Then
            '    FillACTIDs(h_EMPID.Value)
            '    'h_BSUID.Value = ""
            'End If
            If Not IsPostBack Then

              
                Select Case MainMnu_code
                    Case "P153045"
                        lblrptCaption.Text = "Report on Employee Left"
                        HideShowAllROWS(True)
                    Case "P156005"
                        lblrptCaption.Text = "Leave Salary"
                        HideShowAllROWS(True)
                    Case "P130178" 'V1.1
                        lblrptCaption.Text = "Salary Hold Released"
                        HideShowAllROWS(True)
                        trDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDepartment.Visible = False
                        trSelDesignation.Visible = False
                        chkViewSummary.Visible = False
                        trBSUTree.Visible = True
                        trSelBSU.Visible = False
                        trBSUnit.Visible = False
                        trPAID.Visible = True
                    Case "P130176"
                        lblrptCaption.Text = "Salary On Hold Till Date"
                        HideShowAllROWS(True)
                        trDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDepartment.Visible = False
                        trSelDesignation.Visible = False
                        chkViewSummary.Visible = False
                        trBSUTree.Visible = True
                        trSelBSU.Visible = False
                        trBSUnit.Visible = False
                        trPAID.Visible = False
                        trEmpHoldReport.Visible = True
                        'trEMPName.Visible = True
                        'trSelEMPName.Visible = True
                    Case "H000173"
                        lblrptCaption.Text = "Employee Movement Details"
                        HideShowAllROWS(True)
                        trDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDepartment.Visible = False
                        trSelDesignation.Visible = False
                        chkViewSummary.Visible = False
                        trBSUTree.Visible = True
                        trSelBSU.Visible = False
                        trBSUnit.Visible = False
                        trPAID.Visible = False
                        trEmpHoldReport.Visible = False
                        trEmpStatus.Visible = True
                    Case "P153055"
                        lblrptCaption.Text = "Final Settlement Processed Details"
                   
                        HideShowAllROWS(True)
                        trDepartment.Visible = True
                        trDesignation.Visible = True
                        trSelDepartment.Visible = True
                        trSelDesignation.Visible = True
                        chkViewSummary.Visible = False
                        trBSUTree.Visible = True
                        trSelBSU.Visible = False
                        trBSUnit.Visible = False
                        trPAID.Visible = False
                        trEmpHoldReport.Visible = False
                        trEMPABCCat.Visible = False
                        trSelEMPName.Visible = True
                        trEMPName.Visible = True
                       
                End Select

                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                    txtFromDate.Text = Cache("fromDate")
                    txtToDate.Text = Cache("toDate")
                Else
                    txtToDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

                End If
                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
               
               
            End If
            If MainMnu_code = "P153055" Then
                ImageButton3.Attributes.Remove("OnClientClick")
                ImageButton3.Attributes.Add("OnClientClick", "return GetSchoolDESGName();")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub HideShowAllROWS(ByVal val As Boolean)
        trDepartment.Visible = val
        trCategory.Visible = val
        trDesignation.Visible = val
        trBSUTree.Visible = False
        trSelDepartment.Visible = val
        trSelcategory.Visible = val
        trSelDesignation.Visible = val
        trEMPName.Visible = False
        trEMPABCCat.Visible = val
        trPAID.Visible = False
        trSelEMPName.Visible = False
        trEmpHoldReport.Visible = False
        trEmpStatus.Visible = False
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        If h_EMPID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case MainMnu_code
            Case "P153045"
                GenerateEmployeeLeft()
            Case "P156005"
                GenerateLeaveSalary()
            Case "P130178" 'V1.1
                GenerateSalaryHoldReleaseReport()
            Case "P130176"
                GenerateHoldTillDateReport()
            Case "H000173"
                GenerateEmployeeMovementDetails()
            Case "P153055"
                GenerateFinalSettlementDetails()
        End Select
    End Sub
    Protected Sub GenerateFinalSettlementDetails()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim strABC As String = ""
        'If chkEMPABC_A.Checked Then
        '    strABC = "A"
        'End If
        'If chkEMPABC_B.Checked Then
        '    strABC += "||B"
        'End If
        'If chkEMPABC_C.Checked Then
        '    strABC += "||C"
        'End If
        Try

            Dim cmd As New SqlCommand("getFinalSettlementProcessedReport", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)


            'Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            'sqlpEMP_ID.Value = h_EMPID.Value
            'cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpDtFrom As New SqlParameter("@fromdt", SqlDbType.DateTime)
            sqlpDtFrom.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpDtFrom)


            Dim sqlpDtTo As New SqlParameter("@Todate", SqlDbType.DateTime)
            sqlpDtTo.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpDtTo)

            'Dim sqlpStatus As New SqlParameter("@status", SqlDbType.VarChar, 50)
            'sqlpStatus.Value = ""

            'For Each li As ListItem In chkEmpStatus.Items
            '    If li.Selected = True Then
            '        sqlpStatus.Value = sqlpStatus.Value + "|" + li.Value
            '    End If
            'Next
            'cmd.Parameters.Add(sqlpStatus)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 10)
            sqlpCat.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpDes As New SqlParameter("@Des_ID", SqlDbType.VarChar)
            sqlpDes.Value = h_DESGID.Value
            cmd.Parameters.Add(sqlpDes)

            Dim sqlpDpt As New SqlParameter("@Dpt_ID", SqlDbType.VarChar)
            sqlpDpt.Value = h_DEPTID.Value
            cmd.Parameters.Add(sqlpDpt)

            Dim sqlpEmpId As New SqlParameter("@Emp_ID", SqlDbType.VarChar)
            sqlpEmpId.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEmpId)
            'Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 10)
            'sqlpABC_CAT.Value = strABC
            'cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")
            params("FromDt") = txtFromDate.Text
            params("ToDt") = txtToDate.Text

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptFinalSettlementDetails.rpt"
            repSource.IncludeBSUImage = True

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Protected Sub GenerateEmployeeMovementDetails()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim strABC As String = ""
        If chkEMPABC_A.Checked Then
            strABC = "A"
        End If
        If chkEMPABC_B.Checked Then
            strABC += "||B"
        End If
        If chkEMPABC_C.Checked Then
            strABC += "||C"
        End If
        Try

            Dim cmd As New SqlCommand("rptGetBSUEmployeeStatus_DateWise", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)


            'Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            'sqlpEMP_ID.Value = h_EMPID.Value
            'cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpDtFrom As New SqlParameter("@FromDt", SqlDbType.DateTime)
            sqlpDtFrom.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpDtFrom)


            Dim sqlpDtTo As New SqlParameter("@Todt", SqlDbType.DateTime)
            sqlpDtTo.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpDtTo)

            Dim sqlpStatus As New SqlParameter("@status", SqlDbType.VarChar, 50)
            sqlpStatus.Value = ""

            For Each li As ListItem In chkEmpStatus.Items
                If li.Selected = True Then
                    sqlpStatus.Value = sqlpStatus.Value + "|" + li.Value
                End If
            Next
            cmd.Parameters.Add(sqlpStatus)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 10)
            sqlpCat.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 10)
            sqlpABC_CAT.Value = strABC
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")
            params("FromDt") = txtFromDate.Text
            params("ToDt") = txtToDate.Text

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmpStatusDateWise.rpt"
            repSource.IncludeBSUImage = True

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Protected Sub GenerateHoldTillDateReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim strABC As String = ""
        If chkEMPABC_A.Checked Then
            strABC = "A"
        End If
        If chkEMPABC_B.Checked Then
            strABC += "||B"
        End If
        If chkEMPABC_C.Checked Then
            strABC += "||C"
        End If
        Try
          

            Dim cmd As New SqlCommand("rptHoldTillNowDetails", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@bsu_id", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)


            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@FromDt", SqlDbType.DateTime)
            sqlpYear.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpYear)


            Dim sqlpMonth As New SqlParameter("@Todate", SqlDbType.DateTime)
            sqlpMonth.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = strABC
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
           
            params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptHoldTillNowReport.rpt"
            If trEmpHoldReport.Visible = True And chkEmployeeHold.Checked = True Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptHoldTillNowReportIndividual.rpt"
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    ''' <summary>
    ''' V1.1 - function to fill hold relesed data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GenerateSalaryHoldReleaseReport()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            Dim cmd As New SqlCommand("", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure

            cmd.CommandType = CommandType.Text

            cmd.CommandText = " exec rptGetHoldReleaseDetails'" & UsrBSUnits1.GetSelectedNode("||") & "', '" & txtFromDate.Text & "','" & txtToDate.Text & "','" & h_CATID.Value & "','" & strABC & "'," & chkPaid.Checked

            'Dim rptClass As New rptClass
            'With rptClass
            '    .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            '    .reportParameters = param
            '    .reportPath = Server.MapPath("../RPT/rptSibling.rpt")
            'End With
            'Session("rptClass") = rptClass

            If 1 = 1 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("FromDate") = txtFromDate.Text
                params("ToDate") = txtToDate.Text
                'params.Add("@IMG_BSU_ID", Session("sbsuid"))
                'params.Add("@IMG_TYPE", "LOGO")
                'params("bsuName") = Session("bsu_name")
                'params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd

                Dim repSourceSubRep(1) As MyReportClass
                'repSourceSubRep(0) = New MyReportClass
                'Dim cmdSubEarn As New SqlCommand
                'cmdSubEarn.CommandText = str_Sql
                'cmdSubEarn.Connection = New SqlConnection(str_conn)
                'cmdSubEarn.CommandType = CommandType.Text
                'repSourceSubRep(0).Command = cmdSubEarn

                'Dim cmdSubEarn As New SqlCommand("", New SqlConnection(str_conn))
                'cmdSubEarn.CommandType = CommandType.StoredProcedure
                'cmdSubEarn.CommandType = CommandType.Text
                'cmdSubEarn.CommandText = " exec ReportHeader_Subreport'" & Session("sbsuid") & "','" & "LOGO"
                'repSourceSubRep(0).Command = cmdSubEarn
                'repSource.SubReport = repSourceSubRep
                repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptHoldReleaseDetails.rpt"
                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
   

    Protected Sub h_BSUID_ValueChanged(sender As Object, e As EventArgs) Handles h_BSUID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillBSUNames(h_BSUID.Value)
        'h_BSUID.Value = ""
    End Sub

    Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDeptNames(h_DEPTID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub

    Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDESGNames(h_DESGID.Value)
        'h_DESGID.Value = ""
    End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub
    Private Sub GenerateEmployeeLeft()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim cmd As New SqlCommand("", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure

            cmd.CommandType = CommandType.Text

            cmd.CommandText = "SELECT     EM.EMP_ID, ISNULL(EM.EMP_FNAME, '') +' '+ ISNULL(EM.EMP_MNAME, '') +' '+ " _
                & " ISNULL(EM.EMP_LNAME, '') AS EMP_NAME, EM.EMP_DES_ID, EM.EMP_ECT_ID," _
                & " DES.DES_DESCR, ECT.ECT_DESCR, EM.EMP_RESGDT, EM.EMP_DPT_ID, EM.EMPNO, " _
                & " DPT.DPT_DESCR, EM.EMP_BSU_ID, BSU.BSU_NAME FROM DEPARTMENT_M AS DPT INNER JOIN" _
                & " EMPLOYEE_M AS EM INNER JOIN EMPDESIGNATION_M AS DES " _
                & " ON EM.EMP_DES_ID = DES.DES_ID INNER JOIN" _
                & " EMPCATEGORY_M AS ECT ON EM.EMP_ECT_ID = ECT.ECT_ID " _
                & " ON DPT.DPT_ID = EM.EMP_DPT_ID INNER JOIN BUSINESSUNIT_M AS BSU ON EM.EMP_BSU_ID = BSU.BSU_ID" _
                 & GetFilter("EMP_BSU_ID", h_BSUID.Value, True) _
                 & GetFilter("EMP_ECT_ID", h_CATID.Value, True) _
                 & GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) _
                 & GetFilter("DES_ID", h_DESGID.Value, True) _
                 & " AND isnull(emp_lastattdt,'31-Dec-2055') between '" & txtFromDate.Text & "' AND '" & txtToDate.Text & "'"


            Dim adpt As New SqlDataAdapter
            Dim ds As New DataSet
            adpt.SelectCommand = cmd
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            adpt.Fill(ds)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            If 1 = 1 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("FromDate") = txtFromDate.Text
                params("ToDate") = txtToDate.Text

                'params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeLeft.rpt"
                Session("ReportSource") = repSource
                '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()

            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

   
    Private Sub GenerateLeaveSalary()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim cmd As New SqlCommand("", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure

            cmd.CommandType = CommandType.Text

            cmd.CommandText = " SELECT * FROM dbo.fn_GetLeaveScheduleReport('" & Session("sBsuid") & "', '" & txtFromDate.Text & "')  " _
                 & GetFilter("EMP_ECT_ID", h_CATID.Value, True) _
                 & GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) _
                 & GetFilter("DES_ID", h_DESGID.Value, True)
              

            'Dim adpt As New SqlDataAdapter
            'Dim ds As New DataSet
            'adpt.SelectCommand = cmd
            'Dim objConn As New SqlConnection(str_conn)
            'objConn.Open()
            'adpt.Fill(ds)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            If 1 = 1 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("FromDate") = txtFromDate.Text
                params("ToDate") = txtToDate.Text
                params("bsuName") = Session("bsu_name")
                'params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptfn_GetLeaveSchedule.rpt"
                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    
    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' and DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMPNO as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

   

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub
    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub
    

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If MainMnu_code = "P153055" Then
            ImageButton3.Attributes.Remove("OnClientClick")
            ImageButton3.Attributes.Add("OnClientClick", "return GetSchoolDESGName();")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub  
End Class
