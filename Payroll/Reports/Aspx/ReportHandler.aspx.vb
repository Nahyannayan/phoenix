Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Net

Partial Class Payroll_Reports_ASPX_Report_ReportHandler
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim params As New Hashtable
            Dim MODE As String = IIf(Request.QueryString("MODE") <> Nothing, Request.QueryString("MODE"), String.Empty)
            Select Case MODE
                Case "RecoSum"
                    GenerateRecoSummaryDetail()
                Case "SalarySummary"
                    GenerateSalarySummaryReport()
                Case "SalarySlip"
                    GenerateSalarySlip()
            End Select
        End If
    End Sub
    Private Sub GenerateSalarySlip()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
            Dim EMPID As Int16 = IIf(Request.QueryString("EMPID") <> Nothing, Request.QueryString("EMPID"), String.Empty)
            Dim Month As Int16 = IIf(Request.QueryString("Month") <> Nothing, Request.QueryString("Month"), String.Empty)
            Dim Year As Int16 = IIf(Request.QueryString("Year") <> Nothing, Request.QueryString("Year"), String.Empty)
         

            Dim str_Sql As String = "select DISTINCT ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE ESD_BSU_ID='" & BSUID & "'"
            str_Sql &= " and ESD_EMP_ID='" & EMPID & "'"
            str_Sql &= " and ESD_MONTH='" & Month & "'"
            str_Sql &= " and ESD_YEAR='" & Year & "'"
        
            Dim ESD_IDs As String = String.Empty
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            Dim comma As String = String.Empty
            While (dr.Read())
                ESD_IDs += comma + dr(0).ToString
                comma = ","
            End While


            str_Sql = "SELECT * FROM vw_OSO_EMPSALARYHEADERDETAILS WHERE ESD_BSU_ID='" & BSUID & "'"
            str_Sql &= " and ESD_EMP_ID='" & EMPID & "'"
            str_Sql &= " and ESD_MONTH='" & Month & "'"
            str_Sql &= " and ESD_YEAR='" & Year & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                Dim bMultiMonth As Boolean
                If Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
                    bMultiMonth = False
                ElseIf Session("EMP_MONTH_YEAR_COIN").Rows.Count <= 1 Then
                    bMultiMonth = False
                Else
                    bMultiMonth = True
                End If
                params("bMultiMonth") = bMultiMonth
                Dim repSourceSubRep(1) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubEarn As New SqlCommand
                cmdSubEarn.CommandText = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='D' AND ESS_ESD_ID in(" & ESD_IDs & ")"
                cmdSubEarn.Connection = New SqlConnection(str_conn)
                cmdSubEarn.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubEarn

                repSourceSubRep(1) = New MyReportClass
                Dim cmdSubDed As New SqlCommand
                cmdSubDed.CommandText = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='E' AND ESS_ESD_ID in(" & ESD_IDs & ")"
                cmdSubDed.Connection = New SqlConnection(str_conn)
                cmdSubDed.CommandType = CommandType.Text
                repSourceSubRep(1).Command = cmdSubDed

                repSource.SubReport = repSourceSubRep
                'repSource.IncludeBSUImage = True

                repSource.Parameter = params
                repSource.Command = cmd
                'repSource.IncludeBSUImage = True

                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip.rpt"
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/RptviewerNew.aspx", True)
                'ReportLoadSelection()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GenerateRecoSummaryDetail()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
            Dim CATID As Int16 = IIf(Request.QueryString("CATID") <> Nothing, Request.QueryString("CATID"), String.Empty)
            Dim EMP_ABCs As String = IIf(Request.QueryString("EMP_ABCs") <> Nothing, Request.QueryString("EMP_ABCs"), String.Empty)
            Dim GroupType As String = IIf(Request.QueryString("GroupType") <> Nothing, Request.QueryString("GroupType"), String.Empty)
            Dim Month As Int16 = IIf(Request.QueryString("Month") <> Nothing, Request.QueryString("Month"), String.Empty)
            Dim Year As Int16 = IIf(Request.QueryString("Year") <> Nothing, Request.QueryString("Year"), String.Empty)

            Dim cmd As New SqlCommand("GetSalaryRecon_Detail", objConn)
            cmd.CommandType = CommandType.StoredProcedure
            '[dbo].[GetSalaryRecon] (@BSU_ID varchar(20),@Month int,@Year int)
            Dim sqlpPAYMONTH As New SqlParameter("@Month", SqlDbType.Int)
            sqlpPAYMONTH.Value = Month
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@Year", SqlDbType.Int)
            sqlpPAYYEAR.Value = Year
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSUID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ABCs As New SqlParameter("@EMP_ABCs", SqlDbType.VarChar, 30)
            sqlpEMP_ABCs.Value = EMP_ABCs
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim sqlpID As New SqlParameter("@CATID", SqlDbType.VarChar, 30)
            sqlpID.Value = CATID
            cmd.Parameters.Add(sqlpID)

            Dim sqlpGroupType As New SqlParameter("@GroupType", SqlDbType.VarChar, 30)
            sqlpGroupType.Value = GroupType
            cmd.Parameters.Add(sqlpGroupType)

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = MonthName(Month)
            params("PAYYEAR") = Year
            params("RPT_CAPTION") = "Salary Reconciliation Statement Summary (" & UtilityObj.GetDataFromSQL( _
            "select BSU_NAME from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' ", _
            ConnectionManger.GetOASISConnectionString) & ")"

            If GroupType = "ALL" Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetSalaryRecon_Detail.rpt"
            Else
                params("TYPE") = GroupType
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryReconDetail.rpt"
            End If
            repSource.Parameter = params
            repSource.Command = cmd
            Session("ReportSource") = repSource
            Response.Redirect("../../../Reports/ASPX Report/RptviewerNew.aspx", True)
            'ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GenerateSalarySummaryReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
            Dim CATID As String = IIf(Request.QueryString("CATID") <> Nothing, Request.QueryString("CATID"), String.Empty)
            Dim EMP_ABCs As String = IIf(Request.QueryString("EMP_ABCs") <> Nothing, Request.QueryString("EMP_ABCs"), String.Empty)
            Dim GroupType As String = IIf(Request.QueryString("GroupType") <> Nothing, Request.QueryString("GroupType"), String.Empty)
            Dim Month As Int16 = IIf(Request.QueryString("Month") <> Nothing, Request.QueryString("Month"), String.Empty)
            Dim Year As Int16 = IIf(Request.QueryString("Year") <> Nothing, Request.QueryString("Year"), String.Empty)
            Dim PROCESSDATE As String = IIf(Request.QueryString("PROCESSDATE") <> Nothing, Request.QueryString("PROCESSDATE"), String.Empty)
            Dim SALTYPE As String = IIf(Request.QueryString("SALTYPE") <> Nothing, Request.QueryString("SALTYPE"), String.Empty)
            Dim IsFinalSettlement As Boolean = IIf(Request.QueryString("IsFinalSettlement") <> Nothing, Request.QueryString("IsFinalSettlement"), 0) ' swapna added for FF
            Dim Combined As Boolean = IIf(Request.QueryString("Combined") <> Nothing, Request.QueryString("Combined"), 0)

            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format4_Detail]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = Month
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = Year
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSUID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = CATID
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = EMP_ABCs
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlSALTYPE As New SqlParameter("@SALTYPE", SqlDbType.VarChar, 20)
            sqlSALTYPE.Value = SALTYPE
            cmd.Parameters.Add(sqlSALTYPE)

            Dim sqlpPROCESS_DATE As New SqlParameter("@PROCESS_DATE", SqlDbType.DateTime)
            If IsDate(PROCESSDATE) Then
                sqlpPROCESS_DATE.Value = PROCESSDATE
            Else
                sqlpPROCESS_DATE.Value = DBNull.Value
            End If
            cmd.Parameters.Add(sqlpPROCESS_DATE)

            Dim sqlpFF As New SqlParameter("@IsFinalSettlement", SqlDbType.Bit)
            sqlpFF.Value = IsFinalSettlement
            cmd.Parameters.Add(sqlpFF)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = Month
            params("EMPABC") = EMP_ABCs
            params("PAYYEAR") = Year
            params("IsFinalSettlement") = IsFinalSettlement
            'V1.1
            'params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkExcludeProcessDate.Checked AndAlso ddlProcessingDate.Items.Count <> 0, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            If IsDate(PROCESSDATE) Then
                params("PROCESS_DATE") = PROCESSDATE
            Else
                params("PROCESS_DATE") = ""
            End If
            'V1.1 ends

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format4_Detail.rpt"

            Session("ReportSource") = repSource
            Response.Redirect("../../../Reports/ASPX Report/RptviewerNew.aspx", False)
            'ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
