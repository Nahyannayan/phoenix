Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Payroll_Reports_Aspx_empGetGratuvitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle

        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                txtDate.Text = UtilityObj.GetDiplayDate()
                bind_BusinessUnit()
                fill_Emp_for_BSU()
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Select Case MainMnu_code
                Case "P156015"
                    lblrptCaption.Text = "Gratuity Schedule"
                Case "P080005"
                    lblrptCaption.Text = "Salary Report on a Date"
                Case "P152035"
                    lblrptCaption.Text = "Salary Standard Deduction As on a Date"
                Case Else
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
            End Select


            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If



            FillEmpNames(h_EMPID.Value)

            If Not IsPostBack Then

                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        h_EMPID.Value = ""
    End Sub
    Sub bind_BusinessUnit()

        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else

            Try
                'if user is super admin give access to all the Business Unit
                If tblbUSuper = True Then
                    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        'create a list item to bind records from reader to dropdownlist ddlBunit
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        'check if it return rows or not
                        If BUnitreaderSuper.HasRows = True Then
                            While BUnitreaderSuper.Read
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBUnit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                Else
                    'if user is not super admin get access to the selected  Business Unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        If BUnitreader.HasRows = True Then
                            While BUnitreader.Read
                                di = New ListItem(BUnitreader(2), BUnitreader(0))
                                ddlBUnit.Items.Add(di)
                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If



    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvEMPName.DataSource = ds
            gvEMPName.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = ACTIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "P156015"
                GenerateGratuityReport()
            Case "P080005"
                SalaryReportonaDate()
            Case "P152035"
                GenerateStandardSalaryDeduction()
        End Select

    End Sub

    Private Sub GenerateStandardSalaryDeduction()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "|B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "|C"
            End If
            If strABC = "" Then
                strABC = "Z"
            End If

            Dim str_sql As String = String.Empty
            str_sql = "select * from dbo.vw_OSO_EMPSALARY_HS WHERE ERN_TYP = 0 " & _
            " AND EMP_BSU_ID = '" & ddlBUnit.SelectedValue & "' "
            If ddCategory.SelectedValue <> "0" Then
                str_sql += " AND EMP_ECT_ID = " & ddCategory.SelectedValue
            End If
            str_sql += GetFilter("ESH_EMP_ID", h_EMPID.Value, True)
            str_sql += " AND '" & txtDate.Text & "' between ESH_FROMDT  AND ESH_TODT "
            Dim cmd As New SqlCommand(str_sql, New SqlConnection(str_conn))
            cmd.CommandType = CommandType.Text

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("FromDate") = Format(CDate(txtDate.Text), OASISConstants.DateFormat)
            params("reportCaption") = ddlBUnit.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLOYEE_Stand_ded.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub GenerateGratuityReport()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "|B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "|C"
            End If
            If strABC = "" Then
                strABC = "Z"
            End If

            Dim cmd As New SqlCommand("GetGratuvitySchedule", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = ddlBUnit.SelectedItem.Value
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ECT_ID As New SqlParameter("@EMP_ECT_ID", SqlDbType.Int)
            sqlpEMP_ECT_ID.Value = ddCategory.SelectedItem.Value
            cmd.Parameters.Add(sqlpEMP_ECT_ID)

            Dim sqlpSCHEDULE_DATE As New SqlParameter("@SCHEDULE_DATE", SqlDbType.DateTime)
            sqlpSCHEDULE_DATE.Value = txtDate.Text
            cmd.Parameters.Add(sqlpSCHEDULE_DATE)

            Dim sqlpEMP_IDS As New SqlParameter("@EMP_IDS", SqlDbType.Xml)
            sqlpEMP_IDS.Value = UtilityObj.GenerateXML(h_EMPID.Value, XMLType.EMPName)
            cmd.Parameters.Add(sqlpEMP_IDS)

            Dim sqlpEMP_ABCs As New SqlParameter("@EMP_ABCs", SqlDbType.VarChar, 10)
            sqlpEMP_ABCs.Value = strABC
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter(cmd)

            'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey

            adpt.Fill(ds)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = Format(CDate(txtDate.Text), OASISConstants.DateFormat)
            params("BSU_NAME") = ddlBUnit.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetGratuvitySchedule.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            'ReportLoadSelection()
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub SalaryReportonaDate()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "|B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "|C"
            End If
            If strABC = "" Then
                strABC = "Z"
            End If

            Dim cmd As New SqlCommand("RPTSalaryOnaDate", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure

            '@BSU_ID VARCHAR (20)='125003',
            '@EMP_ECT_ID INT=0 ,
            '@ASONDT DATETIME='1-MAR-2007',
            '@EMP_IDS XML='<EMP_DETAILS><EMP_DETAIL><EMP_ID>853</EMP_ID></EMP_DETAIL></EMP_DETAILS>',

            '@EMP_ABCs VARCHAR(20)='A'

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = ddlBUnit.SelectedItem.Value
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ECT_ID As New SqlParameter("@EMP_ECT_ID", SqlDbType.Int)
            sqlpEMP_ECT_ID.Value = ddCategory.SelectedItem.Value
            cmd.Parameters.Add(sqlpEMP_ECT_ID)

            Dim sqlpSCHEDULE_DATE As New SqlParameter("@ASONDT", SqlDbType.DateTime)
            sqlpSCHEDULE_DATE.Value = txtDate.Text
            cmd.Parameters.Add(sqlpSCHEDULE_DATE)

            Dim sqlpEMP_IDS As New SqlParameter("@EMP_IDS", SqlDbType.Xml)
            sqlpEMP_IDS.Value = UtilityObj.GenerateXML(h_EMPID.Value, XMLType.EMPName)
            cmd.Parameters.Add(sqlpEMP_IDS)

            Dim sqlpEMP_ABCs As New SqlParameter("@EMP_ABCs", SqlDbType.VarChar, 10)
            sqlpEMP_ABCs.Value = strABC
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter(cmd)

            'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey

            adpt.Fill(ds)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = Format(CDate(txtDate.Text), OASISConstants.DateFormat)
            params("BSU_NAME") = ddlBUnit.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/RPTSalaryOnaDate.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit


    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function








    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillACTIDs(h_EMPID.Value)
        End If

    End Sub




    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Sub fill_Emp_for_BSU()
        h_EMPID.Value = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select EMP_ID as ID from EMPLOYEE_M where 1=1 AND EMP_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' AND EMP_BACTIVE=1"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            h_EMPID.Value = dr(0) & "||" & h_EMPID.Value
        End While

        FillEmpNames(h_EMPID.Value)

    End Sub



    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub gvEMPName_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        gvEMPName.DataBind()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
