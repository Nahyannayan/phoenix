Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports GemBox.Spreadsheet
Imports UtilityObj
Imports Encryption64

'Version            Date            Author          Change
'1.1                5-Jun-2011      Swapna          Added reports to filter
'2.0                1-Aug-2019      Vikranth        Changed Export to Excel functionality
Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
#Region "Public Variable"
    Public Property bLateCheckin() As Boolean
        Get
            Return ViewState("bLateCheckin")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bLateCheckin") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As ScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(Me.lnkExporttoexcel)
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle
                BindCategory()
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
                'Added by vikranth on 28th July 2019
                CheckCountOfEleaveEnabledEmployees()
                'Added by vikranth on 1st Aug 2019 for showing radio button list
                'commented by vikranth on 23rd Aug 2020
                'If MainMnu_code = "H000704" Or MainMnu_code = "H000705" Then
                '    trELeave.Visible = True
                'Else trELeave.Visible = False
                'End If
            End If
            If MainMnu_code = "H000704" Or MainMnu_code = "H000705" Then
                chkShowAll.Visible = True
            Else
                chkShowAll.Visible = False
            End If
            If MainMnu_code = "H790005" Or MainMnu_code = "H790006" Then
                Leavetype.Visible = False
                trGroup.Visible = True
            End If
            FillEmpNames(h_EMPID.Value)
            txtReport.Attributes.Add("ReadOnly", "ReadOnly")
            If Not IsPostBack Then
                trAsOnDate.Visible = False
                SetReportingTo()
                bLateCheckin = False
                If MainMnu_code = "H000705" Then
                    chkAll.Checked = True
                ElseIf MainMnu_code = "P159058" Or MainMnu_code = "P159059" Then
                    trAsOnDate.Visible = True
                    trDateRange.Visible = False
                    trEMPABCCAT.Visible = True
                    chkAll.Visible = True
                    btnReport.Visible = True
                    lblrptCaption.Text = "Employee Leave Report"
                ElseIf MainMnu_code = "H790006" Then
                    bLateCheckin = True
                    lblrptCaption.Text = "Employee Late Attendance Log"
                End If
                If Session("sBITSupport") = True Or MainMnu_code = "H000705" Or MainMnu_code = "P159059" Then
                    chkAll.Visible = True
                    btnReport.Visible = True
                Else
                    btnReport.Visible = False
                    If hfReport.Value = "" Then
                        hfReport.Value = "-1"
                        txtReport.Text = "Employee Not Linked"
                    End If
                    chkAll.Visible = False
                End If
                StoreEMPFilter()
                Dim FromDt As DateTime = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                UsrDateFrom.SetDate = Format(FromDt, OASISConstants.DateFormat)
                UsrDateTo.SetDate = Format(DateTime.Now, OASISConstants.DateFormat)
                UsrAsOnDate.SetDate = Format(DateTime.Now, OASISConstants.DateFormat)
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub SetReportingTo()
        Dim str_sql As String = "SELECT ISNULL(EMP.EMP_FNAME, '') + ' ' + ISNULL(EMP.EMP_MNAME, '') + ' ' + ISNULL(EMP.EMP_LNAME, '') AS EMP_NAME, " _
            & " USR.USR_NAME, EMP.EMP_ID FROM  USERS_M AS USR INNER JOIN" _
            & " vw_OSO_EMPLOYEE_M AS EMP ON USR.USR_EMP_ID = EMP.EMP_ID" _
            & " WHERE (USR.USR_NAME = '" & Session("sUsr_name") & "')"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            txtReport.Text = ds.Tables(0).Rows(0)("EMP_NAME")
            hfReport.Value = ds.Tables(0).Rows(0)("EMP_ID")
        End If
    End Sub
    ' Added by vikranth for getting count of E-leave enabled employees 
    Sub CheckCountOfEleaveEnabledEmployees()
        'Dim str_sql As String = "SELECT COUNT(EMP_ID) EMPCount FROM" _
        '    & " EMPLOYEE_M WHERE EMP_bOn_Eleave=1 AND EMP_BSU_ID = '" & h_BSUID.Value & "'"
        Dim str_sql As String = "exec check_BSU_Employee_Eleave_access '" & h_BSUID.Value & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("BUS_bHasEleave") = True Then
                trELeave.Visible = True
            Else trELeave.Visible = False
            End If
            'trELeave.Visible = True
        Else trELeave.Visible = False
        End If
    End Sub

    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "H000704", "H000705"
                GenerateAttendanceReport()
            Case "P159058", "P159059"
                GenerateEmployeeReport()
            Case "H790005"
                GenerateAttendancePunchingDetailsReport()
            Case "H790006"
                GenerateAttendancePunchingDetailsReport()
        End Select
    End Sub

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Sub GenerateEmployeeReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            'Written by vikranth on 1st Aug 2019
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim Report_Emp As String = String.Empty
                Dim LeaveTypes As String = String.Empty
                If txtReport.Text <> "" And hfReport.Value <> "" And Not chkAll.Checked Then
                    Report_Emp = hfReport.Value
                Else
                    Report_Emp = 0
                End If

                For Each Li As ListItem In CBLLeaveTypes.Items
                    If Li.Selected = True Then
                        LeaveTypes += "|" & Li.Value
                    End If
                Next
                'Dim objConn As New SqlConnection(str_conn)
                Dim param(8) As SqlClient.SqlParameter
                'Dim SqlCmd As New SqlCommand("rptEmployeeLeaves")
                'SqlCmd.CommandType = CommandType.StoredProcedure
                'SqlCmd.Parameters.AddWithValue("@DTFROM", txtFromDate.Text)
                'SqlCmd.Parameters.AddWithValue("@DTTO", txtToDate.Text)

                param(0) = New SqlClient.SqlParameter("@LOG_BSU_ID", Session("sBSUID"))
                param(1) = New SqlClient.SqlParameter("@ASONDT", UsrAsOnDate.SelectedText)
                param(2) = New SqlClient.SqlParameter("@EMP_ID", h_EMPID.Value.Replace("||", "|"))
                param(3) = New SqlClient.SqlParameter("@CAT_ID", UsrTreeView1.GetSelectedNode)
                param(4) = New SqlClient.SqlParameter("@ABC_CAT", strABC)
                param(5) = New SqlClient.SqlParameter("@LeaveTypeIDs", LeaveTypes)
                param(6) = New SqlClient.SqlParameter("@REP_EMP_ID", Report_Emp)
                param(7) = New SqlClient.SqlParameter("@ShowAll", chkShowAll.Checked)
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SP_RPT_Unit_LEAVE_BALANCE_COMBINEDREPORT", param)
                If ds.Tables(0).Rows.Count = 0 Then
                    lblError.Text = " No Data To Export"
                Else
                    ExportExcel(ds)
                End If
            Else
                Dim cmd As New SqlCommand("GetEmpConsolidatedLeaveDetails", objConn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpBSU_ID.Value = Session("sBSUID")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpCAT As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
                sqlpCAT.Value = UsrTreeView1.GetSelectedNode
                cmd.Parameters.Add(sqlpCAT)

                Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar)
                sqlpEMP_ID.Value = h_EMPID.Value.Replace("||", "|")
                cmd.Parameters.Add(sqlpEMP_ID)

                Dim sqlpLType_ID As New SqlParameter("@LeaveTypeIDs", SqlDbType.VarChar)
                sqlpLType_ID.Value = ""
                For Each Li As ListItem In CBLLeaveTypes.Items
                    If Li.Selected = True Then
                        sqlpLType_ID.Value += "|" & Li.Value
                    End If
                Next
                sqlpLType_ID.Value = sqlpLType_ID.Value
                cmd.Parameters.Add(sqlpLType_ID)

                Dim sqlpABC As New SqlParameter("@ABC_CAT", SqlDbType.VarChar)
                sqlpABC.Value = strABC
                cmd.Parameters.Add(sqlpABC)

                Dim sqlpREP_EMP_ID As New SqlParameter("@REP_EMP_ID", SqlDbType.BigInt)
                If txtReport.Text <> "" And hfReport.Value <> "" And Not chkAll.Checked Then
                    sqlpREP_EMP_ID.Value = hfReport.Value
                Else
                    sqlpREP_EMP_ID.Value = 0
                End If
                cmd.Parameters.Add(sqlpREP_EMP_ID)

                Dim sqlpFROMDT As New SqlParameter("@AsOnDate", SqlDbType.DateTime)
                sqlpFROMDT.Value = UsrAsOnDate.SelectedText
                cmd.Parameters.Add(sqlpFROMDT)

                ''Added new parameter by vikranth on 29th July 2019
                'Dim sqlpELeave As New SqlParameter("@ELeave", SqlDbType.Bit)
                'If rblLeave.SelectedValue = "Eleave" Then
                '    sqlpELeave.Value = True
                'Else sqlpELeave.Value = False
                'End If
                'cmd.Parameters.Add(sqlpELeave)

                'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("reportCaption") = "Employee Leave Status Report ( As On " & UsrAsOnDate.SelectedText & ")"
                params("BSUNAME") = UtilityObj.GetDataFromSQL(
                "select BSU_NAME from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' ",
                ConnectionManger.GetOASISConnectionString) & ""

                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmpAttendanceDetails.rpt"

                Session("ReportSource") = repSource
                If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)

                Else
                    ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                    ReportLoadSelection()
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub

    Private Sub GenerateAttendanceReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            'Written by vikranth on 31st July 2019
            If rblLeave.SelectedValue = "Eleave" Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim Report_Emp As String = String.Empty
                Dim LeaveTypes As String = String.Empty
                If txtReport.Text <> "" And hfReport.Value <> "" And Not chkAll.Checked Then
                    Report_Emp = hfReport.Value
                Else
                    Report_Emp = 0
                End If

                For Each Li As ListItem In CBLLeaveTypes.Items
                    If Li.Selected = True Then
                        LeaveTypes += "|" & Li.Value
                    End If
                Next
                'Dim objConn As New SqlConnection(str_conn)
                Dim param(9) As SqlClient.SqlParameter
                'Dim SqlCmd As New SqlCommand("rptEmployeeLeaves")
                'SqlCmd.CommandType = CommandType.StoredProcedure
                'SqlCmd.Parameters.AddWithValue("@DTFROM", txtFromDate.Text)
                'SqlCmd.Parameters.AddWithValue("@DTTO", txtToDate.Text)

                param(0) = New SqlClient.SqlParameter("@FROMDT", UsrDateFrom.SelectedText)
                param(1) = New SqlClient.SqlParameter("@TODT", UsrDateTo.SelectedText)
                param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
                param(3) = New SqlClient.SqlParameter("@EMP_ID", h_EMPID.Value.Replace("||", "|"))
                param(4) = New SqlClient.SqlParameter("@ABC_CAT", strABC)
                param(5) = New SqlClient.SqlParameter("@ShowAll", chkShowAll.Checked)
                param(6) = New SqlClient.SqlParameter("@REP_EMP_ID", Report_Emp)
                param(7) = New SqlClient.SqlParameter("@CAT_ID", UsrTreeView1.GetSelectedNode())
                param(8) = New SqlClient.SqlParameter("@LeaveTypeIDs", LeaveTypes)
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rptEmployeeLeaves_ELeave", param)
                If ds.Tables(0).Rows.Count = 0 Then
                    lblError.Text = " No Data To Export"
                Else
                    ExportExcel(ds)
                End If
            Else
                Dim cmd As New SqlCommand("GET_EMP_DAILYATT_REPORT", objConn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
                sqlpFROMDT.Value = UsrDateFrom.SelectedText
                cmd.Parameters.Add(sqlpFROMDT)

                Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
                sqlpTODT.Value = UsrDateTo.SelectedText
                cmd.Parameters.Add(sqlpTODT)

                Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpBSU_ID.Value = Session("sBSUID")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar)
                sqlpEMP_ID.Value = h_EMPID.Value.Replace("||", "|")
                cmd.Parameters.Add(sqlpEMP_ID)

                Dim sqlpABC As New SqlParameter("@ABC_CAT", SqlDbType.VarChar)
                sqlpABC.Value = strABC
                cmd.Parameters.Add(sqlpABC)

                Dim sqlpShowAll As New SqlParameter("@ShowAll", SqlDbType.Bit)
                sqlpShowAll.Value = chkShowAll.Checked
                cmd.Parameters.Add(sqlpShowAll)

                'V1.1
                Dim sqlpREP_EMP_ID As New SqlParameter("@REP_EMP_ID", SqlDbType.BigInt)
                'sqlpREP_EMP_ID.Value = Session("EmployeeId")
                'Changed by swapna based on new requirement
                If txtReport.Text <> "" And hfReport.Value <> "" And Not chkAll.Checked Then
                    sqlpREP_EMP_ID.Value = hfReport.Value
                Else
                    sqlpREP_EMP_ID.Value = 0
                End If
                cmd.Parameters.Add(sqlpREP_EMP_ID)

                Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
                sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
                cmd.Parameters.Add(sqlpCAT_ID)
                Dim sqlpLType_ID As New SqlParameter("@LeaveTypeIDs", SqlDbType.VarChar)
                sqlpLType_ID.Value = ""
                For Each Li As ListItem In CBLLeaveTypes.Items
                    If Li.Selected = True Then
                        sqlpLType_ID.Value += "|" & Li.Value
                    End If
                Next
                sqlpLType_ID.Value = sqlpLType_ID.Value
                cmd.Parameters.Add(sqlpLType_ID)

                ''Added new parameter by vikranth on 29th July 2019
                'Dim sqlpELeave As New SqlParameter("@ELeave", SqlDbType.Bit)
                'If rblLeave.SelectedValue = "Eleave" Then
                '    sqlpELeave.Value = True
                'Else sqlpELeave.Value = False
                'End If
                'cmd.Parameters.Add(sqlpELeave)

                'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("reportCaption") = "Daily Attendance Report (" & UtilityObj.GetDataFromSQL(
                "select BSU_NAME from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' ",
                ConnectionManger.GetOASISConnectionString) & ")"

                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/EMPDAILYAttendanceReport_WITH_DATE_RANGE.rpt"

                Session("ReportSource") = repSource
                '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
                Else
                    ReportLoadSelection()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GenerateAttendancePunchingDetailsReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim cmd As New SqlCommand("GET_EMP_DAILYATT_PUNCHING_REPORT", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
            sqlpFROMDT.Value = UsrDateFrom.SelectedText
            cmd.Parameters.Add(sqlpFROMDT)

            Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
            sqlpTODT.Value = UsrDateTo.SelectedText
            cmd.Parameters.Add(sqlpTODT)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("sBSUID")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar)
            sqlpEMP_ID.Value = h_EMPID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpABC As New SqlParameter("@ABC_CAT", SqlDbType.VarChar)
            sqlpABC.Value = strABC.Replace("||", "|")
            cmd.Parameters.Add(sqlpABC)

            Dim sqlpShowAll As New SqlParameter("@ShowAll", SqlDbType.Bit)
            sqlpShowAll.Value = chkShowAll.Checked
            cmd.Parameters.Add(sqlpShowAll)

            If bLateCheckin = True Then
                Dim sqlpbLateCheckin As New SqlParameter("@bLateCheckin", SqlDbType.Bit)
                sqlpbLateCheckin.Value = True
                cmd.Parameters.Add(sqlpbLateCheckin)
            End If

            'V1.1
            Dim sqlpREP_EMP_ID As New SqlParameter("@REP_EMP_ID", SqlDbType.BigInt)
            If txtReport.Text <> "" And hfReport.Value <> "" And Not chkAll.Checked Then
                sqlpREP_EMP_ID.Value = hfReport.Value
            Else
                sqlpREP_EMP_ID.Value = 0
            End If
            cmd.Parameters.Add(sqlpREP_EMP_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()

            cmd.Parameters.Add(sqlpCAT_ID)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            If bLateCheckin Then
                params("reportCaption") = "Employee Late Attendance Log ("
            Else
                params("reportCaption") = "Daily Attendance Report ("
            End If
            params("reportCaption") &= UtilityObj.GetDataFromSQL(
            "select BSU_NAME from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' ",
            ConnectionManger.GetOASISConnectionString) & ")"

            repSource.Parameter = params
            repSource.Command = cmd
            If rbEmployee.Checked = True Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/EMPDAILYAttendancePunchingReport_WITH_DATE_RANGE_Employee.rpt"
            ElseIf rbCategory.Checked = True Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/EMPDAILYAttendancePunchingReport_WITH_DATE_RANGE_Category.rpt"
            Else
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/EMPDAILYAttendancePunchingReport_WITH_DATE_RANGE_Date.rpt"
            End If

            Session("ReportSource") = repSource
            '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
            Else
                ReportLoadSelection()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "');", True)
        Else
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "','_blank');", True)
        End If
    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMPNO as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        FillEmpNames(h_EMPID.Value)
    End Sub
    'Added by vikranth on 29th July 2019
    Protected Sub rblLeave_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblLeave.SelectedIndexChanged
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "H000704", "H000705"
                If rblLeave.SelectedValue = "Eleave" Then
                    btnGenerateReport.Visible = False
                    lblrptCaption.Text = "Employee E-leave Report"
                Else btnGenerateReport.Visible = True
                    lblrptCaption.Text = "Employee Daily Attendance Report"
                End If

        End Select
    End Sub
    'Added by vikranth on 31st Jul 2019 
    Sub ExportExcel(ByVal ds As DataSet)

        If (ds.Tables.Count > 0) Then
            If (ds.Tables(0).Rows.Count > 0) Then
                Dim dt As DataTable = ds.Tables(0)
                Dim photopath As String = WebConfigurationManager.AppSettings("UploadExcelFile").ToString
                Dim tempFileName As String = photopath + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile

                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                'ws.InsertDataTable(dt, "A1", True)

                'ef.SaveXls(tempFileName)
                ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                ef.Save(tempFileName)

                Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

                'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(tempFileName)
                'HttpContext.Current.Response.Flush()
                'HttpContext.Current.Response.Close()

                System.IO.File.Delete(tempFileName)
            Else
                lblError.Text = " No Data To Export"
            End If
        End If

    End Sub
End Class