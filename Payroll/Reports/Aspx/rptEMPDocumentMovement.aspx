<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEMPDocumentMovement.aspx.vb" Inherits="rptFEEOutstandingDetails" title="Untitled Page" %>

<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits"
    TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">            
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
        ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%" >
        
        <tr runat="server" id ="trPeriod">
            <td align="left" width="20%">
                <span class="field-label">From Date</span></td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
            <td align="left" width="20%">
                <span class="field-label">To Date</span></td>
            <td align="left" width="30%">
                <asp:TextBox id="txtToDate" runat="server" CssClass="inputbox" Width="122px">
                </asp:TextBox>
                <asp:ImageButton id="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false">
                </asp:ImageButton>
                <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator id="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span class="field-label">Business Unit</span></td>
            <td align="left" colspan="3" valign="top">
                <div class="checkbox-list">
                <uc1:usrBSUnits ID="usrBSUnits1" runat="server" />
                </div>
            </td>
        </tr>
        <tr runat="server" id="tr_show_radiobuttons">
            <td align="left" valign="middle">
                <span class="field-label">Show</span></td>
            <td align="left" colspan="3" valign="middle">
                <asp:RadioButton id="radIssued" runat="server" CssClass="field-label" GroupName="SHOW"
                    Text="Issued"></asp:RadioButton>&nbsp;<asp:RadioButton id="radReturned" CssClass="field-label" runat="server" GroupName="SHOW" Text="Returned"></asp:RadioButton>
                <asp:RadioButton id="radAll" runat="server" CssClass="field-label" GroupName="SHOW" Text="All" Checked="True">
                </asp:RadioButton></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <ajaxtoolkit:calendarextender id="calFromDate1" runat="server" format="dd/MMM/yyyy"
        popupbuttonid="imgFromDate" targetcontrolid="txtFromDate">
    </ajaxtoolkit:calendarextender><ajaxtoolkit:calendarextender id="calFromDate2" runat="server" format="dd/MMM/yyyy" targetcontrolid="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate" PopupButtonID="imgToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>

        </div>
    </div>
</div>

</asp:Content>

