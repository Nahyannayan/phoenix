<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEMPSalaryReports.aspx.vb" Inherits="Reports_ASPX_Report_BankBook"
    Title="Untitled Page" %>

<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../../../Asset/UserControls/usrBSUnitsAsset.ascx" TagName="usrBSUnits"
    TagPrefix="uc2" %>
<%@ Register Src="../../../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">

        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'none') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=trSelBSU.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'none') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'none') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
            }
            return false;
        }

        function ToggleSearchBSUnit() {
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'display') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=trSelBSU.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfBSU.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelBSU.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=hfBSU.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchDepartment() {
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'display') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchCategory() {
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchDesignation() {
            if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'display') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDesignation.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=hfDesignation.ClientID %>').value = 'display';
            }
            return false;
        }



        function HideAll() {
            document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelBSU.ClientID %>').style.display = '';
            document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
            document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
            document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
            document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
        }

        function SearchHide() {
            document.getElementById('trBSUnit').style.display = 'none';
        }

        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=BSU", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=BSU", "pop_bsu");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientBsuClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_BSUID.ClientID %>').value = NameandCode;
                document.getElementById('<%=txtBSUName.ClientID%>').value = NameandCode;
                __doPostBack('<%=txtBSUName.ClientID%>', "");
            }
        }

        function GetDeptName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=DEPT", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=DEPT", "pop_dept");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_DEPTID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientDeptClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_DEPTID.ClientID %>').value = NameandCode;
                 document.getElementById('<%=txtDeptName.ClientID%>').value = NameandCode;
                __doPostBack('<%=txtDeptName.ClientID%>', "");
            }
        }

        function GetCATName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=CAT", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=CAT", "pop_cat");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_CATID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

         function OnClientCatClose(oWnd, args) {

            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_CATID.ClientID %>').value = NameandCode;
                document.getElementById('<%=txtCatName.ClientID%>').value = NameandCode;
                __doPostBack('<%=txtCatName.ClientID%>', "");
            }
        }


        function GetDESGName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("SelIDDESC.aspx?ID=DESG", "pop_up2")
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_DESGID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                //NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=h_DESGID.ClientID %>').value = arg.NameandCode;
                document.getElementById('<%=txtDesgName.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%= txtDesgName.ClientID%>', 'TextChanged');
            }
        }


        function GetEMPNAME() {

            var NameandCode;
            var result;

            //result = window.showModalDialog("../../SelIDDESC.aspx?ID=EMP&Bsu_id="+document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)
            result = radopen("../Aspx/SelIDDESC.aspx?ID=EMP", "pop_up")

            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>

        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                //NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=h_EMPID.ClientID %>').value = arg.NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetEMPNAME2() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("../../SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)

            <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
            var url = "../../SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value
            var oWnd = radopen(url, "pop_empnme");
        }

        function OnClientEmpClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                //NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=h_EMPID.ClientID %>').value = arg.NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }



        
        
       
    </script>
    <style type="text/css">
        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 100%;
            display: inline-block;
            list-style-type: none;
        }

        .col1,
        .col2,
        .col3,
        .col4 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 110px;
            line-height: 14px;
            float: left;
        }
    </style>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientBsuClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_empnme" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEmpClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_dept" runat="server" Behaviors="Close,Move" OnClientClose="OnClientDeptClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cat" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCatClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Employee Salary Slip"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%;">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" runat="server" id="tblMain"
                    cellpadding="0" cellspacing="0" width="100%">
                    <%--    <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 1px" valign="middle">
                <asp:Label ID="lblrptCaption" runat="server" Text="Employee Salary Slip"></asp:Label>&nbsp;
            </td>
        </tr>--%>
                    <tr id="trMonth_Year">
                        <td align="left" width="20%"><span class="field-label">Month</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlPayMonth" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlPayMonth_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Year </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlPayYear" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlPayYear_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trProcessingDate" runat="server">
                        <td align="left" width="20%"><span class="field-label">Processing Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlProcessingDate" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkExcludeProcessDate" runat="server" AutoPostBack="True" OnCheckedChanged="chkExcludeProcessDate_CheckedChanged"
                                Text="Exclude Process Date" CssClass="field-label"></asp:CheckBox>
                        </td>

                    </tr>
                    <tr id="trAsOnDate" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">As On Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True">
                            </asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgAsOn" TargetControlID="txtAsOnDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgAsOn" TargetControlID="txtAsOnDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgAsOn" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvAsOnDate" ControlToValidate="txtAsOnDate"
                                ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trBSUTree" runat="server">
                        <td align="left" width="20%" valign="top">
                            <asp:Label ID="lblBSUtext" runat="server" Text=" Business Unit" CssClass="field-label"></asp:Label>

                        </td>
                        <td align="left" colspan="2">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                        
                        <td></td>
                    </tr>
                    <tr id="treeCat" visible="true" runat="server">
                        <td align="left" width="20%" valign="top"><span class="field-label">Category</span>
                        </td>
                        <td align="left" width="30%" style="text-align: left">
                            <div class="checkbox-list">
                                <uc3:usrTreeView ID="UsrTreeView1" runat="server" />
                            </div>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trSelBSU">
                        <td colspan="4" align="left">
                            <asp:ImageButton ID="imgPlusBSUnit" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchBSUnit();return false;" CausesValidation="False" />
                            <span class="field-label">Select Business Unit Filter</span>
                        </td>

                    </tr>
                    <tr id="trBSUnit" runat="server" style="display: none;">
                        <td align="left" width="20%">
                            <asp:ImageButton ID="imgMinusBSUnit" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchBSUnit();return false;" CausesValidation="False" /><span class="field-label"><span class="field-label">Business Unit</span> </span>
                        </td>
                        <td align="left" colspan="3">(Enter the BSU ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtBSUName" runat="server" OnTextChanged="txtBSUName_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddBSUID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgGetBSUName" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick=" GetBSUName(); return false;" /><br />
                            <asp:GridView ID="grdBSU" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="BSU ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSUID" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="BSU_Name" HeaderText="BSU Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trSelDepartment">
                        <td align="left" colspan="4" valign="top" class="title-bg">
                            <asp:ImageButton ID="imgPlusDepartment" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                            <span class="field-label">Select Department Filter</span>
                        </td>

                    </tr>
                    <tr id="trDepartment" style="display: none;">
                        <td align="left" width="20%" valign="top">
                            <asp:ImageButton ID="imgMinusDepartment" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" /><span class="field-label">Department</span>
                        </td>
                        <td align="left" colspan="3">(Enter the Department ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDeptName" runat="server" CssClass="inputbox" OnTextChanged="txtDeptName_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDEPTID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick=" GetDeptName(); return false;" /><br />
                            <asp:GridView ID="gvDept" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="DEPT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDEPTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DEPT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDeptDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr id="trSelcategory">
                        <td align="left" colspan="4" class="title-bg">
                            <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                            <span class="field-label">Select Category Filter</span>
                        </td>

                    </tr>
                    <tr id="trCategory" style="display: none;">
                        <td align="left" width="20%">
                            <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Category</span>
                        </td>
                        <td align="left" colspan="3">(Enter the Category ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtCatName" runat="server" CssClass="inputbox" OnTextChanged="txtCatName_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick=" GetCATName(); return false;" /><br />
                            <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="CAT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr id="trSelDesignation">
                        <td align="left" colspan="4" class="title-bg">
                            <asp:ImageButton ID="imgPlusDesignation" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" />
                            <span class="field-label">Select Designation Filter</span>
                        </td>

                    </tr>
                    <tr id="trDesignation" style="display: none;">
                        <td align="left" width="20%">
                            <asp:ImageButton ID="imgMinusDesignation" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" /><span class="field-label">Designation</span>
                        </td>
                        <td align="left" colspan="3">(Enter the Designation ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDesgName" runat="server" OnTextChanged="txtDesgName_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDESGID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetDESGName(); return false;" /><br />
                            <asp:GridView ID="gvDesg" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="DESG ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDESGID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DESG NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDESGDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr id="trSelEMPName">
                        <td align="left" colspan="4" valign="top" class="title-bg">
                            <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                                OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                            <span class="field-label">Select Employee Name Filter</span>
                        </td>

                    </tr>
                    <tr id="trEMPName" style="display: none;">
                        <td align="left" width="20%">
                            <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                                OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" /><span class="field-label">Employee Name</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label><br />
                            <asp:TextBox ID="txtEMPNAME" runat="server" OnTextChanged="txtEMPNAME_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr id="trEMPABCCAT" runat="server">
                        <td align="left" width="20%"><span class="field-label">ABC Category</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" />
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" />
                        </td>

                    </tr>

                    <tr id="trInsurance" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Insurance Category (Data Available)</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="rbYes" runat="server" GroupName="IC" Text="YES" Checked="True"></asp:RadioButton>
                            <asp:RadioButton ID="rbNo" runat="server" GroupName="IC" Text="NO"></asp:RadioButton>
                            <asp:RadioButton ID="rbAll" runat="server" GroupName="IC" Text="ALL"></asp:RadioButton>
                        </td>

                    </tr>

                    <tr id="trDataUpdation" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Data Updation Status</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="rbCompleted" runat="server" GroupName="DUS" Text="COMPLETED" Checked="True"></asp:RadioButton>
                            <asp:RadioButton ID="rbPartial" runat="server" GroupName="DUS" Text="PARTIAL"></asp:RadioButton>
                            <asp:RadioButton ID="rbDataUpdationAll" runat="server" GroupName="DUS" Text="ALL"></asp:RadioButton>
                        </td>

                    </tr>




                    <tr id="trSignatory" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Signatory</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSignatory" runat="server" AutoPostBack="True" Visible="false"
                                OnSelectedIndexChanged="ddlPayMonth_SelectedIndexChanged">
                            </asp:DropDownList>
                            <telerik:RadComboBox runat="server" ID="rcbSignatory"
                                MarkFirstMatch="true" EnableLoadOnDemand="true" OnItemDataBound="rcbSignatory_ItemDataBound" EmptyMessage="Type an employee name to search."
                                OnItemsRequested="rcbSignatory_ItemsRequested"
                                HighlightTemplatedItems="true">
                                <HeaderTemplate>
                                    <ul>
                                        <li class="col1">EmpNo</li>
                                        <li class="col2">Name</li>
                                        <li class="col3">Designation</li>
                                        <li class="col4">Department</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>
                                        <li class="col1">
                                            <%#DataBinder.Eval(Container.DataItem, "EMPNO")%></li>
                                        <li class="col2">
                                            <%#DataBinder.Eval(Container.DataItem, "EMPNAME")%></li>
                                        <li class="col3">
                                            <%#DataBinder.Eval(Container.DataItem, "EMP_DES_DESCR")%></li>
                                        <li class="col4">
                                            <%#DataBinder.Eval(Container.DataItem, "EMP_DEPT_DESCR")%></li>
                                    </ul>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trOnLeaveSeparation" runat="server" visible="false">
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkOnLeaveSeparation" runat="server" Text="On Leave Separation"  CssClass="field-label"/>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr id="trFinalSettlement" runat="server" visible="false">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkFF" runat="server" Text="Final Settlement" CssClass="field-label" />
                        </td>
                     
                    </tr>
                    <tr id="trCombined" runat="server" visible="False">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkCombined" runat="server" Text="Combined (Normal + Final Settlement)" CssClass="field-label" />
                        </td>
                        
                    </tr>
                    <tr id="trWPS" runat="server">
                        <td align="left" width="20%">
                            <asp:RadioButton ID="radWithWPS" runat="server" GroupName="WPS" Text="With WPS"></asp:RadioButton>
                            <asp:RadioButton ID="radWithoutWPS" runat="server" GroupName="WPS" Text="Without WPS"></asp:RadioButton>
                            <asp:RadioButton ID="radAll" runat="server" Checked="True" GroupName="WPS" Text="All"></asp:RadioButton>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr id="trSummary">
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkSummary" runat="server" Text="Summary" CssClass="field-label" />
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr id="tryear" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Calendar Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trPrintA3" runat="server" visible="false">
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkPrintA3" runat="server" Text="Print On A3 Paper" CssClass="field-label" />
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr id="trShowAllMonths" runat="server" visible="false">
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkShowAllMonths" runat="server" Text="Show All Months" CssClass="field-label" />
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr runat="server" visible="false" id="trException">
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkException" runat="server" Text="Exception Report" CssClass="field-label" /></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnExportExcel" runat="server" CssClass="button" Text="Export To Excel" Visible="false" />
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_DEPTID" runat="server" />
                <asp:HiddenField ID="h_CATID" runat="server" />
                <asp:HiddenField ID="h_DESGID" runat="server" />
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfDepartment" runat="server" type="hidden" />
                <input id="hfCategory" runat="server" type="hidden" />
                <input id="hfDesignation" runat="server" type="hidden" />
                <input id="hfEmpName" runat="server" type="hidden" />

            </div>
        </div>
    </div>
</asp:Content>
