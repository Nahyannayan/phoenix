Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String
    'Version            Author          Date            Change
    '1.1                Swapna          27/mar/2012     To add staff transfer report
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle
                BindCategory()
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            If Not IsPostBack Then
                UsrDatePicker1.SetDate = Format(DateTime.Now, OASISConstants.DateFormat)
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If MainMnu_code = "P150027" Then

                    txtToDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
                End If
            End If
            Select Case MainMnu_code
                Case "P150027"
                    StoreEMPFilter()
                    trdateRow.Visible = False
                    trDateSel.Visible = True
                    trChkreportName.Visible = True
                    'trEMPName.Visible = True
                    trSelEMPName.Visible = True
                    trToBsu.Visible = True

            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function
    'V1.1
    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub
    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "H000702"
                GenerateAttendanceReport()
            Case "P150027"
                GenerateStaffTransferReport() 'V1.1 

        End Select

    End Sub
    'V1.1 
    Private Sub GenerateStaffTransferReport()
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("Month") = "January"
        params("@FromDt") = txtFromDate.Text
        params("@ToDt") = txtToDate.Text
        params("@Bsu_id") = UsrBSUnits1.GetSelectedNode("|")
        params("@ToBsu_id") = UsrBSUnits2.GetSelectedNode("|")
        params("@CAT_id") = h_CATID.Value

        params("@EMPID") = h_EMPID.Value
        params("@Filter") = rblReportFilter.SelectedValue

        params("@IMG_BSU_ID") = Session("sBsuid")
        params("@IMG_TYPE") = "LOGO"



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = params
            'If ViewState("MainMnu_code") = "S200055" Then
            ' If rblReportFilter.SelectedValue = "1" Then
            '.reportPath = Server.MapPath("../Rpt/rptEmpAllLeaveDetails.rpt")
            'ElseIf rblReportFilter.SelectedValue = "2" Then
            '.reportPath = Server.MapPath("../Rpt/rptEmpleaves_Monthwise.rpt")
            'Else

            .reportPath = Server.MapPath("../Rpt/empStaffTransfers.rpt")

            ' End If


            'End If
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("../../../Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Private Sub GenerateAttendanceReport()

        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("EMPDAILYAttendanceReport", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpDATE As New SqlParameter("@DATE", SqlDbType.DateTime)
            sqlpDATE.Value = UsrDatePicker1.SelectedText
            cmd.Parameters.Add(sqlpDATE)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode()
            cmd.Parameters.Add(sqlpCAT_ID)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "Daily Attendance Report"

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/EMPDAILYAttendanceReport.rpt"

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection_2()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function
   
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        FillEmpNames(h_EMPID.Value)
    End Sub
End Class