Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Version        Date        Author          Change
'1.1            Jun-1-2011  Swapna          Added link for Adjustment report-menu id P150048
'1.2            Sep-11-2011 swapna          New report for employee card 
'1.3            Dec-13-2011 swapna          Cash/bank summary report added
'1.4                03-Jun-2012     Swapna          To add Final Settlement filter fro salary reports
Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle

                Select Case MainMnu_code
                    Case "P152001"
                        imgGetBSUName.OnClientClick = "GetBSUName(); return false;"
                        lblrptCaption.Text = "Employee Master Details"
                        trMonth_Year.Visible = False
                        imgEmpSel.OnClientClick = "GetEMPNAME_Current(); return false;"
                        chkSummary.Checked = True
                    Case "P152020", "P152030"  'Bank master � Employee wise
                        imgGetBSUName.OnClientClick = "GetBSUName(); return false;"
                        If MainMnu_code = "P152020" Then
                            lblrptCaption.Text = "Category Count"
                        Else
                            lblrptCaption.Text = "Bank master � Employee wise"
                        End If
                        trMonth_Year.Visible = False
                        trSelDepartment.Visible = False
                        trDepartment.Visible = False
                        trSelcategory.Visible = False
                        trCategory.Visible = False
                        trSelDesignation.Visible = False
                        trDesignation.Visible = False
                        trSelEMPName.Visible = False
                        trEMPName.Visible = False
                        imgEmpSel.OnClientClick = "return GetEMPNAME_Current(); return false;"
                        trSummary.Visible = False
                        chkSummary.Visible = False
                    Case "P152002"  'Category wise Employee Details
                        imgGetBSUName.OnClientClick = "GetBSUName(); return false;"
                        lblrptCaption.Text = "Category wise Employee Details"
                        trMonth_Year.Visible = False
                        imgEmpSel.OnClientClick = "GetEMPNAME_Current(); return false;"
                        trSummary.Visible = False
                        chkSummary.Visible = False
                    Case "P152026" 'Deduction
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle(); return false;"
                        lblrptCaption.Text = "Employees Monthly Deductions Details"
                        trSummary.Visible = False
                        imgEmpSel.OnClientClick = "GetEMPNAME_Current(); return false;"
                    Case "P152025" ' Earnings
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle(); return false;"
                        lblrptCaption.Text = "Employees Monthly Earnings Details"
                        trSummary.Visible = False
                    Case "P150045"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle(); return false;"
                        lblrptCaption.Text = "Employees Salary Details"
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trEMPABCCAT.Visible = False
                    Case "P150030", "P150031"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle(); return false;"
                        lblrptCaption.Text = "Employees Salary Reconciliation"
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trEMPABCCAT.Visible = False
                    Case "P150050"
                        lblrptCaption.Text = "Employees Salary Deductions"
                        trSummary.Visible = False
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        trFinalSettlement.Visible = True 'V1.4
                    Case "P150065"
                        lblrptCaption.Text = "Employees Salary Earnings"
                        trSummary.Visible = False
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        trFinalSettlement.Visible = True 'V1.4
                    Case "P150025", "P150040", "P150035", "P150026"
                        imgGetBSUName.OnClientClick = "GetBSUName(); return false;"
                        Select Case MainMnu_code
                            Case "P150025"
                                lblrptCaption.Text = "Employees Salary Summary"
                            Case "P150035"
                                lblrptCaption.Text = "Employees Salary Summary With Coinage"
                            Case "P150040"
                                lblrptCaption.Text = "Employees Loan Details"
                            Case "P150026"
                                lblrptCaption.Text = "Employees Salary Comparison(Actual/Contract)"
                        End Select
                    Case "P150055"
                        lblrptCaption.Text = "Employee Loan Summary"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                    Case "P150047"
                        lblrptCaption.Text = "LOP Details"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle(); return false;"
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trSummary.Visible = False
                        imgEmpSel.OnClientClick = "GetEMPNAME_Current(); return false;"
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                    Case "P150048"    'V1.1
                        lblrptCaption.Text = "Adjustment Details"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle(); return false;"
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trSummary.Visible = False
                        imgEmpSel.OnClientClick = "GetEMPNAME_Current(); return false;"
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                    Case "H000170"  'V1.2
                        lblrptCaption.Text = "Employee Card"
                        trMonth_Year.Visible = False
                        imgEmpSel.OnClientClick = "GetEMPNAMEInBSU(); return false;"
                        trGratuity.Visible = True
                        chkShowGratuity.Checked = False
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trSummary.Visible = False
                    Case "P150058"
                        lblrptCaption.Text = "Salary Reconciliation Statement Summary"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trEMPABCCAT.Visible = True
                        trSummary.Visible = True
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        chkSummary.Checked = True

                    Case "P159046"
                        lblrptCaption.Text = "Cash/Bank Salary Payment Summary"
                        trBSUnit.Visible = True
                        trSelBSU.Visible = True
                        trCategory.Visible = True
                        trSelcategory.Visible = True
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trEMPABCCAT.Visible = True
                        trSummary.Visible = False
                        trPayMode.Visible = True
                End Select

            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            Select Case MainMnu_code
                Case "P152001", "P152002", "P152026", "P152025", "P150025", "P150040", "P150035", "P150026", "H000170", "P150047", "P150048", "P150055", "P150050", "P150065"
                    FillDeptNames(h_DEPTID.Value)
                    FillCATNames(h_CATID.Value)
                    FillDESGNames(h_DESGID.Value)
                    FillEmpNames(h_EMPID.Value)
                    StoreEMPFilter()

                Case "P159046"
                    FillCATNames(h_CATID.Value)
            End Select
            If Not IsPostBack Then
                FillPayYearPayMonth()
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Protected Sub h_BSUID_ValueChanged(sender As Object, e As EventArgs) Handles h_BSUID.ValueChanged
    '    Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
    '    FillBSUNames(h_BSUID.Value)
    '    h_BSUID.Value = ""
    'End Sub

    'Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
    '    Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
    '    FillDeptNames(h_DEPTID.Value)
    '    h_DEPTID.Value = ""
    'End Sub

    'Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
    '    Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
    '    FillCATNames(h_CATID.Value)
    '    h_CATID.Value = ""
    'End Sub

    'Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
    '    Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
    '    FillDESGNames(h_DESGID.Value)
    '    h_DESGID.Value = ""
    'End Sub

    'Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
    '    Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
    '    FillEmpNames(h_EMPID.Value)
    '    h_EMPID.Value = ""
    'End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        'swapna changed
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "P150025"
                GenerateSalarySummary()
            Case "P150030"
                GenerateSalaryReconciliation()
            Case "P150031"
                GenerateSalaryComparison()
            Case "P150035"
                GenerateSalarySummaryWithCoinage()
            Case "P150040"
                GenerateLoanDetails()
            Case "P150055"
                GenerateLoanSummary()
            Case "P152001"
                If chkSummary.Checked Then
                    GenerateEmployeeMasterSummary()
                Else
                    GenerateEmployeeMasterHistory()
                End If
            Case "P152020"
                GenerateCategoryCount()
            Case "P152030"
                GenerateBankEmployeewise()
            Case "P152002"
                GenerateEmployeeDetailsCategorywise()
            Case "P150045"
                GenerateSalaryDetails()
            Case "P152026" 'Deduction
                GenerateEmployeeEarningsDeductionReport("D")
            Case "P152025" ' Earnings
                GenerateEmployeeEarningsDeductionReport("E")
            Case "P150026"
                GenerateSalaryCompare_Actual_Contract()
            Case "P150050"
                GenerateSalaryDeductionEarning(True)
            Case "P150065"
                GenerateSalaryDeductionEarning(False)
            Case "P150046"
                GenerateSalarySummaryFormat1()
            Case "P150047"
                GenerateLOPReport()
            Case "P150048"    'V1.1
                GenerateAdjustmentReport()
            Case "H000170"
                GenarateEmployeeCard()
            Case "P150058"
                GetSalaryRecon()
            Case "P159046"
                GetBankCashPaymentReport()
        End Select
    End Sub
    Private Sub GetBankCashPaymentReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim cmd As New SqlCommand("rptBank_CashSalarySummary", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@ESD_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@ESD_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BsuIDs", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = h_BSUID.Value
            cmd.Parameters.Add(sqlpBSU_ID)


            Dim sqlpEMPCAT As New SqlParameter("@CatIds", SqlDbType.VarChar, 5000)
            sqlpEMPCAT.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpEMPCAT)

            Dim sqlpEMPABC As New SqlParameter("@ABC", SqlDbType.VarChar, 10)
            sqlpEMPABC.Value = strABC
            cmd.Parameters.Add(sqlpEMPABC)

            Dim sqlpMode As New SqlParameter("@Mode", SqlDbType.VarChar, 4)


            If chkBank.Checked = True And chkCash.Checked = False Then
                sqlpMode.Value = 1
            ElseIf chkBank.Checked = False And chkCash.Checked = True Then
                sqlpMode.Value = 0
            Else
                sqlpMode.Value = ""
            End If

            cmd.Parameters.Add(sqlpMode)



            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim params As New Hashtable
            params.Add("@CatIds", h_CATID.Value)
            params.Add("@ESD_MONTH", sqlpPAYMONTH.Value)
            params.Add("@ESD_YEAR", sqlpPAYYEAR.Value)
            params.Add("@BSUIds", h_BSUID.Value)
            params.Add("@ABC", strABC)
            params.Add("@mode", sqlpMode.Value)
            params.Add("@IMG_BSU_ID", Session("sbsuid"))
            params.Add("@IMG_TYPE", "LOGO")

            params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
            params.Add("reportCaption", "Salary Payment Details for  " + MonthName(ddlPayMonth.SelectedValue) + " to " + ddlPayYear.SelectedValue)

            ' params("VoucherName") = labHead.Text.ToUpper().ToString()   'V1.1

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "Oasis"
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../Rpt/rptBSUCash_BankSalSummary.rpt")
                '.reportPath = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"

                'End If
            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection_2()
            'Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            'repSourceSubRep(0).Command = cmdSub
            'repSource.SubReport = repSourceSubRep

            'params("userName") = Session("sUsr_name")
            'params("@IMG_BSU_ID") = Session("SBSUID")
            'params("@IMG_TYPE") = "logo"
            'repSource.Parameter = params
            'repSource.Command = cmd
            'repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptBSUCash_BankSalSummary.rpt"
            'Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub GetSalaryRecon()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "|B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "|C"
            End If

            Dim cmd As New SqlCommand("GetSalaryRecon", objConn)
            cmd.CommandType = CommandType.StoredProcedure
            '[dbo].[GetSalaryRecon] (@BSU_ID varchar(20),@Month int,@Year int)
            Dim sqlpPAYMONTH As New SqlParameter("@Month", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@Year", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ABCs As New SqlParameter("@EMP_ABCs", SqlDbType.VarChar, 30)
            sqlpEMP_ABCs.Value = strABC
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            If chkSummary.Checked Then params("EMPABC") = strABC
            params("RPT_CAPTION") = "Salary Reconciliation Statement Summary "
            '(" & UtilityObj.GetDataFromSQL( _
            '            "select BSU_NAME from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' ", _
            '            ConnectionManger.GetOASISConnectionString) & ")"
            repSource.Parameter = params
            repSource.Command = cmd
            If chkSummary.Checked Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetSalaryRecon.rpt"
            Else
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetSalaryRecon_Detail.rpt"
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    'V1.1 addition
    Private Sub GenerateAdjustmentReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            Dim cmd As New SqlCommand("EMP_Adjustment_Report", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value.Replace("||", "")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMPCAT As New SqlParameter("@EMP_CAT", SqlDbType.VarChar)
            sqlpEMPCAT.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpEMPCAT)

            Dim sqlpABC As New SqlParameter("@ABC_CAT", SqlDbType.VarChar)
            sqlpABC.Value = strABC
            cmd.Parameters.Add(sqlpABC)

            Dim sqlpEMPIDs As New SqlParameter("@EMPIDs", SqlDbType.VarChar)
            sqlpEMPIDs.Value = h_EMPID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpEMPIDs)



            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            repSource.Parameter = params
            repSource.Command = cmd
            'repSource.DisplayGroupTree = False
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptAdjustmentReport.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    'V1.2 addition
    Private Sub GenarateEmployeeCard()
        Try
            If h_EMPID.Value = "" Then
                lblError.Text = "Please select an employee"
                Exit Sub
            End If
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("@Emp_id") = h_EMPID.Value
            'params("@SubBSU_Id") = h_EMPID.Value
            params("@BSU_ID") = Session("sBsuid").ToString
            params("@IMG_BSU_ID") = Session("sBsuid").ToString
            'params("@SubBSU_ID") = Session("sBsuid").ToString
            params("@IMG_TYPE") = "Logo"
            params("@ShowGratuity") = chkShowGratuity.Checked
            ' repSource.Command = cmd
            'repSource.DisplayGroupTree = False

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = params
                .reportPath = Server.MapPath("../RPT/rptEmployeeCard.rpt")
                '.selectionFormula = "exec getrptEmployeeCardDetails "

            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection_2()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateLOPReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If


            Dim cmd As New SqlCommand("[EMP_LOP_REPORT]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value.Replace("||", "")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMPCAT As New SqlParameter("@EMP_CAT", SqlDbType.VarChar)
            sqlpEMPCAT.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpEMPCAT)

            Dim sqlpABC As New SqlParameter("@ABC_CAT", SqlDbType.VarChar)
            sqlpABC.Value = strABC
            cmd.Parameters.Add(sqlpABC)

            Dim sqlpEMPIDs As New SqlParameter("@EMPIDs", SqlDbType.VarChar)
            sqlpEMPIDs.Value = h_EMPID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpEMPIDs)




            'Dim sqlpECT_ID As New SqlParameter("@ECT_ID", SqlDbType.VarChar)
            'sqlpECT_ID.Value = hfCategory.Value
            'cmd.Parameters.Add(sqlpECT_ID)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMP_LOPReport.rpt"
            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryFormat1()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("[EMPSalaryDetails_Format1]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value.Replace("||", "sss")
            cmd.Parameters.Add(sqlpBSU_ID)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            params("PROCESS_DATE") = ""
            params("IsFinalSettlement") = 0
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1.rpt"
            Session("ReportSource") = repSource
            '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalaryDeductionEarning(ByVal bDeduction As Boolean)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("EMP_BSU_ID", h_BSUID.Value, False)

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If


            Dim cmd As New SqlCommand("GenerateSalaryDeductionEarning")
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = strABC
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpDESID As New SqlParameter("@DES_ID", SqlDbType.VarChar, 20)
            sqlpDESID.Value = h_DESGID.Value
            cmd.Parameters.Add(sqlpDESID)

            Dim sqlpDPT_ID As New SqlParameter("@DPT_ID", SqlDbType.VarChar, 20)
            sqlpDPT_ID.Value = h_DEPTID.Value
            cmd.Parameters.Add(sqlpDPT_ID)

            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 20)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpTYPE As New SqlParameter("@TYPE", SqlDbType.VarChar, 20)
            If bDeduction Then
                sqlpTYPE.Value = "D"
            Else
                sqlpTYPE.Value = "E"
            End If
            cmd.Parameters.Add(sqlpTYPE)

            Dim sqlpFF As New SqlParameter("@IsFinalSettlement", SqlDbType.Bit)  'V1.4
            sqlpFF.Value = chkFF.Checked
            cmd.Parameters.Add(sqlpFF)


            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim strText As String = ""
            If chkFF.Checked = True Then
                strText = "- Final Settlement"
            End If
            If bDeduction Then
                params("RPT_CAPTION") = " Salary Deduction ( " & ddlPayMonth.SelectedItem.Text.ToUpper & " - " & ddlPayYear.SelectedItem.Text & " )" + strText
            Else
                params("RPT_CAPTION") = " Salary Earnings ( " & ddlPayMonth.SelectedItem.Text.ToUpper & " - " & ddlPayYear.SelectedItem.Text & " )" + strText
            End If

            repSource.Parameter = params
            repSource.Command = cmd
            'repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryEarn_Ded.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            'ReportLoadSelection()
            ReportLoadSelection()
            'Else
            'lblError.Text = "No Records with specified condition"
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateEmployeeEarningsDeductionReport(ByVal type As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strFiltBSUID As String = GetFilter("EDD_BSU_ID", h_BSUID.Value, False)

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim str_Sql As String = " select * from vw_OSO_EMPMONTHLY_E_D WHERE  " & _
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EDD_EMP_ID", h_EMPID.Value, True) & _
            " AND EDD_PAYMONTH = " & ddlPayMonth.SelectedValue & _
            " AND EDD_TYPE ='" & type & "' " & _
            " AND EDD_PAYYEAR = " & ddlPayYear.SelectedValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("@IMG_BSU_ID") = Session("sbsuid")
                'params("@IMG_TYPE") = "LOGO"
                Select Case type
                    Case "E"
                        params("reportCaption") = "SALARY EARNINGS REPORT FOR THE MONTH OF " & ddlPayMonth.SelectedItem.Text.ToUpper() & "-" & ddlPayYear.SelectedItem.Text
                    Case "D"
                        params("reportCaption") = "SALARY DEDUCTIONS REPORT FOR THE MONTH OF " & ddlPayMonth.SelectedItem.Text.ToUpper() & "-" & ddlPayYear.SelectedItem.Text
                End Select

                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeEarn_Ded.rpt"
                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                'ReportLoadSelection()
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateEmployeeMasterHistory()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("EMP_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = " SELECT * FROM vw_OSO_EMPLOYEEMASTERDETAILS " _
                            & " WHERE " & strFiltBSUID & " ORDER BY EMP_STATUS_DESCR"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                'params("userName") = Session("sUsr_name")

                Dim repSourceSubRep(1) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubDesg_Cat As New SqlCommand()
                cmdSubDesg_Cat.Connection = New SqlConnection(str_conn)
                cmdSubDesg_Cat.CommandType = CommandType.Text
                cmdSubDesg_Cat.CommandText = "select * from dbo.VW_OSO_EMPQUALIFICATION "
                repSourceSubRep(0).Command = cmdSubDesg_Cat

                repSource.SubReport = repSourceSubRep
                repSource.Parameter = params
                repSource.Command = cmd
                'repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeMasterHistory.rpt"
                Session("ReportSource") = repSource
                '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateEmployeeMasterSummary()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("EMP_BSU_ID", h_BSUID.Value, False)

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim str_Sql As String = " SELECT * FROM vw_OSO_EMPLOYEEMASTERDETAILS WHERE  " & _
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EMP_ID", h_EMPID.Value, True)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text.ToUpper
                'params("PAYYEAR") = ddlPayYear.SelectedValue
                repSource.Parameter = params
                repSource.Command = cmd
                'repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeMasterSummary.rpt"
                'repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmpMasterDetailsCategorywise.rpt"
                Session("ReportSource") = repSource
                ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateSalaryDetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = " SELECT     ESD.* , BNK.*" _
                & " FROM         VW_OSO_SALARYDATA AS ESD LEFT OUTER JOIN" _
                & " BANK_M AS BNK ON ESD.EMP_BANK = BNK.BNK_ID " _
                            & " WHERE " & strFiltBSUID & _
            " AND  ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("PAYMONTH") = ddlPayMonth.SelectedItem.Text.ToUpper
                params("PAYYEAR") = ddlPayYear.SelectedValue

                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummary_Detailed.rpt"
                Session("ReportSource") = repSource
                ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateSalaryCompare_Actual_Contract()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = " SELECT     ESD.* , BNK.*" _
                & " FROM         VW_OSO_SALARYDATA AS ESD LEFT OUTER JOIN" _
                & " BANK_M AS BNK ON ESD.EMP_BANK = BNK.BNK_ID " _
                            & " WHERE " & strFiltBSUID & _
                            GetFilter("ESD_ECT_ID", h_CATID.Value, True) & _
                            GetFilter("ESD_DPT_ID", h_DEPTID.Value, True) & _
                            GetFilter("ESD_DES_ID", h_DESGID.Value, True) & _
                            GetFilter("ESD_EMP_ID", h_EMPID.Value, True) & _
                            " AND  ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("PAYMONTH") = ddlPayMonth.SelectedItem.Text.ToUpper
                params("PAYYEAR") = ddlPayYear.SelectedValue

                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryCompare_actual_Contract.rpt"
                Session("ReportSource") = repSource
                ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateSalaryReconciliation()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("GETSALARYRECONCILIATION", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value
            cmd.Parameters.Add(sqlpBSU_ID)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("CurrentDate") = New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, 1)
            repSource.Parameter = params
            repSource.Command = cmd
            If chkSummary.Checked Then
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryReconciliationSummary.rpt"
            Else
                params("Summary") = True
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryReconciliation.rpt"
            End If
            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalaryComparison()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("GETSALARYCOMPARISON", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpPAYMONTH As New SqlParameter("@PAYMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@PAYYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value.Replace("||", "")
            cmd.Parameters.Add(sqlpBSU_ID)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("CurrentDate") = New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, 1)
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryComparison.rpt"
            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateSalarySummaryWithCoinage()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = "select * from dbo.vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("ESD_EMP_ID", h_EMPID.Value, True) & _
            " AND ESD_MONTH = " & ddlPayMonth.SelectedValue & _
            " AND ESD_MODE = 0 " & _
            " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim dr As SqlDataReader
            dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If dr.HasRows Then
                Dim strAmount As String = String.Empty
                Dim strBSUID As String = String.Empty
                While (dr.Read())
                    strAmount += dr("ESD_EARN_NET").ToString + "||"
                    strBSUID += dr("ESD_BSU_ID") + "||"
                End While
                strBSUID = GenerateXML(strBSUID, XMLType.BSUName)
                strAmount = GenerateXML(strAmount, XMLType.AMOUNT)
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")

                Dim repSourceSubRep(3) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubCoinage As New SqlCommand("getCoinage", New SqlConnection(str_conn))
                cmdSubCoinage.CommandType = CommandType.StoredProcedure

                Dim sqlpAmount As New SqlParameter("@Amount", SqlDbType.Xml)
                sqlpAmount.Value = strAmount
                cmdSubCoinage.Parameters.Add(sqlpAmount)

                Dim sqlpbsu_id As New SqlParameter("@BSUIDs", SqlDbType.Xml)
                sqlpbsu_id.Value = strBSUID
                cmdSubCoinage.Parameters.Add(sqlpbsu_id)

                repSourceSubRep(0).Command = cmdSubCoinage

                repSourceSubRep(1) = New MyReportClass

                repSourceSubRep(1).Command = cmdSubCoinage

                repSourceSubRep(2) = New MyReportClass
                repSourceSubRep(2).Command = cmdSubCoinage

                repSource.SubReport = repSourceSubRep
                repSource.Parameter = params
                repSource.Command = cmd
                If chkSummary.Checked Then
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummaryDetailsCoinage.rpt"
                Else
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummaryDetailsCoinage.rpt"
                End If
                repSource.IncludeBSUImage = True
                Session("ReportSource") = repSource
                ReportLoadSelection()
                '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateSalarySummary()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = "select * from dbo.vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("ESD_EMP_ID", h_EMPID.Value, True) & _
            " AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")

                repSource.Parameter = params
                repSource.Command = cmd
                If chkSummary.Checked Then
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummary.rpt"
                Else
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummaryDetails.rpt"
                End If
                Session("ReportSource") = repSource
                ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GenerateLoanSummary()
        Try
            Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim cmd As New SqlCommand("[EMPLOYEELOAN_SUMMARY]", objConn)
            cmd.CommandType = CommandType.StoredProcedure
            '[dbo].[GetSalaryRecon] (@BSU_ID varchar(20),@Month int,@Year int)
            Dim sqlpPAYMONTH As New SqlParameter("@ASONMONTH", SqlDbType.Int)
            sqlpPAYMONTH.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpPAYMONTH)

            Dim sqlpPAYYEAR As New SqlParameter("@ASONYEAR", SqlDbType.Int)
            sqlpPAYYEAR.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpPAYYEAR)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_CATs As New SqlParameter("@CAT_ID", SqlDbType.VarChar, 500)
            sqlpEMP_CATs.Value = h_CATID.Value
            cmd.Parameters.Add(sqlpEMP_CATs)

            Dim sqlpEMP_IDs As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 500)
            sqlpEMP_IDs.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_IDs)

            Dim sqlpEMP_ABCs As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 30)
            sqlpEMP_ABCs.Value = strABC
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            params("RPT_CAPTION") = "Loan Summary Report (" & UtilityObj.GetDataFromSQL( _
            "select BSU_NAME from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' ", _
            ConnectionManger.GetOASISConnectionString) & ")"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeLoanSummary.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GenerateLoanDetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("ELH_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = "select ELH_EMP_ID from dbo.vw_OSO_EMPLOANDETAILS WHERE " & _
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("ELH_EMP_ID", h_EMPID.Value, True)

            Dim dr As SqlDataReader
            dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If dr.HasRows Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                repSource.Parameter = params
                Dim cmd As SqlCommand
                If chkSummary.Checked Then
                    Dim EMPIDs As String = String.Empty
                    While (dr.Read())
                        EMPIDs += dr("ELH_EMP_ID").ToString + "||"
                    End While
                    params("ASONDT") = Format(New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, 1), OASISConstants.DateFormat)

                    cmd = New SqlCommand("rptGetLoanSchedule", New SqlConnection(str_conn))
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpASONDT As New SqlParameter("@ASONDT", SqlDbType.DateTime)
                    sqlpASONDT.Value = New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, 1)
                    cmd.Parameters.Add(sqlpASONDT)

                    Dim sqlpEMPIDs As New SqlParameter("@EMPIDs", SqlDbType.Xml)
                    sqlpEMPIDs.Value = GenerateXML(EMPIDs, XMLType.EMPName)
                    cmd.Parameters.Add(sqlpEMPIDs)

                    Dim sqlpBSUIDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
                    sqlpBSUIDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
                    cmd.Parameters.Add(sqlpBSUIDs)
                    repSource.Command = cmd
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLOANSummary.rpt"
                Else
                    params("userName") = Session("sUsr_name")
                    repSource.Parameter = params
                    cmd = New SqlCommand
                    cmd.Connection = New SqlConnection(str_conn)
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = "select * from dbo.vw_OSO_EMPLOANDETAILS WHERE " & _
                    strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
                    GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
                    GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
                    GetFilter("ELH_EMP_ID", h_EMPID.Value, True)

                    repSource.Command = cmd
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLOANDetails.rpt"
                End If
                Session("ReportSource") = repSource
                '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID,EMPNO as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Private Sub GenerateEmployeeDetailsCategorywise()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("EMP_BSU_ID", h_BSUID.Value, False)

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim str_Sql As String = " SELECT * FROM vw_OSO_EMPLOYEEMASTERDETAILS WHERE  " & _
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) & _
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EMP_ID", h_EMPID.Value, True)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmpMasterDetailsCategorywise.rpt"
                Session("ReportSource") = repSource
                '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateBankEmployeewise()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim cmd As New SqlCommand("spgetbank_employeewisedetails", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = h_BSUID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpABC As New SqlParameter("@EMP_ABC", SqlDbType.VarChar)
            sqlpABC.Value = strABC
            cmd.Parameters.Add(sqlpABC)

            Dim sqlpEMPIDs As New SqlParameter("@EMP_ID", SqlDbType.VarChar)
            sqlpEMPIDs.Value = h_EMPID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpEMPIDs)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            repSource.Parameter = params
            repSource.Command = cmd
            'repSource.DisplayGroupTree = False
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptBank_EmployeeWiseDetails.rpt"

            Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub GenerateCategoryCount()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim cmd As New SqlCommand("rpt_CategoryCount", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_ID.Value = h_BSUID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpABC As New SqlParameter("@EMP_ABC", SqlDbType.VarChar)
            sqlpABC.Value = strABC
            cmd.Parameters.Add(sqlpABC)

            Dim sqlpEMPIDs As New SqlParameter("@EMP_ID", SqlDbType.VarChar)
            sqlpEMPIDs.Value = h_EMPID.Value.Replace("||", "|")
            cmd.Parameters.Add(sqlpEMPIDs)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            repSource.Parameter = params
            repSource.Command = cmd
            'repSource.DisplayGroupTree = False
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptCategoryCount.rpt"

            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub txtBSUName_TextChanged(sender As Object, e As EventArgs)
        txtBSUName.Text = ""
        FillBSUNames(h_BSUID.Value)
    End Sub
    Protected Sub txtDeptName_TextChanged(sender As Object, e As EventArgs)
        txtDeptName.Text = ""
        FillDeptNames(h_DEPTID.Value)
    End Sub
    Protected Sub txtCatName_TextChanged(sender As Object, e As EventArgs)
        txtCatName.Text = ""
        FillCATNames(h_CATID.Value)
    End Sub
    Protected Sub txtDesgName_TextChanged(sender As Object, e As EventArgs)
        txtDesgName.Text = ""
        FillDESGNames(h_DESGID.Value)
    End Sub
    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        FillEmpNames(h_EMPID.Value)
    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class