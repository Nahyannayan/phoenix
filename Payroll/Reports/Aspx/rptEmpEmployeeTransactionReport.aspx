<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEmpEmployeeTransactionReport.aspx.vb" Inherits="Payroll_Reports_Aspx_rptEmpEmployeeTransactionReport" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
      
    
 function parseDMY(s){
            return new Date(s.replace(/^(\d+)\W+(\w+)\W+/, '$2 $1 '));
        }

     function GetEMPNAME()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var dtFrom =  parseDMY(document.getElementById('<%=txtFDate.ClientID %>').value); 
            <%--result = window.showModalDialog("SelIDDESC.aspx?ID=EMP_ALLOC&year=" +  dtFrom.getFullYear() +"&month="+ dtFrom.getMonth(),"", sFeatures)            
              
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
        
         var url = "SelIDDESC.aspx?ID=EMP_ALLOC&year=" +  dtFrom.getFullYear() +"&month="+ dtFrom.getMonth();
         var oWnd = radopen(url, "pop_empname");


     }

     function OnClientClose1(oWnd, args) {
         //get the transferred arguments

         var arg = args.get_argument();

         if (arg) {
            
             NameandCode = arg.NameandCode;
             document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
             document.getElementById('<%=txtEMPNAME.ClientID %>').value = NameandCode;
             __doPostBack('<%=txtEMPNAME.ClientID%>', 'TextChanged');
            }
     }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        
        <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblrptCaption" runat="server" Text="Accommodation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

   <table align="center" width="100%" >
       <tr align ="left" >
           <td>
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
           </td>
       </tr>
    </table>

    <table align="center" runat = "server"  id="tblMain" width="100%">
        
        <tr>
            <td align="left" width="20%" >
                <span class="field-label">From</span></td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtFDate" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFrom" TargetControlID="txtFDate">
                            </ajaxToolkit:CalendarExtender>
                            
            </td>
                    <td align="left" width="20%">
                        <span class="field-label">To</span></td>
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtTDate" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="ImageTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="5" />
                        <ajaxToolkit:CalendarExtender ID="DocToDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageTo" TargetControlID="txtTDate">
                            </ajaxToolkit:CalendarExtender>
                    </td>
        </tr>
         <tr>
            <td align="left" >
                <span class="field-label">Business Unit</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlBUnit" runat="server" >
                </asp:DropDownList></td>
             <td></td>
             <td></td>
           
        </tr>
        <tr  >
            <td align="left">
                <span class="field-label">Employee Name</span></td>
            <td align="left" colspan="3">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                <asp:TextBox ID="txtEMPNAME" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddEMPID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNAME(); return false;" /><br />
                <asp:GridView ID="gvEMPName" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderText="EMP ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                             <HeaderStyle CssClass="gridheader_new" />
                    </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_EMPID" runat="server" />
    <input id="hfEmpName" runat = "server" type="hidden" />

                </div>
        </div>
    </div>

</asp:Content>