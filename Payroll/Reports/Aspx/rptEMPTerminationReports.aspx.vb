Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Payroll_Reports_Aspx_rptEMPTerminationReports
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        Try
            If Not IsPostBack Then
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Cache.SetExpires(Now.AddSeconds(-1))
                Response.Cache.SetNoStore()
                Response.AppendHeader("Pragma", "no-cache")
                If Page.IsPostBack = False Then
                    If isPageExpired() Then
                        Response.Redirect("expired.htm")
                    Else
                        Session("TimeStamp") = Now.ToString
                        ViewState("TimeStamp") = Now.ToString
                    End If
                    txtDate.Text = UtilityObj.GetDiplayDate()
                End If
                If Request.QueryString("MainMnu_code") = "" Then
                    Response.Redirect("..\..\noAccess.aspx")
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                UsrBSUnits1.MenuCode = MainMnu_code
                filldrp()
                Select Case MainMnu_code
                    Case "P159050"
                        lblrptCaption.Text = "Gratuity Report"
                        trEmployee.Visible = False
                        trCategory.Visible = False
                        trABCCategory.Visible = False
                        If ddCategory.Items.FindByValue(0) IsNot Nothing Then
                            ddCategory.Items.FindByValue(0).Selected = True
                        End If
                    Case "P159053"
                        lblrptCaption.Text = "Leave Salary Report"
                        trEmployee.Visible = False
                        trCategory.Visible = False
                        trABCCategory.Visible = False
                        If ddCategory.Items.FindByValue(0) IsNot Nothing Then
                            ddCategory.Items.FindByValue(0).Selected = True
                        End If
                    Case Else
                        If Not Request.UrlReferrer Is Nothing Then
                            Response.Redirect(Request.UrlReferrer.ToString())
                        Else
                            Response.Redirect("~\noAccess.aspx")
                        End If
                End Select

                Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
                If nodata Then
                    lblError.Text = "No Records with specified condition"
                Else
                    lblError.Text = ""
                End If
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
            FillEmpNames(h_EMPID.Value)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        h_EMPID.Value = ""
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)
        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub

    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT     ECT_ID, ECT_DESCR FROM EMPCATEGORY_M union select 0 as ECT_ID, 'ALL' as ECT_DESCR"
            fillDropdown(ddCategory, Query, "ECT_DESCR", "ECT_ID", "OASISConnectionString", False)
            'ddLocation.SelectedValue = Session("sBsuid").ToString()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "P159050"
                GenerateGratuityReport()
            Case "P159053"
                GenerateLeaveSalaryReport()
        End Select

    End Sub

    Sub GenerateGratuityReport()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "|B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "|C"
            End If
            If strABC = "" Then
                strABC = "Z"
            End If

            Dim cmd As New SqlCommand("SP_RPTGetGratuityDetail", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpBSU_ID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 1000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ECT_ID As New SqlParameter("@EMP_ECT_IDs", SqlDbType.Int)
            sqlpEMP_ECT_ID.Value = ddCategory.SelectedItem.Value
            cmd.Parameters.Add(sqlpEMP_ECT_ID)

            Dim sqlpSCHEDULE_DATE As New SqlParameter("@ASONDATE", SqlDbType.DateTime)
            sqlpSCHEDULE_DATE.Value = txtDate.Text
            cmd.Parameters.Add(sqlpSCHEDULE_DATE)

            Dim sqlpEMP_IDS As New SqlParameter("@EMP_IDS", SqlDbType.VarChar, 5000)
            sqlpEMP_IDS.Value = ""
            cmd.Parameters.Add(sqlpEMP_IDS)

            Dim sqlpEMP_ABCs As New SqlParameter("@EMP_ABCs", SqlDbType.VarChar, 10)
            sqlpEMP_ABCs.Value = strABC
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("AsOnDate") = Format(CDate(txtDate.Text), OASISConstants.DateFormat)

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetGratuityDetail.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GenerateLeaveSalaryReport()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "|B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "|C"
            End If
            If strABC = "" Then
                strABC = "Z"
            End If

            Dim cmd As New SqlCommand("SP_RPTGetLeaveSalaryDetail", New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpBSU_ID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 1000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ECT_ID As New SqlParameter("@EMP_ECT_IDs", SqlDbType.Int)
            sqlpEMP_ECT_ID.Value = ddCategory.SelectedItem.Value
            cmd.Parameters.Add(sqlpEMP_ECT_ID)

            Dim sqlpSCHEDULE_DATE As New SqlParameter("@ASONDATE", SqlDbType.DateTime)
            sqlpSCHEDULE_DATE.Value = txtDate.Text
            cmd.Parameters.Add(sqlpSCHEDULE_DATE)

            Dim sqlpEMP_IDS As New SqlParameter("@EMP_IDS", SqlDbType.VarChar, 5000)
            sqlpEMP_IDS.Value = ""
            cmd.Parameters.Add(sqlpEMP_IDS)

            Dim sqlpEMP_ABCs As New SqlParameter("@EMP_ABCs", SqlDbType.VarChar, 10)
            sqlpEMP_ABCs.Value = strABC
            cmd.Parameters.Add(sqlpEMP_ABCs)

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("AsOnDate") = Format(CDate(txtDate.Text), OASISConstants.DateFormat)

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptGetLeaveSalaryDetail.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub gvEMPName_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        gvEMPName.DataBind()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
