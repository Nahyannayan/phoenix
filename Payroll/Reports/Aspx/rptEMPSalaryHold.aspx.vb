Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Version        Author          Date            Change
'1.1            Swapna          May25,2011      Adding month and year selection instead of from and to date

Partial Class Reports_ASPX_Report_SalaryHold
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                If txtFromDate.Text = "" And txtToDate.Text = "" Then
                    txtFromDate.Text = Cache("fromDate")
                    txtToDate.Text = Cache("toDate")
                End If
            Else
                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

                Cache.Insert("fromDate", txtFromDate.Text)
                Cache.Insert("toDate", txtToDate.Text)
            End If

            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
                FillPayYearPayMonth()

            End If
            FillCATNames(h_CATID.Value)
            FillEmpNames(h_EMPID.Value)
            StoreEMPFilter()

            If Not IsPostBack Then

                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                chkEMPABC_A.Checked = True
                chkEMPABC_B.Checked = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub
    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty

        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If

        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvEMPName.DataSource = ds
            gvEMPName.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = ACTIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString


            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            Dim str_Sql As String = "SELECT     EHD.EHD_EMP_ID, EHD.EHD_PAYMONTH, EHD.EHD_PAYYEAR, " _
            & " EHD.EHD_ESD_ID, EHD.EHD_bHold, EHD.EHD_Remarks, EMP.EMPNO, " _
            & " ISNULL(EMP.EMP_FNAME, '') +' '+ ISNULL(EMP.EMP_MNAME, '') +' '+ " _
            & " ISNULL(EMP.EMP_LNAME, '') AS EMP_NAME, EMP.EMP_ECT_ID, ECT.ECT_DESCR, " _
            & " ESD.ESD_EARN_NET, ESD.ESD_DTTO,left(dbo.fn_getmonth(EHD.EHD_PAYMONTH),3)+'/'+ltrim(EHD.EHD_PAYYEAR) as monthyear" _
            & " , cast('1/'+left(dbo.fn_getmonth(EHD.EHD_PAYMONTH),3)+'/'+ltrim(EHD.EHD_PAYYEAR)as datetime) as monthyearorder, " _
            & " case ESD.ESD_MODE when 1 then 'Bank'   else 'Cash' end as ESD_CASHBANK, ESD.ESD_MODE " _
            & " FROM EMPSALHOLD_D AS EHD INNER JOIN" _
            & " EMPLOYEE_M AS EMP ON EHD.EHD_EMP_ID = EMP.EMP_ID INNER JOIN" _
            & " EMPCATEGORY_M AS ECT ON EMP.EMP_ECT_ID = ECT.ECT_ID INNER JOIN" _
            & " EMPSALARYDATA_D AS ESD ON EHD.EHD_ESD_ID = ESD.ESD_ID" _
            & " WHERE     (EHD.EHD_bHold = 1) AND (EMP.EMP_BSU_ID = '" & Session("sBsuid") & "')" _
            & " and EHD.ehd_paymonth=" & ddlPayMonth.SelectedValue & " and EHD.EHD_PAYYEAR=" & ddlPayYear.SelectedValue _
            & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) & _
            GetFilter("EHD_EMP_ID", h_EMPID.Value, True) '& _
            '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue
            '& " AND ESD_DTTO BETWEEN '" & txtFromDate.Text & "' AND '" & txtToDate.Text & "'" _   'V1.1

            Dim cmd As New SqlCommand(str_Sql, New SqlConnection(str_conn))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "RPT_GetSalaryHoldDetail"
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@ESD_MONTH", ddlPayMonth.SelectedValue, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@ESD_YEAR", ddlPayYear.SelectedValue, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@ECT_IDs", h_CATID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@EMP_ABCs", strABC, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_IDs", h_EMPID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            Dim objConn As New SqlConnection(str_conn)
            cmd.Connection = objConn


            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = Format(CDate(txtFromDate.Text), OASISConstants.DateFormat)
            params("toDate") = Format(CDate(txtToDate.Text), OASISConstants.DateFormat)
            params("BSU_NAME") = Session("bsu_name")

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeSalaryHold.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function



    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function








    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillACTIDs(h_EMPID.Value)
        End If

    End Sub





    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub


    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        'swapna changed
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                'ddlPayMonth.SelectedValue = CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub



    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub



    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub


End Class