Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If Page.IsPostBack = False Then
                Select Case MainMnu_code
                    Case "P150006"
                        lblrptCaption.Text = "Employees Attendance Report"
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                    Case "P150070"
                        lblrptCaption.Text = "Salary Summary by Cash "
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                End Select
            End If


            If h_BSUID.Value Is Nothing Or (h_BSUID.Value = "" Or h_BSUID.Value = "undefined") Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            FillDeptNames(h_DEPTID.Value)
            FillCATNames(h_CATID.Value)
            FillDESGNames(h_DESGID.Value)
            FillEmpNames(h_EMPID.Value)
            If Not IsPostBack Then
                FillPayYearPayMonth()
                Select Case MainMnu_code
                    Case "P150006"
                        ddlPayMonth.SelectedValue = Date.Now.Month
                        ddlPayYear.SelectedValue = Date.Now.Year
                    Case "P150020"
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                End Select
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub h_BSUID_ValueChanged(sender As Object, e As EventArgs) Handles h_BSUID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillBSUNames(h_BSUID.Value)
        'h_BSUID.Value = ""
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDeptNames(h_DEPTID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub

    Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDESGNames(h_DESGID.Value)
        'h_DESGID.Value = ""
    End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        'swapna changed
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        Dim payYear As Integer = Session("BSU_PAYYEAR")
        Dim payMonth As Integer = 0
        AccessPayrollClass.SetPayMonth_YearDetails(Session("sBSUID"), payYear, payMonth)

        ddlPayMonth.SelectedValue = payMonth
        ddlPayYear.SelectedValue = payYear

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Select Case MainMnu_code
                Case "P150006"
                    GetEmployeeAttendance()
            End Select

            '"P150006"
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            'Dim str_Sql As String = "select ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            'GetFilter("ESD_EMP_ID", h_EMPID.Value, False) & _
            '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If


            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim strFiltDEPTID As String = GetFilter("ESD_DPT_ID", h_DEPTID.Value, True)
            Dim strFiltCATID As String = GetFilter("ESD_ECT_ID", h_CATID.Value, True)
            Dim strFiltDESGID As String = GetFilter("ESD_DES_ID", h_DESGID.Value, True)
            Dim strFiltEMPID As String = GetFilter("ESD_EMP_ID", h_EMPID.Value, True)
            Dim strCategory As String = GetFilter("EMP_ABC", strABC, True)


            Dim str_Sql As String = "select DISTINCT EMP_NO, ESD_BSU_ID, ESD_MONTH, ESD_YEAR, ESD_EARN_NET, " & _
            " ESD_ACCOUNT, ESD_PAID, EMP_NAME, BSU_NAME, ESD_BANK, bHOLD from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            strFiltBSUID & strFiltDEPTID & strFiltCATID & strFiltDESGID & strFiltEMPID & strCategory & GetMonthYear()
            '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Select Case MainMnu_code
                Case "P150070"
                    str_Sql += " AND ESD_MODE = 0 "

                Case Else
                    str_Sql += " AND ESD_MODE = 1 "
            End Select


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("UserName") = Session("sUsr_name")

                repSource.Parameter = params
                repSource.Command = cmd
                Select Case MainMnu_code
                    Case "P150070"
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalSummaryCash.rpt"
                    Case Else
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalCoverLetter.rpt"
                End Select
                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                'ReportLoadSelection()
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GetEmployeeAttendance()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim cmd As New SqlCommand("[GETEMPMONTH_ATT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFORMONTH As New SqlParameter("@FORMONTH", SqlDbType.VarChar, 3)
        sqlpFORMONTH.Value = "OCT"
        cmd.Parameters.Add(sqlpFORMONTH)

        Dim sqlpSTARATT_DATE As New SqlParameter("@STARATT_DATE", SqlDbType.DateTime)
        sqlpSTARATT_DATE.Value = New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, 1)
        cmd.Parameters.Add(sqlpSTARATT_DATE)

        Dim sqlpENDDATE As New SqlParameter("@ENDDATE", SqlDbType.DateTime)
        sqlpENDDATE.Value = New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, Date.DaysInMonth(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue))
        cmd.Parameters.Add(sqlpENDDATE)

        Dim sqlpWEEKEND1 As New SqlParameter("@WEEKEND1", SqlDbType.VarChar, 12)
        sqlpWEEKEND1.Value = "FRIDAY"
        cmd.Parameters.Add(sqlpWEEKEND1)

        Dim sqlpWEEKEND2 As New SqlParameter("@WEEKEND2", SqlDbType.VarChar, 12)
        sqlpWEEKEND2.Value = "SATURDAY"
        cmd.Parameters.Add(sqlpWEEKEND2)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RPT_CAPTION") = "EMPLOYEE ATTENDANCE REPORT FOR " & ddlPayMonth.SelectedItem.Text & " - " & ddlPayYear.SelectedItem.Text
        'params("FromDT") = txtFromDate.Text
        'params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeAttendance_Month.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        'If FillBSUNames(txtBSUName.Text) Then
        '    h_BSUID.Value = txtBSUName.Text
        'End If
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub gvMonthYear_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvMonthYear.PageIndex = e.NewPageIndex
        FillMonth_Year()
    End Sub
    Private Sub FillMonth_Year()
        If Session("EMP_MONTH_YEAR") Is Nothing Then
            Session("EMP_MONTH_YEAR") = CreateMonthYearTable()
        End If
        Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR")
        Dim i As Integer
        Dim repeated As Boolean = False
        For i = 0 To dtMonthYear.Rows.Count - 1
            If dtMonthYear.Rows(i)(1) = ddlPayMonth.SelectedItem.Text And dtMonthYear.Rows(i)(2) = ddlPayYear.SelectedItem.Text Then
                repeated = True
                Exit For
            End If
        Next
        If Not repeated Then
            Dim dr As DataRow = dtMonthYear.NewRow()
            dr("ID") = ddlPayMonth.SelectedItem.Text & ddlPayYear.SelectedValue & dtMonthYear.Rows.Count
            dr("MONTH") = ddlPayMonth.SelectedItem.Text
            dr("YEAR") = ddlPayYear.SelectedValue
            dtMonthYear.Rows.Add(dr)
        End If
        If dtMonthYear Is Nothing Or dtMonthYear.Rows.Count <= 0 Then
            Exit Sub
        End If
        gvMonthYear.DataSource = dtMonthYear
        gvMonthYear.DataBind()

    End Sub
    Private Function CreateMonthYearTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim cMONTH As New DataColumn("MONTH", System.Type.GetType("System.String"))
            Dim cYEAR As New DataColumn("YEAR", System.Type.GetType("System.Int32"))

            dtDt.Columns.Add(cID)
            dtDt.Columns.Add(cMONTH)
            dtDt.Columns.Add(cYEAR)
            Return dtDt

        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub
    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub
    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub
    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub gvEMPName_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub lnkMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        trPayMonths.Visible = True
        FillMonth_Year()
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub
    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub lnkbtnMonthDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblMONTHYEARID As New Label
        lblMONTHYEARID = TryCast(sender.FindControl("lblMonthYearID"), Label)
        If Not lblMONTHYEARID Is Nothing Then
            If Not Session("EMP_MONTH_YEAR") Is Nothing Then
                Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR")
                Dim i As Integer
                For i = 0 To dtMonthYear.Rows.Count - 1
                    If dtMonthYear.Rows(i)(0) = lblMONTHYEARID.Text Then
                        dtMonthYear.Rows.RemoveAt(i)
                        Exit For
                    End If
                Next
                Session("EMP_MONTH_YEAR") = dtMonthYear
                gvMonthYear.DataSource = dtMonthYear
                gvMonthYear.DataBind()
            End If
        End If
    End Sub

    Protected Sub lnkbtngrdDelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDelete_Click2(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDelete_Click3(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub
    Private Function GetMonthYear() As String
        Try
            Dim retVal As String = " AND ("
            Dim AddOR As String = String.Empty
            If Not Session("EMP_MONTH_YEAR") Is Nothing AndAlso Session("EMP_MONTH_YEAR").Rows.Count > 0 Then
                Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR")
                Dim i As Integer
                For i = 0 To dtMonthYear.Rows.Count - 1
                    retVal += AddOR & " (ESD_MONTH = " & GetMonthNumber(dtMonthYear.Rows(i)(1))
                    retVal += " AND ESD_YEAR = " & dtMonthYear.Rows(i)(2) & ")"
                    AddOR = " OR "
                Next
            Else
                retVal += "ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue
            End If
            retVal += " ) "
            Return retVal
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Private Function GetMonthNumber(ByVal month As String) As Integer
        Dim retVal As Integer = -1
        Select Case month
            Case "January"
                retVal = 1
            Case "February"
                retVal = 2
            Case "March"
                retVal = 3
            Case "April"
                retVal = 4
            Case "May"
                retVal = 5
            Case "June"
                retVal = 6
            Case "July"
                retVal = 7
            Case "August"
                retVal = 8
            Case "September"
                retVal = 9
            Case "October"
                retVal = 10
            Case "November"
                retVal = 11
            Case "December"
                retVal = 12
        End Select
        Return retVal
    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class