<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEMPAirfare.aspx.vb" Inherits="Reports_ASPX_Report_AirfareDetails" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'none') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelBSU.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = 'none';
             }
             if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'none') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
             }
             if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
             }
             if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'none') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
             }
             return false;
         }

         function ToggleSearchBSUnit() {
             if (document.getElementById('<%=hfBSU.ClientID %>').value == 'display') {
                 document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelBSU.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = 'none';
                 document.getElementById('<%=hfBSU.ClientID %>').value = 'none';
             }
             else {
                 document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
                 document.getElementById('<%=trSelBSU.ClientID %>').style.display = '';
                 document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = '';
                 document.getElementById('<%=hfBSU.ClientID %>').value = 'display';
             }
             return false;
         }

         function ToggleSearchDepartment() {
             if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'display') {
                 document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
                 document.getElementById('<%=hfDepartment.ClientID %>').value = 'none';
             }
             else {
                 document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
                 document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
                 document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = '';
                 document.getElementById('<%=hfDepartment.ClientID %>').value = 'display';
             }
             return false;
         }

         function ToggleSearchCategory() {
             if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
             }
             else {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
             }
             return false;
         }

         function ToggleSearchDesignation() {
             if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'display') {
                 document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
                 document.getElementById('<%=hfDesignation.ClientID %>').value = 'none';
             }
             else {
                 document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
                 document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
                 document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = '';
                 document.getElementById('<%=hfDesignation.ClientID %>').value = 'display';
             }
             return false;
         }



         function HideAll() {
             document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelBSU.ClientID %>').style.display = '';
             document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
             document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
             document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
             document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
         }

         function SearchHide() {
             document.getElementById('trBSUnit').style.display = 'none';
         }

         function GetBSUName() {
             var sFeatures;
             sFeatures = "dialogWidth: 729px; ";
             sFeatures += "dialogHeight: 445px; ";
             sFeatures += "help: no; ";
             sFeatures += "resizable: no; ";
             sFeatures += "scroll: yes; ";
             sFeatures += "status: no; ";
             sFeatures += "unadorned: no; ";
             var NameandCode;
             var result;
             //result = window.showModalDialog("SelIDDESC.aspx?ID=BSU&multiSelect=false", "", sFeatures)
             var oWnd = radopen("SelIDDESC.aspx?ID=BSU&multiSelect=false", "pop_bsu");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
         }
        function GetDeptName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=DEPT", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=DEPT", "pop_dept");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_DEPTID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
        }
        function GetCATName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=CAT", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=CAT", "pop_cat");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_CATID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
        }
        function GetDESGName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=DESG", "", sFeatures)
            var oWnd = radopen("SelIDDESC.aspx?ID=DESG", "pop_desg");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_DESGID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
        }
        function parseDMY(s) {
            return new Date(s.replace(/^(\d+)\W+(\w+)\W+/, '$2 $1 '));
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var dtFrom = parseDMY(document.getElementById('<%=txtFromDate.ClientID %>').value);
            //result = window.showModalDialog("SelIDDESC.aspx?ID=EMP_ALLOC&year=" + dtFrom.getFullYear() + "&month=" + dtFrom.getMonth(), "", sFeatures)
            var url = "SelIDDESC.aspx?ID=EMP_ALLOC&year=" + dtFrom.getFullYear() + "&month=" + dtFrom.getMonth()
            var oWnd = radopen(url, "pop_empnme");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_BSUID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_BSUID.ClientID%>', "");
            }
        }
        function OnClientDeptClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_DEPTID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_DEPTID.ClientID%>', "");
            }
        }
        function OnClientCatClose(oWnd, args) {

            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_CATID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_CATID.ClientID%>', "");
            }
        }

        function OnClientDesgClose(oWnd, args) {
            //alert(1);
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_DESGID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_DESGID.ClientID%>', "");
            }
        }

        function OnClientEmpName(oWnd, args) {
            //alert(1);
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_EMPID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_EMPID.ClientID%>', "");
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_dept" runat="server" Behaviors="Close,Move" OnClientClose="OnClientDeptClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cat" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCatClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_desg" runat="server" Behaviors="Close,Move" OnClientClose="OnClientDesgClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_empnme" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEmpName"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Employee Airfare Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="left" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>

                <table align="center" runat="server" id="tblMain" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_img">
                        <td align="left" colspan="2" style="height: 1px" valign="middle">
                            

                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">As on Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trSelBSU">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusBSUnit" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchBSUnit();return false;" CausesValidation="False" />
                            <span class="field-label">Select Business Unit Filter</span></td>
                    </tr>
                    <tr id="trBSUnit" runat="server" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusBSUnit" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchBSUnit();return false;" CausesValidation="False" /><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">(Enter the BSU ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtBSUName" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddBSUID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgGetBSUName" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetBSUName(); return false;" /><br />
                            <asp:GridView ID="grdBSU" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="BSU ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSUID" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="BSU_Name" HeaderText="BSU Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelDepartment">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusDepartment" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                            <span class="field-label">Select Department Filter</span></td>
                    </tr>
                    <tr id="trDepartment" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusDepartment" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" /><span class="field-label">Department</span></td>
                        <td align="left" colspan="3">(Enter the Department ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDeptName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDEPTID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetDeptName(); return false;" /><br />
                            <asp:GridView ID="gvDept" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="DEPT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDEPTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DEPT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDeptDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelcategory">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                            <span class="field-label">Select Category Filter</span></td>
                    </tr>
                    <tr id="trCategory" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Category</span></td>
                        <td align="left" colspan="3">(Enter the Category ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtCatName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetCATName(); return false;" /><br />
                            <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="CAT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelDesignation">
                        <td align="left" class="matters" colspan="2">
                            <asp:ImageButton ID="imgPlusDesignation" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" />
                            <span class="field-label">Select Designation Filter</span></td>
                    </tr>
                    <tr id="trDesignation" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusDesignation" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" /><span class="field-label">Designation</span></td>
                        <td align="left" colspan="3">(Enter the Designation ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDesgName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDESGID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetDESGName(); return false;" /><br />
                            <asp:GridView ID="gvDesg" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="DESG ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDESGID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DESG NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDESGDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelEMPName">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                            <span class="field-label">Select Employee Name Filter</span></td>
                    </tr>
                    <tr id="trEMPName" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" /><span class="field-label">Employee Name</span></td>
                        <td align="left" colspan="3">
                            <asp:Label ID="Label1" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtEMPNAME" runat="server"
                                Width="448px" CssClass="inputbox" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="EMPABCCat" runat="server">
                        <td align="left"><span class="field-label">Employee Category</span></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" />
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">&nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_DOCTYPE" runat="server" />
                <asp:HiddenField ID="h_DEPTID" runat="server" />
                <asp:HiddenField ID="h_CATID" runat="server" />
                <asp:HiddenField ID="h_DESGID" runat="server" />
                <asp:HiddenField ID="hfDocType" runat="server" />
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfDepartment" runat="server" type="hidden" />
                <input id="hfCategory" runat="server" type="hidden" />
                <input id="hfDesignation" runat="server" type="hidden" />
                <input id="hfEmpName" runat="server" type="hidden" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>


            </div>
        </div>
    </div>
</asp:Content>

