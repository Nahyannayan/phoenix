<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEMPGetGratuvitySchedule.aspx.vb" Inherits="Payroll_Reports_Aspx_empGetGratuvitySchedule" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function parseDMY(s) {
            return new Date(s.replace(/^(\d+)\W+(\w+)\W+/, '$2 $1 '));
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var dtFrom = parseDMY(document.getElementById('<%=txtDate.ClientID %>').value);
            //result = window.showModalDialog("SelIDDESC.aspx?ID=EMP_ALLOC&year=" + dtFrom.getFullYear() + "&month=" + dtFrom.getMonth(), "", sFeatures)
            var url = "SelIDDESC.aspx?ID=EMP_ALLOC&year=" + dtFrom.getFullYear() + "&month=" + dtFrom.getMonth()
            var oWnd = radopen(url, "pop_empnme");
           <%-- if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientEmpName(oWnd, args) {
            //alert(1);
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_EMPID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_EMPID.ClientID%>', "");
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_empnme" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEmpName"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>

                <table align="center" runat="server" id="tblMain" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr class="subheader_img">
                        <td align="left" colspan="2" style="height: 1px" valign="middle"></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>

                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBUnit" runat="server">
                            </asp:DropDownList></td>
                       <%-- <td align="left"></td>
                        <td align="left"></td>--%>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Category</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddCategory" runat="server" DataSourceID="dbcategory" DataTextField="ECT_DESCR" DataValueField="ECT_ID">
                            </asp:DropDownList><asp:SqlDataSource ID="dbcategory" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT     ECT_ID, ECT_DESCR&#13;&#10;FROM EMPCATEGORY_M&#13;&#10;union select &#13;&#10;0 as ect_id, 'ALL' as ect_descr FROM  EMPCATEGORY_M"></asp:SqlDataSource>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Employee Name</span></td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtEMPNAME" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="EMPABCCAT" style="display: none">
                        <td align="left"><span class="field-label">Employee Category</span></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" Checked="True" />
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" Checked="True" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" Checked="True" /></td>


                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <input id="hfEmpName" runat="server" type="hidden" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>
