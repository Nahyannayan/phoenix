<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptEMPContactDetails.aspx.vb" Inherits="Reports_ASPX_Report_EmployeeContactDetails"
    Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../../../Asset/UserControls/usrBSUnitsAsset.ascx" TagName="usrBSUnits"
    TagPrefix="uc2" %>
<%@ Register Src="../../../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'none') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=trSelBSU.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'none') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'none') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
            }
            return false;
        }

        function ToggleSearchBSUnit() {
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'display') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=trSelBSU.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfBSU.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelBSU.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusBSUnit.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusBSUnit.ClientID %>').style.display = '';
                document.getElementById('<%=hfBSU.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchDepartment() {
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'display') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchCategory() {
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchDesignation() {
            if (document.getElementById('<%=hfDesignation.ClientID %>').value == 'display') {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDesignation.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDesignation.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDesignation.ClientID %>').style.display = '';
                document.getElementById('<%=hfDesignation.ClientID %>').value = 'display';
            }
            return false;
        }



        function HideAll() {
            document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelBSU.ClientID %>').style.display = '';
            document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
            document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
            document.getElementById('<%=trDesignation.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDesignation.ClientID %>').style.display = '';
            document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
        }

        function SearchHide() {
            document.getElementById('trBSUnit').style.display = 'none';
        }

        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=BSU", "", sFeatures)
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>

            var url = "SelIDDESC.aspx?ID=BSU";
            var oWnd = radopen(url, "pop_bsuname");
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_BSUID.ClientID %>').value = NameandCode;
                document.getElementById('<%=txtBSUName.ClientID%>').value = NameandCode;
                __doPostBack('<%= txtBSUName.ClientID%>', 'TextChanged');
            }
        }
        function GetDeptName() {

            var NameandCode;
            var result;
            <%--result = window.showModalDialog("SelIDDESC.aspx?ID=DEPT", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_DEPTID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
            var url = "SelIDDESC.aspx?ID=DEPT";
            var oWnd = radopen(url, "pop_deptname");
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_DEPTID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtDeptName.ClientID%>').value = NameandCode;
                __doPostBack('<%= txtDeptName.ClientID%>', 'TextChanged');
            }
        }

        function GetCATName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%--result = window.showModalDialog("SelIDDESC.aspx?ID=CAT", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_CATID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
            var url = "SelIDDESC.aspx?ID=CAT";
            var oWnd = radopen(url, "pop_catname");
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_CATID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtCatName.ClientID%>').value = NameandCode;
                __doPostBack('<%= txtCatName.ClientID%>', 'TextChanged');
            }
        }

        function GetDESGName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%--result = window.showModalDialog("SelIDDESC.aspx?ID=DESG", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_DESGID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
            var url = "SelIDDESC.aspx?ID=DESG";
            var oWnd = radopen(url, "pop_desgname");
        }

        function OnClientClose4(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_DESGID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtDesgName.ClientID%>').value = NameandCode;
                __doPostBack('<%= txtDesgName.ClientID%>', 'TextChanged');
            }
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("../../SelIDDESC.aspx?ID=EMP&Bsu_id="+document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)
               <%--result = window.showModalDialog("../Aspx/SelIDDESC.aspx?ID=EMP","", sFeatures)          
           
             if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
            var url = "SelIDDESC.aspx?ID=EMP";
            var oWnd = radopen(url, "pop_empname");
        }

        function OnClientClose5(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }

        function GetEMPNAME2() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            result = window.showModalDialog("../../SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)

            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bsuname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_deptname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cattname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_desgname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblrptCaption" runat="server" Text="Employee Contact Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr align="left">
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
            </td>
        </tr>
        <tr align="left">
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <table align="center" runat="server" id="tblMain" width="100%">
         
        <tr id="trMonth_Year" runat="server" visible="True">
            <td align="left" width="20%">
                <span class="field-label">Joining
                Date
                (From)</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlPayMonth" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlPayMonth_SelectedIndexChanged" Visible="False">
                </asp:DropDownList>
                <asp:TextBox ID="txtFromDate" runat="server" AutoPostBack="True"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" 
                    CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOn" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="txtFromDate_CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgAsOn0" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ImageButton ID="imgAsOn0" runat="server" ImageUrl="~/Images/calendar.gif" 
                    TabIndex="4" />
            </td>
            <td align="left" style="color: #1b80b6;" width="20%">
                <span class="field-label">Joining
                Date (To)</span>
            </td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlPayYear" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlPayYear_SelectedIndexChanged" Visible="False">
                </asp:DropDownList>
                <asp:TextBox ID="txtToDate" runat="server" AutoPostBack="True"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" 
                    CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOn" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgAsOn1" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ImageButton ID="imgAsOn1" runat="server" ImageUrl="~/Images/calendar.gif" 
                    TabIndex="4" />
            </td>
        </tr>
        <tr id="trProcessingDate" runat="server" visible="false">
            <td align="left">
                <span class="field-label">Include Joining Date</span>
            </td>
            <td align="left" colspan="3">
                <asp:DropDownList ID="ddlProcessingDate" runat="server" Visible="False">
                </asp:DropDownList>
                <asp:CheckBox ID="chkIncludeJoiningDate" runat="server" AutoPostBack="True" 
                    OnCheckedChanged="chkExcludeProcessDate_CheckedChanged"></asp:CheckBox>
            </td>
        </tr>
        <tr id="trAsOnDate" runat="server" visible="false">
            <td align="left">
                <span class="field-label">As On Date</span>
            </td>
            <td align="left" colspan="3" style="text-align: left">
                <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True">
                </asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOn" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgAsOn" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ImageButton ID="imgAsOn" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvAsOnDate" ControlToValidate="txtAsOnDate"
                    ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator></td>
        </tr>
         <tr id="trBSUTree" runat="server">
            <td align="left">
                <span class="field-label">Business Unit</span>
            </td>
            <td align="left" colspan="3">
                <div class="checkbox-list">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                </div>
            </td>
        </tr>
        <tr id="treeCat" visible="true" runat="server">
            <td align="left" valign="top">
                <span class="field-label">Category</span>
            </td>
            <td align="left" colspan="3">
                <div class="checkbox-list">
                <uc3:usrTreeView ID="UsrTreeView1" runat="server" />
                </div>
            </td>
        </tr>
        <tr id="trSelBSU" runat="server" visible="false">
            <td colspan="4" style="text-align: left" valign="top">
                <asp:ImageButton ID="imgPlusBSUnit" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                    OnClientClick="return ToggleSearchBSUnit();return false;" CausesValidation="False" />
                <span class="field-label">Select Business Unit&nbsp;Filter</span>
            </td>
        </tr>
        <tr id="trBSUnit" runat="server" style="display: none;">
            <td align="left" valign="top">
                <asp:ImageButton ID="imgMinusBSUnit" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                    OnClientClick="return ToggleSearchBSUnit();return false;" CausesValidation="False" />
                <span class="field-label">Business Unit</span>
            </td>
            <td align="left" valign="top" colspan="3" >
                (Enter the BSU ID you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtBSUName" runat="server" CssClass="inputbox" Width="446px"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddBSUID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgGetBSUName" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="return GetBSUName()" /><br />
                <asp:GridView ID="grdBSU" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="BSU ID">
                            <ItemTemplate>
                                <asp:Label ID="lblBSUID" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BSU_Name" HeaderText="BSU Name" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trSelDepartment">
            <td align="left" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusDepartment" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                    OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                <span class="field-label">Select Department Filter</span>
            </td>
        </tr>
        <tr id="trDepartment" style="display: none;">
            <td align="left" valign="top">
                <asp:ImageButton ID="imgMinusDepartment" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                    OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                <span class="field-label">Department</span>
            </td>
            <td align="left" colspan="3">
                (Enter the Department ID you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtDeptName" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddDEPTID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetDeptName(); return false;" /><br />
                <asp:GridView ID="gvDept" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="DEPT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblDEPTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="DEPT NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDeptDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trSelcategory">
            <td align="left" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                    OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                <span class="field-label">Select Category Filter</span>
            </td>
        </tr>
        <tr id="trCategory" style="display: none;">
            <td align="left" valign="top">
                <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                    OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                <span class="field-label">Category</span>
            </td>
            <td align="left" colspan="3" style="text-align: left">
                (Enter the Category ID you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtCatName" runat="server" CssClass="inputbox" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetCATName(); return false;" /><br />
                <asp:GridView ID="gvCat" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="CAT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trSelDesignation">
            <td align="left" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusDesignation" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                    OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" />
                <span class="field-label">Select Designation Filter</span>
            </td>
        </tr>
        <tr id="trDesignation" style="display: none;">
            <td align="left" valign="top">
                <asp:ImageButton ID="imgMinusDesignation" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                    OnClientClick="return ToggleSearchDesignation();return false;" CausesValidation="False" />
                <span class="field-label">Designation</span>
            </td>
            <td align="left" colspan="3" style="text-align: left">
                (Enter the Designation ID you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtDesgName" runat="server" CssClass="inputbox" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddDESGID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetDESGName(); return false;" /><br />
                <asp:GridView ID="gvDesg" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="DESG ID">
                            <ItemTemplate>
                                <asp:Label ID="lblDESGID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="DESG NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDESGDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trSelEMPName">
            <td align="left" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../../../Images/PLUS.jpg"
                    OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                <span class="field-label">Select Employee Name Filter</span>
            </td>
        </tr>
        <tr id="trEMPName" style="display: none;">
            <td align="left" valign="top">
                <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg"
                    OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                <span class="field-label">Employee Name</span>
            </td>
            <td align="left" colspan="3" valign="top">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                <asp:TextBox ID="txtEMPNAME" runat="server" Width="448px" CssClass="inputbox" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetEMPNAME();return false;" /><br />
                <asp:GridView ID="gvEMPName" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="EMP ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trEMPABCCAT" runat="server">
            <td align="left" valign="top">
               <span class="field-label"> ABC Category</span>
            </td>
            <td align="left" colspan="3" valign="top">
                <asp:CheckBox ID="chkEMPABC_A" runat="server" CssClass="field-label" Text="A" />
                <asp:CheckBox ID="chkEMPABC_B" runat="server" CssClass="field-label" Text="B" />
                <asp:CheckBox ID="chkEMPABC_C" runat="server" CssClass="field-label" Text="C" />
            </td>
        </tr>
          <tr id="trFinalSettlement" runat="server" visible="false">
        <td align="left" valign="top" colspan="4">
        <asp:CheckBox ID="chkFF" runat="server" CssClass="field-label" Text="Final Settlement" />
        </td>
        </tr>
        <tr id="trCombined" runat="server" visible="False">
        <td align="left" valign="top" colspan="4">
        <asp:CheckBox ID="chkCombined" runat="server" CssClass="field-label" Text="Combined (Normal + Final Settlement)" />
        </td>
        </tr>
        <tr id="trWPS" runat="server" visible="false">
            <td align="left" colspan="4">
                <asp:RadioButton ID="radWithWPS" runat="server" CssClass="field-label" GroupName="WPS" Text="With WPS"></asp:RadioButton>
                <asp:RadioButton ID="radWithoutWPS" runat="server" CssClass="field-label" GroupName="WPS" Text="Without WPS">
                </asp:RadioButton>
                <asp:RadioButton ID="radAll" runat="server" CssClass="field-label" Checked="True" GroupName="WPS" Text="All">
                </asp:RadioButton>
            </td>
        </tr>
        <tr id="trSummary" runat="server" visible="false">
            <td align="left" colspan="4" style="text-align: left">
                <asp:CheckBox ID="chkSummary" runat="server" CssClass="field-label" Text="Summary" />
            </td>
        </tr>
        <tr id="tryear" runat="server" visible="false">
            <td align="left">
                <span class="field-label">Calendar Year</span>
            </td>
            <td align="left" style="text-align: left" colspan="3">
                <asp:DropDownList ID="ddlYear" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trPrintA3" runat="server" visible="false">
            <td align="left" colspan="4">
                <asp:CheckBox ID="chkPrintA3" runat="server" Text="Print On A3 Paper" />
            </td>
        </tr>
        <tr id="trShowAllMonths" runat="server" visible="false"> 
            <td align="left" colspan="4">
                <asp:CheckBox ID="chkShowAllMonths" runat="server" Text="Show All Months" />
            </td>
        </tr>
        <tr runat="server" visible="false" id="trException">
            <td align="left" colspan="4" style="text-align: left">
                <asp:CheckBox ID="chkException" runat="server" Text="Exception Report" /></td>
        </tr>
        <tr>
            <td align="center" colspan="4" nowrap="nowrap">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="h_EMPID" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_DEPTID" runat="server" />
    <asp:HiddenField ID="h_CATID" runat="server" />
    <asp:HiddenField ID="h_DESGID" runat="server" />
    <input id="hfBSU" runat="server" type="hidden" />
    <input id="hfDepartment" runat="server" type="hidden" />
    <input id="hfCategory" runat="server" type="hidden" />
    <input id="hfDesignation" runat="server" type="hidden" />
    <input id="hfEmpName" runat="server" type="hidden" />
</asp:Content>
