Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEOutstandingDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim MainMnu_code As String
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            usrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            trPeriod.Visible = True
            Select Case MainMnu_code
                Case OASISConstants.mnuREP_DOC_MOVEMENT
                    lblReportCaption.Text = "Document Movement Report"
                Case "P150056"
                    lblReportCaption.Text = "Leave Salary Summary"
                    tr_show_radiobuttons.Visible = False
            End Select

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
                txtToDate.Text = Session("RPTToDate")
            Else
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        If usrBSUnits1.GetSelectedNode() = String.Empty Then
            lblError.Text = "***Please select atleast one business Unit***"
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.mnuREP_DOC_MOVEMENT
                GETDOCUMENTMOVEMNTDETAILS()
            Case "P150056"
                Emp_LEaveSalaryReport()
        End Select
    End Sub

    Private Sub GETDOCUMENTMOVEMNTDETAILS()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim cmd As New SqlCommand("GETDOCUMENTMOVEMNTDETAILS")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 300)
        sqlpBSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(usrBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpBSU_IDs)

        Dim sqlpFromDt As New SqlParameter("@FromDt", SqlDbType.DateTime)
        sqlpFromDt.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFromDt)

        Dim sqlpTODt As New SqlParameter("@TODt", SqlDbType.DateTime)
        sqlpTODt.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODt)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.SmallInt)
        If radAll.Checked Then
            sqlpTyp.Value = 0
        ElseIf radIssued.Checked Then
            sqlpTyp.Value = 1
        ElseIf radReturned.Checked Then
            sqlpTyp.Value = 2
        End If
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("ReportCaption") = "Document Movement Report"
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../PayRoll/REPORTS/RPT/rptEMPDocumentMovement.rpt"
        'params("bSummary") = True

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection_2()
    End Sub

    Private Sub Emp_LEaveSalaryReport()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim strBsuid As String = UtilityObj.GetBSUnitWithSeperator(usrBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
            'EXEC [Emp_LEaveSalaryReport_Summary] '19/Jan/2011','19/Dec/2012','131001'  
            Dim str_Sql As String = "EXEC Emp_LEaveSalaryReport_Summary '" & txtFromDate.Text & "','" & txtToDate.Text & "','" & strBsuid & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")

                ''params("decimal") = Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd

                params("reportCaption") = "Leave Salary Summary For the period from " + txtFromDate.Text + " to " + txtToDate.Text
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/Emp_LEaveSalaryReport_Summary.rpt"
                'End If
                repSource.IncludeBSUImage = True
                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection_2()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
