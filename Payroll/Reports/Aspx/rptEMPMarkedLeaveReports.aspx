<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptEMPMarkedLeaveReports.aspx.vb" Inherits="Reports_ASPX_Report_rptEMPMarkedLeaveReports"
    Title="Untitled Page" %>

<%@ Register Src="../../../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc4" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../../../Asset/UserControls/usrBSUnitsAsset.ascx" TagName="usrBSUnits"
    TagPrefix="uc2" %>
<%@ Register Src="../../../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetEMPNameREP() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            <%--var result;
            result = window.showModalDialog("../../../Accounts/accShowEmpDetail.aspx?id=ER", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                //
                return true;
            }
            return false;--%>
            var url = "../../../Accounts/accShowEmpDetail.aspx?id=ER";
            var oWnd = radopen(url, "pop_empnameREP");

        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfReport.ClientID%>').value = NameandCode[1];
                <%--document.getElementById('<%=txtEmpNo.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');--%>
            }
        }


        function GetEMPNameATTREP() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/accShowEmpDetail.aspx?id=EAR", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                //
                return true;
            }
            return false;
        }

        function parseDMY(s) {
            return new Date(s.replace(/^(\d+)\W+(\w+)\W+/, '$2 $1 '));
        }
        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            <%--result = window.showModalDialog("SelIDDESC.aspx?ID=EMP", "", sFeatures)


            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>

            var url = "SelIDDESC.aspx?ID=EMP";
            var oWnd = radopen(url, "pop_empname");

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }

    
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="empnameREP" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblrptCaption" runat="server" Text="Employee Daily Attendance Report">
                </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr align="left">
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
            </td>
        </tr>
        <tr align="left">
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <table align="center" runat="server" id="tblMain" width="100%">
        
        <tr id="trDateRange" runat="server">
            <td align="left" width="20%">
               <span class="field-label"> From Date</span>
            </td>
            <td align="left" width="30%">
                <uc4:usrDatePicker ID="UsrDateFrom" runat="server" />
            </td>
            <td align="left" width="20%">
                <span class="field-label">To Date</span>
            </td>
            <td align="left" width="30%">
                <uc4:usrDatePicker ID="UsrDateTo" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Category</span>
            </td>
            <td align="left" colspan="3" >
                <div class="checkbox-list">
                <uc3:usrTreeView ID="UsrTreeView1" runat="server" />
                </div>
            </td>
        </tr>
        <tr id="trEMPName">
            <td align="left" valign="top">
               <span class="field-label">Employee Name</span> 
            </td>
            <td align="left" colspan="3" valign="top">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)">
                </asp:Label>
                <asp:TextBox ID="txtEMPNAME" runat="server" CssClass="inputbox" ReadOnly="True">
                </asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetEMPNAME(); return false;" /><br />
                <asp:GridView ID="gvEMPName" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" PageSize="5" >
                    <Columns>
                        <asp:TemplateField HeaderText="EMP ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4" >
                <asp:CheckBox ID="chkShowAll" runat="server" CssClass="field-label" Text="Show All" />
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table>
                </div>
            </div>
        </div>


    <asp:HiddenField ID="h_EMPID" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_DEPTID" runat="server" />
    <asp:HiddenField ID="h_CATID" runat="server" />
    <asp:HiddenField ID="h_DESGID" runat="server" />
    <input id="hfBSU" runat="server" type="hidden" />
    <input id="hfDepartment" runat="server" type="hidden" />
    <input id="hfCategory" runat="server" type="hidden" />
    <input id="hfDesignation" runat="server" type="hidden" />
    <input id="hfEmpName" runat="server" type="hidden" />
    <input id="hfReport" runat="server" type="hidden" />
</asp:Content>
