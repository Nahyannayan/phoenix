<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptEMPDailyAttendanceReports.aspx.vb" Inherits="Reports_ASPX_Report_BankBook"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc4" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../../../Asset/UserControls/usrBSUnitsAsset.ascx" TagName="usrBSUnits"
    TagPrefix="uc2" %>
<%@ Register Src="../../../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function GetEMPNameREP() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../../../Accounts/accShowEmpDetail.aspx?id=ER", "pop_up1")
           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtReport.ClientID %>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetEMPNameATTREP() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../../../Accounts/accShowEmpDetail.aspx?id=EAR", "pop_up2")
           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtReport.ClientID %>', 'TextChanged');
            }
        }

        function parseDMY(s) {
            return new Date(s.replace(/^(\d+)\W+(\w+)\W+/, '$2 $1 '));
        }
        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            result = radopen("SelIDDESC.aspx?ID=EMP", "pop_up3")

           <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;--%>
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                // NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_EMPID.ClientID%>').value = arg.NameandCode; //NameandCode[0];
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID %>', 'TextChanged');
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Employee Daily Attendance Report">
            </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" style="width: 100%;">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" runat="server" id="tblMain" width="100%">
                    <%--This below tr tag added by vikranth on 28th July 2019--%>
                    <tr id="trELeave" visible="false" runat="server">
                        <td align="left" width="20%" valign="top"><span class="field-label">Select</span>
                        </td>
                        <td align="left" width="30%" style="text-align: left">
                            <asp:RadioButtonList ID="rblLeave" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Daily Attendance" Value="DailyAttendance" Selected />
                                <asp:ListItem Text="E-Leave" Value="Eleave" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="trDateRange" runat="server">
                        <td align="left" width="20%" valign="top"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" width="30%" style="text-align: left">
                            <uc4:usrDatePicker ID="UsrDateFrom" runat="server" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">To Date</span>
                        </td>
                        <td align="left" width="30%" style="text-align: left">
                            <uc4:usrDatePicker ID="UsrDateTo" runat="server" />
                        </td>
                    </tr>
                    <tr id="trAsOnDate" runat="server">
                        <td align="left" width="20%" valign="top"><span class="field-label">As On Date</span>
                        </td>
                        <td align="left" style="text-align: left">
                            <uc4:usrDatePicker ID="UsrAsOnDate" runat="server" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Employee Reports To</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtReport" runat="server" Width="90%">
                            </asp:TextBox>
                            <asp:ImageButton ID="btnReport" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNameATTREP(); return false;"
                                OnClick="btnReport_Click" />

                        </td>
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkAll" runat="server" Text="Include All" CssClass="field-label" Visible="False" /></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="Leavetype" runat="server">
                        <td align="left" width="20%"><span class="field-label">Leave Types</span></td>
                        <td align="left" width="30%">
                            <div class="checkbox-list-full">
                                <asp:CheckBoxList ID="CBLLeaveTypes" runat="server"
                                    DataSourceID="SqlDataSource1" DataTextField="ELT_DESCR" DataValueField="ELT_ID"
                                    RepeatColumns="2">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                        <td align="left" width="20%" valign="top"><span class="field-label">Category</span>
                        </td>
                        <td align="left" width="30%" style="text-align: left">
                            <div class="checkbox-list">
                                <uc3:usrTreeView ID="UsrTreeView1" runat="server" />
                            </div>
                        </td>
                    </tr>

                    <tr id="trEMPName">
                        <td align="left" width="20%" valign="top"><span class="field-label">Employee Name</span>
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)">
                            </asp:Label><br />
                            <asp:TextBox ID="txtEMPNAME" runat="server" OnTextChanged="txtEMPNAME_TextChanged" AutoPostBack="true">
                            </asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trEMPABCCAT" runat="server">
                        <td align="left" valign="top" width="20%"><span class="field-label">Employee Category</span>
                        </td>
                        <td align="left" colspan="3" valign="top">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" CssClass="field-label" Checked="True" />
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" CssClass="field-label" Checked="True" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr id="trGroup" runat="server" visible="false">
                        <td align="left" valign="top" width="20%"><span class="field-label">Group By</span>
                        </td>
                        <td align="left" colspan="3" valign="top">
                            <asp:RadioButton ID="rbEmployee" runat="server" CssClass="field-label" GroupName="GroupBy"
                                Checked="true" Text="Employee" AutoPostBack="True" />
                            <asp:RadioButton ID="rbDate" runat="server" CssClass="field-label" GroupName="GroupBy" Text="Date" AutoPostBack="True" />
                            <asp:RadioButton ID="rbCategory" runat="server" CssClass="field-label" GroupName="GroupBy" Text="Category" AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:CheckBox ID="chkShowAll" runat="server" CssClass="field-label" Text="Show All" Visible="False" />
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_DEPTID" runat="server" />
                <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                    ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                    SelectCommand="SELECT [ELT_ID], [ELT_DESCR] FROM [EMPLEAVETYPE_M]"></asp:SqlDataSource>
                <asp:HiddenField ID="h_CATID" runat="server" />
                <asp:HiddenField ID="h_DESGID" runat="server" />
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfDepartment" runat="server" type="hidden" />
                <input id="hfCategory" runat="server" type="hidden" />
                <input id="hfDesignation" runat="server" type="hidden" />
                <input id="hfEmpName" runat="server" type="hidden" />
                <input id="hfReport" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>
