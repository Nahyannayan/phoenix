Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Reports_ASPX_Report_rptEmpLeaveDetailsMonthly
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String
    'Version            Date            Author          Change
    '1.1                16-Jun-2011     Swapna          added menu and code for Leave salary Details-P150049
    '1.2                26-Jun-2011     Swapna          LOP after leave report link added-Leave Salary Details-Format I-P150051
    '1.3                27-Jul-2011     Swapna          As on Date value det to "31/Dec/YYYY" to vies leave details
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'ClientScript.RegisterStartupScript(Me.GetType(), _
            '"script", "<script language='javascript'>  CheckOnPostback(); </script>")

            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            
            BindCategory()

            Me.FillLeaveTypes()
          

            If Not IsPostBack Then

                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If MainMnu_code = "P150007" Then
                    txtFromDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = "31/Dec/" & Today.Year.ToString    'V1.3
                    txtToDate.Text = txtFromDate.Text

                ElseIf Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                    txtFromDate.Text = Cache("fromDate")
                    txtToDate.Text = Cache("toDate")
                Else
                    txtToDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-1))
                End If

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub

    

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GenerateEmpLeaveDetailsMonthly()
    End Sub
    

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillLeaveTypes() As Boolean
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet

        str_Sql = "select * from empleavetype_m"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Me.ddLeaveType.DataValueField = "ELT_ID"
        Me.ddLeaveType.DataTextField = "ELT_DESCR"
        ddLeaveType.DataSource = ds.Tables(0)
        ddLeaveType.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        'ViewState("isExport") = True
        'Select Case MainMnu_code
        '    Case "H000205"
        '        GenerateEmployeeDetails()
        'End Select
    End Sub


    

    Private Sub GenerateEmpLeaveDetailsMonthly()
       Try
                        
        Dim rptClass As New rptClass

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim cmd As New SqlCommand("[dbo].[rptEmpLeaveDetailsMonthly]")
            cmd.CommandType = CommandType.StoredProcedure

            Dim pFromDate As New SqlParameter("@FromDt", Me.txtFromDate.Text)
            cmd.Parameters.Add (pFromDate)
             Dim pToDate As New SqlParameter("@ToDt", Me.txtToDate.Text)
            cmd.Parameters.Add (pToDate)
            'Dim pBsuId As New SqlParameter("@BSU_ID", me.h_BSUID.value)
            Dim pBsuId As New SqlParameter("@BSU_ID", UsrBSUnits1.GetSelectedNode("|"))
            cmd.Parameters.Add(pBsuId)
            'Dim pCatId As New SqlParameter("@Cat_ID", Me.h_CATID.value)
            Dim pCatId As New SqlParameter("@Cat_ID", UsrTreeView1.GetSelectedNode())
            cmd.Parameters.Add(pcatid)
            Dim pABCCategory As new SqlParameter("@ABC_Cat_Id",SqlDbType.VarChar )
            Dim strabc As string
            If Me.chkEMPABC_A.Checked then
                'pABCCategory.Value="A"
                strabc="A|"
                End if
                If Me.chkEMPABC_B.Checked then
                'pABCCategory.Value="B"
                strabc=strabc & "B|"
                End if
                If Me.chkEMPABC_c.Checked then
                'pABCCategory.Value="C"
                strabc = strabc & "C|"
             End If
            'If strabc.EndsWith(",") then
            '    strabc=strabc.TrimEnd(",")
            'End If
            'strabc = "(" & strabc & ")"
            pABCCategory.Value = strabc 
            cmd.Parameters.Add (pABCCategory) 
            Dim pLeaveType As New SqlParameter("@Leave_Type", "LOP" )
            cmd.Parameters.Add(pLeaveType)

            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            
            Dim params As New Hashtable            
            params("pFromDate") = txtFromDate.Text
            params("pToDate") = txtToDate.Text
            params("userName") = Session("sUsr_name")

            repSource.Parameter = params
            'repSource.IncludeBSUImage = False
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../Payroll/Reports/RPT/rptEmpLeaveDetailsMonthly.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "fill_Studs", "window.open('../../../Reports/ASPX Report/Rptviewer.aspx','_blank', 'width=1024,height=600,resizable=yes,scrollbars=yes');", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class