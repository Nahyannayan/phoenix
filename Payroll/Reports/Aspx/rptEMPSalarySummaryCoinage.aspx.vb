Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(),
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            FillDeptNames(h_DEPTID.Value)
            FillCATNames(h_CATID.Value)
            FillDESGNames(h_DESGID.Value)
            FillEmpNames(h_EMPID.Value)
            StoreEMPFilter()

            If Not IsPostBack Then
                btnEmailSalarySlip.Visible = False
                imgGetBSUName.OnClientClick = "GetBSUName(); return false;"
                trPayMonths.Visible = False
                Select Case MainMnu_code
                    Case "P150035"
                        chkSummary.Visible = False
                        trSummary.Visible = False
                        lblrptCaption.Text = "Employees Salary Summary With Coinage"
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                    Case "P150015"
                        chkSummary.Text = "Vacation Salary Slip"
                        chkSummary.Visible = True
                        trSummary.Visible = True
                        lblrptCaption.Text = "Employees PaySlip"
                        lnkMonth.Visible = False
                        btnEmailSalarySlip.Visible = True
                        Session("EMP_MONTH_YEAR_COIN") = Nothing
                    Case "P150032"
                        chkSummary.Visible = False
                        trSummary.Visible = False
                        lblrptCaption.Text = "Employees Salary Reconciliation"
                End Select

                FillPayYearPayMonth()
                Session("EMP_MONTH_YEAR_COIN") = CreateMonthYearTable()
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'Added by vikranth on 14th Nov 2019
                If (Session("BSU_COUNTRY_ID") = "105") Then
                    trAabic.Visible = True
                Else trAabic.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub h_BSUID_ValueChanged(sender As Object, e As EventArgs) Handles h_BSUID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillBSUNames(h_BSUID.Value)
        'h_BSUID.Value = ""
    End Sub

    Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDeptNames(h_DEPTID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub

    Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDESGNames(h_DESGID.Value)
        'h_DESGID.Value = ""
    End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "P150035"
                GenerateSalarySummaryWithCoinage()
            Case "P150015"
                GenerateSalarySlip()
            Case "P150032"
                GenerateSalaryReconciliationEmployee()
        End Select
    End Sub

    Private Sub GenerateSalaryReconciliationEmployee()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = " SELECT     ESD.* , BNK.* FROM         VW_OSO_SALARYDATA AS ESD LEFT OUTER JOIN BANK_M AS BNK ON ESD.EMP_BANK = BNK.BNK_ID   " _
            & " WHERE " & strFiltBSUID & GetMonthYear()
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text.ToUpper
                'params("PAYYEAR") = ddlPayYear.SelectedValue

                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummary_Employee_Det.rpt"
                Session("ReportSource") = repSource
                Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateSalarySlip()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            'Dim str_Sql As String = "select ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            'GetFilter("ESD_EMP_ID", h_EMPID.Value, False) & _
            '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = "select DISTINCT ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " &
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) &
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) &
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) &
            GetFilter("EMP_ABC", strABC, True) &
            GetFilter("ESD_EMP_ID", h_EMPID.Value, True) & GetMonthYear() '& _
            If chkSummary.Checked Then
                str_Sql += " AND ESD_bVaccation = 1"
            End If
            '" AND ESD_bVaccation = " & IIf(chkSummary.Checked, 1, 0)
            '" OR (ESD_MONTH = " & ddlPayMonth.SelectedValue - 1 & " AND ESD_YEAR = " & ddlPayYear.SelectedValue & "))" & _
            Dim ESD_IDs As String = String.Empty
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            Dim comma As String = String.Empty
            While (dr.Read())
                ESD_IDs += comma + dr(0).ToString
                comma = ","
            End While

            'str_Sql = "SELECT  ESD_ID AS ESD_EMP_ID, ESD_ID , ESD_BSU_ID, ESD_MONTH, ESD_YEAR, ESD_DTTO, " & _
            '"ESD_DTFROM,  ESD_SAL_HRA, ESD_SAL_BASIC, ESD_SAL_TA, " & _
            '"ESD_SAL_OTHALLOW, ESD_SAL_EDUALLOW, ESD_EARN_HRA, ESD_EARN_BASIC, " & _
            '"ESD_EARN_TA, ESD_EARN_OTHALLOW, ESD_EARN_EDUALLOW, ESD_ADJUSTMENT, " & _
            '"ESD_EARN_GROSS, ESD_EARN_DED, ESD_PAIDAMT, ESD_EARN_NET, ESD_CUR_ID, " & _
            '"ESD_PAY_CUR_ID, ESD_MODE, ESD_ACCOUNT, ESD_DES_ID, ESD_ECT_ID, " & _
            '"ESD_DPT_ID, ESD_DAYS, ESD_DAYSWORKED, ESD_ADJDAYS, " & _
            '"ELV_ID, ELV_DTFROM, ELV_ACTUALDAYS, ELV_DTTO, ELV_REMARKS, " & _
            '"ELV_ELIGIBLEDAYS, ESD_ROUNDOFF, ESD_PAID, ESD_bPOSTED, " & _
            '"ESD_REFDOCNO, BSU_NAME, EMP_NAME, EMPNO, CAT_DESCR, DES_DESCR," & _
            '"GRADE_DESCR, EMP_JOINDT, CUR_DESCR, EMP_LASTREJOINDT, " & _
            '"CUR_DENOMINATION, ESD_DATE, EMP_DES_ID, EMP_ECT_ID, EMP_EGD_ID, " & _
            '"EMP_DPT_ID, DPT_DESCR, ESD_BANK, BSU_LOGO, EMP_ABC, " & _
            '"ESD_bVaccation, SAL_GROSS, ESD_LOPDAYS, ESD_NOPAYDAYS, " & _
            '"ESD_LEAVESALARY, ESD_AIRFARE, BHOLD " & _
            '"FROM vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            str_Sql = "SELECT * FROM vw_OSO_EMPSALARYHEADERDETAILS WHERE " &
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) &
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) &
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) &
            GetFilter("EMP_ABC", strABC, True) &
            GetFilter("ESD_EMP_ID", h_EMPID.Value, True) & GetMonthYear() '& _
            If chkSummary.Checked Then
                str_Sql += " AND ESD_bVaccation = 1"
            End If
            '" AND ESD_bVaccation = " & IIf(chkSummary.Checked, 1, 0)
            '" OR (ESD_MONTH = " & ddlPayMonth.SelectedValue - 1 & " AND ESD_YEAR = " & ddlPayYear.SelectedValue & "))" & _

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("@IMG_BSU_ID") = Session("sbsuid")
                'params("@IMG_TYPE") = "LOGO"
                Dim bMultiMonth As Boolean
                If Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
                    bMultiMonth = False
                ElseIf Session("EMP_MONTH_YEAR_COIN").Rows.Count <= 1 Then
                    bMultiMonth = False
                Else
                    bMultiMonth = True
                End If
                params("bMultiMonth") = bMultiMonth
                Dim repSourceSubRep(2) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubEarn As New SqlCommand
                cmdSubEarn.CommandText = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='D' AND ESS_ESD_ID in(" & ESD_IDs & ")"
                cmdSubEarn.Connection = New SqlConnection(str_conn)
                cmdSubEarn.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubEarn

                repSourceSubRep(1) = New MyReportClass
                Dim cmdSubDed As New SqlCommand
                cmdSubDed.CommandText = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='E' AND ESS_ESD_ID in(" & ESD_IDs & ")"
                cmdSubDed.Connection = New SqlConnection(str_conn)
                cmdSubDed.CommandType = CommandType.Text
                repSourceSubRep(1).Command = cmdSubDed
                ''changed from 126008-adv by swapana on 20may2020
                If Session("sBsuid") = "950040" Then
                    repSourceSubRep(2) = New MyReportClass
                    Dim cmdSubOT As New SqlCommand
                    cmdSubOT.CommandText = "select * from dbo.vw_OSO_EMPOTDetails WHERE EDD_ESD_ID in(" & ESD_IDs & ")"
                    cmdSubOT.Connection = New SqlConnection(str_conn)
                    cmdSubOT.CommandType = CommandType.Text
                    repSourceSubRep(2).Command = cmdSubOT
                End If

                'If Session("sBsuid") = "126008" Then
                '    repSourceSubRep(2) = New MyReportClass
                '    Dim cmdSubOT As New SqlCommand
                '    cmdSubDed.CommandText = "Get_OT_Details_For_Payslip"
                '    cmdSubDed.Connection = New SqlConnection(str_conn)
                '    Dim ESDIDs As New SqlParameter("@ESD_IDs", SqlDbType.Xml)
                '    ESDIDs.Value = ESD_IDs
                '    cmdSubDed.Parameters.Add(ESDIDs)
                '    repSourceSubRep(0).Command = cmdSubDed
                '    'repSourceSubRep(0).ResourceName = "CoinageHeading.rpt"
                '    cmdSubDed.CommandType = CommandType.StoredProcedure
                '    repSourceSubRep(2).Command = cmdSubOT
                'End If


                'repSourceSubRep(2) = New MyReportClass
                'Dim cmdSubEarn2 As New SqlCommand("", New SqlConnection(str_conn))
                'cmdSubEarn2.CommandType = CommandType.StoredProcedure
                'cmdSubEarn2.CommandType = CommandType.Text
                'cmdSubEarn2.CommandText = " exec ReportHeader_Subreport'" & Session("sbsuid") & "','" & "LOGO"
                'repSourceSubRep(2).Command = cmdSubEarn2
                'repSource.SubReport = repSourceSubRep


                repSource.SubReport = repSourceSubRep
                    repSource.IncludeBSUImage = True

                    repSource.Parameter = params
                    repSource.Command = cmd
                    'repSource.IncludeBSUImage = True
                    If chkSummary.Checked Then
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip_Vac.rpt"
                    Else
                    If Session("sBsuid") = "950040" Then ' This condition added by vikranth on 3rd Sept 2019  --''changed from 126008-adv by swapana on 20may2020
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip_Infracare.rpt"
                    ElseIf Session("BSU_COUNTRY_ID") = "217" Then ' This condition added by vikranth on 3rd Sept 2019
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip_Egypt.rpt"
                    Else
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip.rpt"
                    End If
                    End If
                    If chkForArabicRpt.Checked Then
                        Session("ForArabicRpt") = True
                        repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip_Ar.rpt"
                    Else
                        Session("ForArabicRpt") = Nothing
                    End If
                    Session("ReportSource") = repSource
                    'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                    ReportLoadSelection()
                Else
                    lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateSalarySummaryWithCoinage()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            'Dim str_Sql As String = "select ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            'GetFilter("ESD_EMP_ID", h_EMPID.Value, False) & _
            '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("ESD_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = "select * from dbo.vw_OSO_EMPSALARYHEADERDETAILS WHERE " &
            strFiltBSUID & GetFilter("EMP_ECT_ID", h_CATID.Value, True) &
            GetFilter("EMP_DPT_ID", h_DEPTID.Value, True) &
            GetFilter("EMP_DES_ID", h_DESGID.Value, True) &
            GetFilter("EMP_ABC", strABC, True) &
            GetFilter("ESD_EMP_ID", h_EMPID.Value, True) &
            " and    ISNULL(bHold, 0) = 0  AND ESD_MODE = 0 " & GetMonthYear() & " and isnull(esd_erd_id,0)=0 "

            Dim str_Sql_TotalSum As String = str_Sql.Replace("*", "sum(ESD_EARN_NET) as TOTAL_EARN_NET, ESD_BSU_ID") & " GROUP BY ESD_EMP_ID, ESD_BSU_ID"

            Dim dr As SqlDataReader
            dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If dr.HasRows Then
                Dim strAmount As String = String.Empty
                Dim strBSUID As String = String.Empty
                Dim drTotalSum As SqlDataReader
                drTotalSum = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql_TotalSum)
                While (drTotalSum.Read())
                    strAmount += drTotalSum("TOTAL_EARN_NET").ToString + "||"
                    strBSUID += drTotalSum("ESD_BSU_ID") + "||"
                End While
                strBSUID = GenerateXML(strBSUID, XMLType.BSUName)
                strAmount = GenerateXML(strAmount, XMLType.AMOUNT)
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("PeriodHeading") = "For the month " & GetPeriodHeading()

                Dim repSourceSubRep(3) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubCoinage As New SqlCommand("getCoinage", New SqlConnection(str_conn))
                cmdSubCoinage.CommandType = CommandType.StoredProcedure

                Dim sqlpAmount As New SqlParameter("@Amount", SqlDbType.Xml)
                sqlpAmount.Value = strAmount
                cmdSubCoinage.Parameters.Add(sqlpAmount)

                Dim sqlpbsu_id As New SqlParameter("@BSUIDs", SqlDbType.Xml)
                sqlpbsu_id.Value = strBSUID
                cmdSubCoinage.Parameters.Add(sqlpbsu_id)

                repSourceSubRep(0).Command = cmdSubCoinage
                repSourceSubRep(0).ResourceName = "CoinageHeading.rpt"

                repSourceSubRep(1) = New MyReportClass
                'Dim cmdSubCoinage_H As New SqlCommand("getCoinage", New SqlConnection(str_conn))
                'cmdSubCoinage_H.CommandType = CommandType.StoredProcedure

                'Dim sqlpAmount_H As New SqlParameter("@Amount", SqlDbType.Xml)
                'sqlpAmount_H.Value = strAmount
                'cmdSubCoinage_H.Parameters.Add(sqlpAmount_H)

                'Dim sqlpbsu_id_H As New SqlParameter("@bsu_id", SqlDbType.Xml)
                'sqlpbsu_id_H.Value = strBSUID
                'cmdSubCoinage_H.Parameters.Add(sqlpbsu_id_H)
                repSourceSubRep(1).ResourceName = "CoinageDet.rpt"
                repSourceSubRep(1).Command = cmdSubCoinage

                repSourceSubRep(2) = New MyReportClass
                repSourceSubRep(2).ResourceName = "CoinageDetSum.rpt"
                repSourceSubRep(2).Command = cmdSubCoinage

                repSourceSubRep(3) = New MyReportClass
                repSourceSubRep(3).ResourceName = "SalaryCoinage.rpt"
                repSourceSubRep(3).Command = cmdSubCoinage

                'repSourceSubRep(4) = New MyReportClass
                'repSourceSubRep(4).ResourceName = "ReportHeader_Sub_Landscape.rpt"
                'repSourceSubRep(3).Command = cmdSubCoinage

                repSource.SubReport = repSourceSubRep
                repSource.Parameter = params
                repSource.Command = cmd
                'repSource.IncludeBSUImage = True
                If chkSummary.Checked Then
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummaryDetailsCoinage.rpt"
                Else
                    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySummaryDetailsCoinage.rpt"
                End If

                Session("ReportSource") = repSource
                'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetPeriodHeading() As String
        Dim retVal As String = String.Empty
        If Not Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
            Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR_COIN")
            If dtMonthYear.Rows.Count > 0 Then
                Dim i As Integer
                Dim AddComma As String = String.Empty
                For i = 0 To dtMonthYear.Rows.Count - 1
                    retVal += AddComma & dtMonthYear.Rows(i)(1) & "-" & dtMonthYear.Rows(i)(2)
                    AddComma = ", "
                Next
            Else
                retVal = ddlPayMonth.SelectedItem.Text & "-" & ddlPayYear.SelectedValue
            End If
        Else
            retVal = ddlPayMonth.SelectedItem.Text & "-" & ddlPayYear.SelectedValue
        End If
        Return retVal
    End Function

    Private Function GetMonthYear() As String
        Try
            Dim retVal As String = " AND ("
            Dim AddOR As String = String.Empty
            If Not Session("EMP_MONTH_YEAR_COIN") Is Nothing AndAlso Session("EMP_MONTH_YEAR_COIN").Rows.Count > 0 Then
                Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR_COIN")
                Dim i As Integer
                For i = 0 To dtMonthYear.Rows.Count - 1
                    retVal += AddOR & " (ESD_MONTH = " & GetMonthNumber(dtMonthYear.Rows(i)(1))
                    retVal += " AND ESD_YEAR = " & dtMonthYear.Rows(i)(2) & ")"
                    AddOR = " OR "
                Next
            Else
                retVal += "ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue
            End If
            retVal += " ) "
            Return retVal
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetMonthNumber(ByVal month As String) As Integer
        Dim retVal As Integer = -1
        Select Case month
            Case "January"
                retVal = 1
            Case "February"
                retVal = 2
            Case "March"
                retVal = 3
            Case "April"
                retVal = 4
            Case "May"
                retVal = 5
            Case "June"
                retVal = 6
            Case "July"
                retVal = 7
            Case "August"
                retVal = 8
            Case "September"
                retVal = 9
            Case "October"
                retVal = 10
            Case "November"
                retVal = 11
            Case "December"
                retVal = 12
        End Select
        Return retVal
    End Function


    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID,EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Private Sub FillMonth_Year()
        If Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
            Session("EMP_MONTH_YEAR_COIN") = CreateMonthYearTable()
        End If
        Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR_COIN")
        Dim i As Integer
        Dim repeated As Boolean = False
        For i = 0 To dtMonthYear.Rows.Count - 1
            If dtMonthYear.Rows(i)(1) = ddlPayMonth.SelectedItem.Text And dtMonthYear.Rows(i)(2) = ddlPayYear.SelectedItem.Text Then
                repeated = True
                Exit For
            End If
        Next
        If Not repeated Then
            Dim dr As DataRow = dtMonthYear.NewRow()
            dr("ID") = ddlPayMonth.SelectedItem.Text & ddlPayYear.SelectedValue & dtMonthYear.Rows.Count
            dr("MONTH") = ddlPayMonth.SelectedItem.Text
            dr("YEAR") = ddlPayYear.SelectedValue
            dtMonthYear.Rows.Add(dr)
        End If
        If dtMonthYear Is Nothing Or dtMonthYear.Rows.Count <= 0 Then
            Exit Sub
        End If
        gvMonthYear.DataSource = dtMonthYear
        gvMonthYear.DataBind()

    End Sub

    Private Function CreateMonthYearTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim cMONTH As New DataColumn("MONTH", System.Type.GetType("System.String"))
            Dim cYEAR As New DataColumn("YEAR", System.Type.GetType("System.Int32"))

            dtDt.Columns.Add(cID)
            dtDt.Columns.Add(cMONTH)
            dtDt.Columns.Add(cYEAR)
            Return dtDt

        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub lnkMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMonth.Click
        trPayMonths.Visible = True
        FillMonth_Year()
    End Sub

    Protected Sub lnkbtnMonthDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblMONTHYEARID As New Label
        lblMONTHYEARID = TryCast(sender.FindControl("lblMonthYearID"), Label)
        If Not lblMONTHYEARID Is Nothing Then
            If Not Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
                Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR_COIN")
                Dim i As Integer
                For i = 0 To dtMonthYear.Rows.Count - 1
                    If dtMonthYear.Rows(i)(0) = lblMONTHYEARID.Text Then
                        dtMonthYear.Rows.RemoveAt(i)
                        Exit For
                    End If
                Next
                Session("EMP_MONTH_YEAR_COIN") = dtMonthYear
                gvMonthYear.DataSource = dtMonthYear
                gvMonthYear.DataBind()
            End If
        End If
    End Sub

    Protected Sub chkSummary_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSummary.CheckedChanged
        If chkSummary.Checked = True Then
            trPayMonths.Visible = True
            lnkMonth.Visible = True
            Session("EMP_MONTH_YEAR_COIN") = Nothing
        Else
            trPayMonths.Visible = False
            lnkMonth.Visible = False
            Session("EMP_MONTH_YEAR_COIN") = Nothing
        End If
    End Sub

    Protected Sub btnEmailSalarySlip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailSalarySlip.Click
        If chkSummary.Checked = True Then
            If gvMonthYear.Rows.Count > 1 Then
                lblError.Text = "Emails cannot be sent for multiple months.Please select single month."
                Exit Sub
            End If
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Using objConn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim transaction As SqlTransaction = objConn.BeginTransaction("SampleTransaction")
            Dim errorNo As Integer = EmailSalarySlip(objConn, transaction)
            If errorNo = 0 Then
                transaction.Commit()
                lblError.Text = "The Salary Slip will be mailed to the specified E-mail Address"
            Else
                lblError.Text = UtilityObj.getErrorMessage(errorNo)
                transaction.Rollback()
            End If
        End Using
    End Sub

    Private Function EmailSalarySlip(ByVal objConn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Try
            Dim iReturnvalue As Integer
            Dim strABC As String = String.Empty

            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim strFiltBSUID As String = GetFilter("EMP_BSU_ID", h_BSUID.Value, False)
            Dim str_Sql As String = String.Empty
            Dim htEMPDtails As New Hashtable
            str_Sql = "SELECT EMPLOYEE_M.EMP_ID, Isnull(EMPLOYEE_D.EMD_EMAIL,'swapna.tv@gemseducation.com') EMD_EMAIL, vw_OSO_EMPSALARYHEADERDETAILS.ESD_ID" &
            " FROM EMPLOYEE_M INNER JOIN vw_OSO_EMPSALARYHEADERDETAILS ON " &
            " EMPLOYEE_M.EMP_ID = vw_OSO_EMPSALARYHEADERDETAILS.ESD_EMP_ID INNER JOIN " &
            "EMPLOYEE_D ON EMPLOYEE_D.EMD_EMP_ID = EMPLOYEE_M.EMP_ID WHERE " &
            strFiltBSUID & GetFilter("EMPLOYEE_M.EMP_ECT_ID", h_CATID.Value, True) &
            GetFilter("EMPLOYEE_M.EMP_DPT_ID", h_DEPTID.Value, True) &
            GetFilter("EMPLOYEE_M.EMP_DES_ID", h_DESGID.Value, True) &
            GetFilter("EMPLOYEE_M.EMP_ABC", strABC, True) &
            GetFilter("EMPLOYEE_M.EMP_ID", h_EMPID.Value, True) &
             " and ESD_MONTH =" & ddlPayMonth.SelectedValue & " and isnull(ESD_PAID,0)=1 and isnull(esd_erd_id,0)=0 and isnull(esd_bvaccation,0)= " & IIf(chkSummary.Checked = True, 1, 0) & " and  ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim drEMPDetails As SqlDataReader = SqlHelper.ExecuteReader(objConn.ConnectionString, CommandType.Text, str_Sql)
            Dim strXMLString As New StringBuilder
            While (drEMPDetails.Read())
                If htEMPDtails(drEMPDetails("EMP_ID")) Is Nothing Then
                    htEMPDtails(drEMPDetails("EMP_ID")) = GenerateEmployeeDetailsXML(strXMLString.ToString(), drEMPDetails("EMP_ID"), drEMPDetails("ESD_ID"), drEMPDetails("EMD_EMAIL"))
                Else
                    htEMPDtails(drEMPDetails("EMP_ID")) = AppendNewValueToXML(strXMLString.ToString, drEMPDetails("EMP_ID"), drEMPDetails("ESD_ID"), drEMPDetails("EMD_EMAIL"))
                End If
            End While
            drEMPDetails.Close()

            Dim arrStr(htEMPDtails.Count) As String
            htEMPDtails.Values.CopyTo(arrStr, 0)

            strXMLString.Append("<EMPLOYEE_SALARY>")
            Dim ienum As IDictionaryEnumerator = htEMPDtails.GetEnumerator()
            While (ienum.MoveNext())
                strXMLString.Append(ienum.Value)
            End While
            strXMLString.Append("</EMPLOYEE_SALARY>")

            Dim bMultiMonth As Boolean
            If Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
                bMultiMonth = False
            ElseIf Session("EMP_MONTH_YEAR_COIN").Rows.Count <= 1 Then
                bMultiMonth = False
            Else
                bMultiMonth = True
            End If

            Dim cmd As New SqlCommand("SaveEMAILSALSLIP", objConn, trans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim sqlpEML_ESD_ID As New SqlParameter("@EML_EMP_DET", SqlDbType.Xml)
            sqlpEML_ESD_ID.Value = strXMLString.ToString() 'GenerateEmployeeDetailsXML(EML_EMP_IDs, EML_ESD_IDs)
            cmd.Parameters.Add(sqlpEML_ESD_ID)

            Dim sqlpEML_MONTHYEAR As New SqlParameter("@EML_MONTHYEAR", SqlDbType.Xml)
            sqlpEML_MONTHYEAR.Value = GenerateMonthYearXML()
            cmd.Parameters.Add(sqlpEML_MONTHYEAR)

            Dim sqlpEML_BMULTIMONTH As New SqlParameter("@EML_BMULTIMONTH", SqlDbType.Bit)
            sqlpEML_BMULTIMONTH.Value = bMultiMonth
            cmd.Parameters.Add(sqlpEML_BMULTIMONTH)

            Dim sqlpEML_DATE As New SqlParameter("@EML_DATE", SqlDbType.DateTime)
            sqlpEML_DATE.Value = DateTime.Now
            cmd.Parameters.Add(sqlpEML_DATE)

            Dim sqlpEML_USR_NAME As New SqlParameter("@EML_USR_NAME", SqlDbType.VarChar, 20)
            sqlpEML_USR_NAME.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpEML_USR_NAME)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
            Return 0
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000

        End Try
    End Function

    Public Function AppendNewValueToXML(ByVal XmlVal As String, ByVal EMP_ID As String, ByVal ESD_ID As String, ByVal EMAIL_ID As String) As String
        Dim xmlDocument As New XmlDocument()
        Dim XMLEESD_IDs As XmlElement
        Dim XMLEESD_ID As XmlElement
        Dim XMLEEMP_ID As XmlElement
        Dim SALARY_DETAILS As XmlElement
        Try
            XmlVal = "<EMPLOYEE_SALARY>" & XmlVal & "</EMPLOYEE_SALARY>"
            'Dim newEmployee As Boolean = True
            xmlDocument.LoadXml(XmlVal)
            For Each node As XmlNode In xmlDocument.SelectNodes("/EMPLOYEE_SALARY/SALARY_DETAILS")
                SALARY_DETAILS = TryCast(node, XmlElement)
                XMLEEMP_ID = DirectCast(SALARY_DETAILS.SelectSingleNode("EMP_ID"), XmlElement)
                If XMLEEMP_ID IsNot Nothing Then
                    If XMLEEMP_ID.InnerText = EMP_ID Then
                        XMLEESD_IDs = DirectCast(SALARY_DETAILS.SelectSingleNode("ESD_IDs"), XmlElement)
                        XMLEESD_ID = xmlDocument.CreateElement("ESD_ID")
                        XMLEESD_ID.InnerText = ESD_ID
                        XMLEESD_IDs.AppendChild(XMLEESD_ID)
                        SALARY_DETAILS.AppendChild(XMLEESD_IDs)
                        xmlDocument.DocumentElement.InsertBefore(SALARY_DETAILS, xmlDocument.DocumentElement.LastChild)
                        'newEmployee = False
                        Exit For
                    End If
                End If
            Next
            'If newEmployee Then
            '    AppendNewEMPID(XmlVal, EMP_ID, ESD_ID, EMAIL_ID)
            'End If
            Return xmlDocument.DocumentElement.InnerXml
            'Return xmlDocument.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Function GenerateEmployeeDetailsXML(ByVal xmlval As String, ByVal EMP_ID As String, ByVal ESD_ID As String, ByVal EMAIL_ID As String) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLEEMP_ID As XmlElement
        Dim XMLEESD_IDs As XmlElement
        Dim XMLEESD_ID As XmlElement
        Dim XMLESALARY_DETAILS As XmlElement
        Dim elements As String() = New String(3) {}
        elements(0) = "SALARY_DETAILS"
        elements(1) = "EMP_ID"
        elements(2) = "ESD_ID"
        elements(3) = "ESD_IDs"
        Try
            XMLESALARY_DETAILS = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(XMLESALARY_DETAILS)
            XMLEEMP_ID = xmlDoc.CreateElement(elements(1))
            XMLEEMP_ID.InnerText = EMP_ID
            XMLEESD_ID = xmlDoc.CreateElement(elements(2))
            XMLEESD_ID.InnerText = ESD_ID

            XMLEESD_IDs = xmlDoc.CreateElement(elements(3))
            XMLEESD_IDs.InsertBefore(XMLEESD_ID, xmlDoc.DocumentElement.LastChild)

            XMLESALARY_DETAILS.AppendChild(XMLEEMP_ID)
            XMLESALARY_DETAILS.AppendChild(XMLEESD_IDs)
            ' xmlDoc.DocumentElement.InsertBefore(XMLESALARY_DETAILS, xmlDoc.DocumentElement.LastChild)

            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Function GenerateMonthYearXML() As String
        Dim xmlDoc As New XmlDocument
        Dim MONTH_YEARDETAILS As XmlElement
        Dim XMLEMONTH As XmlElement
        Dim XMLEYEAR As XmlElement
        Dim XMLEMONTHYEARDetail As XmlElement
        Dim elements As String() = New String(3) {}
        elements(0) = "MONTH_YEARDETAILS"
        elements(1) = "MONTH_YEAR"
        elements(2) = "MONTH"
        elements(3) = "YEAR"
        Try
            If Not Session("EMP_MONTH_YEAR_COIN") Is Nothing _
            AndAlso Session("EMP_MONTH_YEAR_COIN").Rows.Count > 0 Then
                Dim dtMonthYear As DataTable = Session("EMP_MONTH_YEAR_COIN")
                Dim i As Integer
                MONTH_YEARDETAILS = xmlDoc.CreateElement(elements(0))
                xmlDoc.AppendChild(MONTH_YEARDETAILS)
                For i = 0 To dtMonthYear.Rows.Count - 1
                    XMLEMONTHYEARDetail = xmlDoc.CreateElement(elements(1))
                    XMLEMONTH = xmlDoc.CreateElement(elements(2))
                    XMLEMONTH.InnerText = GetMonthNumber(dtMonthYear.Rows(i)(1))
                    XMLEYEAR = xmlDoc.CreateElement(elements(3))
                    XMLEYEAR.InnerText = dtMonthYear.Rows(i)(2)
                    XMLEMONTHYEARDetail.AppendChild(XMLEMONTH)
                    XMLEMONTHYEARDetail.AppendChild(XMLEYEAR)
                    xmlDoc.DocumentElement.InsertBefore(XMLEMONTHYEARDetail, xmlDoc.DocumentElement.LastChild)
                Next
            Else
                MONTH_YEARDETAILS = xmlDoc.CreateElement(elements(0))
                xmlDoc.AppendChild(MONTH_YEARDETAILS)
                XMLEMONTHYEARDetail = xmlDoc.CreateElement(elements(1))
                XMLEMONTH = xmlDoc.CreateElement(elements(2))
                XMLEMONTH.InnerText = ddlPayMonth.SelectedValue
                XMLEYEAR = xmlDoc.CreateElement(elements(3))
                XMLEYEAR.InnerText = ddlPayYear.SelectedValue
                XMLEMONTHYEARDetail.AppendChild(XMLEMONTH)
                XMLEMONTHYEARDetail.AppendChild(XMLEYEAR)
                xmlDoc.DocumentElement.InsertBefore(XMLEMONTHYEARDetail, xmlDoc.DocumentElement.LastChild)
            End If
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Shared Function GenerateXML(ByVal ESD_IDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim xmlDetails As XmlElement
        Dim XMLE_ID As XmlElement
        Dim elements As String() = New String(2) {}
        elements(0) = "ESD_IDs"
        elements(1) = "ESD_ID"
        Try
            xmlDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(xmlDetails)
            Dim IDs As String() = ESD_IDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLE_ID = xmlDoc.CreateElement(elements(1))
                XMLE_ID.InnerText = IDs(i)
                xmlDoc.DocumentElement.InsertBefore(XMLE_ID, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


    Private Sub EmailReport(ByVal EMP_Email As String, ByVal subj As String, ByVal attchment As Stream)
        Dim msgMail As New System.Net.Mail.MailMessage()
        Dim smtp As New System.Net.Mail.SmtpClient("10.0.5.3")
        msgMail.To.Add(EMP_Email)
        msgMail.Subject = "Payslip for the Month of " & subj
        Dim mailfrom As New System.Net.Mail.MailAddress("shijin.ca@gmail.com", "SHIJIN")
        msgMail.From = mailfrom
        msgMail.IsBodyHtml = True
        msgMail.Body = "<html><Body>Hi, <br> Please find attached Salary Slip for the month of " & subj & "</Body></html>"
        Dim att As New Net.Mail.Attachment(attchment, "PaySlip_" & subj & ".pdf")
        msgMail.Attachments.Add(att)
        smtp.Send(msgMail)
    End Sub

    Private Sub SubReport(ByVal subRep As MyReportClass, ByRef repClassVal As RepClass, ByVal bIncludeImage As Boolean, ByVal repSource As MyReportClass)
        If subRep.SubReport IsNot Nothing Then
            Dim istart As Integer = IIf(bIncludeImage, 1, 0)
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(subRep, repClassVal, False, repSource)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataTable
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        'If ds Is Nothing Or ds.Rows.Count <= 0 Then
                        '    ds = Nothing
                        'End If
                        repClassVal.Subreports(i + istart).SetDataSource(ds)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gvMonthYear_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMonthYear.PageIndexChanging
        gvMonthYear.PageIndex = e.NewPageIndex
        FillMonth_Year()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/OASIS_LITE/Reports/ASPX Report/RptviewerNew.aspx');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "');", True)
        Else
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/OASIS_LITE/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + Page.ResolveUrl("~/Reports/ASPX Report/RptviewerNew.aspx") + "','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/OASIS_LITE/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" + Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx") + "');", True)
        Else
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/OASIS_LITE/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + Page.ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx") + "','_blank');", True)
        End If
    End Sub
End Class