Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CURRICULUM
Imports System.Collections
Imports GemBox.Spreadsheet

Partial Class Reports_rptEMPSalaryDetail
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String
    Dim rs As New ReportDocument
    'Version            Date            Author          Change
    '1.1                16-Jun-2011     Swapna          added menu and code for Leave salary Details-P150049
    '1.2                26-Jun-2011     Swapna          LOP after leave report link added-Leave Salary Details-Format I-P150051
    '1.3                27-Jul-2011     Swapna          As on Date value det to "31/Dec/YYYY" to vies leave details
    '1.4                04/nov/2012     Jacob           Added direct export of crosstab report to excel on button click. 
    '1.5                27/Nov/2013     Swapna          Added Export to Excel button for exporting employee CTC and other details
    '1.6                24/Dec/2013     Swapna          New report added to get gross comparison with last month
    '2.0                01/Feb/2017     Swapna          Adding status filter in salary consolidated report- Ref ticket 129554
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"

            End If
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.Cache.SetExpires(Now.AddSeconds(-1))
            'Response.Cache.SetNoStore()
            'Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value = Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillDeptNames(h_DEPTID.Value)
            FillCATNames(h_CATID.Value)
            BindCategory()
            'BindEmpStatus()
            FillDESGNames(h_DESGID.Value)
            FillEmpNames(h_EMPID.Value)
            StoreEMPFilter()
            UsrBSUnits1.MenuCode = MainMnu_code
            'BindEmpStatus() 'V2.0
            If Not IsPostBack Then
                trGroupBy.Visible = False
                chkViewSummary.Visible = True
                trChkViewSummary.Visible = True
                trSalComp.Style("display") = "none"
                trCostCtr.Style("display") = "none"
                trDateSel.Style("display") = "none"
                trEMPABCCat.Style("display") = "none"
                trChkViewSummary.Style("display") = "none"
                BindEmpStatus()
                Select Case MainMnu_code
                    Case "P150063"
                        lblrptCaption.Text = "Salary Detail-Consolidated"
                        trDateSel.Visible = False
                        lnkExporttoexcel.Visible = False
                        trGroupBy.Visible = True
                        trChkViewSummary.Visible = False
                        trDateSel.Style("display") = ""
                        trEMPABCCat.Style("display") = ""
                        trChkViewSummary.Style("display") = ""
                        ' trStatus.Style("display") = ""  'V2.0
                        btnExport.Visible = True
                    Case "P150064"
                        lblrptCaption.Text = "Salary Comparison Report"
                        trDateSel.Visible = False
                        lnkExporttoexcel.Visible = False
                        trGroupBy.Visible = False
                        trChkViewSummary.Visible = False
                        trDateSel.Style("display") = ""
                        trEMPABCCat.Style("display") = ""
                        trChkViewSummary.Style("display") = ""
                        btnExport.Visible = False
                        trMonthSel.Visible = True
                        FillPayYearPayMonth()
                End Select
                imgBankSel.Attributes.Remove("OnClick")
                imgBankSel.Attributes.Add("OnClick", " GetEMPNAME_ALL(); return false;")

                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                If MainMnu_code = "P150007" Then
                    txtFromDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = "31/Dec/" & Today.Year.ToString    'V1.3
                    txtToDate.Text = txtFromDate.Text
                    trDateSel.Style("display") = ""
                    trEMPABCCat.Style("display") = ""
                    trChkViewSummary.Style("display") = ""
                ElseIf MainMnu_code = "P159063" Then
                    Me.lnkExporttoexcel.Visible = True
                    trSalComp.Style("display") = ""
                    trCostCtr.Style("display") = ""
                    BindSalaryComponents()
                    BindCostCenter()
                    ' trStatus.Style("display") = "" 'V2.0
                    ViewState("SALCOMP_IDs") = ""
                    ViewState("CCTR_IDs") = ""
                    For i As Integer = 0 To trvSalComp.Nodes.Count - 1
                        trvSalComp.Nodes(i).CollapseAll()
                    Next
                    For i As Integer = 0 To trvCostCtr.Nodes.Count - 1
                        trvCostCtr.Nodes(i).CollapseAll()
                    Next
                ElseIf Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                    txtFromDate.Text = Cache("fromDate")
                    txtToDate.Text = Cache("toDate")
                Else
                    txtToDate.Text = UtilityObj.GetDiplayDate()
                    txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
                End If

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")

                Select Case MainMnu_code
                    Case "P450056"
                        lblrptCaption.Text = "Salary Revision Report"
                        trDateSel.Visible = True
                        trEMPABCCat.Visible = True
                        trDateSel.Style("display") = "table-row"
                        trEMPABCCat.Style("display") = "table-row"
                        trSelDepartment.Visible = False
                        trDepartment.Visible = False
                        trSelEMPName.Visible = False
                        trEMPName.Visible = False
                        trChkViewSummary.Visible = False
                        trSelcategory.Style("display") = "none"
                        trCategory.Style("display") = "none"
                        trCategoryTree.Visible = True
                    Case Else
                        'trCategory.Style("display") = "none"
                        trCategoryTree.Visible = False
                End Select

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkExporttoexcel)
    End Sub

    Protected Sub h_DEPTID_ValueChanged(sender As Object, e As EventArgs) Handles h_DEPTID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDeptNames(h_DEPTID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Protected Sub h_CATID_ValueChanged(sender As Object, e As EventArgs) Handles h_CATID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillCATNames(h_CATID.Value)
        'h_CATID.Value = ""
    End Sub

    Protected Sub h_DESGID_ValueChanged(sender As Object, e As EventArgs) Handles h_DESGID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillDESGNames(h_DESGID.Value)
        'h_DESGID.Value = ""
    End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs) Handles h_EMPID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillEmpNames(h_EMPID.Value)
        'h_EMPID.Value = ""
    End Sub

    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        tvCategory.DataSource = ds
        tvCategory.DataTextField = "DESCR"
        tvCategory.DataValueField = "ID"
        tvCategory.DataBind()
    End Sub
    Private Sub BindEmpStatus()  'V2.0
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT EST_ID as ID, EST_DESCR as DESCR FROM EMPSTATUS_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        chkempStatus.DataSource = ds
        chkempStatus.DataTextField = "DESCR"
        chkempStatus.DataValueField = "ID"
        chkempStatus.DataBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        'swapna changed
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub HideShowAllROWS(ByVal val As Boolean)
        trDepartment.Visible = val
        trCategory.Visible = val
        trDesignation.Visible = val
        trEMPName.Visible = val
        trSelDepartment.Visible = val
        trSelcategory.Visible = val
        trSelDesignation.Visible = val
        trSelEMPName.Visible = val
        trEMPABCCat.Visible = val
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("isExport") = False
        Select Case MainMnu_code
            Case "P150063"
                GenerateConsildatedSalaryDetail()
            Case "P159063"
                GenerateSalaryComponentsDetail()
            Case "P150064"
                GenerateSalaryComparisonReport()
            Case "P450056"
                GenerateSalaryRevisionReport()
        End Select
    End Sub

    Private Sub GenerateSalaryRevisionReport()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            Dim objConn As New SqlConnection(str_conn)
            cmd.Connection = objConn
            cmd.CommandText = "[rptGetSalaryRevisions]"

            Dim sal1, sal2 As Decimal
            If Me.txtSal1.Text = Nothing OrElse Val(Me.txtSal1.Text) <= 0 Then
                sal1 = 1
            Else
                sal1 = Convert.ToDecimal(Me.txtSal1.Text)
            End If
            If Me.txtSal2.Text = Nothing OrElse Val(Me.txtSal2.Text) <= 0 Then
                sal2 = 999999999.99
            Else
                sal2 = Convert.ToDecimal(Me.txtSal2.Text)
            End If

            Dim sqlParam(7) As SqlParameter

            sqlParam(0) = Mainclass.CreateSqlParameter("@From_Date", Me.txtFromDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@To_Date", Me.txtToDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            'sqlParam(3) = Mainclass.CreateSqlParameter("@Category", h_CATID.Value, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@Category", tvCategory.GetSelectedNode, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@DESIGNATION", h_DESGID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@ABC", strABC, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            If Me.optSalaryRangeType.SelectedValue = "B" Then
                sqlParam(6) = Mainclass.CreateSqlParameter("@SALARY_FROM", sal1, SqlDbType.Decimal)
                cmd.Parameters.Add(sqlParam(6))
                sqlParam(7) = Mainclass.CreateSqlParameter("@SALARY_To", sal2, SqlDbType.Decimal)
                cmd.Parameters.Add(sqlParam(7))
            ElseIf Me.optSalaryRangeType.SelectedValue = "G" Then
                sqlParam(6) = Mainclass.CreateSqlParameter("@SALARY_FROM", sal1, SqlDbType.Decimal)
                cmd.Parameters.Add(sqlParam(6))
                sqlParam(7) = Mainclass.CreateSqlParameter("@SALARY_To", sal2, SqlDbType.Decimal)
                cmd.Parameters.Add(sqlParam(7))
            Else
                sqlParam(6) = Mainclass.CreateSqlParameter("@SALARY_FROM", sal1, SqlDbType.Decimal)
                cmd.Parameters.Add(sqlParam(6))
                sqlParam(7) = Mainclass.CreateSqlParameter("@SALARY_To", sal2, SqlDbType.Decimal)
                cmd.Parameters.Add(sqlParam(7))
            End If

            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("@IMG_BSU_ID") = Session("sBsuid")
            'params("@IMG_TYPE") = "LOGO"
            params("reportCaption") = "For the period " & txtFromDate.Text & " to " & txtToDate.Text
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryRevision.rpt"
            Session("ReportSource") = repSource
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                'Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx?isExport=true", True)
                ReportLoadSelectionExport()
            Else
                'Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GenerateSalaryComparisonReport()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Dim strABC As String = String.Empty
        If chkEMPABC_A.Checked Then
            strABC = "A"
        End If
        If chkEMPABC_B.Checked Then
            strABC += "||B"
        End If
        If chkEMPABC_C.Checked Then
            strABC += "||C"
        End If
        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        Dim objConn As New SqlConnection(str_conn)
        cmd.Connection = objConn
        Try
            cmd.CommandText = "[GetSalaryDifferenceForMonths]"
            Dim sqlParam(8) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@DPT_ID", h_DEPTID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@DES_ID", h_DESGID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@CAT_ID", h_CATID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@ABC_CAT", strABC, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_ID", h_EMPID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            sqlParam(6) = Mainclass.CreateSqlParameter("@Month", ddlPayMonth.SelectedValue, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(6))
            sqlParam(7) = Mainclass.CreateSqlParameter("@Year", ddlPayYear.SelectedValue, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(7))

            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("GroupType") = ddGroupby.SelectedValue
            'params("fromDate") = txtFromDate.Text
            'params("toDate") = txtToDate.Text
            'params("BSU_NAME") = Mainclass.GetBSUName(Session("sBsuid"))
            'params("decimal") = Session("BSU_ROUNDOFF")
            params("reportCaption") = ddlPayMonth.SelectedItem.Text & " " & ddlPayYear.SelectedValue
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../Payroll/REPORTS/RPT/rptSalaryComparisonReport.rpt"
            Session("ReportSource") = repSource
            Dim rptClass As New rptClass

            With rptClass
                .crDatabase = objConn.Database
                .reportParameters = params
                .reportPath = Server.MapPath(repSource.ResourceName)
            End With

            'Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx", True)
            ReportLoadSelection()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Sub GenerateConsildatedSalaryDetail()


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Dim strABC As String = String.Empty
        If chkEMPABC_A.Checked Then
            strABC = "A"
        End If
        If chkEMPABC_B.Checked Then
            strABC += "||B"
        End If
        If chkEMPABC_C.Checked Then
            strABC += "||C"
        End If
        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        Dim objConn As New SqlConnection(str_conn)
        cmd.Connection = objConn
        Try

            cmd.CommandText = "[RPT_GetEmployeeSalaryDetails]"
            Dim sqlParam(7) As SqlParameter 'V2.0
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@DPT_ID", h_DEPTID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@DES_ID", h_DESGID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@CAT_ID", h_CATID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@EMP_ABC", strABC, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_ID", h_EMPID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            sqlParam(6) = Mainclass.CreateSqlParameter("@GROUPTYPE", ddGroupby.SelectedValue, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(6))
            'V2.0 starts
            Dim chkStatusvalues As String = ""
            For i As Integer = 0 To chkempStatus.Items.Count - 1
                If chkempStatus.Items(i).Selected Then
                    chkStatusvalues += chkempStatus.Items(i).Value + "|"
                End If
            Next
            chkStatusvalues = chkStatusvalues.TrimEnd("|")

            sqlParam(7) = Mainclass.CreateSqlParameter("@EMPstatus", chkStatusvalues, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(7))
            'V2.0 ends

            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("GroupType") = ddGroupby.SelectedValue
            params("fromDate") = txtFromDate.Text
            params("toDate") = txtToDate.Text
            params("BSU_NAME") = Mainclass.GetBSUName(Session("sBsuid"))
            params("decimal") = Session("BSU_ROUNDOFF")
            params("reportCaption") = "Employee Details - Consolidated"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmpConsolidatedSalaryDetails.rpt"
            Session("ReportSource") = repSource
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                'Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx?isExport=true", True)
                ReportLoadSelectionExport()
            Else
                'Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelectionExport()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
    Private Sub GenerateSalaryComponentsDetail()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Dim strABC As String = String.Empty
        Dim isExport As Boolean = False
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            isExport = True
        End If
        UpdateSalaryCompSelected()
        UpdateCostCenterSelected()

        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        Dim objConn As New SqlConnection(str_conn)
        cmd.Connection = objConn
        Try
          
            cmd.CommandText = IIf(isExport = True, "[RPT_GetEmployeeSalaryComponentDetails]", "[RPT_GetEmployeeSalaryComponentDetailNormal]")
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@DPT_ID", h_DEPTID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@DES_ID", h_DESGID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@CAT_ID", h_CATID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@SAL_COMP", ViewState("SALCOMP_IDs"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_ID", h_EMPID.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            sqlParam(6) = Mainclass.CreateSqlParameter("@COST_CTR", ViewState("CCTR_IDs"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(6))
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("GroupType") = ddGroupby.SelectedValue
            params("fromDate") = txtFromDate.Text
            params("toDate") = txtToDate.Text
            params("BSU_NAME") = Mainclass.GetBSUName(Session("sBsuid"))
            params("reportCaption") = "Employee Salary Component Details"
            params(sqlParam(0).ParameterName) = sqlParam(0).Value
            params(sqlParam(1).ParameterName) = sqlParam(1).Value
            params(sqlParam(2).ParameterName) = sqlParam(2).Value
            params(sqlParam(3).ParameterName) = sqlParam(3).Value
            params(sqlParam(4).ParameterName) = sqlParam(4).Value
            params(sqlParam(5).ParameterName) = sqlParam(5).Value
            params(sqlParam(6).ParameterName) = sqlParam(6).Value

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = IIf(isExport = True, "../RPT/rptEmpComponentSalaryDetailsEXP.rpt", "../../Payroll/REPORTS/RPT/rptEmpComponentSalaryDetails.rpt")
            Session("ReportSource") = repSource
            Dim rptClass As New rptClass

            With rptClass
                .crDatabase = objConn.Database
                .reportParameters = params
                .reportPath = Server.MapPath(repSource.ResourceName)
            End With
            If isExport = True Then
                LoadReports(rptClass)
              
            Else
                'Response.Redirect("../../../Reports/ASPX Report/rptviewer.aspx", True)
                ReportLoadSelection()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Sub LoadReports(ByVal rptClass)


        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If

                If ViewState("MainMnu_code") = "PdfReport" Then
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.ContentType = "application/pdf"
                    rs.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                    rs.Close()
                    rs.Dispose()
                Else
                    Try

                        exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.Excel)

                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message, "exportreport007")
                    End Try
                    rs.Close()
                    rs.Dispose()
                End If
                ' Response.Flush()
                'Response.Close()
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            rs.Close()
            rs.Dispose()
        End Try
        'GC.Collect()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next

        myReportDocument.VerifyDatabase()
    End Sub
    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("~/Curriculum/ReportDownloads/")
        Dim tempFileName As String = Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                Dim hop As New CrystalDecisions.Shared.ExportOptions
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()

        'Dim mimeType As String
        'HttpContext.Current.Response.ContentType = mimeType
        Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()
        h_LNKBTN.Value = 0
        System.IO.File.Delete(tempFileNameUsed)
    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                'SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                'SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub
    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    Public Sub RootNode(ByVal Tree As TreeView)
        Tree.Nodes.Clear()
        Dim tn As New TreeNode()
        tn.Text = "All"
        tn.Value = "0"
        tn.Target = "_self"
        tn.NavigateUrl = "javascript:void(0)"
        Tree.Nodes.Add(tn)
    End Sub
    Public Sub BindSalaryComponents()
        RootNode(Me.trvSalComp)
        For Each node As TreeNode In trvSalComp.Nodes
            BindChildNodes(node, "SALCOM")
        Next
    End Sub
    Public Sub BindCostCenter()
        RootNode(Me.trvCostCtr)
        For Each node As TreeNode In trvCostCtr.Nodes
            BindChildNodes(node, "COSTCTR")
        Next
    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode, ByVal Tree As String)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = String.Empty
        Dim NText As String = ""
        Dim NValue As String = ""

        If Tree = "SALCOM" Then
            str_Sql = " SELECT ERN_ID,ERN_DESCR FROM EMPSALCOMPO_M order BY ERN_ORDER "
            NText = "ERN_DESCR"
            NValue = "ERN_ID"
        ElseIf Tree = "COSTCTR" Then
            str_Sql = " SELECT DISTINCT CCT_ID,CCT_DESCR,CCT_ORDER FROM OASISFIN.dbo.COSTCENTER_M A INNER JOIN Employees B ON B.EMP_CCT_ID=A.CCT_ID order BY A.CCT_ORDER "
            NText = "CCT_DESCR"
            NValue = "CCT_ID"
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item(NText).ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item(NValue).ToString()

                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub
    Private Sub UpdateSalaryCompSelected()
        Try
            Dim SALCOMP_IDs As String = ""
            For Each node As TreeNode In trvSalComp.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If SALCOMP_IDs.Trim() = "" Then
                    SALCOMP_IDs = node.Value
                Else
                    SALCOMP_IDs = SALCOMP_IDs + "|" + node.Value
                End If
            Next
            ViewState("SALCOMP_IDs") = SALCOMP_IDs
        Catch
            ViewState("SALCOMP_IDs") = ""
        End Try
    End Sub
    Private Sub UpdateCostCenterSelected()
        Try
            Dim CCTR_IDs As String = ""
            For Each node As TreeNode In trvCostCtr.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If CCTR_IDs.Trim() = "" Then
                    CCTR_IDs = node.Value
                Else
                    CCTR_IDs = CCTR_IDs + "|" + node.Value
                End If
            Next
            ViewState("CCTR_IDs") = CCTR_IDs
        Catch
            ViewState("CCTR_IDs") = ""
        End Try
    End Sub
    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        If h_LNKBTN.Value = 1 Then
            ViewState("isExport") = True
            Select Case MainMnu_code
                Case "P150063"
                    GenerateConsildatedSalaryDetail()
                Case "P159063"
                    GenerateSalaryComponentsDetail()
            End Select
            h_LNKBTN.Value = 0
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click 'V1.5
        Dim strABC As String = String.Empty
        If chkEMPABC_A.Checked Then
            strABC = "A"
        End If
        If chkEMPABC_B.Checked Then
            strABC += "||B"
        End If
        If chkEMPABC_C.Checked Then
            strABC += "||C"
        End If

        Dim cmd As New SqlCommand("getEmployeeCurrentDetailsWithSalaryReport")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "getEmployeeCurrentDetailsWithSalaryReport"

        Dim sqlparam(6) As SqlParameter  'V1.4
        sqlparam(0) = Mainclass.CreateSqlParameter("@BSU_ids", UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(0))
        sqlparam(1) = Mainclass.CreateSqlParameter("@DPT_ID", h_DEPTID.Value, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(1))
        sqlparam(2) = Mainclass.CreateSqlParameter("@DES_ID", h_DESGID.Value, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(2))
        sqlparam(3) = Mainclass.CreateSqlParameter("@CAT_ID", h_CATID.Value, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(3))
        sqlparam(4) = Mainclass.CreateSqlParameter("@EMP_ABC", strABC, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(4))
        sqlparam(5) = Mainclass.CreateSqlParameter("@EMP_ID", h_EMPID.Value, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(5))

        Dim chkStatusvalues As String = ""
        For i As Integer = 0 To chkempStatus.Items.Count - 1
            If chkempStatus.Items(i).Selected Then
                chkStatusvalues += chkempStatus.Items(i).Value + "|"
            End If
        Next
        chkStatusvalues = chkStatusvalues.TrimEnd("|")

        sqlparam(6) = Mainclass.CreateSqlParameter("@EMPstatus", chkStatusvalues, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlparam(6))




        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Dim conn As New SqlConnection
        conn.ConnectionString = str_conn
        cmd.Connection = conn
        Dim ds As New DataSet
        Dim _adapter As New SqlDataAdapter(cmd)
        _adapter.Fill(ds)


        ' = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "getEmployeeCurrentDetailsWithSalaryReport", sqlparam)
        ExporttoExcel(ds)
    End Sub
    Protected Sub ExporttoExcel(ByVal ds As DataSet) 'V 1.5

        Dim dt As DataTable

        dt = ds.Tables(0)
        'swapna added for name format
        Dim strmonth As String, strMinute As String
        strmonth = CStr(Today.Date.Month)
        strMinute = CStr(Now.Minute.ToString)

        If strmonth.Length = 1 Then
            strmonth = "0" + strmonth
        End If
        If strMinute.Length = 1 Then
            strMinute = "0" + strMinute
        End If


      
        ' Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls"
        'swapna adding for naming format
        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx" ' Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") +

        ' Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'ws.Rows(0).Cells.Style.Font.Weight = 700
        Dim colTot As Integer = dt.Columns.Count

        If colTot > 32 Then


            For c As Integer = 32 To colTot - 1

                ws.Columns(c).Style.NumberFormat = "#,##0.00"
            Next
        End If

        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        ''HttpContext.Current.Response.Flush()
        ''HttpContext.Current.Response.Close()
        'HttpContext.Current.Response.End()

        System.IO.File.Delete(tempFileName)

    End Sub

    Protected Sub optSalaryRangeType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSalaryRangeType.SelectedIndexChanged
        If Me.optSalaryRangeType.SelectedValue = "G" Then
            Me.lblAnd.Visible = False
            Me.txtSal2.Visible = False
        Else
            Me.lblAnd.Visible = True
            Me.txtSal2.Visible = True
        End If

    End Sub
End Class