﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Math

Imports UtilityObj
Partial Class Payroll_empInsuranceExportExcelNew
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Catid As String = ""
        If Not Request.QueryString("Catid") Is Nothing Then
            Catid = Request.QueryString("Catid")
        End If
        Dim ABCCat As String = ""
        If Not Request.QueryString("ABCCat") Is Nothing Then
            ABCCat = Request.QueryString("ABCCat")
        End If
        Dim DesID As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "exec  rptGetDependanceInsuranceDetailsNew  '" & Session("InsuBSUIds") & "','" & Session("InsuEmpIds") & "','" & Catid & "','" & ABCCat & "','" & Session("InsuDesIds") & "'," & Session("InsuCategory") & "," & Session("Status"))

        gvExport.DataSource = ds
        gvExport.DataBind()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        Return
    End Sub
    Protected Sub gvExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblNum As Label = CType(e.Row.FindControl("lblEmpDepID"), Label)
                Dim lblBsuName As Label = CType(e.Row.FindControl("lblBsuName"), Label)
                Dim lblStaffId As Label = CType(e.Row.FindControl("lblStaffId"), Label)
                Dim imgEmployee As Image = CType(e.Row.FindControl("imgEMployee"), Image)
                Dim RbDep As Telerik.Web.UI.RadBinaryImage = CType(e.Row.FindControl("RbDep"), Telerik.Web.UI.RadBinaryImage)
                'Dim imgFileName As String = "\" + Session("susr_name") + "_Insurance_" + Session("sBsuid") + "_" + lblNum.Text + ".jpeg"
                Dim imgFileName As String = "\" + Session("susr_name") + "_Insurance_" + lblBsuName.Text + "_" + lblStaffId.Text + "_" + lblNum.Text + ".jpeg"
                If imgEmployee.AlternateText <> "" Then
                    Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "/" & imgEmployee.AlternateText
                    Dim strPath As String = strImagePath ' GetPhotoPath()
                    Try
                        If strPath <> "" Then
                            ' System.IO.File.Copy(strPath, imgFilePath)
                        End If
                    Catch ex As Exception
                    End Try
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New  _
                      System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Try
                        Dim fullSizeImg As System.Drawing.Image
                        fullSizeImg = System.Drawing.Image.FromFile(strPath)
                        Dim thumbNailImg As System.Drawing.Image
                        thumbNailImg = fullSizeImg.GetThumbnailImage(60, 60, _
                                                               dummyCallBack, IntPtr.Zero)
                        thumbNailImg.Save(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName, System.Drawing.Imaging.ImageFormat.Jpeg)
                        imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                        RbDep.DataValue = Nothing
                        imgEmployee.Visible = True
                        thumbNailImg.Dispose()
                    Catch ex As Exception
                        UtilityObj.Errorlog("empIsurerowdatabound", ex.Message)
                    End Try
                ElseIf RbDep.DataValue.Length > 0 Then
                    Try
                        Dim OC As System.Drawing.Image '= EOS_MainClass.ConvertBytesToImage(PhotoBytes)
                        OC = EOS_MainClass.ConvertBytesToImage(RbDep.DataValue)

                        Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                        dummyCallBack = New  _
                          System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                        Dim fullSizeImg As System.Drawing.Image
                        fullSizeImg = OC
                        Dim thumbNailImg As System.Drawing.Image
                        thumbNailImg = fullSizeImg.GetThumbnailImage(60, 60, _
                                                               dummyCallBack, IntPtr.Zero)
                        thumbNailImg.Save(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName, System.Drawing.Imaging.ImageFormat.Jpeg)
                        imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                        imgEmployee.Visible = True
                    Catch ex As Exception
                        UtilityObj.Errorlog("ins rowdatabound", ex.Message)
                    End Try
                Else
                    imgEmployee.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("empIsurerowdatabound", ex.Message)
        End Try
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub Excel_Export()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
    "attachment;filename=GridViewExport.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        gvExport.AllowPaging = False
        gvExport.DataBind()
        For i As Integer = 0 To gvExport.Rows.Count - 1
            Dim row As GridViewRow = gvExport.Rows(i)
            row.Attributes.Add("class", "textmode")
        Next
        gvExport.RenderControl(hw)
        Dim style As String = "<style> .textmode " & "{ mso-number-format:\@; } </style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
    Protected Sub lnkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExport.Click
        Excel_Export()
    End Sub
End Class
