﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Telerik.Web.UI
Partial Class Payroll_TrackCovidPositiveCasesEntry
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")

        ScriptManager1.RegisterPostBackControl(btnDocAdd)
        Dim CurBsUnit As String = Session("sBsuid")
        If Page.IsPostBack = False Then
            'Get_Positive_Track_Infected_Details()
            Page.Title = OASISConstants.Gemstitle
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            imgsearch.Attributes.Add("OnClick", "GetStaffStudent('Staff');return false;")
            ClearAllSession()
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), CurBsUnit, ViewState("MainMnu_code"))
            If (Request.QueryString("datamode") Is Nothing) Or (Request.QueryString("MainMnu_code") Is Nothing) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim Bsu_ID As String = Session("sBsuid")
                Dim MNu_code As String = ViewState("MainMnu_code")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("datamode") = "view" Then
                    Try
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        ViewState("refName") = Encr_decrData.Decrypt(Request.QueryString("refName").Replace(" ", "+"))
                        ViewState("reftype") = Encr_decrData.Decrypt(Request.QueryString("reftype").Replace(" ", "+"))
                        ViewState("CTH_ID") = Encr_decrData.Decrypt(Request.QueryString("cthid").Replace(" ", "+"))
                        'FillEmployeeMasterValues(ViewState("viewid"))
                        rbtnStaffStudent.SelectedValue = ViewState("reftype")
                        rbtnStaffStudent.Enabled = False
                        rbtnStaffStudent_SelectedIndexChanged(Nothing, Nothing)
                        h_StaffNoStudentNo.Value = ViewState("viewid")
                        txtStaffNoStudentNo.Text = ViewState("refName")
                        txtInfectedPerson.Text = ViewState("refName")
                        txtCloseContactPerson.Text = ViewState("refName")
                        txtStaffNoStudentNo_TextChanged(Nothing, Nothing)
                        txtStaffNoStudentNo.Enabled = False
                        imgsearch.Enabled = False
                        Get_Positive_Track_Infected_Details()
                        Get_Positive_Track_Close_Contact_Details()
                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message)
                        lblError.Text = "Some Error occured while retrieving the data ... " & ex.Message
                        'btnSave.Visible = False
                        Exit Sub
                    Finally
                    End Try
                End If
            End If
        End If

    End Sub
    Public Shared Function GetDate(ByVal vDate As String) As String
        If vDate = "" OrElse CDate(vDate) = New Date(1900, 1, 1) Then
            Return "-"
        Else
            Return Format(CDate(vDate), OASISConstants.DateFormat)
        End If
    End Function
    Private Sub ClearAllSession()
        Session("EMPDOCDETAILS") = Nothing
        Session("CLOSEPERSONDETAILS") = Nothing
        ViewState("CTH_ID") = Nothing
    End Sub

    'Protected Sub btnDocAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocAdd.Click
    '    If Session("EMPDOCDETAILS") Is Nothing Then
    '        Session("EMPDOCDETAILS") = CreateDocDetailsTable()
    '    End If
    '    Dim dtDocDetails As DataTable = Session("EMPDOCDETAILS")
    '    Dim status As String = String.Empty
    '    If CDate(txtIsolationDate.Text) > CDate(txtDeIsolationDate.Text) Then
    '        lblError.Text = "De-Isolation Date should be less than Isolation Date"
    '        GridBindDocDetails()
    '        Exit Sub
    '    End If

    '    'Dim i As Integer
    '    'For i = 0 To dtDocDetails.Rows.Count - 1
    '    '    If Session("EMPDocEditID") IsNot Nothing And
    '    '    Session("EMPDocEditID") <> dtDocDetails.Rows(i)("UniqueID") Then
    '    '        If dtDocDetails.Rows(i)("Document") = ddlInfectedPerson.Text And
    '    '            dtDocDetails.Rows(i)("DocID") = h_DOC_DOCID.Value And
    '    '             dtDocDetails.Rows(i)("Doc_No") = txtDocDocNo.Text And
    '    '             dtDocDetails.Rows(i)("Doc_IssuePlace") = txtDocIssuePlace.Text And
    '    '             dtDocDetails.Rows(i)("Doc_IssueDate") = CDate(txtDocIssueDate.Text) And
    '    '             dtDocDetails.Rows(i)("Doc_ExpDate") = CDate(txtDocExpDate.Text) Then
    '    '            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
    '    '            GridBindDocDetails()
    '    '            Exit Sub
    '    '        End If
    '    '    End If
    '    'Next

    '    If btnDocAdd.Text = "Add" Then
    '        Dim newDR As DataRow = dtDocDetails.NewRow()
    '        newDR("UniqueID") = gvEMPDocDetails.Rows.Count()
    '        Dim categoryValue As String = ""
    '        If rbtnStaffStudent.SelectedValue = "Staff" Then
    '            newDR("InfectedCategory") = ddlStaffCategory.SelectedValue
    '            categoryValue = ddlStaffCategory.SelectedValue
    '        Else newDR("InfectedCategory") = ddlStudentCategory.SelectedValue
    '            categoryValue = ddlStudentCategory.SelectedValue
    '        End If
    '        If categoryValue <> "OTH" Then
    '            newDR("InfectedPerson") = ddlInfectedPerson.SelectedValue
    '        Else newDR("InfectedPerson") = txtInfectedPerson.Text
    '        End If
    '        newDR("IsolationDate") = CDate(txtIsolationDate.Text)
    '        newDR("DeIsolationDate") = CDate(txtDeIsolationDate.Text)
    '        newDR("Remarks") = txtRemarks.Text
    '        newDR("Status") = "Insert"
    '        If FileUploadDoc.HasFile Then
    '            Dim File As HttpPostedFile
    '            File = FileUploadDoc.PostedFile
    '            Dim imgByte As Byte() = Nothing
    '            imgByte = New Byte(File.ContentLength - 1) {}
    '            File.InputStream.Read(imgByte, 0, File.ContentLength)
    '            newDR("DocFile") = imgByte
    '            newDR("Document") = FileUploadDoc.FileName
    '            newDR("DocFileType") = System.IO.Path.GetExtension(FileUploadDoc.FileName)
    '            newDR("DocFileMIME") = FileUploadDoc.PostedFile.ContentType
    '        End If

    '        status = "Insert"
    '        lblError.Text = ""
    '        dtDocDetails.Rows.Add(newDR)
    '        btnDocAdd.Text = "Add"
    '    ElseIf btnDocAdd.Text = "Modify" Then
    '        Dim iIndex As Integer = 0
    '        Dim str_Search As String = Session("EMPDocEditID")
    '        For iIndex = 0 To dtDocDetails.Rows.Count - 1
    '            If str_Search = dtDocDetails.Rows(iIndex)("UniqueID") And dtDocDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
    '                Dim categoryValue As String = ""
    '                If rbtnStaffStudent.SelectedValue = "Staff" Then
    '                    dtDocDetails.Rows(iIndex)("InfectedCategory") = ddlStaffCategory.SelectedValue
    '                    categoryValue = ddlStaffCategory.SelectedValue
    '                Else dtDocDetails.Rows(iIndex)("InfectedCategory") = ddlStudentCategory.SelectedValue
    '                    categoryValue = ddlStudentCategory.SelectedValue
    '                End If
    '                If categoryValue <> "OTH" Then
    '                    dtDocDetails.Rows(iIndex)("InfectedPerson") = ddlInfectedPerson.SelectedValue
    '                Else dtDocDetails.Rows(iIndex)("InfectedPerson") = txtInfectedPerson.Text
    '                End If
    '                dtDocDetails.Rows(iIndex)("IsolationDate") = CDate(txtIsolationDate.Text)
    '                dtDocDetails.Rows(iIndex)("DeIsolationDate") = CDate(txtDeIsolationDate.Text)
    '                dtDocDetails.Rows(iIndex)("Remarks") = txtRemarks.Text

    '                If FileUploadDoc.HasFile Then
    '                    Dim File As HttpPostedFile
    '                    File = FileUploadDoc.PostedFile
    '                    Dim imgByte As Byte() = Nothing
    '                    imgByte = New Byte(File.ContentLength - 1) {}
    '                    File.InputStream.Read(imgByte, 0, File.ContentLength)
    '                    dtDocDetails.Rows(iIndex)("Document") = FileUploadDoc.FileName
    '                    dtDocDetails.Rows(iIndex)("DocFile") = imgByte
    '                    dtDocDetails.Rows(iIndex)("DocFileType") = System.IO.Path.GetExtension(FileUploadDoc.FileName)
    '                    dtDocDetails.Rows(iIndex)("DocFileMIME") = FileUploadDoc.PostedFile.ContentType
    '                End If


    '                If dtDocDetails.Rows(iIndex)("Status").ToString.ToUpper <> "INSERT" Then
    '                    dtDocDetails.Rows(iIndex)("Status") = "Edit"
    '                End If
    '                Exit For
    '            End If
    '        Next
    '        btnDocAdd.Text = "Add"
    '        ClearEMPDocDetails()
    '    End If

    '    If ViewState("datamode") = "EDIT" Then
    '        'If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
    '        '    ClearEMPDocDetails()
    '        'Else
    '        '    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
    '        'End If
    '        ClearEMPDocDetails()
    '    End If
    '    Session("EMPDOCDETAILS") = dtDocDetails
    '    GridBindDocDetails()

    'End Sub


    Protected Sub btnDocAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocAdd.Click
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If Trim(h_StaffNoStudentNo.Value) = "" Then
                    lblError.Text = "Select Staff/Student"
                    Exit Sub
                End If
                If CDate(txtIsolationDate.Text) > CDate(txtDeIsolationDate.Text) Then
                    lblError.Text = "De-Isolation Date should be less than Isolation Date"
                    Exit Sub
                End If

                If rbtnStaffStudent.SelectedValue <> "Staff" Then
                    If ddlStudentCategory.SelectedValue = "PAR" Then
                        If ddlInfectedPerson.SelectedItem.Text = "Other" Then
                            If txtInfectedPersonOther.Text.Trim = "" Then
                                lblError.Text = "Please enter Infected Person Other"
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                If ViewState("CTH_ID") = Nothing Then
                    ViewState("CTH_ID") = Save_Positive_Track_details(conn, transaction)
                End If
                Dim id As Integer
                id = Save_Positive_Track_Infected_details(conn, transaction, Convert.ToInt32(ViewState("CTH_ID")), 0, False)
                If id <> 0 Then
                    Dim retval As Integer = 0
                    If FileUploadDoc.HasFile Then
                        If Not (SaveDocument(FileUploadDoc, conn, transaction, id)) Then
                            retval = 991
                        End If
                    End If
                    If retval <> 991 Then
                        transaction.Commit()
                        lblError.Text = "Saved successfully."
                        Get_Positive_Track_Infected_Details()
                        ClearEMPDocDetails()
                    Else transaction.Rollback()
                        lblError.Text = "Unable to Save the record."
                    End If
                Else transaction.Rollback()
                    lblError.Text = "Unable to Save the record."
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using
    End Sub

    Protected Sub btnCloseContactAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseContactAdd.Click
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If Trim(h_StaffNoStudentNo.Value) = "" Then
                    lblError.Text = "Select Staff/Student"
                    Exit Sub
                End If
                If CDate(txtDTContactPositivecase.Text) > CDate(txtStDTQuarantine.Text) Then
                    lblError.Text = "Date of contact with the Positive case should be less tan start date of qurantine"
                    GridBindClosecontactDetails()
                    Exit Sub
                End If

                If CDate(txtStDTQuarantine.Text) > CDate(txtEndDTQuarantine.Text) Then
                    lblError.Text = "Start date of Qurantine should be less than end date of qurantine"
                    GridBindClosecontactDetails()
                    Exit Sub
                End If

                If rbtnStaffStudent.SelectedValue <> "Staff" Then
                    If ddlCloseStudentCategory.SelectedValue = "PAR" Then
                        If ddlCloseContactPerson.SelectedItem.Text = "Other" Then
                            If txtCloseContactOther.Text.Trim = "" Then
                                lblError.Text = "Please enter Close Contact Person Other"
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                If ViewState("CTH_ID") = Nothing Then
                    ViewState("CTH_ID") = Save_Positive_Track_details(conn, transaction)
                End If
                Dim id As Integer
                id = Save_Positive_Track_Close_Contact_details(conn, transaction, Convert.ToInt32(ViewState("CTH_ID")), 0, False)
                If id <> 0 Then
                    transaction.Commit()
                    lblError.Text = "Saved successfully."
                    Get_Positive_Track_Close_Contact_Details()
                    ClearCLOSEPERSONDETAILS()
                Else transaction.Rollback()
                    lblError.Text = "Unable to Save the record."
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using
    End Sub

    Private Function Save_Positive_Track_details(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer = 0

        Try

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("COV.SAVE_POSITIVE_TRACKING_H", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpCTH_LOG_USR As New SqlParameter("@CTH_LOG_USR", SqlDbType.VarChar)
            sqlpCTH_LOG_USR.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpCTH_LOG_USR)
            Dim sqlpCTH_BSU_ID As New SqlParameter("@CTH_BSU_ID", SqlDbType.VarChar)
            sqlpCTH_BSU_ID.Value = Session("sBSUID")
            cmd.Parameters.Add(sqlpCTH_BSU_ID)
            Dim sqlpCTH_REF_TYPE As New SqlParameter("@CTH_REF_TYPE", SqlDbType.VarChar)
            sqlpCTH_REF_TYPE.Value = rbtnStaffStudent.SelectedValue
            cmd.Parameters.Add(sqlpCTH_REF_TYPE)
            Dim sqlpCTH_REF_ID As New SqlParameter("@CTH_REF_ID", SqlDbType.Int)
            sqlpCTH_REF_ID.Value = h_StaffNoStudentNo.Value
            cmd.Parameters.Add(sqlpCTH_REF_ID)
            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            'Dim pParms(4) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@CTH_LOG_USR", Session("sUsr_name"))
            'pParms(1) = New SqlClient.SqlParameter("@CTH_BSU_ID", Session("sBSUID"))
            'pParms(2) = New SqlClient.SqlParameter("@CTH_REF_TYPE", rbtnStaffStudent.SelectedValue)
            'pParms(3) = New SqlClient.SqlParameter("@CTH_REF_ID", h_StaffNoStudentNo.Value)
            'pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'stTrans.Rollback()
            Return 0
        End Try
    End Function

    Private Function Save_Positive_Track_Infected_details(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal CTH_ID As Integer, ByVal CTD_ID As Integer, ByVal isDelete As Boolean) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("COV.SAVE_POSITIVE_TRACKING_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpCTD_ID As New SqlParameter("@CTD_ID", SqlDbType.BigInt)
            sqlpCTD_ID.Value = IIf(h_CTD_ID.Value = 0, CTD_ID, h_CTD_ID.Value)
            cmd.Parameters.Add(sqlpCTD_ID)

            Dim sqlpCTD_CTH_ID As New SqlParameter("@CTD_CTH_ID", SqlDbType.VarChar)
            sqlpCTD_CTH_ID.Value = CTH_ID
            cmd.Parameters.Add(sqlpCTD_CTH_ID)

            Dim sqlpCTD_INF_CLOSE_CONTACT As New SqlParameter("@CTD_INF_CLOSE_CONTACT", SqlDbType.VarChar)
            sqlpCTD_INF_CLOSE_CONTACT.Value = "INFECTED"
            cmd.Parameters.Add(sqlpCTD_INF_CLOSE_CONTACT)
            Dim selectedCategory As String = ""
            Dim sqlpCTD_CATEGORY As New SqlParameter("@CTD_CATEGORY", SqlDbType.VarChar)
            If rbtnStaffStudent.SelectedValue = "Staff" Then
                sqlpCTD_CATEGORY.Value = ddlStaffCategory.SelectedValue
                selectedCategory = ddlStaffCategory.SelectedValue
            Else sqlpCTD_CATEGORY.Value = ddlStudentCategory.SelectedValue
                selectedCategory = ddlStudentCategory.SelectedValue
            End If
            cmd.Parameters.Add(sqlpCTD_CATEGORY)

            Dim sqlpCTD_REF_ID As New SqlParameter("@CTD_REF_ID", SqlDbType.Int)
            If selectedCategory <> "OTH" AndAlso selectedCategory <> "SELF" Then
                Dim InfectPersonValue As String
                InfectPersonValue = ddlInfectedPerson.SelectedValue
                If selectedCategory = "PAR" Then
                    InfectPersonValue = InfectPersonValue.Remove(InfectPersonValue.Length - 1)
                End If
                sqlpCTD_REF_ID.Value = Convert.ToInt32(InfectPersonValue)
            Else sqlpCTD_REF_ID.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_REF_ID)
            '1
            Dim sqlpCTD_PERSON_NAME As New SqlParameter("@CTD_PERSON_NAME", SqlDbType.VarChar)
            If selectedCategory = "OTH" Or selectedCategory = "SELF" Then
                sqlpCTD_PERSON_NAME.Value = txtInfectedPerson.Text
            ElseIf ddlInfectedPerson.SelectedItem.Text = "Other" Then
                sqlpCTD_PERSON_NAME.Value = txtInfectedPersonOther.Text
            Else
                sqlpCTD_PERSON_NAME.Value = ddlInfectedPerson.SelectedItem.Text
            End If
            cmd.Parameters.Add(sqlpCTD_PERSON_NAME)

            Dim sqlpCTD_REMARKS As New SqlParameter("@CTD_REMARKS", SqlDbType.VarChar)
            sqlpCTD_REMARKS.Value = txtRemarks.Text
            cmd.Parameters.Add(sqlpCTD_REMARKS)

            Dim sqlpCTD_ISOL_DATE As New SqlParameter("@CTD_ISOL_DATE", SqlDbType.DateTime)
            If txtIsolationDate.Text <> "" Then
                sqlpCTD_ISOL_DATE.Value = txtIsolationDate.Text
            Else sqlpCTD_ISOL_DATE.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_ISOL_DATE)

            Dim sqlpCTD_DEISOL_DATE As New SqlParameter("@CTD_DEISOL_DATE", SqlDbType.DateTime)
            If txtDeIsolationDate.Text <> "" Then
                sqlpCTD_DEISOL_DATE.Value = txtDeIsolationDate.Text
            Else sqlpCTD_DEISOL_DATE.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_DEISOL_DATE)

            Dim sqlpCTD_DATE_OF_CONTACT As New SqlParameter("@CTD_DATE_OF_CONTACT", SqlDbType.DateTime)
            sqlpCTD_DATE_OF_CONTACT.Value = Nothing
            cmd.Parameters.Add(sqlpCTD_DATE_OF_CONTACT)

            Dim sqlpCTD_QUARANTINE_START As New SqlParameter("@CTD_QUARANTINE_START", SqlDbType.DateTime)
            sqlpCTD_QUARANTINE_START.Value = Nothing
            cmd.Parameters.Add(sqlpCTD_QUARANTINE_START)

            Dim sqlpCTD_QUARANTINE_END As New SqlParameter("@CTD_QUARANTINE_END", SqlDbType.DateTime)
            sqlpCTD_QUARANTINE_END.Value = Nothing
            cmd.Parameters.Add(sqlpCTD_QUARANTINE_END)

            Dim sqlpCTD_MOBILENO As New SqlParameter("@CTD_MOBILENO", SqlDbType.VarChar)
            sqlpCTD_MOBILENO.Value = Nothing
            cmd.Parameters.Add(sqlpCTD_MOBILENO)

            Dim sqlpCTD_DELETED As New SqlParameter("@CTD_DELETED", SqlDbType.Bit)
            sqlpCTD_DELETED.Value = isDelete
            cmd.Parameters.Add(sqlpCTD_DELETED)

            Dim sqlpCTD_INFLUENZA As New SqlParameter("@CTD_INFLUENZA", SqlDbType.VarChar)
            sqlpCTD_INFLUENZA.Value = ddlInfluezaSymptoms.SelectedValue
            cmd.Parameters.Add(sqlpCTD_INFLUENZA)

            Dim sqlpCTD_EDIT As New SqlParameter("@CTD_EDIT", SqlDbType.Bit)
            sqlpCTD_EDIT.Value = IIf(h_CTD_ID.Value = 0, False, True)
            cmd.Parameters.Add(sqlpCTD_EDIT)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            'Dim pParms(4) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@CTH_LOG_USR", Session("sUsr_name"))
            'pParms(1) = New SqlClient.SqlParameter("@CTH_BSU_ID", Session("sBSUID"))
            'pParms(2) = New SqlClient.SqlParameter("@CTH_REF_TYPE", rbtnStaffStudent.SelectedValue)
            'pParms(3) = New SqlClient.SqlParameter("@CTH_REF_ID", h_StaffNoStudentNo.Value)
            'pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'stTrans.Rollback()
            Return 0

        End Try
    End Function

    Private Function Save_Positive_Track_Close_Contact_details(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal CTH_ID As Integer, ByVal CTD_ID As Integer, ByVal isDelete As Boolean) As Integer
        Dim iReturnvalue As Integer = 0

        Try
            Dim strOtherStudId As String()
            If h_OtherStaffNoStudentNo.Value <> "" AndAlso Not h_OtherStaffNoStudentNo.Value Is Nothing Then
                strOtherStudId = h_OtherStaffNoStudentNo.Value.Split("||")
            End If

            If strOtherStudId IsNot Nothing Then
                If strOtherStudId.Length > 0 Then
                    For Each value As String In strOtherStudId
                        If value <> "" Then
                            If ddlCloseStaffCategory.SelectedValue = "OTHSTUD" Or ddlCloseStudentCategory.SelectedValue = "OTHSTUD" Then
                                Dim str_Sql As String = "select  STU_PASPRTNAME from STUDENT_M where stu_id=" & value
                                Dim ds As New DataSet
                                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
                                If ds.Tables.Count > 0 Then
                                    txtOtherStaffNoStudentNo.Text = ds.Tables(0).Rows(0)("STU_PASPRTNAME").ToString
                                End If
                            End If
                            iReturnvalue = Save_Multi_Positive_Track_Close_Contact_details(objConn, stTrans, CTH_ID, CTD_ID, isDelete, Convert.ToInt32(value))
                        End If
                    Next
                Else iReturnvalue = Save_Multi_Positive_Track_Close_Contact_details(objConn, stTrans, CTH_ID, CTD_ID, isDelete, Nothing)
                End If
            Else iReturnvalue = Save_Multi_Positive_Track_Close_Contact_details(objConn, stTrans, CTH_ID, CTD_ID, isDelete, Nothing)
            End If
            Return iReturnvalue
            'Dim cmd As New SqlCommand
            'cmd = New SqlCommand("COV.SAVE_POSITIVE_TRACKING_D", objConn, stTrans)
            'cmd.CommandType = CommandType.StoredProcedure

            'Dim sqlpCTD_ID As New SqlParameter("@CTD_ID", SqlDbType.BigInt)
            'sqlpCTD_ID.Value = CTD_ID
            'cmd.Parameters.Add(sqlpCTD_ID)

            'Dim sqlpCTD_CTH_ID As New SqlParameter("@CTD_CTH_ID", SqlDbType.VarChar)
            'sqlpCTD_CTH_ID.Value = CTH_ID
            'cmd.Parameters.Add(sqlpCTD_CTH_ID)

            'Dim sqlpCTD_INF_CLOSE_CONTACT As New SqlParameter("@CTD_INF_CLOSE_CONTACT", SqlDbType.VarChar)
            'sqlpCTD_INF_CLOSE_CONTACT.Value = "CLOSE_CONTACT"
            'cmd.Parameters.Add(sqlpCTD_INF_CLOSE_CONTACT)
            'Dim selectedCategory As String = ""
            'Dim sqlpCTD_CATEGORY As New SqlParameter("@CTD_CATEGORY", SqlDbType.VarChar)
            'If rbtnStaffStudent.SelectedValue = "Staff" Then
            '    sqlpCTD_CATEGORY.Value = ddlCloseStaffCategory.SelectedValue
            '    selectedCategory = ddlCloseStaffCategory.SelectedValue
            'Else sqlpCTD_CATEGORY.Value = ddlCloseStudentCategory.SelectedValue
            '    selectedCategory = ddlCloseStudentCategory.SelectedValue
            'End If
            'cmd.Parameters.Add(sqlpCTD_CATEGORY)

            'Dim sqlpCTD_REF_ID As New SqlParameter("@CTD_REF_ID", SqlDbType.Int)
            'If selectedCategory = "OTHSTAFF" Or selectedCategory = "OTHSTUD" Then
            '    sqlpCTD_REF_ID.Value = h_OtherStaffNoStudentNo.Value
            'ElseIf selectedCategory <> "OTH" AndAlso selectedCategory <> "SELF" Then
            '    sqlpCTD_REF_ID.Value = ddlCloseContactPerson.SelectedValue
            'Else sqlpCTD_REF_ID.Value = Nothing
            'End If
            'cmd.Parameters.Add(sqlpCTD_REF_ID)
            ''1
            'Dim sqlpCTD_PERSON_NAME As New SqlParameter("@CTD_PERSON_NAME", SqlDbType.VarChar)
            'If selectedCategory = "OTHSTAFF" Or selectedCategory = "OTHSTUD" Then
            '    sqlpCTD_PERSON_NAME.Value = txtOtherStaffNoStudentNo.Text
            'ElseIf selectedCategory = "OTH" Or selectedCategory = "SELF" Then
            '    sqlpCTD_PERSON_NAME.Value = txtCloseContactPerson.Text
            'Else sqlpCTD_PERSON_NAME.Value = ddlCloseContactPerson.SelectedItem.Text
            'End If
            'cmd.Parameters.Add(sqlpCTD_PERSON_NAME)

            'Dim sqlpCTD_REMARKS As New SqlParameter("@CTD_REMARKS", SqlDbType.VarChar)
            'sqlpCTD_REMARKS.Value = txtCloseContactRemarks.Text
            'cmd.Parameters.Add(sqlpCTD_REMARKS)

            'Dim sqlpCTD_ISOL_DATE As New SqlParameter("@CTD_ISOL_DATE", SqlDbType.DateTime)
            'sqlpCTD_ISOL_DATE.Value = Nothing
            'cmd.Parameters.Add(sqlpCTD_ISOL_DATE)

            'Dim sqlpCTD_DEISOL_DATE As New SqlParameter("@CTD_DEISOL_DATE", SqlDbType.DateTime)
            'sqlpCTD_DEISOL_DATE.Value = Nothing
            'cmd.Parameters.Add(sqlpCTD_DEISOL_DATE)

            'Dim sqlpCTD_DATE_OF_CONTACT As New SqlParameter("@CTD_DATE_OF_CONTACT", SqlDbType.DateTime)
            'If txtDTContactPositivecase.Text <> "" Then
            '    sqlpCTD_DATE_OF_CONTACT.Value = txtDTContactPositivecase.Text
            'Else sqlpCTD_DATE_OF_CONTACT.Value = Nothing
            'End If
            'cmd.Parameters.Add(sqlpCTD_DATE_OF_CONTACT)

            'Dim sqlpCTD_QUARANTINE_START As New SqlParameter("@CTD_QUARANTINE_START", SqlDbType.DateTime)
            'If txtStDTQuarantine.Text <> "" Then
            '    sqlpCTD_QUARANTINE_START.Value = txtStDTQuarantine.Text
            'Else sqlpCTD_QUARANTINE_START.Value = Nothing
            'End If
            'cmd.Parameters.Add(sqlpCTD_QUARANTINE_START)

            'Dim sqlpCTD_QUARANTINE_END As New SqlParameter("@CTD_QUARANTINE_END", SqlDbType.DateTime)
            'If txtEndDTQuarantine.Text <> "" Then
            '    sqlpCTD_QUARANTINE_END.Value = txtEndDTQuarantine.Text
            'Else sqlpCTD_QUARANTINE_END.Value = Nothing
            'End If
            'cmd.Parameters.Add(sqlpCTD_QUARANTINE_END)

            'Dim sqlpCTD_DELETED As New SqlParameter("@CTD_DELETED", SqlDbType.Bit)
            'sqlpCTD_DELETED.Value = isDelete
            'cmd.Parameters.Add(sqlpCTD_DELETED)

            'Dim sqlpCTD_MOBILENO As New SqlParameter("@CTD_MOBILENO", SqlDbType.VarChar)
            'sqlpCTD_MOBILENO.Value = txtMobileNo.Text
            'cmd.Parameters.Add(sqlpCTD_MOBILENO)

            'Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            'retValParam.Direction = ParameterDirection.ReturnValue
            'cmd.Parameters.Add(retValParam)
            'cmd.ExecuteNonQuery()
            'iReturnvalue = retValParam.Value
            ''Dim pParms(4) As SqlClient.SqlParameter
            ''pParms(0) = New SqlClient.SqlParameter("@CTH_LOG_USR", Session("sUsr_name"))
            ''pParms(1) = New SqlClient.SqlParameter("@CTH_BSU_ID", Session("sBSUID"))
            ''pParms(2) = New SqlClient.SqlParameter("@CTH_REF_TYPE", rbtnStaffStudent.SelectedValue)
            ''pParms(3) = New SqlClient.SqlParameter("@CTH_REF_ID", h_StaffNoStudentNo.Value)
            ''pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)

            'Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'stTrans.Rollback()
            Return 0
        End Try
    End Function

    Private Function Save_Multi_Positive_Track_Close_Contact_details(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal CTH_ID As Integer, ByVal CTD_ID As Integer, ByVal isDelete As Boolean, Optional ByVal OtherStaffNoStudentNo As Integer = Nothing) As Integer
        Dim iReturnvalue As Integer = 0

        Try

            Dim cmd As New SqlCommand
            cmd = New SqlCommand("COV.SAVE_POSITIVE_TRACKING_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpCTD_ID As New SqlParameter("@CTD_ID", SqlDbType.BigInt)
            sqlpCTD_ID.Value = IIf(h_CTD_ID.Value = 0, CTD_ID, h_CTD_ID.Value)
            cmd.Parameters.Add(sqlpCTD_ID)

            Dim sqlpCTD_CTH_ID As New SqlParameter("@CTD_CTH_ID", SqlDbType.VarChar)
            sqlpCTD_CTH_ID.Value = CTH_ID
            cmd.Parameters.Add(sqlpCTD_CTH_ID)

            Dim sqlpCTD_INF_CLOSE_CONTACT As New SqlParameter("@CTD_INF_CLOSE_CONTACT", SqlDbType.VarChar)
            sqlpCTD_INF_CLOSE_CONTACT.Value = "CLOSE_CONTACT"
            cmd.Parameters.Add(sqlpCTD_INF_CLOSE_CONTACT)
            Dim selectedCategory As String = ""
            Dim sqlpCTD_CATEGORY As New SqlParameter("@CTD_CATEGORY", SqlDbType.VarChar)
            If rbtnStaffStudent.SelectedValue = "Staff" Then
                sqlpCTD_CATEGORY.Value = ddlCloseStaffCategory.SelectedValue
                selectedCategory = ddlCloseStaffCategory.SelectedValue
            Else sqlpCTD_CATEGORY.Value = ddlCloseStudentCategory.SelectedValue
                selectedCategory = ddlCloseStudentCategory.SelectedValue
            End If
            cmd.Parameters.Add(sqlpCTD_CATEGORY)

            Dim sqlpCTD_REF_ID As New SqlParameter("@CTD_REF_ID", SqlDbType.Int)
            If selectedCategory = "OTHSTAFF" Or selectedCategory = "OTHSTUD" Then
                'sqlpCTD_REF_ID.Value = h_OtherStaffNoStudentNo.Value
                sqlpCTD_REF_ID.Value = OtherStaffNoStudentNo
            ElseIf selectedCategory <> "OTH" AndAlso selectedCategory <> "SELF" Then
                Dim InfectPersonValue As String
                InfectPersonValue = ddlCloseContactPerson.SelectedValue
                If selectedCategory = "PAR" Then
                    InfectPersonValue = InfectPersonValue.Remove(InfectPersonValue.Length - 1)
                End If
                sqlpCTD_REF_ID.Value = Convert.ToInt32(InfectPersonValue)
                'sqlpCTD_REF_ID.Value = ddlCloseContactPerson.SelectedValue
            Else sqlpCTD_REF_ID.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_REF_ID)
            '1
            Dim sqlpCTD_PERSON_NAME As New SqlParameter("@CTD_PERSON_NAME", SqlDbType.VarChar)
            If selectedCategory = "OTHSTAFF" Or selectedCategory = "OTHSTUD" Then
                sqlpCTD_PERSON_NAME.Value = txtOtherStaffNoStudentNo.Text
            ElseIf selectedCategory = "OTH" Or selectedCategory = "SELF" Then
                sqlpCTD_PERSON_NAME.Value = txtCloseContactPerson.Text
            ElseIf ddlCloseContactPerson.SelectedItem.Text = "Other" Then
                sqlpCTD_PERSON_NAME.Value = txtCloseContactOther.Text
            Else
                sqlpCTD_PERSON_NAME.Value = ddlCloseContactPerson.SelectedItem.Text
            End If
            cmd.Parameters.Add(sqlpCTD_PERSON_NAME)

            Dim sqlpCTD_REMARKS As New SqlParameter("@CTD_REMARKS", SqlDbType.VarChar)
            sqlpCTD_REMARKS.Value = txtCloseContactRemarks.Text
            cmd.Parameters.Add(sqlpCTD_REMARKS)

            Dim sqlpCTD_ISOL_DATE As New SqlParameter("@CTD_ISOL_DATE", SqlDbType.DateTime)
            sqlpCTD_ISOL_DATE.Value = Nothing
            cmd.Parameters.Add(sqlpCTD_ISOL_DATE)

            Dim sqlpCTD_DEISOL_DATE As New SqlParameter("@CTD_DEISOL_DATE", SqlDbType.DateTime)
            sqlpCTD_DEISOL_DATE.Value = Nothing
            cmd.Parameters.Add(sqlpCTD_DEISOL_DATE)

            Dim sqlpCTD_DATE_OF_CONTACT As New SqlParameter("@CTD_DATE_OF_CONTACT", SqlDbType.DateTime)
            If txtDTContactPositivecase.Text <> "" Then
                sqlpCTD_DATE_OF_CONTACT.Value = txtDTContactPositivecase.Text
            Else sqlpCTD_DATE_OF_CONTACT.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_DATE_OF_CONTACT)

            Dim sqlpCTD_QUARANTINE_START As New SqlParameter("@CTD_QUARANTINE_START", SqlDbType.DateTime)
            If txtStDTQuarantine.Text <> "" Then
                sqlpCTD_QUARANTINE_START.Value = txtStDTQuarantine.Text
            Else sqlpCTD_QUARANTINE_START.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_QUARANTINE_START)

            Dim sqlpCTD_QUARANTINE_END As New SqlParameter("@CTD_QUARANTINE_END", SqlDbType.DateTime)
            If txtEndDTQuarantine.Text <> "" Then
                sqlpCTD_QUARANTINE_END.Value = txtEndDTQuarantine.Text
            Else sqlpCTD_QUARANTINE_END.Value = Nothing
            End If
            cmd.Parameters.Add(sqlpCTD_QUARANTINE_END)

            Dim sqlpCTD_DELETED As New SqlParameter("@CTD_DELETED", SqlDbType.Bit)
            sqlpCTD_DELETED.Value = isDelete
            cmd.Parameters.Add(sqlpCTD_DELETED)

            Dim sqlpCTD_MOBILENO As New SqlParameter("@CTD_MOBILENO", SqlDbType.VarChar)
            sqlpCTD_MOBILENO.Value = txtMobileNo.Text
            cmd.Parameters.Add(sqlpCTD_MOBILENO)

            Dim sqlpCTD_INFLUENZA As New SqlParameter("@CTD_INFLUENZA", SqlDbType.VarChar)
            sqlpCTD_INFLUENZA.Value = ddlCloseInfluenzaSymptoms.SelectedValue
            cmd.Parameters.Add(sqlpCTD_INFLUENZA)

            Dim sqlpCTD_EDIT As New SqlParameter("@CTD_EDIT", SqlDbType.Bit)
            sqlpCTD_EDIT.Value = IIf(h_CTD_ID.Value = 0, False, True)
            cmd.Parameters.Add(sqlpCTD_EDIT)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value
            'Dim pParms(4) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@CTH_LOG_USR", Session("sUsr_name"))
            'pParms(1) = New SqlClient.SqlParameter("@CTH_BSU_ID", Session("sBSUID"))
            'pParms(2) = New SqlClient.SqlParameter("@CTH_REF_TYPE", rbtnStaffStudent.SelectedValue)
            'pParms(3) = New SqlClient.SqlParameter("@CTH_REF_ID", h_StaffNoStudentNo.Value)
            'pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'stTrans.Rollback()
            Return 0
        End Try
    End Function


    Private Function SaveDocument(ByVal fuUpload As FileUpload, ByVal con As SqlConnection, ByVal tran As SqlTransaction, ByVal PCD_ID As String) As Boolean
        Try


            Dim filePath As String = fuUpload.PostedFile.FileName
            'Dim fuUpload As FileUpload
            'filePath = System.IO.Path.GetExtension(fuUpload.FileName)

            Dim filename As String = Path.GetFileName(filePath)

            Dim ext As String = Path.GetExtension(filename)

            Dim contenttype As String = String.Empty


            Select Case ext
                Case ".doc"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".docx"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".xls"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".xlsx"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".jpg"

                    contenttype = "image/jpg"
                    Exit Select

                Case ".jpeg"

                    contenttype = "image/jpeg"
                    Exit Select

                Case ".tif"

                    contenttype = "image/tiff"
                    Exit Select

                Case ".png"

                    contenttype = "image/png"
                    Exit Select

                Case ".gif"

                    contenttype = "image/gif"
                    Exit Select

                Case ".pdf"

                    contenttype = "application/pdf"
                    Exit Select



            End Select

            If contenttype <> String.Empty Then

                Dim fs As Stream = fuUpload.PostedFile.InputStream
                Dim br As New BinaryReader(fs)
                Dim bytes As Byte() = br.ReadBytes(fs.Length)


                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", "COVIDINFECTED")
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bytes)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", Session("sBsuid"))
                Dim bUploadfile As Boolean = InsertUploadedFile(cmd, con, tran)
                If bUploadfile Then
                    lblError.Text = "File Uploaded Successfully"
                    Return True
                Else
                    lblError.Text = UtilityObj.getErrorMessage("991")
                    Return False
                End If

            Else
                lblError.Text = "File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function
    Public Function InsertUploadedFile(ByVal cmd As SqlCommand, ByVal con As SqlConnection, ByVal tran As SqlTransaction) As Boolean

        'Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("conString").ConnectionString()
        'Dim strConnString As String = "Data Source=GEMSPROJECT;Initial Catalog=OASIS_DOCS;Persist Security Info=True;User ID=sa;Password=xf6mt"
        ' Dim con As New SqlConnection(strConnString)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        cmd.Transaction = tran

        Try
            'con.Open()
            cmd.ExecuteNonQuery()
            Return True

        Catch ex As Exception
            Response.Write(ex.Message)
            Return False

        Finally

            'con.Close()
            'con.Dispose()

        End Try

    End Function
    Private Sub Get_Positive_Track_Infected_Details()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@CTD_INF_CLOSE_CONTACT", "INFECTED", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.GET_POSITIVE_TRACKING_REF_ID", param)
            If ds.Tables.Count > 0 Then
                gvEMPDocDetails.DataSource = ds.Tables(0)
                gvEMPDocDetails.DataBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Get_Positive_Track_Close_Contact_Details()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@CTD_INF_CLOSE_CONTACT", "CLOSE_CONTACT", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.GET_POSITIVE_TRACKING_REF_ID", param)
            If ds.Tables.Count > 0 Then
                gvCLOSEPERSONDETAILS.DataSource = ds.Tables(0)
                gvCLOSEPERSONDETAILS.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function CreateDocDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cInfectedCategory As New DataColumn("InfectedCategory", System.Type.GetType("System.String"))
            Dim cInfectedPerson As New DataColumn("InfectedPerson", System.Type.GetType("System.String"))
            Dim cIsolationDate As New DataColumn("IsolationDate", System.Type.GetType("System.DateTime"))
            Dim cDeIsolationDate As New DataColumn("DeIsolationDate", System.Type.GetType("System.DateTime"))
            Dim cRemarks As New DataColumn("Remarks", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cDocument As New DataColumn("Document", System.Type.GetType("System.String"))
            Dim cDocID As New DataColumn("DocID", System.Type.GetType("System.String"))
            Dim cDocFile As New DataColumn("DocFile", System.Type.GetType("System.Object"))
            Dim cDocFileType As New DataColumn("DocFileType", System.Type.GetType("System.String"))
            Dim cDocFileMIME As New DataColumn("DocFileMIME", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cInfectedCategory)
            dtDt.Columns.Add(cInfectedPerson)
            dtDt.Columns.Add(cIsolationDate)
            dtDt.Columns.Add(cDeIsolationDate)
            dtDt.Columns.Add(cRemarks)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cDocument)
            dtDt.Columns.Add(cDocID)
            dtDt.Columns.Add(cDocFile)
            dtDt.Columns.Add(cDocFileType)
            dtDt.Columns.Add(cDocFileMIME)


            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnDocCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocCancel.Click
        ClearEMPDocDetails()
    End Sub

    Sub ClearEMPDocDetails()
        'txtDocDocNo.Text = ""
        h_DOC_DOCID.Value = ""
        txtInfectedPerson.Text = ""
        txtRemarks.Text = ""
        txtIsolationDate.Text = ""
        txtDeIsolationDate.Text = ""
        btnDocAdd.Text = "Add"
        'lblError.Text = ""
        Session.Remove("EMPDocEditID")
        h_CTD_ID.Value = 0
    End Sub
    Protected Sub lnkDocSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        For iIndex As Integer = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If Session("EMPDOCDETAILS").Rows(iIndex)("UniqueID") = lblTid.Text Then

                If Not String.IsNullOrEmpty(Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType")) Then
                    Dim imgByte As Byte() = CType(Session("EMPDOCDETAILS").Rows(iIndex)("DocFile"), Byte())
                    'Response.ContentType = Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    'Response.BinaryWrite(imgByte)

                    Dim Filename As String = Session("EMPDOCDETAILS").Rows(iIndex)("Document").ToString() + Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    ' Session("EMPDOCDETAILS").Rows(iIndex)("DocFileType").ToString()
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = Session("EMPDOCDETAILS").Rows(iIndex)("DocFileMIME").ToString()
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Filename)
                    Response.BinaryWrite(imgByte)
                    Response.Flush()
                End If

            End If
        Next
    End Sub
    Protected Sub lnkDocEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
            If str_Search = Session("EMPDOCDETAILS").Rows(iIndex)("UniqueID") And Session("EMPDOCDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                Dim categoryValue As String = ""
                If rbtnStaffStudent.SelectedValue = "Staff" Then
                    ddlStaffCategory.SelectedValue = Session("EMPDOCDETAILS").Rows(iIndex)("InfectedCategory")
                    categoryValue = ddlStaffCategory.SelectedValue
                Else ddlStudentCategory.SelectedValue = Session("EMPDOCDETAILS").Rows(iIndex)("InfectedCategory")
                    categoryValue = ddlStudentCategory.SelectedValue
                End If
                If categoryValue <> "OTH" Then
                    ddlInfectedPerson.SelectedValue = Session("EMPDOCDETAILS").Rows(iIndex)("InfectedPerson")
                Else txtInfectedPerson.Text = Session("EMPDOCDETAILS").Rows(iIndex)("InfectedPerson")
                End If
                If Session("EMPDOCDETAILS").Rows(iIndex)("DOCID").ToString <> "" Then
                    h_DOC_DOCID.Value = Session("EMPDOCDETAILS").Rows(iIndex)("DOCID")
                End If
                txtRemarks.Text = Session("EMPDOCDETAILS").Rows(iIndex)("Remarks")
                If Session("EMPDOCDETAILS").Rows(iIndex)("IsolationDate") Is Nothing OrElse
            Session("EMPDOCDETAILS").Rows(iIndex)("IsolationDate").ToString = "" OrElse
            Session("EMPDOCDETAILS").Rows(iIndex)("IsolationDate") = New Date(1900, 1, 1) Then
                    txtIsolationDate.Text = ""
                Else
                    txtIsolationDate.Text = Format(Session("EMPDOCDETAILS").Rows(iIndex)("IsolationDate"), OASISConstants.DateFormat)
                End If
                If Session("EMPDOCDETAILS").Rows(iIndex)("DeIsolationDate") Is Nothing OrElse
            Session("EMPDOCDETAILS").Rows(iIndex)("DeIsolationDate").ToString = "" OrElse
            Session("EMPDOCDETAILS").Rows(iIndex)("DeIsolationDate") = New Date(1900, 1, 1) Then
                    txtDeIsolationDate.Text = ""
                Else
                    txtDeIsolationDate.Text = Format(Session("EMPDOCDETAILS").Rows(iIndex)("DeIsolationDate"), OASISConstants.DateFormat)
                End If
                gvEMPDocDetails.SelectedIndex = iIndex
                btnDocAdd.Text = "Modify"
                Session("EMPDocEditID") = str_Search
                'UtilityObj.beforeLoopingControls(vwDocuments)
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvEMPDocDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPDocDetails.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim lnkDocSelect As LinkButton = DirectCast(e.Row.FindControl("lnkDocSelect"), LinkButton)
        '    Dim ScriptManager1 As ScriptManager = Page.Master.FindControl("ScriptManager1")
        '    ScriptManager1.RegisterPostBackControl(lnkDocSelect)

        '    Dim HF_DocFileMIME As HiddenField = DirectCast(e.Row.FindControl("HF_DocFileMIME"), HiddenField)
        '    If String.IsNullOrEmpty(HF_DocFileMIME.Value) Then
        '        lnkDocSelect.Enabled = False
        '    End If
        'End If
        Dim imgDoc As HyperLink = (TryCast(e.Row.FindControl("imgDoc"), HyperLink))
        Dim hfCOVDOC_ID As HiddenField = (TryCast(e.Row.FindControl("hfCOVDOC_ID"), HiddenField))
        If hfCOVDOC_ID IsNot Nothing Then
            If hfCOVDOC_ID.Value <> "" Then
                If hfCOVDOC_ID.Value > 0 Then
                    Dim hfCOV_ID As String = (TryCast(e.Row.FindControl("hfCOVDOC_ID"), HiddenField)).Value
                    imgDoc.Visible = True
                    imgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(hfCOV_ID)
                Else imgDoc.Visible = False
                End If
            End If
        End If
    End Sub
    Protected Sub gvEMPDocDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEMPDocDetails.RowDeleting
        'Dim categoryID As Integer = CInt(gvEMPDocDetails.DataKeys(e.RowIndex).Value)
        Dim categoryID As Integer = e.RowIndex
        DeleteRowDocDetails(categoryID)
    End Sub
    Protected Sub gvEMPDocDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvEMPDocDetails.RowEditing
        Try
            Dim lblCTDID As New Label
            Dim lblCategory As New Label
            Dim lblInfectedPerson As New Label
            Dim lblSOLDT As Label
            Dim lblDEISOLDT As Label
            Dim lblInflueza As Label
            Dim lblRemarks As Label
            Dim hfInfectedPersonId As HiddenField
            Dim index As Integer = Convert.ToInt32(e.NewEditIndex)
            Dim selectedRow As GridViewRow = DirectCast(gvEMPDocDetails.Rows(index), GridViewRow)
            lblCTDID = selectedRow.FindControl("lblCTDID")
            lblCategory = selectedRow.FindControl("lblCategory")
            lblInfectedPerson = selectedRow.FindControl("lblInfectedPerson")
            hfInfectedPersonId = selectedRow.FindControl("hfInfectedPersonId")
            lblSOLDT = selectedRow.FindControl("lblSOLDT")
            lblDEISOLDT = selectedRow.FindControl("lblDEISOLDT")
            lblInflueza = selectedRow.FindControl("lblInflueza")
            lblRemarks = selectedRow.FindControl("lblRemarks")
            If rbtnStaffStudent.SelectedValue <> "Staff" Then
                ddlStudentCategory.SelectedValue = lblCategory.Text
                ddlStudentCategory_SelectedIndexChanged(Nothing, Nothing)
                If lblCategory.Text = "SELF" Or lblCategory.Text = "OTH" Then
                    txtInfectedPerson.Text = lblInfectedPerson.Text
                ElseIf lblCategory.Text = "PAR" Then
                    'Dim tt As Boolean = Convert.ToBoolean(ddlCloseContactPerson.Items.FindByText(lblInfectedPerson.Text).Text)
                    Dim avaialbeItem As ListItem = ddlInfectedPerson.Items.FindByText(lblInfectedPerson.Text)
                    'If ddlInfectedPerson.Items.Contains(New ListItem(lblInfectedPerson.Text.ToString())) Then
                    If avaialbeItem IsNot Nothing Then
                        ddlInfectedPerson.Items.FindByText(lblInfectedPerson.Text).Selected = True
                        txtInfectedPersonOther.Visible = False
                        'ddlInfectedPerson.SelectedItem.Value = hfInfectedPersonId.Value
                    Else txtInfectedPersonOther.Text = lblInfectedPerson.Text
                        txtInfectedPersonOther.Visible = True
                        ddlInfectedPerson.Items.FindByText("Other").Selected = True
                    End If
                ElseIf lblCategory.Text = "SIB" Then
                    ddlInfectedPerson.SelectedValue = hfInfectedPersonId.Value
                End If
            Else
                ddlStaffCategory.SelectedValue = lblCategory.Text
                ddlStaffCategory_SelectedIndexChanged(Nothing, Nothing)
                If lblCategory.Text = "SELF" Or lblCategory.Text = "OTH" Then
                    txtInfectedPerson.Text = lblInfectedPerson.Text
                ElseIf lblCategory.Text = "DEP" Then
                    ddlInfectedPerson.SelectedValue = hfInfectedPersonId.Value
                End If
            End If
            txtIsolationDate.Text = lblSOLDT.Text
            txtDeIsolationDate.Text = lblDEISOLDT.Text
            txtRemarks.Text = lblRemarks.Text
            ddlInfluezaSymptoms.SelectedItem.Value = lblInflueza.Text
            h_CTD_ID.Value = lblCTDID.Text
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub DeleteRowDocDetails(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim selectedRow As GridViewRow = DirectCast(gvEMPDocDetails.Rows(pId), GridViewRow)
        Dim lblTid As New Label
        lblTid = selectedRow.FindControl("lblCTDID") 'TryCast(gvEMPDocDetails.Rows(pId).FindControl("lblId"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Dim id As Integer = 1000
            id = Save_Positive_Track_Infected_details(conn, transaction, Convert.ToInt32(ViewState("CTH_ID")), uID, True)
            If id = 0 Then
                transaction.Commit()
                lblError.Text = "Deleted successfully."
                Get_Positive_Track_Infected_Details()
            Else transaction.Rollback()
                lblError.Text = "Unable to delete the record."
            End If
        End Using
        'For iRemove = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
        '    If (Session("EMPDOCDETAILS").Rows(iRemove)("UniqueID") = uID) Then
        '        Session("EMPDOCDETAILS").Rows(iRemove)("Status") = "DELETED"
        '        'If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
        '        GridBindDocDetails()
        '        'Else
        '        '    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        '        'End If
        '        Exit For
        '    End If
        'Next
    End Sub

    Private Sub GridBindDocDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("EMPDOCDETAILS") Is Nothing Then
            gvEMPDocDetails.DataSource = Nothing
            gvEMPDocDetails.DataBind()
            Return
        End If
        Dim strColumnName As String = String.Empty
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateDocDetailsTable()
        If Session("EMPDOCDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPDOCDETAILS").Rows.Count - 1
                If (Session("EMPDOCDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPDOCDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("EMPDOCDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvEMPDocDetails.DataSource = dtTempDtl
        gvEMPDocDetails.DataBind()
    End Sub
    Protected Sub rbtnStaffStudent_SelectedIndexChanged(sender As Object, e As EventArgs)
        h_StaffNoStudentNo.Value = ""
        txtStaffNoStudentNo.Text = ""
        ddlInfectedPerson.Items.Clear()
        txtInfectedPerson.Visible = True
        txtInfectedPerson.Text = ""
        ddlInfectedPerson.Visible = False
        If rbtnStaffStudent.SelectedValue = "Staff" Then
            imgsearch.Attributes.Add("OnClick", "GetStaffStudent('Staff');return false;")
            ddlStaffCategory.Visible = True
            ddlStaffCategory.SelectedValue = "SELF"
            ddlStudentCategory.Visible = False
            ddlCloseStaffCategory.Visible = True
            ddlCloseStudentCategory.Visible = False
        ElseIf rbtnStaffStudent.SelectedValue = "Student" Then
            imgsearch.Attributes.Add("OnClick", "GetStaffStudent('Student');return false;")
            ddlStaffCategory.Visible = False
            ddlStudentCategory.Visible = True
            ddlStudentCategory.SelectedValue = "SELF"
            ddlCloseStaffCategory.Visible = False
            ddlCloseStudentCategory.Visible = True
        End If
    End Sub
    Protected Sub txtStaffNoStudentNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStaffNoStudentNo.TextChanged
        If rbtnStaffStudent.SelectedValue = "Staff" Then
            If ddlStaffCategory.SelectedValue <> "OTH" Then
                If ddlStaffCategory.SelectedValue <> "SELF" Then
                    BindInfectedPersons(ddlStaffCategory.SelectedValue)
                End If
            End If
            Get_Positive_Track_Infected_Details()
            Get_Positive_Track_Close_Contact_Details()
        ElseIf rbtnStaffStudent.SelectedValue = "Student" Then
            If ddlStudentCategory.SelectedValue <> "OTH" Then
                If ddlStudentCategory.SelectedValue <> "SELF" Then
                    BindInfectedPersons(ddlStudentCategory.SelectedValue)
                End If
            End If
            Get_Positive_Track_Infected_Details()
            Get_Positive_Track_Close_Contact_Details()
        End If
    End Sub
    Protected Sub ddlStaffCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStaffCategory.SelectedIndexChanged
        If ddlStaffCategory.SelectedValue <> "OTH" AndAlso ddlStaffCategory.SelectedValue <> "SELF" Then
            ddlInfectedPerson.Visible = True
            txtInfectedPerson.Visible = False
            If ddlStaffCategory.SelectedValue <> "SELF" Then
                BindInfectedPersons(ddlStaffCategory.SelectedValue)
            End If
        Else ddlInfectedPerson.Visible = False
            txtInfectedPerson.Visible = True
            txtInfectedPerson.Text = txtStaffNoStudentNo.Text
        End If
    End Sub
    Protected Sub ddlStudentCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStudentCategory.SelectedIndexChanged
        txtInfectedPersonOther.Text = ""
        txtInfectedPersonOther.Visible = False
        If ddlStudentCategory.SelectedValue <> "OTH" AndAlso ddlStudentCategory.SelectedValue <> "SELF" Then
            ddlInfectedPerson.Visible = True
            txtInfectedPerson.Visible = False
            If ddlStudentCategory.SelectedValue <> "SELF" Then
                BindInfectedPersons(ddlStudentCategory.SelectedValue)
            End If
        Else ddlInfectedPerson.Visible = False
            txtInfectedPerson.Visible = True
            txtInfectedPerson.Text = txtStaffNoStudentNo.Text
        End If
    End Sub

    Protected Sub ddlInfectedPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInfectedPerson.SelectedIndexChanged
        If ddlInfectedPerson.SelectedItem.Text = "Other" Then
            txtInfectedPersonOther.Visible = True
        Else txtInfectedPersonOther.Visible = False
        End If
    End Sub
    Protected Sub ddlCloseContactPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCloseContactPerson.SelectedIndexChanged
        If ddlCloseContactPerson.SelectedItem.Text = "Other" Then
            txtCloseContactOther.Visible = True
        Else txtCloseContactOther.Visible = False
        End If
    End Sub

    Private Sub BindInfectedPersons(ByVal type As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        If type = "DEP" Then
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@GET_TYPE", "DEP", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.Get_Dep_Par_Sib", param)
            ddlInfectedPerson.DataSource = ds
            ddlInfectedPerson.DataTextField = "EDD_NAME"
            ddlInfectedPerson.DataValueField = "EDD_ID"
            ddlInfectedPerson.DataBind()
        ElseIf type = "PAR" Then
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@GET_TYPE", "PAR", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.Get_Dep_Par_Sib", param)
            ddlInfectedPerson.DataSource = ds
            ddlInfectedPerson.DataTextField = "STS_FNAME"
            ddlInfectedPerson.DataValueField = "STS_STU_ID"
            ddlInfectedPerson.DataBind()
        ElseIf type = "SIB" Then
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@GET_TYPE", "SIB", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.Get_Dep_Par_Sib", param)
            ddlInfectedPerson.DataSource = ds
            ddlInfectedPerson.DataTextField = "SIB_Name"
            ddlInfectedPerson.DataValueField = "STU_ID"
            ddlInfectedPerson.DataBind()
        End If
    End Sub

    Protected Sub ddlCloseStaffCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCloseStaffCategory.SelectedIndexChanged
        If ddlCloseStaffCategory.SelectedValue = "OTHSTAFF" Then
            ddlCloseContactPerson.Visible = False
            txtCloseContactPerson.Visible = False
            txtOtherStaffNoStudentNo.Visible = True
            imgOtherStaffStud.Visible = True
            txtOtherStaffNoStudentNo.Text = ""
            h_OtherStaffNoStudentNo.Value = ""
            imgOtherStaffStud.Attributes.Add("OnClick", "GetOtherStaffStudent('Staff');return false;")
        ElseIf ddlCloseStaffCategory.SelectedValue = "OTHSTUD" Then
            ddlCloseContactPerson.Visible = False
            txtCloseContactPerson.Visible = False
            txtOtherStaffNoStudentNo.Visible = True
            imgOtherStaffStud.Visible = True
            txtOtherStaffNoStudentNo.Text = ""
            h_OtherStaffNoStudentNo.Value = ""
            imgOtherStaffStud.Attributes.Add("OnClick", "GetOtherStaffStudent('Student');return false;")
        ElseIf ddlCloseStaffCategory.SelectedValue <> "OTH" AndAlso ddlCloseStaffCategory.SelectedValue <> "SELF" Then
            ddlCloseContactPerson.Visible = True
            txtCloseContactPerson.Visible = False
            txtOtherStaffNoStudentNo.Visible = False
            imgOtherStaffStud.Visible = False
            If ddlCloseStaffCategory.SelectedValue <> "SELF" Then
                BindCloseContactPersons(ddlCloseStaffCategory.SelectedValue)
            End If
        Else ddlCloseContactPerson.Visible = False
            txtCloseContactPerson.Visible = True
            txtOtherStaffNoStudentNo.Visible = False
            imgOtherStaffStud.Visible = False
        End If
    End Sub
    Protected Sub ddlCloseStudentCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCloseStudentCategory.SelectedIndexChanged
        txtCloseContactOther.Text = ""
        txtCloseContactOther.Visible = False
        If ddlCloseStudentCategory.SelectedValue = "OTHSTAFF" Then
            ddlCloseContactPerson.Visible = False
            txtCloseContactPerson.Visible = False
            txtOtherStaffNoStudentNo.Visible = True
            imgOtherStaffStud.Visible = True
            txtOtherStaffNoStudentNo.Text = ""
            h_OtherStaffNoStudentNo.Value = ""
            imgOtherStaffStud.Attributes.Add("OnClick", "GetOtherStaffStudent('Staff');return false;")
        ElseIf ddlCloseStudentCategory.SelectedValue = "OTHSTUD" Then
            ddlCloseContactPerson.Visible = False
            txtCloseContactPerson.Visible = False
            txtOtherStaffNoStudentNo.Visible = True
            imgOtherStaffStud.Visible = True
            txtOtherStaffNoStudentNo.Text = ""
            h_OtherStaffNoStudentNo.Value = ""
            imgOtherStaffStud.Attributes.Add("OnClick", "GetOtherStaffStudent('Student');return false;")
        ElseIf ddlCloseStudentCategory.SelectedValue <> "OTH" AndAlso ddlCloseStudentCategory.SelectedValue <> "SELF" Then
            ddlCloseContactPerson.Visible = True
            txtCloseContactPerson.Visible = False
            txtOtherStaffNoStudentNo.Visible = False
            imgOtherStaffStud.Visible = False
            If ddlCloseStudentCategory.SelectedValue <> "SELF" Then
                BindCloseContactPersons(ddlCloseStudentCategory.SelectedValue)
            End If
        Else ddlCloseContactPerson.Visible = False
            txtCloseContactPerson.Visible = True
            txtOtherStaffNoStudentNo.Visible = False
            imgOtherStaffStud.Visible = False
        End If
    End Sub

    Private Sub BindCloseContactPersons(ByVal type As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        If type = "DEP" Then
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@GET_TYPE", "DEP", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.Get_Dep_Par_Sib", param)
            ddlCloseContactPerson.DataSource = ds
            ddlCloseContactPerson.DataTextField = "EDD_NAME"
            ddlCloseContactPerson.DataValueField = "EDD_ID"
            ddlCloseContactPerson.DataBind()
        ElseIf type = "PAR" Then
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@GET_TYPE", "PAR", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.Get_Dep_Par_Sib", param)
            ddlCloseContactPerson.DataSource = ds
            ddlCloseContactPerson.DataTextField = "STS_FNAME"
            ddlCloseContactPerson.DataValueField = "STS_STU_ID"
            ddlCloseContactPerson.DataBind()
        ElseIf type = "SIB" Then
            Dim param(1) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@REF_ID", h_StaffNoStudentNo.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@GET_TYPE", "SIB", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, "COV.Get_Dep_Par_Sib", param)
            ddlCloseContactPerson.DataSource = ds
            ddlCloseContactPerson.DataTextField = "SIB_Name"
            ddlCloseContactPerson.DataValueField = "STU_ID"
            ddlCloseContactPerson.DataBind()
        End If
    End Sub

    'Protected Sub btnCloseContactAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseContactAdd.Click
    '    If Session("CLOSEPERSONDETAILS") Is Nothing Then
    '        Session("CLOSEPERSONDETAILS") = CreateCloseContactDetailsTable()
    '    End If
    '    Dim dtCloseContactDetails As DataTable = Session("CLOSEPERSONDETAILS")
    '    Dim status As String = String.Empty
    '    If CDate(txtDTContactPositivecase.Text) > CDate(txtStDTQuarantine.Text) Then
    '        lblError.Text = "Date of contact with the Positive case should be less tan start date of qurantine"
    '        GridBindClosecontactDetails()
    '        Exit Sub
    '    End If

    '    If CDate(txtStDTQuarantine.Text) > CDate(txtEndDTQuarantine.Text) Then
    '        lblError.Text = "Start date of Qurantine should be less than end date of qurantine"
    '        GridBindClosecontactDetails()
    '        Exit Sub
    '    End If

    '    'Dim i As Integer
    '    'For i = 0 To dtCloseContactDetails.Rows.Count - 1
    '    '    If Session("EMPCloseContactEditID") IsNot Nothing And
    '    '    Session("EMPCloseContactEditID") <> dtCloseContactDetails.Rows(i)("UniqueID") Then
    '    '        If dtCloseContactDetails.Rows(i)("Document") = ddlInfectedPerson.Text And
    '    '            dtCloseContactDetails.Rows(i)("DocID") = h_DOC_DOCID.Value And
    '    '             dtCloseContactDetails.Rows(i)("Doc_No") = txtDocDocNo.Text And
    '    '             dtCloseContactDetails.Rows(i)("Doc_IssuePlace") = txtDocIssuePlace.Text And
    '    '             dtCloseContactDetails.Rows(i)("Doc_IssueDate") = CDate(txtDocIssueDate.Text) And
    '    '             dtCloseContactDetails.Rows(i)("Doc_ExpDate") = CDate(txtDocExpDate.Text) Then
    '    '            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
    '    '            GridBindClosecontactDetails()
    '    '            Exit Sub
    '    '        End If
    '    '    End If
    '    'Next

    '    If btnDocAdd.Text = "Add" Then
    '        Dim newDR As DataRow = dtCloseContactDetails.NewRow()
    '        newDR("UniqueID") = gvCLOSEPERSONDETAILS.Rows.Count()
    '        Dim categoryValue As String = ""
    '        If rbtnStaffStudent.SelectedValue = "Staff" Then
    '            newDR("CloseContactCategory") = ddlCloseStaffCategory.SelectedValue
    '            categoryValue = ddlCloseStaffCategory.SelectedValue
    '        Else newDR("CloseContactCategory") = ddlCloseStudentCategory.SelectedValue
    '            categoryValue = ddlCloseStudentCategory.SelectedValue
    '        End If
    '        If categoryValue <> "OTH" Then
    '            newDR("CloseContactPerson") = ddlCloseContactPerson.SelectedValue
    '        Else newDR("CloseContactPerson") = txtCloseContactPerson.Text
    '        End If
    '        newDR("DTContactWithPositiveCase") = CDate(txtDTContactPositivecase.Text)
    '        newDR("QuarantineStartDate") = CDate(txtStDTQuarantine.Text)
    '        newDR("QuarantineEndDate") = CDate(txtEndDTQuarantine.Text)
    '        newDR("CloseContactRemarks") = txtCloseContactRemarks.Text
    '        newDR("MobileNo") = txtMobileNo.Text
    '        newDR("Status") = "Insert"

    '        status = "Insert"
    '        lblError.Text = ""
    '        dtCloseContactDetails.Rows.Add(newDR)
    '        btnDocAdd.Text = "Add"
    '    ElseIf btnDocAdd.Text = "Modify" Then
    '        Dim iIndex As Integer = 0
    '        Dim str_Search As String = Session("EMPCloseContactEditID")
    '        For iIndex = 0 To dtCloseContactDetails.Rows.Count - 1
    '            If str_Search = dtCloseContactDetails.Rows(iIndex)("UniqueID") And dtCloseContactDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
    '                Dim categoryValue As String = ""
    '                If rbtnStaffStudent.SelectedValue = "Staff" Then
    '                    dtCloseContactDetails.Rows(iIndex)("CloseContactCategory") = ddlCloseStaffCategory.SelectedValue
    '                    categoryValue = ddlCloseStaffCategory.SelectedValue
    '                Else dtCloseContactDetails.Rows(iIndex)("CloseContactCategory") = ddlCloseStudentCategory.SelectedValue
    '                    categoryValue = ddlCloseStudentCategory.SelectedValue
    '                End If
    '                If categoryValue <> "OTH" Then
    '                    dtCloseContactDetails.Rows(iIndex)("CloseContactPerson") = ddlCloseContactPerson.SelectedValue
    '                Else dtCloseContactDetails.Rows(iIndex)("CloseContactPerson") = txtCloseContactPerson.Text
    '                End If
    '                dtCloseContactDetails.Rows(iIndex)("DTContactWithPositiveCase") = CDate(txtDTContactPositivecase.Text)
    '                dtCloseContactDetails.Rows(iIndex)("QuarantineStartDate") = CDate(txtStDTQuarantine.Text)
    '                dtCloseContactDetails.Rows(iIndex)("QuarantineEndDate") = CDate(txtEndDTQuarantine.Text)
    '                dtCloseContactDetails.Rows(iIndex)("CloseContactRemarks") = txtCloseContactRemarks.Text
    '                dtCloseContactDetails.Rows(iIndex)("MobileNo") = txtMobileNo.Text

    '                If dtCloseContactDetails.Rows(iIndex)("Status").ToString.ToUpper <> "INSERT" Then
    '                    dtCloseContactDetails.Rows(iIndex)("Status") = "Edit"
    '                End If
    '                Exit For
    '            End If
    '        Next
    '        btnDocAdd.Text = "Add"
    '        ClearCLOSEPERSONDETAILS()
    '    End If

    '    If ViewState("datamode") = "EDIT" Then
    '        'If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
    '        '    ClearCLOSEPERSONDETAILS()
    '        'Else
    '        '    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
    '        'End If
    '        ClearCLOSEPERSONDETAILS()
    '    End If
    '    Session("CLOSEPERSONDETAILS") = dtCloseContactDetails
    '    GridBindClosecontactDetails()

    'End Sub


    Private Function CreateCloseContactDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cCloseContactCategory As New DataColumn("CloseContactCategory", System.Type.GetType("System.String"))
            Dim cCloseContactPerson As New DataColumn("CloseContactPerson", System.Type.GetType("System.String"))
            Dim cDTContactWithPositiveCase As New DataColumn("DTContactWithPositiveCase", System.Type.GetType("System.DateTime"))
            Dim cQuarantineStartDate As New DataColumn("QuarantineStartDate", System.Type.GetType("System.DateTime"))
            Dim cQuarantineEndDate As New DataColumn("QuarantineEndDate", System.Type.GetType("System.DateTime"))
            Dim cCloseContactRemarks As New DataColumn("CloseContactRemarks", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cMobileNo As New DataColumn("MobileNo", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cCloseContactCategory)
            dtDt.Columns.Add(cCloseContactPerson)
            dtDt.Columns.Add(cDTContactWithPositiveCase)
            dtDt.Columns.Add(cQuarantineStartDate)
            dtDt.Columns.Add(cQuarantineEndDate)
            dtDt.Columns.Add(cCloseContactRemarks)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cMobileNo)


            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnCloseContactCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseContactCancel.Click
        ClearCLOSEPERSONDETAILS()
    End Sub

    Sub ClearCLOSEPERSONDETAILS()
        'txtDocDocNo.Text = ""
        h_DOC_DOCID.Value = ""
        txtCloseContactPerson.Text = ""
        txtCloseContactRemarks.Text = ""
        txtDTContactPositivecase.Text = ""
        txtStDTQuarantine.Text = ""
        txtEndDTQuarantine.Text = ""
        btnCloseContactAdd.Text = "Add"
        'lblError.Text = ""
        Session.Remove("EMPCloseContactEditID")
        h_CTD_ID.Value = 0
    End Sub

    Protected Sub lnkCloseContactEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblID2"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("CLOSEPERSONDETAILS").Rows.Count - 1
            If str_Search = Session("CLOSEPERSONDETAILS").Rows(iIndex)("UniqueID") And Session("CLOSEPERSONDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                Dim categoryValue As String = ""
                If rbtnStaffStudent.SelectedValue = "Staff" Then
                    ddlCloseStaffCategory.SelectedValue = Session("CLOSEPERSONDETAILS").Rows(iIndex)("CloseContactCategory")
                    categoryValue = ddlCloseStaffCategory.SelectedValue
                Else ddlCloseStudentCategory.SelectedValue = Session("CLOSEPERSONDETAILS").Rows(iIndex)("CloseContactCategory")
                    categoryValue = ddlCloseStudentCategory.SelectedValue
                End If
                If categoryValue <> "OTH" Then
                    ddlCloseContactPerson.SelectedValue = Session("CLOSEPERSONDETAILS").Rows(iIndex)("CloseContactPerson")
                Else txtCloseContactPerson.Text = Session("CLOSEPERSONDETAILS").Rows(iIndex)("CloseContactPerson")
                End If

                txtCloseContactRemarks.Text = Session("CLOSEPERSONDETAILS").Rows(iIndex)("CloseContactRemarks")
                txtMobileNo.Text = Session("CLOSEPERSONDETAILS").Rows(iIndex)("MobileNo")

                If Session("CLOSEPERSONDETAILS").Rows(iIndex)("DTContactWithPositiveCase") Is Nothing OrElse
            Session("CLOSEPERSONDETAILS").Rows(iIndex)("DTContactWithPositiveCase").ToString = "" OrElse
            Session("CLOSEPERSONDETAILS").Rows(iIndex)("DTContactWithPositiveCase") = New Date(1900, 1, 1) Then
                    txtDTContactPositivecase.Text = ""
                Else
                    txtDTContactPositivecase.Text = Format(Session("CLOSEPERSONDETAILS").Rows(iIndex)("DTContactWithPositiveCase"), OASISConstants.DateFormat)
                End If

                If Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineStartDate") Is Nothing OrElse
            Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineStartDate").ToString = "" OrElse
            Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineStartDate") = New Date(1900, 1, 1) Then
                    txtStDTQuarantine.Text = ""
                Else
                    txtStDTQuarantine.Text = Format(Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineStartDate"), OASISConstants.DateFormat)
                End If

                If Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineEndDate") Is Nothing OrElse
            Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineEndDate").ToString = "" OrElse
            Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineEndDate") = New Date(1900, 1, 1) Then
                    txtEndDTQuarantine.Text = ""
                Else
                    txtEndDTQuarantine.Text = Format(Session("CLOSEPERSONDETAILS").Rows(iIndex)("QuarantineEndDate"), OASISConstants.DateFormat)
                End If
                gvCLOSEPERSONDETAILS.SelectedIndex = iIndex
                btnCloseContactAdd.Text = "Modify"
                Session("EMPCloseContactEditID") = str_Search
                'UtilityObj.beforeLoopingControls(vwDocuments)
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvCLOSEPERSONDETAILS_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvCLOSEPERSONDETAILS.RowDeleting
        'Dim categoryID As Integer = CInt(gvCLOSEPERSONDETAILS.DataKeys(e.RowIndex).Value)
        Dim categoryID As Integer = e.RowIndex
        DeleteRowCloseContactDetails(categoryID)
    End Sub

    Protected Sub gvCLOSEPERSONDETAILS_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvCLOSEPERSONDETAILS.RowEditing
        Try
            Dim lblCTDID As New Label
            Dim lblCategory As New Label
            Dim lblContactedPerson As New Label
            Dim lblDTofContact As Label
            Dim lblQuarStDT As Label
            Dim lblQuarEndDT As Label
            Dim lblInflueza As Label
            Dim lblRemarks As Label
            Dim lblMobileNo As Label
            Dim hfContactedPersonId As HiddenField
            Dim index As Integer = Convert.ToInt32(e.NewEditIndex)
            Dim selectedRow As GridViewRow = DirectCast(gvCLOSEPERSONDETAILS.Rows(index), GridViewRow)
            lblCTDID = selectedRow.FindControl("lblCTDID")
            lblCategory = selectedRow.FindControl("lblCategory")
            lblContactedPerson = selectedRow.FindControl("lblContactedPerson")
            hfContactedPersonId = selectedRow.FindControl("hfContactedPersonId")
            lblDTofContact = selectedRow.FindControl("lblDTofContact")
            lblQuarStDT = selectedRow.FindControl("lblQuarStDT")
            lblQuarEndDT = selectedRow.FindControl("lblQuarEndDT")
            lblInflueza = selectedRow.FindControl("lblInflueza")
            lblRemarks = selectedRow.FindControl("lblRemarks")
            lblMobileNo = selectedRow.FindControl("lblMobileNo")
            If rbtnStaffStudent.SelectedValue <> "Staff" Then
                ddlCloseStudentCategory.SelectedValue = lblCategory.Text
                ddlCloseStudentCategory_SelectedIndexChanged(Nothing, Nothing)
                If lblCategory.Text = "SELF" Or lblCategory.Text = "OTH" Then
                    txtInfectedPerson.Text = lblContactedPerson.Text
                ElseIf lblCategory.Text = "PAR" Then
                    'Dim tt As Boolean = Convert.ToBoolean(ddlCloseContactPerson.Items.FindByText(lblInfectedPerson.Text).Text)
                    Dim avaialbeItem As ListItem = ddlCloseContactPerson.Items.FindByText(lblContactedPerson.Text)
                    'If ddlInfectedPerson.Items.Contains(New ListItem(lblInfectedPerson.Text.ToString())) Then
                    If avaialbeItem IsNot Nothing Then
                        ddlCloseContactPerson.Items.FindByText(lblContactedPerson.Text).Selected = True
                        txtCloseContactOther.Visible = False
                        'ddlInfectedPerson.SelectedItem.Value = hfInfectedPersonId.Value
                    Else txtCloseContactOther.Text = lblContactedPerson.Text
                        txtCloseContactOther.Visible = True
                        ddlCloseContactPerson.Items.FindByText("Other").Selected = True
                    End If
                ElseIf lblCategory.Text = "SIB" Then
                    ddlCloseContactPerson.SelectedValue = hfContactedPersonId.Value
                ElseIf lblCategory.Text = "OTHSTAFF" Or lblCategory.Text = "OTHSTUD" Then
                    h_OtherStaffNoStudentNo.Value = hfContactedPersonId.Value
                    txtOtherStaffNoStudentNo.Text = lblContactedPerson.Text
                End If
            Else
                ddlCloseStaffCategory.SelectedValue = lblCategory.Text
                ddlCloseStaffCategory_SelectedIndexChanged(Nothing, Nothing)
                If lblCategory.Text = "SELF" Or lblCategory.Text = "OTH" Then
                    txtCloseContactPerson.Text = lblContactedPerson.Text
                ElseIf lblCategory.Text = "DEP" Then
                    ddlCloseContactPerson.SelectedValue = hfContactedPersonId.Value
                ElseIf lblCategory.Text = "OTHSTAFF" Or lblCategory.Text = "OTHSTUD" Then
                    h_OtherStaffNoStudentNo.Value = hfContactedPersonId.Value
                    txtOtherStaffNoStudentNo.Text = lblContactedPerson.Text
                End If
            End If
            txtDTContactPositivecase.Text = lblDTofContact.Text
            txtStDTQuarantine.Text = lblQuarStDT.Text
            txtEndDTQuarantine.Text = lblQuarEndDT.Text
            txtCloseContactRemarks.Text = lblRemarks.Text
            ddlCloseInfluenzaSymptoms.ClearSelection()
            If lblInflueza.Text = "No" Then
                ddlCloseInfluenzaSymptoms.Items.FindByText("No").Selected = True
            Else ddlCloseInfluenzaSymptoms.Items.FindByText("Yes").Selected = True
            End If
            'ddlCloseInfluenzaSymptoms.SelectedItem.Value = lblInflueza.Text
            txtMobileNo.Text = lblMobileNo.Text
            h_CTD_ID.Value = lblCTDID.Text
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub DeleteRowCloseContactDetails(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim lblTid As New Label
        lblTid = TryCast(gvCLOSEPERSONDETAILS.Rows(pId).FindControl("lblCTDID"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Dim id As Integer = 1000
            id = Save_Positive_Track_Close_Contact_details(conn, transaction, Convert.ToInt32(ViewState("CTH_ID")), uID, True)
            If id = 0 Then
                transaction.Commit()
                lblError.Text = "Deleted successfully."
                Get_Positive_Track_Close_Contact_Details()
            Else transaction.Rollback()
                lblError.Text = "Unable to delete the record."
            End If
        End Using
        'For iRemove = 0 To Session("CLOSEPERSONDETAILS").Rows.Count - 1
        '    If (Session("CLOSEPERSONDETAILS").Rows(iRemove)("UniqueID") = uID) Then
        '        Session("CLOSEPERSONDETAILS").Rows(iRemove)("Status") = "DELETED"
        '        'If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, vwDocuments) = 0 Then
        '        GridBindClosecontactDetails()
        '        'Else
        '        '    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        '        'End If
        '        Exit For
        '    End If
        'Next
    End Sub

    Private Sub GridBindClosecontactDetails()
        'gvCLOSEPERSONDETAILS.DataSource = Session("CLOSEPERSONDETAILS")
        If Session("CLOSEPERSONDETAILS") Is Nothing Then
            gvCLOSEPERSONDETAILS.DataSource = Nothing
            gvCLOSEPERSONDETAILS.DataBind()
            Return
        End If
        Dim strColumnName As String = String.Empty
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateCloseContactDetailsTable()
        If Session("CLOSEPERSONDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("CLOSEPERSONDETAILS").Rows.Count - 1
                If (Session("CLOSEPERSONDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("CLOSEPERSONDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("CLOSEPERSONDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvCLOSEPERSONDETAILS.DataSource = dtTempDtl
        gvCLOSEPERSONDETAILS.DataBind()
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub h_OtherStaffNoStudentNo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_OtherStaffNoStudentNo.ValueChanged
        If ddlCloseStaffCategory.SelectedValue = "OTHSTUD" Or ddlCloseStudentCategory.SelectedValue = "OTHSTUD" Then
            Dim str As String()
            str = h_OtherStaffNoStudentNo.Value.Split("||")
            If str.Length <= 1 AndAlso str.Length > 0 Then
                Dim str_Sql As String = "select  STU_PASPRTNAME from STUDENT_M where stu_id=" & h_OtherStaffNoStudentNo.Value
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
                If ds.Tables.Count > 0 Then
                    txtOtherStaffNoStudentNo.Text = ds.Tables(0).Rows(0)("STU_PASPRTNAME").ToString
                End If
            End If
        End If
    End Sub
End Class
