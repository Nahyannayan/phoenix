﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports UtilityObj
'Version            Date            Author          Change
'1.1                06-sep-2011     Swapna          Screen to export payslips
Partial Class Payroll_PaySlips
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String
    Dim cryRpt As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                lblSTSMsg.Visible = False

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                'lblError.Text = "No Records with specified condition"
                usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
            Else
                'lblError.Text = ""
            End If
            GetLoggedInEmployee()
            If Not IsPostBack Then
                FillPayYearPayMonth()
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                DisableMonths()
            End If
            'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerate)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnJan)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnFeb)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnMar)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnApr)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnMay)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnJun)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnJul)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnAug)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSep)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnOct)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnNov)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDec)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub GetLoggedInEmployee()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT " _
         & " EMP_ID,EMP_BSU_ID ,ISNULL(EM.EMP_FNAME,'')+' '+" _
         & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
         & " AS EMP_NAME, EMPNO FROM dbo.EMPLOYEE_M EM WITH(NOLOCK) INNER JOIN dbo.USERS_M WITH(NOLOCK)" _
         & " ON EM.EMP_ID = USERS_M.USR_EMP_ID" _
         & " WHERE USERS_M.USR_NAME = '" & Session("sUsr_name") & "'" ' and em.emp_bsu_id ='" & Session("sBsuid") & "'"
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
            h_EmpNoDesc.Value = ds.Tables(0).Rows(0)("EMPNO")

            If ds.Tables(0).Rows(0)("EMP_BSU_ID") <> "900500" Or ds.Tables(0).Rows(0)("EMP_BSU_ID") <> "900501" Or ds.Tables(0).Rows(0)("EMP_BSU_ID") <> "900510" Then
                If Session("sBsuid") = "900510" Or Session("sBsuid") = "900500" Or Session("sBsuid") = "900501" Then
                    lblSTSMsg.Visible = True
                End If
            Else
                lblSTSMsg.Visible = False
            End If
        Else
            usrMessageBar.ShowNotification("User cannot be identified.Pls try to login once again.", UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Text = "User cannot be identified.Pls try to login once again."
            Exit Sub
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub FillPayYearPayMonth()

        'Dim lst(12) As System.Web.UI.WebControls.ListItem
        'lst(0) = New System.Web.UI.WebControls.ListItem("January", 1)
        'lst(1) = New System.Web.UI.WebControls.ListItem("February", 2)
        'lst(2) = New System.Web.UI.WebControls.ListItem("March", 3)
        'lst(3) = New System.Web.UI.WebControls.ListItem("April", 4)
        'lst(4) = New System.Web.UI.WebControls.ListItem("May", 5)
        'lst(5) = New System.Web.UI.WebControls.ListItem("June", 6)
        'lst(6) = New System.Web.UI.WebControls.ListItem("July", 7)
        'lst(7) = New System.Web.UI.WebControls.ListItem("August", 8)
        'lst(8) = New System.Web.UI.WebControls.ListItem("September", 9)
        'lst(9) = New System.Web.UI.WebControls.ListItem("October", 10)
        'lst(10) = New System.Web.UI.WebControls.ListItem("November", 11)
        'lst(11) = New System.Web.UI.WebControls.ListItem("December", 12)
        'For i As Integer = 0 To 11
        '    ddlPayMonth.Items.Add(lst(i))
        'Next

    
        Dim iyear As Integer = Session("BSU_PAYYEAR")
        Dim dttbl As New DataTable
        dttbl.Columns.Add("Years")

        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
            dttbl.Rows.Add(i.ToString())
        Next


        'monthRepeater.DataSource = dttbl
        'monthRepeater.DataBind()

        ' ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        ddlPayYear.SelectedValue = iyear 'Session("BSU_PAYYEAR")
        '''''''
        'Try
        '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        '    Dim str_Sql As String
        '    str_Sql = "SELECT BSU_ID,BSU_FREEZEDT,  BSU_PAYMONTH, BSU_PAYYEAR" _
        '        & " FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID = '" & Session("sBsuid") & "'"

        '    Dim ds As New DataSet
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
        '    Else
        '    End If
        'Catch ex As Exception

        'End Try

    End Sub
    Private Sub OpenFileFromDB(ByVal DocId As String)

        Dim conn As String = ""
        conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
        Dim objcon As SqlConnection = New SqlConnection(conn)
        Try

            objcon.Open()
            Dim cmd As New SqlCommand("OpenDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)
            cmd.Parameters.AddWithValue("@Doc_type", "PAYSLIP")
            cmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid").ToString)

            Dim myReader As SqlDataReader = cmd.ExecuteReader()

            If myReader.Read Then
                Response.ClearHeaders()
                Response.ContentType = myReader("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(myReader("DOC_DOCUMENT"), Byte())
                'Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.AddHeader("Cache-control", "no-cache")
                Response.AddHeader("content-disposition", "attachment;filename=" & myReader("DOC_DOCNO").ToString())
                'Response.AddHeader("Pragma", "public")
                'Response.AddHeader("Cache-Control:max-age", "0")
                'Response.Clear()
                Response.BinaryWrite(bytes)
                'Response.Flush()
                'Response.Close()
                Response.End()

            End If
            myReader.Close()

        Catch ex As Exception
            'lblError.Text = ex.Message
            'lblError.Text = UtilityObj.getErrorMessage("1000")
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            objcon.Close()
        End Try

    End Sub

    Private Function GeneratePayslipPDF(ByVal paymonth As Integer) As Integer
        GeneratePayslipPDF = 0
        Dim ESD_IDs As String = String.Empty
        Dim EMAIL_ID As String = String.Empty
        Dim ESD_BSUID As String = String.Empty
        Dim EML_ID As String = String.Empty
        Dim str_Sql As String
        Dim pdfFile As String
        Dim strESD_ID As String = String.Empty
        Try
            Dim str_conn As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim objConn As SqlConnection = New SqlConnection(str_conn)

            pdfFile = System.Configuration.ConfigurationManager.AppSettings("TempFileFolder").ToString()
            If Not Directory.Exists(pdfFile & "\SalaryPayslip") Then
                Directory.CreateDirectory(pdfFile & "\SalaryPayslip")
            End If
            pdfFile = pdfFile & "SalaryPayslip\"
            Dim abc As String = pdfFile & paymonth & ddlPayYear.SelectedValue & "_" & h_EmpNoDesc.Value & ".pdf"
            Dim abc2 As String = pdfFile & paymonth & ddlPayYear.SelectedValue & "_" & h_EmpNoDesc.Value & "Copy.pdf"
            Dim filePath As String = HttpContext.Current.Server.MapPath("~/PAYROLL/REPORTS/RPT/rptEmailSalarySlip.rpt") '-- System.Configuration.ConfigurationManager.AppSettings("salaryReportPath").ToString


            ' Dim abc As String = pdfFile & "_" & strESD_ID & ".pdf"
            ESD_IDs = strESD_ID
            str_Sql = "select DISTINCT ESD_ID,ESD_BSU_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE isnull(esd_paid,0)=1 and Isnull(esd_bposted,0)=1 and esd_month = " & paymonth & " and esd_year= " & ddlPayYear.SelectedValue & _
            " and esd_emp_id= " & h_Emp_No.Value
            '& dRow.Item("ESD_ID")
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            ESD_BSUID = Session("sBsuid")
            Dim comma As String = String.Empty
            While (dr.Read())
                ESD_IDs += comma + dr(0).ToString
                comma = ","
            End While
            If ESD_IDs = "" Then
                'lblError.Text = "No payslip available for selected period!"
                usrMessageBar.ShowNotification("No payslip available for selected period!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Function
            Else
                'lblError.Text = ""

            End If
            If ESD_BSUID = "900510" Or ESD_BSUID = "900500" Or ESD_BSUID = "900501" Then
                filePath = HttpContext.Current.Server.MapPath("~/PAYROLL/REPORTS/RPT/rptEmailSalarySlip_STS.rpt")

            End If
            cryRpt.Load(filePath)

            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New  _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = pdfFile
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            str_Sql = "SELECT * FROM vw_OSO_EMPSALARYHEADERDETAILS WHERE ESD_ID  in (" & ESD_IDs & ")"

            Dim dsMain As New DataSet
            dsMain = SqlHelper.ExecuteDataset(objConn, CommandType.Text, str_Sql)
            If Not dsMain Is Nothing And dsMain.Tables(0).Rows.Count > 0 Then
                cryRpt.SetDataSource(dsMain.Tables(0))
            End If

            Dim ds1 As New DataSet
            str_Sql = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='D' AND ESS_ESD_ID   in (" & ESD_IDs & ")" ' (" & ESD_IDs & ")"
            ds1 = SqlHelper.ExecuteDataset(objConn, CommandType.Text, str_Sql)
            If Not ds1 Is Nothing And ds1.Tables.Count > 0 Then
                cryRpt.Subreports(0).SetDataSource(ds1.Tables(0))
            End If

            Dim ds2 As New DataSet
            str_Sql = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='E' AND ESS_ESD_ID   in (" & ESD_IDs & ")" '(" & ESD_IDs & ")"
            ds2 = SqlHelper.ExecuteDataset(objConn, CommandType.Text, str_Sql)
            If Not ds2 Is Nothing And ds2.Tables.Count > 0 Then
                cryRpt.Subreports(1).SetDataSource(ds2.Tables(0))
            End If
            Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
            myConnectionInfo.ServerName = Web.Configuration.WebConfigurationManager.AppSettings("InstanceName").ToString()
            myConnectionInfo.DatabaseName = "OASIS"
            myConnectionInfo.UserID = Web.Configuration.WebConfigurationManager.AppSettings("UserName").ToString()
            myConnectionInfo.Password = Web.Configuration.WebConfigurationManager.AppSettings("Password").ToString()

            Dim ds3 As New DataSet
            'str_Sql = "exec [dbo].[getBsuInFoWithImage_PaySlip]  '" & ESD_BSUID & "','logo' "
            str_Sql = "exec [dbo].[getBsuInFoWithImage_PaySlip] '" & Session("sBsuid") & "','logo' "
            ds3 = SqlHelper.ExecuteDataset(objConn, CommandType.Text, str_Sql)
            If Not ds3 Is Nothing And ds3.Tables.Count > 0 Then
                'cryRpt.Subreports(2).SetDataSource(ds3.Tables(0))
                cryRpt.Subreports(2).SetDatabaseLogon(myConnectionInfo.UserID, myConnectionInfo.Password, myConnectionInfo.ServerName, myConnectionInfo.DatabaseName)
            End If


            cryRpt.SetParameterValue("userName", "Gems Education")
            cryRpt.SetParameterValue("bMultiMonth", False)

            'cryRpt.SetParameterValue("userName", "System Generated")
            'cryRpt.SetParameterValue("bMultiMonth", False)

            Dim abc1 As String = ""

            Dim pdfPassword As String = ""
            Dim pwdDS As New DataSet
            str_Sql = "getEmployeeSalarySlipPassword '" & h_Emp_No.Value & "','" & ESD_IDs & "'"
            pwdDS = SqlHelper.ExecuteDataset(objConn, CommandType.Text, str_Sql)
            If Not pwdDS Is Nothing And pwdDS.Tables(0).Rows.Count > 0 Then
                pdfPassword = pwdDS.Tables(0).Rows(0).Item(0).ToString
                Dim passwordEncr As New Encryption64
                If pdfPassword <> "" Then
                    pdfPassword = passwordEncr.Decrypt(pdfPassword).Replace(" ", "+")
                End If
            End If


            If pdfPassword <> "" Then
                Dim SourceStream As Stream
                SourceStream = cryRpt.ExportToStream(ExportFormatType.PortableDocFormat)
                AddPasswordToPDF(SourceStream, abc, pdfPassword)
                SourceStream.Dispose()
                ' System.IO.File.Copy(abc, abc2)
                'File.Open(abc2, FileMode.Open)
                'System.Diagnostics.Process.Start(abc)

            Else
                '' abc= ""

                'Dim cryRpt1 As New ReportDocument
                'pdfFile = System.Configuration.ConfigurationManager.AppSettings("TempFileFolder").ToString()
                'If Not Directory.Exists(pdfFile & "\SalaryPayslip") Then
                '    Directory.CreateDirectory(pdfFile & "\SalaryPayslip")
                'End If
                'pdfFile = pdfFile & "SalaryPayslip\"
                'abc1 = pdfFile & paymonth & ddlPayYear.SelectedValue & "_" & h_EmpNoDesc.Value & ".pdf"
                'Dim filePath1 As String = HttpContext.Current.Server.MapPath("~/PAYROLL/REPORTS/RPT/Test.rpt")

                'cryRpt1.Load(filePath1)

                'Dim CrExportOptions1 As ExportOptions
                'Dim CrDiskFileDestinationOptions1 As New  _
                'DiskFileDestinationOptions()
                'Dim CrFormatTypeOptions1 As New PdfRtfWordFormatOptions
                'CrDiskFileDestinationOptions1.DiskFileName = pdfFile
                'CrExportOptions1 = cryRpt.ExportOptions
                'With CrExportOptions1
                '    .ExportDestinationType = ExportDestinationType.DiskFile
                '    .ExportFormatType = ExportFormatType.PortableDocFormat
                '    .DestinationOptions = CrDiskFileDestinationOptions1
                '    .FormatOptions = CrFormatTypeOptions1
                'End With
                cryRpt.ExportToDisk(ExportFormatType.PortableDocFormat, abc)


            End If
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Payslip", Session("sUsr_name"), "Insert", _
                                           Page.User.Identity.Name.ToString, Me.Page, "Payslip Generated for-" & h_EmpNoDesc.Value & "-" & paymonth & "-" & ddlPayYear.SelectedValue)


            'End If
            Dim bytes() As Byte = File.ReadAllBytes(abc)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(abc))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.Close()
            System.IO.File.Delete(abc) 'Commented to keep pdf files for future reference
            'HttpContext.Current.Response.ContentType = "application/pdf"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(abc))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(abc)
            ''HttpContext.Current.Response.Flush()
            ''HttpContext.Current.Response.Close()
            'HttpContext.Current.Response.End()

            cryRpt.Close()
            cryRpt.Dispose()




            'System.IO.File.Delete(abc2)
            Return 0
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Payslip01")
            Return 1000
        Finally
            cryRpt.Close()
            cryRpt.Dispose()
        End Try
    End Function
    'Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
    '    Dim ds As DataSet
    '    Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
    '    Dim Sql_Query = "select top 1 ESD_ID from dbo.EMPSALARYDATA_D INNER JOIN EMPLOYEE_M ON EMP_ID=ESD_EMP_ID INNER JOIN USERS_M ON USR_EMP_ID=EMP_ID where EMP_BSU_ID = '" & Session("sBsuid") & _
    '    "' AND USERS_M.USR_NAME ='" & Session("sUsr_name") & "' AND ESD_MONTH=" & ddlPayMonth.SelectedValue & " AND ESD_YEAR=" & ddlPayYear.SelectedValue
    '    ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
    '    Dim strESD_id As String = ""
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        strESD_id = ds.Tables(0).Rows(0).Item(0).ToString()
    '    Else
    '        lblError.Text = "Salary not processed for selected period"
    '        ' Exit Sub
    '    End If

    '    'If strESD_id = "" Then
    '    '    lblError.Text = "Salary for selected month is not processed yet."
    '    'End If
    '    str_conn2 = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
    '    Sql_Query = "select DOC_ID from Fin.FDOCUMENTS where DOC_BSU_ID = '" & Session("sBsuid") & _
    '    "' AND DOC_PCD_ID ='" & strESD_id & "' AND DOC_TYPE='PS '"
    '    ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
    '    Dim strDoc_id As String = ""
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        strDoc_id = ds.Tables(0).Rows(0).Item(0).ToString()
    '    End If
    '    'If strDoc_id <> "" Then
    '    ' OpenFileFromDB("35")

    '    GeneratePayslipPDF(35)
    '    ' End If

    'End Sub

    Private Shared Sub AddPasswordToPDF(ByVal sourceStream As System.IO.Stream, ByVal outputFile As String, ByVal password As String)
        Try
            Try
                Dim fsStream As New FileStream(outputFile, FileMode.Create)
                fsStream.Close()
                Dim pReader As PdfReader = New PdfReader(sourceStream)
                PdfEncryptor.Encrypt(pReader, New FileStream(outputFile, FileMode.Open), PdfWriter.STRENGTH128BITS, password, password, PdfWriter.AllowCopy)
            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    
    Protected Sub lnkbackbtn_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("../ESSDashboard.aspx")
        Response.Redirect(URL)
    End Sub
    
    Protected Sub btnJan_Click(sender As Object, e As EventArgs) Handles btnJan.Click
        GeneratePayslipPDF(1)
    End Sub

    Protected Sub btnFeb_Click(sender As Object, e As EventArgs) Handles btnFeb.Click
        GeneratePayslipPDF(2)
    End Sub

    Protected Sub btnMar_Click(sender As Object, e As EventArgs) Handles btnMar.Click
        GeneratePayslipPDF(3)
    End Sub

    Protected Sub btnApr_Click(sender As Object, e As EventArgs) Handles btnApr.Click
        GeneratePayslipPDF(4)
    End Sub

    Protected Sub btnMay_Click(sender As Object, e As EventArgs) Handles btnMay.Click
        GeneratePayslipPDF(5)
    End Sub

    Protected Sub btnJun_Click(sender As Object, e As EventArgs) Handles btnJun.Click
        GeneratePayslipPDF(6)
    End Sub

    Protected Sub btnJul_Click(sender As Object, e As EventArgs) Handles btnJul.Click
        GeneratePayslipPDF(7)
    End Sub

    Protected Sub btnAug_Click(sender As Object, e As EventArgs) Handles btnAug.Click
        GeneratePayslipPDF(8)
    End Sub

    Protected Sub btnSep_Click(sender As Object, e As EventArgs) Handles btnSep.Click
        GeneratePayslipPDF(9)
    End Sub

    Protected Sub btnOct_Click(sender As Object, e As EventArgs) Handles btnOct.Click
        GeneratePayslipPDF(10)
    End Sub

    Protected Sub btnNov_Click(sender As Object, e As EventArgs) Handles btnNov.Click
        GeneratePayslipPDF(11)
    End Sub

    Protected Sub btnDec_Click(sender As Object, e As EventArgs) Handles btnDec.Click
        GeneratePayslipPDF(12)
    End Sub

    Private Sub DisableMonths()
        Dim CurrYear As Integer = Date.Now.Year
        Dim CurrMonth As Integer = Date.Now.Month

        If ddlPayYear.SelectedValue = CurrYear Then
            Select Case CurrMonth
                Case 1
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 2
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 3
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 4
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 5
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 6
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 7
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 8
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalEnabled")

                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 9
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 10
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 11
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
                Case 12
                    iJan.Attributes.Remove("class")
                    iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iFeb.Attributes.Remove("class")
                    iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMar.Attributes.Remove("class")
                    iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iApr.Attributes.Remove("class")
                    iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iMay.Attributes.Remove("class")
                    iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJun.Attributes.Remove("class")
                    iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iJul.Attributes.Remove("class")
                    iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iAug.Attributes.Remove("class")
                    iAug.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iSep.Attributes.Remove("class")
                    iSep.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iOct.Attributes.Remove("class")
                    iOct.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iNov.Attributes.Remove("class")
                    iNov.Attributes.Add("class", "fa fa-calendar CalEnabled")
                    iDec.Attributes.Remove("class")
                    iDec.Attributes.Add("class", "fa fa-calendar CalEnabled")
            End Select
        ElseIf ddlPayYear.SelectedValue > CurrYear Then
            iJan.Attributes.Remove("class")
            iJan.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iFeb.Attributes.Remove("class")
            iFeb.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iMar.Attributes.Remove("class")
            iMar.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iApr.Attributes.Remove("class")
            iApr.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iMay.Attributes.Remove("class")
            iMay.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iJun.Attributes.Remove("class")
            iJun.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iJul.Attributes.Remove("class")
            iJul.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iAug.Attributes.Remove("class")
            iAug.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iSep.Attributes.Remove("class")
            iSep.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iOct.Attributes.Remove("class")
            iOct.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iNov.Attributes.Remove("class")
            iNov.Attributes.Add("class", "fa fa-calendar CalDisabled")
            iDec.Attributes.Remove("class")
            iDec.Attributes.Add("class", "fa fa-calendar CalDisabled")
        Else
            iJan.Attributes.Remove("class")
            iJan.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iFeb.Attributes.Remove("class")
            iFeb.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iMar.Attributes.Remove("class")
            iMar.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iApr.Attributes.Remove("class")
            iApr.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iMay.Attributes.Remove("class")
            iMay.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iJun.Attributes.Remove("class")
            iJun.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iJul.Attributes.Remove("class")
            iJul.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iAug.Attributes.Remove("class")
            iAug.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iSep.Attributes.Remove("class")
            iSep.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iOct.Attributes.Remove("class")
            iOct.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iNov.Attributes.Remove("class")
            iNov.Attributes.Add("class", "fa fa-calendar CalEnabled")
            iDec.Attributes.Remove("class")
            iDec.Attributes.Add("class", "fa fa-calendar CalEnabled")
        End If
    End Sub

    Protected Sub ddlPayYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPayYear.SelectedIndexChanged
        DisableMonths()
    End Sub
End Class
