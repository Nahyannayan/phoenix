<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empLeaveAdj.aspx.vb" Inherits="Payroll_empLeaveAdj" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getEmpID(mode) {
            //var sFeatures;
            //sFeatures = "dialogWidth: 445px; ";
            //sFeatures += "dialogHeight: 310px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'EN') {
                result =radopen(url, "pop_up");

                <%--if (result == '' || result == undefined)
                { return false; }

                NameandCode = result.split('___');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];--%>
            }
        }


        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Employee Leave Adjustment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="4" valign="middle">Employee Leave Adjustment</td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Employee Name </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp_Name" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClientClick="getEmpID('EN');return false;"
                                             />
                                        <asp:RequiredFieldValidator ID="rfvEmpName" runat="server" ControlToValidate="txtEmpName"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Employee name required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Pay Month </span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPayMonth" runat="server">
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <span class="field-label">Pay Year</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPayYear" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">From Date </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom_date" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrom_date"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter From  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDateFrom" runat="server" CssClass="error" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="From Date entered is not a valid date" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvFrom_Date" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left">
                                        <span class="field-label">To Date </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo_date" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnTo_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTo_date"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter To  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvTodate" runat="server" CssClass="error" Display="Dynamic"
                                            EnableViewState="False" ErrorMessage="To Date entered is not a valid date and must be greater than or equal to From date"
                                            ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="To Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator><br />
                                        <asp:LinkButton ID="LinkButton1" runat="server">View History</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Leave Type </span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLtype" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <span class="field-label">Adjustment Type </span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAdjType" runat="server">
                                            <asp:ListItem Value="0">Addition</asp:ListItem>
                                            <asp:ListItem Value="1">Deduction</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Remark </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ControlToValidate="txtRemark"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Remark can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">Employee Leave History</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR" />
                                                <asp:BoundField DataField="MonthYear" HeaderText="Month Year" ReadOnly="True" SortExpression="MonthYear" />
                                                <asp:BoundField DataField="Days" HeaderText="Days" ReadOnly="True" SortExpression="Days" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%-- <tr>
            <td   valign="bottom" style="height: 1px">
            </td>
        </tr>--%>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="hfEmp_ID" runat="server" />

                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnFrom_date" TargetControlID="txtFrom_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFrom_date" TargetControlID="txtFrom_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="DocDateto" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgbtnTo_date" TargetControlID="txtTo_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderto" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtTo_date" TargetControlID="txtTo_date">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

