Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_accccViewCreditcardReceipt
    Inherits System.Web.UI.Page
     
    Dim Encr_decrData As New Encryption64
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            hlAddNew.NavigateUrl = "empMonthlyE_D.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
           
            h_Grid.Value = "top"
            lblError.Text = ""
            ''''' 
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'Or _
            '            (ViewState("MainMnu_code") <> "P050090")
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, _
                ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("MainMnu_code") = "P130020" Then
                    lblHeader.Text = "MONTHLY DEDUCTION"
                    ViewState("type") = "D"
                ElseIf ViewState("MainMnu_code") = "P130025" Then
                    lblHeader.Text = "MONTHLY EARNINGS"
                    ViewState("type") = "E"
                End If
                gvEMPMONTHLYDEDUCTIONS.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
                If Request.QueryString("deleted") <> "" Then
                    lblError.Text = getErrorMessage("520")
                End If
            End If
        End If
    End Sub
    'ts
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvEMPMONTHLYDEDUCTIONS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Protected Sub gvEMPDETAILS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPMONTHLYDEDUCTIONS.PageIndexChanging
        gvEMPMONTHLYDEDUCTIONS.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvEMPDETAILS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPMONTHLYDEDUCTIONS.RowDataBound
        Try
            Dim lblEMPID, lblEMPMONTH, lblEMPYEAR As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            lblEMPMONTH = TryCast(e.Row.FindControl("lblMONTH"), Label)
            lblEMPYEAR = TryCast(e.Row.FindControl("lblYEAR"), Label)
            Dim cmdCol As Integer = gvEMPMONTHLYDEDUCTIONS.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing And lblEMPMONTH IsNot Nothing And lblEMPYEAR IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Session("EMP_E_D_MONTH") = Encr_decrData.Encrypt(lblEMPMONTH.Text)
                Session("EMP_E_D_YEAR") = lblEMPYEAR.Text
                hlview.NavigateUrl = "empMonthlyE_D.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & _
                Request.QueryString("MainMnu_code") & "&month=" & Encr_decrData.Encrypt(lblEMPMONTH.Text) & "&year=" & _
                Encr_decrData.Encrypt(lblEMPYEAR.Text) & "&datamode=" & ViewState("datamode")
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrEmpMONTH As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrYEAR As String = String.Empty
        Dim lstrTYPE As String = String.Empty
        Dim lstrAMOUNT As String = String.Empty
        Dim lstrREMARKS As String = String.Empty

        Dim lstrFiltMONTH As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltYEAR As String = String.Empty
        Dim lstrFiltTYPE As String = String.Empty
        Dim lstrFiltAMOUNT As String = String.Empty
        Dim lstrFiltREMARKS As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        If gvEMPMONTHLYDEDUCTIONS.Rows.Count > 0 Then
            ' --- Initialize The Variables


            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtMONTH")
            lstrEmpMONTH = Trim(txtSearch.Text)
            If (lstrEmpMONTH <> "") Then lstrFiltMONTH = SetCondn(lstrOpr, "EMP_MONTH", lstrEmpMONTH)

            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "empno", lstrEMPNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtYEAR")
            lstrYEAR = txtSearch.Text
            If (lstrYEAR <> "") Then lstrFiltYEAR = SetCondn(lstrOpr, "EDD_PAYYEAR", lstrYEAR)

            '   -- 5  COLLUN
            'larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            'lstrOpr = larrSearchOpr(0)
            'txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtDedType")
            'lstrTYPE = txtSearch.Text
            'If (lstrTYPE <> "") Then lstrFiltTYPE = SetCondn(lstrOpr, "ERN_DESCR", lstrTYPE)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtAMOUNT")
            lstrAMOUNT = txtSearch.Text
            If (lstrAMOUNT <> "") Then lstrFiltAMOUNT = SetCondn(lstrOpr, "EDD_AMOUNT", lstrAMOUNT)

            '   -- 5  COLLUN
            'larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            'lstrOpr = larrSearchOpr(0)
            'txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtREMARKS")
            'lstrREMARKS = txtSearch.Text
            'If (lstrREMARKS <> "") Then lstrFiltREMARKS = SetCondn(lstrOpr, "EDD_REMARKS", lstrREMARKS)

        End If
        str_Sql = " SELECT EDD_EMP_ID,EMPNO, EMPNAME, EMP_MONTH, EDD_PAYMONTH, EDD_PAYYEAR, sum(EDD_AMOUNT) as EDD_AMOUNT from vw_OSO_EMPMONTHLY_E_D "
        str_Sql += " WHERE EDD_BSU_ID ='" & Session("sBsuid") & "' and EDD_TYPE='" & ViewState("type") & "' AND isnull(EDD_REFDOCNO,'')='' " & _
        lstrFiltMONTH & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltYEAR & lstrFiltTYPE & lstrFiltAMOUNT & lstrFiltREMARKS & _
        "GROUP BY EMP_MONTH,EDD_PAYMONTH, EDD_PAYYEAR, EDD_EMP_ID ,EMPNO, EMPNAME" & _
        " order by cast('01/'+cast(EDD_PAYMONTH as varchar(4))+'/'+cast(EDD_PAYYEAR as varchar(4)) as datetime)  DESC"

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT," & _
        '" EMP_PASSPORT, EMP_STATUS_DESCR, EMP_DES_DESCR FROM vw_OSO_EMPLOYEEMASTER" & _
        '" WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' " _
        '& str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDesignation & lstrFiltPassportNo

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPMONTHLYDEDUCTIONS.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEMPMONTHLYDEDUCTIONS.DataBind()
            Dim columnCount As Integer = gvEMPMONTHLYDEDUCTIONS.Rows(0).Cells.Count

            gvEMPMONTHLYDEDUCTIONS.Rows(0).Cells.Clear()
            gvEMPMONTHLYDEDUCTIONS.Rows(0).Cells.Add(New TableCell)
            gvEMPMONTHLYDEDUCTIONS.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEMPMONTHLYDEDUCTIONS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEMPMONTHLYDEDUCTIONS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvEMPMONTHLYDEDUCTIONS.DataBind()
        End If

        'gvJournal.DataBind()
        txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtMONTH")
        txtSearch.Text = lstrEmpMONTH

        txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtYEAR")
        txtSearch.Text = lstrYEAR

        'txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtDedTYPE")
        'txtSearch.Text = lstrTYPE

        txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtAMOUNT")
        txtSearch.Text = lstrAMOUNT

        'txtSearch = gvEMPMONTHLYDEDUCTIONS.HeaderRow.FindControl("txtREMARKS")
        'txtSearch.Text = lstrREMARKS

        gvEMPMONTHLYDEDUCTIONS.SelectedIndex = p_selected_id
    End Sub

     

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
