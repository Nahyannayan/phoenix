Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_accccViewCreditcardReceipt
    Inherits System.Web.UI.Page
     
    Dim Encr_decrData As New Encryption64

    Private Enum JobResponsibility
        EMPAllocation = 0
        BSUnit = 1
    End Enum
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            hlAddNew.NavigateUrl = "empJobResponsibility_Alloc.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            h_Grid.Value = "top"
            lblError.Text = ""
            ''''' 
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'Or _
            '            (ViewState("MainMnu_code") <> "P050090")
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, _
                ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
                Select Case ViewState("MainMnu_code").ToString
                    Case "P050105"
                        lblHeader.Text = "JOB RESPONSIBILITY"
                        gvEMPJOBResponsibility.Columns(1).Visible = False
                        gvEMPJOBResponsibility.Columns(2).Visible = False
                        gridbind(JobResponsibility.BSUnit)
                    Case "P130055"
                        lblHeader.Text = "EMPLOYEE JOB RESPONSIBILITY"
                        gridbind(JobResponsibility.EMPAllocation)
                End Select
                gvEMPJOBResponsibility.Attributes.Add("bordercolor", "#1b80b6")
                If Request.QueryString("deleted") <> "" Then
                    lblError.Text = getErrorMessage("520")
                End If
            End If
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvEMPJOBResponsibility.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvEMPJOBResponsibility.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Protected Sub gvEMPDETAILS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPJOBResponsibility.PageIndexChanging
        gvEMPJOBResponsibility.PageIndex = e.NewPageIndex
        gridbind(0)
    End Sub

    Protected Sub gvEMPDETAILS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPJOBResponsibility.RowDataBound
        Try
            Dim lblGUID As Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvEMPJOBResponsibility.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblGUID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = "empJobResponsibility_Alloc.aspx?viewid=" & Encr_decrData.Encrypt(lblGUID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal type As JobResponsibility, Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim str_Filter As String = String.Empty
        Dim lstrEmpBSUNAME As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrResp As String = String.Empty
        Dim lstrFROMDT As String = String.Empty
        Dim lstrAMOUNT As String = String.Empty
        Dim lstrTODT As String = String.Empty

        Dim lstrFiltBSUNAME As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltResp As String = String.Empty
        Dim lstrFiltFROMDT As String = String.Empty
        Dim lstrFiltAMOUNT As String = String.Empty
        Dim lstrFiltTODT As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        If gvEMPJOBResponsibility.Rows.Count > 0 Then
            ' --- Initialize The Variables


            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtBSUNAME")
            lstrEmpBSUNAME = Trim(txtSearch.Text)
            If (lstrEmpBSUNAME <> "") Then lstrFiltBSUNAME = SetCondn(lstrOpr, "BSU_NAME", lstrEmpBSUNAME)

            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EMPNO", lstrEMPNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtResp")
            lstrResp = txtSearch.Text
            If (lstrResp <> "") Then lstrFiltResp = SetCondn(lstrOpr, "RES_DESCR", lstrResp)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtFromDT")
            lstrFROMDT = txtSearch.Text
            If (lstrFROMDT <> "") Then lstrFiltFROMDT = SetCondn(lstrOpr, "FROMDT", lstrFROMDT)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtAMOUNT")
            lstrAMOUNT = txtSearch.Text
            If (lstrAMOUNT <> "") Then lstrFiltAMOUNT = SetCondn(lstrOpr, "AMOUNT", lstrAMOUNT)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtTODT")
            lstrTODT = txtSearch.Text
            If (lstrTODT <> "") Then lstrFiltTODT = SetCondn(lstrOpr, "TODT", lstrTODT)

        End If
        Select Case type
            Case JobResponsibility.BSUnit
                str_Sql = "SELECT *,'' as EMPNO, '' as EMPNAME from vw_OSO_JOB_RESPONSIBILITY_ALLOC_BSU WHERE BSU_ID ='" & Session("sBsuid") & "' " & _
                lstrFiltBSUNAME & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltResp & lstrFiltFROMDT & lstrFiltAMOUNT & lstrFiltTODT
            Case JobResponsibility.EMPAllocation
                str_Sql = "SELECT * from vw_OSO_JOB_RESPONSIBILITY_ALLOC_EMP WHERE BSU_ID ='" & Session("sBsuid") & "' " & _
                lstrFiltBSUNAME & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltResp & lstrFiltFROMDT & lstrFiltAMOUNT & lstrFiltTODT
        End Select

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT," & _
        '" EMP_PASSPORT, EMP_STATUS_DESCR, EMP_DES_DESCR FROM vw_OSO_EMPLOYEEMASTER" & _
        '" WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' " _
        '& str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDesignation & lstrFiltPassportNo

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPJOBResponsibility.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEMPJOBResponsibility.DataBind()
            Dim columnCount As Integer = gvEMPJOBResponsibility.Rows(0).Cells.Count

            gvEMPJOBResponsibility.Rows(0).Cells.Clear()
            gvEMPJOBResponsibility.Rows(0).Cells.Add(New TableCell)
            gvEMPJOBResponsibility.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEMPJOBResponsibility.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEMPJOBResponsibility.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvEMPJOBResponsibility.DataBind()
        End If

        'gvJournal.DataBind()
        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtBSUNAME")
        txtSearch.Text = lstrEmpBSUNAME

        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtResp")
        txtSearch.Text = lstrResp

        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtFromDT")
        txtSearch.Text = lstrFROMDT

        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtAMOUNT")
        txtSearch.Text = lstrAMOUNT

        txtSearch = gvEMPJOBResponsibility.HeaderRow.FindControl("txtTODT")
        txtSearch.Text = lstrTODT

        gvEMPJOBResponsibility.SelectedIndex = p_selected_id
    End Sub

     

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(0)
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(0)
    End Sub
End Class
