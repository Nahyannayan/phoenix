<%@ Page Language="VB" MasterPageFile="~/mainMasterPageSS.master" AutoEventWireup="false" CodeFile="EOSReports.aspx.vb" Inherits="EOSReports" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   
    <table align="center"  style="width: 80%;" >
       <tr align =left >
           <td>
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
           </td>
       </tr>
    </table>

    <table align="center" runat = "server"  id="tblMain" border="1" 
        bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        style="border-collapse:collapse; width: 80%; height: 36px">
        <tr  class="subheader_img">
            <td align="left" colspan="4" style="height: 1px" valign="middle">                 
                        <asp:Label ID="lblrptCaption" runat="server" Text="Employee Salary Slip"></asp:Label>&nbsp;

            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 87px; height: 18px;">
                <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                </td>
            <td align="left" class="matters" 
                style="width: 152px; text-align: left; height: 18px;">
                <asp:TextBox ID="txtFromDate" runat="server" Width="101px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                    EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" 
                style="width: 54px; color: #1b80b6; height: 18px;">
                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                </td>
            <td align="left" class="matters" style="text-align: left; height: 18px;">
                <asp:TextBox ID="txtToDate" runat="server" Width="101px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="4" style=" text-align: right">
                &nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
       <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
  <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

