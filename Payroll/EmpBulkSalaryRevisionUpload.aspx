<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EmpBulkSalaryRevisionUpload.aspx.vb" Inherits="empBulkSalaryRevisionUpload" title="Untitled Page" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <link href="../cssfiles/Popup.css" rel="stylesheet" />
<script  type = "text/javascript" >



    function GetEMPNAME() {

        var NameandCode;

        var url = "Reports/Aspx/SelIDDESC.aspx?ID=EMP";
        var oWnd = radopen(url, "pop_empname");

    }

    function OnClientClose1(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {

            NameandCode = arg.NameandCode;
            document.getElementById('<%=hf_EMP_IDs.ClientID%>').value = NameandCode;
              document.getElementById('<%=txtEmpNo.ClientID%>').value = NameandCode;
              __doPostBack('<%=txtEmpNo.ClientID%>', 'TextChanged');
          }
      }



      function GetSalaryGrade() {
          var sFeatures;
          sFeatures = "dialogWidth: 590px; ";
          sFeatures += "dialogHeight: 370px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: no; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          var url;
         <%--url='empShowOtherDetail.aspx?id=SG';
         result = window.showModalDialog(url,"", sFeatures);
         if (result=='' || result==undefined)
         {
           return false;
         } 
         else
         {
           NameandCode = result.split('___'); 
           document.getElementById('<%=txtSalGrade.ClientID %>').value=NameandCode[0];
           document.getElementById('<%=hfSalGrade.ClientID %>').value=NameandCode[1]; 
         } --%>

          var url = "empShowOtherDetail.aspx?id=SG";
          var oWnd = radopen(url, "pop_SalaryGrade");

      }

      function OnClientClose2(oWnd, args) {
          //get the transferred arguments

          var arg = args.get_argument();

          if (arg) {

              NameandCode = arg.NameandCode.split('||');
              document.getElementById('<%=txtSalGrade.ClientID %>').value = NameandCode[0];
              document.getElementById('<%=hfSalGrade.ClientID %>').value = NameandCode[1];
              __doPostBack('<%=txtSalGrade.ClientID%>', 'TextChanged');
          }
      }

   <%--function getDate(left,top,txtControl) 
   {
        var sFeatures;
        sFeatures="dialogWidth: 250px; ";
        sFeatures+="dialogHeight: 270px; ";
        sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";

        var NameandCode;
        var result;
        result = window.showModalDialog("calendar.aspx","", sFeatures);
        if(result != '' && result != undefined)
        {
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtRevisionDate.ClientID %>').value=result;
                break;
            }   
        }
        return false;    
   }     --%>

    function ViewDetails(url) {
        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 300px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;

        //result = window.showModalDialog("PopupSalaryCompareDetails.aspx?"+url,"", sFeatures)
        return ShowWindowWithClose("PopupSalaryCompareDetails.aspx?" + url, 'search', '55%', '85%')
        return false;
    }

    function closepopup() {
        jQuery.fancybox.close();
    }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       <Windows>
            <telerik:RadWindow ID="pop_SalaryGrade" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-usd mr-3"></i> Salary Revision Option
        </div>
        <div class="card-body">
            <div class="table-responsive">            

    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALEDITSAL" />

    <table id="tblRevisionOption" runat="server" align="center" width="100%">
         
        <tr>
            <td align="left" >
                <asp:RadioButton ID="optManual" runat="server" CssClass="field-label"  AutoPostBack="True" 
                    GroupName="RevisionOption" Text="Manual" />
                <asp:RadioButton ID="optUpload" runat="server" CssClass="field-label"  AutoPostBack="True" 
                    GroupName="RevisionOption" Text="Upload from Excel" />
            </td>
        </tr>
    </table>

    <table id="Table1" runat="server" align="center" width="100%" >
         <tr>
                <td align="left" class="title-bg" colspan="4"> Salary Revision</td>
            </tr>
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Select Employees</span>
            </td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtEmpNo" runat="server" OnTextChanged="txtEmpNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgEMPSel" runat="server"
                        ImageUrl="~/Images/cal.gif" OnClientClick="GetEMPNAME(); return false;" />
                </td>
                <td align="left" width="20%">
                <span class="field-label">Revision Date </span>
            </td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtRevisionDate" runat="server" Width="116px"></asp:TextBox>
                <asp:ImageButton ID="imgdatepick"
                        runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(550, 310, 0);" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgdatepick" TargetControlID="txtRevisionDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtRevisionDate" TargetControlID="txtRevisionDate">
                            </ajaxToolkit:CalendarExtender>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRevisionDate"
                    ErrorMessage="Revision Date required" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtRevisionDate"
                    EnableViewState="False" ErrorMessage="Enter the Revision Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>        
    </table>
    <table id = "tblSalDetails" runat = "server" align="center" width="100%"> 
             <tr>
                 <td align="left"  class="title-bg" width="50%">
                     Selected Employees</td>
                 <td align="left" width="50%"  class="title-bg">
                 Salary change</td>
             </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td align="left" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                           
                     <asp:GridView ID="gvSalRevisionBulk" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" AutoGenerateColumns="False"
                         PageSize="15" EmptyDataText="No Employees selected" Width="100%">
                         <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                         <Columns>
                             <asp:TemplateField HeaderText="EMP NO">
                                 <EditItemTemplate>
                                     &nbsp;
                                 </EditItemTemplate>
                                 <HeaderTemplate>
                                    Employee No.<br />
                                     <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                                             <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                 OnClick="ImageButton1_Click" />
                                 </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="Center"/>
                                 <ItemTemplate>
                                     <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="EMP NAME">
                                 <EditItemTemplate>
                                     &nbsp;
                                 </EditItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                 </ItemTemplate>
                                 <HeaderTemplate>
                                     Employee Name<br />
                                     <asp:TextBox ID="txtEmpName" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                                             <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                 OnClick="ImageButton1_Click" />
                                 </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="Left"/>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Cur_SalaryScale">
                                 <EditItemTemplate>
                                     &nbsp;
                                 </EditItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="lblCurrSalAmt" runat="server"></asp:Label>
                                 </ItemTemplate>
                                 <HeaderTemplate>
                                     Current Salary<br />
                                     <asp:TextBox ID="txtMonth" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                                             <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                 OnClick="ImageButton1_Click" />
                                 </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                                 <HeaderStyle HorizontalAlign="Center" />
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Revised_SalScale">
                                 <EditItemTemplate>
                                     
                                 </EditItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemTemplate>
                                     <asp:Label ID="lblRevSalAmt" runat="server"></asp:Label>
                                 </ItemTemplate>
                                 <HeaderTemplate>
                                     Revised Salary Amount
                                 </HeaderTemplate>
                                 <HeaderStyle HorizontalAlign="Center" />
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="View" ShowHeader="False">
                                 <ItemTemplate>
                                     <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                     
                                 </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                 <ItemTemplate>
                                     <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                         </Columns>
                         <RowStyle CssClass="griditem"/>
                         <SelectedRowStyle CssClass="griditem_hilight" />
                         <HeaderStyle CssClass="gridheader_pop" />
                         <AlternatingRowStyle CssClass="griditem_alternative" />
                     </asp:GridView>
                     
                      </td>
                        </tr>
                    </table>
                </td>            
            <td align="left" width="100%" valign="top">          
       <table id ="tblSalary" runat="server" width="100%">
           <tr>
               <td align="left" colspan="4">
                  <asp:RadioButton ID="radSalScale" runat="server" CssClass="field-label" Checked="True" GroupName="SalChange"
                        Text="Salary Scale" AutoPostBack="True" />
                    <asp:RadioButton ID="radAddAmount" runat="server" CssClass="field-label" GroupName="SalChange" Text="Add Amount" AutoPostBack="True" /></td>
           </tr>
           <tr id = "trSalScale1">
               <td align="left" class="title-bg" colspan="4">
                   Change Salary Scale</td>
           </tr>
             <tr id = "trSalScale2">
                 <td align="left">
                    <span class="field-label"> Select Salary Scale</span></td>
                 <td align="left">
                     <asp:TextBox ID="txtSalGrade" runat="server" OnTextChanged="txtSalGrade_TextChanged" AutoPostBack="true"></asp:TextBox>
                     <asp:ImageButton
                         ID="btnSalScale" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetSalaryGrade(); return false;"
                          /></td>
             </tr>
             <tr id = "trSalScale3">
                 <td align="left" valign="top" colspan="4">
                    <asp:GridView ID="gvEmpSalary" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." Width="100%" PageSize="5">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <Columns>
                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ENR ID" Visible="False"><ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Earing">
                                <ItemTemplate>
                                    <asp:Label ID="lblERN_DESCR" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount(Eligible)">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountEligible" runat="server" Text='<%#AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount(Actual)">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="bMonthly" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBMonthly" runat="server" Text='<%#bind("bMonthly") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PayTerm" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblPayTerm" runat="server" Text='<%# bind("PAYTERM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Schedule">
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblSchedule" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_new" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                     </td>
             </tr>
             <tr id ="trEditSalAmt">
                 <td align="left" class="title-bg" colspan="4">
                     Edit Amount</td>
             </tr>
             <tr id ="trEditSalAmt1">
                <td align="left" width="30%">
                   <span class="field-label">Earning</span>
                </td>
                
                <td align="left" colspan="3" >
                    <asp:DropDownList ID="ddSalEarnCode" runat="server" ValidationGroup="VALADDNEWSAL">
                    </asp:DropDownList></td>
            </tr>
           <tr id ="trEditSalAmt2">
               <td align="left">
                   <span class="field-label">Amount (Added to Earning)</span></td>
              
               <td align="left">
                    <asp:TextBox ID="txtSalAmount" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSalAmount"
                        ErrorMessage="Enter the Salary Amount" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator><br />
                   <asp:CheckBox ID="chkSalPayMonthly" runat="server" Text="Per Month" />
               </td>
           </tr>
            <tr id ="trEditSalAmt3">
            <td align="center" colspan="4"> <asp:Button ID="btnSalAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALADDNEWSAL" />
                    <asp:Button ID="btnSalCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                </tr>                      
           <tr id ="trEditSalAmt4">
               <td align="center" colspan="4">
                   <asp:GridView ID="gvAddSalaryDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." Width="100%">
                       <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                       <Columns>
                           <asp:TemplateField HeaderText="Unique ID" Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="ENR ID" Visible="False">
                               <ItemStyle HorizontalAlign="Center" />
                               <ItemTemplate>
                                   <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Earing">
                               <ItemTemplate>
                                   <asp:Label ID="lblERN_DESCR" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                               </ItemTemplate>
                               <ItemStyle HorizontalAlign="Left" />
                               <HeaderStyle HorizontalAlign="Center" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Amount(Added)">
                               <ItemStyle HorizontalAlign="Right" />
                               <HeaderStyle HorizontalAlign="Right" />
                               <ItemTemplate>
                                   <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField ShowHeader="False">
                               <HeaderTemplate>
                                   Edit
                               </HeaderTemplate>
                               <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                               <ItemTemplate>
                                   <asp:LinkButton ID="lnkSalEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                       OnClick="lnkSalEdit_Click" Text="Edit"></asp:LinkButton>
                               </ItemTemplate>
                           </asp:TemplateField>
                       </Columns>
                       <RowStyle CssClass="griditem" />
                       <SelectedRowStyle CssClass="griditem_hilight" />
                       <HeaderStyle CssClass="gridheader_new"/>
                       <AlternatingRowStyle CssClass="griditem_alternative" />
                   </asp:GridView>
               </td>
           </tr>
           <tr>
               <td align="center" colspan="3">
                     <span class="field-label">Gross Salary</span>
               </td>
                <td align="left">
                    <asp:TextBox ID="txtGrossSalary" runat="server" ReadOnly="True"></asp:TextBox></td>
           </tr>
            </table> 
                 </td>
             </tr>
            <tr valign="top">
                <td align="center" colspan="2" valign="top" width="100%">
                    <asp:Button ID="btnSalRevSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALEDITSAL" />
                    <asp:Button ID="btnSalRevCancel" runat="server" CssClass="button" Text="Cancel" /></td>
            </tr>
            </table>
    <table id="tblUpload" runat="server" align="center" width="100%" >
         <tr>
            <td align="left" valign="middle" >
                 <span class="field-label">Upload from Excel</span>
                            <asp:FileUpload ID="flUpExcel" runat="server" />
                    <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
               </td>
        </tr>
         <tr>
            <td align="left" valign="middle" >
                   <asp:GridView ID="gvUpload" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" 
                    EmptyDataText="No Transaction details added yet." Width="100%" 
                    EnableModelValidation="True" SkinID="GridViewView" AllowPaging="True">
                       <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                       <Columns>
                           <asp:TemplateField HeaderText="EMP ID" Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblEmpId" runat="server" Text='<%# bind("emp_id") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Emp No">
                               <ItemTemplate>
                                   <asp:Label ID="lblEmpNo" runat="server" Text='<%# bind("empno") %>'></asp:Label>
                               </ItemTemplate>
                               <ItemStyle Width="7%" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Emp Name">
                               <ItemTemplate>
                                   <asp:Label ID="lblEmpName" runat="server" Text='<%# bind("emp_name") %>'></asp:Label>
                               </ItemTemplate>
                               <ItemStyle Width="20%" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Bsu Id" Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblBsuId" runat="server" Text='<%# bind("bsu_id") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Business Unit" Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblBsuName" runat="server" Text='<%# bind("bsu_name") %>'></asp:Label>
                               </ItemTemplate>
                               <ItemStyle Width="15%" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Earning Description">
                               <ItemTemplate>
                                   <asp:Label ID="lblEarningCode" runat="server" 
                                       Text='<%# bind("EarningDesc") %>'></asp:Label>
                               </ItemTemplate>
                               <ItemStyle Width="10%" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Current Amount">
                               <ItemTemplate>
                                   <asp:TextBox ID="txtCurrentAmount" runat="server" ReadOnly="True" style="text-align: right"
                                       Text='<%# bind("current_amount") %>' Width="100px" BackColor="Silver" Enabled="False"></asp:TextBox>
                               </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Right" />
                               <ItemStyle HorizontalAlign="Right" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Eligible Amount">
                               <ItemTemplate>
                                   <asp:TextBox ID="txteligibleAmount" runat="server" ReadOnly="True" style="text-align: right"
                                       Text='<%# bind("eligibleamount") %>' Width="100px"></asp:TextBox>
                               </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Right" />
                               <ItemStyle HorizontalAlign="Right" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Actual Amount">
                               <ItemTemplate>
                                   <asp:TextBox ID="txtActualAmount" runat="server" ReadOnly="True" style="text-align: right"
                                       Text='<%# bind("actualamount") %>' Width="100px"></asp:TextBox>
                               </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Right" />
                               <ItemStyle HorizontalAlign="Right" />
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Effective Date">
                               <ItemTemplate>
                                   <asp:Label ID="lblEffectiveDate" runat="server" 
                                       Text='<%# bind("EffectiveDate") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Applicable Date">
                               <ItemTemplate>
                                   <asp:Label ID="lblFromDate" runat="server" 
                                       text='<%# Eval("fromdate", "{0:MMMM d, yyyy}") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="bActive" Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblbActive" runat="server" Text='<%# bind("bACTIVE") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField>
                               <ItemTemplate>
                                   <asp:HyperLink ID="lnkView" runat="server">Current Salary Details</asp:HyperLink>
                               </ItemTemplate>
                               <ItemStyle HorizontalAlign="Center" />
                           </asp:TemplateField>
                           <asp:TemplateField Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblUploaded" runat="server" Text='<%# bind("bUploaded") %>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Undefined Earning Description" Visible="False">
                               <ItemTemplate>
                                   <asp:Label ID="lblUndefinedEarningCode" runat="server" Text='<%# Bind("EarningCode")%>'></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>
                       </Columns>
                       <RowStyle CssClass="griditem" />
                       <SelectedRowStyle CssClass="griditem_hilight" />
                       <HeaderStyle CssClass="gridheader_new" />
                       <AlternatingRowStyle CssClass="griditem_alternative" />
                   </asp:GridView>
             </td>
        </tr>
         <tr>
            <td align="right" valign="middle" >
                    <asp:Button ID="btnSaveUpload" runat="server" CssClass="button" Text="Save" 
                        UseSubmitBehavior="True" 
                        onclientclick="return confirm('Salary components not included in the upload file will not be effected by this revision process. Are you sure you want to continue?');" />

                 <asp:Button ID="btnSaveUploadToAXandOASIS" runat="server" CssClass="button" Text="Save to AX" 
                        UseSubmitBehavior="True" 
                        onclientclick="return confirm('Salary components not included in the upload file will not be effected by this revision process. Are you sure you want to continue?');" Visible="False" />
         
                    <asp:Button ID="btnCancelUpload" runat="server" CssClass="button" 
                       Text="Cancel" />
             </td>
        </tr>
    </table>

                 </div>
            </div>
         </div>

    <asp:HiddenField ID="hf_EMP_IDs" runat="server" />
    <asp:HiddenField ID="hfSalGrade" runat="server" />
    <input id="h_Grid" runat="server" type="hidden" value="top" /><input id="h_SelectedId"
        runat="server" type="hidden" value="-1" /><input id="h_selected_menu_1" runat="server"
            type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden"
                value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><br />
    <input
                    id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5"
                        runat="server" type="hidden" value="=" /><input id="h_Selected_menu_6" runat="server"
                            type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server" type="hidden"
                                value="=" /><input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />

    

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        </script>

</asp:Content>

