Imports System.Data
Imports system.Web.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Payroll_empSalaryRevision
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    'Version        Author               Date            Purpose
    ''1.1            swapna              27/Feb/2013     To display earning types bsu wise.

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
        If Not IsPostBack Then
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(IIf(Request.QueryString("MainMnu_code") IsNot Nothing, Request.QueryString("MainMnu_code").Replace(" ", "+"), String.Empty))
            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then
                ViewState("Eid") = Encr_decrData.Decrypt(IIf(Request.QueryString("Eid") IsNot Nothing, Request.QueryString("Eid").Replace(" ", "+"), ""))
            End If
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If ViewState("MainMnu_code") <> OASISConstants.MenuEMPSALARYREVISION Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                txtEmpNo.Attributes.Add("ReadOnly", "ReadOnly")
                txtDate.Attributes.Add("onBlur", "checkdate(this)")
                txtSalAmtEligible.Attributes.Add("onblur", "javascript:GetPayableFromEligible();")
                BindENR()
                GetPayMonth_YearDetails()
                BindYearDetails()
            End If
            'Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        End If

        If txtEmpNo.Text <> "" Then 'And txtDate.Text <> "" Then
            'If ViewState("CurrEmpNo") Is Nothing Then
            '    ViewState("CurrEmpNo") = h_Emp_No.Value
            'End If
            'If ViewState("CurrBSUID") Is Nothing Then
            '    ViewState("CurrBSUID") = h_BSU_ID.Value
            'End If
            If ViewState("CurrEmpNo") <> h_Emp_No.Value Then
                ViewState("CurrEmpNo") = h_Emp_No.Value
                tblSalDetails.Visible = True
                'FillSalHistoryDetails(h_Emp_No.Value, h_BSU_ID.Value)
                FillSalHistoryDetails(h_Emp_No.Value)
                FillCurrentSalDetails(h_Emp_No.Value)
                'Else
                '    gvEmpSalary.DataSource = Nothing
                '    gvEmpSalary.DataBind()
                '    gvEmpSalHistory.DataSource = Nothing
                '    gvEmpSalHistory.DataBind()
            End If
        Else
            tblSalDetails.Visible = False
        End If
    End Sub

    Private Sub BindYearDetails()
        Dim yrs As Integer = Session("F_YEAR")
        If Not ViewState("PAYYEAR") Is Nothing Then
            yrs = ViewState("PAYYEAR")
        End If
        For i As Integer = yrs To yrs + 1
            ddSalPayYear.Items.Add(i)
        Next
        ddSalPayMonth.SelectedValue = ViewState("PAYMONTH")
        ddSalPayYear.SelectedValue = ViewState("PAYYEAR")
    End Sub

    Private Sub GetPayMonth_YearDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT BSU_PAYMONTH, BSU_PAYYEAR, BSU_CURRENCY FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            ViewState("PAYMONTH") = dr("BSU_PAYMONTH")
            ViewState("PAYYEAR") = dr("BSU_PAYYEAR")
            ViewState("PAY_CUR_ID") = dr("BSU_CURRENCY")
        End While
    End Sub

    Private Function CheckPayMonth() As Boolean
        Try
            Dim dt As DateTime = New DateTime(ViewState("PAYYEAR"), ViewState("PAYMONTH"), 1)
            Dim dtCompare As DateTime = New DateTime(CDate(txtDate.Text).Year, CDate(txtDate.Text).Month, 1)
            If DateTime.Compare(dtCompare, dt) <= 0 Then
                Return False
            Else
                Return True
            End If
        Catch
            Return False
        End Try
    End Function

    Private Function FillSalHistoryDetails(ByVal empNo As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String = "SELECT  ESH_ID, ESH_EMP_ID, ESH_BSU_ID, ESH_ERN_ID as ENR_ID," & _
        '" ESH_AMOUNT, ESH_FROMDT, ESH_TODT, ESH_CUR_ID, ESH_PAY_CUR_ID, ESH_bMonthly, ESL_PAYTERM, " & _
        '" ESH_PAYMONTH, ESH_PAYYEAR, ESH_bPAID,CASE WHEN isnull(ESH_bMonthly, 1) = 1  " & _
        '"then ESH_AMOUNT/12 else ESH_AMOUNT end as AMOUNT FROM EMPSALARYHS_s" & _
        Dim str_Sql As String = "SELECT EMPSALCOMPO_M.ERN_DESCR AS ENR_ID, EMPSALARYHS_s.ESH_AMOUNT, " & _
        "EMPSALARYHS_s.ESH_FROMDT, EMPSALARYHS_s.ESH_TODT, " & _
        "EMPSALARYHS_s.ESH_bMonthly, CASE WHEN isnull(ESH_bMonthly, 0) = 0 THEN ESH_AMOUNT / 12 ELSE ESH_AMOUNT END AS AMOUNT, " & _
        "EMPSALARYHS_s.ESH_ID FROM EMPSALARYHS_s LEFT OUTER JOIN " & _
        "EMPSALCOMPO_M ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID" & _
        " WHERE ESH_EMP_ID ='" & empNo & "' and ESH_BSU_ID='" & Session("sBSUID") & "'" & _
        " order by ESH_FROMDT DESC, ESH_TODT, ERN_ORDER "
        Dim dt As DataTable = CreateSalaryHistoryTable()
        Dim dr As SqlDataReader
        Dim drGrandTotal As DataRow
        Dim totalamt As Double = 0
        Dim tempString As String = String.Empty
        Dim todate As DateTime = DateTime.MinValue
        dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            Dim curToDate As DateTime = DateTime.MinValue
            If Not IsDBNull(dr("ESH_TODT")) Then
                curToDate = Format(dr("ESH_TODT"), OASISConstants.DateFormat)
            End If
            Dim drow As DataRow = dt.NewRow

            drow("ENR_ID") = dr("ENR_ID")
            tempString = Format(dr("ESH_FROMDT"), OASISConstants.DateFormat) & " To "
            If curToDate = DateTime.MinValue Then
                tempString += "Till Date"
            Else
                tempString += Format(curToDate, OASISConstants.DateFormat)
            End If
            If DateTime.Compare(todate, curToDate) <> 0 Then
                'tempString = ""
                If Not IsDBNull(dr("ESH_TODT")) Then
                    todate = Convert.ToDateTime(dr("ESH_TODT"))
                Else
                    todate = DateTime.MinValue
                End If
                'New row should be created here and to be added...
                drGrandTotal = dt.NewRow
                drGrandTotal("ENR_ID") = "GRAND TOTAL"
                drGrandTotal("Amount") = AccountFunctions.Round(totalamt)
                If dt.Rows.Count > 1 Then
                    drGrandTotal("Duration") = dt.Rows(dt.Rows.Count - 1)("duration")
                End If
                dt.Rows.Add(drGrandTotal)
                totalamt = dr("AMOUNT")
            Else
                totalamt += dr("AMOUNT")
            End If
            drow("Amount") = AccountFunctions.Round(dr("AMOUNT"))
            drow("Duration") = tempString
            dt.Rows.Add(drow)
        End While

        drGrandTotal = dt.NewRow
        drGrandTotal("ENR_ID") = "GRAND TOTAL"
        drGrandTotal("Amount") = AccountFunctions.Round(totalamt)
        drGrandTotal("Duration") = tempString
        dt.Rows.Add(drGrandTotal)

        gvEmpSalHistory.DataSource = dt
        gvEmpSalHistory.DataBind()
        Return True
    End Function

    Private Function CreateSalaryHistoryTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cDuration As New DataColumn("Duration", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))

            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cDuration)
            dtDt.Columns.Add(cAmount)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Private Function FillCurrentSalDetails(ByVal empNo As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMPSALARYHS_s.GUID, ESH_ID,ESH_EMP_ID, ESH_BSU_ID, ESH_ERN_ID as ENR_ID," & _
        " ESH_bMonthly, ESL_PAYTERM, ESH_PAYMONTH, ESH_PAYYEAR, ESH_bPAID, EMPSALCOMPO_M.ERN_DESCR, " & _
        " ESH_AMOUNT as AMOUNT,ESH_ELIGIBILITY , ESH_FROMDT, ESH_TODT, ESH_CUR_ID, ESH_PAY_CUR_ID FROM EMPSALARYHS_s " & _
        " LEFT OUTER JOIN EMPSALCOMPO_M ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        " WHERE ESH_EMP_ID ='" & empNo & "' AND ESH_TODT is null AND ESH_BSU_ID='" & Session("sBSUID") & "'" & _
        " order by ERN_ORDER "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim dtTempDtl As New DataTable
            dtTempDtl = CreateSalaryRevDetailsTable()
            'Session("EMPSALREVDETAILS") = CreateSalaryRevDetailsTable()
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim newRow As DataRow
                newRow = dtTempDtl.NewRow
                newRow("UniqueID") = dr("ESH_ID")
                newRow("ENR_ID") = dr("ENR_ID")
                newRow("ERN_DESCR") = dr("ERN_DESCR")
                newRow("FROM_DATE") = dr("ESH_FROMDT")
                newRow("PAY_CUR_ID") = dr("ESH_PAY_CUR_ID")
                newRow("PAYTERM") = dr("ESL_PAYTERM")
                newRow("PAY_MONTH") = dr("ESH_PAYMONTH")
                newRow("PAY_YEAR") = dr("ESH_PAYYEAR")
                newRow("bPAID") = dr("ESH_bPAID")
                newRow("bMonthly") = dr("ESH_bMonthly")
                newRow("Amount") = dr("AMOUNT")
                newRow("Amount_ELIGIBILITY") = dr("ESH_ELIGIBILITY")
                newRow("Status") = "Nothing"
                dtTempDtl.Rows.Add(newRow)
            Next
            Session("EMPSALREVDETAILS") = dtTempDtl
            GridBindCurrentSalaryDetails()
            'gvEmpSalary.DataSource = Session("EMPSALREVDETAILS")
            'gvEmpSalary.DataBind()
            Return True
        Else
            gvEmpSalary.DataSource = Nothing
            txtGrossSalary.Text = ""
            gvEmpSalary.DataBind()
            Return False
        End If

    End Function

    Private Sub GridBindCurrentSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("EMPSALREVDETAILS") Is Nothing Then Return
        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        Dim grossSal As Double
        dtTempDtl = CreateSalaryRevDetailsTable()
        If Session("EMPSALREVDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("EMPSALREVDETAILS").Rows.Count - 1
                If (Session("EMPSALREVDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("EMPSALREVDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("EMPSALREVDETAILS").Rows(i)(strColumnName)
                            If Not Session("EMPSALREVDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("EMPSALREVDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        txtGrossSalary.Text = grossSal
        gvEmpSalary.DataSource = dtTempDtl
        gvEmpSalary.DataBind()
    End Sub

    Private Sub BindENR()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 1  order by Isnull(ERN_ORDER,100)" 
        Dim str_Sql As String = "exec GetEarnTypesBSUWise '" & Session("sBSUID") & "',1"          'V1.1
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataBind()
    End Sub

    Protected Sub btnSalAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalAdd.Click

        'If btnSalAdd.Text = "Save" Then
        '    'In Edit Mode

        'ElseIf btnSalAdd.Text = "Add" Then
        '    'In Add Mode

        'End If
        Dim dt As DateTime = New DateTime(ViewState("PAYYEAR"), ViewState("PAYMONTH"), 1)
        Dim dtCompare As DateTime = New DateTime(ddSalPayYear.SelectedValue, ddSalPayMonth.SelectedValue, 1)
        If Not chksalArreasPaid.Checked Then
            If DateTime.Compare(dtCompare, dt) <= 0 Then
                lblError.Text = "Arrears payed period is not valid"
                Exit Sub
            End If
        End If

        If Session("EMPSALREVDETAILS") Is Nothing Then
            Session("EMPSALREVDETAILS") = CreateSalaryRevDetailsTable()
        End If
        If Not IsNumeric(txtSalAmount.Text) Or Not IsNumeric(txtSalAmtEligible.Text) Then
            lblError.Text = "Amount should be valid"
            Exit Sub
        End If
        If CDbl(txtSalAmount.Text) > CDbl(txtSalAmtEligible.Text) Then
            lblError.Text = "Actual amount should be less than eligible"
            Exit Sub
        End If
        Dim status As String = String.Empty
        Dim dtSalDetails As DataTable = Session("EMPSALREVDETAILS")
        Dim i As Integer
        Dim gvRow As GridViewRow = gvEmpSalary.SelectedRow()

        For i = 0 To dtSalDetails.Rows.Count - 1
            If Session("EMPREVEditID") Is Nothing Or _
            Session("EMPREVEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                If dtSalDetails.Rows(i)("ENR_ID") = ddSalEarnCode.SelectedItem.Value Then
                    lblError.Text = "Cannot add transaction details.The Earn Code details are repeating."
                    GridBindCurrentSalaryDetails()
                    Exit Sub
                End If
            End If
        Next
        If btnSalAdd.Text = "Add" Then
            Dim newDR As DataRow = dtSalDetails.NewRow()
            newDR("UniqueID") = dtSalDetails.Rows.Count()
            newDR("ENR_ID") = ddSalEarnCode.SelectedValue
            newDR("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
            newDR("Amount") = txtSalAmount.Text
            newDR("Amount_ELIGIBILITY") = txtSalAmtEligible.Text

            newDR("PAYTERM") = CInt(ddlayInstallment.SelectedValue)
            newDR("PAY_MONTH") = ddSalPayMonth.SelectedValue
            newDR("PAY_YEAR") = ddSalPayYear.SelectedValue

            newDR("bPAID") = chksalArreasPaid.Checked
            newDR("bMonthly") = chkSalPayMonthly.Checked

            'newDR("FROM_DATE") = CDate(txtDate.Text)
            newDR("PAY_CUR_ID") = ViewState("PAY_CUR_ID")
            newDR("Status") = "Insert"
            status = "Insert"
            dtSalDetails.Rows.Add(newDR)
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), CDbl(txtSalAmount.Text))
        ElseIf btnSalAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPREVEditID")
            For iIndex = 0 To dtSalDetails.Rows.Count - 1
                If str_Search = dtSalDetails.Rows(iIndex)("UniqueID") And dtSalDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtSalDetails.Rows(iIndex)("ENR_ID") = ddSalEarnCode.SelectedValue
                    dtSalDetails.Rows(iIndex)("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
                    dtSalDetails.Rows(iIndex)("Amount") = CDbl(txtSalAmount.Text)
                    dtSalDetails.Rows(iIndex)("Amount_ELIGIBILITY") = CDbl(txtSalAmtEligible.Text)
                    dtSalDetails.Rows(iIndex)("PAYTERM") = CInt(ddlayInstallment.SelectedValue)
                    dtSalDetails.Rows(iIndex)("PAY_MONTH") = ddSalPayMonth.SelectedValue
                    dtSalDetails.Rows(iIndex)("PAY_YEAR") = ddSalPayYear.SelectedValue
                    dtSalDetails.Rows(iIndex)("bPAID") = chksalArreasPaid.Checked
                    dtSalDetails.Rows(iIndex)("bMonthly") = chkSalPayMonthly.Checked

                    status = "Edit/Update"
                    Exit For
                End If
            Next
            btnSalAdd.Text = "Add"
            ddSalEarnCode.Enabled = True
            lblError.Text = ""
            'Dim amt As Double = CDbl(IIf(chkSalPayMonthly.Checked, txtSalAmount.Text * 12, txtSalAmount.Text))
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), amt)
        End If
        'If UtilityObj.operOnAudiTable(ViewState("MnuCode"), h_Emp_No.Value, status, Page.User.Identity.Name.ToString, Me) = 0 Then
        '    ClearEMPSALREVDETAILS()
        'Else
        '    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
        'End If
        ClearEMPSALREVDETAILS()
        Session("EMPSALREVDETAILS") = dtSalDetails
        GridBindCurrentSalaryDetails()
    End Sub

    'Sub FillAllocateDetails(ByVal ERN_ID As String, ByVal pay_Term As Integer, ByVal salAmt As Double)
    '    If Session("EMPSALSCHEDULE") Is Nothing Then
    '        Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
    '    End If
    '    Dim term As Integer
    '    '0-Monthly,1-Bimonthly,2-Quarterly,3-Half-year,4 Yearly
    '    Select Case pay_Term
    '        Case 0
    '            Exit Sub
    '        Case 1
    '            term = 6
    '        Case 2
    '            term = 4
    '        Case 3
    '            term = 2
    '        Case 4
    '            term = 1
    '    End Select
    '    ViewState("CurrMonth") = ViewState("PAYMONTH")
    '    ViewState("CurrYear") = ViewState("PAYYEAR")
    '    Dim amtBreakDown As Double = AccountFunctions.Round(salAmt / term)
    '    Dim dttab As DataTable = Session("EMPSALSCHEDULE")
    '    Dim drNewRow As DataRow
    '    For i As Integer = 0 To term - 1
    '        drNewRow = dttab.NewRow()
    '        drNewRow("UniqueID") = i & h_Emp_No.Value & ERN_ID
    '        drNewRow("ESH_ID") = "0"
    '        drNewRow("PayMonth") = ViewState("CurrMonth")
    '        drNewRow("PayYear") = ViewState("CurrYear")
    '        drNewRow("ENR_ID") = ERN_ID
    '        drNewRow("PaidAmount") = 0
    '        drNewRow("Amount") = amtBreakDown
    '        drNewRow("Status") = "LOADED"
    '        drNewRow("EditPaidAmount") = True

    '        dttab.Rows.Add(drNewRow)
    '        If (ViewState("CurrMonth") + (12 / term)) > 12 Then
    '            ViewState("CurrMonth") = (ViewState("CurrMonth") + (12 / term)) - 12
    '            ViewState("CurrYear") += 1
    '        Else
    '            ViewState("CurrMonth") += (12 / term)
    '        End If
    '    Next
    '    Session("EMPSALSCHEDULE") = dttab
    'End Sub

    'Private Function CreateTableSalSchedule() As DataTable
    '    Dim dtDt As New DataTable
    '    Try
    '        Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
    '        Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
    '        Dim cESH_ID As New DataColumn("ESH_ID", System.Type.GetType("System.String"))
    '        Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
    '        Dim cPaidAmount As New DataColumn("PaidAmount", System.Type.GetType("System.Decimal"))
    '        Dim cPayYear As New DataColumn("PayYear", System.Type.GetType("System.Int32"))
    '        Dim cPAYMONTH As New DataColumn("PayMonth", System.Type.GetType("System.Int16"))
    '        Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
    '        Dim cEditPaidamt As New DataColumn("EditPaidAmount", System.Type.GetType("System.Boolean"))

    '        dtDt.Columns.Add(cUniqueID)
    '        dtDt.Columns.Add(cENR_ID)
    '        dtDt.Columns.Add(cESH_ID)
    '        dtDt.Columns.Add(cAmount)
    '        dtDt.Columns.Add(cPaidAmount)
    '        dtDt.Columns.Add(cPayYear)
    '        dtDt.Columns.Add(cPAYMONTH)
    '        dtDt.Columns.Add(cStatus)
    '        dtDt.Columns.Add(cEditPaidamt)
    '        Return dtDt
    '    Catch ex As Exception
    '        Return dtDt
    '    End Try
    '    Return Nothing
    'End Function

    Private Sub ClearEMPSALREVDETAILS()
        ddSalEarnCode.SelectedIndex = 0
        txtSalAmount.Text = ""
        txtSalAmtEligible.Text = ""
        btnSalAdd.Text = "Add"
        lblError.Text = ""
        ddSalEarnCode.SelectedIndex = 0
        ddSalPayMonth.SelectedIndex = ViewState("PAYMONTH")
        ddSalPayYear.SelectedValue = ViewState("PAYYEAR")
        ddlayInstallment.SelectedValue = 0
        chksalArreasPaid.Checked = True
        chkSalPayMonthly.Checked = True
        Session.Remove("EMPREVEditID")

    End Sub

    Function CreateSalaryRevDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cENR_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))

            Dim cFROMDATE As New DataColumn("FROM_DATE", System.Type.GetType("System.DateTime"))
            Dim cPAY_CUR_ID As New DataColumn("PAY_CUR_ID", System.Type.GetType("System.String"))

            Dim cPay_Year As New DataColumn("PAY_YEAR", System.Type.GetType("System.Int16"))
            Dim cPay_Month As New DataColumn("PAY_MONTH", System.Type.GetType("System.Int32"))

            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cPAYTERM As New DataColumn("PAYTERM", System.Type.GetType("System.Int32"))
            Dim cbPAID As New DataColumn("bPAID", System.Type.GetType("System.Boolean"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY As New DataColumn("Amount_ELIGIBILITY", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cENR_DESCR)
            dtDt.Columns.Add(cFROMDATE)
            dtDt.Columns.Add(cPay_Month)
            dtDt.Columns.Add(cPay_Year)

            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cPAYTERM)
            dtDt.Columns.Add(cbPAID)

            dtDt.Columns.Add(cPAY_CUR_ID)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cAmount_ELIGIBILITY)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub lnkSalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String = "SELECT * FROM EMPSALARYHS_s " & _
        '" WHERE GUID ='" & lblRowId.Text & "'"
        'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim dt As DataTable = Session("EMPSALREVDETAILS")
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("UniqueID") = lblRowId.Text Then
                ddSalEarnCode.SelectedValue = dt.Rows(i)("ENR_ID")
                Session("EMPREVEditID") = lblRowId.Text
                ddSalEarnCode.Enabled = False
                txtSalAmount.Text = AccountFunctions.Round(dt.Rows(i)("AMOUNT"))
                txtSalAmtEligible.Text = AccountFunctions.Round(dt.Rows(i)("Amount_ELIGIBILITY"))

                ddlayInstallment.SelectedValue = dt.Rows(i)("PAYTERM")
                'ddSalPayMonth.SelectedValue = dt.Rows(i)("PAY_MONTH")
                'ddSalPayYear.SelectedValue = dt.Rows(i)("PAY_YEAR")
                'chksalArreasPaid.Checked = dt.Rows(i)("bPAID")
                chkSalPayMonthly.Checked = dt.Rows(i)("bMonthly")
                btnSalAdd.Text = "Save"

                If Not Session("EMPSALSCHEDULE") Is Nothing Then
                    Dim dtable As DataTable = Session("EMPSALSCHEDULE")
                    For Each dr As DataRow In Session("EMPSALSCHEDULE").Rows
                        If dr("ENR_ID") = dt.Rows(i)("ENR_ID") Then
                            dr("status") = "DELETED"
                        End If
                    Next
                    Session("EMPSALSCHEDULE") = dtable
                End If

            End If
        Next
    End Sub

    Protected Sub btnSalRevSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalRevSave.Click
        If CDate(txtDate.Text).Day <> 1 Then
            lblFormError.Text = "Effective date should be the first day of the month"
            Return
        End If
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim transaction As SqlTransaction = conn.BeginTransaction("SampleTransaction")
        'If CheckPayMonth() Then
        If 1 = 1 Then
            Dim errorNo As Integer = SaveEMPSalaryDetails(conn, transaction)
            'If errorNo = 0 Then errorNo = SaveEMPSalarySchedule(conn, transaction)
            If errorNo = 0 Then
                If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), h_Emp_No.Value, "Salary Revised", Page.User.Identity.Name.ToString, Nothing) <> 0 Then
                    transaction.Rollback()
                    lblFormError.Text = "Failed to update Audit Trail"
                    UtilityObj.Errorlog("Failed to update Audit Trail")
                Else
                    transaction.Commit()
                    FillCurrentSalDetails(h_Emp_No.Value)
                    FillSalHistoryDetails(h_Emp_No.Value)
                    lblFormError.Text = " Salary Revised Successfully"
                End If
            Else
                lblFormError.Text = UtilityObj.getErrorMessage(errorNo)
                transaction.Rollback()
            End If
        Else
            lblFormError.Text = "Date is not Valid"
            transaction.Rollback()
        End If
    End Sub

    'Private Function SaveEMPSalarySchedule(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
    '    Dim iReturnvalue As Integer
    '    Try
    '        If Session("EMPSALSCHEDULE") Is Nothing Then
    '            Return 0
    '        End If
    '        Dim cmd As New SqlCommand
    '        Dim iIndex As Integer
    '        'Dim ERN_IDs As New ArrayList
    '        Dim htERN_ID As New Hashtable

    '        If Session("EMPSALSCHEDULE").Rows.Count > 0 Then
    '            For iIndex = 0 To Session("EMPSALSCHEDULE").Rows.Count - 1
    '                Dim dr As DataRow = Session("EMPSALSCHEDULE").Rows(iIndex)
    '                If dr("status").ToString().ToUpper() <> "DELETED" And _
    '                dr("status").ToString().ToUpper() = "SCHEDULED" Then
    '                    If Not htERN_ID.ContainsKey(dr("ENR_ID")) Then
    '                        Dim ERNDet As New ERNDetails
    '                        ERNDet.ERNID = dr("ENR_ID")
    '                        ERNDet.ESH_ID = dr("ESH_ID")
    '                        ERNDet.amount = 0
    '                        ERNDet.fromDate = New DateTime(dr("PayYear"), dr("PayMonth"), 1)
    '                        ERNDet.toDate = New DateTime(dr("PayYear"), dr("PayMonth"), 1)
    '                        htERN_ID(dr("ENR_ID")) = ERNDet
    '                    End If
    '                    Dim det As ERNDetails
    '                    det = htERN_ID(dr("ENR_ID"))
    '                    If Not det Is Nothing Then
    '                        Dim curdt As New DateTime(dr("PayYear"), dr("PayMonth"), 1)
    '                        If DateTime.Compare(curdt, det.fromDate) < 0 Then
    '                            det.fromDate = curdt
    '                        End If
    '                        If DateTime.Compare(curdt, det.toDate) > 0 Then
    '                            det.toDate = curdt
    '                        End If
    '                        det.amount += dr("Amount")
    '                    End If
    '                End If
    '            Next
    '            Dim ienum As IDictionaryEnumerator = htERN_ID.GetEnumerator
    '            While (ienum.MoveNext())

    '                Dim newESS_ID As Integer
    '                Dim det As ERNDetails = ienum.Value

    '                cmd = New SqlCommand("SaveEMPSALARYSCH_H", objConn, stTrans)
    '                cmd.CommandType = CommandType.StoredProcedure

    '                Dim sqlpESS_ID As New SqlParameter("@ESS_ID", SqlDbType.Int)
    '                sqlpESS_ID.Value = 0
    '                cmd.Parameters.Add(sqlpESS_ID)

    '                Dim sqlpESS_EMP_ID As New SqlParameter("@ESS_EMP_ID", SqlDbType.Int)
    '                sqlpESS_EMP_ID.Value = h_Emp_No.Value
    '                cmd.Parameters.Add(sqlpESS_EMP_ID)

    '                Dim sqlpESS_ERN_ID As New SqlParameter("@ESS_ERN_ID", SqlDbType.VarChar, 10)
    '                sqlpESS_ERN_ID.Value = det.ERNID
    '                cmd.Parameters.Add(sqlpESS_ERN_ID)

    '                Dim sqlpESS_ESH_ID As New SqlParameter("@ESS_ESH_ID", SqlDbType.Int)
    '                sqlpESS_ESH_ID.Value = det.ESH_ID
    '                cmd.Parameters.Add(sqlpESS_ESH_ID)

    '                Dim sqlpESS_AMOUNT As New SqlParameter("@ESS_AMOUNT", SqlDbType.Decimal)
    '                sqlpESS_AMOUNT.Value = det.amount
    '                cmd.Parameters.Add(sqlpESS_AMOUNT)

    '                Dim sqlpESS_DTFROM As New SqlParameter("@ESS_DTFROM", SqlDbType.DateTime)
    '                sqlpESS_DTFROM.Value = det.fromDate
    '                cmd.Parameters.Add(sqlpESS_DTFROM)

    '                Dim sqlpESS_DTTO As New SqlParameter("@ESS_DTTO", SqlDbType.DateTime)
    '                sqlpESS_DTTO.Value = det.toDate
    '                cmd.Parameters.Add(sqlpESS_DTTO)

    '                Dim retNewESS_ID As New SqlParameter("@NewESS_ID", SqlDbType.Int)
    '                retNewESS_ID.Direction = ParameterDirection.Output
    '                cmd.Parameters.Add(retNewESS_ID)

    '                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
    '                retValParam.Direction = ParameterDirection.ReturnValue
    '                cmd.Parameters.Add(retValParam)

    '                cmd.ExecuteNonQuery()
    '                iReturnvalue = retValParam.Value
    '                newESS_ID = retNewESS_ID.Value
    '                If iReturnvalue <> 0 Then
    '                    Exit While
    '                End If
    '                For iIndex = 0 To Session("EMPSALSCHEDULE").Rows.Count - 1
    '                    Dim drow As DataRow = Session("EMPSALSCHEDULE").Rows(iIndex)
    '                    If drow("status").ToString().ToUpper() <> "DELETED" And _
    '                    drow("status").ToString().ToUpper() = "SCHEDULED" Then

    '                        cmd = New SqlCommand("SaveEMPSALARYSCH_D", objConn, stTrans)
    '                        cmd.CommandType = CommandType.StoredProcedure

    '                        Dim sqlpEDD_ID As New SqlParameter("@EDD_ID", SqlDbType.Int)
    '                        sqlpEDD_ID.Value = 0
    '                        cmd.Parameters.Add(sqlpEDD_ID)

    '                        Dim sqlpEDD_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
    '                        sqlpEDD_EMP_ID.Value = h_Emp_No.Value
    '                        cmd.Parameters.Add(sqlpEDD_EMP_ID)

    '                        Dim sqlpEDD_ESS_ID As New SqlParameter("@EDD_ESS_ID", SqlDbType.Int)
    '                        sqlpEDD_ESS_ID.Value = newESS_ID
    '                        cmd.Parameters.Add(sqlpEDD_ESS_ID)

    '                        Dim sqlpEDD_AMOUNT As New SqlParameter("@EDD_AMOUNT", SqlDbType.Decimal)
    '                        sqlpEDD_AMOUNT.Value = drow("Amount")
    '                        cmd.Parameters.Add(sqlpEDD_AMOUNT)

    '                        Dim sqlpEDD_PAIDAMT As New SqlParameter("@EDD_PAIDAMT", SqlDbType.Decimal)
    '                        sqlpEDD_PAIDAMT.Value = drow("PaidAmount")
    '                        cmd.Parameters.Add(sqlpEDD_PAIDAMT)

    '                        Dim sqlpEDD_PAYMONTH As New SqlParameter("@EDD_PAYMONTH", SqlDbType.Int)
    '                        sqlpEDD_PAYMONTH.Value = drow("PayMonth")
    '                        cmd.Parameters.Add(sqlpEDD_PAYMONTH)

    '                        Dim sqlpEDD_PAYYEAR As New SqlParameter("@EDD_PAYYEAR", SqlDbType.Int)
    '                        sqlpEDD_PAYYEAR.Value = drow("PayYear")
    '                        cmd.Parameters.Add(sqlpEDD_PAYYEAR)

    '                        Dim sqlpEDD_ESD_ID As New SqlParameter("@EDD_ESD_ID", SqlDbType.Int)
    '                        sqlpEDD_ESD_ID.Value = drow("ESH_ID")
    '                        cmd.Parameters.Add(sqlpEDD_ESD_ID)

    '                        Dim sqlpEDD_REFDOCNO As New SqlParameter("@EDD_REFDOCNO", SqlDbType.VarChar, 20)
    '                        sqlpEDD_REFDOCNO.Value = "0"
    '                        cmd.Parameters.Add(sqlpEDD_REFDOCNO)

    '                        Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
    '                        sqlpbDeleted.Value = False
    '                        cmd.Parameters.Add(sqlpbDeleted)

    '                        Dim retVal As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
    '                        retVal.Direction = ParameterDirection.ReturnValue
    '                        cmd.Parameters.Add(retVal)

    '                        cmd.ExecuteNonQuery()
    '                        iReturnvalue = retVal.Value
    '                        If iReturnvalue <> 0 Then
    '                            Exit For
    '                        End If
    '                    End If
    '                Next
    '            End While
    '            Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
    '        End If
    '        Return iReturnvalue
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '        Return 1000
    '    End Try
    '    Return False
    'End Function

    Private Function SaveEMPSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPSALREVDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("EMPSALREVDETAILS").Rows.Count > 0 Then
                For iIndex = 0 To Session("EMPSALREVDETAILS").Rows.Count - 1

                    Dim dr As DataRow = Session("EMPSALREVDETAILS").Rows(iIndex)

                    cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
                    sqlpESH_ID.Value = 0
                    cmd.Parameters.Add(sqlpESH_ID)

                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                    sqlpUser.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpUser)

                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                    sqlpESH_EMP_ID.Value = h_Emp_No.Value 'h_Emp_No.Value
                    cmd.Parameters.Add(sqlpESH_EMP_ID)

                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                    sqlpESH_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpESH_BSU_ID)

                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                    sqlpESH_ERN_ID.Value = dr("ENR_ID")
                    cmd.Parameters.Add(sqlpESH_ERN_ID)

                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                    sqlpESH_AMOUNT.Value = dr("Amount")
                    cmd.Parameters.Add(sqlpESH_AMOUNT)

                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                    sqlpESH_ELIGIBILITY.Value = dr("Amount_ELIGIBILITY")
                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                    sqlpESH_bMonthly.Value = dr("bMonthly")
                    cmd.Parameters.Add(sqlpESH_bMonthly)

                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                    sqlpESH_PAYTERM.Value = dr("PAYTERM")
                    cmd.Parameters.Add(sqlpESH_PAYTERM)

                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
                    cmd.Parameters.Add(sqlpESH_CUR_ID)

                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_PAY_CUR_ID.Value = dr("PAY_CUR_ID") 'FromDataBase
                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                    sqlpESH_PAYMONTH.Value = dr("PAY_MONTH") 'FromDataBase
                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                    sqlpESH_PAYYEAR.Value = dr("PAY_YEAR")
                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                    sqlpESH_FROMDT.Value = CDate(txtDate.Text)  'FromDataBase
                    cmd.Parameters.Add(sqlpESH_FROMDT)

                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
                    sqlpbDeleted.Value = False
                    cmd.Parameters.Add(sqlpbDeleted)

                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                    sqlpESH_TODT.Value = DBNull.Value
                    cmd.Parameters.Add(sqlpESH_TODT)

                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retSValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retSValParam)

                    Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                    retNewESH_ID.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(retNewESH_ID)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retSValParam.Value
                    If iReturnvalue <> 0 Then
                        Exit For
                    Else
                        'Set the UniqueID to the ESH_ID that is returned....
                        If Not Session("EMPSALSCHEDULE") Is Nothing Then
                            If Session("EMPSALSCHEDULE").Rows.Count > 0 Then
                                For index As Integer = 0 To Session("EMPSALSCHEDULE").Rows.Count - 1
                                    Dim drow As DataRow = Session("EMPSALSCHEDULE").Rows(index)
                                    If drow("ENR_ID") = dr("ENR_ID") Then
                                        drow("ESH_ID") = retNewESH_ID.Value
                                    End If
                                Next
                            End If
                        End If
                    End If
                Next
                    End If
                    Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Protected Sub gvEmpSalHistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpSalHistory.PageIndexChanging
        gvEmpSalHistory.PageIndex = e.NewPageIndex
        FillSalHistoryDetails(h_Emp_No.Value)
    End Sub

    Protected Sub gvEmpSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpSalary.RowDataBound
        Try
            Dim lblSchedule As New Label
            Dim lblPayTerm As New Label
            lblSchedule = e.Row.FindControl("lblSchedule")
            lblPayTerm = e.Row.FindControl("lblPayTerm")
            If lblSchedule IsNot Nothing Then
                Select Case lblPayTerm.Text
                    Case 0
                        lblSchedule.Text = "Monthly"
                    Case 1
                        lblSchedule.Text = "Bimonthly"
                    Case 2
                        lblSchedule.Text = "Quarterly"
                    Case 3
                        lblSchedule.Text = "Half-yearly"
                    Case 4
                        lblSchedule.Text = "Yearly"
                End Select
            End If
        Catch
        End Try
    End Sub

    Protected Sub btnSalRevCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalRevCancel.Click
        Session("EMPSALSCHEDULE") = Nothing
        Session("EMPSALREVDETAILS") = Nothing
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearEMPSALREVDETAILS()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgEmpSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmpSel.Click
        Session("EMPSALSCHEDULE") = Nothing
        Session("EMPSALREVDETAILS") = Nothing
        GridBindCurrentSalaryDetails()
        FillCurrentSalDetails(h_Emp_No.Value)
        FillSalHistoryDetails(h_Emp_No.Value)
        FillFTEValue(h_Emp_No.Value)
    End Sub

    Private Sub FillFTEValue(ByVal empNo As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMP_FTE FROM vw_OSO_EMPLOYEEMASTERDETAILS WHERE EMP_ID = '" & empNo & "'"
        Dim empFTE As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If empFTE Is DBNull.Value Then
            hf_FTE.Value = 1
        Else
            hf_FTE.Value = Convert.ToDouble(empFTE)
        End If

    End Sub

    Protected Sub btnSalCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalCancel.Click
        ClearEMPSALREVDETAILS()
    End Sub

    Protected Sub txtEmpNo_TextChanged(sender As Object, e As EventArgs)
        Session("EMPSALSCHEDULE") = Nothing
        Session("EMPSALREVDETAILS") = Nothing
        GridBindCurrentSalaryDetails()
        FillCurrentSalDetails(h_Emp_No.Value)
        FillSalHistoryDetails(h_Emp_No.Value)
        FillFTEValue(h_Emp_No.Value)
    End Sub
End Class
 