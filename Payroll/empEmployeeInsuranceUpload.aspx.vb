﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet
Partial Class Payroll_empEmployeeInsuranceUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnLoad)
        smScriptManager.RegisterPostBackControl(btnSaveToDB)

        'smScriptManager.RegisterPostBackControl(gvExport)
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        End If

    End Sub


    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        gvDependancedetails.DataSource = Nothing
        gvDependancedetails.DataBind()
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ext = ".xls" Then
                '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                ' getdataExcel(strFileName)
                UpLoadDBF()
                If gvDependancedetails.Rows.Count > 0 Then
                    btnSaveToDB.Visible = True
                Else
                    If lblError.Text = "" Then
                        lblError.Text = "No data uploaded!"
                    End If


                End If

            Else
                lblError.Text = "Please upload .xls files only."
            End If

        Else
            lblError.Text = "File not uploaded"
        End If

    End Sub
    Private Function CreateEmpDetailsTable() As DataTable
        Dim dtDt As New DataTable

        Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
        Dim cEMPNAME As New DataColumn("EMPNAME", System.Type.GetType("System.String"))
        Dim cEMPNO As New DataColumn("EMPNO", System.Type.GetType("System.String"))
        Dim cEMPID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
        Dim cInsuranceCategory As New DataColumn("EMP_INSU_ID", System.Type.GetType("System.String"))
        Dim cInsuranceCategoryDESC As New DataColumn("InsuranceCategoryDESC", System.Type.GetType("System.String"))
        Dim cInsuranceEligibleID As New DataColumn("EMP_INSUFLAG_ELIGIBLE", System.Type.GetType("System.String"))
        Dim cInsuranceEligibleDescr As New DataColumn("EMP_InsuranceEligibleDescr", System.Type.GetType("System.String"))
        Dim cInsuranceActualID As New DataColumn("EMP_INSUFLAG", System.Type.GetType("System.String"))
        Dim cInsuranceActualDescr As New DataColumn("EMP_InsuranceActualDescr", System.Type.GetType("System.String"))
        Dim cEmiratesIDNO As New DataColumn("Emp_EMIRATESID_NO", System.Type.GetType("System.String"))
        Dim cEmiratesIDIssuedt As New DataColumn("Emp_EMIRATESID_IssueDT", System.Type.GetType("System.DateTime"))
        Dim cEmiratesIDExpirydt As New DataColumn("Emp_EMIRATESID_Expirydt", System.Type.GetType("System.DateTime"))
        Dim cEmiratesIDPlace As New DataColumn("Emp_EMIRATESID_Place", System.Type.GetType("System.String"))
        Dim cUIDNo As New DataColumn("Emp_UIDNo", System.Type.GetType("System.String"))

        Dim cVisaNo As New DataColumn("Emp_VISANO", System.Type.GetType("System.String"))
        Dim cVisaPlace As New DataColumn("Emp_VISA_Place", System.Type.GetType("System.String"))
        Dim cVisaIssuedt As New DataColumn("Emp_VISA_IssueDT", System.Type.GetType("System.DateTime"))
        Dim cVisaExpirydt As New DataColumn("Emp_VISA_Expirydt", System.Type.GetType("System.DateTime"))
        Dim cPassportNo As New DataColumn("Emp_PASSPORTNo", System.Type.GetType("System.String"))
        Dim cContactNo As New DataColumn("Emp_ContactNo", System.Type.GetType("System.String"))
        Dim cEmailID As New DataColumn("Emp_EMAILID", System.Type.GetType("System.String"))

        dtDt.Columns.Add(cUniqueID)
        dtDt.Columns.Add(cEMPNAME)
        dtDt.Columns.Add(cEMPNO)
        dtDt.Columns.Add(cEMPID)
        dtDt.Columns.Add(cInsuranceCategory)
        dtDt.Columns.Add(cInsuranceCategoryDESC)
        dtDt.Columns.Add(cInsuranceEligibleID)
        dtDt.Columns.Add(cInsuranceEligibleDescr)
        dtDt.Columns.Add(cInsuranceActualID)
        dtDt.Columns.Add(cInsuranceActualDescr)
        dtDt.Columns.Add(cEmiratesIDNO)
        dtDt.Columns.Add(cEmiratesIDIssuedt)
        dtDt.Columns.Add(cEmiratesIDExpirydt)
        dtDt.Columns.Add(cEmiratesIDPlace)
        dtDt.Columns.Add(cUIDNo)
        dtDt.Columns.Add(cVisaNo)
        dtDt.Columns.Add(cVisaPlace)
        dtDt.Columns.Add(cVisaIssuedt)
        dtDt.Columns.Add(cVisaExpirydt)
        dtDt.Columns.Add(cPassportNo)
        dtDt.Columns.Add(cContactNo)
        dtDt.Columns.Add(cEmailID)
        Return dtDt
    End Function
    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Try


            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 18)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(1).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(1).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next

            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If

            Dim ValidEmployee As New DataTable
            Dim ValidInsurance As New DataTable
            Dim ValidEligibility As New DataTable
            Dim mTable As New DataTable
            mTable = CreateEmpDetailsTable()
            Dim rowId As Integer = 0

            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                ValidEmployee = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in (" & strEmpNos & ") and emp_bsu_id='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)
                mRow = mTable.NewRow
                mRow("UniqueID") = xlRow(0)

                mRow("EMPNAME") = xlRow(2)
                mRow("EMPNO") = xlRow(1)
                mRow("InsuranceCategoryDESC") = xlRow(3).ToString
                mRow("EMP_INSUFLAG_ELIGIBLE") = xlRow(4).ToString
                mRow("EMP_INSUFLAG") = xlRow(5).ToString



                If ValidEmployee.Select("Empno='" & xlRow(1) & "'", "").Length = 0 Then

                    lblError.Text = "Invalid Employee No:" & mRow("EMPNO") & ", Sl No. :" & mRow("UniqueID") & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidEmployee.Select("Empno='" & mRow("EMPNO") & "'", "")(0)
                    If ValidEmployee.Rows.Count > 0 Then
                        mRow("EMP_ID") = drEmp("Emp_id")
                        mRow("EMPName") = drEmp("EMPNAME")
                    End If
                End If

                ValidInsurance = Mainclass.getDataTable("select INSU_ID,[INSU_DESCR]+'('+ [INSU_SUBDESCR]+')' as Descr  from [INSURANCETYPE_M] where [INSU_DESCR]+'('+ [INSU_SUBDESCR]+')' = '" & xlRow(3).ToString & "'", ConnectionManger.GetOASISConnectionString)
                If ValidInsurance.Rows.Count > 0 Then
                    mRow("EMP_INSU_ID") = ValidInsurance.Rows(0).Item("INSU_ID")
                    mRow("InsuranceCategoryDESC") = ValidInsurance.Rows(0).Item("Descr")
                Else
                    lblError.Text = "Incorrect Insurance category : Sl No. " & mRow("UniqueID")
                    Exit Sub
                End If
                If xlRow(4).ToString = "" Then
                    lblError.Text = "Incorrect Insurance Eligibility : Sl No. " & mRow("UniqueID")
                    Exit Sub
                End If
                ValidEligibility = Mainclass.getDataTable("select [ELG_ID],[ELG_DESCR]  from [ELIGIBILITY_M] where [ELG_ID] = " & xlRow(4).ToString, ConnectionManger.GetOASISConnectionString)
                If ValidInsurance.Rows.Count > 0 Then
                    mRow("EMP_INSUFLAG_ELIGIBLE") = ValidEligibility.Rows(0).Item("ELG_ID")
                    mRow("EMP_InsuranceEligibleDescr") = ValidEligibility.Rows(0).Item("ELG_DESCR")
                Else
                    lblError.Text = "Incorrect Insurance Eligibility : Sl No. " & mRow("UniqueID")
                    Exit Sub
                End If

                If xlRow(5).ToString = "" Then
                    lblError.Text = "Incorrect Insurance (Actual) : Sl No. " & mRow("UniqueID")
                    Exit Sub
                End If
                ValidEligibility = Mainclass.getDataTable("select [ELG_ID],[ELG_DESCR]  from [ELIGIBILITY_M] where [ELG_ID] = " & xlRow(5).ToString, ConnectionManger.GetOASISConnectionString)
                If ValidInsurance.Rows.Count > 0 Then
                    mRow("EMP_INSUFLAG") = ValidEligibility.Rows(0).Item("ELG_ID")
                    mRow("EMP_InsuranceActualDescr") = ValidEligibility.Rows(0).Item("ELG_DESCR")
                Else
                    lblError.Text = "Incorrect Insurance (Actual) : Sl No. " & mRow("UniqueID")
                    Exit Sub
                End If

                If xlRow(6).ToString <> "" Then
                    mRow("Emp_EMIRATESID_NO") = xlRow(6).ToString

                    If IsDate(xlRow(7).ToString) = True Then
                        mRow("Emp_EMIRATESID_IssueDT") = xlRow(7).ToString
                    Else
                        lblError.Text = "Incorrect Issue Date  : Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If
                    If IsDate(xlRow(8).ToString) = True Then
                        mRow("Emp_EMIRATESID_Expirydt") = xlRow(8).ToString
                    Else
                        lblError.Text = "Incorrect Expiry Date : Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If
                    If xlRow(9).ToString = "" Then
                        lblError.Text = "Place of Issue required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_EMIRATESID_Place") = xlRow(9).ToString
                    End If

                    If mRow("Emp_EMIRATESID_IssueDT") > mRow("Emp_EMIRATESID_Expirydt") Then
                        lblError.Text = "Issue date cannot be greater than Expiry Date: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If

                    If xlRow(10).ToString = "" Then
                        lblError.Text = "UID No required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_UIDNo") = xlRow(10).ToString
                    End If

                    If xlRow(11).ToString = "" Then
                        lblError.Text = "Visa Document No required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_VISANO") = xlRow(11).ToString
                    End If
                    If xlRow(12).ToString = "" Then
                        lblError.Text = "Visa Issue Place required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_VISA_place") = xlRow(12).ToString
                    End If

                    If IsDate(xlRow(13).ToString) = True Then
                        mRow("Emp_VISA_IssueDT") = xlRow(13).ToString
                    Else
                        lblError.Text = "Incorrect Visa Issue Date  : Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If
                    If IsDate(xlRow(14).ToString) = True Then
                        mRow("Emp_VISA_Expirydt") = xlRow(14).ToString
                    Else
                        lblError.Text = "Incorrect Visa Expiry Date : Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If
                    If mRow("Emp_VISA_IssueDT") > mRow("Emp_VISA_Expirydt") Then
                        lblError.Text = "Visa Issue date cannot be greater than Expiry Date: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If
                    If xlRow(15).ToString = "" Then
                        lblError.Text = "Passport No required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_PASSPORTNo") = xlRow(15).ToString
                    End If
                    If xlRow(16).ToString = "" Then
                        lblError.Text = "Contact No required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_ContactNo") = xlRow(16).ToString
                    End If
                    If xlRow(17).ToString = "" Then
                        lblError.Text = "Email ID required: Sl No. " & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("Emp_EMAILID") = xlRow(17).ToString
                    End If
                End If

                rowId = rowId + 1
                mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()
            'gvFeeDetails.DataSource = mTable
            'gvFeeDetails.DataBind()
            gvDependancedetails.DataSource = mTable
            gvDependancedetails.DataBind()


        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveToDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveToDB.Click
        Dim status As Boolean

        status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("sBsuid") & "-" & "Dependents Data", "Excel Upload", Page.User.Identity.Name.ToString, Me.Page)
        If status <> 0 Then
            Throw New ArgumentException("Could not complete your request")
            Exit Sub
        End If
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try


                transaction = conn.BeginTransaction("SampleTransaction")
                Dim retVal As Integer = SaveUploadDetails(conn, transaction)
                If retVal = 0 Then
                    transaction.Commit()
                    lblError.Text = "Data saved successfully!"
                    gvDependancedetails.DataSource = Nothing
                    gvDependancedetails.DataBind()
                Else
                    transaction.Rollback()
                    lblError.Text = "Error occured while saving."
                End If
            Catch ex As Exception
                lblError.Text = "Error occured while saving."
            End Try

        End Using
    End Sub
    Private Function SaveUploadDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Try


            For Each dtrow As GridViewRow In gvDependancedetails.Rows

                Dim lblEDD_EMIRATES_ID As Label = CType(dtrow.FindControl("lblEDD_EMIRATES_ID"), Label)
                Dim lblEDD_EID_IssueDT As Label = CType(dtrow.FindControl("lblEDD_EID_IssueDT"), Label)
                Dim lblEDD_EID_EXPDT As Label = CType(dtrow.FindControl("lblEDD_EID_EXPDT"), Label)
                Dim lblEDD_EMIRATES_IDPlace As Label = CType(dtrow.FindControl("lblEDD_EMIRATES_IDPlace"), Label)
                Dim lblEmp_UIDNO As Label = CType(dtrow.FindControl("lblEmp_UIDNO"), Label)
                Dim lblVisa_IssueDT As Label = CType(dtrow.FindControl("lblVisa_IssueDT"), Label)
                Dim lblVisa_EXPDT As Label = CType(dtrow.FindControl("lblVisa_EXPDT"), Label)
                Dim lblEmp_PASSPORTNO As Label = CType(dtrow.FindControl("lblEmp_PASSPORTNO"), Label)
                Dim lblEmp_CONTACTNO As Label = CType(dtrow.FindControl("lblEmp_CONTACTNO"), Label)
                Dim lblEmp_EMAILID As Label = CType(dtrow.FindControl("lblEmp_EMAILID"), Label)
                Dim lblVISA_No As Label = CType(dtrow.FindControl("lblVISA_No"), Label)
                Dim lblVisa_Place As Label = CType(dtrow.FindControl("lblEDD_Visa_Place"), Label)
                '  Dim lblIsMarried As Label = CType(dtrow.FindControl("lblIsMarried"), Label)


                Dim cmd As New SqlCommand
                cmd = New SqlCommand("SaveEmpUploadExcel", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 0
                Dim pParms(18) As SqlClient.SqlParameter

                Dim sqlpESL_EMP_ID As New SqlParameter("@EMP_ID", SqlDbType.Int)
                sqlpESL_EMP_ID.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EMP_ID"))
                cmd.Parameters.Add(sqlpESL_EMP_ID)
                lblError.Text = sqlpESL_EMP_ID.Value


                Dim sqlpEDD_Insu_id As New SqlParameter("@EMP_Insu_id", SqlDbType.Int)
                If gvDependancedetails.DataKeys(dtrow.RowIndex)("EMP_INSU_ID") <> "" Then
                    sqlpEDD_Insu_id.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EMP_INSU_ID"))
                Else
                    sqlpEDD_Insu_id.Value = DBNull.Value
                End If

                cmd.Parameters.Add(sqlpEDD_Insu_id)

                Dim sqlpESL_INSUFLAG As New SqlParameter("@EMP_INSUFLAG", SqlDbType.Int)
                sqlpESL_INSUFLAG.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EMP_INSUFLAG"))
                cmd.Parameters.Add(sqlpESL_INSUFLAG)


                Dim sqlpESL_INSUFLAG_ELIGIBLE As New SqlParameter("@EMP_INSUFLAG_ELIGIBLE", SqlDbType.Int)
                sqlpESL_INSUFLAG_ELIGIBLE.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EMP_INSUFLAG_ELIGIBLE"))
                cmd.Parameters.Add(sqlpESL_INSUFLAG_ELIGIBLE)



                Dim sqlpEDD_BSU_ID As New SqlParameter("@EMP_BSU_ID", SqlDbType.VarChar)
                sqlpEDD_BSU_ID.Value = Session("sBsuid").ToString
                cmd.Parameters.Add(sqlpEDD_BSU_ID)


                Dim sqlpEmiratesID As New SqlParameter("Emp_EMIRATESID_NO", SqlDbType.VarChar)
                sqlpEmiratesID.Value = lblEDD_EMIRATES_ID.Text
                cmd.Parameters.Add(sqlpEmiratesID)


                Dim sqlpExpiryDt As New SqlParameter("@EMP_EID_EXPDT", SqlDbType.DateTime)
                If IsDate(lblEDD_EID_EXPDT.Text) Then
                    sqlpExpiryDt.Value = lblEDD_EID_EXPDT.Text
                Else
                    sqlpExpiryDt.Value = DBNull.Value

                End If
                cmd.Parameters.Add(sqlpExpiryDt)

                Dim sqlpIssueDt As New SqlParameter("@EMP_EID_IssueDT", SqlDbType.DateTime)
                If IsDate(lblEDD_EID_IssueDT.Text) Then
                    sqlpIssueDt.Value = lblEDD_EID_IssueDT.Text
                Else
                    sqlpIssueDt.Value = DBNull.Value

                End If
                cmd.Parameters.Add(sqlpIssueDt)

                Dim sqlpIssuePlace As New SqlParameter("@EMP_EID_IssuePlace", SqlDbType.VarChar)

                sqlpIssuePlace.Value = lblEDD_EMIRATES_IDPlace.Text
                cmd.Parameters.Add(sqlpIssuePlace)

                Dim sqlpUidNo As New SqlParameter("@Emp_UIDNO", SqlDbType.VarChar)

                sqlpUidNo.Value = lblEmp_UIDNO.Text
                cmd.Parameters.Add(sqlpUidNo)

                'Added by Rajesh Starts here
                Dim sqlpVisaNo As New SqlParameter("@Emp_VISANO", SqlDbType.VarChar)

                sqlpVisaNo.Value = lblVISA_No.Text
                cmd.Parameters.Add(sqlpVisaNo)

                Dim sqlpVisa_place As New SqlParameter("@Emp_Visa_Issueplace", SqlDbType.VarChar)

                sqlpVisa_place.Value = lblVisa_Place.Text
                cmd.Parameters.Add(sqlpVisa_place)

                Dim sqlpVisaIssueDt As New SqlParameter("@EMP_VISA_IssueDT", SqlDbType.DateTime)
                If IsDate(lblVisa_IssueDT.Text) Then
                    sqlpVisaIssueDt.Value = lblVisa_IssueDT.Text
                Else
                    sqlpVisaIssueDt.Value = DBNull.Value

                End If
                cmd.Parameters.Add(sqlpVisaIssueDt)

                Dim sqlpVisaExpDt As New SqlParameter("@EMP_VISA_EXPDT", SqlDbType.DateTime)
                If IsDate(lblVisa_EXPDT.Text) Then
                    sqlpVisaExpDt.Value = lblVisa_EXPDT.Text
                Else
                    sqlpVisaExpDt.Value = DBNull.Value

                End If
                cmd.Parameters.Add(sqlpVisaExpDt)

                Dim sqlpPassportno As New SqlParameter("@Emp_PASSPORTNO", SqlDbType.VarChar)

                sqlpPassportno.Value = lblEmp_PASSPORTNO.Text
                cmd.Parameters.Add(sqlpPassportno)

                Dim sqlpContactno As New SqlParameter("@Emp_CONTACTNO", SqlDbType.VarChar)

                sqlpContactno.Value = lblEmp_CONTACTNO.Text
                cmd.Parameters.Add(sqlpContactno)

                Dim sqlpemailid As New SqlParameter("@Emp_EMAILID", SqlDbType.VarChar)

                sqlpemailid.Value = lblEmp_EMAILID.Text
                cmd.Parameters.Add(sqlpemailid)

                'ends here

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim retMsg As New SqlParameter("@errMsg", SqlDbType.Bit)
                retMsg.Direction = ParameterDirection.Output
                cmd.Parameters.Add(retMsg)


                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    lblError.Text = UtilityObj.getErrorMessage(iReturnvalue) & retMsg.Value.ToString
                    Exit For
                End If


            Next

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = ex.Message
            Return 1000
        End Try
        Return False
    End Function
End Class
