<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empViewJobResponse_Alloc.aspx.vb" Inherits="Accounts_accccViewCreditcardReceipt" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">




        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
     }
     window.onload = scroll_page;
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
           <asp:Label  ID="lblHeader" runat="server"> Employee Monthly Deduction</asp:Label> 
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td  valign="top" align="left" colspan="2">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">
                    
                    <tr>
                        <td align="center" valign="top"  width="100%">
                            <asp:GridView ID="gvEMPJOBResponsibility" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="20">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="BSUName">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSUName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Business Unit
                                            <br />
                                            <asp:TextBox ID="txtBSUNAME" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            EMPLOYEE No.
                                            <br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle  />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            EMPLOYEE NAME
                                            <br />
                                            <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Responsibility">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblResp" runat="server" Text='<%# Bind("RES_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Responsibility
                                            <br />
                                            <asp:TextBox ID="txtResp" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FromDate">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFrmDt" runat="server" Text='<%# Bind("FROMDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            From Date<br />
                                            <asp:TextBox ID="txtFromDT" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ToDate">
                                        <HeaderTemplate>
                                            To Date
                                            <br />
                                            <asp:TextBox ID="txtTODT" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearchre" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblToDt" runat="server" Text='<%# Bind("TODT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Amount
                                            <br />
                                            <asp:TextBox ID="txtAmount" runat="server" SkinID="Gridtxt" ></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearchs" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False" HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>&nbsp;&nbsp;
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <a id='detail'></a>
                            
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
