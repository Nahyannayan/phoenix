<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empMasterDetail.aspx.vb" Inherits="Payroll_empMasterDetail" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script>
        $(function () {
            $(".menu_a a").each(function (index) {
                var state = $(this).attr("disabled");
                if (state == "disabled") {
                    $(this).addClass('hidetab')
                }
            });
        });

    </script>
    <style type="text/css">
        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }

        .hidetab {
            display: none !important;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

            .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                background-color: transparent !important;
                border-radius: 0px !important;
                border: 0px !important;
                padding: initial !important;
                box-shadow: none !important;
            }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function DoPostBack() {
            __doPostBack("txtBName", "TextChanged");
        }

        function clickme(e, buttonid, controlid) {
            var t = document.getElementById(buttonid);
            if (typeof t == 'object') {
                if (navigator.appName.indexOf("Netscape") > (-1)) {
                    if (e.keyCode == 13) {
                        t.click();
                        return false;
                    }
                }
                if (navigator.appName.indexOf("Microsoft Internet Explorer") > (-1)) {
                    var keyCode;


                    if (e == null) {

                        keyCode = window.event.keyCode;

                    }

                    else {

                        keyCode = e.keyCode;

                    }



                    if (keyCode == 113 || keyCode == 13 || controlid == "btnsearch") {
                        t = document.getElementById("<%=imgemployee.ClientID %>");
                        t.click();
                        // alert(keyCode);
                        //                              var bh2 = $find("EmpPopUp");
                        //
                        //                                 bh2.showPopup();

                        document.getElementById("<%=pnlPopup.ClientID %>").style.display = 'block';
                        document.getElementById('<%=txttest.ClientID %>').focus();
                        //$find("EmpPopUp").showPopup();            



                        return false;
                        //      alert("hiiiiiiiiii");
                        //        $find('ctl00_cphMasterpage_modalEmpPopUp').showPopup();            



                    }
                    else {
                        var bh = $find("EmpPopUp");
                        // document.getElementById("<%=pnlPopup.ClientID %>").style.display = 'none';
                        //return false;
                        //  bh.hidePopup();
                        // alert("nooo");

                    }
                }
                //}
            }

        }


        var oldMenu_HideItems = Menu_HideItems;

        if (oldMenu_HideItems) {
            Menu_HideItems = function (items) {
                if (!items || ((typeof (items.tagName) == "undefined") && (items instanceof Event))) { items = __rootMenuItem; }
                if (items && items.rows && items.rows.length == 0) { items.insertRow(0); }
                return oldMenu_HideItems(items);
            }
        }

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
           <%-- result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=ERP", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>

            var url = "../Accounts/accShowEmpDetail.aspx?id=ERP";
            var oWnd = radopen(url, "pop_EMPName");
            oWnd.center()

        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=txtReport.ClientID%>', 'TextChanged');
            }
        }

        function GetEMPAttName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%--result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfAttendBy.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtAttendBy.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>

            var url = "../Accounts/accShowEmpDetail.aspx?id=ERP";
            var oWnd = radopen(url, "pop_EMPAttName");
            oWnd.center()

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=hfAttendBy.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtAttendBy.ClientID %>").value = NameandCode[0];
                __doPostBack("<%=txtAttendBy.ClientID%>", 'TextChanged');
            }
        }

        function getPageCode(mode) {

            var NameandCode;
            var result;
            var url;
            document.getElementById("<%=hf_mode.ClientID%>").value = mode
            if (mode == 'DEPU') {
                url = 'empShowMasterEmp.aspx?id=' + "WU";
            }
            else {
                url = 'empShowMasterEmp.aspx?id=' + mode;
            }
            var oWnd = radopen(url, "pop_pagecode");
            oWnd.center()


        }

        function OnClientClose1(oWnd, args) {

            var mode = document.getElementById("<%=hf_mode.ClientID%>").value

            var arg = args.get_argument();

            if (arg) {

                if (mode == 'BK') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtBname.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfBank_ID.ClientID %>").value = NameandCode[1];
                    __doPostBack("<%=hfBank_ID.ClientID%>", 'TextChanged');
                }

                else if (mode == 'S') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCountry.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfStatus_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'C') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCountry.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfCountry_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'DC') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtDepCountry.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfDepCountry_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'CC') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtContCurrCountry.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfContCCountry_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'PC') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtContPermCountry.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfContPCountry_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'D') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtDept.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfDept_ID.ClientID %>").value = NameandCode[1];
                }

                else if (mode == 'T') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtTeachGrade.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfTeachGrade_ID.ClientID %>").value = NameandCode[1];
                }

                else if (mode == 'CT') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCat.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfCat_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'AP') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtEmpApprovalPol.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_ApprovalPolicy.ClientID %>").value = NameandCode[1];
                }

                else if (mode == 'SD') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtSD.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfSD_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'ME') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtME.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfME_ID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'ML') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtML.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfML_ID.ClientID %>").value = NameandCode[1];
                }

                else if (mode == 'IU') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtIU.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfIU.ClientID %>").value = NameandCode[1];
                    //             document.getElementById("<%=txtWU.ClientID %>").value=NameandCode[0];
                    //              document.getElementById("<%=hfWU.ClientID %>").value=NameandCode[1]; 


                }
                else if (mode == 'EP') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtReport.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfReport.ClientID %>").value = NameandCode[1];


                }
                else if (mode == 'WU') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtWU.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfWU.ClientID %>").value = NameandCode[1];
                }

                else if (mode == 'DEPU') {

                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtDependUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hf_dependant_unit.ClientID %>").value = NameandCode[1];

                }

                else if (mode == 'WPS') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtWPSDesc.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hf_WPSID.ClientID %>").value = NameandCode[1];


                }
                else if (mode == 'COC') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCostCenter.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hf_CostCenterID.ClientID %>").value = NameandCode[1];


                }
                else if (mode == 'JOB') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtJobMaster.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hf_jobmaster_id.ClientID %>").value = NameandCode[1];
                }
}
}


function GetPayableFromEligible() {
    var eligibleAmt, FTEamt;
    eligibleAmt = document.getElementById("<%=txtSalAmtEligible.ClientID %>").value;
    FTEamt = document.getElementById("<%=txtFTE.ClientID %>").value;
    document.getElementById("<%=txtSalAmount.ClientID %>").value = FTEamt * eligibleAmt;
}

function getPageCode2(mode) {

    if (mode == 'Q') {
    }

    else if (mode == 'SG') {

        var NameandCode;


                <%--result = window.showModalDialog(url, "", sFeatures);
                if (result == '' || result == 'undefined') {
                    return false;
                }
                else {
                    NameandCode = result.split('___');
                    document.getElementById("<%=txtSalGrade.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfSalGrade.ClientID %>").value = NameandCode[1];
                    return true;
                }--%>
            var url = 'empShowOtherDetail.aspx?id=' + mode;
            var oWnd = radopen(url, "pop_PageCode2");
            oWnd.center()

        }

}

function OnClientClose4(oWnd, args) {
    //get the transferred arguments

    var arg = args.get_argument();

    if (arg) {

        NameandCode = arg.NameandCode.split('||');
        document.getElementById("<%=txtSalGrade.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfSalGrade.ClientID %>").value = NameandCode[1];
            __doPostBack('<%=txtSalGrade.ClientID%>', 'TextChanged');
        }
    }



    function getDocDocument(val) {

        document.getElementById("<%=hf_val.ClientID%>").value = val;
            var NameandCode;
            var result;
            var BSU_ID = document.getElementById("<%=hfWU.ClientID %>").value;
            var oWnd = radopen("selIDDesc.aspx?ID=Document&BSU_ID=" + BSU_ID + "&bMAINDOC=" + val, "pop_document");
            oWnd.center()
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                if (val == 1) {
                    document.getElementById('<%=h_DOC_DOCID.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtDocDocument.ClientID %>').value = NameandCode[1];
                }
                else {
                    document.getElementById('<%=h_DOC_DOCID1.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtDocDocument1.ClientID %>').value = NameandCode[1];
                }
                if (NameandCode[0] == '1') {
                    document.getElementById('<%=txtDocDocNo.ClientID %>').value = document.getElementById('<%=h_PASSPORTNO.ClientID %>').value;
                }
            }
            return false;--%>
        }

        function OnClientClose_doc(oWnd, args) {
            //get the transferred arguments
            val = document.getElementById("<%=hf_val.ClientID%>").value;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (val == 1) {
                    document.getElementById('<%=h_DOC_DOCID.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtDocDocument.ClientID %>').value = NameandCode[1];
                    __doPostBack('<%= txtDocDocument.ClientID%>', 'TextChanged');
                }
                else {
                    document.getElementById('<%=h_DOC_DOCID1.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtDocDocument1.ClientID %>').value = NameandCode[1];
                    __doPostBack('<%= txtDocDocument1.ClientID%>', 'TextChanged');
                }
                if (NameandCode[0] == '1') {
                    document.getElementById('<%=txtDocDocNo.ClientID %>').value = document.getElementById('<%=h_PASSPORTNO.ClientID %>').value;
                    __doPostBack('<%= txtDocDocNo.ClientID%>', 'TextChanged');
                }
            }
        }


        function getHRLang() {

            var NameandCode;


            <%--result = window.showModalDialog("selIDDesc.aspx?ID=Language", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_LANG_LANGID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtLanguage.ClientID %>').value = NameandCode[1];

            }
            return false;--%>

            var url = 'selIDDesc.aspx?ID=Language';
            var oWnd = radopen(url, "pop_HRLang");
            oWnd.center()

        }



        function OnClientClose5(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_LANG_LANGID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtLanguage.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtLanguage.ClientID%>', 'TextChanged');
            }
        }



        function getCostUnit() {

            var NameandCode;
            var result;
            <%--result = window.showModalDialog("../Accounts/accShowEmpPP.aspx?id=CU", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=txtContCostUnit.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_ContCostUnitID.ClientID %>').value = NameandCode[1];
            }
            return false;--%>

            var url = '../Accounts/accShowEmpPP.aspx?id=CU';
            var oWnd = radopen(url, "pop_CostUnit");
            oWnd.center()

        }



        function OnClientClose6(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtContCostUnit.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_ContCostUnitID.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtContCostUnit.ClientID%>', 'TextChanged');
            }
        }

        function getCity() {

            var NameandCode;


<%--            result = window.showModalDialog("selIDDesc3Cols.aspx?ID=city", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_AirTcket_City_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtAirticketCity.ClientID %>').value = NameandCode[1];
            }
            return false;--%>

            var url = 'selIDDesc3Cols.aspx?ID=city';
            var oWnd = radopen(url, "pop_Cityname");
            oWnd.center()
        }

        function OnClientClose7(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_AirTcket_City_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtAirticketCity.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtAirticketCity.ClientID%>', 'TextChanged');
            }
        }

        function getFromCity() {

            var NameandCode;

            <%--result = window.showModalDialog("selIDDesc3Cols.aspx?ID=city", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_FromAirTcket_City_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFromAirCity.ClientID %>').value = NameandCode[1];
            }
            return false;--%>

            var url = 'selIDDesc3Cols.aspx?ID=city';
            var oWnd = radopen(url, "pop_FromCityname");
            oWnd.center()
        }

        function OnClientClose8(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_FromAirTcket_City_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFromAirCity.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtFromAirCity.ClientID%>', 'TextChanged');
            }
        }

        function getVacationPolicy() {

            var NameandCode;
            var result;
            var catID;
            catID = document.getElementById('<%=hfCat_ID.ClientID %>').value;
            if (catID != '') {
                var oWnd = radopen("selIDDesc.aspx?ID=VP&CATID=" + catID, "pop_VacationPolicy")
                oWnd.center()
                <%--if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=hf_VacationPolicy.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtVacationPolicy.ClientID %>').value = NameandCode[1];
                }--%>
            }
            //return false;
        }

        function OnClientClose9(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hf_VacationPolicy.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtVacationPolicy.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtVacationPolicy.ClientID%>', 'TextChanged');
            }

        }

        function getEMPQUALIFICATIONCATEGORY() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var oldcat_id = document.getElementById('<%=h_Qual_category_ID.ClientID %>').value;

                result = window.showModalDialog("selIDDesc.aspx?ID=QUALIFICATIONCATEGORY", "", sFeatures)
                if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=h_Qual_category_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtQual_QualifCategory.ClientID %>').value = NameandCode[1];

                if (oldcat_id != document.getElementById('<%=h_Qual_category_ID.ClientID %>').value) {
                    document.getElementById('<%=h_Qual_QualID.ClientID %>').value = "";
                    document.getElementById('<%=txtQualQualification.ClientID %>').value = "";
                }
            }
            return false;
        }

        function getEMPQUALIFICATION() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var cat_id = document.getElementById('<%=h_Qual_category_ID.ClientID %>').value;
            if (cat_id == "") {
                alert('Please select Qualififcation Category first');
                return false;
            }
            result = window.showModalDialog("selIDDesc.aspx?ID=QUALIFICATION&CAT_ID=" + cat_id, "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Qual_QualID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtQualQualification.ClientID %>').value = NameandCode[1];
            }
            return false;
        }
        function getGrade() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

<%--            result = window.showModalDialog("selIDDesc.aspx?ID=EMPGRADE", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Qual_Grade.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtQualGrade.ClientID %>').value = NameandCode[1];
            }
            return false;--%>

            var url = 'selIDDesc.aspx?ID=EMPGRADE';
            var oWnd = radopen(url, "pop_Grade");
            oWnd.center()
        }

        function OnClientClose8(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_FromAirTcket_City_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFromAirCity.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtFromAirCity.ClientID%>', 'TextChanged');
            }
        }

        function AddDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 550px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("empPopupSalarySchedule.aspx?" + url, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            return false;
        }
        function UploadPhoto() {
            //        document.forms[0].Submit();
            var filepath = document.getElementById('<%=FUUploadEmpPhoto.ClientID %>').value;
            if ((filepath != '') && (document.getElementById('<%=h_EmpImagePath.ClientID %>').value != filepath)) {
                document.getElementById('<%=h_EmpImagePath.ClientID %>').value = filepath;
                document.forms[0].submit();
            }
        }
        function UploadDependencePhoto() {
            //        document.forms[0].Submit();
            var filepath = document.getElementById('<%=FUUploadDependance.ClientID %>').value;
            if ((filepath != '') && (document.getElementById('<%=h_EmpDependantImgPath.ClientID %>').value != filepath)) {
                document.getElementById('<%=h_EmpDependantImgPath.ClientID %>').value = filepath;
                document.forms[0].submit();
            }
        }
        function GetEMPDependantName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            <%-- var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN&Bsu_id=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value, "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
            var url = "../Accounts/accShowEmpDetail.aspx?id=EN&Bsu_id=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value;
            var oWnd = radopen(url, "pop_getempdept");
            oWnd.center()
        }

        function OnClientClose10(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=txtDependName.ClientID%>', 'TextChanged');
            }
        }
        function GetStudentSingle() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            <%--var result;
            var url = "../Fees/ShowStudent.aspx?type=NO&bsu=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value;
            result = window.showModalDialog(url, "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('||');

                document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[1];

                return true;
            }
            else {
                return false;
            }
        }--%>
            var url = "../Fees/ShowStudent.aspx?type=NO&bsu=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value
            var oWnd = radopen(url, "pop_getstudsing");
            oWnd.center()
        }

        function OnClientClose11(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtDependName.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_pagecode" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_EMPAttName" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_EMPName" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_PageCode2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_HRLang" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_CostUnit" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose6"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_Cityname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_FromCityname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose8"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_VacationPolicy" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose9"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_document" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_doc"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getempdept" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose10"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getstudsing" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose11"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Employee Master Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="98%">
                    <tr >
                        <td align="left"  ><asp:ValidationSummary ID="ValidationSummary5" runat="server" ValidationGroup="ValDocDet" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="ValQualDet" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValExpDet" />
                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr id="trsearchTable" runat="server" visible="false" >                       
                        <td>
                            <asp:Panel ID="pnlSearchHeader" runat="server" CssClass="collapsePanelHeader" 
                                Width="100%">
                                <div style="width: 100%; padding: 5px; cursor: pointer; vertical-align: top;" class="bluebg">
                                    <asp:ImageButton ID="imgSearchpnl" Style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif"
                                        AlternateText="(Show Search...)" />
                                   <span class="field-label"> Employee Search<asp:Label ID="lblPending" runat="server" Font-Bold="false">(Show 
                  search...)</asp:Label></span>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpePending" runat="Server" CollapseControlID="pnlSearchHeader"
                                Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show search...)"
                                ExpandControlID="pnlSearchHeader" ExpandedImage="~/images/password/collapse.gif"
                                ExpandedText="(Hide search...)" ImageControlID="imgSearchpnl" SuppressPostBack="true"
                                TargetControlID="pnlSearchDetail" TextLabelID="lblPending">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="pnlSearchDetail" runat="server" Width="100%" Visible="true">
                                <table align="left"  cellpadding="3" cellspacing="0" width="100%">
                                    <tr >
                                        <td align="left"  class="title-bg" valign="middle" >
                                            Search Options
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td  valign="top" width="20%"> <span class="field-label">Employee No</span>
                                                    </td>
                                                   
                                                    <td   valign="top" width="30%">
                                                        <asp:TextBox ID="txtEmpNoS" runat="server"  OnTextChanged="txtEmpNoS_TextChanged"></asp:TextBox>
                                                    </td>
                                                    <td valign="top"  align="left" width="20%"><span class="field-label">Name</span>
                                            <asp:Label ID="txttest" runat="server" ></asp:Label>
                                                    </td>
                                                  
                                                    <td style="text-align: left" valign="top" width="30%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="80%">
                                                                    <asp:TextBox ID="txtFNameS" runat="server" CssClass="field-label"  AutoPostBack="True"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnsearch" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClientClick="return clickme('113','ctl00_cphMasterpage_imgEmployee','btnsearch');"
                                                                        CausesValidation="False" />
                                                                    <ajaxToolkit:PopupControlExtender ID="modalEmpPopUp" TargetControlID="txttest" PopupControlID="pnlPopup"
                                                                        runat="server" Position="Center">
                                                                    </ajaxToolkit:PopupControlExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMNameS" runat="server" CssClass="field-label"  Style="display: none;"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtLNameS" runat="server" Style="display: none;"></asp:TextBox>
                                                                    <asp:Button ID="imgEmployee" runat="server" OnClick="imgEmployee_Click" Style="display: none;" />
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="30%">
                                                                </td>
                                                                <td align="center" style="display: none;" width="30%">(Middle)
                                                                </td>
                                                                <td align="center" style="display: none;" width="30%">(Last)
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>                        
                    </tr>                   
                    <tr>
                        <td align="left">Fields Marked with (<span class="text-danger">*</span>) are mandatory
                        </td>
                    </tr>
                    <tr>                        
                        <td>
                            <table align="left" width="100%">
                                <tr >
                                    <td align="left" class="title-bg"  valign="middle" >
                                    <span class="field-label">    Employee Master </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"   rowspan="1" >
                                        <table align="left"  cellpadding="4" cellspacing="0"
                                            style="width: 100%">
                                            <tr>
                                                <td  valign="top" width="10%" > <span class="field-label">Employee No </span>
                                                </td>
                                               
                                                <td   valign="top"  width="20%" >
                                                    <asp:TextBox ID="txtEmpno1" runat="server" ></asp:TextBox>
                                                </td>
                                                <td valign="top"  width="10%" ><span class="field-label">Name</span><font color="#c00000">*</font>
                                                </td>
                                               
                                                <td style="text-align: left" valign="top" nowrap="noWrap"  width="50%" >
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddlEmpSalute" runat="server">
                                                                    <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                                                    <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                                                    <asp:ListItem Value="Miss">Miss</asp:ListItem>
                                                                    <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                                                    <asp:ListItem Value="Dr.">Dr.</asp:ListItem>
                                                                    <asp:ListItem Value="Prof.">Prof.</asp:ListItem>
                                                                    <asp:ListItem Value="Rev.">Rev.</asp:ListItem>
                                                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFname1" runat="server" placeholder="First Name.." ></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMname1" runat="server" placeholder="Middle Name.."></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtLname1" runat="server" placeholder="Last Name.."></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                       <%-- <tr>
                                                            <td></td>
                                                            <td align="center">(First)
                                                            </td>
                                                            <td align="center">(Middle)
                                                            </td>
                                                            <td align="center">(Last)
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                                                    ControlToValidate="txtFName1" ErrorMessage="Only alphabets are allowed in First Name"
                                                                    ForeColor="" ValidationExpression="^[a-zA-Z ]+$" ValidationGroup="groupM1" Display="Dynamic"> </asp:RegularExpressionValidator>
                                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                                             ControlToValidate="txtMname1" ErrorMessage="Only alphabets are allowed in Middle Name"
                                                             ForeColor="" ValidationExpression="^[a-zA-Z ]+$" ValidationGroup="groupM1" Display="Dynamic"> </asp:RegularExpressionValidator>

                                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                                             ControlToValidate="txtLname1" ErrorMessage="Only alphabets are allowed in Last Name"
                                                             ForeColor="" ValidationExpression="^[a-zA-Z ]+$" ValidationGroup="groupM1" Display="Dynamic"> </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="tr_NatNo" runat="server">
                                    <td align="left" rowspan="1" >
                                        <table align="left" width="100%">
                                            <tr>
                                                <td  style="text-align: left;" valign="top" width="20%"><span  class="field-label">National No</span>
                                                </td>
                                               
                                                <td   style="text-align: left; " valign="top" width="30%">
                                                    <asp:TextBox ID="txtNat_No" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>                       
                    </tr>
                    <tr>                       
                        <td align="left" >
                           <%-- <asp:Menu ID="mnuMaster" Orientation="Horizontal" runat="server"  CssClass="menu_a">
                                <Items>
                                    <asp:MenuItem Selected="True" Value="0" Text="MAIN"  ></asp:MenuItem>
                                    <asp:MenuItem Value="1" Text="VISA" ></asp:MenuItem>
                                    <asp:MenuItem Value="2" Text="QUALIFICATIONS" ></asp:MenuItem>
                                    <asp:MenuItem Value="3" Text="EXPERIENCE" ></asp:MenuItem>
                                    <asp:MenuItem Value="4" Text="COMMUNICATION" ></asp:MenuItem>
                                    <asp:MenuItem Value="5" Text="PERSONAL" ></asp:MenuItem>
                                    <asp:MenuItem Value="6" Text="SALARY" ></asp:MenuItem>
                                    <asp:MenuItem Value="7" Text="BENEFITS"  ></asp:MenuItem>
                                    <asp:MenuItem Value="8" Text="OTHERS"></asp:MenuItem>
                                    <asp:MenuItem Value="9" Text="DOCUMENTS" ></asp:MenuItem>
                                </Items>
                               
                                 <StaticMenuItemStyle CssClass="menuItem" />
                                    <StaticSelectedStyle CssClass="selectedItem" />
                                    <StaticHoverStyle CssClass="hoverItem" />
                                 <DynamicSelectedStyle  />
                            </asp:Menu>--%>
                            <asp:Menu ID="mnuMaster" Orientation="Horizontal" runat="server" CssClass="menu_a">
                    <Items>
                        <asp:MenuItem Selected="True" Value="0" Text="MAIN"  ></asp:MenuItem>
                                    <asp:MenuItem Value="1" Text="VISA" ></asp:MenuItem>
                                    <asp:MenuItem Value="2" Text="QUALIFICATIONS" ></asp:MenuItem>
                                    <asp:MenuItem Value="3" Text="EXPERIENCE" ></asp:MenuItem>
                                    <asp:MenuItem Value="4" Text="COMMUNICATION" ></asp:MenuItem>
                                    <asp:MenuItem Value="5" Text="PERSONAL" ></asp:MenuItem>
                                    <asp:MenuItem Value="6" Text="SALARY" ></asp:MenuItem>
                                    <asp:MenuItem Value="7" Text="BENEFITS"   ></asp:MenuItem>
                                    <asp:MenuItem Value="8" Text="OTHERS"></asp:MenuItem>
                                    <asp:MenuItem Value="9" Text="DOCUMENTS" ></asp:MenuItem>
                    </Items>
                   <StaticMenuItemStyle CssClass="menuItem" />
                   <StaticSelectedStyle CssClass="selectedItem" />
                   <StaticHoverStyle CssClass="hoverItem" />
                </asp:Menu>
                        </td>                        
                    </tr>
                    <tr>                        
                        <td class="menu_a border" width="100%">
                            <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">
                                <asp:View ID="vwMain" runat="server" >
                                    <asp:Panel ID="pnlMain" runat="server">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" width="20%" ><span class="field-label">Name as in Passport</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtPassportname" runat="server" ></asp:TextBox>
                                                </td>
                                                <td rowspan="4" valign="top" align="left">
                                                    <asp:CheckBox ID="chkActive" runat="server" Text="Active" CssClass="field-label" /><br />
                                                    <asp:CheckBox ID="chkTempEmp" runat="server" Text="Temporary Employee" CssClass="field-label" /><br />
                                                    <asp:CheckBox ID="chkOverSeasRecr" runat="server" Text="Overseas Recruitment" CssClass="field-label" /><br />
                                                    <asp:CheckBox ID="chkAllowOT" runat="server" Text="Allow Over Time " CssClass="field-label" /><br />
                                                    <asp:CheckBox ID="chkbPunching" runat="server" Text="Attendance Binding" Checked="true" CssClass="field-label" />
                                                </td>
                                                 <td rowspan="4" valign="top" align="left" style="text-align: center">
                                                    <asp:Image ID="imgEmpImage" runat="server" Height="202px" Width="189px" ImageUrl="~/Images/Photos/no_image.gif" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Known As</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtKnownas" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"   width="20%"><span class="field-label">Arabic Name</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtArabicName" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"  ><span class="field-label">Application No</span>
                                                </td>
                                               
                                                <td align="left"  width="30%" >
                                                    <asp:TextBox ID="txtAppNo" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnAppNo" runat="server" ImageUrl="~/Images/forum_search.gif" Visible="False"
                                                        />
                                                </td>
                                                                                              
                                               
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"  >
                                                    <span class="field-label">Present Status</span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" >
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblEmpStatus" CssClass="field-value" runat="server" Visible="False"></asp:Label>
                                                </td>
                                                <td align="left"  width="20%" ><span class="field-label">Add Photo</span> </td>
                                                <td>
                                        <asp:FileUpload ID="FUUploadEmpPhoto" runat="server" ToolTip='Click "Browse" to select the photo. The file size should be less than 250 KB' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Joining Date</span>
                                                    <span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtJdate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton1" TargetControlID="txtJdate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtJdate" TargetControlID="txtJdate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtJdate"
                                                        Display="Dynamic" ErrorMessage="Enter the Joining Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                            ID="CustomValidator1" runat="server" ControlToValidate="txtJdate" CssClass="error"
                                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Joining Date entered is not a valid date"
                                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                                <td align="left"  width="20%" >
                                                    <span class="field-label">Joining Date(Group) </span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtDOJGroup" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton4" TargetControlID="txtDOJGroup">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDOJGroup" TargetControlID="txtDOJGroup"></ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtDOJGroup"
                                                        Display="Dynamic" ErrorMessage="Enter the Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator><span style="color: #c00000">
                                                        </span>
                                                    <asp:CustomValidator ID="CustomValidator4" runat="server" ControlToValidate="txtDOJGroup"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Date entered is not a valid date"
                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                           
                                            <tr id="tr_Passport" runat="server">
                                                <td align="left"  width="20%" ><span class="field-label">Passport No</span><font class="text-danger"><span class="text-danger">*</span></font>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtPassNo" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left"  width="20%"  ><span class="field-label">Nationality</span><font class="text-danger">*</font>
                                                </td>
                                             
                                                <td align="left" >
                                                    <asp:TextBox ID="txtCountry" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnCountry" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('C');return false;"
                                                        />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td align="left"  width="20%" >
                                                   <span class="field-label">Designation</span><font class="text-danger">*</font>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtSD" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnE_Desig" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('SD');return false;"
                                                          />
                                                </td>
                                                <td  align="left"  width="20%" >
                                                    <span class="field-label">Staff No.</span> </td>
                                                <td>
                                                    <asp:TextBox ID="txtStaffNo" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Category</span><font class="text-danger">*</font>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtCat" runat="server"  AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="btnCat" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('CT'); return false;"
                                                          />
                                                </td>
                                                <td align="left"  width="20%" ><span class="field-label">Teaching Grade</span><span class="text-danger"></span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtTeachGrade" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnGrade" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('T');return false;"
                                                          />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Department</span><span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtDept" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnDept" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('D');return false;"
                                                          />
                                                </td>
                                                <td align="left" width="20%"  ><span class="field-label">Cost Center </span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left"   >
                                                    <asp:TextBox ID="txtCostCenter" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnCostCenter" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('COC');return false;"
                                                         />
                                                </td>
                                            </tr>
                                            <tr id="trGrade" runat="server">
                                                <td align="left"  width="20%"  ><span class="field-label">Grade</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"  OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"   width="20%"><span class="field-label">Level</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True" >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" >
                                                    <span class="field-label">Employee Reports To</span><font
                                            class="text-danger">*</font>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtReport" runat="server" OnTextChanged="txtReport_TextChanged"
                                                        AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="btnReport" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;"
                                                          />
                                                </td>
                                                <td align="left"   width="20%"><span class="field-label">Attendance Approved By</span>
                                                </td>
                                             
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtAttendBy" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnAttendBy" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPAttName(); return false;"
                                                          />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Vacation Policy</span> <span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtVacationPolicy" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnVacationPolicy" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getVacationPolicy();return false;"
                                                          />
                                                </td>
                                                <td align="left"  width="20%" ><span class="field-label">Staff Point</span>
                                                </td>
                                                
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlStaffPoint" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Date Of Birth</span><span class="text-danger">*</span>
                                                </td>
                                             
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtEMPDOB" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif"
                                                       />
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender7" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton3" TargetControlID="txtEMPDOB">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender8" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtEMPDOB" TargetControlID="txtEMPDOB">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtEMPDOB"
                                                        Display="Dynamic" ErrorMessage="Enter the DOB in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtEMPDOB"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="DOB entered is not a valid date"
                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                                <td align="left"   width="20%"><span class="field-label">Gender</span><font class="text-danger">*</font>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" CssClass="field-label" Text="Male" />
                                                    <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" CssClass="field-label" Text="Female" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%" ><span class="field-label">Marital Status</span><font class="text-danger">*</font>
                                                </td>
                                            
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlMstatus" runat="server" >
                                                        <asp:ListItem Value="0">Married</asp:ListItem>
                                                        <asp:ListItem Value="1">Single</asp:ListItem>
                                                        <asp:ListItem Value="2">Widowed</asp:ListItem>
                                                        <asp:ListItem Value="3">Divorced</asp:ListItem>
                                                        <asp:ListItem Value="4">Separated</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"   width="20%"><span class="field-label">Religion</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlEmpReligion" runat="server" >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%" ><span class="field-label">Father's Name</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtFather" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left"  width="20%" ><span class="field-label">Mother's Name</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:TextBox ID="txtMother" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"  ><span class="field-label">Blood Group</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlBgroup" runat="server">
                                                        <asp:ListItem Value="--">
                                                        </asp:ListItem>
                                                        <asp:ListItem Value="0">AB+</asp:ListItem>
                                                        <asp:ListItem Value="1">AB-</asp:ListItem>
                                                        <asp:ListItem Value="2">B+</asp:ListItem>
                                                        <asp:ListItem Value="3">A+</asp:ListItem>
                                                        <asp:ListItem Value="4">B-</asp:ListItem>
                                                        <asp:ListItem Value="5">A-</asp:ListItem>
                                                        <asp:ListItem Value="6">O+</asp:ListItem>
                                                        <asp:ListItem Value="7">O-</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"  width="20%"   nowrap="nowrap"><span class="field-label">Personal Email Id </span></td>
                                                <td>
                                        <asp:TextBox ID="txtPersonalMail" runat="server" ></asp:TextBox></td>
                                            </tr>
                                            <tr id="tr1" runat="server" visible="true">
                                                <td align="left"  width="20%"  ><span class="field-label">Country of Birth</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlCOB" runat="server" ></asp:DropDownList>

                                                </td>
                                                <td align="left"  width="20%"  ><span class="field-label">Native Language</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlNativeLang" runat="server" ></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="tr2" runat="server" visible="true">
                                                <td align="left"  width="20%"  ><span class="field-label">Highest level of Education Achieved</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlHighEdu" runat="server" ></asp:DropDownList>

                                                </td>
                                                <td align="left"  width="20%"  ><span class="field-label">No of Dependents</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtNoofDepen" runat="server" Text="0"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftxMNum" runat="server" FilterType="Numbers"
                                                        TargetControlID="txtNoofDepen">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr id="tr4" runat="server" visible="true">
                                                <td align="left"  width="20%"  ><span class="field-label">Dimensions-Department</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlDimDepart" runat="server"></asp:DropDownList>

                                                </td>
                                                <td align="left" width="20%"  ><span class="field-label">Dimensions-Staff Levels</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlDimStaffLvl" runat="server" ></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="tr3" runat="server" >
                                                <td align="left" width="20%"  ><span class="field-label">Position Type</span>
                                                </td>                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlPostype" runat="server" ></asp:DropDownList>
                                                </td>
                                                <td align="left"  width="20%"  ><span class="field-label">Job Master Name</span> 
                                                </td>                                           
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtJobMaster" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="Button2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('JOB');return false;"
                                                          />
                                                </td>
                                            </tr>                                           

                                             <tr id="id_ind" runat="server">
                                                <td align="left"  width="20%" ><span class="field-label">Aadhar Card No</span><span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left" width="30%"  >
                                                    <asp:TextBox ID="txt_AadharCardNo" runat="server"></asp:TextBox>

                                                </td>
                                                <td align="left"  width="20%" ><span class="field-label">PAN Card No</span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txt_PanCardNo" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>

                                             <tr  id="tr7_POS_ID" runat="server">
                                                <td align="left"  width="20%" ><span class="field-label">Position ID</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtPosID" runat="server" Enabled="false"></asp:TextBox>

                                                </td>
                                                <td align="left"  width="20%"  ><span class="field-label">Position Description</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtPosDesc" runat="server"  Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwAmt" runat="server">
                                    <asp:Panel ID="pnlBenifits" runat="server">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" ><span class="field-label">Grade Level</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlGradelevel" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" ><span class="field-label">Leave Approval Policy</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtEmpApprovalPol" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="Button4" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('AP');return false;"
                                                          />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="5"><span class="field-label">Dependance Details</span>
                                                </td>
                                            </tr>
                                            <tr valign="middle">
                                                <td align="left" ><span class="field-label">Relation</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlDependRelation" runat="server" >

                                                        <asp:ListItem Value="0">Spouse</asp:ListItem>
                                                        <asp:ListItem Value="1">Child</asp:ListItem>
                                                        <asp:ListItem Value="2">Father</asp:ListItem>
                                                        <asp:ListItem Value="3">Mother</asp:ListItem>
                                                        <asp:ListItem Value="4">Grand Child</asp:ListItem>
                                                        <asp:ListItem Value="5">Grand Parent</asp:ListItem>
                                                        <asp:ListItem Value="6">Other</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td  align="left" nowrap="nowrap" colspan="2">
                                                    <asp:RadioButtonList ID="rblIsGems" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                        <asp:ListItem Value="N" class="field-label" Selected="True">Non-GEMS</asp:ListItem>
                                                        <asp:ListItem Value="E" class="field-label">GEMS Staff</asp:ListItem>
                                                        <asp:ListItem Value="S" class="field-label">GEMS Student</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                
                                                <td rowspan="3">
                                                    <asp:ImageMap ID="imgDependent" runat="server"  AlternateText="No Image"
                                                        ImageUrl="~/Images/Photos/no_image.gif" style="height:202px;width:189px;border-width:0px;">
                                                    </asp:ImageMap>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Gender</span>
                                                </td>
                                               
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:RadioButton ID="rdDepdMale" runat="server" CssClass="field-label" GroupName="Gender" Text="Male" />
                                                    <asp:RadioButton ID="rdDepdFemale" runat="server" CssClass="field-label" GroupName="Gender" Text="Female" />
                                                </td>
                                                <td align="left"  nowrap="nowrap" ><span class="field-label">Marital Status</span>
                                                </td>
                                              
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:DropDownList ID="ddlDependMstatus" runat="server">
                                                        <asp:ListItem Value="0">Married</asp:ListItem>
                                                        <asp:ListItem Value="1">Single</asp:ListItem>
                                                        <asp:ListItem Value="2">Widowed</asp:ListItem>
                                                        <asp:ListItem Value="3">Divorced</asp:ListItem>
                                                        <asp:ListItem Value="4">Separated</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right"  colspan="6">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" width="15%"><span class="field-label">Photo</span>
                                                            </td>
                                                            
                                                            <td align="left"  colspan="4" nowrap="nowrap">
                                                                <asp:FileUpload ID="FUUploadDependance" runat="server" ToolTip="Click &quot;Browse&quot; to select the photo. The file size should be less than 50 KB"
                                                                     />
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>


                                            <%--<tr>
                                                <td align="left" ></td>
                                                <td  style="width: 4px; color: #1b80b6"></td>
                                                <td align="left"  nowrap="nowrap"></td>
                                                <td align="left"  nowrap="nowrap" style="width: 124px"></td>
                                                <td  style="width: 4px"></td>
                                                <td align="left"  colspan="2" nowrap="nowrap"></td>
                                            </tr>--%>
                                            <tr>
                                                <td align="left" ><span class="field-label">Select Unit</span>
                                                </td>
                                                
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:TextBox ID="txtDependUnit" runat="server" AutoPostBack="True" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnDepUnit" runat="server" ImageUrl="~/Images/forum_search.gif" Enabled="false"
                                                        OnClientClick="getPageCode('DEPU');return false;"  />
                                                </td>
                                                <td align="left"  nowrap="nowrap" ><span class="field-label">Name</span>
                                                </td>
                                               
                                                <td align="left"  nowrap="nowrap" colspan="2">
                                                    <asp:TextBox ID="txtDependName" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="GetEMPDependantName();return false;" />
                                                    <asp:ImageButton ID="imgStudSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="GetStudentSingle();return false;" />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Date of Birth</span>
                                                </td>
                                               
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:TextBox ID="txtDependDOB" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender9" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton7" TargetControlID="txtDependDOB">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender10" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDependDOB" TargetControlID="txtDependDOB">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                        ControlToValidate="txtDependDOB" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Dependance DOB Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator8" runat="server" ControlToValidate="txtLRdate"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Dependance DOB  entered is not a valid date"
                                                        ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtDependDOB"
                                                        ErrorMessage="Dependance DOB Date is not entered" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left"  nowrap="nowrap" ><span class="field-label">Nationality</span></td>
                                                
                                                <td align="left"  colspan="2" nowrap="nowrap">
                                                    <asp:TextBox ID="txtDepCountry" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnDepCountry" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('DC');return false;"
                                                          />
                                                </td>
                                            </tr>
                                            <tr id="trDependInsurance" runat="server">
                                                <td align="left" ><span class="field-label">Insurance Plan</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlDepInsuCardTypes" runat="server" >
                                                    </asp:DropDownList>
                                                </td>
                                                <td colspan="4" align="left" >
                                                    <asp:CheckBox ID="chkDepCompInsu" CssClass="field-label" runat="server" Text="Company paid Insurance"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr id="trConcession" visible="false" runat="server">
                                                <td align="left"  ><span class="field-label">Concession</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:CheckBox ID="ChkDependConcession" CssClass="field-label" runat="server" />
                                                </td>
                                                <td align="left" ><span class="field-label">Student ID</span>
                                                </td>
                                               
                                                <td align="left"   colspan="2">
                                                    <asp:TextBox ID="txtStudID" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trEID1" runat="server">
                                                <td align="left"  ><span class="field-label">EmiratesID No</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtEIDNo" runat="server" TabIndex="18"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">EID Expiry Date</span>
                                                </td>
                                               
                                                <td align="left"   colspan="2">
                                                    <asp:TextBox ID="txtEIDExpryDT" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImgCalEIDEXP" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender11" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImgCalEIDEXP" TargetControlID="txtEIDExpryDT">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender12" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtEIDExpryDT" TargetControlID="txtEIDExpryDT">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                                        ControlToValidate="txtEIDExpryDT" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Emirates ID expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator7" runat="server" ControlToValidate="txtEIDExpryDT"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Emirates ID expiry Date entered is not a valid date"
                                                        ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Passport No</span>
                                                </td>
                                              
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:TextBox ID="txtDepPassportNo" runat="server"></asp:TextBox>
                                                </td>
                                                <td align="left" ><span class="field-label">UID No</span>
                                                </td>
                                               
                                                <td align="left"  nowrap="nowrap" colspan="2">
                                                    <asp:TextBox ID="txtDepUidno" runat="server" ></asp:TextBox>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Visa Issued Date</span>
                                                </td>
                                                
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:TextBox ID="txtDepVissuedate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender13" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton8" TargetControlID="txtDepVissuedate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender14" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDepVissuedate" TargetControlID="txtDepVissuedate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                                        ControlToValidate="txtDepVissuedate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Visa Issued Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator5" runat="server" ControlToValidate="txtDepVissuedate"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Issued Date entered is not a valid date"
                                                        ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                                                </td>
                                                <td align="left" ><span class="field-label">Visa Expiry Date</span>
                                                </td>
                                               
                                                <td align="left"  nowrap="nowrap" colspan="2">
                                                    <asp:TextBox ID="txtDepVexpdate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender15" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton2" TargetControlID="txtDepVexpdate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender16" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDepVexpdate" TargetControlID="txtDepVexpdate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                                        ControlToValidate="txtDepVexpdate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Visa expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtDepVexpdate"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa expiry Date entered is not a valid date"
                                                        ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Visa Issued Place</span>
                                                </td>
                                               
                                                <td align="left"  nowrap="nowrap">
                                                    <asp:TextBox ID="txtDepVissueplace" runat="server" ></asp:TextBox>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="center"  colspan="7">
                                                    <asp:Button ID="btnDependanceAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValDocDet" />
                                        <asp:Button ID="btndependanceCancel" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="7">
                                                    <asp:GridView ID="gvDependancedetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                        EmptyDataText="No Transaction details added yet." Width="100%" DataKeyNames="EDD_bMALE,EDD_MARITALSTATUS">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="EDD_NAME" HeaderText="Name" ReadOnly="True" />
                                                            <asp:BoundField DataField="EDD_RELATION" HeaderText="Relation" />
                                                            <asp:BoundField DataField="EDD_DOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="D O B"
                                                                HtmlEncode="False" />
                                                            <asp:BoundField DataField="EDD_NoofTicket" HeaderText="No. Tickets/Yr" HtmlEncode="False"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="bConcession" HeaderText="Concession" Visible="false" />
                                                            <asp:BoundField DataField="InsuranceCategoryDESC" HeaderText="Insurance Category" />
                                                            <asp:TemplateField HeaderText="Company Paid Insurance">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblIsCompInsu" runat="server" Text='<%# IIF(Eval("EDD_bCompanyInsurance")="True","Yes","No") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="EDD_GEMS_TYPE" HeaderText="GemsType" Visible="false" />
                                                            <asp:BoundField DataField="EDD_GEMS_TYPE_DESCR" HeaderText="With GEMS" />
                                                            <asp:BoundField DataField="EDD_BSU_ID" HeaderText="EDD_BSU_ID" Visible="false" />
                                                            <asp:BoundField DataField="EDD_BSU_NAME" HeaderText="GEMS Unit" />
                                                            <asp:BoundField DataField="EDD_GEMS_ID" HeaderText="GEMS ID" Visible="false" />
                                                            <asp:BoundField DataField="bConcession" HeaderText="Concession" Visible="false" />
                                                            <asp:BoundField DataField="EDC_DOCUMENT_NO" HeaderText="EID NO" />
                                                            <asp:BoundField DataField="EDC_EXP_DT" HeaderText="EID Exp-Date" />
                                                            <asp:BoundField DataField="EDC_Gender" HeaderText="Gender">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="EDD_MSTATUS" HeaderText="MartialStatus" />
                                                            <asp:BoundField DataField="EDD_bPhoto" HeaderText="Photo" />
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDependanceEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkDependanceEdit_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwConnDet" runat="server">
                                    <asp:Panel ID="pnlCommunicationDet" runat="server">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Official Communication Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%" ><span class="field-label">E-mail</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtContEmail" runat="server"  TabIndex="18"></asp:TextBox>
                                                </td>
                                                <td align="left" width="20%" ><span class="field-label">Mobile No</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtContCurrMobNo" runat="server"  TabIndex="5"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2" class="title-bg"><span class="field-label">Current Address</span>
                                                </td>
                                                <td align="left" colspan="2"  class="title-bg"><span class="field-label">Permanent Address</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Address</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContCurAddress" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Address</span>
                                                </td>
                                                
                                                <td align="left" >
                                                    <asp:TextBox ID="txtContPermAddress" runat="server"  TabIndex="6"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">P O Box</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContCurrPOBox" runat="server"  TabIndex="1"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">P O Box</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtContPermPOBox" runat="server"  TabIndex="7"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">City</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContCurrCity" runat="server"  TabIndex="2"></asp:TextBox>
                                                </td>
                                                <td align="left"  "><span class="field-label"><span class="field-label">City</span></span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtContPermCity" runat="server"  TabIndex="8"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Country</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContCurrCountry" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnContCurrentCountry" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('CC');return false;"
                                                         TabIndex="3" />
                                                </td>
                                                <td align="left"  >
                                                   <span class="field-label">Country</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtContPermCountry" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnContPCountry" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('PC');return false;"
                                                         TabIndex="9" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Phone No</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContCurrPhoneNo" runat="server"  TabIndex="4"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Phone No</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContPermPhoneNo" runat="server"  TabIndex="10"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Emergency Contact details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Contact Person(Local)</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContLocalContact" runat="server" TabIndex="19"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Phone No</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:TextBox ID="txtContLocalContNo" runat="server"  TabIndex="20"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Contact Person(Home)</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContHomecont" runat="server"  TabIndex="21"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Phone No</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtContHomecontNo" runat="server"  TabIndex="22"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwSalary" runat="server">
                                    <asp:Panel ID="pnlSalary" runat="server" HorizontalAlign="Left" BorderStyle="None">
                                        <table align="center"  cellpadding="5" cellspacing="0"
                                            width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="left"  ><span class="field-label">Salary Currency</span>
                                                    </td>
                                                  
                                                    <td align="left"  >
                                                        <asp:DropDownList ID="ddlSalC" runat="server" >
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left"  ><span class="field-label">Payment Currency</span>
                                                    </td>
                                                  
                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlPayC" runat="server" >
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"  ><span class="field-label">Payment Mode</span>
                                                    </td>
                                                 
                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlPayMode" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlPayMode_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Cash</asp:ListItem>
                                                            <asp:ListItem Value="1">Bank</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left"  ><span class="field-label">FTE</span>
                                                    </td>
                                                   
                                                    <td align="left"  >
                                                        <asp:TextBox ID="txtFTE" runat="server" ></asp:TextBox>
                                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtFTE"
                                                            ErrorMessage="Enter valid FTE" Operator="DataTypeCheck" Type="Double" ValidationGroup="VALSALDET">*</asp:CompareValidator>
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtFTE"
                                                            ErrorMessage="Enter FTE Range from 0 to 1" MaximumValue="1" MinimumValue="0"
                                                            Type="Double" ValidationGroup="VALSALDET">*</asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td align="left"  ><span class="field-label">Bank Name</span>
                                                    </td>
                                                   
                                                    <td align="left"  nowrap="nowrap">
                                                        <asp:TextBox ID="txtBname" runat="server" AutoPostBack="True"></asp:TextBox>
                                                        <asp:ImageButton ID="btnBank_name" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('BK');return false;"
                                                             OnClick="btnBank_name_Click" />
                                                    </td>
                                                    <td align="left"  ><span class="field-label">Account No</span>
                                                    </td>
                                                   
                                                    <td align="left"  >
                                                        <asp:TextBox ID="txtAccCode" runat="server"  MaxLength="30"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"  ><span class="field-label">Swift Code</span>
                                                    </td>
                                                    
                                                    <td align="left"  >
                                                        <asp:TextBox ID="txtBcode" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left"  ><span class="field-label">Max. child concession</span>
                                                    </td>
                                                  
                                                    <td align="left"  >
                                                        <asp:TextBox ID="txtMaxChildConcession" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"  ><span class="field-label">Transportation</span>
                                                    </td>
                                                   
                                                    <td align="left"  >
                                                        <asp:DropDownList ID="ddlEmpTransportation" runat="server" >
                                                            <asp:ListItem Value="0">Own</asp:ListItem>
                                                            <asp:ListItem Value="1">Company's Transportation</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left"  ><span class="field-label">Salary Scale</span><span class="text-danger"></span>
                                                    </td>
                                                   
                                                    <td align="left"  >
                                                        <asp:TextBox ID="txtSalGrade" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSalScale" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode2('SG'); return false;"
                                                             />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="title-bg" colspan="6">Salary Components
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Earn Code</span>
                                                    </td>
                                                   
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="ddSalEarnCode" runat="server" >
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" width="20%"><span class="field-label">Amount (Eligibility)</span>
                                                    </td>
                                                  
                                                    <td align="left" width="30%">
                                            <asp:TextBox ID="txtSalAmtEligible" runat="server" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtSalAmtEligible"
                                                ErrorMessage="Enter the Eligibility Amount" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"  ><span class="field-label">Amount (Actual)</span>
                                                    </td>
                                                  
                                                    <td align="left"  >
                                                        <asp:TextBox ID="txtSalAmount" runat="server" ></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSalAmount"
                                                            ErrorMessage="Enter the Salary Amount" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator><asp:CheckBox
                                                                ID="chkSalPayMonthly" runat="server" Text="Per Month" />
                                                    </td>
                                                    <td align="left" ><span class="field-label">Frequecy</span>
                                                    </td>
                                                 
                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlayInstallment" runat="server" >
                                                            <asp:ListItem Value="0">Monthly</asp:ListItem>
                                                            <asp:ListItem Value="1">Bimonthly</asp:ListItem>
                                                            <asp:ListItem Value="2">Quarterly</asp:ListItem>
                                                            <asp:ListItem Value="3">Half-yearly</asp:ListItem>
                                                            <asp:ListItem Value="4">Yearly</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trWPSAgent" runat="server" visible="false">
                                                    <td align="left" >
                                                        <span class="field-label">WPS Agent</span>
                                                    </td>
                                               
                                                    <td align="left">
                                                        <asp:TextBox ID="txtWPSDesc" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnWPS" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('WPS');return false;"
                                                             Visible="False" />
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td align="center"  colspan="6">
                                                        <asp:Button ID="btnSalAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSALDET" />
                                            <asp:Button ID="btnSalCancel" runat="server" CssClass="button" Text="Cancel" />
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td align="left"  colspan="8">
                                                        <asp:GridView ID="gvEmpSalary" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                            Width="100%">
                                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                            <Columns>
                                                                <asp:TemplateField Visible="False" HeaderText="Unique ID">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="False" HeaderText="Earn_ID">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Earning">
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblERN_ID" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="False" HeaderText="BMonthly">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBMonthly" runat="server" Text='<%# bind("BMonthly") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Amount(Eligible)">
                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmEligible" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Amount(Actual)">
                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="False" HeaderText="PayTerm">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPayTerm" runat="server" Text='<%# bind("PAYTERM") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Schedule">
                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSchedule" runat="server" Text='<%# bind("PAY_SCHEDULE") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <HeaderTemplate>
                                                                        Edit
                                                                    </HeaderTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkSalEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                            OnClick="lnkSalEdit_Click" Text="Edit"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ShowDeleteButton="True" HeaderText="Delete"></asp:CommandField>
                                                            </Columns>
                                                            <RowStyle CssClass="griditem"  />
                                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                                            <HeaderStyle CssClass="gridheader_new"  />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                        <br />
                                                        
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="field-label">Gross Salary</span></td>
                                                    <td><asp:TextBox ID="txtGsalary" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwVisa" runat="server">
                                    <asp:Panel ID="pnlVisaDetails" runat="server" HorizontalAlign="Left">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" ><span class="field-label">Visa/ Labour card status</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlVstatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVstatus_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">Applied</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">Not Applied</asp:ListItem>
                                                        <asp:ListItem Value="2">Visa Holder</asp:ListItem>
                                                        <asp:ListItem Value="3">LC Holder</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" ><span class="field-label">Visa Type</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlVtype" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rqddlVtype" runat="server" ControlToValidate="ddlVtype"
                                                        InitialValue="5" ErrorMessage="Select Visa Type" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                                                    <%--   <asp:SqlDataSource ID="SqlVisaType" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                            SelectCommand="SELECT [EVM_ID], [EVM_DESCR] FROM [EMPVISATYPE_M]  ORDER BY [EVM_ID] DESC"></asp:SqlDataSource>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Visa Issued Unit</span>
                                                </td>
                                             
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtIU" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnVIss_unit" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('IU');return false;"
                                                         />
                                                </td>
                                                <td align="left" ><span class="field-label">Working Unit</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtWU" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnW_unit" runat="server" ImageUrl="~/Images/forum_search.gif" Enabled="false" OnClientClick="getPageCode('WU');return false;"
                                                         />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">MOL Profession</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtML" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnV_Desig" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('ML');return false;"
                                                         />
                                                </td>
                                                <td align="left" ><span class="field-label">MOE Profession</span>
                                                </td>
                                            
                                                <td align="left" >
                                                    <asp:TextBox ID="txtME" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnMoe_desig" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('ME');return false;"
                                                         />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" >
                                                    <span class="field-label">Visa Prog. ID</span>
                                                </td>
                                             
                                                <td align="left" >
                                                    <asp:TextBox ID="txtVisaPrgID" runat="server"></asp:TextBox>
                                                </td>
                                                <td align="left" ><span class="field-label">Contract Type</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlEmpContractType" runat="server">
                                                        <asp:ListItem Value="0">Limited</asp:ListItem>
                                                        <asp:ListItem Value="1">Unlimited</asp:ListItem>
                                                        <asp:ListItem Value="3">N/A</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" >
                                                    <span class="field-label">Person ID</span>
                                                </td>
                                             
                                                <td align="left" >
                                                    <asp:TextBox ID="txtPersonID" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left" ><span class="field-label">ABC Category</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlCategory" runat="server">
                                                        <asp:ListItem Value="C">C</asp:ListItem>
                                                        <asp:ListItem Value="B">B</asp:ListItem>
                                                        <asp:ListItem Value="A">A</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" >
                                                    <span class="field-label">UID No</span>
                                                </td>
                                               
                                                <td align="left"  colspan="4">
                                                    <asp:TextBox ID="txtUIDNo" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" class="title-bg" colspan="4"><span class="field-label">Document Details</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Document</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocDocument" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgDocDocSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getDocDocument(1);return false;" />
                                                    <asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtDocDocument"
                                                        ErrorMessage="Select Document" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><br />
                                                    <asp:CheckBox ID="chkDocActive" runat="server" Text="Active" />
                                                </td>
                                           
                                                <td align="left" width="20%"><span class="field-label">Document No</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocDocNo" runat="server" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqDocNo" runat="server" ControlToValidate="txtDocDocNo"
                                                        ErrorMessage="Enter Document No " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Document SoftCopy</span>
                                                </td>
                                               
                                                <td align="left">
                                                    <asp:FileUpload ID="FileUploadDoc" runat="server" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Only .pdf,.png,.gif,.jpeg|.jpg files are allowed."
                                                        ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.png|.gif|.jpeg|.jpg|.PDF|.PNG|.GIF|.JPEG|.JPG)$"
                                                        ControlToValidate="FileUploadDoc" ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                    <%--.doc|.docx|.pdf|.png|.gif|.jpeg|.jpg|.DOC|.DOCX|.PDF|.PNG|.GIF|.JPEG|.JPG--%>
                                                    <%--       <ajaxToolkit:AsyncFileUpload ID="AsyncFileUpload1" runat="server" />--%>
                                                </td>
                                            
                                                <td align="left" ><span class="field-label">Issue Place</span>
                                                </td>
                                               
                                                <td align="left">
                                                    <asp:TextBox ID="txtDocIssuePlace" runat="server" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqIssuePlace" runat="server" ControlToValidate="txtDocIssuePlace"
                                                        ErrorMessage="Enter Issue place" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Issue Date</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:TextBox ID="txtDocIssueDate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgDocIssueDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender17" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDocIssueDate" TargetControlID="txtDocIssueDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender18" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDocIssueDate" TargetControlID="txtDocIssueDate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtDocIssueDate"
                                                        ErrorMessage="Please enter Issue Date " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDocIssueDate"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td align="left" ><span class="field-label">Exp. Date</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:TextBox ID="txtDocExpDate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgDocExpDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                       />
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender19" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDocExpDate" TargetControlID="txtDocExpDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender20" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDocExpDate" TargetControlID="txtDocExpDate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtDocExpDate"
                                                        ErrorMessage="Please enter Exp. Date " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                            ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtDocExpDate"
                                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                            ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"  colspan="4">
                                                    <asp:Button ID="btnDocAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValDocDet" />
                                                    <asp:Button ID="btnDocCancel" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="6" valign="top">
                                                    <asp:GridView ID="gvEMPDocDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Document" HeaderText="Document" ReadOnly="True" />
                                                            <asp:BoundField DataField="Doc_No" HeaderText="Document No" />
                                                            <asp:BoundField DataField="Doc_IssuePlace" HeaderText="Issue Place" />
                                                            <asp:TemplateField HeaderText="Issue Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# GetDate(Container.DataItem("Doc_IssueDate")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Exp. Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# GetDate(Container.DataItem("Doc_ExpDate")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDocEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkDocEdit_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    View
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDocSelect" runat="server" CausesValidation="false" CommandName="Select"
                                                                        OnClick="lnkDocSelect_Click" Text="View"></asp:LinkButton>
                                                                    <asp:HiddenField ID="HF_DocFileMIME" runat="server" Value='<%# Eval("DocFileMIME") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" Visible="false">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwQualification" runat="server">
                                    <asp:Panel ID="pnlQualificationDet" runat="server" HorizontalAlign="Left">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Qualification Category</span><span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlQualCatagory" runat="server" AutoPostBack="true" >
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtQual_QualifCategory" runat="server" Visible="False"></asp:TextBox>
                                                    <asp:ImageButton ID="imgQualCategory" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="return getEMPQUALIFICATIONCATEGORY();" Visible="False" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlQualCatagory"
                                                        ErrorMessage="Select Qualification Category" ValidationGroup="ValQualDet" InitialValue="0">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Qualification Level</span><span class="text-danger">*</span>
                                                </td>
                                                <td align="left"  width="30%">
                                                    <asp:DropDownList ID="ddlQualificationLevel" runat="server" >
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtQualQualification" runat="server"  Visible="False"></asp:TextBox><asp:ImageButton
                                                        ID="imgSelQualification" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="return getEMPQUALIFICATION();" Visible="False" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlQualificationLevel"
                                                        ErrorMessage="Select Qualification level" ValidationGroup="ValQualDet" InitialValue="0">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td align="left" ><span class="field-label">Degree/Certificate</span>
                                                </td>
                                               
                                                <td align="left" colspan="3">
                                                    <asp:RadioButtonList ID="RbtnDegreeCerti" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                                        Font-Bold="True" RepeatDirection="Horizontal">
                                                    </asp:RadioButtonList>
                                                </td>
                                               
                                            </tr>
                                            
                                            <tr>
                                                <td align="left" ><span class="field-label">Degree/Certificate</span>
                                                </td>
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlECertificateType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlECertificateType_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" ><span class="field-label">Qualification</span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <telerik:RadComboBox ID="ddlQualDegreeCert" runat="server" RenderMode="Lightweight" Width="100%"  Filter="Contains"
                                                        AutoPostBack="True">
                                                    </telerik:RadComboBox>

                                                    <telerik:RadTextBox ID="txtOtherQualification" runat="server" EmptyMessage="Enter Other Details" Visible="false"></telerik:RadTextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="ddlQualDegreeCert"
                                                        ErrorMessage="Select Qualification Degree/Certificate" InitialValue="---Select--"
                                                        ValidationGroup="ValQualDet">*</asp:RequiredFieldValidator>
                                                </td>
                                                
                                            </tr>
                                            
                                            <tr>
                                                <td align="left" ><span class="field-label">Specialization</span>
                                                </td>
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtQualSpecialization" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left" ><span class="field-label">Awarding Body</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:TextBox ID="txtQualAwardingBody" runat="server" ></asp:TextBox>
                                                </td>
                                                
                                            </tr>
                                           
                                            <tr>
                                                <td align="left" ><span class="field-label">Institute</span><span class="text-danger">*</span>
                                                </td>
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtQualInstitute" runat="server" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtQualInstitute"
                                                        Enabled="False" ErrorMessage="Enter Institute" ValidationGroup="ValQualDet">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" ><span class="field-label">Location</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtQualLocation" runat="server" ></asp:TextBox>
                                                </td>
                                                
                                            </tr>
                                           
                                            <tr>
                                                <td align="left" ><span class="field-label">Year of Qualification</span>
                                                </td>
                                                 <td align="left" >
                                                    <asp:DropDownList ID="ddlQualYear" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" ><span class="field-label">Start Date</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtQualCertStartDate" runat="server" AutoPostBack="false"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCertStartDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        TabIndex="4" />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="txtQualCertStartDate" TargetControlID="txtQualCertStartDate" PopupPosition="BottomLeft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgCertStartDate" TargetControlID="txtQualCertStartDate" PopupPosition="BottomLeft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                                        ControlToValidate="txtQualCertStartDate" ErrorMessage="Not a valid date" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="ValQualDet" Foreclass="text-danger">*</asp:RegularExpressionValidator>

                                                </td>
                                                 
                                            </tr>
                                            
                                            <tr>
                                                <td align="left" ><span class="field-label">Expiry Date</span>
                                                </td>
                                                 <td align="left"  >
                                                    <asp:TextBox ID="txtQualCertExpDate" runat="server" AutoPostBack="false"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCertExpDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        TabIndex="4" />
                                                    <ajaxToolkit:CalendarExtender ID="CertExpDate" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="txtQualCertExpDate" TargetControlID="txtQualCertExpDate" PopupPosition="BottomLeft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgCertExpDate" TargetControlID="txtQualCertExpDate" PopupPosition="BottomLeft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                                        ControlToValidate="txtQualCertExpDate" ErrorMessage="Not a valid date" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="ValQualDet" Foreclass="text-danger">*</asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RFV_txtQlExpDate" runat="server" ControlToValidate="txtQualCertExpDate"
                                                        ErrorMessage="Expiry Date Required" SetFocusOnError="True" ValidationGroup="ValQualDet"
                                                        Foreclass="text-danger">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" ><span class="field-label">Registration/Ref No.</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtQualRegNo" runat="server" ></asp:TextBox>
                                                </td>
                                                 
                                            </tr>
                                           
                                            <tr>
                                                <td align="left" ><span class="field-label">Remarks</span>
                                                </td>
                                                 <td align="left"  >
                                                    <asp:TextBox ID="txtQualRemarks" runat="server"  SkinID="MultiText"></asp:TextBox>
                                                </td>
                                                <td align="left" ><span class="field-label">Certificate Upload</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:FileUpload ID="FileUploadQualCertificate" runat="server"  />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                        ErrorMessage="Only .pdf,.png,.gif,.jpeg,.jpg files are allowed." ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.png|.gif|.jpeg|.jpg|.PDF|.PNG|.GIF|.JPEG|.JPG)$"
                                                        ControlToValidate="FileUploadQualCertificate" ValidationGroup="ValQualDet">*</asp:RegularExpressionValidator>
                                                </td>
                                                
                                            </tr>
                                           
                                            <tr>
                                                <td align="left" ><span class="field-label">Grade</span>
                                                </td>
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtQualGrade" runat="server" ></asp:TextBox>
                                                    
                                                </td>
                                                <td align="left" ><span class="field-label">Verification Status</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddQualVStatus" runat="server" >
                                                        <asp:ListItem>Verified</asp:ListItem>
                                                        <asp:ListItem>Failed</asp:ListItem>
                                                        <asp:ListItem>Pending</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td align="center"  colspan="4" >
                                                    <asp:Button ID="btnQualAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValQualDet" />
                                                    <asp:Button ID="btnQualCancel" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="4" valign="top">
                                                    <asp:ValidationSummary ID="ValidationSummary6" runat="server" ValidationGroup=" ValQualDet" />
                                                    <asp:GridView ID="gvQualification" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Qualification" HeaderText="Qualification" ReadOnly="True" />
                                                            <asp:BoundField DataField="Institute" HeaderText="Institute" />
                                                            <asp:BoundField DataField="Grade" HeaderText="Grade" />
                                                            <asp:BoundField DataField="VStatus" HeaderText="Verification Status" />
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkQualEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkQualEdit_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Document">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkViewQualDoc" runat="server" OnClick="lnkViewQualDoc_Click">View</asp:LinkButton>
                                                                    <asp:HiddenField ID="HF_QualDocFileMIME" Value='<%# bind("DocFileMIME") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwExperience" runat="server">
                                    <asp:Panel ID="pnlExperience" runat="server" HorizontalAlign="Left">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Organisation</span>
                                                </td>
                                                
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtExpOrganisation" runat="server" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtExpOrganisation"
                                            ErrorMessage="Enter Organisation" ValidationGroup="ValExpDet">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Designation</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtExpDesignation" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtExpDesignation"
                                                        ErrorMessage="Enter designation" ValidationGroup="ValExpDet">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            
                                            <tr >
                                                <td align="left" width="20%"><span class="field-label">From Date</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtExpFromDate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgExpFromDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                       />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender21" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgExpFromDate" TargetControlID="txtExpFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender22" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtExpFromDate" TargetControlID="txtExpFromDate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtExpFromDate"
                                                        ErrorMessage="Enter From Date" ValidationGroup="ValExpDet">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left"  width="20%"><span class="field-label">To Date</span>
                                                </td>
                                              
                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtExpToDate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgExpToDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender23" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgExpToDate" TargetControlID="txtExpToDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender24" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtExpToDate" TargetControlID="txtExpToDate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtExpToDate"
                                                        ErrorMessage="Enter To Date">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Address</span>
                                                </td>
                                               
                                                <td align="left">
                                                    <asp:TextBox ID="txtExpAddress" runat="server" CssClass="MultiText" MaxLength="100"
                                                        TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtExpAddress"
                                                        ErrorMessage="Enter Address" ValidationGroup="ValExpDet">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Reference 1</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtExpRefer1" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left"  width="20%"><span class="field-label">Phone No</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtExpReferPhNo1" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Reference 2</span>
                                                </td>
                                                
                                                <td align="left" >
                                                    <asp:TextBox ID="txtExpRefer2" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Phone No</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:TextBox ID="txtExpReferPhNo2" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Remarks</span>
                                                </td>
                                               
                                                <td align="left">
                                                    <asp:TextBox ID="txtExpRemarks" runat="server" CssClass="MultiText" 
                                                        MaxLength="1000" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="center"  colspan="6" >
                                                    <asp:Button ID="btnExpAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValExpDet" />
                                                    <asp:Button ID="btnExpCancel" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="6" valign="top">
                                                    <asp:GridView ID="gvExperience" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Organisation" HeaderText="Organisation" ReadOnly="True" />
                                                            <asp:BoundField DataField="Address" HeaderText="Address" />
                                                            <asp:BoundField DataField="FromDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                                                HtmlEncode="False" />
                                                            <asp:BoundField DataField="ToDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="To Date"
                                                                HtmlEncode="False" />
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkExpEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkExpEdit_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwAsset" runat="server">
                                    <asp:Panel ID="pnlOthers" runat="server">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Last Annual vacation From</span>
                                                </td>
                                              
                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtViss_date" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnViss_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender25" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnViss_date" TargetControlID="txtViss_date">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender26" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtViss_date" TargetControlID="txtViss_date">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                            runat="server" ControlToValidate="txtViss_date" Display="Dynamic" EnableViewState="False"
                                                            ErrorMessage="Enter Vacation  From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                                ID="cvIss_date" runat="server" ControlToValidate="txtViss_date" CssClass="error"
                                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Vacation From Date entered is not a valid date"
                                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Last vacation Till</span>
                                                </td>
                                               
                                                <td align="left"  width="30%">
                                                    <asp:TextBox ID="txtVExp_date" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgbtnVexp_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender27" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgbtnVexp_date" TargetControlID="txtVExp_date">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender28" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtVExp_date" TargetControlID="txtVExp_date">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                            runat="server" ControlToValidate="txtVExp_date" Display="Dynamic" EnableViewState="False"
                                                            ErrorMessage="Enter the Vacation To  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                                ID="cvExp_date" runat="server" ControlToValidate="txtVExp_date" CssClass="error"
                                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Vacation To Date entered is not a valid date and must be greater than Issue Date"
                                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                    <asp:SqlDataSource ID="SqlDataSourceEligibility" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                                        SelectCommand="SELECT [ELG_ID], [ELG_DESCR] FROM [ELIGIBILITY_M] order by ELG_ID"></asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Rejoining Date after vacation</span>
                                                </td>                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtLRdate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnLrejoingdate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender29" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnLrejoingdate" TargetControlID="txtLRdate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender30" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtLRdate" TargetControlID="txtLRdate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="revLjoing" runat="server" ControlToValidate="txtLRdate"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cvLrejoining" runat="server" ControlToValidate="txtLRdate"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Rejoining Date entered is not a valid date"
                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                                <td align="left"  ><span class="field-label">Opening LOP Days</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtLOPOpening" runat="server" ></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtLOPOpening"
                                            ErrorMessage="Enter only numbers" Operator="DataTypeCheck" Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr class="text-danger">
                                                <td align="left" ><span class="field-label">Date of Resignation</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtResig" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnReg" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender31" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnReg" TargetControlID="txtResig">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender32" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtResig" TargetControlID="txtResig">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="revResignPeriod" runat="server" ControlToValidate="txtResig"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cv" runat="server" ControlToValidate="txtResig" CssClass="error"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Resignation Period entered is not a valid date"
                                                        ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                                <td align="left"  >
                                                    <font class="text-danger"><span class="field-label">Last working Date</span></font>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtLastPresentDate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/calendar.gif"
                                                     />
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender33" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton6" TargetControlID="txtLastPresentDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender34" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtLastPresentDate" TargetControlID="txtLastPresentDate">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                        ControlToValidate="txtLastPresentDate" Display="Dynamic" EnableViewState="False"
                                                        ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator6" runat="server" ControlToValidate="txtLastPresentDate"
                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Last Present Date entered is not a valid date"
                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Probationary Period Till</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtProb" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnProb" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        />
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender35" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnProb" TargetControlID="txtProb">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender36" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtProb" TargetControlID="txtProb">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="revProb" runat="server" ControlToValidate="txtProb"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Probationary Period in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cvProb" runat="server" ControlToValidate="txtProb" CssClass="error"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Probationary Period entered is not a valid date"
                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                </td>
                                                <td width="20%"><span class="field-label">Remark</span></td>
                                                <td width="30%"><asp:TextBox ID="txtRemark" runat="server" CssClass="MultiText" MaxLength="300" SkinID="MultiText"
                                                        TextMode="MultiLine"></asp:TextBox></td>
                                            </tr>
                                            
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Accomodation Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Accomodation </span><font class="text-danger">*</font>
                                                </td>
                                              
                                                <td align="left"  width="30%">
                                                    <asp:DropDownList ID="ddlAccom" runat="server"  TabIndex="12">
                                                        <asp:ListItem Value="0">Own Accomodation</asp:ListItem>
                                                        <asp:ListItem Value="1">Company Single Accomodation</asp:ListItem>
                                                        <asp:ListItem Value="2">Company Family Accomodation</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"  width="20%"><span class="field-label">Accomodation Type</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlContAccomType" runat="server" TabIndex="13">
                                                        <asp:ListItem Value="0">1 Bed Room</asp:ListItem>
                                                        <asp:ListItem Value="1">2 Bed Room</asp:ListItem>
                                                        <asp:ListItem Value="2">3 Bed Room</asp:ListItem>
                                                        <asp:ListItem Value="3">Villa</asp:ListItem>
                                                        <asp:ListItem Value="4">Studio</asp:ListItem>
                                                        <asp:ListItem Value="5">Sharing</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Accomodation Location</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContAccomLocation" runat="server"  TabIndex="14"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Property Ref.</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContCostUnit" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCostUnit();return false;"
                                                        TabIndex="15" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  ><span class="field-label">Flat No</span>
                                                </td>
                                                
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtContFlatNo" runat="server"  TabIndex="16"></asp:TextBox>
                                                </td>
                                                <td align="left"  ><span class="field-label">Bachelor Sharing</span>
                                                </td>
                                              
                                                <td align="left"  ><asp:CheckBox ID="chkContBSharing" runat="server" TabIndex="17" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Languages Fluent In
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Language</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtLanguage" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgHRLang" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getHRLang();return false;" />
                                                </td>
                                                <td width="20%"></td>
                                                <td width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="6">
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td align="left"  width="15%"><span class="field-label">Speak</span>
                                                            </td>
                                                           
                                                            
                                                                <td align="left"  width="15%">
                                                                    <asp:DropDownList ID="ddlSpeak" runat="server" TabIndex="12">
                                                                        <asp:ListItem Value="0">-</asp:ListItem>
                                                                        <asp:ListItem Value="1">Intermediate</asp:ListItem>
                                                                        <asp:ListItem Value="2">Fluent</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="15%" ><span class="field-label">Read</span>
                                                                </td>
                                                               
                                                                <td align="left"  width="15%">
                                                                    <asp:DropDownList ID="ddlRead" runat="server" TabIndex="13">
                                                                        <asp:ListItem Value="0">-</asp:ListItem>
                                                                        <asp:ListItem Value="1">Intermediate</asp:ListItem>
                                                                        <asp:ListItem Value="2">Fluent</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left"  width="15%"><span class="field-label">Write</span>
                                                                </td>
                                                                
                                                                <td align="left" width="20%" >
                                                                    <asp:DropDownList ID="ddlWrite" runat="server" TabIndex="13">
                                                                        <asp:ListItem Value="0">-</asp:ListItem>
                                                                        <asp:ListItem Value="1">Intermediate</asp:ListItem>
                                                                        <asp:ListItem Value="2">Fluent</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"  colspan="6">
                                                    <asp:Button ID="btnLangAdd" runat="server" CssClass="button" Text="Add" />
                                                    <asp:Button ID="btnLangCancel" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="6" valign="top">
                                                    <asp:GridView ID="gvLang" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Language" HeaderText="Language" ReadOnly="True" />
                                                            <asp:BoundField DataField="Speak" HeaderText="Speak" />
                                                            <asp:BoundField DataField="Read" HeaderText="Read" />
                                                            <asp:BoundField DataField="Write" HeaderText="Write" />
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkLangEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkLangEdit_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6" rowspan="1">Assets Allocated
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="6" valign="top">
                                                    <asp:GridView ID="gvAssetAllocated" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:BoundField DataField="EAD_ISSUE_DT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Issue Date"
                                                                HtmlEncode="False" />
                                                            <asp:BoundField DataField="ASSETNAME" HeaderText="Fixed Asset" ReadOnly="True" />
                                                            <asp:BoundField DataField="EAD_VALUE" HeaderText="Asset Value" />
                                                            <asp:BoundField DataField="EAD_STATUS" HeaderText="Status" />
                                                            <asp:BoundField DataField="EAD_REMARKS" HeaderText="Remarks" />
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwNewBenefits" runat="server">
                                    <asp:Panel ID="pnlNewBenefits" runat="server">
                                        <table align="center"  cellpadding="5" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Medical Insurance Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%">
                                                    <span class="field-label">Insurance Category</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlInsuCardTypes" runat="server" >
                                                    </asp:DropDownList>
                                                </td>
                                                <td  width="20%"></td>
                                                <td width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  nowrap="nowrap"><span class="field-label">Insurance (Eligible)</span><span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlInsuEligible" runat="server"  DataSourceID="SqlDataSourceEligibility"
                                                        DataTextField="ELG_DESCR" DataValueField="ELG_ID">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" ><span class="field-label">Insurance (Actual)</span><span class="text-danger">*</span>
                                                </td>
                                              
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlInsuActual" runat="server"  DataSourceID="SqlDataSourceEligibility"
                                                        DataTextField="ELG_DESCR" DataValueField="ELG_ID">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Airticket Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Ticket Class</span>
                                                </td>
                                               
                                                <td align="left"  width="30%">
                                                    <asp:DropDownList ID="ddlAirTicketClass" runat="server" >
                                                        <asp:ListItem Value="E">Economy</asp:ListItem>
                                                        <asp:ListItem Value="B">Business</asp:ListItem>
                                                        <asp:ListItem Value="F">First Class</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"  width="20%"><span class="field-label">No of times Tickets paid/Year</span>
                                        <br />
                                                    <span class="text-info">0.5  - for once
                                            in 2 years,<br />
                                                        1   -for once in a year </span>
                                                </td>
                                               
                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtAirTicketNo" runat="server" ToolTip="0.5 (ticket once in 2 years), 1 (ticket once in a year)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Tickets (Eligible)</span><span class="field-label"><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlAirTicketAvailable" runat="server" DataSourceID="SqlDataSourceEligibility"
                                                        DataTextField="ELG_DESCR" DataValueField="ELG_ID">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"  ><span class="field-label">Tickets (Actual)</span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlTicketActual" runat="server"  DataSourceID="SqlDataSourceEligibility"
                                                        DataTextField="ELG_DESCR" DataValueField="ELG_ID">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">From City</span>
                                                </td>
                                              
                                                <td align="left" >
                                                    <asp:TextBox ID="txtFromAirCity" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnFromAirTicketCity" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getFromCity();return false;"
                                                          />
                                                </td>
                                                <td align="left"  ><span class="field-label">Destination City</span>
                                                </td>
                                               
                                                <td align="left"  >
                                                    <asp:TextBox ID="txtAirticketCity" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnAirTicketCity" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCity();return false;"
                                                          />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6"><span class="field-label">Tuition Fee Concession Details</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%" ><span class="field-label">Eligibility Category</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlConEligibility" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="20%"></td>
                                                <td width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%" nowrap="nowrap"><span class="field-label">No. of Children (Eligible)</span><span class="text-danger">*</span>
                                                </td>
                                                
                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtFeeEligible" runat="server" ></asp:TextBox>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">No. of Children (Actual)</span><span class="text-danger">*</span>
                                                </td>
                                               
                                                <td align="left"  width="30%"><asp:TextBox ID="txtFeeActual" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Concession Availed � Data from Fee module
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" >
                                                    <asp:GridView runat="server" ID="gvFeeConcessionDetails" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%" AutoGenerateColumns="False"> 

                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <Columns>
                                                            <asp:BoundField DataField="STUDENT NO" HeaderText="Student Id." />
                                                            <asp:BoundField DataField="NAME" HeaderText="Name" />
                                                            <asp:BoundField DataField="Concession Unit" HeaderText="Concession Unit" />
                                                            <asp:BoundField DataField="GRADE" HeaderText="Grade" />
                                                            <asp:BoundField DataField="CURRICULUM" HeaderText="Curriculum" />
                                                            <asp:BoundField DataField="ACADEMIC YEAR" HeaderText="Academic Year" />
                                                            <asp:BoundField DataField="FEE TYPE" HeaderText="Fee Type" />
                                                            <asp:BoundField DataField="ACTUAL AMOUNT" HeaderText="Actual Fee" />
                                                            <asp:BoundField DataField="CONCESSION AMOUNT" HeaderText="Concession Amount" />
                                                            <asp:BoundField DataField="CONC PERC" HeaderText="Concession Percent" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwDocuments" runat="server">
                                    <asp:Panel ID="pnlDocDetails" runat="server" HorizontalAlign="Left">
                                        <table align="center"  cellpadding="5" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="left" class="title-bg" colspan="6">Document Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Document</span>
                                                </td>
                                            
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocDocument1" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgDocDocSel1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getDocDocument(0);return false;" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtDocDocument1"
                                                        ErrorMessage="Select Document" ValidationGroup="ValDocDet1">*</asp:RequiredFieldValidator><br />
                                                    <asp:CheckBox ID="chkDocActive1" runat="server" Text="Active" />
                                                </td>
                                            
                                                <td align="left" width="20%" ><span class="field-label">Document No</span>
                                                </td>
                                              
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocDocNo1" runat="server" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtDocDocNo1"
                                                        ErrorMessage="Enter Document No " ValidationGroup="ValDocDet1">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Document SoftCopy</span>
                                                </td>
                                              
                                                <td align="left">
                                                    <asp:FileUpload ID="FileUploadDoc1" runat="server" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                                        ErrorMessage="Only .doc , .docx or .txt files are allowed." ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.doc|.docx|.pdf|.png|.gif|.jpeg|.jpg|.DOC|.DOCX|.PDF|.PNG|.GIF|.JPEG|.JPG)$"
                                                        ControlToValidate="FileUploadDoc1" ValidationGroup="ValDocDet1">*</asp:RegularExpressionValidator>
                                                    <%--       <ajaxToolkit:AsyncFileUpload ID="AsyncFileUpload1" runat="server" />--%>
                                                </td>
                                            
                                                <td align="left" ><span class="field-label">Issue Place</span>
                                                </td>
                                              
                                                <td align="left">
                                                    <asp:TextBox ID="txtDocIssuePlace1" runat="server" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtDocIssuePlace1"
                                                        ErrorMessage="Enter Issue place" ValidationGroup="ValDocDet1">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Issue Date</span>
                                                </td>
                                               
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocIssueDate1" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender37" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton5" TargetControlID="txtDocIssueDate1">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender38" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDocIssueDate1" TargetControlID="txtDocIssueDate1">
                            </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtDocIssueDate1"
                                                        ErrorMessage="Please enter Issue Date " ValidationGroup="ValDocDet1">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                        ControlToValidate="txtDocIssueDate1" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet1">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Exp. Date</span>
                                                </td>
                                             
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocExpDate1" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgDocExpDate1" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender39" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDocExpDate1" TargetControlID="txtDocExpDate1">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender40" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDocExpDate1" TargetControlID="txtDocExpDate1">
                            </ajaxToolkit:CalendarExtender>
                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtDocExpDate1"
                                            ErrorMessage="Please enter Exp. Date " ValidationGroup="ValDocDet1">*</asp:RequiredFieldValidator>--%>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                                        ControlToValidate="txtDocExpDate" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"  colspan="6">
                                                    <asp:Button ID="btnDocAdd1" runat="server" CssClass="button" Text="Add" ValidationGroup="ValDocDet1" />
                                                    <asp:Button ID="btnDocCancel1" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="6" valign="top">
                                                    <asp:GridView ID="gvEMPDocDetails1" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Document" HeaderText="Document" ReadOnly="True" />
                                                            <asp:BoundField DataField="Doc_No" HeaderText="Document No" />
                                                            <asp:BoundField DataField="Doc_IssuePlace" HeaderText="Issue Place" />
                                                            <asp:TemplateField HeaderText="Issue Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# GetDate(Container.DataItem("Doc_IssueDate")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Exp. Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Doc_ExpDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDocEdit1" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkDocEdit1_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    View
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDocSelect1" runat="server" CausesValidation="false" CommandName="Select"
                                                                        OnClick="lnkDocSelect1_Click" Text="View"></asp:LinkButton>
                                                                    <asp:HiddenField ID="HF_DocFileMIME" runat="server" Value='<%# Eval("DocFileMIME") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" Visible="false">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                </asp:View>
                            </asp:MultiView>
                        </td>                       
                    </tr>                  
                    <tr>                        
                        <td  valign="middle" align="center" >
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                        ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                            CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False"
                                                CssClass="button" Text="Delete" />
                        </td>                       
                    </tr>
                    <tr>                       
                        <td align="center"  >
                            <asp:HiddenField ID="h_LANG_LANGID" runat="server" />
                            <asp:HiddenField ID="hfBank_ID" runat="server" OnValueChanged="hfBank_ID_ValueChanged" />
                            <asp:HiddenField ID="hfCountry_ID" runat="server" />
                            <asp:HiddenField ID="hfDepCountry_ID" runat="server" />
                            <asp:HiddenField ID="hfDept_ID" runat="server" />
                            <asp:HiddenField ID="hfTeachGrade_ID" runat="server" />
                            <asp:HiddenField ID="hfGrade_ID" runat="server" />
                            <asp:HiddenField ID="hfCat_ID" runat="server" />
                            <asp:HiddenField ID="hfSD_ID" runat="server" />
                            <asp:HiddenField ID="hfME_ID" runat="server" />
                            <asp:HiddenField ID="hfML_ID" runat="server" />
                            <asp:HiddenField ID="hfIU" runat="server" />
                            <asp:HiddenField ID="hfWU" runat="server" />
                            <asp:HiddenField ID="hfQul" runat="server" />
                            <asp:HiddenField ID="hfSalGrade" runat="server" />
                            <asp:HiddenField ID="hfReport" runat="server" />
                            <asp:HiddenField ID="hfStatus_ID" runat="server" />
                            <asp:HiddenField ID="hfContPCountry_ID" runat="server" />
                            <asp:HiddenField ID="hfContCCountry_ID" runat="server" />
                            <asp:HiddenField ID="h_AirTcket_City_ID" runat="server" />
                            <asp:HiddenField ID="h_Qual_QualID" runat="server" />
                            <asp:HiddenField ID="h_Qual_Grade" runat="server" />
                            <asp:HiddenField ID="h_DOC_DOCID" runat="server" />
                            <asp:HiddenField ID="h_DOC_DOCID1" runat="server" />
                            <asp:HiddenField ID="h_EmpImagePath" runat="server" />
                            <asp:HiddenField ID="h_EmpDependantImgPath" runat="server" />
                            <asp:HiddenField ID="h_ApprovalPolicy" runat="server" />
                            <asp:HiddenField ID="h_ContCostUnitID" runat="server" />
                            <asp:HiddenField ID="hf_VacationPolicy" runat="server" />
                            <asp:HiddenField ID="h_Qual_category_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_PASSPORTNO" runat="server" />
                            <asp:HiddenField ID="hf_WPSID" runat="server" />
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <asp:HiddenField ID="hfAttendBy" runat="server" />
                            <asp:HiddenField ID="hf_CostCenterID" runat="server" />
                            <asp:HiddenField ID="h_FromAirTcket_City_ID" runat="server" />
                            <asp:HiddenField ID="hf_FeeConcessionCategory_ID" runat="server" />
                            <asp:HiddenField ID="hf_dependant_ID" runat="server" />
                            <asp:HiddenField ID="hf_dependant_unit" runat="server" />
                            <asp:HiddenField ID="hf_jobmaster_id" runat="server" />
                            <asp:HiddenField ID="hf_Grade" runat="server" />
                            <asp:HiddenField ID="hf_HRL" runat="server" />
                            <asp:HiddenField ID="hf_val" runat="server" />
                        </td>                       
                    </tr>
                </table>
                <asp:Panel ID="pnlPopup" runat="server">
                    <div id="InnDiv" align="left">
                        <table id="MainTable" style="cursor: hand;" width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvEmpSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        >
                                        <RowStyle CssClass="griditem_red" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee Name" SortExpression="EMP_FNAME" ControlStyle-CssClass="griditem_red">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEmpFN" runat="server" Text='<%# Eval("EMP_FNAME") %>' OnClick="lnkEmpFN_Click"
                                                         Font-Underline="False"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle  />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="EMPNO" HeaderText="EMPNO" ControlStyle-CssClass="griditem_red">
                                                <ItemStyle />
                                            </asp:BoundField>
                                        </Columns>                                       
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>

    <script type="text/javascript" language="javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        function EndRequestHandler(sender, args) {

            if (args.get_error() != undefined) {

                args.set_errorHandled(true); location.reload(true);

            }

        }

    </script>
    <asp:HiddenField ID="hf_mode" runat="server" />
</asp:Content>
