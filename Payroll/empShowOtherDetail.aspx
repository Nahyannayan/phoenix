<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empShowOtherDetail.aspx.vb" Inherits="Payroll_empShowOtherDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
        <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet"/>
    
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
    </script>
</head>
<body onload="listen_window();" topmargin="-12px">
    <form id="form1" runat="server">

        <table width="100%">
            <tr>

                <td colspan="2"
                    valign="top">
                    <table width="100%">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="center" class="matters" colspan="4" valign="top">
                                            <asp:GridView ID="gvEmpInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CssClass="table table-row table-bordered" DataKeyNames="ID" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText=" ID" SortExpression="EMP_ID">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("ID") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                Text="ID"></asp:Label><br />
                                                            <asp:TextBox ID="txtcode" runat="server" Width="92px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearchEmpId_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Description"
                                                                Width="214px"></asp:Label><br />
                                                            <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearchEmpName_Click" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                                OnClick="LinkButton1_Click" Text='<%# Eval("E_Name") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Cat") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Cat") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblCat" runat="server" CssClass="gridheader_text" Text="Category"
                                                                Width="214px"></asp:Label><br />
                                                            <asp:TextBox ID="txtCat" runat="server" Width="150px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchCat" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearchCat_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("subject") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("subject") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblSub" runat="server" CssClass="gridheader_text" Text="Subject" Width="214px"></asp:Label><br />
                                                            <asp:TextBox ID="txtSub" runat="server" Width="150px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchSub" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearchSub_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_pop" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <RowStyle Height="25px" CssClass="griditem" />
                                            </asp:GridView>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="4" style="width: 567px; height: 28px"
                    valign="middle">
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                </td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="4" style="height: 12px" valign="middle"></td>
            </tr>
        </table>
    </form>
</body>

</html>
