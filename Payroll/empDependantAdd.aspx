<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDependantAdd.aspx.vb" Inherits="Payroll_empDependantAdd" Title="Untitled Page" %>

<%--<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>

    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script language="javascript" type="text/javascript">

        function OnDateSelected(sender, eventArgs) {
            var date1 = sender.get_selectedDate();
            // date1.setDate(date1.getDate() + 31);
            var datepicker = $find("<%= txtDOB.ClientID %>");
            datepicker.set_maxDate(date1);
        }


        function OnDateSelected2(sender, eventArgs) {
            var date1 = sender.get_selectedDate();
            // date1.setDate(date1.getDate() + 31);
            var datepicker = $find("<%= txtEIDExpryDT.ClientID%>");
            datepicker.set_maxDate(date1);
        }

        function OnDateSelected3(sender, eventArgs) {
            var date1 = sender.get_selectedDate();
            //  date1.setDate(date1.getDate() + 31);
            var datepicker = $find("<%= txtDepVissuedate.ClientID%>");
            datepicker.set_maxDate(date1);
        }

        function OnDateSelected4(sender, eventArgs) {
            var date1 = sender.get_selectedDate();
            // date1.setDate(date1.getDate() + 31);
            var datepicker = $find("<%= txtDepVexpdate.ClientID %>");
            datepicker.set_maxDate(date1);
        }



        function getPageCode(mode) {

            var sFeatures;
            sFeatures = "dialogWidth: 100%; ";
            sFeatures += "dialogHeight: 100%; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result, oWnd;

            if (mode == 'DEPU') {

                oWnd = radopen("empShowMasterInfo.aspx?id=DEPENDADD", "RadWindow1");
                document.getElementById("<%=hdnMode.ClientID%>").value = 'DEPU';
            }
            if (mode == 'DC') {

                oWnd = radopen("empShowMasterInfo.aspx?id=DC", "RadWindow1");
                document.getElementById("<%=hdnMode.ClientID%>").value = 'DC';
            }
            if (mode == 'DependentMode') {

                oWnd = radopen("../Accounts/accShowEmpDetailInfo.aspx?id=EN&Bsu_id=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value, "RadWindow1");
                document.getElementById("<%=hdnMode.ClientID%>").value = 'DependentMode';
            }

            if (mode == 'StudentMode') {

                oWnd = radopen("../Fees/ShowStudentInfo.aspx?type=DEP&bsu=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value, "RadWindow1");
                document.getElementById("<%=hdnMode.ClientID%>").value = 'StudentMode';
            }


        }

        function OnClientClose(oWnd, args) {

            var mode = document.getElementById("<%=hdnMode.ClientID%>").value;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.Students.split('___');
                if (mode == 'DEPU') {
                    document.getElementById("<%=txtDependUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hf_dependant_unit.ClientID %>").value = NameandCode[1];
                }
                if (mode == 'DC') {
                    document.getElementById("<%=txtDepCountry.ClientID %>").value = NameandCode[0];
                            document.getElementById("<%=hfDepCountry_ID.ClientID %>").value = NameandCode[1];
                        }
                        if (mode == 'DependentMode') {
                            document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[1];
                            document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[0];
                        }

                        if (mode == 'StudentMode') {
                            NameandCode = arg.Students.split('||');

                            document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[1];
                        }
                //below is required to post data do not remove
                        document.getElementById('<%= btnPostData.ClientID%>').click();
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }








        function getPageCodeNotusing2(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 500px; ";
            sFeatures += "dialogHeight: 470px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;

            url = 'empShowMasterEmp.aspx?id=' + mode;

            if (mode == 'DEPU') {
                url = 'empShowMasterEmp.aspx?id=' + "DEPENDADD";
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtDependUnit.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hf_dependant_unit.ClientID %>").value = NameandCode[1];

            }
            else if (mode == 'DC') {
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtDepCountry.ClientID %>").value = NameandCode[0];
                 document.getElementById("<%=hfDepCountry_ID.ClientID %>").value = NameandCode[1];
             }


     }
     //Accounts/accShowEmpDetailInfo.aspx
     function GetEMPDependantName() {
         var sFeatures;
         sFeatures = "dialogWidth: 729px; ";
         sFeatures += "dialogHeight: 445px; ";
         sFeatures += "help: no; ";
         sFeatures += "resizable: no; ";
         sFeatures += "scroll: yes; ";
         sFeatures += "status: no; ";
         sFeatures += "unadorned: no; ";
         var NameandCode;
         var result;
         result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN&Bsu_id=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value, "", sFeatures)
        if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;
        }
        //Fees/ShowStudentInfo.aspx
        function GetStudentSingle() {

            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "../Fees/ShowStudent.aspx?type=DEP&bsu=" + document.getElementById("<%=hf_dependant_unit.ClientID %>").value;
            result = window.showModalDialog(url, "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('||');

                document.getElementById('<%=hf_dependant_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtDependName.ClientID %>').value = NameandCode[1];

                return true;
            }
            else {
                return false;
            }
        }
        function getStudent() {

            var sFeatures, url;
            sFeatures = "dialogWidth: 755px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var selType = 'EOS';
            var EMPID = document.getElementById('<%=h_Emp_ID.ClientID %>').value;
            var url;
            url = '../FEES/ShowStudent.aspx?TYPE=' + selType + '&EMPID=' + EMPID;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%= h_Student_id.ClientID %>').value = NameandCode[0];
        return true;
    }
    function getDate(val, allowfuturedt) {
        var sFeatures;
        sFeatures = "dialogWidth: 225px; ";
        sFeatures += "dialogHeight: 252px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: no; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var nofuture = "calendar.aspx?nofuture=no";
        var SelDate;
        SelDate = document.getElementById('<%=txtDOB.ClientID %>').value;
        SelDate = replaceSubstring(SelDate, "/", "-");
        switch (allowfuturedt) {
            case 0:
                {
                    result = window.showModalDialog(nofuture + "&dt=" + SelDate, "", sFeatures);
                    break;
                }
            case 1:
                {
                    result = window.showModalDialog("calendar.aspx?dt=" + SelDate, "", sFeatures);
                    break;
                }
        }
        if (result == '' || result == undefined) {
            return false;
        }
        if (val == 0) {
        }
        else if (val == 1) {
            document.getElementById('<%=txtDOB.ClientID %>').value = result;
        }
        else if (val == 17) {
            document.getElementById('<%=txtEIDExpryDT.ClientID %>').value = result;
        }
        else if (val == 18) {
            document.getElementById('<%=txtDepVissuedate.ClientID %>').value = result;
    }
    else if (val == 19) {
        document.getElementById('<%=txtDepVexpdate.ClientID %>').value = result;
    }
    return false;
}


function UploadPhoto() {

    //        document.forms[0].Submit();
    var filepath = document.getElementById('<%=UploadDepPhoto.ClientID %>').value;
    if ((filepath != '') && (document.getElementById('<%=h_DepImagePath.ClientID %>').value != filepath)) {
        document.getElementById('<%=h_DepImagePath.ClientID %>').value = filepath;
                document.forms[0].submit();
            }
        }
    </script>

    <style type="text/css">
         .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
             left: 40%;
            top: 40%;
            position: fixed;
            width: 25%;
        }
    </style>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="900px" Height="410px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>

    <div class="text-right">
                    <asp:LinkButton ID="lnkbackbtn" runat="server" OnClick="lnkbackbtn_Click">Back to Dashboard</asp:LinkButton>
    </div>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="CLIENTERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table width="100%" id="tblCategory">

                    <tr>
                        <%-- <td align="left"   width="20%">
             Dependant Name</td>
         <td align="left"   style="width: 488px">
             :</td>
         <td align="left" width="50%">
             <asp:TextBox ID="txtName" runat="server" Width="303px"></asp:TextBox>
             <asp:ImageButton
                ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return true;"
                TabIndex="7" />
             </td>--%>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Relation </span><font class="text-danger">*</font></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlDependRelation" runat="server">
                                <asp:ListItem Value="0">Spouse</asp:ListItem>
                                <asp:ListItem Value="1">Child</asp:ListItem>
                                <asp:ListItem Value="2">Father</asp:ListItem>
                                <asp:ListItem Value="3">Mother</asp:ListItem>
                                <asp:ListItem Value="4">Grand Child</asp:ListItem>
                                <asp:ListItem Value="5">Grand Parent</asp:ListItem>
                                <asp:ListItem Value="6">Other</asp:ListItem>
                            </asp:DropDownList>


                        </td>
                        <td align="center" rowspan="4" width="30%" colspan="2">
                            <asp:ImageMap ID="imgDependant" runat="server" CssClass="img-profile">
                            </asp:ImageMap>
                        </td>

                    </tr>
                    <tr runat="server" id="tr_issue">
                        <td align="left" valign="middle"><span class="field-label">Date Of Birth</span><font class="text-danger">*</font></td>
                        
                        <td align="left" valign="middle">
                            <asp:TextBox ID="txtDOB" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDOB" TargetControlID="txtDOB" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <%--<telerik:RadDatePicker MinDate="1920/1/1" MaxDate="2099/1/1" RenderMode="Lightweight" ID="txtDOB" Width="130px" DateInput-ReadOnly="true" ClientEvents-OnDateSelected="OnDateSelected" runat="server" DateInput-DateFormat="dd/MMM/yyyy">
                </telerik:RadDatePicker>--%>
                            <%-- <telerik:RadCalendar RenderMode="Lightweight" ID="RadCalendar1" Width="100%" EnableMultiSelect="false" EnableKeyboardNavigation="true"
                    ShowColumnHeaders="true" ShowDayCellToolTips="true" SelectedDate="08/10/2015"  ShowRowHeaders="true" runat="server"
                    AutoPostBack="true">--%>
                            <%-- <FastNavigationSettings EnableTodayButtonSelection="true">
                    </FastNavigationSettings>
                </telerik:RadCalendar>--%>
                            <asp:ImageButton ID="imgDOB" Visible="false" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6"
                                runat="server" ControlToValidate="txtDOB"
                                Display="Dynamic" ErrorMessage="Enter the DOB in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtDOB"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="DOB entered is not a valid date"
                                ForeColor="" ValidationGroup="CLIENTERROR">*</asp:CustomValidator>&nbsp;</td>
                       
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Add Photo</span></td>

                        <td align="left" >
                            <asp:FileUpload ID="UploadDepPhoto" runat="server"
                                ToolTip='Click "Browse" to select the photo. The file size should be less than 250 KB' />
                            <br />
                            <font >(File size should be less than 250kb, image should be 51x51mm, format must be .jpg  )</font>
                        </td>
                        
                    </tr>


                    <tr>
                        <td align="left" nowrap="nowrap" colspan="4">

                            <asp:RadioButtonList ID="rblIsGems" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem Value="N" class="field-label" Selected="True">Non-GEMS</asp:ListItem>
                                <asp:ListItem Value="E" class="field-label">GEMS Staff</asp:ListItem>
                                <asp:ListItem Value="S" class="field-label">GEMS Student</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Gender</span><font class="text-danger">*</font>
                        </td>
                        
                        <td align="left" nowrap="nowrap" width="30%">
                            <asp:RadioButton ID="rdDepdMale" runat="server" class="field-label" GroupName="Gender" Text="Male" />
                            <asp:RadioButton ID="rdDepdFemale" runat="server" class="field-label" GroupName="Gender" Text="Female" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Marital Status</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap" width="30%">
                            <asp:DropDownList ID="ddlDependMstatus" runat="server" Width="91px">
                                <asp:ListItem Value="-1">-Select-</asp:ListItem>
                                <asp:ListItem Value="0">Married</asp:ListItem>
                                <asp:ListItem Value="1">Single</asp:ListItem>
                                <asp:ListItem Value="2">Widowed</asp:ListItem>
                                <asp:ListItem Value="3">Divorced</asp:ListItem>
                                <asp:ListItem Value="4">Separated</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvmarital" runat="server" ValidationGroup="CLIENTERROR" ControlToValidate="ddlDependMstatus" InitialValue="-1" ErrorMessage="Please select"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Unit</span>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDependUnit" runat="server" AutoPostBack="True"></asp:TextBox>
                            &nbsp;<asp:ImageButton ID="btnDepUnit" runat="server" imageUrl="~/Images/forum_search.gif" Enabled="false"
                                OnClientClick="getPageCode('DEPU');return false;"  />
                        </td>
                        <td align="left"><span class="field-label">Name</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDependName" runat="server"></asp:TextBox>
                            <%-- <asp:RegularExpressionValidator ID="rfvDep" runat="server" ControlToValidate="txtDependName" ErrorMessage="*"></asp:RegularExpressionValidator>--%>
                            <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getPageCode('DependentMode');return false;" />
                            <asp:ImageButton ID="imgStudSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getPageCode('StudentMode');return false;" />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" nowrap="nowrap"><span class="field-label">Nationality</span><font class="text-danger">*</font></td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDepCountry" runat="server"></asp:TextBox>
                            <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDepCountry" ErrorMessage="*"></asp:RegularExpressionValidator>--%>
                            <asp:ImageButton ID="btnDepCountry" runat="server" imageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('DC');return false;"
                                 />
                        </td>
                        <td align="left"><span class="field-label">Passport No</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDepPassportNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trDependInsurance" runat="server" visible="false">
                        <td align="left"><span class="field-label">Insurance Plan</span>
                        </td>

                        <td align="left">
                            <asp:DropDownList ID="ddlDepInsuCardTypes" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2" align="left">
                            <asp:CheckBox ID="chkDepCompInsu" runat="server" Text="Company paid Insurance"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr id="trConcession" visible="false" runat="server">
                        <td align="left"><span class="field-label">Concession</span>
                        </td>

                        <td align="left">
                            <asp:CheckBox ID="ChkDependConcession" runat="server" />
                        </td>
                        <td align="left"><span class="field-label">Student ID</span>
                        </td>

                        <td align="left" >
                            <asp:TextBox ID="txtStudID" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trEID1" runat="server">
                        <td align="left"><span class="field-label">EmiratesID No</span><font class="text-danger">*</font>
                        </td>

                        <td align="left">
                            <asp:TextBox ID="txtEIDNo" runat="server" TabIndex="18" Visible="false"></asp:TextBox>
                            <telerik:RadTextBox ID="txtEIDNo1" runat="server" EmptyMessage="000" Width="56px" MaxLength="3"></telerik:RadTextBox>-
                                          <telerik:RadTextBox ID="txtEIDNo2" runat="server" EmptyMessage="0000" Width="64px" MaxLength="4"></telerik:RadTextBox>-
                                           <telerik:RadTextBox ID="txtEIDNo3" runat="server" EmptyMessage="0000000" Width="94px" MaxLength="7"></telerik:RadTextBox>-
                                        <telerik:RadTextBox ID="txtEIDNo4" runat="server" EmptyMessage="0" Width="40px" MaxLength="1"></telerik:RadTextBox>



                        </td>
                        <td align="left"><span class="field-label">EID Expiry Date</span><font class="text-danger">*</font>
                        </td>

                        <td align="left">
                            <asp:TextBox ID="txtEIDExpryDT" runat="server"></asp:TextBox>

                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtEIDExpryDT" TargetControlID="txtEIDExpryDT" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <%--  <telerik:RadTextBox ID="txtEIDExpryDT1" runat="server" EmptyMessage="DD/MMM/yyyy" Visible="false"></telerik:RadTextBox>
                <telerik:RadDatePicker RenderMode="Lightweight" ID="txtEIDExpryDT" Width="130px" MinDate="2010/1/1" MaxDate="2099/1/1" DateInput-ReadOnly="true" ClientEvents-OnDateSelected="OnDateSelected2" runat="server" DateInput-DateFormat="dd/MMM/yyyy">
                </telerik:RadDatePicker>--%>

                            <asp:ImageButton ID="imgEIDExpryDT" runat="server" ImageUrl="~/Images/calendar.gif" Visible="false" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                ControlToValidate="txtEIDExpryDT" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Emirates ID expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator7" runat="server" ControlToValidate="txtEIDExpryDT"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Emirates ID expiry Date entered is not a valid date"
                                ForeColor="" ValidationGroup="CLIENTERROR">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>

                        <td align="left"><span class="field-label">UID No</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDepUidno" runat="server"></asp:TextBox>
                        </td>


                        <td align="left"><span class="field-label">Visa Issued Date</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDepVissuedate" runat="server"></asp:TextBox>

                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDepVissuedate" TargetControlID="txtDepVissuedate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <%-- <telerik:RadDatePicker RenderMode="Lightweight" ID="txtDepVissuedate" Width="130px" MaxDate="2099/1/1" DateInput-ReadOnly="true"  ClientEvents-OnDateSelected="OnDateSelected3" runat="server" DateInput-DateFormat="dd/MMM/yyyy">
                </telerik:RadDatePicker>
                <telerik:RadTextBox ID="txtDepVissuedate1" runat="server" EmptyMessage="DD/MMM/yyyy" MinDate="2000/1/1" MaxDate="2099/1/1"  Visible="false"></telerik:RadTextBox>--%>
                            <asp:ImageButton ID="imgDepVissuedate" Visible="false" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                ControlToValidate="txtDepVissuedate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Visa Issued Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator5" runat="server" ControlToValidate="txtDepVissuedate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Issued Date entered is not a valid date"
                                ForeColor="" ValidationGroup="CLIENTERROR">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Visa Expiry Date</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDepVexpdate" runat="server"></asp:TextBox>

                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDepVexpdate" TargetControlID="txtDepVexpdate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <%-- <telerik:RadDatePicker RenderMode="Lightweight" ID="txtDepVexpdate" Width="130px" MinDate="2000/1/1" MaxDate="2099/1/1" ClientEvents-OnDateSelected="OnDateSelected4" DateInput-ReadOnly="true" runat="server" DateInput-DateFormat="dd/MMM/yyyy">
                </telerik:RadDatePicker>
                <telerik:RadTextBox ID="txtDepVexpdate1" runat="server" EmptyMessage="DD/MMM/yyyy" MaxDate="2099/1/1" MinDate="2000/1/1" Visible="false"></telerik:RadTextBox>
                            --%>
                            <asp:ImageButton ID="imgDepVexpdate" Visible="false" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                ControlToValidate="txtDepVexpdate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Visa expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtDepVexpdate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa expiry Date entered is not a valid date"
                                ForeColor="">*</asp:CustomValidator>
                        </td>


                        <td align="left"><span class="field-label">Visa Issued Place</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtDepVissueplace" runat="server" Width="225px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"><span class="field-label">Visa Sponsor</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddlSponsor" runat="server">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Company" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Spouse" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Father" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Mother" Value="4"></asp:ListItem>

                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="rfSponsor" runat="server" ControlToValidate="ddlSponsor" InitialValue="0" ErrorMessage="select Visa sponsor" CssClass="error" ValidationGroup="CLIENTERROR"></asp:RequiredFieldValidator>

                        </td>
                        <td align="left"><span class="field-label">Existing Insurance Card No.</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:TextBox ID="txtInsCardNum" runat="server" Width="220px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="display: none;">
                        <td align="left"><span class="field-label">Res Emirate</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddlEmirate" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>


                        <td align="left"><span class="field-label">Area</span><font class="text-danger">*</font>
                        </td>

                        <td align="left" nowrap="nowrap">

                            <asp:DropDownList ID="ddlArea" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Visa File Number</span><font class="text-danger" runat="server" visible="false" id="spanVisFileNoCheck">*</font>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtfileno" runat="server" ></asp:TextBox>
                        </td>
                        <td colspan="2"></td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CLIENTERROR" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="btnDelete" ConfirmText="Dependent details will be deleted permanently.Are you sure you want to continue?"
                                runat="server">
                            </ajaxToolkit:ConfirmButtonExtender>

                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:LinkButton ID="btnPostData" runat="server"></asp:LinkButton></td>


                    </tr>
                </table>
            </div>

             <asp:Panel ID="divDrill" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:ImageButton ID="btClose" type="button" runat="server" ImageUrl="../Images/close.png" style="float:right;"></asp:ImageButton>
                            <div class="title-bg p-3 mb-3" width="100%"></div>
                            <div>
                                <div align="center" id="lbluerror" runat="server">
                                    
                                </div>
                                <div align="center">
                                <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="button"></asp:Button>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
        </div>
    </div>
    <asp:HiddenField ID="h_Emp_ID" runat="server" />
    <asp:HiddenField ID="h_Student_id" runat="server" />
    <asp:HiddenField ID="hdnEDDID" runat="server" />
    <asp:HiddenField ID="h_DepImagePath" runat="server" />
    <asp:HiddenField ID="hf_dependant_unit" runat="server" />
    <asp:HiddenField ID="hfDepCountry_ID" runat="server" />
    <asp:HiddenField ID="hf_dependant_ID" runat="server" />
    <asp:HiddenField ID="hdnMode" runat="server" />
</asp:Content>
