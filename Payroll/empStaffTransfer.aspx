<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empStaffTransfer.aspx.vb" Inherits="EmpStaffTransfer" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url

            url = 'empShowMasterEmp.aspx?id=' + mode;

            if (mode == 'BK') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtBname.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBank_ID.ClientID %>").value = NameandCode[1];--%>

            }
            else if (mode == 'D') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
               <%-- if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtDept.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfDept_ID.ClientID %>").value = NameandCode[1];--%>
            }

            else if (mode == 'T') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
               <%-- if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtGrade.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfGrade_ID.ClientID %>").value = NameandCode[1];--%>
            }

            else if (mode == 'CT') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtCat.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfCat_ID.ClientID %>").value = NameandCode[1];--%>
            }
            else if (mode == 'AP') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtEmpApprovalPol.ClientID %>").value = NameandCode[0];
                 document.getElementById("<%=h_ApprovalPolicy.ClientID %>").value = NameandCode[1];--%>
             }

             else if (mode == 'SD') {
                 result = radopen(url, "pop_up");
                 document.getElementById("<%=hf_mode.ClientID %>").value = mode
                <%-- if (result == '' || result == undefined) {
                     return false;
                 }
                 NameandCode = result.split('___');
                 document.getElementById("<%=txtSD.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfSD_ID.ClientID %>").value = NameandCode[1];--%>
            }

            else if (mode == 'ME') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
              <%--  if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtME.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfME_ID.ClientID %>").value = NameandCode[1];--%>
            }
            else if (mode == 'ML') {
                result = radopen(url, "pop_up");
                document.getElementById("<%=hf_mode.ClientID %>").value = mode
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtML.ClientID %>").value = NameandCode[0];
               document.getElementById("<%=hfML_ID.ClientID %>").value = NameandCode[1];--%>
           }
           else if (mode == 'EP') {
               result = radopen(url, "pop_up");
               document.getElementById("<%=hf_mode.ClientID %>").value = mode
              <%-- if (result == '' || result == undefined) {
                   return false;
               }
               NameandCode = result.split('___');
               document.getElementById("<%=txtReport.ClientID %>").value = NameandCode[0];
                 //document.getElementById("<%=hfReport.ClientID %>").value = NameandCode[1];--%>
             }

        }

         function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            var mode;
            if (arg) {
              
                NameandCode = arg.NameandCode.split('||');                 
                mode = document.getElementById("<%=hf_mode.ClientID %>").value                 
                if (mode == 'BK') {
                    document.getElementById("<%=txtBname.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBank_ID.ClientID %>").value = NameandCode[1];                   
                    __doPostBack('<%=txtBname.ClientID%>', "");
                }
                else if (mode == 'D') {
                    document.getElementById("<%=txtDept.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfDept_ID.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtDept.ClientID%>', "");
                }
                else if (mode == 'T') {
                    document.getElementById("<%=txtGrade.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfGrade_ID.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtGrade.ClientID%>', "");
                }
                else if (mode == 'CT') {
                    document.getElementById("<%=txtCat.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfCat_ID.ClientID %>").value = NameandCode[1];
                      __doPostBack('<%=txtCat.ClientID%>', "");
                }
                else if (mode == 'AP') {
                    document.getElementById("<%=txtEmpApprovalPol.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_ApprovalPolicy.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtEmpApprovalPol.ClientID%>', "");
                }
                else if (mode == 'SD') {
                    document.getElementById("<%=txtSD.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfSD_ID.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtSD.ClientID%>', "");
                }
                else if (mode == 'ME') {
                    document.getElementById("<%=txtME.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfME_ID.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtME.ClientID%>', "");
                }
                else if (mode == 'ML') {
                    document.getElementById("<%=txtML.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfML_ID.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtML.ClientID%>', "");
                }
                else if (mode == 'EP') {
                    document.getElementById("<%=txtReport.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfReport.ClientID %>").value = NameandCode[1];
                     __doPostBack('<%=txtReport.ClientID%>', "");
                }
               
            }
         }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function GetRepEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=ERP", "pop_up2")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose2(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');                
                document.getElementById('<%=hfReport.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtReport.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=txtReport.ClientID%>', "");
            }
        }


        function GetPayableFromEligible() {
            var eligibleAmt, FTEamt;
            eligibleAmt = document.getElementById("<%=txtSalAmtEligible.ClientID %>").value;
            FTEamt = document.getElementById("<%=txtFTE.ClientID %>").value;
            if (FTEamt == '')
                FTEamt = 1
            document.getElementById("<%=txtSalAmount.ClientID %>").value = FTEamt * eligibleAmt;
        }


        function getVacationPolicy() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var catID;
            catID = document.getElementById('<%=hfCat_ID.ClientID %>').value;
            if (catID != '') {
                result = radopen("selIDDesc.aspx?ID=VP&CATID=" + catID, "pop_up3")
                <%-- if (result != '' && result != undefined) {
                    NameandCode = result.split('_');
                    document.getElementById('<%=hf_VacationPolicy.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtVacationPolicy.ClientID %>').value = NameandCode[1];
                }
            }
            return false;--%>
            }
        }

        function OnClientClose3(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hf_VacationPolicy.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtVacationPolicy.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtVacationPolicy.ClientID%>', "");
            }
        }

        function GetEMPAttName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up4")
          <%--  if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfAttApprov.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtAttApprov.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose4(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfAttApprov.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtAttApprov.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=txtAttApprov.ClientID%>', "");
            }
        }

        function getPageCode2(mode) {

            if (mode == 'Q') {
            }

            else if (mode == 'SG') {
                var sFeatures;
                sFeatures = "dialogWidth: 590px; ";
                sFeatures += "dialogHeight: 370px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: no; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                var NameandCode;
                var result;
                var url;
                url = 'empShowOtherDetail.aspx?id=' + mode;
                result = radopen(url, "pop_up5");
                <%--if (result == '' || result == 'undefined') {
                    return false;
                }
                else {
                    NameandCode = result.split('___');
                    document.getElementById("<%=txtSalGrade.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfSalGrade.ClientID %>").value = NameandCode[1];

                    return true;
                }--%>
            }
        }

        function OnClientClose5(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtSalGrade.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfSalGrade.ClientID %>").value = NameandCode[1];
                __doPostBack('<%=txtSalGrade.ClientID%>', "");
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
           <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>  
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="Label4" runat="server" Text="Employee Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="VALSALDET" />

                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False" HeaderText="Following condition required:" ValidationGroup="All1" CssClass="error" ForeColor="" SkinID="error" />
                            <asp:Label ID="lblError" runat="server" CssClass="error" ></asp:Label><br />
                            <asp:MultiView ID="MVEMPTRANSFER" runat="server" ActiveViewIndex="0">
                                <asp:View ID="vwOtherDetails" runat="server">
                                    <br />
                                    <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                        <%-- <tr class="subheader_img">                             <td align="center"  colspan="6" valign="middle">
                              <div align="left">
                                  <ul>
                                      <li><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                          <asp:Label ID="Label4" runat="server" Text="Employee Details"></asp:Label>
                                          </font></li>
                                  </ul>
                                 </div>
                             </td>
                            </tr>--%>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Employee Name</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Transfer Date </span></td>
                                            <td width="30%">
                                                <asp:TextBox ID="txtJdate" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Transfered From</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtFromBSUName" runat="server" ReadOnly="True"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">MOL Profession</span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtML" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnV_Desig" runat="server" CssClass="button" OnClientClick="getPageCode('ML');return false;"
                                                    Text="..." /></td>
                                            <td align="left" width="20%"><span class="field-label">Employee Designation</span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtSD" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnE_Desig" runat="server" CssClass="button" OnClientClick="getPageCode('SD');return false;"
                                                    Text="..." /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">MOE Profession</span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtME" runat="server" ReadOnly="false"></asp:TextBox>
                                                <asp:Button ID="btnMoe_desig" runat="server" CssClass="button" OnClientClick="getPageCode('ME');return false;"
                                                    Text="..." /></td>
                                            <td align="left" width="20%"><span class="field-label">Employee Catagory</span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtCat" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnCat" runat="server" CssClass="button" OnClientClick="getPageCode('CT');return false;"
                                                    Text="..." /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"> <span class="field-label">Teaching
                                    Grade</span><span style="color: red"></span></td>

                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnGrade" runat="server" CssClass="button" OnClientClick="getPageCode('T');return false;"
                                                    Text="..." /></td>
                                            <td align="left" width="20%"><span class="field-label">Department</span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtDept" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnDept" runat="server" CssClass="button" OnClientClick="getPageCode('D');return false;"
                                                    Text="..." /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Employee Reports To</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtReport" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnReport" runat="server" CssClass="button" OnClientClick="GetRepEMPName();return false;"
                                                    Text="..." /></td>

                                            <td align="left" width="20%"><span class="field-label">Attendance Approver</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtAttApprov" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnAttendance" runat="server" CssClass="button" OnClientClick="GetEMPAttName();return false;"
                                                    Text="..." /></td>

                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Vacation Policy </span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtVacationPolicy" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnVacationPolicy" runat="server" CssClass="button" OnClientClick="getVacationPolicy();return false;"
                                                    Text="..." /></td>

                                            <td align="left" width="20%"><span class="field-label">Leave Approval Policy</span><span style="color: red">*</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtEmpApprovalPol" runat="server"></asp:TextBox>
                                                <asp:Button ID="Button4" runat="server" CssClass="button" OnClientClick="getPageCode('AP');return false;"
                                                    Text="..." /></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4" style="text-align: center">
                                                <asp:Button ID="btnSalSet" runat="server" CssClass="button" Text="Update Salary Settings" />
                                                <asp:Button
                                                    ID="btnCancel1" runat="server" CssClass="button" Text="Cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="vwSalarydetails" runat="server">
                                    <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr class="title-bg">
                                                <td align="center" colspan="4" valign="middle">
                                                    <div align="left">
                                                        <asp:Label ID="Label5" runat="server" Text="Salary Details"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Salary Currency</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlSalC" runat="server">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%"><span class="field-label">Payment Currency</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlPayC" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Payment Mode</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlPayMode" runat="server" AutoPostBack="True">
                                                        <asp:ListItem Value="0">Cash</asp:ListItem>
                                                        <asp:ListItem Value="1">Bank</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%"><span class="field-label">FTE</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtFTE" runat="server"></asp:TextBox>
                                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtFTE"
                                                        ErrorMessage="Enter valid FTE" Operator="DataTypeCheck" Type="Double" ValidationGroup="VALSALDET">*</asp:CompareValidator>
                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtFTE"
                                                        ErrorMessage="Enter FTE Range from 0 to 1" MaximumValue="1" MinimumValue="0"
                                                        Type="Double" ValidationGroup="VALSALDET">*</asp:RangeValidator></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Bank Name</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtBname" runat="server"></asp:TextBox>
                                                    <asp:Button ID="btnBank_name" runat="server" CssClass="button" OnClientClick="getPageCode('BK');return false;"
                                                        Text="..." /></td>
                                                <td align="left" width="20%">  <span class="field-label">Account No</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtAccCode" runat="server"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Swift Code</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtSwiftcode" runat="server"></asp:TextBox></td>
                                                <td align="left" width="20%"><span class="field-label">Salary Scale</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtSalGrade" runat="server"></asp:TextBox>
                                                    <asp:Button ID="btnSalScale" runat="server" CssClass="button" OnClientClick="getPageCode2('SG');return false;"
                                                        Text="..." OnClick="btnSalScale_Click" /></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label"> Transportation</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlEmpTransportation" runat="server">
                                                        <asp:ListItem Value="0">Own</asp:ListItem>
                                                        <asp:ListItem Value="1">Company's Transportation</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="title-bg" colspan="4">Salary Components</td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Earn Code</span></td>
                                                <td align="left" width="30%"> 
                                                    <asp:DropDownList ID="ddSalEarnCode" runat="server">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%"> <span class="field-label">Amount (Eligibility)</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtSalAmtEligible" runat="server"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Amount (Actual)</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtSalAmount" runat="server"></asp:TextBox>
                                                    <asp:CheckBox
                                                        ID="chkSalPayMonthly" runat="server" Text="Per Month" /></td>
                                                <td align="left" width="20%"><span class="field-label">Frequecy</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlayInstallment" runat="server">
                                                        <asp:ListItem Value="0">Monthly</asp:ListItem>
                                                        <asp:ListItem Value="1">Bimonthly</asp:ListItem>
                                                        <asp:ListItem Value="2">Quarterly</asp:ListItem>
                                                        <asp:ListItem Value="3">Half-yearly</asp:ListItem>
                                                        <asp:ListItem Value="4">Yearly</asp:ListItem>
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4" style="text-align: center">
                                                    <asp:Button ID="btnSalAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSALDET" />
                                                    <asp:Button ID="btnSalCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="left" colspan="4">
                                                    <asp:GridView ID="gvEmpSalary" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                                        Width="100%">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Earn_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Earning">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblERN_ID" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="BMonthly" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBMonthly" runat="server" Text='<%# bind("BMonthly") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount(Eligible)">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAmEligible" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount(Actual)">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Right" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PayTerm" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPayTerm" runat="server" Text='<%# bind("PAYTERM") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Schedule">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSchedule" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    Edit
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkSalEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                        OnClick="lnkSalEdit_Click" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                    <br />
                                                    Gross Salary
                                                    <asp:TextBox ID="txtGsalary" runat="server" ReadOnly="True"></asp:TextBox></td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="center" colspan="4" style="text-align: center">
                                                    <asp:Button ID="btnSalBack" runat="server" CssClass="button" Text="<< Back" />
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALSALDET" />
                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Cancel" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:View>
                            </asp:MultiView></td>
                    </tr>
                </table>

                <script type="text/javascript">
                </script>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <asp:HiddenField ID="hfDept_ID" runat="server" />
                <asp:HiddenField ID="hfGrade_ID" runat="server" />
                <asp:HiddenField ID="hfCat_ID" runat="server" />
                <asp:HiddenField ID="hfSD_ID" runat="server" />
                <asp:HiddenField ID="hfME_ID" runat="server" />
                <asp:HiddenField ID="hfML_ID" runat="server" />
                <asp:HiddenField ID="hfSalGrade" runat="server" />
                <asp:HiddenField ID="hfBank_ID" runat="server" />
                <asp:HiddenField ID="hfReport" runat="server" />
                <asp:HiddenField ID="hfAttApprov" runat="server" />
                <asp:HiddenField ID="h_ApprovalPolicy" runat="server" />
                <asp:HiddenField ID="hf_VacationPolicy" runat="server" />
                <asp:HiddenField ID="hfEmployeeNum" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

