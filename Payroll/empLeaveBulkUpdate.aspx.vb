Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports InfoSoftGlobal
Imports Telerik.Web.UI
Partial Class Payroll_empLeaveBulkUpdate
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If Not Request.QueryString("empid") Is Nothing Then
                    h_Emp_ID.Value = Request.QueryString("empid")
                    If Not Request.QueryString("attdate") Is Nothing Then
                        h_ATT_DATE.Value = CDate(Request.QueryString("attdate")).ToString("dd/MMM/yyyy")
                        BindEmployeeDetails(h_Emp_ID.Value)
                        RadToDate.SelectedDate = h_ATT_DATE.Value
                        RadFromDate.SelectedDate = h_ATT_DATE.Value
                        RadFromDate.MaxDate = Now.Date
                        RadToDate.MaxDate = Now.Date
                        ' RadFromDate.SelectedDate = CDate(h_ATT_DATE.Value).AddMonths(-1).ToString("dd/MMM/yyyy")
                        gridbind(h_Emp_ID.Value, RadFromDate.SelectedDate, RadToDate.SelectedDate)
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindEmployeeDetails(ByVal EMP_ID As Integer)
        Try
            Dim sqlstr As String
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            sqlstr = "  SELECT EMP.EMP_ID,EMP.EMPNO, EMP.EMP_FNAME+ ' ' + EMP.EMP_MNAME + ' ' + EMP.EMP_LNAME EMP_NAME,EMP.EMP_JOINDT,EMD_PHOTO ,"
            sqlstr &= " RT_EMP.EMP_FNAME+ ' ' + RT_EMP.EMP_MNAME + ' ' + RT_EMP.EMP_LNAME RT_EMP_NAME,"
            sqlstr &= " ART_EMP.EMP_FNAME+ ' ' + ART_EMP.EMP_MNAME + ' ' + ART_EMP.EMP_LNAME ART_EMP_NAME,"
            sqlstr &= "  CASE WHEN ISNULL(EMP.EMP_bReqPunching,0)=1 THEN 'YES' else 'NO' end ATT_BIND_REQ "
            sqlstr &= " FROM EMPLOYEE_M EMP left OUTER JOIN EMPLOYEE_D on EMP.EMP_ID=EMD_EMP_ID "
            sqlstr &= " LEFT OUTER JOIN EMPLOYEE_M ART_EMP on EMP.EMP_ATT_Approv_EMP_ID=ART_EMP.EMP_ID "
            sqlstr &= "  LEFT OUTER JOIN EMPLOYEE_M RT_EMP ON EMP.EMP_REPORTTO_EMP_ID=RT_EMP.EMP_ID "
            sqlstr &= " WHERE EMP.EMP_ID = '" & EMP_ID & "'"

            Dim mTBL As New DataTable
            mTBL = Mainclass.getDataTable(sqlstr, str_conn)
            If mTBL.Rows.Count > 0 Then
                lblEmpNo.Text = mTBL.Rows(0).Item("EMPNO")
                lblEMPName.Text = mTBL.Rows(0).Item("EMP_NAME")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub gridbind(ByVal EMP_ID As Int64, ByVal FROMDT As Date, ByVal TODT As Date)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            ' 
            Dim param(7) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlClient.SqlParameter("@FROMDT", FROMDT.ToString("dd/MMM/yyyy"))
            param(2) = New SqlClient.SqlParameter("@TODT", TODT.ToString("dd/MMM/yyyy"))
            param(3) = New SqlClient.SqlParameter("@CAT_ID", 0)
            param(4) = New SqlClient.SqlParameter("@TYPE", 0)
            param(5) = New SqlClient.SqlParameter("@REP_EMP_ID", 0)
            param(6) = New SqlClient.SqlParameter("@ELT_ID", "All")
            param(7) = New SqlClient.SqlParameter("@EMP_ID", EMP_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_EMP_ATT_DASHBOARD_DETAILS_NEW", param)
            RadEmpAttendance.DataSource = ds
            RadEmpAttendance.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub RadEmpAttendance_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs)
        Try
            If (e.Item.ItemType = GridItemType.Item) OrElse (e.Item.ItemType = GridItemType.AlternatingItem) Then
                Dim hdnStatus As HiddenField = DirectCast(e.Item.FindControl("hdnStatus"), HiddenField)
                Dim StatusRadComboBox As RadComboBox = DirectCast(e.Item.FindControl("StatusRadComboBox"), RadComboBox)
                If StatusRadComboBox IsNot Nothing Then
                    BindStatusDD(StatusRadComboBox)
                    StatusRadComboBox.ClearSelection()
                    If Not StatusRadComboBox.Items.FindItemByValue(hdnStatus.Value, True) Is Nothing Then
                        StatusRadComboBox.Items.FindItemByValue(hdnStatus.Value, True).Selected = True
                    End If
                End If
                Dim hdnEMPID As HiddenField = DirectCast(e.Item.FindControl("hdnEMPID"), HiddenField)
                If hdnEMPID Is Nothing Then Exit Sub
                Dim txtRemarks As RadTextBox
                txtRemarks = e.Item.FindControl("RemarksRadTextBox")
                Dim hdnLeaveAvailable, hdnLeaveStatus, hdnProcessed As HiddenField
                hdnLeaveAvailable = e.Item.FindControl("hdnLeaveAvailable")
                hdnLeaveStatus = e.Item.FindControl("hdnLeaveStatus")
                hdnProcessed = e.Item.FindControl("hdnProcessed")
                If hdnLeaveAvailable IsNot Nothing Then
                    If hdnLeaveAvailable.Value = "False" Then
                        e.Item.ForeColor = Drawing.Color.Red
                        e.Item.ToolTip = hdnLeaveStatus.Value
                        txtRemarks.ForeColor = Drawing.Color.Red
                        StatusRadComboBox.ForeColor = Drawing.Color.Red
                    Else
                        e.Item.ForeColor = Nothing
                        txtRemarks.ForeColor = Nothing
                        StatusRadComboBox.ForeColor = Nothing
                        e.Item.ToolTip &= Trim(IIf(Trim(e.Item.ToolTip) <> "", Environment.NewLine, "") & hdnLeaveStatus.Value)
                    End If
                End If
                If hdnProcessed IsNot Nothing Then
                    If hdnProcessed.Value = "False" Then
                        e.Item.ForeColor = Drawing.Color.Red
                        txtRemarks.ForeColor = Drawing.Color.Red
                        StatusRadComboBox.ForeColor = Drawing.Color.Red
                    Else
                        e.Item.ForeColor = Nothing
                        txtRemarks.ForeColor = Nothing
                        StatusRadComboBox.ForeColor = Nothing
                    End If
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindStatusDD(ByVal StatusRadComboBox As RadComboBox, Optional ByVal ShowAll As Boolean = False)
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = ""
        If ShowAll Then
            str_Sql = " SELECT 'All' EAS_ELT_ID,'-------------All--------------' EAS_DESCR  UNION ALL "
        End If
        str_Sql &= "SELECT DISTINCT ISNULL(EAS_ELT_ID, EAS_DESCR) EAS_ELT_ID, EAS_DESCR FROM EMPATTENDANCE_STATUS INNER JOIN BSU_LEAVESLAB_S ON EAS_ELT_ID=BLS_ELT_ID "
        str_Sql &= " AND BLS_BSU_ID ='" & Session("sBsuid") & "'"
        str_Sql &= " UNION ALL SELECT 'HO','Holiday'"
        str_Sql &= " UNION ALL SELECT 'OD','On Duty'"
        str_Sql &= " UNION ALL SELECT 'PRESENT','PRESENT'"

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        StatusRadComboBox.DataTextField = "EAS_DESCR"
        StatusRadComboBox.DataValueField = "EAS_ELT_ID"
        StatusRadComboBox.DataSource = ds.Tables(0)
        StatusRadComboBox.DataBind()
        If ShowAll Then
            StatusRadComboBox.SelectedItem.Value = "All"
        End If
    End Sub
    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim transaction As SqlTransaction
        transaction = conn.BeginTransaction("SampleTransaction")
        Try
            Dim Failure As Boolean = False
            Dim ErrNo As Integer
            Failure = SaveEMPAttendanceCorrection(conn, transaction, ErrNo)
            If Not Failure Then
                transaction.Commit()
                gridbind(h_Emp_ID.Value, RadFromDate.SelectedDate, RadToDate.SelectedDate)
                ShowErrorMessage("Data Saved Successfully")
            Else
                transaction.Rollback()
                ShowErrorMessage(UtilityObj.getErrorMessage(ErrNo))
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                transaction.Rollback()
                conn.Close()
            End If
            ShowErrorMessage(ex.Message)
        End Try
    End Sub
    Private Function SaveEMPAttendanceCorrection(ByVal conn As SqlConnection, ByVal transaction As SqlTransaction, ByRef ErrNo As Integer) As Boolean
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim hdnStatus As HiddenField
        Dim ddlStatus As RadComboBox
        Dim txtRemarks As RadTextBox
        Dim hdnEMPID As HiddenField
        Dim chkDocChecked As CheckBox
        Dim ATT_DATELabel As Label
        Dim ELT_EVENT As String
        Dim Failure As Boolean
        If RadEmpAttendance.Items.Count > 0 Then
            For Each drRow As GridDataItem In RadEmpAttendance.Items
                hdnStatus = drRow.FindControl("hdnStatus")
                ddlStatus = drRow.FindControl("StatusRadComboBox")
                txtRemarks = drRow.FindControl("RemarksRadTextBox")
                hdnEMPID = drRow.FindControl("hdnEMPID")
                chkDocChecked = drRow.FindControl("chkDocChecked")
                ATT_DATELabel = drRow.FindControl("ATT_DATELabel")
                If CDate(ATT_DATELabel.Text) > Now.Date Then
                    ShowErrorMessage("Cannot process the attendance for future date !!!")
                    Failure = True
                    Exit For
                End If
                If hdnEMPID Is Nothing OrElse hdnStatus Is Nothing _
                OrElse ddlStatus Is Nothing OrElse txtRemarks Is Nothing Then
                    Continue For
                End If
                If ddlStatus.SelectedItem.Text.ToUpper = hdnStatus.Value.ToUpper Then
                    ELT_EVENT = "Supervisor Approval"
                Else
                    ELT_EVENT = "Supervisor Correction"
                End If

                cmd = New SqlCommand("SaveEMP_DAILYATT_CORRECTION_NEW", conn, transaction)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpATT_EMP_ID As New SqlParameter("@ATT_EMP_ID", SqlDbType.BigInt)
                sqlpATT_EMP_ID.Value = hdnEMPID.Value
                cmd.Parameters.Add(sqlpATT_EMP_ID)

                Dim sqlpATT_EAS_ID As New SqlParameter("@ATT_EAS_ID", SqlDbType.BigInt)
                sqlpATT_EAS_ID.Value = 0
                cmd.Parameters.Add(sqlpATT_EAS_ID)

                Dim sqlpATT_ELT_ID As New SqlParameter("@ATT_ELT_ID", SqlDbType.VarChar)
                sqlpATT_ELT_ID.Value = ddlStatus.SelectedValue
                cmd.Parameters.Add(sqlpATT_ELT_ID)

                Dim sqlpATT_EDIT_USR_ID As New SqlParameter("@ATT_EDIT_USR_ID", SqlDbType.VarChar)
                sqlpATT_EDIT_USR_ID.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpATT_EDIT_USR_ID)

                Dim sqlpATT_BSU_ID As New SqlParameter("@ATT_BSU_ID", SqlDbType.VarChar)
                sqlpATT_BSU_ID.Value = Session("sBSUID")
                cmd.Parameters.Add(sqlpATT_BSU_ID)

                Dim sqlpEDP_BDocChecked As New SqlParameter("@EDP_BDocChecked", SqlDbType.Bit)
                sqlpEDP_BDocChecked.Value = chkDocChecked.Checked
                cmd.Parameters.Add(sqlpEDP_BDocChecked)

                Dim sqlpATT_DATE As New SqlParameter("@ATT_DATE", SqlDbType.DateTime)
                sqlpATT_DATE.Value = CDate(ATT_DATELabel.Text).ToString("dd/MMM/yyyy")
                cmd.Parameters.Add(sqlpATT_DATE)

                Dim sqlpATT_REMARKS As New SqlParameter("@ATT_REMARKS", SqlDbType.VarChar)
                sqlpATT_REMARKS.Value = txtRemarks.Text
                cmd.Parameters.Add(sqlpATT_REMARKS)

                Dim sqlpEDP_EVENT As New SqlParameter("@EDP_EVENT", SqlDbType.VarChar)
                sqlpEDP_EVENT.Value = ELT_EVENT
                cmd.Parameters.Add(sqlpEDP_EVENT)

                Dim sqlpCheckLeave As New SqlParameter("@CheckLeaveAvailable", SqlDbType.Bit)
                sqlpCheckLeave.Value = True
                cmd.Parameters.Add(sqlpCheckLeave)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlpbFROM_SUPEERVIOSOR As New SqlParameter("@bFROM_SUPEERVIOSOR", SqlDbType.Bit)
                sqlpbFROM_SUPEERVIOSOR.Value = True
                cmd.Parameters.Add(sqlpbFROM_SUPEERVIOSOR)

                Dim retStatusRemarks As New SqlParameter("@StatusRemarks", SqlDbType.VarChar, 200)
                retStatusRemarks.Direction = ParameterDirection.Output
                cmd.Parameters.Add(retStatusRemarks)

                Dim retLeaveAvailable As New SqlParameter("@LeaveAvailable", SqlDbType.Bit)
                retLeaveAvailable.Direction = ParameterDirection.Output
                cmd.Parameters.Add(retLeaveAvailable)
                drRow.ToolTip = ""
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                ErrNo = iReturnvalue

                If Not retLeaveAvailable.Value Then
                    drRow.ForeColor = Drawing.Color.Red
                    txtRemarks.ForeColor = Drawing.Color.Red
                    ddlStatus.ForeColor = Drawing.Color.Red
                    drRow.ToolTip = retStatusRemarks.Value
                    Failure = True
                Else
                    drRow.BackColor = Nothing
                    txtRemarks.ForeColor = Nothing
                    ddlStatus.ForeColor = Nothing
                    drRow.ToolTip &= Trim(IIf(Trim(drRow.ToolTip) <> "", Environment.NewLine, "") & retStatusRemarks.Value)
                End If

                If iReturnvalue <> 0 Then
                    Failure = True
                    Exit For
                End If

            Next
            If iReturnvalue <> 0 Then
                ShowErrorMessage("Error in updating Audit trial")
            End If
        End If
        Return Failure
    End Function
    Private Sub ShowErrorMessage(ByVal strMessage As String)
        lblError.Text = strMessage
    End Sub
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        If RadFromDate.SelectedDate > Now.Date Or RadToDate.SelectedDate > Now.Date Then
            lblError.Text = "Future date not allowed"
            Exit Sub
        End If
        gridbind(h_Emp_ID.Value, RadFromDate.SelectedDate, RadToDate.SelectedDate)
    End Sub

    Protected Sub h_Emp_ID_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_Emp_ID.ValueChanged

    End Sub

    Protected Sub RadToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles RadToDate.SelectedDateChanged
        If RadFromDate.SelectedDate > Now.Date Or RadToDate.SelectedDate > Now.Date Then
            lblError.Text = "Future date not allowed"
            Exit Sub
        End If
        gridbind(h_Emp_ID.Value, RadFromDate.SelectedDate, RadToDate.SelectedDate)
    End Sub

    Protected Sub RadFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles RadFromDate.SelectedDateChanged
        If RadFromDate.SelectedDate > Now.Date Or RadToDate.SelectedDate > Now.Date Then
            lblError.Text = "Future date not allowed"
            Exit Sub
        End If
        gridbind(h_Emp_ID.Value, RadFromDate.SelectedDate, RadToDate.SelectedDate)
    End Sub
End Class

