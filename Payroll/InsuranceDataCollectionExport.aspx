﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="InsuranceDataCollectionExport.aspx.vb" Inherits="Payroll_InsuranceDataCollectionExport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Staff Insurance Data Collection
        </div>
        <div class="card-body">
            <div class="table-responsive">

     <div align="center">
            <img src="../images/loginImage/oasis-logo-login.png" style="height: 92px;" />
            <h1>
                Staff Insurance Data Collection
            </h1>
            <br />
            <span class="field-label">Business Unit :</span> 
            <div style="width:30%;"><asp:DropDownList ID="ddbsu" runat="server" >
            </asp:DropDownList></div>
            <asp:Button ID="btnDownload" runat="server" Text="Download Excel&Photo" CssClass="button" OnClick="btnDownload_Click"  />
          <asp:Button ID="btnDownloadExcel" runat="server" Text="Download Excel" CssClass="button" OnClick="btnDownloadExcel_Click" />
           <br />
            <asp:Label ID="lblmessage" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            <br />
         </div>

            </div>
        </div>
    </div>
</asp:Content>

