Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class EOSReports
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")

            If Not IsPostBack Then
                If Request.QueryString("MainMnu_code") = "" Then
                    Response.Redirect("..\..\noAccess.aspx")
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle

                Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
                If nodata Then
                    lblError.Text = "No Records with specified condition"
                    Response.Redirect("../homePageSS.aspx", True)
                Else
                    lblError.Text = ""
                End If

                Select Case ViewState("MainMnu_code")
                    Case "U000084"
                        Response.Redirect("LeaveHistoryDashboard.aspx")
                        lblrptCaption.Text = "Employee Leave Details"
                        GenerateEmployeeLeaveDetails()
                End Select
                Response.Redirect("../homePageSS.aspx", True)
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If ViewState("MainMnu_code") = "U000084" Then
                    txtFromDate.Text = UtilityObj.GetDiplayDate()
                    txtToDate.Text = txtFromDate.Text
                    txtToDate.Attributes.Add("onBlur", "checkdate(this)")
                End If
            Else
                Response.Redirect("../homePageSS.aspx", True)
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'Response.Redirect("../homePageSS.aspx", True)
        End Try

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case ViewState("MainMnu_code")
            Case "U000084"
                GenerateEmployeeLeaveDetails()
        End Select
    End Sub
    Private Sub GenerateEmployeeLeaveDetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            Dim objConn As New SqlConnection(str_conn)
            cmd.Connection = objConn
            cmd.CommandText = "RPT_GetEmployeeLeaveDetails"
            Dim sqlParam(6) As SqlParameter
            Dim Sql, bsu_id, dep_id, des_id, cat_id, abc, emp_id, todate As String
            emp_id = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            Sql = "Select EMP_BSU_ID,EMP_DPT_ID,EMP_ABC,EMP_ECT_ID,EMP_DES_ID  from EMPLOYEE_M  WHERE EMP_ID='" & emp_id & "'"
            Dim empdetails As DataTable
            empdetails = Mainclass.getDataTable(Sql, str_conn)
            If empdetails.Rows.Count > 0 Then

                bsu_id = empdetails.Rows(0).Item("EMP_BSU_ID")
                dep_id = empdetails.Rows(0).Item("EMP_DPT_ID")
                abc = empdetails.Rows(0).Item("EMP_ABC")
                cat_id = empdetails.Rows(0).Item("EMP_ECT_ID")
                des_id = empdetails.Rows(0).Item("EMP_DES_ID")
                ' fromdate = UtilityObj.GetDiplayDate()
                Dim sqlFINANCIALYEAR As String
                Dim lYear As String
                Dim fromdate As String
                If Not Request.QueryString("lYear") Is Nothing Then
                    lYear = Request.QueryString("lYear")
                    sqlFINANCIALYEAR = " SELECT max(ELS_DTTO) as maxDate FROM EMPLEAVESCHEDULE_D WHERE ELS_BSU_ID = '" & Session("sBsuid") & "' AND  ELS_EMP_ID='" & emp_id & "' AND (year(ELS_DTFROM) = '" & lYear.ToString & "' OR year(ELS_DTTO)='" & lYear.ToString & "')"
                Else
                    sqlFINANCIALYEAR = " SELECT max(ELS_DTTO) as maxDate FROM EMPLEAVESCHEDULE_D WHERE ELS_BSU_ID = '" & Session("sBsuid") & "' AND  ELS_EMP_ID='" & emp_id & "' AND getdate() BETWEEN ELS_DTFROM AND ELS_DTTO "
                End If
                Dim frmDatatable As DataTable
                frmDatatable = Mainclass.getDataTable(sqlFINANCIALYEAR, str_conn)
                If frmDatatable.Rows.Count > 0 Then
                    fromdate = frmDatatable.Rows(0).Item(0).ToString
                Else
                    fromdate = Now.ToString
                End If

                ' fromdate = Mainclass.getDataTable(sqlFINANCIALYEAR, "OASISConnectionString").Rows(0).Item(0).ToString
                If Not IsDate(fromdate) Then
                    fromdate = UtilityObj.GetDiplayDate()
                Else
                    fromdate = Format(CDate(fromdate), OASISConstants.DateFormat)
                End If

                lYear = Year(fromdate)

                Dim strSelectedNodes As String = bsu_id
                Dim str_bsu_Segment As String = ""
                Dim str_bsu_shortname As String = ""
                Dim str_bsu_CURRENCY As String = ""
                getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, str_bsu_shortname, str_bsu_CURRENCY)

                sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@DPT_ID", "", SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@DES_ID", "", SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
                sqlParam(3) = Mainclass.CreateSqlParameter("@CAT_ID", "", SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(3))
                sqlParam(4) = Mainclass.CreateSqlParameter("@EMP_ABC", "", SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(4))
                sqlParam(5) = Mainclass.CreateSqlParameter("@EMP_ID", emp_id, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(5))
                sqlParam(6) = Mainclass.CreateSqlParameter("@FromDate", fromdate, SqlDbType.DateTime)
                cmd.Parameters.Add(sqlParam(6))
                cmd.Connection = objConn
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("fromDate") = fromdate
                params("toDate") = fromdate
                params("decimal") = Session("BSU_ROUNDOFF")
                params("Bsu_Segments") = str_bsu_Segment
                repSource.Parameter = params
                repSource.Command = cmd
                params("reportCaption") = "Employee Leave Details"
                repSource.ResourceName = Request.PhysicalApplicationPath & "PAYROLL\Reports\Rpt\rptEmployeeLeaveDetails.rpt"
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmployeeLeaveDetails.rpt"

                repSource.DisplayGroupTree = False
                Session("lastMnuCode") = ViewState("MainMnu_code")
                Session("ReportSource") = repSource
                Try
                    Response.Redirect("../Reports/ASPX Report/RptviewerSS.aspx?SelValue=" & lYear.ToString, True)
                Catch ex As Exception

                End Try

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'Response.Redirect("../homePageSS.aspx", True)
        End Try
    End Sub
End Class