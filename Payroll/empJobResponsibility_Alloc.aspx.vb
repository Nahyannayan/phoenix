Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Payroll_empJobResponsibility_Alloc
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Enum JobResponsibility
        EMPAllocation = 0
        BSUnit = 1
    End Enum

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then
                ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P050105" And ViewState("MainMnu_code") <> "P130055") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                txtEmpName.Attributes.Add("readonly", "readonly")
                txtBSUName.Attributes.Add("readonly", "readonly")
                txtRespName.Attributes.Add("readonly", "readonly")
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            txtWBSUName.Text = Session("BSU_Name").ToString
            Select Case ViewState("MainMnu_code")
                Case "P050105"
                    TrEmpName.Visible = False
                    trWBSU.Visible = False
                    lblJobABU.Text = "Business Unit"
                Case "P130055"
                    TrEmpName.Visible = True
                    trWBSU.Visible = True
                    lblJobABU.Text = "Job Allocated Business unit"
            End Select
            If ViewState("datamode") = "add" Then
                txtBSUName.Text = Session("BSU_Name").ToString
                h_BSUID.Value = Session("sBSUID").ToString
            End If
        End If
        If ViewState("datamode") = "view" Then
            Select Case ViewState("MainMnu_code")
                Case "P050105"
                    TrEmpName.Visible = False
                    FillData(JobResponsibility.BSUnit, ViewState("viewid"))
                Case "P130055"
                    TrEmpName.Visible = True
                    FillData(JobResponsibility.EMPAllocation, ViewState("viewid"))
            End Select
        End If
        If ViewState("datamode") = "add" And txtBSUName.Text <> "" And txtRespName.Text <> "" Then
            GetAllocatedJobRespDetails()
        End If
    End Sub

    Sub GetAllocatedJobRespDetails()
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim str_Sql As String = String.Empty
        str_Sql = "select * from dbo.EMP_JOBRESPONSIBILITY_S where ERS_RES_ID = " & h_ResposibilityID.Value & " AND ERS_BSU_ID='" & h_BSUID.Value & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
        While dr.Read()
            txtFrom_date.Text = Format(dr("ERS_FROMDT"), OASISConstants.DateFormat)
            If ((dr("ERS_TODT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("ERS_TODT")), _
            CDate("1/1/1900")) <> 0)) Then txtTo_date.Text = Format(CDate(dr("ERS_TODT").ToString), _
            OASISConstants.DateFormat) Else txtTo_date.Text = ""
            txtAmount.Text = AccountFunctions.Round(dr("ERS_AMOUNT").ToString)
        End While
    End Sub

    Private Sub FillData(ByVal type As JobResponsibility, ByVal guid As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Select Case type
            Case JobResponsibility.BSUnit
                str_Sql = "select *, '' as EMPID,'' as EMPNAME from vw_OSO_JOB_RESPONSIBILITY_ALLOC_BSU where ID = '" & guid & "'"
            Case JobResponsibility.EMPAllocation
                str_Sql = "select * from vw_OSO_JOB_RESPONSIBILITY_ALLOC_EMP where ID = '" & guid & "'"
        End Select
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            txtBSUName.Text = dr("BSU_NAME").ToString
            h_BSUID.Value = dr("BSU_ID").ToString
            txtRespName.Text = dr("RES_DESCR").ToString
            h_ResposibilityID.Value = dr("RES_ID").ToString
            txtEmpName.Text = dr("EMPNAME").ToString
            hfEmp_ID.Value = dr("EMPID").ToString
            txtFrom_date.Text = Format(dr("FROMDT"), OASISConstants.DateFormat)
            If ((dr("TODT").ToString <> "") AndAlso (DateTime.Compare(CDate(dr("TODT")), _
            CDate("1/1/1900")) <> 0)) Then txtTo_date.Text = Format(CDate(dr("TODT").ToString), _
            OASISConstants.DateFormat) Else txtTo_date.Text = ""
            txtAmount.Text = AccountFunctions.Round(dr("AMOUNT").ToString)
        End While
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ServerValidate() Then
            If txtTo_date.Text <> "" Then
                If CDate(txtFrom_date.Text) >= CDate(txtTo_date.Text) Then
                    lblError.Text = "Invalid Date!!!"
                    Exit Sub
                End If
            End If 

            Dim transaction As SqlTransaction
            Dim retVal As Integer
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                Select Case ViewState("MainMnu_code")
                    Case "P050105"
                        'Job Resposibility
                        retVal = SaveJobResponsibility(conn, transaction)
                    Case "P130055"
                        'Job Resposibility Emp Alloc
                        retVal = SaveJobResponsibilityAlloc(conn, transaction)
                End Select
                If retVal = 0 Then
                    transaction.Commit()
                    lblError.Text = "Record Updated Successfully"
                    ClearDetails()
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Else
                    transaction.Rollback()
                    lblError.Text = "Failed to Update Records"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
                lblError.Text = "Failed to Update Records"
            End Try
        End If
    End Sub

    Private Sub ClearDetails()
        txtAmount.Text = ""
        txtBSUName.Text = ""
        txtFrom_date.Text = ""
        txtTo_date.Text = ""
        txtRespName.Text = ""
        h_BSUID.Value = ""
        h_ResposibilityID.Value = ""
        hfEmp_ID.Value = ""
        txtEmpName.Text = ""
    End Sub

    Function SaveJobResponsibilityAlloc(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer

        Dim cmd As New SqlCommand
        cmd = New SqlCommand("SaveEMP_JOBRESPONSILITY_ALLOC", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpERA_ID As New SqlParameter("@ERA_ID", SqlDbType.Int)
        Dim id As Integer = 0
        If Not ViewState("viewid") Is Nothing Then
            id = CInt(ViewState("viewid"))
        End If
        sqlpERA_ID.Value = id
        cmd.Parameters.Add(sqlpERA_ID)

        Dim sqlpERA_BSU_ID As New SqlParameter("@ERA_BSU_ID", SqlDbType.VarChar, 15)
        sqlpERA_BSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpERA_BSU_ID)

        Dim sqlpERA_RES_BSU_ID As New SqlParameter("@ERA_RES_BSU_ID", SqlDbType.VarChar, 15)
        sqlpERA_RES_BSU_ID.Value = h_BSUID.Value
        cmd.Parameters.Add(sqlpERA_RES_BSU_ID)

        Dim sqlpERA_RES_ID As New SqlParameter("@ERA_RES_ID", SqlDbType.Int)
        sqlpERA_RES_ID.Value = h_ResposibilityID.Value
        cmd.Parameters.Add(sqlpERA_RES_ID)

        Dim sqlpERA_EMPID As New SqlParameter("@ERA_EMPID", SqlDbType.Int)
        sqlpERA_EMPID.Value = hfEmp_ID.Value
        cmd.Parameters.Add(sqlpERA_EMPID)

        Dim sqlpERA_FROMDT As New SqlParameter("@ERA_FROMDT", SqlDbType.DateTime)
        sqlpERA_FROMDT.Value = txtFrom_date.Text
        cmd.Parameters.Add(sqlpERA_FROMDT)

        Dim sqlpERA_TODT As New SqlParameter("@ERA_TODT", SqlDbType.DateTime)
        If txtTo_date.Text <> "" Then
            sqlpERA_TODT.Value = txtTo_date.Text
        Else
            sqlpERA_TODT.Value = DBNull.Value
        End If
        cmd.Parameters.Add(sqlpERA_TODT)

        Dim sqlpERA_AMOUNT As New SqlParameter("@ERA_AMOUNT", SqlDbType.Decimal)
        sqlpERA_AMOUNT.Value = txtAmount.Text
        cmd.Parameters.Add(sqlpERA_AMOUNT)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = 0
        If ViewState("datamode").ToString.ToLower = "edit" Then
            sqlpbEdit.Value = 1
        End If
        cmd.Parameters.Add(sqlpbEdit)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        Dim iReturnvalue As Integer = retValParam.Value

        Return iReturnvalue
    End Function

    Function SaveJobResponsibility(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer

        Dim cmd As New SqlCommand
        cmd = New SqlCommand("SaveEMP_JOBRESPONSIBILITY_S", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpERS_ID As New SqlParameter("@ERS_ID", SqlDbType.Int)
        Dim id As Integer = 0
        If Not ViewState("viewid") Is Nothing Then
            id = CInt(ViewState("viewid"))
        End If
        sqlpERS_ID.Value = id
        cmd.Parameters.Add(sqlpERS_ID)

        Dim sqlpERS_BSU_ID As New SqlParameter("@ERS_BSU_ID", SqlDbType.VarChar, 15)
        sqlpERS_BSU_ID.Value = h_BSUID.Value
        cmd.Parameters.Add(sqlpERS_BSU_ID)

        Dim sqlpERS_RES_ID As New SqlParameter("@ERS_RES_ID", SqlDbType.Int)
        sqlpERS_RES_ID.Value = h_ResposibilityID.Value
        cmd.Parameters.Add(sqlpERS_RES_ID)

        Dim sqlpERS_FROMDT As New SqlParameter("@ERS_FROMDT", SqlDbType.DateTime)
        sqlpERS_FROMDT.Value = txtFrom_date.Text
        cmd.Parameters.Add(sqlpERS_FROMDT)

        Dim sqlpERS_TODT As New SqlParameter("@ERS_TODT", SqlDbType.DateTime)
        If txtTo_date.Text <> "" Then
            sqlpERS_TODT.Value = txtTo_date.Text
        Else
            sqlpERS_TODT.Value = DBNull.Value
        End If
        cmd.Parameters.Add(sqlpERS_TODT)

        Dim sqlpERS_AMOUNT As New SqlParameter("@ERS_AMOUNT", SqlDbType.Decimal)
        sqlpERS_AMOUNT.Value = txtAmount.Text
        cmd.Parameters.Add(sqlpERS_AMOUNT)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = 0
        If ViewState("datamode").ToString.ToLower = "edit" Then
            sqlpbEdit.Value = 1
        End If
        cmd.Parameters.Add(sqlpbEdit)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        Dim iReturnvalue As Integer = retValParam.Value

        Return iReturnvalue
    End Function

    Function ServerValidate() As Boolean
        Return True
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        txtBSUName.Text = Session("BSU_Name").ToString
        h_BSUID.Value = Session("sBSUID").ToString
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call ClearDetails()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
