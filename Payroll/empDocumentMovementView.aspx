<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDocumentMovementView.aspx.vb" Inherits="Payroll_empDocumentMovementvIEW" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

        function ShowApprovalDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            //var result;
            //result = window.showModalDialog("empDocumentMovementReturn.aspx?ela_id=" + id, "", sFeatures)

            //return false;
            var url = "empDocumentMovementReturn.aspx?ela_id=" + id;
            var oWnd = radopen(url, "pop_approvaldetail");

            
        } 

        

        function ShowLeaveDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowEmpLeave.aspx?ela_id=" + id, "", sFeatures)

            return false;
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Document 
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        <td align="right" >
                            <asp:RadioButton ID="rbIssued" runat="server" AutoPostBack="True" Checked="True" GroupName="doc" Text="Issued" />
                            <asp:RadioButton ID="rbReturned" runat="server" AutoPostBack="True" GroupName="doc" Text="Returned" />                            
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table style="border-collapse: collapse" align="center"  cellpadding="5" cellspacing="0" width="100%">
                    <tr class="title-bg" valign="top">
                        <td align="left" colspan="4" valign="middle">Document Deatils</td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="matters" colspan="9">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO" CssClass="table table-bordered table-row"
                                SkinID="GridViewView" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="Emp No" SortExpression="EMPNO">
                                        <HeaderTemplate>
                                            Emp No
                                            <br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp Name" SortExpression="EMP_NAME">
                                        <HeaderTemplate>
                                            Emp Name
                                            <br />
                                            <asp:TextBox ID="txtEmpname" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doc Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMD_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Issue Dt">
                                        <HeaderTemplate>
                                            From Date
                                            <br />
                                            <asp:TextBox ID="txtFrom" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("EDM_OUTDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Return Dt">
                                        <HeaderTemplate>
                                            Date
                                            <br />
                                            <asp:TextBox ID="txtTDate" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("EDM_RETURNDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Issued By">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("EMP_ANAME") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("EMP_ANAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks
                                            <br />
                                            <asp:TextBox ID="txtRemarks" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("EDM_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EAT_ID" Visible="False">
                                        <EditItemTemplate>
                                            
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("EDM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Return" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbApprove" runat="server" CausesValidation="False" OnClientClick="<%# &quot;javascript:ShowApprovalDetail('&quot;&Container.DataItem(&quot;EDM_ID&quot;)&&quot;');return true;&quot; %>"
                                                Text='Return'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>


