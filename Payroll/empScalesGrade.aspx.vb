Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Payroll_empScalesGrade
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If

        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")

        If Not IsPostBack Then
            ViewState("datamode") = Encr_decrData.Decrypt(IIf(Request.QueryString("datamode") IsNot Nothing, Request.QueryString("datamode").Replace(" ", "+"), String.Empty))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(IIf(Request.QueryString("MainMnu_code") IsNot Nothing, Request.QueryString("MainMnu_code").Replace(" ", "+"), String.Empty))
            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then
                ViewState("Eid") = Encr_decrData.Decrypt(IIf(Request.QueryString("Eid") IsNot Nothing, Request.QueryString("Eid").Replace(" ", "+"), ""))
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MenuEMPSCALE_GRADE) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MenuEMPSCALE_GRADE
                        SetEmpScaleGrade()
                End Select
            End If
        End If
        'disable the control buttons based on the rights
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        If Not IsPostBack Then
            txtEMPGrade.Attributes.Add("ReadOnly", "ReadOnly")
        End If
        'Make the Controls readOnly if dataMode is View
        If ViewState("datamode") = "view" Then
            MakeControlsReadOnly(True)
        Else
            MakeControlsReadOnly(False)
        End If

    End Sub
    Private Sub SetEmpScaleGrade()
        If ViewState("datamode") <> "add" And ViewState("Eid") IsNot Nothing Then
            SetViewEmpScaleGrade(ViewState("Eid"))
        Else
            ClearScreen()
        End If
    End Sub
    Private Sub SetViewEmpScaleGrade(ByVal Eid As Integer)
        'Get the Data Here
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_Sql As String = "SELECT SGD_EGD_ID, SGD_DESCR FROM EMPSCALES_GRADES_M WHERE SGD_ID = " & Eid
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            str_Sql = "SELECT EGD_DESCR FROM EMPGRADES_M WHERE EGD_ID=" & dr("SGD_EGD_ID")
            txtEMPGrade.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
            h_EMP_EGD_ID.Value = dr("SGD_EGD_ID")
            txtDescr.Text = dr("SGD_DESCR")
        End While
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If Request.UrlReferrer IsNot Nothing Then
        '    Response.Redirect(Request.UrlReferrer.ToString())
        'Else
        '    Response.Redirect("../Homepage.aspx")
        'End If

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call ClearScreen()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect("../Homepage.aspx")
        End If
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        MakeControlsReadOnly(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        MakeControlsReadOnly(False)
        ClearScreen()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub MakeControlsReadOnly(ByVal ctrlReadOnly As Boolean)
        txtEMPGrade.ReadOnly = ctrlReadOnly
        txtDescr.ReadOnly = ctrlReadOnly
    End Sub
    Private Sub ClearScreen()
        txtEMPGrade.Text = ""
        txtDescr.Text = ""
    End Sub

    Private Function SaveEMPSCALE_GRADE(ByRef bUpdated As Boolean) As String
        bUpdated = False
        Dim transaction As SqlTransaction
        Try
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("InsertEMPSCALE_GRADE")
                Try
                    Dim cmd As New SqlCommand("Save_EMPSCALES_GRADES_M", conn, transaction)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpSGD_ID As New SqlParameter("@SGD_ID", SqlDbType.Int)
                    If ViewState("datamode") = "add" Then
                        sqlpSGD_ID.Value = 0
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpSGD_ID.Value = ViewState("Eid")
                    End If
                    cmd.Parameters.Add(sqlpSGD_ID)

                    Dim sqlpSGD_EGD_ID As New SqlParameter("@SGD_EGD_ID", SqlDbType.Int)
                    sqlpSGD_EGD_ID.Value = h_EMP_EGD_ID.Value
                    cmd.Parameters.Add(sqlpSGD_EGD_ID)

                    Dim sqlpSGD_DESCR As New SqlParameter("@SGD_DESCR", SqlDbType.VarChar, 100)
                    sqlpSGD_DESCR.Value = txtDescr.Text
                    cmd.Parameters.Add(sqlpSGD_DESCR)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    If ViewState("datamode") = "add" Then
                        sqlpbEdit.Value = False
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                    End If

                    cmd.Parameters.Add(sqlpbEdit)

                    If cmd.ExecuteNonQuery() = 1 Then
                        bUpdated = True
                        transaction.Commit()
                        Return "The Data successfully saved"
                    Else
                        transaction.Rollback()
                        Return "Failed to Save the Data"
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    UtilityObj.Errorlog(ex.Message)
                    Return "Failed to Save the Data"
                End Try
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ex.Message
            Return "Failed to Save the Data"
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            Dim bUpdated As Boolean
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MenuEMPSCALE_GRADE
                    lblError.Text = SaveEMPSCALE_GRADE(bUpdated)
            End Select
            If bUpdated Then
                ViewState("datamode") = "none"
                MakeControlsReadOnly(False)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ClearScreen()
            End If
        End If
    End Sub

End Class
