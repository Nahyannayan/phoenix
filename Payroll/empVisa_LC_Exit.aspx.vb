﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Xml
Partial Class Payroll_empVisa_LC_Exit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            ViewState("ChkDeduction") = ""
            txtEmpNo.Attributes.Add("readonly", "readonly")
        

           


            ViewState("iID") = 0
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
           
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            '            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153070" And ViewState("MainMnu_code") <> "H000082" And ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("MainMnu_code") = "P153072" Then
                Session("menu_type") = "EMP_bVISACANCEL"
                TBLVISACANCEL.Visible = True
                btnAdd.Visible = False
                btnSave.Text = "Save"
            ElseIf ViewState("MainMnu_code") = "P153073" Then
                Session("menu_type") = "EMP_bLCCANCEL"
                TBLLCCANCEL.Visible = True
                btnAdd.Visible = False
                btnSave.Text = "Save"
            ElseIf ViewState("MainMnu_code") = "P153074" Then
                Session("menu_type") = "EMP_bEXIT"
                TBLEMPEXIT.Visible = True
                btnAdd.Visible = False
                btnSave.Text = "Save"
            End If
            If (ViewState("MainMnu_code") = "P153072" Or ViewState("MainMnu_code") = "P153073" Or ViewState("MainMnu_code") = "P153074") And (ViewState("datamode") = "edit" Or ViewState("datamode") = "view") Then
                ViewState("Emp_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                FillResignationDetails(ViewState("Emp_ID"))
            End If


            If Request.QueryString("viewid") <> "" Then
                LockControls(True)
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                LockControls(False)
            End If
        End If
        If ViewState("MainMnu_code") = "P153072" Then
            lblTitle.Text = "Employee Visa Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153073" Then
            lblTitle.Text = "Employee Labour Card Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153074" Then
            lblTitle.Text = "Employee Exit"
        ElseIf ViewState("MainMnu_code") = "P153070" Then
            lblTitle.Text = "Final Settlement"
            btnAdd.Visible = False
            imgEmployee.Visible = False
        ElseIf ViewState("MainMnu_code") = "H000082" Then
            lblTitle.Text = "Approve Final Settlement"
        End If
        PopupControlExtender1.DynamicContextKey = ViewState("Emp_ID")
    End Sub

    Sub FillResignationDetails(ByVal RegId As String)
        Try
            Dim visatype As String, visastatus As String
            Dim Param(1) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@Emp_ID", RegId)

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetEmp_visa_lc_dets", Param)
            If (ds.Tables(0).Rows.Count > 0) Then
                lnkLog.Style.Add("display", "block")


                txtEmpNo.Text = ds.Tables(0).Rows(0).Item("EMPNO").ToString
                lbl_Empname.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
                lbldesg.Text = ds.Tables(0).Rows(0).Item("EMP_DES_DESCR").ToString
                lbldpt.Text = ds.Tables(0).Rows(0).Item("EMP_DEPT_DESCR").ToString


                ' DropDownListResignTerm.SelectedValue = ds.Tables(0).Rows(0).Item("EREG_REGTYPE").ToString
                h_Emp_No.Value = ds.Tables(0).Rows(0).Item("EMP_ID").ToString
                lblVisaBSU.Text = ds.Tables(0).Rows(0).Item("VISA_BSU_NAME").ToString
                lblWorkingBSU.Text = ds.Tables(0).Rows(0).Item("WORK_BSU_NAME").ToString
                lblDOJBSU.Text = Format(ds.Tables(0).Rows(0).Item("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                lblDOJGroup.Text = Format(ds.Tables(0).Rows(0).Item("EMP_JOINDT"), OASISConstants.DateFormat)
                lblLWD.Text = Format(ds.Tables(0).Rows(0).Item("EMP_LASTATTDT"), OASISConstants.DateFormat)

                ViewState("visatype") = ds.Tables(0).Rows(0).Item("EMP_VISATYPE").ToString
                ViewState("visastatus") = ds.Tables(0).Rows(0).Item("EMP_VISASTATUS").ToString

                If ViewState("visatype") = "0" And ViewState("visastatus") = "2" Then
                    TBLVISACANCEL.Visible = True
                Else
                    TBLVISACANCEL.Visible = False
                End If


                If IsDate(ds.Tables(0).Rows(0).Item("VISACANCEL_DT")) Then txtVisaCancelDt.Text = Format(ds.Tables(0).Rows(0).Item("VISACANCEL_DT"), OASISConstants.DateFormat)
                If IsDate(ds.Tables(0).Rows(0).Item("LC_DT")) Then txtLCCancelDt.Text = Format(ds.Tables(0).Rows(0).Item("LC_DT"), OASISConstants.DateFormat)
                If IsDate(ds.Tables(0).Rows(0).Item("EXIT_DT")) Then txtExitDt.Text = Format(ds.Tables(0).Rows(0).Item("EXIT_DT"), OASISConstants.DateFormat)
                If txtVisaCancelDt.Text <> "" Then rbVisaYes.Checked = True Else rbVisaNo.Checked = True
                If txtLCCancelDt.Text <> "" Then rbLCYes.Checked = True Else rbLCNo.Checked = True
                If txtExitDt.Text <> "" Then rbExitYes.Checked = True Else rbExitNo.Checked = True


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim vGSD_GSM_SLAB_ID As String = contextKey
        Dim drReader As SqlDataReader
        Try
            'Dim str_Sql As String = "SELECT REPLACE(CONVERT(VARCHAR(11), AUD_LOG_DATE, 106), ' ', '/') +' '+STUFF(RIGHT( CONVERT(VARCHAR,AUD_LOG_DATE,100 ) ,7), 6, 0, ' ') as AUD_LOG_DATE,AUD_USR_NAME,AUD_TYPE,REPLACE(CONVERT(VARCHAR(11), AUD_PRE_DATE, 106), ' ', '/')  as AUD_PRE_DATE FROM AUDIT_UPDATE_VISA_LC_CANCEL WHERE  " & _
            '"  aud_regid = " & vGSD_GSM_SLAB_ID & _
            '" order by AUD_id desc "

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim Param(1) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@Emp_ID", vGSD_GSM_SLAB_ID)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "get_visa_lc_Log", Param)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='popdetails' width=100%>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=4 class='title-bg-lite'><b>Details</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            sTemp.Append("<td><b>Log Date</b></td>")
            sTemp.Append("<td><b>User Name</b></td>")
            sTemp.Append("<td><b>Type</b></td>")
            sTemp.Append("<td><b>Cancel Date</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drReader("AUD_LOG_DATE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AUD_USR_NAME").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AUD_TYPE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AUD_PRE_DATE").ToString & "</td>")
                sTemp.Append("</tr>")
            End While

        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function
    Private Sub LockControls(ByVal mDisable As Boolean)
        'imgTo.Enabled = True
        If ViewState("datamode") = "add" Then
            imgEmployee.Enabled = Not mDisable
        Else
            imgEmployee.Enabled = False
        End If


        btnSave.Visible = Not mDisable

        rbVisaYes.Enabled = Not mDisable
        rbVisaNo.Enabled = Not mDisable
        rbLCYes.Enabled = Not mDisable
        rbLCNo.Enabled = Not mDisable
        rbExitNo.Enabled = Not mDisable
        rbExitYes.Enabled = Not mDisable

        imgVisaCancelDt.Enabled = rbVisaYes.Checked And Not mDisable
        imgLCCancelDt.Enabled = rbLCYes.Checked And Not mDisable
        imgExitDt.Enabled = rbExitYes.Checked And Not mDisable
        txtVisaCancelDt.ReadOnly = Not rbVisaYes.Checked Or mDisable
        txtLCCancelDt.ReadOnly = Not rbLCYes.Checked Or mDisable
        txtExitDt.ReadOnly = Not rbExitYes.Checked Or mDisable


        If ViewState("ACCPOSTED") And ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074" Then
            btnApprove.Visible = False
            btnSave.Visible = False
            btnEdit.Visible = False
        Else
            btnSave.Visible = Not mDisable
            btnEdit.Visible = mDisable
            If ViewState("MainMnu_code") = "H000082" Then
                btnApprove.Visible = True
            Else
                btnApprove.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ViewState("MainMnu_code") = "P153072" Or ViewState("MainMnu_code") = "P153073" Or ViewState("MainMnu_code") = "P153074" Then
                Dim mCancelDt As String = ""
                Dim mVisaCancelDt As String = ""
                Dim CancelType As String = ""

                If ViewState("MainMnu_code") = "P153072" Then
                    If rbVisaYes.Checked Then
                        If Not IsDate(txtVisaCancelDt.Text) Then
                            lblError.Text = "Invalid Date !!!"
                            Exit Sub
                        End If

                        mCancelDt = CDate(txtVisaCancelDt.Text).ToString("dd/MMM/yyyy")


                    Else
                        mCancelDt = ""
                    End If
                    CancelType = "VISA"
                ElseIf ViewState("MainMnu_code") = "P153073" Then
                    If rbLCYes.Checked Then
                        If Not IsDate(txtLCCancelDt.Text) Then
                            lblError.Text = "Invalid Date !!!"
                            Exit Sub
                        End If
                        mCancelDt = CDate(txtLCCancelDt.Text).ToString("dd/MMM/yyyy")
                    Else
                        mCancelDt = ""
                    End If
                    If ViewState("visatype") = "0" And ViewState("visastatus") = "2" Then
                        If rbVisaYes.Checked Then
                            If Not IsDate(txtVisaCancelDt.Text) Then
                                lblError.Text = "Invalid Date !!!"
                                Exit Sub
                            End If

                            mVisaCancelDt = CDate(txtVisaCancelDt.Text).ToString("dd/MMM/yyyy")


                        Else
                            mVisaCancelDt = ""
                        End If
                    End If

                    CancelType = "LC"
                ElseIf ViewState("MainMnu_code") = "P153074" Then
                    If rbExitYes.Checked Then
                        If Not IsDate(txtExitDt.Text) Then
                            lblError.Text = "Invalid Date !!!"
                            Exit Sub
                        End If
                        mCancelDt = CDate(txtExitDt.Text).ToString("dd/MMM/yyyy")
                    Else
                        mCancelDt = ""
                    End If
                    CancelType = "EXIT"
                End If

                If CDate(mCancelDt) > CDate(Now).ToString("dd/MMM/yyyy") Then
                    lblError.Text = "Invalid Date !!! Date entry should not allow future dates"
                    Exit Sub
                End If

                Dim sqlParam(5) As SqlParameter
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                sqlParam(0) = Mainclass.CreateSqlParameter("@EREG_ID", Val(ViewState("Emp_ID")), SqlDbType.Int)
                sqlParam(1) = Mainclass.CreateSqlParameter("@CANCEL_DT", mCancelDt, SqlDbType.VarChar)
                sqlParam(2) = Mainclass.CreateSqlParameter("@VISA_CANCEL_DT", mVisaCancelDt, SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@CANCEL_BY", Session("sUsr_name"), SqlDbType.VarChar)
                sqlParam(4) = Mainclass.CreateSqlParameter("@CANCEL_TYPE", CancelType, SqlDbType.VarChar)
                sqlParam(5) = Mainclass.CreateSqlParameter("@v_ReturnMsg", "", SqlDbType.VarChar, True)

                Dim str_success As String
                str_success = Mainclass.ExecuteParamQRY(str_conn, "UPDATE_VISA_LC_CANCEL", sqlParam)
                str_success = sqlParam(5).Value
                If str_success = 0 Then
                    lblError.Text = getErrorMessage("0")
                    LockControls(True)
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Error occured while processing the record."
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            LockControls(True)
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ViewState("ACCPOSTED") = False
        LockControls(False)
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub clear_All()
        h_Emp_No.Value = ""
        txtEmpNo.Text = ""


        txtEmpNo.Text = ""
        lbl_Empname.Text = ""
        lbldesg.Text = ""
        lbldpt.Text = ""


        DropDownListResignTerm.SelectedIndex = -1
        h_Emp_No.Value = ""
        lblVisaBSU.Text = ""
        lblWorkingBSU.Text = ""
        lblDOJBSU.Text = ""
        lblDOJGroup.Text = ""

    End Sub



    Protected Sub txtEmpNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNo.TextChanged
        bindEmployeeDetails()
    End Sub
    Sub bindEmployeeDetails()
        Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
        Using EmpReader As SqlDataReader = SqlHelper.ExecuteReader(sqlcon, CommandType.Text, "SELECT O.*,D.DPT_DESCR DPT_NAME,S.DES_DESCR DES_NAME  FROM vw_OSO_EMPLOYEEMASTERDETAILS O,DEPARTMENT_M D ,EMPDESIGNATION_M S where  O.EMP_DPT_ID=D.DPT_ID AND O.EMP_DES_ID=S.DES_ID AND EMPNO='" & txtEmpNo.Text & "'")

            If EmpReader.HasRows = True Then
                While EmpReader.Read
                    ViewState("Emp_ID") = Convert.ToString(EmpReader("EMP_ID"))
                    h_Emp_No.Value = Convert.ToString(EmpReader("EMP_ID"))
                    lbl_Empname.Text = Convert.ToString(EmpReader("EMPNAME"))
                    lbldpt.Text = Convert.ToString(EmpReader("DPT_NAME"))
                    lbldesg.Text = Convert.ToString(EmpReader("DES_NAME"))
                    lblVisaBSU.Text = Convert.ToString(EmpReader("VISA_BSU_NAME"))
                    lblWorkingBSU.Text = Convert.ToString(EmpReader("WORK_BSU_NAME"))
                    lblDOJBSU.Text = Format(EmpReader("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                    lblDOJGroup.Text = Format(EmpReader("EMP_JOINDT"), OASISConstants.DateFormat)
                    lblLWD.Text = Format(EmpReader("EMP_LASTATTDT"), OASISConstants.DateFormat)
                    lnkLog.Style.Add("display", "block")
                    ViewState("visatype") = Convert.ToString(EmpReader("EMP_VISATYPE"))
                    ViewState("visastatus") = Convert.ToString(EmpReader("EMP_VISASTATUS"))

                    If ViewState("visatype") = "0" And ViewState("visastatus") = "2" Then
                        TBLVISACANCEL.Visible = True
                    Else
                        TBLVISACANCEL.Visible = False
                    End If
                End While
            Else
                h_Emp_No.Value = ""
                lbl_Empname.Text = ""
                lbldpt.Text = ""
                lbldesg.Text = ""
            End If
        End Using
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        
        LockControls(False)
        btnSave.Visible = True
    End Sub

    Protected Sub RdVisaStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbVisaYes.CheckedChanged
        If rbVisaYes.Checked Then
            txtVisaCancelDt.Enabled = True
            imgVisaCancelDt.Enabled = True
        Else
            txtVisaCancelDt.Enabled = False
            imgVisaCancelDt.Enabled = False
        End If
    End Sub
    Protected Sub RdLCStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLCYes.CheckedChanged
        If rbLCYes.Checked Then
            txtLCCancelDt.Enabled = True
            imgLCCancelDt.Enabled = True
        Else
            txtLCCancelDt.Enabled = False
            imgLCCancelDt.Enabled = False
        End If
    End Sub
    Protected Sub RdExitStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbExitYes.CheckedChanged
        If rbExitYes.Checked Then
            txtExitDt.Enabled = True
            imgExitDt.Enabled = True
        Else
            txtExitDt.Enabled = False
            imgExitDt.Enabled = False
        End If
    End Sub

    Protected Sub rbVisaNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbVisaNo.CheckedChanged
        If rbVisaNo.Checked Then
            txtVisaCancelDt.Enabled = False
            imgVisaCancelDt.Enabled = False
        Else
            txtVisaCancelDt.Enabled = True
            imgVisaCancelDt.Enabled = True
        End If
    End Sub

    Protected Sub rbLCNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbLCNo.CheckedChanged
        If rbLCNo.Checked Then
            txtLCCancelDt.Enabled = False
            imgLCCancelDt.Enabled = False
        Else
            txtLCCancelDt.Enabled = True
            imgLCCancelDt.Enabled = True
        End If
    End Sub

    Protected Sub rbExitNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbExitNo.CheckedChanged
        If rbExitNo.Checked Then
            txtExitDt.Enabled = False
            imgExitDt.Enabled = False
        Else
            txtExitDt.Enabled = True
            imgExitDt.Enabled = True
        End If
    End Sub
End Class
