Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet
Imports Telerik.Web.UI
Partial Class Payroll_empSalaryTransferView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    '1.1           Swapna              10-Apr-2011     To display records  processed through WPS only(mnu_code:P130171)
    '                                                   and for records not processed through WPS (P130170)
    '1.2           Shijin               8-May-2011      To display cash/bank transfer records
    '1.3            Swapna              18-Sep-2011     To modify bank transfer letter fields
    '1.4            Swapna              27-Mar-2012     To add refdocno filter for Export , sort records datewise
    '1.5            Swapna              23-apr-2012     nEW wps CHANGES
    '1.6            Swapna              17/Apr/2014     Non WPS new format
    '1.7            Swapna              1-Dec-2015      To redirect AX unit's postings to new screen.

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If ViewState("MainMnu_code") = "P130180" Then
                    hlAddNew.Visible = True
                    lblHeading.Visible = True
                    rcbSignatory.Visible = True
                    lblHeading2.Visible = True
                    rcbSignatory2.Visible = True
                    If Session("BSU_IsOnDAX") = 1 Then
                        hlAddNew.NavigateUrl = "AXempSalaryTransferCash.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add") 'V1.7
                    Else
                        hlAddNew.NavigateUrl = "empSalaryTransferCash.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    End If
                Else
                    hlAddNew.Visible = False
                    lblHeading.Visible = False
                    rcbSignatory.Visible = False
                    lblHeading2.Visible = False
                    rcbSignatory2.Visible = False
                    'shlAddNew.NavigateUrl = "empSalaryTransfer.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                End If

                    If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P130170" And ViewState("MainMnu_code") <> "P130171" And ViewState("MainMnu_code") <> "P130172" _
                    And ViewState("MainMnu_code") <> "P130180") Then
                        If Not Request.UrlReferrer Is Nothing Then
                            Response.Redirect(Request.UrlReferrer.ToString())
                        Else
                            Response.Redirect("~\noAccess.aspx")
                        End If
                    Else
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        'rcbSignatory_Initialize()
                        rcbSignatory_ItemsRequested() 'V1.6 
                        rcbSignatory2_ItemsRequested() 'V1.6 
                        gridbind()

                End If
                If ViewState("MainMnu_code") = "P130170" Then
                    lblTitle.Text = "Salary Transfer (Bank)"
                ElseIf ViewState("MainMnu_code") = "P130171" Then
                    lblTitle.Text = "Salary Transfer (WPS)"
                ElseIf ViewState("MainMnu_code") = "P130172" Then
                    lblTitle.Text = "Salary Transfer (GIRO)"
                ElseIf ViewState("MainMnu_code") = "P130180" Then
                    lblTitle.Text = "Salary Transfer(Cash/Bank)"

                End If


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            'Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            'Dim larrSearchOpr() As String
            'Dim lstrOpr As String
            'Dim txtSearch As New TextBox

            'lstrCondn1 = ""
            'lstrCondn2 = ""
            'lstrCondn3 = ""
            'lstrCondn4 = ""
            'lstrCondn5 = ""

            'str_Filter = ""
            'If gvJournal.Rows.Count > 0 Then
            '    ' --- Initialize The Variables


            '    larrSearchOpr = h_selected_menu_1.Value.Split("__")
            '    lstrOpr = larrSearchOpr(0)


            '    '   --- FILTER CONDITIONS ---
            '    '   -- 1   txtEmpNo
            '    larrSearchOpr = h_selected_menu_1.Value.Split("__")
            '    lstrOpr = larrSearchOpr(0)
            '    txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            '    lstrCondn1 = Trim(txtSearch.Text)
            '    If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

            '    '   -- 1  txtEmpname
            '    larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            '    lstrOpr = larrSearchOpr(0)
            '    txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            '    lstrCondn2 = Trim(txtSearch.Text)
            '    If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

            '    '   -- 2  txtLType
            '    larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            '    lstrOpr = larrSearchOpr(0)
            '    txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
            '    lstrCondn3 = txtSearch.Text
            '    If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELT_DESCR", lstrCondn3)

            '    '   -- 3   txtDate

            '    larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            '    lstrOpr = larrSearchOpr(0)
            '    txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            '    lstrCondn4 = txtSearch.Text
            '    If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELV_DTFROM", lstrCondn4)

            '    '   -- 5  city

            '    larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            '    lstrOpr = larrSearchOpr(0)
            '    txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            '    lstrCondn5 = txtSearch.Text
            '    If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELV_REMARKS", lstrCondn5)


            'End If


            str_Sql = "SELECT EBT_REFDOCNO, EBT_OURBANK_ACT_ID, BANK, " _
            & " SUM(ESD_EARN_NET) AS AMOUNYT, EBT_DATE, ESD_BSU_ID, EBT_NARRATION,Isnull(ACT_IBAN_NO,'') ACT_IBAN_NO" _
            & " FROM  VW_OSO_SALARYTRANSFERTOBANK" _
            & " GROUP BY EBT_REFDOCNO, EBT_OURBANK_ACT_ID, BANK, EBT_DATE, ESD_BSU_ID,EBT_NARRATION,ESD_BProcessWPS,ACT_IBAN_NO " _
            & " HAVING      (ESD_BSU_ID = '" & Session("sBSUID") & "') and ISNULL(EBT_REFDOCNO,'')<>''"
            'V1.1 addition
            If ViewState("MainMnu_code") = "P130170" Then
                str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=0  "
            ElseIf ViewState("MainMnu_code") = "P130171" Or ViewState("MainMnu_code") = "P130172" Then
                str_Sql = "SELECT EBT_REFDOCNO, EBT_OURBANK_ACT_ID, BANK, " _
                            & " SUM(ESD_EARN_NET) AS AMOUNYT, EBT_DATE, ESD_BSU_ID, EBT_NARRATION ,Isnull(ACT_IBAN_NO,'') ACT_IBAN_NO " _
                            & " FROM  VW_OSO_SALARYTRANSFERTOBANK_WPS" _
                            & " GROUP BY EBT_REFDOCNO, EBT_OURBANK_ACT_ID, BANK, EBT_DATE, ESD_BSU_ID,EBT_NARRATION,ESD_BProcessWPS,ACT_IBAN_NO " _
                            & " HAVING      (ESD_BSU_ID = '" & Session("sBSUID") & "')and ISNULL(EBT_REFDOCNO,'')<>''"
                str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=1  "
            ElseIf ViewState("MainMnu_code") = "P130180" Then
                str_Sql = "SELECT EBT_REFDOCNO, EBT_OURBANK_ACT_ID, BANK, " _
                            & " SUM(ESD_EARN_NET) AS AMOUNYT, EBT_DATE, ESD_BSU_ID, EBT_NARRATION,Isnull(ACT_IBAN_NO,'') ACT_IBAN_NO " _
                            & " FROM  VW_OSO_SALARYTRANSFERTOBANK_WPS" _
                            & " GROUP BY EBT_REFDOCNO, EBT_OURBANK_ACT_ID, BANK, EBT_DATE, ESD_BSU_ID,EBT_NARRATION,ESD_BProcessWPS,ACT_IBAN_NO " _
                            & " HAVING      (ESD_BSU_ID = '" & Session("sBSUID") & "')and ISNULL(EBT_REFDOCNO,'')<>''"
                str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=0  "
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & " order by EBT_DATE desc")  ' V1.4
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            'txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            'txtSearch.Text = lstrCondn1

            'txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            'txtSearch.Text = lstrCondn2

            'txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
            'txtSearch.Text = lstrCondn3

            'txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            'txtSearch.Text = lstrCondn4

            'txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            'txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empLeaveHistory.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblDocNo As Label = sender.parent.findcontrol("lblDocNo")
            If lblDocNo Is Nothing Then
                Return
            End If
            If lblDocNo.Text = "" Then
                Return
            End If

            Dim lblDate As Label = sender.parent.findcontrol("lblDate")
            If lblDate Is Nothing Then
                Return
            End If
            If lblDate.Text = "" Then
                Return
            End If

            Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", lblDocNo.Text, False, True)
            'Session("ReportSource") = AccountsReports.BankPaymentVoucher(lblDocNo.Text, lblDate.Text, Session("sBSUID"), Session("F_YEAR"))
            ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
            ReportLoadSelection()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub lbBankTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblDate As Label = sender.parent.findcontrol("lblDate")
            If lblDate Is Nothing Then
                Return
            End If
            If lblDate.Text = "" Then
                Return
            End If

            Dim tDate As Date = Convert.ToDateTime(lblDate.Text)

            Dim lblDocNo As Label = sender.parent.findcontrol("lblDocNo")
            If lblDocNo Is Nothing Then
                Return
            End If
            If lblDocNo.Text = "" Then
                Return
            End If

            If ViewState("MainMnu_code") <> "P130180" Then
                Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

                Dim cmd As New SqlCommand("DebitNote_Authority_Letter", objConn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpPAYMONTH As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
                sqlpPAYMONTH.Value = tDate.Month
                cmd.Parameters.Add(sqlpPAYMONTH)

                Dim sqlpPAYYEAR As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
                sqlpPAYYEAR.Value = tDate.Year
                cmd.Parameters.Add(sqlpPAYYEAR)

                Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpBSU_ID.Value = Session("sBsuid")
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpCAT_ID As New SqlParameter("@BP_NO", SqlDbType.VarChar)
                sqlpCAT_ID.Value = lblDocNo.Text '"10BP-0001108"
                cmd.Parameters.Add(sqlpCAT_ID)

                Dim repSourceSubRep(1) As MyReportClass
                repSourceSubRep(0) = New MyReportClass

                Dim objConn1 As SqlConnection = ConnectionManger.GetOASISConnection

                Dim cmdsub As New SqlCommand("DebitNote_Authority_Letter_Details", objConn1)
                cmdsub.CommandType = CommandType.StoredProcedure

                Dim sqlpPAYMONTH_sub As New SqlParameter("@PAY_MONTH", SqlDbType.Int)
                sqlpPAYMONTH_sub.Value = tDate.Month
                cmdsub.Parameters.Add(sqlpPAYMONTH_sub)

                Dim sqlpPAYYEAR_sub As New SqlParameter("@PAY_YEAR", SqlDbType.Int)
                sqlpPAYYEAR_sub.Value = tDate.Year
                cmdsub.Parameters.Add(sqlpPAYYEAR_sub)

                Dim sqlpBSU_ID_sub As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpBSU_ID_sub.Value = Session("sBsuid")
                cmdsub.Parameters.Add(sqlpBSU_ID_sub)

                Dim sqlpBP_NO_sub As New SqlParameter("@BP_NO", SqlDbType.VarChar)
                sqlpBP_NO_sub.Value = lblDocNo.Text '"10BP-0001108"
                cmdsub.Parameters.Add(sqlpBP_NO_sub)

                repSourceSubRep(0).Command = cmdsub

                'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                'params("userName") = Session("sUsr_name")
                'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
                'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

                repSource.SubReport = repSourceSubRep
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptDebit_Authority_Letter.rpt"
                Session("ReportSource") = repSource
                'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            Else
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
                'v1.5

                ''Dim str_Sql As String = "select ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
                ''GetFilter("ESD_EMP_ID", h_EMPID.Value, False) & _
                ''" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue
                ' swapna commented
                '          Dim strFiltBSUID As String = "WHERE     (ESD_BSU_ID = '" & Session("sBsuid") & "') AND (EBT_DATE = CONVERT(DATETIME, '" & lblDate.Text & "', 102))"
                '          Dim str_Sql As String = "SELECT ACT_BANKACCNO as [EBT_OURBANK_ACT_ID],[BANK],[BNK_DESCRIPTION],[EBT_AMOUNT],[CHQNO]," _
                '& "[ESD_EARN_NET],case [ESD_ACCOUNT] when '' then 'CASH' else [ESD_ACCOUNT] end as ESD_ACCOUNT,[EMP_SALUTE],[EMP_FNAME],[EMP_MNAME],[EMP_LNAME],[EBT_REFDOCNO],[ESD_BSU_ID]," _
                '& " [EBT_DATE],[ESD_MONTH],[ESD_YEAR],[ESD_CUR_ID],[BSU_NAME],[EBT_NARRATION],[EMP_ECT_ID],[ESD_BProcessWPS],BNK_SHORT,BankCode," _
                '& " chq_short_address,chq_wps_contact,chq_wps_address , BSU_AccOfficerEmail, BSU_ACCOfficerContactNO, BSU_ACCOfficerMobileNO, BSU_ACCOfficerName,EBT_PURPOSE" _
                '& ", EBT_VALUE_Date from  VW_OSO_SALARYTRANSFERTOBANK_WPS inner join businessunit_sub on ESD_BSU_ID= BUS_BSU_ID " 'V1.3
                '          str_Sql = str_Sql + strFiltBSUID
                '          'V1.1 addition
                '          'If ViewState("MainMnu_code") = "P130170" Then
                '          str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=0  "
                '          str_Sql = str_Sql & "AND EBT_REFDOCNO ='" & lblDocNo.Text & "'"
                '          'ElseIf ViewState("MainMnu_code") = "P130171" Then
                '          'str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=1  "
                '          ' End If
                Dim str_Sql As String = " exec GetNonWPSprintDetails '" & Session("sBsuid") & "','" & lblDate.Text & "','" & lblDocNo.Text & "'"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                    Dim cmd As New SqlCommand
                    cmd.CommandText = str_Sql
                    cmd.Connection = New SqlConnection(str_conn)
                    cmd.CommandType = CommandType.Text

                    Dim repSource As New MyReportClass
                    Dim params As New Hashtable

                    params("UserName") = Session("sUsr_name")
                    'V1.6  starts
                    If rcbSignatory.Items.Count > 0 Then
                        params("SignatoryName") = rcbSignatory.SelectedItem.Text 'V1.4   
                        params("SignatoryDesg") = rcbSignatory.SelectedItem.Value.Split(",")(0)
                    Else
                        params("SignatoryName") = "" 'V1.4   
                        params("SignatoryDesg") = "" 'V1.4   
                    End If
                    If rcbSignatory2.Items.Count > 0 Then
                        params("SignatoryName2") = rcbSignatory2.SelectedItem.Text 'V1.4   
                        params("SignatoryDesg2") = rcbSignatory2.SelectedItem.Value.Split(",")(0)
                    Else
                        params("SignatoryName2") = "" 'V1.4   
                        params("SignatoryDesg2") = "" 'V1.4   
                    End If
                    If rcbSignatory2.SelectedItem.Value.Split(",")(0) = "--" Then
                        params("SignatoryName2") = "" 'V1.4   
                        params("SignatoryDesg2") = "" 'V1.4   
                    End If

                    ''V1.6  ends
                    repSource.Parameter = params
                    repSource.IncludeBSUImage = True
                    repSource.HeaderBSUID = Session("sBsuid")
                    repSource.Command = cmd
                    'repSource.ResourceName = "../../payroll/Reports/RPT/rptSALARYTRANSFERTOBANK.rpt"
                    repSource.ResourceName = "../../payroll/Reports/RPT/Non-WPSPrintNewFormat2.rpt" 'V1.6 
                    If Session("sBsuid") = "800444" Then
                        repSource.ResourceName = "../../payroll/Reports/RPT/rptDebitAuthorityletter_Qatar.rpt" 'V1.6 ()
                    End If


                    Session("ReportSource") = repSource
                    'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                    ReportLoadSelection()
                    'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                Else
                    lblError.Text = "No Records with specified condition"
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ExporttoExcel(ByVal ds As DataSet, ByVal IbanNo As String)

        Dim dt As DataTable

        dt = ds.Tables(0)
        'swapna added for name format
        Dim strmonth As String, strMinute As String
        strmonth = CStr(Today.Date.Month)
        strMinute = CStr(Now.Minute.ToString)

        If strmonth.Length = 1 Then
            strmonth = "0" + strmonth
        End If
        If strMinute.Length = 1 Then
            strMinute = "0" + strMinute
        End If


        'swapna adding for naming format
        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + IbanNo.Substring(IbanNo.Length - 12).Replace("-", "") + CStr(Today.Date.Day) + strmonth + CStr(Today.Date.Year) + Now.Hour.ToString + strMinute + ".xlsx" ' Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") +

        ' Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls"
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        'V1.6  starts
        ws.Columns(0).Width = 20 * 256
        ws.Columns(1).Width = 40 * 256
        ws.Columns(2).Width = 15 * 256
        Dim colTot As Integer = ds.Tables(0).Columns.Count

        For c As Integer = 3 To colTot - 1
            ws.Columns(c).Width = 16 * 256
        Next
        'V1.6  ends
        Dim rowTot As Integer = dt.Rows.Count
        If Session("BSU_COUNTRY_ID") = 97 Then
            ws.Columns(4).Style.NumberFormat = "#,##0.00"
            Dim TotQr As Double = 0
            For r As Integer = 1 To rowTot
                TotQr = TotQr + ws.Columns(4).Cells(r).Value
            Next
            ws.Columns(4).Cells(rowTot + 1).Value = TotQr
            ws.Columns(4).Cells(rowTot + 1).Style.Font.Weight = ExcelFont.BoldWeight
            ws.Columns(3).Cells(rowTot + 1).Value = "Total Qrs."
            ws.Columns(3).Cells(rowTot + 1).Style.Font.Weight = ExcelFont.BoldWeight
        End If
        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        ''HttpContext.Current.Response.Flush()
        ''HttpContext.Current.Response.Close()
        'HttpContext.Current.Response.End()

        System.IO.File.Delete(tempFileName)

    End Sub
    Protected Sub lbExportBankTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblDate As Label = sender.parent.findcontrol("lblDate")
            If lblDate Is Nothing Then
                Return
            End If
            If lblDate.Text = "" Then
                Return
            End If
            Dim lblDocNo As Label = sender.parent.findcontrol("lblDocNo")
            Dim lblIBAN As Label = sender.parent.findcontrol("lblIBAN") 'swapna added for name format
            If lblDocNo Is Nothing Then
                Return
            End If
            If lblDocNo.Text = "" Then
                Return
            End If
            'swapna added for name format
            If lblIBAN Is Nothing Then
                lblError.Text = "IBAN No. not available for the Account.Please update!"
                Return
            End If
            If lblIBAN.Text = "" Then
                lblError.Text = "IBAN No. not available for the Account.Please update!"
                Return
            End If
            Dim tDate As Date = Convert.ToDateTime(lblDate.Text)
            'Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmd As New SqlCommand("GetDataForSalaryTransferExport")
            cmd.CommandType = CommandType.StoredProcedure
            Dim sqlparam(4) As SqlParameter  'V1.4
            sqlparam(0) = Mainclass.CreateSqlParameter("BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            'cmd.Parameters.Add(sqlparam(0))
            sqlparam(1) = Mainclass.CreateSqlParameter("ESD_MONTH", tDate.Month, SqlDbType.Int)
            'cmd.Parameters.Add(sqlparam(1))
            sqlparam(2) = Mainclass.CreateSqlParameter("ESD_YEAR", tDate.Year, SqlDbType.Int)
            'cmd.Parameters.Add(sqlparam(2))
            'V1.4
            sqlparam(3) = Mainclass.CreateSqlParameter("EBT_REFDOCNO", lblDocNo.Text, SqlDbType.VarChar)
            ' cmd.Parameters.Add(sqlparam(3))

            If ViewState("MainMnu_code") = "P130180" Then
                sqlparam(4) = Mainclass.CreateSqlParameter("IsWPS", False, SqlDbType.Bit)
            Else
                sqlparam(4) = Mainclass.CreateSqlParameter("IsWPS", True, SqlDbType.Bit)
            End If

            'cmd.Parameters.Add(sqlparam(4))
            'V1.4 ends
            'V1.5
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetDataForSalaryTransferExport", sqlparam)
            ExporttoExcel(ds, lblIBAN.Text)
            'Dim repSource As New MyReportClass
            'Dim params As New Hashtable
            'repSource.Command = cmd
            'Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx?isExport=1", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
  
    'Protected Sub lbBankTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

    '        'Dim str_Sql As String = "select ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
    '        'GetFilter("ESD_EMP_ID", h_EMPID.Value, False) & _
    '        '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

    '        Dim strFiltBSUID As String = "WHERE     (ESD_BSU_ID = '" & Session("sBsuid") & "') AND (EBT_DATE = CONVERT(DATETIME, '" & sender.Parent.parent.cells(4).text & "', 102))"
    '        Dim str_Sql As String = "SELECT     * FROM         VW_OSO_SALARYTRANSFERTOBANK " & _
    '        strFiltBSUID
    '        'V1.1 addition
    '        If ViewState("MainMnu_code") = "P130170" Then
    '            str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=0  "
    '        ElseIf ViewState("MainMnu_code") = "P130171" Then
    '            str_Sql = str_Sql & " AND IsNull(ESD_BProcessWPS,0)=1  "
    '        End If
    '        Dim ds As New DataSet
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
    '            Dim cmd As New SqlCommand
    '            cmd.CommandText = str_Sql
    '            cmd.Connection = New SqlConnection(str_conn)
    '            cmd.CommandType = CommandType.Text

    '            Dim repSource As New MyReportClass
    '            Dim params As New Hashtable
    '            params("UserName") = Session("sUsr_name")
    '            repSource.Parameter = params
    '            repSource.Command = cmd
    '            repSource.ResourceName = "../../payroll/Reports/RPT/rptSALARYTRANSFERTOBANK.rpt"
    '            Session("ReportSource") = repSource
    '            Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
    '        Else
    '            lblError.Text = "No Records with specified condition"
    '        End If

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub rcbSignatory_ItemsRequested() 'V1.6 
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "exec dbo.GetLetterSignatory '" & Session("SBsuid") & "','NONWPS','1'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        rcbSignatory.DataSource = ds
        rcbSignatory.DataBind()
        rcbSignatory.SelectedIndex = 0


    End Sub
    Protected Sub rcbSignatory2_ItemsRequested() 'V1.6 
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "exec dbo.GetLetterSignatory '" & Session("SBsuid") & "','NONWPS','2'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        rcbSignatory2.DataSource = ds
        rcbSignatory2.DataBind()
        rcbSignatory2.SelectedIndex = 0


    End Sub
    Protected Sub rcbSignatory_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs) 'V1.6 

        'set the Text and Value property of every item

        'here you can set any other properties like Enabled, ToolTip, Visible, etc.

        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()

        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("EMP_DES_DESCR").ToString() & "," & (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()


    End Sub

    Protected Sub rcbSignatory2_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs) 'V1.6 

        'set the Text and Value property of every item

        'here you can set any other properties like Enabled, ToolTip, Visible, etc.

        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()

        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("EMP_DES_DESCR").ToString() & "," & (DirectCast(e.Item.DataItem, DataRowView))("EmpName").ToString()


    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    
End Class
