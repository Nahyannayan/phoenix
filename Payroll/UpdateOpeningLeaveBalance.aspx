<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="UpdateOpeningLeaveBalance.aspx.vb" Inherits="Students_UpdateOpeningLeaveBalance" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function GetEMPNameSingle() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            //var result;
            //result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_EmpID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpName.ClientID %>').value = NameandCode[0];
                return true;
            }
            return false;--%>

            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_employee");
        }


        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_EmpID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');
            }
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("../../SelIDDESC.aspx?ID=EMP&Bsu_id="+document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)
            //result = window.showModalDialog("../Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)
            //result = window.showModalDialog("Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)
            result = window.showModalDialog("Reports/Aspx/SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)

            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }

        }
        function GetEMPNAME2() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("../../SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)
            result = window.showModalDialog("../Payroll/SelIDDESC.aspx?ID=EMP&Bsu_id=" + document.getElementById('<%=h_BSUID.ClientID %>').value, "", sFeatures)

            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }

        function GetEMPNAME_ALL() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("SelIDDESC.aspx?ID=EMP", "", sFeatures)
            result = window.showModalDialog("Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)

            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <!--    
<script type="text/javascript">
  function validate_Late()
    {
           for(i=0;i<chkLate.length;i++)
            {
           if(document.getElementById(chkLate[i]).checked ==true)
                {  
                   document.getElementById(chkPresent[i]).checked=true;
             }
           }
                  }
    
 function validate_Present()
    {
           for(i=0;i<chkPresent.length;i++)
            {
           if(document.getElementById(chkPresent[i]).checked ==false)
                {  
                   document.getElementById(chkLate[i]).checked=false;
             }
           }
                  }

   </script>

-->

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calendar mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server"
                Text="Update Opening Leave Balance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                </div>
                            </span>
                        </td>
                    </tr>








                    <tr>
                        <td>
                            <table align="center" width="100%">



                                <tr id="trProcessingDate" runat="server" visible="true">
                                    <td align="left" width="20%">
                                        <span class="field-label">Employee</span></td>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                                        <asp:TextBox ID="txtEMPNAME" runat="server" CssClass="inputbox" ReadOnly="True"></asp:TextBox>
                                        <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetEMPNameSingle(); return false;" />
                                    </td>
                                    <td width="30%"></td>
                                </tr>
                                 <tr id="trExcelUpload" runat="server">
                                    <td align="left"><span class="field-label">Bulk Upload</span></td>
                                    <td align="left" >
                                        <asp:FileUpload ID="flUpExcel" runat="server" />
                                        <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
                                        <asp:LinkButton ID="lnkDownload" Text = "Download Sample Template" runat="server"></asp:LinkButton>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr runat="server" visible="false" id="trGridview">
                                    <td align="left" colspan="3">
                                        <div style="width: 100%; height: 400px; overflow-y: scroll">
                                        <asp:GridView ID="gvMonthE_D" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                            Width="100%" SkinID="GridViewNormal" Height="30%" >
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EMP_ID" HeaderText="Emp ID" Visible="False">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMPNO" HeaderText="EmpNO">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMP_Name" HeaderText="Employee Name">
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMP_CarryFrdDays" HeaderText="Carry Forward Days"
                                                    ApplyFormatInEditMode="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="REMARKS" HeaderText="Remarks">
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                            </div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trBulkSave" visible="false">
                                    <td align="center" colspan="3">
                                        <asp:Button
                                            ID="btnBulkSave" runat="server" Text="Update" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button
                                            ID="btnCancelBulk" runat="server" Text="Cancel" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td valign="bottom">&nbsp;</td>
                    </tr>


                    <tr>
                        <td valign="bottom"><b><label style="color:maroon">Note:</label></b><b> You can update carry forward days only for annual leave type.</b></td>
                    </tr>
                    <tr>
                        <td valign="bottom">&nbsp;</td>
                    </tr>
                    <tr id="trGridStudents" runat="server">
                        <td align="center">
                            <asp:GridView ID="gvStudents" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Width="100%"
                                EnableModelValidation="True">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ELS ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblElsId" runat="server" Text='<%# bind("ELS_ID") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeaveType" runat="server"
                                                Text='<%# bind("Elt_Descr") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="25%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("From_Date", "{0:dd/MMM/yyyy}") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblToDate" runat="server"
                                                Text='<%# Bind("To_Date", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Full Pay Days">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullPayDays" runat="server"
                                                Text='<%# bind("Els_FullPayDays") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="14%" HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Carry Forward Days">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCarryFwdDays" runat="server" Style="text-align: right"
                                                Text='<%# Bind("ELS_CARRYFWDDAYS") %>' Width="50%"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ELT ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEltId" runat="server"
                                                Text='<%# bind("ELS_ELT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <%--<alternatingrowstyle cssclass="griditem_alternative" height="25px" />--%>
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr id="trRemarks" runat="server">
                        <td>
                            <table align="center" width="100%">

                                <tr id="tr2" runat="server" visible="false">
                                    <td align="left">
                                        <span class="field-label">Remarks</span></td>
                                    <td align="left" colspan="3">

                                        <asp:TextBox ID="txtRemarks" runat="server"
                                            CssClass="inputbox_multi" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>

                                    </td>
                                </tr>
                            </table>

                        </td>

                    </tr>



                    <tr id="trSave" runat="server">
                        <td align="center">

                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Update"
                                Visible="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" Visible="false" />
                        </td>
                    </tr>




                    <%-- AUDIT--%>

                    <tr id="trHistory" runat="server">
                        <td align="center">
                            <asp:GridView ID="GVEmpLeaveAudit" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Width="100%"
                                EnableModelValidation="True">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Leave Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeaveType" runat="server"
                                                Text='<%# bind("ELS_ELT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="From Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeaveType" runat="server"
                                                Text='<%# Bind("ELS_DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="To Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeaveType" runat="server"
                                                Text='<%# Bind("ELS_DTTO", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="OLD CFD">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOLDCFD" runat="server"
                                                Text='<%#Bind("ELD_OLD_CARRY_FWD_DAYS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="NEW CFD">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeaveType" runat="server" Style="text-align: right"
                                                Text='<%#Bind("ELD_NEW_CARRY_FWD_DAYS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="LOG USER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullPayDays" runat="server" Text='<%#Bind("ELD_LOGUSER") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="12%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="LOG DATE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEltId" runat="server"
                                                Text='<%#Bind("ELD_LOGDATE", "{0:dd/MMM/yyyy H:mm:ss }") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="REMARKS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullPayDays" runat="server" Text='<%# bind("ELD_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="24%" />
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <%--<alternatingrowstyle cssclass="griditem_alternative"  />--%>
                            </asp:GridView>
                        </td>
                    </tr>


                    <tr>
                        <td style="height: 1px" valign="bottom"></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfDay1" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay2" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay3" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay4" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay5" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfMode" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

