<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empWPSExportView.aspx.vb" Inherits="Payroll_empWPSExportView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
            }
            else if (pId == 4) {
                document.getElementById("<%=getid("mnu_4_img") %>").src = path;
                }
                else if (pId == 5) {
                    document.getElementById("<%=getid("mnu_5_img") %>").src = path;
               }
               else if (pId == 6) {
                   document.getElementById("<%=getid("mnu_6_img") %>").src = path;
               }
               else if (pId == 7) {
                   document.getElementById("<%=getid("mnu_7_img") %>").src = path;
             }

    document.getElementById(ctrl1).value = val + '__' + path;
}

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="WPS Export Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left" style="height: 19px">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" style="height: 19px">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>

                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top" width="20%">
                            <asp:Label ID="lblSignHead1" runat="server" Text="Signatory 1" CssClass="field-label"></asp:Label>
                        </td>
                        <td width="30%">
                            <telerik:RadComboBox runat="server" ID="rcbSignatory" RenderMode="Lightweight" Width="100%"
                                EnableLoadOnDemand="true"
                                MarkFirstMatch="true" OnItemDataBound="rcbSignatory_ItemDataBound"
                                HighlightTemplatedItems="true">

                                <HeaderTemplate>

                                    <ul>
                                        <li class="col1">Name</li>
                                        <li class="col2">Designation</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>
                                        <li class="col1">
                                            <%#DataBinder.Eval(Container.DataItem, "EMPNAME")%></li>
                                        <li class="col2">
                                            <%#DataBinder.Eval(Container.DataItem, "EMP_DES_DESCR")%></li>
                                    </ul>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%">
                            <asp:Label ID="lblHeading2" runat="server" Text="Signatory 2" CssClass="field-label"></asp:Label>
                        </td>
                        <td width="30%">
                            <telerik:RadComboBox runat="server" ID="rcbSignatory2" RenderMode="Lightweight" Width="100%"
                                EnableLoadOnDemand="true"
                                MarkFirstMatch="true" OnItemDataBound="rcbSignatory2_ItemDataBound"
                                HighlightTemplatedItems="true">
                                <HeaderTemplate>
                                    <ul>
                                        <li class="col1">Name</li>
                                        <li class="col2">Designation</li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul>
                                        <li class="col1">
                                            <%#DataBinder.Eval(Container.DataItem, "EMPNAME")%></li>
                                        <li class="col2">
                                            <%#DataBinder.Eval(Container.DataItem, "EMP_DES_DESCR")%></li>
                                    </ul>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>

                    </tr>
                    <tr>

                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

                <table width="100%">

                    <tr id="Trbank">

                        <td>
                            <asp:GridView ID="gvSalary" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Emp No" SortExpression="EMPNO">
                                        <HeaderTemplate>
                                            Visa Unit<br />
                                            <asp:TextBox ID="txtVisaUnit" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVisaBsu" runat="server" Text='<%# Bind("ESW_VISA_BSU_DESCR") %>'></asp:Label>
                                            <asp:Label ID="lblVUnit" runat="server" Text='<%# Bind("ESW_VISA_BSU_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Month">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblFromdate" runat="server" Text="Month/Year"></asp:Label><br />
                                            <asp:TextBox ID="txtMonyr" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMonthYear" runat="server" Text='<%# Bind("MONTHYEAR") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exported Date" SortExpression="ESW_DATE">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ESW_DATE") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblExpDate" runat="server" Text='<%# Bind("ESW_DATE") %>'></asp:Label>
                                            <asp:Label ID="lblEWH_ID" runat="server" Text='<%# Bind("EWH_ID") %>' Visible="false"></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Amount" SortExpression="Amount">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Account_Descr" HeaderText="Account" SortExpression="Account_Descr">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">Print</asp:LinkButton>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sif File">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDoc" runat="server" AlternateText=' <%#Eval("HasSifFile")%> ' ToolTip="Click To View Document" CommandName="View" CommandArgument=' <%#Eval("EWH_ID")%> ' ImageUrl="../Images/ViewDoc.png" Visible="false" />


                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transfer &lt;br/&gt;Salary">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbTransfer" OnClick="lbTransfer_Click" runat="server" CommandArgument=' <%#Eval("ESW_DOCNO")%> '>Transfer</asp:LinkButton>
                                            <asp:LinkButton ID="lbVoucher" runat="server" OnClick="lbVoucher_Click" Enabled="false"
                                                Text='<%# Bind("ESW_DOCNO") %>'></asp:LinkButton>
                                            <asp:TextBox ID="txtEWH_ID" runat="server" Text='<%# Bind("EWH_ID") %>' Visible="false"></asp:TextBox>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />       
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remove &lt;br/&gt;WPS">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Remove</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EWH_ID" HeaderText="ID" Visible="false"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDetails" runat="server" OnClick="lnkDetails_Click">View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

