Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports PayrollFunctions
Partial Class Payroll_empEmpCategortnDpt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtDpt.Attributes.Add("readonly", "readonly")
            txtCat.Attributes.Add("readonly", "readonly")
            txtGrade.Attributes.Add("readonly", "readonly")
            txtNGrade.Attributes.Add("readonly", "readonly")
            txtNDpt.Attributes.Add("readonly", "readonly")
            txtNCat.Attributes.Add("readonly", "readonly")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P450020" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") <> "" Then

                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If


        End If
    End Sub




    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT EAT.EAT_ID, " _
            & " EAT.EAT_BSU_ID, EAT.EAT_EMP_ID, " _
            & " EAT.EAT_ELT_ID, EAT.EAT_DT, EAT.EAT_FLAG, " _
            & " EAT.EAT_ELV_ID, EAT.EAT_REMARKS," _
            & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+ " _
            & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
            & " FROM EMPATTENDANCE AS EAT INNER JOIN" _
            & " EMPLOYEE_M AS EM ON EAT.EAT_EMP_ID = EM.EMP_ID" _
            & " WHERE EAT.EAT_ID='" & p_Modifyid & "'"
            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'ddLeavetypeMonthly.DataBind()
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EAT_EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                'ddLeavetypeMonthly.SelectedIndex = -1
                'ddLeavetypeMonthly.Items.FindByValue(ds.Tables(0).Rows(0)("EAT_ELT_ID").ToString).Selected = True
                'txtCategory.Text = Format(CDate(ds.Tables(0).Rows(0)("EAT_DT")), "dd/MMM/yyyy")

                txtRemarks.Text = ds.Tables(0).Rows(0)("EAT_REMARKS").ToString
            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub




    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtDpt.Attributes.Add("readonly", "readonly")
        txtCat.Attributes.Add("readonly", "readonly")
        txtGrade.Attributes.Add("readonly", "readonly")
        txtNDpt.Attributes.Add("readonly", "readonly")
        txtNCat.Attributes.Add("readonly", "readonly")
        txtABC.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
        imgEmployee.Enabled = False
    End Sub




    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
    End Sub





    Sub clear_All()
        txtFrom.Text = ""
        txtRemarks.Text = ""
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        h_Cat.Value = ""
        h_Dpt.Value = ""
        txtDpt.Text = ""
        txtCat.Text = ""
        txtGrade.Text = ""
        txtNGrade.Text = ""
        txtNDpt.Text = ""
        txtNCat.Text = ""
        ddABC.SelectedIndex = 0
        bindgrid()
    End Sub





    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsDate(txtFrom.Text) = False Then
            lblError.Text = "Invalid Date"
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            'ViewState("EDVISA_ID") 
            'ViewState("EDMOE_ID")  
            '    ViewState("EDEMP_ID") 
            Dim retval As String = "1000"
            Dim str_update_master As String = ""

            If txtNDpt.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "9", h_Dpt.Value, _
                          txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("DPT_ID"), False, stTrans)
                str_update_master = " EMP_DPT_ID='" & h_Dpt.Value & "' "
            End If

            If txtNGrade.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "2", h_Grade.Value, _
                  txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EGD_ID"), False, stTrans)
                If str_update_master = "" Then
                    str_update_master = " EMP_EGD_ID='" & h_Grade.Value & "' "
                Else
                    str_update_master = str_update_master & " ,EMP_EGD_ID='" & h_Grade.Value & "' "
                End If
            End If
            If txtNCat.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "3", h_Cat.Value, _
                  txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("ECT_ID"), False, stTrans)
                If str_update_master = "" Then
                    str_update_master = " EMP_ECT_ID='" & h_Cat.Value & "' "
                Else
                    str_update_master = str_update_master & ", EMP_ECT_ID='" & h_Cat.Value & "' "
                End If
            End If
            ''''''
            If ddABC.SelectedItem.Value <> "N" And ddABC.SelectedItem.Value <> txtABC.Text.Trim Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "18", ddABC.SelectedItem.Value, _
                  txtFrom.Text, Nothing, txtRemarks.Text.ToString, txtABC.Text.Trim, False, stTrans)
                If str_update_master = "" Then
                    str_update_master = " EMP_ABC='" & ddABC.SelectedItem.Value & "' "
                Else
                    str_update_master = str_update_master & ", EMP_ABC='" & ddABC.SelectedItem.Value & "' "
                End If
            End If

            ''''''''
            If retval = "0" Then
                Dim myCommand As New SqlCommand("UPDATE  EMPLOYEE_M SET " & str_update_master & "WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.ExecuteNonQuery()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                h_Emp_No.Value & "-" & h_Grade.Value & "-" & txtFrom.Text & "-" & _
                txtRemarks.Text.ToString & "-" & ViewState("EGD_ID"), _
                     "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If

            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


 


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub




    Sub setEditdata()
        'txtCategory.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")

        'imgCalendar.Enabled = True

        imgEmployee.Enabled = True
    End Sub




    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub




    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click 
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub




    Protected Sub imgEmployee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String


            str_Sql = "SELECT EM.EMP_ID, " _
            & " EM.EMP_ECT_ID, ECT.ECT_DESCR, EM.EMP_DPT_ID, " _
            & " DPT.DPT_DESCR, EM.EMP_EGD_ID, EMPGRADES_M.EGD_DESCR,EM.EMP_ABC" _
            & " FROM EMPLOYEE_M AS EM LEFT OUTER JOIN" _
            & " EMPCATEGORY_M AS ECT ON EM.EMP_ECT_ID = ECT.ECT_ID LEFT OUTER join" _
            & " DEPARTMENT_M AS DPT ON EM.EMP_DPT_ID = DPT.DPT_ID LEFT OUTER join" _
            & " EMPGRADES_M ON EM.EMP_EGD_ID = EMPGRADES_M.EGD_ID" _
            & " WHERE (EM.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EM.EMP_ID = '" & h_Emp_No.Value & "')"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDpt.Text = ds.Tables(0).Rows(0)("DPT_DESCR").ToString
                txtCat.Text = ds.Tables(0).Rows(0)("ECT_DESCR").ToString
                txtGrade.Text = ds.Tables(0).Rows(0)("EGD_DESCR").ToString
                txtABC.Text = ds.Tables(0).Rows(0)("EMP_ABC").ToString

                ViewState("DPT_ID") = ds.Tables(0).Rows(0)("EMP_DPT_ID").ToString
                ViewState("EGD_ID") = ds.Tables(0).Rows(0)("EMP_EGD_ID").ToString
                ViewState("ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID").ToString
            Else

            End If
            bindgrid()
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub




    Sub bindgrid() 
        Dim ds As New DataSet
        Dim str_Sql As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        str_Sql = "GetDepCatGrade  '" & h_Emp_No.Value & "','" & Session("sBsuid") & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDetails.DataSource = ds
        gvDetails.DataBind() 
    End Sub




    'Private Function SaveEMPTRANTYPE_TRN(ByVal p_bsu_id As String, _
    'ByVal p_EMP_ID As String, ByVal p_EST_TTP_ID As String, ByVal p_EST_CODE As String, _
    ' ByVal p_EST_DTFROM As String, ByVal p_EST_REMARKS As String, _
    '  ByVal p_EST_OLDCODE As String, ByVal p_bFromMAsterForm As Boolean, ByVal p_stTrans As SqlTransaction) As String
    '    '@return_value = [dbo].[SaveEMPTRANTYPE_TRN]
    '    '@EST_EMP_ID = 999,
    '    '@EST_BSU_ID = N'XXXXXX',
    '    '@EST_TTP_ID = 1,

    '    '@EST_CODE = N'1',
    '    '@EST_DTFROM = N'12-DEC-2007',
    '    '@EST_DTTO = NULL,
    '    '@EST_REMARKS = N'VERUTHE',
    '    '@EST_OLDCODE = N'2',
    '    '@bFromMAsterForm = 0

    '    Try
    '        Dim pParms(9) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@EST_BSU_ID", SqlDbType.VarChar, 20)
    '        pParms(0).Value = p_bsu_id
    '        pParms(1) = New SqlClient.SqlParameter("@EST_EMP_ID", SqlDbType.Int)
    '        pParms(1).Value = p_EMP_ID
    '        pParms(2) = New SqlClient.SqlParameter("@EST_TTP_ID", SqlDbType.Int)
    '        pParms(2).Value = p_EST_TTP_ID
    '        pParms(3) = New SqlClient.SqlParameter("@EST_CODE", SqlDbType.VarChar, 20)
    '        pParms(3).Value = p_EST_CODE
    '        pParms(4) = New SqlClient.SqlParameter("@EST_DTFROM", SqlDbType.DateTime)
    '        pParms(4).Value = p_EST_DTFROM
    '        pParms(5) = New SqlClient.SqlParameter("@EST_DTTO", SqlDbType.DateTime)
    '        pParms(5).Value = System.DBNull.Value
    '        pParms(6) = New SqlClient.SqlParameter("@EST_REMARKS", SqlDbType.VarChar, 100)
    '        pParms(6).Value = p_EST_REMARKS
    '        pParms(7) = New SqlClient.SqlParameter("@EST_OLDCODE", SqlDbType.VarChar, 20)
    '        pParms(7).Value = p_EST_OLDCODE
    '        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
    '        pParms(8).Direction = ParameterDirection.ReturnValue
    '        pParms(9) = New SqlClient.SqlParameter("@bFromMAsterForm", SqlDbType.Bit)
    '        pParms(9).Value = p_bFromMAsterForm
    '        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '        Dim retval As Integer
    '        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPTRANTYPE_TRN", pParms)
    '        If pParms(8).Value = "0" Then
    '            SaveEMPTRANTYPE_TRN = pParms(8).Value
    '        Else
    '            SaveEMPTRANTYPE_TRN = pParms(8).Value
    '        End If
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '        SaveEMPTRANTYPE_TRN = "1000"
    '    End Try

    'End Function



End Class
