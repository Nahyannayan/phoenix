Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Globalization

'Version        Date             Author          Change
'1.1            20-Feb-2011      Swapna          To filter view screen based on logged in user's id
Partial Class Payroll_empLeaveApplicationListCompare
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    If Session("sModule") = "SS" Then
    '        Me.MasterPageFile = "../mainMasterPageSS.master"
    '    Else
    '        Me.MasterPageFile = "../mainMasterPage.master"
    '    End If

    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
            If Not Session("Plan_Chart") Is Nothing Then
                'RadPivotGrid1.Width = hf_width.Value
                'RadPivotGrid1.Height = hf_height.Value
                RadHtmlChart1.DataSource = Session("Plan_Chart")
                RadHtmlChart1.DataBind()
                RadPivotGrid1.DataSource = Session("Plan_Chart")
                RadPivotGrid1.DataBind()
                lt_Lengend.Text = "<span style='background:#FFFF00;'>&nbsp;&nbsp;P&nbsp;&nbsp;</span> - PENDING FOR APPROVAL  &nbsp;&nbsp;&nbsp;<span style='background:#13d09d;'>&nbsp;&nbsp;A&nbsp;&nbsp;</span> - APPROVED &nbsp;&nbsp;&nbsp;<span style='background:#de7465;'>&nbsp;&nbsp;R&nbsp;&nbsp;</span> - REJECTED "
            End If
        End If
    End Sub
    Private columnNames As New HashSet(Of String)()
    Private columnSeriesByRowName As New Dictionary(Of String, ColumnSeries)()
    Private Function GetName(indexes As Object()) As String
        Dim builder As New StringBuilder()
        For Each index As Object In indexes
            builder.Append(index.ToString())
            builder.Append(" / ")
        Next
        builder.Remove(builder.Length - 3, 3)
        Return builder.ToString()
    End Function
    Protected Sub RadPivotGrid1_CellDataBound(sender As Object, e As PivotGridCellDataBoundEventArgs)
        If TypeOf e.Cell Is PivotGridColumnHeaderCell Then
            Dim month As Integer = 0
            Dim cell As PivotGridColumnHeaderCell = TryCast(e.Cell, PivotGridColumnHeaderCell)

            If cell.Text.IndexOf("-") >= 0 Then
                Dim year As String = cell.Text.Split("-")(0)
                Dim mon As String = cell.Text.Split("-")(1)
                Dim day As String = cell.Text.Split("-")(2)
                Dim dayofweeks As Date = mon + "/" + day + "/" + year
                If Integer.TryParse(day, month) Then
                    cell.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Integer.Parse(mon)) + " " + day + "  (" + dayofweeks.ToString("ddd") + ")"
                End If
            End If
        End If

        Dim dataCell As PivotGridDataCell = TryCast(e.Cell, PivotGridDataCell)
        If columnNames.Count = 0 Then
            Me.RadHtmlChart1.PlotArea.XAxis.Items.Clear()
            Me.RadHtmlChart1.PlotArea.Series.Clear()
        End If
        If dataCell IsNot Nothing AndAlso dataCell.CellType = PivotGridDataCellType.DataCell Then
            Dim rowName As String = GetName(dataCell.ParentRowIndexes)
            Dim columnName As String = GetName(dataCell.ParentColumnIndexes)
            'columnName = columnName.Replace("'"c, " "c)
            rowName = rowName.Split("/")(0)
            If columnNames.Add(columnName) Then
                Dim axisItem As New AxisItem(columnName)
                Me.RadHtmlChart1.PlotArea.XAxis.Items.Add(axisItem)
            End If
            Dim columnSeries As ColumnSeries = Nothing
            If columnSeriesByRowName.ContainsKey(rowName) Then
                columnSeries = columnSeriesByRowName(rowName)
            Else
                columnSeries = New ColumnSeries()
                columnSeriesByRowName.Add(rowName, columnSeries)
                columnSeries.Name = rowName
                'columnSeries.LabelsAppearance.DataFormatString = "N2"
                columnSeries.LabelsAppearance.Visible = False

                columnSeries.TooltipsAppearance.ClientTemplate = rowName '+ "</br>"
                Me.RadHtmlChart1.PlotArea.Series.Add(columnSeries)

            End If
            Dim item As New CategorySeriesItem()
            Dim value As Decimal = 0
            If e.Cell.DataItem IsNot Nothing AndAlso Decimal.TryParse(e.Cell.DataItem.ToString(), value) Then
                If (e.Cell.DataItem > 0) Then
                    item.Y = 1
                Else
                    item.Y = 0
                End If
            Else
                item.Y = Nothing
            End If
            columnSeries.SeriesItems.Add(item)
        End If


        'REPLACING THE ACTUAL LEAVES 
        If TypeOf e.Cell Is PivotGridDataCell Then

            If dataCell IsNot Nothing AndAlso DirectCast(dataCell.Field, PivotGridAggregateField).DataField = "Y" Then
                Dim LeaveType As String = dataCell.Text
                If LeaveType = "1" Then
                    dataCell.Text = "A"
                    dataCell.BackColor = System.Drawing.Color.FromArgb(19, 208, 157)
                ElseIf LeaveType = "2" Then
                    dataCell.Text = "P"
                    dataCell.BackColor = System.Drawing.Color.FromArgb(255, 255, 0)
                ElseIf LeaveType = "3" Then
                    dataCell.Text = "R"
                    dataCell.BackColor = System.Drawing.Color.FromArgb(222, 116, 101)
                Else
                    dataCell.Text = " "
                End If

            End If
        End If


    End Sub
    Protected Sub RadPivotGrid1_NeedDataSource(sender As Object, e As Telerik.Web.UI.PivotGridNeedDataSourceEventArgs) Handles RadPivotGrid1.NeedDataSource
        RadPivotGrid1.DataSource = Session("Plan_Chart")
    End Sub
End Class
