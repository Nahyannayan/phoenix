<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empSalaryTransfer.aspx.vb" Inherits="Payroll_empSalaryTransfer" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script>
        function ShowBankDetail(id) {
            Popup("ShowBankDetail.aspx?ela_id=" + id + "&WPS=1")

        }

        function get_Bank() {
            var NameandCode;
            var result;
            var url = '../accounts/PopUp.aspx?ShowType=BANK&codeorname=' + document.getElementById('<%=txtBankCode.ClientID %>').value;
            var oWnd = radopen(url, "pop_bank");

        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtChqBook.ClientId %>').value = '';
                document.getElementById('<%=hCheqBook.ClientId %>').value = '';
                document.getElementById('<%=txtChqNo.ClientId %>').value = ''
            }
        }
        function get_Cheque() {

            var NameandCode;
            var result;
            var url = "..\/accounts\/ShowChqs.aspx?ShowType=CHQBOOK_PDC&BankCode=" + document.getElementById('<%= txtBankCode.ClientId %>').value + "&docno=0";

            if (document.getElementById('<%= txtBankCode.ClientId %>').value == "") {
                alert("Please Select The Bank");
                return false;
            }
            var oWnd = radopen(url, "pop_cheq");

        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hCheqBook.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtChqBook.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtChqNo.ClientID %>').value = NameandCode[1];
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>


    <script language="javascript" type="text/javascript">
        //function ShowBankDetail(id) {
        //    var sFeatures;
        //    sFeatures = "dialogWidth: 559px; ";
        //    sFeatures += "dialogHeight: 405px; ";
        //    sFeatures += "help: no; ";
        //    sFeatures += "resizable: no; ";
        //    sFeatures += "scroll: yes; ";
        //    sFeatures += "status: no; ";
        //    sFeatures += "unadorned: no; ";
        //    var NameandCode;
        //    var result;
        //    result = window.showModalDialog("ShowBankDetail.aspx?ela_id=" + id + "&WPS=1", "", sFeatures)

        //    return false;
        //}


        <%--function get_Bank() {
            var sFeatures;
            sFeatures = "dialogWidth: 509px; ";
            sFeatures += "dialogHeight: 434px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            result = window.showModalDialog("..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
      document.getElementById('<%= txtChqBook.ClientId %>').value = '';
      document.getElementById('<%= hCheqBook.ClientId %>').value = '';
      return false;
  }--%>

  <%--function get_Cheque() {
      var sFeatures;
      sFeatures = "dialogWidth: 509px; ";
      sFeatures += "dialogHeight: 434px; ";
      sFeatures += "help: no; ";
      sFeatures += "resizable: no; ";
      sFeatures += "scroll: yes; ";
      sFeatures += "status: no; ";
      sFeatures += "unadorned: no; ";
      var NameandCode;
      var result;

      if (document.getElementById('<%= txtBankCode.ClientId %>').value == "") {
                alert("Please Select The Bank");
                return false;
            }
        /////
            result = window.showModalDialog("..\/accounts\/ShowChqsPDC.aspx?ShowType=PDCCHQBOOK&BankCode=" + document.getElementById('<%= txtBankCode.ClientId %>').value + "", "", sFeatures);
        if (result == '' || result == undefined)
        { return false; }
        lstrVal = result.split('||');

        document.getElementById('<%= txtChqBook.ClientId %>').value = lstrVal[0];
                     document.getElementById('<%= hCheqBook.ClientId %>').value = lstrVal[1];
    }--%>
        function UpdateSum() {
            var sum = 0.0;
            var dsum = 0.0;
            var chk = 0;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0)
                        if (document.forms[0].elements[i].checked == true)
                        { chk = 1; }
                }
                if (document.forms[0].elements[i].name.search(/txtAmount/) > 0) {
                    if (document.forms[0].elements[i].name != '<%=txtAmount.ClientID %>' && chk == 1) {
                    chk = 0;
                    sum += parseFloat(document.forms[0].elements[i].value);
                }
            }
        }

        document.getElementById('<%=txtAmount.ClientID %>').value = sum;
    }

    function change_chk_state_(chkchecked) {
        var chk_state = chkchecked.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0 || document.forms[0].elements[i].name.search(/chkSelall/) > 0)
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
        }
        UpdateSum();
    }

    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cheq" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <asp:Label ID="lblHeading" runat="server" Text="Salary Transfer (WPS)"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">

                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Document Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtDocdate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Bank A/C</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank();return false;" Visible="False" />

                        </td>
                        <td align="left" class="matters"><span class="field-label">Bank Name</span></td>
                        <td>
                            <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Select Month Year</span></td>
                        <td align="left" class="matters">
                            <asp:CheckBoxList ID="clMonthYear" runat="server" AutoPostBack="True" RepeatColumns="4" RepeatLayout="Flow" RepeatDirection="Horizontal">
                            </asp:CheckBoxList></td>
                        <td align="left" class="matters"><span class="field-label">Narration</span></td>
                        <td align="left" class="matters" colspan="1">
                            <asp:TextBox ID="txtNarrn" runat="server" CssClass="inputbox_multi" MaxLength="300"
                                SkinID="MultiText" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Cheque Date</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtChequedate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trChkLot" runat="server" visible="false">
                        <td align="left" class="matters"><span class="field-label">Cheque Lot</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtChqBook" runat="server"></asp:TextBox>
                            <a href="#" onclick="get_Cheque();return false;">
                                <img id="IMG1" border="0" language="javascript" src="../Images/cal.gif" /></a>
                            <asp:TextBox ID="txtChqNo" runat="server" Visible="False"></asp:TextBox></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Instrument Reference No.</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtRefNo" runat="server"></asp:TextBox>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trMaster" runat="server" visible="false">
                        <td align="left" valign="top" class="matters"><span class="field-label">Salary Transfer Details</span></td>
                        <td align="left" class="matters" valign="top" colspan="3">
                            <asp:GridView ID="gvMaster" CssClass="table table-row table-bordered" Width="100%" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Visa Unit"></asp:BoundField>
                                    <asp:BoundField DataField="EWH_AMOUNT" HeaderText="Amount"></asp:BoundField>
                                    <asp:BoundField DataField="ACT_BANKACCNO" HeaderText="Account No"></asp:BoundField>
                                    <asp:BoundField DataField="ACt_NAME" HeaderText="Account Name"></asp:BoundField>
                                    <asp:BoundField DataField="EWH_ID" HeaderText="ID" Visible="False"></asp:BoundField>
                                </Columns>

                            </asp:GridView>

                        </td>
                    </tr>
                    <tr id="Trbank">
                        <td align="left" valign="top" class="matters"><span class="field-label">Bankwise Transfer Details</span></td>
                        <td align="left" class="matters" valign="top" colspan="2">
                            <asp:Panel ID="pnlBanks" runat="server" Enabled="True">

                                <asp:GridView ID="gvDetails" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-row table-bordered">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>

                                                <input id="chkSelall" type="checkbox" onclick="javascript: change_chk_state_(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="chkESD_BANK" runat="server" value='<%# Bind("ESD_BANK") %>' type="checkbox" onclick="UpdateSum();" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS">
                                            <ItemTemplate>
                                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="<%# &quot;ShowBankDetail('&quot; & Container.DataItem(&quot;ESD_BANK&quot;) & &quot;');return false;&quot; %>"
                                                    Text='<%# Bind("BNK_DESCRIPTION") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAmount" runat="server" OnPreRender="txtAmount_PreRender" Text='<%# Bind("amount") %>'></asp:TextBox>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                        <td></td>
                    </tr>

                    <tr id="trTotal" visible="false" runat="server">
                        <td align="left" class="matters"><span class="field-label">Total Amount Selected</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" class="matters"><span class="field-label">Value Date</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtValueDate" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server"
                                CssClass="MyCalendar" PopupPosition="TopRight"
                                Format="dd/MMM/yyyy" PopupButtonID="imgValueDate" TargetControlID="txtValueDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="txtValueDate_CalendarExtender2" runat="server"
                                CssClass="MyCalendar" PopupPosition="TopRight"
                                Format="dd/MMM/yyyy" PopupButtonID="txtValueDate" TargetControlID="txtValueDate">
                            </ajaxToolkit:CalendarExtender>

                            <asp:ImageButton ID="imgValueDate" runat="server"
                                ImageUrl="~/Images/calendar.gif" />
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" Visible="false" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Transfer Salary" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Font-Bold="True" Text="Print" />
                            <asp:Button ID="btnWPS" runat="server" OnClick="btnWPS_Click" Text="Print WPS"
                                Visible="False" CssClass="button" /></td>

                    </tr>
                </table>
                <asp:HiddenField ID="hCheqBook" runat="server" />

                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtDocdate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtDocdate" TargetControlID="txtDocdate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtChequedate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtChequedate" TargetControlID="txtChequedate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>
