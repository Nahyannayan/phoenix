Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Payroll_PopupSalaryCompareDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'ts
    Private Sub GridBindCurrentSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("EMPSALDETAILS") Is Nothing Then
            gvEmpSalary.DataSource = Nothing
            gvEmpSalary.DataBind()
            Return
        End If
        Dim EMP_ID As String = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
        If EMP_ID = "" Or EMP_ID = String.Empty Then Return

        Dim i As Integer
        Dim amt As Double
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        Dim ldrTempNew As DataRow
        Dim dtTempGridTab As DataTable = CreateSalaryRevDetailsTable()
        Dim grossSal, grossSalNew As Double
        dtTempDtl = Session("EMPSALDETAILS")
        Dim dRows() As DataRow
        dRows = dtTempDtl.Select("EMP_ID = '" & EMP_ID & "'", "ERN_ORDER")
        If dRows.Length > 0 Then
            For i = 0 To dRows.Length - 1
                ldrTempNew = dtTempGridTab.NewRow()
                For iColCount As Integer = 0 To dtTempDtl.Columns.Count - 1
                    strColumnName = dtTempDtl.Columns(iColCount).ColumnName
                    Select Case strColumnName
                        Case "Amount"
                            amt = GetVal(dRows(i)(strColumnName), SqlDbType.Decimal)
                            If Not dRows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            ldrTempNew(strColumnName) = amt
                            grossSal += amt
                        Case "Amount_NEW"
                            amt = GetVal(dRows(i)(strColumnName), SqlDbType.Decimal)
                            If Not dRows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSalNew += amt
                            ldrTempNew(strColumnName) = amt
                        Case "Amount_ELIGIBILITY_NEW"
                            amt = GetVal(dRows(i)(strColumnName), SqlDbType.Decimal)
                            If Not dRows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            ldrTempNew(strColumnName) = amt
                        Case "Amount_ELIGIBILITY"
                            amt = GetVal(dRows(i)(strColumnName), SqlDbType.Decimal)
                            If Not dRows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            ldrTempNew(strColumnName) = amt
                        Case Else
                            ldrTempNew(strColumnName) = dRows(i)(strColumnName)
                    End Select
                Next
                dtTempGridTab.Rows.Add(ldrTempNew)
            Next
        End If
        txtGrossSalCur.Text = grossSal
        txtGrossSalRev.Text = grossSalNew
        gvEmpSalary.DataSource = dtTempGridTab
        gvEmpSalary.DataBind()
    End Sub

    Private Sub GridBindCurrentSalaryDetails_Upload()
        Try
            Dim EMP_ID As String = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If EMP_ID = "" Or EMP_ID = String.Empty Then Exit Sub
            Dim query As String = "SELECT sc.ERN_DESCR AS Earning, esh.esh_amount AS Amount FROM dbo.EMPSALARYhs_S ESH INNER JOIN  dbo.EMPSALCOMPO_M SC ON SC.ERN_ID = esh.ESH_ERN_ID WHERE esh.ESH_EMP_ID = " & EMP_ID & " AND esh.ESH_TODT is null and esh.esh_bsu_id='" & Session("sBSUID") & "'  ORDER BY sc.ERN_SORTORDER"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, query)
            If Not ds Is Nothing Then
                Me.gvEmpSalaryUpload.DataSource = ds
                Me.gvEmpSalaryUpload.DataBind()
                Me.gvEmpSalary.Visible = False
                Me.gvEmpSalaryUpload.Visible = True
                Me.trGross.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function GetVal(ByVal obj As Object, Optional ByVal datatype As SqlDbType = SqlDbType.VarChar) As Object
        If obj Is DBNull.Value Then
            Select Case datatype
                Case SqlDbType.Decimal
                    Return 0
                Case SqlDbType.Int
                    Return 0
                Case SqlDbType.Bit
                    Return False
                Case SqlDbType.VarChar
                    Return ""
            End Select
        Else
            Return obj
        End If
        Return ""
    End Function

    Function CreateSalaryRevDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cEMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim cERN_ORDER As New DataColumn("ERN_ORDER", System.Type.GetType("System.Int32"))
            Dim cENR_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))

            Dim cFROMDATE As New DataColumn("FROM_DATE", System.Type.GetType("System.DateTime"))
            Dim cPAY_CUR_ID As New DataColumn("PAY_CUR_ID", System.Type.GetType("System.String"))

            Dim cPay_Year As New DataColumn("PAY_YEAR", System.Type.GetType("System.Int16"))
            Dim cPay_Month As New DataColumn("PAY_MONTH", System.Type.GetType("System.Int32"))

            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cPAYTERM As New DataColumn("PAYTERM", System.Type.GetType("System.Int32"))
            Dim cbPAID As New DataColumn("bPAID", System.Type.GetType("System.Boolean"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY As New DataColumn("Amount_ELIGIBILITY", System.Type.GetType("System.Decimal"))
            Dim cAmount_NEW As New DataColumn("Amount_NEW", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY_NEW As New DataColumn("Amount_ELIGIBILITY_NEW", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEMP_ID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cERN_ORDER)
            dtDt.Columns.Add(cENR_DESCR)
            dtDt.Columns.Add(cFROMDATE)
            dtDt.Columns.Add(cPay_Month)
            dtDt.Columns.Add(cPay_Year)

            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cPAYTERM)
            dtDt.Columns.Add(cbPAID)

            dtDt.Columns.Add(cPAY_CUR_ID)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cAmount_ELIGIBILITY)
            dtDt.Columns.Add(cAmount_NEW)
            dtDt.Columns.Add(cAmount_ELIGIBILITY_NEW)
            dtDt.Columns.Add(cStatus)

            Dim dc(1) As DataColumn
            dc(0) = dtDt.Columns("EMP_ID")
            dc(1) = dtDt.Columns("ENR_ID")
            dtDt.PrimaryKey = dc

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Session("EMPSALDETAILS") Is Nothing Then
                GridBindCurrentSalaryDetails()
            Else
                'user is trying to view the current salary details while uploading new details through excel upload option.
                'show current salary details
                GridBindCurrentSalaryDetails_Upload()
            End If
        End If
    End Sub

    Protected Sub gvEmpSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpSalary.RowDataBound
        Try
            Dim lblSchedule As New Label
            Dim lblPayTerm As New Label
            lblSchedule = e.Row.FindControl("lblSchedule")
            lblPayTerm = e.Row.FindControl("lblPayTerm")
            If lblSchedule IsNot Nothing Then
                Select Case lblPayTerm.Text
                    Case 0
                        lblSchedule.Text = "Monthly"
                    Case 1
                        lblSchedule.Text = "Bimonthly"
                    Case 2
                        lblSchedule.Text = "Quarterly"
                    Case 3
                        lblSchedule.Text = "Half-yearly"
                    Case 4
                        lblSchedule.Text = "Yearly"
                End Select
            End If
        Catch
        End Try
    End Sub


End Class
