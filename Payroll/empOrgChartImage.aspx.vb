﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Drawing.Imaging
Partial Class Payroll_empOrgChartImage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' Change the response headers to output a JPEG image.
        Try
            Me.Response.Clear()
            Me.Response.ContentType = "image/jpeg"
            Dim OC As System.Drawing.Image = Nothing
            'Build the image 
            Dim imageName As String = Request.QueryString("ID")
            If Cache(imageName) IsNot Nothing Then
                OC = DirectCast(Cache(imageName), System.Drawing.Image)
                ' Write the image to the response stream in JPEG format.
                OC.Save(Me.Response.OutputStream, ImageFormat.Jpeg)
                OC.Dispose()
            End If
        Catch ex As Exception
            'Throw ex
        End Try

    End Sub

End Class
