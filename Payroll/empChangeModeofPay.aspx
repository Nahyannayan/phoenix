<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empChangeModeofPay.aspx.vb" Inherits="Payroll_empChangeModeofPay" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <script language="javascript" type="text/javascript">

        function ValidateDate() {
            //debugger;
            var varDate = document.getElementById("<%=txtFrom.ClientID %>");

            var Days = 30;
            if (varDate.value.length < 1) {
                return false;
            }
            else {
                var DateSplit = varDate.value.split('/');
                if (DateSplit[0] > 1) {
                    alert("Enter first date of the month");
                }
            }
        }
   <%--     function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = 'empShowMasterEmp.aspx?id=' + mode;

            if (mode == 'BK') {
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtBname.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfBank_ID.ClientID %>").value = NameandCode[1];
        }
    }

    function GetEMPName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
        if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                 document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                 return true;
             }
             return false;
         }--%>

    </script>

    

 <script>
     function getPageCode(mode) {
            var url;
            url = 'empShowMasterEmp.aspx?id=' + mode;
            var oWnd = radopen(url, "pop_code");
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
               document.getElementById("<%=txtBname.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBank_ID.ClientID %>").value = NameandCode[1];
                
        }
        }

     function GetEMPName() {
            var url;
            url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
               document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                 __doPostBack('<%= h_Emp_No.ClientID%>', 'ValueChanged');
        }
    }



    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_code" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left"  class="matters" width="20%"><span class="field-label"> Select Employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo" ValidationGroup="vg"  CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Change To</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddMode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddMode_SelectedIndexChanged">
                                <asp:ListItem Value="1">Bank</asp:ListItem>
                                <asp:ListItem Value="0">Cash</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddAccomodation" runat="server">
                                <asp:ListItem Value="0">Own Accomodation</asp:ListItem>
                                <asp:ListItem Value="1">Company Single Accomodation</asp:ListItem>
                                <asp:ListItem Value="2">Company Family Accomodation</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddTransport" runat="server">
                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                <asp:ListItem Value="False">No</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="tr_Bankaccount" runat="server">
                        <td align="left" class="matters"><span class="field-label">Account</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtBname" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnBank_name" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                OnClientClick="getPageCode('BK');return false;" />
                        </td>
                        <td align="left" class="matters"><span class="field-label">Account No</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAccCode" runat="server" MaxLength="30"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Select Date</span>
                            <asp:Label ID="lblText" runat="server" Text="(First day of Month)" Visible="false"></asp:Label>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td class="matters" align="left"><span class="field-label">Remarks</span></td>
                        <td class="matters"  align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr class="title-bg" runat="server" id="tr_Deatailhead">
                        <td colspan="4" align="left">
                            <asp:Label ID="lblDetail" runat="server"></asp:Label></td>
                    </tr>
                    <tr runat="server" id="tr_Deatails">
                        <td colspan="4" class="matters" align="center" valign="top">
                            <asp:GridView ID="gvDetails" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="EST_DTFROM" HeaderText="From Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_DTTO" HeaderText="To Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_REMARKS" HeaderText="Remarks">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NEW" HeaderText="New">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OLD" HeaderText="Old">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vg" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" OnValueChanged="h_Emp_No_ValueChanged" />
                <asp:HiddenField ID="h_Cat" runat="server" />
                <asp:HiddenField ID="h_Dpt" runat="server" />
                <asp:HiddenField ID="h_Grade" runat="server" />
                <asp:HiddenField ID="hfBank_ID" runat="server" />
                <asp:HiddenField ID="hf_JoinDt" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

