<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empViewDocExpiry.aspx.vb" Inherits="Accounts_accccViewCreditcardReceipt" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">


        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }
        window.onload = scroll_page;
    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server">Employee Document Expiry</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top">

                        <td valign="top" align="left" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server"
                                type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden"
                                value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden"
                                value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input
                                id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server"
                                type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                            <input id="h_Selected_menu_9" runat="server"
                                type="hidden" value="=" />
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table id="tbl_test" runat="server" align="center" border="0"
                    cellpadding="0" cellspacing="0" width="100%">


                    <tr>
                        <td align="left" width="20%"><span class="field-label">Filter </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="0">ALL</asp:ListItem>
                                <asp:ListItem Value="1">Working Unit</asp:ListItem>
                                <asp:ListItem Value="2">Visa Unit</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4" width="100%">
                            <asp:GridView ID="gvDocExpiry" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="20" OnRowDataBound="gvDocExpiry_RowDataBound">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            EMPLOYEE No.<br />
                                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# EVAL("EMP_ID") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            EMPLOYEE NAME<br />
                                            <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DOC. TYPE">
                                        <EditItemTemplate>
                                            &nbsp;
                                
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Doc. Type<br />
                                            <asp:TextBox ID="txtDocType" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOCTYPE" runat="server" Text='<%# Bind("ESD_DESCR") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="EXP. DATE">
                                        <HeaderTemplate>
                                            Exp. Date<br />
                                            <asp:TextBox ID="txtExpDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearchre" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblExpDate" runat="server" Text='<%# Bind("EMD_EXPDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Working Unit">
                                        <EditItemTemplate>
                                            &nbsp;
                                
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Working Unit<br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Visa Unit">
                                        <EditItemTemplate>
                                            &nbsp;
                                
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Visa Unit<br />
                                            <asp:TextBox ID="txtIssPlace" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIssuePlace" runat="server" Text='<%# Bind("VISA_BSU") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ISSUE DATE">
                                        <EditItemTemplate>
                                            &nbsp;
                                
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Visa Status<br />
                                            <asp:TextBox ID="txtIssueDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearchs" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIssueDate" runat="server" Text='<%# Bind("Visastatus") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VIEW">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <a id='detail'></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;<br />
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
