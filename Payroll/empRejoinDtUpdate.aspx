﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empRejoinDtUpdate.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Payroll_empRejoinDtUpdate" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getEmpID(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 495px; ";
            sFeatures += "dialogHeight: 410px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var payYear, payMonth;
            <%--url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'EN') {

                url = "../Accounts/accShowEmpDetail.aspx?id=EN"; //&payMonth=" + payMonth + "&PAYYEAR=" + payYear;
                result = window.showModalDialog(url, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                NameandCode = result.split('___');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_EDD_EMP_ID.ClientID %>").value = NameandCode[1];

            }--%>

            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_employee");
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_EDD_EMP_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpName.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>


     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblMonthly" runat="server" Text="Last Rejoin Date Updates"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 98%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary6" runat="server" ValidationGroup="groupM1"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" style="width: 100%">
                            <table align="center" cellpadding="5" cellspacing="0"
                                style="border-collapse: collapse; width: 100%">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="4" valign="middle">
                            <asp:Label ID="lblMonthly" runat="server" Text="Last Rejoin Date Updates"></asp:Label></td>
                    </tr>--%>


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Employee Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp_Name" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getEmpID('EN'); return false;"
                                             /></td>
                                    <td align="left" width="20%"><span class="field-label">Employee ID</span></td>
                                    <td align="left" width="30%">

                                        <asp:TextBox ID="txtEmpID" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Rejoining Date after vacation</span>
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID="txtLRdate" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revLjoing" runat="server" ControlToValidate="txtLRdate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 "
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <ajaxToolkit:CalendarExtender ID="txtLRdate_CalendarExtender" runat="server"
                                            CssClass="MyCalendar" PopupPosition="TopRight"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgLRdate" TargetControlID="txtLRdate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtLRdate_CalendarExtender2" runat="server"
                                            CssClass="MyCalendar" PopupPosition="TopRight"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtValueDate" TargetControlID="txtLRdate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgLRdate" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="trExcelUpload" runat="server">
                                    <td align="left"><span class="field-label">Bulk Upload</span></td>
                                    <td align="left" >
                                        <asp:FileUpload ID="flUpExcel" runat="server" />
                                        <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvMonthE_D" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                            Width="100%" SkinID="GridViewNormal">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EMP_ID" HeaderText="Emp ID" Visible="False">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMPNO" HeaderText="EmpNO">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMP_Name" HeaderText="Employee Name">
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMP_LRejoinDt" HeaderText="Last Rejoin Date"
                                                    ApplyFormatInEditMode="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <ItemStyle Width="15%" Wrap="True" />
                                                    <HeaderStyle Width="15%" Wrap="True" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button
                                            ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                                        <asp:Button
                                            ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button
                                            ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EDD_EMP_ID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

