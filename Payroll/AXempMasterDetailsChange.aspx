﻿<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AXempMasterDetailsChange.aspx.vb" Inherits="Payroll_AXempMasterDetailsChange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function GetEMPName() {

            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= h_Emp_No.ClientID%>', 'ValueChanged');
            }
        }


        function GetEMPAttName() {

            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=ERP", "pop_up1")
           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=hfAttendBy.ClientID %>').value = NameandCode[1];
                 document.getElementById('<%=txtAttendBy.ClientID %>').value = NameandCode[0];
                 //
                 return true;
             }
             return false;--%>
        }


        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfAttendBy.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtAttendBy.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtAttendBy.ClientID%>', 'TextChanged');
            }
        }


        function getPageCode(mode) {

            var NameandCode;
            var result;
            var url;
            document.getElementById("<%=hf_mode.ClientID%>").value = mode;
            url = 'empShowMasterEmp.aspx?id=' + mode;
            result = radopen(url, "pop_up2");


             <%--if (result == '' || result == undefined) {
                 return false;
             }
             NameandCode = result.split('___');
             if (mode == 'SD') {
                <%--document.getElementById("<%=txtNEmp.ClientID %>").value = NameandCode[0]
                 document.getElementById("<%=h_Empcat.ClientID %>").value = NameandCode[1];
                 return true;
                 //            PageMethods.setDefaultLeavePolicy();
             }
             if (mode == 'ME') {
                 //            document.getElementById("<%=txtNVisa.ClientID %>").value=NameandCode[0];
                //              document.getElementById("<%=h_VisaDES.ClientID%>").value=NameandCode[1];
                document.getElementById("<%=txtNMoe.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_MoeDES.ClientID%>").value = NameandCode[1];
            }
            if (mode == 'ML') {
                //            document.getElementById("<%=txtNMoe.ClientID %>").value=NameandCode[0];
              //              document.getElementById("<%=h_MoeDES.ClientID%>").value=NameandCode[1];
              document.getElementById("<%=txtNVisa.ClientID %>").value = NameandCode[0];
              document.getElementById("<%=h_VisaDES.ClientID%>").value = NameandCode[1];
          }
          else if (mode == 'AP') {
              // result = window.showModalDialog(url, "", sFeatures);
              if (result == '' || result == undefined) {
                  return false;
              }
              NameandCode = result.split('___');
              document.getElementById("<%=txtEmpApprovalPol.ClientID%>").value = NameandCode[0];
                  document.getElementById("<%=h_ApprovalPolicy.ClientID %>").value = NameandCode[1];
              }--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var mode;
            mode = document.getElementById("<%=hf_mode.ClientID%>").value;

            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                if (mode == 'SD') {
                <%-- document.getElementById("<%=txtNEmp.ClientID %>").value = NameandCode[0]; --%>
                    document.getElementById("<%=h_Empcat.ClientID %>").value = NameandCode[1];
                    __doPostBack('<%= h_Empcat.ClientID%>', 'TextChanged');
                    //   return true;
                    //            PageMethods.setDefaultLeavePolicy();
                }
                if (mode == 'ME') {
                    //            document.getElementById("<%=txtNVisa.ClientID %>").value=NameandCode[0];
                 //              document.getElementById("<%=h_VisaDES.ClientID%>").value=NameandCode[1];
                    document.getElementById("<%=txtNMoe.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_MoeDES.ClientID%>").value = NameandCode[1];
                    __doPostBack('<%= txtNMoe.ClientID%>', 'TextChanged');
                }
                if (mode == 'ML') {
                 //            document.getElementById("<%=txtNMoe.ClientID %>").value=NameandCode[0];
                //              document.getElementById("<%=h_MoeDES.ClientID%>").value=NameandCode[1];
                    document.getElementById("<%=txtNVisa.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_VisaDES.ClientID%>").value = NameandCode[1];
                    __doPostBack('<%= txtNVisa.ClientID%>', 'TextChanged');
                }
                else if (mode == 'AP') {
                    document.getElementById("<%=txtEmpApprovalPol.ClientID%>").value = NameandCode[0];
                    document.getElementById("<%=h_ApprovalPolicy.ClientID %>").value = NameandCode[1];
                    __doPostBack('<%= txtEmpApprovalPol.ClientID%>', 'TextChanged');
                }


            }
        }


        function clears(id) {
            if (id == '1')
                document.getElementById("<%=txtNVisa.ClientID %>").value = '';
            else if (id == '2')
                document.getElementById("<%=txtNMoe.ClientID %>").value = '';
      <%-- else if (id == '3')
           document.getElementById("<%=txtNEmp.ClientID %>").value = '';--%>
        }

        function getVacationPolicy() {

            var NameandCode;
            var result;
            var catID;
            catID = document.getElementById('<%=hfCat_ID.ClientID %>').value;
            if (catID != '') {
                result = radopen("selIDDesc.aspx?ID=VP&CATID=" + catID, "pop_up3");
                <%--if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=hf_VacationPolicy.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtVacationPolicy.ClientID %>').value = NameandCode[1];
                 }--%>
            }
            //return false;
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hf_VacationPolicy.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtVacationPolicy.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtVacationPolicy.ClientID%>', 'TextChanged');
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Change Employee non-AX details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table cellpadding="5" cellspacing="0" align="center" width="100%">
                    <%-- <tr class="subheader_img">
            <td colspan="4" style="height: 19px" align="left"></td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox><asp:ImageButton
                                ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                                CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator></td>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">School Designation</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtDes" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Category</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtCategory" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" width="20%"><span class="field-label">MOL Designation</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtVisa" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">New
             Visa Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtNVisa" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgVisa" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('ML');return false;" />
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="javascript:clears('1');return false;">Clear</asp:LinkButton></td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" width="20%"><span class="field-label">MOE Designation</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtMoe" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">New
             MOE Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtNMoe" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgMoe" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('ME');return false;" />
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="javascript:clears('2');return false;">Clear</asp:LinkButton></td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" width="20%"><span class="field-label">Current ABC Category</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtABC" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" width="20%"><span class="field-label">New ABC Category</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddABC" runat="server">
                                <asp:ListItem Value="N">No Change</asp:ListItem>
                                <asp:ListItem>A</asp:ListItem>
                                <asp:ListItem>B</asp:ListItem>
                                <asp:ListItem>C</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Leave
                        Approval Policy</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblApprovalPolicy" CssClass="field-value" runat="server"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">New Leave
                        Approval Policy</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpApprovalPol" runat="server" Wrap="False"></asp:TextBox>
                            <asp:ImageButton ID="Button4" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('AP');return false;" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Vacation Policy</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblVacPolicy" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">New Vacation Policy</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtVacationPolicy" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnVacationPolicy" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getVacationPolicy();return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Weekend 1</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlWeekEnd1" runat="server">
                                <asp:ListItem Value="">Please Select</asp:ListItem>
                                <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                                <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Weekend 2</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlWeekEnd2" runat="server">
                                <asp:ListItem Value="">Please Select</asp:ListItem>
                                <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                                <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Attendance Approved By</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAttendBy" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnAttendBy" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetEMPAttName(); return false;" />
                        </td>
                        <td runat="server" align="left" width="20%"><span class="field-label">Attendance Binding</span>  </td>
                        <td runat="server" align="left" width="30%">
                            <asp:CheckBox ID="chkbPunching" runat="server" CssClass="field-label" Checked="true" /></td>
                    </tr>
                    <tr>
                        <td rowspan="2" align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td rowspan="2" align="left" width="30%">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" TextMode="MultiLine"></asp:TextBox></td>
                        <td runat="server" id="tdArabicLabel" visible="false" align="left" width="20%"><span class="field-label">Email Payslip In Arabic</span>  </td>
                        <td runat="server" id="tdChckboxArabicLabel" visible="false" align="left" width="30%">
                            <asp:CheckBox ID="chkbEmailPayslipInArabic" runat="server" CssClass="field-label" /></td>
                        <td runat="server" id="tdABCCategory" visible="false" align="left" width="20%"><span class="field-label">ABC Category</span>  </td>
                        <td runat="server" id="tdddlABCCategory" visible="false" align="left" width="30%">
                            <asp:DropDownList ID="ddlABC" runat="server">
                                <asp:ListItem>A</asp:ListItem>
                                <asp:ListItem>B</asp:ListItem>
                                <asp:ListItem>C</asp:ListItem>
                            </asp:DropDownList></td>

                    </tr>
                    <tr runat="server">
                        <td runat="server" id="tdEleaveLbl" align="left" visible="false" width="20%"><span class="field-label">Enable E-Leave</span>  </td>
                        <td runat="server" id="tdChkboxEleave" align="left" visible="false" width="30%">
                            <asp:CheckBox ID="chkbEnabledELeave" runat="server" CssClass="field-label" /></td>

                    </tr>

                    <tr runat="server" id="trTax" visible="true">
                        <td align="left" width="20%"><span class="field-label">Enable Tax/CPF</span>  </td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkTax" runat="server" Checked="false" CssClass="field-label" /></td>
                        <td id="tdSILabel" align="left" width="20%"><span class="field-label">Enable Social Insurance(SI)</span>  </td>
                        <td id="tdSICheckbox" align="left" width="30%">
                            <asp:CheckBox ID="chkSICalculation" runat="server" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr class="title-bg" runat="server" id="tr_Deatailhead">
                        <td colspan="4" align="left">Employee Designation History</td>
                    </tr>
                    <tr runat="server" id="tr_Deatails">
                        <td colspan="4" align="center" valign="top">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="TTP_DESCR" HeaderText="Description" InsertVisible="False" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_DTFROM" HeaderText="From Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_DTTO" HeaderText="To Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_REMARKS" HeaderText="Remarks">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OLDDES" HeaderText="Old Designation">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NEWDES" HeaderText="New Designation">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" OnValueChanged="h_Emp_No_ValueChanged" />
                <asp:HiddenField ID="h_Empcat" runat="server" />
                <asp:HiddenField ID="h_VisaDES" runat="server" />
                <asp:HiddenField ID="h_MoeDES" runat="server" />
                <asp:HiddenField ID="h_ApprovalPolicy" runat="server" />
                <asp:HiddenField ID="hf_VacationPolicy" runat="server" />
                <asp:HiddenField ID="hfCat_ID" runat="server" />
                <asp:HiddenField ID="hfAttendBy" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

