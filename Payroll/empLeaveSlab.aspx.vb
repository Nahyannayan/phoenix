Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empLeaveSlab
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtCategory.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If 
            cblSplitup.DataBind()
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P100015" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If

            If Request.QueryString("viewid") <> "" Then

                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            Else
                ResetViewData()
            End If

        End If
    End Sub






    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT BLS.BLS_ID, " _
                  & " EC.ECT_DESCR, BLS.BLS_ELT_ID, ELT.ELT_DESCR," _
                  & " BLS.BLS_DESCRIPTION, BLS.BLS_DTFROM, BLS.BLS_DTTO, " _
                  & " BLS.BLS_ECT_ID, BLS.BLS_Days, BLS.BLS_DaysPerMonth," _
                  & " BLS.BLS_FULLPAYDAYS, BLS.BLS_HALFPAYDAYS, BLS.BLS_MinimumLeaveDays," _
                  & " BLS.BLS_ALLOWEDPERIODFROM, BLS.BLS_ALLOWEDPERIODTO, " _
                  & " BLS.BLS_bCheckLeavePeriod, BLS.BLS_bAccLeavetyp," _
                  & " BLS.BLS_MaxCarryFWD, BLS.BLS_MaxEnCashDays, BLS_EligMinMonths," _
                  & " BLS.BLS_bHolidaysAreLeave,BLS.BLS_maxLOPDays, " _
                  & " BLS.BLS_bLeaveSalary,isnull(BLS.BLS_bPAID,0) as BLS_bPAID, isnull(BLS.BLS_EOB,0) as BLS_EOB, BLS.BLS_SplitMonths FROM BSU_LEAVESLAB_S AS BLS " _
                  & " INNER JOIN EMPCATEGORY_M AS EC ON " _
                  & " BLS.BLS_ECT_ID = EC.ECT_ID INNER JOIN" _
                  & " EMPLEAVETYPE_M AS ELT ON BLS.BLS_ELT_ID = ELT.ELT_ID" _
                  & " WHERE BLS.BLS_BSU_ID='" & Session("sBSUID") & "'" _
                  & " AND  BLS.BLS_ID='" & p_Modifyid & "'"


            Dim ds As New DataSet
            ViewState("canedit") = "no"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddMonthstatusPeriodically.DataBind() 
                h_catid.Value = ds.Tables(0).Rows(0)("BLS_ECT_ID").ToString
                txtCategory.Text = ds.Tables(0).Rows(0)("ECT_DESCR").ToString
                ddMonthstatusPeriodically.SelectedIndex = -1
                ddMonthstatusPeriodically.Items.FindByValue(ds.Tables(0).Rows(0)("BLS_ELT_ID").ToString).Selected = True
                setSalaryComponents()
                txtFrom.Text = ds.Tables(0).Rows(0)("BLS_DTFROM").ToString
                txtTo.Text = ds.Tables(0).Rows(0)("BLS_DTTO").ToString
                txtVDateFrom.Text = ds.Tables(0).Rows(0)("BLS_ALLOWEDPERIODFROM").ToString
                txtVdateto.Text = ds.Tables(0).Rows(0)("BLS_ALLOWEDPERIODTO").ToString
                txtMaxwoBos.Text = ds.Tables(0).Rows(0)("BLS_maxLOPDays").ToString

                txtRemarks.Text = ds.Tables(0).Rows(0)("BLS_DESCRIPTION").ToString
                txtDaysperYear.Text = ds.Tables(0).Rows(0)("BLS_Days").ToString
                txtDayspermonth.Text = ds.Tables(0).Rows(0)("BLS_DaysPerMonth").ToString
                txtFullpaidDays.Text = ds.Tables(0).Rows(0)("BLS_FULLPAYDAYS").ToString
                txtHalfpaidDays.Text = ds.Tables(0).Rows(0)("BLS_HALFPAYDAYS").ToString
                txtMinLeavedays.Text = ds.Tables(0).Rows(0)("BLS_MinimumLeaveDays").ToString
                txtEligMinMonths.Text = ds.Tables(0).Rows(0)("BLS_EligMinMonths").ToString
                chkHolidaysareleave.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("BLS_bHolidaysAreLeave").ToString)
                chkAccumulated.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("BLS_bAccLeavetyp").ToString)
                chkLeaveSalary.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("BLS_bLeaveSalary").ToString)
                txtMaxcarryforward.Text = ds.Tables(0).Rows(0)("BLS_MaxCarryFWD").ToString
                txtMAxencashdays.Text = ds.Tables(0).Rows(0)("BLS_MaxEnCashDays").ToString

                chkPaid.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("BLS_bPAID"))
                chkEosCalc.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("BLS_EOB"))
                txtSalsplitup.Text = ds.Tables(0).Rows(0)("BLS_SplitMonths").ToString

                str_Sql = "SELECT  BLG.BLG_EARNCODE " _
                      & " FROM BSU_LEAVESLAB_S AS BLS INNER JOIN" _
                      & " BSU_LEAVEnGRATUITYEARN_S AS BLG ON BLS.BLS_ID = BLG.BLG_REF_ID" _
                      & " WHERE BLG.BLG_REF_ID=" & p_Modifyid & " AND   BLG.BLG_TRANTYPE='L'"
                ds.Tables.Clear() 'BLG.BLG_REF_ID,   BLG.BLG_ID,

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    cblSplitup.Items.FindByValue(ds.Tables(0).Rows(I)(0)).Selected = True
                Next
                str_Sql = "SELECT BLD_SLABFROM, BLD_SLABTO, BLD_FACTOR" _
                & " FROM BSU_LEAVESLAB_S_D" _
                & " WHERE (BLD_BLS_ID ='" & p_Modifyid & "')"
                ds.Tables.Clear()
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtRate1.Text = ds.Tables(0).Rows(0)("BLD_FACTOR")
                    txtRateFrom1.Text = ds.Tables(0).Rows(0)("BLD_SLABFROM")
                    txtRateTo1.Text = ds.Tables(0).Rows(0)("BLD_SLABTO")
                End If
                If ds.Tables(0).Rows.Count > 1 Then
                    txtRate2.Text = ds.Tables(0).Rows(1)("BLD_FACTOR")
                    txtRateFrom2.Text = ds.Tables(0).Rows(1)("BLD_SLABFROM")
                    txtRateTo2.Text = ds.Tables(0).Rows(1)("BLD_SLABTO")
                End If
                If ds.Tables(0).Rows.Count > 2 Then
                    txtRate3.Text = ds.Tables(0).Rows(2)("BLD_FACTOR")
                    txtRateFrom3.Text = ds.Tables(0).Rows(2)("BLD_SLABFROM")
                    txtRateTo3.Text = ds.Tables(0).Rows(2)("BLD_SLABTO")
                End If

            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub





    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtTo.Attributes.Add("readonly", "readonly")
        'txtOn.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")


        imgFrom.Enabled = False
        imgTo.Enabled = False
        imgCategory.Enabled = False
        setSalaryComponents()
        ddMonthstatusPeriodically.Enabled = False
        ddMonthstatusPeriodically.Enabled = False
    End Sub





    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtTo.Attributes.Remove("readonly")

        txtRemarks.Attributes.Remove("readonly")


        imgFrom.Enabled = True
        imgCategory.Enabled = True
        imgTo.Enabled = True
        ddMonthstatusPeriodically.Enabled = True
        ddMonthstatusPeriodically.Enabled = True
    End Sub





    Sub clear_All()

        txtRemarks.Text = ""
        txtFrom.Text = ""
        txtTo.Text = ""
        h_catid.Value = ""
        txtDaysperYear.Text = ""
        txtDayspermonth.Text = ""
        txtCategory.Text = ""
        txtFullpaidDays.Text = ""
        txtHalfpaidDays.Text = ""
        txtMinLeavedays.Text = ""
        txtVDateFrom.Text = ""
        txtVdateto.Text = ""
        txtMaxwoBos.Text = ""
        txtMaxcarryforward.Text = ""
        txtMAxencashdays.Text = ""
        For Each item As ListItem In cblSplitup.Items
            item.Selected = False
        Next
    End Sub





    Sub bindMonthstatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("strHoliday1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("strHoliday2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub






    Sub bindevents()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT ELT_ID, ELT_DESCR FROM EMPLEAVETYPE_M WHERE ELT_bLeave=1"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddMonthstatusPeriodically.DataSource = ds.Tables(0)
            ddMonthstatusPeriodically.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub






    Function checkErrors() As String
        Dim Str_Error As String = ""
        If txtRemarks.Text.Trim = "" Then
            Str_Error = "Please Enter Remarks <Br>"
        End If
        If IsNumeric(txtEligMinMonths.Text) = False Then
            Str_Error = Str_Error & "Please Min. Months For Eligibility <Br>"
        End If
        If txtFrom.Text.Trim = "" Then
            Str_Error = Str_Error & "Please Enter From Date <Br>"
        Else
            If IsDate(txtFrom.Text.Replace("yyyy", "2001")) = False Then
                Str_Error = Str_Error & "Please Enter Valid From Date <Br>"
            End If
        End If
        If IsDate(txtFrom.Text.Replace("yyyy", "2001")) = False Then
            Str_Error = Str_Error & "Please Valid To Date <Br>"
        End If
        If IsNumeric(txtDaysperYear.Text) = False Then
            Str_Error = Str_Error & "Please Valid Days Per Year <Br>"
        End If
        If IsNumeric(txtDayspermonth.Text) = False Then
            Str_Error = Str_Error & "Please Valid Days Per Month <Br>"
        End If
        If IsNumeric(txtFullpaidDays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Full Paid Days <Br>"
        End If
        If IsNumeric(txtHalfpaidDays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Half Paid Days <Br>"
        End If
        If IsNumeric(txtMinLeavedays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Minimum Leave Days <Br>"
        End If
        If IsNumeric(txtMaxcarryforward.Text) = False Then
            Str_Error = Str_Error & "Please Valid Maximum Carry Forward <Br>"
        End If
        If IsNumeric(txtMAxencashdays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Maximum Encash Days <Br>"
        End If
        If IsNumeric(txtMaxwoBos.Text) = False Then
            Str_Error = Str_Error & "Please Valid Max Allowed W/O Break of Service <Br>"
        End If

        If IsNumeric(txtMinLeavedays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Minimum Leave days <Br>"
        End If
        If txtVDateFrom.Text = "" Then
            Str_Error = Str_Error & "Please Begining Of Vacation <Br>"
        Else
            If IsDate(txtVDateFrom.Text.Replace("yyyy", "2001")) = False Then
                Str_Error = Str_Error & "Please Enter Valid  Begining Of Vacation <Br>"
            End If
        End If
        If chkLeaveperiod.Checked Then
            If txtSalsplitup.Text.Trim = "" Then
                Str_Error = Str_Error & "Please Enter Salary Split up <Br>"
            Else
                Dim str_months As String()
                str_months = txtSalsplitup.Text.Split(",")
                For i As Integer = 0 To str_months.Length - 1
                    If IsNumeric(str_months(i)) Then
                        If CInt(str_months(i)) > 12 Or CInt(str_months(i)) < 1 Then
                            Str_Error = Str_Error & "Please Enter Valid Split up <Br>"
                        End If
                    End If
                Next

            End If
        End If
        If txtVdateto.Text = "" Then
            Str_Error = Str_Error & "Please Enter End Of Vacation <Br>"
        Else
            If IsDate(txtVdateto.Text.Replace("yyyy", "2001")) = False Then
                Str_Error = Str_Error & "Please Enter Valid End Of Vacation <Br>"
            End If
        End If


        checkErrors = Str_Error
        ' txtDaysperYear.Text, txtDayspermonth.Text, _
        '                txtFullpaidDays.Text, txtHalfpaidDays.Text, txtMinLeavedays.Text, txtVDateFrom.Text, 
        'txtVdateto.Text, _
        '                chkHolidaysareleave.Checked, chkAccumulated.Checked, txtMaxcarryforward.Text, txtMAxencashdays.Text, _
        '                chkHolidaysareleave.Checked, txtMaxwoBos.Text,
    End Function
  





    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_error As String = checkErrors()
        If str_error <> "" Then
            lblError.Text = "Please check the Following : <br>" & str_error
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If ddMonthstatusPeriodically.SelectedIndex = -1 Then
                ddMonthstatusPeriodically.SelectedIndex = 0
            End If

            Dim retval As String = "1000"
            Dim BLS_ID As String
            Dim edit_bool As Boolean
            Dim NEW_BLS_ID As Integer = 0
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                'BLS_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                BLS_ID = 0
                Dim str_earncode As String = ""
                For Each item As ListItem In cblSplitup.Items
                    If (item.Selected) Then
                        If str_earncode <> "" Then
                            str_earncode = str_earncode & "|" & item.Value
                        Else
                            str_earncode = item.Value
                        End If
                    End If
                Next
                If str_earncode = "" Then
                    lblError.Text = "Cannot save. Please Select Atleast One Comopnents"
                    Exit Sub
                End If

                retval = PayrollFunctions.SaveBSU_LEAVESLAB_S(BLS_ID, Session("sbsuid"), 0, ddMonthstatusPeriodically.SelectedItem.Value, _
                txtRemarks.Text, txtFrom.Text, txtTo.Text, h_catid.Value, txtDaysperYear.Text, txtDayspermonth.Text, _
                txtFullpaidDays.Text, txtHalfpaidDays.Text, txtMinLeavedays.Text, txtVDateFrom.Text, txtVdateto.Text, _
                chkLeaveperiod.Checked, chkAccumulated.Checked, txtMaxcarryforward.Text, txtMAxencashdays.Text, _
                chkHolidaysareleave.Checked, txtMaxwoBos.Text, str_earncode, edit_bool, NEW_BLS_ID, txtEligMinMonths.Text, _
                chkLeaveSalary.Checked, txtSalsplitup.Text.Replace(",", "|"), chkPaid.Checked, chkEosCalc.Checked, stTrans)

            End If
            If retval = "0" Then
                '''''SAVE DETAILS...
                If IsNumeric(txtRateFrom1.Text) And IsNumeric(txtRateTo1.Text) And IsNumeric(txtRate1.Text) Then
                    retval = PayrollFunctions.SaveBSU_LEAVESLAB_S_D(0, NEW_BLS_ID, txtRateFrom1.Text, _
                    txtRateTo1.Text, txtRate1.Text, stTrans)
                Else
                    stTrans.Rollback()
                    lblError.Text = "Check Leave Rate 1"
                    Exit Sub
                End If
                If txtRateFrom2.Text <> "" And txtRateTo2.Text <> "" And txtRate2.Text <> "" Then
                    If IsNumeric(txtRateFrom2.Text) And IsNumeric(txtRateTo2.Text) And IsNumeric(txtRate2.Text) Then
                        retval = PayrollFunctions.SaveBSU_LEAVESLAB_S_D(0, NEW_BLS_ID, txtRateFrom2.Text, _
                        txtRateTo2.Text, txtRate2.Text, stTrans)
                    Else
                        stTrans.Rollback()
                        lblError.Text = "Check Leave Rate 2"
                        Exit Sub
                    End If
                End If
                If txtRateFrom3.Text <> "" And txtRateTo3.Text <> "" And txtRate3.Text <> "" Then
                    If IsNumeric(txtRateFrom3.Text) And IsNumeric(txtRateTo3.Text) And IsNumeric(txtRate3.Text) Then
                        retval = PayrollFunctions.SaveBSU_LEAVESLAB_S_D(0, NEW_BLS_ID, txtRateFrom3.Text, _
                        txtRateTo3.Text, txtRate3.Text, stTrans)
                    Else
                        stTrans.Rollback()
                        lblError.Text = "Check Leave Rate 3"
                        Exit Sub
                    End If
                End If

            End If


            '''''
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                ddMonthstatusPeriodically.SelectedItem.Value & "-" & txtRemarks.Text & "-" & _
                h_catid.Value & "-" & txtDaysperYear.Text & "-" & txtDayspermonth.Text & "-" & _
                txtFullpaidDays.Text & "-" & txtHalfpaidDays.Text & "-" & txtMinLeavedays.Text & "-" & _
                txtVDateFrom.Text & "-" & txtVdateto.Text & "-" & txtMaxcarryforward.Text & "-" & _
                txtMAxencashdays.Text, _
                    "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = "Cannot save. There is some errors"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

 





    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub






    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Sub setSalaryComponents()
        If ddMonthstatusPeriodically.SelectedIndex <> -1 Then
            If ddMonthstatusPeriodically.SelectedItem.Value = "AL" Then
                tr_AnnualDt.Visible = True
                tr_AnnualHd.Visible = True
            Else
                tr_AnnualDt.Visible = False
                tr_AnnualHd.Visible = False
            End If
        End If

    End Sub


     
    Protected Sub ddMonthstatusPeriodically_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMonthstatusPeriodically.SelectedIndexChanged
        setSalaryComponents()
    End Sub
End Class
