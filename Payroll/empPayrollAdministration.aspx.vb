﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Xml
Partial Class Payroll_empPayrollAdministration
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub txtBPNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBPNo.TextChanged
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "exec GetBPVoucherDetails '" & txtBPNo.Text & "','" & Session("sBsuid") & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (dr.Read())
                lblAmount.Text = AccountFunctions.Round(dr("VHH_AMOUNT"))
                lblVoucherDt.Text = dr("VHH_DOCDT").ToString
                lblNarration.Text = dr("VHH_NARRATION").ToString
                lblBSU.Text = dr("bsu_name").ToString
                lblBSUCloseDt.Text = dr("BSU_DAYCLOSEDT").ToString
                'tblDetails.Visible = True
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub
    Protected Sub ClearDetails()
        lblAmount.Text = ""
        lblBSU.Text = ""
        lblBSUCloseDt.Text = ""
        lblNarration.Text = ""
        lblVoucherDt.Text = ""
        txtBPNo.Text = ""
        txtRemovalRemarks.Text = ""
        h_Emp_No.Value = ""
        hfBPNo.Value = ""

        hfEmployeeNum.Value = ""
        lblEmpno.Text = ""
        txtEmpNo.Text = ""
        txtCat.Text = ""
        txtDept.Text = ""
        txtSD.Text = ""
        txtWithdrawRemarks.Text = ""
        lblDOJ.Text = ""
        lblResgDt.Text = ""
        lblLWD.Text = ""
        lblBPNo.Text = ""
        lblJVNo.Text = ""

        lblLEmpNo.Text = ""
        txtLEmpName.Text = ""
        gvAttendance.DataSource = Nothing
        gvAttendance.DataBind()
        txtLeaveRemarks.Text = ""

        lblDes.Text = ""
        lblCat.Text = ""
        lblDpt.Text = ""
        lblGDOJ.Text = ""
        lblBSUDOJ.Text = ""
        lblGross.Text = ""

        txtPaymonthRemarks.Text = ""
        txtWPSUnitName.Text = ""
        txtAccEmailId.Text = ""
        txtAccOffName.Text = ""
        txtEstablishmentCode.Text = ""
        txtlandline.Text = ""
        txtMobNo.Text = ""
        txtRoutingCode.Text = ""

        txtArrearEmpName.Text = ""
        txtArrearRemarks.Text = ""
        lblArrearEmpNo.Text = ""
        gvArrears.DataSource = Nothing
        gvArrears.DataBind()

        'Added by vikranth on 22nd Oct 2019
        txtEmpDOJ.Text = ""
        txtEmpBsuDOJ.Text = ""
        lblUpdateEmpNo.Text = ""
        txtUpdateEmpName.Text = ""
        txtUpdateEMPDOJRemarks.Text = ""

        'Added by vikranth on 30th oct 2019
        txtUpdateEMPReportsToRemarks.Text = ""

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpdateEMPReportsTo)
        If Not Page.IsPostBack Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'collect the url of the file to be redirected in view state
            '"P050115" changed to "H000079" -- V1.2
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P170011") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnRemoveBP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveBP.Click
        ' If ViewState("datamode") = "view" Then
        If txtRemovalRemarks.Text = "" Then
            lblError.Text = "Please enter removal remarks."
            Exit Sub
        End If
        RemoveBPVoucher()

    End Sub
    Protected Sub RemoveBPVoucher()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("RemovePayrollBP", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@BPNumber", hfBPNo.Value)
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@remarks", txtRemovalRemarks.Text)
            SqlCmd.Parameters.AddWithValue("@userName", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                ClearDetails()
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try


    End Sub
    Protected Sub BSULeaveSalarySetUp()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("BSULeaveSalaryFirstTimeSetUp", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))

            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                ClearDetails()
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try


    End Sub

    Protected Sub BSUGratuitySetUp()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("BSUGratuityFirstTimeSetUp", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))

            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                ClearDetails()
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try


    End Sub

    Protected Sub btnSetUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetUp.Click
        If rblGratLeave.SelectedIndex = -1 Then
            lblError.Text = "Please select the settings to copy."
            Exit Sub
        End If
        If rblGratLeave.SelectedValue = "L" Then
            BSULeaveSalarySetUp()
        ElseIf rblGratLeave.SelectedValue = "G" Then
            BSUGratuitySetUp()
        End If
    End Sub

    Protected Sub imgEmpSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmpSel.Click
        FillResignationDatas(h_Emp_No.Value, Session("sBSUID"))
    End Sub

    Private Function FillResignationDatas(ByVal EMP_ID As String, ByVal BSU_ID As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " exec GetEmployeeResignationDetails " & EMP_ID & ",'" & BSU_ID & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (dr.Read())
                txtEmpNo.Text = dr("EMPNAME").ToString
                txtSD.Text = dr("EMP_DES_DESCR").ToString        'V1.1
                txtCat.Text = dr("CATEGORY_DESC").ToString

                txtDept.Text = dr("EMP_DEPT_DESCR").ToString

                hfEmployeeNum.Value = dr("EMPNO").ToString 'V1.4
                lblEmpno.Text = dr("EMPNO").ToString 'V1.4
                lblDOJ.Text = Format(dr("EMP_JOINDT"), OASISConstants.DateFormat)
                lblResgDt.Text = Format(dr("EMP_RESGDT"), OASISConstants.DateFormat)
                lblLWD.Text = Format(dr("EMP_LastAttDt"), OASISConstants.DateFormat)
                lblBPNo.Text = dr("ESD_REFDOCNO").ToString
                lblJVNo.Text = dr("ESD_PAYROLLJV").ToString
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Function

    Protected Sub btnWithdrawResign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWithdrawResign.Click
        If hfEmployeeNum.Value = "" Or lblEmpno.Text = "" Then
            lblError.Text = "Please select an employee!"
            Exit Sub
        End If
        If lblBPNo.Text <> "" Then
            lblError.Text = "Final settlement already paid.Please remove the BP first."
            Exit Sub
        End If
        If lblBPNo.Text <> "" Then
            lblError.Text = "Final settlement already posted.Please contact IT to remove JV first."
            Exit Sub
        End If
        If txtWithdrawRemarks.Text = "" Then
            lblError.Text = "Please enter withdrawal remarks."
            Exit Sub
        End If
        WithdrawResignation()
    End Sub

    Protected Sub WithdrawResignation()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("employeeResignationWithdrawal", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@emp_id", h_Emp_No.Value)
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@USER", Session("sUsr_name"))
            SqlCmd.Parameters.AddWithValue("@remarks", txtWithdrawRemarks.Text)


            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                ClearDetails()
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try


    End Sub

    Protected Sub btnCancelWithdraw_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelWithdraw.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub
    Private Sub setModifyHeader(ByVal p_viewempid As String) 'setting header data on view/edit
        Try
            txtLeaveRemarks.Text = ""
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim dMonthYear As New Date

            str_Sql = "SELECT EAT_ID AS id, EAT_REMARKS AS remarks, " _
            & " EAT_DT AS Date, EAT_ELT_ID AS Leavetype,'Normal' as Status  , EAT_DAYS AS days, 1 AS Present,EMPNO,EAT_ADJ_ID " _
            & " FROM EMPATTENDANCE  AS EAT inner join employee_m on emp_id=eat_emp_id and eat_bsu_id=emp_bsu_id " _
            & " WHERE (EAT_BSU_ID = '" & Session("sBsuid") & "') AND (EAT_EMP_ID =" & p_viewempid & ") and eat_esd_id is null " _
            & " order by EAT_DT "
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                gvAttendance.DataSource = ds.Tables(0)
                gvAttendance.DataBind()
                lblLEmpNo.Text = ds.Tables(0).Rows(0).Item("EMPNO")

            Else
                gvAttendance.DataSource = Nothing
                gvAttendance.DataBind()
                gvAttendance.EmptyDataText = "No  unpaid leave data to display"
            End If
        Catch ex As Exception
            '  Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rtsAdminMenus_TabClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTabStripEventArgs) Handles rtsAdminMenus.TabClick
        If Not IsPostBack Then
            ClearDetails()
        End If
        If rtsAdminMenus.Tabs.FindTabByValue("10").Selected = True Then
            btnWithdrawResign.Visible = False
            btnRemoveProvision.Visible = True
        End If
        If rtsAdminMenus.Tabs.FindTabByValue("3").Selected = True Then
            btnWithdrawResign.Visible = True
            btnRemoveProvision.Visible = False

        End If

    End Sub


    Protected Sub txtLEmpName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        setModifyHeader(h_Emp_No.Value)
    End Sub

    Protected Sub btnSetLeavesPaid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetLeavesPaid.Click

        If txtLeaveRemarks.Text = "" Then
            lblError.Text = "Please enter remarks."
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim returnVal As Integer
        Try
            For Each gvr As GridViewRow In gvAttendance.Rows


                Dim SqlCmd As New SqlCommand("SaveAttendanceLeaveAsPaid", objConn, stTrans)

                SqlCmd.CommandType = CommandType.StoredProcedure

                If gvr.RowType = DataControlRowType.DataRow Then
                    Dim cb As HtmlInputCheckBox = New HtmlInputCheckBox
                    Dim lblAdj_ID As Label = New Label
                    cb = CType(gvr.FindControl("chkEAT_ID"), HtmlInputCheckBox)
                    lblAdj_ID = CType(gvr.FindControl("lblAdj_ID"), Label)
                    SqlCmd.Parameters.Clear()
                    If cb.Checked = True Then
                        SqlCmd.Parameters.AddWithValue("@EAT_ID", cb.Value)
                        SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
                        SqlCmd.Parameters.AddWithValue("@user", Session("sUsr_name"))
                        SqlCmd.Parameters.AddWithValue("@Remarks", txtLeaveRemarks.Text)
                        SqlCmd.Parameters.AddWithValue("@EAT_EMP_ID", h_Emp_No.Value)
                        SqlCmd.Parameters.AddWithValue("@EAT_ADJ_ID", lblAdj_ID.Text)
                        SqlCmd.Parameters.AddWithValue("@EAT_DT", gvr.Cells(1).Text)
                        SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
                        SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

                        SqlCmd.ExecuteNonQuery()
                        returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

                        If returnVal <> 0 Then
                            lblError.Text = UtilityObj.getErrorMessage(returnVal)
                            stTrans.Rollback()
                        End If
                    End If
                End If

            Next
            If returnVal = 0 Then
                lblError.Text = UtilityObj.getErrorMessage("0")
                stTrans.Commit()
                setModifyHeader(h_Emp_No.Value)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value, "Update",
                  Page.User.Identity.Name.ToString, Me.Page, txtLeaveRemarks.Text)
            End If
        Catch ex As Exception
            lblError.Text = UtilityObj.getErrorMessage("1000")
            stTrans.Rollback()

        Finally
            objConn.Close() 'Finally, close the connection
        End Try

    End Sub

    Protected Sub txtMoveEmpName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillMovingEmployeeDatas(h_Emp_No.Value)
    End Sub
    Private Function FillMovingEmployeeDatas(ByVal EMP_ID As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " select * from vw_OSO_EMPLOYEEMASTERDETAILS_COMPACT where emp_id= " & EMP_ID
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (dr.Read())

                lblDes.Text = dr("EMP_DES_DESCR").ToString        'V1.1
                lblCat.Text = dr("CATEGORY_DESC").ToString

                lblDpt.Text = dr("EMP_DEPT_DESCR").ToString

                ' hfEmployeeNum.Value = dr("EMPNO").ToString 'V1.4
                lblMoveEmpno.Text = dr("EMPNO").ToString 'V1.4
                lblGDOJ.Text = Format(dr("EMP_JOINDT"), OASISConstants.DateFormat)
                lblBSUDOJ.Text = Format(dr("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                lblGross.Text = dr("EMP_GROSSSAL").ToString
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Function
    Private Function FillTransferredEmployeeDatas(ByVal EMP_ID As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "exec GetTransferredEmployeeLeaveDetails " & EMP_ID
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If dr.HasRows Then


                While (dr.Read())

                    lblTranDes.Text = dr("EMP_DES_DESCR").ToString        'V1.1
                    lblTranCat.Text = dr("CATEGORY_DESC").ToString

                    lblTranDpt.Text = dr("EMP_DEPT_DESCR").ToString

                    ' hfEmployeeNum.Value = dr("EMPNO").ToString 'V1.4
                    lblTranEmpno.Text = dr("EMPNO").ToString 'V1.4
                    lblTranGDOJ.Text = Format(dr("EMP_JOINDT"), OASISConstants.DateFormat)
                    lblTranBSUDOJ.Text = Format(dr("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                    lblVacFrom.Text = Format(dr("ELV_DTFROM"), OASISConstants.DateFormat)
                    lblVacTo.Text = Format(dr("ELV_DTTO"), OASISConstants.DateFormat)
                    lblLastUnit.Text = dr("BSU_NAME").ToString
                End While
            Else
                lblError.Text = "No open leaves are there for the employee."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Function
    Private Sub FillWPSContactDetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "exec GetBSUWPSContactDetails '" & Session("sBsuid") & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (dr.Read())

                txtWPSUnitName.Text = dr("BSU_WPSName").ToString
                txtAccOffName.Text = dr("BSU_ACCOfficerName").ToString

                txtAccEmailId.Text = dr("BSU_AccOfficerEmail").ToString


                txtMobNo.Text = dr("BSU_ACCOfficerMobileNO").ToString
                txtlandline.Text = dr("BSU_ACCOfficerContactNO").ToString
                lblBsuName.Text = dr("BSU_NAME").ToString
                txtRoutingCode.Text = dr("BSU_WPSRouteID").ToString
                txtEstablishmentCode.Text = dr("bsu_wpsfld1").ToString
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub

    Protected Sub btnCancelMove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelMove.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub MoveEmployeeToGVS()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("SaveEmployeeMovementToGVS", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@EMP_id", h_Emp_No.Value)
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))

            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                returnVal = UtilityObj.operOnAudiTable(Master.MenuName, Session("sBsuid"),
                 "MoveToGVS", Page.User.Identity.Name.ToString, Me.Page, txtGVSRemarks.Text)
                If returnVal <> 0 Then
                    lblError.Text = UtilityObj.getErrorMessage(returnVal)
                    stTrans.Rollback()
                Else

                    stTrans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage(returnVal)
                    ClearDetails()
                End If
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try


    End Sub

    Protected Sub btnMoveToGVS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMoveToGVS.Click
        If txtMoveEmpName.Text = "" Or h_Emp_No.Value = "" Then
            lblError.Text = "Please select an employee to move to GVS."
            Exit Sub
        End If
        If txtGVSRemarks.Text = "" Then
            lblError.Text = "Please enter remarks."
            Exit Sub
        End If
        MoveEmployeeToGVS()
    End Sub

    Protected Sub imgTranEmployee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTranEmployee.Click
        FillTransferredEmployeeDatas(h_Emp_No.Value)
    End Sub

    Protected Sub RPVWPS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles RPVWPS.Load
        If Not IsPostBack() Then
            FillWPSContactDetails()
        End If

    End Sub

    Protected Sub btnSaveWPS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveWPS.Click
        If txtRoutingCode.Text.Length <> 9 Then
            lblError.Text = "Routing code needs to be 9 digit"
            Exit Sub
        End If
        If txtEstablishmentCode.Text.Length <> 13 Then
            lblError.Text = "Employer ID needs to be 13 digit"
            Exit Sub
        End If
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("UpdateWPS_DA_LetterDetails", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure

            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@Email", txtAccEmailId.Text)
            SqlCmd.Parameters.AddWithValue("@Mob", txtMobNo.Text)
            SqlCmd.Parameters.AddWithValue("@Landline", txtlandline.Text)
            SqlCmd.Parameters.AddWithValue("@ContactName", txtAccOffName.Text)
            SqlCmd.Parameters.AddWithValue("@routingCode", txtRoutingCode.Text)
            SqlCmd.Parameters.AddWithValue("@EmployerId", txtEstablishmentCode.Text)
            SqlCmd.Parameters.AddWithValue("@BSUWPSName", txtWPSUnitName.Text)

            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                returnVal = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
               "update", Page.User.Identity.Name.ToString, Me.Page, "Update WPS letter" & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(returnVal)

                FillWPSContactDetails()
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try

    End Sub
    Private Sub FillPayYearPayMonth()
        ddlPayMonth.Items.Clear()
        ddlPayYear.Items.Clear()
        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        'swapna changed
        For i As Integer = 2011 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next
        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
                lblPaymonth.Text = MonthName(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))
                lblPayYear.Text = ds.Tables(0).Rows(0)("BSU_PAYYEAR")
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub RPVPaymonth_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles RPVPaymonth.Load
        If Not IsPostBack Then
            FillPayYearPayMonth()
        End If
    End Sub

    Protected Sub btnUpdatePaymonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePaymonth.Click
        If txtPaymonthRemarks.Text = "" Then
            lblError.Text = "Please enter remarks for changing pay month/year."
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try

            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("UpdateBSUPayMonth", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure

            SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@Month", ddlPayMonth.SelectedValue)
            SqlCmd.Parameters.AddWithValue("@Year", ddlPayYear.SelectedValue)


            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            Dim Retval As String
            Retval = SqlCmd.Parameters("@RETURN_MSG").Value

            If Retval <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(Retval)
                stTrans.Rollback()

            Else

                Retval = UtilityObj.operOnAudiTable(Master.MenuName, Session("sBsuid"),
                  "update", Page.User.Identity.Name.ToString, Me.Page, txtPaymonthRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                If Retval <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(Retval)
                Else
                    stTrans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage("0")

                End If
                ClearDetails()
            End If

            FillPayYearPayMonth()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message

        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub btnCancelPaymonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelPaymonth.Click
        FillPayYearPayMonth()
        ClearDetails()
    End Sub

    Protected Sub btnSaveRejoin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveRejoin.Click
        If txtPaymonthRemarks.Text = "" Then
            lblError.Text = "Please enter remarks for changing pay month/year."
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try

            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("UpdateBSUPayMonth", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure

            SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@Month", ddlPayMonth.SelectedValue)
            SqlCmd.Parameters.AddWithValue("@Year", ddlPayYear.SelectedValue)


            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            Dim Retval As String
            Retval = SqlCmd.Parameters("@RETURN_MSG").Value

            If Retval <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(Retval)
                stTrans.Rollback()

            Else

                Retval = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
                  "update", Page.User.Identity.Name.ToString, Me.Page, txtPaymonthRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                If Retval <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(Retval)
                Else
                    stTrans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage("0")

                End If
                ClearDetails()
            End If

            FillPayYearPayMonth()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message

        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub btnCancelLeaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelLeaves.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnRemoveProvision_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveProvision.Click
        If txtEmpNo.Text = "" Or h_Emp_No.Value = "" Then
            lblError.Text = "Please select an employee."
            Exit Sub
        End If
        If txtWithdrawRemarks.Text = "" Then
            lblError.Text = "Please enter remarks."
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim ds1 As New DataSet
        Dim returnVal As String
        Try
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("UpdateFFSFlag", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@emp_id", h_Emp_No.Value)
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))

            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.Output

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> "" Then
                lblError.Text = returnVal
                stTrans.Rollback()
            Else
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage("0")
                ClearDetails()
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub imgArrearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgArrearSearch.Click
        If txtArrearEmpName.Text <> "" And h_Emp_No.Value <> "" Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String = " exec GetSalaryArrears " & h_Emp_No.Value
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvArrears.DataSource = ds.Tables(0)
                    gvArrears.DataBind()
                    lblArrearEmpNo.Text = ds.Tables(0).Rows(0)("empno").ToString
                Else
                    gvArrears.DataSource = Nothing
                    gvArrears.DataBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, Page.Title)
            End Try
        End If
    End Sub


    Protected Sub btnSetArrearsPaid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetArrearsPaid.Click
        If txtArrearRemarks.Text = "" Then
            lblError.Text = "Please enter remarks."
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim chkdRows As Integer = 0

        For Each gr As GridViewRow In gvArrears.Rows
            Dim cb1 As HtmlInputCheckBox = New HtmlInputCheckBox

            cb1 = CType(gr.FindControl("chkESD_ID"), HtmlInputCheckBox)
            If cb1.Checked Then
                chkdRows = chkdRows + 1
            End If
        Next
        If chkdRows = 0 Then
            lblError.Text = "No rows selected."
            Exit Sub
        End If
        objConn.Open()
        Try
            For Each grow As GridViewRow In gvArrears.Rows
                Dim cb As HtmlInputCheckBox = New HtmlInputCheckBox

                cb = CType(grow.FindControl("chkESD_ID"), HtmlInputCheckBox)
                If cb.Checked Then


                    Dim stTrans As SqlTransaction = objConn.BeginTransaction
                    Dim SqlCmd As New SqlCommand("UpdateSalaryPaid", objConn, stTrans)

                    SqlCmd.CommandType = CommandType.StoredProcedure

                    SqlCmd.Parameters.AddWithValue("@EMP_ID", h_Emp_No.Value)
                    SqlCmd.Parameters.AddWithValue("@ESD_ID", cb.Value)

                    SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
                    SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

                    SqlCmd.ExecuteNonQuery()
                    Dim Retval As String
                    Retval = SqlCmd.Parameters("@RETURN_MSG").Value

                    If Retval <> 0 Then
                        lblError.Text = UtilityObj.getErrorMessage(Retval)
                        stTrans.Rollback()

                    Else

                        Retval = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
                          "update", Page.User.Identity.Name.ToString, Me.Page, txtArrearRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                        If Retval <> 0 Then
                            stTrans.Rollback()
                            lblError.Text = UtilityObj.getErrorMessage(Retval)
                        Else
                            stTrans.Commit()
                            lblError.Text = UtilityObj.getErrorMessage("0")

                        End If

                    End If
                End If

            Next
            imgArrearSearch_Click(Nothing, Nothing)
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message

        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub btnArrearsCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArrearsCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub ImgLoan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgLoan.Click
        If txtLoanEmpname.Text <> "" And h_Emp_No.Value <> "" Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String = " exec GetEmployeeOpenLoans " & h_Emp_No.Value & ",'" & Session("sBsuid").ToString & "'"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvLoanDetails.DataSource = ds.Tables(0)
                    gvLoanDetails.DataBind()
                    lblLoanEmpno.Text = ds.Tables(0).Rows(0)("empno").ToString
                Else
                    gvLoanDetails.DataSource = Nothing
                    gvLoanDetails.DataBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, Page.Title)
            End Try
        End If
    End Sub

    Protected Sub btnSaveLoanPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveLoanPayment.Click


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim chkdRows As Integer = 0

        For Each gr As GridViewRow In gvLoanDetails.Rows
            Dim cb1 As HtmlInputCheckBox = New HtmlInputCheckBox
            cb1 = CType(gr.FindControl("chkELD_ID"), HtmlInputCheckBox)
            Dim txtPaidRemarks As TextBox = New TextBox
            txtPaidRemarks = CType(gr.FindControl("txtPaidRemarks"), TextBox)
            Dim txtPaidAmount As TextBox = New TextBox
            txtPaidAmount = CType(gr.FindControl("txtPaidAmt"), TextBox)
            If cb1.Checked Then
                chkdRows = chkdRows + 1
                If txtPaidRemarks.Text = "" Or Not IsNumeric(txtPaidAmount.Text) Then
                    lblError.Text = "Please enter remarks and amount correctly for selected records."
                    Exit Sub

                End If
            End If
        Next
        If chkdRows = 0 Then
            lblError.Text = "No records selected."
            Exit Sub
        End If
        objConn.Open()
        Try
            For Each grow As GridViewRow In gvLoanDetails.Rows
                Dim cb As HtmlInputCheckBox = New HtmlInputCheckBox

                cb = CType(grow.FindControl("chkELD_ID"), HtmlInputCheckBox)
                Dim txtPaidRemarks As TextBox = New TextBox
                txtPaidRemarks = CType(grow.FindControl("txtPaidRemarks"), TextBox)
                Dim txtPaidAmount As TextBox = New TextBox
                txtPaidAmount = CType(grow.FindControl("txtPaidAmt"), TextBox)
                If cb.Checked Then


                    Dim stTrans As SqlTransaction = objConn.BeginTransaction
                    Dim SqlCmd As New SqlCommand("SaveLoanPayments", objConn, stTrans)

                    SqlCmd.CommandType = CommandType.StoredProcedure

                    SqlCmd.Parameters.AddWithValue("@eld_id", cb.Value)
                    SqlCmd.Parameters.AddWithValue("@PaidAmt", CDbl(txtPaidAmount.Text))
                    SqlCmd.Parameters.AddWithValue("@Remarks", txtPaidRemarks.Text)

                    SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
                    SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

                    SqlCmd.ExecuteNonQuery()
                    Dim Retval As String
                    Retval = SqlCmd.Parameters("@RETURN_MSG").Value

                    If Retval <> 0 Then
                        lblError.Text = UtilityObj.getErrorMessage(Retval)
                        stTrans.Rollback()

                    Else

                        Retval = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
                          "update", Page.User.Identity.Name.ToString, Me.Page, txtPaidRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                        If Retval <> 0 Then
                            stTrans.Rollback()
                            lblError.Text = UtilityObj.getErrorMessage(Retval)
                        Else
                            stTrans.Commit()
                            lblError.Text = UtilityObj.getErrorMessage("0")

                        End If

                    End If
                End If

            Next
            ImgLoan_Click(Nothing, Nothing)
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message

        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub txtEmpNo_TextChanged(sender As Object, e As EventArgs)
        FillResignationDatas(h_Emp_No.Value, Session("sBSUID"))
    End Sub

    Protected Sub txtTranEmpName_TextChanged(sender As Object, e As EventArgs)
        FillTransferredEmployeeDatas(h_Emp_No.Value)
    End Sub

    Protected Sub txtLoanEmpname_TextChanged(sender As Object, e As EventArgs)
        If txtLoanEmpname.Text <> "" And h_Emp_No.Value <> "" Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String = " exec GetEmployeeOpenLoans " & h_Emp_No.Value & ",'" & Session("sBsuid").ToString & "'"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvLoanDetails.DataSource = ds.Tables(0)
                    gvLoanDetails.DataBind()
                    lblLoanEmpno.Text = ds.Tables(0).Rows(0)("empno").ToString
                Else
                    gvLoanDetails.DataSource = Nothing
                    gvLoanDetails.DataBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, Page.Title)
            End Try
        End If
    End Sub
    Protected Sub txtArrearEmpName_TextChanged(sender As Object, e As EventArgs)
        If txtArrearEmpName.Text <> "" And h_Emp_No.Value <> "" Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String = " exec GetSalaryArrears " & h_Emp_No.Value
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvArrears.DataSource = ds.Tables(0)
                    gvArrears.DataBind()
                    lblArrearEmpNo.Text = ds.Tables(0).Rows(0)("empno").ToString
                Else
                    gvArrears.DataSource = Nothing
                    gvArrears.DataBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, Page.Title)
            End Try
        End If
    End Sub
    'Added by vikranth on 22nd Oct 2019
    Protected Sub txtUpdateEmpName_TextChanged(sender As Object, e As EventArgs) Handles txtUpdateEmpName.TextChanged
        If txtUpdateEmpName.Text <> "" And h_Emp_No.Value <> "" Then
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String = " select * from vw_OSO_EMPLOYEEMASTERDETAILS_COMPACT where emp_id= " & h_Emp_No.Value
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                While (dr.Read())
                    lblUpdateEmpNo.Text = dr("EMPNO").ToString 'V1.4
                    txtEmpDOJ.Text = Format(dr("EMP_JOINDT"), OASISConstants.DateFormat)
                    txtEmpBsuDOJ.Text = Format(dr("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                End While
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, Page.Title)
            End Try
        End If
    End Sub
    'Added by vikranth on 22nd Oct 2019
    Protected Sub btnUpdateEMPDOJ_Click(sender As Object, e As EventArgs) Handles btnUpdateEMPDOJ.Click

        If txtUpdateEmpName.Text = "" Then
            lblError.Text = "Please select Employee for changing the date of Join & BSU date of join."
            Exit Sub
        End If
        If txtEmpDOJ.Text = "" Then
            lblError.Text = "Please enter Date of Join."
            Exit Sub
        End If
        If txtEmpBsuDOJ.Text = "" Then
            lblError.Text = "Please enter BSU Date of Join."
            Exit Sub
        End If
        If txtUpdateEMPDOJRemarks.Text = "" Then
            lblError.Text = "Please enter remarks for changing the date of Join & BSU date of join."
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try

            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("DOJUpdateAfterPayroll", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@empno", lblUpdateEmpNo.Text)
            SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@JoinDT", txtEmpDOJ.Text)
            SqlCmd.Parameters.AddWithValue("@BSUJoinDT", txtEmpBsuDOJ.Text)


            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            Dim Retval As String
            Retval = SqlCmd.Parameters("@RETURN_MSG").Value

            If Retval <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(Retval)
                stTrans.Rollback()

            Else

                Retval = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
                  "update", Page.User.Identity.Name.ToString, Me.Page, txtUpdateEMPDOJRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                If Retval <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(Retval)
                Else
                    stTrans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage("0")

                End If
                ClearDetails()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message

        Finally
            objConn.Close()
        End Try
    End Sub
    'Added by vikranth on 22nd Oct 2019
    Protected Sub btnupdateEMPDOJCancel_Click(sender As Object, e As EventArgs) Handles btnupdateEMPDOJCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    'Added by vikranth on 23rd Oct 2019
    Protected Sub btnUpdateEMPReportsTo_Click(sender As Object, e As EventArgs) Handles btnUpdateEMPReportsTo.Click

        If txtUpdateEMPReportsToRemarks.Text = "" Then
            lblError.Text = "Please enter remarks."
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim strFileName As String
            If flUpExcel.HasFile Then
                strFileName = flUpExcel.PostedFile.FileName
                Dim ext As String = Path.GetExtension(strFileName)
                If ext = ".xls" Then
                    '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                    ' getdataExcel(strFileName)
                    UpLoadDBF()
                Else
                    lblError.Text = "Please upload .xls files only."
                    Exit Sub
                End If

            Else
                lblError.Text = "File not uploaded"
                Exit Sub
            End If
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            'Dim SqlCmd As New SqlCommand("DOJUpdateAfterPayroll", objConn, stTrans)

            'SqlCmd.CommandType = CommandType.StoredProcedure
            'SqlCmd.Parameters.AddWithValue("@empno", lblUpdateEmpNo.Text)
            'SqlCmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))
            'SqlCmd.Parameters.AddWithValue("@JoinDT", txtEmpDOJ.Text)
            'SqlCmd.Parameters.AddWithValue("@BSUJoinDT", txtEmpBsuDOJ.Text)


            'SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            'SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            'SqlCmd.ExecuteNonQuery()
            'Dim Retval As String
            'Retval = SqlCmd.Parameters("@RETURN_MSG").Value

            'If Retval <> 0 Then
            '    lblError.Text = UtilityObj.getErrorMessage(Retval)
            '    stTrans.Rollback()

            'Else

            '    Retval = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
            '      "update", Page.User.Identity.Name.ToString, Me.Page, txtUpdateEMPDOJRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
            '    If Retval <> 0 Then
            '        stTrans.Rollback()
            '        lblError.Text = UtilityObj.getErrorMessage(Retval)
            '    Else
            '        stTrans.Commit()
            '        lblError.Text = UtilityObj.getErrorMessage("0")

            '    End If
            '    ClearDetails()
            'End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message

        Finally
            objConn.Close()
        End Try
    End Sub
    'Added by vikranth on 23rd Oct 2019
    Protected Sub btnUpdateEMPReportsToCancel_Click(sender As Object, e As EventArgs) Handles btnUpdateEMPReportsToCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 9)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(0).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(0).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next

            'Dim strEarnDescr As String = ""
            'ColName = xltable.Columns(4).ColumnName
            'For Each xlRow In xltable.Rows
            '    If xlRow(ColName).ToString <> "" Then
            '        strEarnDescr &= IIf(strEarnDescr <> "", ",'", "'") & xlRow(ColName).ToString & "'"
            '    End If
            'Next

            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If

            Dim ValidStudent As New DataTable
            Dim ValidCode As New DataTable

            Dim mTable As New DataTable
            mTable = CreateDataTableUpdateReportsTo()

            Dim rowId As Integer = 0
            Dim dataStartRowId As Integer = 2
            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                ValidStudent = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in (" & strEmpNos & ") and emp_bsu_id='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)


                mRow = mTable.NewRow
                mRow("EMPNO") = xlRow(0)
                mRow("EMP_Name") = xlRow(1)
                mRow("EMP_BSU_ID") = Session("sBsuid")
                mRow("Position_ID") = xlRow(2)
                mRow("Reports_To_Empno") = xlRow(4)
                mRow("Reports_To_Position_ID") = xlRow(6)
                mRow("Effective_Date") = xlRow(8)

                'mRow("Delete_flag") = ""
                'mRow("EDD_EMP_ID")=
                If ValidStudent.Select("Empno='" & xlRow(0) & "'", "").Length = 0 Then
                    'mRow("NotValid") = "1"
                    'mRow("ErrorText") = "Invalid Employee No"
                    lblError.Text = "Invalid Employee No:" & xlRow(0) & ", at row :" & dataStartRowId & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidStudent.Select("Empno='" & xlRow(0) & "'", "")(0)
                    mRow("EMP_ID") = drEmp("Emp_id")
                    mRow("EMP_Name") = drEmp("EMPNAME")
                    'mRow("EDD_EMP_ID") = ValidStudent.Rows(rowId).Item("Emp_id")
                    'mRow("EDD_EMP_Name") = ValidStudent.Rows(rowId).Item("EMPNAME")
                End If
                rowId = rowId + 1
                dataStartRowId = dataStartRowId + 1
                mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim trans As SqlTransaction = objConn.BeginTransaction("SampleTransaction")

            Session("dtDt") = mTable
            Dim htEMPDtails As New Hashtable
            Dim strXMLString As New StringBuilder
            For Each row As DataRow In mTable.Rows
                'strDetail = row("Detail")
                htEMPDtails(row("EMPNO")) = GenerateEmployeeDetailsXML(strXMLString.ToString(), row("EMPNO"), row("EMP_Name"), row("EMP_BSU_ID"), row("Position_ID"), row("Reports_To_Empno"), row("Reports_To_Position_ID"), row("Effective_Date"))

            Next row
            Dim arrStr(htEMPDtails.Count) As String
            htEMPDtails.Values.CopyTo(arrStr, 0)

            strXMLString.Append("<EMPLOYEE_DETAILS>")
            Dim ienum As IDictionaryEnumerator = htEMPDtails.GetEnumerator()
            While (ienum.MoveNext())
                strXMLString.Append(ienum.Value)
            End While
            strXMLString.Append("</EMPLOYEE_DETAILS>")
            Dim iReturnvalue As Integer
            Dim cmd As New SqlCommand("UpdateEmployeeReortsTO", objConn, trans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim sqlpEML_ESD_ID As New SqlParameter("@EMP_DET", SqlDbType.Xml)
            sqlpEML_ESD_ID.Value = strXMLString.ToString() 'GenerateEmployeeDetailsXML(EML_EMP_IDs, EML_ESD_IDs)
            cmd.Parameters.Add(sqlpEML_ESD_ID)

            Dim Return_Output As New SqlParameter("@Return_Output", SqlDbType.VarChar, 5000)
            Return_Output.Direction = ParameterDirection.Output
            cmd.Parameters.Add(Return_Output)


            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
                lblError.Text = Return_Output.Value
                'Return iReturnvalue
            Else
                iReturnvalue = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value,
                  "update", Page.User.Identity.Name.ToString, Me.Page, txtUpdateEMPReportsToRemarks.Text & "-(" & rtsAdminMenus.SelectedTab.Value & ")")
                If iReturnvalue <> 0 Then
                    trans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
                Else
                    trans.Commit()
                    lblError.Text = Return_Output.Value

                End If
                ClearDetails()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub
    Public Function AppendNewValueToXML(ByVal XmlVal As String, ByVal EMP_ID As String, ByVal ESD_ID As String, ByVal EMAIL_ID As String) As String
        Dim xmlDocument As New XmlDocument()
        Dim XMLEESD_IDs As XmlElement
        Dim XMLEESD_ID As XmlElement
        Dim XMLEEMP_ID As XmlElement
        Dim SALARY_DETAILS As XmlElement
        Try
            XmlVal = "<EMPLOYEE_SALARY>" & XmlVal & "</EMPLOYEE_SALARY>"
            'Dim newEmployee As Boolean = True
            xmlDocument.LoadXml(XmlVal)
            For Each node As XmlNode In xmlDocument.SelectNodes("/EMPLOYEE_SALARY/SALARY_DETAILS")
                SALARY_DETAILS = TryCast(node, XmlElement)
                XMLEEMP_ID = DirectCast(SALARY_DETAILS.SelectSingleNode("EMP_ID"), XmlElement)
                If XMLEEMP_ID IsNot Nothing Then
                    If XMLEEMP_ID.InnerText = EMP_ID Then
                        XMLEESD_IDs = DirectCast(SALARY_DETAILS.SelectSingleNode("ESD_IDs"), XmlElement)
                        XMLEESD_ID = xmlDocument.CreateElement("ESD_ID")
                        XMLEESD_ID.InnerText = ESD_ID
                        XMLEESD_IDs.AppendChild(XMLEESD_ID)
                        SALARY_DETAILS.AppendChild(XMLEESD_IDs)
                        xmlDocument.DocumentElement.InsertBefore(SALARY_DETAILS, xmlDocument.DocumentElement.LastChild)
                        'newEmployee = False
                        Exit For
                    End If
                End If
            Next
            'If newEmployee Then
            '    AppendNewEMPID(XmlVal, EMP_ID, ESD_ID, EMAIL_ID)
            'End If
            Return xmlDocument.DocumentElement.InnerXml
            'Return xmlDocument.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Function GenerateEmployeeDetailsXML(ByVal xmlval As String, ByVal EMPNO As String, ByVal EMP_Name As String, ByVal EMP_BSU_ID As String, ByVal Position_ID As String, ByVal Reports_To_Empno As String, ByVal Reports_To_Position_ID As String, ByVal Effective_Date As String) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLEEMPNO As XmlElement
        Dim XMLEEMP_Name As XmlElement
        Dim XMLEEMP_BSU_ID As XmlElement
        Dim XMLEPosition_ID As XmlElement
        Dim XMLEReports_To_Empno As XmlElement
        Dim XMLEReports_To_Position_ID As XmlElement
        Dim XMLEEffective_Date As XmlElement
        Dim XMLEEMP_DETAILS As XmlElement
        Dim elements As String() = New String(7) {}
        elements(0) = "EMP_DETAILS"
        elements(1) = "EMPNO"
        elements(2) = "EMP_Name"
        elements(3) = "EMP_BSU_ID"
        elements(4) = "Position_ID"
        elements(5) = "Reports_To_Empno"
        elements(6) = "Reports_To_Position_ID"
        elements(7) = "Effective_Date"
        Try
            XMLEEMP_DETAILS = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(XMLEEMP_DETAILS)
            XMLEEMPNO = xmlDoc.CreateElement(elements(1))
            XMLEEMPNO.InnerText = EMPNO
            XMLEEMP_Name = xmlDoc.CreateElement(elements(2))
            XMLEEMP_Name.InnerText = EMP_Name
            XMLEEMP_BSU_ID = xmlDoc.CreateElement(elements(3))
            XMLEEMP_BSU_ID.InnerText = EMP_BSU_ID
            XMLEPosition_ID = xmlDoc.CreateElement(elements(4))
            XMLEPosition_ID.InnerText = Position_ID
            XMLEReports_To_Empno = xmlDoc.CreateElement(elements(5))
            XMLEReports_To_Empno.InnerText = Reports_To_Empno
            XMLEReports_To_Position_ID = xmlDoc.CreateElement(elements(6))
            XMLEReports_To_Position_ID.InnerText = Reports_To_Position_ID
            XMLEEffective_Date = xmlDoc.CreateElement(elements(7))
            XMLEEffective_Date.InnerText = Effective_Date
            XMLEEMP_DETAILS.AppendChild(XMLEEMPNO)
            XMLEEMP_DETAILS.AppendChild(XMLEEMP_Name)
            XMLEEMP_DETAILS.AppendChild(XMLEEMP_BSU_ID)
            XMLEEMP_DETAILS.AppendChild(XMLEPosition_ID)
            XMLEEMP_DETAILS.AppendChild(XMLEReports_To_Empno)
            XMLEEMP_DETAILS.AppendChild(XMLEReports_To_Position_ID)
            XMLEEMP_DETAILS.AppendChild(XMLEEffective_Date)
            ' xmlDoc.DocumentElement.InsertBefore(XMLESALARY_DETAILS, xmlDoc.DocumentElement.LastChild)

            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Function CreateDataTableUpdateReportsTo() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim EMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim EMPNO As New DataColumn("EMPNO", System.Type.GetType("System.String"))
            Dim EMP_Name As New DataColumn("EMP_Name", System.Type.GetType("System.String"))
            Dim EMP_BSU_ID As New DataColumn("EMP_BSU_ID", System.Type.GetType("System.String"))
            Dim Position_ID As New DataColumn("Position_ID", System.Type.GetType("System.String"))
            Dim Reports_To_Empno As New DataColumn("Reports_To_Empno", System.Type.GetType("System.String"))
            Dim Reports_To_Position_ID As New DataColumn("Reports_To_Position_ID", System.Type.GetType("System.String"))
            Dim Effective_Date As New DataColumn("Effective_Date", System.Type.GetType("System.String"))


            dtDt.Columns.Add(EMP_ID)
            dtDt.Columns.Add(EMPNO)
            dtDt.Columns.Add(EMP_Name)
            dtDt.Columns.Add(EMP_BSU_ID)
            dtDt.Columns.Add(Position_ID)
            dtDt.Columns.Add(Reports_To_Empno)
            dtDt.Columns.Add(Reports_To_Position_ID)
            dtDt.Columns.Add(Effective_Date)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
End Class
