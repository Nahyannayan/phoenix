Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO

Partial Class Payroll_empProbationExpiryList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gridbind()

            lblExpiry.Text = "(With in " & Session("DocExpDays") & " Days)"
        End If


    End Sub

    Protected Sub gvDocExpiry_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDocExpiry.RowDataBound
        Try
            Dim lblESD_ID As New Label
            lblESD_ID = TryCast(e.Row.FindControl("lblESD_ID"), Label)
            Dim lnkView As New HyperLink
            lnkView = TryCast(e.Row.FindControl("lnkView"), HyperLink)
            If lnkView IsNot Nothing And lblESD_ID IsNot Nothing Then
                ' Dim i As New Encryption64
                'hlCEdit.NavigateUrl = "journalvoucher.aspx?editid=" & lblGUID.Text
                Dim strID As String = lblESD_ID.Text 'Encr_decrData.Encrypt()
                lnkView.NavigateUrl = String.Format("empViewDocExpiry.aspx?MainMnu_code={0}&docid=" & strID, "menucode")
                lnkView.Enabled = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Sub gridbind()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Using conn As SqlConnection = New SqlConnection(str_conn)

                Dim str_Sql As String
                If chkAll.Checked Then
                    str_Sql = " SELECT EMP.EMP_ID, EMP.EMPNO, ISNULL(EMP.EMP_FNAME, '') + ' ' + ISNULL(EMP.EMP_MNAME, '') + ' ' + " _
                      & " ISNULL(EMP.EMP_LNAME, '') AS EMP_NAME, ECT.ECT_DESCR, " _
                      & " REPLACE(CONVERT(VARCHAR(11), EMP.EMP_PROBTILL, 113), ' ', '/')  AS EMP_PROBTILL," _
                      & " REPLACE(CONVERT(VARCHAR(11), EMP.EMP_JOINDT, 113), ' ', '/') AS EMP_JOINDT" _
                      & " FROM EMPLOYEE_M AS EMP INNER JOIN EMPCATEGORY_M AS ECT " _
                      & " ON EMP.EMP_ECT_ID = ECT.ECT_ID WHERE     (EMP.EMP_bACTIVE = 1) AND (EMP.EMP_BSU_ID = '" & Session("sBsuid") & "')" _
                      & " AND (ISNULL(EMP.EMP_PROBTILL, '01-JAN-1900') <> '01-JAN-1900')"

                Else
                    str_Sql = " SELECT EMP.EMP_ID, EMP.EMPNO, ISNULL(EMP.EMP_FNAME, '') + ' ' + ISNULL(EMP.EMP_MNAME, '') + ' ' + " _
                    & " ISNULL(EMP.EMP_LNAME, '') AS EMP_NAME, ECT.ECT_DESCR, " _
                    & " REPLACE(CONVERT(VARCHAR(11), EMP.EMP_PROBTILL, 113), ' ', '/')  AS EMP_PROBTILL," _
                    & " REPLACE(CONVERT(VARCHAR(11), EMP.EMP_JOINDT, 113), ' ', '/') AS EMP_JOINDT" _
                    & " FROM EMPLOYEE_M AS EMP INNER JOIN EMPCATEGORY_M AS ECT " _
                    & " ON EMP.EMP_ECT_ID = ECT.ECT_ID WHERE     (EMP.EMP_bACTIVE = 1) AND (EMP.EMP_BSU_ID = '" & Session("sBsuid") & "')" _
                    & " AND (ISNULL(EMP.EMP_PROBTILL, '01-JAN-1900') <> '01-JAN-1900')" _
                    & " AND EMP.EMP_PROBTILL between'" & Date.Now.AddDays(Session("DocExpDays") * -1) & "' and  '" & Date.Now.AddDays(Session("DocExpDays")) & "' "
                End If

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                gvDocExpiry.DataSource = ds
                gvDocExpiry.DataBind()
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        gridbind()
    End Sub
End Class
