Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports PayrollFunctions

Partial Class Payroll_empDesignationChange
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'Dim smScriptManager As New ScriptManager
            'smScriptManager = Master.FindControl("ScriptManager1")
            'smScriptManager.EnablePageMethods = True
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtVisa.Attributes.Add("readonly", "readonly")
            txtEmp.Attributes.Add("readonly", "readonly")
            txtMoe.Attributes.Add("readonly", "readonly")
            txtEmpApprovalPol.Attributes.Add("readonly", "readonly")
            txtNVisa.Attributes.Add("readonly", "readonly")
            txtNEmp.Attributes.Add("readonly", "readonly")
            txtNMoe.Attributes.Add("readonly", "readonly")
            txtNEmpApprovalPol.Attributes.Add("readonly", "readonly")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P450015" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") <> "" Then

                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))

                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If


        End If
    End Sub




    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT EAT.EAT_ID, " _
            & " EAT.EAT_BSU_ID, EAT.EAT_EMP_ID, " _
            & " EAT.EAT_ELT_ID, EAT.EAT_DT, EAT.EAT_FLAG, " _
            & " EAT.EAT_ELV_ID, EAT.EAT_REMARKS," _
            & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+ " _
            & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
            & " FROM EMPATTENDANCE AS EAT INNER JOIN" _
            & " EMPLOYEE_M AS EM ON EAT.EAT_EMP_ID = EM.EMP_ID" _
            & " WHERE EAT.EAT_ID='" & p_Modifyid & "'"
            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'ddLeavetypeMonthly.DataBind()
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EAT_EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                'ddLeavetypeMonthly.SelectedIndex = -1
                'ddLeavetypeMonthly.Items.FindByValue(ds.Tables(0).Rows(0)("EAT_ELT_ID").ToString).Selected = True
                'txtCategory.Text = Format(CDate(ds.Tables(0).Rows(0)("EAT_DT")), "dd/MMM/yyyy")

                txtRemarks.Text = ds.Tables(0).Rows(0)("EAT_REMARKS").ToString
            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub




    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtVisa.Attributes.Add("readonly", "readonly")
        txtEmp.Attributes.Add("readonly", "readonly")
        txtMoe.Attributes.Add("readonly", "readonly")
        txtNVisa.Attributes.Add("readonly", "readonly")
        txtNEmp.Attributes.Add("readonly", "readonly")
        txtNMoe.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        txtEmpApprovalPol.Attributes.Add("readonly", "readonly")
        txtNEmpApprovalPol.Attributes.Add("readonly", "readonly")
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
        imgEmployee.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
    End Sub
    Protected Sub imgEmp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmp.Click
        Try
            setDefaultLeavePolicy()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub setDefaultLeavePolicy()
        Try
            Dim sqlStr As String
            Dim mLeaveTbl As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            sqlStr = "SELECT TOP 1 LPS_ID,LPS_DESCRIPTION FROM APPROVALPOLICY_H H INNER join APPROVALPOLICY_D D ON LPS_ID=LPD_LPS_ID AND LPD_BSU_ID='" & Session("sBsuid") & "' AND LPD_DES_ID ='" & h_Empcat.Value & "' WHERE LPS_bActive = 1 ORDER BY H.LPS_ID desc"
            mLeaveTbl = Mainclass.getDataTable(sqlStr, str_conn)
            If mLeaveTbl.Rows.Count > 0 Then
                txtNEmpApprovalPol.Text = mLeaveTbl.Rows(0).Item("LPS_DESCRIPTION")
                h_ApprovalPolicy.Value = mLeaveTbl.Rows(0).Item("LPS_ID")
            End If
        Catch ex As Exception

        End Try
    End Sub



    Sub clear_All()
        txtFrom.Text = ""
        txtRemarks.Text = ""
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        h_Empcat.Value = ""
        h_Visacat.Value = ""
        txtVisa.Text = ""
        txtEmp.Text = ""
        txtMoe.Text = ""
        txtNVisa.Text = ""
        txtNEmp.Text = ""
        txtNMoe.Text = ""
        txtEmpApprovalPol.Text = ""
        txtNEmpApprovalPol.Text = ""
        bindgrid()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            'ViewState("EDVISA_ID") 
            'ViewState("EDMOE_ID")  
            '    ViewState("EDEMP_ID") 
            If Not IsDate(txtFrom.Text) Then
                lblError.Text = "Invalid date"
                Exit Sub
            End If
            Dim retval As String = "1000"
            Dim str_update_master As String = ""

            If txtNVisa.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "33", h_Visacat.Value, _
                          txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EDVISA_ID"), False, stTrans)
                str_update_master = " EMP_VISA_DES_ID='" & h_Visacat.Value & "' "


            End If
            If txtNMoe.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "22", h_Moecat.Value, _
                  txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EDMOE_ID"), False, stTrans)
                If str_update_master = "" Then
                    str_update_master = " EMP_MOE_DES_ID='" & h_Moecat.Value & "' "
                Else
                    str_update_master = str_update_master & " ,EMP_MOE_DES_ID='" & h_Moecat.Value & "' "
                End If
            End If
            If txtNEmp.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "1", h_Empcat.Value, _
                  txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EDEMP_ID"), False, stTrans)
                If str_update_master = "" Then
                    str_update_master = " EMP_des_ID='" & h_Empcat.Value & "' "
                Else
                    str_update_master = str_update_master & ", EMP_DES_ID='" & h_Empcat.Value & "' "
                End If

            End If

            If txtNEmpApprovalPol.Text <> "" Then
                retval = SaveEMPTRANTYPE_TRN(Session("sbsuid"), h_Emp_No.Value, "1", h_ApprovalPolicy.Value, _
                  txtFrom.Text, Nothing, txtRemarks.Text.ToString, ViewState("EDLPS_ID"), False, stTrans)
                If str_update_master = "" Then
                    str_update_master = " EMP_LPS_ID='" & h_ApprovalPolicy.Value & "' "
                Else
                    str_update_master = str_update_master & ", EMP_LPS_ID='" & h_ApprovalPolicy.Value & "' "
                End If

            End If


            If retval = "0" Then
                Dim myCommand As New SqlCommand("UPDATE  EMPLOYEE_M SET " & str_update_master & "WHERE EMP_ID='" & h_Emp_No.Value & "'", objConn, stTrans)
                myCommand.ExecuteNonQuery()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_No.Value & "-" & h_Visacat.Value & "-" & txtFrom.Text & "-" & txtRemarks.Text.ToString, _
                  "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If

            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub




    'Private Function SaveEMPTRANTYPE_TRN(ByVal p_bsu_id As String, _
    'ByVal p_EMP_ID As String, ByVal p_EST_TTP_ID As String, ByVal p_EST_CODE As String, _
    ' ByVal p_EST_DTFROM As String, ByVal p_EST_REMARKS As String, _
    '  ByVal p_EST_OLDCODE As String, ByVal p_bFromMAsterForm As Boolean, ByVal p_stTrans As SqlTransaction) As String
    '    '@return_value = [dbo].[SaveEMPTRANTYPE_TRN]
    '    '@EST_EMP_ID = 999,
    '    '@EST_BSU_ID = N'XXXXXX',
    '    '@EST_TTP_ID = 1,

    '    '@EST_CODE = N'1',
    '    '@EST_DTFROM = N'12-DEC-2007',
    '    '@EST_DTTO = NULL,
    '    '@EST_REMARKS = N'VERUTHE',
    '    '@EST_OLDCODE = N'2',
    '    '@bFromMAsterForm = 0

    '    Try
    '        Dim pParms(9) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@EST_BSU_ID", SqlDbType.VarChar, 20)
    '        pParms(0).Value = p_bsu_id
    '        pParms(1) = New SqlClient.SqlParameter("@EST_EMP_ID", SqlDbType.Int)
    '        pParms(1).Value = p_EMP_ID
    '        pParms(2) = New SqlClient.SqlParameter("@EST_TTP_ID", SqlDbType.Int)
    '        pParms(2).Value = p_EST_TTP_ID
    '        pParms(3) = New SqlClient.SqlParameter("@EST_CODE", SqlDbType.VarChar, 20)
    '        pParms(3).Value = p_EST_CODE
    '        pParms(4) = New SqlClient.SqlParameter("@EST_DTFROM", SqlDbType.DateTime)
    '        pParms(4).Value = p_EST_DTFROM
    '        pParms(5) = New SqlClient.SqlParameter("@EST_DTTO", SqlDbType.DateTime)
    '        pParms(5).Value = System.DBNull.Value
    '        pParms(6) = New SqlClient.SqlParameter("@EST_REMARKS", SqlDbType.VarChar, 100)
    '        pParms(6).Value = p_EST_REMARKS
    '        pParms(7) = New SqlClient.SqlParameter("@EST_OLDCODE", SqlDbType.VarChar, 20)
    '        pParms(7).Value = p_EST_OLDCODE
    '        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
    '        pParms(8).Direction = ParameterDirection.ReturnValue
    '        pParms(9) = New SqlClient.SqlParameter("@bFromMAsterForm", SqlDbType.Bit)
    '        pParms(9).Value = p_bFromMAsterForm
    '        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '        Dim retval As Integer
    '        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPTRANTYPE_TRN", pParms)
    '        If pParms(8).Value = "0" Then
    '            SaveEMPTRANTYPE_TRN = pParms(8).Value
    '        Else
    '            SaveEMPTRANTYPE_TRN = pParms(8).Value
    '        End If
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '        SaveEMPTRANTYPE_TRN = "1000"
    '    End Try

    'End Function

 


 
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Sub setEditdata()
        'txtCategory.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        'imgCalendar.Enabled = True
        imgEmployee.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub
    Protected Sub imgEmployee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     EM.EMP_DES_ID, " _
            & " EM.EMP_FNAME, EM.EMP_VISA_DES_ID, " _
            & " EM.EMP_MOE_DES_ID, EDE.DES_DESCR AS EDEMP, " _
            & " EDM.DES_DESCR AS EDMOE, EDV.DES_DESCR AS EDVISA,EM.EMP_LPS_ID,LPS.LPS_DESCRIPTION AS LPS_DES" _
            & " FROM EMPLOYEE_M AS EM INNER JOIN" _
            & " EMPDESIGNATION_M AS EDE ON EM.EMP_DES_ID = EDE.DES_ID INNER JOIN" _
            & " EMPDESIGNATION_M AS EDM ON EM.EMP_MOE_DES_ID = EDM.DES_ID INNER JOIN" _
            & " EMPDESIGNATION_M AS EDV ON EM.EMP_VISA_DES_ID = EDV.DES_ID LEFT OUTER JOIN APPROVALPOLICY_H LPS ON EM.EMP_LPS_ID= LPS.LPS_ID" _
            & " WHERE     (EM.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EM.EMP_ID = '" & h_Emp_No.Value & "')"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtVisa.Text = ds.Tables(0).Rows(0)("EDVISA")
                txtMoe.Text = ds.Tables(0).Rows(0)("EDMOE").ToString
                txtEmp.Text = ds.Tables(0).Rows(0)("EDEMP").ToString
                txtEmpApprovalPol.Text = ds.Tables(0).Rows(0)("LPS_DES").ToString
                ViewState("EDVISA_ID") = ds.Tables(0).Rows(0)("EMP_VISA_DES_ID")
                ViewState("EDMOE_ID") = ds.Tables(0).Rows(0)("EMP_MOE_DES_ID")
                ViewState("EDEMP_ID") = ds.Tables(0).Rows(0)("EMP_DES_ID")
                ViewState("EDLPS_ID") = ds.Tables(0).Rows(0)("EMP_LPS_ID")
            Else

            End If
            bindgrid()
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub




    Sub bindgrid()

        Dim ds As New DataSet
        Dim str_Sql As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        str_Sql = "SELECT EST.EST_BSU_ID, TTP.TTP_DESCR," _
        & " EST.EST_EMP_ID, EST.EST_CODE, EST.EST_DTFROM, EST.EST_DTTO, " _
        & " EST.EST_REMARKS, EST.EST_OLDCODE, EDN.DES_DESCR AS NEWDES, " _
        & " EDO.DES_DESCR AS OLDDES FROM EMPTRANTYPE_M AS TTP RIGHT OUTER JOIN" _
        & " EMPTRANTYPE_TRN AS EST INNER JOIN EMPDESIGNATION_M AS EDN " _
        & " ON EST.EST_CODE = EDN.DES_ID INNER JOIN EMPDESIGNATION_M" _
        & " AS EDO ON EST.EST_OLDCODE = EDO.DES_ID ON TTP.TTP_ID = EST.EST_TTP_ID" _
        & " WHERE (EST.EST_EMP_ID = '" & h_Emp_No.Value & "') AND (EST.EST_BSU_ID = '" & Session("sBsuid") & "') " _
        & " AND (EST.EST_TTP_ID IN( 1,22,33)) ORDER BY EST.EST_TTP_ID"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        GridView1.DataSource = ds
        GridView1.DataBind()
    End Sub

    Protected Sub h_Emp_No_ValueChanged(sender As Object, e As EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     EM.EMP_DES_ID, " _
            & " EM.EMP_FNAME, EM.EMP_VISA_DES_ID, " _
            & " EM.EMP_MOE_DES_ID, EDE.DES_DESCR AS EDEMP, " _
            & " EDM.DES_DESCR AS EDMOE, EDV.DES_DESCR AS EDVISA,EM.EMP_LPS_ID,LPS.LPS_DESCRIPTION AS LPS_DES" _
            & " FROM EMPLOYEE_M AS EM INNER JOIN" _
            & " EMPDESIGNATION_M AS EDE ON EM.EMP_DES_ID = EDE.DES_ID INNER JOIN" _
            & " EMPDESIGNATION_M AS EDM ON EM.EMP_MOE_DES_ID = EDM.DES_ID INNER JOIN" _
            & " EMPDESIGNATION_M AS EDV ON EM.EMP_VISA_DES_ID = EDV.DES_ID LEFT OUTER JOIN APPROVALPOLICY_H LPS ON EM.EMP_LPS_ID= LPS.LPS_ID" _
            & " WHERE     (EM.EMP_BSU_ID = '" & Session("sBsuid") & "') AND (EM.EMP_ID = '" & h_Emp_No.Value & "')"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtVisa.Text = ds.Tables(0).Rows(0)("EDVISA")
                txtMoe.Text = ds.Tables(0).Rows(0)("EDMOE").ToString
                txtEmp.Text = ds.Tables(0).Rows(0)("EDEMP").ToString
                txtEmpApprovalPol.Text = ds.Tables(0).Rows(0)("LPS_DES").ToString
                ViewState("EDVISA_ID") = ds.Tables(0).Rows(0)("EMP_VISA_DES_ID")
                ViewState("EDMOE_ID") = ds.Tables(0).Rows(0)("EMP_MOE_DES_ID")
                ViewState("EDEMP_ID") = ds.Tables(0).Rows(0)("EMP_DES_ID")
                ViewState("EDLPS_ID") = ds.Tables(0).Rows(0)("EMP_LPS_ID")
            Else

            End If
            bindgrid()
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub
End Class
