Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports UtilityObj

Partial Class Students_UpdateOpeningLeaveBalance
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        If EMPIDs.Contains("||") Then
            Me.lblError.Text = "Kindly select only one employee at a time."
            Return False
        End If
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next

        'str_Sql = "select EMPNO as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        'gvEMPName.DataSource = ds
        'gvEMPName.DataBind()
        'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
        '    Return False
        'End If
        str_Sql = "select EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        Me.txtEMPNAME.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        GetEmployeeLeaveScheduleDetails()
        Return True
    End Function


    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub
    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave2)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnCancel)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnCancel2)

        If h_BSUID.Value = Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If

        'If Session("UpdateOpeningLeaveBalance_SaveState") = False Then
        'FillEmpNames(h_EMPID.Value)
        StoreEMPFilter()
        'End If
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnLoad)
        smScriptManager.RegisterPostBackControl(lnkDownload)
        If Page.IsPostBack = False Then

            Try
                Session("dtDt") = ""
                Session("dtDt") = CreateDataTableCarryForwardDays()
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    hfDate.Value = DateTime.Now
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    'btnSave.Attributes.Add("onclick", "hideButton()")
                    'btnSave2.Attributes.Add("onclick", "hideButton()")
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    'BindEmployees()
                    'btnSave2.Visible = False

                    'btnCancel2.Visible = False

                    'txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub



    Sub bindMonthPRESENT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2,ISNULL(BSU_bAPP_LEAVE_ABSENT,'False') AS bAPP_LEAVE_ABS " _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
                ViewState("bAPP_LEAVE_ABS") = ds.Tables(0).Rows(0)(2)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Function ValidateDate() As String
        'Try
        '    Dim CommStr As String = String.Empty
        '    Dim ErrorStatus As String = String.Empty
        '    CommStr = ""

        '    If txtDate.Text <> "" Then
        '        Dim strfDate As String = txtDate.Text
        '        Dim str_err As String = DateFunctions.checkdate(strfDate)
        '        If str_err <> "" Then
        '            ErrorStatus = "-1"
        '            CommStr = CommStr & "Attendance Date format is Invalid"
        '        Else
        '            txtDate.Text = strfDate
        '            Dim dateTime1 As String
        '            dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
        '            'check for the leap year date
        '            If Not IsDate(dateTime1) Then
        '                ErrorStatus = "-1"
        '                CommStr = CommStr & "Attendance Date format is Invalid"
        '            End If
        '        End If
        '    Else
        '        ErrorStatus = "-1"
        '        CommStr = CommStr & "Attendance Date required"

        '    End If

        '    If ErrorStatus <> "-1" Then
        '        Return "0"
        '    Else
        '        lblError.Text = CommStr
        '    End If


        '    Return ErrorStatus
        'Catch ex As Exception
        '    'UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
        '    Return "-1"
        'End Try

    End Function

    Private Function ValidateData() As Boolean
        Dim CarryFwDays As String
        For i As Integer = 0 To gvStudents.Rows.Count - 1
            Dim row As GridViewRow = gvStudents.Rows(i)
            CarryFwDays = DirectCast(row.FindControl("txtCarryFwdDays"), TextBox).Text
            If Not IsNumeric(CarryFwDays) Then
                Me.lblError.Text = "Some of the rows contains invalid carry forward days. Please enter numeric values for carry forward days."
                Return False
            End If
        Next
        Return True
    End Function

    Sub DisableControls()
        'For j As Integer = 0 To gvInfo.Rows.Count - 1
        '    Dim row As GridViewRow = gvInfo.Rows(j)
        '    Dim G_Stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
        '    If Session("Astud_ID_GROUP").Contains(G_Stud_ID) Then

        '        DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

        '        'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
        '        'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False

        '        DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
        '    End If
        'Next
    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer

        'Dim RAL_ID As String = String.Empty
        'Dim RAL_CREATEDDT As String = String.Empty
        'Dim RAL_CREATEDTM As String = String.Empty
        'Dim RAL_MODIFIEDDT As String = String.Empty
        'Dim RAL_MODIFIEDTM As String = String.Empty
        'Dim STR_XML As String = String.Empty
        'Dim RAL_ATTDT As String
        'Dim EMP_ID As String = ViewState("EMP_ID")
        'Dim RAL_BSU_ID As String = Session("sBsuid")
        ''Dim ACD_ID As String = ddlBusinessUnit.SelectedItem.Value
        'Dim ALS_STU_ID As String = String.Empty
        'Dim ALS_APD_ID As String = String.Empty
        'Dim ALS_REMARKS As String = String.Empty
        'Dim AppLeave As String = String.Empty
        ''Dim RAL_SGR_ID As String = ddlSubjectGroup.SelectedValue
        ''Dim RAL_PERIOD_ID As String = ddlPERIOD.SelectedValue

        'RAL_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)


        'Dim transaction As SqlTransaction

        'Using conn As SqlConnection = ConnectionManger.GetOASISConnection
        '    transaction = conn.BeginTransaction("SampleTransaction")
        '    Try

        '        Dim status As Integer
        '        For i As Integer = 0 To gvInfo.Rows.Count - 1
        '            Dim row As GridViewRow = gvInfo.Rows(i)

        '            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
        '            ALS_STU_ID = DirectCast(row.FindControl("lblStud_ID"), Label).Text
        '            ALS_REMARKS = DirectCast(row.FindControl("txtRemarks"), TextBox).Text
        '            AppLeave = DirectCast(row.FindControl("lblAppLeave"), Label).Text
        '            If (ddlStatus.SelectedIndex <> 0) Then
        '                If (AppLeave <> "APPROVED") Then
        '                    ALS_APD_ID = ddlStatus.SelectedItem.Value
        '                    STR_XML += String.Format("<STUDENT STUID='{0}' ALS_APD_ID='{1}' ALS_REMARKS='{2}'> </STUDENT>", ALS_STU_ID, ALS_APD_ID, ALS_REMARKS)
        '                End If
        '            End If
        '        Next


        '        If STR_XML <> "" Then
        '            STR_XML = "<STUDENTS>" + STR_XML + "</STUDENTS>"
        '        Else
        '            STR_XML = ""
        '        End If




        '        If ViewState("datamode") = "add" Then
        '            RAL_CREATEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        '            RAL_CREATEDTM = String.Format("{0:T}", DateTime.Now)

        '            status = AccessStudentClass.SAVEATT_LOG_ROOM(STR_XML, ACD_ID, RAL_ATTDT, _
        '                                          RAL_SGR_ID, RAL_ID, EMP_ID, RAL_BSU_ID, RAL_CREATEDDT, _
        '                                            RAL_CREATEDTM, RAL_MODIFIEDDT, RAL_MODIFIEDTM, RAL_PERIOD_ID, _
        '                                             ViewState("datamode"), transaction)


        '        ElseIf ViewState("datamode") = "edit" Then
        '            RAL_MODIFIEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        '            RAL_MODIFIEDTM = String.Format("{0:T}", DateTime.Now)


        '            status = AccessStudentClass.SAVEATT_LOG_ROOM(STR_XML, ACD_ID, RAL_ATTDT, _
        '                                          RAL_SGR_ID, ViewState("RAL_ID"), EMP_ID, RAL_BSU_ID, RAL_CREATEDDT, _
        '                                            RAL_CREATEDTM, RAL_MODIFIEDDT, RAL_MODIFIEDTM, RAL_PERIOD_ID, _
        '                                             ViewState("datamode"), transaction)


        '        End If



        '        ViewState("datamode") = "view"

        '        Session("dt_ATT_GROUP").Rows.Clear()

        '        'gvInfo.DataSource = Session("dt_ATT_GROUP")
        '        'gvInfo.DataBind()

        '        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '        Button3.Visible = False
        '        'btnCancel2.Visible = False
        '        'btnSave2.Visible = False
        '        calltransaction = "0"
        '        ResetControl()


        '    Catch ex As Exception
        '        calltransaction = "1"
        '        errorMessage = "Error Occured While Saving."
        '    Finally
        '        If calltransaction <> "0" Then
        '            UtilityObj.Errorlog(errorMessage)
        '            transaction.Rollback()
        '        Else
        '            errorMessage = ""
        '            transaction.Commit()
        '        End If
        '    End Try

        'End Using

    End Function

    Sub setControl()
        ' ddlEmployee.Enabled = False
    End Sub
    Sub ResetControl()
        'ddlEmployee.Enabled = True
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub gvStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudents.RowDataBound
        If e.Row.RowIndex = -1 Then Exit Sub
        Try
            Dim row As GridViewRow = e.Row
            Dim leavetype As String = CType(row.FindControl("lblEltId"), Label).Text
            If leavetype <> "AL" Then 'if not annual leave
                CType(row.FindControl("txtCarryFwdDays"), TextBox).Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveData()

        If Not ValidateData() Then Exit Sub

        Dim Els_Id As Integer, CarryFwDays As Integer
        Dim ELS_ELT_ID As String = ""

        Dim transaction As SqlTransaction

        Dim Conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand
        transaction = Conn.BeginTransaction("SampleTransaction")
        Dim Remarks As String = txtRemarks.Text
        Try

            For i As Integer = 0 To gvStudents.Rows.Count - 1
                Dim row As GridViewRow = gvStudents.Rows(i)
                Dim params(5) As SqlParameter
                params(0) = New SqlParameter("@ELS_ID", DirectCast(row.FindControl("lblElsId"), Label).Text)
                params(1) = New SqlParameter("@ELS_ELT_ID", DirectCast(row.FindControl("lblEltId"), Label).Text)
                params(2) = New SqlParameter("@CARRY_FWD_DAYS", CType(row.FindControl("txtCarryFwdDays"), TextBox).Text)
                params(3) = New SqlParameter("@User_Id", Session("sUsr_Id"))
                params(4) = New SqlParameter("@REMARKS", Remarks)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "dbo.UpdateEmployeeLeaveScheduleDetails", params)
            Next
            transaction.Commit()
            GetEmployeeLeaveScheduleDetails()
            'Me.gvStudents.DataSource = Nothing
            'Me.gvStudents.DataBind()
            Me.btnSave.Visible = False
            Me.btnCancel.Visible = False
            Me.lblError.Text = "Data updated successfully."
        Catch ex As Exception
            Me.lblError.Text = "Error occurred while updating the data."
        Finally

        End Try



    End Sub


    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Me.SaveData()
        btnCancel.Visible = True
    End Sub

    Private Sub GetEmployeeLeaveScheduleDetails()
        trHistory.Visible = True
        trRemarks.Visible = True
        trGridStudents.Visible = True
        trSave.Visible = True
        clear_details()
        Dim Conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand
        Dim ds As DataSet
        Dim Ads As DataSet


        If Me.h_EMPID.Value = Nothing Then Exit Sub

        Try

            Dim params(1) As SqlParameter
            Dim Audparams(1) As SqlParameter
            params(0) = New SqlParameter("@EMP_ID", Me.h_EMPID.Value)
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "[GetEmployeeLeaveScheduleDetails]", params(0))
            Me.gvStudents.DataSource = ds
            Session("UpdateOpeningLeaveBalance") = ds
            Me.gvStudents.DataBind()
            If Me.gvStudents.Rows.Count > 0 Then
                Me.btnSave.Visible = True
                Me.btnCancel.Visible = True
                Me.lblError.Text = Nothing
                tr2.Visible = True

                Audparams(0) = New SqlParameter("@EMP_ID", Me.h_EMPID.Value)
                Audparams(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                Ads = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "[dbo].[GetEmployeeLeaveScheduleDetailsAudit]", Audparams)
                Me.GVEmpLeaveAudit.DataSource = Ads
                Me.GVEmpLeaveAudit.DataBind()


            Else
                Me.btnSave.Visible = False
                Me.btnCancel.Visible = False
                tr2.Visible = False
                Me.lblError.Text = "No leave record found"
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while displaying employee leave schedule details."
        Finally

        End Try

    End Sub

    'Protected Sub ddlEmployee_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEmployee.SelectedIndexChanged
    '    Me.GetEmployeeLeaveScheduleDetails()
    'End Sub

    Protected Sub lnlbtnAddEMPID_Click(sender As Object, e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    'Protected Sub gvStudents_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudents.RowEditing
    '    Me.gvStudents.EditIndex = e.NewEditIndex
    '    If Not Session("UpdateOpeningLeaveBalance") Is Nothing Then
    '        Me.gvStudents.DataSource = CType(Session("UpdateOpeningLeaveBalance"), DataSet)
    '        Me.gvStudents.DataBind()
    '    End If
    'End Sub

    'Protected Sub gvStudents_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvStudents.RowUpdating
    '    If Not Session("UpdateOpeningLeaveBalance") Is Nothing Then
    '        Dim dt = CType(Session("UpdateOpeningLeaveBalance"), DataSet).Tables(0)
    '        Dim lbl As String = ""

    '        'Dim row As GridViewRow = Me.gvStudents.Rows(e.RowIndex)
    '        Dim row As GridViewRow = Me.gvStudents.Rows(Me.gvStudents.EditIndex)

    '        lbl = Convert.ToDecimal(CType(row.FindControl("txtCarryFwdDays_New"), TextBox).Text)

    '        e.NewValues("ELS_CARRYFWDDAYS") = lbl

    '        dt.Rows(row.DataItemIndex)("ELS_CARRYFWDDAYS") = Convert.ToDecimal(CType(row.FindControl("txtCarryFwdDays_New"), TextBox).Text)
    '        dt.AcceptChanges()

    '        Me.gvStudents.EditIndex = -1

    '        CType(Session("UpdateOpeningLeaveBalance"), DataSet).Tables.RemoveAt(0)
    '        CType(Session("UpdateOpeningLeaveBalance"), DataSet).Tables.Add(dt)
    '        CType(Session("UpdateOpeningLeaveBalance"), DataSet).AcceptChanges()
    '        Me.gvStudents.DataSource = CType(Session("UpdateOpeningLeaveBalance"), DataSet)
    '        Me.gvStudents.DataBind()
    '    End If

    'End Sub

    Protected Sub h_EMPID_ValueChanged(sender As Object, e As System.EventArgs) Handles h_EMPID.ValueChanged
        FillEmpNames(h_EMPID.Value)
        StoreEMPFilter()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub
    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ext = ".xls" Then
                '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                ' getdataExcel(strFileName)
                UpLoadDBF()
                trHistory.Visible = False
                trRemarks.Visible = False
                trGridStudents.Visible = False
                trSave.Visible = False
            Else
                lblError.Text = "Please upload .xls files only."
                Exit Sub
            End If

        Else
            lblError.Text = "File not uploaded"
            Exit Sub
        End If
    End Sub
    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 5)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(0).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(1).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next

            Dim strEarnDescr As String = ""
            ColName = xltable.Columns(4).ColumnName
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEarnDescr &= IIf(strEarnDescr <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                End If
            Next

            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                trBulkSave.Visible = False
                trGridview.Visible = False
                'Dim dt As DataTable = New DataTable
                'gvMonthE_D.DataSource = dt
                'gvMonthE_D.DataBind()
                Exit Sub
            End If

            Dim ValidStudent As New DataTable
            Dim ValidCode As New DataTable

            Dim mTable As New DataTable
            mTable = CreateDataTableCarryForwardDays()

            Dim rowId As Integer = 0

            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                ValidStudent = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in (" & strEmpNos & ") and emp_bsu_id='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)


                mRow = mTable.NewRow
                mRow("Id") = xlRow(0)
                mRow("EMPNO") = xlRow(1)
                mRow("EMP_Name") = xlRow(2)
                mRow("EMP_CarryFrdDays") = xlRow(3)
                mRow("REMARKS") = xlRow(4)
                mRow("EMP_BSU_ID") = Session("sBsuid")

                mRow("Delete_flag") = ""
                'mRow("EDD_EMP_ID")=
                If ValidStudent.Select("Empno='" & xlRow(1) & "'", "").Length = 0 Then
                    'mRow("NotValid") = "1"
                    'mRow("ErrorText") = "Invalid Employee No"
                    lblError.Text = "Invalid Employee No:" & xlRow(1) & ", Sl No. :" & xlRow(0) & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidStudent.Select("Empno='" & xlRow(1) & "'", "")(0)
                    mRow("EMP_ID") = drEmp("Emp_id")
                    mRow("EMP_Name") = drEmp("EMPNAME")
                    'mRow("EDD_EMP_ID") = ValidStudent.Rows(rowId).Item("Emp_id")
                    'mRow("EDD_EMP_Name") = ValidStudent.Rows(rowId).Item("EMPNAME")
                End If
                If System.Decimal.Truncate(xlRow(3)) <> xlRow(3) Then

                    lblError.Text = "Invalid Carry Forward Days :" & xlRow(3) & ", Sl No. :" & xlRow(0) & " for current Unit"
                    Exit Sub
                End If
                rowId = rowId + 1
                mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()

            gvMonthE_D.DataSource = mTable
            Session("dtDt") = mTable
            gvMonthE_D.DataBind()
            If gvMonthE_D.Rows.Count > 1 Then
                trBulkSave.Visible = True
                trGridview.Visible = True
            Else trBulkSave.Visible = False
                trGridview.Visible = False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub

    Public Function CreateDataTableCarryForwardDays() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.Int64"))
            Dim EMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim EMPNO As New DataColumn("EMPNO", System.Type.GetType("System.String"))
            Dim EMP_Name As New DataColumn("EMP_Name", System.Type.GetType("System.String"))
            Dim EMP_CarryFrdDays As New DataColumn("EMP_CarryFrdDays", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim EMP_BSU_ID As New DataColumn("EMP_BSU_ID", System.Type.GetType("System.String"))
            Dim Delete_flag As New DataColumn("Delete_flag", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(EMP_ID)
            dtDt.Columns.Add(EMPNO)
            dtDt.Columns.Add(EMP_Name)
            dtDt.Columns.Add(EMP_CarryFrdDays)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(EMP_BSU_ID)
            dtDt.Columns.Add(Delete_flag)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
    Protected Sub btnBulkSave_Click(sender As Object, e As EventArgs) Handles btnBulkSave.Click
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_connOasis)
        objConn.Open()
        Dim ret As Integer = 0
        Dim count As Integer = 0
        Dim iIndex As Integer

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If Session("dtDt").Rows.Count > 0 Then
                For iIndex = 0 To Session("dtDt").Rows.Count - 1
                    Dim dr As DataRow = Session("dtDt").Rows(iIndex)
                    count = count + 1
                    Dim SqlCmd As New SqlCommand("UpdateEmployeeLeaveScheduleDetails_Bulk", objConn, stTrans)
                    SqlCmd.CommandType = CommandType.StoredProcedure

                    SqlCmd.Parameters.AddWithValue("@Emp_Id", dr("EMP_ID"))
                    SqlCmd.Parameters.AddWithValue("@Bsu_Id", Session("sBsuid")) '("EMP_ID")
                    SqlCmd.Parameters.AddWithValue("@CarryFrdDays", dr("EMP_CarryFrdDays"))
                    SqlCmd.Parameters.AddWithValue("@USER_ID", Session("sUsr_name"))
                    SqlCmd.Parameters.AddWithValue("@REMARKS", dr("REMARKS"))
                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    ret = SqlCmd.Parameters("@ReturnValue").Value
                    If ret <> 0 Then
                        stTrans.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(ret)
                        Exit Sub
                    End If
                    'UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), dr("EMPLOYEE_ID"), "Update", Page.User.Identity.Name.ToString, Me.Page, "-Last Rejoin Date Updated directly.")

                Next

            End If

            If count = 0 Then
                stTrans.Rollback()
                lblError.Text = "Please enter required details to save."
                ret = 1000
            End If

            If ret = 0 Then
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage(ret)
                'Session("dtDt") = ""
                ViewState("datamode") = "none"
                clear_details()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage("1000")
            objConn.Close()
        End Try
    End Sub

    Private Sub clear_details()
        Session("dtDt") = ""
        gvMonthE_D.DataSource = Nothing
        gvMonthE_D.DataBind()
        trGridview.Visible = False
        trBulkSave.Visible = False
    End Sub
    Protected Sub lnkDownload_Click(sender As Object, e As EventArgs) Handles lnkDownload.Click
        Try
            Dim FilePath As String = "~\\Payroll\\DownloadSampleTemplates\\Update Opening Leave Balance.xls"
            Response.Clear()
            Response.ContentType = ContentType
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & Path.GetFileName(FilePath))
            Response.WriteFile(FilePath)
            Response.End()
        Catch ex As Exception
            'Errorlog(ex.Message, "lnkDownload")
        End Try
    End Sub
    Protected Sub btnCancelBulk_Click(sender As Object, e As EventArgs) Handles btnCancelBulk.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub
End Class
