<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDesignationChange.aspx.vb" Inherits="Payroll_empDesignationChange" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <script language="javascript" type="text/javascript">
        function GetEMPName() {
            
            var NameandCode;
            var result;
            // result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN","", sFeatures)
            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_employee");

            
   }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= h_Emp_No.ClientID%>', 'ValueChanged');
            }
        }



        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            

            // url='empShowMasterEmp.aspx?id='+mode;

            document.getElementById("<%=hf_SearchMode.ClientID%>").value = mode;
            var url = "empShowMasterEmp.aspx?id=" + mode;
            var oWnd = radopen(url, "pop_pagecode");
           

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var SearchMode = document.getElementById("<%=hf_SearchMode.ClientID%>").value

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                if (SearchMode == 'SD') {
                    document.getElementById("<%=txtNEmp.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_Empcat.ClientID %>").value = NameandCode[1];
                    return true;
                    //            PageMethods.setDefaultLeavePolicy();
                }
                if (SearchMode == 'ME') {
                    //            document.getElementById("<%=txtNVisa.ClientID %>").value=NameandCode[0];
                    //              document.getElementById("<%=h_Visacat.ClientID %>").value=NameandCode[1];
                    document.getElementById("<%=txtNMoe.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_Moecat.ClientID %>").value = NameandCode[1];
                }
                if (SearchMode == 'ML') {
                    //            document.getElementById("<%=txtNMoe.ClientID %>").value=NameandCode[0];
                    //              document.getElementById("<%=h_Moecat.ClientID %>").value=NameandCode[1];
                    document.getElementById("<%=txtNVisa.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_Visacat.ClientID %>").value = NameandCode[1];
                }
                else if (SearchMode == 'AP') {
                    // result = window.showModalDialog(url, "", sFeatures);
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtNEmpApprovalPol.ClientID %>").value = NameandCode[0];
              document.getElementById("<%=h_ApprovalPolicy.ClientID %>").value = NameandCode[1];
                }
        }
    }


    function clears(id) {
        if (id == '1')
            document.getElementById("<%=txtNVisa.ClientID %>").value = '';
       else if (id == '2')
           document.getElementById("<%=txtNMoe.ClientID %>").value = '';
       else if (id == '3')
           document.getElementById("<%=txtNEmp.ClientID %>").value = '';
}



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_pagecode" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Change Designation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" cellpadding="0" cellspacing="0" align="center" width="98%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                                CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">MOL Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtVisa" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">New 
                 Visa Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtNVisa" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgVisa" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('ML');return false;" />
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="javascript:clears('1');return false;">Clear</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">MOE Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtMoe" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">New 
                 MOE Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtNMoe" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgMoe" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('ME');return false;" />
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="javascript:clears('2');return false;">Clear</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Employee Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmp" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">New
             Employee Designation</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtNEmp" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgEmp" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('SD');return false;" />
                            <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="javascript:clears('3');return false;">Clear</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label" __designer:mapid="1fa">Leave
                        Approval Policy</span> <span class="text-danger" __designer:mapid="1fb">*</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpApprovalPol" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label" __designer:mapid="1fa">New Leave
                        Approval Policy</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtNEmpApprovalPol" runat="server" Wrap="False"></asp:TextBox>
                            <asp:ImageButton ID="Button4" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPageCode('AP');return false;" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select a Period</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>

                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                        <td align="left" width="20%">&nbsp;</td>
                        <td align="left" width="30%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Remarks</span></td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine" Width="472px"></asp:TextBox></td>
                    </tr>
                    <tr class="subheader_img" runat="server" id="tr_Deatailhead">
                        <td colspan="4" align="left">
                            <span class="field-label">Employee Designation History</span></td>
                    </tr>
                    <tr runat="server" id="tr_Deatails">
                        <td colspan="4" align="center" valign="top">
                            <asp:GridView ID="GridView1" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found">
                                <Columns>
                                    <asp:BoundField DataField="TTP_DESCR" HeaderText="Description" InsertVisible="False" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_DTFROM" HeaderText="From Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_DTTO" HeaderText="To Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EST_REMARKS" HeaderText="Remarks">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OLDDES" HeaderText="Old Designation">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NEWDES" HeaderText="New Designation">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" />
                                <%--<AlternatingRowStyle CssClass="griditem_alternative" />--%>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="h_Emp_No" runat="server" OnValueChanged="h_Emp_No_ValueChanged" />
    <asp:HiddenField ID="h_Empcat" runat="server" />
    <asp:HiddenField ID="h_Visacat" runat="server" />
    <asp:HiddenField ID="h_Moecat" runat="server" />
    <asp:HiddenField ID="h_ApprovalPolicy" runat="server" />
    <asp:HiddenField ID="hf_SearchMode" runat="server" />

</asp:Content>
