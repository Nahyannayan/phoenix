Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class SelIDDesc3Cols
    Inherits System.Web.UI.Page
    ''' <summary>
    ''' Version          Author             Date              Change
    ''' 1.1             Swapna              6-Feb-2014        To add city IATA Code
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
            If str_ID <> String.Empty Then
                Select Case (str_ID.ToUpper)

                    Case "CITY"
                        gvGroup.Columns(2).Visible = True
                        gvGroup.Columns(0).ItemStyle.Width = Unit.Pixel(30)
                        gvGroup.Columns(2).ItemStyle.Width = Unit.Pixel(30)
                        GridBindCity()

                End Select
            End If
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        'V1.2
        str_Sid_img = h_selected_menu_2.Value.Split("__")
        getid3(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

 
   
  

    Sub GridBindCity()
        Try
            Page.Title = "CITY"
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtIATA As String = String.Empty
            Dim str_filter_IATA As String = String.Empty
            Dim str_filter As StringBuilder = New StringBuilder
            Dim str_Sql As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("CIT_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("CIT_DESCR", str_Sid_search(0), str_txtName)
                ''column1
                '' IATA search
                ''name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCitCode")
                str_txtIATA = txtSearch.Text.Trim
                str_filter_IATA = set_search_filter("Isnull(CIT_IATA_CODE,'')", str_Sid_search(0), str_txtIATA)

            End If
            If str_filter_code <> String.Empty Or str_filter_name <> String.Empty Or str_filter_IATA <> String.Empty Then
                str_filter.Append(" WHERE 1 = 1 ")
                str_filter.Append(str_filter_code)
                str_filter.Append(str_filter_name)
                str_filter.Append(str_filter_IATA)
            End If
            str_Sql = "SELECT CIT_ID as ID, CIT_DESCR as DESCR, Isnull(CIT_IATA_CODE,'') as DESCR2 FROM CITY_M " & str_filter.ToString()
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode

            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            txtSearch = gvGroup.HeaderRow.FindControl("txtCitCode")
            txtSearch.Text = str_txtIATA

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "OASIS ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "City Name"
            lblheader = gvGroup.HeaderRow.FindControl("lblCitCode")
            lblheader.Text = "IATA CODE"
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

 

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
        If str_ID <> String.Empty Then
            Select Case (str_ID.ToUpper)
                
                Case "CITY"
                    gvGroup.Columns(2).Visible = True
                    gvGroup.Columns(0).ItemStyle.Width = Unit.Pixel(30)
                    gvGroup.Columns(2).ItemStyle.Width = Unit.Pixel(30)
                    GridBindCity()
                
            End Select
        End If
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
        If str_ID <> String.Empty Then
            Select Case (str_ID.ToUpper)
                
                Case "CITY"
                    gvGroup.Columns(2).Visible = True
                    gvGroup.Columns(0).ItemStyle.Width = Unit.Pixel(30)
                    gvGroup.Columns(2).ItemStyle.Width = Unit.Pixel(30)
                    GridBindCity()
                
            End Select
        End If
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim str_ID As String = IIf(Request.QueryString("ID") IsNot Nothing, Request.QueryString("ID"), String.Empty)
        If str_ID <> String.Empty Then
            Select Case (str_ID.ToUpper)
                Case "EMPGRADE"
               
                Case "CITY"
                    gvGroup.Columns(2).Visible = True
                    gvGroup.Columns(0).ItemStyle.Width = Unit.Pixel(30)
                    gvGroup.Columns(2).ItemStyle.Width = Unit.Pixel(30)
                    GridBindCity()
                
            End Select
        End If
    End Sub

    Protected Sub gvGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup.Load
        'gvGroup.Columns(2).Visible = False
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")

            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub
End Class

