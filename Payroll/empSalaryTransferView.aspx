<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empSalaryTransferView.aspx.vb" Inherits="Payroll_empSalaryTransferView" EnableViewState="true"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function ShowLeaveDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowEmpLeave.aspx?ela_id=" + id, "", sFeatures)
            return false;
        }

    </script>
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr valign="top">
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;
                                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            </td>
                        </tr>
                        <td class="matters" valign="top" align="left" width="20%">
                            <asp:HyperLink ID="hlAddNew" runat="server" Width="">Add New</asp:HyperLink>

                        </td>
                        <td class="matters" valign="top" align="right" width="10%">
                            <asp:Label ID="lblHeading" runat="server" Text="Signatory 1:" CssClass="field-label"></asp:Label>
                            </td>
                        <td width="30%">
                            <telerik:RadComboBox runat="server" ID="rcbSignatory" RenderMode="Lightweight" Width="100%"
                                EnableLoadOnDemand="true"
                                MarkFirstMatch="true" OnItemDataBound="rcbSignatory_ItemDataBound"
                                HighlightTemplatedItems="true">

                                <HeaderTemplate>

                                    <ul>

                                        <li class="col1">Name</li>

                                        <li class="col2">Designation</li>

                                    </ul>

                                </HeaderTemplate>

                                <ItemTemplate>

                                    <ul>



                                        <li class="col1">

                                            <%#DataBinder.Eval(Container.DataItem, "EMPNAME")%></li>

                                        <li class="col2">

                                            <%#DataBinder.Eval(Container.DataItem, "EMP_DES_DESCR")%></li>



                                    </ul>

                                </ItemTemplate>



                            </telerik:RadComboBox>
                            </td>
                        <td width="10%">
                            <asp:Label ID="lblHeading2" runat="server" Text="Signatory 2:" CssClass="field-label"></asp:Label>
                            </td>
                        <td width="30%">
                            <telerik:RadComboBox runat="server" ID="rcbSignatory2" RenderMode="Lightweight" Width="100%"
                                EnableLoadOnDemand="true"
                                MarkFirstMatch="true" OnItemDataBound="rcbSignatory2_ItemDataBound"
                                HighlightTemplatedItems="true">

                                <HeaderTemplate>

                                    <ul>

                                        <li class="col1">Name</li>

                                        <li class="col2">Designation</li>

                                    </ul>

                                </HeaderTemplate>

                                <ItemTemplate>

                                    <ul>



                                        <li class="col1">

                                            <%#DataBinder.Eval(Container.DataItem, "EMPNAME")%></li>

                                        <li class="col2">

                                            <%#DataBinder.Eval(Container.DataItem, "EMP_DES_DESCR")%></li>



                                    </ul>

                                </ItemTemplate>



                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>

                <table width="100%">

                    <tr>
                        <td align="center" valign="top" class="matters">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"  CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="25">
                                <Columns>
                                    <asp:TemplateField HeaderText="Docno" SortExpression="EBT_REFDOCNO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("EBT_REFDOCNO") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EBT_OURBANK_ACT_ID" HeaderText="Bank" SortExpression="EBT_OURBANK_ACT_ID"></asp:BoundField>
                                    <asp:BoundField DataField="BANK" HeaderText="Bank" SortExpression="BANK"></asp:BoundField>
                                    <asp:BoundField DataField="AMOUNYT" HeaderText="Amount" ReadOnly="True" SortExpression="AMOUNYT"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Date" SortExpression="EBT_DATE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("EBT_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ESD_BSU_ID" HeaderText="ESD_BSU_ID" SortExpression="ESD_BSU_ID"
                                        Visible="False"></asp:BoundField>
                                    <asp:BoundField DataField="EBT_NARRATION" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Export">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbExpBankTransfer" runat="server" OnClick="lbExportBankTransfer_Click">Export</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transfer Letter">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbBankTransfer" runat="server" OnClick="lbBankTransfer_Click">Bank Transfer Letter</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print Voucher">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbVoucher" runat="server" OnClick="lbVoucher_Click">Print Voucher</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" SortExpression="EBT_IBANNo" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIBAN" runat="server" Text='<%# Bind("ACT_IBAN_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
