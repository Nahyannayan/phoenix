Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Payroll_empSalaryFreezeApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property FreezeDetail() As DataRow
        Get
            If Not ViewState("FreezeDetail") Is Nothing And ViewState("FreezeDetail").rows.count > 0 Then
                Return ViewState("FreezeDetail").rows(0)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataRow)
            ViewState("FreezeDetail") = value.Table
        End Set
    End Property
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                FillPayYearPayMonth()
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P130048" And ViewState("MainMnu_code") <> "P130198") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If ViewState("MainMnu_code") = "P130048" Then
                    lblTitle.Text = "Forward Salary For Approval"
                    btnSend.Text = "Forward"
                ElseIf ViewState("MainMnu_code") = "P130198" Then
                    lblTitle.Text = "Approve Salary Detail"
                    btnSend.Text = "Approve"
                End If
                h_BSU_ID.Value = Session("sBsuid")
                If Request.QueryString("viewid") <> "" Then
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    SetDataMode("view")
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                    txtTrDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Sub FillPayYearPayMonth()
        Try
            Dim lst(12) As ListItem
            lst(0) = New ListItem("January", 1)
            lst(1) = New ListItem("February", 2)
            lst(2) = New ListItem("March", 3)
            lst(3) = New ListItem("April", 4)
            lst(4) = New ListItem("May", 5)
            lst(5) = New ListItem("June", 6)
            lst(6) = New ListItem("July", 7)
            lst(7) = New ListItem("August", 8)
            lst(8) = New ListItem("September", 9)
            lst(9) = New ListItem("October", 10)
            lst(10) = New ListItem("November", 11)
            lst(11) = New ListItem("December", 12)
            For i As Integer = 0 To 11
                ddlPayMonth.Items.Add(lst(i))
            Next
            Dim iyear As Integer = Session("BSU_PAYYEAR")
            For i As Integer = iyear - 2 To iyear + 2
                ddlPayYear.Items.Add(i.ToString())
            Next
        Catch ex As Exception

        End Try
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            FreezeDetail = GetSalaryFreezeDetail(p_Modifyid)
            If FreezeDetail Is Nothing Then Exit Sub
            h_SFH_ID.Value = FreezeDetail("SFH_ID").ToString
            If IsDate(FreezeDetail("SFH_ENTEREDDT")) Then
                txtTrDate.Text = Format(FreezeDetail("SFH_ENTEREDDT"), "dd/MMM/yyyy")
            Else
                txtTrDate.Text = ""
            End If
            ddlPayMonth.SelectedValue = FreezeDetail("SFH_MONTH").ToString
            ddlPayYear.SelectedValue = FreezeDetail("SFH_YEAR").ToString
            txtRemarks.Text = FreezeDetail("SFH_REMARKS").ToString
            txtLoggedInuser.Text = FreezeDetail("SFH_ENTEREDBY").ToString
            txtApprovedBy.Text = FreezeDetail("SFH_APPROVEDBY").ToString
            txtApproverRemarks.Text = FreezeDetail("SFH_APPROVERREMARKS").ToString

            If FreezeDetail("SFH_bApproved") Is DBNull.Value Then
                h_Approved.Value = 0
            Else
                h_Approved.Value = Val(IIf(FreezeDetail("SFH_bApproved"), 1, 0))
            End If
            If FreezeDetail("SFH_bForwarded") Is DBNull.Value Then
                h_Forwarded.Value = 0
            Else
                h_Forwarded.Value = Val(IIf(FreezeDetail("SFH_bForwarded"), 1, 0))
            End If
            txtApproverRemarks.Text = FreezeDetail("SFH_APPROVERREMARKS").ToString
            txtAmount.Text = Convert.ToDouble(Val(FreezeDetail("SFD_EARNED").ToString)).ToString("####0.00")
            txtBSUName.Text = Mainclass.GetBSUName(h_BSU_ID.Value)
            If ViewState("datamode") <> "add" Then
                LoadSummary()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        If (ViewState("datamode") = "add" Or Val(h_Approved.Value) = "0") And ViewState("MainMnu_code") <> "P130198" Then
            trApprovedBy.Visible = False
            trApproverRemarks.Visible = False
        Else
            trApprovedBy.Visible = True
            trApproverRemarks.Visible = True
        End If
        btnEdit.Visible = mDisable
        btnSave.Visible = Not mDisable
        ' btnSend.Visible = Not mDisable
        btnAdd.Visible = ViewState("MainMnu_code") <> "P130198"
        txtTrDate.Enabled = Not mDisable And ViewState("MainMnu_code") <> "P130198"
        ddlPayMonth.Enabled = Not mDisable And ViewState("MainMnu_code") <> "P130198"
        ddlPayYear.Enabled = Not mDisable And ViewState("MainMnu_code") <> "P130198"
        txtRemarks.Enabled = Not mDisable And ViewState("MainMnu_code") <> "P130198"
        txtApproverRemarks.Enabled = (ViewState("MainMnu_code") = "P130198" And Val(h_Forwarded.Value) = 1) And Val(h_Approved.Value) = 0
        mDisable = Val(h_Approved.Value) = 1 Or (ViewState("MainMnu_code") = "P130048" And Val(h_Forwarded.Value) = 1)
        btnSave.Enabled = Not mDisable
        btnEdit.Enabled = Not mDisable
        btnSend.Enabled = Not mDisable
        btnLoadData.Enabled = Not mDisable
        btnDelete.Enabled = Not mDisable And ViewState("MainMnu_code") <> "P130198"
    End Sub
    Sub clear_All()
        h_SFH_ID.Value = ""
        txtTrDate.Text = Format(Now.Date, "dd/MMM/yyyy")
        ddlPayMonth.SelectedValue = Now.Month
        ddlPayYear.SelectedValue = Now.Year
        txtRemarks.Text = ""
        txtApproverRemarks.Text = ""
        txtApprovedBy.Text = ""
        txtLoggedInuser.Text = Session("sUsr_name")
        h_Approved.Value = ""
        h_Forwarded.Value = ""
        GvSalaryDetail.DataSource = Nothing
        GvSalaryDetail.DataBind()
        gvSalarySummary.DataSource = Nothing
        gvSalarySummary.DataBind()
        txtAmount.Text = "0.00"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            retval = DeleteSalaryFreezeDetail(h_SFH_ID.Value)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Deleted Successfully !!!!"
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = retval
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub SaveData(ByVal SendStatus As Int16)
        Try
            Dim Trandate As String = txtTrDate.Text.Trim
            If Not IsDate(Trandate) Then
                lblError.Text = "Invalid Transaction Date !!!!"
                Exit Sub
            End If
            'If CDate(Trandate).Month <> ddlPayMonth.SelectedValue Or CDate(Trandate).Year <> ddlPayYear.SelectedValue Then
            '    lblError.Text = "Selected Transaction date does not belong to the month !!!!"
            '    Exit Sub
            'End If

            If CheckIfAlreadyFreezed(h_BSU_ID.Value, ddlPayMonth.SelectedValue, ddlPayYear.SelectedValue, Val(h_SFH_ID.Value)) Then
                lblError.Text = "The Month is already freezed"
                Exit Sub
            End If

            If FreezeDetail Is Nothing Then Exit Sub
            If ViewState("datamode") = "add" Then
                FreezeDetail("SFH_ID") = 0
                h_SFH_ID.Value = 0
            Else
                FreezeDetail("SFH_ID") = h_SFH_ID.Value
            End If
            FreezeDetail("SFH_BSU_ID") = h_BSU_ID.Value
            FreezeDetail("SFH_ENTEREDBY") = Session("sUsr_name")
            FreezeDetail("SFH_MONTH") = ddlPayMonth.SelectedValue
            FreezeDetail("SFH_YEAR") = ddlPayYear.SelectedValue
            FreezeDetail("SFH_REMARKS") = txtRemarks.Text
            FreezeDetail("SFH_ENTEREDDT") = Trandate
            FreezeDetail("SFH_APPROVERREMARKS") = txtApproverRemarks.Text
            FreezeDetail("SFH_ENTEREDDT") = Trandate
            Dim retval As String
            Dim SFH_ID As String = ""
            retval = SaveSalaryFreezeDetail(FreezeDetail, SFH_ID, SendStatus)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, h_SFH_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
                Select Case SendStatus
                    Case 0
                        lblError.Text = "Data Saved Successfully !!!"
                    Case 1
                        lblError.Text = "Data Forwarded Successfully !!!"
                    Case 2
                        lblError.Text = "Data Approved Successfully !!!"
                End Select
                h_SFH_ID.Value = SFH_ID
                setModifyHeader(SFH_ID)
                SetDataMode("view")
            Else
                lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ViewState("MainMnu_code") = "P130048" Then
            SaveData(0)
        ElseIf ViewState("MainMnu_code") = "P130198" Then
            SaveData(1)
        End If
    End Sub
    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        If ViewState("MainMnu_code") = "P130048" Then
            SaveData(1)
        ElseIf ViewState("MainMnu_code") = "P130198" Then
            SaveData(2)
        End If
    End Sub
    Public Shared Function GetSalaryFreezeDetail(ByVal SFH_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SFH_ID", SFH_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("GetSalaryFreezeDetail", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function SaveSalaryFreezeDetail(ByVal FreezeDetail As DataRow, ByRef SFH_ID As String, ByVal SendStatus As Int16) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(9) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SFH_ID", FreezeDetail("SFH_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SFH_BSU_ID", FreezeDetail("SFH_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SFH_MONTH", FreezeDetail("SFH_MONTH"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SFH_YEAR", FreezeDetail("SFH_YEAR"), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SFH_REMARKS", FreezeDetail("SFH_REMARKS"), SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@SFH_ENTEREDDT", FreezeDetail("SFH_ENTEREDDT"), SqlDbType.DateTime)
            sqlParam(6) = Mainclass.CreateSqlParameter("@SFH_ENTEREDBY", FreezeDetail("SFH_ENTEREDBY"), SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@SFH_APPROVERREMARKS", FreezeDetail("SFH_APPROVERREMARKS"), SqlDbType.VarChar)
            sqlParam(8) = Mainclass.CreateSqlParameter("@SENDSTATUS", SendStatus, SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "SaveSalaryFreezeDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(9).Value = "" Then
                SaveSalaryFreezeDetail = ""
                SFH_ID = sqlParam(0).Value
            Else
                SaveSalaryFreezeDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(9).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    
    Public Shared Function DeleteSalaryFreezeDetail(ByVal SFH_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SFH_ID", SFH_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "DeleteSalaryFreezeDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteSalaryFreezeDetail = ""
            Else
                DeleteSalaryFreezeDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function CheckIfAlreadyFreezed(ByVal BSU_ID As String, ByVal Month As String, ByVal Year As String, ByVal CurrID As String) As Boolean
        Try
            Dim sql As String
            sql = "select count(*) from SALARY_FREEZE_H where SFH_BSU_ID='" & BSU_ID & "' AND SFH_MONTH='" & Month & "' and SFH_YEAR='" & Year & "'"
            sql &= " AND SFH_ID not in (0, " & CurrID & ")"
            If Mainclass.getDataValue(sql, "OASISConnectionString") > 0 Then
                CheckIfAlreadyFreezed = True
            Else
                CheckIfAlreadyFreezed = False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            CheckIfAlreadyFreezed = False
            Throw ex
        End Try
    End Function
    Private Sub LoadSummary()
        Try
            HideDetails()
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ESD_MONTH", ddlPayMonth.SelectedValue, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ESD_YEAR", ddlPayYear.SelectedValue, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim mDatatable As New DataTable
            mDatatable = Mainclass.getDataTable("GetEmployeeSalarySummary", sqlParam, str_conn)
            If Not mDatatable Is Nothing Then
                gvSalarySummary.DataSource = mDatatable
                gvSalarySummary.DataBind()
                If mDatatable.Rows.Count = 0 Then
                    lblError.Text = "No Salary processed on this month."
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Sub
    Protected Sub btnLoadData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadData.Click
        If CheckIfAlreadyFreezed(h_BSU_ID.Value, ddlPayMonth.SelectedValue, ddlPayYear.SelectedValue, Val(h_SFH_ID.Value)) Then
            lblError.Text = "The Month is already freezed"
            gvSalarySummary.DataSource = Nothing
            gvSalarySummary.DataBind()
            Exit Sub
        End If
        LoadSummary()
    End Sub
    Protected Sub lnkViewDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim gvCell As DataControlFieldCell
            Dim gvRow As GridViewRow
            For Each gvRow In gvSalarySummary.Rows
                gvRow.BackColor = Nothing
            Next
            gvRow = Nothing
            gvCell = sender.Parent
            If gvCell Is Nothing Then Exit Sub
            gvRow = gvCell.Parent
            If gvRow Is Nothing Then Exit Sub
            Dim lblECTID, lblMonth, lblYear As Label
            lblECTID = gvRow.FindControl("lblECTID")
            lblMonth = gvRow.FindControl("lblMonth")
            lblYear = gvRow.FindControl("lblYear")
            If lblECTID Is Nothing Or lblMonth Is Nothing Or lblYear Is Nothing Then Exit Sub
            trSalaryDetail.Visible = True
            trHideDetail.Visible = True
            gvRow.BackColor = Drawing.Color.Pink
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(4) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ESD_MONTH", lblMonth.Text, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ESD_YEAR", lblYear.Text, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ECT_ID", lblECTID.Text, SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim mDatatable As New DataTable
            mDatatable = Mainclass.getDataTable("GetEmployeeSalarySummary", sqlParam, str_conn)
            If Not mDatatable Is Nothing Then
                GvSalaryDetail.DataSource = mDatatable
                GvSalaryDetail.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Sub
    Private Sub HideDetails()
        Try
            Dim gvRow As GridViewRow
            For Each gvRow In gvSalarySummary.Rows
                gvRow.BackColor = Nothing
            Next
            trSalaryDetail.Visible = False
            trSalaryRecon.Visible = False
            trHideDetail.Visible = False
            GvSalaryDetail.DataSource = Nothing
            GvSalaryDetail.DataBind()
            gvSalaryRecon.DataSource = Nothing
            gvSalaryRecon.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Sub
    Protected Sub btnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        HideDetails()
    End Sub

    Protected Sub btnRecon_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecon.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ESD_MONTH", ddlPayMonth.SelectedValue, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ESD_YEAR", ddlPayYear.SelectedValue, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim mDatatable As New DataTable
            mDatatable = Mainclass.getDataTable("GetEmployeeSalaryRecon", sqlParam, str_conn)
            trSalaryRecon.Visible = True
            trHideDetail.Visible = True
            If Not mDatatable Is Nothing Then
                gvSalaryRecon.DataSource = mDatatable
                gvSalaryRecon.DataBind()
                If mDatatable.Rows.Count = 0 Then
                    lblError.Text = "No Salary processed on this month."
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Sub
End Class
