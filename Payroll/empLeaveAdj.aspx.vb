Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports UtilityObj

Partial Class Payroll_empLeaveAdj
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim EAH_PAYYEAR As String
    Dim EAH_PAYMONTH As String
    Dim EAH_bADJADD As Boolean
    Dim EAH_ELT_ID As String
    'Version        Date        Author      Chenge
    '1.1            22-Jun-2011 Swapna      To reset from date to 1st day for pay month and pay year
    '1.2            15-Dec-2016 Swapna      To list only leaves in leave type drop down-Ticket 53567 correction


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P130030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtEmpName.Attributes.Add("readonly", "readonly")
                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        btnEmp_Name.Visible = False
                        Using LeaveAdj_IDreader As SqlDataReader = AccessPayrollClass.GetLeaveAdj_ID(ViewState("viewid"))
                            While LeaveAdj_IDreader.Read
                                'handle the null value returned from the reader incase  convert.tostring
                                hfEmp_ID.Value = Convert.ToString(LeaveAdj_IDreader("EMP_ID"))
                                txtEmpName.Text = Convert.ToString(LeaveAdj_IDreader("EName"))
                                txtFrom_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((LeaveAdj_IDreader("DTFROM"))))
                                txtTo_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((LeaveAdj_IDreader("DTTO"))))
                                EAH_PAYYEAR = Convert.ToString(LeaveAdj_IDreader("PAYYEAR"))
                                EAH_PAYMONTH = Convert.ToString(LeaveAdj_IDreader("PAYMONTH"))
                                EAH_ELT_ID = Convert.ToString(LeaveAdj_IDreader("ELT_ID"))
                                txtRemark.Text = Convert.ToString(LeaveAdj_IDreader("REMARKS"))
                                EAH_bADJADD = Convert.ToBoolean(LeaveAdj_IDreader("AdjAdd"))
                            End While
                            'clear of the resource end using
                        End Using
                    ElseIf ViewState("datamode") = "add" Then

                    End If
                    '----------create a list item to bind the years or  based on EAH_ID  record-------------
                    Dim totyear, i As Integer
                    ' Session("BSU_PAYMONTH")
                    Dim startYear As Integer
                    totyear = Session("BSU_PAYYEAR") 'AccessPayrollClass.GetMax_Min_Year(minyear) 'class for getting the total years
                    startYear = totyear - 20
                    Dim liYear As ListItem
                    ddlPayYear.Items.Clear()
                    For i = 0 To 40
                        liYear = New ListItem(startYear + i, startYear + i)
                        ddlPayYear.Items.Add(liYear)
                        'if  new record needs to be added
                        If ViewState("datamode") = "add" Then
                            Try
                                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                                Dim str_Sql As String
                                str_Sql = "SELECT BSU_ID," _
                                    & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                                    & " FROM  BUSINESSUNIT_M " _
                                    & " where BSU_ID='" & Session("sBsuid") & "'"

                                Dim ds As New DataSet
                                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                                If ds.Tables(0).Rows.Count > 0 Then
                                    'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                                    'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                                    ddlPayMonth.SelectedIndex = -1
                                    ddlPayYear.SelectedIndex = -1
                                    ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                                    ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True

                                Else
                                End If
                            Catch ex As Exception

                            End Try

                            'if record is based on the ID
                        Else
                            If ddlPayYear.Items(i).Value = EAH_PAYYEAR Then
                                ddlPayYear.SelectedIndex = i
                            End If
                        End If
                    Next
                    ' --- Commented and replaced with methode - bindevents // 
                    '----------create a list item to bind Leave Type-------------
                    'Using leaveTypeReader As SqlDataReader = AccessPayrollClass.GetOnlyLeaveTypes() 'V1.2 AccessPayrollClass.GetLeaveType() Ticket 53567   - Leave adjustment to list only leave types.
                    '    Dim liLeave As ListItem
                    '    ddlLtype.Items.Clear()
                    '    'check if it return rows or not
                    '    If leaveTypeReader.HasRows = True Then
                    '        While leaveTypeReader.Read
                    '            liLeave = New ListItem(leaveTypeReader("ELT_DESCR"), leaveTypeReader("ELT_ID"))
                    '            'adding listitems into the dropdownlist
                    '            ddlLtype.Items.Add(liLeave)
                    '        End While
                    '    End If
                    'End Using
                    '----- End of Comment
                    bindevents(Session("sBsuid"), Session("sUsr_name"))


                    If ViewState("datamode") = "view" Then
                        'keep loop until you get the counter for the EAH_ID Leave type into  the SelectedIndex
                        Dim ItemTypeCounter As Integer
                        For ItemTypeCounter = 0 To ddlLtype.Items.Count - 1
                            If ddlLtype.Items(ItemTypeCounter).Value = EAH_ELT_ID Then
                                ddlLtype.SelectedIndex = ItemTypeCounter
                            End If
                        Next
                        'Select the leave type  based on EAH_ID  record
                        If EAH_bADJADD = True Then
                            ddlAdjType.SelectedIndex = 0
                        Else
                            ddlAdjType.SelectedIndex = 1
                        End If
                    End If
                    If ViewState("datamode") <> "add" Then
                        '---loop & make default paymonth selected or based on EAH_ID  record
                        Dim MonthCount As Integer = ddlPayMonth.Items.Count
                        Dim j As Integer
                        Do While j <= MonthCount - 1
                            If ddlPayMonth.Items(j).Value = EAH_PAYMONTH Then
                                ddlPayMonth.SelectedIndex = j
                            End If
                            j = j + 1
                        Loop
                    End If
                End If
                If Page.IsPostBack = False Then
                    If ViewState("datamode") = "add" Then
                        'ddlLtype.SelectedItem.Value = "LOP"
                        ddlLtype.Items.FindByValue("LOP").Selected = True
                        'lblError.Text = ddlLtype.SelectedItem.Text
                    End If
                End If


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub
    Sub bindevents(ByVal bsuid As String, ByVal usrname As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "exec GetBSULeaveTypes '" & bsuid & "','" & usrname & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlLtype.DataSource = ds.Tables(0)
            ddlLtype.DataTextField = "ELT_DESCR"
            ddlLtype.DataValueField = "ELT_ID"
            ddlLtype.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then

            Dim dtPaydate As Date = New Date(ddlPayYear.SelectedValue, ddlPayMonth.SelectedValue, 1)

            If CDate(txtFrom_date.Text) > CDate(txtTo_date.Text) Then
                lblError.Text = "From Date should be less than To Date"
                Return
            End If

            If CDate(txtTo_date.Text) >= dtPaydate Then
                lblError.Text = "Dates should be less than Pay Term"
                Return
            End If

            Dim EAH_ID As Integer = 0
            Dim EAH_EMP_ID As Integer = hfEmp_ID.Value

            Dim EAH_BSU_ID As String = Session("sBsuid")

            Dim EAH_DTFROM As Date = txtFrom_date.Text
            Dim EAH_DTTO As Date = txtTo_date.Text
            Dim EAH_REMARKS As String = txtRemark.Text

            EAH_ELT_ID = ddlLtype.SelectedValue
            EAH_PAYYEAR = ddlPayYear.Text
            EAH_PAYMONTH = ddlPayMonth.SelectedValue

            If ddlAdjType.SelectedValue = 0 Then
                EAH_bADJADD = True
            Else
                EAH_bADJADD = False
            End If

            Dim bEdit As Boolean = False
            Dim status As Integer
            Dim transaction As SqlTransaction

            If ViewState("datamode") = "add" Then
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into EMPLVLADJUSTMENT_H & EMPLVLADJUSTMENT_D
                        status = AccessPayrollClass.SaveEMPLVLADJUSTMENT(EAH_ID, EAH_EMP_ID, EAH_BSU_ID, EAH_DTFROM, EAH_DTTO, _
                                EAH_PAYYEAR, EAH_PAYMONTH, EAH_ELT_ID, EAH_REMARKS, EAH_bADJADD, bEdit, transaction)

                        'throw error if insert to the EMPLVLADJUSTMENT_H & EMPLVLADJUSTMENT_D is not Successfully

                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If

                        'insert record for the last month

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, EAH_EMP_ID & "-" & EAH_PAYMONTH & "-" & EAH_PAYYEAR, "Insert", Page.User.Identity.Name.ToString)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        lblError.Text = "Record Inserted Successfully"

                        ViewState("datamode") = "none"

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        ' Call clearRecords()
                        ClearTexts()  'V1.1
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Inserted"

                    End Try
                End Using

            ElseIf ViewState("datamode") = "edit" Then
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into EMPLVLADJUSTMENT_H & EMPLVLADJUSTMENT_D
                        EAH_ID = ViewState("viewid")
                        status = AccessPayrollClass.SaveEMPLVLADJUSTMENT(EAH_ID, EAH_EMP_ID, EAH_BSU_ID, EAH_DTFROM, EAH_DTTO, _
                                EAH_PAYYEAR, EAH_PAYMONTH, EAH_ELT_ID, EAH_REMARKS, EAH_bADJADD, bEdit, transaction)

                        'throw error if insert to the EMPLVLADJUSTMENT_H & EMPLVLADJUSTMENT_D is not Successfully

                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If

                        'insert record for the last month

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, EAH_ID, "edit", Page.User.Identity.Name.ToString)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        lblError.Text = "Record Updated Successfully"

                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        'Call clearRecords()
                        ClearTexts() 'V1.1
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Updated"

                    End Try
                End Using

            End If


        End If
    End Sub

    Protected Sub txtFrom_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom_date.TextChanged
        Dim strfDate As String = txtFrom_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFrom_date.Text = strfDate
        End If
    End Sub

    Protected Sub txtTo_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo_date.TextChanged
        Dim strfDate As String = txtTo_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtTo_date.Text = strfDate
        End If
    End Sub

    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False

        Dim DateTime2 As Date
        Dim dateTime1 As Date
        Try
            DateTime2 = Date.ParseExact(txtTo_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1 Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(dateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try

    End Sub
    'V1.1 addition
    Sub ClearTexts()
        hfEmp_ID.Value = ""
        txtEmpName.Text = ""

        txtFrom_date.Text = "01/" & MonthName(Session("BSU_PAYMONTH"), True) & "/" & Session("BSU_PAYYEAR")
        txtTo_date.Text = ""
        txtRemark.Text = ""
        ViewState("viewid") = 0
        ' ddlLtype.SelectedIndex = 0
        ddlAdjType.SelectedIndex = 0
    End Sub
    Sub clearRecords()
        hfEmp_ID.Value = ""
        txtEmpName.Text = ""

        txtFrom_date.Text = UtilityObj.GetDiplayDate()
        txtTo_date.Text = ""
        txtRemark.Text = ""

        ddlLtype.SelectedIndex = 0
        ViewState("viewid") = 0

        Dim YearCount As Integer = ddlPayYear.Items.Count
        Dim i As Integer
        Do While i <= YearCount - 1
            If ddlPayYear.Items(i).Value = Session("F_YEAR") Then
                ddlPayYear.SelectedIndex = i
            End If
            i = i + 1
        Loop

        Dim MonthCount As Integer = ddlPayMonth.Items.Count
        Dim j As Integer
        Do While j <= MonthCount - 1
            If ddlPayMonth.Items(j).Value = Session("BSU_PAYMONTH") Then
                ddlPayMonth.SelectedIndex = j
            End If
            j = j + 1
        Loop


        ddlAdjType.SelectedIndex = 0
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try


            ViewState("datamode") = "add"
            'Call clearRecords()
            ClearTexts() '--V1.1

            'ddlLtype.SelectedValue = "LOP"
            'lblError.Text = ddlLtype.SelectedItem.Text

            btnEmp_Name.Visible = True
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("Leave Adjustment", ex.Message)
        End Try


    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ViewState("datamode") = "edit"
        UtilityObj.beforeLoopingControls(Me.Page)
        btnEmp_Name.Visible = False
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearRecords()
            'clear the textbox and set the default settings
            btnEmp_Name.Visible = True
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Status = AccessPayrollClass.Delete_EAH_ID(ViewState("viewid"), transaction)
                If Status = -1 Then
                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record could not be Deleted")
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        Throw New ArgumentException("Could not complete your request")
                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearRecords()
                    lblError.Text = "Record Deleted Successfully"
                End If
            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message)
                transaction.Rollback()
            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        GRIDBIND()
    End Sub


    Sub GRIDBIND()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT EAT_ELT_ID,EAT_EMP_ID,EAT_BSU_ID, MonthYear, ELT_DESCR, sum( EAT_DAYS) AS Days " _
                & " FROM ( SELECT  LEFT(DATENAME(MONTH, EAT_DT),3)+'/'+ LTRIM(RTRIM( DATEPART(YEAR, EAT_DT)))" _
                & " AS MonthYear, EAT_ELT_ID,EAT_EMP_ID,EAT.EAT_ID, EAT.EAT_BSU_ID, EAT.EAT_DT, ELM.ELT_DESCR, " _
                & " EAT.EAT_DAYS FROM EMPATTENDANCE AS EAT INNER JOIN EMPLOYEE_M AS EM ON EAT.EAT_EMP_ID = EM.EMP_ID " _
                & " INNER JOIN EMPLEAVETYPE_M AS ELM ON EAT.EAT_ELT_ID = ELM.ELT_ID AND EAT_BSU_ID = '" & Session("sBsuid") & "'  " _
                & " AND EAT_EMP_ID= " & hfEmp_ID.Value & " AND EAT.EAT_DT BETWEEN  '" & txtFrom_date.Text & "'   AND  '" & txtTo_date.Text & "'  ) AS DB  " _
                & " GROUP BY  EAT_ELT_ID,EAT_EMP_ID,EAT_BSU_ID, MonthYear, ELT_DESCR  " _
                & " ,'01/'+CAST(MONTH(EAT_DT) AS varchar(4)) +'/'+ CAST(YEAR(EAT_DT) AS varchar(4)) " _
                & " order by cast('01/'+CAST(MONTH(EAT_DT) AS varchar(4)) +'/'+ CAST(YEAR(EAT_DT) AS varchar(4))  as datetime) desc"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvHistory.DataSource = ds.Tables(0)
            gvHistory.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub btnEmp_Name_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmp_Name.Click
        GRIDBIND()
    End Sub

End Class
