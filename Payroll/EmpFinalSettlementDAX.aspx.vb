﻿Imports System.Web
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Security.Principal
Imports System.Globalization
Imports EmployeeCompensationService


Partial Class EmpFinalSettlementDAX
    Inherits System.Web.UI.Page

    Dim SaveMode As String = ""
    Dim Usr_Id As String = ""

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not Request.QueryString("UsrName") Is Nothing Then
            Me.hfUsrId.Value = Request.QueryString("UsrName")
        Else
            Me.hfUsrId.Value = ""
        End If

        If Not Request.QueryString("MenuCode") Is Nothing Then
            Me.hfMenuCode.Value = Request.QueryString("MenuCode")
        Else
            Me.hfMenuCode.Value = ""
        End If

        If Not Request.QueryString("empid") Is Nothing Then
            Me.hfEmpId.Value = Request.QueryString("empid")
            Me.hfEmpNo.Value = Me.GetOasisEmpNo(Me.hfEmpId.Value)
        Else
            Me.btnSyncToDAX.Visible = False
            Me.ShowInfoLog("Invalid employee", "W")
            Exit Sub
        End If

        'If Not Request.QueryString("BsuId") Is Nothing Then
        '    Me.hfBsuId.Value = Request.QueryString("BsuId")
        '    Me.hfCompany.Value = Me.GetAXBsuIdFromOasisBsuId(Me.hfBsuId.Value)
        'Else
        '    Me.btnSave.Visible = False
        '    Me.ShowInfoLog("Invalid business unit", "W")
        '    Exit Sub
        'End If

        Me.hfBsuId.Value = Me.GetOasisEmpBsuId(Me.hfEmpId.Value)
        Me.hfCompany.Value = Me.GetAXBsuIdFromOasisBsuId(Me.hfBsuId.Value)
        If Me.hfBsuId.Value = Nothing Then
            Me.btnSyncToDAX.Visible = False
            Me.ShowInfoLog("Invalid business unit", "W")
            Exit Sub
        End If


        If Me.hfCompany.Value = "" Then
            Me.btnSyncToDAX.Visible = False
            Me.ShowInfoLog("Invalid legal entity", "W")
            Exit Sub
        End If

        'If Not Me.IsPostBack Then
        '    Me.SalaryCollapser.ExpandedSize = 115
        'End If

        'SaveMode = Me.GetDataMode(Me.hfPosId.Value)

        If Not Me.IsPostBack Then

            Me.hfDaxRecId.Value = Me.GetDAXRecId(Me.hfEmpId.Value, Me.hfBsuId.Value)
            Me.GetFinalSettlementDetails(Me.hfEmpId.Value, Me.hfBsuId.Value)
            Me.CalculateGrossSalary()
            '  Me.ResizeSalaryCollapser("A")

        Else

        End If
    End Sub

    Private Function EmployeeExists(ByVal EmpNo As String) As Boolean
        EmployeeExists = False
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim conn As SqlConnection = New SqlConnection(str_conn)
        Dim count As Integer
        Try
            Dim str_query As String = "SELECT ISNULL(COUNT(EMP_ID),0) AS 'COUNT' FROM OASIS.dbo.EMPLOYEE_M with (nolock) WHERE EMPNO = '" & EmpNo & "'"
            count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If count > 0 Then
                Return True
            Else
                Me.ShowErrorMessage("Cannot find any employee with the employee no: " & EmpNo)
                Return False
            End If
        Catch exc As Exception
            Me.ShowErrorMessage("Error occured while getting employee details")
            Return False
        Finally
        End Try
    End Function

    Private Function GetOasisEmpId(ByVal Emp_No As String) As String
        GetOasisEmpId = Nothing
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Try
            str_query = "SELECT isnull(EMP_ID,'0') as 'EMP_ID' FROM oasis.dbo.EMPLOYEE_M with (nolock) WHERE EMPNO = '" & Emp_No & "'"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            Me.ShowErrorMessage("Error occured getting employee details")
        End Try
    End Function

    Private Function GetOasisEmpNo(ByVal Emp_Id As Integer) As String
        GetOasisEmpNo = Nothing
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Try
            str_query = "SELECT isnull(EMPNo,'0') as 'EMP_NO' FROM oasis.dbo.EMPLOYEE_M with (nolock) WHERE EMP_ID = '" & Emp_Id & "'"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            Me.ShowErrorMessage("Error occured getting employee details")
        End Try
    End Function

    Private Function GetOasisEmpBsuId(ByVal Emp_Id As Integer) As String
        GetOasisEmpBsuId = Nothing
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Try
            str_query = "SELECT EMP_BSU_ID FROM OASIS.dbo.EMPLOYEE_M WITH (NOLOCK) WHERE EMP_ID = " & Emp_Id
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            Me.ShowErrorMessage("Error occured getting employee business unit")
        End Try
    End Function

    Private Sub GetPositionMasterDetails(ByVal PosId As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Dim dr As IDataReader
        Try
            str_query = "exec GetPositionDetails @POS_ID = '" & PosId & "'"
            dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            Do While dr.Read
                Me.hfGradeId.Value = dr.Item("HGRADE_ID")
                Me.hfGradeLevelId.Value = dr.Item("HRL_ID")
                Me.txtPosition.Text = dr.Item("pos_descr")
                Me.txtDepartment.Text = dr.Item("dpt_descr")
                Me.txtDesignation.Text = dr.Item("des_descr")
                Me.txtGrade.Text = dr.Item("HGRADE_VALUE")
                If Not IsDBNull(dr.Item("pos_dtfrom")) Then
                    Me.hfFromDate.Value = Format(dr.Item("pos_dtfrom"), "dd/MMM/yyyy")
                    Me.txtCompensationFromDate.Text = Format(dr.Item("pos_dtfrom"), "dd/MMM/yyyy")
                Else
                    Me.hfFromDate.Value = Format(Now.Date, "dd/MMM/yyyy")
                    Me.txtCompensationFromDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                End If
                If Not IsDBNull(dr.Item("pos_dtTO")) Then
                    Me.hfToDate.Value = Format(dr.Item("pos_dtto"), "dd/MMM/yyyy")
                Else
                    Me.hfToDate.Value = Format(Now.Date, "dd/MMM/yyyy")
                End If
                Me.txtGradeLevel.Text = IIf(dr.Item("HRL_BAND") = "0", "", dr.Item("HRL_BAND"))
            Loop
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while getting position details"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    Private Sub GetPositionSalaryComponents_ForAddEdit(ByVal Pos_Id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Dim ds As DataSet
        Try
            'str_query = "exec GetPositionSalaryComponents_ForAddEdit @POS_ID = '" & Pos_Id & "'"
            str_query = "exec GetPositionSalaryComponents_ForAddEdit_Staging @POS_ID = '" & Pos_Id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Select("Ern_Id = ''").Length > 0 Then
                ds.Tables(0).Rows.Remove(ds.Tables(0).Select("Ern_Id = ''")(0))
            End If
            If Not ds Is Nothing Then
                Me.gvCompensation.DataSource = ds.Tables(0)
                Me.gvCompensation.DataBind()
            End If
            If Session("dtPositionCompensationDetails") Is Nothing Then
                Session.Add("dtPositionCompensationDetails", ds.Tables(0))
            End If
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while getting position compensation details"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    Private Sub GetFinalSettlementDetails(ByVal Emp_Id As Integer, ByVal Bsu_Id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Dim ds As DataSet
        Try
            str_query = "exec [dbo].[GetFinalSettlementSalaryDetails] @EMP_ID = " & Emp_Id & ", @Bsu_Id = '" & Bsu_Id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.gvCompensation.DataSource = ds.Tables(0)
                Me.gvCompensation.DataBind()

                'If ds.Tables(0).Rows.Count = 0 Then
                '    Dim row As DataRow = ds.Tables(0).NewRow
                '    row.Item("ESS_ERNCODE") = "Basic"
                '    row.Item("EDD_DESCR") = "Demo final settlement basic"
                '    row.Item("ESS_TYPE") = "E"
                '    row.Item("ESS_GROSS") = 5000
                '    row.Item("ESS_EARNED") = 5000
                '    row.Item("ERN_DESCR") = "Basic salary"
                '    ds.Tables(0).Rows.Add(row)
                '    ds.AcceptChanges()

                '    Me.gvCompensation.DataSource = ds.Tables(0)
                '    Me.gvCompensation.DataBind()
                'End If
            End If

        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while getting final settlement details"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    Sub GetEmpData(empNo As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)       
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSyncToDAX.Click
        'Me.SaveData()
        trMsg.Visible = True
        Me.lblError.Visible = True
        Me.ShowInfoLog("Please wait while the data gets synced...", "S")
        Me.SaveAllData()
    End Sub

    'Protected Sub btnTransfer_Click(sender As Object, e As System.EventArgs) Handles btnTransfer.Click
    'Try
    '    Dim client As New AleSalComp.ALE_EmplSalaryCompDocServiceClient
    '    'client.ClientCredentials.UserName.UserName = "gemserp\shakeel.shakkeer"
    '    'client.ClientCredentials.UserName.Password = "P@ssw0rd2"
    '    client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\shakeel.shakkeer"
    '    client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"
    '    'client.ClientCredentials.Windows.AllowedImpersonationLevel = Security.Principal.TokenImpersonationLevel.
    '    Dim context As New AleSalComp.CallContext
    '    context.Company = "GCO"
    '    Dim salDoc As New AleSalComp.AxdALE_EmplSalaryCompDoc

    '    Dim sal(2) As AleSalComp.AxdEntity_ALE_EmplSalaryComponent
    '    sal(0) = New AleSalComp.AxdEntity_ALE_EmplSalaryComponent
    '    sal(0).EmployeeId = lblEmpId.Text
    '    sal(0).SalaryComponentId = "BASIC"
    '    sal(0).FromDate = Now
    '    sal(0).FromDateSpecified = True
    '    sal(0).Amount = Val(txtBasic.Text)
    '    sal(0).AmountSpecified = True

    '    sal(1) = New AleSalComp.AxdEntity_ALE_EmplSalaryComponent
    '    sal(1).EmployeeId = lblEmpId.Text
    '    sal(1).SalaryComponentId = "TA"
    '    sal(1).FromDate = Now
    '    sal(1).FromDateSpecified = True
    '    sal(1).Amount = Val(txtTA.Text)
    '    sal(1).AmountSpecified = True

    '    sal(2) = New AleSalComp.AxdEntity_ALE_EmplSalaryComponent
    '    sal(2).EmployeeId = lblEmpId.Text
    '    sal(2).SalaryComponentId = "HRA"
    '    sal(2).FromDate = Now
    '    sal(2).FromDateSpecified = True
    '    sal(2).Amount = Val(txtHRA.Text)
    '    sal(2).AmountSpecified = True

    '    salDoc.ALE_EmplSalaryComponent = sal

    '    Dim ent(1) As AleSalComp.EntityKey
    '    ent = client.create(context, salDoc)
    '    lblError.Text = "Transferred Successfully"
    '    lblError.ForeColor = Drawing.Color.DarkGreen
    'Catch ex As Exception
    '    lblError.Text = ex.Message '"Error While Transferring"
    '    lblError.ForeColor = Drawing.Color.Red
    'End Try
    'End Sub

    Protected Sub lnkSalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1
            If str_Search = Session("EMPSALDETAILS").Rows(iIndex)("UniqueID") And Session("EMPSALDETAILS").Rows(iIndex)("Status") & "" <> "Deleted" Then
                ddlEarnings.SelectedIndex = -1
                ddlEarnings.Items.FindByValue(Session("EMPSALDETAILS").Rows(iIndex)("ENR_ID")).Selected = True
                'txtSalAmount.Text = Session("EMPSALDETAILS").Rows(iIndex)("Amount")
                txtCompAmount.Text = Session("EMPSALDETAILS").Rows(iIndex)("Amount_ELIGIBILITY")
                'chkSalPayMonthly.Checked = Session("EMPSALDETAILS").Rows(iIndex)("bMonthly")
                'ddlayInstallment.SelectedIndex = -1
                'ddlayInstallment.Items.FindByValue(Session("EMPSALDETAILS").Rows(iIndex)("PAYTERM")).Selected = True

                'Session("EMPSALGrossSalary") -= CDbl(txtSalAmount.Text)
                gvCompensation.SelectedIndex = iIndex
                btnSalAdd.Text = "Save"
                Session("EMPSalEditID") = str_Search
                'UtilityObj.beforeLoopingControls(vwSalary)
                Exit For
            End If
        Next
    End Sub

    Sub setmvMaster_ActiveViewIndex(ByVal sel_ActiveViewIndex As Integer)
        'mvMaster.ActiveViewIndex = sel_ActiveViewIndex
        'Session("mvMaster_ActiveViewIndex") = mvMaster.ActiveViewIndex
    End Sub

    'Protected Sub mainMenu_MenuItemClick(sender As Object, e As MenuEventArgs) Handles mainMenu.MenuItemClick
    '    Dim Iindex As Integer = Int32.Parse(e.Item.Value)
    '    Dim i As Integer = -1
    '    'setmvMaster_ActiveViewIndex(Iindex)
    '    Select Case Iindex
    '        Case 0
    '            i = 0
    '        Case 1
    '            i = 1
    '        Case 2
    '            i = 2
    '    End Select
    '    'mvMaster.ActiveViewIndex = i
    '    'Session("mvMaster_ActiveViewIndex") = mvMaster.ActiveViewIndex
    '    mainMenu.Items(i).Selectable = True
    'End Sub

    Private Sub BindEarnings()
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Dim dr As IDataReader
        Dim DS As DataSet
        Try
            'str_query = "SELECT * FROM (Select '' as Ern_Id, '[Select]' as ern_descr, 0 AS ern_order union SELECT ESC.ERN_ID, ESC.ERN_DESCR, ISNULL(ESC.ERN_ORDER,0) AS ern_order FROM oasis.dbo.EMPSALCOMPO_M ESC WHERE ESC.ERN_TYP = 1) T ORDER BY ERN_ORDER"
            str_query = "SELECT * FROM (Select '' as Ern_Id, '[Select]' as ern_descr, 0 AS ern_order union SELECT ESC.ERN_ID, ESC.ERN_DESCR, isnull(ESC.ERN_ORDER,999999) as 'ERN_ORDER' FROM oasis.dbo.EMPSALCOMPO_M ESC WHERE ESC.ERN_TYP = 1 And (ESC.ERN_JOINING = 0 OR ESC.ERN_JOINING IS NULL)) T ORDER BY ERN_ORDER"
            DS = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If Not DS Is Nothing Then
                Me.ddlEarnings.DataValueField = "ern_id"
                Me.ddlEarnings.DataTextField = "ern_descr"
                Me.ddlEarnings.DataSource = DS
                Me.ddlEarnings.DataBind()



            End If
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while loading earning types"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    Private Sub BindMedicalInsurance()
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Dim dr As IDataReader
        Dim DS As DataSet
        Try
            str_query = "SELECT * FROM (SELECT 0 AS INR_ID, '[Select]' AS INR_DESCR UNION ALL  SELECT MI.INR_ID, MI.INR_DESCR FROM dbo.MEDICALINSURANCE_M MI) T ORDER BY T.INR_ID"
            DS = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If Not DS Is Nothing Then

            End If
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while loading medical insurance types"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    'Protected Sub ddlAirfare_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAirfare.SelectedIndexChanged
    '    Me.LoadAirFareDetails(ddlAirfare.SelectedValue, Me.gvAirFare)
    'End Sub

    Private Sub LoadAirFareDetails(ByVal Air_Id As Integer, ByVal gvAirFareDetails As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Dim dr As IDataReader
        Dim DS As DataSet
        Try
            str_query = "SELECT AIR.AIR_ID, AIR.AIR_DESCR, AIR.AIR_ELG_ID, AIR.AIR_ECONOMY, AIR.AIR_BUSINESS,AIR.AIR_FIRSTCLASS, CASE WHEN AIR.AIR_EAF_TYPE = 1 THEN 'Return' ELSE 'One Way' END AS AIR_EAF_TYPE, AIR.AIR_EAF_TICKETCOUNT FROM dbo.AIRFARE_M AIR WHERE AIR.AIR_ID = " & Air_Id
            DS = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If Not DS Is Nothing Then
                gvAirFareDetails.DataSource = DS
                gvAirFareDetails.DataBind()
            End If
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while loading air fare details"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub



    Protected Sub btnSalAdd_Click(sender As Object, e As EventArgs) Handles btnSalAdd.Click
        If Me.ddlEarnings.SelectedValue = "" Then ' OrElse (Me.txtCompAmount.Text = Nothing OrElse Val(Me.txtCompAmount.Text) < 0) Then
            trMsg.Visible = True
            'Me.lblError.Text = "Please select valid earning type and enter valid compensation amount"
            Me.lblError.Text = "Please select valid earning type"
            Exit Sub
        End If

        If Not IsDate(Me.txtCompensationFromDate.Text) Then
            trMsg.Visible = True
            Me.lblError.Text = "Please enter valid from date for the compensation type"
            Exit Sub
        End If

        If Not Me.txtMaxRangeAmount.Text = "" AndAlso Not IsNumeric(Me.txtMaxRangeAmount.Text) OrElse Val(Me.txtMaxRangeAmount.Text) < 0 Then
            trMsg.Visible = True
            Me.lblError.Text = "Please enter valid value for max amount"
            Exit Sub
        End If

        If Not Me.txtMidRangeAmount.Text = "" AndAlso Not IsNumeric(Me.txtMidRangeAmount.Text) OrElse Val(Me.txtMidRangeAmount.Text) < 0 Then
            trMsg.Visible = True
            Me.lblError.Text = "Please enter valid value for mid amount"
            Exit Sub
        End If

        If Not Me.txtMinRangeAmount.Text = "" AndAlso Not IsNumeric(Me.txtMinRangeAmount.Text) OrElse Val(Me.txtMinRangeAmount.Text) < 0 Then
            trMsg.Visible = True
            Me.lblError.Text = "Please enter valid value for min amount"
            Exit Sub
        End If

        'Check if again adding a pre-existing (duplicate) earning component
        Dim i As Integer = 0
        For i = 0 To gvCompensation.Rows.Count - 1
            If LCase(Me.ddlEarnings.SelectedValue) = LCase(CType(gvCompensation.Rows(i).FindControl("lblern_id"), Label).Text) Then
                Me.ShowErrorMessage("Selected earning component is already added")
                Exit Sub
            End If
        Next

        Try
            Dim dt As DataTable
            If Session("dtPositionCompensationDetails") Is Nothing Then
                dt = New DataTable
                dt.Columns.Add("pcomp_id", GetType(Int32))
                dt.Columns.Add("hcomp_id", GetType(Int32))
                'dt.Columns.Add("pcomp_pos_id", GetType(Int32))
                dt.Columns.Add("pcomp_pos_id", GetType(String))
                dt.Columns.Add("ern_id", GetType(String))
                dt.Columns.Add("ern_DESCR", GetType(String))
                dt.Columns.Add("pcomp_frequency", GetType(String))
                dt.Columns.Add("PCOMP_FROMDATE", GetType(String))
                dt.Columns.Add("PCOMP_TODATE", GetType(String))
                dt.Columns.Add("max", GetType(Decimal))
                dt.Columns.Add("mid", GetType(Decimal))
                dt.Columns.Add("min", GetType(Decimal))
                dt.Columns.Add("base", GetType(Decimal))
                dt.AcceptChanges()
                Session.Add("dtPositionCompensationDetails", dt)
            Else
                dt = CType(Session("dtPositionCompensationDetails"), DataTable)
            End If
            Dim row As DataRow = dt.NewRow
            row.Item("pcomp_id") = 0
            row.Item("hcomp_id") = 0
            row.Item("pcomp_pos_id") = Me.hfPosId.Value
            row.Item("ern_id") = Me.ddlEarnings.SelectedValue
            row.Item("ern_descr") = Me.ddlEarnings.SelectedItem.Text
            row.Item("pcomp_frequency") = "M"
            row.Item("pcomp_fromdate") = Me.txtCompensationFromDate.Text
            row.Item("max") = Math.Round(Val(Me.txtMaxRangeAmount.Text), 2)
            row.Item("mid") = Math.Round(Val(Me.txtMidRangeAmount.Text), 2)
            row.Item("min") = Math.Round(Val(Me.txtMinRangeAmount.Text), 2)
            'row.Item("base") = Val(Me.txtCompAmount.Text)
            row.Item("base") = Math.Round(Val(Me.txtCompAmount.Text), 2)
            dt.Rows.Add(row)
            dt.AcceptChanges()
            Session("dtPositionCompensationDetails") = dt

            Me.gvCompensation.DataSource = dt
            Me.gvCompensation.DataBind()

            Me.CalculateGrossSalary()

            Me.ddlEarnings.SelectedValue = ""
            Me.txtCompAmount.Text = ""
            Me.txtMaxRangeAmount.Text = ""
            Me.txtMidRangeAmount.Text = ""
            Me.txtMinRangeAmount.Text = ""

            trMsg.Visible = False
            Me.lblError.Text = ""

            '  Me.ResizeSalaryCollapser("A")

            Exit Sub
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while adding compensation detail"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    Protected Sub gvCompensation_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvCompensation.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '    Dim lblhcomp_id As Label = CType(e.Row.FindControl("lblhcomp_id"), Label)
            Dim lnk As LinkButton = CType(e.Row.FindControl("lnkRemoveCompensation"), LinkButton)
            '    If lblhcomp_id.Text = "0" Then
            lnk.Visible = True

            lnk.Enabled = True
            lnk.OnClientClick = "return confirm('Are you sure you want to remove this compensation?');"
            '    Else
            '        lnk.Enabled = False
            '        lnk.ForeColor = Drawing.Color.Gray

            '        'lnk.Visible = False
            '    End If

            'Disable Min, Mid & Max if grade is defined against the position
            Dim lblhcomp_id As Label = CType(e.Row.FindControl("lblhcomp_id"), Label)
            'If Me.hfGradeId.Value <> "0" Then
            If Not lblhcomp_id Is Nothing AndAlso (lblhcomp_id.Text = "" Or lblhcomp_id.Text <> "0") Then
                CType(e.Row.FindControl("txtMin"), TextBox).ReadOnly = True
                CType(e.Row.FindControl("txtMid"), TextBox).ReadOnly = True
                CType(e.Row.FindControl("txtMax"), TextBox).ReadOnly = True
            End If

        End If
    End Sub

    'Private Function SaveCompensationData(ByVal tr As SqlTransaction) As Boolean
    '    SaveCompensationData = False
    '    'POSITION_COMPENSATION_M
    '    Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
    '    Dim conn As SqlConnection = New SqlConnection(str_conn)
    '    'Dim tr As SqlTransaction
    '    Dim qry As String = ""
    '    Dim lblpcomp_id, lblhcomp_id, lblpos_id, lblern_id, lblmax, lblmid, lblmin, lblhays_base, lblfromdate, lblSalFrequency As String
    '    ' Try
    '    'conn.Open()
    '    'tr = conn.BeginTransaction
    '    For Each row As GridViewRow In gvCompensation.Rows
    '        lblpcomp_id = CType(row.FindControl("lblpcomp_id"), Label).Text
    '        lblhcomp_id = CType(row.FindControl("lblhcomp_id"), Label).Text
    '        lblpos_id = CType(row.FindControl("lblPOS_ID"), Label).Text
    '        lblern_id = CType(row.FindControl("lblern_id"), Label).Text
    '        'lblfromdate = CType(row.FindControl("txtRangeFromDate"), TextBox).Text
    '        lblfromdate = Me.txtCompensationFromDate.Text
    '        'lblmax = Math.Round(Val(CType(row.FindControl("lblmax"), Label).Text), 2)
    '        'lblmid = Math.Round(Val(CType(row.FindControl("lblmid"), Label).Text), 2)
    '        'lblmin = Math.Round(Val(CType(row.FindControl("lblmin"), Label).Text), 2)
    '        lblmax = Math.Round(Val(CType(row.FindControl("txtmax"), TextBox).Text), 2)
    '        lblmid = Math.Round(Val(CType(row.FindControl("txtmid"), TextBox).Text), 2)
    '        lblmin = Math.Round(Val(CType(row.FindControl("txtmin"), TextBox).Text), 2)
    '        lblhays_base = Math.Round(Val(CType(row.FindControl("txtcompensation_amount"), TextBox).Text), 2)
    '        lblSalFrequency = CType(row.FindControl("ddlSalFrequency"), DropDownList).SelectedValue

    '        'If Not lblfromdate Is Nothing Then
    '        '    If Not IsProperDate(lblfromdate) Then
    '        '        trMsg.Visible = True
    '        '        lblError.Text = "Please enter valid from date for this compensation detail"
    '        '        lblError.ForeColor = Drawing.Color.DarkRed
    '        '        row.BackColor = Drawing.Color.Orange
    '        '        Return False
    '        '    End If
    '        'End If

    '        'validation if salary components loaded based on the grade assigned to the position
    '        'If lblhcomp_id <> "0" Then
    '        '    If lblhays_base < lblmin Or lblhays_base > lblmax Then
    '        '        trMsg.Visible = True
    '        '        lblError.Text = "Compensation amount cannot be lesser than min amount or cannot be greater than the max amount defined for the grade"
    '        '        lblError.ForeColor = Drawing.Color.DarkRed
    '        '        row.BackColor = Drawing.Color.Orange
    '        '        Return False
    '        '    Else
    '        '        row.BackColor = Drawing.Color.White
    '        '    End If
    '        'End If

    '        'qry &= "EXEC dbo.SAVE_POSITION_COMPONENTS_M @PCOMP_ID = " & lblpcomp_id & ", " & _
    '        qry &= "EXEC dbo.SAVE_POSITION_COMPONENTS_M_Staging @PCOMP_ID = " & lblpcomp_id & ", " & _
    '                     "@PCOMP_HCOMP_ID = " & lblhcomp_id & ", " & _
    '                     "@PCOMP_POS_ID = '" & lblpos_id & "', " & _
    '                     "@PCOMP_ERN_ID = '" & lblern_id & "', " & _
    '                     IIf(lblfromdate <> Nothing, "@PCOMP_FROMDATE = '" & lblfromdate & "', ", "@PCOMP_FROMDATE = '" & Format(Now.Date, "dd/MMM/yyyy") & "', ") & _
    '                     "@PCOMP_MAX = " & lblmax & "," & _
    '                     "@PCOMP_Mid = " & lblmid & ", " & _
    '                     "@PCOMP_Min = " & lblmin & ", " &
    '                     "@PCOMP_HAYS_BASE = " & lblhays_base & ", " &
    '                     "@PCOMP_FLAG = NULL, " &
    '                     "@USR_ID = '" & Me.hfUsrId.Value & "', " &
    '                     "@PCOMP_DAX_ACTIONNUMBER = '" & Me.hfDAXActionNumber.Value & "', " &
    '                     "@PCOMP_Menu_Code = '" & Me.hfMenuCode.Value & "', " &
    '                     "@PCOMP_FREQUENCY = '" & lblSalFrequency & "'"
    '        SqlHelper.ExecuteNonQuery(tr, CommandType.Text, qry)
    '        qry = ""
    '    Next

    '    ''commit the transaction if we reach here
    '    'tr.Commit()
    '    'trMsg.Visible = True
    '    'lblError.Text = "Compensation data saved successfully!!!"
    '    'lblError.ForeColor = Drawing.Color.DarkRed

    '    'Me.btnSave.Visible = False
    '    ''Me.btnDelete.Visible = False

    '    'Catch ex As Exception
    '    '    If Not tr Is Nothing Then tr.Rollback()
    '    '    trMsg.Visible = True
    '    '    lblError.Text = "Error occured while saving the data"
    '    '    lblError.ForeColor = Drawing.Color.DarkRed
    '    'Finally
    '    '    If Not conn.State = ConnectionState.Closed Then conn.Close()
    '    'End Try
    '    Return True
    'End Function

    Private Function GetDataMode(ByVal Pos_Id As String) As String
        GetDataMode = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Try
            'Dim Count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT isnull(COUNT(PCOMP_ID),0) as Count FROM dbo.POSITION_COMPONENTS_M WHERE PCOMP_POS_ID = " & Pos_Id)
            'Dim Count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT isnull(COUNT(PCOMP_ID),0) as Count FROM dbo.POSITION_COMPONENTS_M_Staging WHERE PCOMP_POS_ID = " & Pos_Id)
            Dim Count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT isnull(COUNT(PCOMP_ID),0) as Count FROM dbo.POSITION_COMPONENTS_M_Staging WHERE PCOMP_CURRENT = 1 AND (PCOMP_APPR_STATUS <> 'D' OR PCOMP_APPR_STATUS IS  NULL) AND  PCOMP_POS_ID = '" & Pos_Id & "'")
            If Count = 0 Then
                Return "A"
            ElseIf Count > 0 Then
                Return "E"
            End If
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error while loading position compensation details"
            lblError.ForeColor = Drawing.Color.DarkRed
            Me.btnSyncToDAX.Visible = False
            Me.btnDelete.Visible = False
            'Me.btnClose.Visible = True
        End Try
    End Function

    Private Function GetDAXRecId(ByVal Emp_Id As Integer, ByVal Bsu_Id As String) As String
        GetDAXRecId = Nothing
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Try
            Dim DAX_REC_ID As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT ISNULL(SFNL_DAX_REC_ID, '') as 'DAX_REC_ID' FROM dbo.SALARY_FINAL_SETTLEMENT WHERE SFNL_EMP_ID = " & Emp_Id & " And SFNL_BSU_ID = '" & Bsu_Id & "'")
            Return DAX_REC_ID
        Catch ex As Exception
            Me.btnSyncToDAX.Visible = False
            Me.btnDelete.Visible = False
            Me.ShowInfoLog("Error getting final settlement details", "E")
        End Try
    End Function

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        'Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Try

            Me.ClearFormData()
            Me.GetFinalSettlementDetails(Me.hfEmpId.Value, Me.hfBsuId.Value)
            Me.CalculateGrossSalary()

        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error resetting final settlement details"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub

    Private Sub ClearFormData()
        Me.txtPosition.Text = ""
        Me.txtDepartment.Text = ""
        Me.txtDesignation.Text = ""
        Me.txtGrade.Text = ""
        Me.txtGradeLevel.Text = ""

        If ddlEarnings.Items.Count >= 1 Then Me.ddlEarnings.SelectedValue = ""
        Me.txtCompAmount.Text = ""

        Me.gvCompensation.DataSource = Nothing
        Me.gvCompensation.DataBind()

        Me.txtGrossSalary.Text = ""



        If Not Session("dtPositionJoiningAllowance") Is Nothing Then
            Session.Remove("dtPositionJoiningAllowance")
        End If

        If Not Session("dtPositionCompensationDetails") Is Nothing Then
            Session.Remove("dtPositionCompensationDetails")
        End If

    End Sub


    Private Function IsProperDate(ByVal dt As String) As Boolean
        IsProperDate = False
        Dim desDateTime As DateTime
        Try
            Return DateTime.TryParseExact(dt, "dd/MMM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, desDateTime)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub CalculateGrossSalary()
        Try
            Dim TotalAmount As Decimal = 0
            Dim RowAmount As Decimal = 0
            For Each row As GridViewRow In Me.gvCompensation.Rows
                If row.RowType = DataControlRowType.DataRow Then
                    If (CType(row.FindControl("lbless_type"), Label).Text = "E") Then
                        RowAmount = Val(CType(row.FindControl("lbless_earned"), Label).Text)
                    ElseIf (CType(row.FindControl("lbless_type"), Label).Text = "D") Then
                        RowAmount = -1 * Val(CType(row.FindControl("lbless_earned"), Label).Text)
                    End If
                    TotalAmount += RowAmount
                End If
            Next
            Me.txtGrossSalary.Text = Format(Math.Round(TotalAmount, 2), "0.00")
        Catch ex As Exception
            trMsg.Visible = True
            lblError.Text = "Error calculating gross salary"
            lblError.ForeColor = Drawing.Color.DarkRed
        End Try
    End Sub


    'Public Sub ResizeSalaryCollapser(Optional ByVal ResizeType As String = "A")
    '    Dim initialHeight As Integer = 115
    '    'Add or remove 25 pixels for each row to the grid height
    '    If ResizeType = "A" Then 'add to the height
    '        Me.SalaryCollapser.ExpandedSize = initialHeight
    '        For Each row As GridViewRow In Me.gvCompensation.Rows
    '            Me.SalaryCollapser.ExpandedSize += 30
    '        Next
    '    ElseIf ResizeType = "R" Then
    '        'For Each row As GridViewRow In Me.gvCompensation.Rows
    '        Me.SalaryCollapser.ExpandedSize -= 30
    '        'Next
    '    End If

    'End Sub


    Private Sub SaveAllData()
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim conn As SqlConnection = New SqlConnection(str_conn)
        Dim tr As SqlTransaction
        Dim retValue As Boolean = False
        Dim ErrorOccured As Boolean = False
        Dim ServiceErrorMsg As String = ""
        Try

            'Send the details back to DAX

            'retValue = Me.SendToDAX_Employee(Me.Get6DigitRandomNumber(), Me.hfEmpId.Value, Me.hfDAXActionNumber.Value,
            '            Me.GetUniqueCompensationId(Me.hfPosId.Value),
            '             Now.Date, Now.Date.AddYears(1), Me.gvCompensation, ServiceErrorMsg)

            conn.Open()
            tr = conn.BeginTransaction

            'If Me.hfDaxRecId.Value <> "" Then
            '    retValue = Me.DeleteFromDAX_Employee(Me.hfDaxRecId.Value, ServiceErrorMsg)
            '    If retValue = True Then
            '        retValue = Me.SendToDAX_Employee(tr, Me.Get6DigitRandomNumber(), Me.hfEmpNo.Value, Me.hfPosId.Value, Me.hfDAXActionNumber.Value,
            '                    Me.GetUniqueCompensationId(Me.hfEmpNo.Value),
            '                    Now.Date, Format(Now.Date.AddYears(1), "dd/MMM/yyyy"), Me.gvCompensation, ServiceErrorMsg)
            '    End If
            'Else
            '    retValue = Me.SendToDAX_Employee(tr, Me.Get6DigitRandomNumber(), Me.hfEmpNo.Value, Me.hfPosId.Value, Me.hfDAXActionNumber.Value,
            '                    Me.GetUniqueCompensationId(Me.hfEmpNo.Value),
            '                    Now.Date, Format(Now.Date.AddYears(1), "dd/MMM/yyyy"), Me.gvCompensation, ServiceErrorMsg)
            'End If

            retValue = Me.SendToDAX_Employee(tr, Me.Get6DigitRandomNumber(), Me.hfEmpNo.Value, Me.hfPosId.Value, Me.hfDAXActionNumber.Value, _
                                Me.GetUniqueCompensationId(Me.hfEmpNo.Value), _
                                Now.Date, Format(Now.Date.AddYears(1), "dd/MMM/yyyy"), Me.gvCompensation, ServiceErrorMsg)

            If retValue = True Then
                tr.Commit()
                trMsg.Visible = True
                Me.ShowInfoLog("Data synched successfully!!!", "S")
                Me.btnSyncToDAX.Visible = False
                Me.btnDelete.Visible = False
            Else
                'tr.Rollback()
                ErrorOccured = True
                Exit Sub
            End If

        Catch ex As Exception
            tr.Rollback()
            If ServiceErrorMsg = "" Then
                Me.ShowInfoLog("Error occured while syncing the data", "E")
            Else
                Me.ShowInfoLog(ServiceErrorMsg, "W")
            End If

            Me.LogError(ex, "EmpFinalSettlement.apsx", "SaveAllData")

        Finally
            If Not conn.State = ConnectionState.Closed Then conn.Close()
        End Try

    End Sub

    Private Function Get3DigitRandomNumber(ByVal Max As Integer) As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Dim Min As Integer = 100
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function

    Private Function Get3DigitRandomNumber() As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Dim Min As Integer = 100
        Dim Max As Integer = 999
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function

    Private Function Get6DigitRandomNumber() As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Dim Min As Integer = 111111
        Dim Max As Integer = 999999
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function

    Private Function GetUniqueCompensationId(ByVal PosId As String) As String
        Dim Min As Integer = 111
        'Dim Max As Integer = 999999
        Dim Max As Integer = 9999
        Static Generator As System.Random = New System.Random()
        Return PosId & "-FNST-" & Generator.Next(Min, Max).ToString
        'Return Generator.Next(Min, Max).ToString
    End Function
    Private Function GetAXBsuIdFromOasisBsuId(ByVal Bsu_Id As String) As String
        GetAXBsuIdFromOasisBsuId = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Try
            str_query = "EXEC dbo.GetAXBsuIdFromOasisBsuId  @BSU_ID = '" & Bsu_Id & "'"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            LogError(ex, "EmpFinalSettlement.aspx", "GetAXBsuIdFromOasisBsuId")
            Me.ShowErrorMessage("Error getting legal entity details")
        End Try
    End Function

    Private Function GetActualAmount(ByVal Amount As String) As Decimal
        GetActualAmount = 0
        'Return Replace(Amount, ",", Nothing)
        Return Val(Replace(Amount, ",", Nothing))
    End Function

    Private Function GetActualAmountAsString(ByVal Amount As String) As String
        GetActualAmountAsString = ""
        Return Replace(Amount, ",", Nothing)
    End Function

    Private Function SendToDAX_Employee(ByVal tr As SqlTransaction, ByVal RecId As Integer, ByVal EmpId As String, ByVal Pos_Id As String, ByVal ActionState As String, CompensationId As String, ValidFrom As Date, ValidTo As Date, gvCompensation As GridView, ByRef ServiceErrorMsg As String) As Boolean
        SendToDAX_Employee = False
        'Private Sub SendToDAX(ByVal RecId As Integer, ByVal PosId As String, ActionState As String, CompensationId As String, GradeId As String, ValidFrom As String, ValidTo As String, gvCompensation As GridView)
        Try
            Dim client As New ALE_EmpCompServiceClient

            'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\shakeel.shakkeer"
            'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"

            'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\swapna.tv"
            'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"

            client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
            client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString

            Dim context As New CallContext
            'context.Company = "GCO" ' disable it when not in demo mode and pass actual legal entity
            context.Company = Me.hfCompany.Value

            'Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count
            'Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count + 6 + 1 + gvJoiningAllowance.Rows.Count

            Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count '+ 6 + 1 + gvJoiningAllowance.Rows.Count

            Dim EmpCompFactory As New AxdALE_EmpComp
            Dim EmpComps(0) As AxdEntity_ALE_EmployeeCompensation
            Dim EmpComp As AxdEntity_ALE_EmployeeCompensation = Nothing
            Dim EmpCompLines(EmpCompLinesCount - 1) As AxdEntity_ALE_EmployeeCompensationLine
            Dim EmpCompLine As AxdEntity_ALE_EmployeeCompensationLine = Nothing
            'Dim EmpCompRef As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing

            'Initialize and fill PosComp (header) details
            EmpComp = New AxdEntity_ALE_EmployeeCompensation
            EmpComp.RecId = RecId
            EmpComp.RecIdSpecified = True
            EmpComp.EmployeeId = Me.hfEmpNo.Value
            EmpComp.HcmActionState = "AS000001" '000000001'ActionState 'Ignore this comment. As of now, skipping this as any value passed for it is throwing error message. Need to check with them what valid value we can pass

            EmpComp.CompensationId = CompensationId
            EmpComp.Description = "Full and Final settlement"
            EmpComp.ValidFrom = ValidFrom
            EmpComp.ValidFromSpecified = True
            EmpComp.ValidTo = ValidTo 'Pass some distant future date
            EmpComp.ValidToSpecified = True
            ' EmpComp.ActionType = "Terminate Empl"
            EmpComp.ActionType = "Termination" 'As confirmed by Puneet on skype
            EmpComp.class = "entity" 'This value is hard coded as suggested in the service documentation

            'Now fill up the EmpCompLines() array with all EmpCompLine. Compensation details.
            Dim i As Integer = 0
            For i = 0 To gvCompensation.Rows.Count - 1
                EmpCompLine = New AxdEntity_ALE_EmployeeCompensationLine

                EmpCompLine.RecId = Get6DigitRandomNumber()
                EmpCompLine.RecIdSpecified = True

                EmpCompLine.EmployeeCompensationRef = RecId
                EmpCompLine.EmployeeCompensationRefSpecified = True

                EmpCompLine.Amount = Me.GetActualAmount(CType(gvCompensation.Rows(i).FindControl("lbless_earned"), Label).Text)
                EmpCompLine.AmountSpecified = True
                EmpCompLine.CurrencyCode = "AED" 'Me.ddlCurrency.SelectedValue
                EmpCompLine.ComponentCode = CType(gvCompensation.Rows(i).FindControl("lblern_Code"), Label).Text
                EmpCompLine.Description = CType(gvCompensation.Rows(i).FindControl("lblern_descr"), Label).Text
                EmpCompLine.FromDate = ValidFrom
                EmpCompLine.FromDateSpecified = True
                'EmpCompLine.PayType = IIf(CType(gvCompensation.Rows(i).FindControl("ddlSalFrequency"), DropDownList).SelectedValue = "M", EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly, EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Annual)
                EmpCompLine.PayType = AxdExtType_ALE_HcmCompPayType.NonCyclical
                EmpCompLine.PayTypeSpecified = True
                EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

                EmpCompLines(i) = EmpCompLine
            Next

            'Now assign the EmpCompLines() array to EmpComp
            EmpComp.ALE_EmployeeCompensationLine = EmpCompLines

            'Now add EmpComp to EmpComps array
            EmpComps(0) = EmpComp

            'Now assing EmpComps to EmpComp field of EmpCompFactory
            EmpCompFactory.ALE_EmployeeCompensation = EmpComps

            Dim ent(0) As EmployeeCompensationService.EntityKey
            ent = client.create(context, EmpCompFactory)

            If ent.Length > 0 Then
                Dim tmp1, tmp2 As String
                tmp1 = ent(0).KeyData(0).Field
                tmp2 = ent(0).KeyData(0).Value
                Dim DAXRecId As String = tmp2
                Dim UpdateQuery As String = "SAVE_SALARY_FINAL_SETTLEMENT_DAX_ID " & Me.hfEmpId.Value & ", '" & Me.hfBsuId.Value & "', '" & DAXRecId & "'"
                SqlHelper.ExecuteNonQuery(tr, CommandType.Text, UpdateQuery)
            End If

            Return True

        Catch ex As Exception
            'Me.ShowErrorMessage("Following error occured in DAX service: " & ex.Message)
            ServiceErrorMsg = "Following error occured in DAX service: " & ex.Message
            Throw ex 'throw back the error to the caller and let it be handled there
            Return False
        Finally
        End Try
    End Function

    Public Shared Sub RemoveArrayItem(Of T)(ByRef arr As T(), index As Integer)
        For a As Integer = index To arr.Length - 2
            ' moving elements downwards, to fill the gap at [index]
            arr(a) = arr(a + 1)
        Next
        ' finally, let's decrement Array's size by one
        Array.Resize(arr, arr.Length - 1)
    End Sub



    Private Sub LogError(ByVal ex As Exception, ByVal Err_Page As String, ByVal Err_Source As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim conn As SqlConnection = New SqlConnection(str_conn)
        Try
            Dim str_query As String = "EXEC OASIS_DAX_AUDIT.dbo.LOG_ERROR @ERR_PAGE = '" & Err_Page & "', " & _
                                       " @ERR_SOURCE = '" & Err_Source & "', " & _
                                       " @ERR_DESCRIPTION = '" & ex.Message & ". " & ex.StackTrace.ToString & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch exc As Exception

        Finally

        End Try
    End Sub

    Private Sub ShowErrorMessage(ByVal Err_Msg As String)
        trMsg.Visible = True
        lblError.Text = Err_Msg
        lblError.ForeColor = Drawing.Color.DarkRed
    End Sub

    Private Sub ShowSuccessMessage(ByVal Msg As String)
        If trMsg.Visible = False Then trMsg.Visible = True
        lblError.Text = Msg
        lblError.ForeColor = Drawing.Color.DarkRed
    End Sub

    Private Function PositionExists(ByVal PosId As String) As Boolean
        PositionExists = False
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim conn As SqlConnection = New SqlConnection(str_conn)
        Dim count As Integer
        Try
            Dim str_query As String = "SELECT ISNULL(COUNT(POS_ID),0) AS 'COUNT' FROM dbo.POSITION_M WHERE POS_ID = '" & PosId & "'"
            count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If count > 0 Then
                Return True
            Else
                Me.ShowErrorMessage("Cannot find any position with the position id: " & PosId)
                Return False
            End If
        Catch exc As Exception
            Me.ShowErrorMessage("Error occured while getting position details")
            Return False
        Finally

        End Try
    End Function

    Private Sub ShowInfoLog(ByVal Msg As String, Optional ByVal Msg_Type As String = "W")
        If trMsg.Visible = False Then trMsg.Visible = True
        lblError.Text = Msg
        lblError.ForeColor = Drawing.Color.DarkRed
        'lblError.Focus()
        Page.SetFocus(Me.gvCompensation)
        'ScriptManager1.RegisterStartupScript(Me, Me.[GetType](), "SetFocus", "<script>document.getElementById('" + lblError.ClientID + "').focus();</script>", False)
        'Page.SetFocus(Me.txtCompAmount)
        'Page.SetFocus(lblError.ClientID)
        'ScriptManager1.RegisterStartupScript(Me, Me.[GetType](), "ScrollToTop", "ScrollToTop();", True)

        'Session("LAST_MSG") = Msg
        'Session("LAST_MSG_TYPE") = Msg_Type
        'ScriptManager1.RegisterStartupScript(Me, Me.[GetType](), "currcompinfo", "ShowInfoLog();", True)
    End Sub

    Private Function DeleteFromDAX_Employee(ByVal RecId As String, ByRef ServiceErrorMsg As String) As Boolean
        DeleteFromDAX_Employee = False
        'Private Sub SendToDAX(ByVal RecId As Integer, ByVal PosId As String, ActionState As String, CompensationId As String, GradeId As String, ValidFrom As String, ValidTo As String, gvCompensation As GridView)
        Try
            'Dim client As New EmployeeCompensationService.ALE_HcmempCompensationServiceClient
            Dim client As New ALE_EmpCompServiceClient

            'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\swapna.tv"
            'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"

            client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
            client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString

            Dim context As New CallContext
            'context.Company = "GCO" 'Disable this value when not in demo mode and pass actual ax bsu id or legal entity

            context.Company = Me.hfCompany.Value

            Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count + 6 + 1 '+ gvJoiningAllowance.Rows.Count

            Dim EmpCompFactory As New AxdALE_EmpComp
            Dim EmpComps(0) As AxdEntity_ALE_EmployeeCompensation
            Dim EmpComp As AxdEntity_ALE_EmployeeCompensation = Nothing
            Dim EmpCompLines(EmpCompLinesCount - 1) As AxdEntity_ALE_EmployeeCompensationLine
            Dim EmpCompLine As AxdEntity_ALE_EmployeeCompensationLine = Nothing

            Dim Criteria As New QueryCriteria
            Dim CriteriaElements(0) As CriteriaElement
            Dim CriteriaElement As New CriteriaElement

            'CriteriaElement.DataSourceName = "empComp"
            CriteriaElement.DataSourceName = "ALE_EmployeeCompensation"
            CriteriaElement.FieldName = "RecId"
            CriteriaElement.Operator = EmployeeCompensationService.Operator.Equal
            CriteriaElement.Value1 = RecId
            CriteriaElement.Value2 = Nothing
            CriteriaElements(0) = CriteriaElement
            Criteria.CriteriaElement = CriteriaElements
            EmpCompFactory = client.find(context, Criteria)

            If Not EmpCompFactory Is Nothing AndAlso Not EmpCompFactory.ALE_EmployeeCompensation Is Nothing AndAlso EmpCompFactory.ALE_EmployeeCompensation.Length > 0 Then
                EmpComps = EmpCompFactory.ALE_EmployeeCompensation
                EmpComp = EmpComps(0)
                EmpComp.class = "entity" 'This value is hard coded as suggested in the service documentation
                'EmpComp.action = EmployeeCompensationService.AxdEnum_AxdEntityAction.update
                EmpComp.action = EmployeeCompensationService.AxdEnum_AxdEntityAction.delete
                EmpComp.actionSpecified = True

                'Now delete final settlement component from Employee compensation details
                ReDim EmpCompLines(EmpComp.ALE_EmployeeCompensationLine.Length)
                EmpCompLines = EmpComp.ALE_EmployeeCompensationLine

                For i As Integer = 0 To EmpCompLines.Length - 1
                    EmpCompLines(i).action = EmployeeCompensationService.AxdEnum_AxdEntityAction.delete
                    EmpCompLines(i).actionSpecified = True
                Next

                Dim ent(0) As EmployeeCompensationService.EntityKey
                Dim KeyData(0) As EmployeeCompensationService.KeyField
                ent(0) = New EmployeeCompensationService.EntityKey
                KeyData(0) = New EmployeeCompensationService.KeyField
                KeyData(0).Field = "RecId"
                KeyData(0).Value = RecId
                ent(0).KeyData = KeyData
                client.update(context, ent, EmpCompFactory)
            End If

            Return True

        Catch ex As Exception
            'Me.ShowInfoLog("Following error occured in DAX service: " & ex.Message, "E")
            ServiceErrorMsg = "Following error occured in DAX service: " & ex.Message
            Throw ex 'throw back the error to the caller and let it be handled there
            Return False
        Finally
        End Try
    End Function

    Private Sub TestFunch()

    End Sub

    Private Function EmployeeCompensationService() As Object
        Throw New NotImplementedException
    End Function


End Class