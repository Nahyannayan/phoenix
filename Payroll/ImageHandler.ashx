﻿<%@ WebHandler Language="VB" Class="ImageHandler" %>

Imports System
Imports System.Web
Imports System.Data
Imports System.Web.Configuration
Public Class ImageHandler : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim sql As String
        Dim imgByte() As Byte = Nothing
        Dim NoImage As Boolean
        Try
            If context.Request.QueryString("TYPE") = "EDD" Then
                sql = "select EDD_PHOTO from EMPDEPENDANTS_D where EDD_ID=" & context.Request.QueryString("ID")
                Dim Pictable As New DataTable
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Pictable = Mainclass.getDataTable(sql, str_conn)

                If Pictable.Rows.Count > 0 Then
                    If Not Pictable.Rows(0).Item(0) Is Nothing And Not IsDBNull(Pictable.Rows(0).Item(0)) Then
                        imgByte = Pictable.Rows(0).Item(0)
                        context.Response.BinaryWrite(imgByte)
                    End If
                End If
            ElseIf context.Request.QueryString("TYPE") = "PATH" Then
                Dim Path As String
                Path = context.Request.QueryString("ID")
                If IO.File.Exists(Path) Then
                    imgByte = EOS_MainClass.ConvertImageFiletoBytes(Path)
                Else
                    NoImage = True
                End If
            End If
            If NoImage Or imgByte Is Nothing Then
                imgByte = EOS_MainClass.ConvertImageFiletoBytes(WebConfigurationManager.AppSettings.Item("NoImagePath"))
            End If
            If Not imgByte Is Nothing Then
                context.Response.BinaryWrite(imgByte)
            End If
        Catch ex As Exception

        End Try
       
    End Sub
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class