﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Partial Class Payroll_empTutionFeeConcessionReqDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Property EMP_ID() As String
        Get
            Return ViewState("EMP_ID")
        End Get
        Set(value As String)
            ViewState("EMP_ID") = value
        End Set
    End Property

    Public Property STU_ID() As String
        Get
            Return ViewState("STU_ID")
        End Get
        Set(value As String)
            ViewState("STU_ID") = value
        End Set
    End Property

    Public Property SCR_ID() As String
        Get
            Return ViewState("SCR_ID")
        End Get
        Set(value As String)
            ViewState("SCR_ID") = value
        End Set
    End Property

    Public Property SCD_ID() As String
        Get
            Return ViewState("SCD_ID")
        End Get
        Set(value As String)
            ViewState("SCD_ID") = value
        End Set
    End Property

    Public Property STATUS() As String
        Get
            Return ViewState("STATUS")
        End Get
        Set(value As String)
            ViewState("STATUS") = value
        End Set
    End Property

    Public Property bTRANSPORT() As Boolean
        Get
            Return ViewState("bTRANSPORT")
        End Get
        Set(value As Boolean)
            ViewState("bTRANSPORT") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Me.EMP_ID = IIf(Request.QueryString("EMP_ID").Replace(" ", "+") <> "", Encr_decrData.Decrypt(Request.QueryString("EMP_ID").Replace(" ", "+")), "0")
                Me.STU_ID = IIf(Request.QueryString("STU_ID").Replace(" ", "+") <> "", Encr_decrData.Decrypt(Request.QueryString("STU_ID").Replace(" ", "+")), "0")
                Me.SCR_ID = IIf(Request.QueryString("SCR_ID").Replace(" ", "+") <> "", Encr_decrData.Decrypt(Request.QueryString("SCR_ID").Replace(" ", "+")), "0")
                Me.SCD_ID = IIf(Request.QueryString("SCD_ID").Replace(" ", "+") <> "", Encr_decrData.Decrypt(Request.QueryString("SCD_ID").Replace(" ", "+")), "0")
                Me.STATUS = IIf(Request.QueryString("STATUS").Replace(" ", "+") <> "", Encr_decrData.Decrypt(Request.QueryString("STATUS").Replace(" ", "+")), "0")
                Me.bTRANSPORT = IIf(Request.QueryString("bTRANSPORT").Replace(" ", "+") <> "", Encr_decrData.Decrypt(Request.QueryString("bTRANSPORT").Replace(" ", "+")), "0")

                If EMP_ID <> 0 And STU_ID <> 0 Then
                    usrParentChildProfile1.EMP_ID = EMP_ID
                    usrParentChildProfile1.STU_ID = STU_ID
                    usrParentChildProfile1.Bind_Student_Info()
                    usrParentChildProfile1.Bind_Employee_Info()

                    GetRequestDetail()
                End If
                If STATUS = "P" Or bTRANSPORT Then
                    Me.btnApprove.Enabled = True
                    Me.btnReject.Enabled = True
                    disablecontrols(True)
                Else
                    Me.btnApprove.Enabled = False
                    Me.btnReject.Enabled = False
                    disablecontrols(False)
                End If
                'Me.btnClose.Attributes.Add("onClick", "fancyClose();")
                'Me.btnCloseedit.Attributes.Add("onClick", "fancyClose();")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As System.EventArgs) Handles btnApprove.Click
        'Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        'objConn.Open()
        'Dim Trans As SqlTransaction = objConn.BeginTransaction("APRVE")
        'Try
        '    'Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION

        '    'objcls.PROCESS = "A"
        '    'objcls.User = Session("sUsr_name")
        '    'objcls.SCR_SCD_IDs = SCD_ID
        '    'objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
        '    'objcls.bTRANSPORT = bTRANSPORT

        '    Dim retval As String = Approve_Employee_Child_ConcessionRequest(objConn, Trans)
        '    If retval <> "0" Then
        '        Me.lblPopError.Text = UtilityObj.getErrorMessage(retval)
        '        Trans.Rollback()
        '    Else
        '        Trans.Commit()
        '        disablecontrols(False)
        '        Me.lblPopError.Text = "Selected Request(s) have been approved"
        '    End If
        'Catch ex As Exception
        '    Me.lblPopError.Text = "Unable to approve"
        '    Trans.Rollback()
        'Finally
        '    If objConn.State = ConnectionState.Open Then
        '        objConn.Close()
        '    End If
        'End Try
    End Sub
    

    Protected Sub btnReject_Click(sender As Object, e As System.EventArgs) Handles btnReject.Click
        Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim Trans As SqlTransaction = objConn.BeginTransaction("REJECT")
        Try
            objcls.PROCESS = "R"
            objcls.User = Session("sUsr_name")
            objcls.SCR_SCD_IDs = SCR_ID & "#" & SCD_ID
            objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
            objcls.bTRANSPORT = bTRANSPORT
            Dim retval As String = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
            If retval <> "0" Then
                Me.lblPopError.Text = UtilityObj.getErrorMessage(retval)
                Trans.Rollback()
            Else
                Trans.Commit()
                disablecontrols(False)
                Me.lblPopError.Text = "Selected Request(s) have been rejected"
            End If
        Catch ex As Exception
            Me.lblPopError.Text = "Unable to Reject"
            Trans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub disablecontrols(ByVal show As Boolean)
        Me.btnReject.Visible = show
        Me.btnApprove.Visible = show
    End Sub

    Sub GetRequestDetail()
        'Dim qry As String = "SELECT * FROM FEES.vw_STUDENT_CONCESSION_REQUEST WHERE SCR_ID=" & SCR_ID & " AND SCD_ID=" & SCD_ID & ""
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        'If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
        '    Me.lblGrossFees.Text = ds.Tables(0).Rows(0)("SCD_GROSS_AMT")
        '    Me.lblConcession.Text = ds.Tables(0).Rows(0)("SCD_CONCESSION_AMT")
        'End If
    End Sub

End Class

