Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Payroll_empBankLoan
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'version            author                Date          change
    '1.1                Swapna              13-mar-2012     added automatic bank details filling
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache") 

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P130053") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtEmpName.Attributes.Add("readonly", "readonly")
                    txtBName.Attributes.Add("readonly", "readonly")
                    txtAccno.Attributes.Add("readonly", "readonly") 'V1.1
                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        btnEmp_Name.Visible = False
                        Using EmpLoan_IDreader As SqlDataReader = AccessPayrollClass.GetEmpLoan_ID(ViewState("viewid"))
                            While EmpLoan_IDreader.Read
                                hfEmp_ID.Value = Convert.ToString(EmpLoan_IDreader("EMP_ID"))
                                txtEmpName.Text = Convert.ToString(EmpLoan_IDreader("EName"))
                                txtFrom_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((EmpLoan_IDreader("DTFROM"))))
                                If EmpLoan_IDreader("DTTO").ToString <> "" Then
                                    txtTo_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((EmpLoan_IDreader("DTTO"))))
                                End If
                                txtBName.Text = Convert.ToString(EmpLoan_IDreader("Bname"))
                                txtBranchName.Text = Convert.ToString(EmpLoan_IDreader("Branch"))
                                hfBNK_ID.Value = Convert.ToString(EmpLoan_IDreader("BNK_ID"))
                                txtAccno.Text = Convert.ToString(EmpLoan_IDreader("EBL_ACCNUM"))
                                txtRemark.Text = Convert.ToString(EmpLoan_IDreader("REMARKS"))
                                chkLActive.Checked = Convert.ToBoolean(EmpLoan_IDreader("bActive"))
                                chkTransfer.Checked = Convert.ToBoolean(EmpLoan_IDreader("Sal_tran"))
                                txtAmount.Text = Convert.ToString(EmpLoan_IDreader("Amount"))
                            End While
                            'clear of the resource end using
                        End Using
                    ElseIf ViewState("datamode") = "add" Then
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog("Bank Loan", ex.Message)
            End Try
        End If
    End Sub
    Sub setButton()
        btnBankName.Visible = False
        btnEmp_Name.Visible = False

    End Sub
    Sub resetButton()
        btnBankName.Visible = True
        btnEmp_Name.Visible = True
    End Sub


    Sub clearRecords()
        txtAccno.Text = ""
        txtEmpName.Text = ""
        txtAmount.Text = ""
        txtBName.Text = ""
        txtBranchName.Text = ""
        txtFrom_date.Text = ""
        txtTo_date.Text = ""
        txtRemark.Text = ""
        chkLActive.Checked = False
        chkTransfer.Checked = False
        hfEmp_ID.Value = ""
        hfBNK_ID.Value = ""
        ViewState("viewid") = 0
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            ViewState("datamode") = "add"
            Call clearRecords()
            resetButton()

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("Bank Loan", ex.Message)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"

        resetButton()
        btnEmp_Name.Visible = False
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearRecords()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            setButton()
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then

            Dim EBL_ID As Integer = 0
            Dim EBL_EMP_ID As Integer = hfEmp_ID.Value
            Dim EBL_BNK_ID As Integer = hfBNK_ID.Value
            Dim EBL_BRANCH As String = txtBranchName.Text
            Dim EBL_REMARKS As String = txtRemark.Text
            Dim EBL_FROMDT As Date = txtFrom_date.Text
            Dim EBL_TODT As String = txtTo_date.Text
            Dim EBL_AMOUNT As String = Val(txtAmount.Text)
            Dim EBL_bActive As Boolean = chkLActive.checked
            Dim EBL_bSALARYTRANSFER As Boolean = chkTransfer.Checked
            Dim EBL_BSU_ID As String = Session("sBsuid")
            If EBL_TODT <> "" Then
                If IsDate(EBL_TODT) = False Then
                    lblError.Text = "Please Enter Valid Date"
                    Exit Sub
                End If
            End If
            'V1.1
            If hfBNK_ID.Value = "" Then
                lblError.Text = "No Bank data available."
                Exit Sub
            End If
            If txtAccno.Text = "" Then
                lblError.Text = "No Account No. data available."
                Exit Sub
                'ElseIf txtAccno.Text.Trim.Length <> 23 Then
                '    lblError.Text = "Account No. must have 23 digits."
                '    Exit Sub

            End If
            Dim status As Integer
            Dim transaction As SqlTransaction


            If ViewState("datamode") = "add" Then

                'get the max Id to be inserted in audittrial report
                Dim sqlBankID As String = "Select isnull(max(EBL_ID),0)+1  from EMPBANKLOAN_D"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBankID, connection)
                Dim temp As String
                command.CommandType = CommandType.Text
                temp = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()


                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into EMPBANKLOAN_D
                        status = AccessPayrollClass.SaveEMPBANKLOAN_D(EBL_ID, EBL_EMP_ID, EBL_BNK_ID, EBL_BRANCH, EBL_REMARKS, _
                                            EBL_FROMDT, EBL_TODT, EBL_AMOUNT, EBL_bActive, EBL_bSALARYTRANSFER, EBL_BSU_ID, txtAccno.Text, transaction)

                        'throw error if insert to the EMPBANKLOAN_D is not Successfully

                        If status <> 0 Then
                            Throw New ArgumentException("Record could not be Inserted")
                        End If

                        'insert record in EMPBANKLOAN_D

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EBL_EMP_ID & "-" & temp, "Insert", Page.User.Identity.Name.ToString)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        lblError.Text = "Record Inserted Successfully"

                        ViewState("datamode") = "none"

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        Call clearRecords()
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = ex.Message ' "Record could not be Inserted"

                    End Try
                End Using

            ElseIf ViewState("datamode") = "edit" Then
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into EMPEXPENSES_D
                        EBL_ID = ViewState("viewid")
                        status = AccessPayrollClass.SaveEMPBANKLOAN_D(EBL_ID, EBL_EMP_ID, EBL_BNK_ID, EBL_BRANCH, EBL_REMARKS, _
                                    EBL_FROMDT, EBL_TODT, EBL_AMOUNT, EBL_bActive, EBL_bSALARYTRANSFER, EBL_BSU_ID, txtAccno.Text, transaction)
                        'throw error if insert to the EMPLVLADJUSTMENT_H & EMPLVLADJUSTMENT_D is not Successfully

                        If status <> 0 Then
                            Throw New ArgumentException("Record could not be Updated")
                        End If

                        'insert record for the last month

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EBL_EMP_ID & "-" & EBL_ID, "edit", Page.User.Identity.Name.ToString, Me.Page)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        lblError.Text = "Record Updated Successfully"

                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearRecords()
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = ex.Message '"Record could not be Updated"

                    End Try
                End Using

            End If


        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try


                Status = AccessPayrollClass.Delete_EBL_ID(ViewState("viewid"), transaction)


                If Status = -1 Then

                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then


                    Throw New ArgumentException("Record could not be Deleted")

                Else

                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)


                    If Status <> 0 Then


                        Throw New ArgumentException("Could not complete your request")

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()

                    Call clearRecords()
                    lblError.Text = "Record Deleted Successfully"

                End If

            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message)
                transaction.Rollback()

            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using
    End Sub
    'V1.1
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmp_Name.Click
        Try
            lblError.Text = ""
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As New DataSet()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid").ToString

            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = hfEmp_ID.Value

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getEmpBankDetails", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                txtBName.Text = ds.Tables(0).Rows(0)("BANK_DESCR").ToString
                txtAccno.Text = ds.Tables(0).Rows(0)("EMP_ACCOUNT").ToString
                hfBNK_ID.Value = ds.Tables(0).Rows(0)("EMP_BANK").ToString

            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    'ts
    Protected Sub txtEmpName_TextChanged(sender As Object, e As EventArgs)
        Try
            lblError.Text = ""
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As New DataSet()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid").ToString

            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = hfEmp_ID.Value

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getEmpBankDetails", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                txtBName.Text = ds.Tables(0).Rows(0)("BANK_DESCR").ToString
                txtAccno.Text = ds.Tables(0).Rows(0)("EMP_ACCOUNT").ToString
                hfBNK_ID.Value = ds.Tables(0).Rows(0)("EMP_BANK").ToString

            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class
