﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports UtilityObj
Partial Class Payroll_OT_Remove_Approved_overtime_aspx
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'ClientScript.RegisterStartupScript(Me.GetType(),
            '"script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ddlBSUinit.Enabled = False
                SetTransferDetail()
            End If

            'txtFromDate.Text = Format(New Date(Now.Date.Year, Now.Date.Month, 1), OASISConstants.DateFormat)
            'txtToDate.Text = Format(New Date(Now.Date.Year, Now.Date.Month, Date.DaysInMonth(Now.Date.Year, Now.Date.Month)), OASISConstants.DateFormat)
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub SetTransferDetail()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(0) As SqlClient.SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        Dim ds As DataSet
        txtFromDate.Enabled = False
        txtToDate.Enabled = False
        imgFromDate.Enabled = False
        imgToDate.Enabled = False
        lblError.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        ds = Mainclass.getDataSet("Get_BSU_OTFreezeDates", param, str_conn)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("ReturnValue").ToString = "" Then
                    If IsDate(ds.Tables(0).Rows(0).Item("TODT")) Then
                        txtToDate.Text = Format(ds.Tables(0).Rows(0).Item("TODT"), OASISConstants.DateFormat)
                    End If
                    If IsDate(ds.Tables(0).Rows(0).Item("FROMDT")) Then
                        txtFromDate.Text = Format(ds.Tables(0).Rows(0).Item("FROMDT"), OASISConstants.DateFormat)
                    End If
                    If IsNumeric(ds.Tables(0).Rows(0).Item("PAYMONTH")) And IsNumeric(ds.Tables(0).Rows(0).Item("PAYYEAR")) Then
                        'LoadSummary(ds.Tables(0).Rows(0).Item("PAYMONTH"), ds.Tables(0).Rows(0).Item("PAYYEAR"))
                    End If
                    'If ds.Tables(0).Rows(0).Item("AllowReTransfer") Then
                    '    btnRemoveOT.Enabled = True
                    'Else
                    '    btnRemoveOT.Enabled = False
                    'End If
                    btnRemoveOT.Visible = True
                Else
                    lblError.Text = ds.Tables(0).Rows(0).Item("ReturnValue").ToString
                    If IsDate(ds.Tables(0).Rows(0).Item("TODT")) Then
                        txtToDate.Text = Format(ds.Tables(0).Rows(0).Item("TODT"), OASISConstants.DateFormat)
                    End If
                    If IsDate(ds.Tables(0).Rows(0).Item("FROMDT")) Then
                        txtFromDate.Text = Format(ds.Tables(0).Rows(0).Item("FROMDT"), OASISConstants.DateFormat)
                    End If
                    'btnRemoveOT.Enabled = False
                    btnRemoveOT.Visible = True
                End If
            End If
        Else
            btnRemoveOT.Enabled = False
            btnRemoveOT.Visible = False
            lblError.Text = "Payroll freeze data not initiated."
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnRemoveOT_Click(sender As Object, e As EventArgs) Handles btnRemoveOT.Click
        If txtFromDate.Text = "" Then
            lblError.Text = "From date & To date are required."
            Exit Sub
        End If
        If txtToDate.Text = "" Then
            lblError.Text = "From date & To date are required."
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim count As Integer = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim returnVal As Integer
        Try
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_Id", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")
            pParms(1) = New SqlClient.SqlParameter("@FromDate", SqlDbType.VarChar)
            pParms(1).Value = txtFromDate.Text
            pParms(2) = New SqlClient.SqlParameter("@ToDate", SqlDbType.VarChar)
            pParms(2).Value = txtToDate.Text
            If chkboxClearBioMetricData.Checked = True Then
                pParms(3) = New SqlClient.SqlParameter("@ClearBioMetricDataFlag", SqlDbType.VarChar)
                pParms(3).Value = 1
            Else pParms(3) = New SqlClient.SqlParameter("@ClearBioMetricDataFlag", SqlDbType.VarChar)
                pParms(3).Value = 0
            End If
            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            Dim cmd As New SqlCommand
            cmd.Parameters.Add(pParms(0))
            cmd.Parameters.Add(pParms(1))
            cmd.Parameters.Add(pParms(2))
            cmd.Parameters.Add(pParms(3))
            cmd.Parameters.Add(pParms(4))

            cmd.Connection = objConn
            cmd.Transaction = stTrans
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "OT_Calculation"
            cmd.CommandTimeout = 0

            returnVal = cmd.ExecuteNonQuery
            returnVal = pParms(4).Value
            If returnVal = 0 Then
                stTrans.Commit()
                lblError.Text = "Data removed sucessfully"
                'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alert", "alert('Data removed sucessfully');Response.Redirect(" + Request.RawUrl + ")", True)

            Else lblError.Text = getErrorMessage(returnVal)
                stTrans.Rollback()
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            If returnVal <> 0 Then
                stTrans.Rollback()
            End If
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
