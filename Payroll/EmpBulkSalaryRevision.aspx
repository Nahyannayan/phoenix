<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empBulkSalaryRevision.aspx.vb" Inherits="empBulkSalaryRevision" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

    <script type="text/javascript">

        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
}
window.onload = scroll_page;

        <%--function GetEMPNAME() {
    var sFeatures;
    sFeatures = "dialogWidth: 729px; ";
    sFeatures += "dialogHeight: 445px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    result = window.showModalDialog("Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)
    if (result != '' && result != undefined) {
        document.getElementById('<%=hf_EMP_IDs.ClientID %>').value = result;//NameandCode[0];
        return true;
    }
    else {
        return false;
    }
}--%>

<%--function GetSalaryGrade() {
    var sFeatures;
    sFeatures = "dialogWidth: 590px; ";
    sFeatures += "dialogHeight: 370px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: no; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var url;
    url = 'empShowOtherDetail.aspx?id=SG';
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) {
        return false;
    }
    else {
        NameandCode = result.split('___');
        document.getElementById('<%=txtSalGrade.ClientID %>').value = NameandCode[0];
             document.getElementById('<%=hfSalGrade.ClientID %>').value = NameandCode[1];
         }
     }--%>

        function ViewDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            result = window.showModalDialog("PopupSalaryCompareDetails.aspx?" + url, "", sFeatures)

        }

    </script>
    <script>
        function ViewDetails(url1) {
            var url;
            url = "PopupSalaryCompareDetails.aspx?" + url1
            var oWnd = radopen(url, "pop_view");
        }
        function GetSalaryGrade() {
            var url;
            url = 'empShowOtherDetail.aspx?id=SG';
            var oWnd = radopen(url, "pop_salary");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtSalGrade.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=hfSalGrade.ClientID %>').value = NameandCode[1];
            }
        }

        function GetEMPNAME() {
            var url;
            url = "Reports/Aspx/SelIDDESC.aspx?ID=EMP"
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                // NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hf_EMP_IDs.ClientID %>').value = arg.NameandCode;
                document.getElementById('<%=txtEmpNo.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%=txtEmpNo.ClientID%>', '');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_salary" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_view" runat="server" Behaviors="Close,Move" OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Salary Revision"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALEDITSAL" />
                <table id="Table1" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Select Employees</span></td>
                        <td width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server" OnTextChanged="txtEmpNo_TextChanged"></asp:TextBox><asp:ImageButton ID="imgEMPSel" runat="server"
                                ImageUrl="~/Images/cal.gif" OnClientClick="GetEMPNAME(); return false" /></td>
                        <td width="20%">
                            <span class="field-label">Revision Date</span></td>
                        <td width="30%">
                            <asp:TextBox ID="txtRevisionDate"
                                runat="server"></asp:TextBox><asp:ImageButton ID="imgdatepick"
                                    runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(550, 310, 0);" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRevisionDate"
                                ErrorMessage="Revision Date required" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtRevisionDate"
                                EnableViewState="False" ErrorMessage="Enter the Revision Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator></td>
                    </tr>
                </table>
                <table id="tblSalDetails" runat="server" width="100%">
                    <tr>
                        <td width="60%" class="title-bg">Selected Employees</td>
                        <td width="40%" class="title-bg">Salary change</td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="top">
                            <table width="100%">
                                <tr>
                                    <td valign="top">

                                        <asp:GridView ID="gvSalRevisionBulk" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                            PageSize="15" EmptyDataText="No Employees selected" Width="100%">
                                            <EmptyDataRowStyle Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="EMP NO">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Employee No<br />
                                                        <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EMP NAME">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Employee Name<br />
                                                        <asp:TextBox ID="txtEmpName" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                                        <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cur_SalaryScale">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrSalAmt" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Current Salary<br />
                                                        <asp:TextBox ID="txtMonth" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Revised_SalScale">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRevSalAmt" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Revised Salary Amount
                                                    </HeaderTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" class="matters" width="100%" valign="top">
                            <table id="tblSalary" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters" colspan="3">
                                        <asp:RadioButton ID="radSalScale" runat="server" Checked="True" GroupName="SalChange" CssClass="field-label"
                                            Text="Salary Scale" AutoPostBack="True" />
                                        <asp:RadioButton ID="radAddAmount" runat="server" GroupName="SalChange" Text="Add Amount" AutoPostBack="True" CssClass="field-label" /></td>
                                </tr>
                                <tr id="trSalScale1">
                                    <td align="left" class="title-bg" colspan="3">Change Salary Scale</td>
                                </tr>
                                <tr id="trSalScale2">
                                    <td align="left" class="matters" colspan="3"><span class="field-label">Select Salary Scale</span>
                                        <asp:TextBox ID="txtSalGrade" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnSalScale" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClientClick="GetSalaryGrade();return false" />

                                    </td>
                                </tr>
                                <tr id="trSalScale3">
                                    <td align="left" class="matters" valign="top" colspan="3">
                                        <asp:GridView ID="gvEmpSalary" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered" EmptyDataText="No Transaction details added yet." Width="100%" PageSize="5">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENR ID" Visible="False">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Earing">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblERN_DESCR" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount(Eligible)">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmountEligible" runat="server" Text='<%#AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY"))%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount(Actual)">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="bMonthly" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBMonthly" runat="server" Text='<%#bind("bMonthly") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PayTerm" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPayTerm" runat="server" Text='<%# bind("PAYTERM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Schedule">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSchedule" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trEditSalAmt">
                                    <td align="left" class="title-bg" style="height: 20px" colspan="3">Edit Amount</td>
                                </tr>
                                <tr id="trEditSalAmt1">
                                    <td align="left" class="matters"><span class="field-label">Earning</span></td>
                                    <td align="left" width="54%" class="matters">
                                        <asp:DropDownList ID="ddSalEarnCode" runat="server" ValidationGroup="VALADDNEWSAL">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="trEditSalAmt2">
                                    <td align="left" class="matters"><span class="field-label">Amount (Added to Earning)</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtSalAmount" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSalAmount"
                                            ErrorMessage="Enter the Salary Amount" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                        <br />
                                        <asp:CheckBox ID="chkSalPayMonthly" runat="server" Text="Per Month" CssClass="field-label" />
                                    </td>
                                </tr>
                                <tr id="trEditSalAmt3">
                                    <td align="center" colspan="3" style="text-align: right">
                                        <asp:Button ID="btnSalAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALADDNEWSAL" />
                                        <asp:Button ID="btnSalCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                </tr>
                                <tr id="trEditSalAmt4">
                                    <td align="center" colspan="3" style="text-align: right">
                                        <asp:GridView ID="gvAddSalaryDetails" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." Width="100%">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENR ID" Visible="False">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Earing">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblERN_DESCR" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount(Added)">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkSalEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="lnkSalEdit_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="text-align: left"><span class="field-label">Gross Salary</span></td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtGrossSalary" runat="server" ReadOnly="True"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="left" class="matters" colspan="2" style="text-align: center" valign="top"
                            width="60%">
                            <asp:Button ID="btnSalRevSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALEDITSAL" />
                            <asp:Button ID="btnSalRevCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hf_EMP_IDs" runat="server" />
                <asp:HiddenField ID="hfSalGrade" runat="server" />
                <input id="h_Grid" runat="server" type="hidden" value="top" />
                <input id="h_SelectedId"
                    runat="server" type="hidden" value="-1" />
                <input id="h_selected_menu_1" runat="server"
                    type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden"
                    value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input
                    id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server"
                    type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden"
                    value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgdatepick"
                    TargetControlID="txtRevisionDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

