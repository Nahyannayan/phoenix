Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Payroll_empLeaveAdj_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    '---------Needs to be worked out -----------------
    'adjust the emp name size in the grid view
    'Check for the duplication  entry in terms oof the between date of that employee
    '---------Needs to be worked out -----------------

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                hlAddNew.NavigateUrl = "empLeaveAdj.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvPrePayment.Attributes.Add("bordercolor", "#1b80b6")
                If USR_NAME = "" Or MainMnu_code <> "P130030" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    '  h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    ' h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_9.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_10.Value = "LI__../Images/operations/like.gif"
                    rbPositive.Checked = True
                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_2.Value.Split("__")
        ' getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_7.Value.Split("__")
        'getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_9.Value.Split("__")
        getid9(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid9(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid10(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_10_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim AdjAddType As String = String.Empty
            If rbPositive.Checked = True Then
                AdjAddType = " And a.AdjAdd=1 "
            ElseIf rbNegative.Checked = True Then
                AdjAddType = " And a.AdjAdd=0 "
            End If
            Dim str_Sql As String = String.Empty
            Dim str_filter_EmpName As String = String.Empty
            Dim str_filter_LeaveType As String = String.Empty
            Dim str_filter_year As String = String.Empty
            Dim str_filter_Month As String = String.Empty
            Dim str_filter_FromDate As String = String.Empty
            Dim str_filter_ToDate As String = String.Empty
            Dim str_filter_Remark As String = String.Empty
            Dim str_filter_EmpNo As String = String.Empty
            Dim ds As New DataSet
            str_Sql = "select EAH_ID,BSU_NAME,EMPNO,EName,DTFROM,DTTO,PAYYEAR,REMARKS,DESCR,PAYMONTH from(SELECT  EMPLVLADJUSTMENT_H.EAH_ID as EAH_ID," & _
            " BUSINESSUNIT_M.BSU_ID  as BSU_NAME,isnull(EMPLOYEE_M.EMP_FNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_MNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_LNAME,'') as EName, Empno ," & _
            "EMPLVLADJUSTMENT_H.EAH_DTFROM as DTFROM,EMPLVLADJUSTMENT_H.EAH_DTTO as DTTO,EMPLVLADJUSTMENT_H.EAH_PAYYEAR as PAYYEAR,EMPLVLADJUSTMENT_H.EAH_REMARKS as REMARKS, " & _
            "EMPLEAVETYPE_M.ELT_DESCR as DESCR,EMPLVLADJUSTMENT_H.EAH_ELT_ID as ELT_ID, EMPLVLADJUSTMENT_H.EAH_PAYMONTH as PAYMONTH,EMPLVLADJUSTMENT_H.EAH_bADJADD as AdjAdd " & _
            "FROM  BUSINESSUNIT_M INNER JOIN EMPLVLADJUSTMENT_H ON BUSINESSUNIT_M.BSU_ID = EMPLVLADJUSTMENT_H.EAH_BSU_ID INNER JOIN " & _
            "EMPLOYEE_M ON EMPLVLADJUSTMENT_H.EAH_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN EMPLEAVETYPE_M ON EMPLVLADJUSTMENT_H.EAH_ELT_ID = EMPLEAVETYPE_M.ELT_ID)a where a.BSU_NAME='" & Session("sBsuid") & "' and   a.EAH_ID<>''"
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_EmpName As String = String.Empty
            Dim str_LeaveType As String = String.Empty
            Dim str_Year As String = String.Empty
            Dim str_Month As String = String.Empty
            Dim str_FromDate As String = String.Empty
            Dim str_ToDate As String = String.Empty
            Dim str_Remark As String = String.Empty
            Dim str_EmpNo As String = String.Empty
            If gvPrePayment.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtEmpName")
                str_EmpName = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_EmpName = " AND a.EName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EmpName = " AND a.EName NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EmpName = " AND a.EName LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EmpName = " AND a.EName NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EmpName = " AND a.EName LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EmpName = " AND a.EName NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtLeaveType")
                str_LeaveType = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_LeaveType = " AND a.DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_LeaveType = "  AND  NOT a.DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_LeaveType = " AND a.DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_LeaveType = " AND a.DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_LeaveType = " AND a.DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_LeaveType = " AND a.DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtFromdate")
                str_FromDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_FromDate = " AND a.DTFROM LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FromDate = "  AND  NOT a.DTFROM LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FromDate = " AND a.DTFROM  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FromDate = " AND a.DTFROM NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FromDate = " AND a.DTFROM LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FromDate = " AND a.DTFROM NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtToDate")
                str_ToDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_ToDate = " AND a.DTTO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ToDate = "  AND  NOT a.DTTO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ToDate = " AND a.DTTO  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ToDate = " AND a.DTTO  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ToDate = " AND a.DTTO LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ToDate = " AND a.DTTO NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtMonth")

                str_Month = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Month = " AND a.PAYMONTH LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Month = "  AND  NOT a.PAYMONTH LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Month = " AND a.PAYMONTH  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Month = " AND a.PAYMONTH  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Month = " AND a.PAYMONTH LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Month = " AND a.PAYMONTH NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_8.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtYear")
                str_Year = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_year = " AND a.PAYYEAR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_year = "  AND  NOT a.PAYYEAR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_year = " AND a.PAYYEAR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_year = " AND a.PAYYEAR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_year = " AND a.PAYYEAR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_year = " AND a.PAYYEAR NOT LIKE '%" & txtSearch.Text & "'"
                End If
                str_Sid_search = h_Selected_menu_9.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtRemark")

                str_Remark = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Remark = " AND a.REMARKS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Remark = "  AND  NOT a.REMARKS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Remark = " AND a.REMARKS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Remark = " AND a.REMARKS  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Remark = " AND a.REMARKS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Remark = " AND a.REMARKS NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_10.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtEmpno")

                str_EmpNo = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Remark = " AND a.Empno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Remark = "  AND  NOT a.Empno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Remark = " AND a.Empno  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Remark = " AND a.Empno  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Remark = " AND a.Empno LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Remark = " AND a.Empno NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & str_filter_EmpName & str_filter_LeaveType & str_filter_FromDate & str_filter_ToDate & str_filter_Month & str_filter_year & str_filter_Remark & AdjAddType & "  order by a.Ename desc")
            gvPrePayment.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then
                gvPrePayment.DataBind()
                gvPrePayment.SelectedIndex = p_sindex
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPrePayment.DataBind()
                Dim columnCount As Integer = gvPrePayment.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPrePayment.Rows(0).Cells.Clear()
                gvPrePayment.Rows(0).Cells.Add(New TableCell)
                gvPrePayment.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPrePayment.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPrePayment.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtEmpno")
            txtSearch.Text = str_EmpNo
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtyear")
            'txtSearch.Text = str_txtYear
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = str_EmpName
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtLeaveType")
            txtSearch.Text = str_LeaveType
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtFromDate")
            txtSearch.Text = str_FromDate
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtToDate")
            txtSearch.Text = str_ToDate
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtCurrency")
            'txtSearch.Text = str_txtCurrency
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtMonth")
            txtSearch.Text = str_Month
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtYear")
            txtSearch.Text = str_Year
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtRemark")
            txtSearch.Text = str_Remark
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblEAH As New Label
            Dim url As String
            Dim viewid As String
            lblEAH = TryCast(sender.FindControl("lblEAH_ID"), Label)
            viewid = lblEAH.Text
            ViewState("datamode") = "view"
            MainMnu_code = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Payroll\empLeaveAdj.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvPrePayment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrePayment.PageIndexChanging
        gvPrePayment.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub rbPositive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPositive.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbNegative_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbNegative.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchLeaveType_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchFromDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchMonth_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchYear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchRemark_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

End Class
