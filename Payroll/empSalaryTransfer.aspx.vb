Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empSalaryTransfer
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrUsedChqNos As String
    'Version        Author              Date            Change
    '1.1            Swapna              07-Apr-2011     passing id instead of "EMP" to SP SaveVOUCHER_
    '1.2            Swapna              07-Apr-2011     To display records  processed through WPS only(mnu_code:P130171)
    '                                                   and for records not processed through WPS (P130170)
    '1.3            Swapna              18 Dec 2011     To add parameter  "@VHH_bNoEDit 
    '2.0            Swapna              13 Aug 2014     To add GIRo file salary transfer-Singapore
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            txtAmount.Attributes.Add("readonly", "readonly")
            txtDocdate.Text = GetDiplayDate()
            'bind_BusinessUnit()
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
            If Request.QueryString("MainMnu_code") <> "P130171" Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                ViewState("EWH_ID") = Request.QueryString("EWH_ID")
                ViewState("Filetype") = Request.QueryString("Filetype")
            End If

            txtBankCode.Text = Session("CollectBank")
            txtBankDescr.Text = Session("Collect_name")
            btnPrint.Visible = False
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P130170" And ViewState("MainMnu_code") <> "P130171") Then
            'If Not Request.UrlReferrer Is Nothing Then
            '    Response.Redirect(Request.UrlReferrer.ToString())
            'Else
            '    Response.Redirect("~\noAccess.aspx")
            'End If
            'Else
            '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
            '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            'End If
            If ViewState("Filetype") = "GIRO" Then
                lblHeading.Text = "Salary Transfer (GIRO)"
                getValueDateforGIRO()
            End If
            set_Comboanddate()
            'fill_Emp_for_BSU()
        Else
            'FillEmpNames(h_EMPID.Value)
        End If
    End Sub
    Protected Sub getValueDateforGIRO()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            str_Sql = "Select EWH_VALUE_DATE from EMPSALARYDATA_D_WPS_H where [EWH_BSU_ID]= '" & Session("sBsuid") & "' and EWH_ID = " & ViewState("EWH_ID")

            Dim drReader As SqlDataReader
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (drReader.Read())
                txtValueDate.Text = Format(drReader("EWH_VALUE_DATE"), OASISConstants.DateFormat)
                If txtValueDate.Text <> "" Then
                    txtValueDate.Enabled = False
                    imgValueDate.Visible = False
                End If
            End While
           
            
              
        Catch ex As Exception

        End Try
    End Sub
    Function validate_controls() As String
        Dim str_error As String = ""
        Dim strfDate As String = txtDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            str_error = "Please Enter Valid Document Date <br>"
        Else
            txtDocdate.Text = strfDate
        End If

        If txtBankCode.Text = "" Then
            str_error = str_error & "Please Select Bank <br>"
        End If
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            str_error = str_error & "Invalid bank selected <br>"
        End If
        If txtNarrn.Text = "" Then
            str_error = str_error & "Please Enter Narration <br>"
        End If
        If txtChequedate.Text = "" Then
            str_error = str_error & "Please Enter  Date <br>"
        Else
            If IsDate(txtChequedate.Text) = False Then
                str_error = str_error & "Please Enter Valid  Date <br>"
            End If
        End If
        'If txtChqBook.Text = "" Then
        '    str_error = str_error & "Please Select Cheque <br>"
        'End If
        If ViewState("Filetype") <> "GIRO" Then
            If txtRefNo.Text = "" Then
                str_error = str_error & "Please Enter Reference No. <br>"
            End If
        End If
        validate_controls = str_error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = ""
        str_err = validate_controls()
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        End If
        If txtValueDate.Text = "" Then
            lblError.Text = "Please enter Value Date"
            Exit Sub
        ElseIf CDate(txtValueDate.Text) < Today.Date Then
            lblError.Text = "Value date cannot be less than today's date"
            Exit Sub
        End If
        Dim retval As String = "1000"
        AccountFunctions.GetNextDocId("BP", Session("sBsuid"), CType(txtDocdate.Text, Date).Month, CType(txtDocdate.Text, Date).Year)

        Save_voucher()

    End Sub

    Protected Function SaveToWPS_H(ByVal DocNo As String, ByVal objCon As SqlConnection, ByVal stTran As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Try

            Dim SqlCmd As New SqlCommand("saveDocNoToWPSH", objCon, stTran)
            SqlCmd.CommandType = CommandType.StoredProcedure

            SqlCmd.Parameters.AddWithValue("@DocNo", DocNo)
            SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@Remarks", txtNarrn.Text)
            SqlCmd.Parameters.AddWithValue("@WPSHID", ViewState("EWH_ID"))

            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            iReturnvalue = SqlCmd.Parameters("@ReturnValue").Value

            Return iReturnvalue


        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"


        End Try




    End Function


    Protected Sub Save_voucher()

        Dim lstrNewDocNo As String

        Dim str_connOasisFin As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_connOasisFin)
        Dim dblTotal As Double = 0
        Dim strEWH_IDs As String = String.Empty
        'For Each gvr As GridViewRow In gvMaster.Rows

        '    Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkWPS"), HtmlInputCheckBox)
        '    '''''''''
        '    If (cb.Checked = True) And (CDbl(gvr.Cells(1).Text) > 0 And gvr.Cells(1).Text <> "") Then
        '        dblTotal = dblTotal + CDbl(gvr.Cells(1).Text)
        '        strEWH_IDs = strEWH_IDs + "," + gvr.Cells(4).Text
        '    End If
        'Next

        For Each gvr As GridViewRow In gvDetails.Rows

            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkESD_BANK"), HtmlInputCheckBox)
            Dim txtAmount_D As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)

            '''''''''
            If (cb.Checked = True) And (CDbl(txtAmount_D.Text) > 0 And txtAmount_D.Text <> "") Then
                dblTotal = dblTotal + CDbl(txtAmount_D.Text)
            End If
        Next
        txtAmount.Text = dblTotal
        'txtAmount.Text = gvMaster.Rows(0).Cells(1).Text
        Dim objOasisCon As New SqlConnection(str_connOasis)
        Dim gdsSysInfo As New DataSet
        Dim str_err As String = ""

        Dim str_Provacc As String = ""
        Dim str_Salaryacc As String = ""
        Dim str_Cashflow As String = ""
        Dim str_Exgrate As Decimal
        Dim str_CorCurid As String = ""
        Dim str_Sql As String
        str_Sql = " SELECT SYS_SALARYPDCPROVACC, SYS_SALARYCASHFLOW,SYS_CUR_ID,SYS_SALARYPAYABLE_ACC_ID From vw_OSF_SYSINFO_S"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasis, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Provacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPDCPROVACC")
            str_Cashflow = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYCASHFLOW")
            str_CorCurid = gdsSysInfo.Tables(0).Rows(0)("SYS_CUR_ID")
            str_Salaryacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPAYABLE_ACC_ID")
        Else
            lblError.TabIndex = "Provision code not set"
            Exit Sub
        End If
        gdsSysInfo.Tables.Clear() 'GET EEXGRATE WRT CORP.OFFICE

        str_Sql = "SELECT EXG_RATE " _
        & " FROM EXGRATE_S WHERE EXG_BSU_ID='" & Session("sBsuid") & "'" _
        & " AND EXG_CUR_ID='" & str_CorCurid & "' AND '" & txtDocdate.Text & "' " _
        & " BETWEEN EXG_FDATE AND ISNULL(EXG_TDATE,GETDATE())"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasisFin, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Exgrate = gdsSysInfo.Tables(0).Rows(0)("EXG_RATE")
        Else
            lblError.TabIndex = "Provision code not set"
            Exit Sub
        End If
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
            sqlpGUID.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpGUID)
            SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
            SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
            SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "BP")
            SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_REFNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "P")
            'SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", Convert.ToInt32(hCheqBook.Value))
            SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtDocdate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChequedate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAuto", True)
            SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", str_Provacc)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", str_Exgrate)
            SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAdvance", False)

            SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
            SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", "")

            Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
            sqlpJHD_TIMESTAMP.Value = System.DBNull.Value

            SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
            SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
            SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            SqlCmd.Parameters.AddWithValue("@bEdit", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bNoEDit", True)
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            str_err = CInt(SqlCmd.Parameters("@ReturnValue").Value)

            lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)

            SqlCmd.Parameters.Clear()

            If (str_err = 0) Then

                Dim iBAnkLineno As Integer = 1

                '''''''chq
                'ldrNew("ChqBookId") = lstrChqDet.Split("|")(0) chbid
                'ldrNew("ChqBookLot") = lstrChqDet.Split("|")(1) bklot
                'ldrNew("ChqNo") = lstrChqDet.Split("|")(2) bkno
                'Dim lstrChqDet, lstrChqBookId, lstrChqBookLotNo As String
                'Dim lintChqs, lintMaxChqs As Integer
                'lstrChqBookId = hCheqBook.Value

                For Each gvr As GridViewRow In gvDetails.Rows

                    Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkESD_BANK"), HtmlInputCheckBox)
                    Dim txtAmount_D As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)

                    '''''''''
                    If (cb.Checked = True) And (CDbl(txtAmount_D.Text) > 0 And txtAmount_D.Text <> "") Then
                        '        lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False, stTrans)
                        '        lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
                        '        lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
                        '        lstrChqBookId = lstrChqDet.Split("|")(2)
                        '        lstrChqBookLotNo = lstrChqDet.Split("|")(3)

                        '        lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), iBAnkLineno, False, stTrans)
                        '        If lstrChqDet = "" Then
                        '            lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), iBAnkLineno, True, stTrans)
                        '        ElseIf lstrChqDet = "INSUFFICIENT LOT" Then
                        '            lblError.Text = "Insufficient ChqBook Lot(s)"
                        '            Exit Sub
                        '        End If
                        '        If lstrChqDet = "" Or lstrChqDet = "INSUFFICIENT LOT" Then
                        '            lblError.Text = "Insufficient ChqBook Lot(s)"
                        '            Exit Sub
                        '        End If
                        '        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo, 1, _
                        '        str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                        '        lstrChqDet.Split("|")(0), lstrChqDet.Split("|")(2), str_Salaryacc)

                        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo, 1, _
                                                              str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                                                              "0", txtRefNo.Text, str_Salaryacc)

                        iBAnkLineno = iBAnkLineno + 1
                        If str_err <> 0 Then
                            lblError.Text = getErrorMessage(str_err)
                            stTrans.Rollback()
                            Exit Sub
                        End If
                    End If
                Next
                If str_err = 0 Then

                    str_err = post_voucher(lstrNewDocNo, stTrans, objConn)
                End If
                If str_err = "0" Then
                    str_err = SaveToWPS_H(lstrNewDocNo, objConn, stTrans)
                End If
                If str_err = "0" Then

                    btnPrint.Visible = True
                    ViewState("lstrNewDocNo") = lstrNewDocNo
                    ViewState("DocDate") = txtDocdate.Text
                    clear_All()
                    stTrans.Commit()
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtDocdate.Text = GetDiplayDate()
                    'btnWPS.Visible = True
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", _
                    Page.User.Identity.Name.ToString, Me.Page)
                    lblError.Text = "Data Successfully Saved... Ref: " & lstrNewDocNo
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Else
                    lblError.Text = getErrorMessage(str_err)
                    stTrans.Rollback()
                End If
            Else
                lblError.Text = getErrorMessage(str_err)
                stTrans.Rollback()
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try

    End Sub

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_Exgrate1 As String, ByVal p_Exgrate2 As String, _
      ByVal p_Bankid As String, ByVal p_Lineid As String, _
      ByVal p_Amount As Decimal, ByVal p_Cashflow As String, _
      ByVal p_Chbid As String, ByVal p_Chqno As String, ByVal str_Salaryacc As String) As String

        Dim iReturnvalue As Integer
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim cmdUpdate As New SqlCommand
            Dim str_err As String = ""
            Dim dTotal As Double = 0
            Dim iEbtID As Integer = 0

            iReturnvalue = PayrollFunctions.SaveEMPSALARYTRF_D(p_Bankid, txtBankCode.Text, _
                      p_Chbid, p_Chqno, txtDocdate.Text, p_Amount, p_docno, txtNarrn.Text, _
                      False, iEbtID, "BP", stTrans, Session("sBsuid"), 0, txtValueDate.Text)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
            '''''update empsal_d here 


            cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            Dim str_crdb As String = "DR"
            '''''update empsal_d here

            str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, _
            str_crdb, p_Lineid, str_Salaryacc, p_Amount, txtDocdate.Text, p_Bankid)
            If str_err <> "0" Then
                Return str_err
            End If

            Dim str_sql As String = ""
            str_sql = "update ESD   set   ESD_REFDOCNO='" & p_docno & "', " _
            & " ESD_DATE ='" & txtDocdate.Text & "', ESD_EBT_ID='" & iEbtID & "', ESD_PAID =1  " _
            & " from " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D ESD " _
            & " INNER JOIN " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D_WPS EWPS ON EWPS.ESW_ESD_ID = ESD.ESD_ID  " _
            & " INNER JOIN " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D_WPS_H EWH ON EWH.EWH_ID = EWPS.ESW_EWH_ID " _
            & " where ESD_BSU_ID='" & Session("sBsuid") & "' AND " _
            & " ESD_MONTH in (" & Session("monthselected") & ") AND " _
            & " ESD_YEAR in(" & Session("yearselected") & ") AND (ESD_MODE = 1) AND (ESD_PAID = 0) " _
            & " AND ESD_BANK='" & p_Bankid & "'" _
            & " AND ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM " & OASISConstants.dbPayroll & ".DBO.EMPSALHOLD_D WHERE (EHD_bHold = 1) )"
            'V1.2 addition
            If ViewState("MainMnu_code") = "P130170" Then
                str_sql = str_sql & " AND IsNull(ESD_BProcessWPS,0)=0  "
            ElseIf ViewState("MainMnu_code") = "P130171" Then
                str_sql = str_sql & "AND EWH.EWH_ID=" & ViewState("EWH_ID") & "  AND IsNull(ESD_BProcessWPS,0)=1  "
            End If

            cmdUpdate = New SqlCommand(str_sql, objConn, stTrans)
            cmdUpdate.CommandType = CommandType.Text
            cmdUpdate.ExecuteNonQuery()

            '' '' 
            cmd.Parameters.AddWithValue("@GUID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
            cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
            cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "BP")
            cmd.Parameters.AddWithValue("@VHD_DOCNO", p_docno)
            cmd.Parameters.AddWithValue("@VHD_LINEID", p_Lineid)
            cmd.Parameters.AddWithValue("@VHD_ACT_ID", str_Salaryacc)
            cmd.Parameters.AddWithValue("@VHD_AMOUNT", p_Amount)
            cmd.Parameters.AddWithValue("@VHD_NARRATION", txtNarrn.Text)
            cmd.Parameters.AddWithValue("@VHD_CHQID", p_Chbid)
            cmd.Parameters.AddWithValue("@VHD_CHQNO", p_Chqno)
            cmd.Parameters.AddWithValue("@VHD_CHQDT", txtChequedate.Text)
            cmd.Parameters.AddWithValue("@VHD_RSS_ID", p_Cashflow)
            cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
            cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
            cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
            cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
            cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
            'cmd.Parameters.AddWithValue("@VHD_bCheque", True)
            cmd.Parameters.AddWithValue("@VHD_bCheque", False)
            cmd.Parameters.AddWithValue("@VHD_COL_ID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@bEdit", False)
            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

            cmd.Parameters.Clear()
            Return iReturnvalue
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
          ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
          ByVal p_crdr As String, ByVal p_slno As Integer, ByVal p_accountid As String, _
          ByVal p_amount As String, ByVal p_date As String, ByVal p_Bankid As String) As String

        Dim iReturnvalue As Integer
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim dTotal As Double = 0
            Dim str_sql As String

            str_sql = "SELECT ESD.ESD_EMP_ID, ESD.ESD_EARN_NET, " _
                & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+" _
                & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
                & " FROM  " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D AS ESD INNER JOIN " _
                & OASISConstants.dbPayroll & ".DBO.EMPLOYEE_M AS EM ON ESD.ESD_EMP_ID = EM.EMP_ID " _
                 & " INNER JOIN " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D_WPS EWPS ON EWPS.ESW_ESD_ID = ESD.ESD_ID  " _
                 & " INNER JOIN " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D_WPS_H EWH ON EWH.EWH_ID = EWPS.ESW_EWH_ID " _
                & " where ESD.ESD_BSU_ID='" & Session("sBsuid") & "' AND " _
                & " ESD.ESD_MONTH in (" & Session("monthselected") & ") AND " _
                & " ESD.ESD_YEAR in(" & Session("yearselected") & ") AND (ESD.ESD_MODE = 1) AND (ESD.ESD_PAID = 0) " _
                & " AND ESD.ESD_BANK='" & p_Bankid & "'" _
                & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM " & OASISConstants.dbPayroll & ".DBO.EMPSALHOLD_D WHERE (EHD_bHold = 1) )"
            'V1.2 addition
            If ViewState("MainMnu_code") = "P130170" Then
                str_sql = str_sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=0  "
            ElseIf ViewState("MainMnu_code") = "P130171" Then
                str_sql = str_sql & "AND EWH.EWH_ID=" & ViewState("EWH_ID") & " AND IsNull(ESD.ESD_BProcessWPS,0)=1  "
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_sql)

            For iVDSIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmd.Dispose()
                cmd = New SqlCommand("SaveVOUCHER_D_S", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                sqlpGUID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpJDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                sqlpJDS_ID.Value = p_slno
                cmd.Parameters.Add(sqlpJDS_ID)

                Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                cmd.Parameters.Add(sqlpJDS_SUB_ID)

                Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpJDS_BSU_ID)

                Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJDS_FYEAR)

                Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                sqlpJDS_DOCTYPE.Value = "BP"
                cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpJDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpJDS_DOCNO)

                Dim sqlpJDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                sqlpJDS_DOCDT.Value = txtDocdate.Text & ""
                cmd.Parameters.Add(sqlpJDS_DOCDT)

                Dim sqlpJDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                sqlpJDS_ACT_ID.Value = p_accountid
                cmd.Parameters.Add(sqlpJDS_ACT_ID)

                Dim sqlpbJDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                sqlpbJDS_SLNO.Value = p_slno
                cmd.Parameters.Add(sqlpbJDS_SLNO)

                Dim sqlpJDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                sqlpJDS_AMOUNT.Value = ds.Tables(0).Rows(iVDSIndex)("ESD_EARN_NET")
                cmd.Parameters.Add(sqlpJDS_AMOUNT)

                dTotal = dTotal + ds.Tables(0).Rows(iVDSIndex)("ESD_EARN_NET")

                Dim sqlpJDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                'sqlpJDS_CCS_ID.Value = "EMP"       'V1.1
                sqlpJDS_CCS_ID.Value = "0001"       'VDS_CCS_ID for EMPLOYEES
                cmd.Parameters.Add(sqlpJDS_CCS_ID)

                Dim sqlpJDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                sqlpJDS_CODE.Value = ds.Tables(0).Rows(iVDSIndex)("ESD_EMP_ID")
                cmd.Parameters.Add(sqlpJDS_CODE)

                Dim sqlpJDS_Descr As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                sqlpJDS_Descr.Value = ds.Tables(0).Rows(iVDSIndex)("EMP_NAME")
                cmd.Parameters.Add(sqlpJDS_Descr)

                Dim sqlpbJDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                sqlpbJDS_DRCR.Value = p_crdr
                cmd.Parameters.Add(sqlpbJDS_DRCR)

                Dim sqlpJDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                sqlpJDS_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpJDS_bPOSTED)

                Dim sqlpbJDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                sqlpbJDS_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbJDS_BDELETED)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                sqlpbEdit.Value = False
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    Return "1000"
                End If
                cmd.Parameters.Clear()
            Next
            If dTotal <> p_amount Then
                Return "1000"
            End If
            Return iReturnvalue
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function

    Function post_voucher(ByVal p_Docno As String, ByVal stTrans As SqlTransaction, ByVal objConn As SqlConnection) As Integer

        Try
            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim cmd As New SqlCommand("POSTVOUCHER", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid")
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = Session("F_YEAR")
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpJHD_DOCTYPE.Value = "BP"
                cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                sqlpJHD_DOCNO.Value = p_Docno
                cmd.Parameters.Add(sqlpJHD_DOCNO)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                Return iReturnvalue
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                Errorlog(ex.Message)
                Return 1000
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Protected Sub txtAmount_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("readonly", "readonly")
        txtAmount.Attributes.Add("onFocus", "this.select();")
        txtAmount.Text = AccountFunctions.Round(txtAmount.Text)
        txtAmount.Attributes.CssStyle.Add("TEXT-ALIGN", "right")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub clear_All()
        txtDocdate.Text = GetDiplayDate()
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtNarrn.Text = ""
        txtChequedate.Text = ""
        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtAmount.Text = ""
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")

        clMonthYear.DataBind()
        clMonthYear.SelectedIndex = -1
        gvDetails.DataBind()
    End Sub

    Sub set_Comboanddate()
        Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            'V1.2 condition added
            str_Sql = "SELECT DISTINCT CAST(CAST(ESD_MONTH AS varchar(4)) + '/1' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME) as DT," _
            & " CAST(ESD_MONTH AS varchar(4)) + '_' + " _
            & " CAST(ESD_YEAR AS varchar(4)) AS month_year, " _
            & " LEFT( DATENAME(MONTH, CAST(CAST(ESD_MONTH AS varchar(4)) + " _
            & " '/22' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME)), 3)+" _
            & " '/'+CAST(ESD_YEAR AS varchar(4)) AS monthyear, ESD_DTFROM" _
            & " FROM EMPSALARYDATA_D AS ESD INNER JOIN " _
             & " EMPSALARYDATA_D_WPS EWPS ON EWPS.ESW_ESD_ID = ESD.ESD_ID INNER JOIN " _
             & " EMPSALARYDATA_D_WPS_H EWH ON EWH.EWH_ID = EWPS.ESW_EWH_ID " _
             & " WHERE (IsNull(ESD_MODE,0) = 1) AND (IsNull(ESD_PAID,0) = 0)" _
            & " AND (ESD_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE (EHD_bHold = 1)) "
            'V1.2 addition
            If ViewState("MainMnu_code") = "P130170" Then
                str_Sql = str_Sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=0  order by DT"
            ElseIf ViewState("MainMnu_code") = "P130171" Then
                str_Sql = str_Sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=1 and EWH.EWH_ID= " & ViewState("EWH_ID") & " order by DT"
            End If


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            clMonthYear.DataSource = ds.Tables(0)
            clMonthYear.DataTextField = "monthyear"
            clMonthYear.DataValueField = "month_year"
            clMonthYear.DataBind()
            If ViewState("EWH_ID") <> "" Then
                clMonthYear.Enabled = False
                clMonthYear.Items(0).Selected = True
                clMonthYear_SelectedIndexChanged(Nothing, Nothing)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub clMonthYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clMonthYear.SelectedIndexChanged
        Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            'V1.2 condition added for bank accounts with/without WPS Processing
            str_Sql = "SELECT DISTINCT " _
            & " BANK_M.BNK_DESCRIPTION, SUM(ESD.ESD_EARN_NET) AS amount,ESD.ESD_BANK" _
            & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
            & " BANK_M ON ESD.ESD_BANK = BANK_M.BNK_ID INNER JOIN" _
             & " EMPSALARYDATA_D_WPS EWPS ON EWPS.ESW_ESD_ID = ESD.ESD_ID INNER JOIN" _
             & " EMPSALARYDATA_D_WPS_H EWH ON EWH.EWH_ID = EWPS.ESW_EWH_ID " _
            & " WHERE (ESD.ESD_MODE = 1) AND (ESD.ESD_PAID = 0) " _
            & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "')" _
            & " AND ESD.ESD_YEAR in (" & get_Selected_YearMonths(False) & ") and ESD.ESD_MONTH in (" & get_Selected_YearMonths(True) & ")  " _
            & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE IsNull(EHD_bHold,0) = 1)  and Isnull(EWH.ESW_DOCNO ,'')='' "

            If ViewState("MainMnu_code") = "P130170" Then
                str_Sql = str_Sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=0  "
            ElseIf ViewState("MainMnu_code") = "P130171" Then
                str_Sql = str_Sql & "AND EWH.EWH_ID=" & ViewState("EWH_ID") & " AND IsNull(ESD.ESD_BProcessWPS,0)=1  "
            End If
            str_Sql = str_Sql & " GROUP BY BANK_M.BNK_DESCRIPTION,ESD.ESD_BANK" _
                        & " ORDER BY BNK_DESCRIPTION"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvDetails.DataSource = ds.Tables(0)

            gvDetails.DataBind()
            If ViewState("MainMnu_code") = "P130171" Then
                str_Sql = "exec getSalaryTransfertoWPSList '" & Session("sBsuid") & "','" & get_Selected_YearMonths(True) & "','" & get_Selected_YearMonths(False) & "'," & ViewState("EWH_ID")
                Dim ds2 As New DataSet
                ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
               
                If ds2.Tables(0).Rows.Count > 0 Then
                    gvMaster.DataSource = ds2.Tables(0)
                    gvMaster.DataBind()
                    trMaster.Visible = True
                    pnlBanks.Enabled = False
                    txtBankCode.Text = ds2.Tables(0).Rows(0).Item("act_id")
                    txtBankDescr.Text = ds2.Tables(0).Rows(0).Item("ACt_NAME")
                    txtBankCode.Attributes.Add("readonly", True)
                    txtBankDescr.Attributes.Add("readonly", True)
                    For Each gvr As GridViewRow In gvDetails.Rows
                        If gvr.RowType = DataControlRowType.Header Then
                            Dim cbH As HtmlInputCheckBox = CType(gvr.FindControl("chkSelall"), HtmlInputCheckBox)
                            cbH.Checked = True
                        End If
                        Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkESD_BANK"), HtmlInputCheckBox)
                        cb.Checked = True
                    Next
                    txtNarrn.Text = txtNarrn.Text & "( " & gvMaster.Rows(0).Cells(0).Text & " )"

                End If

            End If


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function get_Selected_YearMonths(ByVal isMonth As Boolean) As String
        Dim str_code As String = ""
        Dim str_narrn As String = ""
        Dim i_yearMonth As Integer = 1 'year
        If isMonth = True Then
            i_yearMonth = 0
        End If
        For Each item As ListItem In clMonthYear.Items
            If (item.Selected) Then
                If str_code <> "" Then
                    str_code = str_code & "," & item.Value.Split("_")(i_yearMonth)
                    str_narrn = str_narrn & "," & item.Text
                Else
                    str_code = item.Value.Split("_")(i_yearMonth)
                    str_narrn = item.Text
                End If
            End If
        Next
        If isMonth = True Then
            If str_code = "" Then
                Session("monthselected") = "1900"
                str_code = "1900"
            Else
                Session("monthselected") = str_code
            End If
        Else
            If str_code = "" Then
                Session("yearselected") = "20"
                str_code = "20"
            Else
                Session("yearselected") = str_code
            End If
            Session("yearselected") = str_code
        End If
        txtNarrn.Text = "Salary Transfer For " & str_narrn
        get_Selected_YearMonths = str_code

    End Function

    Protected Function GetNextChqNo(ByVal pId As Integer, ByVal pItr As Integer, _
    ByVal pGetNext As Boolean, ByVal stTrans As SqlTransaction) As String

        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        Dim lstrChqNo As String


        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "SELECT CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If

        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_PREV_CHB_ID='" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If
        If pId = 0 Then
            Return "INSUFFICIENT LOT"
        End If



        Dim str_Sql As String

        If pItr = 1 Then
            str_Sql = "SELECT CHB_Id,CHB_LOTNO,CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_ALLOTED=0 ORDER BY CHD_NO"
        Else
            str_Sql = "SELECT CHB_Id,CHB_LOTNO,CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_ALLOTED=0 AND CHD_NO NOT IN (" & Mid(lstrUsedChqNos, 2) & ") ORDER BY CHD_NO"
        End If

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lstrChqBookId = ds.Tables(0).Rows(0)(0)
            hCheqBook.Value = lstrChqBookId
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(1)
            lstrChqNo = ds.Tables(0).Rows(0)(2)
            lstrUsedChqNos = lstrUsedChqNos & "," & lstrChqNo
            Return Convert.ToString(lstrChqBookId) & "|" & Convert.ToString(lstrChqBookLotNo) & "|" & lstrChqNo
        Else
            Return ""
        End If



    End Function

    Protected Function GetChqNos(ByVal pId As Integer, ByVal pGetNext As Boolean, ByVal stTrans As SqlTransaction) As String
        Dim lintChqs As Integer
        Dim lintMaxChqs As Integer
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String


        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "select CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If


        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_ID<>'" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If

        Dim str_Sql As String = "select CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lintChqs = ds.Tables(0).Rows(0)(0)
            lintMaxChqs = ds.Tables(0).Rows(0)(1)
            lstrChqBookId = ds.Tables(0).Rows(0)(2)
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(3)
            Return Convert.ToString(lintChqs) & "|" & Convert.ToString(lintMaxChqs) & "|" & lstrChqBookId & "|" & lstrChqBookLotNo
        Else
            Return ""
        End If
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        btnPrint.Visible = False
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        ' Session("ReportSource") = AccountsReports.BankPayment(ViewState("lstrNewDocNo"), String.Format("{0:MM-dd-yyyy}", CDate(txtDocdate.Text)), Session("sBSUID"), Session("F_YEAR"))
        'Session("ReportSource") = AccountsReports.BankPayment(ViewState("lstrNewDocNo"), txtDocdate.Text, Session("sBSUID"), Session("F_YEAR"))
        Session("ReportSource") = AccountsReports.BankPaymentVoucher(ViewState("lstrNewDocNo"), ViewState("DocDate"), Session("sBSUID"), Session("F_YEAR"))
        Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
    End Sub

    Protected Sub btnWPS_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim lblVUnit, lblMonthYear, lblExpDate As Label
            lblVUnit = TryCast(sender.Parent.FindControl("lblVUnit"), Label)
            lblExpDate = TryCast(sender.Parent.FindControl("lblExpDate"), Label)
            lblMonthYear = TryCast(sender.Parent.FindControl("lblMonthYear"), Label)
            'Dim str_Sql As String = "exec getWPSPrintData '" & Session("sBsuid") & "','" & clMonthYear.Items(0).Text & "','" & Session("sBsuid") & "'" & ",'" & Today.Date & "'," & ViewState("EWH_ID")
            Dim str_Sql As String = "exec getWPSPrintData_NewFormat '" & Session("sBsuid") & "','" & clMonthYear.Items(0).Text & "','" & Session("sBsuid") & "'" & ",'" & Today.Date & "'," & ViewState("EWH_ID") & ",'" & ViewState("lstrNewDocNo") & "'"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("UserName") = Session("sUsr_name")
                repSource.Parameter = params
                repSource.Command = cmd
                ' repSource.ResourceName = "../../payroll/Reports/RPT/rptWPSPrint.rpt"
                repSource.ResourceName = "../../payroll/Reports/RPT/WPSPrintNewFormat.rpt"
                Session("ReportSource") = repSource
                Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
