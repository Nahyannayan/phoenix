<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowBankDetail.aspx.vb" Inherits="Payroll_ShowBankDetail" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bank Details</title>
     <base target="_self" />
      <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    
</head>
<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server"> 
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>   <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
         <table border="0" cellpadding="1" cellspacing="0" width="100%">
                
                <tr>  
                    <td align="center"><asp:GridView ID="gvApprovals" runat="server" AutoGenerateColumns="False" EmptyDataText="History empty" Width="100%"  CssClass="table table-bordered table-row">
                       <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                            <Columns>
                                <asp:BoundField DataField="EMPNO" HeaderText="Emp No" />
                                <asp:BoundField DataField="EMP_NAME" HeaderText="Emp Name" />
                                <asp:BoundField DataField="PayMonth" HeaderText="Pay Month" ReadOnly="True" >
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="BNK_DESCRIPTION" HeaderText="Bank" ReadOnly="True" >
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ESD_EARN_NET" HeaderText="Amount" ReadOnly="True" >
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                <td colspan="1" align="right">
                    <asp:Label ID="lblTotal" runat="server" CssClass="error"></asp:Label>
                    &nbsp;&nbsp;
                </td>
           </table> 
                </div>
            </div>
            </div>
 </form>
</body>
</html>

