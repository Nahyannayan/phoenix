<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDoMonthendProcessAirfare.aspx.vb" Inherits="Payroll_empDoMonthendProcessAirfare" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"  >
                            <table   width="100%">

                                <tr class="matters">
                                    <td align="left"><span class="field-label"> Date</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" 
                                            TabIndex="4" /></td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Business Unit</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlBUnit" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
                    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
</asp:Content>
