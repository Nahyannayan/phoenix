Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empMonthlyE_D
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnLoad)

        If Page.IsPostBack = False Then
            Session("dtDt") = CreateDataTableMonthlyE_D()
            gridbind()
            gvMonthE_D.Attributes.Add("bordercolor", "#1b80b6")
            gvOldData.Attributes.Add("bordercolor", "#1b80b6")
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then

                ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))


            End If


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P130020" And ViewState("MainMnu_code") <> "P130025") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                txtEmpName.Attributes.Add("readonly", "readonly")
                txtType.Attributes.Add("readonly", "readonly")

                txtEmpID.Attributes.Add("readonly", "readonly")
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                If ViewState("MainMnu_code") = "P130020" Then
                    h_Deduction.Value = "DEDUCTION"
                    lblMonthly.Text = "Monthly Deduction"
                    lblMonthlysub.Text = "Monthly Deduction For Selected Month"
                    lblDedType.Text = "Deduction  Type"
                    ViewState("Type") = "D"
                    trExcelUpload.Visible = True
                Else
                    h_Deduction.Value = "EARNING"
                    lblMonthly.Text = "Monthly Earning"
                    lblMonthlysub.Text = "Monthly Earning For Selected Month"
                    lblDedType.Text = "Earning Type"
                    ViewState("Type") = "E"
                    trExcelUpload.Visible = True
                End If

                '----------create a list item to bind the years-------------
                Dim totyear, i As Integer
                ' Session("BSU_PAYMONTH")
                'Session("BSU_PAYYEAR")

                Dim startYear As Integer
                totyear = Session("BSU_PAYYEAR") 'AccessPayrollClass.GetMax_Min_Year(minyear) 'class for getting the total years
                startYear = totyear - 20
                Dim liYear As ListItem
                ddlPayYear.Items.Clear()
                For i = 0 To 40

                    liYear = New ListItem(startYear + i, startYear + i)
                    ddlPayYear.Items.Add(liYear)
                    'If ddlPayYear.Items(i).Value = Session("F_YEAR") Then
                    '    ddlPayYear.SelectedIndex = i
                    'End If
                Next
                'ddlPayMonth.SelectedIndex = Session("BSU_PAYMONTH") - 1
                'ViewState("Type") = "D"
                Try

                    str_sql = "SELECT BSU_ID," _
                        & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                        & " FROM  BUSINESSUNIT_M " _
                        & " where BSU_ID='" & Session("sBsuid") & "'"

                    Dim ds As New DataSet
                    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
                    If ds.Tables(0).Rows.Count > 0 Then
                        'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                        'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                        ddlPayMonth.SelectedIndex = -1
                        ddlPayYear.SelectedIndex = -1
                        ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                        ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                        ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
                    Else
                    End If
                Catch ex As Exception
                    Errorlog(ex.Message)
                End Try
                Session("dtDt") = CreateDataTableMonthlyE_D()
                Session("dtDt").Rows.Clear()
                ViewState("idTr") = 0
                Session("gDtlDataMode") = "ADD"

                'If Deduction

                'If earning type
                If ViewState("datamode") = "view" Then
                    ViewEmpE_DDetails(ViewState("viewid"))
                End If
            End If
        End If
    End Sub

    Sub ViewEmpE_DDetails(ByVal Empid As Integer)
        Session("dtDt") = CreateDataTableMonthlyE_D()
        Dim dt As DataTable = CreateDataTableMonthlyE_D()
        Dim cmd As New SqlCommand
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection()
        cmd.Connection = conn
        Dim empMonth As String = Encr_decrData.Decrypt(Request.QueryString("month").Replace(" ", "+")) 'Session("EMP_E_D_MONTH")
        Dim empYEAR As Integer = Encr_decrData.Decrypt(Request.QueryString("year").Replace(" ", "+")) 'Session("EMP_E_D_YEAR")
        cmd.CommandText = "select * from vw_OSO_EMPMONTHLY_E_D  where EDD_EMP_ID = " & Empid & _
        " AND EMP_MONTH = '" & empMonth & "' AND EDD_PAYYEAR =" & empYEAR & " AND EDD_TYPE = '" & _
        ViewState("Type") & "' AND isnull(EDD_REFDOCNO,'')=''"
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        While (dr.Read())
            Dim drow As DataRow = dt.NewRow()
            drow("Id") = dt.Rows.Count
            drow("EDD_EMP_ID") = dr("EDD_EMP_ID")
            drow("EDD_EMP_Name") = dr("EMPNAME")
            drow("EDD_ERNCODE") = dr("EDD_ERNCODE")
            drow("Code_Descr") = dr("ERN_DESCR")
            drow("EDD_PayMonth") = dr("EDD_PAYMONTH")
            drow("Desc_Month") = dr("EMP_MONTH")
            drow("EDD_Payyear") = dr("EDD_PAYYEAR")
            drow("EDD_Amount") = dr("EDD_AMOUNT")
            drow("EDD_Remarks") = dr("EDD_REMARKS")
            drow("EDD_Type") = ViewState("Type")
            drow("EDD_ID") = dr("EDD_ID")

            drow("EDD_CUR_ID") = Session("BSU_CURRENCY")
            drow("EDD_BSU_ID") = Session("sBsuid")

            drow("Delete_flag") = "LOADED"
            txtEmpName.Text = dr("EMPNAME")
            'txtEmpID.Text = dr("EDD_EMP_ID")
            txtEmpID.Text = dr("EMPNO").ToString()
            drow("EDD_EMPNO") = dr("EMPNO").ToString()
            h_EDD_EMP_ID.Value = dr("EDD_EMP_ID")
            'txtEmpID.Text = dr("EMPNO")
            hfEmp_ID.Value = Empid
            dt.Rows.Add(drow)
        End While
        Session("dtDt") = dt
        btnEmp_Name.Enabled = False
        gridbind()
        ToggleCols(False)
        ViewEmpE_DDetails_sELECTED()
    End Sub

    Protected Sub DeleteBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        Dim lintIndex As Integer = 0

        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)

        For lintIndex = 0 To Session("dtDt").Rows.Count - 1

            If (Session("dtDt").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                Session("dtDt").Rows(lintIndex)("Delete_flag") = "DELETED"

            End If
        Next
        gridbind()
    End Sub

    Protected Sub EditBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblRowId As New Label
            Dim lintIndex As Integer = 0
            Dim iDelcount As Integer = 0
            lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
            Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)

            For lintIndex = 0 To Session("dtDt").Rows.Count - 1
                If Session("dtDt").Rows(lintIndex)("Delete_flag") = "DELETED" Then
                    iDelcount = iDelcount + 1
                End If

                If (Session("dtDt").Rows(lintIndex)("Id") = Session("gintEditLine")) Then

                    'txtEmpID.Text = Session("dtDt").Rows(lintIndex)("EDD_EMP_ID")
                    h_EDD_EMP_ID.Value = Session("dtDt").Rows(lintIndex)("EDD_EMP_ID")
                    txtEmpName.Text = Session("dtDt").Rows(lintIndex)("EDD_EMP_Name")
                    hfTypeID.Value = Session("dtDt").Rows(lintIndex)("EDD_ERNCODE")
                    txtEmpID.Text = Session("dtDt").Rows(lintIndex)("EDD_EMPNO")
                    txtType.Text = Session("dtDt").Rows(lintIndex)("Code_Descr")
                    ddlPayMonth.SelectedIndex = -1
                    ddlPayMonth.Items.FindByValue(Session("dtDt").Rows(lintIndex)("EDD_PayMonth")).Selected = True
                    ddlPayYear.SelectedIndex = -1
                    ddlPayYear.Items.FindByValue(Session("dtDt").Rows(lintIndex)("EDD_Payyear")).Selected = True
                    txtAmount.Text = Session("dtDt").Rows(lintIndex)("EDD_Amount")
                    txtRemark.Text = Session("dtDt").Rows(lintIndex)("EDD_Remarks")
                    hfEDD_ID.Value = Session("dtDt").Rows(lintIndex)("EDD_ID")
                    ' chkReturn.Checked = Session("dtDt").Rows(lintIndex)("EAD_bCHKFORLEAVE")
                    Session("dtDt").Rows(lintIndex)("Delete_flag") = "EDIT"
                    gvMonthE_D.SelectedIndex = lintIndex - iDelcount ' Session("gintEditLine") 
                    gvMonthE_D.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                    gvMonthE_D.SelectedRowStyle.ForeColor = Drawing.Color.Black

                    Session("gDtlDataMode") = "UPDATE"
                    btnFill.Text = "UPDATE"

                    ToggleCols(False)

                    Exit For
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub fillData()
        'Use the function to fill the grid view
        Dim i, lintIndex As Integer
        Try
            If Session("gDtlDataMode") = "ADD" Then
                Dim rDt As DataRow
                rDt = Session("dtDt").NewRow
                rDt("Id") = ViewState("idTr")

                rDt("EDD_EMP_ID") = h_EDD_EMP_ID.Value
                rDt("EDD_EMP_Name") = txtEmpName.Text
                rDt("EDD_ERNCODE") = hfTypeID.Value
                rDt("Code_Descr") = txtType.Text
                rDt("Desc_Month") = ddlPayMonth.SelectedItem.Text
                rDt("EDD_PayMonth") = ddlPayMonth.SelectedValue
                rDt("EDD_Payyear") = ddlPayYear.SelectedValue
                rDt("EDD_Amount") = txtAmount.Text
                rDt("EDD_EMPNO") = txtEmpID.Text
                rDt("EDD_Remarks") = txtRemark.Text
                rDt("EDD_Type") = ViewState("Type")
                rDt("EDD_ID") = 0

                rDt("EDD_CUR_ID") = Session("BSU_CURRENCY")
                rDt("EDD_BSU_ID") = Session("sBsuid")

                rDt("Delete_flag") = ""


                If gvMonthE_D.Rows.Count = 0 Then
                    Session("dtDt").Rows.Add(rDt)
                    ViewState("idTr") = ViewState("idTr") + 1
                    gridbind()
                Else
                    'Check for duplicate entry

                    For i = 0 To Session("dtDt").Rows.Count - 1
                        If Session("dtDt").Rows(i)("EDD_EMP_ID") = rDt("EDD_EMP_ID") _
                         And Session("dtDt").Rows(i)("EDD_ERNCODE") = rDt("EDD_ERNCODE") And Session("dtDt").Rows(i)("Delete_flag") = "" Then

                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                            gridbind()
                            Exit Sub
                        End If
                    Next



                    Session("dtDt").Rows.Add(rDt)
                    ViewState("idTr") = ViewState("idTr") + 1
                    gridbind()

                End If

            ElseIf (Session("gDtlDataMode") = "UPDATE") Then

                For lintIndex = 0 To Session("dtDt").Rows.Count - 1
                    If (Session("dtDt").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                        Session("dtDt").Rows(lintIndex)("EDD_EMP_ID") = h_EDD_EMP_ID.Value
                        Session("dtDt").Rows(lintIndex)("EDD_EMP_Name") = txtEmpName.Text
                        Session("dtDt").Rows(lintIndex)("EDD_EMPNO") = txtEmpID.Text
                        Session("dtDt").Rows(lintIndex)("EDD_ERNCODE") = hfTypeID.Value
                        Session("dtDt").Rows(lintIndex)("Code_Descr") = txtType.Text
                        Session("dtDt").Rows(lintIndex)("EDD_PayMonth") = ddlPayMonth.SelectedValue
                        Session("dtDt").Rows(lintIndex)("Desc_Month") = ddlPayMonth.SelectedItem.Text
                        Session("dtDt").Rows(lintIndex)("EDD_Payyear") = ddlPayYear.SelectedValue
                        Session("dtDt").Rows(lintIndex)("EDD_Amount") = txtAmount.Text
                        Session("dtDt").Rows(lintIndex)("EDD_Remarks") = txtRemark.Text
                        Session("dtDt").Rows(lintIndex)("EDD_Type") = ViewState("Type")
                        Session("dtDt").Rows(lintIndex)("EDD_ID") = hfEDD_ID.Value
                        Session("dtDt").Rows(lintIndex)("EDD_CUR_ID") = Session("BSU_CURRENCY")

                        Session("dtDt").Rows(lintIndex)("EDD_BSU_ID") = Session("sBsuid")

                        Session("dtDt").Rows(lintIndex)("Delete_flag") = "EDIT"


                        'CHECK FOR DUPLICATE ENTRY BEFORE UPDATING
                        For i = 0 To Session("dtDt").Rows.Count - 1
                            If (Session("dtDt").Rows(i)("EDD_EMP_ID") = h_EDD_EMP_ID.Value) And (Session("dtDt").Rows(i)("EDD_ERNCODE") = hfTypeID.Value) _
                            And (Session("dtDt").Rows(i)("Id") <> Session("gintEditLine")) And Session("dtDt").Rows(i)("Delete_flag") = "" Then

                                lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                                ' gridbind()
                                Exit Sub
                            End If
                        Next

                        ToggleCols(True)
                        Session("gDtlDataMode") = "ADD"
                        btnFill.Text = "ADD"
                        gvMonthE_D.SelectedIndex = -1
                        gridbind()
                        clear_details()

                        Exit For
                    End If
                Next

            End If
            clear_details()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub ToggleCols(ByVal pSet As Boolean)
        'gvMonthE_D.Columns(14).Visible = pSet
        gvMonthE_D.Columns(15).Visible = pSet
        gvMonthE_D.Columns(16).Visible = pSet
        ' gvFixAsset.Columns(17).Visible = pSet

    End Sub
    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ext = ".xls" Then
                '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                ' getdataExcel(strFileName)
                UpLoadDBF()
            Else
                lblError.Text = "Please upload .xls files only."
            End If

        Else
            lblError.Text = "File not uploaded"
        End If

        'Dim DS As New DataSet
        'MyCommand.Fill(DS)
        'Dim Dt As DataTable = DS.Tables(0)
        'gvNewdata.DataSource = Dt
        'gvNewdata.DataBind()

    End Sub
    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 6)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(0).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(1).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next

            Dim strEarnDescr As String = ""
            ColName = xltable.Columns(3).ColumnName
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEarnDescr &= IIf(strEarnDescr <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                End If
            Next

            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If

            Dim ValidStudent As New DataTable
            Dim ValidCode As New DataTable
           
            Dim mTable As New DataTable
            mTable = CreateDataTableMonthlyE_D()

            Dim rowId As Integer = 0
           
            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                ValidStudent = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in (" & strEmpNos & ") and emp_bsu_id='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)
                If ViewState("MainMnu_code") = "P130020" Then
                    ValidCode = Mainclass.getDataTable("select ERN_ID,ERN_DESCR from EMPSALCOMPO_M where ERN_DESCR in (" & strEarnDescr & ") and ERN_TYP=0 and  '" & Session("sBsuid") & "' in (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe (Isnull(ERN_BSU_IDs ,'" & Session("sBsuid") & "'),'|'))", ConnectionManger.GetOASISConnectionString)

                Else
                    ValidCode = Mainclass.getDataTable("select ERN_ID,ERN_DESCR from EMPSALCOMPO_M where ERN_DESCR in (" & strEarnDescr & ") and ERN_TYP=1 and '" & Session("sBsuid") & "' in (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe (Isnull(ERN_BSU_IDs ,'" & Session("sBsuid") & "'),'|'))", ConnectionManger.GetOASISConnectionString)
                End If


                mRow = mTable.NewRow
                mRow("id") = xlRow(0)
                mRow("EDD_EMPNO") = xlRow(1)
                mRow("EDD_EMP_Name") = xlRow(2)
                mRow("Code_Descr") = xlRow(3)
                mRow("EDD_Amount") = xlRow(4)
                mRow("EDD_REMARKS") = xlRow(5).ToString

                mRow("Desc_Month") = ddlPayMonth.SelectedItem.Text
                mRow("EDD_Paymonth") = ddlPayMonth.SelectedValue
                mRow("EDD_Payyear") = ddlPayYear.SelectedItem.Text
                ' mRow("Delete_flag") = "LOADED"

                mRow("EDD_Type") = ViewState("Type")
                mRow("EDD_ID") = 0

                mRow("EDD_CUR_ID") = Session("BSU_CURRENCY")
                mRow("EDD_BSU_ID") = Session("sBsuid")

                mRow("Delete_flag") = ""
                'mRow("EDD_EMP_ID")=
                If ValidStudent.Select("Empno='" & xlRow(1) & "'", "").Length = 0 Then
                    'mRow("NotValid") = "1"
                    'mRow("ErrorText") = "Invalid Employee No"
                    lblError.Text = "Invalid Employee No:" & xlRow(1) & ", Sl No. :" & xlRow(0) & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidStudent.Select("Empno='" & xlRow(1) & "'", "")(0)
                    mRow("EDD_EMP_ID") = drEmp("Emp_id")
                    mRow("EDD_EMP_Name") = drEmp("EMPNAME")
                    'mRow("EDD_EMP_ID") = ValidStudent.Rows(rowId).Item("Emp_id")
                    'mRow("EDD_EMP_Name") = ValidStudent.Rows(rowId).Item("EMPNAME")
                End If
                If ValidCode.Select("ERN_DESCR='" & xlRow(3) & "'", "").Length = 0 Then
                    'mRow("NotValid") = "1"
                    'mRow("ErrorText") = "Invalid Earning/deduction"
                    If ViewState("MainMnu_code") = "P130020" Then
                        lblError.Text = "Invalid Deduction Type '" & xlRow(3) & "' for EmpNo:" & xlRow(1)
                        Exit Sub
                    Else
                        lblError.Text = "Invalid Earnings Type '" & xlRow(3) & "' for EmpNo:" & xlRow(1)
                        Exit Sub
                    End If

                Else
                    Dim drCode As DataRow
                    drCode = ValidCode.Select("ERN_DESCR='" & xlRow(3) & "'", "")(0)
                    mRow("EDD_ERNCODE") = drCode("ERN_ID")
                    mRow("Code_Descr") = drCode("ERN_DESCR")

                    'mRow("EDD_ERNCODE") = ValidCode.Rows(rowId).Item("ERN_ID")
                    'mRow("Code_Descr") = ValidCode.Rows(rowId).Item("ERN_DESCR")
                End If
                    rowId = rowId + 1
                    mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()
            'gvFeeDetails.DataSource = mTable
            'gvFeeDetails.DataBind()
            gvMonthE_D.DataSource = mTable
            Session("dtDt") = mTable
            gvMonthE_D.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub
    Protected Sub clear_details()

        txtType.Text = ""
        txtRemark.Text = ""
        txtAmount.Text = ""

        ddlPayMonth.SelectedValue = Session("BSU_PAYMONTH")
        ddlPayYear.SelectedValue = Session("BSU_PAYYEAR") ' Session("F_YEAR")
        lblRowNum.Text = ""
        lblRowNum.Visible = False

    End Sub

    Public Function gridbind() As String


        Try
            'Dim dtInfo As DataTable = CreateDataTable()

            Dim i As Integer
            Dim dtDtTemp As New DataTable
            dtDtTemp = CreateDataTableMonthlyE_D()


            If Session("dtDt").Rows.Count > 0 Then
                For i = 0 To Session("dtDt").Rows.Count - 1
                    If (Session("dtDt").Rows(i)("Delete_flag") <> "DELETED") Then
                        Dim ldrTempNew As DataRow
                        ldrTempNew = dtDtTemp.NewRow
                        For j As Integer = 0 To Session("dtDt").Columns.Count - 1
                            ldrTempNew.Item(j) = Session("dtDt").Rows(i)(j)
                        Next
                        dtDtTemp.Rows.Add(ldrTempNew)
                    End If
                Next

            End If



            gvMonthE_D.DataSource = dtDtTemp
            ' gvMonthE_D.Columns(1).Visible = False
            ' gvMonthE_D.Columns(2).Visible = False
            ' gvMonthE_D.Columns(4).Visible = False
            gvMonthE_D.DataBind()
            Return "1"
        Catch ex As Exception
            Return "1"
        End Try
    End Function

    Private Function CreateDataTableMonthlyE_D() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.Int64"))
            Dim EDD_EMP_ID As New DataColumn("EDD_EMP_ID", System.Type.GetType("System.String"))
            Dim EDD_EMPNO As New DataColumn("EDD_EMPNO", System.Type.GetType("System.String"))
            Dim EDD_EMP_Name As New DataColumn("EDD_EMP_Name", System.Type.GetType("System.String"))
            Dim EDD_ERNCODE As New DataColumn("EDD_ERNCODE", System.Type.GetType("System.String"))
            Dim Code_Descr As New DataColumn("Code_Descr", System.Type.GetType("System.String"))
            Dim EDD_PayMonth As New DataColumn("EDD_PayMonth", System.Type.GetType("System.Int16"))
            Dim Desc_Month As New DataColumn("Desc_Month", System.Type.GetType("System.String"))
            Dim EDD_Payyear As New DataColumn("EDD_Payyear", System.Type.GetType("System.Int64"))
            Dim EDD_Amount As New DataColumn("EDD_Amount", System.Type.GetType("System.Double"))
            ' Dim Str_EDD_STATUS As New DataColumn("Str_EDD_STATUS", System.Type.GetType("System.String"))
            Dim EDD_Remarks As New DataColumn("EDD_Remarks", System.Type.GetType("System.String"))
            Dim EDD_Type As New DataColumn("EDD_Type", System.Type.GetType("System.String"))
            Dim EDD_ID As New DataColumn("EDD_ID", System.Type.GetType("System.Int64"))
            Dim EDD_CUR_ID As New DataColumn("EDD_CUR_ID", System.Type.GetType("System.String"))
            Dim EDD_BSU_ID As New DataColumn("EDD_BSU_ID", System.Type.GetType("System.String"))
            Dim Delete_flag As New DataColumn("Delete_flag", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(EDD_EMP_ID)
            dtDt.Columns.Add(EDD_EMPNO)
            dtDt.Columns.Add(EDD_EMP_Name)
            dtDt.Columns.Add(EDD_ERNCODE)
            dtDt.Columns.Add(Code_Descr)
            dtDt.Columns.Add(EDD_PayMonth)
            dtDt.Columns.Add(Desc_Month)
            dtDt.Columns.Add(EDD_Payyear)
            dtDt.Columns.Add(EDD_Amount)
            'dtDt.Columns.Add(Str_EDD_STATUS)
            dtDt.Columns.Add(EDD_Remarks)
            dtDt.Columns.Add(EDD_Type)
            dtDt.Columns.Add(EDD_ID)
            dtDt.Columns.Add(EDD_CUR_ID)
            dtDt.Columns.Add(EDD_BSU_ID)
            dtDt.Columns.Add(Delete_flag)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Protected Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        'condition to be checked before entrying the data into the grid
        btnAdd.Enabled = False
        If ddlPayYear.SelectedValue > Session("BSU_PAYYEAR") Then

            fillData()

        ElseIf ddlPayMonth.SelectedValue >= Session("BSU_PAYMONTH") And ddlPayYear.SelectedValue = Session("BSU_PAYYEAR") Then
            fillData()
        Else
            lblError.Text = "You have selected invalid period"
        End If
        btnAdd.Enabled = True
    End Sub

    Protected Sub btnChildCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChildCancel.Click
        ToggleCols(True)
        Session("gDtlDataMode") = "ADD"
        btnFill.Text = "ADD"
        gvMonthE_D.SelectedIndex = -1
        clear_details()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim transaction As SqlTransaction
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        transaction = conn.BeginTransaction("SampleTransaction")

        Dim i As Integer = SaveEMPMONTHLYDATA_D(conn, transaction)
        If i = 0 Then
            transaction.Commit()
            lblError.Text = "Data Saved Successfully"
            ' clear_details()
            txtType.Text = ""
            txtRemark.Text = ""
            txtAmount.Text = ""
            ViewEmpE_DDetails_sELECTED()

            gvMonthE_D.DataSource = Nothing
            gvMonthE_D.DataBind()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            transaction.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(i)
        End If
    End Sub

    Private Function SaveEMPMONTHLYDATA_D(ByVal conn As SqlConnection, ByVal transaction As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("dtDt") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            If Session("dtDt").Rows.Count > 0 Then
                For iIndex = 0 To Session("dtDt").Rows.Count - 1
                    Dim dr As DataRow = Session("dtDt").Rows(iIndex)

                    If (Session("dtDt").Rows(iIndex)("Delete_flag") <> "DELETED") Then
                        If Session("dtDt").Rows(iIndex)("Delete_flag") <> "LOADED" Then
                            If Session("dtDt").Rows(iIndex)("Delete_flag") = "EDIT" Then
                                cmd = New SqlCommand("DeleteEMPMONTHLYDATA_D", conn, transaction)
                                cmd.CommandType = CommandType.StoredProcedure

                                Dim sqlpDELEDD_ID As New SqlParameter("@EDD_ID", SqlDbType.Int)
                                sqlpDELEDD_ID.Value = dr("EDD_ID")
                                cmd.Parameters.Add(sqlpDELEDD_ID)

                                Dim sqlpDELEDD_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
                                sqlpDELEDD_EMP_ID.Value = dr("EDD_EMP_ID")
                                cmd.Parameters.Add(sqlpDELEDD_EMP_ID)

                                Dim sqlpDELEDD_BSU_ID As New SqlParameter("@EDD_BSU_ID", SqlDbType.VarChar, 10)
                                sqlpDELEDD_BSU_ID.Value = Session("sBSUID")
                                cmd.Parameters.Add(sqlpDELEDD_BSU_ID)

                                Dim retDelValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                                retDelValParam.Direction = ParameterDirection.ReturnValue
                                cmd.Parameters.Add(retDelValParam)

                                cmd.ExecuteNonQuery()
                                iReturnvalue = retDelValParam.Value
                                If iReturnvalue <> 0 Then
                                    Exit For
                                End If
                            End If
                            cmd = New SqlCommand("SaveEMPMONTHLYDATA_D", conn, transaction)
                            cmd.CommandType = CommandType.StoredProcedure

                            Dim sqlpEDD_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
                            sqlpEDD_EMP_ID.Value = dr("EDD_EMP_ID")  'EmployeeID
                            cmd.Parameters.Add(sqlpEDD_EMP_ID)

                            Dim sqlpEDD_BSU_ID As New SqlParameter("@EDD_BSU_ID", SqlDbType.VarChar, 10)
                            sqlpEDD_BSU_ID.Value = Session("sBSUID")
                            cmd.Parameters.Add(sqlpEDD_BSU_ID)

                            Dim sqlpEDD_PAYMONTH As New SqlParameter("@EDD_PAYMONTH", SqlDbType.TinyInt)
                            sqlpEDD_PAYMONTH.Value = dr("EDD_PayMonth") 'PAYMONTH
                            cmd.Parameters.Add(sqlpEDD_PAYMONTH)

                            Dim sqlpEDD_PAYYEAR As New SqlParameter("@EDD_PAYYEAR", SqlDbType.Int)
                            sqlpEDD_PAYYEAR.Value = dr("EDD_Payyear") 'PAYYEAR
                            cmd.Parameters.Add(sqlpEDD_PAYYEAR)

                            Dim sqlpEDD_CUR_ID As New SqlParameter("@EDD_CUR_ID", SqlDbType.VarChar, 10)
                            sqlpEDD_CUR_ID.Value = dr("EDD_CUR_ID") 'Cur_ID
                            cmd.Parameters.Add(sqlpEDD_CUR_ID)

                            Dim sqlpEDD_ERNCODE As New SqlParameter("@EDD_ERNCODE", SqlDbType.VarChar, 10)
                            sqlpEDD_ERNCODE.Value = dr("EDD_ERNCODE")
                            cmd.Parameters.Add(sqlpEDD_ERNCODE)

                            Dim sqlpEDD_TYPE As New SqlParameter("@EDD_TYPE", SqlDbType.VarChar, 1)
                            sqlpEDD_TYPE.Value = dr("EDD_Type")
                            cmd.Parameters.Add(sqlpEDD_TYPE)

                            Dim sqlpEDD_AMOUNT As New SqlParameter("@EDD_AMOUNT", SqlDbType.Decimal)
                            sqlpEDD_AMOUNT.Value = dr("EDD_Amount")
                            cmd.Parameters.Add(sqlpEDD_AMOUNT)

                            Dim sqlpEDD_REMARKS As New SqlParameter("@EDD_REMARKS", SqlDbType.VarChar, 100)
                            sqlpEDD_REMARKS.Value = dr("EDD_Remarks")
                            cmd.Parameters.Add(sqlpEDD_REMARKS)

                            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                            retValParam.Direction = ParameterDirection.ReturnValue
                            cmd.Parameters.Add(retValParam)

                            cmd.ExecuteNonQuery()
                            iReturnvalue = retValParam.Value
                            If iReturnvalue <> 0 Then
                                lblRowNum.Text = "Excel row number: " & (iIndex + 1).ToString
                                lblRowNum.Visible = True
                                Exit For
                            End If
                            If Session("dtDt").Rows(iIndex)("Delete_flag") = "EDIT" Then
                                iReturnvalue = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("dtDt").Rows(iIndex)("EDD_EMP_ID") & "-" & Session("dtDt").Rows(iIndex)("EDD_ERNCODE"), "Edited", Page.User.Identity.Name.ToString)
                            Else
                                iReturnvalue = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("dtDt").Rows(iIndex)("EDD_EMP_ID") & "-" & Session("dtDt").Rows(iIndex)("EDD_ERNCODE"), "Insert", Page.User.Identity.Name.ToString)
                            End If
                            If iReturnvalue <> 0 Then
                                lblError.Text = "Error in updating Audit trial"
                                Exit For
                            End If
                        End If
                    Else
                        'Delete Code will Come here
                        cmd = New SqlCommand("DeleteEMPMONTHLYDATA_D", conn, transaction)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpEDD_ID As New SqlParameter("@EDD_ID", SqlDbType.Int)
                        sqlpEDD_ID.Value = dr("EDD_ID")
                        cmd.Parameters.Add(sqlpEDD_ID)

                        Dim sqlpEDD_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
                        sqlpEDD_EMP_ID.Value = dr("EDD_EMP_ID")
                        cmd.Parameters.Add(sqlpEDD_EMP_ID)

                        Dim sqlpEDD_BSU_ID As New SqlParameter("@EDD_BSU_ID", SqlDbType.VarChar, 10)
                        sqlpEDD_BSU_ID.Value = Session("sBSUID")
                        cmd.Parameters.Add(sqlpEDD_BSU_ID)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                        iReturnvalue = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("dtDt").Rows(iIndex)("EDD_EMP_ID") & "-" & Session("dtDt").Rows(iIndex)("EDD_ERNCODE"), "Delete", Page.User.Identity.Name.ToString)
                        If iReturnvalue <> 0 Then
                            lblError.Text = "Error in updating Audit trial"
                            Exit For
                        End If

                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ToggleCols(True)
        ViewState("datamode") = "edit"
        'disable the control buttons based on the rights
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Session("gDtlDataMode") = "ADD"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ToggleCols(True)
        btnEmp_Name.Enabled = True
        Session("dtDt") = CreateDataTableMonthlyE_D()
        gvMonthE_D.DataSource = Nothing
        gvMonthE_D.DataBind()
        ViewEmpE_DDetails_sELECTED()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call clear_details()
            'clear the textbox and set the default settings
            ViewEmpE_DDetails_sELECTED()
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub ViewEmpE_DDetails_sELECTED()
        Dim dt As DataTable = CreateDataTableMonthlyE_D()
        Dim cmd As New SqlCommand
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection()
        cmd.Connection = conn
        GetEmployeeNo()
        Dim empMonth As String = ddlPayMonth.SelectedItem.Value
        Dim empYEAR As Integer = ddlPayYear.SelectedItem.Value
        cmd.CommandText = "select * from vw_OSO_EMPMONTHLY_E_D  where EDD_EMP_ID = '" & h_EDD_EMP_ID.Value & _
        "' AND EDD_PAYMONTH = '" & empMonth & "' AND EDD_PAYYEAR =" & empYEAR & " AND EDD_TYPE = '" & _
        ViewState("Type") & "' AND isnull(EDD_REFDOCNO,'')=''"
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        While (dr.Read())
            Dim drow As DataRow = dt.NewRow()
            drow("Id") = dt.Rows.Count
            drow("EDD_EMP_ID") = dr("EDD_EMP_ID")
            drow("EDD_EMP_Name") = dr("EMPNAME")
            drow("EDD_ERNCODE") = dr("EDD_ERNCODE")
            drow("Code_Descr") = dr("ERN_DESCR")
            drow("EDD_PayMonth") = dr("EDD_PAYMONTH")
            drow("Desc_Month") = dr("EMP_MONTH")
            drow("EDD_Payyear") = dr("EDD_PAYYEAR")
            drow("EDD_Amount") = dr("EDD_AMOUNT")
            drow("EDD_Remarks") = dr("EDD_REMARKS")
            drow("EDD_Type") = ViewState("Type")
            drow("EDD_ID") = dr("EDD_ID")

            drow("EDD_CUR_ID") = Session("BSU_CURRENCY")
            drow("EDD_BSU_ID") = Session("sBsuid")

            drow("Delete_flag") = "LOADED"
            txtEmpName.Text = dr("EMPNAME")
            'txtEmpID.Text = dr("EDD_EMP_ID")
            h_EDD_EMP_ID.Value = dr("EDD_EMP_ID")
            txtEmpID.Text = dr("EMPNO").ToString()
            dt.Rows.Add(drow)
        End While
        gvOldData.DataSource = dt
        gvOldData.DataBind()
    End Sub

    Protected Sub ddlPayMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayMonth.SelectedIndexChanged
        ViewEmpE_DDetails_sELECTED()
    End Sub

    Protected Sub ddlPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayYear.SelectedIndexChanged
        ViewEmpE_DDetails_sELECTED()
    End Sub

    Protected Sub btnEmp_Name_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmp_Name.Click
        ViewEmpE_DDetails_sELECTED()
    End Sub

    Sub GetEmployeeNo()
        Dim cmd As New SqlCommand
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection()
        cmd.Connection = conn
        If h_EDD_EMP_ID.Value <> "" Then
            cmd.CommandText = "SELECT EMPNO FROM EMPLOYEE_M WHERE EMP_bActive=1 AND EMP_BSU_ID='" & Session("sBsuid") & "' AND EMP_ID='" & h_EDD_EMP_ID.Value & "'"
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            While (dr.Read())
                txtEmpID.Text = dr("EMPNO").ToString()
            End While
        End If
    End Sub

    Protected Sub txtEmpName_TextChanged(sender As Object, e As EventArgs)
        GetEmployeeNo()
    End Sub
End Class
