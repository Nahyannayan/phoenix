<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empEmpAllocation.aspx.vb" Inherits="empEmpAllocation" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx?dt=" + document.getElementById('<%=txtfromDate.ClientID %>').value, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            switch (txtControl) {
                case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                    return true;
                    break;

            }
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Employee Allocation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False" HeaderText="Following condition required:" ValidationGroup="All1" CssClass="error" />
                            <br />
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblAllDate" runat="server" Text="Allocation Date" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server" ValidationGroup="All1" AutoPostBack="True" CausesValidation="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="return getDate(550, 310, 0)" ImageUrl="../Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="Allocation date required" ValidationGroup="All1" EnableViewState="False" Display="Dynamic" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFromDate"
                                            Display="Dynamic" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label3" runat="server" Text="Search What?" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnBsu" runat="server" Text="Business Unit" CssClass="button" />
                                        <asp:Button ID="btnEmpName" runat="server" Text="Employee Name" CssClass="button" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="lblEmp" runat="server" Text="Select Employee Name" CssClass="field-label"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="Label1" runat="server" Text="Allocate Business Unit" CssClass="field-label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plEmp" runat="server">
                                                <asp:CheckBoxList ID="chkEmp" runat="server">
                                                </asp:CheckBoxList>
                                            </asp:Panel>
                                        </div>
                                        <asp:CustomValidator ID="cvEmpNameValidate" runat="server" ErrorMessage="Please select the employee to be  alloacted"
                                            ValidationGroup="All1" EnableViewState="False" CssClass="error">*</asp:CustomValidator>
                                    </td>
                                    <td align="left" colspan="2">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plBus_unit" runat="server">
                                                <asp:RadioButtonList ID="rdBus_unit" runat="server">
                                                </asp:RadioButtonList>
                                            </asp:Panel>
                                        </div>
                                        <asp:CustomValidator ID="cvBsuValidate" runat="server" ErrorMessage="Please select the Business unit to be  alloacted"
                                            EnableViewState="False" ValidationGroup="All1">*</asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label2" runat="server" Text="Remark" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemark" runat="server" ValidationGroup="All1"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemark"
                                            ErrorMessage="Remark can not be left empty" ValidationGroup="All1" CssClass="error" Display="Dynamic">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="All1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>


                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>

