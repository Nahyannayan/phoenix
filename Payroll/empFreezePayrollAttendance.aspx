<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empFreezePayrollAttendance.aspx.vb" Inherits="Payroll_empFreezePayrollAttendance"
    Title="Asset Location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Freeze Payroll /Attendance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td>
                             <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td align="left" class="matters" width="30%"  >
                                        <asp:TextBox ID="txtBSUName" runat="server" TabIndex="1" MaxLength="100"  
                                            CssClass="inputbox" Enabled="False"  ></asp:TextBox>
                                    </td>
                                    <td align="left" class="matters" width="20%"  ><span class="field-label">Freeze Type</span>
                                    </td>
                                    <td align="left" class="matters"  width="30%"  >
                                        <asp:DropDownList ID="ddFreezeType" runat="server"   AutoPostBack="True">
                                            <asp:ListItem Selected="True" Value="P">Payroll</asp:ListItem>
                                            <asp:ListItem Value="A">Attendance</asp:ListItem>
                                            <asp:ListItem Value="O">Overtime</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Button ID="btnHistory" runat="server" CssClass="button" CausesValidation="False"
                                              Text="View History"  ></asp:Button>
                                        <ajaxToolkit:PopupControlExtender ID="pce" runat="server" PopupControlID="pnlDetail"
                                            TargetControlID="btnHistory" DynamicControlID="pnlDetail" DynamicServiceMethod="GetDynamicContent"
                                            Position="Left" BehaviorID="pce_0">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"  ><span class="field-label">Month</span>
                                    </td>
                                    <td align="left" class="matters"  >
                                        <asp:DropDownList ID="ddlPayMonth" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters" ><span class="field-label">Year</span>
                                    </td>
                                    <td align="left" class="matters" >
                                        <asp:DropDownList ID="ddlPayYear" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" ><span class="field-label">Freeze Date</span>
                                    </td>
                                    <td align="left" class="matters"    >
                                        <asp:TextBox ID="txtTrDate" runat="server"   AutoPostBack="True"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFrom" TargetControlID="txtTrDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtTrDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgTrDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvTrDate" ControlToValidate="txtTrDate"
                                            ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Remarks</span>
                                    </td>
                                    <td align="left" class="matters"   >
                                        <asp:TextBox ID="txtRemarks" runat="server"  TextMode="MultiLine" 
                                            EnableTheming="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Entered By</span></td>
                                    <td align="left" class="matters"    >
                                        <asp:TextBox ID="txtLoggedInuser" runat="server"   CssClass="inputbox"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td>

                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                        </td>
                    </tr>
                    <tr>
                        <td  align="center" >
                            <div id="divDetail" runat="server">
                                <asp:Panel ID="pnlDetail" runat="server" CssClass="panel-cover" >
                                    <table   width="100%">
                                        <tr class="title-bg">
                                            <td align="left" valign="middle"  >Freeze Payroll/Attendance History
                                            </td>
                                            <td align="right" valign="middle" style="height: 100%; width: 2%">
                                                <button id="lnkClose" runat="server" style="height: 100%; width: 100%">
                                                    X
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="matters" valign="top" align="center" colspan="2">
                                                <asp:Repeater ID="rptFreezeHistory" runat="server">
                                                    <HeaderTemplate>
                                                        <table border="1" width="100%"  >
                                                            <tr class="subheader_img">
                                                                <th align="center">Month
                                                                </th>
                                                                <th align="center">Payroll Freeze Date
                                                                </th>
                                                                <th align="center">Attendance Freeze Date
                                                                </th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <%#Container.DataItem("MONTH")%>
                                                            </td>
                                                            <td>
                                                                <%#Container.DataItem("PAYROLLFREEZE")%>
                                                            </td>
                                                            <td>
                                                                <%#Container.DataItem("ATTFREEZE")%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                            <asp:HiddenField ID="h_PFZ_ID" runat="server" />
                            <asp:HiddenField ID="h_BSU_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTrDate" TargetControlID="txtTrDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtTrDate" TargetControlID="txtTrDate">
                            </ajaxToolkit:CalendarExtender>
                            
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
