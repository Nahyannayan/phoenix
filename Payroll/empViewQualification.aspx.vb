Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class accIUViewJournalVoucher
    Inherits System.Web.UI.Page
    Dim menu_rights As String
    Dim datamode As String = "none"
    Dim content As ContentPlaceHolder
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mnuCode As String = Encr_decrData.Decrypt(IIf(Request.QueryString("MainMnu_code") IsNot Nothing, Request.QueryString("MainMnu_code").Replace(" ", "+"), String.Empty))
        'datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        datamode = "none"
        If Page.IsPostBack = False Or ViewState("mnuCode") <> mnuCode Then

            'hlAddNew.NavigateUrl = GetAddNewURL(mnuCode)
            ViewState("mnuCode") = mnuCode
            hlAddNew.NavigateUrl = GetAddNewURL(mnuCode)
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"


            '   --- Lijo's Code ---

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            lblError.Text = ""
            gvQualDetails.Attributes.Add("bordercolor", "#1b80b6")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or CurBsUnit = "" Then 'Or viewstate("mnuCode") <> "123" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("mnuCode"))
                'disable the control based on the rights
                content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(content, menu_rights, datamode)
                gvQualDetails.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
            End If
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvQualDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvQualDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
     
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        'str_Sid_img = h_Selected_menu_2.Value.Split("__")
        'getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        'str_Sid_img = h_Selected_menu_7.Value.Split("__")
        'getid("mnu_7_img", str_Sid_img(2))

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvQualDetails.RowDataBound
        Try
            Select Case ViewState("mnuCode").ToString
                Case OASISConstants.MenuEMPSCALE_GRADE
                    SetLinkEditButton_EmpGrade(e)

                Case OASISConstants.MenuEMPQUALIFICATION_CAT_M, _
                    OASISConstants.MenuEMPQUALIFICATION_M, _
                    OASISConstants.MenuEMPSUBJECT_M
                    SetLinkEditButton_EmpQualificatoin_CAT(e)
            End Select
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetLinkEditButton_EmpGrade(ByRef e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim lblID As New Label
            lblID = TryCast(e.Row.FindControl("lblID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblID IsNot Nothing Then
                ' Dim i As New Encryption64
                'hlCEdit.NavigateUrl = "journalvoucher.aspx?editid=" & lblGUID.Text
                Dim MainMnu_codeEncr As String = Encr_decrData.Encrypt(ViewState("mnuCode"))
                datamode = "view"
                datamode = Encr_decrData.Encrypt(datamode)
                Dim strID As String = Encr_decrData.Encrypt(lblID.Text)
                hlEdit.NavigateUrl = String.Format("empScalesGrade.aspx?MainMnu_code={0}&datamode={1}&Eid=" & strID, MainMnu_codeEncr, datamode)
                hlEdit.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetLinkEditButton_EmpQualificatoin_CAT(ByRef e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim lblID As New Label
            lblID = TryCast(e.Row.FindControl("lblID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblID IsNot Nothing Then
                ' Dim i As New Encryption64
                'hlCEdit.NavigateUrl = "journalvoucher.aspx?editid=" & lblGUID.Text
                Dim MainMnu_codeEncr As String = Encr_decrData.Encrypt(ViewState("mnuCode"))
                datamode = "view"
                datamode = Encr_decrData.Encrypt(datamode)
                Dim strID As String = Encr_decrData.Encrypt(lblID.Text)
                hlEdit.NavigateUrl = String.Format("empQualification.aspx?MainMnu_code={0}&datamode={1}&Eid=" & strID, MainMnu_codeEncr, datamode)
                hlEdit.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Select Case ViewState("mnuCode").ToString
            Case OASISConstants.MenuEMPQUALIFICATION_CAT_M
                GridBindEMPQUALIFICATION_CAT_M(p_selected_id)
                h_MnuCode.Value = "0"
            Case OASISConstants.MenuEMPSUBJECT_M
                GridBindEMPSUBJECT_M(p_selected_id)
                h_MnuCode.Value = "1"
            Case OASISConstants.MenuEMPQUALIFICATION_M
                GridBindEMPQUALIFICATION_M()
                h_MnuCode.Value = "2"
            Case OASISConstants.MenuEMPSCALE_GRADE
                GridBindEMPSCALE_GRADE()
                h_MnuCode.Value = "3"
        End Select
    End Sub

    Private Sub GridBindEMPSCALE_GRADE(Optional ByVal p_selected_id As Integer = -1)

        Dim strFiltID As String = String.Empty
        Dim strFiltDescr As String = String.Empty
        Dim strFiltSecDescr As String = String.Empty

        Dim strID As String = String.Empty
        Dim strDescr As String = String.Empty
        Dim strSecDescr As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim lblSecDescr As New Label

        lblPageCaption.Text = "Employee Scales Grade"

        gvQualDetails.Columns(4).Visible = False

        If gvQualDetails.Rows.Count > 0 Then
             '   --- FILTER CONDITIONS ---
            '   -- 1   
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
            strID = Trim(txtSearch.Text)
            If (strID <> "") Then strFiltID = SetCondn(lstrOpr, "SGD_ID", strID)

            '   -- 2 
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
            strDescr = txtSearch.Text
            If (strDescr <> "") Then strFiltDescr = SetCondn(lstrOpr, "SGD_DESCR", strDescr)

            '-- 3
            larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtSecDescr")
            strSecDescr = txtSearch.Text
            If (strSecDescr <> "") Then strFiltSecDescr = SetCondn(lstrOpr, "EGD_DESCR", strDescr)

        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = String.Empty
        Dim str_ListDoc As String = String.Empty

        str_Filter = strFiltID & strFiltDescr & strFiltSecDescr
        If str_Filter.Trim() <> "" Then str_Filter = " WHERE 1=1 " & str_Filter
        str_Sql = "SELECT  EMPSCALES_GRADES_M.GUID as GUID, EMPGRADES_M.EGD_DESCR as SEC_DESCR," & _
        " EMPSCALES_GRADES_M.SGD_ID as ID," & _
        " EMPSCALES_GRADES_M.SGD_DESCR as DESCR, '' AS SUBJECT  FROM EMPGRADES_M RIGHT OUTER JOIN" & _
        " EMPSCALES_GRADES_M ON EMPGRADES_M.EGD_ID = EMPSCALES_GRADES_M.SGD_EGD_ID " & str_Filter
        'str_Sql = "SELECT GUID, SUB_ID as ID, SUB_DESCR as DESCR, '' as SEC_DESCR ,'' AS SUBJECT FROM SUBJECT_M " & str_Filter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvQualDetails.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvQualDetails.DataBind()
            Dim columnCount As Integer = gvQualDetails.Rows(0).Cells.Count

            gvQualDetails.Rows(0).Cells.Clear()
            gvQualDetails.Rows(0).Cells.Add(New TableCell)
            gvQualDetails.Rows(0).Cells(0).ColumnSpan = columnCount
            gvQualDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvQualDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            gvQualDetails.SelectedIndex = -1
        Else
            gvQualDetails.DataBind()
            gvQualDetails.SelectedIndex = p_selected_id
        End If

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
        txtSearch.Text = strID

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
        txtSearch.Text = strDescr

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtSecDescr")
        txtSearch.Text = strSecDescr

        lblSecDescr = gvQualDetails.HeaderRow.FindControl("lblHeadSecDescr")
        lblSecDescr.Text = "GRADE DESCRIPTION"

        set_Menu_Img()
    End Sub

    Private Sub GridBindEMPSUBJECT_M(Optional ByVal p_selected_id As Integer = -1)

        Dim strFiltID As String = String.Empty
        Dim strFiltDescr As String = String.Empty
        Dim strFiltSecDescr As String = String.Empty

        Dim strID As String = String.Empty
        Dim strDescr As String = String.Empty
        Dim strSecDescr As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim lblSecDescr As New Label

        lblPageCaption.Text = "Subject Master"

        gvQualDetails.Columns(3).Visible = False
        gvQualDetails.Columns(4).Visible = False

        If gvQualDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)

            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
            strID = Trim(txtSearch.Text)
            If (strID <> "") Then strFiltID = SetCondn(lstrOpr, "SUB_ID", strID)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
            strDescr = txtSearch.Text
            If (strDescr <> "") Then strFiltDescr = SetCondn(lstrOpr, "SUB_DESCR", strDescr)
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = String.Empty
        Dim str_ListDoc As String = String.Empty

        str_Filter = strFiltID & strFiltDescr & strFiltSecDescr
        If str_Filter.Trim() <> "" Then str_Filter = " WHERE 1=1 " & str_Filter
        str_Sql = "SELECT GUID, SBM_ID as ID, SBM_DESCR as DESCR, '' as SEC_DESCR ,'' AS SUBJECT FROM SUBJECT_M " & str_Filter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvQualDetails.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvQualDetails.DataBind()
            Dim columnCount As Integer = gvQualDetails.Rows(0).Cells.Count

            gvQualDetails.Rows(0).Cells.Clear()
            gvQualDetails.Rows(0).Cells.Add(New TableCell)
            gvQualDetails.Rows(0).Cells(0).ColumnSpan = columnCount
            gvQualDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvQualDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            gvQualDetails.SelectedIndex = -1
        Else
            gvQualDetails.DataBind()
            gvQualDetails.SelectedIndex = p_selected_id
        End If

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
        txtSearch.Text = strID

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
        txtSearch.Text = strDescr

        set_Menu_Img()

    End Sub

    Private Sub GridBindEMPQUALIFICATION_M(Optional ByVal p_selected_id As Integer = -1)

        Dim strFiltID As String = String.Empty
        Dim strFiltDescr As String = String.Empty
        Dim strFiltSecDescr As String = String.Empty
        Dim strFiltSubject As String = String.Empty

        Dim strID As String = String.Empty
        Dim strDescr As String = String.Empty
        Dim strSecDescr As String = String.Empty
        Dim strSubject As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim lblSecDescr As New Label

        lblPageCaption.Text = "Employee Qualification"

        If gvQualDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)

            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
            strID = Trim(txtSearch.Text)
            If (strID <> "") Then strFiltID = SetCondn(lstrOpr, "QLF_ID", strID)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
            strDescr = txtSearch.Text
            If (strDescr <> "") Then strFiltDescr = SetCondn(lstrOpr, "QLF_DESCR", strDescr)

            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtSubject")
            strSubject = txtSearch.Text
            If (strSubject <> "") Then strFiltSubject = SetCondn(lstrOpr, "SUB_DESCR", strDescr)

            larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtSecDescr")
            strSecDescr = txtSearch.Text
            If (strSecDescr <> "") Then strFiltSecDescr = SetCondn(lstrOpr, "QCT_Descr", strDescr)

        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = String.Empty
        Dim str_ListDoc As String = String.Empty

        str_Filter = strFiltID & strFiltDescr & strFiltSecDescr & strFiltSubject
        If str_Filter.Trim() <> "" Then str_Filter = " WHERE 1=1 " & str_Filter
        str_Sql = "SELECT  GUID, QLF_ID as ID, QLF_DESCR as DESCR,QCT_Descr as SEC_DESCR, SUB_DESCR AS SUBJECT FROM vw_OSO_EMPQUALIFICATION_M " & str_Filter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvQualDetails.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvQualDetails.DataBind()
            Dim columnCount As Integer = gvQualDetails.Rows(0).Cells.Count

            gvQualDetails.Rows(0).Cells.Clear()
            gvQualDetails.Rows(0).Cells.Add(New TableCell)
            gvQualDetails.Rows(0).Cells(0).ColumnSpan = columnCount
            gvQualDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvQualDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            gvQualDetails.SelectedIndex = -1
        Else
            gvQualDetails.DataBind()
            gvQualDetails.SelectedIndex = p_selected_id
        End If

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
        txtSearch.Text = strID

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
        txtSearch.Text = strDescr

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtSubject")
        txtSearch.Text = strSubject

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtSecDescr")
        txtSearch.Text = strSecDescr

        set_Menu_Img()

    End Sub

    Private Sub GridBindEMPQUALIFICATION_CAT_M(Optional ByVal p_selected_id As Integer = -1)

        Dim strFiltID As String = String.Empty
        Dim strFiltDescr As String = String.Empty
        Dim strFiltSecDescr As String = String.Empty

        Dim strID As String = String.Empty
        Dim strDescr As String = String.Empty
        Dim strSecDescr As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim lblSecDescr As New Label

        lblPageCaption.Text = "Employee Qualification Category"

        gvQualDetails.Columns(4).Visible = False

        If gvQualDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)

            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
            strID = Trim(txtSearch.Text)
            If (strID <> "") Then strFiltID = SetCondn(lstrOpr, "QCT_ID", strID)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
            strDescr = txtSearch.Text
            If (strDescr <> "") Then strFiltDescr = SetCondn(lstrOpr, "QCT_Descr", strDescr)

            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvQualDetails.HeaderRow.FindControl("txtSecDescr")
            strSecDescr = txtSearch.Text
            If (strSecDescr <> "") Then strFiltSecDescr = SetCondn(lstrOpr, "QCT_SHORT", strSecDescr)
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = String.Empty
        Dim str_ListDoc As String = String.Empty

        str_Filter = strFiltID & strFiltDescr & strFiltSecDescr
        If str_Filter.Trim() <> "" Then str_Filter = " WHERE 1=1 " & str_Filter
        str_Sql = "SELECT GUID, QCT_ID as ID, QCT_Descr as DESCR, QCT_SHORT as SEC_DESCR ,'' AS SUBJECT FROM EMPQUALIFICATION_CAT_M " & str_Filter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvQualDetails.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvQualDetails.DataBind()
            Dim columnCount As Integer = gvQualDetails.Rows(0).Cells.Count

            gvQualDetails.Rows(0).Cells.Clear()
            gvQualDetails.Rows(0).Cells.Add(New TableCell)
            gvQualDetails.Rows(0).Cells(0).ColumnSpan = columnCount
            gvQualDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvQualDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            gvQualDetails.SelectedIndex = -1
        Else
            gvQualDetails.DataBind()
            gvQualDetails.SelectedIndex = p_selected_id
        End If

        lblSecDescr = gvQualDetails.HeaderRow.FindControl("lblHeadSecDescr")
        lblSecDescr.Text = "SHORT DESCRIPTION"

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtID")
        txtSearch.Text = strID

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtDescr")
        txtSearch.Text = strDescr

        txtSearch = gvQualDetails.HeaderRow.FindControl("txtSecDescr")
        txtSearch.Text = strSecDescr

        set_Menu_Img()

    End Sub

     
    Function GetAddNewURL(ByVal mnuCode As String) As String

        datamode = "add"
        datamode = Encr_decrData.Encrypt(datamode)
        mnuCode = Encr_decrData.Encrypt(mnuCode)
        Select Case ViewState("mnuCode").ToString
            Case OASISConstants.MenuEMPSCALE_GRADE
                Return String.Format("empScalesGrade.aspx?MainMnu_code={0}&datamode={1}", mnuCode, datamode)
            Case OASISConstants.MenuEMPQUALIFICATION_CAT_M, _
                OASISConstants.MenuEMPQUALIFICATION_M, _
                OASISConstants.MenuEMPSUBJECT_M
                Return String.Format("empQualification.aspx?MainMnu_code={0}&datamode={1}", mnuCode, datamode)
        End Select
        Return String.Empty
    End Function
    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvQualDetails.PageIndexChanging
        gvQualDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

End Class
