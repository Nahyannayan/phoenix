﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empCListCutoffdate.aspx.vb" Inherits="Payroll_empCListCutoffdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            C List Cutoff Date
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                <%--  <tr class="title-bg">
                        <td align="left" colspan="9" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana"></span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Financial Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlacademicyear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:GridView ID="gvCLIST" runat="server" EmptyDataText="No Records" AutoGenerateColumns="false" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("CCD_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Month">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmnth" runat="server" Text='<%# Bind("MTH")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle Width="15%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblyear" runat="server" Text='<%# Bind("YER")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle Width="15%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cutoff Date">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCut" runat="server" Text='<%# Bind("CCD_DATE", "{0:dd/MMM/yyyy}")%>'></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                            Format="dd/MMM/yyyy" TargetControlID="txtCut">
                                                        </ajaxToolkit:CalendarExtender>
                                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgFrom" TargetControlID="txtCut">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="350px" />
                                                    <HeaderStyle Width="50%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Save" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</asp:Content>

