﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet

Partial Class Payroll_empDependantsExcelUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass

    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        gvDependancedetails.DataSource = Nothing
        gvDependancedetails.DataBind()
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ext = ".xls" Then
                '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                ' getdataExcel(strFileName)
                UpLoadDBF()
                If gvDependancedetails.Rows.Count > 0 Then
                    btnSaveToDB.Visible = True
                Else
                    btnSaveToDB.Visible = False

                End If

            Else
                lblError.Text = "Please upload .xls files only."
            End If

        Else
            lblError.Text = "File not uploaded"
        End If

        'Dim DS As New DataSet
        'MyCommand.Fill(DS)
        'Dim Dt As DataTable = DS.Tables(0)
        'gvNewdata.DataSource = Dt
        'gvNewdata.DataBind()

    End Sub
    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Dim rowId As Integer = 0
        Try
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 16)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(1).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(2).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next



            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If

            Dim DependantValidStudent As New DataTable
            Dim ValidEmployee As New DataTable
            Dim DependantValidEmployee As New DataTable
            Dim DependantBSU As New DataTable
            Dim ValidRelation As New DataTable
            Dim ValidInsurance As New DataTable
            Dim ValidNationality As New DataTable
            Dim chkDuplicate As New DataTable
            Dim mTable As New DataTable

            mTable = CreateDependanceDetailsTable()



            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                ValidEmployee = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in (" & strEmpNos & ") and emp_bsu_id='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)


                mRow = mTable.NewRow
                mRow("UniqueID") = xlRow(0)

                mRow("EMPNAME") = xlRow(1)
                mRow("EMPNO") = xlRow(2)
                mRow("EDD_NAME") = xlRow(3)

                mRow("EDD_bMale") = xlRow(4)


                mRow("EDD_bMaritalStatus") = xlRow(6)

                mRow("EDD_GEMS_TYPE_DESCR") = xlRow(7)

                mRow("EDD_GEMS_DisplayID") = xlRow(8).ToString
                mRow("EDD_BSU_NAME") = xlRow(9).ToString
                mRow("EDD_RELATIONDescr") = xlRow(10).ToString
                mRow("EDD_DOB") = xlRow(11).ToString
                mRow("EDD_bCompanyInsurance") = xlRow(12).ToString


                mRow("EDD_EMIRATES_ID") = xlRow(14).ToString


                If xlRow(15).ToString <> "" Then
                    If Not IsDate(xlRow(15).ToString) Then
                        lblError.Text = "Please correct Emirated Id expiry date format, Sl No. :" & mRow("UniqueID")
                        Exit Sub
                    Else
                        mRow("EDD_EID_EXPDT") = xlRow(15).ToString
                    End If
                End If

                If xlRow(7) = "Gems Staff" Then
                    mRow("EDD_GEMS_TYPE") = "E"
                ElseIf xlRow(7) = "Gems Student" Then
                    mRow("EDD_GEMS_TYPE") = "S"
                Else
                    mRow("EDD_GEMS_TYPE") = "N"
                End If



                'mRow("EDD_EMP_ID")=
                If ValidEmployee.Select("Empno='" & xlRow(2) & "'", "").Length = 0 Then

                    lblError.Text = "Invalid Employee No:" & mRow("EMPNO") & ", Sl No. :" & mRow("UniqueID") & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidEmployee.Select("Empno='" & mRow("EMPNO") & "'", "")(0)
                    If ValidEmployee.Rows.Count > 0 Then
                        mRow("EMP_ID") = drEmp("Emp_id")
                        mRow("EMPName") = drEmp("EMPNAME")
                    End If


                End If
                ValidRelation = Mainclass.getDataTable("select RLT_ID,RLT_DESCR from EMPRELATIONS_M where RLT_DESCR = '" & xlRow(10).ToString & "'", ConnectionManger.GetOASISConnectionString)
                If ValidRelation.Rows.Count = 0 Then
                    lblError.Text = "Please specify relation : Sl No. :" & mRow("UniqueID")
                    Exit Sub
                Else
                    mRow("EDD_RELATION") = ValidRelation.Rows(0).Item("RLT_ID")
                End If


                chkDuplicate = Mainclass.getDataTable("select count(*) from [EMPDEPENDANTS_D] where EDD_EMP_ID=" & mRow("EMP_ID") & " and edd_name='" & mRow("EDD_NAME") & "'", ConnectionManger.GetOASISConnectionString)
                If chkDuplicate.Rows(0).Item(0) > 0 Then
                    lblError.Text = "Duplicate dependant data found for Employee No:" & mRow("EMPNO") & ", Sl No. :" & mRow("UniqueID") & " : " & mRow("EDD_NAME")
                    Exit Sub
                End If

                If mRow("EDD_GEMS_TYPE") <> "N" Then
                    DependantBSU = Mainclass.getDataTable("select Isnull(BSU_ID,'') BSU_ID from BUSINESSUNIT_M where bsu_name = ('" & mRow("EDD_BSU_NAME") & "') or  BSU_SHORTNAME='" & mRow("EDD_BSU_NAME") & "'", ConnectionManger.GetOASISConnectionString)
                    If DependantBSU.Rows.Count > 0 Then

                        mRow("EDD_BSU_ID") = DependantBSU.Rows(0).Item("BSU_ID").ToString
                    Else

                        lblError.Text = "Dependant BSU name is not correct., Sl No. :" & mRow("UniqueID")
                        Exit Sub
                    End If
                    If mRow("EDD_GEMS_TYPE") = "E" Then
                        DependantValidEmployee = Mainclass.getDataTable("select empno,emp_id,EMPNAME from vw_OSO_EMPLOYEEMASTER where empno in ('" & mRow("EDD_GEMS_DisplayID") & "') and emp_bsu_id='" & DependantBSU.Rows(0).Item("BSU_ID").ToString & "'", ConnectionManger.GetOASISConnectionString)
                        If DependantValidEmployee.Select("Empno='" & mRow("EDD_GEMS_DisplayID") & "'", "").Length = 0 Then
                            lblError.Text = "Invalid Dependant Employee No:" & mRow("EDD_GEMS_DisplayID") & ", Sl No. :" & mRow("UniqueID") & " for Unit " & mRow("EDD_BSU_NAME")
                            Exit Sub
                        End If

                    Else
                        DependantValidEmployee = Mainclass.getDataTable("select STU_ID emp_id,STU_NO empno,Name EMPNAME  from STUDENTS where STU_NO = ('" & mRow("EDD_GEMS_DisplayID") & "') and STU_bsu_id='" & DependantBSU.Rows(0).Item("BSU_ID").ToString & "'", ConnectionManger.GetOASISConnectionString)
                        If DependantValidEmployee.Select("Empno='" & mRow("EDD_GEMS_DisplayID") & "'", "").Length = 0 Then
                            lblError.Text = "Invalid Student No:" & mRow("EDD_GEMS_DisplayID") & ", Sl No. :" & mRow("UniqueID") & " for Unit " & mRow("EDD_BSU_NAME")
                            Exit Sub
                        End If
                    End If


                    Dim drEmp As DataRow
                    drEmp = DependantValidEmployee.Select("Empno='" & mRow("EDD_GEMS_DisplayID") & "'", "")(0)
                    If DependantValidEmployee.Rows.Count > 0 Then
                        mRow("EDD_GEMS_ID") = DependantValidEmployee.Rows(0).Item("Emp_id")
                        mRow("EDD_NAME") = DependantValidEmployee.Rows(0).Item("EMPNAME")
                    End If


                End If
                If xlRow(13).ToString <> "" Then
                    ValidInsurance = Mainclass.getDataTable("select INSU_ID,[INSU_DESCR]+'('+ [INSU_SUBDESCR]+')' as Descr  from [INSURANCETYPE_M] where [INSU_DESCR]+'('+ [INSU_SUBDESCR]+')' = '" & xlRow(13).ToString & "'", ConnectionManger.GetOASISConnectionString)
                    If ValidInsurance.Rows.Count > 0 Then
                        mRow("EDD_Insu_id") = ValidInsurance.Rows(0).Item("INSU_ID")
                        mRow("InsuranceCategoryDESC") = ValidInsurance.Rows(0).Item("Descr")
                    Else
                        lblError.Text = "Incorrect Insurance category : Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If

                End If
                If xlRow(5) <> "" Then
                    ValidNationality = Mainclass.getDataTable("select cty_id,cty_nationality from country_m where cty_nationality='" & xlRow(5).ToString & "'", ConnectionManger.GetOASISConnectionString)
                    If ValidNationality.Rows.Count > 0 Then
                        mRow("EDD_Nationality") = ValidNationality.Rows(0).Item("cty_nationality")
                        mRow("EDD_CTY_ID") = ValidNationality.Rows(0).Item("cty_id")
                    Else
                        lblError.Text = "Please check Nationality  : Sl No. " & mRow("UniqueID")
                        Exit Sub
                    End If
                End If



                rowId = rowId + 1
                mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()
            'gvFeeDetails.DataSource = mTable
            'gvFeeDetails.DataBind()
            gvDependancedetails.DataSource = mTable
            Session("EMPDEPENDANCEDETAILS") = mTable
            gvDependancedetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message & "Row No. " & rowId
        End Try
    End Sub


    Private Function CreateDependanceDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cEDD_NAME As New DataColumn("EDD_NAME", System.Type.GetType("System.String"))
            Dim cEDD_RELATION As New DataColumn("EDD_RELATION", System.Type.GetType("System.String"))
            Dim cEDD_RELATIONDescr As New DataColumn("EDD_RELATIONDescr", System.Type.GetType("System.String"))

            Dim cEDD_DOB As New DataColumn("EDD_DOB", System.Type.GetType("System.DateTime"))
            Dim cEDD_NoofTicket As New DataColumn("EDD_NoofTicket", System.Type.GetType("System.Double"))
            Dim cbConcession As New DataColumn("bConcession", System.Type.GetType("System.Boolean"))
            Dim cEDD_STU_ID As New DataColumn("EDD_STU_ID", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cInsuranceCategory As New DataColumn("EDD_Insu_id", System.Type.GetType("System.String"))
            Dim cInsuranceCategoryDESC As New DataColumn("InsuranceCategoryDESC", System.Type.GetType("System.String"))
            Dim cCompanyPaidInsurance As New DataColumn("EDD_bCompanyInsurance", System.Type.GetType("System.String"))
            Dim cGemsType As New DataColumn("EDD_GEMS_TYPE", System.Type.GetType("System.String"))
            Dim cGemsTypeDESC As New DataColumn("EDD_GEMS_TYPE_DESCR", System.Type.GetType("System.String"))
            Dim cEDD_GEMS_ID As New DataColumn("EDD_GEMS_ID", System.Type.GetType("System.String"))
            Dim cEDD_GEMS_DisplayID As New DataColumn("EDD_GEMS_DisplayID", System.Type.GetType("System.String"))

            Dim cEDD_BSU_ID As New DataColumn("EDD_BSU_ID", System.Type.GetType("System.String"))
            Dim cEDD_BSU_NAME As New DataColumn("EDD_BSU_NAME", System.Type.GetType("System.String"))
            Dim cEDD_EmiratesID As New DataColumn("EDD_EMIRATES_ID", System.Type.GetType("System.String"))
            Dim cEDD_EID_ExpiryDT As New DataColumn("EDD_EID_EXPDT", System.Type.GetType("System.DateTime"))
            Dim cEDD_PHOTO As New DataColumn("EDD_PHOTO", System.Type.GetType("System.String"))
            Dim cEMPNO As New DataColumn("EMPNO", System.Type.GetType("System.String"))
            Dim cEMPNAME As New DataColumn("EMPNAME", System.Type.GetType("System.String"))
            Dim cEMPID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim cEDD_bMale As New DataColumn("EDD_bMale", System.Type.GetType("System.String"))
            Dim cEDD_bMarritalStatus As New DataColumn("EDD_bMaritalStatus", System.Type.GetType("System.String"))
            Dim cEDD_CTY_ID As New DataColumn("EDD_CTY_ID", System.Type.GetType("System.String"))
            Dim cEDD_Nationality As New DataColumn("EDD_Nationality", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEDD_NAME)
            dtDt.Columns.Add(cEDD_RELATION)
            dtDt.Columns.Add(cEDD_RELATIONDescr)
            dtDt.Columns.Add(cEDD_DOB)
            dtDt.Columns.Add(cEDD_NoofTicket)
            dtDt.Columns.Add(cbConcession)
            dtDt.Columns.Add(cEDD_STU_ID)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cInsuranceCategory)
            dtDt.Columns.Add(cInsuranceCategoryDESC)
            dtDt.Columns.Add(cCompanyPaidInsurance)
            dtDt.Columns.Add(cGemsType)
            dtDt.Columns.Add(cGemsTypeDESC)
            dtDt.Columns.Add(cEDD_GEMS_ID)
            dtDt.Columns.Add(cEDD_GEMS_DisplayID)

            dtDt.Columns.Add(cEDD_BSU_ID)
            dtDt.Columns.Add(cEDD_BSU_NAME)

            dtDt.Columns.Add(cEDD_EmiratesID)
            dtDt.Columns.Add(cEDD_EID_ExpiryDT)
            dtDt.Columns.Add(cEDD_PHOTO)
            dtDt.Columns.Add(cEMPNO)
            dtDt.Columns.Add(cEMPNAME)
            dtDt.Columns.Add(cEMPID)


            dtDt.Columns.Add(cEDD_bMale)
            dtDt.Columns.Add(cEDD_bMarritalStatus)
            dtDt.Columns.Add(cEDD_CTY_ID)
            dtDt.Columns.Add(cEDD_Nationality)

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnLoad)
        smScriptManager.RegisterPostBackControl(btnSaveToDB)
        Me.Page.Form.Enctype = "multipart/form-data"

        'smScriptManager.RegisterPostBackControl(gvExport)
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        End If

    End Sub
    

    Protected Sub btnSaveToDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveToDB.Click
        Dim status As Boolean

        status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("sBsuid") & "-" & "Dependents Data", "Excel Upload", Page.User.Identity.Name.ToString, Me.Page)
        If status <> 0 Then
            Throw New ArgumentException("Could not complete your request")
            Exit Sub
        End If
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try


                transaction = conn.BeginTransaction("SampleTransaction")
                Dim retVal As Integer = SaveDependanceDetails(conn, transaction)
                If retVal = 0 Then
                    transaction.Commit()
                    lblError.Text = "Data saved successfully!"
                    gvDependancedetails.DataSource = Session("EMPDEPENDANCEDETAILS")
                    gvDependancedetails.DataBind()

                Else
                    transaction.Rollback()
                    If lblError.Text = "" Then
                        lblError.Text = "Error occured while saving."
                    End If

                End If
            Catch ex As Exception
                lblError.Text = "Error occured while saving."
            End Try

        End Using
    End Sub
    Private Function SaveDependanceDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Try
            If Session("EMPDEPENDANCEDETAILS") Is Nothing Then
                Return 0
            End If
           
            Dim mt As DataTable = Session("EMPDEPENDANCEDETAILS")
            Dim counts As Integer = 0
            For Each dtrow As GridViewRow In gvDependancedetails.Rows
                Dim chkChecked As CheckBox = CType(dtrow.FindControl("chkPay"), CheckBox)
                If chkChecked.Checked = True Then
                    counts = counts + 1
                End If
            Next
            If counts = 0 Then
                lblError.Text = "Please select atleast 1 record to save."
                Return 1000

            End If

            For Each dtrow As GridViewRow In gvDependancedetails.Rows
                Dim chkChecked As CheckBox = CType(dtrow.FindControl("chkPay"), CheckBox)
                Dim flDepImage As FileUpload = CType(dtrow.FindControl("flDepImage"), FileUpload)
                Dim lblEDD_NAME As Label = CType(dtrow.FindControl("lblEDD_NAME"), Label)
                Dim lblEDD_DOB As Label = CType(dtrow.FindControl("lblEDD_DOB"), Label)
                Dim lblEDD_bCompanyInsurance As Label = CType(dtrow.FindControl("lblEDD_bCompanyInsurance"), Label)
                Dim lblEDD_EMIRATES_ID As Label = CType(dtrow.FindControl("lblEDD_EMIRATES_ID"), Label)
                Dim lblEDD_EID_EXPDT As Label = CType(dtrow.FindControl("lblEDD_EID_EXPDT"), Label)
                Dim lblEDD_bMale As Label = CType(dtrow.FindControl("lblEDD_bMale"), Label)
                Dim lblIsMarried As Label = CType(dtrow.FindControl("lblIsMarried"), Label)

                If chkChecked.Checked = True Then

                    Dim cmd As New SqlCommand
                    cmd = New SqlCommand("SaveEMPDEPENDANTS_D_Excel", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandTimeout = 0
                    Dim pParms(14) As SqlClient.SqlParameter

                    Dim sqlpESL_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
                    sqlpESL_EMP_ID.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EMP_ID"))
                    cmd.Parameters.Add(sqlpESL_EMP_ID)
                    lblError.Text = sqlpESL_EMP_ID.Value

                    Dim sqlpEDD_NAME As New SqlParameter("@EDD_NAME", SqlDbType.VarChar, 200)
                    sqlpEDD_NAME.Value = lblEDD_NAME.Text
                    cmd.Parameters.Add(sqlpEDD_NAME)

                    Dim sqlpEDD_RELATION As New SqlParameter("@EDD_RELATION", SqlDbType.VarChar, 30)
                    sqlpEDD_RELATION.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_RELATION"))
                    cmd.Parameters.Add(sqlpEDD_RELATION)

                    Dim sqlpEDD_DOB As New SqlParameter("@EDD_DOB", SqlDbType.DateTime, 20)
                    If (Convert.ToString(lblEDD_DOB.Text) = "") Then
                        sqlpEDD_DOB.Value = DBNull.Value
                    Else
                        sqlpEDD_DOB.Value = lblEDD_DOB.Text
                    End If
                    'sqlpEDD_DOB.Value = IIf(dr("EDD_DOB") = "", DBNull.Value, dr("EDD_DOB"))
                    cmd.Parameters.Add(sqlpEDD_DOB)

                    Dim sqlpEDD_bCONCESSION As New SqlParameter("@EDD_bCONCESSION", SqlDbType.Bit)
                    sqlpEDD_bCONCESSION.Value = 0
                    cmd.Parameters.Add(sqlpEDD_bCONCESSION)

                    Dim sqlpEDD_NoofTicket As New SqlParameter("@EDD_NoofTicket", SqlDbType.TinyInt)
                    sqlpEDD_NoofTicket.Value = 0
                    cmd.Parameters.Add(sqlpEDD_NoofTicket)

                    Dim sqlpEDD_STU_ID As New SqlParameter("@EDD_STU_ID", SqlDbType.Int)
                    sqlpEDD_STU_ID.Value = 0
                    cmd.Parameters.Add(sqlpEDD_STU_ID)

                    Dim sqlpEDD_Insu_id As New SqlParameter("@EDD_Insu_id", SqlDbType.Int)
                    If gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_Insu_id") <> "" Then
                        sqlpEDD_Insu_id.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_Insu_id"))
                    Else
                        sqlpEDD_Insu_id.Value = DBNull.Value
                    End If

                    cmd.Parameters.Add(sqlpEDD_Insu_id)

                    Dim sqlpEDD_bCompanyInsurance As New SqlParameter("@EDD_bCompanyInsurance", SqlDbType.Bit)
                    If lblEDD_bCompanyInsurance.Text = "Yes" Then
                        sqlpEDD_bCompanyInsurance.Value = True
                    Else
                        sqlpEDD_bCompanyInsurance.Value = False
                    End If
                    cmd.Parameters.Add(sqlpEDD_bCompanyInsurance)

                    Dim sqlpEDD_GEMSTYPE As New SqlParameter("@EDD_GEMS_TYPE", SqlDbType.VarChar)
                    sqlpEDD_GEMSTYPE.Value = Convert.ToString(gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_GEMS_TYPE"))
                    cmd.Parameters.Add(sqlpEDD_GEMSTYPE)

                    Dim sqlpEDD_GEMS_ID As New SqlParameter("@EDD_GEMS_ID", SqlDbType.VarChar)
                    sqlpEDD_GEMS_ID.Value = Convert.ToString(gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_GEMS_ID"))
                    cmd.Parameters.Add(sqlpEDD_GEMS_ID)

                    Dim sqlpEDD_BSU_ID As New SqlParameter("@EDD_BSU_ID", SqlDbType.VarChar)
                    sqlpEDD_BSU_ID.Value = Convert.ToString(gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_BSU_ID"))
                    cmd.Parameters.Add(sqlpEDD_BSU_ID)

                    Dim sqlpStatus As New SqlParameter("@Action", SqlDbType.VarChar)
                    sqlpStatus.Value = "insert"
                    cmd.Parameters.Add(sqlpStatus)

                    Dim sqlpEmiratesID As New SqlParameter("@EDD_EMIRATES_ID", SqlDbType.VarChar)
                    sqlpEmiratesID.Value = lblEDD_EMIRATES_ID.Text
                    cmd.Parameters.Add(sqlpEmiratesID)

                    Dim sqlpExpiryDt As New SqlParameter("@EDD_EID_EXPDT", SqlDbType.DateTime)
                    If IsDate(lblEDD_EID_EXPDT.Text) Then
                        sqlpExpiryDt.Value = lblEDD_EID_EXPDT.Text
                    Else
                        sqlpExpiryDt.Value = DBNull.Value

                    End If

                    cmd.Parameters.Add(sqlpExpiryDt)


                    If flDepImage.HasFile Then
                        If flDepImage.PostedFile.ContentLength > 50500 Then '4MB
                            'MsgBox("Photo not to exceed 50 KB in size!!!", MsgBoxStyle.Information, "Photo size!!!")
                            lblError.Text = "Photo not to exceed 50 KB in size!!!"
                            iReturnvalue = 1000
                            Return iReturnvalue

                        End If

                        Dim fs As Stream = flDepImage.PostedFile.InputStream
                        Dim br As New BinaryReader(fs)
                        Dim bytes As Byte() = br.ReadBytes(fs.Length)
                        Dim sqlImage As New SqlParameter("@EDD_PHOTO", SqlDbType.Image)
                        'Dim filePath As String = flDepImage.PostedFile.FileName
                        sqlImage.Value = bytes ' ConvertImageFiletoBytes(filePath)
                        cmd.Parameters.Add(sqlImage)
                    End If

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    Dim retMsg As New SqlParameter("@errMsg", SqlDbType.VarChar, 300)
                    retMsg.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(retMsg)

                    Dim sqlpMale As New SqlParameter("@EDD_bMALE", SqlDbType.Bit)
                    If lblEDD_bMale.Text = "Male" Then
                        sqlpMale.Value = True
                    Else
                        sqlpMale.Value = False
                    End If
                    cmd.Parameters.Add(sqlpMale)

                    Dim sqlpNationality As New SqlParameter("@EDD_CTY_ID", SqlDbType.Int)
                    If gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_CTY_ID") <> "" Then
                        sqlpNationality.Value = Convert.ToInt32(gvDependancedetails.DataKeys(dtrow.RowIndex)("EDD_CTY_ID"))
                    Else
                        sqlpNationality.Value = DBNull.Value
                    End If
                    cmd.Parameters.Add(sqlpNationality)

                    Dim sqlpMaritalStatus As New SqlParameter("@EDD_MARITALSTATUS", SqlDbType.Int)
                    If lblIsMarried.Text <> "" Then
                        If lblIsMarried.Text = "Married" Then
                            sqlpMaritalStatus.Value = 0
                        Else
                            sqlpMaritalStatus.Value = 1

                        End If
                    Else
                        sqlpMaritalStatus.Value = DBNull.Value
                    End If
                    cmd.Parameters.Add(sqlpMaritalStatus)


                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = UtilityObj.getErrorMessage(iReturnvalue) & retMsg.Value.ToString
                        Exit For
                    Else

                        mt.Rows(dtrow.RowIndex).Delete()

                    End If


                End If
            Next
            mt.AcceptChanges()
            If iReturnvalue = 0 Then
                Session("EMPDEPENDANCEDETAILS") = mt
            End If

            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = ex.Message
            Return 1000
        End Try
        Return False
    End Function
    Public Shared Function SaveDocumentDetail(ByRef EDC_Detail As EOS_EmployeeDocument.DocumentDetail) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(12) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDC_ID", EDC_Detail.EDC_ID, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@EDC_TYPE", EDC_Detail.EDC_TYPE, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@EDC_ESD_ID", EDC_Detail.EDC_ESD_ID, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@EDC_BSU_ID", EDC_Detail.EDC_BSU_ID, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@EDC_EMP_ID", EDC_Detail.EDC_EMP_ID, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@EDC_EDD_ID", EDC_Detail.EDC_EDD_ID, SqlDbType.Int)
            sqlParam(6) = Mainclass.CreateSqlParameter("@EDC_NARRATION", EDC_Detail.EDC_NARRATION, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@EDC_EXP_DT", EDC_Detail.EDC_EXP_DT, SqlDbType.DateTime)
            sqlParam(8) = Mainclass.CreateSqlParameter("@EDC_bREqReminder", EDC_Detail.EDC_bREqReminder, SqlDbType.Bit)
            sqlParam(9) = Mainclass.CreateSqlParameter("@EDC_Bactive", EDC_Detail.EDC_Bactive, SqlDbType.Bit)
            sqlParam(10) = Mainclass.CreateSqlParameter("@EDC_DOCUMENT", EDC_Detail.EDC_DOCUMENT, SqlDbType.VarBinary)
            sqlParam(11) = Mainclass.CreateSqlParameter("@EDC_CONTENT_TYPE", EDC_Detail.EDC_CONTENT_TYPE, SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "EOS_SaveEmpDocumentDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(12).Value = "" Then
                SaveDocumentDetail = ""
                EDC_Detail.EDC_ID = sqlParam(0).Value
            Else
                SaveDocumentDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(12).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Protected Sub gvDependancedetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDependancedetails.PageIndexChanging
        gvDependancedetails.PageIndex = e.NewPageIndex
        gvDependancedetails.DataSource = Session("EMPDEPENDANCEDETAILS")
        gvDependancedetails.DataBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Master.DisableScriptManager()
    End Sub

  
    Protected Sub ExporttoExcel(ByVal ds As DataSet, ByVal IbanNo As String)

        Dim dt As DataTable

        dt = ds.Tables(0)
        'swapna added for name format
        Dim strmonth As String, strMinute As String
        strmonth = CStr(Today.Date.Month)
        strMinute = CStr(Now.Minute.ToString)

        If strmonth.Length = 1 Then
            strmonth = "0" + strmonth
        End If
        If strMinute.Length = 1 Then
            strMinute = "0" + strMinute
        End If


        'swapna adding for naming format
        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + "swapna" 'IbanNo.Substring(IbanNo.Length - 12).Replace("-", "") + CStr(Today.Date.Day) + strmonth + CStr(Today.Date.Year) + Now.Hour.ToString + strMinute + ".xls" ' Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") +

        ' Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xls"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        System.IO.File.Delete(tempFileName)

    End Sub

   


End Class
