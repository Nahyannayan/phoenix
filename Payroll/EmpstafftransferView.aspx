<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EmpstafftransferView.aspx.vb" Inherits="EmpstafftransferView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function Selrdbtn(id) {
            var rdBtn = document.getElementById(id);

            var grid = document.getElementById('<%= gvJournal.ClientID %>');

            var List = grid.getElementsByTagName("input");
            for (i = 0; i < List.length; i++) {
                if (List[i].type == "radio" && List[i].id != rdBtn.id) {
                    List[i].checked = false;
                }
            }
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Employee Status"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr valign="top">

                        <td class="matters" valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
                        <td align="right">&nbsp;<input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table width="100%">
                    <tr>
                        <td align="center" valign="top" class="matters" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="Emp Name" SortExpression="EMP_NAME">
                                        <HeaderTemplate>
                                            Emp Name<br />
                                            <asp:TextBox ID="txtEmpname" runat="server"  ></asp:TextBox></td>
                                                                    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            Business Unit<br />
                                            <asp:TextBox ID="txtBSUName" runat="server"  ></asp:TextBox></td>
                                                                    <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transfer Date">
                                        <HeaderTemplate>
                                            Transfer Date<br />
                                            <asp:TextBox ID="txtDate" runat="server">  </asp:TextBox></td>
                                                                    <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ETF_FROMDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks<br />
                                            <asp:TextBox ID="txtRemarks" runat="server"  ></asp:TextBox></td>
                                                                    <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("ETF_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EAT_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMP_ID" runat="server" Text='<%# Bind("ETF_EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Approval" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkApproval" runat="server" Checked='<%# Bind("ETF_bApproved_FromBSU") %>' />
                                            <asp:RadioButton ID="rbtApproval" runat="server"
                                                OnClick="javascript:Selrdbtn(this.id)" Visible="false" AutoPostBack="True"
                                                OnCheckedChanged="rbtApproval_CheckedChanged" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblETFID" runat="server" Text='<%# Bind("ETF_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button"
                                Font-Bold="True"
                                Text="Approve" Visible="False" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
