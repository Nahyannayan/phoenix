Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Web.Configuration

Partial Class Payroll_EMPBookStaffTransfer
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    '-------------------------------------------------------------------------
    ''Purpose:To show details of selected employee for transferring
    'Version      Date          Done by         Purpose
    ' 1.1       15-Dec-2010     Swapna          To add approval procedure by transferring unit
    ' 1.2       16-Jan-2011     Swapna          Changed menu options in DB as follows:
    '                                           "H000074" instead of "P013190"
    '                                           "H000075" instead of "P050040"
    '                                           "H000077"
    '1.3        27-Mar-2011     Swapna          To change validation for super user when binding Business units
    '1.4        23-Feb-2012     Swapna          To add mail sending program
    '1.5        9-Apr-2014      Hashmi          Sending mail customized from screen

    '-------------------------------------------------------------------------

    Private Function FillEmployeeDatas(ByVal EMP_ID As String, ByVal BSU_ID As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "select * from dbo.vw_OSO_EMPLOYEEMASTER WHERE EMP_ID ='" & _
            EMP_ID & "' AND EMP_BSU_ID = '" & BSU_ID & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While (dr.Read())
                txtEmpNo.Text = dr("EMPNAME").ToString
                txtML.Text = dr("EMPVISA_DESCR").ToString
                txtME.Text = dr("EMPMOE_DESCR").ToString
                txtGrade.Text = dr("EMP_GRADE_DESCR").ToString
                'txtReport.Text = dr("EMPMOE_DESCR").ToString     V1.1
                txtReport.Text = dr("EMP_REP_TO_Name").ToString  'V1.1
                txtVacationPolicy.Text = dr("BLS_DESCRIPTION").ToString
                'txtSD.Text = dr("EMPMOE_DESCR").ToString         V1.1
                txtSD.Text = dr("EMP_DES_DESCR").ToString        'V1.1
                txtCat.Text = dr("CATEGORY_DESC").ToString
                'txtDept.Text = dr("EMPMOE_DESCR").ToString       V1.1
                txtEmpApprovalPol.Text = dr("APPROVAL_POLICY").ToString
                txtDept.Text = dr("EMP_DEPT_DESCR").ToString
                txtTGrade.Text = dr("TEACH_GRADE_DESCR").ToString
                hfEmployeeNum.Value = dr("EMPNO").ToString 'V1.4
                lblEmpno.Text = dr("EMPNO").ToString 'V1.4
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Function

    Private Function FillOtherDetails(ByVal EMP_ID As String, ByVal BSU_ID As String) As Boolean
        Try
            h_Emp_No.Value = EMP_ID
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "select * from dbo.EMPTRANSFER WHERE ETF_EMP_ID ='" & _
            EMP_ID & "' AND ETF_FROM_BSU_ID = '" & BSU_ID & "' AND ETF_TO_bAccept <> 1  AND ETF_ID= '" & ViewState("ETF_ID") & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If dr.HasRows Then
                While (dr.Read())
                    txtDate.Text = Format(dr("ETF_FROMDT"), OASISConstants.DateFormat)
                    txtgratuity.Text = AccountFunctions.Round(dr("ETF_GRATUITY"))
                    txtLeaveSalary.Text = AccountFunctions.Round(dr("ETF_LEAVESALARY"))
                    txtAirFare.Text = AccountFunctions.Round(dr("ETF_AIRFARE"))
                    txtRemarks.Text = dr("ETF_REMARKS").ToString
                    h_Trans_ID.Value = dr("ETF_ID").ToString
                    ViewState("IsApproved") = dr("ETF_bApproved_FromBSU")
                    If ViewState("datamode") = "view" Then
                        'ddlBSUnit.SelectedIndex = -1
                        ddlBSUnit.SelectedValue = dr("ETF_TO_BSU_ID")
                    End If
                    If dr("ETF_bApproved_FromBSU") = 1 Then
                        btnDelete.Visible = False
                    Else
                        btnDelete.Visible = True
                    End If
                    'chkConcession.Checked = dr("").ToString
                End While
            Else
                txtDate.Text = ""
                txtgratuity.Text = "0.00"
                txtLeaveSalary.Text = "0.00"
                txtAirFare.Text = "0.00"
                txtRemarks.Text = ""
                h_Trans_ID.Value = ""
                'ddlBSUnit.SelectedIndex = -1                       Commented for V1.1 to prevent BSU change on changing employee selection.
                ' ddlBSUnit.SelectedValue = dr("ETF_TO_BSU_ID")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Function

    Protected Sub SendEmailToNextApprover() 'V1.4
        Dim Mailstatus As String = ""
        Try

            Dim ToEmailId As String = ""
            'If ds.Tables(0).Rows.Count > 0 Then
            '    ToEmailId = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            'End If
            Dim pParms(0) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = Session("sBsuid")
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[getTransferMailIDs]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
            End Using

            If ToEmailId = "" Then
                ToEmailId = "robbie@gemseducation.com"
            End If
            Dim fromemailid = "system@gemseducation.com"
            Dim sb As New StringBuilder
            Dim MainText As String = ""
            Dim Subject As String = ""
            'If ds.Tables(1).Rows.Count > 0 Then

            Subject = "Staff Transfer"

            MainText = "Kindly note that following employee is being transferred from your unit."
            ' End If

            sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
            sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Hi,</td></tr>")
            sb.Append("<tr><td ><br /><br /></td></tr>")
            sb.Append("<tr><td>" & MainText & "</td></tr>")
            sb.Append("<tr><td>Employee No. : " & hfEmployeeNum.Value & "</td></tr>")
            sb.Append("<tr><td>Employee Name : " & txtEmpNo.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Transferred To : " & ddlBSUnit.SelectedItem.Text & "</td></tr>")
            sb.Append("<tr><td>Designation : " & txtSD.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Transfer Date: " & txtDate.Text.ToString & "</td></tr>")
            sb.Append("<tr><td>Department : " & txtDept.Text.ToString & "</td></tr>")
            sb.Append("<tr><td ><br /><br /></td></tr>")


            sb.Append("<tr><td ><br /><br />Regards,</td></tr>")
            sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
            sb.Append("<tr></tr><tr></tr>")
            sb.Append("</table>")

            Dim ds2 As New DataSet
            ds2 = GetCommunicationSettings()

            Dim username = ""
            Dim password = ""
            Dim port = 0
            Dim host = ""


            If ds2.Tables(0).Rows.Count > 0 Then
                username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            End If
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    For Each drow As DataRow In ds.Tables(0).Rows
                        ToEmailId = drow.Item("MailID")
                        Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb.ToString, username, password, host, port, 0, False)
                    Next
                End If
            End If


        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
            'UtilityObj.Errorlog(Mailstatus)

        End Try
    End Sub
    Protected Sub SendEmailToNextApprover_Alternate() 'V1.5
        Dim Mailstatus As String = ""
        Try
            Dim dsApprovers As DataSet
            Dim ToEmailId As String = ""
            dsApprovers = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, "exec GetTransferEmailIdsNew '" & Session("sBsuId") & "',0")
            If Not dsApprovers Is Nothing Then
                For Each rowApprover As DataRow In dsApprovers.Tables(0).Rows
                    'Dim pParms(8) As SqlClient.SqlParameter
                    'pParms(0) = New SqlClient.SqlParameter("@TO_EMP_ID", SqlDbType.VarChar)
                    'pParms(0).Value = rowApprover.Item("emd_email").ToString
                    'pParms(1) = New SqlClient.SqlParameter("@TRANS_EMP_NO", SqlDbType.VarChar)
                    'pParms(1).Value = hfEmployeeNum.Value
                    'pParms(2) = New SqlClient.SqlParameter("@TRANS_EMP_NAME", SqlDbType.VarChar)
                    'pParms(2).Value = txtEmpNo.Text.ToString
                    'pParms(3) = New SqlClient.SqlParameter("@TRANS_BSU_NAME", SqlDbType.VarChar)
                    'pParms(3).Value = ddlBSUnit.SelectedItem.Text
                    'pParms(4) = New SqlClient.SqlParameter("@DESIGNATION", SqlDbType.VarChar)
                    'pParms(4).Value = txtSD.Text.ToString
                    'pParms(5) = New SqlClient.SqlParameter("@TRANSFER_DATE", SqlDbType.VarChar)
                    'pParms(5).Value = txtDate.Text.ToString
                    'pParms(6) = New SqlClient.SqlParameter("@DEPARTMENT", SqlDbType.VarChar)
                    'pParms(6).Value = txtDept.Text.ToString
                    'pParms(7) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar)
                    'pParms(7).Value = "F"
                    'pParms(8) = New SqlClient.SqlParameter("@ETF_ID", SqlDbType.Int)
                    'pParms(8).Value = 0
                    '  SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, "SEND_EMPLOYEE_TRANSFER_EMAIL", pParms)
                    Dim ret As Integer = 0
                    Dim sqlQuery As String = " exec SEND_EMPLOYEE_TRANSFER_EMAIL '" & rowApprover.Item("emd_email") & "','" & hfEmployeeNum.Value & _
                    "','" & txtEmpNo.Text.ToString & "','" & ddlBSUnit.SelectedItem.Text & "','" & txtSD.Text.ToString & "','" & txtDate.Text.ToString & "','" & _
                     txtDept.Text.ToString & "','F'," & ViewState("ETF_ID")
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, CommandType.Text, sqlQuery)
                   
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'UtilityObj.Errorlog(Mailstatus)
        End Try
    End Sub
    Public Function GetCommunicationSettings() As DataSet 'V1.4


        Dim ds As DataSet

        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)

        Return ds

    End Function
    Sub bind_BusinessUnit()

        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else

            Try
                'if user is super admin give access to all the Business Unit
                ' If tblbUSuper = True Then    'V1.3
                Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                    'create a list item to bind records from reader to dropdownlist ddlBunit
                    Dim di As ListItem
                    ddlBSUnit.Items.Clear()
                    'check if it return rows or not
                    If BUnitreaderSuper.HasRows = True Then
                        While BUnitreaderSuper.Read
                            If BUnitreaderSuper(0) <> Session("sBSUID") Then
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBSUnit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBSUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBSUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBSUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End If
                        End While
                    End If
                    ddlBSUnit.Items.Insert(0, "-- Select--")
                End Using
                If ddlBSUnit.SelectedIndex = -1 Then
                    ddlBSUnit.SelectedIndex = 0
                End If
                'Else
                'if user is not super admin get access to the selected  Business Unit
                '----------- Code commented for V1.3--------------
                'Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                '    Dim di As ListItem
                '    ddlBSUnit.Items.Clear()
                '    If BUnitreader.HasRows = True Then
                '        While BUnitreader.Read
                '            If BUnitreader(0) <> Session("sBSUID") Then
                '                di = New ListItem(BUnitreader(2), BUnitreader(0))
                '                ddlBSUnit.Items.Add(di)
                '                For ItemTypeCounter = 0 To ddlBSUnit.Items.Count - 1
                '                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                '                    If ddlBSUnit.Items(ItemTypeCounter).Value = buser_id Then
                '                        ddlBSUnit.SelectedIndex = ItemTypeCounter
                '                    End If
                '                Next
                '            End If
                '        End While
                '    End If
                ' End Using
                'If ddlBSUnit.SelectedIndex = -1 Then
                '    ddlBSUnit.SelectedIndex = 0
                'End If
                'End If                
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If

    End Sub

    Protected Sub imgEmpSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmpSel.Click

        FillEmployeeDatas(h_Emp_No.Value, Session("sBSUID"))
        FillOtherDetails(h_Emp_No.Value, Session("sbsuid"))
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not Page.IsPostBack Then
            ViewState("ETF_ID") = ""
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'collect the url of the file to be redirected in view state
            '"P050115" changed to "H000079" -- V1.2
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000079" And ViewState("MainMnu_code") <> "H000074") Then    'V1.1 ,V1.2
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            bind_BusinessUnit()
            'ts

            If ViewState("datamode") = "view" Then
                ViewState("EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("ETF_ID") = Encr_decrData.Decrypt(Request.QueryString("transID").Replace(" ", "+"))
                FillEmployeeDatas(ViewState("EMP_ID"), Session("sbsuid"))
                FillOtherDetails(ViewState("EMP_ID"), Session("sbsuid"))
                imgEmpSel.Visible = False
            End If
            txtEmpNo.Attributes.Add("ReadOnly", "ReadOnly")
            txtML.Attributes.Add("ReadOnly", "ReadOnly")
            txtME.Attributes.Add("ReadOnly", "ReadOnly")
            txtTGrade.Attributes.Add("ReadOnly", "ReadOnly")
            txtGrade.Attributes.Add("ReadOnly", "ReadOnly")
            txtVacationPolicy.Attributes.Add("ReadOnly", "ReadOnly")
            txtSD.Attributes.Add("ReadOnly", "ReadOnly")
            txtDept.Attributes.Add("ReadOnly", "ReadOnly")
            txtReport.Attributes.Add("ReadOnly", "ReadOnly")
            txtEmpApprovalPol.Attributes.Add("ReadOnly", "ReadOnly")
            If ViewState("datamode") = "add" Then
                imgEmpSel.Enabled = True
                imgEmpSel.Visible = True
                ddlBSUnit.Enabled = True
            Else
                imgEmpSel.Enabled = False
                imgEmpSel.Visible = False

            End If
            If ViewState("MainMnu_code") = "H000074" Then

                btnDelete.Visible = False
                btnAdd.Visible = False
                btnEdit.Visible = False
                ddlBSUnit.Enabled = False
            End If
            If ViewState("IsApproved") = True Then
                btnEdit.Visible = False
                btnDelete.Visible = False
                ddlBSUnit.Enabled = False
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim status As Integer
        Dim flagAudit As Integer
        Dim EAL_EMP_ID As String = h_Emp_No.Value
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim EAL_BSU_ID As String = ddlBSUnit.SelectedValue
                Dim EAL_REMARKS As String = txtRemarks.Text
                Dim EAL_FROMDT As String = txtDate.Text
                Dim bEdit As Boolean
                If ddlBSUnit.SelectedIndex = 0 Then
                    lblError.Text = "Please select Destination Business Unit"
                    Exit Sub
                End If
                If ViewState("datamode") = "edit" Then
                    bEdit = True
                Else
                    bEdit = False
                End If
                If ViewState("ETF_ID") = "" Or ViewState("ETF_ID") Is Nothing Then
                    ViewState("ETF_ID") = "0"
                End If
                status = AccessRoleUser.EMPID_Transfer_Request(EAL_EMP_ID, EAL_BSU_ID, Session("sBsuid"), _
                EAL_REMARKS, EAL_FROMDT, CDbl(Val(txtgratuity.Text)), CDbl(Val(txtLeaveSalary.Text)), _
                CDbl(Val(txtAirFare.Text)), True, False, bEdit, transaction, ViewState("ETF_ID"), Session("sUsr_name"))
                If status <> 0 Then
                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                End If
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), EAL_EMP_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                End If
                transaction.Commit()
                ' SendEmailToNextApprover()
                SendEmailToNextApprover_Alternate()
                lblError.Text = "Record Updated Successfully"
                ClearDetails()
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, "Employee Transfer")
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record can not be Updated"
                UtilityObj.Errorlog(ex.Message, Page.Title)
            End Try
        End Using
    End Sub

    Private Sub ClearDetails()
        txtME.Text = ""
        txtML.Text = ""
        txtGrade.Text = ""
        txtReport.Text = ""
        txtVacationPolicy.Text = ""
        txtSD.Text = ""
        txtCat.Text = ""
        txtDept.Text = ""
        txtEmpApprovalPol.Text = ""
        txtDept.Text = ""
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        txtDate.Text = ""
        txtgratuity.Text = ""
        txtAirFare.Text = ""
        txtLeaveSalary.Text = ""
        txtRemarks.Text = ""
        txtTGrade.Text = ""
        chkConcession.Checked = False
        ddlBSUnit.SelectedIndex = 0
        lblEmpno.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        imgEmpSel.Enabled = True
        imgEmpSel.Visible = True
        ClearDetails()
        ddlBSUnit.Enabled = True
        ddlBSUnit.SelectedIndex = -1 'V1.1-- Bug fixing
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ClearDetails()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If ViewState("datamode") = "view" Then

            DeleteTransfer()
            'ClearDetails()
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    'To delete transfer record from DB (only when approvals are not done)

    Protected Sub DeleteTransfer()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim ds1 As New DataSet
            Dim returnVal As Integer

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("Emp_DELETE_TRANSFER", objConn, stTrans)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@TransID", h_Trans_ID.Value)
            SqlCmd.Parameters.Add("@RETURN_MSG", SqlDbType.VarChar, 200)
            SqlCmd.Parameters("@RETURN_MSG").Direction = ParameterDirection.ReturnValue

            SqlCmd.ExecuteNonQuery()
            returnVal = SqlCmd.Parameters("@RETURN_MSG").Value

            If returnVal <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage(returnVal)
                stTrans.Rollback()
            Else
                stTrans.Commit()
                lblError.Text = UtilityObj.getErrorMessage("958")
            End If
            objConn.Close()

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try


    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim ret As String = ""
        Try
            If Not hfEmployeeNum.Value Is Nothing Then
                objConn.Open()

                Dim SqlCmd As New SqlCommand("GetTransferBetweenTheMonthMessageFirstStage", objConn)
                SqlCmd.CommandType = CommandType.StoredProcedure

                SqlCmd.Parameters.AddWithValue("@ETF_Emp_ID", h_Emp_No.Value)
                SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@TRFDt", CDate(txtDate.Text))
                SqlCmd.Parameters.Add("@returnMsg", SqlDbType.VarChar, 5000)
                SqlCmd.Parameters("@returnMsg").Direction = ParameterDirection.Output
                lblError.Text = ""
                SqlCmd.ExecuteNonQuery()
                ret = SqlCmd.Parameters("@returnMsg").Value
                If ret <> "" Then
                    'lblError.Text = ret
                    btnSave.Attributes.Add("OnClick", "return window.confirm('" & ret & "');")
                Else
                    btnSave.Attributes.Remove("OnClick")

                End If
            End If

        Catch ex As Exception
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try
    End Sub

    Protected Sub txtEmpNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNo.TextChanged
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim ret As String = ""
        Try
            If Not txtDate.Text <> "" And IsDate(txtDate.Text) = True Then
                objConn.Open()

                Dim SqlCmd As New SqlCommand("GetTransferBetweenTheMonthMessageFirstStage", objConn)
                SqlCmd.CommandType = CommandType.StoredProcedure

                SqlCmd.Parameters.AddWithValue("@ETF_Emp_ID", h_Emp_No.Value)
                SqlCmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@TRFDt", CDate(txtDate.Text))
                SqlCmd.Parameters.Add("@returnMsg", SqlDbType.VarChar, 5000)
                SqlCmd.Parameters("@returnMsg").Direction = ParameterDirection.Output
                lblError.Text = ""
                SqlCmd.ExecuteNonQuery()
                ret = SqlCmd.Parameters("@returnMsg").Value
                If ret <> "" Then
                    'lblError.Text = ret
                    btnSave.Attributes.Add("OnClick", "return window.confirm('" & ret & "');")
                Else
                    btnSave.Attributes.Remove("OnClick")

                End If
            End If

        Catch ex As Exception
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try

    End Sub
    Protected Sub h_Emp_No_ValueChanged(sender As Object, e As EventArgs)
        FillEmployeeDatas(h_Emp_No.Value, Session("sBSUID"))
        FillOtherDetails(h_Emp_No.Value, Session("sbsuid"))
    End Sub
End Class
