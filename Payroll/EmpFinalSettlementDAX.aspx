﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmpFinalSettlementDAX.aspx.vb" Inherits="EmpFinalSettlementDAX" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link href="cssfiles/axstyles.css" rel="stylesheet" />--%>
    <%--   <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">





        function CloseWindow() {
            //if (confirm('Are you sure you want to exit?'))
            //{
            //    //var win = window.open("about:blank", "_self");
            //    //win.close
            //    window.close(); 
            //}    
            ////else
            ////{
            ////    return true;
            ////}

            //if (confirm('Are you sure you want to exit?')) {
            //   window.close();
            //}

            var win = window.open("about:blank", "_self");
            win.close();
        }

        function SetFromDate() {
            <%--document.getElementById('<%=txtInsuranceFromDate.ClientID%>').value = document.getElementById('<%=txtCompensationFromDate.ClientID%>').value;--%>
            document.getElementById('txtInsuranceFromDate').value = document.getElementById('txtCompensationFromDate').value;
            document.getElementById('txtAirFareFromDate').value = document.getElementById('txtCompensationFromDate').value;
            document.getElementById('txtTutionFromDate').value = document.getElementById('txtCompensationFromDate').value;
            document.getElementById('txtLifeInsuranceFromDate').value = document.getElementById('txtCompensationFromDate').value;
            document.getElementById('txtLTIPFromDate').value = document.getElementById('txtCompensationFromDate').value;
            document.getElementById('txtTargetBonusFromDate').value = document.getElementById('txtCompensationFromDate').value;
        }

    </script>

    <style type="text/css">
        .auto-style1 {
            font-family: Segoe UI;
            font-size: 12px;
            width: 15%;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
            <ContentTemplate>
                <div align="center">
                    <table cellpadding="7" cellspacing="0" height="100%" width="100%">
                        <tr id="trMsg" runat="server" visible="false">
                            <td align="left" colspan="2" valign="middle">

                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trMainHeading" runat="server" visible="true" class="title-bg">
                            <td align="left" colspan="2" valign="middle">Sync Final Settlement Details to DAX</td>
                        </tr>
                        <tr id="trPositionName" runat="server" visible="false">
                            <td align="left" class="auto-style1"><span class="field-label">Position Name</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtPosition" runat="server" ReadOnly="True"></asp:TextBox></td>

                        </tr>
                        <tr id="trDesignation" runat="server" visible="false">
                            <td align="left" style="width: 20%;"><span class="field-label">Designation</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtDesignation" runat="server" ReadOnly="True"></asp:TextBox></td>

                        </tr>
                        <tr id="trDepartment" runat="server" visible="false">
                            <td align="left" style="width: 20%;"><span class="field-label">Department</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="True"></asp:TextBox></td>

                        </tr>
                        <tr id="trGrade" runat="server" visible="false">
                            <td align="left" style="width: 20%;"><span class="field-label">Grade</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtGrade" runat="server" ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr id="trGradeLevel" runat="server" visible="false">
                            <td align="left" style="width: 20%;"><span class="field-label">Grade Level</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtGradeLevel" runat="server" ReadOnly="True"></asp:TextBox>
                                <asp:TextBox ID="txtCompAmount" runat="server" CssClass="txtEntryNumeric" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:Panel ID="pHeader" runat="server">
                                    <asp:Label ID="lblText" runat="server" Visible="False" CssClass="field-value" />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:Panel ID="pnlSalary" runat="server" HorizontalAlign="Left" Style="overflow: hidden;">
                                    <table align="left" cellpadding="5" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr runat="server" visible="false">
                                                <td align="left" colspan="6" class="matters_bold">
                                                    <%--<img alt="" src="images/down.png" id="imgSalaryCollapser" />Compensation Details--%>
                                                </td>
                                            </tr>
                                            <tr id="trEarningType" runat="server" visible="false">
                                                <td align="left"><span class="field-label">Earning</span>
                                                </td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddlEarnings" runat="server" CssClass="dropDown">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"><span class="field-label">From Date</span></td>

                                                <td align="left" valign="middle">
                                                    <asp:TextBox ID="txtCompensationFromDate" runat="server" CssClass="txtEntry" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCompensationFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                                    <cc1:CalendarExtender ID="Calendar1" PopupButtonID="imgCompensationFromDate" runat="server" TargetControlID="txtCompensationFromDate"
                                                        Format="dd/MMM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <cc1:TextBoxWatermarkExtender ID="WM1" runat="server" TargetControlID="txtCompensationFromDate" WatermarkText="dd/MMM/yyyy" WatermarkCssClass="txtEntryWatermark" />
                                                </td>

                                            </tr>
                                            <tr id="trRange" runat="server" visible="false">
                                                <td align="left" width="15%"><span class="field-label">Min</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtMinRangeAmount" runat="server" CssClass="txtEntryNumeric"></asp:TextBox>
                                                </td>
                                                <td align="left" width="15%"><span class="field-label">Mid</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtMidRangeAmount" runat="server" CssClass="txtEntryNumeric"></asp:TextBox>
                                                </td>

                                                <%--<td align="left"  >Min</td>
                                        <td align="left"  width="5px">:</td>
                                        <td align="left" >
                                            <asp:TextBox ID="txtMinRangeAmount" runat="server" Width="75px"></asp:TextBox>
                                        </td>--%>

                                                <td align="left" width="15%">Max</td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtMaxRangeAmount" runat="server" CssClass="txtEntryNumeric"></asp:TextBox>
                                                    <asp:Button ID="btnSalAdd" runat="server" CssClass="button" Text="Add" />
                                                </td>

                                            </tr>
                                            <tr>
                                                <td align="left" colspan="6">
                                                    <asp:GridView ID="gvCompensation" runat="server" AutoGenerateColumns="False" EmptyDataText="No details added yet." Width="100%" CssClass="table table-bordered table-row">
                                                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ERN Code">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblern_Code" runat="server" Text='<%# Bind("ESS_ERNCODE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle  />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Earn Description">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblern_descr" runat="server" Text='<%# Bind("ERN_DESCR") %>' text-align="right"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Description">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbledd_descr" runat="server" Text='<%# Bind("EDD_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbless_type" runat="server" Text='<%# Bind("ESS_TYPE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle  HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Gross">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbless_gross" runat="server" Text='<%# Bind("ESS_GROSS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                <ItemStyle  HorizontalAlign="Right" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Earned">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbless_earned" runat="server" Text='<%# Bind("ESS_EARNED") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False" Visible="False">

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkRemoveCompensation" runat="server" CausesValidation="false" CommandName="Delete" Text="Remove"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle CssClass="griditem_tr_hover" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>

                                                <%-- <td align="left" ></td>
                                        <td align="left" ></td>
                                        <td align="left" ></td>
                                        
                                        <td align="left" ></td>
                                        <td align="left" ></td>
                                        <td align="left" ></td>
                                        
                                        <td align="left" ></td>--%>
                                            </tr>

                                            <tr id="tr2" runat="server" visible="true">
                                                <td align="left">
                                                    <span class="field-label">Total Earning</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtGrossSalary" runat="server" CssClass="txtEntryNumeric" ReadOnly="True"></asp:TextBox>
                                                </td>



                                                <%--<td align="left"  >Min</td>
                                        <td align="left"  width="5px">:</td>
                                        <td align="left" >
                                            <asp:TextBox ID="txtMinRangeAmount" runat="server" Width="75px"></asp:TextBox>
                                        </td>--%>
                                            </tr>


                                        </tbody>
                                    </table>
                                </asp:Panel>


                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSyncToDAX" runat="server" CssClass="button" Text="Sync to DAX" />
                                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Reset All" OnClientClick="return confirm('Are you sure you want to reset all compensation, benefits and joining allowances details for this position?');" Visible="False" />
                                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Close" OnClientClick="CloseWindow();" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"></td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hfPosId" runat="server" />
                    <asp:HiddenField ID="hfGradeId" runat="server" />
                    <asp:HiddenField ID="hfGradeLevelId" runat="server" />
                    <asp:HiddenField ID="hfUsrId" runat="server" />
                    <asp:HiddenField ID="hfDAXActionNumber" runat="server" />
                    <asp:HiddenField ID="hfFromDate" runat="server" />
                    <asp:HiddenField ID="hfToDate" runat="server" />
                    <asp:HiddenField ID="hfMenuCode" runat="server" />
                    <asp:HiddenField ID="hfDaxRecId" runat="server" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <%-- <cc1:CollapsiblePanelExtender ID="SalaryCollapser" runat="server" TargetControlID="pnlSalary" CollapsedText="Click to show compensation details" ExpandedText="Click to hide compensation details"
                Collapsed="false" CollapsedSize="25" CollapsedImage="images/down.png" ExpandedImage="images/up.png" ImageControlID="imgSalaryCollapser" />--%>

                    <%-- <cc1:CollapsiblePanelExtender ID="BenefitCollapser" runat="server" TargetControlID="pnlBenefit" CollapsedText="Click to show benefit details" ExpandedText="Click to hide benefit details"
                CollapsedSize="25" Collapsed="true" CollapsedImage="images/down.png" ExpandedImage="images/up.png" ImageControlID="imgBenefitCollapser" />

            <cc1:CollapsiblePanelExtender ID="JoiningAllowanceCollapser" runat="server" TargetControlID="pnlJoiningAllowance" CollapsedText="Click to show joining allowance details" ExpandedText="Click to hide joining allowance details"
                CollapsedSize="25" Collapsed="true" CollapsedImage="images/down.png" ExpandedImage="images/up.png" ImageControlID="imgJoiningAllowanceCollapser" />--%>
                    <asp:HiddenField ID="hfEmpNo" runat="server" />
                    <asp:HiddenField ID="hfEmpId" runat="server" />
                    <asp:HiddenField ID="hfBsuId" runat="server" />
                    <asp:HiddenField ID="hfCompany" runat="server" />
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
