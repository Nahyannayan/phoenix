<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empProcessVacation.aspx.vb" Inherits="Payroll_empProcessVacation" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function showdiv() {
            document.all("testdiv").style.visibility = "visible";
        }

        function hidediv() {
            document.all("testdiv").style.visibility = "hidden";
        }
    </script>
    <script language="javascript" type="text/javascript">

        function change_chk_state(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox' && (document.forms[0].elements[i].name.search(/chkEmployee/) > 0)) {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }

        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                 document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
             }

             if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
             }
             return false;
         }

         function ToggleSearchCategory() {
             if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
             }
             else {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
             }
             return false;
         }



         function HideAll() {

             document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';

             document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
         }

         function SearchHide() {
             document.getElementById('trBSUnit').style.display = 'none';
         }


<%-- 
         function GetCATName() {
0             var sFeatures;
             sFeatures = "dialogWidth: 729px; ";
             sFeatures += "dialogHeight: 445px; ";
             sFeatures += "help: no; ";
             sFeatures += "resizable: no; ";
             sFeatures += "scroll: yes; ";
             sFeatures += "status: no; ";
             sFeatures += "unadorned: no; ";
             var NameandCode;
             var result;
             result = window.showModalDialog("Reports/Aspx/SelIDDESC.aspx?ID=CAT", "", sFeatures)
             if (result != '' && result != undefined) {
                 document.getElementById('<%=h_CATID.ClientID %>').value = result;//NameandCode[0];
                    document.forms[0].submit();
                }
                else {
                    return false;
                }
            }

            function GetEMPNAME() {
                var sFeatures;
                sFeatures = "dialogWidth: 729px; ";
                sFeatures += "dialogHeight: 445px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                var NameandCode;
                var result;
                result = window.showModalDialog("Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }
       function GetLeave() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowLeaveApplications.aspx?id=12", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Leaveid.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtLeaveRemarks.ClientID %>').value = NameandCode[1];
                //
                return true;
            }
            return true;
        }
        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;


            if (mode == 'VPB') {
                result = window.showModalDialog("../common/PopupFromIDHiddenFive.aspx?id=Vacation_Batch&multiselect=false", "", sFeatures);


                if (result == '' || result == undefined) {
                    return false;
                }

                NameandCode = result.split('___');
                document.getElementById("<%=txtBatchname.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hfBatch_ID.ClientID %>").value = NameandCode[0];
            }
        }--%>

    </script>


    
 <script>
     function GetLeave() {

            url = "ShowLeaveApplications.aspx?id=12";
            var oWnd = radopen(url, "pop_leave");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Leaveid.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtLeaveRemarks.ClientID %>').value = NameandCode[1];
        }
        }

     function getPageCode(mode) {

         url = "../common/PopupFromIDHiddenFive.aspx?id=Vacation_Batch&multiselect=false";
            var oWnd = radopen(url, "pop_code");
        }
        function OnClientClose2(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=hfBatch_ID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtBatchname.ClientID %>").value = NameandCode[1];
        }
        }


     function GetCATName() {

         url = "Reports/Aspx/SelIDDESC.aspx?ID=CAT";
            var oWnd = radopen(url, "pop_category");
        }
        function OnClientClose3(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_CATID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtCatName.ClientID%>', '')
        }
        }
     function GetEMPNAME() {

         url = "Reports/Aspx/SelIDDESC.aspx?ID=EMP";
            var oWnd = radopen(url, "pop_employee");
        }
        function OnClientClose4(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_EMPID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEMPNAME.ClientID%>', '');
        }
    }






    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>

      <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_leave" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
                  <Windows>
            <telerik:RadWindow ID="pop_code" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
                        <Windows>
            <telerik:RadWindow ID="pop_category" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
                        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>





    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Vacation Processing"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Ref #<br />
                            (Approved Leave Appl.)</span></td>
                        <td align="left" class="matters" width="30%"
                            nowrap="nowrap">
                            <asp:TextBox ID="txtLeaveRemarks" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgLeavedetails" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetLeave();return false;" />
                            <asp:LinkButton ID="lbClear" runat="server">Clear</asp:LinkButton></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Select Batch</span></td>
                        <td align="left" class="matters" width="30%"
                            nowrap="nowrap">
                            <asp:TextBox ID="txtBatchName" runat="server" OnTextChanged="txtBatchName_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgBatchSearch" runat="server"
                                ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getPageCode('VPB');return false;" />
                            <asp:LinkButton ID="lbClearBatch" runat="server">Clear</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Vacation From</span></td>
                        <td align="left" class="matters" style="text-align: left">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                        <td align="left" class="matters"><span class="field-label">Vacation To</span></td>
                        <td align="left" class="matters" style="text-align: left">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDate"
                                EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr id="trSelcategory" runat="server">
                        <td align="left" class="matters" colspan="4" valign="top">
                            <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../Images/PLUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                            <span class="field-label">Select Category Filter</span></td>
                    </tr>
                    <tr id="trCategory" style="display: none;" runat="server">
                        <td align="left" valign="top" class="matters">
                            <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../Images/MINUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Category</span></td>
                        <td align="left" class="matters" style="text-align: left">(Enter the Category ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtCatName" runat="server" OnTextChanged="txtCatName_TextChanged" CssClass="inputbox"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddCATID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetCATName();return false" /></td>
                        <td colspan="2">
                            <asp:GridView ID="gvCat" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnUnload="gvCat_Unload">
                                <Columns>
                                    <asp:TemplateField HeaderText="CAT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelEMPName" runat="server">
                        <td align="left" class="matters" colspan="4" valign="top">
                            <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../Images/PLUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                          <span class="field-label">  Select Employee Name Filter</span></td>
                    </tr>
                    <tr id="trEMPName" runat="server" style="display: none;">
                        <td align="left" valign="top" class="matters">
                            <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../Images/MINUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" /><span class="field-label">Employee Name</span></td>
                        <td align="left" class="matters" valign="top">
                            <asp:Label ID="Label1" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtEMPNAME" runat="server"  OnTextChanged="txtEMPNAME_TextChanged" 
                                CssClass="inputbox"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNAME();return false" /></td>
                        <td colspan="2">
                            <asp:GridView ID="gvEMPName" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" OnPageIndexChanging="gvEMPName_PageIndexChanging" OnUnload="gvEMPName_Unload">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMPNO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="EMPABCCat" runat="server">
                        <td align="left" class="matters" valign="top"><span class="field-label">Employee Category</span></td>
                        <td align="left" class="matters" colspan="3" valign="top">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" Checked="True" />
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" Checked="True" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" Checked="True" />


                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT [BSU_ID], [BSU_NAME] FROM [BUSINESSUNIT_M]"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr id="tr_GridListAll" runat="server">
                        <td align="center" class="matters" colspan="4">
                            <asp:GridView ID="gvEmpDetails" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Details Found" AllowPaging="True" PageSize="15">
                                <Columns>
                                    <asp:BoundField DataField="EMPNO" HeaderText="EmpNo" SortExpression="EMPNO"></asp:BoundField>
                                    <asp:BoundField DataField="EMP_NAME" HeaderText="Name" SortExpression="EMP_NAME"></asp:BoundField>
                                    <asp:BoundField DataField="ECT_DESCR" HeaderText="Category" SortExpression="ECT_DESCR"></asp:BoundField>
                                    <asp:BoundField DataField="EMP_LASTREJOINDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Last Rejoin Dt" HtmlEncode="False" SortExpression="EMP_LASTREJOINDT"></asp:BoundField>
                                    <asp:BoundField DataField="DAYSINPERIOD" HeaderText="T.Days" SortExpression="DAYSINPERIOD"></asp:BoundField>
                                    <asp:BoundField DataField="LOPDAYS" HeaderText="LOP" SortExpression="LOPDAYS"></asp:BoundField>
                                    <asp:BoundField DataField="ALLOWEDDAYS" HeaderText="Days/Year" SortExpression="ALLOWEDDAYS"></asp:BoundField>
                                    <asp:BoundField DataField="TKTCOUNT" HeaderText="Tkt Count"></asp:BoundField>
                                    <asp:BoundField DataField="TKTAMOUNT" HeaderText="Tkt Amount" SortExpression="TKTAMOUNT"></asp:BoundField>
                                    <asp:BoundField DataField="LEAVE_SALARYDAYS_SCHEDULED" HeaderText="Acr.Days" SortExpression="LEAVE_SALARYDAYS_SCHEDULED"></asp:BoundField>
                                    <asp:BoundField DataField="ELIGIBLE_DAYS" HeaderText="E.Days"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Air Fare">
                                        <ItemTemplate>
                                            <input id="chkAirfare" runat="server" checked="checked" type="checkbox" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leaves Settled">
                                        <ItemTemplate>
                                            <input id="chkLeavesSettled" runat="server" checked="checked" type="checkbox" />

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <input id="chkAll" value='<%# Bind("EMP_ID") %>' onclick="change_chk_state(this);" type="checkbox" runat="server" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkEmployee" value='<%# Bind("EMP_ID") %>' type="checkbox" runat="server" />

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr id="tr_airfare" runat="server" visible="false">
                        <td align="center" class="matters"><span class="field-label">Select Air fare</span>
                        </td>
                        <td align="left" class="matters" colspan="3">
                            <asp:CheckBoxList ID="ChkListAirFare" runat="server" RepeatDirection="Horizontal">
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr id="tr_ViewProcessing" runat="server">
                        <td align="left" class="matters" colspan="4" style="text-align: center">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="dayBook" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Process Vacation" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                        </td>
                    </tr>
                    <tr id="tr_ViewDetailPrint" runat="server" visible="false">
                        <td   class="matters" colspan="4" align="center"
                              nowrap="noWrap">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                                CssClass="table table-row table-bordered" PageSize="1" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="EMPNO" HeaderText="Emp No" />
                                    <asp:BoundField DataField="EMP_NAME" HeaderText="Emp Name" />
                                    <asp:BoundField DataField="ELV_DTTO" HeaderText="To Dt." HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ELV_ACTUALDAYS" HeaderText="A. Days">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ELV_AIRTICKET" HeaderText="Air Ticket" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ELV_TKTCOUNT" HeaderText="Tkt Count">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add New" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnLeaveRpt" runat="server" CssClass="button" Text="Leave Settlement Details" />
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_CATID" runat="server" />
                <asp:HiddenField ID="h_Leaveid" runat="server" />
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfDepartment" runat="server" type="hidden" />
                <input id="hfCategory" runat="server" type="hidden" />
                <input id="hfDesignation" runat="server" type="hidden" />
                <input id="hfEmpName" runat="server" type="hidden" />
                <input id="hfBatch_ID" runat="server" type="hidden" />

                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

