Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
'Version            Date                Author                  Change
'1.1                3-Feb-2013          Swapna              To add salary hold payment outside system
Partial Class Payroll_empSalaryHold
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrUsedChqNos As String
    Private Property IsInitialLoad() As Boolean
        Get
            Return ViewState("IsInitialLoad")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsInitialLoad") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            txtAmount.Attributes.Add("readonly", "readonly")
            'bind_BusinessUnit()
            Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P130175" And ViewState("MainMnu_code") <> "P130177" And ViewState("MainMnu_code") <> "P130179") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("MainMnu_code") = "P130175" Then
                ImageButton1.Visible = True
                lblTitle.Text = "Hold Salary"
            ElseIf ViewState("MainMnu_code") = "P130177" Then
                ImageButton1.Visible = False
                lblTitle.Text = "Release Salary"
            ElseIf ViewState("MainMnu_code") = "P130179" Then 'V1.1
                ImageButton1.Visible = False
                lblTitle.Text = "Salary Hold - Already Paid"
            End If

            gridbind()
            set_Comboanddate()
        End If
        If h_EMPID.Value <> "" Then
            gridbind()
            h_EMPID.Value = ""
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lintRetVal As Integer
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If (lintRetVal = 0) Then
                For Each gvr As GridViewRow In gvDetails.Rows
                    Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkESD_BANK"), HtmlInputCheckBox)
                    Dim txtAmount_D As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
                    Dim txtRemark As TextBox = CType(gvr.FindControl("txtRemark"), TextBox)
                    Dim str_Remarks As String = txtRemark.Text.Trim
                    If str_Remarks = "" Then
                        str_Remarks = txtNarrn.Text
                    End If
                    Dim isUnhold As Boolean = False
                    Dim IsPaidOutside As Boolean = False
                    Dim lblmonth_year As Label = CType(gvr.FindControl("lblmonth_year"), Label)
                    Dim lblESD_EMP_ID As Label = CType(gvr.FindControl("lblESD_EMP_ID"), Label)
                    Dim IsRelease As Boolean
                    If ViewState("MainMnu_code") = "P130175" Then
                        IsRelease = False
                        IsPaidOutside = False
                    Else
                        IsRelease = True

                        IsPaidOutside = False 'V1.1 starts
                        If ViewState("MainMnu_code") = "P130179" Then
                            If cb.Checked = True Then
                                IsPaidOutside = True
                            Else
                                IsRelease = False
                            End If
                        End If
                    End If

                    If ViewState("MainMnu_code") <> "P130179" Then
                        lintRetVal = PayrollFunctions.SaveEMPSALHOLD_D(0, lblESD_EMP_ID.Text, lblmonth_year.Text.Split("_")(0), _
                                             lblmonth_year.Text.Split("_")(1), cb.Value, cb.Checked, str_Remarks, IsRelease, isUnhold, stTrans)
                    Else 'V1.1
                        If cb.Checked Then
                            lintRetVal = PayrollFunctions.SaveEMPSALHOLD_D(0, lblESD_EMP_ID.Text, lblmonth_year.Text.Split("_")(0), _
                                            lblmonth_year.Text.Split("_")(1), cb.Value, cb.Checked, str_Remarks, IsRelease, isUnhold, stTrans, True)
                        End If

                    End If

                    If cb.Checked Then
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblESD_EMP_ID.Text, "Hold Salary", _
                                           Page.User.Identity.Name.ToString, Me.Page, lblmonth_year.Text)
                    Else
                        If isUnhold Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblESD_EMP_ID.Text, "UnHold Salary", _
                                                                      Page.User.Identity.Name.ToString, Me.Page, lblmonth_year.Text)
                        End If
                    End If

                    If lintRetVal <> 0 Then
                        Exit For
                    End If

                Next

                If lintRetVal = 0 Then
                    clear_All()
                    stTrans.Commit()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", "HOLD", _
                    'Page.User.Identity.Name.ToString, Me.Page)
                    lblError.Text = "Data Successfully Saved..."
                Else
                    lblError.Text = getErrorMessage(lintRetVal)
                    stTrans.Rollback()
                End If
            Else
                lblError.Text = getErrorMessage(lintRetVal)
                stTrans.Rollback()
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try


    End Sub

    Protected Sub txtAmount_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Text = AccountFunctions.Round(txtAmount.Text)
        txtAmount.Attributes.Add("readonly", "readonly")
        txtAmount.Attributes.Add("onFocus", "this.select();")
        txtAmount.Attributes.CssStyle.Add("TEXT-ALIGN", "right")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub clear_All()
        h_EMPID.Value = ""
        clMonthYear.DataBind()
        clMonthYear.SelectedIndex = -1
        gvDetails.DataBind()
    End Sub

    Sub set_Comboanddate()
        Try
            Dim str_Sql As String = "SELECT DISTINCT CAST(CAST(ESD_MONTH AS varchar(4)) + '/1' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME) as DT," _
            & " CAST(ESD_MONTH AS varchar(4)) + '_' + " _
            & " CAST(ESD_YEAR AS varchar(4)) AS month_year, " _
            & " LEFT( DATENAME(MONTH, CAST(CAST(ESD_MONTH AS varchar(4)) + " _
            & " '/22' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME)), 3)+" _
            & " '/'+CAST(ESD_YEAR AS varchar(4)) AS monthyear,ESD_DTFROM" _
            & " FROM EMPSALARYDATA_D AS ESD" _
            & " WHERE  (ESD_PAID = 0)" _
            & " AND (ESD_BSU_ID = '" & Session("sBsuid") & "') order by DT"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            clMonthYear.DataSource = ds.Tables(0)
            clMonthYear.DataTextField = "monthyear"
            clMonthYear.DataValueField = "month_year"
            clMonthYear.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbind()
        Try
            Dim IDs As String() = h_EMPID.Value.Split("||")
            Dim condition As String = String.Empty
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            Dim str_Sql As String = "SELECT DISTINCT  CAST(ESD_MONTH AS varchar(2)) + '_' + "
            str_Sql = str_Sql & " CAST(ESD_YEAR AS varchar(4)) AS month_year, "
            str_Sql = str_Sql & " LEFT( DATENAME(MONTH, CAST(CAST(ESD_MONTH AS varchar(4)) +  '/22' + '/' +"
            str_Sql = str_Sql & " CAST(ESD_YEAR AS varchar(4)) AS DATETIME)), 3)+  '/'+"
            str_Sql = str_Sql & " CAST(ESD_YEAR AS varchar(4)) AS monthyear , ESD.ESD_ID, "
            str_Sql = str_Sql & " ESD.ESD_EARN_NET AS AMOUNT,  ESD.ESD_EMP_ID, EM.EMPNO,"
            str_Sql = str_Sql & " ISNULL( EM.EMP_FNAME, '')+' '+  ISNULL(EM.EMP_MNAME,'')+' '+ "
            str_Sql = str_Sql & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME,ESD_DTFROM,EHD_RLSDATE,EHD_REMARKS,"
            If ViewState("MainMnu_code") = "P130175" Then
                str_Sql = str_Sql & " isnull(EHD_bHold,0) as hold ,"
                str_Sql = str_Sql & " case when EHD_bHold is null then 0 else 1 end as Locked "
            Else
                str_Sql = str_Sql & " case when isnull(EHD_bHold,0) = 0 then 1 else 0 end as hold ,  "
                str_Sql = str_Sql & " case when isnull(EHD_bHold,0)=1 then 0 else 1 end as Locked "
            End If
            str_Sql = str_Sql & " FROM EMPLOYEE_M AS EM "
            str_Sql = str_Sql & " INNER JOIN EMPSALARYDATA_D AS ESD   ON EM.EMP_ID = ESD.ESD_EMP_ID"
            str_Sql = str_Sql & " LEFT OUTER JOIN EMPSALHOLD_D ON EMP_ID=EHD_EMP_ID"
            str_Sql = str_Sql & " and ESD_ID=EHD_ESD_ID "
            str_Sql = str_Sql & " WHERE   (ESD.ESD_PAID = 0) and isnull(esd_erd_id,0)<>-1  "
            If ViewState("MainMnu_code") = "P130175" Then
                str_Sql = str_Sql & " AND (ESD.ESD_EMP_ID IN (" & condition & ") or EHD_bHold is not null )" 'isnull(EHD_bHold,0)=1)  "
                'Else
                '    str_Sql = str_Sql & " AND (ESD.ESD_EMP_ID IN (" & condition & ") or EHD_bHold is not null  )"
                'V1.1
            ElseIf ViewState("MainMnu_code") = "P130177" Then
                str_Sql = str_Sql & " AND (ESD.ESD_EMP_ID IN (" & condition & ") or EHD_bHold is not null  )"
            Else
                str_Sql = str_Sql & " AND (ESD.ESD_EMP_ID IN (" & condition & ") or " & condition & "='') and isnull(EHD_bHold,0)=1 "
            End If

            str_Sql = str_Sql & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "') AND (ESD.ESD_YEAR IN (" & get_Selected_YearMonths(False) & ")) "
            str_Sql = str_Sql & " AND (ESD.ESD_MONTH IN (" & get_Selected_YearMonths(True) & "))  order by hold desc"

            Session("EMP_SEL_COND") = "AND EMP_ID IN ( SELECT DISTINCT ESD.ESD_EMP_ID " _
            & " FROM EMPLOYEE_M AS EM INNER JOIN EMPSALARYDATA_D AS ESD" _
            & " ON EM.EMP_ID = ESD.ESD_EMP_ID AND ESD.ESD_PAID = 0" _
            & " WHERE (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "')" _
            & " AND (ESD.ESD_YEAR IN (" & Session("yearselected") & ")) AND (ESD.ESD_MONTH IN (" & Session("monthselected") & ")) )" '_
            '& " AND(EMP_bACTIVE = 1) ) " -- commented by swapna for displaying resigned employees.
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            gvDetails.DataSource = ds.Tables(0)
            gvDetails.DataBind()
            DisableGridRow()
        Catch ex As Exception
            Errorlog(ex.Message)
            gvDetails.DataBind()
            DisableGridRow()
        End Try
    End Sub

    Protected Sub clMonthYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clMonthYear.SelectedIndexChanged
        gridbind()
    End Sub

    Private Function get_Selected_YearMonths(ByVal isMonth As Boolean) As String
        Dim str_code As String = ""
        Dim str_narrn As String = ""
        Dim i_yearMonth As Integer = 1 'year
        If isMonth = True Then
            i_yearMonth = 0
        End If
        For Each item As ListItem In clMonthYear.Items
            If (item.Selected) Then
                If str_code <> "" Then
                    str_code = str_code & "," & item.Value.Split("_")(i_yearMonth)
                    str_narrn = str_narrn & "," & item.Text
                Else
                    str_code = item.Value.Split("_")(i_yearMonth)
                    str_narrn = item.Text
                End If
            End If
        Next

        If isMonth = True Then
            If str_code = "" Then
                Session("monthselected") = "1900"
                str_code = "1900"
            Else
                Session("monthselected") = str_code
            End If
        Else
            If str_code = "" Then
                Session("yearselected") = "20"
                str_code = "20"
            Else
                Session("yearselected") = str_code
            End If
            Session("yearselected") = str_code
        End If
        txtNarrn.Text = "Salary Hold For " & str_narrn
        get_Selected_YearMonths = str_code

    End Function
    Private Sub DisableGridRow()
        Try
            Dim gvRow As GridViewRow
            Dim chkLocked As CheckBox
            Dim chkESD_BANK As HtmlInputCheckBox
            Dim txtRemark As TextBox
            For Each gvRow In gvDetails.Rows
                chkLocked = gvRow.FindControl("chkLocked")
                chkESD_BANK = gvRow.FindControl("chkESD_BANK")
                txtRemark = gvRow.FindControl("txtRemark")
                If chkLocked Is Nothing Or chkESD_BANK Is Nothing Or txtRemark Is Nothing Then Exit Sub
                If chkLocked.Checked = True Then
                    chkESD_BANK.Disabled = True
                    txtRemark.Enabled = False
                Else
                    chkESD_BANK.Disabled = False
                    txtRemark.Enabled = True
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

End Class
