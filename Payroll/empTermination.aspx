<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="empTermination.aspx.vb" Inherits="Payroll_empTermination" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
     
       function GetEMPName() {
          
           var NameandCode;
           var result;
           result = radopen("../Accounts/accShowEmpDetail.aspx?id=RS_EM&EMPNO=1", "pop_up")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                return true;
            }
            return false;--%>
        }
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=h_Emp_No.ClientID%>").value = NameandCode[1];
                document.getElementById("<%=txtEmpNo.ClientID%>").value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
     </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" EnableViewState="False" CssClass="BlueTable"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table width="100%" align="center">
                    <tr>
                        <td colspan="3" align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    </tr>
             <tr>
                 <td align="left" width="20%">
                     <span class="field-label">Select Employee</span></td>
                 <td align="left" width="30%">
                     <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                         ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;" />
                 </td>
                 <td width="20%"><span class="field-label">Employee Name</span></td>
                 <td width="30%"><asp:Label ID="lbl_Empname" runat="server" CssClass="field-value"></asp:Label>
                            <%--<asp:LinkButton ID="lnkLog" runat="server" Style="display: none;">Transaction Log</asp:LinkButton>
                            <ajaxToolkit:PopupControlExtender
                                ID="PopupControlExtender1" runat="server"
                                DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                Position="Bottom" TargetControlID="lnkLog">
                            </ajaxToolkit:PopupControlExtender>--%></td>
             </tr>
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Designation</span></td>
                        <td align="left">
                            <asp:Label ID="lbldpt" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left">
                            <span class="field-label">Department</span></td>
                        <td align="left">
                            <asp:Label ID="lbldesg" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Visa Business Unit</span></td>
                        <td align="left">
                            <asp:Label ID="lblVisaBSU" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left">
                            <span class="field-label">Working Business Unit</span></td>
                        <td align="left">
                            <asp:Label ID="lblWorkingBSU" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Date Of Joining the Unit</span></td>
                        <td align="left">
                            <asp:Label ID="lblDOJBSU" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left">
                            <span class="field-label">Date Of Joining the Group</span></td>
                        <td align="left">
                            <asp:Label ID="lblDOJGroup" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Resignattion Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="DropDownListResignTerm" runat="server" Enabled="False">
                                <asp:ListItem Value="4">Resignation</asp:ListItem>
                                <asp:ListItem Value="5">Termination</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Last Working Date</span></td>
                        <td align="left">
                            <asp:Label ID="lblLastWorkDate" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Resignation Date</span></td>
                        <td align="left">
                            <asp:Label ID="lblResgDate" runat="server" CssClass="field-value"></asp:Label></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <table id="TBLSettlementDetails" runat="server" width="100%" align="center">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Remarks For Accounts</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtRemarks_ACC" runat="server" TextMode="MultiLine"
                                EnableTheming="False" AutoPostBack="True"></asp:TextBox></td>
                       <td width="20%"></td>
                        <td width="30%"></td>

                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Last date for the purpose of Gratuity</span></td>
                        <td align="left">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                       
                        <td align="center" colspan="4">
                            <asp:Button ID="btnViewArrears" runat="server" CssClass="button" Text="View Arrears" CausesValidation="False" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" class="title-bg">Arrears &amp; Gratuity&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Arrears &amp; Gratuity</span></td>
                        <td align="left" valign="top" colspan="3">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
                                <tr>
                                    <td align="center" width="50%" style="vertical-align:top !important;">
                                        <asp:GridView ID="GridViewArrears" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="Arrear Data">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <Columns>
                                                <asp:BoundField DataField="MONTHYEAR" HeaderText="Month" ReadOnly="True" SortExpression="MONTHYEAR">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMOUNT" HeaderText="Amount" ReadOnly="True" SortExpression="AMOUNT" DataFormatString="{0:n0}">
                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                        &nbsp;</td>
                                    <td align="center" valign="top" width="50%" style="vertical-align:top !important;">
                                        <asp:FormView ID="FormViewGratuity" runat="server" Width="100%" EmptyDataText="Gratuity Data">
                                            <EmptyDataRowStyle CssClass="gridheader_new" />
                                            <ItemTemplate>
                                                <table id="tbl_Gratuity" width="100%" align="center" class="table table-bordered table-row">
                                                    <tbody>
                                                        <tr>
                                                            <td class="gridheader_pop" colspan="2"><span class="field-label">Gratuity Details</span></td>
                                                        </tr>
                                                        <tr></tr>
                                                        <tr>
                                                            <td align="left">Days Worked</td>
                                                            <td align="left">
                                                                <asp:Label ID="DAYSWORKEDLabel" runat="server" Text='<%# Bind("DAYSWORKED","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Days Eligible</td>
                                                            <td align="left">
                                                                <asp:Label ID="DAY_SCHEDULEDLabel" runat="server" Text='<%# Bind("DAY_SCHEDULED","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Accrued Amount</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblACCRUED_AMOUNT" runat="server" Text='<%# Bind("ACCRUED_AMOUNT","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">This Month Amount</td>
                                                            <td align="left">
                                                                <asp:Label ID="MONTH_AMOUNTLabel" runat="server" Text='<%# Bind("MONTH_AMOUNT","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">&lt;5 Year Days</td>
                                                            <td align="left">
                                                                <asp:Label ID="BEFORE5YEARLabel" runat="server" Text='<%# Bind("BEFORE5YEAR","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">&gt; 5 Year Days</td>
                                                            <td align="left">
                                                                <asp:Label ID="AFTER5YEARLabel" runat="server" Text='<%# Bind("AFTER5YEAR","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Current Month Days:</td>
                                                            <td align="left">
                                                                <asp:Label ID="CurMonthDaysLabel" runat="server" Text='<%# Bind("CurMonthDays","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <br />
                                        <asp:Button ID="btnRefreshGratuity" runat="server" CssClass="button" Text="Refresh"
                                            CausesValidation="False" />
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <span class="field-label">Vacation</span></td>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="center" valign="top" width="50%" style="vertical-align:top !important;">
                                        <asp:FormView ID="FormViewVacation" runat="server" Width="100%" EmptyDataText="Vacation Data">
                                            <EmptyDataRowStyle CssClass="gridheader_new" />
                                            <ItemTemplate>
                                                <table width="100%" align="center" class="table table-bordered table-row">
                                                    <tbody>
                                                        <tr>
                                                            <td class="gridheader_pop" colspan="2"><span class="field-label">Vacation Salary</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Last Rejoin Dt</td>
                                                            <td align="left">
                                                                <asp:Label ID="EMP_LASTREJOINDTLabel" runat="server" Text='<%# Bind("EMP_LASTREJOINDT","{0:dd/MMM/yyyy}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Days in Period</td>
                                                            <td align="left">
                                                                <asp:Label ID="DAYSINPERIODLabel" runat="server" Text='<%# Bind("DAYSINPERIOD","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">LOP Days</td>
                                                            <td align="left">
                                                                <asp:Label ID="LOPDAYSLabel" runat="server" Text='<%# Bind("LOPDAYS","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Allowed Days</td>
                                                            <td align="left">
                                                                <asp:Label ID="ALLOWEDDAYSLabel" runat="server" Text='<%# Bind("ALLOWEDDAYS","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Leave Taken</td>
                                                            <td align="left">
                                                                <asp:Label ID="LEAVE_TAKENLabel" runat="server" Text='<%# Bind("LEAVE_TAKEN","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Carry Forward</td>
                                                            <td align="left">
                                                                <asp:Label ID="CARRYFORWARDLabel" runat="server" Text='<%# Bind("CARRYFORWARD","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Salary</td>
                                                            <td align="left">
                                                                <asp:Label ID="SALARYLabel" runat="server" Text='<%# Bind("SALARY","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Leave Salary</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLEAVE_SALARY" runat="server" Text='<%# Bind("LEAVE_SALARY","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Eligible Days</td>
                                                            <td align="left">
                                                                <asp:Label ID="LEAVE_SALARYDAYS_SCHEDULEDLabel" runat="server" Text='<%# Bind("LEAVE_SALARYDAYS_SCHEDULED","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 17px" align="left">Category</td>
                                                            <td style="height: 17px" align="left">
                                                                <asp:Label ID="ECT_DESCRLabel" runat="server" Text='<%# Bind("ECT_DESCR") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Ticket Count:</td>
                                                            <td align="left">
                                                                <asp:Label ID="TKTCOUNTLabel" runat="server" Text='<%# Bind("TKTCOUNT","{0:n0}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">Ticket Amount</td>
                                                            <td align="left">
                                                                <asp:Label ID="TKTAMOUNTLabel" runat="server" Text='<%# Bind("TKTAMOUNT","{0:n2}") %>'></asp:Label>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </ItemTemplate>
                                        </asp:FormView>
                                    </td>
                                    <td style="vertical-align:top !important;">
                                        <asp:GridView ID="gvEmpSalary" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <Columns>
                                                <asp:BoundField DataField="ERN_DESCR" HeaderText="Earning">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ESH_AMOUNT" DataFormatString="{0:n2}" HeaderText="Amount" HtmlEncode="False">
                                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                      
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <span class="field-label">Assets Issued</span></td>
                        <td align="center" colspan="3"  style="vertical-align:top !important;">
                            <asp:GridView ID="gvAssetAllocated" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="Assets Issued"
                                Width="100%">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:BoundField DataField="EAD_ISSUE_DT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Issue Date"
                                        HtmlEncode="False" />
                                    <asp:BoundField DataField="ASSETNAME" HeaderText="Fixed Asset" ReadOnly="True" />
                                    <asp:BoundField DataField="EAD_VALUE" HeaderText="Asset Value" />
                                    <asp:BoundField DataField="EAD_STATUS" HeaderText="Status" />
                                    <asp:BoundField DataField="EAD_REMARKS" HeaderText="Remarks" />
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            &nbsp;
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Additional Payments/Deductions</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddSalEarnCode" runat="server" Width="70%">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddSalDedCode" runat="server" Width="70%">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Amount </span>
                            </td>
                          <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                CssClass="error" Display="Dynamic" ErrorMessage="Amount entry required" ForeColor=""
                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvAmount" runat="server" ControlToValidate="txtAmount"
                                CssClass="error" Display="Dynamic" ErrorMessage="Enter valid amount" ForeColor=""
                                Operator="DataTypeCheck" Type="Double" ValidationGroup="groupM1">*</asp:CompareValidator></td>
                       

                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Remark</span></td>
                        <td>
                            <asp:TextBox ID="txtRemarkdeduction" runat="server"
                                TextMode="MultiLine" MaxLength="195"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ControlToValidate="txtRemarkdeduction"
                                CssClass="error" Display="Dynamic" ErrorMessage="Remark can not be left empty"
                                ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkDeduction" runat="server" Checked="True" Text="Deduction" AutoPostBack="True" />
                            <asp:Button ID="btnFill" runat="server" CssClass="button" Text="Add" ValidationGroup="groupM1" />
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" ValidationGroup="groupM1" />
                            <asp:Button ID="btnChildCancel" runat="server" CssClass="button" Text="Cancel" ValidationGroup="groupM1" /></td>
                      
                    </tr>
                    <tr>
                        <td align="left" rowspan="2"><span class="field-label">Deductions/Earnings</span></td>
                        <td align="center" colspan="3">
                            <asp:GridView ID="GridViewDeductions" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="Deduction Data"
                                Width="100%">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DeductionDESCR" HeaderText="Deduction Type" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:CheckBoxField DataField="bDeduction" HeaderText="Deduction">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CheckBoxField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:CommandField ShowEditButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:CommandField ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <span class="field-label">Earnings&nbsp; (Total) :</span>
                            <asp:TextBox ID="txtEarning" runat="server" Width="89px" Style="width:89px;"></asp:TextBox>
                                    </td>
                                    <td>
                                        <span class="field-label">Deductions (Total) :</span>
                            <asp:TextBox ID="txtDeductions" runat="server" Width="89px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <span class="field-label">Net Pay :</span>
                            <asp:TextBox ID="txtTotal" runat="server" Width="89px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine" Width="472px"></asp:TextBox></td>
                    </tr>

                </table>
                <table id="TBLVISACANCEL" visible="false" runat="server" width="100%" align="center">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Visa Cancelled ?</span></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbVisaYes" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="VisaPOST" Text="Yes" Checked="True" />
                            <asp:RadioButton ID="rbVisaNo" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="VisaPOST" Text="No" />
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Visa Cancellation Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtVisaCancelDt" runat="server"></asp:TextBox>

                            <asp:ImageButton ID="imgVisaCancelDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <table id="TBLLCCANCEL" visible="false" runat="server" width="100%" align="center">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Labour Card Cancelled ?</span></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbLCYes" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="LCPOST" Text="Yes" Checked="True" />
                            <asp:RadioButton ID="rbLCNo" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="LCPOST" Text="No" />
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Labour Card Cancellation Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLCCancelDt" runat="server"></asp:TextBox>

                            <asp:ImageButton ID="imgLCCancelDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" />
                        </td>
                         <td></td>
                        <td></td>
                    </tr>
                </table>
                <table id="TBLEMPEXIT" visible="false" runat="server" border="1" cellpadding="5" bordercolor="#1b80b6" cellspacing="0" width="98%" align="center" style="border-collapse: collapse">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Employee Exited ?</span></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbExitYes" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="EXTPOST" Text="Yes" Checked="True" />
                            <asp:RadioButton ID="rbExitNo" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="EXTPOST" Text="No" />
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Employee Exited Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtExitDt" runat="server"></asp:TextBox>

                            <asp:ImageButton ID="imgExitDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" Visible="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit"
                                CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Process" />
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve"
                                EnableViewState="False" Visible="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="txtVisaCancelDt_CalendarExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgVisaCancelDt" TargetControlID="txtVisaCancelDt">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgLCCancelDt" TargetControlID="txtLCCancelDt">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgExitDt" TargetControlID="txtExitDt">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_Emp_No" runat="server" />
                <asp:Panel ID="Panel1" CssClass="panel-cover" runat="server">
                </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>

