<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empSalaryFreezeApproval.aspx.vb" Inherits="Payroll_empSalaryFreezeApproval"
    Title="Asset Location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtBSUName" runat="server" TabIndex="1" MaxLength="100"
                                            CssClass="inputbox" Enabled="False">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Transaction&nbsp; Date</span>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtTrDate" runat="server" AutoPostBack="True">
                                        </asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFrom" TargetControlID="txtTrDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgtrdate" TargetControlID="txtTrDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgTrDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                        &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvTrDate" ControlToValidate="txtTrDate"
                                            ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Month</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlPayMonth" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Year</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlPayYear" runat="server">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnLoadData" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Load" TabIndex="5" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Remarks</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                            EnableTheming="False">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Entered By</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtLoggedInuser" runat="server" CssClass="inputbox"
                                            Enabled="False">
                                        </asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Total Amount</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtAmount" runat="server" CssClass="inputbox" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trApproverRemarks" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Approver Remarks</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtApproverRemarks" runat="server" TextMode="MultiLine"
                                            EnableTheming="False"></asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trApprovedBy" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Approved By</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtApprovedBy" runat="server" CssClass="inputbox"
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="right" class="matters" colspan="4">
                                        <asp:GridView ID="gvSalarySummary" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CaptionAlign="Top" PageSize="15" CssClass="table table-row table-bordered">
                                            <Columns>
                                                <asp:TemplateField HeaderText="CategoryID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblECTID" runat="server" Text='<%# Bind("ESD_ECT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Month" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMonth" runat="server" Text='<%# Bind("ESD_MONTH") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblYear" runat="server" Text='<%# Bind("ESD_YEAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ECT_DESCR" HeaderText="Category Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TOTALCOUNT" HeaderText="Count">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Amount" DataFormatString="{0:n2}" HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PAIDCASH" DataFormatString="{0:n2}"
                                                    HeaderText="Cash">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PAIDBANK" DataFormatString="{0:n2}"
                                                    HeaderText="Bank">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkView" runat="server" Text="View Details" OnClick="lnkViewDetails_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:Button ID="btnRecon" runat="server" CausesValidation="False" CssClass="button"
                                            Text="View Reconcilation" TabIndex="9" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" TabIndex="5" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" TabIndex="6" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                        <asp:Button ID="btnSend" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save & Send" UseSubmitBehavior="False" TabIndex="8" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                                    </td>
                                </tr>
                                <tr id="trHideDetail" runat="server" visible="false">
                                    <td align="right" colspan="4">
                                        <asp:Button ID="btnHide" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Hide Detail" TabIndex="5" OnClick="btnHide_Click" />
                                    </td>
                                </tr>
                                <tr id="trSalaryRecon" runat="server" visible="false">
                                    <td align="left" class="matters" colspan="4">
                                        <asp:Label ID="lblSalaryRecon" runat="server" EnableViewState="False" Font-Underline="True">Salary Reconcilation</asp:Label>
                                        <asp:GridView ID="gvSalaryRecon" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CaptionAlign="Top" PageSize="15"
                                            CssClass="table table-row table-bordered">
                                            <Columns>
                                                <asp:TemplateField HeaderText="CategoryID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblECTID" runat="server" Text='<%# Bind("ESD_ECT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Month" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMonth" runat="server" Text='<%# Bind("ESD_MONTH") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblYear" runat="server" Text='<%# Bind("ESD_YEAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ECT_DESCR" HeaderText="Category Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PrevAmount" HeaderText="Previous Month Amount"
                                                    DataFormatString="{0:n2}">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CurrAmount" DataFormatString="{0:n2}"
                                                    HeaderText="Current Month Amount">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trSalaryDetail" runat="server" visible="false">
                                    <td align="left" colspan="6">
                                        <asp:Label ID="lblSalaryDetail" runat="server" EnableViewState="False"
                                            Font-Underline="True">Employee Salary Detail</asp:Label>
                                        <asp:GridView ID="GvSalaryDetail" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False"
                                            CaptionAlign="Top" PageSize="15" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="ESD_ECT_ID" HeaderText="CategoryID" Visible="False"></asp:BoundField>
                                                <asp:BoundField DataField="ESD_MONTH" HeaderText="Month" Visible="False"></asp:BoundField>
                                                <asp:BoundField DataField="ESD_YEAR" HeaderText="Year" Visible="False"></asp:BoundField>
                                                <asp:BoundField DataField="EMPNO" HeaderText="Employee No">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMP_NAME" HeaderText="Employee Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ECT_DESCR" HeaderText="Category Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Amount" DataFormatString="{0:n2}" HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:HiddenField ID="h_SFH_ID" runat="server" />
                            <asp:HiddenField ID="h_BSU_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtTrDate" TargetControlID="txtTrDate">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                <asp:HiddenField ID="h_Approved" runat="server" />
                            <asp:HiddenField ID="h_Forwarded" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
