<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empGratuvityslabView.aspx.vb" Inherits="Payroll_empGratuvityslabView" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Gratuity Rule
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table align="center" width="100%">
            <tr valign="top" > 
                <td valign="top" align="left" >
                <asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink>
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    </td>
            </tr>
        </table>  
    <table align="center" width="100%">
           
            <tr>
                <td align="center" valign="top" >
                    <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                        SkinID="GridViewView" Width="100%" AllowPaging="True" PageSize="30"> 
                        <Columns> 
                            <asp:TemplateField HeaderText="Category">
                                <HeaderTemplate>
                                    Category<br />
                                    <asp:TextBox ID="txtCategory" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ECT_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contract Type">
                                <HeaderTemplate>
                                    Contract Type<br />
                                    <asp:TextBox ID="txtContract" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("BGS_CONTRACTTYPE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction Type" SortExpression="ELT_DESCR">
                                <HeaderTemplate>
                                   Transaction Type<br />
                                    <asp:TextBox ID="txtTransaction" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("BGS_TRANTYPE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From">
                                <HeaderTemplate>
                                    From<br />
                                    <asp:TextBox ID="txtFrom" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("BGS_DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To">
                                <HeaderTemplate>
                                    To<br />
                                    <asp:TextBox ID="txtTo" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("BGS_DTTO", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="BGS_DESCRIPTION"  HeaderText="Remarks" />
                              <asp:BoundField DataField="fromto" HeaderText="Setting For" ReadOnly="True" />
                                <asp:BoundField DataField="BGS_PROVISIONDAYS" HeaderText="Provision Days" ReadOnly="True" /> 
                                 <asp:BoundField DataField="BGS_ACTUALDAYS" HeaderText="Actual Days" ReadOnly="True" />
                            <asp:TemplateField HeaderText="View">
                                <ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BGS_ID" Visible="False">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("BGS_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView> 
                </td>
            </tr>
        </table>

            </div>
        </div>
    </div>

</asp:Content>


