﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Payroll_empMasterDetail_CovidDcocuments
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or
            (ViewState("MainMnu_code") <> "P050139") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit,
                ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"),
                ViewState("menu_rights"), ViewState("datamode"))
                gvEMPDETAILS.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
                If Request.QueryString("deleted") <> "" Then
                    lblError.Text = getErrorMessage("520")
                End If
                ViewState("datamode") = Encr_decrData.Encrypt("add")
            End If
        End If
    End Sub

    Public Function returnpath(ByVal p_bPhotos As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_bPhotos)
            If p_bPhotos Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvEMPDETAILS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvEMPDETAILS.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvEMPDETAILS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPDETAILS.PageIndexChanging
        gvEMPDETAILS.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvEMPDETAILS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPDETAILS.RowDataBound
        Try
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            'Dim cmdCol As Integer = gvEMPDETAILS.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            Dim imgDoc As HyperLink = (TryCast(e.Row.FindControl("imgDoc"), HyperLink))
            Dim hfCOVDOC_ID As HiddenField = (TryCast(e.Row.FindControl("hfCOVDOC_ID"), HiddenField))
            If hfCOVDOC_ID IsNot Nothing Then
                If hfCOVDOC_ID.Value > 0 Then
                    Dim hfCOV_ID As String = (TryCast(e.Row.FindControl("hfCOVDOC_ID"), HiddenField)).Value
                    imgDoc.Visible = True
                    imgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(hfCOV_ID)
                Else imgDoc.Visible = False
                End If
            End If
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.Attributes.Add("OnClick", "OpenCovidPopup('" & lblEMPID.Text & "');return false;")
                'If Session("sBSUId") <> "800017" Then
                '    hlview.NavigateUrl = "empMasterDetail_Documents.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                'Else
                '    hlview.NavigateUrl = "empMasterDetail_Documents_Aqaba.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                'End If

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrEmpDOJ As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrPassportNo As String = String.Empty
        Dim lstrDesignation As String = String.Empty
        Dim lstrVisaID As String = String.Empty

        Dim lstrFiltDOJ As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltPassportNo As String = String.Empty
        Dim lstrFiltDesignation As String = String.Empty
        Dim lstrFiltVisaID As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        If gvEMPDETAILS.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDOJ")
            lstrEmpDOJ = Trim(txtSearch.Text)
            If (lstrEmpDOJ <> "") Then lstrFiltDOJ = SetCondn(lstrOpr, "EMP_JOINDT", lstrEmpDOJ)
            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EMPNO", lstrEMPNo)
            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)
            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPassportNo")
            lstrPassportNo = txtSearch.Text
            If (lstrPassportNo <> "") Then lstrFiltPassportNo = SetCondn(lstrOpr, "EMP_PASSPORT", lstrPassportNo)
            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDesignation")
            lstrDesignation = txtSearch.Text
            If (lstrDesignation <> "") Then lstrFiltDesignation = SetCondn(lstrOpr, "CATEGORY_DESC", lstrDesignation)
            larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            'txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtVisaID")
            'lstrVisaID = txtSearch.Text
            'If (lstrVisaID <> "") Then lstrFiltVisaID = SetCondn(lstrOpr, "EMP_VISA_ID", lstrVisaID)
        End If
        Dim searchStg As String = ""
        searchStg = str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName
        If searchStg <> "" Then
            searchStg = searchStg.Replace("'%", "''%")
            searchStg = searchStg.Replace("%'", "%''")
        End If
        'lstrFiltDesignation & lstrFiltPassportNo &
        'str_Sql = "exec Get_EMP_COVID_DETAILS '" & Session("sBsuid") & "'"
        str_Sql = "exec Get_EMP_COVID_DETAILS '" & Session("sBsuid") & "','" & searchStg & "'"

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT, EMP_VISA_ID, " &
        '" EMP_PASSPORT, EMP_STATUS_DESCR, CATEGORY_DESC as EMP_DES_DESCR," &
        '" CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 0 ELSE 1 END bPhotoExists  " &
        '" FROM vw_OSO_EMPLOYEEMASTER" &
        '" WHERE (EMP_VISA_BSU_ID = '" & Session("sBsuid") & "' " &
        '" or EMP_VISA_BSU_ID in (SELECT UBL_BSU_ID FROM USR_BSULIST where UBL_MNU_ID='" & ViewState("MainMnu_code") & "' and UBL_USR_NAME='" & Session("sUsr_name") & "'))" &
        'str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName &
        'lstrFiltDesignation & lstrFiltPassportNo & lstrFiltVisaID & " order by EMP_STATUS_DESCR, EMPNO, EMPNAME, CATEGORY_DESC"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        gvEMPDETAILS.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEMPDETAILS.DataBind()
            Dim columnCount As Integer = gvEMPDETAILS.Rows(0).Cells.Count

            gvEMPDETAILS.Rows(0).Cells.Clear()
            gvEMPDETAILS.Rows(0).Cells.Add(New TableCell)
            gvEMPDETAILS.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEMPDETAILS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEMPDETAILS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvEMPDETAILS.DataBind()
        End If

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDOJ")
        txtSearch.Text = lstrEmpDOJ

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPassportNo")
        txtSearch.Text = lstrPassportNo

        'txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtVisaID")
        'txtSearch.Text = lstrVisaID

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDesignation")
        txtSearch.Text = lstrDesignation

        gvEMPDETAILS.SelectedIndex = p_selected_id
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function SaveCOVIDData(ByVal bsuid As String, ByVal COV_TEST_DATE As String, ByVal COV_RESULT As String, ByVal usrId As String, ByVal filepath As String, ByVal fileext As String, ByVal filename As String) As String
        Dim returnstring As String = ""

        If COV_TEST_DATE = "0" Or COV_TEST_DATE = "" Or COV_TEST_DATE Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter date of COVID test"
            Return returnstring
        End If

        If COV_RESULT = "0" Or COV_RESULT = "" Or COV_RESULT Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter test result"
            Return returnstring
        End If

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim retMsg As String = ""
            Dim bIsLeaveExceeded As Boolean = False


            'Dim pParms(7) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@COV_EMP_ID", SqlDbType.BigInt)
            'pParms(0).Value = HttpContext.Current.Session("EmployeeID")

            'pParms(1) = New SqlClient.SqlParameter("@COV_LOGDATE", SqlDbType.DateTime)
            'pParms(1).Value = Today.Date

            'pParms(2) = New SqlClient.SqlParameter("@COV_TEST_DATE", SqlDbType.DateTime)
            'pParms(2).Value = COV_TEST_DATE.Trim

            'pParms(3) = New SqlClient.SqlParameter("@COV_RESULT", SqlDbType.VarChar, 200)
            'pParms(3).Value = COV_RESULT

            'pParms(4) = New SqlClient.SqlParameter("@COV_UPLOAD_FILE", SqlDbType.VarChar, 200)
            'pParms(4).Value = filepath


            Dim pParms(6) As SqlClient.SqlParameter 'V1.2 ,V1.3
            pParms(0) = Mainclass.CreateSqlParameter("@COV_EMP_ID", Convert.ToInt64(usrId), SqlDbType.BigInt)
            ' pParms(1) = Mainclass.CreateSqlParameter("@ELA_EMP_ID", EOS_MainClass.GetEmployeeIDFromUserName(usrname), SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@COV_LOGDATE", Today.Date, SqlDbType.DateTime)
            pParms(2) = Mainclass.CreateSqlParameter("@COV_TEST_DATE", COV_TEST_DATE.Trim, SqlDbType.DateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@COV_RESULT", COV_RESULT, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@COV_UPLOAD_FILE", filepath, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@COV_ID", 0, SqlDbType.Int, True)
            pParms(6) = Mainclass.CreateSqlParameter("@COV_CREATED_BY", HttpContext.Current.Session("EmployeeID"), SqlDbType.BigInt)

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Save_EMP_COVID_TESTS", pParms)

            If pParms(5).Value > 0 Then
                If Not filepath Is Nothing Then
                    If (filepath <> "") Then
                        If Not SaveCOVIDDocument(filepath, objConn, stTrans, pParms(5).Value, bsuid, fileext, filename) Then
                            retval = "991"
                        End If
                    End If
                End If
            End If


            If pParms(5).Value > 0 Then
                retval = 0
                stTrans.Commit()
                returnstring = retval + "||" + pParms(5).ToString + "||" + "Successfully Saved, Click close button to proceed"

            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            If ex.Message <> "" Then
                returnstring = "1000" + "||" + "0" + "||" + ex.Message
            Else
                returnstring = "1000" + "||" + "0" + "||" + UtilityObj.getErrorMessage(1000)
            End If

            stTrans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return returnstring
    End Function
    Public Shared Function SaveCOVIDDocument(ByVal fuUpload As String, ByVal con As SqlConnection, tran As SqlTransaction, ByVal PCD_ID As String, ByVal bsuid As String, ByVal fileext As String, ByVal filename As String) As Boolean
        Try


            Dim contenttype As String = String.Empty


            Select Case fileext.ToLower()
                Case ".doc"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".docx"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".xls"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".xlsx"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".jpg"

                    contenttype = "image/jpg"
                    Exit Select

                Case ".jpeg"

                    contenttype = "image/jpeg"
                    Exit Select

                Case ".tif"

                    contenttype = "image/tiff"
                    Exit Select

                Case ".png"

                    contenttype = "image/png"
                    Exit Select

                Case ".gif"

                    contenttype = "image/gif"
                    Exit Select

                Case ".pdf"

                    contenttype = "application/pdf"
                    Exit Select



            End Select

            If contenttype <> String.Empty Then

                Dim bitmapData() As Byte
                Dim sbText As StringBuilder = New StringBuilder(fuUpload, fuUpload.Length)
                sbText.Replace("\r\n", String.Empty)
                sbText.Replace(" ", String.Empty)
                Dim firststring As String = fuUpload.Substring(0, fuUpload.IndexOf(",") + 1)
                sbText.Replace(firststring, String.Empty)
                bitmapData = Convert.FromBase64String(sbText.ToString())

                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", "COVID")
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bitmapData)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", bsuid)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Transaction = tran
                cmd.ExecuteNonQuery()
                Return True

            Else
                '   lblmsg.CssClass = "divInValidMsg"
                '  lblmsg.Text = "<i class='fa fa-2x fa-info text-danger v-align-middle mr-3'></i> File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

End Class

