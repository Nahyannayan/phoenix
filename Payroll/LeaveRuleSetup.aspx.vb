Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Payroll_LeaveRuleSetup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim EAH_PAYYEAR As String
    Dim EAH_PAYMONTH As String
    Dim EAH_bADJADD As Boolean
    Dim EAH_ELT_ID As String
    'Version        Date        Author      Chenge
    '1.1            09-Dec-2012 JacobC      To setup leave rule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvSlab.Attributes.Add("bordercolor", "#1b80b6")

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000081") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                End If
                BindCategory()
                If Me.ddlCategory.Items.Count > 0 Then
                    ddlCategory_SelectedIndexChanged(sender, e)
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub
    Private Sub BindCategory()
        ddlCategory.Items.Clear()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT ECT_ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M where ECT_ID<>8 order by ECT_SORT_ID"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlCategory.DataSource = dr
        ddlCategory.DataTextField = "DESCR"
        ddlCategory.DataValueField = "ECT_ID"
        ddlCategory.DataBind()
        dr.Close()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then

            Dim bEdit As Boolean = False
            Dim transaction As SqlTransaction
            Dim sqlqry As String
            If Me.gvSlab.Rows.Count > 0 Then
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        For Each gvr As GridViewRow In Me.gvSlab.Rows
                            Dim TxtWKD As TextBox = DirectCast(gvr.FindControl("txtiWKD"), TextBox)
                            Dim TxtHOL As TextBox = DirectCast(gvr.FindControl("txtiHOL"), TextBox)
                            Dim chkWKD As CheckBox = DirectCast(gvr.FindControl("chkWKD"), CheckBox)
                            Dim chkHOL As CheckBox = DirectCast(gvr.FindControl("chkHOL"), CheckBox)
                            Dim BLS_ID As Integer = Me.gvSlab.DataKeys(gvr.RowIndex)(0)

                            sqlqry = "update BSU_LEAVESLAB_S set BLS_CascadeIfExceeds_WKD='" & TxtWKD.Text.Trim & "'," & _
                            "BLS_CascadeIfExceeds_HOL='" & TxtHOL.Text.Trim & "',BLS_bCascadeIfCrossing_WKD='" & chkWKD.Checked & "'," & _
                            "BLS_bCascadeIfCrossing_HOL='" & chkHOL.Checked & "' where BLS_ID=" & BLS_ID & ""

                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sqlqry)
                        Next


                        transaction.Commit()
                        lblError.Text = "Record Inserted Successfully"
                        'clearRecords()
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Inserted"

                    End Try
                End Using
            End If
           

        End If
    End Sub

    Sub clearRecords()
        BindCategory()
        Me.ddlCategory.SelectedIndex = 0
        Me.gvSlab.DataSource = ""
        Me.gvSlab.DataBind()
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select A.BLS_ID,isnull(A.BLS_ELT_ID,'')+' - '+A.BLS_DESCRIPTION ELT_DESCR,isnull(BLS_CascadeIfExceeds_WKD,0)iWKD,isnull(BLS_CascadeIfExceeds_HOL,0)iHOL," & _
        "cast(isnull(BLS_bCascadeIfCrossing_WKD,0)as bit)bWKD,cast(isnull(BLS_bCascadeIfCrossing_HOL,0)as bit)bHOL " & _
        "from BSU_LEAVESLAB_S A INNER JOIN EMPLEAVETYPE_M B ON A.BLS_ELT_ID=B.ELT_ID where A.BLS_BSU_ID='" & Session("sBsuid") & "' and  A.BLS_ECT_ID='" & Me.ddlCategory.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Me.gvSlab.DataSource = ""
        Me.gvSlab.DataBind()

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Me.gvSlab.DataSource = ds.Tables(0)
                Me.gvSlab.DataBind()
            End If
        End If


    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clearRecords()
    End Sub
End Class
