Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Imports AjaxControlToolkit
Imports Telerik.Web.UI
Imports System.Collections
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Globalization
Imports System.Xml
Imports System.Collections.Generic
Imports System.EnterpriseServices
Imports System.Net
Imports System.Web.Script.Services

Partial Class Payroll_empDailyAttendanceCorrection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim NotProcessed As Drawing.Color = Drawing.Color.FromArgb(240, 217, 113)
    Dim NoBalanceLeave As Drawing.Color = Drawing.Color.Pink

    Private Property DataLoaded() As Boolean
        Get
            Return ViewState("DataLoaded")
        End Get
        Set(ByVal value As Boolean)
            ViewState("DataLoaded") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            If Not IsPostBack Then
                Page.Title = OASISConstants.Gemstitle

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'Comment Removed by mahesh
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                Dim CAT_ID As String = Request.QueryString("CAT_ID")
                hdnCATID.Value = CAT_ID
                Dim vTYPE As String = "0"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim userSuper As Boolean
                userSuper = Session("sBITSupport")

                hdnHREntry.Value = "0"
                If ViewState("MainMnu_code") = "P170023" Then
                    ViewState("FROMDATE") = Format(DateAdd(DateInterval.Day, -7, CDate(DateTime.Now.Date)), OASISConstants.DateFormat)
                    ViewState("TODATE") = Format(CDate(DateTime.Now.Date), OASISConstants.DateFormat)
                    RadReportTo.Visible = userSuper
                    trSuperUser.Visible = userSuper
                    RadReportTo.Enabled = userSuper
                    trCate.Visible = False
                    BindReportTo()
                    BindCategory()
                    RadAttDate.SelectedDate = Format(DateAdd(DateInterval.Day, -7, CDate(DateTime.Now.Date)), OASISConstants.DateFormat)
                    RadAttToDate.SelectedDate = Format(CDate(DateTime.Now.Date), OASISConstants.DateFormat)
                ElseIf ViewState("MainMnu_code") = "P170024" Then
                    ViewState("FROMDATE") = Format(DateAdd(DateInterval.Day, -7, CDate(DateTime.Now.Date)), OASISConstants.DateFormat)
                    ViewState("TODATE") = Format(CDate(DateTime.Now.Date), OASISConstants.DateFormat)
                    RadReportTo.Visible = True
                    trSuperUser.Visible = True
                    RadReportTo.Enabled = True
                    trCate.Visible = False
                    BindReportTo()
                    BindCategory()
                    hdnHREntry.Value = "1"
                    RadAttDate.SelectedDate = Format(DateAdd(DateInterval.Day, -7, CDate(DateTime.Now.Date)), OASISConstants.DateFormat)
                    RadAttToDate.SelectedDate = Format(CDate(DateTime.Now.Date), OASISConstants.DateFormat)
                Else
                    ViewState("FROMDATE") = Encr_decrData.Decrypt(Request.QueryString("FROMDATE").Replace(" ", "+"))
                    ViewState("TODATE") = Encr_decrData.Decrypt(Request.QueryString("TODATE").Replace(" ", "+"))
                    RadReportTo.Visible = False
                    trSuperUser.Visible = False
                    trFilter.Visible = False
                    trCategory.Visible = False
                End If

                If Request.QueryString("TYPE") IsNot Nothing Then
                    ViewState("TYPE") = Request.QueryString("TYPE")
                Else
                    ViewState("TYPE") = 0
                End If
                hdnType.Value = ViewState("TYPE")
                hdnFromDT.Value = ViewState("FROMDATE")
                hdnToDT.Value = ViewState("TODATE")
                ViewState("datamode") = "add"

                Select Case vTYPE
                    Case "0"
                        lblType.Text = "(All)"
                    Case "1"
                        lblType.Text = "(On Leave)"
                    Case "2"
                        lblType.Text = "(On Approved Leave)"
                    Case "3"
                        lblType.Text = "(Absentees)"
                End Select
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "" And ViewState("MainMnu_code") <> "P170023" And ViewState("MainMnu_code") <> "P170022" And ViewState("MainMnu_code") <> "P170024") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If ViewState("MainMnu_code") = "P170023" Then

                        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                        Dim sql_str As String = " SELECT ISNULL(ECT_DESCR,'') FROM  EMPCATEGORY_M WHERE ECT_ID = " & RadCategory.SelectedValue
                        Dim strCategory As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_str)

                        'gridbind(GetCategoryOfEmployee(Session("EmployeeId")), ViewState("FROMDATE"), ViewState("TODATE"), 0, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
                        'gridbind(GetCategoryOfEmployee(Session("EmployeeId")), ViewState("FROMDATE"), ViewState("TODATE"), 0, RadEmpAttendance.PageSize, Session("EmployeeId"))

                        lblType.Text = "Employee Daily Attendance / Category - All" & " / Date Range - " & Format(CDate(ViewState("FROMDATE")), OASISConstants.DateFormat).ToString & " To " & Format(CDate(ViewState("TODATE")), OASISConstants.DateFormat).ToString
                        gridbind(CAT_ID, ViewState("FROMDATE"), ViewState("TODATE"), 0, 0, 1)
                    ElseIf ViewState("MainMnu_code") = "P170024" Then
                        lblType.Text = Mainclass.GetMenuCaption("P170024")
                    Else

                        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                        Dim sql_str As String = " SELECT ISNULL(ECT_DESCR,'') FROM  EMPCATEGORY_M WHERE ECT_ID = " & CAT_ID
                        Dim strCategory As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_str)
                        lblType.Text = "Employee Daily Attendance / Category - " & strCategory & " / Date Range - " & Format(CDate(ViewState("FROMDATE")), OASISConstants.DateFormat).ToString & " To " & Format(CDate(ViewState("TODATE")), OASISConstants.DateFormat).ToString
                        BindCategory(CAT_ID)
                        gridbind(CAT_ID, ViewState("FROMDATE"), ViewState("TODATE"), 0, 0)

                    End If
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub BindCategory(ByVal NotInCatID As Int16)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim sql_str As String
            sql_str = " SELECT   DISTINCT ECT_DESCR, ECT_ID  FROM EMPCATEGORY_M WHERE ECT_ID <> " & NotInCatID.ToString & " and ECT_ID in (select EMP_ECT_ID from EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "') ORDER BY ECT_ID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
            dlOtherCategories.DataSource = ds
            dlOtherCategories.DataBind()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub BindCategoryEmployeeid(ByVal NotInCatID As Int16)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim sql_str As String
            sql_str = " SELECT   DISTINCT ECT_DESCR, ECT_ID  FROM EMPCATEGORY_M WHERE ECT_ID <> " & NotInCatID.ToString & " and ECT_ID in (select EMP_ECT_ID from EMPLOYEE_M WHERE EMP_ID='" & Session("EmployeeId") & "') ORDER BY ECT_ID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
            dlOtherCategories.DataSource = ds
            dlOtherCategories.DataBind()
        Catch ex As Exception
        End Try
    End Sub


    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Try
                Dim encrData As New Encryption64
                Dim h_ECTID As New HiddenField
                Dim url As String = String.Empty
                h_ECTID = TryCast(sender.FindControl("h_ECTID"), HiddenField)
                If Not h_ECTID Is Nothing AndAlso Not h_ECTID Is Nothing Then
                    Dim FromDtStr As String
                    Dim ToDtStr As String
                    FromDtStr = encrData.Encrypt(CDate(hdnFromDT.Value).ToString("dd/MMM/yyyy"))
                    ToDtStr = encrData.Encrypt(CDate(hdnToDT.Value).ToString("dd/MMM/yyyy"))
                    url = String.Format("{0}?CAT_ID={1}&FROMDATE={3}&TODATE={4}&MainMnu_code={2}&Type={5}", "empDailyAttendanceCorrection.aspx", h_ECTID.Value, Request.QueryString("MainMnu_code"), FromDtStr, ToDtStr, hdnType.Value.ToString)
                    Response.Redirect(url)
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Shared Function getAttendanceDetail(ByVal vCAT_ID As String, ByVal vEMP_ID As String, ByVal vType As Int16, ByVal vFromDate As DateTime, ByVal vToDate As DateTime, ByVal LastLoadedID As Int16, ByVal LoadRecordCount As Int16, ByVal CheckLeaveBalance As Boolean, ByVal EMPNO As String, ByVal EMPNAME As String, Optional ByVal RepEmpId As Integer = 0) As DataSet
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(14) As SqlClient.SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@BSU_ID", HttpContext.Current.Session("sBsuid"), SqlDbType.VarChar, False)
            param(1) = Mainclass.CreateSqlParameter("@FROMDT", vFromDate, SqlDbType.DateTime, False)
            param(2) = Mainclass.CreateSqlParameter("@TODT", vToDate, SqlDbType.DateTime, False)
            param(3) = Mainclass.CreateSqlParameter("@TYPE", vType, SqlDbType.Int, False)
            param(4) = Mainclass.CreateSqlParameter("@CAT_ID", vCAT_ID, SqlDbType.Int, False)
            'param(5) = Mainclass.CreateSqlParameter("@REP_EMP_ID", 0, SqlDbType.Int, False)
            param(5) = Mainclass.CreateSqlParameter("@REP_EMP_ID", RepEmpId, SqlDbType.Int, False)
            param(6) = Mainclass.CreateSqlParameter("@ELT_ID", "All", SqlDbType.VarChar, False)
            param(7) = Mainclass.CreateSqlParameter("@EMP_ID", vEMP_ID, SqlDbType.Int, False)
            param(8) = Mainclass.CreateSqlParameter("@ProcessStatus", 0, SqlDbType.Int, False)
            param(9) = Mainclass.CreateSqlParameter("@Summary", 2, SqlDbType.Int, False)
            param(10) = Mainclass.CreateSqlParameter("@LastLoadedID", 0, SqlDbType.Int, False) 'LastLoadedID
            param(11) = Mainclass.CreateSqlParameter("@LoadRecordCount", 0, SqlDbType.Int, False) 'LoadRecordCount
            param(12) = Mainclass.CreateSqlParameter("@CheckLeaveBalance", CheckLeaveBalance, SqlDbType.Bit, False)
            param(13) = Mainclass.CreateSqlParameter("@EMPNO", EMPNO, SqlDbType.VarChar, False)
            param(14) = Mainclass.CreateSqlParameter("@EMPNAME", EMPNAME, SqlDbType.VarChar, False)
            Dim ds As New DataSet
            'ds = Mainclass.getDataSet( "GET_EMP_ATT_DASHBOARD_CORRECTION", param,str_conn)
            ds = SqlHelper.ExecuteDataset(str_conn, "GET_EMP_ATT_DASHBOARD_CORRECTION", param)
            Return ds
        Catch ex As Exception
            Errorlog(ex.Message)
            Return Nothing
        End Try
    End Function

    Sub gridbind(ByVal vCAT_ID As String, ByVal vFromDate As DateTime, ByVal vToDate As DateTime, ByVal LastLoadedID As Int16, ByVal LoadRecordCount As Int16, Optional ByVal RepEmpId As Integer = 0)
        Try
            If vCAT_ID Is Nothing Then
                vCAT_ID = 0
            End If
            Dim ds As DataSet
            ds = getAttendanceDetail(vCAT_ID, 0, ViewState("TYPE"), vFromDate, vToDate, LastLoadedID, LoadRecordCount, False, txtEmpNo.Text, txtEmployeeName.Text, RepEmpId)
            Session("DataSource") = ds
            hdnLastLoadedID.Value = ds.Tables(0).Rows.Count
            RadEmpAttendance.DataSource = ds.Tables(0)
            RadEmpAttendance.DataBind()
            RadEmpAttendance.MasterTableView.AutoGeneratedColumns(2).ItemStyle.Wrap = False
            RadEmpAttendance.MasterTableView.AutoGeneratedColumns(0).Visible = False
            Dim LeaveLegandStr As String = ""
            DataLoaded = True
            Dim mRow As DataRow
            For Each mRow In ds.Tables(1).Rows
                LeaveLegandStr &= mRow(0) & " - " & mRow(1) & "</br>"
            Next
            lblLeaveLegand.Text = LeaveLegandStr

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal _SelectedEmpID As String, ByVal _FromDT As String, ByVal _ToDT As String, ByVal _Type As String, ByVal _ReportTo As String) As String
        Try
            Dim ds As New DataTable
            ds = getAttendanceDetail(0, _SelectedEmpID, _Type, _FromDT, _ToDT, 0, 0, True, "", "", Val(_ReportTo)).Tables(0)
            '  ds = getAttendanceDetail(0, _SelectedEmpID, _Type, _FromDT, _ToDT, 0, 0, True, "", "").Tables(0)
            Dim ColStr As String = ""
            If ds.Rows.Count > 0 Then
                Dim i As Int16
                For i = 3 To ds.Columns.Count - 1
                    ColStr &= IIf(ColStr <> "", "|", "") & ds.Rows(0).Item(i)
                Next
                ColStr = _SelectedEmpID & "@" & ColStr
            End If
            Return ColStr
        Catch ex As Exception

        End Try
    End Function
    Protected Sub RadEmpAttendance_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs)
        Try
            If (e.Item.ItemType = GridItemType.Item) OrElse (e.Item.ItemType = GridItemType.AlternatingItem) Then
                Dim hdnEMPID As HiddenField = DirectCast(e.Item.FindControl("hdnEMPID"), HiddenField)
                If hdnEMPID Is Nothing Then Exit Sub
                Dim imgEmpDetailView As ImageButton = DirectCast(e.Item.FindControl("imgEmpDetailView"), ImageButton)
                Dim imgEmpBulkUpd As ImageButton = DirectCast(e.Item.FindControl("imgEmpBulkUpd"), ImageButton)
                If Not imgEmpDetailView Is Nothing Then
                    imgEmpDetailView.Attributes.Add("OnClick", "OpenEmployeeDetailScreen('" & hdnEMPID.Value & "','" & hdnFromDT.Value & "','');return false;")
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub ShowErrorMessage(ByVal strMessage As String)
        lblError.Text = strMessage
        lblErrorFooter.Text = ""
        If RadEmpAttendance.Items.Count > 10 Then
            lblErrorFooter.Text = strMessage
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub


    'Protected Sub RadEmpAttendance_PageIndexChanged(ByVal source As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles RadEmpAttendance.PageIndexChanged
    '    Try
    '        If ViewState("MainMnu_code") = "P170023" Then
    '            'If RadReportTo.SelectedValue = 0 Then
    '            gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, (e.NewPageIndex * RadEmpAttendance.PageSize) - 1, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
    '            'Else
    '            '    gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, (e.NewPageIndex * RadEmpAttendance.PageSize) - 1, RadEmpAttendance.PageSize, 0)
    '            'End If
    '        Else
    '            Dim CATID As Int16
    '            If hdnCATID.Value Is Nothing Or hdnCATID.Value = "" Then
    '                CATID = RadCategory.SelectedValue
    '            Else
    '                CATID = hdnCATID.Value
    '            End If
    '            gridbind(CATID, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, (e.NewPageIndex * RadEmpAttendance.PageSize) - 1, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
    '        End If

    '    Catch ex As Exception
    '        lblError.Text = "Request Not Processed"
    '    End Try
    'End Sub

    'Protected Sub RadEmpAttendance_PageSizeChanged(ByVal source As Object, ByVal e As Telerik.Web.UI.GridPageSizeChangedEventArgs) Handles RadEmpAttendance.PageSizeChanged
    '    Try
    '        If ViewState("MainMnu_code") = "P170023" Then
    '            'If RadReportTo.SelectedValue = 0 Then
    '            gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, (RadEmpAttendance.CurrentPageIndex * RadEmpAttendance.PageSize) - 1, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
    '            'Else
    '            '    gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, (RadEmpAttendance.CurrentPageIndex * RadEmpAttendance.PageSize) - 1, RadEmpAttendance.PageSize, 0)
    '            'End If

    '        Else
    '            gridbind(hdnCATID.Value, ViewState("FROMDATE"), ViewState("TODATE"), (RadEmpAttendance.CurrentPageIndex * RadEmpAttendance.PageSize) - 1, RadEmpAttendance.PageSize)
    '        End If

    '    Catch ex As Exception
    '        lblError.Text = "Request Not Processed"
    '    End Try
    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If ViewState("MainMnu_code") = "P170023" Then
                'If RadReportTo.SelectedValue = 0 Then
                gridbind(RadCategory.SelectedValue, ViewState("FROMDATE"), ViewState("TODATE"), 0, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
                'Else
                '    gridbind(RadCategory.SelectedValue, ViewState("FROMDATE"), ViewState("TODATE"), 0, RadEmpAttendance.PageSize, 0)
                'End If
            Else
                gridbind(hdnCATID.Value, ViewState("FROMDATE"), ViewState("TODATE"), 0, RadEmpAttendance.PageSize)
            End If

        Catch ex As Exception
            lblError.Text = "Request Not Processed"
        End Try
    End Sub

    Private Sub BindReportTo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_str As String = " Select DISTINCT * FROM"
        sql_str &= "  ("
        sql_str &= "  SELECT EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME EMP_NAME  "
        sql_str &= "  from EMPLOYEE_M"
        sql_str &= "  where EMP_ID IN (SELECT DISTINCT EMP_ATT_Approv_EMP_ID from EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "')  "

        sql_str &= "  UNION ALL"
        sql_str &= "  SELECT EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME EMP_NAME  "
        sql_str &= "  from EMPLOYEE_M"
        sql_str &= "  where EMP_ID IN (SELECT DISTINCT EMP_REPORTTO_EMP_ID from EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "')  "

        sql_str &= "  UNION ALL"
        sql_str &= "  SELECT EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME EMP_NAME  "
        sql_str &= "  from EMPLOYEE_M"
        sql_str &= "  where EMP_ID IN (SELECT DISTINCT EJD_DEL_EMP_ID FROM dbo.EMPJOBDELEGATE_S "
        sql_str &= "  WHERE EJD_EMP_ID IN ( SELECT DISTINCT EMP_ATT_Approv_EMP_ID from EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "')"
        sql_str &= "  AND GETDATE() BETWEEN EJD_DTFROM AND EJD_DTTO AND ISNULL(EJD_bForward,0)=1 AND ISNULL(EJD_bDisable,0)=0  AND EJD_EJM_ID ='AT'"
        sql_str &= "  )"
        sql_str &= "  ) A"
        sql_str &= "  ORDER BY EMP_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
        RadReportTo.DataSource = ds
        RadReportTo.DataTextField = "EMP_NAME"
        RadReportTo.DataValueField = "EMP_ID"
        RadReportTo.DataBind()
        Dim lstDrp As New RadComboBoxItem
        lstDrp.Value = 0
        lstDrp.Text = ""
        RadReportTo.Items.Insert(0, lstDrp)
        lstDrp = RadReportTo.Items.FindItemByValue(Session("EmployeeId"))
        If Not lstDrp Is Nothing Then
            RadReportTo.SelectedValue = lstDrp.Value
        Else
            RadReportTo.SelectedValue = 0
        End If
    End Sub
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        lblError.Text = ""
        Me.ViewData()
    End Sub
    Private Sub ViewData()
        If RadReportTo.SelectedValue = 0 Then
            ShowErrorMessage("Please Select the Approver first !!!")

        Else


            If RadAttDate.SelectedDate > Now.Date Then
                ShowErrorMessage("Cannot process the attendance for future date !!!")
                RadAttDate.SelectedDate = Now.Date.ToString("dd/MMM/yyyy")
            Else
                If ViewState("MainMnu_code") = "P170023" Then
                    hdnFromDT.Value = CDate(RadAttDate.SelectedDate).ToString("dd/MMM/yyyy")
                    hdnToDT.Value = CDate(RadAttToDate.SelectedDate).ToString("dd/MMM/yyyy")
                    hdnReportTo.Value = RadReportTo.SelectedValue
                End If


                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim sql_str As String = " SELECT ISNULL(ECT_DESCR,'') FROM  EMPCATEGORY_M WHERE ECT_ID = " & RadCategory.SelectedValue
                Dim strCategory As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_str)
                If RadCategory.SelectedValue = 0 Then strCategory = " All"

                If ViewState("MainMnu_code") <> "P170024" Then
                    lblType.Text = "Employee Daily Attendance / Category - " & strCategory & " / Date Range - " & hdnFromDT.Value & " To " & hdnToDT.Value
                End If
                '    gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, 0, RadEmpAttendance.PageSize, 0)
                'Else
                gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, 0, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
            End If
        End If
    End Sub
    Public Shared Function GetCategoryOfEmployee(ByVal EMPID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "select EMP_ECT_ID from EMPLOYEE_M WHERE EMP_ID=" & EMPID
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetCategoryOfEmployee = RetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim userSuper As Boolean
        userSuper = Session("sBITSupport")
        Dim RepEMPID As String
        If Not RadReportTo.SelectedItem Is Nothing And userSuper Then
            RepEMPID = RadReportTo.SelectedValue
        Else
            RepEMPID = Session("EmployeeId")
        End If
        Dim sql_str As String

        sql_str = " SELECT '-------All-------' ECT_DESCR,0 EMP_ECT_ID UNION ALL "
        sql_str &= " SELECT   DISTINCT ECT_DESCR, EMP_ECT_ID  FROM EMPLOYEE_M INNER JOIN EMPCATEGORY_M "
        sql_str &= " ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' AND EMP_STATUS <> 4 "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)

        RadCategory.DataSource = ds
        RadCategory.DataTextField = "ECT_DESCR"
        RadCategory.DataValueField = "EMP_ECT_ID"
        RadCategory.DataBind()
        RadCategory.SelectedIndex = 0
    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        pnlViewPopup.Attributes.Add("style", "display:none")
        If ViewState("MainMnu_code") = "P170024" Then
            gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, 0, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
        Else
            gridbind(hdnCATID.Value, ViewState("FROMDATE"), ViewState("TODATE"), 0, RadEmpAttendance.PageSize)
        End If
    End Sub


    Protected Sub RadEmpAttendance_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        If DataLoaded Then
            'FillReportData(False)
            ' gridbind(RadCategory.SelectedValue, RadAttDate.SelectedDate, RadAttToDate.SelectedDate, 0, RadEmpAttendance.PageSize, RadReportTo.SelectedValue)
            RadEmpAttendance.DataSource = Session("DataSource")
        End If
    End Sub

End Class


