Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empDoMonthendProcessAirfare
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64



    'ts
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function





    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
       
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim CurBsUnit As String = Trim(Session("sBsuid") & "")
                Dim USR_NAME As String = Trim(Session("sUsr_name") & "")

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P174015" And ViewState("MainMnu_code") <> "P174010" And ViewState("MainMnu_code") <> "P174005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    bind_BusinessUnit()
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "P174015"
                            lblHead.Text = "Air fare"
                            str_transaction = "A"
                        Case "P174010"
                            lblHead.Text = "Gratuity"
                            str_transaction = "G"
                        Case "P174005"
                            lblHead.Text = "Leave Salary"
                            str_transaction = "L"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    txtDate.Text = get_MJH_DOCDT(str_transaction)

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub



    Sub bind_BusinessUnit()

        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else

            Try
                'if user is super admin give access to all the Business Unit
                If tblbUSuper = True Then
                    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        'create a list item to bind records from reader to dropdownlist ddlBunit
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        'check if it return rows or not
                        If BUnitreaderSuper.HasRows = True Then
                            While BUnitreaderSuper.Read
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBUnit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                Else
                    'if user is not super admin get access to the selected  Business Unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        If BUnitreader.HasRows = True Then
                            While BUnitreader.Read
                                di = New ListItem(BUnitreader(2), BUnitreader(0))
                                ddlBUnit.Items.Add(di)
                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If

    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim btnClicked As String = btnSave.ID

        Dim STATUS As Integer
        Dim JHD_NEWDOCNO As String = ""
        Dim str_transaction As String = ""
        If IsDate(txtDate.Text) = False Then
            lblError.Text = "Please Enter Valid Date"
            Exit Sub
        End If
        Select Case ViewState("MainMnu_code").ToString
            Case "P174015"
                str_transaction = "A"
            Case "P174010"
                str_transaction = "G"
            Case "P174005"
                str_transaction = "L"

        End Select
        Dim transaction As SqlTransaction
        'update  the new user
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                STATUS = PayrollFunctions.DoMonthendProcess(ddlBUnit.SelectedItem.Value, _
                txtDate.Text, str_transaction, JHD_NEWDOCNO, transaction, conn)

                If STATUS = 0 Then
                    transaction.Commit()
                    lblError.Text = "Document No Generated : " & JHD_NEWDOCNO & "<br>Data Successfully Saved."
                Else
                    transaction.Rollback()
                    lblError.Text = getErrorMessage(STATUS)
                End If

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Updated"
            End Try
        End Using
    End Sub

  
    Function get_MJH_DOCDT(ByVal p_transaction As String) As String



        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT   dbo.fN_GetLastDayOFmonth (isnull(  DATEADD(month, 1, MAX(MJH_DOCDT)) ,getdate()))" _
                 & " FROM " & OASISConstants.dbFinance & ".DBO.MONTHLYJV_H" _
                 & " WHERE (MJH_BSU_ID = '" & Session("sBsuid") & "') AND (MJH_TYPE = '" & p_transaction & "')"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                Return Format(CDate(ds.Tables(0).Rows(0)(0)), OASISConstants.DateFormat)
            Else
                Return Format(Now.Date, OASISConstants.DateFormat)
            End If
        Catch ex As Exception
            Return Format(Now.Date, OASISConstants.DateFormat)
        End Try

    End Function

End Class
