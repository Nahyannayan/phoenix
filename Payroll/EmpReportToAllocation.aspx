<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="EmpReportToAllocation.aspx.vb" Inherits="Payroll_EmpReportToAllocation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" language="javascript">
        function GetEmp(AorB) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            var bALL;
            sFeatures = "dialogWidth: 820px; ";
            sFeatures += "dialogHeight: 470px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            if (AorB == "A") {
                pMode = "EMPSEARCH";
                document.getElementById('<%= h_PMMode.ClientID%>').value = pMode;
            }
            else if (AorB == "B") {
                pMode = "APPEMPSEARCH";
                document.getElementById('<%= h_PMMode.ClientID%>').value = pMode;
            }
            if (document.getElementById('<%= chkAllEMP.ClientID %>').checked == true)
                bALL = "1";
            else
                bALL = "0";
            url = "../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1&AllStaff=" + bALL;  //+ bsu;
            result = radopen(url, "pop_up");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            else {
                if (pMode == "EMPSEARCH") {
                    document.getElementById('<%= txtEmpName.ClientID %>').value = "Multiple Employees selected";
                    document.getElementById('<%= h_EMP_ID.ClientID %>').value = result;

                }
                else if (pMode = "APPEMPSEARCH") {
                    NameandCode = result.split('___');
                    //document.getElementById('<%= txtAppEmp.ClientID %>').value = "";
                    document.getElementById('<%= h_APP_EMP_ID.ClientID %>').value = NameandCode[0];
                }--%>
        //}

        }

         function OnClientClose(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
                if (document.getElementById('<%= h_PMMode.ClientID%>').value == "EMPSEARCH") {
                    if (arg) {
                                               
                        document.getElementById('<%= txtEmpName.ClientID %>').value = arg.NameandCode;
                        document.getElementById('<%= h_EMP_ID.ClientID %>').value = arg.NameandCode;
                         __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');
                    }


                }
                else if (document.getElementById('<%= h_PMMode.ClientID%>').value = "APPEMPSEARCH") {
                    if (arg) {
                       
                        NameandCode = arg.NameandCode.split('||');                        
                        document.getElementById('<%= txtAppEmp.ClientID %>').value = NameandCode[2];
                        document.getElementById('<%= h_APP_EMP_ID.ClientID %>').value = NameandCode[0];
                         __doPostBack('<%= txtAppEmp.ClientID%>', 'TextChanged');
                    }
                }
            }
         }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table style="border-collapse: collapse" align="center" width="100%" cellpadding="5" cellspacing="0">
                    <%--   <tr class="subheader_BlueTableView">
            <th align="left" colspan="3" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
            </th>
        </tr>--%>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Select Employees</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpName" runat="server" AutoPostBack="True" OnTextChanged="txtEmpName_TextChanged"></asp:TextBox><asp:LinkButton
                                ID="lblAddNewEmp" runat="server" CausesValidation="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgEmp" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEmp('A'); return false;" />
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <telerik:RadGrid ID="gvEmployee" runat="server" AutoGenerateColumns="False" Width="100%" Visible="false"
                                AllowPaging="True" PageSize="20" AllowSorting="True" CellSpacing="1" GridLines="None"
                                EnableTheming="False">
                                <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="True">
                                </ClientSettings>
                                <MasterTableView DataKeyNames="EMP_ID">
                                    <ItemStyle CssClass="RadGridRow" HorizontalAlign="Left" />
                                    <AlternatingItemStyle CssClass="RadGridRow" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="EMPNO" HeaderText="EMPLOYEE NO." UniqueName="column">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EMP_NAME" HeaderText="EMPLOYEE NAME" UniqueName="column1">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ECT_DESCR" HeaderText="CATEGORY" UniqueName="column4">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RTO_EMP_NAME" HeaderText="REPORTING TO" UniqueName="column2">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AAP_EMP_NAME" HeaderText="ATTENDANCE APPROVED BY"
                                            UniqueName="column3">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn UniqueName="TemplateColumn">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click">Clear All</asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn>
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                            </telerik:RadGrid>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Reports To/Approver</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAppEmp" runat="server" Width="50%" AutoPostBack="True"></asp:TextBox>
                            <asp:LinkButton ID="lblAddAppNewEmp" runat="server" CausesValidation="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgAppEmp" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEmp('B');return false;" />
                            <asp:CheckBox ID="chkAllEMP" runat="server" Text="All Employees" Style="display: none;" />
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" align="center" colspan="4">
                            <asp:RadioButtonList ID="rblAppType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="A">Attendance ReportingTo</asp:ListItem>
                                <asp:ListItem Value="N">Reporting To</asp:ListItem>
                                <asp:ListItem Value="B">Both</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMP_ID" runat="server" />
                <asp:HiddenField ID="h_APP_EMP_ID" runat="server" />
                <asp:HiddenField ID="h_PMMode" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
