﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empTerminationMasterUpdate.aspx.vb" Inherits="Payroll_empTerminationMasterUpdate" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>

    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script type="text/javascript" language="javascript">
 
    </script>
    <script type="text/javascript" language="javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
        <table width="100%">
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tr>
                            <td align="left" width="100%">
                                <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <ajaxToolkit:ToolkitScriptManager ID="ScriptManagerNew" runat="server">
                    </ajaxToolkit:ToolkitScriptManager>

                </td>
            </tr>
            <tr>
                <td align="center">

                    <table width="100%" cellspacing="0">
                        <tr class="title-bg">
                            <td colspan="4" align="left">Employee master update</td>

                        </tr>
                        <tr>
                            <td align="left" style="width: 20%;"><span class="field-label">Employee Number</span></td>

                            <td align="left" width="30%">
                                <asp:Label ID="lblemployeeno" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                            <td align="left" style="width: 20%;"><span class="field-label">Employee Name</span></td>

                            <td align="left" width="30%">
                                <asp:Label ID="lbl_Empname" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <%-- <tr>
                           
                        </tr>--%>
                        <tr>
                            <td align="left" style="width: 20%;"><span class="field-label">Contract Type</span></td>

                            <td align="left" style="width: 30%;">
                                <asp:DropDownList ID="ddlEmpContractType" runat="server">
                                    <asp:ListItem Value="0">Limited</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Unlimited</asp:ListItem>
                                    <asp:ListItem Value="3">N/A</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 20%;"><span class="field-label">Contract End Date</span><br />
                                (for Limited Contract only)</td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="txtContractEndDt" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                    BackColor="White" /></td>

                        </tr>
                        <%-- <tr>
                            

                        </tr>--%>
                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server"
                            Format="dd/MMM/yyyy" PopupButtonID="txtContractEndDt" TargetControlID="txtContractEndDt">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="txtVisaCancelDt_CalendarExtender" runat="server"
                            Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtContractEndDt">
                        </ajaxToolkit:CalendarExtender>
                        <tr>
                            <td align="left" style="width: 20%;"><span class="field-label">Total&nbsp; LOP Days</span></td>
                            <td align="left" style="width: 30%;">
                                <asp:Label ID="lblCurrentLOPs" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                            <td align="left" style="width: 20%;"><span class="field-label">LOP for first 5 years</span></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="txtLOPFirstFiveYr" runat="server"></asp:TextBox>
                                <br />
                                <asp:Label ID="lblLopfirst" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <%--  <tr>
                            

                        </tr>--%>
                        <tr>
                            <td align="left" style="width: 20%;"><span class="field-label">LOP after first 5 years</span></td>
                            <td align="left" style="width: 30%;">
                                <asp:TextBox ID="txtLOPAfterFiveYr" runat="server"></asp:TextBox>
                                <br />
                                <asp:Label ID="lblLOPLast" runat="server" CssClass="field-value"></asp:Label></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
