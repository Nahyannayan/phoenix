Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Partial Class Payroll_empLoanApplicationView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_AbortTransaction(sender As Object, e As EventArgs) Handles Me.AbortTransaction

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If Session("BSU_IsOnDAX") = 1 Then
                hlAddNew.NavigateUrl = "AXempLoanApplication.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Else
                hlAddNew.NavigateUrl = "empLoanApplication.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            End If

            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            Try
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "P130050" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""
            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)
                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)
                '   -- 2  txtLType
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ERN_DESCR", lstrCondn3)
                '   -- 3   txtDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELH_AMOUNT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELH_PURPOSE", lstrCondn5)
            End If
            str_Sql = "SELECT * FROM( SELECT EMPLOAN_H.ELH_BSU_ID, EMPLOYEE_M.EMPNO, EMPLOAN_H.ELH_PURPOSE, " _
                & " EMPLOAN_H.ELH_DATE, EMPLOAN_H.ELH_INSTNO, EMPLOAN_H.ELH_CUR_ID," _
                & " EMPLOAN_H.ELH_AMOUNT, EMPLOAN_H.ELH_BALANCE, EMPLOAN_H.ELH_bPERSONAL, " _
                & " EMPLOAN_H.ELH_bPosted, EMPLOAN_H.ELH_ID, " _
                & " ISNULL(EMPLOYEE_M.EMP_FNAME,'')+' '+ ISNULL(EMPLOYEE_M.EMP_MNAME,'')+' '+ " _
                & " ISNULL(EMPLOYEE_M.EMP_LNAME,'') AS EMP_NAME," _
                & " EMPSALCOMPO_M.ERN_DESCR ,convert(varchar(10),isnull(EMPLOAN_H.ELH_IsAXPosting,0)) ELH_IsAXPosting,convert(varchar(10),isnull(EMPLOAN_H.ELH_IsPostedToDAX,0)) ELH_IsPostedToDAX , " _
                & " ISNULL(EMPLOAN_H.ELH_PAYREFNO,'') AS ELH_PAYREFNO, ISNULL(EMPLOAN_H.ELH_DOCTYPE,'') AS ELH_DOCTYPE " _
                & " FROM EMPLOAN_H INNER JOIN" _
                & " EMPLOYEE_M ON EMPLOAN_H.ELH_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN" _
                & " EMPSALCOMPO_M ON EMPLOAN_H.ELH_ERN_ID = EMPSALCOMPO_M.ERN_ID) AS  DB" _
                & " WHERE ELH_BSU_ID='" & Session("SBSUID") & "'" & str_Filter & " order by elh_id desc "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
                Dim cnt As Integer = 0
                cnt = gvJournal.Columns.Count

                If Session("BSU_IsOnDAX") = 1 Then
                    gvJournal.Columns(cnt - 1).Visible = True
                    gvJournal.Columns(9).Visible = False
                Else
                    gvJournal.Columns(cnt - 1).Visible = False
                End If
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim lblELH_IsAXPosting As New Label
            lblELH_IsAXPosting = TryCast(e.Row.FindControl("lblELH_IsAXPosting"), Label)
            Dim lblIsPostedToDAX As New Label
            lblIsPostedToDAX = TryCast(e.Row.FindControl("lblIsPostedToDAX"), Label)
            Dim btnPostToDAX As New Button
            btnPostToDAX = TryCast(e.Row.FindControl("btnPostToDAX"), Button)
            Dim lblPosted As New Label
            lblPosted = TryCast(e.Row.FindControl("lblPosted"), Label)
            Dim lblPAYREFNo As New Label
            lblPAYREFNo = TryCast(e.Row.FindControl("lblPAYREFNo"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                If lblELH_IsAXPosting.Text = "0" Then
                    hlEdit.NavigateUrl = "empLoanApplication.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")

                Else
                    hlEdit.NavigateUrl = "AXempLoanApplication.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    If lblIsPostedToDAX.Text = "0" Then
                        btnPostToDAX.Visible = True
                        lblPosted.Visible = False
                    Else
                        btnPostToDAX.Visible = False
                        lblPosted.Visible = True
                        lblPosted.Text = lblPAYREFNo.Text '"Posted"
                    End If

                End If

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

#Region "PostToDAX"


    '**************************** Posting to DAX *********************************
    Protected Sub btnPostToDAX_Click(sender As Object, e As EventArgs)
        Try


            Dim retval As String = "1000"
            Dim lblELH_IsAXPosting As Label, lblPAYREFNo As Label, lblDoctype As Label, lblIsPostedToDAX As Label, lblELA_ID As Label
            lblELH_IsAXPosting = sender.Parent.parent.FindControl("lblELH_IsAXPosting")
            lblPAYREFNo = sender.Parent.parent.FindControl("lblPAYREFNo")
            lblDoctype = sender.Parent.parent.FindControl("lblDoctype")
            lblIsPostedToDAX = sender.Parent.parent.FindControl("lblIsPostedToDAX")
            lblELA_ID = sender.Parent.parent.FindControl("lblELA_ID")
            If lblIsPostedToDAX.Text = "0" Then
                If lblPAYREFNo.Text <> "" Then
                    retval = post_voucher(lblPAYREFNo.Text, lblDoctype.Text, lblELA_ID.Text)

                End If
            End If
            gridbind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Function post_voucher(ByVal p_Docno As String, ByVal p_DocTYPE As String, LoanID As String) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            'your transaction here
            '@JHD_SUB_ID	varchar(10),
            '	@JHD_BSU_ID	varchar(10),
            '	@JHD_FYEAR	int,
            '	@JHD_DOCTYPE varchar(20),
            '	@JHD_DOCNO	varchar(20) 
            ''get header info
            Dim cmd As New SqlCommand("FIN.POSTVOUCHER", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
            sqlpJHD_SUB_ID.Value = Session("SUB_ID")
            cmd.Parameters.Add(sqlpJHD_SUB_ID)

            Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
            sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

            Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
            sqlpJHD_FYEAR.Value = Session("F_YEAR")
            cmd.Parameters.Add(sqlpJHD_FYEAR)

            Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
            sqlpJHD_DOCTYPE.Value = p_DocTYPE
            cmd.Parameters.Add(sqlpJHD_DOCTYPE)

            Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
            sqlpJHD_DOCNO.Value = p_Docno
            cmd.Parameters.Add(sqlpJHD_DOCNO)

            Dim iReturnvalue As Integer
            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.ExecuteNonQuery()

            iReturnvalue = retValParam.Value

            If iReturnvalue = 0 Then
                iReturnvalue = PostToPayrollAXStaging(objConn, stTrans, p_Docno)

            End If
            If iReturnvalue = 0 Then

                Dim str_sql As String
                str_sql = " update OASIS.DBO.EMPLOAN_H set ELH_IsPostedToDAX=1 where ELH_ID= " & LoanID
                iReturnvalue = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
                If iReturnvalue <> -1 Then
                    stTrans.Commit()
                    lblError.Text = "Posted to DAX successfully!.. Ref :" & p_Docno
                Else
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
                    Return iReturnvalue
                    Exit Function
                End If

            Else
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
                Return iReturnvalue
                Exit Function
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, p_Docno, "Posting", Page.User.Identity.Name.ToString, Me.Page)
            Return iReturnvalue
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Function

    Private Function PostToPayrollAXStaging(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal RefDocNo As String) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            If RefDocNo <> "" Then
                Dim cmd As New SqlCommand("oasis_dax.dbo.SavePayroll_DAX_Postings", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure


                Dim sqlpJHD_DocNo As New SqlParameter("@DOCNO", SqlDbType.VarChar)
                sqlpJHD_DocNo.Value = RefDocNo
                cmd.Parameters.Add(sqlpJHD_DocNo)

                Dim sqlpJHD_Narration As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpJHD_Narration.Value = Session("sBSuid").ToString
                cmd.Parameters.Add(sqlpJHD_Narration)


                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                Return iReturnvalue
            Else
                Return 1000

            End If

        Catch ex As Exception
            lblError.Text = ex.Message
            Return 1000
        End Try

    End Function

#End Region
End Class
