Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Partial Class Payroll_empProcessVacationView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'ts

            'hlAddNew.NavigateUrl = "empProcessVacation.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If ViewState("MainMnu_code") = "P153059" Then
                    hlAddNew.NavigateUrl = "empProcessVacation_STS.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                Else
                    hlAddNew.NavigateUrl = "empProcessVacation.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

                End If
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153060" And ViewState("MainMnu_code") <> "P130192" And ViewState("MainMnu_code") <> "P153059") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
                If ViewState("MainMnu_code") = "P130192" Then
                    btnRemove.Visible = True
                    hlAddNew.Visible = False
                    gvJournal.Columns(10).Visible = True
                Else
                    btnRemove.Visible = False
                    hlAddNew.Visible = True
                    gvJournal.Columns(10).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)
                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)
                '   -- 2  txtLType
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ERN_DESCR", lstrCondn3)
                '   -- 3   txtDate
                'larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                'lstrCondn4 = txtSearch.Text
                'If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELH_AMOUNT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELH_PURPOSE", lstrCondn5)
            End If
            Dim strSalCondition As String = ""

            If ViewState("MainMnu_code") = "P130192" Then
                strSalCondition = " and ELV_ID not in (select distinct esd_elv_id from empsalarydata_d where (esd_paid=1 or esd_bposted=1) and esd_bsu_id=  '" & Session("SBSUID") & "') "
            End If

            str_Sql = "SELECT * FROM(SELECT ELV.ELV_ID, ELV.ELV_BSU_ID, ELV.ELV_EMP_ID, ELV.ELV_ELT_ID, ELV.ELV_ELA_ID, " _
            & " REPLACE(CONVERT(VARCHAR(11), ELV.ELV_DTFROM ,113), ' ', '/') AS ELV_DTFROM, REPLACE(CONVERT(VARCHAR(11), ELV.ELV_DTTO, 113), ' ', '/') AS ELV_DTTO, ELV.ELV_REMARKS, ELV.ELV_SALARY, ELV.ELV_LVSALARY, " _
            & " ELV.ELV_AIRTICKET, convert(decimal,ELV.ELV_TKTCOUNT) as ELV_TKTCOUNT,convert(decimal,ELV.ELV_ACTUALDAYS) as ELV_ACTUALDAYS, ELV.ELV_LOPDAYS, EM.EMPNO, " _
            & " ISNULL(EM.EMP_FNAME,'') +' '+ ISNULL(EM.EMP_MNAME,'')+' '+ ISNULL(EM.EMP_LNAME,'') AS EMP_NAME,ELV.ELV_bOpening " _
            & " FROM EMPLEAVE_D AS ELV INNER JOIN EMPLOYEE_M AS EM ON ELV.ELV_EMP_ID = EM.EMP_ID) AS  DB" _
            & " WHERE ELV_BSU_ID = '" & Session("SBSUID") & "' AND ELV_bOpening = 0 and ELV_ELT_ID='AL'" & strSalCondition & str_Filter & " order by convert(datetime,ELV_DTFROM) desc"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
                lblError.Text = ""
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
            txtSearch.Text = lstrCondn3

            'txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            'txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empProcessVacation.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim count As Integer = 0
        ' Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim str_removed As String = ""
            For i As Integer = 0 To gvJournal.Rows.Count - 1
                Dim lblid As New Label
                Dim chkESD_ID As New HtmlInputCheckBox
                Dim lblESD_EMP_ID As New Label


                Dim K As Integer = True
                chkESD_ID = TryCast(gvJournal.Rows(i).FindControl("chkESD_ID"), HtmlInputCheckBox)
                lblESD_EMP_ID = TryCast(gvJournal.Rows(i).FindControl("Label1"), Label)
                'V1.1 changes    
                If chkESD_ID.Checked Then
                    count = count + 1
                    If ViewState("MainMnu_code") = "P130192" Then

                        retval = RemoveVacationProcessing(chkESD_ID.Value, objConn)
                        str_removed = str_removed & lblESD_EMP_ID.Text & ","

                        If retval <> 0 Then
                            Exit For
                        End If
                    End If
                End If
            Next
            If count = 0 Then
                lblError.Text = "No records selected."
                retval = 1000
                Exit Sub
            End If
            If retval = "0" Then

                'V1.1 changes
                Dim flagAudit As Integer
                Dim err As String = ""

                flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, str_removed, _
                     "Delete", Page.User.Identity.Name.ToString, Me.Page)
                err = "Vacation Processing Removed Successfully ..."

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                ' stTrans.Commit()
                gridbind()
                lblError.Text = err
            Else
                'stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            'stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Private Function RemoveVacationProcessing(ByVal p_ELA_ID As Integer, ByVal conn As SqlConnection) As String


        Try

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.Int)
            pParms(0).Value = p_ELA_ID
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "DeleteEMPLEAVESALARYDATA", pParms)
            RemoveVacationProcessing = pParms(1).Value

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function


  
End Class
