﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Payroll_AX_emp_PnODocumentRequests
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
   
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Page.Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSave)
        GetLoggedInEmployeeDetails()
        If Page.IsPostBack = False Then

            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            'txtFrom.Text = GetDiplayDate()
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "U000025" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            BindDocumentTypes()
            'clear_All()
            If Request.QueryString("viewid") <> "" Then
                GetSelectedPnODocumentRequestDetails(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            End If
        End If
    End Sub

    Sub BindDocumentTypes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "exec GetPnODocumentList '" & Session("sBsuid") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlDocType.DataSource = ds.Tables(0)

            ddlDocType.DataTextField = "PnO_Doc_Description"
            ddlDocType.DataValueField = "PnO_Doc_ID"
            ddlDocType.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Page.Validate()
        If Not Page.IsValid Then
            Exit Sub
        End If
        If ddlDocType.SelectedIndex = 0 Then
            lblError.Text = "Please select a document type."
            Exit Sub
        End If
       
        Dim Ret As String, RecId As String
        Ret = "1000"
        RecId = ""
        Try
          
            If Request.QueryString("viewid") <> "" Then
                ' GetSelectedPnODocumentRequestDetails(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                RecId = SaveDocumentDetailstoAX()
                If RecId = "" Or RecId = "1000" Then
                    lblError.Text = getErrorMessage(Ret)
                    Exit Sub
                End If
                Ret = SaveEMPPnO_DocumentRequest(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")), ddlDocType.SelectedValue, txtAddressTo.Text, txtEmpNotes.Text, txtAddNotes.Text, h_Emp_No.Value,
                                               h_emp_bsu.Value, "")

            Else
                RecId = SaveDocumentDetailstoAX()
                If RecId = "" Or RecId = "1000" Then
                    lblError.Text = getErrorMessage(Ret)
                    Exit Sub
                End If
                Ret = SaveEMPPnO_DocumentRequest(0, ddlDocType.SelectedValue, txtAddressTo.Text, txtEmpNotes.Text, txtAddNotes.Text, h_Emp_No.Value,
                                                    h_emp_bsu.Value, RecId)
            End If
            If Ret = "0" Then
                ViewState("datamode") = "add"

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                clear_All()
            End If
            lblError.Text = getErrorMessage(Ret)
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message


        End Try

    End Sub
    Public Sub GetSelectedPnODocumentRequestDetails(ByVal reqstid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            'str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "'"     'V1.1 comment
            str_Sql = " exec DAX.GetEmployeePnO_DocumentsRequestList '" & Session("SbSUID") & "','" & h_Emp_No.Value & "','" & reqstid & "'" ' & Session("SbSUID") & "','" & Session("sUsr_name") & "'"    'V1.1
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            trDocnum.Visible = True
            lblDocnum.Text = ds.Tables(0).Rows(0).Item("EPnO_AX_RECID")
            ddlDocType.SelectedValue = ds.Tables(0).Rows(0).Item("EPnO_PnO_DOC_ID")
            txtAddNotes.Text = ds.Tables(0).Rows(0).Item("EPnO_ADDRESS_NOTES")
            txtAddressTo.Text = ds.Tables(0).Rows(0).Item("EPnO_ADDRESS_TO")
            txtEmpNotes.Text = ds.Tables(0).Rows(0).Item("EPnO_EMP_NOTES")
            txtHRNotes.Text = ds.Tables(0).Rows(0).Item("EPnO_HR_NOTES")
            lblDocStatus.Text = ds.Tables(0).Rows(0).Item("PnOStatus_Descr")
            h_DaxRecId.Value = ds.Tables(0).Rows(0).Item("EPnO_AX_RECID")
            trHRText.Visible = True
            ddlDocType.Enabled = False
            'trstatus.Visible = True
            h_docStatusID.Value = ds.Tables(0).Rows(0).Item("PnOStatus_ID")
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Function SaveDocumentDetailstoAX() As String
        Try
            Dim AleDocDetails As New PnODocumentService.AxdGMS_HRDocuRequest
            Dim tmp1, tmp2 As String
            tmp1 = ""
            tmp2 = ""

            ' Dim BankFindRequest As PnODocumentService.ALE_EmpBankDetailsServiceFindRequest

            Dim client As New PnODocumentService.GMS_HRDocuRequestServiceClient
            'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\swapna.tv"
            'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"
            client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
            client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString

            Dim context As New PnODocumentService.CallContext
            context.Company = h_emp_bsu_code.Value

            Dim AxdEntity_ALE_HRDocumentRequest(0) As PnODocumentService.AxdEntity_ALE_HRDocumentRequest
            AxdEntity_ALE_HRDocumentRequest(0) = New PnODocumentService.AxdEntity_ALE_HRDocumentRequest
            ' Dim RespAleBankDetails As New PnODocumentService.AxdEntity_HcmWorkerBankAccount

            Dim Criteria As New PnODocumentService.QueryCriteria
            Dim CriteriaElements(0) As PnODocumentService.CriteriaElement
            Dim CriteriaElement As New PnODocumentService.CriteriaElement

            CriteriaElement.DataSourceName = "ALE_HRDocumentRequest"
            CriteriaElement.FieldName = "DocumentRequest"
            CriteriaElement.Operator = PnODocumentService.Operator.Equal
            CriteriaElement.Value1 = h_DaxRecId.Value ' Request.QueryString("Recid")
            'CriteriaElement.Value2 = "0"
            CriteriaElements(0) = CriteriaElement
            Criteria.CriteriaElement = CriteriaElements
            AleDocDetails = client.find(context, Criteria)
            ' Dim RespAleBankDetails = AleBankDetails.HcmWorkerBankAccount

            'Dim RecordID As String = ""

            ' Dim AleBankDetails As New PnODocumentService.AxdALE_EmpBankDetails
            If Not AleDocDetails.ALE_HRDocumentRequest Is Nothing Then 'AndAlso Not RespAleBankDetails Is Nothing Then

                AleDocDetails.ALE_HRDocumentRequest(0).EmployeeNotes = txtEmpNotes.Text ' rcbBank.SelectedValue   ' "Test"
                AleDocDetails.ALE_HRDocumentRequest(0).AddressToNotes = txtAddNotes.Text
                AleDocDetails.ALE_HRDocumentRequest(0).AddressTo = txtAddressTo.Text
                AleDocDetails.ALE_HRDocumentRequest(0).class = "entity"
                AleDocDetails.ALE_HRDocumentRequest(0).action = PnODocumentService.AxdEnum_AxdEntityAction.update

                ' As discussed with Puneet and Robbie send blank data if CASH mode"


                Dim ent(0) As PnODocumentService.EntityKey
                Dim KeyData(0) As PnODocumentService.KeyField
                ent(0) = New PnODocumentService.EntityKey
                KeyData(0) = New PnODocumentService.KeyField
                KeyData(0).Field = "DocumentRequest"
                KeyData(0).Value = h_DaxRecId.Value
                ent(0).KeyData = KeyData


                client.update(context, ent, AleDocDetails)
                tmp2 = h_DaxRecId.Value
            Else

                Dim ent(1) As PnODocumentService.EntityKey


                AxdEntity_ALE_HRDocumentRequest(0).EmployeeNotes = txtEmpNotes.Text ' rcbBank.SelectedValue   ' "Test"
                AxdEntity_ALE_HRDocumentRequest(0).AddressToNotes = txtAddNotes.Text
                AxdEntity_ALE_HRDocumentRequest(0).AddressTo = txtAddressTo.Text
                AxdEntity_ALE_HRDocumentRequest(0).EmployeeId = h_empnumValue.Value
                AxdEntity_ALE_HRDocumentRequest(0).DateOfRequestSpecified = False
                AxdEntity_ALE_HRDocumentRequest(0).class = "entity"
                AxdEntity_ALE_HRDocumentRequest(0).action = PnODocumentService.AxdEnum_AxdEntityAction.create
                'AxdEntity_ALE_HRDocumentRequest(0).Status = "0"
                AxdEntity_ALE_HRDocumentRequest(0).CreatedFromPhoenix = PnODocumentService.AxdEnum_NoYes.Yes
                AxdEntity_ALE_HRDocumentRequest(0).CreatedFromPhoenixSpecified = True
                AxdEntity_ALE_HRDocumentRequest(0).DocumentID = GetDAXDocumentTypeMappingID() ' ddlDocType.SelectedItem.Text


                AleDocDetails.ALE_HRDocumentRequest = AxdEntity_ALE_HRDocumentRequest


                ent = client.create(context, AleDocDetails)
                If ent.Length > 0 Then

                    tmp1 = ent(0).KeyData(0).Field
                    tmp2 = ent(0).KeyData(0).Value
                End If

                'lblError.Text = "Transferred Successfully"
            End If
            Return tmp2
        Catch ex As Exception
            lblError.Text = ex.Message
            ' ShowInfoLog(ex.Message, "E")
            Return 1000
        End Try
    End Function
    Public Sub GetLoggedInEmployeeDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT " _
         & " EM.EMP_ID, ISNULL(EM.EMP_FNAME,'')+' '+" _
         & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
         & " AS EMP_NAME,EMPNO,EMP_BSU_ID ,bsu_shortname FROM  EMPLOYEE_M EM INNER JOIN USERS_M" _
         & " ON EM.EMP_ID=USERS_M.USR_EMP_ID inner join businessunit_m on bsu_id=emp_bsu_id " _
         & " WHERE USERS_M.USR_NAME ='" & Session("sUsr_name") & "' and em.emp_bsu_id ='" & Session("sBsuid") & "'"
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then

            h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
            h_emp_bsu.Value = ds.Tables(0).Rows(0)("emp_bsu_id")
            h_empnumValue.Value = ds.Tables(0).Rows(0)("EMPNO")
            h_emp_bsu_code.Value = ds.Tables(0).Rows(0)("bsu_shortname")
        End If

    End Sub
    Public Function GetDAXDocumentTypeMappingID() As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "exec getDAXDocumentMappingID " & ddlDocType.SelectedValue
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("PnO_Doc_AXID")
        End If

    End Function
    Public Sub clear_All()
        h_Emp_No.Value = ""
        h_emp_bsu.Value = ""
        txtAddNotes.Text = ""
        txtAddressTo.Text = ""
        txtEmpNotes.Text = ""
        txtHRNotes.Text = ""
        lblDocStatus.Text = ""
        ' trstatus.Visible = False
        trHRText.Visible = False
        trDocnum.Visible = False
        lblDocnum.Text = ""
        ddlDocType.SelectedIndex = 0
        h_emp_bsu_code.Value = ""
        h_DaxRecId.Value = ""
        h_emp_bsu_code.Value = ""
        h_empnumValue.Value = ""
        h_docStatusID.Value = ""
    End Sub
    Public Shared Function SaveEMPPnO_DocumentRequest(ByVal p_EPnO_REQUEST_ID As Integer, ByVal p_EPnO_PnO_DOC_ID As Integer, ByVal p_EPnO_ADDRESS_TO As String, ByVal p_EPnO_EMP_NOTES As String, _
   ByVal p_EPnO_ADDRESS_NOTES As String, ByVal p_EPnO_EMP_ID As Integer, _
     ByVal p_EPnO_BSU_ID As String, p_EPnO_DAX_RecId As String) As String



        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        'Dim objConn As New SqlConnection(str_conn) '



        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()

        Dim str_err As String = "1000"
        Dim lstrNewDocNo As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim SqlCmd As New SqlCommand("DAX.SaveEMP_PnODocumentRequests", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure

            Dim pParms(8) As SqlClient.SqlParameter 'V1.2 ,V1.3

            SqlCmd.Parameters.AddWithValue("@EPnO_REQUEST_ID", p_EPnO_REQUEST_ID)
            SqlCmd.Parameters.AddWithValue("@EPnO_PnO_DOC_ID", p_EPnO_PnO_DOC_ID)
            SqlCmd.Parameters.AddWithValue("@EPnO_ADDRESS_TO", p_EPnO_ADDRESS_TO)
            SqlCmd.Parameters.AddWithValue("@EPnO_EMP_NOTES", p_EPnO_EMP_NOTES)
            'SqlCmd.Parameters.AddWithValue("@EPnO_HR_NOTES", p_EPnO_HR_NOTES)
            SqlCmd.Parameters.AddWithValue("@EPnO_ADDRESS_NOTES", p_EPnO_ADDRESS_NOTES)
            SqlCmd.Parameters.AddWithValue("@EPnO_EMP_ID", p_EPnO_EMP_ID)
            SqlCmd.Parameters.AddWithValue("@EPnO_BSU_ID", p_EPnO_BSU_ID)
            SqlCmd.Parameters.AddWithValue("@EPnO_AX_RECID", p_EPnO_DAX_RecId)

            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            str_err = CInt(SqlCmd.Parameters("@ReturnValue").Value)

            'lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)

            SqlCmd.Parameters.Clear()
            If str_err = "0" Then

                stTrans.Commit()

                Return "0"
            End If

            'SaveEMPPnO_DocumentRequest = pParms(19).Value.ToString & "||" & pParms(18).Value.ToString
        Catch ex As Exception

            stTrans.Rollback()
            Errorlog(ex.Message)
            Return "1000"
        Finally
            objConn.Close() 'Finally, close the connection

        End Try
        Return "1000"
    End Function

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click

        If h_docStatusID.Value = 3 Then
            lblError.Text = getErrorMessage("928")
        Else
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
