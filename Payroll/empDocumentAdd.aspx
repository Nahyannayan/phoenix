<%@ Page Language="VB" MasterPageFile="~/mainMasterPageSS.master" AutoEventWireup="false" CodeFile="empDocumentAdd.aspx.vb" Inherits="Payroll_empDocumentAdd" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPageSS.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
        
    <script language="javascript" type="text/javascript">

     function getDate(val, allowfuturedt) {
        var sFeatures;
        sFeatures = "dialogWidth: 225px; ";
        sFeatures += "dialogHeight: 252px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: no; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var nofuture = "calendar.aspx?nofuture=no";
        var SelDate;
        SelDate = document.getElementById('<%=txtExpirydate.ClientID %>').value;
        SelDate = replaceSubstring(SelDate, "/", "-");
        switch (allowfuturedt) {
            case 0:
                {
                    result = window.showModalDialog(nofuture + "&dt=" + SelDate , "", sFeatures);
                    break;
                }
            case 1:
                {
                    result = window.showModalDialog("calendar.aspx?dt=" + SelDate , "", sFeatures);
                    break;
                }
        }
        if (result == '' || result == undefined) {
            return false;
        }
        if (val == 0) {
        }
        else if (val == 1) {
        document.getElementById('<%=txtExpirydate.ClientID %>').value = result;
                    }
        return false;
    }
    
    
    function UploadPhoto() {
        //        document.forms[0].Submit();
        var filepath = document.getElementById('<%=UploadDocPhoto.ClientID %>').value;
        if ((filepath != '') && (document.getElementById('<%=h_DocumentPath.ClientID %>').value != filepath)) {
            document.getElementById('<%=h_DocumentPath.ClientID %>').value = filepath;
//            document.forms[0].submit();
        }
    }
    </script>
 <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" >
    <tr>
    <td  colspan="4" align="left">
        <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
        <asp:ValidationSummary id="ValidationSummary1" runat="server" ValidationGroup="CLIENTERROR">
        </asp:ValidationSummary>
    </td>    
    </tr>
 </table>
 <table border="1" cellpadding="5" style="border-collapse:collapse" 
        borderColor="#1b80b6"  cellspacing="0" width="95%" align="center" >            
            <tr class="subheader_img">
                <td colspan="3" style="height: 19px" align="left">
                    Add/Edit Document</td>                
                <td style="height: 19px" align="left">
                    &nbsp;</td>                
            </tr>
     <tr>
         <td align="left" class="matters" valign="top" width="20%">
             Document Owner&nbsp; Name</td>
         <td align="left" class="matters" valign="top" style="width: 488px">
             :</td>
         <td align="left" width="50%">
             <asp:TextBox ID="txtName" runat="server" Width="303px" ReadOnly="True"></asp:TextBox>
             </td>
         <td align="center" rowspan="5" width="30%">
                            <asp:ImageMap ID="imgDocument" runat="server" Height="198px" >
                            </asp:ImageMap>
                                 </td>
     </tr>
            <tr  runat="server" id="tr_issue">
         <td align="left" class="matters" valign="middle">
             Document Type</td>
         <td align="left" class="matters" valign="middle" style="width: 488px">
             :</td>
                <td align="left" colspan="1" class="matters" valign="middle" >
                        <asp:DropDownList ID="ddlDocumentType" runat="server" Width="128px" AutoPostBack="True">
                        </asp:DropDownList>
                 <asp:CheckBox ID="chkActive" runat="server" 
                     Text="Active" /> </td> 
     </tr>
            <tr  runat="server" id="tr_issue1">
         <td align="left" class="matters" valign="middle">
             Narration</td>
         <td align="left" class="matters" valign="middle" style="height: 13px; width: 488px">
             &nbsp;</td>
                <td align="left" colspan="1" class="matters" valign="middle" >
                <asp:TextBox ID="txtRemarks" runat="server" Height="54px" TextMode="MultiLine" 
                    Width="295px" EnableTheming="False"></asp:TextBox></td> 
     </tr>
            <tr  runat="server" id="tr_issue2">
         <td align="left" class="matters" valign="middle">
             Expiry Date</td>
         <td align="left" class="matters" valign="middle" style="height: 13px; width: 488px">
             :</td>
                <td align="left" colspan="1" class="matters" valign="middle" >
                        <asp:TextBox ID="txtExpirydate" runat="server" Width="122px"></asp:TextBox>
                        <asp:ImageButton ID="imgExpiryDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="getDate(1,1);return false;" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" 
                        runat="server" ControlToValidate="txtExpiryDate"
                            Display="Dynamic" ErrorMessage="Enter the Expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtExpiryDate"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Expiry Date entered is not a valid date"
                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>&nbsp;
                 <asp:CheckBox ID="chkReminder" runat="server" 
                     Text="Reminder Required ?" /> </td> 
     </tr>
     <tr>
                <td class="matters" align="left" valign="top">
                    Upload File</td>
                <td class="matters" align="left" valign="top" style="width: 488px">
                    :</td>
         <td align="left" class="matters" valign="top">
                        <asp:FileUpload ID="UploadDocPhoto" runat="server" Width="325px" 
                            
                        
                            
                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' /></td>
     </tr>
            <tr>
                <td class="matters" colspan="4">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CLIENTERROR" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
        </table>
    <asp:HiddenField ID="h_Emp_ID" runat="server" />
    <asp:HiddenField ID="h_ESDType" runat="server" />
    <asp:HiddenField ID="h_DocType" runat="server" />
     <asp:HiddenField ID="hdnEDDID" runat="server" />
     <asp:HiddenField ID="hdnEDCID" runat="server" />
      <asp:HiddenField ID="AllowEdit" runat="server" />
    <asp:HiddenField ID="h_DocumentPath" runat="server" />
    </asp:Content>
