Imports System.Data
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Diagnostics

Partial Class empBulkSalaryRevisionUpload
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Private Enum DatabaseRecordAction As Integer
        None
        Add
        Edit
        Delete
    End Enum
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvSalRevisionBulk.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvSalRevisionBulk.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Gridbind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnLoad)

        Me.Page.Form.Enctype = "multipart/form-data"

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            txtRevisionDate.Attributes.Add("onBlur", "checkdate(this)")
            h_Grid.Value = "top"
            lblError.Text = ""
            Session("EMP_SEL_COND") = " AND EMP_BSU_ID = '" & Session("Sbsuid") & "'"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'If USR_NAME = "" Or CurBsUnit = "" Or _
            'ViewState("MainMnu_code") <> OASISConstants.MenuEMPSALARYBULKREVISION Then
            '    If Not Request.UrlReferrer Is Nothing Then
            '        Response.Redirect(Request.UrlReferrer.ToString())
            '    Else
            '        Response.Redirect("~\noAccess.aspx")
            '    End If
            'Else
            '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, _
            '    ViewState("MainMnu_code"))
            '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
            '    ViewState("menu_rights"), ViewState("datamode"))
            '    gvSalRevisionBulk.Attributes.Add("bordercolor", "#1b80b6")
            '    'Gridbind()
            'End If 

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(IIf(Request.QueryString("MainMnu_code") IsNot Nothing, Request.QueryString("MainMnu_code").Replace(" ", "+"), String.Empty))
            Page.Title = OASISConstants.Gemstitle
            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then
                ViewState("Eid") = Encr_decrData.Decrypt(IIf(Request.QueryString("Eid") IsNot Nothing, Request.QueryString("Eid").Replace(" ", "+"), ""))
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            txtEmpNo.Attributes.Add("ReadOnly", "ReadOnly")
            txtSalGrade.Attributes.Add("ReadOnly", "ReadOnly")
            BindENR()
            tblSalDetails.Visible = False

        End If

        If Not IsPostBack Then
            'If Not Me.optManual.Checked Or Not Me.optUpload.Checked Then
            '    Me.optManual.Checked = True
            ''End If
            Me.optUpload.Checked = True
            Me.tblUpload.Visible = False
            Me.Table1.Visible = True

            Me.tblUpload.Visible = True
            Me.Table1.Visible = False
            Me.tblSalDetails.Visible = False
        End If

    End Sub

    Private Sub Gridbind(Optional ByVal p_selected_id As Integer = -1)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrEmpMONTH As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrYEAR As String = String.Empty
        Dim lstrTYPE As String = String.Empty
        Dim lstrAMOUNT As String = String.Empty
        Dim lstrREMARKS As String = String.Empty

        Dim lstrFiltMONTH As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltYEAR As String = String.Empty
        Dim lstrFiltTYPE As String = String.Empty
        Dim lstrFiltAMOUNT As String = String.Empty
        Dim lstrFiltREMARKS As String = String.Empty


        Dim txtSearch As New TextBox
        'If gvSalRevisionBulk.Rows.Count > 0 Then
        '    ' --- Initialize The Variables

        '    larrSearchOpr = h_selected_menu_1.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)

        '    '   --- FILTER CONDITIONS ---
        '    '   -- 1   refno
        '    larrSearchOpr = h_selected_menu_1.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtMONTH")
        '    lstrEmpMONTH = Trim(txtSearch.Text)
        '    If (lstrEmpMONTH <> "") Then lstrFiltMONTH = SetCondn(lstrOpr, "EMP_MONTH", lstrEmpMONTH)

        '    '   -- 1  docno
        '    larrSearchOpr = h_Selected_menu_2.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpNo")
        '    lstrEMPNo = Trim(txtSearch.Text)
        '    If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EDD_EMP_ID", lstrEMPNo)

        '    '   -- 2  DocDate
        '    larrSearchOpr = h_Selected_menu_5.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpName")
        '    lstrEmpName = txtSearch.Text
        '    If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)

        '    '   -- 5  Narration
        '    larrSearchOpr = h_Selected_menu_3.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtYEAR")
        '    lstrYEAR = txtSearch.Text
        '    If (lstrYEAR <> "") Then lstrFiltYEAR = SetCondn(lstrOpr, "EDD_PAYYEAR", lstrYEAR)

        '    '   -- 5  COLLUN
        '    larrSearchOpr = h_Selected_menu_6.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)

        'End If

        Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next

        str_Sql = "SELECT EMPLOYEE_M.EMP_ID, EMPLOYEE_M.EMPNO, EMPLOYEE_M.EMP_SALUTE + ' ' " & _
        "+ EMPLOYEE_M.EMP_FNAME + ' ' + EMPLOYEE_M.EMP_MNAME + ' ' + EMPLOYEE_M.EMP_LNAME AS EMPNAME, " & _
        " EMPSCALES_GRADES_M.SGD_DESCR as CUR_SAL_SCALE FROM EMPLOYEE_M LEFT OUTER JOIN EMPSCALES_GRADES_M " & _
        " ON EMPLOYEE_M.EMP_SGD_ID = EMPSCALES_GRADES_M.SGD_ID WHERE EMP_ID in (" & condition & ")"

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT," & _
        '" EMP_PASSPORT, EMP_STATUS_DESCR, EMP_DES_DESCR FROM vw_OSO_EMPLOYEEMASTER" & _
        '" WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' " _
        '& str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDesignation & lstrFiltPassportNo

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        'PopulateSalaryData(condition)
        gvSalRevisionBulk.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvSalRevisionBulk.DataBind()
            Dim columnCount As Integer = gvSalRevisionBulk.Rows(0).Cells.Count

            gvSalRevisionBulk.Rows(0).Cells.Clear()
            gvSalRevisionBulk.Rows(0).Cells.Add(New TableCell)
            gvSalRevisionBulk.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSalRevisionBulk.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSalRevisionBulk.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvSalRevisionBulk.DataBind()
            UpdateSalaryAmounts()
        End If

        'gvJournal.DataBind()
        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtMONTH")
        'txtSearch.Text = lstrEmpMONTH

        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpNo")
        'txtSearch.Text = lstrEMPNo

        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpName")
        'txtSearch.Text = lstrEmpName

        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtYEAR")
        'txtSearch.Text = lstrYEAR

        gvSalRevisionBulk.SelectedIndex = p_selected_id
    End Sub

    Private Sub ClearNewAmounts()
        If Session("EMPSALDETAILS") Is Nothing Then Return
        Dim dttab As DataTable = Session("EMPSALDETAILS")
        Dim drSalDet As DataRow
        If dttab.Rows.Count > 0 Then
            For i As Integer = 0 To dttab.Rows.Count - 1
                drSalDet = dttab.Rows(i)
                drSalDet("Amount_NEW") = drSalDet("Amount")
                drSalDet("Amount_ELIGIBILITY_NEW") = drSalDet("Amount_ELIGIBILITY")
            Next
            Session("EMPSALDETAILS") = dttab
            Gridbind()
        End If

    End Sub

    Private Sub UpdateSalaryAmounts()
        Dim dtTempDtl As DataTable = Session("EMPSALDETAILS")
        Dim dRows() As DataRow
        For iRowCount As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
            Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(iRowCount)
            Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
            Dim grossSal, grossSalNew As Double
            grossSal = 0
            grossSalNew = 0
            If lblEMPID Is Nothing Then
                Continue For
            Else
                dRows = dtTempDtl.Select("EMP_ID = '" & lblEMPID.Text & "'")
                If dRows.Length > 0 Then
                    For iColCount As Integer = 0 To dRows.Length - 1
                        Dim amt As Double = GetVal(dRows(iColCount)("Amount"), SqlDbType.Decimal)
                        If Not dRows(iColCount)("bMonthly") Then
                            amt = amt / 12
                        End If
                        grossSal += amt
                        amt = GetVal(dRows(iColCount)("Amount_NEW"), SqlDbType.Decimal)
                        If Not dRows(iColCount)("bMonthly") Then
                            amt = amt / 12
                        End If
                        grossSalNew += amt
                    Next
                End If
                Dim lblEMPAMOUNT As Label = dgViewRow.FindControl("lblCurrSalAmt")
                If lblEMPAMOUNT Is Nothing Then
                    Continue For
                Else
                    lblEMPAMOUNT.Text = AccountFunctions.Round(grossSal)
                End If
                Dim lblEMPAMOUNTNEW As Label = dgViewRow.FindControl("lblRevSalAmt")
                If lblEMPAMOUNTNEW Is Nothing Then
                    Continue For
                Else
                    lblEMPAMOUNTNEW.Text = AccountFunctions.Round(grossSalNew)
                End If
            End If
        Next
    End Sub

    Private Sub PopulateSalaryData(ByVal condition As String)
        Dim str_Sql As String = "SELECT EMPSALARYHS_s.GUID,ERN_ORDER, ESH_ID,ESH_EMP_ID, ESH_BSU_ID, ESH_ERN_ID as ENR_ID," & _
        " ESH_bMonthly, ESL_PAYTERM, ESH_PAYMONTH, ESH_PAYYEAR, ESH_bPAID, EMPSALCOMPO_M.ERN_DESCR, " & _
        " ESH_AMOUNT as AMOUNT,ESH_ELIGIBILITY , ESH_FROMDT, ESH_TODT, ESH_CUR_ID, ESH_PAY_CUR_ID FROM EMPSALARYHS_s " & _
        " LEFT OUTER JOIN EMPSALCOMPO_M ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        " WHERE ESH_EMP_ID in (" & condition & ") AND ESH_TODT is null AND ESH_BSU_ID='" & Session("sBSUID") & "'" & _
        " order by ERN_ORDER "
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand = New SqlCommand(str_Sql, conn)
        cmd.CommandType = CommandType.Text

        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Session("EMPSALDETAILS") = CreateSalaryRevDetailsTable()
        Dim dTemptable As DataTable = Session("EMPSALDETAILS")

        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("EMP_ID") = dr("ESH_EMP_ID")
            ldrTempNew.Item("ENR_ID") = dr("ENR_ID")
            ldrTempNew.Item("ERN_ORDER") = dr("ERN_ORDER")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = dr("ESH_bMonthly")
            ldrTempNew.Item("PAYTERM") = dr("ESL_PAYTERM")
            ldrTempNew.Item("Amount") = dr("AMOUNT")
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("ESH_ELIGIBILITY")
            ldrTempNew.Item("Amount_NEW") = dr("AMOUNT")
            ldrTempNew.Item("Amount_ELIGIBILITY_NEW") = dr("ESH_ELIGIBILITY")
            ldrTempNew.Item("Status") = "INSERTED"
            dTemptable.Rows.Add(ldrTempNew)
        End While

        Session("EMPSALDETAILS") = dTemptable

    End Sub

    Private Sub GridBindAddSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("ADDEDEMPSALDETAILS") Is Nothing Then
            gvAddSalaryDetails.DataSource = Nothing
            gvAddSalaryDetails.DataBind()
            Return
        End If
        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        Dim grossSal As Double
        dtTempDtl = CreateAddDetailsTable()
        If Session("ADDEDEMPSALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("ADDEDEMPSALDETAILS").Rows.Count - 1
                If (Session("ADDEDEMPSALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("ADDEDEMPSALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                            If Not Session("ADDEDEMPSALDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        txtGrossSalary.Text = grossSal
        gvAddSalaryDetails.DataSource = dtTempDtl
        gvAddSalaryDetails.DataBind()
    End Sub

    Private Sub BindENR()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 1 ORDER BY ERN_ORDER"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataBind()
    End Sub

    Protected Sub btnSalAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalAdd.Click

        If Session("ADDEDEMPSALDETAILS") Is Nothing Then
            Session("ADDEDEMPSALDETAILS") = CreateAddDetailsTable()
        End If
        If Not IsNumeric(txtSalAmount.Text) Then
            lblError.Text = "Amount should be valid"
            Exit Sub
        End If

        Dim status As String = String.Empty
        Dim dtSalDetails As DataTable = Session("ADDEDEMPSALDETAILS")
        Dim i As Integer
        Dim gvRow As GridViewRow = gvEmpSalary.SelectedRow()

        For i = 0 To dtSalDetails.Rows.Count - 1
            If Session("EMPREVEditID") Is Nothing Or _
            Session("EMPREVEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                If dtSalDetails.Rows(i)("ENR_ID") = ddSalEarnCode.SelectedItem.Value Then
                    lblError.Text = "Cannot add transaction details.The Earn Code details are repeating."
                    GridBindAddSalaryDetails()
                    Exit Sub
                End If
            End If
        Next
        If btnSalAdd.Text = "Add" Then
            Dim newDR As DataRow = dtSalDetails.NewRow()
            newDR("UniqueID") = dtSalDetails.Rows.Count()
            newDR("ENR_ID") = ddSalEarnCode.SelectedValue
            newDR("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
            newDR("Amount") = txtSalAmount.Text
            newDR("bMonthly") = chkSalPayMonthly.Checked
            newDR("Status") = "Insert"
            status = "Insert"
            dtSalDetails.Rows.Add(newDR)
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), CDbl(txtSalAmount.Text))
        ElseIf btnSalAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPREVEditID")
            For iIndex = 0 To dtSalDetails.Rows.Count - 1
                If str_Search = dtSalDetails.Rows(iIndex)("UniqueID") And dtSalDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtSalDetails.Rows(iIndex)("ENR_ID") = ddSalEarnCode.SelectedValue
                    dtSalDetails.Rows(iIndex)("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
                    dtSalDetails.Rows(iIndex)("Amount") = CDbl(txtSalAmount.Text)
                    dtSalDetails.Rows(iIndex)("bMonthly") = chkSalPayMonthly.Checked
                    status = "Edit/Update"
                    Exit For
                End If
            Next
            btnSalAdd.Text = "Add"
            ddSalEarnCode.Enabled = True
            lblError.Text = ""
        End If
        UpdateSalaryDetails()
        ClearEMPSALDETAILS()
        Session("ADDEDEMPSALDETAILS") = dtSalDetails
        GridBindAddSalaryDetails()
        UpdateSalaryAmounts()
    End Sub

    Private Sub UpdateSalaryScale()
        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryRevDetailsTable()
        End If
        If Session("ADDEDEMPSALDETAILS").Rows.Count < 0 Then Return

        Dim strArr(1) As String
        Dim dTemptable As DataTable = Session("EMPSALDETAILS")
        Dim dc As New DataColumn("Amount_NEW")
        dTemptable.Columns.Remove("Amount_NEW")
        dTemptable.Columns.Add(dc)
        dc = New DataColumn("Amount_ELIGIBILITY_NEW")
        dTemptable.Columns.Remove("Amount_ELIGIBILITY_NEW")
        dTemptable.Columns.Add(dc)

        For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
            Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
            Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
            Dim EMP_ID As String
            If lblEMPID Is Nothing Then
                Exit For
            Else
                EMP_ID = lblEMPID.Text
            End If
            strArr(0) = EMP_ID
            Dim dtable As DataTable = Session("ADDEDEMPSALDETAILS")
            For iCount As Integer = 0 To dtable.Rows.Count - 1
                strArr(1) = dtable.Rows(iCount)("ENR_ID")
                Dim drSalDet As DataRow = dTemptable.Rows.Find(strArr)
                If Not drSalDet Is Nothing Then
                    drSalDet("Amount_NEW") = CDbl(dtable.Rows(iCount)("Amount"))
                    drSalDet("Amount_ELIGIBILITY_NEW") = CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY"))
                    drSalDet("bMonthly") = 1
                    'If drSalDet("bMonthly") Then
                    '    drSalDet("Amount_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount"), CDbl(dtable.Rows(iCount)("Amount")) / 12)
                    '    drSalDet("Amount_ELIGIBILITY_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount_ELIGIBILITY"), CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY")) / 12)
                    'Else
                    '    drSalDet("Amount_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount") * 12, CDbl(dtable.Rows(iCount)("Amount")))
                    '    drSalDet("Amount_ELIGIBILITY_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount_ELIGIBILITY") * 12, CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY")))
                    'End If
                Else
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dTemptable.NewRow
                    ldrTempNew.Item("UniqueID") = GetNextESHID()
                    ldrTempNew.Item("EMP_ID") = EMP_ID
                    ldrTempNew.Item("ENR_ID") = dtable.Rows(iCount)("ENR_ID")
                    ldrTempNew.Item("ERN_DESCR") = dtable.Rows(iCount)("ERN_DESCR")
                    ldrTempNew.Item("bMonthly") = 1
                    ldrTempNew.Item("PAYTERM") = 0
                    ldrTempNew.Item("Amount") = 0
                    ldrTempNew.Item("Amount_ELIGIBILITY") = 0
                    ldrTempNew.Item("Amount_NEW") = CDbl(dtable.Rows(iCount)("Amount"))
                    ldrTempNew.Item("Amount_ELIGIBILITY_NEW") = CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY"))
                    ldrTempNew.Item("Status") = "INSERTED"
                    dTemptable.Rows.Add(ldrTempNew)
                End If
            Next

        Next
        Session("EMPSALDETAILS") = dTemptable
    End Sub

    Private Sub UpdateSalaryDetails()

        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryRevDetailsTable()
        End If
        Dim strArr(1) As String
        Dim dTemptable As DataTable = Session("EMPSALDETAILS")
        For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
            Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
            Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
            Dim EMP_ID As String
            If lblEMPID Is Nothing Then
                Exit For
            Else
                EMP_ID = lblEMPID.Text
            End If
            strArr(0) = EMP_ID
            strArr(1) = ddSalEarnCode.SelectedValue
            Dim drSalDet As DataRow = dTemptable.Rows.Find(strArr)
            If Not drSalDet Is Nothing Then
                Dim amt As Double
                If drSalDet("bMonthly") Then
                    amt = IIf(chkSalPayMonthly.Checked, CDbl(txtSalAmount.Text), CDbl(txtSalAmount.Text) / 12)
                Else
                    amt = IIf(chkSalPayMonthly.Checked, CDbl(txtSalAmount.Text) * 12, CDbl(txtSalAmount.Text))
                End If
                drSalDet("Amount_NEW") = CDbl(GetVal(drSalDet("Amount"), SqlDbType.Decimal)) + amt
                drSalDet("Amount_ELIGIBILITY_NEW") = CDbl(GetVal(drSalDet("Amount_ELIGIBILITY"), SqlDbType.Decimal)) + amt
            Else
                Dim ldrTempNew As DataRow
                ldrTempNew = dTemptable.NewRow
                ldrTempNew.Item("UniqueID") = GetNextESHID()
                ldrTempNew.Item("EMP_ID") = EMP_ID
                ldrTempNew.Item("ENR_ID") = ddSalEarnCode.SelectedValue
                ldrTempNew.Item("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
                ldrTempNew.Item("bMonthly") = chkSalPayMonthly.Checked
                ldrTempNew.Item("PAYTERM") = 0
                ldrTempNew.Item("Amount") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Amount_ELIGIBILITY") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Amount_NEW") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Amount_ELIGIBILITY_NEW") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Status") = "INSERTED"
                dTemptable.Rows.Add(ldrTempNew)
            End If
        Next
        Session("EMPSALDETAILS") = dTemptable
    End Sub

    Private Sub ClearEMPSALDETAILS()
        ddSalEarnCode.SelectedIndex = 0
        txtSalAmount.Text = ""
        btnSalAdd.Text = "Add"
        lblError.Text = ""
        ddSalEarnCode.SelectedIndex = 0
        chkSalPayMonthly.Checked = True
        Session.Remove("EMPREVEditID")

    End Sub

    Function CreateSalaryRevDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cEMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim cENR_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))
            Dim cENR_ORDER As New DataColumn("ERN_ORDER", System.Type.GetType("System.Int32"))

            Dim cFROMDATE As New DataColumn("FROM_DATE", System.Type.GetType("System.DateTime"))
            Dim cPAY_CUR_ID As New DataColumn("PAY_CUR_ID", System.Type.GetType("System.String"))

            Dim cPay_Year As New DataColumn("PAY_YEAR", System.Type.GetType("System.Int16"))
            Dim cPay_Month As New DataColumn("PAY_MONTH", System.Type.GetType("System.Int32"))

            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cPAYTERM As New DataColumn("PAYTERM", System.Type.GetType("System.Int32"))
            Dim cbPAID As New DataColumn("bPAID", System.Type.GetType("System.Boolean"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY As New DataColumn("Amount_ELIGIBILITY", System.Type.GetType("System.Decimal"))
            Dim cAmount_NEW As New DataColumn("Amount_NEW", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY_NEW As New DataColumn("Amount_ELIGIBILITY_NEW", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEMP_ID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cENR_ORDER)
            dtDt.Columns.Add(cENR_DESCR)
            dtDt.Columns.Add(cFROMDATE)
            dtDt.Columns.Add(cPay_Month)
            dtDt.Columns.Add(cPay_Year)

            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cPAYTERM)
            dtDt.Columns.Add(cbPAID)

            dtDt.Columns.Add(cPAY_CUR_ID)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cAmount_ELIGIBILITY)
            dtDt.Columns.Add(cAmount_NEW)
            dtDt.Columns.Add(cAmount_ELIGIBILITY_NEW)
            dtDt.Columns.Add(cStatus)

            Dim dc(1) As DataColumn
            dc(0) = dtDt.Columns("EMP_ID")
            dc(1) = dtDt.Columns("ENR_ID")
            dtDt.PrimaryKey = dc

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Function CreateAddDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cEMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cENR_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))
            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEMP_ID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cENR_DESCR)
            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub lnkSalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String = "SELECT * FROM EMPSALARYHS_s " & _
        '" WHERE GUID ='" & lblRowId.Text & "'"
        'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim dt As DataTable = Session("ADDEDEMPSALDETAILS")
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("UniqueID") = lblRowId.Text Then
                ddSalEarnCode.SelectedValue = dt.Rows(i)("ENR_ID")
                Session("EMPREVEditID") = lblRowId.Text
                ddSalEarnCode.Enabled = False
                txtSalAmount.Text = AccountFunctions.Round(dt.Rows(i)("AMOUNT"))
                chkSalPayMonthly.Checked = dt.Rows(i)("bMonthly")
                btnSalAdd.Text = "Save"
            End If
        Next
    End Sub

    Protected Sub btnSalRevSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalRevSave.Click
        If CDate(txtRevisionDate.Text).Day <> 1 Then
            lblError.Text = "Effective date should be the first day of the month"
            Return
        End If
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim transaction As SqlTransaction = conn.BeginTransaction("SampleTransaction")
            'If CheckPayMonth() Then
            Dim errorNo As Integer
            errorNo = SaveEMPAddToSalaryDetails(conn, transaction)

            'If radSalScale.Checked Then
            '    errorNo = SaveEMPSalaryDetails(conn, transaction)
            'ElseIf radAddAmount.Checked Then
            '    errorNo = SaveEMPAddToSalaryDetails(conn, transaction)
            'End If
            'If errorNo = 0 Then errorNo = SaveEMPSalarySchedule(conn, transaction)
            If errorNo = 0 Then
                If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), hf_EMP_IDs.Value, "Salary Revised", Page.User.Identity.Name.ToString, Nothing) <> 0 Then
                    transaction.Rollback()
                    lblError.Text = "Failed to update Audit Trail"
                    UtilityObj.Errorlog("Failed to update Audit Trail")
                Else
                    transaction.Commit()
                    lblError.Text = " Salary Revised Successfully"
                    ClearAllDetails()
                End If
            Else
                lblError.Text = UtilityObj.getErrorMessage(errorNo)
                transaction.Rollback()
            End If
        End Using
    End Sub

    Private Sub ClearAllDetails()
        Session("EMPSALDETAILS") = Nothing
        Session("ADDEDEMPSALDETAILS") = Nothing

        hf_EMP_IDs.Value = ""
        hfSalGrade.Value = ""
        txtGrossSalary.Text = ""
        txtSalGrade.Text = ""
        txtRevisionDate.Text = ""
        Gridbind()
        GridBindAddSalaryDetails()
        GridBindSalaryDetails()
    End Sub

    Private Sub ClearAllDetails_Upload()
        Session("BulkSalaryRevision_UploadedSalary") = Nothing

        Me.gvUpload.DataSource = Nothing
        Me.gvUpload.DataBind()
    End Sub

    Private Function SaveEMPAddToSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        GetPayMonth_YearDetails()
        Dim iReturnvalue As Integer
        Try
            If Session("EMPSALDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim drSalDet As DataRow

            Dim dttab As DataTable = Session("EMPSALDETAILS")
            If dttab.Rows.Count > 0 Then
                For i As Integer = 0 To dttab.Rows.Count - 1
                    drSalDet = dttab.Rows(i)

                    cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
                    sqlpESH_ID.Value = 0
                    cmd.Parameters.Add(sqlpESH_ID)

                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                    sqlpUser.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpUser)

                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                    sqlpESH_EMP_ID.Value = drSalDet("EMP_ID") 'hf_EMP_IDs.Value
                    cmd.Parameters.Add(sqlpESH_EMP_ID)

                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                    sqlpESH_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpESH_BSU_ID)

                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                    sqlpESH_ERN_ID.Value = drSalDet("ENR_ID")
                    cmd.Parameters.Add(sqlpESH_ERN_ID)

                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                    sqlpESH_AMOUNT.Value = GetVal(drSalDet("AMOUNT_NEW"), SqlDbType.Decimal)
                    cmd.Parameters.Add(sqlpESH_AMOUNT)

                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                    sqlpESH_ELIGIBILITY.Value = GetVal(drSalDet("AMOUNT_ELIGIBILITY_NEW"), SqlDbType.Decimal)
                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                    sqlpESH_bMonthly.Value = drSalDet("bMonthly")
                    cmd.Parameters.Add(sqlpESH_bMonthly)

                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                    sqlpESH_PAYTERM.Value = drSalDet("PAYTERM")
                    cmd.Parameters.Add(sqlpESH_PAYTERM)

                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
                    cmd.Parameters.Add(sqlpESH_CUR_ID)

                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_PAY_CUR_ID.Value = ViewState("PAY_CUR_ID") 'FromDataBase
                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                    sqlpESH_PAYMONTH.Value = ViewState("PAYMONTH") 'FromDataBase
                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                    sqlpESH_PAYYEAR.Value = ViewState("PAYYEAR")
                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                    sqlpESH_FROMDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
                    cmd.Parameters.Add(sqlpESH_FROMDT)

                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
                    sqlpbDeleted.Value = False
                    cmd.Parameters.Add(sqlpbDeleted)

                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                    sqlpESH_TODT.Value = DBNull.Value
                    cmd.Parameters.Add(sqlpESH_TODT)

                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retSValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retSValParam)

                    Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                    retNewESH_ID.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(retNewESH_ID)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retSValParam.Value
                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                    End If
                Next
                If radSalScale.Checked Then
                    For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
                        Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
                        Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
                        Dim EMP_ID As String
                        If lblEMPID Is Nothing Then
                            Exit For
                        Else
                            EMP_ID = lblEMPID.Text
                        End If
                        iReturnvalue = UpdateEmployeeMaster(objConn, stTrans, EMP_ID, Session("sBSUID"), hfSalGrade.Value)
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    Next
                End If
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    Private Function SaveEMPAddToSalaryDetails_Upload(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, Optional ByRef EmpNo As String = Nothing, Optional ByRef EmpName As String = Nothing) As Integer
        GetPayMonth_YearDetails()
        Dim iReturnvalue As Integer
        Try
            If Session("BulkSalaryRevision_UploadedSalary") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim drSalDet As DataRow

            'existing salary revision validation
            Dim count As Integer
            Dim query As String
            Dim tmpDate As String = ""
            Dim tmpEarnCode As String = ""
            Dim UploadedRecordExists As Boolean = False
            Dim dt As DataTable = Session("BulkSalaryRevision_UploadedSalary")
            For i As Integer = 0 To dt.Rows.Count - 1
                tmpDate = Format(CDate(dt.Rows(i).Item("FromDate")), "yyyy-MM-dd")

                'first convert any salary component for which Description is used in the excel file instead of Code
                tmpEarnCode = SqlHelper.ExecuteScalar(stTrans, CommandType.Text, "SELECT ERN_ID FROM dbo.EMPSALCOMPO_M WHERE ERN_DESCR = '" & dt.Rows(i).Item("EarningCode") & "'")
                If Not IsDBNull(tmpEarnCode) AndAlso Not tmpEarnCode Is Nothing Then
                    dt.Rows(i).Item("EarningCode") = tmpEarnCode
                End If

                query = "SELECT IsNull(Count(*),0) FROM dbo.EMPSALARYHS_s WHERE ESH_BSU_ID = '" & dt.Rows(i).Item("Bsu_Id") & "' AND ESH_EMP_ID = " & dt.Rows(i).Item("Emp_Id") & " AND ESH_ERN_ID = '" & dt.Rows(i).Item("EarningCode") & "' AND ESH_FROMDT BETWEEN '" & tmpDate & " 00:00:00.000'" & " AND '" & tmpDate & " 00:00:00.000'"
                count = SqlHelper.ExecuteScalar(stTrans, CommandType.Text, query)
                dt.AcceptChanges()

                If count >= 1 Then
                    dt.Rows(i).Item("Save") = 0
                    dt.Rows(i).Item("bUploaded") = 1
                    UploadedRecordExists = True
                Else
                    dt.Rows(i).Item("Save") = 1
                    dt.Rows(i).Item("bUploaded") = 0
                End If
            Next
            dt.AcceptChanges()
            Me.gvUpload.DataSource = dt
            Me.gvUpload.DataBind()
            Session("BulkSalaryRevision_UploadedSalary") = dt

            'Check if any of the grid's record is previously already uploaded
            If UploadedRecordExists Then
                Dim lblUploaded As Label
                For Each row As GridViewRow In Me.gvUpload.Rows
                    lblUploaded = row.FindControl("lblUploaded")
                    If Not lblUploaded Is Nothing Then
                        If lblUploaded.Text = 1 Then
                            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                        End If
                    End If
                Next
            End If

            If UploadedRecordExists Then
                Return 1702
            End If

            'Dim dttab As DataTable = Session("EMPSALDETAILS")
            Dim dttab As DataTable = Session("BulkSalaryRevision_UploadedSalary")
            If dttab.Rows.Count > 0 Then
                For i As Integer = 0 To dttab.Rows.Count - 1
                    drSalDet = dttab.Rows(i)

                    EmpNo = drSalDet.Item("EmpNo")
                    EmpName = drSalDet.Item("EmpName")

                    If drSalDet.Item("Save") = 1 Then
                        cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
                        sqlpESH_ID.Value = 0
                        cmd.Parameters.Add(sqlpESH_ID)

                        Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                        sqlpUser.Value = Session("sUsr_name")
                        cmd.Parameters.Add(sqlpUser)

                        Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                        sqlpESH_EMP_ID.Value = drSalDet("EMP_ID") 'hf_EMP_IDs.Value
                        cmd.Parameters.Add(sqlpESH_EMP_ID)

                        Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                        'sqlpESH_BSU_ID.Value = Session("sBSUID")
                        sqlpESH_BSU_ID.Value = drSalDet("bsu_ID")
                        cmd.Parameters.Add(sqlpESH_BSU_ID)

                        Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                        sqlpESH_ERN_ID.Value = drSalDet("EarningCode")
                        cmd.Parameters.Add(sqlpESH_ERN_ID)

                        Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                        sqlpESH_AMOUNT.Value = GetVal(drSalDet("ActualAmount"), SqlDbType.Decimal)
                        cmd.Parameters.Add(sqlpESH_AMOUNT)

                        Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                        sqlpESH_ELIGIBILITY.Value = GetVal(drSalDet("EligibleAmount"), SqlDbType.Decimal)
                        cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                        Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                        'sqlpESH_bMonthly.Value = drSalDet("bMonthly")
                        sqlpESH_bMonthly.Value = 1
                        cmd.Parameters.Add(sqlpESH_bMonthly)

                        Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                        sqlpESH_PAYTERM.Value = drSalDet("pay_term")
                        cmd.Parameters.Add(sqlpESH_PAYTERM)

                        Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                        sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
                        cmd.Parameters.Add(sqlpESH_CUR_ID)

                        Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                        sqlpESH_PAY_CUR_ID.Value = ViewState("PAY_CUR_ID") 'FromDataBase
                        cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                        Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                        sqlpESH_PAYMONTH.Value = ViewState("PAYMONTH") 'FromDataBase
                        cmd.Parameters.Add(sqlpESH_PAYMONTH)

                        Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                        sqlpESH_PAYYEAR.Value = ViewState("PAYYEAR")
                        cmd.Parameters.Add(sqlpESH_PAYYEAR)

                        Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                        'sqlpESH_FROMDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
                        sqlpESH_FROMDT.Value = CDate(drSalDet("FromDate"))  'FromDataBase
                        cmd.Parameters.Add(sqlpESH_FROMDT)

                        Dim sqlpESH_EffectiveDT As New SqlParameter("@ESH_EffectiveDT", SqlDbType.DateTime)
                        'sqlpESH_EffectiveDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
                        sqlpESH_EffectiveDT.Value = CDate(drSalDet("EffectiveDate"))  'FromDataBase
                        cmd.Parameters.Add(sqlpESH_EffectiveDT)

                        Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
                        sqlpbDeleted.Value = False
                        cmd.Parameters.Add(sqlpbDeleted)

                        Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                        sqlpESH_TODT.Value = DBNull.Value
                        cmd.Parameters.Add(sqlpESH_TODT)

                        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retSValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retSValParam)

                        Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                        retNewESH_ID.Direction = ParameterDirection.Output
                        cmd.Parameters.Add(retNewESH_ID)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retSValParam.Value
                        If iReturnvalue <> 0 Then
                            Return iReturnvalue
                        End If
                    End If
                Next
                'If radSalScale.Checked Then
                '    For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
                '        Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
                '        Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
                '        Dim EMP_ID As String
                '        If lblEMPID Is Nothing Then
                '            Exit For
                '        Else
                '            EMP_ID = lblEMPID.Text
                '        End If
                '        iReturnvalue = UpdateEmployeeMaster(objConn, stTrans, EMP_ID, Session("sBSUID"), hfSalGrade.Value)
                '        If iReturnvalue <> 0 Then
                '            Exit For
                '        End If
                '    Next
                'End If
            End If
            Dim CheckIfBSUInDax As Boolean = False
            CheckIfBSUInDax = SqlHelper.ExecuteScalar(stTrans, CommandType.Text, "SELECT ISNULL(bsu_IsHROnDAX,0) AS bsu_IsHROnDAX FROM dbo.BUSINESSUNIT_M WHERE 1=2 and BSU_ID = '" & Session("sBSUID").ToString & "'")

            If CheckIfBSUInDax = True Then


                'Send to DAX
                Dim client As New EmployeeCompensationService.ALE_EmpCompServiceClient
                client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
                client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString
                Dim context As New EmployeeCompensationService.CallContext

                Dim EmpCompLinesCount As Integer '= gvCompensation.Rows.Count '+ 6 + 1 + gvJoiningAllowance.Rows.Count

                Dim EmpCompFactory As New EmployeeCompensationService.AxdALE_EmpComp
                Dim EmpComps(0) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
                Dim EmpComp As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation = Nothing
                Dim EmpCompLines(EmpCompLinesCount - 1) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine
                Dim EmpCompLine As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing

                Dim tmpStr As String = ""
                Dim tmpQuery As String = ""
                Dim dbDs As DataSet = Nothing
                Dim dbDs2 As DataSet = Nothing
                Dim Rows(), dbRows(), tmpRow As DataRow
                Dim dtUploadedEmployees As New DataTable
                dtUploadedEmployees.Columns.Add("EmpNo", GetType(String))
                dtUploadedEmployees.AcceptChanges()


                Dim dtUpload As DataTable = Session("BulkSalaryRevision_UploadedSalary")
                For Each row As DataRow In dtUpload.Rows
                    Rows = dtUploadedEmployees.Select("EmpNo = '" & row.Item("EmpNo") & "'")
                    If Rows.Length = 0 Then 'skip the row if this employee's salary details are already sent to DAX
                        Rows = dtUpload.Select("EmpNo = '" & row.Item("EmpNo") & "'")
                        For Each tmpRow In Rows
                            tmpStr &= "'" & tmpRow.Item("EarningCode") & "',"
                        Next
                        If tmpStr.EndsWith(",") Then
                            tmpStr = tmpStr.TrimEnd(",")
                        End If
                        Dim k As Integer = 0
                        tmpQuery = "SELECT ESH_ERN_ID, ERN_DESCR, ISNULL(ESH_AMOUNT,0) AS ESH_AMOUNT , ISNULL(ESH_ELIGIBILITY,0) AS ESH_ELIGIBILITY, ESH_CUR_ID, " & _
                            "ISNULL(ESH_FROMDT,GETDATE()) AS ESH_FROMDT, ISNULL(ESH_EFFECTIVEDT, GETDATE()) AS ESH_EFFECTIVEDT FROM dbo.EMPSALARYHS_s INNER JOIN " & _
                            "dbo.EMPSALCOMPO_M ON ESH_ERN_ID = ERN_ID WHERE ESH_EMP_ID = (SELECT EMP_ID FROM dbo.EMPLOYEE_M WHERE EMPNO = '" & row.Item("EmpNo") & "') " & _
                            "AND ESH_TODT IS NULL AND ESH_ERN_ID NOT IN (" & tmpStr & ")"

                        dbDs = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, tmpQuery)
                        tmpQuery = ""
                        Dim ValidaDt As String = ""
                        tmpQuery = "SELECT max(ISNULL(ESH_EFFECTIVEDT, ESH_FROMDT)) AS ESH_EFFECTIVEDT FROM dbo.EMPSALARYHS_s INNER JOIN dbo.EMPSALCOMPO_M ON " & _
                            "ESH_ERN_ID = ERN_ID WHERE ESH_EMP_ID = (SELECT EMP_ID FROM dbo.EMPLOYEE_M WHERE EMPNO = '" & row.Item("EmpNo") & "') AND ESH_TODT IS NULL " & _
                            "AND ESH_ERN_ID IN (" & tmpStr & ")"

                        dbDs2 = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, tmpQuery)
                        ValidaDt = dbDs2.Tables(0).Rows(0)("ESH_EFFECTIVEDT").ToString
                        If Not dbDs Is Nothing Then
                            EmpCompLinesCount = (Rows.Length + dbDs.Tables(0).Rows.Count) - 1
                        Else
                            EmpCompLinesCount = Rows.Length - 1
                        End If
                        ''getting salary benefits
                        Dim dsSalBen As New DataSet
                        dsSalBen = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, "EXEC OASIS_DAX.dbo.[GetApplicantBenefits_ForBulkSalaryUpload] @EMP_NO = '" & row.Item("EmpNo") & "'")
                        If Not dsSalBen Is Nothing AndAlso dsSalBen.Tables(0).Rows.Count > 0 Then
                            EmpCompLinesCount += dsSalBen.Tables(0).Rows.Count
                        End If
                        ReDim EmpCompLines(EmpCompLinesCount)
                        context.Company = SqlHelper.ExecuteScalar(stTrans, CommandType.Text, "SELECT ISNULL(AX_BSU_ID,'') AS AX_BSU_ID FROM dbo.BUSINESSUNIT_M WHERE BSU_ID = '" & row.Item("bsu_id") & "'")
                        'Initialize and fill PosComp (header) details
                        EmpComp = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
                        EmpComp.RecId = Me.Get6DigitRandomNumber
                        EmpComp.RecIdSpecified = True
                        EmpComp.EmployeeId = row.Item("EmpNo")
                        EmpComp.HcmActionState = "AS000002" 'Ignore this comment. As of now, skipping this as any value passed for it is throwing error message. Need to check with them what valid value we can pass
                        EmpComp.CompensationId = Me.GetUniqueCompensationId(row.Item("EmpNo"), stTrans)
                        If EmpComp.CompensationId = "00" Then
                            stTrans.Rollback()
                            Return 1000
                            Exit Function
                        End If
                        EmpComp.Description = "Bulk Salary Upload"
                        EmpComp.ValidFrom = ValidaDt ' Now.Date
                        EmpComp.ValidFromSpecified = True
                        'EmpComp.ValidTo = ValidTo 'Pass some distant future date
                        EmpComp.ValidToSpecified = False
                        EmpComp.class = "entity" 'This value is hard coded as suggested in the service documentation
                        'Initialize and fill PosCompLine (detail) details
                        For Each tmpRow In Rows
                            EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

                            EmpCompLine.RecId = Get6DigitRandomNumber()
                            EmpCompLine.RecIdSpecified = True

                            EmpCompLine.EmployeeCompensationRef = EmpComp.RecId
                            EmpCompLine.EmployeeCompensationRefSpecified = True

                            EmpCompLine.Amount = tmpRow.Item("EligibleAmount")
                            EmpCompLine.AmountSpecified = True
                            EmpCompLine.CurrencyCode = ViewState("PAY_CUR_ID") '"AED"
                            EmpCompLine.ComponentCode = tmpRow.Item("EarningCode")
                            EmpCompLine.Description = tmpRow.Item("EarningDesc")
                            EmpCompLine.FromDate = CDate(tmpRow.Item("EffectiveDate"))
                            EmpCompLine.FromDateSpecified = True
                            EmpCompLine.PayType = EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly
                            EmpCompLine.PayTypeSpecified = True
                            EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

                            EmpCompLines(k) = EmpCompLine
                            k += 1
                        Next
                        If Not dbDs Is Nothing Then
                            For Each tmpRow In dbDs.Tables(0).Rows
                                EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

                                EmpCompLine.RecId = Get6DigitRandomNumber()
                                EmpCompLine.RecIdSpecified = True

                                EmpCompLine.EmployeeCompensationRef = EmpComp.RecId
                                EmpCompLine.EmployeeCompensationRefSpecified = True

                                EmpCompLine.Amount = tmpRow.Item("ESH_ELIGIBILITY")
                                EmpCompLine.AmountSpecified = True
                                EmpCompLine.CurrencyCode = tmpRow.Item("ESH_CUR_ID")
                                EmpCompLine.ComponentCode = tmpRow.Item("ESH_ERN_ID")
                                EmpCompLine.Description = tmpRow.Item("ERN_DESCR")
                                EmpCompLine.FromDate = CDate(tmpRow.Item("ESH_EFFECTIVEDT"))
                                EmpCompLine.FromDateSpecified = True
                                EmpCompLine.PayType = EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly
                                EmpCompLine.PayTypeSpecified = True
                                EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

                                EmpCompLines(k) = EmpCompLine
                                k += 1
                            Next
                        End If

                        If Not dsSalBen Is Nothing AndAlso dsSalBen.Tables(0).Rows.Count > 0 Then
                            'Now fill up the EmpCompLines() array with all EmpCompLine. Benefits details.
                            For Each dr As DataRow In dsSalBen.Tables(0).Rows
                                Dim dblAmount As Double = GetActualAmount(dr("SBEN_AMOUNT"))
                                If Not IsNumeric(dblAmount) Then
                                    dblAmount = 0
                                End If
                                Select Case dr("SBEN_BENM_ID").ToString
                                    Case "I"
                                        'Medical Insurance
                                        EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "MediIns", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None, dr("INSU_DESCR").ToString, dr("SBEN_ACTUAL_DESCR").ToString)
                                        k += 1
                                    Case "A"
                                        'Airfare
                                        EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "AIR", dr("EAF_TYPE_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None, dr("SBEN_CLASS_DESCR").ToString, dr("SBEN_ACTUAL_DESCR").ToString, dr("FROM_CITY_DESCR").ToString, dr("TO_CITY_DESCR").ToString, dr("FROM_COUNTRY").ToString, dr("TO_COUNTRY").ToString, dr("EAF_TYPE_DESCR").ToString, Val(dr("SBEN_COUNT")))
                                        k += 1
                                    Case "TFC"
                                        'Tution Fee Concession 
                                        EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "TutionFee", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None)
                                        k += 1
                                    Case "TRA"
                                        'Transport
                                        EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "TRA", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None)
                                        k += 1
                                    Case "ACC"
                                        'Accommodation
                                        EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "ACC", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None, , dr("ELG_TYPE_DESCR").ToString)
                                        k += 1
                                        'Case "LI"
                                        '    'Life Insurance
                                        '    EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, Me.GetActualAmount(Me.txtLifeInsurancePremium.Text), dr("BSU_CURRENCY").ToString, "LifeIns", Me.ddlLifeInsurance.SelectedItem.Text, Me.txtLifeInsuranceFromDate.Text, "entity", DatabaseRecordAction.None)
                                        '    k += 1
                                        '    'LTIP
                                        '    EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, Me.GetActualAmount(Me.txtLTIPPremium.Text), dr("BSU_CURRENCY").ToString, "LTIP", Me.GetActualAmount(Me.txtLTIPPremium.Text), Me.txtLTIPFromDate.Text, "entity", DatabaseRecordAction.None)
                                        '    k += 1
                                        '    'Target Bonus
                                        '    EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, Val(Me.txtTargetBonus.Text), dr("BSU_CURRENCY").ToString, "TargetBonus", Me.txtTargetBonus.Text & " month(s)", IIf(Me.txtTargetBonusFromDate.Text <> Nothing, Me.txtTargetBonusFromDate.Text, Now.Date), "entity", DatabaseRecordAction.None)
                                        '    k += 1

                                        ''Now fill up the EmpCompLines() array with all EmpCompLine. Joining Allowances details.
                                        ''Relocation Airfare
                                        'EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, 0, dr("BSU_CURRENCY").ToString, "RF", Left(Me.ddlAirfareRelocation.SelectedItem.Text, 60), Now.Date.ToShortDateString, "entity", DatabaseRecordAction.None, , , Me.txtFromCity.Text, Me.txtToCity.Text, Me.hfFromCountryRelocation.Value, Me.hfToCountryRelocation.Value)
                                        'k += 1
                                End Select


                            Next

                        End If
                        'Now assign the EmpCompLines() array to EmpComp
                        'EmpComp.empCompLine = EmpCompLines
                        EmpComp.ALE_EmployeeCompensationLine = EmpCompLines

                        'Now add EmpComp to EmpComps array
                        EmpComps(0) = EmpComp

                        'Now assing EmpComps to EmpComp field of EmpCompFactory
                        EmpCompFactory.ALE_EmployeeCompensation = EmpComps

                        Dim ent(0) As EmployeeCompensationService.EntityKey
                        ent = client.create(context, EmpCompFactory)
                        Dim UploadedEmpRow As DataRow = dtUploadedEmployees.NewRow
                        UploadedEmpRow.Item("EmpNo") = row.Item("EmpNo")
                        dtUploadedEmployees.Rows.Add(UploadedEmpRow)
                        dtUploadedEmployees.AcceptChanges()
                    End If
                Next
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function


    Private Function SaveEMPAddToSalaryDetails_UploadTo_OASISandAX(Optional ByRef EmpNo As String = Nothing, Optional ByRef EmpName As String = Nothing) As Integer
        GetPayMonth_YearDetails()
        Dim iReturnvalue As Integer
        Try
            If Session("BulkSalaryRevision_UploadedSalary") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim drSalDet As DataRow
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'existing salary revision validation
            Dim count As Integer
            Dim query As String
            Dim tmpDate As String = ""
            Dim tmpEarnCode As String = ""
            Dim UploadedRecordExists As Boolean = False
            Dim LastEmpno As String = ""

            Dim dt As DataTable = Session("BulkSalaryRevision_UploadedSalary")
            For i As Integer = 0 To dt.Rows.Count - 1
                tmpDate = Format(CDate(dt.Rows(i).Item("FromDate")), "yyyy-MM-dd")

                'first convert any salary component for which Description is used in the excel file instead of Code
                tmpEarnCode = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT ERN_ID FROM dbo.EMPSALCOMPO_M WHERE ERN_DESCR = '" & dt.Rows(i).Item("EarningCode") & "'")
                If Not IsDBNull(tmpEarnCode) AndAlso Not tmpEarnCode Is Nothing Then
                    dt.Rows(i).Item("EarningCode") = tmpEarnCode
                End If

                query = "SELECT IsNull(Count(*),0) FROM dbo.EMPSALARYHS_s WHERE 1=2 and  ESH_BSU_ID = '" & dt.Rows(i).Item("Bsu_Id") & "' AND ESH_EMP_ID = " & dt.Rows(i).Item("Emp_Id") & " AND ESH_ERN_ID = '" & dt.Rows(i).Item("EarningCode") & "' AND ESH_FROMDT BETWEEN '" & tmpDate & " 00:00:00.000'" & " AND '" & tmpDate & " 00:00:00.000'"
                count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)
                dt.AcceptChanges()

                If count >= 1 Then
                    dt.Rows(i).Item("Save") = 0
                    dt.Rows(i).Item("bUploaded") = 1
                    UploadedRecordExists = True
                Else
                    dt.Rows(i).Item("Save") = 1
                    dt.Rows(i).Item("bUploaded") = 0
                End If
            Next
            dt.AcceptChanges()
            Me.gvUpload.DataSource = dt
            Me.gvUpload.DataBind()
            Session("BulkSalaryRevision_UploadedSalary") = dt

            'Check if any of the grid's record is previously already uploaded
            If UploadedRecordExists Then
                Dim lblUploaded As Label
                For Each row As GridViewRow In Me.gvUpload.Rows
                    lblUploaded = row.FindControl("lblUploaded")
                    If Not lblUploaded Is Nothing Then
                        If lblUploaded.Text = 1 Then
                            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                        End If
                    End If
                Next
            End If

            If UploadedRecordExists Then
                Return 1702
            End If

            'Dim dttab As DataTable = Session("EMPSALDETAILS")

            Dim CheckIfBSUInDax As Boolean = Session("BSU_IsOnDAX")
            ' CheckIfBSUInDax = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT ISNULL(bsu_IsHROnDAX,0) AS bsu_IsHROnDAX FROM dbo.BUSINESSUNIT_M WHERE 1=1 and BSU_ID = '" & Session("sBSUID").ToString & "'")
            If CheckIfBSUInDax = True Then


                'Send to DAX
                Dim client As New EmployeeCompensationService.ALE_EmpCompServiceClient
                client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
                client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString
                Dim context As New EmployeeCompensationService.CallContext

                Dim EmpCompLinesCount As Integer '= gvCompensation.Rows.Count '+ 6 + 1 + gvJoiningAllowance.Rows.Count

                Dim EmpCompFactory As New EmployeeCompensationService.AxdALE_EmpComp
                Dim EmpComps(0) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
                Dim EmpComp As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation = Nothing
                Dim EmpCompLines(EmpCompLinesCount - 1) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine
                Dim EmpCompLine As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing

                Dim tmpStr As String = ""
                Dim tmpQuery As String = ""
                Dim dbDs As DataSet = Nothing
                Dim dbDs2 As DataSet = Nothing
                Dim Rows(), dbRows(), tmpRow As DataRow
                Dim dtUploadedEmployees As New DataTable
                dtUploadedEmployees.Columns.Add("EmpNo", GetType(String))
                dtUploadedEmployees.AcceptChanges()

                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Dim stTrans As SqlTransaction
                Dim dttab As DataTable = Session("BulkSalaryRevision_UploadedSalary")

                If dttab.Rows.Count > 0 Then
                    For i As Integer = 0 To dttab.Rows.Count - 1
                        If i > 0 Then
                            LastEmpno = dttab.Rows(i - 1).Item("Empno")
                        End If
                        drSalDet = dttab.Rows(i)

                        EmpNo = drSalDet.Item("EmpNo")
                        EmpName = drSalDet.Item("EmpName")


                        If LastEmpno <> EmpNo Then
                            stTrans = objConn.BeginTransaction
                        End If

                        Try




                            If drSalDet.Item("Save") = 1 Then
                                cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                                cmd.CommandType = CommandType.StoredProcedure

                                Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
                                sqlpESH_ID.Value = 0
                                cmd.Parameters.Add(sqlpESH_ID)

                                Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                                sqlpUser.Value = Session("sUsr_name")
                                cmd.Parameters.Add(sqlpUser)

                                Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                                sqlpESH_EMP_ID.Value = drSalDet("EMP_ID") 'hf_EMP_IDs.Value
                                cmd.Parameters.Add(sqlpESH_EMP_ID)

                                Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                                'sqlpESH_BSU_ID.Value = Session("sBSUID")
                                sqlpESH_BSU_ID.Value = drSalDet("bsu_ID")
                                cmd.Parameters.Add(sqlpESH_BSU_ID)

                                Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                                sqlpESH_ERN_ID.Value = drSalDet("EarningCode")
                                cmd.Parameters.Add(sqlpESH_ERN_ID)

                                Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                                sqlpESH_AMOUNT.Value = GetVal(drSalDet("ActualAmount"), SqlDbType.Decimal)
                                cmd.Parameters.Add(sqlpESH_AMOUNT)

                                Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                                sqlpESH_ELIGIBILITY.Value = GetVal(drSalDet("EligibleAmount"), SqlDbType.Decimal)
                                cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                                Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                                'sqlpESH_bMonthly.Value = drSalDet("bMonthly")
                                sqlpESH_bMonthly.Value = 1
                                cmd.Parameters.Add(sqlpESH_bMonthly)

                                Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                                sqlpESH_PAYTERM.Value = drSalDet("pay_term")
                                cmd.Parameters.Add(sqlpESH_PAYTERM)

                                Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                                sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
                                cmd.Parameters.Add(sqlpESH_CUR_ID)

                                Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                                sqlpESH_PAY_CUR_ID.Value = ViewState("PAY_CUR_ID") 'FromDataBase
                                cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                                Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                                sqlpESH_PAYMONTH.Value = ViewState("PAYMONTH") 'FromDataBase
                                cmd.Parameters.Add(sqlpESH_PAYMONTH)

                                Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                                sqlpESH_PAYYEAR.Value = ViewState("PAYYEAR")
                                cmd.Parameters.Add(sqlpESH_PAYYEAR)

                                Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                                'sqlpESH_FROMDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
                                sqlpESH_FROMDT.Value = CDate(drSalDet("FromDate"))  'FromDataBase
                                cmd.Parameters.Add(sqlpESH_FROMDT)

                                Dim sqlpESH_EffectiveDT As New SqlParameter("@ESH_EffectiveDT", SqlDbType.DateTime)
                                'sqlpESH_EffectiveDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
                                sqlpESH_EffectiveDT.Value = CDate(drSalDet("EffectiveDate"))  'FromDataBase
                                cmd.Parameters.Add(sqlpESH_EffectiveDT)

                                Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
                                sqlpbDeleted.Value = False
                                cmd.Parameters.Add(sqlpbDeleted)

                                Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                                sqlpESH_TODT.Value = DBNull.Value
                                cmd.Parameters.Add(sqlpESH_TODT)

                                Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                                retSValParam.Direction = ParameterDirection.ReturnValue
                                cmd.Parameters.Add(retSValParam)

                                Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                                retNewESH_ID.Direction = ParameterDirection.Output
                                cmd.Parameters.Add(retNewESH_ID)

                                cmd.ExecuteNonQuery()
                                iReturnvalue = retSValParam.Value
                                If iReturnvalue <> 0 Then
                                    stTrans.Rollback()
                                    lblError.Text = "Error occured at row number " & i.ToString & " ,Empno: " & EmpNo & ", Name: " & EmpName
                                    Return iReturnvalue
                                End If
                            End If
                            Dim row As DataRow
                            row = dttab.Rows(i)
                            Rows = dtUploadedEmployees.Select("EmpNo = '" & row.Item("EmpNo") & "'")
                            tmpStr &= "'" & dttab.Rows(i).Item("EarningCode") & "',"
                            If i < dttab.Rows.Count - 1 Then
                                If dttab.Rows(i).Item("EMPNO") = dttab.Rows(i + 1).Item("EMPNO") Then GoTo LoopSalary
                            End If



                           
                            If Rows.Length = 0 Then 'skip the row if this employee's salary details are already sent to DAX
                                Rows = dttab.Select("EmpNo = '" & row.Item("EmpNo") & "'")
                                'For Each tmpRow In Rows
                                '    tmpStr &= "'" & tmpRow.Item("EarningCode") & "',"
                                'Next
                                If tmpStr.EndsWith(",") Then
                                    tmpStr = tmpStr.TrimEnd(",")
                                End If
                                Dim k As Integer = 0
                                tmpQuery = "SELECT ESH_ERN_ID, ERN_DESCR, ISNULL(ESH_AMOUNT,0) AS ESH_AMOUNT , ISNULL(ESH_ELIGIBILITY,0) AS ESH_ELIGIBILITY, ESH_CUR_ID, " & _
                                    "ISNULL(ESH_FROMDT,GETDATE()) AS ESH_FROMDT, ISNULL(ESH_EFFECTIVEDT, GETDATE()) AS ESH_EFFECTIVEDT FROM dbo.EMPSALARYHS_s INNER JOIN " & _
                                    "dbo.EMPSALCOMPO_M ON ESH_ERN_ID = ERN_ID WHERE ESH_EMP_ID = (SELECT EMP_ID FROM dbo.EMPLOYEE_M WHERE EMPNO = '" & row.Item("EmpNo") & "') " & _
                                    "AND ESH_TODT IS NULL AND ESH_ERN_ID NOT IN (" & tmpStr & ")"

                                dbDs = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, tmpQuery)
                                tmpQuery = ""
                                Dim ValidaDt As String = ""
                                tmpQuery = "SELECT max(ISNULL(ESH_EFFECTIVEDT, ESH_FROMDT)) AS ESH_EFFECTIVEDT FROM dbo.EMPSALARYHS_s INNER JOIN dbo.EMPSALCOMPO_M ON " & _
                                    "ESH_ERN_ID = ERN_ID WHERE ESH_EMP_ID = (SELECT EMP_ID FROM dbo.EMPLOYEE_M WHERE EMPNO = '" & row.Item("EmpNo") & "') AND ESH_TODT IS NULL " & _
                                    "AND ESH_ERN_ID IN (" & tmpStr & ")"

                                dbDs2 = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, tmpQuery)
                                ValidaDt = dbDs2.Tables(0).Rows(0)("ESH_EFFECTIVEDT").ToString
                                If Not dbDs Is Nothing Then
                                    EmpCompLinesCount = (Rows.Length + dbDs.Tables(0).Rows.Count) - 1
                                Else
                                    EmpCompLinesCount = Rows.Length - 1
                                End If
                                ''getting salary benefits
                                Dim dsSalBen As New DataSet

                                dsSalBen = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, "EXEC OASIS_DAX.dbo.[GetApplicantBenefits_ForBulkSalaryUpload] @EMP_NO = '" & row.Item("EmpNo") & "'")
                                If Not dsSalBen Is Nothing AndAlso dsSalBen.Tables(0).Rows.Count > 0 Then
                                    EmpCompLinesCount += dsSalBen.Tables(0).Rows.Count
                                End If
                                ReDim EmpCompLines(EmpCompLinesCount)
                                context.Company = SqlHelper.ExecuteScalar(stTrans, CommandType.Text, "SELECT ISNULL(AX_BSU_ID,'') AS AX_BSU_ID FROM dbo.BUSINESSUNIT_M WHERE BSU_ID = '" & row.Item("bsu_id") & "'")
                                'Initialize and fill PosComp (header) details
                                EmpComp = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
                                EmpComp.RecId = Me.Get6DigitRandomNumber
                                EmpComp.RecIdSpecified = True
                                EmpComp.EmployeeId = row.Item("EmpNo")
                                EmpComp.HcmActionState = "AS000002" 'Ignore this comment. As of now, skipping this as any value passed for it is throwing error message. Need to check with them what valid value we can pass
                                EmpComp.CompensationId = Me.GetUniqueCompensationId(row.Item("EmpNo"), stTrans)
                                If EmpComp.CompensationId = "00" Then
                                    stTrans.Rollback()
                                    Return 1000
                                    Exit Function
                                End If
                                EmpComp.Description = "Bulk Salary Upload"
                                EmpComp.ValidFrom = ValidaDt ' Now.Date
                                EmpComp.ValidFromSpecified = True
                                'EmpComp.ValidTo = ValidTo 'Pass some distant future date
                                EmpComp.ValidToSpecified = False
                                EmpComp.class = "entity" 'This value is hard coded as suggested in the service documentation
                                'Initialize and fill PosCompLine (detail) details
                                For Each tmpRow In Rows
                                    EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

                                    EmpCompLine.RecId = Get6DigitRandomNumber()
                                    EmpCompLine.RecIdSpecified = True

                                    EmpCompLine.EmployeeCompensationRef = EmpComp.RecId
                                    EmpCompLine.EmployeeCompensationRefSpecified = True

                                    EmpCompLine.Amount = tmpRow.Item("EligibleAmount")
                                    EmpCompLine.AmountSpecified = True
                                    EmpCompLine.CurrencyCode = ViewState("PAY_CUR_ID")   '"AED"
                                    EmpCompLine.ComponentCode = tmpRow.Item("EarningCode")
                                    EmpCompLine.Description = tmpRow.Item("EarningDesc")
                                    EmpCompLine.FromDate = CDate(tmpRow.Item("EffectiveDate"))
                                    EmpCompLine.FromDateSpecified = True
                                    EmpCompLine.PayType = EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly
                                    EmpCompLine.PayTypeSpecified = True
                                    EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

                                    EmpCompLines(k) = EmpCompLine
                                    k += 1
                                Next
                                If Not dbDs Is Nothing Then
                                    For Each tmpRow In dbDs.Tables(0).Rows
                                        EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

                                        EmpCompLine.RecId = Get6DigitRandomNumber()
                                        EmpCompLine.RecIdSpecified = True

                                        EmpCompLine.EmployeeCompensationRef = EmpComp.RecId
                                        EmpCompLine.EmployeeCompensationRefSpecified = True

                                        EmpCompLine.Amount = tmpRow.Item("ESH_ELIGIBILITY")
                                        EmpCompLine.AmountSpecified = True
                                        EmpCompLine.CurrencyCode = tmpRow.Item("ESH_CUR_ID")
                                        EmpCompLine.ComponentCode = tmpRow.Item("ESH_ERN_ID")
                                        EmpCompLine.Description = tmpRow.Item("ERN_DESCR")
                                        EmpCompLine.FromDate = CDate(tmpRow.Item("ESH_EFFECTIVEDT"))
                                        EmpCompLine.FromDateSpecified = True
                                        EmpCompLine.PayType = EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly
                                        EmpCompLine.PayTypeSpecified = True
                                        EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

                                        EmpCompLines(k) = EmpCompLine
                                        k += 1
                                    Next
                                End If

                                If Not dsSalBen Is Nothing AndAlso dsSalBen.Tables(0).Rows.Count > 0 Then
                                    'Now fill up the EmpCompLines() array with all EmpCompLine. Benefits details.
                                    For Each dr As DataRow In dsSalBen.Tables(0).Rows
                                        Dim dblAmount As Double = GetActualAmount(dr("SBEN_AMOUNT"))
                                        If Not IsNumeric(dblAmount) Then
                                            dblAmount = 0
                                        End If
                                        Select Case dr("SBEN_BENM_ID").ToString
                                            Case "I"
                                                'Medical Insurance
                                                EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "MediIns", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None, dr("INSU_DESCR").ToString, dr("SBEN_ACTUAL_DESCR").ToString)
                                                k += 1
                                            Case "A"
                                                'Airfare
                                                EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "AIR", dr("EAF_TYPE_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None, dr("SBEN_CLASS_DESCR").ToString, dr("SBEN_ACTUAL_DESCR").ToString, dr("FROM_CITY_DESCR").ToString, dr("TO_CITY_DESCR").ToString, dr("FROM_COUNTRY").ToString, dr("TO_COUNTRY").ToString, dr("EAF_TYPE_DESCR").ToString, Val(dr("SBEN_COUNT")))
                                                k += 1
                                            Case "TFC"
                                                'Tution Fee Concession 
                                                EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "TutionFee", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None)
                                                k += 1
                                            Case "TRA"
                                                'Transport
                                                EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "TRA", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None)
                                                k += 1
                                            Case "ACC"
                                                'Accommodation
                                                EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), EmpComp.RecId, 0, dblAmount, dr("BSU_CURRENCY").ToString, "ACC", dr("SBEN_ACTUAL_DESCR").ToString, Format(CDate(dr("SBEN_FROMDT")), "dd/MMM/yyyy"), "entity", DatabaseRecordAction.None, , dr("ELG_TYPE_DESCR").ToString)
                                                k += 1
                                                'Case "LI"
                                                '    'Life Insurance
                                                '    EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, Me.GetActualAmount(Me.txtLifeInsurancePremium.Text), dr("BSU_CURRENCY").ToString, "LifeIns", Me.ddlLifeInsurance.SelectedItem.Text, Me.txtLifeInsuranceFromDate.Text, "entity", DatabaseRecordAction.None)
                                                '    k += 1
                                                '    'LTIP
                                                '    EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, Me.GetActualAmount(Me.txtLTIPPremium.Text), dr("BSU_CURRENCY").ToString, "LTIP", Me.GetActualAmount(Me.txtLTIPPremium.Text), Me.txtLTIPFromDate.Text, "entity", DatabaseRecordAction.None)
                                                '    k += 1
                                                '    'Target Bonus
                                                '    EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, Val(Me.txtTargetBonus.Text), dr("BSU_CURRENCY").ToString, "TargetBonus", Me.txtTargetBonus.Text & " month(s)", IIf(Me.txtTargetBonusFromDate.Text <> Nothing, Me.txtTargetBonusFromDate.Text, Now.Date), "entity", DatabaseRecordAction.None)
                                                '    k += 1

                                                ''Now fill up the EmpCompLines() array with all EmpCompLine. Joining Allowances details.
                                                ''Relocation Airfare
                                                'EmpCompLines(k) = Me.PopulateEmpCompLine(Get6DigitRandomNumber(), RecId, Me.hfDAXActionNumber.Value, 0, dr("BSU_CURRENCY").ToString, "RF", Left(Me.ddlAirfareRelocation.SelectedItem.Text, 60), Now.Date.ToShortDateString, "entity", DatabaseRecordAction.None, , , Me.txtFromCity.Text, Me.txtToCity.Text, Me.hfFromCountryRelocation.Value, Me.hfToCountryRelocation.Value)
                                                'k += 1
                                        End Select


                                    Next

                                End If
                                'Now assign the EmpCompLines() array to EmpComp
                                'EmpComp.empCompLine = EmpCompLines
                                EmpComp.ALE_EmployeeCompensationLine = EmpCompLines

                                'Now add EmpComp to EmpComps array
                                EmpComps(0) = EmpComp

                                'Now assing EmpComps to EmpComp field of EmpCompFactory
                                EmpCompFactory.ALE_EmployeeCompensation = EmpComps

                                Dim ent(0) As EmployeeCompensationService.EntityKey
                                ent = client.create(context, EmpCompFactory)
                                Dim UploadedEmpRow As DataRow = dtUploadedEmployees.NewRow
                                UploadedEmpRow.Item("EmpNo") = row.Item("EmpNo")
                                dtUploadedEmployees.Rows.Add(UploadedEmpRow)
                                dtUploadedEmployees.AcceptChanges()
                            End If
                            If i < dttab.Rows.Count - 1 Then
                                If row.Item("EmpNo") <> dttab.Rows(i + 1).Item("EmpNo") Then
                                    stTrans.Commit()
                                    tmpStr = ""
                                End If
                            ElseIf i = dttab.Rows.Count - 1 Then
                                stTrans.Commit()
                                tmpStr = ""
                            End If
                        Catch aifFaults As System.ServiceModel.FaultException(Of EmployeeCompensationService.AifFault)
                            Dim infologMessageList As EmployeeCompensationService.InfologMessage() = aifFaults.Detail.InfologMessageList
                            Dim sLogDetails As sLog
                            sLogDetails.LogMessage = ""
                            For Each infologMessage As EmployeeCompensationService.InfologMessage In infologMessageList
                                LogEvent(String.Format("FaultException while calling Web Service. Excepton Message - {0}", infologMessage.Message), EventLogEntryType.Information)
                                sLogDetails.LogMessage = sLogDetails.LogMessage & infologMessage.Message
                            Next
                            lblError.Text = sLogDetails.LogMessage

                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message)
                            stTrans.Rollback()
                            Return 1000
                        End Try
LoopSalary:
                    Next

                End If


            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function
#Region "STRUCTURE sLog"
    Private Structure sLog
        Public ServiceType As Integer
        Public ServiceName As String
        Public SendToAxID As Integer
        Public SendToAxTime As DateTime
        Public Status As String
        Public AxReferenceID As String
        Public LogMessage As String
    End Structure
#End Region
#Region "LOG EVENTS"
    Private Sub LogEvent(message As String, entryType As EventLogEntryType)
        Dim eventLog As New System.Diagnostics.EventLog()
        eventLog.Source = "OasisAXIntegrationService"
        eventLog.Log = "Application"
        eventLog.WriteEntry(message, entryType)

        Errorlog(message, "Bulk Upload-Employee compensation service")
    End Sub
#End Region

    Private Function PopulateEmpCompLine(ByVal RecId As String, ByVal CompensationRefId As Int64, ByVal ActionNumber As String, ByVal Amount As Decimal, ByVal Currency As String, ByVal ComponentCode As String, ByVal Description As String, ByVal FromDate As String, ByVal ClassType As String, ByVal RecordAction As DatabaseRecordAction, Optional ByVal CategoryClass As String = "", Optional ByVal ActualType As String = "", Optional ByVal FromCity As String = "", Optional ByVal ToCity As String = "", Optional ByVal FromCountry As String = "", Optional ByVal ToCountry As String = "", Optional ByVal TicketType As String = "", Optional ByVal TicketsPerYear As Decimal = 0) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine
        PopulateEmpCompLine = Nothing

        'Dim PosCompRef As PositionCompensationService.AxdEntityKey_ALE_HCMPositionCompensation
        Dim EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

        'EmpCompLine.RecId = Get6DigitRandomNumber() 'Skipped this value here. If value passed, AX is throwing error
        'EmpCompLine.RecIdSpecified = True

        EmpCompLine.EmployeeCompensationRef = CompensationRefId


        EmpCompLine.Amount = Me.GetActualAmount(Amount)
        EmpCompLine.AmountSpecified = True
        EmpCompLine.CurrencyCode = Currency
        EmpCompLine.ComponentCode = ComponentCode
        EmpCompLine.Description = Description
        If Not FromDate = Nothing Then
            EmpCompLine.FromDate = FromDate
            EmpCompLine.FromDateSpecified = True
        Else
            EmpCompLine.FromDateSpecified = False
        End If
        EmpCompLine.class = ClassType

        EmpCompLine.PayType = EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Annual
        EmpCompLine.PayTypeSpecified = True

        If Not CategoryClass = Nothing Then
            EmpCompLine.CategoryClass = CategoryClass
        End If
        If Not ActualType = Nothing Then
            EmpCompLine.ActualType = ActualType
        End If
        If Not FromCity = "" Then
            EmpCompLine.SectorFromCity = FromCity
        End If
        If Not ToCity = "" Then
            EmpCompLine.SectorToCity = ToCity
        End If

        If Not FromCountry = "" Then
            EmpCompLine.SectorFromCountry = FromCountry
        End If
        If Not ToCountry = "" Then
            EmpCompLine.SectorToCountry = ToCountry
        End If
        If Not TicketType = "" Then
            EmpCompLine.TicketType = TicketType
        End If

        EmpCompLine.TicketsPerYear = TicketsPerYear
        EmpCompLine.TicketsPerYearSpecified = True
        'EmpCompLine.action = EmployeeCompensationService.AxdEnum_AxdEntityAction.create
        'EmpCompLine.actionSpecified = True


        Return EmpCompLine
    End Function

    'Private Function SaveEMPSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
    '    GetPayMonth_YearDetails()
    '    Dim iReturnvalue As Integer
    '    Try
    '        If Session("EMPSALDETAILS") Is Nothing Then
    '            Return 0
    '        End If
    '        Dim cmd As New SqlCommand
    '        Dim iIndex As Integer
    '        'Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
    '        Dim EMP_ID As Integer
    '        If Session("EMPSALDETAILS").Rows.Count > 0 Then
    '            For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
    '                Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
    '                Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
    '                If lblEMPID Is Nothing Then
    '                    Exit For
    '                Else
    '                    EMP_ID = lblEMPID.Text
    '                End If
    '                For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1
    '                    Dim dr As DataRow = Session("EMPSALDETAILS").Rows(iIndex)
    '                    cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
    '                    cmd.CommandType = CommandType.StoredProcedure

    '                    Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
    '                    sqlpESH_ID.Value = 0
    '                    cmd.Parameters.Add(sqlpESH_ID)

    '                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
    '                    sqlpUser.Value = Session("sUsr_name")
    '                    cmd.Parameters.Add(sqlpUser)

    '                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
    '                    sqlpESH_EMP_ID.Value = EMP_ID 'hf_EMP_IDs.Value
    '                    cmd.Parameters.Add(sqlpESH_EMP_ID)

    '                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
    '                    sqlpESH_BSU_ID.Value = Session("sBSUID")
    '                    cmd.Parameters.Add(sqlpESH_BSU_ID)

    '                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
    '                    sqlpESH_ERN_ID.Value = dr("ENR_ID")
    '                    cmd.Parameters.Add(sqlpESH_ERN_ID)

    '                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
    '                    sqlpESH_AMOUNT.Value = dr("Amount")
    '                    cmd.Parameters.Add(sqlpESH_AMOUNT)

    '                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
    '                    sqlpESH_ELIGIBILITY.Value = dr("Amount_ELIGIBILITY")
    '                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

    '                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
    '                    sqlpESH_bMonthly.Value = dr("bMonthly")
    '                    cmd.Parameters.Add(sqlpESH_bMonthly)

    '                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
    '                    sqlpESH_PAYTERM.Value = dr("PAYTERM")
    '                    cmd.Parameters.Add(sqlpESH_PAYTERM)

    '                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
    '                    sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
    '                    cmd.Parameters.Add(sqlpESH_CUR_ID)

    '                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
    '                    sqlpESH_PAY_CUR_ID.Value = ViewState("PAY_CUR_ID") 'FromDataBase
    '                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

    '                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
    '                    sqlpESH_PAYMONTH.Value = ViewState("PAYMONTH") 'FromDataBase
    '                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

    '                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
    '                    sqlpESH_PAYYEAR.Value = ViewState("PAYYEAR")
    '                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

    '                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
    '                    sqlpESH_FROMDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
    '                    cmd.Parameters.Add(sqlpESH_FROMDT)

    '                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
    '                    sqlpbDeleted.Value = False
    '                    cmd.Parameters.Add(sqlpbDeleted)

    '                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
    '                    sqlpESH_TODT.Value = DBNull.Value
    '                    cmd.Parameters.Add(sqlpESH_TODT)

    '                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
    '                    retSValParam.Direction = ParameterDirection.ReturnValue
    '                    cmd.Parameters.Add(retSValParam)

    '                    Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
    '                    retNewESH_ID.Direction = ParameterDirection.Output
    '                    cmd.Parameters.Add(retNewESH_ID)

    '                    cmd.ExecuteNonQuery()
    '                    iReturnvalue = retSValParam.Value
    '                    If iReturnvalue <> 0 Then
    '                        Exit For
    '                    End If
    '                Next
    '                iReturnvalue = UpdateEmployeeMaster(objConn, stTrans, EMP_ID, Session("sBSUID"), hfSalGrade.Value)
    '                If iReturnvalue <> 0 Then
    '                    Exit For
    '                End If
    '            Next
    '        End If
    '        Return iReturnvalue
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '        Return 1000
    '    End Try
    '    Return False
    'End Function

    Private Sub GetPayMonth_YearDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT BSU_PAYMONTH, BSU_PAYYEAR, BSU_CURRENCY FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            ViewState("PAYMONTH") = dr("BSU_PAYMONTH")
            ViewState("PAYYEAR") = dr("BSU_PAYYEAR")
            ViewState("PAY_CUR_ID") = dr("BSU_CURRENCY")
        End While
        dr.Close()
    End Sub

    Private Function UpdateEmployeeMaster(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal EMP_ID As String, ByVal BSU_ID As String, ByVal salGrade As String) As Integer
        Dim status As Integer
        Dim newNo As String = String.Empty
        Dim dt As New DataTable
        'Dim drSalScale As SqlDataReader
        Dim drSalScale As DataRow
        Dim str_sql As String = String.Empty
        str_sql = "select * from EMPLOYEE_M WHERE EMP_ID = '" & _
        EMP_ID & "' AND EMP_BSU_ID = '" & BSU_ID & "'"
        Dim cmd1 As SqlCommand = New SqlCommand(str_sql, conn, trans)
        cmd1.CommandType = CommandType.Text
        Dim sqlAdpt As New SqlDataAdapter(cmd1)
        sqlAdpt.Fill(dt)
        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                drSalScale = dt.Rows(0)
                status = AccessRoleUser.SAVEEMPLOYEE_M(GetVal(Session("sUsr_name")), GetVal(drSalScale("EMPNO")), GetVal(drSalScale("EMP_APPLNO")), _
                GetVal(drSalScale("EMP_SALUTE")), GetVal(drSalScale("EMP_FNAME")), GetVal(drSalScale("EMP_MNAME")), _
                GetVal(drSalScale("EMP_LNAME")), GetVal(drSalScale("EMP_DES_ID")), GetVal(drSalScale("EMP_ECT_ID")), _
                GetVal(drSalScale("EMP_EGD_ID")), GetVal(drSalScale("EMP_BSU_ID")), GetVal(drSalScale("EMP_TGD_ID")), _
                GetVal(drSalScale("EMP_VISATYPE")), GetVal(drSalScale("EMP_VISASTATUS")), _
                GetVal(drSalScale("EMP_JOINDT")), GetVal(drSalScale("EMP_BSU_JOINDT")), GetVal(drSalScale("EMP_LASTVACFROM")), _
                GetVal(drSalScale("EMP_LASTVACTO")), GetVal(drSalScale("EMP_LASTREJOINDT")), GetVal(drSalScale("EMP_bOVERSEAS")), _
                GetVal(drSalScale("EMP_STATUS")), GetVal(drSalScale("EMP_RESGDT")), GetVal(drSalScale("EMP_LASTATTDT")), _
                GetVal(drSalScale("EMP_PROBTILL")), GetVal(drSalScale("EMP_ACCOMODATION")), GetVal(drSalScale("EMP_bACTIVE")), _
                GetVal(drSalScale("EMP_CUR_ID")), GetVal(drSalScale("EMP_PAY_CUR_ID")), GetVal(drSalScale("EMP_MODE")), _
                GetVal(drSalScale("EMP_BANK")), GetVal(drSalScale("EMP_ACCOUNT")), GetVal(drSalScale("EMP_SWIFTCODE")), GetVal(drSalScale("EMP_MOE_DES_ID")), _
                GetVal(drSalScale("EMP_VISA_DES_ID")), GetVal(drSalScale("EMP_VISA_BSU_ID")), GetVal(drSalScale("EMP_MARITALSTATUS")), _
                GetVal(drSalScale("EMP_GROSSSAL")), GetVal(drSalScale("EMP_PASSPORT")), GetVal(drSalScale("EMP_REMARKS")), GetVal(drSalScale("EMP_CTY_ID")), _
                GetVal(drSalScale("EMP_SEX_bMALE")), GetVal(drSalScale("EMP_QLF_ID")), salGrade, GetVal(drSalScale("EMP_DPT_ID")), _
                GetVal(drSalScale("EMP_BLOODGRP")), GetVal(drSalScale("EMP_FATHER")), GetVal(drSalScale("EMP_MOTHER")), _
                GetVal(drSalScale("EMP_REPORTTO_EMP_ID")), GetVal(drSalScale("EMP_BLS_ID")), GetVal(drSalScale("EMP_STAFFNO")), _
                GetVal(drSalScale("EMP_ABC")), GetVal(drSalScale("EMP_bOT"), SqlDbType.Bit), EMP_ID, _
                GetVal(drSalScale("EMp_bTEmp"), SqlDbType.Bit), GetVal(drSalScale("EMP_bCompanyTransport"), SqlDbType.Bit), GetVal(drSalScale("EMP_DOB"), SqlDbType.DateTime), _
                GetVal(drSalScale("EMP_RLG_ID")), GetVal(drSalScale("EMP_LPS_ID")), GetVal(drSalScale("EMP_CONTRACTTYPE"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_MAXCHILDCONESSION"), SqlDbType.Int), GetVal(drSalScale("EMP_LOP_OPN"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_FTE"), SqlDbType.Decimal), GetVal(drSalScale("EMP_bReqPunching"), SqlDbType.Bit), _
                GetVal(drSalScale("EMP_TICKETFLAG"), SqlDbType.Int), GetVal(drSalScale("EMP_TICKETCLASS")), GetVal(drSalScale("EMP_TICKETCOUNT"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_TICKET_CIT_ID")), GetVal(drSalScale("EMP_VISA_ID")), GetVal(drSalScale("EMP_PERSON_ID")), True, newNo, EMP_ID, GetVal(drSalScale("EMP_PASSPORTNAME")), conn, trans)
                Return status
            End If
        End If
        Return -1
    End Function

    Private Function GetVal(ByVal obj As Object, Optional ByVal datatype As SqlDbType = SqlDbType.VarChar) As Object
        If obj Is DBNull.Value Then
            Select Case datatype
                Case SqlDbType.Decimal
                    Return 0
                Case SqlDbType.Int
                    Return 0
                Case SqlDbType.Bit
                    Return False
                Case SqlDbType.VarChar
                    Return ""
            End Select
        Else
            Return obj
        End If
        Return ""
    End Function

    Protected Sub gvEmpSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpSalary.RowDataBound
        Try
            Dim lblSchedule As New Label
            Dim lblPayTerm As New Label
            lblSchedule = e.Row.FindControl("lblSchedule")
            lblPayTerm = e.Row.FindControl("lblPayTerm")
            If lblSchedule IsNot Nothing Then
                Select Case lblPayTerm.Text
                    Case 0
                        lblSchedule.Text = "Monthly"
                    Case 1
                        lblSchedule.Text = "Bimonthly"
                    Case 2
                        lblSchedule.Text = "Quarterly"
                    Case 3
                        lblSchedule.Text = "Half-yearly"
                    Case 4
                        lblSchedule.Text = "Yearly"
                End Select
            End If
        Catch
        End Try
    End Sub

    Protected Sub btnSalRevCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalRevCancel.Click
        Session("EMPSALSCHEDULE") = Nothing
        Session("EMPSALDETAILS") = Nothing
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearEMPSALDETAILS()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgEmpSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEMPSel.Click
        Session("EMPSALDETAILS") = Nothing
        Session("ADDEDEMPSALDETAILS") = Nothing
        Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        PopulateSalaryData(condition)
        Gridbind()
        GridBindSalaryDetails()
        tblSalDetails.Visible = True

        trEditSalAmt.Visible = False
        trEditSalAmt1.Visible = False
        trEditSalAmt2.Visible = False
        trEditSalAmt3.Visible = False
        trEditSalAmt4.Visible = False

        trSalScale1.Visible = True
        trSalScale2.Visible = True
        trSalScale3.Visible = True
        lblError.Text = ""
        txtSalGrade.Text = ""
        hfSalGrade.Value = ""
        txtGrossSalary.Text = ""

        'FillCurrentSalDetails(hf_EMP_IDs.Value)
    End Sub

    Protected Sub btnSalCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalCancel.Click
        ClearEMPSALDETAILS()
        ddSalEarnCode.Enabled = True
    End Sub

    Protected Sub btnSalScale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalScale.Click
        'Dim dtable As DataTable = CreateSalaryDetailsTable()
        If hfSalGrade.Value = "" Then Return
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal
        Dim fteVal As Double = 1
        str_Sql = "Select EMPGRADES_M.EGD_ID FROM EMPGRADES_M RIGHT OUTER JOIN" & _
        " EMPSCALES_GRADES_M ON EMPGRADES_M.EGD_ID = EMPSCALES_GRADES_M.SGD_EGD_ID " & _
        " WHERE EMPSCALES_GRADES_M.SGD_ID =" & hfSalGrade.Value
        'Dim objGrade As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        'If Not objGrade Is DBNull.Value Then
        '    hfGrade_ID.Value = objGrade.ToString
        'End If

        str_Sql = "select EMPGRADE_SALARY_S.*, ERN_DESCR from EMPGRADE_SALARY_S " & _
        " EMPGRADE_SALARY_S LEFT OUTER JOIN EMPSALCOMPO_M ON EMPGRADE_SALARY_S.GSS_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        "WHERE GSS_SGD_ID =" & hfSalGrade.Value & " and GSS_DTTO is null ORDER BY ERN_ORDER"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'If Session("EMPSALSCHEDULE") Is Nothing Then
        '    Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        'End If
        Session("ADDEDEMPSALDETAILS") = CreateSalaryRevDetailsTable()

        Dim dTemptable As DataTable = Session("ADDEDEMPSALDETAILS")
        For index As Integer = 0 To dTemptable.Rows.Count - 1
            dTemptable.Rows(index)("Status") = "DELETED"
        Next
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            grossSal += dr("GSS_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("EMP_ID") = GetNextESHID()
            ldrTempNew.Item("ENR_ID") = dr("GSS_ERN_ID")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = True
            ldrTempNew.Item("PAYTERM") = 0
            ldrTempNew.Item("Amount") = dr("GSS_AMOUNT") * fteVal
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("GSS_AMOUNT")
            ldrTempNew.Item("Status") = "EDITED"
            dTemptable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("ADDEDEMPSALDETAILS") = dTemptable
        GridBindSalaryDetails()

        UpdateSalaryScale()
        Gridbind()
        UpdateSalaryAmounts()
    End Sub

    Private Sub GridBindSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPSALDETAILS") Is Nothing Then Return
        If Session("ADDEDEMPSALDETAILS") Is Nothing Then
            gvEmpSalary.DataSource = Nothing
            gvEmpSalary.DataBind()
            Return
        End If

        Dim i As Integer
        Dim grossSal As Double
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateSalaryRevDetailsTable()
        If Session("ADDEDEMPSALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("ADDEDEMPSALDETAILS").Rows.Count - 1
                If (Session("ADDEDEMPSALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("ADDEDEMPSALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                            If Not Session("ADDEDEMPSALDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        Session("EMPSALGrossSalary") = grossSal
        gvEmpSalary.DataSource = dtTempDtl
        gvEmpSalary.DataBind()
        txtGrossSalary.Text = Session("EMPSALGrossSalary")
    End Sub

    Private Function GetNextESHID() As Integer
        If ViewState("ESH_ID") Is Nothing Then
            Dim cmd As New SqlCommand
            cmd.Connection = ConnectionManger.GetOASISConnection()
            cmd.CommandText = "select isnull(max(ESH_ID),0) from EMPSALARYHS_s "
            Dim GSS_ID As Integer = CInt(cmd.ExecuteScalar())
            ViewState("ESH_ID") = GSS_ID
        Else
            ViewState("ESH_ID") += 1
        End If
        Return ViewState("ESH_ID")
    End Function

    Protected Sub radSalScale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSalScale.CheckedChanged
        Session("ADDEDEMPSALDETAILS") = Nothing
        ClearNewAmounts()
        Gridbind()
        GridBindAddSalaryDetails()
        hfSalGrade.Value = ""
        txtSalGrade.Text = ""
        txtGrossSalary.Text = ""
        GridBindSalaryDetails()
        trEditSalAmt.Visible = False
        trEditSalAmt1.Visible = False
        trEditSalAmt2.Visible = False
        trEditSalAmt3.Visible = False
        trEditSalAmt4.Visible = False

        trSalScale1.Visible = True
        trSalScale2.Visible = True
        trSalScale3.Visible = True

    End Sub

    Protected Sub radAddAmount_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAddAmount.CheckedChanged

        Session("ADDEDEMPSALDETAILS") = Nothing
        GridBindSalaryDetails()
        Gridbind()
        ClearNewAmounts()
        txtGrossSalary.Text = ""
        trEditSalAmt.Visible = True
        trEditSalAmt1.Visible = True
        trEditSalAmt2.Visible = True
        trEditSalAmt3.Visible = True
        trEditSalAmt4.Visible = True

        trSalScale1.Visible = False
        trSalScale2.Visible = False
        trSalScale3.Visible = False
    End Sub

    Protected Sub gvSalRevisionBulk_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSalRevisionBulk.RowDataBound
        Try
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                'hlview.NavigateUrl = "PopupSalaryCompareDetails.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text)
                hlview.NavigateUrl = "javascript:ViewDetails('viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "')"
            End If
            Dim dtTempDtl As DataTable = Session("EMPSALDETAILS")
            'Dim dRows() As DataRow
            'dRows = dtTempDtl.Select("EMP_ID = '" & lblEMPID.Text & "'")
            'If dRows.Length > 0 Then
            '    For iColCount As Integer = 0 To dRows.Length - 1
            '        Dim amt As Double = GetVal(dRows(iColCount)("Amount"), SqlDbType.Decimal)
            '        If Not dRows(iColCount)("bMonthly") Then
            '            amt = amt \ 12
            '        End If
            '        grossSal += amt
            '        amt = GetVal(dRows(iColCount)("Amount_NEW"), SqlDbType.Decimal)
            '        If Not dRows(iColCount)("bMonthly") Then
            '            amt = amt \ 12
            '        End If
            '        grossSalNew += amt
            '    Next
            'End If
            'Dim lblEMPAMOUNT As Label = e.Row.FindControl("lblCurrSalAmt")
            'If lblEMPAMOUNT Is Nothing Then
            '    Exit Sub
            'Else
            '    lblEMPAMOUNT.Text = grossSal
            'End If
            'Dim lblEMPAMOUNTNEW As Label = e.Row.FindControl("lblRevSalAmt")
            'If lblEMPAMOUNTNEW Is Nothing Then
            '    Exit Sub
            'Else
            '    lblEMPAMOUNTNEW.Text = grossSalNew
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub optManual_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optManual.CheckedChanged
        If Me.optManual.Checked Then
            Me.tblUpload.Visible = False
            Me.Table1.Visible = True
            'Me.tblSalDetails.Visible = True
        Else
            Me.tblUpload.Visible = True
            Me.Table1.Visible = False
            Me.tblSalDetails.Visible = False
        End If
    End Sub

    Protected Sub optUpload_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optUpload.CheckedChanged
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")
        'smScriptManager.RegisterPostBackControl(btnLoad)
        If Me.optUpload.Checked Then
            Me.tblUpload.Visible = True
            Me.Table1.Visible = False
            Me.tblSalDetails.Visible = False
        Else
            Me.tblUpload.Visible = False
            Me.Table1.Visible = True
            'Me.tblSalDetails.Visible = True
        End If
    End Sub

    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ext = ".xls" Then 'Or ext = ".xlsx" Then
                '"C:\Users\swapna.tv\Desktop\MonthlyDed.xls"

                'getdataExcel(strFileName)
                UpLoadDBF()
            Else
                'lblError.Text = "Please upload .xls or .xlsx files only."
                lblError.Text = "Please upload .xls files only."
            End If

        Else
            lblError.Text = "File not uploaded"
        End If

        'Dim DS As New DataSet
        'MyCommand.Fill(DS)
        'Dim Dt As DataTable = DS.Tables(0)
        'gvNewdata.DataSource = Dt
        'gvNewdata.DataBind()
    End Sub

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 7)
            'xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 5)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(0).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()

            Dim tmpTable As New DataTable
            tmpTable.Columns.Add("EmpNo", GetType(System.String))
            tmpTable.Columns.Add("EmpName", GetType(System.String))
            tmpTable.Columns.Add("EarningCode", GetType(System.String))
            tmpTable.Columns.Add("ActualAmount", GetType(System.Decimal))
            tmpTable.Columns.Add("EligibleAmount", GetType(System.Decimal))
            tmpTable.Columns.Add("EffectiveDate", GetType(System.String))
            tmpTable.Columns.Add("FromDate", GetType(System.String))
            tmpTable.Columns.Add("EarningDesc", GetType(System.String))
            tmpTable.AcceptChanges()

            'copy the data from xltable to tmptable
            Dim copyrow As DataRow
            For Each trow As DataRow In xltable.Rows
                copyrow = tmpTable.NewRow
                copyrow.Item("EmpNo") = trow.Item(0)
                copyrow.Item("EmpName") = trow.Item(1)
                copyrow.Item("EarningCode") = trow.Item(2)
                copyrow.Item("ActualAmount") = trow.Item(3)
                copyrow.Item("EligibleAmount") = trow.Item(4)
                copyrow.Item("EffectiveDate") = trow.Item(5)
                copyrow.Item("FromDate") = trow.Item(6)
                tmpTable.Rows.Add(copyrow)
            Next
            tmpTable.AcceptChanges()

            'delete xltable from the memory and recreate it from tmpTable
            xltable.Clear()
            xltable = Nothing
            xltable = tmpTable.Copy

            'delete tmpTable from the memory
            tmpTable.Clear()
            tmpTable = Nothing

            ColName = xltable.Columns(1).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblError.Text = "Employee No. cannot be blank."
                End If
            Next

            Dim strEarnDescr As String = ""
            ColName = xltable.Columns(3).ColumnName
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEarnDescr &= IIf(strEarnDescr <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                End If
            Next

            If strEmpNos = "" Then
                lblError.Text = "No Data to Import"
                Exit Sub
            End If

            Dim tmpstr As String = ""
            Dim tmpEmpNo As String
            Dim query As String = ""
            If Not xltable Is Nothing Then
                xltable.Columns.Add("emp_id", GetType(Int32))
                xltable.Columns.Add("emp_name", GetType(String))
                xltable.Columns.Add("bsu_id", GetType(String))
                xltable.Columns.Add("bsu_name", GetType(String))
                xltable.Columns.Add("current_amount", GetType(Decimal))
                xltable.Columns.Add("pay_term", GetType(Integer))
                xltable.Columns.Add("bActive", GetType(Integer))
                xltable.Columns.Add("Save", GetType(Integer))
                xltable.Columns.Add("bUploaded", GetType(Integer))
                For Each row As DataRow In xltable.Rows
                    row.Item("ActualAmount") = Format(Convert.ToDecimal(row.Item("ActualAmount")), "0.00")
                    row.Item("EligibleAmount") = Format(Convert.ToDecimal(row.Item("EligibleAmount")), "0.00")
                    If row.Item("EffectiveDate").ToString.Contains("/") Then
                        tmpstr = row.Item("EffectiveDate").ToString.Split(" ")(0)
                        row.Item("EffectiveDate") = tmpstr.Split("/")(0) & "-" & tmpstr.Split("/")(1) & "-" & tmpstr.Split("/")(2)
                    End If
                    If row.Item("FromDate").ToString.Contains("/") Then
                        tmpstr = row.Item("FromDate").ToString.Split(" ")(0)
                        row.Item("FromDate") = tmpstr.Split("/")(0) & "-" & tmpstr.Split("/")(1) & "-" & tmpstr.Split("/")(2)
                    End If
                    tmpEmpNo &= "'" & row.Item("empno") & "',"
                Next

                If tmpEmpNo.ToString.EndsWith(",") Then
                    tmpEmpNo = tmpEmpNo.ToString.TrimEnd(",")
                End If

                'query = "SELECT esh.ESH_EMP_ID, e.empno,e.EMP_bACTIVE as bActive,  esh.ESH_BSU_ID, e.EMP_FNAME + ' ' + ISNULL(e.EMP_MNAME,'') + ' ' + ISNULL(e.EMP_LNAME,'') AS emp_name, bsu.BSU_NAME, esh.esh_ern_id, esh.esh_amount, esh.esl_payterm  FROM dbo.EMPSALARYhs_S ESH RIGHT OUTER JOIN dbo.EMPLOYEE_M E ON esh.ESH_EMP_ID = e.EMP_ID INNER JOIN dbo.BUSINESSUNIT_M bsu ON esh.ESH_BSU_ID = bsu.BSU_ID WHERE e.EMPNO IN (" & tmpEmpNo & ")"
                'query = "SELECT  e.EMP_ID , e.empno , e.EMP_bACTIVE AS bActive , e.emp_BSU_ID , e.EMP_FNAME + ' ' + ISNULL(e.EMP_MNAME, '') + ' ' + ISNULL(e.EMP_LNAME,'') AS emp_name , bsu.BSU_NAME ,esh.esh_ern_id ,esh.esh_amount ,esh.esl_payterm FROM    dbo.EMPSALARYhs_S ESH RIGHT OUTER JOIN dbo.EMPLOYEE_M E ON esh.ESH_EMP_ID = e.EMP_ID INNER JOIN dbo.BUSINESSUNIT_M bsu ON e.EMP_BSU_ID = bsu.BSU_ID WHERE e.EMPNO IN (" & tmpEmpNo & ")"

                query = "SELECT  e.EMP_ID , e.empno , case Isnull(e.EMP_bACTIVE,0) when 0 then '0' when 1 then '1' end AS bActive , e.emp_BSU_ID , e.EMP_FNAME + ' ' + ISNULL(e.EMP_MNAME, '') + ' ' + ISNULL(e.EMP_LNAME,'') AS emp_name , bsu.BSU_NAME ,esh.esh_ern_id, dbo.EMPSALCOMPO_M.ERN_DESCR, esh.esh_amount ,esh.esl_payterm FROM    dbo.EMPLOYEE_M E LEFT OUTER JOIN dbo.EMPSALARYhs_S ESH ON esh.ESH_EMP_ID = e.EMP_ID INNER JOIN dbo.BUSINESSUNIT_M bsu ON e.EMP_BSU_ID = bsu.BSU_ID INNER JOIN dbo.EMPSALCOMPO_M ON EMPSALCOMPO_M.ERN_ID = ESH.ESH_ERN_ID WHERE e.EMPNO IN (" & tmpEmpNo & ") and esh_todt is null AND e.EMP_BSU_ID = '" & Session("sBsuId") & "'"
                'query = "SELECT  e.EMP_ID , e.empno , case Isnull(e.EMP_bACTIVE,0) when 0 then '0' when 1 then '1' end AS bActive , e.emp_BSU_ID , e.EMP_FNAME + ' ' + ISNULL(e.EMP_MNAME, '') + ' ' + ISNULL(e.EMP_LNAME,'') AS emp_name , bsu.BSU_NAME ,esh.esh_ern_id, dbo.EMPSALCOMPO_M.ERN_DESCR, esh.esh_amount ,esh.esl_payterm FROM    dbo.EMPLOYEE_M E LEFT OUTER JOIN dbo.EMPSALARYhs_S ESH ON esh.ESH_EMP_ID = e.EMP_ID INNER JOIN dbo.BUSINESSUNIT_M bsu ON e.EMP_BSU_ID = bsu.BSU_ID Left Outer JOIN dbo.EMPSALCOMPO_M ON EMPSALCOMPO_M.ERN_ID = ESH.ESH_ERN_ID WHERE e.EMPNO IN (" & tmpEmpNo & ") and esh_todt is null AND e.EMP_BSU_ID = '" & Session("sBsuId") & "'"
                Dim tmpds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)

                Dim xlrows(), tmprows() As DataRow, tmpEMProws() As DataRow, EarningDesc As String
                If Not tmpds Is Nothing Then
                    For Each xlRow1 As DataRow In xltable.Rows
                        tmprows = tmpds.Tables(0).Select("EmpNo = " & xlRow1.Item("EmpNo") & " And esh_ern_id = '" & xlRow1.Item("EarningCode") & "'")
                        tmpEMProws = tmpds.Tables(0).Select("EmpNo = " & xlRow1.Item("EmpNo"))
                        If tmprows.Length > 0 Then
                            xlRow1.Item("emp_id") = tmprows(0).Item("emp_id")
                            xlRow1.Item("emp_name") = tmprows(0).Item("emp_name")
                            xlRow1.Item("bsu_id") = tmprows(0).Item("emp_bsu_id")
                            xlRow1.Item("bsu_name") = tmprows(0).Item("bsu_name")
                            xlRow1.Item("current_amount") = Format(Convert.ToDecimal(tmprows(0).Item("esh_amount")), "0.00")
                            xlRow1.Item("pay_term") = tmprows(0).Item("esl_payterm")
                            xlRow1.Item("bActive") = tmprows(0).Item("bACTIVE").ToString
                            xlRow1.Item("EarningDesc") = tmprows(0).Item("Ern_Descr").ToString
                        ElseIf tmpEMProws.Length > 0 Then
                            xlRow1.Item("emp_id") = tmpEMProws(0).Item("emp_id")
                            xlRow1.Item("emp_name") = tmpEMProws(0).Item("emp_name")
                            xlRow1.Item("bsu_id") = tmpEMProws(0).Item("emp_bsu_id")
                            xlRow1.Item("bsu_name") = tmpEMProws(0).Item("bsu_name")
                            xlRow1.Item("current_amount") = Format(Convert.ToDecimal(0), "0.00")
                            xlRow1.Item("pay_term") = 0
                            xlRow1.Item("bActive") = "1"
                            'xlRow1.Item("EarningDesc") = tmpEMProws(0).Item("Ern_Descr").ToString
                            Dim EarnDescr, EarnId As String
                            Dim CurrentAmount As Decimal = 0
                            EarnDescr = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ERN_DESCR FROM dbo.EMPSALCOMPO_M WHERE ERN_ID = '" & xlRow1.Item("earningcode") & "' or Ern_Descr = '" & xlRow1.Item("earningcode") & "'")
                            EarnId = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ERN_Id FROM dbo.EMPSALCOMPO_M WHERE ERN_Descr = '" & EarnDescr & "'")
                            CurrentAmount = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT  ISNULL(ESH_AMOUNT,0) AS CurrentAmount FROM dbo.EMPSALARYHS_s WHERE ESH_EMP_ID = " & tmpEMProws(0).Item("emp_id") & " AND ESH_ERN_ID = '" & EarnId & "' AND ESH_TODT IS NULL")
                            xlRow1.Item("EarningDesc") = EarnDescr
                            xlRow1.Item("current_amount") = CurrentAmount
                        Else
                            'EarningDesc = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ERN_DESCR FROM dbo.EMPSALCOMPO_M WHERE ERN_ID = '" & xlRow1.Item("earningcode") & "'")
                            'If Not EarningDesc = Nothing Then
                            '    xlRow1.Item("EarningDesc") = EarningDesc
                            'Else
                            '    xlRow1.Item("EarningDesc") = xlRow1.Item("earningcode")
                            'End If
                            'xlRow1.Item("emp_name") = xlRow1.Item("empname")
                            ''xlRow1.Item("EarningDesc") = xlRow1.Item("earningcode")

                            With SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT emp_id, EMP_BSU_ID, ISNULL(EMP_bACTIVE,0) AS EMP_bACTIVE, BSU_NAME FROM dbo.EMPLOYEE_M INNER JOIN dbo.BUSINESSUNIT_M ON EMP_BSU_ID = BSU_ID WHERE empno = '" & xlRow1.Item("EmpNo") & "'")
                                Do While .Read
                                    xlRow1.Item("emp_id") = .Item("emp_id")
                                    xlRow1.Item("emp_name") = xlRow1.Item("EmpName")
                                    xlRow1.Item("bsu_id") = .Item("emp_bsu_id")
                                    xlRow1.Item("bsu_name") = .Item("bsu_name")
                                    xlRow1.Item("bActive") = "1"
                                Loop
                                .Close()
                            End With
                            Dim EarnDescr, EarnId As String
                            Dim CurrentAmount As Decimal = 0
                            EarnDescr = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ERN_DESCR FROM dbo.EMPSALCOMPO_M WHERE ERN_ID = '" & xlRow1.Item("earningcode") & "' or Ern_Descr = '" & xlRow1.Item("earningcode") & "'")
                            EarnId = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ERN_Id FROM dbo.EMPSALCOMPO_M WHERE ERN_Descr = '" & EarnDescr & "'")
                            CurrentAmount = Format(Convert.ToDecimal(0), "0.00")
                            xlRow1.Item("EarningDesc") = EarnDescr
                            xlRow1.Item("current_amount") = CurrentAmount
                            xlRow1.Item("pay_term") = 0
                        End If
                    Next
                End If

                xltable.AcceptChanges()

                'get current amount of the salary component
                Me.gvUpload.AllowPaging = False
                Me.gvUpload.DataSource = xltable
                Me.gvUpload.DataBind()
                Session("BulkSalaryRevision_UploadedSalary") = xltable
                ViewState("datamode") = "add"
            End If

            'check that all the employees imported from excel are active in the business unit
            Dim j As Integer = 0
            Dim lblActive As Label, lblEmpNo As Label, lblBsuId As Label, InactiveEmployee As Boolean
            Dim Msg As String = ""
            For Each row As GridViewRow In Me.gvUpload.Rows
                j += 1
                lblEmpNo = row.FindControl("lblEmpNo")
                lblActive = row.FindControl("lblbActive")
                lblBsuId = row.FindControl("lblBsuId")
                ''If Not lblActive Is Nothing Then
                ''If lblActive.Text = "" OrElse lblActive.Text = "0" Then
                If Not lblActive Is Nothing OrElse Not lblBsuId Is Nothing Then
                    If lblActive.Text = "" OrElse lblActive.Text = "0" OrElse lblBsuId.Text = "" OrElse lblBsuId.Text <> Session("sbsuid") Then
                        row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                        InactiveEmployee = True

                        'added on 16th sep 2014
                        'Dim EmpBsu As String = ""
                        'EmpBsu = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BSU_NAME,'') AS BSU_NAME FROM dbo.EMPLOYEE_M INNER JOIN dbo.BUSINESSUNIT_M ON EMP_BSU_ID = BSU_ID WHERE EMPNo = " & lblEmpNo.Text)
                        Msg = Replace(Replace(UtilityObj.getErrorMessage(1705), "*", lblEmpNo.Text), "@", j + 1)
                        lblError.Text = Msg
                        Me.btnSaveUpload.Visible = False
                        Me.gvUpload.DataSource = Nothing
                        Me.gvUpload.DataBind()
                        Exit Sub
                    End If
                End If
            Next

            If InactiveEmployee Then
                'Me.lblError.Text = "Highlighted employee(s) are not active in the business unit. Kindly update the excel and try uploading again."
                lblError.Text = UtilityObj.getErrorMessage(1703)
                Me.btnSaveUpload.Visible = False
                Exit Sub
            Else
                Me.lblError.Text = Nothing
                Me.btnSaveUpload.Visible = True
            End If


            'check that all the salary types imported from excel are defined in the system
            Dim lblEarningCode As Label, lblUndefinedEarningCode As Label, InactiveSalaryType As Boolean
            Dim i As Integer = 0
            Msg = ""
            For Each row As GridViewRow In Me.gvUpload.Rows
                i += 1
                lblEarningCode = row.FindControl("lblEarningCode")
                lblUndefinedEarningCode = row.FindControl("lblUndefinedEarningCode")
                If Not lblEarningCode Is Nothing Then
                    If lblEarningCode.Text = "" Then
                        lblEarningCode.Text = lblUndefinedEarningCode.Text
                        row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                        Msg &= lblUndefinedEarningCode.Text & " : excel row no " & i + 1 & ", "
                        InactiveSalaryType = True
                    End If
                End If
            Next

            If Msg.EndsWith(", ") Then
                Msg = Msg.Remove(Msg.Length - 2, 2)
            End If

            If InactiveSalaryType Then
                lblError.Text = UtilityObj.getErrorMessage(1704) & Msg
                Me.btnSaveUpload.Visible = False
                Me.gvUpload.DataSource = Nothing
                Me.gvUpload.DataBind()
                Exit Sub
            Else
                Me.lblError.Text = Nothing
                Me.btnSaveUpload.Visible = True
            End If

            Me.gvUpload.AllowPaging = True
            Me.gvUpload.DataSource = xltable
            Me.gvUpload.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub

    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            If Path.GetExtension(flUpExcel.FileName).ToString = ".xls" Then
                FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 4)
            ElseIf Path.GetExtension(flUpExcel.FileName).ToString = ".xlsx" Then
                FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 5)
            End If

            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                    'lblError.Text = Nothing
                Catch ex As Exception
                    'Errorlog(ex.Message)
                    lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub

    Public Shared Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("Entry not found for error # " & p_errorno)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ("Entry not found for error # " & p_errorno)
        End Try
    End Function


    Protected Sub btnSaveUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveUpload.Click
        'If CDate(txtRevisionDate.Text).Day <> 1 Then
        '    lblError.Text = "Effective date should be the first day of the month"
        '    Return
        'End If

        If Session("BulkSalaryRevision_UploadedSalary") Is Nothing Or Me.gvUpload.Rows.Count <= 0 Then
            Me.lblError.Text = "Kindly upload the data first before saving"
            Exit Sub
        End If
        If Session("BSU_IsHROnDAX") = 1 Then
            btnSaveUploadToAXandOASIS_Click(Nothing, Nothing)
            Exit Sub
        End If
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim transaction As SqlTransaction = conn.BeginTransaction("SampleTransaction")
            'If CheckPayMonth() Then
            Dim errorNo As Integer
            Dim ErrEmpNo, ErrEmpName As String
            errorNo = SaveEMPAddToSalaryDetails_Upload(conn, transaction, ErrEmpNo, ErrEmpName)

            'If radSalScale.Checked Then
            '    errorNo = SaveEMPSalaryDetails(conn, transaction)
            'ElseIf radAddAmount.Checked Then
            '    errorNo = SaveEMPAddToSalaryDetails(conn, transaction)
            'End If
            'If errorNo = 0 Then errorNo = SaveEMPSalarySchedule(conn, transaction)
            If errorNo = 0 Then
                If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), hf_EMP_IDs.Value, "Salary Revised", Page.User.Identity.Name.ToString, Nothing) <> 0 Then
                    transaction.Rollback()
                    lblError.Text = "Failed to update Audit Trail"
                    UtilityObj.Errorlog("Failed to update Audit Trail")
                Else
                    transaction.Commit()
                    lblError.Text = " Salary Revised Successfully"
                    ClearAllDetails_Upload()
                End If
            ElseIf errorNo = 1702 Then
                'Me.lblError.Text = "Salary data already uploaded for the highlighted employee(s) for the entered period. Kindly update the excel and try uploading again."
                lblError.Text = UtilityObj.getErrorMessage(errorNo)
                transaction.Rollback()
            Else
                lblError.Text = UtilityObj.getErrorMessage(errorNo) & " Employee: " & ErrEmpNo & " - " & ErrEmpName
                transaction.Rollback()
            End If
        End Using
    End Sub

    Protected Sub btnCancelUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUpload.Click
        Session("BulkSalaryRevision_UploadedSalary") = Nothing
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Me.gvUpload.DataSource = Nothing
            Me.gvUpload.DataBind()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub gvUpload_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUpload.PageIndexChanging
        If Not Session("BulkSalaryRevision_UploadedSalary") Is Nothing Then
            gvUpload.PageIndex = e.NewPageIndex
            gvUpload.DataSource = CType(Session("BulkSalaryRevision_UploadedSalary"), DataTable)
            gvUpload.DataBind()
        End If

        'check that all the employees imported from excel are active in the business unit
        Dim lblActive As Label, InactiveEmployee As Boolean
        For Each row As GridViewRow In Me.gvUpload.Rows
            lblActive = row.FindControl("lblbActive")
            If Not lblActive Is Nothing Then
                If lblActive.Text = "" OrElse lblActive.Text = 0 Then
                    row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                    'row.ForeColor = Drawing.Color.White
                    InactiveEmployee = True
                End If
            Else
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                'row.ForeColor = Drawing.Color.White
                InactiveEmployee = True
            End If
        Next
        If InactiveEmployee Then
            'Me.lblError.Text = "Highlighted employee(s) are not active in the business unit. Kindly update the excel and try uploading again."
            lblError.Text = UtilityObj.getErrorMessage(1703)
            Me.btnSaveUpload.Visible = False
        Else
            Me.btnSaveUpload.Visible = True
        End If

        'Check if any of the grid's record is previously already uploaded
        Dim UploadedRecordExists As Boolean = False
        Dim lblUploaded As Label
        For Each row As GridViewRow In Me.gvUpload.Rows
            lblUploaded = row.FindControl("lblUploaded")
            If Not lblUploaded Is Nothing Then
                If lblUploaded.Text <> "" AndAlso lblUploaded.Text = 1 Then
                    row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F5A9")
                    UploadedRecordExists = True
                End If
            End If
        Next
        If UploadedRecordExists Then
            lblError.Text = UtilityObj.getErrorMessage(1702)
        End If

    End Sub

    Protected Sub gvUpload_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUpload.RowDataBound
        Try
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("lnkView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                'ViewState("datamode") = Encr_decrData.Encrypt("view")
                'hlview.NavigateUrl = "PopupSalaryCompareDetails.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text)
                hlview.NavigateUrl = "javascript:ViewDetails('viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "')"
                'hlview.NavigateUrl = "javascript:ViewDetails('viewid=" & lblEMPID.Text & "')"
            End If
            Dim dtTempDtl As DataTable = Session("EMPSALDETAILS")
            'Dim dRows() As DataRow
            'dRows = dtTempDtl.Select("EMP_ID = '" & lblEMPID.Text & "'")
            'If dRows.Length > 0 Then
            '    For iColCount As Integer = 0 To dRows.Length - 1
            '        Dim amt As Double = GetVal(dRows(iColCount)("Amount"), SqlDbType.Decimal)
            '        If Not dRows(iColCount)("bMonthly") Then
            '            amt = amt \ 12
            '        End If
            '        grossSal += amt
            '        amt = GetVal(dRows(iColCount)("Amount_NEW"), SqlDbType.Decimal)
            '        If Not dRows(iColCount)("bMonthly") Then
            '            amt = amt \ 12
            '        End If
            '        grossSalNew += amt
            '    Next
            'End If
            'Dim lblEMPAMOUNT As Label = e.Row.FindControl("lblCurrSalAmt")
            'If lblEMPAMOUNT Is Nothing Then
            '    Exit Sub
            'Else
            '    lblEMPAMOUNT.Text = grossSal
            'End If
            'Dim lblEMPAMOUNTNEW As Label = e.Row.FindControl("lblRevSalAmt")
            'If lblEMPAMOUNTNEW Is Nothing Then
            '    Exit Sub
            'Else
            '    lblEMPAMOUNTNEW.Text = grossSalNew
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    'Private Function Get6DigitRandomNumber() As Integer
    '    Dim Min As Integer = 111111
    '    Dim Max As Integer = 999999
    '    Static Generator As System.Random = New System.Random()
    '    Return Generator.Next(Min, Max)
    'End Function

    Private Function GetActualAmount(ByVal Amount As String) As Decimal
        GetActualAmount = 0
        'Return Replace(Amount, ",", Nothing)
        Return Val(Replace(Amount, ",", Nothing))
    End Function

    'Private Function SendToDAX_Employee(ByVal tr As SqlTransaction, ByVal RecId As Integer, ByVal EmpId As String, ByVal Pos_Id As String, ByVal ActionState As String, CompensationId As String, ValidFrom As Date, ValidTo As Date, gvCompensation As GridView, ByRef ServiceErrorMsg As String) As Boolean
    '    SendToDAX_Employee = False
    '    'Private Sub SendToDAX(ByVal RecId As Integer, ByVal PosId As String, ActionState As String, CompensationId As String, GradeId As String, ValidFrom As String, ValidTo As String, gvCompensation As GridView)
    '    Try
    '        Dim client As New EmployeeCompensationService.ALE_EmpCompServiceClient

    '        'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\shakeel.shakkeer"
    '        'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"

    '        'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\swapna.tv"
    '        'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"

    '        client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
    '        client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString

    '        Dim context As New EmployeeCompensationService.CallContext
    '        'context.Company = "GCO" ' disable it when not in demo mode and pass actual legal entity
    '        'context.Company = Me.GetAXBsuIdFromPosition(Me.hfPosId.Value)
    '        'If context.Company = "" Then
    '        '    ServiceErrorMsg = "Invalid legal entity"
    '        '    Return False
    '        'End If
    '        context.Company = Me.hfCompany.Value


    '        'Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count
    '        'Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count + 6 + 1 + gvJoiningAllowance.Rows.Count

    '        Dim EmpCompLinesCount As Integer = gvCompensation.Rows.Count '+ 6 + 1 + gvJoiningAllowance.Rows.Count

    '        Dim EmpCompFactory As New EmployeeCompensationService.AxdALE_EmpComp
    '        Dim EmpComps(0) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
    '        Dim EmpComp As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation = Nothing
    '        Dim EmpCompLines(EmpCompLinesCount - 1) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine
    '        Dim EmpCompLine As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing
    '        'Dim EmpCompRef As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing

    '        'Initialize and fill PosComp (header) details
    '        EmpComp = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
    '        EmpComp.RecId = RecId
    '        EmpComp.RecIdSpecified = True
    '        EmpComp.EmployeeId = EmpId
    '        EmpComp.HcmActionState = ActionState 'Ignore this comment. As of now, skipping this as any value passed for it is throwing error message. Need to check with them what valid value we can pass
    '        EmpComp.CompensationId = CompensationId
    '        EmpComp.Description = ""
    '        EmpComp.ValidFrom = ValidFrom
    '        EmpComp.ValidFromSpecified = True
    '        EmpComp.ValidTo = ValidTo 'Pass some distant future date
    '        EmpComp.ValidToSpecified = True
    '        EmpComp.class = "entity" 'This value is hard coded as suggested in the service documentation

    '        'Now fill up the EmpCompLines() array with all EmpCompLine. Compensation details.
    '        Dim i As Integer = 0
    '        For i = 0 To gvCompensation.Rows.Count - 1
    '            EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

    '            EmpCompLine.RecId = Get6DigitRandomNumber()
    '            EmpCompLine.RecIdSpecified = True

    '            EmpCompLine.EmployeeCompensationRef = RecId
    '            EmpCompLine.EmployeeCompensationRefSpecified = True

    '            EmpCompLine.Amount = Me.GetActualAmount(CType(gvCompensation.Rows(i).FindControl("txtSal_amount"), TextBox).Text)
    '            EmpCompLine.AmountSpecified = True
    '            EmpCompLine.CurrencyCode = Me.ddlCurrency.SelectedValue
    '            EmpCompLine.ComponentCode = CType(gvCompensation.Rows(i).FindControl("lblern_id"), Label).Text
    '            EmpCompLine.Description = CType(gvCompensation.Rows(i).FindControl("lblern_descr"), Label).Text
    '            EmpCompLine.FromDate = CDate(CType(gvCompensation.Rows(i).FindControl("txtRangeFromDate"), TextBox).Text)
    '            EmpCompLine.FromDateSpecified = True
    '            EmpCompLine.PayType = IIf(CType(gvCompensation.Rows(i).FindControl("ddlSalFrequency"), DropDownList).SelectedValue = "M", EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly, EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Annual)
    '            EmpCompLine.PayTypeSpecified = True
    '            EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

    '            EmpCompLines(i) = EmpCompLine
    '        Next

    '        'Now assign the EmpCompLines() array to EmpComp
    '        'EmpComp.empCompLine = EmpCompLines
    '        EmpComp.ALE_EmployeeCompensationLine = EmpCompLines

    '        'Now add EmpComp to EmpComps array
    '        EmpComps(0) = EmpComp

    '        'Now assing EmpComps to EmpComp field of EmpCompFactory
    '        EmpCompFactory.ALE_EmployeeCompensation = EmpComps

    '        Dim ent(0) As EmployeeCompensationService.EntityKey
    '        ent = client.create(context, EmpCompFactory)

    '        If ent.Length > 0 Then
    '            Dim tmp1, tmp2 As String
    '            tmp1 = ent(0).KeyData(0).Field
    '            tmp2 = ent(0).KeyData(0).Value
    '            Dim DAXRecId As String = tmp2
    '            Dim UpdateQuery As String = "UPDATE dbo.SALARY SET SAL_DAX_REC_ID = '" & DAXRecId & "' WHERE SAL_POS_ID = '" & Pos_Id & "' AND SAL_WORKER_TYPE = 'E' AND SAL_WORKER_ID = '" & Me.hfEmpId.Value & "' AND SAL_BACTIVE = 1 AND SAL_DELETED <> 1 AND ( SAL_APPR_STATUS <> 'R' OR SAL_APPR_STATUS IS NULL)"
    '            SqlHelper.ExecuteNonQuery(tr, CommandType.Text, UpdateQuery)
    '            UpdateQuery = "UPDATE dbo.SALARY_BENEFITS SET SBEN_DAX_REC_ID = '" & DAXRecId & "' WHERE SBEN_POS_ID = '" & Pos_Id & "' AND SBEN_WORKER_TYPE = 'E' AND SBEN_WORKER_ID = '" & Me.hfEmpId.Value & "' AND SBEN_ACTIVE = 1 AND SBEN_DELETED <> 1 AND ( SBEN_APPR_STATUS <> 'R' OR SBEN_APPR_STATUS IS NULL)"
    '            SqlHelper.ExecuteNonQuery(tr, CommandType.Text, UpdateQuery)
    '        End If

    '        Return True

    '    Catch ex As Exception
    '        Me.ShowInfoLog("Following error occured in DAX service: " & ex.Message, "E")
    '        ServiceErrorMsg = "Following error occured in DAX service: " & ex.Message
    '        Throw ex 'throw back the error to the caller and let it be handled there
    '        Return False
    '    Finally
    '    End Try

    'End Function

    Private Function Get6DigitRandomNumber() As Integer
        Dim Min As Integer = 111111
        Dim Max As Integer = 999999
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function

    'Private Function GetUniqueCompensationId(ByVal EmpId As String) As String
    '    Dim Min As Integer = 111
    '    Dim Max As Integer = 99999
    '    Static Generator As System.Random = New System.Random()
    '    Return "OffComp-" & Generator.Next(Min, Max).ToString
    'End Function

    Private Function GetUniqueCompensationId(ByVal EmpId As String, ByVal tr2 As SqlTransaction) As String
        Dim Min As Integer = 15000
        'Dim Max As Integer = 999999
        'Dim Max As Integer = 9999
        Dim Max As Integer = 999999999
        Static Generator As System.Random = New System.Random()

        Dim ds As New DataSet
        Dim compId As String = ""
        Try
            Dim str_query As String = "exec OASIS_DAX.DBO.GENERATECOMP_SEQUENCE 'Bulk' ,'" & EmpId & "'"
            ds = SqlHelper.ExecuteDataset(tr2, CommandType.Text, str_query)
            If Not ds.Tables(0) Is Nothing Then
                compId = ds.Tables(0).Rows(0)("SequenceNumber").ToString
            End If
            If compId <> "" Then
                Return compId
            Else
                lblError.Text = "Error in generating unique id for Employee: " & EmpId
                Return "00"
            End If
        Catch exc As Exception
            lblError.Text = "Error in generating unique id for Employee: " & EmpId
            Return "00"

        Finally

        End Try
    End Function

    Protected Sub btnSaveUploadToAXandOASIS_Click(sender As Object, e As EventArgs) Handles btnSaveUploadToAXandOASIS.Click

        If Session("BulkSalaryRevision_UploadedSalary") Is Nothing Or Me.gvUpload.Rows.Count <= 0 Then
            Me.lblError.Text = "Kindly upload the data first before saving"
            Exit Sub
        End If


        'If CheckPayMonth() Then
        Dim errorNo As Integer
        Dim ErrEmpNo, ErrEmpName As String
        errorNo = SaveEMPAddToSalaryDetails_UploadTo_OASISandAX(ErrEmpNo, ErrEmpName)

        If errorNo = 0 Then
            If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), hf_EMP_IDs.Value, "Salary Revised", Page.User.Identity.Name.ToString, Nothing) <> 0 Then
                lblError.Text = "Failed to update Audit Trail"
                UtilityObj.Errorlog("Failed to update Audit Trail")
            Else
                lblError.Text = " Salary Revised Successfully"
                ClearAllDetails_Upload()
            End If
        ElseIf errorNo = 1702 Then
            'Me.lblError.Text = "Salary data already uploaded for the highlighted employee(s) for the entered period. Kindly update the excel and try uploading again."
            lblError.Text = UtilityObj.getErrorMessage(errorNo)

        Else
            lblError.Text = UtilityObj.getErrorMessage(errorNo) & " Employee: " & ErrEmpNo & " - " & ErrEmpName

        End If

    End Sub
    Protected Sub txtEmpNo_TextChanged(sender As Object, e As EventArgs)
        Session("EMPSALDETAILS") = Nothing
        Session("ADDEDEMPSALDETAILS") = Nothing
        Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        PopulateSalaryData(condition)
        Gridbind()
        GridBindSalaryDetails()
        tblSalDetails.Visible = True

        trEditSalAmt.Visible = False
        trEditSalAmt1.Visible = False
        trEditSalAmt2.Visible = False
        trEditSalAmt3.Visible = False
        trEditSalAmt4.Visible = False

        trSalScale1.Visible = True
        trSalScale2.Visible = True
        trSalScale3.Visible = True
        lblError.Text = ""
        txtSalGrade.Text = ""
        hfSalGrade.Value = ""
        txtGrossSalary.Text = ""
        txtEmpNo.Text = ""
    End Sub

    Protected Sub txtSalGrade_TextChanged(sender As Object, e As EventArgs)
        'Dim dtable As DataTable = CreateSalaryDetailsTable()
        If hfSalGrade.Value = "" Then Return
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal
        Dim fteVal As Double = 1
        str_Sql = "Select EMPGRADES_M.EGD_ID FROM EMPGRADES_M RIGHT OUTER JOIN" & _
        " EMPSCALES_GRADES_M ON EMPGRADES_M.EGD_ID = EMPSCALES_GRADES_M.SGD_EGD_ID " & _
        " WHERE EMPSCALES_GRADES_M.SGD_ID =" & hfSalGrade.Value
        'Dim objGrade As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        'If Not objGrade Is DBNull.Value Then
        '    hfGrade_ID.Value = objGrade.ToString
        'End If

        str_Sql = "select EMPGRADE_SALARY_S.*, ERN_DESCR from EMPGRADE_SALARY_S " & _
        " EMPGRADE_SALARY_S LEFT OUTER JOIN EMPSALCOMPO_M ON EMPGRADE_SALARY_S.GSS_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        "WHERE GSS_SGD_ID =" & hfSalGrade.Value & " and GSS_DTTO is null ORDER BY ERN_ORDER"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'If Session("EMPSALSCHEDULE") Is Nothing Then
        '    Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        'End If
        Session("ADDEDEMPSALDETAILS") = CreateSalaryRevDetailsTable()

        Dim dTemptable As DataTable = Session("ADDEDEMPSALDETAILS")
        For index As Integer = 0 To dTemptable.Rows.Count - 1
            dTemptable.Rows(index)("Status") = "DELETED"
        Next
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            grossSal += dr("GSS_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("EMP_ID") = GetNextESHID()
            ldrTempNew.Item("ENR_ID") = dr("GSS_ERN_ID")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = True
            ldrTempNew.Item("PAYTERM") = 0
            ldrTempNew.Item("Amount") = dr("GSS_AMOUNT") * fteVal
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("GSS_AMOUNT")
            ldrTempNew.Item("Status") = "EDITED"
            dTemptable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("ADDEDEMPSALDETAILS") = dTemptable
        GridBindSalaryDetails()

        UpdateSalaryScale()
        Gridbind()
        UpdateSalaryAmounts()
        txtSalGrade.Text = ""
    End Sub
End Class
