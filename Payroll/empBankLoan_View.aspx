<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empBankLoan_View.aspx.vb" Inherits="Payroll_empBankLoan_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">


        function test1(val) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }


        function test3(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid3()%>").src = path;
                 document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
             }
             function test4(val) {
                 var path;
                 if (val == 'LI') {
                     path = '../Images/operations/like.gif';
                 } else if (val == 'NLI') {
                     path = '../Images/operations/notlike.gif';
                 } else if (val == 'SW') {
                     path = '../Images/operations/startswith.gif';
                 } else if (val == 'NSW') {
                     path = '../Images/operations/notstartwith.gif';
                 } else if (val == 'EW') {
                     path = '../Images/operations/endswith.gif';
                 } else if (val == 'NEW') {
                     path = '../Images/operations/notendswith.gif';
                 }
                 document.getElementById("<%=getid4()%>").src = path;
                    document.getElementById("<%=h_selected_menu_4.ClientID %>").value = val + '__' + path;
                }



                function test5(val) {
                    //alert(val);
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid5()%>").src = path;
                    document.getElementById("<%=h_selected_menu_5.ClientID %>").value = val + '__' + path;
                }
                function test6(val) {
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid6()%>").src = path;
                    document.getElementById("<%=h_selected_menu_6.ClientID %>").value = val + '__' + path;
                }


                function test8(val) {
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid8()%>").src = path;
              document.getElementById("<%=h_selected_menu_8.ClientID %>").value = val + '__' + path;
          }

          function test9(val) {
              var path;
              if (val == 'LI') {
                  path = '../Images/operations/like.gif';
              } else if (val == 'NLI') {
                  path = '../Images/operations/notlike.gif';
              } else if (val == 'SW') {
                  path = '../Images/operations/startswith.gif';
              } else if (val == 'NSW') {
                  path = '../Images/operations/notstartwith.gif';
              } else if (val == 'EW') {
                  path = '../Images/operations/endswith.gif';
              } else if (val == 'NEW') {
                  path = '../Images/operations/notendswith.gif';
              }
              document.getElementById("<%=getid9()%>").src = path;
                    document.getElementById("<%=h_selected_menu_9.ClientID %>").value = val + '__' + path;
                }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Bank Loans By Employee"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table style="width: 100%">
                    <tr>
                        <td align="left" colspan="4"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3"   valign="middle" >
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>
                        <td align="right"  valign="middle">
                            <asp:RadioButton ID="rbActive" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="Actives" Text="Active" />
                            <asp:RadioButton ID="rbInactive" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="Actives" Text="Inactive" /><asp:RadioButton ID="rbAll" runat="server"
                                    AutoPostBack="True" CssClass="radiobutton" GroupName="Actives" Text="All" /></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvPrePayment" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" PageSize="20">
                                            <Columns>
                                                <asp:TemplateField HeaderText="EBL_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("EAH_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEBL_ID" runat="server" Text='<%# Bind("EBL_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Emp Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EName") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEmpNameH" runat="server" CssClass="gridheader_text" Text="Employee Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtEmpName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchToDate_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Bank Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblBnameH" runat="server" CssClass="gridheader_text" Text="Bank Name"
                                                             ></asp:Label><br />
                                                        <asp:TextBox ID="txtBname" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchBName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchBName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBName" runat="server" Text='<%# Bind("Bname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Branch">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("DTTO") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblBranchH" runat="server" CssClass="gridheader_text" Text="Branch"
                                                             ></asp:Label><br />
                                                        <asp:TextBox ID="txtBranch" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchBranch" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchBranch_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Bind("Branch") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblFromDateH" runat="server" CssClass="gridheader_text"
                                                            Text="From Date"></asp:Label><br />
                                                        <asp:TextBox ID="txtFromDate" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchFromDate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchFromDate_Click" />
                                                    </HeaderTemplate>
                                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFromdate" runat="server" Text='<%# Bind("DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date ">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("DTFROM") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label1" runat="server" CssClass="gridheader_text" Text="To Date"></asp:Label><br />
                                                        <asp:TextBox ID="txtToDate" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchToDate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchFromDate_Click" />
                                                    </HeaderTemplate>
                                                    <ControlStyle   />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDate" runat="server" Text='<%# Bind("DTTO", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("PAYMONTH") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAmountH" runat="server" CssClass="gridheader_text" Text="Amount"
                                                             ></asp:Label><br />
                                                        <asp:TextBox ID="txtAmount" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchAmount" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchAmount_Click" />
                                                    </HeaderTemplate>
                                                    
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Bank">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Transfer<br />
                                                        <asp:DropDownList ID="ddlSalaryTranH" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlSalaryTranH_SelectedIndexChanged"  >
                                                            <asp:ListItem Value="2">All</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSalaryTran" runat="server" Checked='<%# Bind("Sal_Tran") %>' Enabled="False" EnableTheming="True" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("PAYYEAR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblRemarkH" runat="server" CssClass="gridheader_text" Text="Remark"
                                                             ></asp:Label><br />
                                                        <asp:TextBox ID="txtRemarks" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchRemarks" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchRemarks_Click" />
                                                    </HeaderTemplate>
                                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_9" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

