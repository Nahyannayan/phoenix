Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class CatEdit_del_new
    Inherits System.Web.UI.Page
    
    Dim Encr_decrData As New Encryption64




    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try


                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

                Dim str_sql As String = ""


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))


                    txtID.Attributes.Add("Readonly", "Readonly")
                    txtDesc.Attributes.Add("Readonly", "Readonly")
                End If


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P050010" And ViewState("MainMnu_code") <> "P050015" And ViewState("MainMnu_code") <> "P050020" And ViewState("MainMnu_code") <> "P050022" And ViewState("MainMnu_code") <> "P050024" And _
                ViewState("MainMnu_code") <> "P050025" And ViewState("MainMnu_code") <> "P050050" And ViewState("MainMnu_code") <> "P050030" And ViewState("MainMnu_code") <> "P050035" And _
                ViewState("MainMnu_code") <> "P050045" And ViewState("MainMnu_code") <> "P050065") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'shijin
                    tbRowEarningCode.Visible = False

                    Dim temp As Integer
                    If ViewState("MainMnu_code") = "P050010" Then

                        ro5.Visible = False
                        ro3.Visible = False
                        ro4.Visible = False
                        ro6.Visible = False
                        lblTitle.Text = "Employee Category"
                        Call hideme()
                        ViewState("Etype") = "C"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                End While
                                'clear of the resource end using
                            End Using

                        ElseIf ViewState("datamode") = "add" Then

                            Dim sqlBusinessUnit As String = "Select id+1 from (select top 1 ECT_ID as id  from EMPCATEGORY_M order by ECT_ID desc)a where id<>''"
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False





                        End If
                        ' str_sql = "Select * from (select ECT_ID as id,ECT_DESCR as descr from EMPCATEGORY_M)a where id<>'' "


                    ElseIf ViewState("MainMnu_code") = "P050015" Then
                        lblTitle.Text = "Employee Grade"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro5.Visible = False
                        ro6.Visible = False
                        Call hideme()
                        ViewState("Etype") = "G"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))

                                End While
                                'clear of the resource end using
                            End Using


                        ElseIf ViewState("datamode") = "add" Then

                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 EGD_ID AS ID FROM EMPGRADES_M  order by EGD_ID desc)a where id<>'' "
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False



                        End If

                        ' str_sql = "Select * from (SELECT EGD_ID AS ID, EGD_DESCR AS DESCR FROM EMPGRADES_M )a where id<>'' "

                    ElseIf ViewState("MainMnu_code") = "P050020" Then
                        lblTitle.Text = "School Designation"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro6.Visible = False
                        Call hideme()
                        ViewState("Etype") = "SD"
                        If ViewState("datamode") = "view" Then




                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    temp = Convert.ToInt64(Userreader("Res_ID"))
                                End While
                                'clear of the resource end using
                            End Using

                            Dim ItemTypeCounter As Integer = 0
                            Using Responreader As SqlDataReader = AccessRoleUser.GetResponsibility()

                                Dim di As ListItem
                                ddlResp.Items.Clear()
                                If Responreader.HasRows = True Then
                                    While Responreader.Read
                                        di = New ListItem(Responreader("RES_DESCR"), Responreader("RES_ID"))
                                        ddlResp.Items.Add(di)
                                        For ItemTypeCounter = 0 To ddlResp.Items.Count - 1
                                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                            If ddlResp.Items(ItemTypeCounter).Value = temp Then
                                                ddlResp.SelectedIndex = ItemTypeCounter
                                            End If
                                        Next
                                    End While
                                End If
                            End Using





                        ElseIf ViewState("datamode") = "add" Then

                            Dim ItemTypeCounter As Integer = 0

                            Using Responreader As SqlDataReader = AccessRoleUser.GetResponsibility()

                                Dim di As ListItem

                                ddlResp.Items.Clear()
                                If Responreader.HasRows = True Then
                                    While Responreader.Read
                                        di = New ListItem(Responreader("RES_DESCR"), Responreader("RES_ID"))
                                        ddlResp.Items.Add(di)

                                    End While
                                End If

                            End Using


                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DES_ID AS ID FROM EMPDESIGNATION_M  order by DES_ID desc)a where id<>'' "
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False





                        End If

                    ElseIf ViewState("MainMnu_code") = "P050022" Then
                        lblTitle.Text = "MOE Designation"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro6.Visible = False
                        Call hideme()

                        ViewState("Etype") = "ME"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    temp = Convert.ToInt64(Userreader("Res_ID"))
                                End While
                                'clear of the resource end using
                            End Using


                            Dim ItemTypeCounter As Integer = 0
                            Using Responreader As SqlDataReader = AccessRoleUser.GetResponsibility()

                                Dim di As ListItem
                                ddlResp.Items.Clear()
                                If Responreader.HasRows = True Then
                                    While Responreader.Read
                                        di = New ListItem(Responreader("RES_DESCR"), Responreader("RES_ID"))
                                        ddlResp.Items.Add(di)
                                        For ItemTypeCounter = 0 To ddlResp.Items.Count - 1
                                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                            If ddlResp.Items(ItemTypeCounter).Value = temp Then
                                                ddlResp.SelectedIndex = ItemTypeCounter
                                            End If
                                        Next
                                    End While
                                End If
                            End Using


                        ElseIf ViewState("datamode") = "add" Then



                            Dim ItemTypeCounter As Integer = 0

                            Using Responreader As SqlDataReader = AccessRoleUser.GetResponsibility()

                                Dim di As ListItem

                                ddlResp.Items.Clear()
                                If Responreader.HasRows = True Then
                                    While Responreader.Read
                                        di = New ListItem(Responreader("RES_DESCR"), Responreader("RES_ID"))
                                        ddlResp.Items.Add(di)

                                    End While
                                End If

                            End Using


                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DES_ID AS ID FROM EMPDESIGNATION_M  order by DES_ID desc)a where id<>'' "
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()

                            txtID.Enabled = False






                        End If
                    ElseIf ViewState("MainMnu_code") = "P050024" Then
                        lblTitle.Text = "MOL Designation"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro6.Visible = False
                        Call hideme()
                        ViewState("Etype") = "ML"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    temp = Convert.ToInt64(Userreader("Res_ID"))
                                End While
                                'clear of the resource end using
                            End Using

                            Dim ItemTypeCounter As Integer = 0
                            Using Responreader As SqlDataReader = AccessRoleUser.GetResponsibility()

                                Dim di As ListItem
                                ddlResp.Items.Clear()
                                If Responreader.HasRows = True Then
                                    While Responreader.Read
                                        di = New ListItem(Responreader("RES_DESCR"), Responreader("RES_ID"))
                                        ddlResp.Items.Add(di)
                                        For ItemTypeCounter = 0 To ddlResp.Items.Count - 1
                                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                            If ddlResp.Items(ItemTypeCounter).Value = temp Then
                                                ddlResp.SelectedIndex = ItemTypeCounter
                                            End If
                                        Next
                                    End While
                                End If
                            End Using





                        ElseIf ViewState("datamode") = "add" Then



                            Dim ItemTypeCounter As Integer = 0

                            Using Responreader As SqlDataReader = AccessRoleUser.GetResponsibility()

                                Dim di As ListItem

                                ddlResp.Items.Clear()
                                If Responreader.HasRows = True Then
                                    While Responreader.Read
                                        di = New ListItem(Responreader("RES_DESCR"), Responreader("RES_ID"))
                                        ddlResp.Items.Add(di)

                                    End While
                                End If

                            End Using


                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DES_ID AS ID FROM EMPDESIGNATION_M  order by DES_ID desc)a where id<>'' "
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False

                        End If
                    ElseIf ViewState("MainMnu_code") = "P050025" Then
                        lblTitle.Text = "Teaching Grade"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro5.Visible = False
                        ro6.Visible = False
                        Call hideme()
                        ViewState("Etype") = "T"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))

                                End While
                                'clear of the resource end using
                            End Using

                        ElseIf ViewState("datamode") = "add" Then

                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 TGD_ID AS ID  FROM EMPTEACHINGGRADES_M order by TGD_ID desc )a where id<>''"
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False

                        End If

                        ' str_sql = "Select * from (SELECT EGD_ID AS ID, EGD_DESCR AS DESCR FROM EMPGRADES_M )a where id<>'' "

                    ElseIf ViewState("MainMnu_code") = "P050030" Then
                        lblTitle.Text = "Job Responsility"
                        tbRowEarningCode.Visible = True
                        ro4.Visible = False
                        ro3.Visible = False
                        ro5.Visible = False
                        ro6.Visible = False
                        Call hideme()
                        ViewState("Etype") = "J"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    txtType.Text = Userreader("ERN_DESCR").ToString
                                    hfTypeID.Value = Userreader("ERN_ID").ToString
                                End While
                                'clear of the resource end using
                            End Using

                        ElseIf ViewState("datamode") = "add" Then

                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 Res_ID AS ID FROM EMPJOBRESPONSILITY_M order by Res_ID desc)a where id<>''"
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False

                        End If

                    ElseIf ViewState("MainMnu_code") = "P050035" Then
                        'ro5.Visible = False

                        lblRep.Text = "Period"
                        lblTitle.Text = "LEAVE TYPE"

                        ViewState("Etype") = "L"
                        Dim di As ListItem
                        ddlResp.Items.Clear()
                        'check if it return rows or not
                        di = New ListItem("Month", "0")
                        ddlResp.Items.Add(di)
                        di = New ListItem("Year", "1")
                        ddlResp.Items.Add(di)
                        tbRowEarningCode.Visible = False
                        ro6.Visible = False
                        ro5.Visible = False
                        ro4.Visible = False
                        ro3.Visible = False
                        txtMonth.Text = 0
                        If ViewState("datamode") = "view" Then
                            Dim paidStatus As Boolean
                            Dim eofStatus As Boolean
                            '  Dim M_month As Double
                            Dim MY_Period As Integer
                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    paidStatus = Convert.ToBoolean(Userreader("paid"))
                                    eofStatus = Convert.ToBoolean(Userreader("EOB"))
                                    txtMonth.Text = Convert.ToString(Userreader("M_month"))
                                    MY_Period = Convert.ToInt64(Userreader("MY_Period"))

                                End While
                                'clear of the resource end using
                            End Using
                            ddlResp.SelectedIndex = MY_Period
                            If paidStatus = True Then
                                rdPaidY.Checked = True
                            Else
                                rdPaidN.Checked = True
                            End If
                            If eofStatus = True Then
                                rdEobY.Checked = True
                            Else
                                rdEobN.Checked = True
                            End If

                        ElseIf ViewState("datamode") = "add" Then

                            rdPaidN.Checked = True
                            rdEobN.Checked = True
                            lblRep.Text = "Period"
                            'ddlResp.
                            
                        End If

                    ElseIf ViewState("MainMnu_code") = "P050045" Then
                        ro5.Visible = False
                        lblTitle.Text = "Document Master"
                        lblBpaid.Text = "Expiry Alerts Required ?"
                        ro4.Visible = False
                        ro1.Visible = False
                        ro6.Visible = False
                        ViewState("Etype") = "DM"
                        If ViewState("datamode") = "view" Then
                            Dim paidStatus As Boolean

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    paidStatus = Convert.ToBoolean(Userreader("paid"))



                                End While
                                'clear of the resource end using
                            End Using
                            If paidStatus = True Then
                                rdPaidY.Checked = True
                            Else
                                rdPaidN.Checked = True
                            End If



                        ElseIf ViewState("datamode") = "add" Then
                            rdPaidN.Checked = True
                            ro1.Visible = False

                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 ESD_ID AS ID FROM EMPSTATDOCUMENTS_M order by ESD_ID desc)a where id<>''"
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False






                        End If

                    ElseIf ViewState("MainMnu_code") = "P050050" Then
                        ro5.Visible = False
                        lblTitle.Text = "Religion"
                        ' lblBpaid.Text = "Expiry"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro6.Visible = False

                        ViewState("Etype") = "R"
                        If ViewState("datamode") = "view" Then
                            'Dim paidStatus As Boolean

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    ' paidStatus = Convert.ToBoolean(Userreader("paid"))



                                End While
                                'clear of the resource end using
                            End Using



                        End If

                    ElseIf ViewState("MainMnu_code") = "P050065" Then
                        ro5.Visible = False
                        lblTitle.Text = "Department"
                        ro4.Visible = False
                        ro3.Visible = False
                        ro6.Visible = False
                        Call hideme()
                        ViewState("Etype") = "DP"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpComByID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))

                                End While
                                'clear of the resource end using
                            End Using


                        ElseIf ViewState("datamode") = "add" Then

                            Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DPT_ID AS ID FROM DEPARTMENT_M  order by DPT_ID desc)a where id<>'' "
                            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                            command.CommandType = CommandType.Text
                            txtID.Text = command.ExecuteScalar()
                            connection.Close()
                            connection.Dispose()
                            txtID.Enabled = False



                        End If


                    End If


                End If






            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
            UtilityObj.beforeLoopingControls(Me.Page)
        End If

    End Sub


    Sub hideme()

        

    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        txtDesc.Attributes.Remove("readonly")
        
        'txtID.Attributes.Remove("readonly")


        'when the edit button is clicked set the datamode to edit
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim btnClicked As String = btnSave.ID
       
        If Page.IsValid = True Then
            Dim Status As Integer
            Dim Descr As String
            Dim editBit As Boolean
            ViewState("Eid") = txtID.Text


            Descr = txtDesc.Text
            'check the data mode to do the required operation

            If ViewState("datamode") = "edit" Then
                editBit = True
                Dim transaction As SqlTransaction

                'update  the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                    transaction = conn.BeginTransaction("SampleTransaction")

                    Try

                        If ViewState("Etype") = "C" Then


                            If IsNumeric(txtID.Text) Then

                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        ElseIf ViewState("Etype") = "G" Then
                            If IsNumeric(txtID.Text) Then

                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If
                        ElseIf ViewState("Etype") = "SD" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, ddlResp.SelectedValue)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        ElseIf ViewState("Etype") = "ME" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, ddlResp.SelectedValue)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If
                        ElseIf ViewState("Etype") = "ML" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, ddlResp.SelectedValue)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If
                        ElseIf ViewState("Etype") = "T" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If


                        ElseIf ViewState("Etype") = "J" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, hfTypeID.Value, "0", "0")
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        ElseIf ViewState("Etype") = "L" Then


                            Dim bpaid As Boolean
                            Dim eob As Boolean
                            If rdPaidY.Checked = True Then
                                bpaid = True
                            ElseIf rdPaidN.Checked = True Then
                                bpaid = False
                            End If
                            If rdEobY.Checked = True Then
                                eob = True
                            ElseIf rdEobN.Checked = True Then
                                eob = False
                            End If

                            Status = AccessRoleUser.UpdateEmpComByID(0, Descr, ViewState("Etype"), transaction, bpaid, eob, txtID.Text, txtMonth.Text, ddlResp.SelectedValue)


                        ElseIf ViewState("Etype") = "DM" Then


                            Dim bpaid As Boolean

                            If rdPaidY.Checked = True Then
                                bpaid = True
                            ElseIf rdPaidN.Checked = True Then
                                bpaid = False
                            End If

                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.SaveEMPSTATDOCUMENTS_M(txtID.Text, Descr, ViewState("Etype"), editBit, transaction, bpaid)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If


                        ElseIf ViewState("Etype") = "R" Then

                            Status = AccessRoleUser.SaveEMPSTATDOCUMENTS_M(0, Descr, ViewState("Etype"), editBit, transaction, False, False, txtID.Text)

                        ElseIf ViewState("Etype") = "DP" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.UpdateEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        End If
                        'If error occured during the process  throw exception and rollback the process

                        If Status = -1 Then

                            Throw New ArgumentException("Record does not exist for updating")
                        ElseIf Status <> 0 Then


                            Throw New ArgumentException("Error while updating the current record")

                        Else
                            'Store the required information into the Audit Trial table when Edited

                            Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "edit", Page.User.Identity.Name.ToString, Me.Page)


                            If Status <> 0 Then

                                Throw New ArgumentException("Could not complete your request")

                            End If
                            ViewState("datamode") = "none"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Updated Successfully"

                        End If




                    Catch myex As ArgumentException

                        transaction.Rollback()
                        lblError.Text = myex.Message

                    Catch ex As Exception

                        transaction.Rollback()
                        lblError.Text = "Record could not be Updated"

                    End Try




                End Using


            ElseIf ViewState("datamode") = "add" Then
                editBit = False
                Dim transaction As SqlTransaction

                'insert the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try


                        If ViewState("Etype") = "C" Then

                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If



                        ElseIf ViewState("Etype") = "G" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If
                        ElseIf ViewState("Etype") = "SD" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, ddlResp.SelectedValue)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        ElseIf ViewState("Etype") = "ME" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, ddlResp.SelectedValue)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If
                        ElseIf ViewState("Etype") = "ML" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, ddlResp.SelectedValue)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        ElseIf ViewState("Etype") = "T" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If
                        ElseIf ViewState("Etype") = "J" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction, False, False, hfTypeID.Value, "0", "0")
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        ElseIf ViewState("Etype") = "L" Then


                            Dim bpaid As Boolean
                            Dim eob As Boolean
                            If rdPaidY.Checked = True Then
                                bpaid = True
                            ElseIf rdPaidN.Checked = True Then
                                bpaid = False
                            End If
                            If rdEobY.Checked = True Then
                                eob = True
                            ElseIf rdEobN.Checked = True Then
                                eob = False
                            End If

                            Status = AccessRoleUser.InsertEmpComByID(0, Descr, ViewState("Etype"), transaction, bpaid, eob, txtID.Text, txtMonth.Text, ddlResp.SelectedValue)

                        ElseIf ViewState("Etype") = "DM" Then
                            Dim bpaid As Boolean

                            If rdPaidY.Checked = True Then
                                bpaid = True
                            ElseIf rdPaidN.Checked = True Then
                                bpaid = False
                            End If



                            Status = AccessRoleUser.SaveEMPSTATDOCUMENTS_M(-1, Descr, ViewState("Etype"), editBit, transaction, bpaid)



                        ElseIf ViewState("Etype") = "R" Then

                            Status = AccessRoleUser.SaveEMPSTATDOCUMENTS_M(0, Descr, ViewState("Etype"), editBit, transaction, False, False, txtID.Text)

                        ElseIf ViewState("Etype") = "DP" Then
                            If IsNumeric(txtID.Text) Then
                                Status = AccessRoleUser.InsertEmpComByID(txtID.Text, Descr, ViewState("Etype"), transaction)
                            Else
                                Throw New ArgumentException("ID must be a numerical value")
                            End If

                        End If


                        If Status = -1 Then

                            Throw New ArgumentException("Record with same ID already exist")
                        ElseIf Status <> 0 Then

                            Throw New ArgumentException("Error while inserting new record")


                        Else
                            'Store the required information into the Audit Trial table when inserted

                            Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "insert", Page.User.Identity.Name.ToString, Me.Page)


                            If Status <> 0 Then


                                Throw New ArgumentException("Could not complete your request")

                            End If

                            ViewState("datamode") = "none"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Inserted Successfully"

                        End If

                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()

                        lblError.Text = "Record could not be Inserted"

                    End Try
                End Using
            End If





        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

        
            Call clearMe()
            txtDesc.Attributes.Remove("readonly")

            txtID.Attributes.Remove("readonly")



            ViewState("datamode") = "add"

            'Menu-code for Category
            If ViewState("MainMnu_code") = "P050010" Then

                Dim sqlBusinessUnit As String = "Select id+1 from (select top 1 ECT_ID as id  from EMPCATEGORY_M order by ECT_ID desc)a where id<>''"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False

                'Menu-code for Grade
            ElseIf ViewState("MainMnu_code") = "P050015" Then



                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 EGD_ID AS ID FROM EMPGRADES_M  order by EGD_ID desc)a where id<>'' "
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False
                'Menu-code for School Designation
            ElseIf ViewState("MainMnu_code") = "P050020" Then


                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DES_ID AS ID FROM EMPDESIGNATION_M  order by DES_ID desc)a where id<>'' "
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False

                'Menu-code for MOE
            ElseIf ViewState("MainMnu_code") = "P050022" Then


                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DES_ID AS ID FROM EMPDESIGNATION_M  order by DES_ID desc)a where id<>'' "
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()

                txtID.Enabled = False


                'Menu-code for MOL
            ElseIf ViewState("MainMnu_code") = "P050024" Then


                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DES_ID AS ID FROM EMPDESIGNATION_M  order by DES_ID desc)a where id<>'' "
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False


                'Menu-code for Teacher Grade
            ElseIf ViewState("MainMnu_code") = "P050025" Then


                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 TGD_ID AS ID  FROM EMPTEACHINGGRADES_M order by TGD_ID desc )a where id<>''"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False

                'Menu-code for Job Responsibility
            ElseIf ViewState("MainMnu_code") = "P050030" Then


                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 Res_ID AS ID FROM EMPJOBRESPONSILITY_M order by Res_ID desc)a where id<>''"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False

            ElseIf ViewState("MainMnu_code") = "P050035" Then



                'Menu-code for Document Master
            ElseIf ViewState("MainMnu_code") = "P050045" Then

                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 ESD_ID AS ID FROM EMPSTATDOCUMENTS_M order by ESD_ID desc)a where id<>''"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False

                'Menu-code for Department_M
            ElseIf ViewState("MainMnu_code") = "P050065" Then
                Dim sqlBusinessUnit As String = "Select id+1 from (SELECT top 1 DPT_ID AS ID FROM DEPARTMENT_M  order by DPT_ID desc)a where id<>'' "
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
                command.CommandType = CommandType.Text
                txtID.Text = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()
                txtID.Enabled = False



            End If

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearMe()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        txtID.Attributes.Add("Readonly", "Readonly")
        txtDesc.Attributes.Add("Readonly", "Readonly")
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                If ViewState("Etype") = "C" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "G" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "SD" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "ME" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "ML" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "T" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "J" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)

                ElseIf ViewState("Etype") = "L" Then
                    Status = AccessRoleUser.DeleteEmpComByID(-1, ViewState("Etype"), transaction, txtID.Text)


                ElseIf ViewState("Etype") = "DM" Then


                    Status = AccessRoleUser.DeleteEMPSTATDOCUMENTS_M(txtID.Text, ViewState("Etype"), transaction)

                ElseIf ViewState("Etype") = "R" Then


                    Status = AccessRoleUser.DeleteEMPSTATDOCUMENTS_M(-1, ViewState("Etype"), transaction, txtID.Text)
                ElseIf ViewState("Etype") = "DP" Then
                    Status = AccessRoleUser.DeleteEmpComByID(txtID.Text, ViewState("Etype"), transaction)

                End If

                If Status = -1 Then

                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then


                    Throw New ArgumentException("Record could not be Deleted")

                Else

                    Status = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("Eid"), "delete", Page.User.Identity.Name.ToString, Me.Page)


                    If Status <> 0 Then


                        Throw New ArgumentException("Could not complete your request")

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearMe()
                    lblError.Text = "Record Deleted Successfully"

                End If

            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, Page.Title)
                transaction.Rollback()

            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message, Page.Title)
                transaction.Rollback()
            End Try
        End Using



       

    End Sub
    Sub clearMe()
        txtID.Text = ""
        txtDesc.Text = ""
        txtType.Text = ""
        hfTypeID.Value = ""
        txtID.Attributes.Add("Readonly", "Readonly")
        txtID.Enabled = True
        txtDesc.Attributes.Add("Readonly", "Readonly")
        If ViewState("Etype") = "L" Then
            rdPaidN.Checked = True
            rdEobN.Checked = True
        End If

    End Sub

    
End Class
