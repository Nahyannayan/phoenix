<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"   AutoEventWireup="false" CodeFile="empDependantsDetail.aspx.vb" Inherits="empDependantsDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ MasterType  VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%--<%@ Register src="ConsultantDatabase/Usercontrol/CD_homeDisplay.ascx" tagname="CD_homeDisplay" tagprefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
      <%--   <style type="text/css">
.normal { background-color: }
.normalActive { background-color:#BDDCF4 }
.normalItemActive{ background-color: #8FBFE7}
.normalItemInActive{  }
        .style3
        {
            height: 30px;
        }
        </style>
 <table border="0" cellpadding="0" cellspacing="0" align="center" 
        style="width: 100%" >
 <tr class="subheader_img">
    <td style="height: 25px;" >
        Dependents</td>
</tr>
 </table>
 
<table border="0" cellpadding="0" cellspacing="0" align="center"  style="width: 100%"> 
<tr valign="top">
    <td width="75%" style="height: 259px" >
        <table border="0" cellpadding="0" cellspacing="0" align="left"  
            style="width: 100%; padding-right: 25px; padding-left: 15px;"> 
        <tr valign="top">
            <td width="100%" style="height: 13px" align="left" >
        <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr valign="top">
            <td width="100%" style="height: 13px" align="left" >
                <asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink>
            </td>
        </tr>
        <tr valign="top">
            <td width="100%" align="center"  >
                <asp:DataList ID="dlDependant" runat="server" RepeatColumns="1" 
                Width="95%" Height="0px" UseAccessibleHeader="True">
                    <ItemTemplate>
                        <table class="matters" 
                            style="border: 1px solid #1b80b6; width:100%; " align="left">
                            <tr>
                                <td class="subheader_img" align="center" colspan="6" 
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; text-align: center;">&nbsp;
                                    <asp:Label ID="lblRelation" runat="server" CssClass="matters" Visible="false" 
                                        EnableViewState="False" Font-Italic="True" Font-Size="Small" 
                                        style="left: 2px; position: relative; top: 0px; text-decoration: underline;" 
                                        Text='<%# bind("EDD_RELATION") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6" 
                                    width="2%">
                                    </td>
                                <td align="left" 
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; border-right-style: dotted; border-right-width: 1px; border-right-color: #1b80b6;" 
                                     width="20%">
                                    </td>
                                <td 
                                    
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; border-right-style: dotted; border-right-width: 1px; border-right-color: #1b80b6;" 
                                    width="2%">
                                    </td>
                                <td  
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6" 
                                    align="left" width="50%">
                                    <asp:HiddenField ID="hdnEDDDocCount" runat="server" Value='<%# Bind("EDD_DocCount") %>' />
                                </td>
                                <td  
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6" 
                                    width="15%">
                                    <asp:HiddenField ID="hdnEDDID" runat="server" Value='<%# Bind("EDD_ID") %>' />
                                </td>
                                <td align="center"  rowspan="4" 
                                    
                                    
                                    
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6">
                                    <asp:ImageButton ID="imgDependant" runat="server" Height="83px" Width="58px">
                                    </asp:ImageButton>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6">
                                    </td>
                                <td align="left" 
                                    
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; border-right-style: dotted; border-right-width: 1px; border-right-color: #1b80b6;" 
                                    >
                                    Name</td>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; border-right-style: dotted; border-right-width: 1px; border-right-color: #1b80b6;">
                                    :</td>
                                <td align="left" 
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6">
                                    <asp:Label ID="lblDepName" runat="server" Text='<%# bind("EDD_NAME") %>'></asp:Label>
                                </td>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6">
                                    <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td  style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6">
                                    </td>
                                <td align="left"                                     
                                    style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; border-right-style: dotted; border-right-width: 1px; border-right-color: #1b80b6;">
                                    Date Of Birth</td>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6; border-right-style: dotted; border-right-width: 1px; border-right-color: #1b80b6;">
                                    :</td>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6" 
                                    align="left">
                                    <asp:Label ID="lblDepDOB" runat="server" 
                                        Text='<%# bind("EDD_DOB") %>'></asp:Label>
                                </td>
                                <td style="border-bottom-style: dotted; border-bottom-width: 1px; border-bottom-color: #1b80b6">
                                    <asp:HyperLink ID="hlDocs" runat="server" 
                                        Text='<%# bind("EDD_DOCCOUNT") %>' Visible="false"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                </td>
                                <td align="left" class="style3">
                                </td>
                                <td class="style3">
                                </td>
                                <td align="left" class="style3">
                                </td>
                                <td class="style3">
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
       
        <tr valign="top">
            <td width="100%" style="height: 259px" align="left" >
                &nbsp;</td>
        </tr>
        </table>
    
    </td>
</tr>
</table>--%>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Dependent Details
        </div>
        <div class="card-body">
            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
            <div style="float:left;"><asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink></div> 
    <div class="container-fluid">
    <div class="row">

           <asp:Repeater ID="dlDependant" runat="server" 
               >
                    <ItemTemplate>
                          <div class="col-sm-6 mb-3">
    <div class="card">       
      <div class="card-body row">
          <div class="col-md-3">
                 <asp:ImageButton ID="imgDependant" runat="server" CssClass="img-thumbnail align-content-center" Style="min-width: 140px;" >
                                    </asp:ImageButton>
           <%--<img class="img-thumbnail align-content-center" alt="Card image cap" id="imgDependant" runat="server" src="ImageHandler.ashx"/>--%>
          </div>
          <div class="col-md-9">
        <ul class="list-group list-group-flush">
        <li class="list-group-item"><strong>Name :</strong><asp:Label ID="lblDepName" runat="server" Text='<%# bind("EDD_NAME") %>'></asp:Label></li>
        <li class="list-group-item"><strong>Date Of Birth :</strong><asp:Label ID="lblDepDOB" runat="server" Text='<%# bind("EDD_DOB") %>'></asp:Label></li>
        <li class="list-group-item"><strong>Relation :</strong> <asp:Label ID="lblRel" runat="server" Text='<%# Bind("EDD_RELATION_DESCR")%>'></asp:Label></li>
        <li class="list-group-item"><strong>EID Number :</strong> <asp:Label ID="lblEID" runat="server" Text='<%# Bind("EDC_DOCUMENT_NO")%>'></asp:Label></li>
        <li class="list-group-item"><asp:HyperLink  class="btn btn-primary float-right" id="hlView" runat="server">View</asp:HyperLink></li>
        </ul> 
        </div>
           </div>
  </div>
  
</div>
                         <asp:HiddenField ID="hdnEDDID" runat="server" Value='<%# Bind("EDD_ID") %>' />
                          <asp:HiddenField ID="hdnEDDDocCount" runat="server" Value='<%# Bind("EDD_DocCount") %>' />
          </ItemTemplate>
               </asp:Repeater>
      </div>
        
   

 
    </div>
              <div class="text-center">
        <asp:Button ID="lnkbackbtn" CssClass="button" runat="server" OnClick="lnkbackbtn_Click" Text="Back"></asp:Button>
    </div>

             </div>
</div>

</asp:Content> 
