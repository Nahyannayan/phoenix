Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empChangeEmployeeStatusView


    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64





    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle

            hlAddNew.NavigateUrl = "empChangeEmployeeStatus1.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "P450025" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Function BindStatus(ByVal ddlStatus As DropDownList) As DropDownList
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim dsStatus As New DataSet
            Dim strSql As String = ""
            strSql = "SELECT EST_ID,EST_DESCR FROM EMPSTATUS_M"

            dsStatus = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            ddlStatus.DataSource = dsStatus.Tables(0)
            ddlStatus.DataTextField = "EST_DESCR"
            ddlStatus.DataValueField = "EST_ID"
            ddlStatus.DataBind()

            Dim lstItm As New ListItem("ALL", "0")
            ddlStatus.Items.Add(lstItm)
            ddlStatus.SelectedValue = 0

            Return ddlStatus

        Catch ex As Exception

        End Try

    End Function


    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet 
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            Dim ddlSearch As New DropDownList
            Dim strStatus As String = ""
            Dim strSelected As String = ""
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""

            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables


                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)


                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFrom")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_DTFROM", lstrCondn3)

                '   -- 3   txtTDate

                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_DTTO", lstrCondn4)

                '   -- 5  city

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EST_REMARKS", lstrCondn5)

                '-- 5  Status
                ' Serach Status Wise
                ddlSearch = gvJournal.HeaderRow.FindControl("ddlStatus")
                If ddlSearch.SelectedValue <> "0" Then
                    str_Filter = str_Filter & " AND EST_CODE='" & ddlSearch.SelectedValue & "' "
                End If
                strStatus = ddlSearch.SelectedValue
                strSelected = ddlSearch.SelectedItem.Text
            End If

            str_Sql = "SELECT * FROM(SELECT EST.EST_TTP_ID,EST.EST_BSU_ID,EST.EST_ID,EST.EST_DTFROM, " _
                & " EST.EST_EMP_ID, EST.EST_DTTO, ES.EST_DESCR, " _
                & " EST.EST_CODE, EST.EST_REMARKS, EM.EMPNO, " _
                & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+ " _
                & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
                & " FROM EMPTRANTYPE_TRN AS EST INNER JOIN" _
                & " EMPSTATUS_M AS ES ON EST.EST_CODE = ES.EST_ID INNER JOIN" _
                & " EMPLOYEE_M AS EM ON EST.EST_EMP_ID = EM.EMP_ID LEFT OUTER JOIN" _
                & " EMPTRANTYPE_M AS TTM ON EST.EST_TTP_ID = TTM.TTP_ID) AS  DB" _
                & " WHERE (EST_TTP_ID = 4) AND (EST_BSU_ID = '" & Session("sBSUID") & "')" & str_Filter
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            BindStatus(TryCast(gvJournal.HeaderRow.FindControl("ddlStatus"), DropDownList))
            If strStatus <> "" Then
                TryCast(gvJournal.HeaderRow.FindControl("ddlStatus"), DropDownList).SelectedValue = strStatus
            End If
            If strStatus <> "0" Then
                Select Case strSelected
                    Case "Missing"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "Missing Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "Last Working Date"
                    Case "Resigned"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "Resigned Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "Last Working Date"
                    Case "Terminated"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "Terminated Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "Last Working Date"
                    Case "Suspended"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "Suspended Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "Last Working Date"
                    Case "Absconding"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "Absconding Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "Last Working Date"
                    Case "Transfer"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "Transfer Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "Last Working Date"
                    Case "On Leave"
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "From Date" '
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "To Date"
                    Case Else
                        TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "From Date"
                        TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = "To Date"
                End Select
            Else
                strSelected = ""
                TryCast(gvJournal.HeaderRow.FindControl("lblFromdate"), Label).Text = "From Date"
                TryCast(gvJournal.HeaderRow.FindControl("lblTodate"), Label).Text = " To Date "
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empChangeEmployeeStatus1.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
End Class
