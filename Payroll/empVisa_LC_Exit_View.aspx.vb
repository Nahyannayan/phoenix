﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Partial Class Payroll_empVisa_LC_Exit_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empVisa_LC_Exit.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
               
               
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
        If ViewState("MainMnu_code") = "P153072" Then
            lblTitle.Text = "Employee Visa Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153073" Then
            lblTitle.Text = "Employee Labour Card Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153074" Then
            lblTitle.Text = "Employee Exit"
        
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty


            Dim lstrCondn5 As String = String.Empty
            Dim lstrOpr As String = String.Empty

            Dim larrSearchOpr() As String
            Dim ds As New DataSet
            Dim txtSearch As New TextBox

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)

                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

              
               
            End If


            Dim st As String = ""
            If ViewState("MainMnu_code") = "P153072" Then
                
                st = "VISA"
               
            ElseIf ViewState("MainMnu_code") = "P153073" Then
               
                st = "LC"
               
            ElseIf ViewState("MainMnu_code") = "P153074" Then
               
                st = "EXIT"

            End If



            Dim PARAM(2) As SqlParameter


            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@TYPE", st)
            PARAM(2) = New SqlParameter("@FILTER_COND", str_Filter)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETEMP_VISA_LC_CANCEL", PARAM)


            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
               
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
           


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblEMP_ID As New Label
                lblEMP_ID = TryCast(e.Row.FindControl("lblEMP_ID"), Label)
                Dim hlEdit As New HyperLink
                hlEdit = TryCast(e.Row.FindControl("lnkView"), HyperLink)
                If hlEdit IsNot Nothing And lblEMP_ID IsNot Nothing Then
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    hlEdit.NavigateUrl = "empVisa_LC_Exit.aspx?viewid=" & Encr_decrData.Encrypt(lblEMP_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End If
                Dim imgupdate As New ImageButton
                imgupdate = TryCast(e.Row.FindControl("imgUpdate"), ImageButton)
                Dim lnkRemove As New LinkButton
                lnkRemove = TryCast(e.Row.FindControl("lnkRemove"), LinkButton)

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMP_ID As Label

        lblEMP_ID = sender.Parent.parent.findcontrol("lblEMP_ID")
        Dim sqlParam(3) As SqlParameter
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim st As String = ""
        If ViewState("MainMnu_code") = "P153072" Then

            st = "VISA"

        ElseIf ViewState("MainMnu_code") = "P153073" Then

            st = "LC"

        ElseIf ViewState("MainMnu_code") = "P153074" Then

            st = "EXIT"

        End If
        sqlParam(0) = Mainclass.CreateSqlParameter("@emp_id ", lblEMP_ID.Text, SqlDbType.VarChar)

        sqlParam(1) = Mainclass.CreateSqlParameter("@STATUS", st, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnMsg", "", SqlDbType.VarChar, True)

        sqlParam(3).Direction = ParameterDirection.ReturnValue

        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "CANCEL_VISA_LC_PROCESS", sqlParam)
        str_success = sqlParam(3).Value
        If str_success = 0 Then
            lblError.Text = getErrorMessage("0")
            gridbind()
        Else
            lblError.Text = getErrorMessage(str_success)
        End If
    End Sub
End Class
