<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empAttendanceView.aspx.vb" Inherits="Payroll_empAttendanceView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Attendance Deatils
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">

                        <td valign="top" width="100%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table style="border-collapse: collapse" align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_img" valign="top" >
                <td align="left" colspan="9" valign="middle">
                    Attendance Deatils</td>
            </tr>--%>
                    <tr>
                        <td align="center" valign="top" colspan="9">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO" CssClass="table table-bordered table-row"
                                SkinID="GridViewView" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="Emp No" SortExpression="EMPNO">
                                        <HeaderTemplate>
                                            Emp No
                                            <br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp Name" SortExpression="EMP_NAME">
                                        <HeaderTemplate>
                                            Emp Name
                                            <br />
                                            <asp:TextBox ID="txtEmpname" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave Type">
                                        <HeaderTemplate>
                                            Leave Type
                                            <br />
                                            <asp:TextBox ID="txtLType" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEAT_ELT_descr" runat="server" Text='<%# Bind("ELT_Descr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                            <br />
                                            <asp:TextBox ID="txtDate" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMonthYear" runat="server" Text='<%# Bind("MonthYear") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks
                                            <br />
                                            <asp:TextBox ID="txtRemarks" runat="server" SkinID="Gridtxt" ></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("EAT_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DAYS" HeaderText="Days">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EAT_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEAT_EMP_ID" runat="server" Text='<%# Bind("EAT_EMP_ID") %>'></asp:Label>
                                            <asp:Label ID="lblEAT_ELT_ID" runat="server" Text='<%# Bind("EAT_ELT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


