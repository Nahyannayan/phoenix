Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64

Partial Class Payroll_empTerminationView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empTermination.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P153070" And ViewState("MainMnu_code") <> "H000082" And ViewState("MainMnu_code") <> "P153072" And ViewState("MainMnu_code") <> "P153073" And ViewState("MainMnu_code") <> "P153074") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
                If ViewState("MainMnu_code") = "P153072" Or ViewState("MainMnu_code") = "P153073" Or ViewState("MainMnu_code") = "P153074" Then
                    gvJournal.Columns(2).Visible = False
                    gvJournal.Columns(3).Visible = False
                    gvJournal.Columns(4).Visible = False
                    gvJournal.Columns(5).Visible = False
                    gvJournal.Columns(6).Visible = False
                    gvJournal.Columns(7).Visible = False
                    gvJournal.Columns(8).Visible = False
                    gvJournal.Columns(9).Visible = False
                    gvJournal.Columns(15).Visible = False
                    gvJournal.Columns(17).Visible = False
                    rbApprove.Text = "Cancelled"
                End If
                If ViewState("MainMnu_code") = "P153072" Then
                    gvJournal.Columns(10).Visible = True
                    gvJournal.Columns(11).Visible = True
                ElseIf ViewState("MainMnu_code") = "P153073" Then
                    gvJournal.Columns(12).Visible = True
                    gvJournal.Columns(13).Visible = True
                ElseIf ViewState("MainMnu_code") = "P153074" Then
                    gvJournal.Columns(14).Visible = True
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
        If ViewState("MainMnu_code") = "P153072" Then
            lblTitle.Text = "Employee Visa Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153073" Then
            lblTitle.Text = "Employee Labour Card Cancellation"
        ElseIf ViewState("MainMnu_code") = "P153074" Then
            lblTitle.Text = "Employee Exit"
        ElseIf ViewState("MainMnu_code") = "P153070" Then
            lblTitle.Text = "Final Settlement"
            hlAddNew.Visible = False
        ElseIf ViewState("MainMnu_code") = "H000082" Then
            lblTitle.Text = "Approve Final Settlement"
        End If
    End Sub
    Protected Sub RdApprove_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbApprove.CheckedChanged
        gridbind()
        If ViewState("MainMnu_code") = "P153070" Then
            If Me.rbApprove.Checked And Session("BSU_IsHROnDAX") = 1 Then
                Me.gvJournal.Columns(Me.gvJournal.Columns.Count - 1).Visible = True
            ElseIf rbAll.Checked And Session("BSU_IsHROnDAX") = 1 Then
                Me.gvJournal.Columns(Me.gvJournal.Columns.Count - 1).Visible = True
            Else
                Me.gvJournal.Columns(Me.gvJournal.Columns.Count - 1).Visible = False
            End If
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrOpr As String = String.Empty

            Dim larrSearchOpr() As String
            Dim ds As New DataSet
            Dim txtSearch As New TextBox

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)

                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

                '   -- 2  txtLType
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ERD_SALARY", lstrCondn3)

                '   -- 3   txtDate

                'larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                'lstrCondn4 = txtSearch.Text
                'If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELH_AMOUNT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ERD_REMARKS", lstrCondn5)
            End If

            If ViewState("MainMnu_code") = "P153072" Then
                If rbOpen.Checked Then
                    str_Filter = str_Filter & " and EREG_VISACANCEL_DT IS NULL "
                ElseIf rbApprove.Checked Then
                    str_Filter = str_Filter & " and EREG_VISACANCEL_DT IS NOT NULL "
                Else
                    str_Filter = str_Filter & " and 1=1 "
                End If
            ElseIf ViewState("MainMnu_code") = "P153073" Then
                If rbOpen.Checked Then
                    str_Filter = str_Filter & " and EREG_LC_DT IS NULL "
                ElseIf rbApprove.Checked Then
                    str_Filter = str_Filter & " and EREG_LC_DT IS NOT NULL "
                Else
                    str_Filter = str_Filter & " and 1=1 "
                End If
            ElseIf ViewState("MainMnu_code") = "P153074" Then
                If rbOpen.Checked Then
                    str_Filter = str_Filter & " and EREG_EXIT_DT IS NULL "
                ElseIf rbApprove.Checked Then
                    str_Filter = str_Filter & " and EREG_EXIT_DT IS NOT NULL "
                Else
                    str_Filter = str_Filter & " and 1=1 "
                End If
            Else
                If rbOpen.Checked Then
                    str_Filter = str_Filter & "   and EREG_BForwaded=1 and isnull(EREG_ApprStatus_ACC,0)=0 "
                ElseIf rbApprove.Checked Then
                    str_Filter = str_Filter & "   and EREG_BForwaded=1 and isnull(EREG_ApprStatus_ACC,0)=1 "
                Else
                End If
            End If
            If ViewState("MainMnu_code") = "P153072" Or ViewState("MainMnu_code") = "P153073" Or ViewState("MainMnu_code") = "P153074" Then
                str_Filter = str_Filter & "  and EREG_ApprStatus<>'N' and EREG_BForwaded=1 and isnull(EREG_ApprStatus_ACC,0)=1 "
            End If
            'WHERE 
            str_Sql = "  select * from (SELECT distinct EREG.EREG_ID,EREG.EREG_BSU_ID,EREG.EREG_EMP_ID, EREG.EREG_BRESIGNED, EREG.EREG_REGDATE , EREG.EREG_REMARKS, EREG.EREG_REMARKS1,  " _
                        & " EREG.EREG_SALARY, EREG.EREG_LVSALARY, EREG.EREG_ARREARS, EREG.EREG_GRATUITY, EREG.EREG_AIRTICKET,  EREG.EREG_TKTCOUNT, EREG.EREG_DEDUCTIONS, " _
                        & " EREG.EREG_OTHEARNINGS,EREG.EREG_BASSETSRETURN, EREG.EREG_BADVANCESSETTLED,  EMP.EMPNO, ISNULL(EMP.EMP_FNAME,'') +' '+ISNULL(EMP.EMP_MNAME,'')+' '+ ISNULL(EMP.EMP_LNAME,'') AS EMP_NAME,EREG.EREG_ApprStatus_ACC, " _
                        & " EREG_VISACANCEL_DT,EREG_VISACANCEL_BY,EREG_LC_DT,EREG_LC_BY,EREG_EXIT_DT, " _
                        & " ISNULL(EMP_VISA.EMP_FNAME,'') +' '+ISNULL(EMP_VISA.EMP_MNAME,'')+' '+ ISNULL(EMP_VISA.EMP_LNAME,'') AS EMP_VISACANCEL_NAME, " _
                        & " ISNULL(EMP_LC.EMP_FNAME,'') +' '+ISNULL(EMP_LC.EMP_MNAME,'')+' '+ ISNULL(EMP_LC.EMP_LNAME,'') AS EMP_LCCANCEL_NAME , EREG_ApprStatus ,EREG_BForwaded" _
                        & " FROM EMPRESIGNATION_D  AS EREG INNER JOIN EMPLOYEE_M AS EMP ON EREG.EREG_EMP_ID = EMP.EMP_ID  " _
                        & " LEFT OUTER JOIN EMPLOYEE_M  AS EMP_VISA  ON ISNULL(EREG.EREG_VISACANCEL_BY,0) = EMP_VISA.EMP_ID  " _
                        & " LEFT OUTER JOIN EMPLOYEE_M  AS EMP_LC  ON ISNULL(EREG.EREG_LC_BY,0) = EMP_LC.EMP_ID  WHERE  ISNULL(EREG_ApprStatus_HR,'')='A') as db " _
                        & " WHERE  EREG_BSU_ID='" & Session("SBSUID") & "'" & str_Filter


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
                If Session("BSU_IsHROnDAX") = 1 And rbApprove.Checked = True Then
                    gvJournal.Columns(gvJournal.Columns.Count - 1).Visible = True
                Else
                    gvJournal.Columns(gvJournal.Columns.Count - 1).Visible = False

                End If
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Private Function CheckIfFFSSynchedToDAX(ByVal empid As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT   ISNULL(COUNT(*), 0) IsProcessed FROM   OASIS_DAX.dbo.SALARY_FINAL_SETTLEMENT WITH ( NOLOCK ) WHERE    SFNL_EMP_ID =" & empid & _
                              " AND SFNL_BSU_ID ='" & Session("sBSUID") & "'"
        Dim dr2 As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr2.Read())
            ViewState("IsProcessed") = dr2("IsProcessed")
            If ViewState("IsProcessed") <> 0 Then
                Return False
            End If
            'ViewState("PAYYEAR") = dr("BSU_PAYYEAR")
            'ViewState("PAY_CUR_ID") = dr("BSU_CURRENCY")
        End While
        Return True
    End Function
    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblEREG_ID As New Label
                lblEREG_ID = TryCast(e.Row.FindControl("lblEREG_ID"), Label)
                Dim hlEdit As New HyperLink
                hlEdit = TryCast(e.Row.FindControl("lnkView"), HyperLink)
                Dim hEmpid As New Label
                hEmpid = TryCast(e.Row.FindControl("lblESD_EMP_ID"), Label)

                If hlEdit IsNot Nothing And lblEREG_ID IsNot Nothing Then
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    hlEdit.NavigateUrl = "empTermination.aspx?viewid=" & Encr_decrData.Encrypt(lblEREG_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End If
                Dim imgupdate As New ImageButton
                imgupdate = TryCast(e.Row.FindControl("imgUpdate"), ImageButton)
                Dim lnkRemove As New LinkButton
                lnkRemove = TryCast(e.Row.FindControl("lnkRemove"), LinkButton)
                Dim lnkSync As New LinkButton
                lnkSync = TryCast(e.Row.FindControl("lnkSync"), LinkButton)
                lnkSync.Visible = CheckIfFFSSynchedToDAX(hEmpid.Text)
                If rbOpen.Checked = True Then
                    imgupdate.Visible = True
                    lnkRemove.Visible = False
                Else
                    imgupdate.Visible = False
                    lnkRemove.Visible = True
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEREG_ID As Label
        lblEREG_ID = sender.Parent.parent.findcontrol("lblEREG_ID")
        GenerateFinalSettlementReport(CInt(lblEREG_ID.Text))
    End Sub

    Private Sub GenerateFinalSettlementReport(ByVal p_EREG_ID As Integer)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            'Dim cmd As New SqlCommand
            'cmd.CommandType = CommandType.StoredProcedure
            'Dim objConn As New SqlConnection(str_conn)
            'cmd.Connection = objConn
            'cmd.CommandText = "RPT_EMPFINALSETTLEMENT "
            'Dim sqlParam(0) As SqlParameter
            'sqlParam(0) = Mainclass.CreateSqlParameter("@EREG_ID", p_EREG_ID, SqlDbType.VarChar)
            'cmd.Parameters.Add(sqlParam(0))
            'cmd.Connection = objConn

            'Dim repSource As New MyReportClass
            'Dim params As New Hashtable
            'params("userName") = Session("sUsr_name")
            'params("reportCaption") = "Final Settlement"
            'params("@SubBSU_id") = Session("sBsuid").ToString
            'repSource.Parameter = params
            'repSource.Command = cmd
            'repSource.IncludeBSUImage = True
            'repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptTermination.rpt"
            'Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)

            '  Dim str_Sql As String = "EXEC RPT_EMPFINALSETTLEMENT '" & p_EREG_ID & "'"


            '  Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            'Dim cmd As New SqlCommand
            'cmd.CommandText = str_Sql
            'cmd.Connection = New SqlConnection(str_conn)
            'cmd.CommandType = CommandType.Text

            Dim repSource As New rptClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("@EREG_ID") = p_EREG_ID
            params("@EREG_ID_sub") = p_EREG_ID
            params("reportCaption") = "FINAL SETTLEMENT"
            params("bsuid") = Session("sBsuid").ToString
            'params("@IMG_BSU_ID") = Session("sBsuid").ToString
            params("@IMG_TYPE") = "logo"

            repSource.reportParameters = params
            repSource.crDatabase = "oasis"
            'repSource.Command = cmd


            repSource.reportPath = Server.MapPath("../payroll/Reports/RPT/rptTermination.rpt")
            'End If
            Session("rptClass") = repSource
            ' Response.Redirect("../Reports/ASPX Report/rptReportViewer.aspx", True)
            'Else
            'lblError.Text = "No Records with specified condition"
            'End If
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    ''' <summary>
    ''' To remove final settlement 2nd stage processed data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEREG_ID As Label, lblESD_EMP_ID As Label
        lblEREG_ID = sender.Parent.parent.findcontrol("lblEREG_ID")
        lblESD_EMP_ID = sender.Parent.parent.findcontrol("lblESD_EMP_ID")
        Dim sqlParam(3) As SqlParameter
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@ereg_id", lblEREG_ID.Text, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@bsu_id", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@emp_id ", lblESD_EMP_ID.Text, SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnMsg", "", SqlDbType.VarChar, True)
        sqlParam(3).Direction = ParameterDirection.ReturnValue

        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "spRemoveResignation_Stage2", sqlParam)
        str_success = sqlParam(3).Value
        If str_success = 0 Then
            lblError.Text = getErrorMessage("0")
            gridbind()
        Else
            lblError.Text = getErrorMessage(str_success)
        End If
    End Sub

    Protected Sub lnkSync_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblESD_EMP_ID As Label
        lblESD_EMP_ID = sender.Parent.parent.findcontrol("lblESD_EMP_ID")

        Dim EmpId, BsuId As String
        EmpId = lblESD_EMP_ID.Text
        BsuId = Session("sBsuid")

        Dim strscript As String = "window.open('EmpFinalSettlementDAX.aspx?EmpId='" & lblESD_EMP_ID.Text & "&BsuId=" & BsuId & ",'finalsettlementinfo','location=no,menubar=No,status=no,toolbar=no,scrollbars=yes,width=680,height=220,left=200,top=200');"

        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "infolog", strscript, True)
    End Sub



End Class
