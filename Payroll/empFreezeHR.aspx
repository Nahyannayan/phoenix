<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empFreezeHR.aspx.vb" Inherits="Payroll_empFreezeHR" Title="Asset Location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_Enable() {

            if (confirm("You are about to unfreeze the logged in Business Unit's HR Data.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function confirm_Disable() {

            if (confirm("You are about to freeze the logged in Business Unit's HR Data.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Freeze HR"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td align="left" width="100%"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <table  width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label"> Business Unit</span>
                                    </td>
                                    <td align="left" class="matters" style="width: 30%">
                                        <asp:TextBox ID="txtBSUName" runat="server" TabIndex="1" MaxLength="100"  
                                            CssClass="inputbox" Enabled="False"  ></asp:TextBox>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="25%"><span class="field-label">Remarks</span>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server"   TextMode="MultiLine"  
                                            EnableTheming="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td  ><span class="field-label">Current Status</span></td>
                                    <td  >
                                        <asp:TextBox ID="txtCurrStatus" runat="server" TabIndex="1" MaxLength="100"  
                                            CssClass="inputbox" Enabled="False"  ></asp:TextBox>
                                        </td>
                                    <td colspan="2" align="left">
                                        <asp:Button ID="btnHistory" runat="server" CssClass="button" CausesValidation="False"
                                             Text="View History"  ></asp:Button>
                                        <ajaxToolkit:PopupControlExtender ID="pce" runat="server" PopupControlID="pnlDetail"
                                            TargetControlID="btnHistory" DynamicControlID="pnlDetail" DynamicServiceMethod="GetDynamicContent"
                                            Position="Bottom" BehaviorID="pce_0">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center">
                            <asp:Button ID="btnDisable" runat="server" CausesValidation="False" CssClass="button"
                                Text="Freeze" TabIndex="5" OnClientClick="return confirm_Disable();" />
                            <asp:Button ID="btnEnable" runat="server" CssClass="button" Text="Unfreeze" ValidationGroup="groupM1"
                                TabIndex="7" OnClientClick="return confirm_Enable();" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom"  >
                            <div id="divDetail" runat="server">
                                <asp:Panel ID="pnlDetail" runat="server" CssClass="panel-cover">
                                    <table   width="100%">
                                        <tr class="title-bg">
                                            <td align="left" valign="middle"  >Freeze HR Data History
                                            </td>
                                            <td align="right" valign="middle" style="height: 100%; width: 2%">
                                                <button id="lnkClose" runat="server" style="height: 100%; width: 100%">
                                                    X
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="matters" valign="top" align="center" colspan="2">
                                                <asp:Repeater ID="rptFreezeHistory" runat="server">
                                                    <HeaderTemplate>
                                                        <table   width="100%"  >
                                                            <tr class="subheader_img">
                                                                <th align="center">Action
                                                                </th>
                                                                <th align="center">Action Date
                                                                </th>
                                                                <th align="center">User
                                                                </th>
                                                                <th align="center">Remarks
                                                                </th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <%#Container.DataItem("Action")%>
                                                            </td>
                                                            <td>
                                                                <%#Container.DataItem("LOGDT")%>
                                                            </td>
                                                            <td>
                                                                <%#Container.DataItem("HFZ_LOGUSER")%>
                                                            </td>
                                                            <td>
                                                                <%#Container.DataItem("HFZ_REMARKS")%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                            <asp:HiddenField ID="h_HFZ_ID" runat="server" />
                            <asp:HiddenField ID="h_IsDisabled" runat="server" />
                            <asp:HiddenField ID="h_BSU_ID" runat="server" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
