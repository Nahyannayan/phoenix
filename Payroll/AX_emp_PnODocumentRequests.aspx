﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AX_emp_PnODocumentRequests.aspx.vb" MasterPageFile="~/mainMasterPage.master"
    Inherits="Payroll_AX_emp_PnODocumentRequests" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">       



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            P&O Document Requests
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"
                                CssClass="error" Font-Size="Small"></asp:Label>
                             <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                                CssClass="error" ForeColor="" HeaderText="Enter following fields:-" ValidationGroup="Datas" />
                        </td>
                    </tr>
                   <%-- <tr>
                        <td colspan="4" align="left">
                           
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left"  valign="middle">&nbsp;Fields Marked<span style="font-size: 8pt; color: red"> </span>with(<span
                            style="font-size: 8pt; color: red">*</span>)are mandatory

                        </td>
                    </tr>                  
                </table>
                <table  cellpadding="5" style="border-collapse: collapse" cellspacing="0" width="100%" align="center">
                    <%--<tr class="subheader_img" style="width: 100%">
            <td colspan="2" style="height: 19px" align="left">P&O Document Requests
            </td>
        </tr>--%>
                    <tr id="trDocnum" runat="server" visible="false">
                        <td align="left" ><span class="field-label">Document Number </span>
                        </td>
                        <td align="left"  >
                            <asp:Label ID="lblDocnum" runat="server"></asp:Label>
                        </td>
                          <td align="left"   ><span class="field-label">Status</span></td>
                        <td align="left" >
                            <asp:Label ID="lblDocStatus" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td colspan="2"></td>
                        </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Document Type </span><span style="color: red">*</span>
                        </td>
                        <td align="left"  >
                            <asp:DropDownList ID="ddlDocType" runat="server"></asp:DropDownList>

                        </td>
                          <td  align="left" ><span class="field-label">Address To</span><span style="color: red">*</span>
                        </td>
                        <td  align="left" valign="top" >
                            <asp:TextBox ID="txtAddressTo" runat="server" SkinID="MultiText"
                                TextMode="MultiLine" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvAddTo" runat="server" ControlToValidate="txtAddressTo"
                                CssClass="error" ErrorMessage="Address To" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>

                        </td>
                    </tr>                  
                    
                  
                     <tr style="width: 100%">
                           <td  align="left" ><span class="field-label">Employee Notes</span>
                        </td>
                        <td  align="left" valign="top" >
                            <asp:TextBox ID="txtEmpNotes" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                TextMode="MultiLine" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNotes"
                                CssClass="error" ErrorMessage="Employee Notes" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                        </td>
                             <td  align="left" ><span class="field-label">Addressing Notes</span>
                        </td>
                        <td  align="left" valign="top" >
                            <asp:TextBox ID="txtAddNotes" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                TextMode="MultiLine" ></asp:TextBox>

                        </td>
                         </tr>
                    <%--<tr style="width: 100%">
                        
                    </tr>--%>
                    <tr style="width: 100%" id="trHRText" runat="server" visible="false">
                        <td  align="left" ><span class="field-label">HR Notes</span>
                        </td>
                        <td  align="left" valign="top" >
                            <asp:TextBox ID="txtHRNotes" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                TextMode="MultiLine" Enabled="false" ></asp:TextBox>

                        </td>
                        <td colspan="2"></td>
                    </tr>
                
                   
          
                    <tr>
                        <td  colspan="4"  align="center">
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Datas" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />

                        </td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" />
                <asp:HiddenField ID="h_emp_bsu" runat="server" />
                <asp:HiddenField ID="h_emp_bsu_code" runat="server" />
                <asp:HiddenField ID="h_empnumValue" runat="server" />
                <asp:HiddenField ID="h_DaxRecId" runat="server" />
                <asp:HiddenField ID="h_docStatusID" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
