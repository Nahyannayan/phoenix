﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empLeavePlan.aspx.vb" Inherits="Payroll_empLeavePlan" %>


<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css" />
    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
      
       
   
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <iframe src="empLeavePlan.aspx" name="modalIframe" align="top" frameborder="0"
        scrolling="no" style="height: 0px; width: 1px"></iframe>

    <form id="form1" runat="server">
        <table style="height: 100%" width="100%">
            <tr style="height: 100%" width="100%">
                <td align="left" valign="top">
                    <CR:CrystalReportViewer ID="crv" runat="server"
                        AutoDataBind="True" HasCrystalLogo="False"
                        HasPrintButton="True" HasSearchButton="False" HasToggleGroupTreeButton="False"
                        HasZoomFactorList="False" Height="50px" ReportSourceID="rs"
                        Width="350px"
                        EnableDatabaseLogonPrompt="False"
                        EnableParameterPrompt="False" PrintMode="ActiveX" SeparatePages="False" HasDrillUpButton="False" HasExportButton="False" />
                    <CR:CrystalReportSource ID="rs" runat="server">
                    </CR:CrystalReportSource>
                </td>
            </tr>
        </table>

    </form>

</body>
</html>
