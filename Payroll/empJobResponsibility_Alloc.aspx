<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="empJobResponsibility_Alloc.aspx.vb" Inherits="Payroll_empJobResponsibility_Alloc" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function getEmpID(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            <%--var result;
            var url;
            url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'EN') {
                result = window.showModalDialog(url, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                NameandCode = result.split('___');
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[0];
           document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];
        }--%>

            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_empid");
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfEmp_ID.ClientID%>').value = NameandCode[1];
                document.getElementById('<%=txtEmpName.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');
            }
        }

    function getJOBRES() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        <%--var result;

        result = window.showModalDialog("selIDDesc.aspx?ID=JOBRES", "", sFeatures)
        if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=h_ResposibilityID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtRespName.ClientID %>').value = NameandCode[1];
            if (document.getElementById('<%=txtBSUName.ClientID %>').value != '') {
                return true;
            }
        }
        return false;
    }--%>

        var url = "selIDDesc.aspx?ID=JOBRES";
        var oWnd = radopen(url, "pop_jobers");
    }

    function OnClientClose3(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {

            NameandCode = arg.NameandCode.split('||');
            document.getElementById('<%=h_ResposibilityID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtRespName.ClientID%>').value = NameandCode[1];
                __doPostBack('<%= txtRespName.ClientID%>', 'TextChanged');
            }
        }
    function GetBSUName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 450px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        <%--var result;
        result = window.showModalDialog("../Accounts/selBussinessUnit.aspx?multiSelect=false&getall=true", "", sFeatures)
        if (result == '' || result == undefined) {
            return false;
        }
        else {
            NameandCode = result.split('___');
            document.getElementById('<%=txtBSUName.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_BSUID.ClientID %>').value = NameandCode[0];
            if (document.getElementById('<%=txtRespName.ClientID %>').value != '') {
                return true;
            }
            else {
                return false;
            }
        }
        return false;--%>
        var url = "../Accounts/SelBussinessUnit.aspx?multiSelect=false&getall=true";
        var oWnd = radopen(url, "pop_bsuname");
    }

    function OnClientClose2(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {

            NameandCode = arg.NameandCode.split('||');
            document.getElementById('<%=h_BSUID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBSUName.ClientID%>').value = NameandCode[1];
                __doPostBack('<%= txtBSUName.ClientID%>', 'TextChanged');
            }
        }
    
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_empid" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_bsuname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_jobers" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="lblHeader" runat="server" Text="Job Responsibility Allocation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" width="100%" cellspacing="0">
                    <tr>
                        <td align="left" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr valign="bottom" align="left">
                        <td align="left" valign="bottom" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td valign="top" align="center">
                            <table align="left" width="100%">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="5" valign="middle">
                               </td>
                    </tr>--%>
                                <tr id="trWBSU" runat="server">
                                    <td align="left" ><span class="field-label" >Working Business unit</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtWBSUName" runat="server"  ReadOnly="True"></asp:TextBox></td>
                                </tr>
                                <tr id="TrEmpName" runat="server">
                                    <td align="left" ><span class="field-label" >Employee Name</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtEmpName" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp_Name" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getEmpID('EN');return false;"
                                             />
                                        <asp:RequiredFieldValidator ID="rfvEmpName" runat="server" ControlToValidate="txtEmpName"
                                            CssClass="error" ErrorMessage="Employee name required" ForeColor=""
                                            ValidationGroup="groupM1" Display="Dynamic">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr runat="server">
                                    <td align="left" >
                                        <asp:Label ID="lblJobABU" runat="server" Text="Job Allocated Business unit" CssClass="field-label"></asp:Label></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtBSUName" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="btnBSUSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetBSUName(); return false;"
                                           />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtBSUName"
                                            CssClass="error" Display="Dynamic" ErrorMessage="BSU name required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                     <td align="left" ><span class="field-label" >Responsibility</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtRespName" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="btnResp" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getJOBRES(); return false;"
                                            />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRespName"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Responsibility Required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>                              
                                <tr>
                                    <td align="left"><span class="field-label" >From Date</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtFrom_date" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrom_date"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter From  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="From Date entered is not a valid date" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvFrom_Date" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" ><span class="field-label" >To Date</span></td>
                                   
                                    <td align="left" >
                                        <asp:TextBox ID="txtTo_date" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnTo_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTo_date"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter To  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvTodate" runat="server" CssClass="error" Display="Dynamic"
                                            EnableViewState="False" ErrorMessage="To Date entered is not a valid date and must be greater than or equal to From date"
                                            ValidationGroup="groupM1">*</asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label" >Amount</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtAmount" runat="server" ValidationGroup="groupM1"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ControlToValidate="txtAmount"
                                CssClass="error" Display="Dynamic" ErrorMessage="Amount can not be left empty"
                                ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfEmp_ID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_ResposibilityID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnFrom_date" TargetControlID="txtFrom_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFrom_date" TargetControlID="txtFrom_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgbtnTo_date" TargetControlID="txtTo_date">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtTo_date" TargetControlID="txtTo_date">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

