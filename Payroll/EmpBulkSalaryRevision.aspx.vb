Imports System.Data
Imports system.Web.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class empBulkSalaryRevision
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvSalRevisionBulk.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvSalRevisionBulk.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Gridbind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            txtRevisionDate.Attributes.Add("onBlur", "checkdate(this)")
            h_Grid.Value = "top"
            lblError.Text = ""
            Session("EMP_SEL_COND") = " AND EMP_BSU_ID = '" & Session("Sbsuid") & "'"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or _
            ViewState("MainMnu_code") <> OASISConstants.MenuEMPSALARYBULKREVISION Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, _
                ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
                gvSalRevisionBulk.Attributes.Add("bordercolor", "#1b80b6")
                'Gridbind()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(IIf(Request.QueryString("MainMnu_code") IsNot Nothing, Request.QueryString("MainMnu_code").Replace(" ", "+"), String.Empty))
            Page.Title = OASISConstants.Gemstitle
            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then
                ViewState("Eid") = Encr_decrData.Decrypt(IIf(Request.QueryString("Eid") IsNot Nothing, Request.QueryString("Eid").Replace(" ", "+"), ""))
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            txtEmpNo.Attributes.Add("ReadOnly", "ReadOnly")
            txtSalGrade.Attributes.Add("ReadOnly", "ReadOnly")
            BindENR()
             tblSalDetails.Visible = False
        End If
    End Sub

    Private Sub Gridbind(Optional ByVal p_selected_id As Integer = -1)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrEmpMONTH As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrYEAR As String = String.Empty
        Dim lstrTYPE As String = String.Empty
        Dim lstrAMOUNT As String = String.Empty
        Dim lstrREMARKS As String = String.Empty

        Dim lstrFiltMONTH As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltYEAR As String = String.Empty
        Dim lstrFiltTYPE As String = String.Empty
        Dim lstrFiltAMOUNT As String = String.Empty
        Dim lstrFiltREMARKS As String = String.Empty

         
        Dim txtSearch As New TextBox
        'If gvSalRevisionBulk.Rows.Count > 0 Then
        '    ' --- Initialize The Variables

        '    larrSearchOpr = h_selected_menu_1.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)

        '    '   --- FILTER CONDITIONS ---
        '    '   -- 1   refno
        '    larrSearchOpr = h_selected_menu_1.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtMONTH")
        '    lstrEmpMONTH = Trim(txtSearch.Text)
        '    If (lstrEmpMONTH <> "") Then lstrFiltMONTH = SetCondn(lstrOpr, "EMP_MONTH", lstrEmpMONTH)

        '    '   -- 1  docno
        '    larrSearchOpr = h_Selected_menu_2.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpNo")
        '    lstrEMPNo = Trim(txtSearch.Text)
        '    If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EDD_EMP_ID", lstrEMPNo)

        '    '   -- 2  DocDate
        '    larrSearchOpr = h_Selected_menu_5.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpName")
        '    lstrEmpName = txtSearch.Text
        '    If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)

        '    '   -- 5  Narration
        '    larrSearchOpr = h_Selected_menu_3.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)
        '    txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtYEAR")
        '    lstrYEAR = txtSearch.Text
        '    If (lstrYEAR <> "") Then lstrFiltYEAR = SetCondn(lstrOpr, "EDD_PAYYEAR", lstrYEAR)

        '    '   -- 5  COLLUN
        '    larrSearchOpr = h_Selected_menu_6.Value.Split("__")
        '    lstrOpr = larrSearchOpr(0)

        'End If

        Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next

        str_Sql = "SELECT EMPLOYEE_M.EMP_ID, EMPLOYEE_M.EMPNO, EMPLOYEE_M.EMP_SALUTE + ' ' " & _
        "+ EMPLOYEE_M.EMP_FNAME + ' ' + EMPLOYEE_M.EMP_MNAME + ' ' + EMPLOYEE_M.EMP_LNAME AS EMPNAME, " & _
        " EMPSCALES_GRADES_M.SGD_DESCR as CUR_SAL_SCALE FROM EMPLOYEE_M LEFT OUTER JOIN EMPSCALES_GRADES_M " & _
        " ON EMPLOYEE_M.EMP_SGD_ID = EMPSCALES_GRADES_M.SGD_ID WHERE EMP_ID in (" & condition & ")"

        'str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT," & _
        '" EMP_PASSPORT, EMP_STATUS_DESCR, EMP_DES_DESCR FROM vw_OSO_EMPLOYEEMASTER" & _
        '" WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' " _
        '& str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltEmpName & lstrFiltDesignation & lstrFiltPassportNo

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        'PopulateSalaryData(condition)
        gvSalRevisionBulk.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvSalRevisionBulk.DataBind()
            Dim columnCount As Integer = gvSalRevisionBulk.Rows(0).Cells.Count

            gvSalRevisionBulk.Rows(0).Cells.Clear()
            gvSalRevisionBulk.Rows(0).Cells.Add(New TableCell)
            gvSalRevisionBulk.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSalRevisionBulk.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSalRevisionBulk.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvSalRevisionBulk.DataBind()
            UpdateSalaryAmounts()
        End If

        'gvJournal.DataBind()
        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtMONTH")
        'txtSearch.Text = lstrEmpMONTH

        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpNo")
        'txtSearch.Text = lstrEMPNo

        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtEmpName")
        'txtSearch.Text = lstrEmpName

        'txtSearch = gvSalRevisionBulk.HeaderRow.FindControl("txtYEAR")
        'txtSearch.Text = lstrYEAR

        gvSalRevisionBulk.SelectedIndex = p_selected_id
    End Sub

    Private Sub ClearNewAmounts()
        If Session("EMPSALDETAILS") Is Nothing Then Return
        Dim dttab As DataTable = Session("EMPSALDETAILS")
        Dim drSalDet As DataRow
        If dttab.Rows.Count > 0 Then
            For i As Integer = 0 To dttab.Rows.Count - 1
                drSalDet = dttab.Rows(i)
                drSalDet("Amount_NEW") = drSalDet("Amount")
                drSalDet("Amount_ELIGIBILITY_NEW") = drSalDet("Amount_ELIGIBILITY")
            Next
            Session("EMPSALDETAILS") = dttab
            Gridbind()
        End If

    End Sub

    Private Sub UpdateSalaryAmounts()
        Dim dtTempDtl As DataTable = Session("EMPSALDETAILS")
        Dim dRows() As DataRow
        For iRowCount As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
            Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(iRowCount)
            Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
            Dim grossSal, grossSalNew As Double
            grossSal = 0
            grossSalNew = 0
            If lblEMPID Is Nothing Then
                Continue For
            Else
                dRows = dtTempDtl.Select("EMP_ID = '" & lblEMPID.Text & "'")
                If dRows.Length > 0 Then
                    For iColCount As Integer = 0 To dRows.Length - 1
                        Dim amt As Double = GetVal(dRows(iColCount)("Amount"), SqlDbType.Decimal)
                        If Not dRows(iColCount)("bMonthly") Then
                            amt = amt / 12
                        End If
                        grossSal += amt
                        amt = GetVal(dRows(iColCount)("Amount_NEW"), SqlDbType.Decimal)
                        If Not dRows(iColCount)("bMonthly") Then
                            amt = amt / 12
                        End If
                        grossSalNew += amt
                    Next
                End If
                Dim lblEMPAMOUNT As Label = dgViewRow.FindControl("lblCurrSalAmt")
                If lblEMPAMOUNT Is Nothing Then
                    Continue For
                Else
                    lblEMPAMOUNT.Text = AccountFunctions.Round(grossSal)
                End If
                Dim lblEMPAMOUNTNEW As Label = dgViewRow.FindControl("lblRevSalAmt")
                If lblEMPAMOUNTNEW Is Nothing Then
                    Continue For
                Else
                    lblEMPAMOUNTNEW.Text = AccountFunctions.Round(grossSalNew)
                End If
            End If
        Next
    End Sub

    Private Sub PopulateSalaryData(ByVal condition As String)
        Dim str_Sql As String = "SELECT EMPSALARYHS_s.GUID,ERN_ORDER, ESH_ID,ESH_EMP_ID, ESH_BSU_ID, ESH_ERN_ID as ENR_ID," & _
        " ESH_bMonthly, ESL_PAYTERM, ESH_PAYMONTH, ESH_PAYYEAR, ESH_bPAID, EMPSALCOMPO_M.ERN_DESCR, " & _
        " ESH_AMOUNT as AMOUNT,ESH_ELIGIBILITY , ESH_FROMDT, ESH_TODT, ESH_CUR_ID, ESH_PAY_CUR_ID FROM EMPSALARYHS_s " & _
        " LEFT OUTER JOIN EMPSALCOMPO_M ON EMPSALARYHS_s.ESH_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        " WHERE ESH_EMP_ID in (" & condition & ") AND ESH_TODT is null AND ESH_BSU_ID='" & Session("sBSUID") & "'" & _
        " order by ERN_ORDER " 
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand = New SqlCommand(str_Sql, conn)
        cmd.CommandType = CommandType.Text

        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Session("EMPSALDETAILS") = CreateSalaryRevDetailsTable()
        Dim dTemptable As DataTable = Session("EMPSALDETAILS")

        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("EMP_ID") = dr("ESH_EMP_ID")
            ldrTempNew.Item("ENR_ID") = dr("ENR_ID")
            ldrTempNew.Item("ERN_ORDER") = dr("ERN_ORDER")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = dr("ESH_bMonthly")
            ldrTempNew.Item("PAYTERM") = dr("ESL_PAYTERM")
            ldrTempNew.Item("Amount") = dr("AMOUNT")
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("ESH_ELIGIBILITY")
            ldrTempNew.Item("Amount_NEW") = dr("AMOUNT")
            ldrTempNew.Item("Amount_ELIGIBILITY_NEW") = dr("ESH_ELIGIBILITY")
            ldrTempNew.Item("Status") = "INSERTED"
            dTemptable.Rows.Add(ldrTempNew)
        End While

        Session("EMPSALDETAILS") = dTemptable

    End Sub

    Private Sub GridBindAddSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        If Session("ADDEDEMPSALDETAILS") Is Nothing Then
            gvAddSalaryDetails.DataSource = Nothing
            gvAddSalaryDetails.DataBind()
            Return
        End If
        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        Dim grossSal As Double
        dtTempDtl = CreateAddDetailsTable()
        If Session("ADDEDEMPSALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("ADDEDEMPSALDETAILS").Rows.Count - 1
                If (Session("ADDEDEMPSALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("ADDEDEMPSALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                            If Not Session("ADDEDEMPSALDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        txtGrossSalary.Text = grossSal
        gvAddSalaryDetails.DataSource = dtTempDtl
        gvAddSalaryDetails.DataBind()
    End Sub

    Private Sub BindENR()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT ERN_ID, ERN_DESCR as DESCR FROM EMPSALCOMPO_M WHERE ERN_TYP = 1 ORDER BY ERN_ORDER"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSalEarnCode.DataSource = dr
        ddSalEarnCode.DataValueField = "ERN_ID"
        ddSalEarnCode.DataTextField = "DESCR"
        ddSalEarnCode.DataBind()
    End Sub

    Protected Sub btnSalAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalAdd.Click

        If Session("ADDEDEMPSALDETAILS") Is Nothing Then
            Session("ADDEDEMPSALDETAILS") = CreateAddDetailsTable()
        End If
        If Not IsNumeric(txtSalAmount.Text) Then
            lblError.Text = "Amount should be valid"
            Exit Sub
        End If

        Dim status As String = String.Empty
        Dim dtSalDetails As DataTable = Session("ADDEDEMPSALDETAILS")
        Dim i As Integer
        Dim gvRow As GridViewRow = gvEmpSalary.SelectedRow()

        For i = 0 To dtSalDetails.Rows.Count - 1
            If Session("EMPREVEditID") Is Nothing Or _
            Session("EMPREVEditID") <> dtSalDetails.Rows(i)("UniqueID") Then
                If dtSalDetails.Rows(i)("ENR_ID") = ddSalEarnCode.SelectedItem.Value Then
                    lblError.Text = "Cannot add transaction details.The Earn Code details are repeating."
                    GridBindAddSalaryDetails()
                    Exit Sub
                End If
            End If
        Next
        If btnSalAdd.Text = "Add" Then
            Dim newDR As DataRow = dtSalDetails.NewRow()
            newDR("UniqueID") = dtSalDetails.Rows.Count()
            newDR("ENR_ID") = ddSalEarnCode.SelectedValue
            newDR("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
            newDR("Amount") = txtSalAmount.Text
            newDR("bMonthly") = chkSalPayMonthly.Checked
            newDR("Status") = "Insert"
            status = "Insert"
            dtSalDetails.Rows.Add(newDR)
            btnSalAdd.Text = "Add"
            lblError.Text = ""
            'FillAllocateDetails(ddSalEarnCode.SelectedValue, CInt(ddlayInstallment.SelectedValue), CDbl(txtSalAmount.Text))
        ElseIf btnSalAdd.Text = "Save" Then
            Dim iIndex As Integer = 0
            Dim str_Search As String = Session("EMPREVEditID")
            For iIndex = 0 To dtSalDetails.Rows.Count - 1
                If str_Search = dtSalDetails.Rows(iIndex)("UniqueID") And dtSalDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dtSalDetails.Rows(iIndex)("ENR_ID") = ddSalEarnCode.SelectedValue
                    dtSalDetails.Rows(iIndex)("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
                    dtSalDetails.Rows(iIndex)("Amount") = CDbl(txtSalAmount.Text)
                    dtSalDetails.Rows(iIndex)("bMonthly") = chkSalPayMonthly.Checked
                    status = "Edit/Update"
                    Exit For
                End If
            Next
            btnSalAdd.Text = "Add"
            ddSalEarnCode.Enabled = True
            lblError.Text = ""
        End If
        UpdateSalaryDetails()
        ClearEMPSALDETAILS()
        Session("ADDEDEMPSALDETAILS") = dtSalDetails
        GridBindAddSalaryDetails()
        UpdateSalaryAmounts()
    End Sub

    Private Sub UpdateSalaryScale()
        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryRevDetailsTable()
        End If
        If Session("ADDEDEMPSALDETAILS").Rows.Count < 0 Then Return

        Dim strArr(1) As String
        Dim dTemptable As DataTable = Session("EMPSALDETAILS")
        Dim dc As New DataColumn("Amount_NEW")
        dTemptable.Columns.Remove("Amount_NEW")
        dTemptable.Columns.Add(dc)
        dc = New DataColumn("Amount_ELIGIBILITY_NEW")
        dTemptable.Columns.Remove("Amount_ELIGIBILITY_NEW")
        dTemptable.Columns.Add(dc)

        For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
            Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
            Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
            Dim EMP_ID As String
            If lblEMPID Is Nothing Then
                Exit For
            Else
                EMP_ID = lblEMPID.Text
            End If
            strArr(0) = EMP_ID
            Dim dtable As DataTable = Session("ADDEDEMPSALDETAILS")
            For iCount As Integer = 0 To dtable.Rows.Count - 1
                strArr(1) = dtable.Rows(iCount)("ENR_ID")
                Dim drSalDet As DataRow = dTemptable.Rows.Find(strArr)
                If Not drSalDet Is Nothing Then
                    drSalDet("Amount_NEW") = CDbl(dtable.Rows(iCount)("Amount"))
                    drSalDet("Amount_ELIGIBILITY_NEW") = CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY"))
                    drSalDet("bMonthly") = 1
                    'If drSalDet("bMonthly") Then
                    '    drSalDet("Amount_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount"), CDbl(dtable.Rows(iCount)("Amount")) / 12)
                    '    drSalDet("Amount_ELIGIBILITY_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount_ELIGIBILITY"), CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY")) / 12)
                    'Else
                    '    drSalDet("Amount_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount") * 12, CDbl(dtable.Rows(iCount)("Amount")))
                    '    drSalDet("Amount_ELIGIBILITY_NEW") = IIf(chkSalPayMonthly.Checked, dtable.Rows(iCount)("Amount_ELIGIBILITY") * 12, CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY")))
                    'End If
                Else
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dTemptable.NewRow
                    ldrTempNew.Item("UniqueID") = GetNextESHID()
                    ldrTempNew.Item("EMP_ID") = EMP_ID
                    ldrTempNew.Item("ENR_ID") = dtable.Rows(iCount)("ENR_ID")
                    ldrTempNew.Item("ERN_DESCR") = dtable.Rows(iCount)("ERN_DESCR")
                    ldrTempNew.Item("bMonthly") = 1
                    ldrTempNew.Item("PAYTERM") = 0
                    ldrTempNew.Item("Amount") = 0
                    ldrTempNew.Item("Amount_ELIGIBILITY") = 0
                    ldrTempNew.Item("Amount_NEW") = CDbl(dtable.Rows(iCount)("Amount"))
                    ldrTempNew.Item("Amount_ELIGIBILITY_NEW") = CDbl(dtable.Rows(iCount)("Amount_ELIGIBILITY"))
                    ldrTempNew.Item("Status") = "INSERTED"
                    dTemptable.Rows.Add(ldrTempNew)
                End If
            Next

        Next
        Session("EMPSALDETAILS") = dTemptable
    End Sub

    Private Sub UpdateSalaryDetails()

        If Session("EMPSALDETAILS") Is Nothing Then
            Session("EMPSALDETAILS") = CreateSalaryRevDetailsTable()
        End If
        Dim strArr(1) As String
        Dim dTemptable As DataTable = Session("EMPSALDETAILS")
        For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
            Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
            Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
            Dim EMP_ID As String
            If lblEMPID Is Nothing Then
                Exit For
            Else
                EMP_ID = lblEMPID.Text
            End If
            strArr(0) = EMP_ID
            strArr(1) = ddSalEarnCode.SelectedValue
            Dim drSalDet As DataRow = dTemptable.Rows.Find(strArr)
            If Not drSalDet Is Nothing Then
                Dim amt As Double
                If drSalDet("bMonthly") Then
                    amt = IIf(chkSalPayMonthly.Checked, CDbl(txtSalAmount.Text), CDbl(txtSalAmount.Text) / 12)
                Else
                    amt = IIf(chkSalPayMonthly.Checked, CDbl(txtSalAmount.Text) * 12, CDbl(txtSalAmount.Text))
                End If
                drSalDet("Amount_NEW") = CDbl(GetVal(drSalDet("Amount"), SqlDbType.Decimal)) + amt
                drSalDet("Amount_ELIGIBILITY_NEW") = CDbl(GetVal(drSalDet("Amount_ELIGIBILITY"), SqlDbType.Decimal)) + amt
            Else
                Dim ldrTempNew As DataRow
                ldrTempNew = dTemptable.NewRow
                ldrTempNew.Item("UniqueID") = GetNextESHID()
                ldrTempNew.Item("EMP_ID") = EMP_ID
                ldrTempNew.Item("ENR_ID") = ddSalEarnCode.SelectedValue
                ldrTempNew.Item("ERN_DESCR") = ddSalEarnCode.SelectedItem.Text
                ldrTempNew.Item("bMonthly") = chkSalPayMonthly.Checked
                ldrTempNew.Item("PAYTERM") = 0
                ldrTempNew.Item("Amount") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Amount_ELIGIBILITY") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Amount_NEW") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Amount_ELIGIBILITY_NEW") = CDbl(txtSalAmount.Text)
                ldrTempNew.Item("Status") = "INSERTED"
                dTemptable.Rows.Add(ldrTempNew)
            End If
        Next
        Session("EMPSALDETAILS") = dTemptable
    End Sub

    Private Sub ClearEMPSALDETAILS()
        ddSalEarnCode.SelectedIndex = 0
        txtSalAmount.Text = ""
        btnSalAdd.Text = "Add"
        lblError.Text = ""
        ddSalEarnCode.SelectedIndex = 0
        chkSalPayMonthly.Checked = True
        Session.Remove("EMPREVEditID")

    End Sub

    Function CreateSalaryRevDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cEMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim cENR_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))
            Dim cENR_ORDER As New DataColumn("ERN_ORDER", System.Type.GetType("System.Int32"))

            Dim cFROMDATE As New DataColumn("FROM_DATE", System.Type.GetType("System.DateTime"))
            Dim cPAY_CUR_ID As New DataColumn("PAY_CUR_ID", System.Type.GetType("System.String"))

            Dim cPay_Year As New DataColumn("PAY_YEAR", System.Type.GetType("System.Int16"))
            Dim cPay_Month As New DataColumn("PAY_MONTH", System.Type.GetType("System.Int32"))

            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cPAYTERM As New DataColumn("PAYTERM", System.Type.GetType("System.Int32"))
            Dim cbPAID As New DataColumn("bPAID", System.Type.GetType("System.Boolean"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY As New DataColumn("Amount_ELIGIBILITY", System.Type.GetType("System.Decimal"))
            Dim cAmount_NEW As New DataColumn("Amount_NEW", System.Type.GetType("System.Decimal"))
            Dim cAmount_ELIGIBILITY_NEW As New DataColumn("Amount_ELIGIBILITY_NEW", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEMP_ID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cENR_ORDER)
            dtDt.Columns.Add(cENR_DESCR)
            dtDt.Columns.Add(cFROMDATE)
            dtDt.Columns.Add(cPay_Month)
            dtDt.Columns.Add(cPay_Year)

            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cPAYTERM)
            dtDt.Columns.Add(cbPAID)

            dtDt.Columns.Add(cPAY_CUR_ID)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cAmount_ELIGIBILITY)
            dtDt.Columns.Add(cAmount_NEW)
            dtDt.Columns.Add(cAmount_ELIGIBILITY_NEW)
            dtDt.Columns.Add(cStatus)

            Dim dc(1) As DataColumn
            dc(0) = dtDt.Columns("EMP_ID")
            dc(1) = dtDt.Columns("ENR_ID")
            dtDt.PrimaryKey = dc

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Function CreateAddDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cEMP_ID As New DataColumn("EMP_ID", System.Type.GetType("System.String"))
            Dim cENR_ID As New DataColumn("ENR_ID", System.Type.GetType("System.String"))
            Dim cENR_DESCR As New DataColumn("ERN_DESCR", System.Type.GetType("System.String"))
            Dim cbMonthly As New DataColumn("bMonthly", System.Type.GetType("System.Boolean"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cEMP_ID)
            dtDt.Columns.Add(cENR_ID)
            dtDt.Columns.Add(cENR_DESCR)
            dtDt.Columns.Add(cbMonthly)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub lnkSalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String = "SELECT * FROM EMPSALARYHS_s " & _
        '" WHERE GUID ='" & lblRowId.Text & "'"
        'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim dt As DataTable = Session("ADDEDEMPSALDETAILS")
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("UniqueID") = lblRowId.Text Then
                ddSalEarnCode.SelectedValue = dt.Rows(i)("ENR_ID")
                Session("EMPREVEditID") = lblRowId.Text
                ddSalEarnCode.Enabled = False
                txtSalAmount.Text = AccountFunctions.Round(dt.Rows(i)("AMOUNT"))
                chkSalPayMonthly.Checked = dt.Rows(i)("bMonthly")
                btnSalAdd.Text = "Save"
            End If
        Next
    End Sub

    Protected Sub btnSalRevSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalRevSave.Click
        If CDate(txtRevisionDate.Text).Day <> 1 Then
            lblError.Text = "Effective date should be the first day of the month"
            Return
        End If
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim transaction As SqlTransaction = conn.BeginTransaction("SampleTransaction")
            'If CheckPayMonth() Then
            Dim errorNo As Integer
            errorNo = SaveEMPAddToSalaryDetails(conn, transaction)

            'If radSalScale.Checked Then
            '    errorNo = SaveEMPSalaryDetails(conn, transaction)
            'ElseIf radAddAmount.Checked Then
            '    errorNo = SaveEMPAddToSalaryDetails(conn, transaction)
            'End If
            'If errorNo = 0 Then errorNo = SaveEMPSalarySchedule(conn, transaction)
            If errorNo = 0 Then
                If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), hf_EMP_IDs.Value, "Salary Revised", Page.User.Identity.Name.ToString, Nothing) <> 0 Then
                    transaction.Rollback()
                    lblError.Text = "Failed to update Audit Trail"
                    UtilityObj.Errorlog("Failed to update Audit Trail")
                Else
                    transaction.Commit()
                    lblError.Text = " Salary Revised Successfully"
                    ClearAllDetails()
                End If
            Else
                lblError.Text = UtilityObj.getErrorMessage(errorNo)
                transaction.Rollback()
            End If
        End Using
    End Sub

    Private Sub ClearAllDetails()
        Session("EMPSALDETAILS") = Nothing
        Session("ADDEDEMPSALDETAILS") = Nothing

        hf_EMP_IDs.Value = ""
        hfSalGrade.Value = ""
        txtGrossSalary.Text = ""
        txtSalGrade.Text = ""
        txtRevisionDate.Text = ""
        Gridbind()
        GridBindAddSalaryDetails()
        GridBindSalaryDetails()
    End Sub

    Private Function SaveEMPAddToSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        GetPayMonth_YearDetails()
        Dim iReturnvalue As Integer
        Try
            If Session("EMPSALDETAILS") Is Nothing Then
                Return 0
            End If
            Dim cmd As New SqlCommand
            Dim drSalDet As DataRow

            Dim dttab As DataTable = Session("EMPSALDETAILS")
            If dttab.Rows.Count > 0 Then
                For i As Integer = 0 To dttab.Rows.Count - 1
                    drSalDet = dttab.Rows(i)

                    cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
                    sqlpESH_ID.Value = 0
                    cmd.Parameters.Add(sqlpESH_ID)

                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
                    sqlpUser.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpUser)

                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
                    sqlpESH_EMP_ID.Value = drSalDet("EMP_ID") 'hf_EMP_IDs.Value
                    cmd.Parameters.Add(sqlpESH_EMP_ID)

                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
                    sqlpESH_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpESH_BSU_ID)

                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
                    sqlpESH_ERN_ID.Value = drSalDet("ENR_ID")
                    cmd.Parameters.Add(sqlpESH_ERN_ID)

                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
                    sqlpESH_AMOUNT.Value = GetVal(drSalDet("AMOUNT_NEW"), SqlDbType.Decimal)
                    cmd.Parameters.Add(sqlpESH_AMOUNT)

                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
                    sqlpESH_ELIGIBILITY.Value = GetVal(drSalDet("AMOUNT_ELIGIBILITY_NEW"), SqlDbType.Decimal)
                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
                    sqlpESH_bMonthly.Value = drSalDet("bMonthly")
                    cmd.Parameters.Add(sqlpESH_bMonthly)

                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
                    sqlpESH_PAYTERM.Value = drSalDet("PAYTERM")
                    cmd.Parameters.Add(sqlpESH_PAYTERM)

                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
                    cmd.Parameters.Add(sqlpESH_CUR_ID)

                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
                    sqlpESH_PAY_CUR_ID.Value = ViewState("PAY_CUR_ID") 'FromDataBase
                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
                    sqlpESH_PAYMONTH.Value = ViewState("PAYMONTH") 'FromDataBase
                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
                    sqlpESH_PAYYEAR.Value = ViewState("PAYYEAR")
                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
                    sqlpESH_FROMDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
                    cmd.Parameters.Add(sqlpESH_FROMDT)

                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
                    sqlpbDeleted.Value = False
                    cmd.Parameters.Add(sqlpbDeleted)

                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
                    sqlpESH_TODT.Value = DBNull.Value
                    cmd.Parameters.Add(sqlpESH_TODT)

                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retSValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retSValParam)

                    Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
                    retNewESH_ID.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(retNewESH_ID)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retSValParam.Value
                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                    End If
                Next
                If radSalScale.Checked Then
                    For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
                        Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
                        Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
                        Dim EMP_ID As String
                        If lblEMPID Is Nothing Then
                            Exit For
                        Else
                            EMP_ID = lblEMPID.Text
                        End If
                        iReturnvalue = UpdateEmployeeMaster(objConn, stTrans, EMP_ID, Session("sBSUID"), hfSalGrade.Value)
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                    Next
                End If
            End If
            Return iReturnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
        Return False
    End Function

    'Private Function SaveEMPSalaryDetails(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
    '    GetPayMonth_YearDetails()
    '    Dim iReturnvalue As Integer
    '    Try
    '        If Session("EMPSALDETAILS") Is Nothing Then
    '            Return 0
    '        End If
    '        Dim cmd As New SqlCommand
    '        Dim iIndex As Integer
    '        'Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
    '        Dim EMP_ID As Integer
    '        If Session("EMPSALDETAILS").Rows.Count > 0 Then
    '            For i As Integer = 0 To gvSalRevisionBulk.Rows.Count - 1
    '                Dim dgViewRow As GridViewRow = gvSalRevisionBulk.Rows(i)
    '                Dim lblEMPID As Label = dgViewRow.FindControl("lblEMPID")
    '                If lblEMPID Is Nothing Then
    '                    Exit For
    '                Else
    '                    EMP_ID = lblEMPID.Text
    '                End If
    '                For iIndex = 0 To Session("EMPSALDETAILS").Rows.Count - 1
    '                    Dim dr As DataRow = Session("EMPSALDETAILS").Rows(iIndex)
    '                    cmd = New SqlCommand("SaveEMPSALARYHS_s", objConn, stTrans)
    '                    cmd.CommandType = CommandType.StoredProcedure

    '                    Dim sqlpESH_ID As New SqlParameter("@ESH_ID", SqlDbType.VarChar, 15)
    '                    sqlpESH_ID.Value = 0
    '                    cmd.Parameters.Add(sqlpESH_ID)

    '                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 20)
    '                    sqlpUser.Value = Session("sUsr_name")
    '                    cmd.Parameters.Add(sqlpUser)

    '                    Dim sqlpESH_EMP_ID As New SqlParameter("@ESH_EMP_ID", SqlDbType.VarChar, 15)
    '                    sqlpESH_EMP_ID.Value = EMP_ID 'hf_EMP_IDs.Value
    '                    cmd.Parameters.Add(sqlpESH_EMP_ID)

    '                    Dim sqlpESH_BSU_ID As New SqlParameter("@ESH_BSU_ID", SqlDbType.VarChar, 10)
    '                    sqlpESH_BSU_ID.Value = Session("sBSUID")
    '                    cmd.Parameters.Add(sqlpESH_BSU_ID)

    '                    Dim sqlpESH_ERN_ID As New SqlParameter("@ESH_ERN_ID", SqlDbType.VarChar, 10)
    '                    sqlpESH_ERN_ID.Value = dr("ENR_ID")
    '                    cmd.Parameters.Add(sqlpESH_ERN_ID)

    '                    Dim sqlpESH_AMOUNT As New SqlParameter("@ESH_AMOUNT", SqlDbType.Decimal)
    '                    sqlpESH_AMOUNT.Value = dr("Amount")
    '                    cmd.Parameters.Add(sqlpESH_AMOUNT)

    '                    Dim sqlpESH_ELIGIBILITY As New SqlParameter("@ESH_ELIGIBILITY", SqlDbType.Decimal)
    '                    sqlpESH_ELIGIBILITY.Value = dr("Amount_ELIGIBILITY")
    '                    cmd.Parameters.Add(sqlpESH_ELIGIBILITY)

    '                    Dim sqlpESH_bMonthly As New SqlParameter("@ESH_bMonthly", SqlDbType.Bit)
    '                    sqlpESH_bMonthly.Value = dr("bMonthly")
    '                    cmd.Parameters.Add(sqlpESH_bMonthly)

    '                    Dim sqlpESH_PAYTERM As New SqlParameter("@ESL_PAYTERM", SqlDbType.TinyInt)
    '                    sqlpESH_PAYTERM.Value = dr("PAYTERM")
    '                    cmd.Parameters.Add(sqlpESH_PAYTERM)

    '                    Dim sqlpESH_CUR_ID As New SqlParameter("@ESH_CUR_ID", SqlDbType.VarChar, 6)
    '                    sqlpESH_CUR_ID.Value = Session("BSU_CURRENCY")
    '                    cmd.Parameters.Add(sqlpESH_CUR_ID)

    '                    Dim sqlpESH_PAY_CUR_ID As New SqlParameter("@ESH_PAY_CUR_ID", SqlDbType.VarChar, 6)
    '                    sqlpESH_PAY_CUR_ID.Value = ViewState("PAY_CUR_ID") 'FromDataBase
    '                    cmd.Parameters.Add(sqlpESH_PAY_CUR_ID)

    '                    Dim sqlpESH_PAYMONTH As New SqlParameter("@ESH_PAYMONTH", SqlDbType.TinyInt)
    '                    sqlpESH_PAYMONTH.Value = ViewState("PAYMONTH") 'FromDataBase
    '                    cmd.Parameters.Add(sqlpESH_PAYMONTH)

    '                    Dim sqlpESH_PAYYEAR As New SqlParameter("@ESH_PAYYEAR", SqlDbType.Int)
    '                    sqlpESH_PAYYEAR.Value = ViewState("PAYYEAR")
    '                    cmd.Parameters.Add(sqlpESH_PAYYEAR)

    '                    Dim sqlpESH_FROMDT As New SqlParameter("@ESH_FROMDT", SqlDbType.DateTime)
    '                    sqlpESH_FROMDT.Value = CDate(txtRevisionDate.Text)  'FromDataBase
    '                    cmd.Parameters.Add(sqlpESH_FROMDT)

    '                    Dim sqlpbDeleted As New SqlParameter("@bDeleted", SqlDbType.Bit)
    '                    sqlpbDeleted.Value = False
    '                    cmd.Parameters.Add(sqlpbDeleted)

    '                    Dim sqlpESH_TODT As New SqlParameter("@ESH_TODT", SqlDbType.DateTime)
    '                    sqlpESH_TODT.Value = DBNull.Value
    '                    cmd.Parameters.Add(sqlpESH_TODT)

    '                    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
    '                    retSValParam.Direction = ParameterDirection.ReturnValue
    '                    cmd.Parameters.Add(retSValParam)

    '                    Dim retNewESH_ID As New SqlParameter("@NewESH_ID", SqlDbType.Int)
    '                    retNewESH_ID.Direction = ParameterDirection.Output
    '                    cmd.Parameters.Add(retNewESH_ID)

    '                    cmd.ExecuteNonQuery()
    '                    iReturnvalue = retSValParam.Value
    '                    If iReturnvalue <> 0 Then
    '                        Exit For
    '                    End If
    '                Next
    '                iReturnvalue = UpdateEmployeeMaster(objConn, stTrans, EMP_ID, Session("sBSUID"), hfSalGrade.Value)
    '                If iReturnvalue <> 0 Then
    '                    Exit For
    '                End If
    '            Next
    '        End If
    '        Return iReturnvalue
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '        Return 1000
    '    End Try
    '    Return False
    'End Function

    Private Sub GetPayMonth_YearDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT BSU_PAYMONTH, BSU_PAYYEAR, BSU_CURRENCY FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("sBSUID") & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            ViewState("PAYMONTH") = dr("BSU_PAYMONTH")
            ViewState("PAYYEAR") = dr("BSU_PAYYEAR")
            ViewState("PAY_CUR_ID") = dr("BSU_CURRENCY")
        End While
        dr.Close()
    End Sub

    Private Function UpdateEmployeeMaster(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal EMP_ID As String, ByVal BSU_ID As String, ByVal salGrade As String) As Integer
        Dim status As Integer
        Dim newNo As String = String.Empty
        Dim dt As New DataTable
        'Dim drSalScale As SqlDataReader
        Dim drSalScale As DataRow
        Dim str_sql As String = String.Empty
        str_sql = "select * from EMPLOYEE_M WHERE EMP_ID = '" & _
        EMP_ID & "' AND EMP_BSU_ID = '" & BSU_ID & "'"
        Dim cmd1 As SqlCommand = New SqlCommand(str_sql, conn, trans)
        cmd1.CommandType = CommandType.Text
        Dim sqlAdpt As New SqlDataAdapter(cmd1)
        sqlAdpt.Fill(dt)
        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                drSalScale = dt.Rows(0)
                status = AccessRoleUser.SAVEEMPLOYEE_M(GetVal(Session("sUsr_name")), GetVal(drSalScale("EMPNO")), GetVal(drSalScale("EMP_APPLNO")), _
                GetVal(drSalScale("EMP_SALUTE")), GetVal(drSalScale("EMP_FNAME")), GetVal(drSalScale("EMP_MNAME")), _
                GetVal(drSalScale("EMP_LNAME")), GetVal(drSalScale("EMP_DES_ID")), GetVal(drSalScale("EMP_ECT_ID")), _
                GetVal(drSalScale("EMP_EGD_ID")), GetVal(drSalScale("EMP_BSU_ID")), GetVal(drSalScale("EMP_TGD_ID")), _
                GetVal(drSalScale("EMP_VISATYPE")), GetVal(drSalScale("EMP_VISASTATUS")), _
                GetVal(drSalScale("EMP_JOINDT")), GetVal(drSalScale("EMP_BSU_JOINDT")), GetVal(drSalScale("EMP_LASTVACFROM")), _
                GetVal(drSalScale("EMP_LASTVACTO")), GetVal(drSalScale("EMP_LASTREJOINDT")), GetVal(drSalScale("EMP_bOVERSEAS")), _
                GetVal(drSalScale("EMP_STATUS")), GetVal(drSalScale("EMP_RESGDT")), GetVal(drSalScale("EMP_LASTATTDT")), _
                GetVal(drSalScale("EMP_PROBTILL")), GetVal(drSalScale("EMP_ACCOMODATION")), GetVal(drSalScale("EMP_bACTIVE")), _
                GetVal(drSalScale("EMP_CUR_ID")), GetVal(drSalScale("EMP_PAY_CUR_ID")), GetVal(drSalScale("EMP_MODE")), _
                GetVal(drSalScale("EMP_BANK")), GetVal(drSalScale("EMP_ACCOUNT")), GetVal(drSalScale("EMP_SWIFTCODE")), GetVal(drSalScale("EMP_MOE_DES_ID")), _
                GetVal(drSalScale("EMP_VISA_DES_ID")), GetVal(drSalScale("EMP_VISA_BSU_ID")), GetVal(drSalScale("EMP_MARITALSTATUS")), _
                GetVal(drSalScale("EMP_GROSSSAL")), GetVal(drSalScale("EMP_PASSPORT")), GetVal(drSalScale("EMP_REMARKS")), GetVal(drSalScale("EMP_CTY_ID")), _
                GetVal(drSalScale("EMP_SEX_bMALE")), GetVal(drSalScale("EMP_QLF_ID")), salGrade, GetVal(drSalScale("EMP_DPT_ID")), _
                GetVal(drSalScale("EMP_BLOODGRP")), GetVal(drSalScale("EMP_FATHER")), GetVal(drSalScale("EMP_MOTHER")), _
                GetVal(drSalScale("EMP_REPORTTO_EMP_ID")), GetVal(drSalScale("EMP_BLS_ID")), GetVal(drSalScale("EMP_STAFFNO")), _
                GetVal(drSalScale("EMP_ABC")), GetVal(drSalScale("EMP_bOT"), SqlDbType.Bit), EMP_ID, _
                GetVal(drSalScale("EMp_bTEmp"), SqlDbType.Bit), GetVal(drSalScale("EMP_bCompanyTransport"), SqlDbType.Bit), GetVal(drSalScale("EMP_DOB"), SqlDbType.DateTime), _
                GetVal(drSalScale("EMP_RLG_ID")), GetVal(drSalScale("EMP_LPS_ID")), GetVal(drSalScale("EMP_CONTRACTTYPE"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_MAXCHILDCONESSION"), SqlDbType.Int), GetVal(drSalScale("EMP_LOP_OPN"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_FTE"), SqlDbType.Decimal), GetVal(drSalScale("EMP_bReqPunching"), SqlDbType.Bit), _
                GetVal(drSalScale("EMP_TICKETFLAG"), SqlDbType.Int), GetVal(drSalScale("EMP_TICKETCLASS")), GetVal(drSalScale("EMP_TICKETCOUNT"), SqlDbType.Int), _
                GetVal(drSalScale("EMP_TICKET_CIT_ID")), GetVal(drSalScale("EMP_VISA_ID")), GetVal(drSalScale("EMP_PERSON_ID")), True, newNo, EMP_ID, GetVal(drSalScale("EMP_PASSPORTNAME")), conn, trans)
                Return status
            End If
        End If
        Return -1
    End Function

    Private Function GetVal(ByVal obj As Object, Optional ByVal datatype As SqlDbType = SqlDbType.VarChar) As Object
        If obj Is DBNull.Value Then
            Select Case datatype
                Case SqlDbType.Decimal
                    Return 0
                Case SqlDbType.Int
                    Return 0
                Case SqlDbType.Bit
                    Return False
                Case SqlDbType.VarChar
                    Return ""
            End Select
        Else
            Return obj
        End If
        Return ""
    End Function

    Protected Sub gvEmpSalary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpSalary.RowDataBound
        Try
            Dim lblSchedule As New Label
            Dim lblPayTerm As New Label
            lblSchedule = e.Row.FindControl("lblSchedule")
            lblPayTerm = e.Row.FindControl("lblPayTerm")
            If lblSchedule IsNot Nothing Then
                Select Case lblPayTerm.Text
                    Case 0
                        lblSchedule.Text = "Monthly"
                    Case 1
                        lblSchedule.Text = "Bimonthly"
                    Case 2
                        lblSchedule.Text = "Quarterly"
                    Case 3
                        lblSchedule.Text = "Half-yearly"
                    Case 4
                        lblSchedule.Text = "Yearly"
                End Select
            End If
        Catch
        End Try
    End Sub

    Protected Sub btnSalRevCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalRevCancel.Click
        Session("EMPSALSCHEDULE") = Nothing
        Session("EMPSALDETAILS") = Nothing
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearEMPSALDETAILS()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgEmpSel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEMPSel.Click
        Session("EMPSALDETAILS") = Nothing
        Session("ADDEDEMPSALDETAILS") = Nothing
        Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        PopulateSalaryData(condition)
        Gridbind()
        GridBindSalaryDetails()
        tblSalDetails.Visible = True

        trEditSalAmt.Visible = False
        trEditSalAmt1.Visible = False
        trEditSalAmt2.Visible = False
        trEditSalAmt3.Visible = False
        trEditSalAmt4.Visible = False

        trSalScale1.Visible = True
        trSalScale2.Visible = True
        trSalScale3.Visible = True
        lblError.Text = ""
        txtSalGrade.Text = ""
        hfSalGrade.Value = ""
        txtGrossSalary.Text = ""

        'FillCurrentSalDetails(hf_EMP_IDs.Value)
    End Sub

    Protected Sub btnSalCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalCancel.Click
        ClearEMPSALDETAILS()
        ddSalEarnCode.Enabled = True
    End Sub

    Protected Sub btnSalScale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalScale.Click
        'Dim dtable As DataTable = CreateSalaryDetailsTable()
        If hfSalGrade.Value = "" Then Return
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        Dim grossSal As Decimal
        Dim fteVal As Double = 1
        str_Sql = "Select EMPGRADES_M.EGD_ID FROM EMPGRADES_M RIGHT OUTER JOIN" & _
        " EMPSCALES_GRADES_M ON EMPGRADES_M.EGD_ID = EMPSCALES_GRADES_M.SGD_EGD_ID " & _
        " WHERE EMPSCALES_GRADES_M.SGD_ID =" & hfSalGrade.Value
        'Dim objGrade As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        'If Not objGrade Is DBNull.Value Then
        '    hfGrade_ID.Value = objGrade.ToString
        'End If

        str_Sql = "select EMPGRADE_SALARY_S.*, ERN_DESCR from EMPGRADE_SALARY_S " & _
        " EMPGRADE_SALARY_S LEFT OUTER JOIN EMPSALCOMPO_M ON EMPGRADE_SALARY_S.GSS_ERN_ID = EMPSALCOMPO_M.ERN_ID " & _
        "WHERE GSS_SGD_ID =" & hfSalGrade.Value & " and GSS_DTTO is null ORDER BY ERN_ORDER"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'If Session("EMPSALSCHEDULE") Is Nothing Then
        '    Session("EMPSALSCHEDULE") = CreateTableSalSchedule()
        'End If
        Session("ADDEDEMPSALDETAILS") = CreateSalaryRevDetailsTable()

        Dim dTemptable As DataTable = Session("ADDEDEMPSALDETAILS")
        For index As Integer = 0 To dTemptable.Rows.Count - 1
            dTemptable.Rows(index)("Status") = "DELETED"
        Next
        While dr.Read()
            Dim ldrTempNew As DataRow
            ldrTempNew = dTemptable.NewRow
            grossSal += dr("GSS_AMOUNT")
            'ESL_BSU_ID
            ldrTempNew.Item("UniqueID") = GetNextESHID()
            ldrTempNew.Item("EMP_ID") = GetNextESHID()
            ldrTempNew.Item("ENR_ID") = dr("GSS_ERN_ID")
            ldrTempNew.Item("ERN_DESCR") = dr("ERN_DESCR")
            ldrTempNew.Item("bMonthly") = True
            ldrTempNew.Item("PAYTERM") = 0
            ldrTempNew.Item("Amount") = dr("GSS_AMOUNT") * fteVal
            ldrTempNew.Item("Amount_ELIGIBILITY") = dr("GSS_AMOUNT")
            ldrTempNew.Item("Status") = "EDITED"
            dTemptable.Rows.Add(ldrTempNew)
        End While
        dr.Close()
        Session("ADDEDEMPSALDETAILS") = dTemptable
        GridBindSalaryDetails()

        UpdateSalaryScale()
        Gridbind()
        UpdateSalaryAmounts()
    End Sub

    Private Sub GridBindSalaryDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPSALDETAILS") Is Nothing Then Return
        If Session("ADDEDEMPSALDETAILS") Is Nothing Then
            gvEmpSalary.DataSource = Nothing
            gvEmpSalary.DataBind()
            Return
        End If

        Dim i As Integer
        Dim grossSal As Double
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateSalaryRevDetailsTable()
        If Session("ADDEDEMPSALDETAILS").Rows.Count > 0 Then
            For i = 0 To Session("ADDEDEMPSALDETAILS").Rows.Count - 1
                If (Session("ADDEDEMPSALDETAILS").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("ADDEDEMPSALDETAILS").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If String.Compare(strColumnName, "Amount", True) = 0 Then
                            Dim amt As Double = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                            If Not Session("ADDEDEMPSALDETAILS").Rows(i)("bMonthly") Then
                                amt = amt \ 12
                            End If
                            grossSal += amt
                        End If
                        ldrTempNew.Item(strColumnName) = Session("ADDEDEMPSALDETAILS").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        Session("EMPSALGrossSalary") = grossSal
        gvEmpSalary.DataSource = dtTempDtl
        gvEmpSalary.DataBind()
        txtGrossSalary.Text = Session("EMPSALGrossSalary")
    End Sub

    Private Function GetNextESHID() As Integer
        If ViewState("ESH_ID") Is Nothing Then
            Dim cmd As New SqlCommand
            cmd.Connection = ConnectionManger.GetOASISConnection()
            cmd.CommandText = "select isnull(max(ESH_ID),0) from EMPSALARYHS_s "
            Dim GSS_ID As Integer = CInt(cmd.ExecuteScalar())
            ViewState("ESH_ID") = GSS_ID
        Else
            ViewState("ESH_ID") += 1
        End If
        Return ViewState("ESH_ID")
    End Function

    Protected Sub radSalScale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSalScale.CheckedChanged
        Session("ADDEDEMPSALDETAILS") = Nothing
        ClearNewAmounts()
        Gridbind()
        GridBindAddSalaryDetails()
        hfSalGrade.Value = ""
        txtSalGrade.Text = ""
        txtGrossSalary.Text = ""
        GridBindSalaryDetails()
        trEditSalAmt.Visible = False
        trEditSalAmt1.Visible = False
        trEditSalAmt2.Visible = False
        trEditSalAmt3.Visible = False
        trEditSalAmt4.Visible = False

        trSalScale1.Visible = True
        trSalScale2.Visible = True
        trSalScale3.Visible = True

    End Sub

    Protected Sub radAddAmount_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAddAmount.CheckedChanged

        Session("ADDEDEMPSALDETAILS") = Nothing
        GridBindSalaryDetails()
        Gridbind()
        ClearNewAmounts()
        txtGrossSalary.Text = ""
        trEditSalAmt.Visible = True
        trEditSalAmt1.Visible = True
        trEditSalAmt2.Visible = True
        trEditSalAmt3.Visible = True
        trEditSalAmt4.Visible = True

        trSalScale1.Visible = False
        trSalScale2.Visible = False
        trSalScale3.Visible = False
    End Sub

    Protected Sub gvSalRevisionBulk_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSalRevisionBulk.RowDataBound
        Try 
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                'hlview.NavigateUrl = "PopupSalaryCompareDetails.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text)
                hlview.NavigateUrl = "javascript:ViewDetails('viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "')"
            End If
            Dim dtTempDtl As DataTable = Session("EMPSALDETAILS")
            'Dim dRows() As DataRow
            'dRows = dtTempDtl.Select("EMP_ID = '" & lblEMPID.Text & "'")
            'If dRows.Length > 0 Then
            '    For iColCount As Integer = 0 To dRows.Length - 1
            '        Dim amt As Double = GetVal(dRows(iColCount)("Amount"), SqlDbType.Decimal)
            '        If Not dRows(iColCount)("bMonthly") Then
            '            amt = amt \ 12
            '        End If
            '        grossSal += amt
            '        amt = GetVal(dRows(iColCount)("Amount_NEW"), SqlDbType.Decimal)
            '        If Not dRows(iColCount)("bMonthly") Then
            '            amt = amt \ 12
            '        End If
            '        grossSalNew += amt
            '    Next
            'End If
            'Dim lblEMPAMOUNT As Label = e.Row.FindControl("lblCurrSalAmt")
            'If lblEMPAMOUNT Is Nothing Then
            '    Exit Sub
            'Else
            '    lblEMPAMOUNT.Text = grossSal
            'End If
            'Dim lblEMPAMOUNTNEW As Label = e.Row.FindControl("lblRevSalAmt")
            'If lblEMPAMOUNTNEW Is Nothing Then
            '    Exit Sub
            'Else
            '    lblEMPAMOUNTNEW.Text = grossSalNew
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub txtEmpNo_TextChanged(sender As Object, e As EventArgs)
        txtEmpNo.Text = ""
        Session("EMPSALDETAILS") = Nothing
        Session("ADDEDEMPSALDETAILS") = Nothing
        Dim IDs As String() = hf_EMP_IDs.Value.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        PopulateSalaryData(condition)
        Gridbind()
        GridBindSalaryDetails()
        tblSalDetails.Visible = True

        trEditSalAmt.Visible = False
        trEditSalAmt1.Visible = False
        trEditSalAmt2.Visible = False
        trEditSalAmt3.Visible = False
        trEditSalAmt4.Visible = False

        trSalScale1.Visible = True
        trSalScale2.Visible = True
        trSalScale3.Visible = True
        lblError.Text = ""
        txtSalGrade.Text = ""
        hfSalGrade.Value = ""
        txtGrossSalary.Text = ""

        'FillCurrentSalDetails(hf_EMP_IDs.Value)
    End Sub
End Class
