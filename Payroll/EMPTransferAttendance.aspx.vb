Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports UtilityObj

Partial Class Payroll_EMPTransferAttendance
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                bind_BusinessUnit()
                ddlBSUinit.Enabled = False
                SetTransferDetail()
            End If

            'txtFromDate.Text = Format(New Date(Now.Date.Year, Now.Date.Month, 1), OASISConstants.DateFormat)
            'txtToDate.Text = Format(New Date(Now.Date.Year, Now.Date.Month, Date.DaysInMonth(Now.Date.Year, Now.Date.Month)), OASISConstants.DateFormat)
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub SetTransferDetail()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(0) As SqlClient.SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSUinit.SelectedItem.Value, SqlDbType.VarChar)
        Dim ds As DataSet
        txtFromDate.Enabled = False
        txtToDate.Enabled = False
        imgFromDate.Enabled = False
        imgToDate.Enabled = False
        lblError.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        ds = Mainclass.getDataSet("GetBSUTransferAttendanceDate", param, str_conn)
        trRemSalary.Visible = False
        trSalSummary.Visible = False
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("ReturnValue").ToString = "" Then
                    If IsDate(ds.Tables(0).Rows(0).Item("TODT")) Then
                        txtToDate.Text = Format(ds.Tables(0).Rows(0).Item("TODT"), OASISConstants.DateFormat)
                    End If
                    If IsDate(ds.Tables(0).Rows(0).Item("FROMDT")) Then
                        txtFromDate.Text = Format(ds.Tables(0).Rows(0).Item("FROMDT"), OASISConstants.DateFormat)
                    End If
                    If IsNumeric(ds.Tables(0).Rows(0).Item("PAYMONTH")) And IsNumeric(ds.Tables(0).Rows(0).Item("PAYYEAR")) Then
                        LoadSummary(ds.Tables(0).Rows(0).Item("PAYMONTH"), ds.Tables(0).Rows(0).Item("PAYYEAR"))
                    End If
                    If ds.Tables(0).Rows(0).Item("AllowReTransfer") Then
                        btnTransferAttendance.Enabled = True
                        FillRemSalary()
                    Else
                        btnTransferAttendance.Enabled = False
                    End If
                Else
                    lblError.Text = ds.Tables(0).Rows(0).Item("ReturnValue").ToString
                    If IsDate(ds.Tables(0).Rows(0).Item("TODT")) Then
                        txtToDate.Text = Format(ds.Tables(0).Rows(0).Item("TODT"), OASISConstants.DateFormat)
                    End If
                    If IsDate(ds.Tables(0).Rows(0).Item("FROMDT")) Then
                        txtFromDate.Text = Format(ds.Tables(0).Rows(0).Item("FROMDT"), OASISConstants.DateFormat)
                    End If
                    btnTransferAttendance.Enabled = False
                End If
            End If
        Else
            btnTransferAttendance.Enabled = False
            lblError.Text = "Payroll freeze data not initiated."
        End If
    End Sub
    Private Sub LoadSummary(ByVal PAYMONTH As Int16, ByVal PAYYEAR As Int16)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ESD_MONTH", PAYMONTH, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ESD_YEAR", PAYYEAR, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim mDatatable As New DataTable
            mDatatable = Mainclass.getDataTable("GetEmployeeSalarySummary", sqlParam, str_conn)
            If Not mDatatable Is Nothing Then
                gvSalarySummary.DataSource = mDatatable
                gvSalarySummary.DataBind()
                trSalSummary.Visible = True
                If mDatatable.Rows.Count = 0 Then
                    trSalSummary.Visible = False
                    lblSalSummary.Text = "No Salary processed on this month."
                Else
                    lblSalSummary.Text = "Salary Summary for the month " & MonthName(PAYMONTH).ToString.ToUpper & "-" & PAYYEAR.ToString
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Sub
    Private Sub FillRemSalary()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(0) As SqlClient.SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSUinit.SelectedItem.Value, SqlDbType.VarChar)
        Dim dt As New DataTable
        dt = Mainclass.getDataTable("GetBSUEmployeeToRemoveSalary", param, str_conn)
        If dt.Rows.Count > 0 Then
            gvSalary.DataSource = dt
            gvSalary.DataBind()
            trRemSalary.Visible = True
            lblRemSalaryMsg.text = "Please remove the salary for the following employees before Attendance Transfer."
        Else
            trRemSalary.Visible = False
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim count As Integer = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim str_removed As String = ""
            For i As Integer = 0 To gvSalary.Rows.Count - 1
                Dim lblid As New Label
                Dim chkESD_ID As New HtmlInputCheckBox
                Dim lblESD_EMP_ID As New Label
                Dim hdnESD_ID As New HiddenField
                Dim K As Integer = True
                chkESD_ID = TryCast(gvSalary.Rows(i).FindControl("chkESD_ID"), HtmlInputCheckBox)
                lblESD_EMP_ID = TryCast(gvSalary.Rows(i).FindControl("lblESD_EMP_ID"), Label)
                hdnESD_ID = TryCast(gvSalary.Rows(i).FindControl("hdnESD_ID"), HiddenField)
                'V1.1 changes    
                If chkESD_ID.Checked Then
                    count = count + 1
                    retval = PayrollFunctions.DeleteEMPSALARYDATA(hdnESD_ID.Value, stTrans)
                    str_removed = str_removed & lblESD_EMP_ID.Text & "_" & gvSalary.Rows(i).Cells(2).Text & ","
                    If retval <> 0 Then
                        Exit For
                    End If
                End If
            Next
            If count = 0 Then
                lblError.Text = "No records selected."
                retval = 1000
                Exit Sub
            End If
            If retval = "0" Then
                stTrans.Commit()
                FillRemSalary()
                'V1.1 changes
                Dim flagAudit As Integer
                Dim err As String = ""
                flagAudit = UtilityObj.operOnAudiTable("B100215", str_removed, _
                     "Delete", Page.User.Identity.Name.ToString, Me.Page)
                err = "Salary is Removed Successfully ..."
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = err
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub bind_BusinessUnit()
        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else

            Try
                'if user is super admin give access to all the Business Unit
                If tblbUSuper = True Then
                    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        'create a list item to bind records from reader to dropdownlist ddlBunit
                        Dim di As ListItem
                        ddlBSUinit.Items.Clear()
                        'check if it return rows or not
                        If BUnitreaderSuper.HasRows = True Then
                            While BUnitreaderSuper.Read
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBSUinit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBSUinit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBSUinit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBSUinit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBSUinit.SelectedIndex = -1 Then
                        ddlBSUinit.SelectedIndex = 0
                    End If
                Else
                    'if user is not super admin get access to the selected  Business Unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        Dim di As ListItem
                        ddlBSUinit.Items.Clear()
                        If BUnitreader.HasRows = True Then
                            While BUnitreader.Read
                                di = New ListItem(BUnitreader(2), BUnitreader(0))
                                ddlBSUinit.Items.Add(di)
                                For ItemTypeCounter = 0 To ddlBSUinit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBSUinit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBSUinit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBSUinit.SelectedIndex = -1 Then
                        ddlBSUinit.SelectedIndex = 0
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If

    End Sub

    Protected Sub btnTransferAttendance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransferAttendance.Click
        Try
            Dim cmd As New SqlCommand
            Dim transaction As SqlTransaction
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            cmd = New SqlCommand("SAVE_EMPATTENDANCE_APPROVAL", conn, transaction)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Dim sqlpEAA_BSU_ID As New SqlParameter("@EAA_BSU_ID", SqlDbType.VarChar)
            sqlpEAA_BSU_ID.Value = ddlBSUinit.SelectedValue
            cmd.Parameters.Add(sqlpEAA_BSU_ID)

            Dim sqlpEAA_DATE As New SqlParameter("@EAA_DATE", SqlDbType.DateTime)
            sqlpEAA_DATE.Value = Now.ToString("dd/MMM/yyyy")
            cmd.Parameters.Add(sqlpEAA_DATE)

            Dim sqlpEAA_FROMDT As New SqlParameter("@EAA_FROMDT", SqlDbType.DateTime)
            sqlpEAA_FROMDT.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpEAA_FROMDT)

            Dim sqlpEAA_TODT As New SqlParameter("@EAA_TODT", SqlDbType.DateTime)
            sqlpEAA_TODT.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpEAA_TODT)

            Dim sqlpEAA_APPROVEDBY As New SqlParameter("@EAA_APPROVEDBY", SqlDbType.VarChar)
            sqlpEAA_APPROVEDBY.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpEAA_APPROVEDBY)

            Dim sqlpEAA_REMARKS As New SqlParameter("@EAA_REMARKS", SqlDbType.VarChar)
            sqlpEAA_REMARKS.Value = "Attendance Approved by " & Session("sUsr_name")
            cmd.Parameters.Add(sqlpEAA_REMARKS)

            Dim sqlpSPRVSR_PENDING As New SqlParameter("@SPRVSR_PENDING", SqlDbType.VarChar, 5000)
            sqlpSPRVSR_PENDING.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpSPRVSR_PENDING)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()
            If sqlpSPRVSR_PENDING.Value.ToString = "" Then
                transaction.Commit()
                lblError.Text = "Data Saved Successfully"
            Else
                transaction.Rollback()
                lblError.Text = sqlpSPRVSR_PENDING.Value
            End If
        Catch ex As Exception
        Finally
        End Try
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If Request.UrlReferrer IsNot Nothing Then
            Response.Redirect(Request.UrlReferrer.ToString())
        Else
            Response.Redirect("../Homepage.aspx")
        End If
    End Sub

    Protected Sub ddlBSUinit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUinit.SelectedIndexChanged
        SetTransferDetail()
    End Sub
End Class
