Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empDependantAdd
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSave)

        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Session("FileUpload1") = Nothing
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            lblTitle.Text = "ADD DEPENDENT DETAILS"
            h_Emp_ID.Value = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "U000064" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            UploadDepPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
            BindInsuranceTypes(ddlDepInsuCardTypes)
            bindEmirate()
            rblIsGems.SelectedValue = "N"
            rblIsGems_SelectedIndexChanged(Nothing, Nothing)



            If Request.QueryString("viewid") <> "" Then
                ''  SetDataMode(ViewState("datamode"))
                lblTitle.Text = "EDIT DEPENDENT DETAILS"
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                btnDelete.Visible = True

                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                '' SetDataMode("add")
                btnDelete.Visible = False
                lblTitle.Text = "ADD DEPENDENT DETAILS"
            End If
            reset_values()

            txtDepCountry.Attributes.Add("readonly", "readonly")
            txtDependUnit.Attributes.Add("readonly", "readonly")
            'Added by vikranth on 15th Jan 2020, if login employee BSU_Country_Id is UAE then visa file number is mandatory
            Dim ds As DataSet = GetEMP_Basic_Details()
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Session("EMP_BSU_COUNTRY_ID") = ds.Tables(0).Rows(0)("BSU_COUNTRY_ID").ToString().Trim()
                    If Session("EMP_BSU_COUNTRY_ID").ToString().Trim() = "172" Then
                        spanVisFileNoCheck.Visible = True
                    Else spanVisFileNoCheck.Visible = False
                    End If
                End If
            End If
            'End by vikranth
        End If

        If Session("FileUpload1") Is Nothing AndAlso UploadDepPhoto.HasFile Then
            Session("FileUpload1") = UploadDepPhoto
            ''lblError.Text = UploadDepPhoto.FileName
            ' Next time submit and Session has values but FileUpload is Blank
            ' Return the values from session to FileUpload
        ElseIf Session("FileUpload1") IsNot Nothing AndAlso (Not UploadDepPhoto.HasFile) Then
            UploadDepPhoto = DirectCast(Session("FileUpload1"), FileUpload)
            ''lblError.Text = UploadDepPhoto.FileName
            ' Now there could be another sictution when Session has File but user want to change the file
            ' In this case we have to change the file in session object
        ElseIf UploadDepPhoto.HasFile Then
            Session("FileUpload1") = UploadDepPhoto
            '' lblError.Text = UploadDepPhoto.FileName
        End If
        If UploadDepPhoto.FileName <> "" Then
            Dim strFilePhoto As String = h_DepImagePath.Value
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If
            If UploadDepPhoto.PostedFile.ContentLength > 252500 Then '4MB
                'MsgBox("Photo not to exceed 50 KB in size!!!", MsgBoxStyle.Information, "Photo size!!!")
                lblError.Text = "Photo not to exceed 250 KB in size!!!"
                Exit Sub
            End If
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDepPhoto.FileName
            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            UploadDepPhoto.PostedFile.SaveAs(strFilepath)
            'If hdnAssetImagePath.Value <> "" Then
            Dim EDD_PHOTO As Byte()
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                EDD_PHOTO = EOS_MainClass.ConvertImageFiletoBytes(strFilepath)
                ViewState("imgDependant") = EDD_PHOTO
                setImageDisplay(imgDependant, EDD_PHOTO)
            End If
        End If




    End Sub
    Private Sub reset_values()
        If txtDependUnit.Text = "0" Then
            txtDependUnit.Text = ""
        End If
        If txtEIDExpryDT.Text = "01/Jan/1900" Then
            txtEIDExpryDT.Text = ""
        End If
        If txtDepVissuedate.Text = "01/Jan/1900" Then
            txtDepVissuedate.Text = ""
        End If
        If txtDepVexpdate.Text = "01/Jan/1900" Then
            txtDepVexpdate.Text = ""
        End If

    End Sub
    'Added by vikranth on 15th Jan 2020 for getting EmpId details
    Private Function GetEMP_Basic_Details() As DataSet
        Dim ds As DataSet = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
        Param(0) = Mainclass.CreateSqlParameter("@EMP_ID", ViewState("EmployeeID"), SqlDbType.VarChar)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetEMP_Basic_Details]", Param)
        Return ds
    End Function
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
            EDD_Detail = EOS_EmployeeDependant.GetDependantDetail(p_Modifyid)
            hdnEDDID.Value = EDD_Detail.EDD_ID
            txtDependName.Text = EDD_Detail.EDD_NAME
            If IsDate(EDD_Detail.EDD_DOB) Then
                txtDOB.Text = Format(CDate(EDD_Detail.EDD_DOB), OASISConstants.DateFormat)
            End If
            h_Emp_ID.Value = EDD_Detail.EDD_EMP_ID
            h_Student_id.Value = EDD_Detail.EDD_STU_ID
            Dim lstDrp As New ListItem
            lstDrp = ddlDependRelation.Items.FindByValue(EDD_Detail.EDD_RELATION)
            If Not lstDrp Is Nothing Then
                ddlDependRelation.SelectedValue = lstDrp.Value
            End If


            rblIsGems.SelectedValue = EDD_Detail.EDD_GEMS_TYPE.ToString
            rblIsGems_SelectedIndexChanged(Nothing, Nothing)


            ChkDependConcession.Checked = EDD_Detail.bConcession
            txtStudID.Text = EDD_Detail.EDD_STU_ID.ToString

            ChkDependConcession.Checked = EDD_Detail.bConcession.ToString
            hf_dependant_ID.Value = EDD_Detail.EDD_GEMS_ID.ToString
            hf_dependant_unit.Value = EDD_Detail.EDD_BSU_ID.ToString
            txtDependUnit.Text = EDD_Detail.EDD_BSU_NAME.ToString
            DoNullCheck(ddlDepInsuCardTypes, EDD_Detail.EDD_Insu_id.ToString)
            chkDepCompInsu.Checked = EDD_Detail.EDD_bCompanyInsurance.ToString

            txtDepPassportNo.Text = EDD_Detail.EDD_PASSPRTNO.ToString
            txtDepUidno.Text = EDD_Detail.EDD_UIDNO.ToString
            If IsDate(EDD_Detail.EDD_VISAISSUEDATE) Then
                If EDD_Detail.EDD_VISAISSUEDATE = "1/1/1900" Then

                Else
                    txtDepVissuedate.Text = Format(EDD_Detail.EDD_VISAISSUEDATE, OASISConstants.DateFormat)
                End If

            End If
            If IsDate(EDD_Detail.EDD_VISAEXPDATE) Then
                If EDD_Detail.EDD_VISAEXPDATE = "1/1/1900" Then
                Else
                    txtDepVexpdate.Text = Format(EDD_Detail.EDD_VISAEXPDATE, OASISConstants.DateFormat)
                End If

            End If
            txtDepVissueplace.Text = EDD_Detail.EDD_VISAISSUEPLACE.ToString

            Me.txtEIDNo.Text = EDD_Detail.EDC_DOCUMENT_NO.ToString

            If Convert.ToString(EDD_Detail.EDC_DOCUMENT_NO.ToString) <> "" AndAlso Convert.ToString(EDD_Detail.EDC_DOCUMENT_NO.ToString).Length >= 15 Then
                txtEIDNo1.Text = Convert.ToString(EDD_Detail.EDC_DOCUMENT_NO.ToString).Substring(0, 3)
                txtEIDNo2.Text = Convert.ToString(EDD_Detail.EDC_DOCUMENT_NO.ToString).Substring(3, 4)
                txtEIDNo3.Text = Convert.ToString(EDD_Detail.EDC_DOCUMENT_NO.ToString).Substring(7, 7)
                txtEIDNo4.Text = Convert.ToString(EDD_Detail.EDC_DOCUMENT_NO.ToString).Substring(14, 1)
            End If
            If IsDate(EDD_Detail.EDC_EXP_DT) Then
                If EDD_Detail.EDC_EXP_DT = "1/1/1900" Then
                Else
                    Me.txtEIDExpryDT.Text = Format(Convert.ToDateTime(EDD_Detail.EDC_EXP_DT), OASISConstants.DateFormat)
                End If


            End If

            Me.ddlDependMstatus.SelectedValue = EDD_Detail.EDD_MARITALSTATUS.ToString
            If (Convert.ToBoolean(EDD_Detail.EDD_bMALE) = True) Then
                Me.rdDepdMale.Checked = True
            Else
                Me.rdDepdFemale.Checked = True
            End If

            Me.hfDepCountry_ID.Value = EDD_Detail.EDD_CTY_ID.ToString
            Me.txtDepCountry.Text = EDD_Detail.CTY_NATIONALITY.ToString

            txtDependName.Text = EDD_Detail.EDD_NAME

            If EDD_Detail.EDD_VISASPONSOR.ToString <> "" Then
                ddlSponsor.ClearSelection()
                ddlSponsor.Items.FindByValue(EDD_Detail.EDD_VISASPONSOR.ToString).Selected = True
            End If

            If EDD_Detail.EDD_EMIRATE.ToString <> "" Then
                ddlEmirate.ClearSelection()
                ddlEmirate.Items.FindByValue(EDD_Detail.EDD_EMIRATE).Selected = True
            End If

            If EDD_Detail.EDD_EMIRATE_AREA.ToString <> "" Then
                bindArea()
                ddlArea.ClearSelection()
                ddlArea.Items.FindByValue(EDD_Detail.EDD_EMIRATE_AREA.ToString).Selected = True
            End If
            txtInsCardNum.Text = EDD_Detail.EDD_INSURANCENUMBER
            txtfileno.Text = EDD_Detail.EDD_FILENO
            setImageDisplay(imgDependant, EDD_Detail.EDD_PHOTO)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
            ViewState("imgDependant") = Nothing
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtDependName.Enabled = Not mDisable And (h_Student_id.Value = "0" Or h_Student_id.Value = "")
        txtDOB.Enabled = Not mDisable
        imgDOB.Enabled = Not mDisable
        'imgStudent.Enabled = Not mDisable And (h_Student_id.Value = "0" Or h_Student_id.Value = "")
        ddlDependRelation.Enabled = Not mDisable And (h_Student_id.Value = "0" Or h_Student_id.Value = "")
    End Sub
    Sub clear_All()
        txtDependName.Text = ""
        txtDOB.Text = ""
        imgDependant.ImageUrl = ""
        hdnEDDID.Value = ""
        h_Student_id.Value = ""
        h_DepImagePath.Value = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim DOBDate As String = txtDOB.Text
            If Not IsDate(DOBDate) Then
                lblError.Text = "Invalid Date Of Birth !!!!"
                Exit Sub
            End If

            If ddlDependMstatus.SelectedItem.Value = "-1" Then
                lblError.Text = "Please select  dependant marital status !!!!"
                Exit Sub
            End If
            If txtDependName.Text = "" Then
                lblError.Text = "Please enter  dependant name !!!!"
                Exit Sub
            End If
            If txtDepCountry.Text = "" Then
                lblError.Text = "Please enter  nationality !!!!"
                Exit Sub
            End If
            If ddlDependRelation.SelectedItem Is Nothing Then
                lblError.Text = "Please select the relation !!!!"
                Exit Sub
            End If
            If txtDepPassportNo.Text = "" Then
                If Me.hfDepCountry_ID.Value <> "172" Then ''if it is not emirati 
                    lblError.Text = "Please enter passport number !!!!"
                    Exit Sub
                End If

            End If
            ''EID not required for QATAR schools TASK Id: 74300 on 17Feb2016
            If txtEIDNo1.Text = "" Then
                If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                    lblError.Text = "Please enter emirates id number !!!!"
                    Exit Sub
                End If

            End If
            If txtEIDNo2.Text = "" Then
                If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                    lblError.Text = "Please enter emirates id number !!!!"
                    Exit Sub
                End If
            End If
            If txtEIDNo3.Text = "" Then
                If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                    lblError.Text = "Please enter emirates id number !!!!"
                    Exit Sub
                End If
            End If
            If txtEIDNo4.Text = "" Then
                If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                    lblError.Text = "Please enter emirates id number !!!!"
                    Exit Sub
                End If
            End If
            Dim EidExpDate As Date

            If txtEIDExpryDT.Text = "" Then
                If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                    lblError.Text = "Please enter emirates id expiry date !!!!"
                    Exit Sub
                End If
            ElseIf txtEIDExpryDT.Text <> "" Then
                EidExpDate = IIf(txtEIDExpryDT.Text = "", "01/Jan/1900", txtEIDExpryDT.Text)
                Dim prevMonthdate1 As Date = Date.Today.AddMonths(-1)
                If EidExpDate < prevMonthdate1 Then
                    lblError.Text = "Emirates ID Expiry Date should not be less than 1 month from current date !!!!"
                    Exit Sub
                End If
            End If

            If txtDepUidno.Text = "" Then
                If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" AndAlso hfDepCountry_ID.Value <> "172" Then
                    lblError.Text = "Please enter UID number !!!!"
                    Exit Sub
                End If
            End If

            If txtDepVissuedate.Text = "" Then
                If Me.hfDepCountry_ID.Value <> "172" Then ''if it is not emirati 
                    lblError.Text = "Please enter Visa issued date !!!!"
                    Exit Sub
                End If
            End If

            If txtDepVissueplace.Text = "" Then
                If Me.hfDepCountry_ID.Value <> "172" Then ''if it is not emirati 
                    lblError.Text = "Please enter Visa issued place !!!!"
                    Exit Sub
                End If
            End If
            If Session("EMP_BSU_COUNTRY_ID") = "172" Then
                If txtfileno.Text = "" Then
                    lblError.Text = "Please enter Visa file number !!!"
                    Exit Sub
                End If
            End If

            Dim vIssuedte As Date, vExpDte As Date

            If txtDepVissuedate.Text <> "" Then
                If Me.hfDepCountry_ID.Value <> "172" Then ''if it is not emirati 
                    vIssuedte = txtDepVissuedate.Text

                    If vIssuedte > Date.Now Then
                        lblError.Text = "Visa Issue date should not be future dated !!!!"
                        Exit Sub
                    End If
                    vExpDte = IIf(txtDepVexpdate.Text = "", "01/Jan/1900", txtDepVexpdate.Text)

                    If vIssuedte > vExpDte Then
                        lblError.Text = "Visa Expiry Date should be more than Issue Date !!!!"
                        Exit Sub
                    End If
                End If
            End If
            If txtDepVexpdate.Text = "" Then
                If Me.hfDepCountry_ID.Value <> "172" Then ''if it is not emirati 
                    lblError.Text = "Please enter Visa expiry date !!!!"
                    Exit Sub
                End If
            ElseIf txtDepVexpdate.Text <> "" Then
                If Me.hfDepCountry_ID.Value <> "172" Then ''if it is not emirati 
                    vIssuedte = txtDepVissuedate.Text
                    vExpDte = IIf(txtDepVexpdate.Text = "", "01/Jan/1900", txtDepVexpdate.Text)
                    If vIssuedte > vExpDte Then
                        lblError.Text = "Visa Expiry Date should be more than Issue Date !!!!"
                        Exit Sub
                    End If

                    Dim prevMonthdate As Date = Date.Today.AddMonths(-1)
                    If vExpDte < prevMonthdate Then
                        lblError.Text = "Visa Expiry Date should not be less than 1 month from current date !!!!"
                        Exit Sub
                    End If
                End If


            End If


            Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
            If UploadDepPhoto.FileName <> "" Or h_DepImagePath.Value <> "" Then

                If h_DepImagePath.Value <> "" Then
                    EDD_Detail.EDD_PHOTO = EOS_MainClass.ConvertImageFiletoBytes(h_DepImagePath.Value)
                End If
                If UploadDepPhoto.FileName <> "" Then
                    If UploadDepPhoto.PostedFile.ContentLength > 252500 Then '4MB
                        'MsgBox("Photo not to exceed 50 KB in size!!!", MsgBoxStyle.Information, "Photo size!!!")
                        lblError.Text = "Photo not to exceed 250 KB in size!!!"
                        Exit Sub
                    End If
                    Dim strFilePhoto As String = h_DepImagePath.Value
                    Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                    Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                    If Not Directory.Exists(str_img & "\temp") Then
                        Directory.CreateDirectory(str_img & "\temp")
                    End If
                    Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDepPhoto.FileName
                    Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
                    UploadDepPhoto.PostedFile.SaveAs(strFilepath)
                    'If hdnAssetImagePath.Value <> "" Then

                    If Not IO.File.Exists(strFilepath) Then
                        lblError.Text = "Invalid FilePath!!!!"
                        Exit Sub
                    Else
                        EDD_Detail.EDD_PHOTO = EOS_MainClass.ConvertImageFiletoBytes(strFilepath)
                        ViewState("imgDependant") = EDD_Detail.EDD_PHOTO
                        IO.File.Delete(strFilepath)
                    End If
                End If
                ''\\tsclient\C\Users\nahyan.nayan\Desktop\IMG_1567.jpg

            ElseIf Not ViewState("imgDependant") Is Nothing Then
                EDD_Detail.EDD_PHOTO = ViewState("imgDependant")
            Else
                EDD_Detail.EDD_PHOTO = Nothing
            End If
            If ViewState("datamode") = "add" Then
                EDD_Detail.EDD_ID = 0
            Else
                EDD_Detail.EDD_ID = hdnEDDID.Value
            End If
            EDD_Detail.EDD_DOB = DOBDate
            EDD_Detail.EDD_EMP_ID = h_Emp_ID.Value
            EDD_Detail.EDD_NAME = txtDependName.Text
            EDD_Detail.EDD_RELATION = ddlDependRelation.SelectedItem.Value
            EDD_Detail.EDD_STU_ID = h_Student_id.Value

            EDD_Detail.bConcession = ChkDependConcession.Checked
            EDD_Detail.EDD_NoofTicket = 0
            EDD_Detail.EDD_GEMS_TYPE = rblIsGems.SelectedValue
            EDD_Detail.EDD_GEMS_ID = hf_dependant_ID.Value
            EDD_Detail.EDD_GEMS_TYPE_DESCR = rblIsGems.SelectedItem.Text
            EDD_Detail.EDD_BSU_ID = hf_dependant_unit.Value
            EDD_Detail.EDD_BSU_NAME = txtDependUnit.Text
            EDD_Detail.EDD_bCompanyInsurance = chkDepCompInsu.Checked

            If ddlDepInsuCardTypes.SelectedItem.Text <> "--Select--" Then
                EDD_Detail.EDD_Insu_id = ddlDepInsuCardTypes.SelectedValue
                EDD_Detail.InsuranceCategoryDESC = ddlDepInsuCardTypes.SelectedItem.Text
            Else
                EDD_Detail.EDD_Insu_id = 0
                EDD_Detail.InsuranceCategoryDESC = ""
            End If
            EDD_Detail.EDD_PASSPRTNO = txtDepPassportNo.Text
            EDD_Detail.EDD_UIDNO = txtDepUidno.Text
            EDD_Detail.EDD_VISAISSUEDATE = IIf(txtDepVissuedate.Text = "", "01/Jan/1900", txtDepVissuedate.Text)
            EDD_Detail.EDD_VISAEXPDATE = IIf(txtDepVexpdate.Text = "", "01/Jan/1900", txtDepVexpdate.Text)
            EDD_Detail.EDD_VISAISSUEPLACE = txtDepVissueplace.Text
            EDD_Detail.EDD_bCompanyInsurance = chkDepCompInsu.Checked

            If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
                If txtEIDNo1.Text <> "" Then
                    If txtEIDNo1.Text.Length = "3" Then
                        EDD_Detail.EDC_DOCUMENT_NO = txtEIDNo1.Text
                    Else
                        lblError.Text = "Please enter emirates Id number in valid format"
                        Exit Sub
                    End If


                End If

                If txtEIDNo2.Text <> "" Then

                    If txtEIDNo2.Text.Length = "4" Then
                        EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo2.Text
                    Else
                        lblError.Text = "Please enter emirates Id number in valid format"
                        Exit Sub
                    End If
                End If

                If txtEIDNo3.Text <> "" Then

                    If txtEIDNo3.Text.Length = "7" Then
                        EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo3.Text
                    Else
                        lblError.Text = "Please enter emirates Id number in valid format"
                        Exit Sub
                    End If
                End If
                If txtEIDNo4.Text <> "" Then
                    If txtEIDNo4.Text.Length = "1" Then
                        EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo4.Text
                    Else
                        lblError.Text = "Please enter emirates Id number in valid format"
                        Exit Sub
                    End If


                End If
            Else
                EDD_Detail.EDC_DOCUMENT_NO = ""
            End If
            ''EDD_Detail.EDC_DOCUMENT_NO = Me.txtEIDNo.Text.Trim
            EDD_Detail.EDC_EXP_DT = IIf(Me.txtEIDExpryDT.Text = "", "01/Jan/1900", txtEIDExpryDT.Text)
            EDD_Detail.EDD_bMALE = rdDepdMale.Checked
            EDD_Detail.EDD_MARITALSTATUS = Me.ddlDependMstatus.SelectedValue
            EDD_Detail.EDC_Gender = IIf(rdDepdMale.Checked = True, "M", "F")
            EDD_Detail.EDD_MSTATUS = Me.ddlDependMstatus.SelectedItem.Text
            EDD_Detail.EDD_CTY_ID = Me.hfDepCountry_ID.Value
            EDD_Detail.CTY_NATIONALITY = Me.txtDepCountry.Text

            EDD_Detail.EDD_VISASPONSOR = Convert.ToInt32(ddlSponsor.SelectedValue)
            EDD_Detail.EDD_EMIRATE = Convert.ToInt32(0)
            EDD_Detail.EDD_EMIRATE_AREA = Convert.ToInt32(0)
            EDD_Detail.EDD_INSURANCENUMBER = txtInsCardNum.Text
            EDD_Detail.EDD_FILENO = txtfileno.Text
            Dim retval As String
            Dim new_elh_id As Integer = 0
            retval = EOS_EmployeeDependant.SaveDependantDetail(EDD_Detail)
            If (retval = "0" Or retval = "") Then
                ' lblError.Text = "Data Saved Successfully !!!"
                'hdnEDDID.Value = EDD_Detail.EDD_ID
                'setModifyHeader(EDD_Detail.EDD_ID)
                'SetDataMode("view")
                '  Response.Redirect("empDependantsDetail.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view"), False)
                divDrill.Visible = True
                lbluerror.InnerHtml = "<div style='padding:5pt;font-weight:bold;'>Record updated successfully.</div>"
            Else
                lblError.Text = getErrorMessage(retval)
            End If
            reset_values()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try

    End Sub
    Protected Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        divDrill.Visible = False
        Response.Redirect("empDependantsDetail.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view"), False)
    End Sub

    Protected Sub btClose_Click(sender As Object, e As EventArgs) Handles btClose.Click
        divDrill.Visible = False
        ''Response.Redirect("/ESSDashboard.aspx", False)
    End Sub
    Protected Sub ddlEmirate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmirate.SelectedIndexChanged
        Try
            If ddlEmirate.SelectedValue <> "0" Then
                bindArea()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
        '    setModifyHeader(hdnEDDID.Value)
        '    SetDataMode("view")
        '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'Else
        '    Response.Redirect(ViewState("ReferrerUrl"))
        'End If

        Response.Redirect("empDependantsDetail.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view"), False)
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            retval = EOS_EmployeeDependant.DeleteDependantDetail(hdnEDDID.Value)
            If (retval = "0" Or retval = "") Then
                lblError.Text = "Data Deleted Successfully !!!!"
                clear_All()
                Response.Redirect("empDependantsDetail.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view"), False)
                Exit Sub
            Else
                lblError.Text = retval
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub h_Student_id_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_Student_id.ValueChanged
        Try
            Dim DepID As String
            DepID = EOS_EmployeeDependant.GetDependantIDOfStudent(h_Student_id.Value)
            If DepID <> hdnEDDID.Value And DepID <> "0" And DepID <> "" Then
                lblError.Text = "This Student Profile is already added!!!"
                h_Student_id.Value = ""
                Exit Sub
            End If
            If hdnEDDID.Value <> "" Then
                lblError.Text = "This will update the current profile data with the selected students data!!! Are you sure?"
            End If
            Dim sqlstr As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            sqlstr = "SELECT STU_PASPRTNAME,STU_DOB,STU_PHOTOPATH  from STUDENT_M where STU_ID='" & h_Student_id.Value & "'"
            Dim mSTUTable As DataTable
            mSTUTable = Mainclass.getDataTable(sqlstr, str_conn)
            If mSTUTable.Rows.Count > 0 Then
                txtDependName.Text = mSTUTable.Rows(0).Item("STU_PASPRTNAME")
                txtDOB.Text = Format(CDate(mSTUTable.Rows(0).Item("STU_DOB")), OASISConstants.DateFormat)
                ddlDependRelation.Items.FindByValue("1").Selected = True
                Dim lstDrp As New ListItem
                lstDrp = ddlDependRelation.Items.FindByValue("1")
                If Not lstDrp Is Nothing Then
                    ddlDependRelation.SelectedValue = lstDrp.Value
                End If
                Dim picPath As String
                picPath = mSTUTable.Rows(0).Item("STU_PHOTOPATH")
                Dim EDD_PHOTO As Byte()
                If picPath <> "" And IO.File.Exists(picPath) Then
                    EDD_PHOTO = EOS_MainClass.ConvertImageFiletoBytes(picPath)
                ElseIf Not ViewState("imgDependant") Is Nothing Then
                    EDD_PHOTO = ViewState("imgDependant")
                Else
                    EDD_PHOTO = Nothing
                End If
                setImageDisplay(imgDependant, EDD_PHOTO)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub setImageDisplay(ByVal imgMap As Object, ByVal PhotoBytes As Byte(), Optional ByVal ID As String = "")
        Try
            Dim OC As System.Drawing.Image '= EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            If Not PhotoBytes Is Nothing Then
                ViewState("imgDependant") = PhotoBytes
                OC = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
                ''lblError.Text = "image available"
            Else
                OC = System.Drawing.Image.FromFile(WebConfigurationManager.AppSettings.Item("NoImagePath"))
                '' lblError.Text = "no image"
            End If
            If OC Is Nothing Then Exit Sub
            Dim nameOfImage As String = Session.SessionID & Replace(Now.ToString, ":", "") & ID & "CurrentImage"
            If Not Cache.Get(nameOfImage) Is Nothing Then
                Cache.Remove(nameOfImage)
            End If
            Cache.Insert(nameOfImage, OC, Nothing, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Default, Nothing)
            imgMap.ImageUrl = "empOrgChartImage.aspx?ID=" & nameOfImage
        Catch ex As Exception
            Errorlog(ex.Message)
            ''lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub rblIsGems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblIsGems.SelectedIndexChanged
        'Dim strFilePhoto As String = h_DepImagePath.Value
        If rblIsGems.SelectedValue = "N" Then
            txtDependUnit.Text = ""
            hf_dependant_ID.Value = ""
            hf_dependant_unit.Value = ""
            btnDepUnit.Enabled = False
            imgEmpSel.Visible = False
            txtDependName.Enabled = True
            txtDependName.Text = ""
            imgStudSel.Visible = False
        ElseIf rblIsGems.SelectedValue = "E" Then
            txtDependName.Text = ""
            txtDependUnit.Text = ""
            hf_dependant_unit.Value = ""
            btnDepUnit.Enabled = True
            imgEmpSel.Visible = True
            txtDependName.Enabled = False
            imgStudSel.Visible = False
        Else
            txtDependUnit.Text = ""
            txtDependName.Text = ""
            hf_dependant_unit.Value = ""
            btnDepUnit.Enabled = True
            imgEmpSel.Visible = False
            txtDependName.Enabled = False
            imgStudSel.Visible = True

        End If
        ''added by nahyan to load image after postback
        'If h_DepImagePath.Value <> "" Then
        '    Dim picPath As String
        '    picPath = h_DepImagePath.Value
        '    Dim EDD_PHOTO As Byte()
        '    If picPath <> "" And IO.File.Exists(picPath) Then
        '        EDD_PHOTO = EOS_MainClass.ConvertImageFiletoBytes(picPath)
        '    ElseIf Not ViewState("imgDependant") Is Nothing Then
        '        EDD_PHOTO = ViewState("imgDependant")
        '    Else
        '        EDD_PHOTO = Nothing
        '    End If
        '    setImageDisplay(imgDependant, EDD_PHOTO)
        'End If

    End Sub
    Private Sub BindInsuranceTypes(ByVal ctrl As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim grdlevelID As String = ""


        Dim str_Sql As String = "exec GetInsuranceCardTypes "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ctrl.DataSource = dr
        ctrl.DataTextField = "INSU_DESCR"
        ctrl.DataValueField = "INSU_ID"
        ctrl.DataBind()
        ' ctrl.Items.Insert(0, "--Select--")
        ctrl.Items.Insert(0, New ListItem("--Select--", "0"))

        dr.Close()
    End Sub
    Private Sub DoNullCheck(ByVal ctrl As WebControl, ByVal input As Object)
        If TypeOf (ctrl) Is DropDownList Then
            Dim ddlist As DropDownList = ctrl
            If (Not input Is Nothing) AndAlso (Not input Is DBNull.Value) Then
                If ddlist.Items.FindByValue(input) Is Nothing Then
                    ddlist.SelectedIndex = 0
                Else
                    ddlist.SelectedIndex = -1
                    ddlist.Items.FindByValue(input).Selected = True
                    'ddlist.SelectedValue = input
                End If
            Else
                ddlist.SelectedIndex = 0
            End If
        End If
    End Sub

    Sub bindEmirate()
        ddlEmirate.Items.Clear()



        Dim ds As New DataSet
        Try
            Dim param(3) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", 0)
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "EMIRATE")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
            CommandType.StoredProcedure, "dbo.GET_EMIRATE_CITY", param)

            ddlEmirate.DataSource = ds.Tables(0)
            ddlEmirate.DataTextField = "EMR_DESCR"
            ddlEmirate.DataValueField = "EMR_ID"
            ddlEmirate.DataBind()
            ddlEmirate.SelectedIndex = -1
            ddlEmirate.Items.Insert(0, New ListItem("Select", "0"))
            ddlEmirate_SelectedIndexChanged(ddlEmirate, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind emirate")
        End Try
    End Sub

    Sub bindArea()
        ddlArea.Items.Clear()



        Dim ds As New DataSet
        Try
            Dim param(3) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", Convert.ToInt32(ddlEmirate.SelectedValue))
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "AREA")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
            CommandType.StoredProcedure, "GET_EMIRATE_CITY", param)


            ddlArea.DataSource = ds.Tables(0)
            ddlArea.DataTextField = "EMA_DESCR"
            ddlArea.DataValueField = "EMA_ID"
            ddlArea.DataBind()
            ddlArea.SelectedIndex = -1
            ddlArea.Items.Insert(0, New ListItem("Select", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind area")
        End Try
    End Sub

    Protected Sub lnkbackbtn_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("../ESSDashboard.aspx")
        Response.Redirect(URL)
    End Sub
End Class
