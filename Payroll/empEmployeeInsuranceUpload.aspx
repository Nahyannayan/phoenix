﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empEmployeeInsuranceUpload.aspx.vb" Inherits="Payroll_empEmployeeInsuranceUpload" MasterPageFile="~/mainMasterPage.master" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="cphMasterpage" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Employee Insurance data upload
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="left" width="20%"><span class="field-label">Upload File</span> </td>
                        <td width="30%" align="left">
                            <asp:FileUpload ID="flUpExcel" runat="server" />
                        </td>

                        <td align="left" width="20%">
                            <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvDependancedetails" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Transaction details added yet." Width="100%" CssClass="table table-bordered table-row"
                                DataKeyNames="EMP_ID,EMP_INSU_ID,EMP_INSUFLAG,EMP_INSUFLAG_ELIGIBLE">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField Visible="false">

                                        <ItemTemplate>

                                            <asp:Label ID="lblSlno" runat="server" Text='<%# bind("UniqueID") %>'> </asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpname" runat="server" Text='<%# bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="EMPNO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNO" runat="server" Text='<%# bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Insurance Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsuranceCategoryDESC" runat="server" Text='<%# bind("InsuranceCategoryDESC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Insurance Eligibility">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsuranceEligible" runat="server" Text='<%# bind("EMP_InsuranceEligibleDescr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Insurance Actual">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsurancActual" runat="server" Text='<%# bind("EMP_InsuranceActualDescr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Emirates ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_EMIRATES_ID" runat="server" Text='<%# bind("Emp_EMIRATESID_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Emirates ID Issue Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_EID_IssueDT" runat="server" Text='<%# Eval("Emp_EMIRATESID_IssueDT","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Emirates ID Exp.Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_EID_EXPDT" runat="server" Text='<%# Eval("Emp_EMIRATESID_Expirydt","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Place of Issue">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_EMIRATES_IDPlace" runat="server" Text='<%# bind("Emp_EMIRATESID_Place") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="UID No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp_UIDNO" runat="server" Text='<%# bind("Emp_UIDNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Visa Document No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVISA_No" runat="server" Text='<%# bind("Emp_VISANO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Visa Place of Issue">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEDD_Visa_Place" runat="server" Text='<%# bind("Emp_Visa_place") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Visa Issue Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVisa_IssueDT" runat="server" Text='<%# Eval("Emp_VISA_IssueDT","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Visa Exp.Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVisa_EXPDT" runat="server" Text='<%# Eval("Emp_VISA_Expirydt","{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passport No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp_PASSPORTNO" runat="server" Text='<%# bind("Emp_PASSPORTNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp_CONTACTNO" runat="server" Text='<%# bind("Emp_CONTACTNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp_EMAILID" runat="server" Text='<%# bind("Emp_EMAILID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button runat="server" ID="btnSaveToDB" CssClass="button" Text="Save"
                                Visible="False" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
