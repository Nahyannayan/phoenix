Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_ShowEmpLeave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        gvApprovals.Attributes.Add("bordercolor", "#1b80b6")
        gvLeaveAppHistory.Attributes.Add("bordercolor", "#1b80b6")
        gvHistory.Attributes.Add("bordercolor", "#1b80b6")
        GRIDBIND()
    End Sub



    Sub GRIDBIND()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql, Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT "
            str_Sql &= " EM.EMPNO, ISNULL(EM.EMP_FNAME,'')+' '+"
            str_Sql &= " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')"
            str_Sql &= " AS EMP_NAME   ,  ELA.ELA_DTFROM, ELA.ELA_DTTO, "
            str_Sql &= "dbo.fn_GetLeaveDaysCount(ELA.ELA_DTFROM,ELA.ELA_DTTO,ELA.ELA_BSU_ID, EM.EMP_ID,ELA.ELA_ELT_ID) AS LEAVEDAYS, "
            str_Sql &= " ELA.ELA_REMARKS, ELA.ELA_APPRSTATUS, ELM.ELT_DESCR,ELA.ELA_HANDOVERTXT "
            str_Sql &= " FROM [VW_EmployeeLeaveApp] AS ELA INNER JOIN"
            str_Sql &= " EMPLEAVETYPE_M AS ELM ON ELA.ELA_ELT_ID = ELM.ELT_ID "
            str_Sql &= " INNER JOIN EMPLOYEE_M AS EM "
            str_Sql &= " ON ELA.ELA_BSU_ID = EM.EMP_BSU_ID AND ELA.ELA_EMP_ID = EM.EMP_ID"

          
         
            Dim DocType As String = "LEAVE"
            Dim myELAID, myID As String
            If Request.QueryString("ela_id") Is Nothing Then
                myELAID = "0"
            Else
                myELAID = Request.QueryString("ela_id")
            End If
            If Not myELAID.StartsWith("A") And Not myELAID.StartsWith("M") Then
                myELAID = Request.QueryString("ela_id")
                myID = myELAID
                DocType = "LEAVE"
                str_Sql &= " WHERE ELA.ELA_ID ='" & myELAID & "'"
            Else
                str_Sql &= " WHERE ELA.ID ='" & Request.QueryString("ela_id") & "'"
                If myELAID.StartsWith("A") Or myELAID = "0" Then 'Not edited One
                    myELAID = Replace(myELAID, "A", "")
                    myID = Replace(myELAID, "A", "")
                    DocType = "LEAVE"
                Else ' Edited record
                    myID = Replace(myELAID, "M", "")
                    Sql = "SELECT ELM_ELA_ID FROM EMPLEAVEAPP_MODIFIED where ELM_ID='" & Replace(myELAID, "M", "") & "'"
                    myELAID = Mainclass.getDataValue(Sql, "OASISConnectionString")
                    DocType = "LEAVE-M"
                End If
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            fvLeave.DataSource = ds.Tables(0)
            fvLeave.DataBind()


          


            'str_Sql = "SELECT ISNULL(EM.EMP_FNAME,'')+' '+ " _
            '       & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'') " _
            '       & " + case when APS_USR_ID<>isnull(APS_USR_ASSIGN,0) and isnull(APS_USR_ASSIGN,0)<>0 then ' (' + ISNULL(DEL.EMP_FNAME,'')+' '+  ISNULL(DEL.EMP_MNAME,'')+' '+ISNULL(DEL.EMP_LNAME,'')  + ')' else '' end  " _
            '       & " AS APS_NAME  ," _
            '       & " CASE APS.APS_STATUS" _
            '       & " WHEN 'A' THEN 'Approved' WHEN 'R' THEN 'Rejected'" _
            '       & " WHEN 'N' THEN 'Not Approved' WHEN 'H' THEN 'HOLD'" _
            '       & " END AS APS_STATUS" _
            '       & " ,CASE APS.APS_bREQPPREVAPPROVAL" _
            '       & " WHEN 0 THEN 'No' WHEN 1 THEN 'Yes'" _
            '       & " END AS PREV,CASE  APS.APS_bREQPHIGHAPPROVAL" _
            '       & " WHEN 0 THEN 'No' WHEN 1 THEN 'Yes'" _
            '       & " END AS FINAL,  APS.APS_REMARKS, APS.APS_DATE" _
            '       & " FROM         APPROVAL_S AS APS INNER JOIN" _
            '       & " EMPLOYEE_M AS EM ON APS.APS_USR_ID = EM.EMP_ID" _
            '        & " LEFT OUTER JOIN EMPLOYEE_M AS DEL ON APS.APS_USR_ASSIGN = DEL.EMP_ID " _
            '       & " WHERE (APS.APS_DOC_ID = '" & myID & "' and APS_DOCTYPE='" & DocType & "')" _
            '       & " ORDER BY APS.APS_ORDERID"

            str_Sql = "select * from vw_GetApprovalDetail where APS_DOC_ID='" & myID & "' and APS_DOCTYPE='" & DocType & "' order by APS_ORDERID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count = 0 Then
                str_Sql = "SELECT  ISNULL(EM.EMP_FNAME, '') + " _
                & " ISNULL(EM.EMP_MNAME, '') + ISNULL(EM.EMP_LNAME, '') AS APS_NAME," _
                & " 'REJECTED' AS APS_STATUS,''AS FINAL,'' AS PREV, " _
                & " AR.ARS_REMARKS AS APS_REMARKS, AR.ARS_DATE AS APS_DATE," _
                & " AR.ARS_DOC_ID   AS APS_DOC_ID" _
                & " FROM APPROVAL_REJECTION AS AR INNER JOIN" _
                & " EMPLOYEE_M AS EM ON AR.ARS_USR_ID = EM.EMP_ID" _
                & " WHERE  AR.ARS_bACTIVE =1  AND  AR.ARS_DOC_ID = '" & myELAID & "'"
                ds.Tables.Clear()
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                gvApprovals.Columns(4).Visible = False
                gvApprovals.Columns(5).Visible = False
            End If
            gvApprovals.DataSource = ds.Tables(0)
            gvApprovals.DataBind()
            If gvApprovals.Rows.Count > 0 Then
                gvApprovals.Rows(0).Cells(1).Text = "Applied"
            End If

           


            str_Sql = "GetLeaveHistory '" & myELAID & "','" & Session("sBsuid") & "'"
            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvHistory.DataSource = ds.Tables(0)
            gvHistory.DataBind()

            str_Sql = "SELECT  DISTINCT LAH.LAH_ELT_ID,ELT_DESCR,LAH.LAH_DTFROM,LAH.LAH_DTTO,LAH.LAH_REMARKS,"
            str_Sql &= " CASE LAH_APPRSTATUS WHEN 'N' THEN 'PENDING FOR APPROVAL'"
            str_Sql &= " WHEN 'A' THEN 'APPROVED'"
            str_Sql &= " WHEN 'C' THEN 'CANCELLED'"
            str_Sql &= " END AS APS_STATUS, REPLACE(CONVERT(VARCHAR(50), LAH_LOGDT, 13), ' ', ' ') LAH_LOGDT,CASE WHEN isnull(LAH_Action,'') ='C' THEN 'Applied For Cancellation' ELSE 'Applied For Amendment' END LAH_ACTION "
            str_Sql &= " FROM    dbo.EMPLEAVEAPP_TRAN AS LAH"
            str_Sql &= " INNER JOIN dbo.EMPLEAVETYPE_M  AS EL ON LAH.LAH_ELT_ID =EL.ELT_ID  "
            str_Sql &= " WHERE LAH.LAH_ELA_ID = '" & Val(myELAID) & "'"
            str_Sql &= " ORDER BY LAH_LOGDT ASC"
            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvLeaveAppHistory.DataSource = ds.Tables(0)
            gvLeaveAppHistory.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                trLeaveAppHistory.Visible = True
                trLeaveAppHistoryHeader.Visible = True
            Else
                trLeaveAppHistory.Visible = False
                trLeaveAppHistoryHeader.Visible = False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


End Class
