<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowEmpLeave.aspx.vb" Inherits="Payroll_ShowEmpLeave" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<%--    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />--%>

 <%--   <script type="../text/javascript" src="../cssfiles/tabber.js"></script>--%>

    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <title>Leave Details</title>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
          <div class="card mb-3">
        <div class="card-body">
            <div class="table-responsive">
    <table  width="100%" >
        <tr>
            <td style="vertical-align:top" >
                <asp:FormView ID="fvLeave" runat="server" Width="100%" GridLines="None">
                    <ItemTemplate>
                        <table width="100%">
                              <tr valign="top">
                        <td colspan="2" class="title-bg">
                            Leave Details
                        </td>
                    </tr>
                            <tr>
                                <td width="40%">
                                  <span class="field-label" >  Emp No</span>
                                </td>
                            
                                <td width="60%">
                                    <asp:TextBox ID="lblEmpno" runat="server"  CssClass="inputbox" 
                                        ReadOnly="True" Text='<%# Bind("EMPNO") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                  <span class="field-label" >  Emp Name</span>
                                </td>
                             
                                <td>
                                    <asp:TextBox ID="lblEMP_NAME" runat="server"   CssClass="inputbox" 
                                        ReadOnly="True" Text='<%# Bind("EMP_NAME") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                  <span class="field-label" >  Leave Type</span>
                                </td>
                              
                                <td>
                                    <asp:TextBox ID="lblELT_DESCR" runat="server"  CssClass="inputbox" 
                                        ReadOnly="True" Text='<%#  Bind("ELT_DESCR") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                 <span class="field-label" >   Date From</span>
                                </td>
                               
                                <td>
                                    <asp:TextBox ID="lblELA_DTFROM" runat="server"   CssClass="inputbox" 
                                        ReadOnly="True" Text='<%# Bind("ELA_DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                   <span class="field-label" > Date To</span>
                                </td>
                           
                                <td>
                                    <asp:TextBox ID="lblELA_DTTO" runat="server"  CssClass="inputbox" 
                                        ReadOnly="True" Text='<%# Bind("ELA_DTTO", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                 <span class="field-label" >   Leave days</span>
                                </td>
                              
                                <td>
                                    <asp:TextBox ID="lblLEAVEDAYS" runat="server"   CssClass="inputbox" 
                                        ReadOnly="True" Text='<%#  Bind("LEAVEDAYS") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                 <span class="field-label" >   Remarks</span>
                                </td>
                               
                                <td>
                                    <asp:TextBox ID="lblELA_REMARKS" runat="server"  CssClass="inputbox" 
                                        ReadOnly="True" Text='<%#  Bind("ELA_REMARKS") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                  <span class="field-label" >  Work Handed to</span>
                                </td>
                             
                                <td>
                                    <asp:TextBox ID="lblhandover" runat="server"   CssClass="inputbox" 
                                        ReadOnly="True" Text='<%#  Bind("ELA_HANDOVERTXT") %>'></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:FormView>
            </td>
            <td width="42%" style="vertical-align:top;">
                <table width="100%">
                    <tr >
                        <td class="title-bg">
                            Leave History
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" Width="100%"
                                EmptyDataText="No Data Found" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR" />
                                    <asp:BoundField DataField="MonthYear" HeaderText="Month Year" ReadOnly="True" SortExpression="MonthYear" />
                                    <asp:BoundField DataField="Days" HeaderText="Days" ReadOnly="True" SortExpression="Days" />
                                </Columns>
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                              
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" class="title-bg">
                Approval History
            </td>
        </tr>
        <tr>
            <td align="center"  colspan="2">
                <asp:GridView ID="gvApprovals" runat="server" AutoGenerateColumns="False" EmptyDataText="History empty"
                    Width="100%" CssClass="table table-bordered table-row">
                    <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                    
                    <SelectedRowStyle CssClass="griditem_hilight" />
                    <HeaderStyle CssClass="gridheader_pop" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <Columns>
                        <asp:BoundField DataField="APS_NAME" HeaderText="Name" ReadOnly="True" SortExpression="APS_NAME" />
                        <asp:BoundField DataField="APS_STATUS" HeaderText="Status" ReadOnly="True" SortExpression="APS_STATUS" />
                        <asp:BoundField DataField="APS_REMARKS" HeaderText="Remarks" SortExpression="APS_REMARKS" />
                        <asp:BoundField DataField="APS_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"
                            HtmlEncode="False" SortExpression="APS_DATE">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PREV" HeaderText="Req Prev Approval" ReadOnly="True" SortExpression="PREV">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FINAL" HeaderText="Req Higher Approval" ReadOnly="True"
                            SortExpression="FINAL">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr id="trLeaveAppHistoryHeader" runat="server">
            <td align="left" colspan="2" class="title-bg">
                Leave Application History
            </td>
        </tr>
        <tr id="trLeaveAppHistory" runat="server">
            <td align="center"  colspan="2">
                <asp:GridView ID="gvLeaveAppHistory" runat="server" AutoGenerateColumns="False" EmptyDataText="History empty"
                    Width="100%" CssClass="table table-bordered table-row">
                    <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                    <RowStyle CssClass="griditem" />
                    <SelectedRowStyle CssClass="griditem_hilight" />
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <Columns>
                        <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" />
                        <asp:BoundField DataField="LAH_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                            HtmlEncode="False" SortExpression="LAH_DTFROM">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="LAH_DTTO" HeaderText="To Date" SortExpression="LAH_DTTO"
                            DataFormatString="{0:dd/MMM/yyyy}"></asp:BoundField>
                        <asp:BoundField DataField="LAH_REMARKS" HeaderText="Remarks" SortExpression="LAH_REMARKS" />
                        <asp:BoundField DataField="LAH_Action" HeaderText="Action" />
                        <asp:BoundField DataField="LAH_LOGDT" HeaderText="Log Date" ReadOnly="True" SortExpression="LAH_LOGDT">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="APS_STATUS" HeaderText="Status" ReadOnly="True" SortExpression="APS_STATUS"
                            Visible="False" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
                </div></div></div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
        SelectCommand="GetLeaveHistory" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter Name="ELA_ID" QueryStringField="elaid" Type="Int32" />
            <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
