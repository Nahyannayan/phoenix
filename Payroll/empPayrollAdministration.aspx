﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empPayrollAdministration.aspx.vb" Inherits="Payroll_empPayrollAdministration" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        function change_chk_state(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkEAT_ID/) > 0 || document.forms[0].elements[i].name.search(/chkSelall/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }

        }

        function change_chkArr_state(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkESD_ID/) > 0 || document.forms[0].elements[i].name.search(/chkArrall/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }

        function change_chkLoan_state(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkELD_ID/) > 0 || document.forms[0].elements[i].name.search(/chkLoanall/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }

        function getPageCode(mode) {
            //  sFeatures = "dialogWidth: 700px; dialogHeight: 500px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
            var NameandCode;
            var result;
            var url;


            if (mode == 'SBP') {
                result = radopen("../Common/PopupFormThreeIDHidden.aspx?ID=SalaryBP&multiSelect=false", "pop_up6")

                //if (result == '' || result == undefined) {
                //    return false;
                //}

                <%-- if (result != '' && result != undefined) {
                    NameandCode = result.split('||');
                    document.getElementById('<%=txtBPNo.ClientID %>').value = NameandCode[0];
                   document.getElementById("<%=hfBPNo.ClientID %>").value = NameandCode[0];
                   document.forms[0].submit();
               }
               else {
                   return false;--%>
                //   }
                //               document.getElementById("<%=txtBPNo.ClientID %>").value = NameandCode[1];
                //               document.getElementById("<%=hfBPNo.ClientID %>").value = NameandCode[0];
            }
        }



        function OnClientClose6(oWnd, args) {
            //get the transferred arguments            
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBPNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=hfBPNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtBPNo.ClientID%>', 'TextChanged');

            }
        }

        function GetEMPName(mode) {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            document.getElementById('<%=hfMode.ClientID%>').value = mode;
            if (mode == 'ERS') {

                result = radopen("../Accounts/accShowEmpDetail.aspx?id=ERS", "pop_up5")

               <%-- if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                   document.getElementById('<%=txtLEmpName.ClientID %>').value = NameandCode[0];
                    //
                    return true;
                }--%>

            }
            if (mode == 'ERA') {

                result = radopen("../Accounts/accShowEmpDetail.aspx?id=ERS", "pop_up5")

                <%--if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                   document.getElementById('<%=txtArrearEmpName.ClientID %>').value = NameandCode[0];
                    //
                    return true;
                }--%>

            }

            return false;
        }



        function OnClientClose5(oWnd, args) {
            //get the transferred arguments

            var mode;
            mode = document.getElementById('<%=hfMode.ClientID%>').value;

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                if (mode == 'ERS') {

                    document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=txtLEmpName.ClientID %>').value = NameandCode[0];
                    __doPostBack('<%= txtLEmpName.ClientID%>', 'TextChanged');

                }

                else if (mode == 'ERA') {

                    document.getElementById('<%=hfMode.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=hfMode.ClientID %>').value = NameandCode[0];
                    __doPostBack('<%= txtArrearEmpName.ClientID%>', 'TextChanged');

                }

            }
        }


        function GetEMPTransName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up4")
          <%--  if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
               document.getElementById('<%=txtTranEmpName.ClientID %>').value = NameandCode[0];
               //
               return true;
           }
           return false;--%>
        }

        function OnClientClose4(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtLEmpName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtTranEmpName.ClientID%>', 'TextChanged');

            }
        }

        function GetEMPNameGVS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up3")
          <%-- if (result != '' && result != undefined) {
               NameandCode = result.split('___');
               document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
               document.getElementById('<%=txtMoveEmpName.ClientID %>').value = NameandCode[0];
               //
               return true;
           }
           return false;--%>
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtMoveEmpName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtMoveEmpName.ClientID%>', 'TextChanged');

            }
        }

        function GetEMPNameLoan() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up2")
           <%--if (result != '' && result != undefined) {
               NameandCode = result.split('___');
               document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
               document.getElementById('<%=txtLoanEmpName.ClientID %>').value = NameandCode[0];
               //
               return true;
           }
           return false;--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtLoanEmpName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtLoanEmpname.ClientID%>', 'TextChanged');

            }
        }


        function GetResignEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: vertical; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = '../Common/PopUpEmployeesList.aspx?id=EMPLOYEES_RESIGNED';
            result = radopen(url, "pop_up")

          <%-- if (result != '' && result != undefined) {
               NameandCode = result.split('___');
               document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtLEmpName.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtArrearEmpName.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        //Added by vikranth on 22nd Oct 2019
        function OnClientClose7(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtUpdateEmpName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtUpdateEmpName.ClientID%>', 'TextChanged');

            }
        }
        //Added by vikranth on 22nd Oct 2019
        function GetEMPNameUpdateEMPDOJ() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=DOJ", "pop_up7")
           <%--if (result != '' && result != undefined) {
               NameandCode = result.split('___');
               document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
               document.getElementById('<%=txtLoanEmpName.ClientID %>').value = NameandCode[0];
               //
               return true;
           }
           return false;--%>
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up6" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose6"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <%--Added by vikranth on 22nd Oct 2019--%>
        <Windows>
            <telerik:RadWindow ID="pop_up7" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <table width="100%" border="0">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
            </td>
        </tr>
        <telerik:RadTabStrip runat="server" ID="rtsAdminMenus" SelectedIndex="0" AutoPostBack="true"
            Font-Names="Helvetic" ScrollChildren="false" EnableDragToReorder="true" Width="100%" Font-Bold="true"
            MultiPageID="plAdminMenus">
            <Tabs>
                <telerik:RadTab Text="BP Removal" PageViewID="RPVBPremoval" Value="1" PostBack="true" Selected="true"></telerik:RadTab>
                <telerik:RadTab Text="Gratuity/Leave Salary SetUp" PageViewID="RPVBGratuity" Value="2" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Resignation Withdrawal" PageViewID="RPVResigWithdrawal" Value="3" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Ignore Leaves from salary" PageViewID="RPVLeaveReset" Value="4" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Update Transferred Employee's On Leave status" Value="5" PageViewID="RPVUpdateStatus" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Move employee to GVS" PageViewID="RPVMoveEmployee" Value="6" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Update WPS Contact Details" PageViewID="RPVWPS" Value="7" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Change BSU pay month/year" PageViewID="RPVPaymonth" Value="8" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Set Loan as Paid" PageViewID="RPVLoan" PostBack="true" Value="9" Visible="true"></telerik:RadTab>
                <telerik:RadTab Text="Remove employee from gratuity and LS provisioning" Value="10" PageViewID="RPVResigWithdrawal" PostBack="true"></telerik:RadTab>
                <telerik:RadTab Text="Remove Arrears" Value="11" PageViewID="RPVArrears" PostBack="true"></telerik:RadTab>
                <%--Added by vikranth on 22nd Oct 2019--%>
                <telerik:RadTab Text="Update Employee Date of Join" Value="12" PageViewID="RPVUpdateEMPDOJ" PostBack="true"></telerik:RadTab>
                 <%--Added by vikranth on 23rd Oct 2019--%>
                <telerik:RadTab Text="Update Employee Reports To" Value="13" PageViewID="RPVUpdateEMPReportsTo" PostBack="true"></telerik:RadTab>
                
            </Tabs>
        </telerik:RadTabStrip>

        <tr>
            <td align="left" style="text-align: left;">Fields Marked with (<font color="red">*</font>) are mandatory
            </td>
        </tr>
    </table>
    <telerik:RadMultiPage runat="server" ID="plAdminMenus" Width="100%"
        ScrollBars="none">
        <telerik:RadPageView runat="server" ID="RPVBPremoval" Selected="true">
            <div>
                <asp:Panel ID="pnlBPRemoval" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="tblMain"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="3">Bank Payment Removal</td>
                        </tr>
                        <tr>
                            <td width="20%"><span class="field-label">Select BP Number to Remove</span><font color="red">*</font>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtBPNo" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:ImageButton ID="imgBPSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getPageCode('SBP'); return false;" />
                                <asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtBPNo"
                                    ErrorMessage="Select a BP" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table id="tblDetails" runat="server" align="center"
                                    cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                                    <tr class="title-bg">
                                        <td align="left" colspan="4">Details</td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Voucher Date</span></td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblVoucherDt" CssClass="field-value" runat="server"></asp:Label></td>
                                        <td align="left" width="20%"><span class="field-label">Total Amount </span></td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblAmount" CssClass="field-value" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Narration </span></td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblNarration" CssClass="field-value" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Business Unit </span></td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblBSU" CssClass="field-value" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">BSU Closed Date </span></td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblBSUCloseDt" CssClass="field-value" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
                                            <span class="field-label">Removal Remarks</span><font color="red">*</font></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtRemovalRemarks" runat="server" MaxLength="500"
                                                TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="text-align: center">
                                            <asp:Button ID="btnRemoveBP" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Remove" OnClientClick="return window.confirm('You are about to remove this BP.Please click OK to Confirm!');" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>


                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RPVBGratuity">
            <asp:Panel ID="pnlGratuity" runat="server" Width="100%" Height="100%">
                <table align="center" runat="server" id="tblGratuity"
                    cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                    <tr class="title-bg">
                        <td align="left" colspan="2">Gratuity/Leave Salary Initial SetUp</td>
                    </tr>


                    <tr>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rblGratLeave" runat="server" RepeatDirection="Horizontal" CssClass="field-label">
                                <asp:ListItem Text="Gratuity SetUp" Value="G"></asp:ListItem>
                                <asp:ListItem Text="Leave Salary SetUp" Value="L"></asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSetUp" runat="server" CssClass="button" Text="Copy Default Settings" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="RPVResigWithdrawal">
            <div>
                <asp:Panel ID="pnlResignation" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="tblResignation"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Resignation Withdrawal</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtEmpNo" runat="server" AutoPostBack="true" OnTextChanged="txtEmpNo_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetResignEMPName(); return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                                    ErrorMessage="Select Employee" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                            <td align="left" width="20%"><span class="field-label">Empno</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblEmpno" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Group Join Date</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblDOJ" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Employee Catagory </span>

                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="txtCat" CssClass="field-value" runat="server"></asp:Label>
                            </td>


                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Employee Designation </span></td>
                            <td align="left" width="30%">
                                <asp:Label ID="txtSD" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Department</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="txtDept" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Resignation date </span></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblResgDt" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Last Working Date</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblLWD" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Reference BP(FFS)</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblBPNo" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Reference JV(FFS)</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblJVNo" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Remarks</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox runat="server" ID="txtWithdrawRemarks" TextMode="MultiLine" Rows="3" MaxLength="500"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnRemoveProvision" runat="server" CssClass="button" Text="Set as Final Settlement Over" Visible="false" />
                                <asp:Button ID="btnWithdrawResign" runat="server" CssClass="button" Text="Withdraw Resignation"
                                    OnClientClick="return window.confirm('You are about to make resigned employee active.Please click OK to Confirm!');" />
                                <asp:Button ID="btnCancelWithdraw" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="RPVLeaveReset">
            <div>
                <asp:Panel ID="pnlLeaves" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="tblLeaves"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Ignore leaves from Payroll proce  ssing</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtLEmpName" runat="server" AutoPostBack="true" OnTextChanged="txtLEmpName_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgLEmpname" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetEMPName('ERS'); return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLEmpName"
                                    ErrorMessage="Select Employee" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                            <td align="left" width="20%"><span class="field-label">Empno</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblLEmpNo" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="title-bg" runat="server" id="tr_Deatailhead">
                            <td colspan="4" align="left">Unpaid Leave Details 
         <asp:Label ID="lblWeekend" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                        </tr>

                        <tr runat="server" id="tr_Deatails">
                            <td colspan="4" align="center">
                                <asp:GridView ID="gvAttendance" runat="server" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                <input id="chkSelall" type="checkbox" onclick="javascript: change_chk_state(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="chkEAT_ID" runat="server" value='<%# Bind("id") %>' type="checkbox" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="date" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"
                                            HtmlEncode="False" />
                                        <asp:TemplateField HeaderText="Leave Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLeave" runat="server" Text='<%# Bind("leavetype") %>'></asp:Label>
                                                <asp:Label ID="lblAdj_ID" runat="server" Text='<%# Bind("EAT_ADJ_ID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="remarks" ControlStyle-Width="208px" HeaderText="Remarks" />
                                        <asp:BoundField DataField="days" ControlStyle-Width="208px" HeaderText="Days(0.5/1)" />

                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_new" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <EmptyDataRowStyle />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Remarks</span><font color="red">*</font></td>
                            <td colspan="3" align="left">
                                <asp:TextBox runat="server" ID="txtLeaveRemarks" Width="80%" TextMode="MultiLine" Rows="3" MaxLength="500"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSetLeavesPaid" runat="server" CssClass="button" Text="Ignore leaves from salary"
                                    OnClientClick="return window.confirm('You are about to set the leave days as paid.Please click OK to Confirm!');" />
                                <asp:Button ID="btnCancelLeaves" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="RPVMoveEmployee">
            <div>
                <asp:Panel ID="Panel1" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="Table1"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Move Employee record to GVS</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee</span><font color="#c00000">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtMoveEmpName" runat="server" AutoPostBack="true" OnTextChanged="txtMoveEmpName_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgMoveEmp" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetEMPNameGVS(); return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmpNo"
                                    ErrorMessage="Select Employee" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                            <td align="left" width="20%"><span class="field-label">Empno</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblMoveEmpno" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Group Join Date</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblGDOJ" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">BSU Join Date  </span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblBSUDOJ" CssClass="field-value" runat="server"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Employee Designation </span></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblDes" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Department</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblDpt" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Employee Category</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblCat" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Gross Salary</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblGross" CssClass="field-value" runat="server"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Remarks</span><font color="red">*</font> </td>
                            <td align="left">
                                <asp:TextBox ID="txtGVSRemarks" runat="server" TextMode="multiLine"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnMoveToGVS" runat="server" CssClass="button" Text="Move to GVS"
                                    OnClientClick="return window.confirm('You are about to remove employee record from current unit.Please click OK to Confirm!');" />
                                <asp:Button ID="btnCancelMove" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="RPVUpdateStatus">
            <div>
                <asp:Panel ID="pnlUpdateStatus" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="tblUpdateStatus"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Update last rejoin date and status of transferred employees.</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee </span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtTranEmpName" runat="server" AutoPostBack="true" OnTextChanged="txtTranEmpName_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgTranEmployee" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetEMPTransName(); return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTranEmpName"
                                    ErrorMessage="Select Employee" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                            <td align="left" width="20%"><span class="field-label">Empno </span></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblTranEmpno" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Group Join Date</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblTranGDOJ" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">BSU Join Date</span>

                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblTranBSUDOJ" CssClass="field-value" runat="server"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Employee Designation</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblTranDes" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Department</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblTranDpt" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>

                            <td align="left" width="20%"><span class="field-label">Last Working Unit</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblLastUnit" CssClass="field-value" runat="server"></asp:Label>

                            </td>
                            <td align="left" width="20%"><span class="field-label">Employee Category </span></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblTranCat" CssClass="field-value" runat="server"></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Last Vacation From</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblVacFrom" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Last Vacation To </span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblVacTo" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Last Rejoin Date </span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtLastRejoinDt" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSaveRejoin" runat="server" CssClass="button" Text="Update Rejoin date and employee status"
                                    OnClientClick="return window.confirm('You are about to change employee status to Active from On Leave.Please click OK to Confirm!');" />
                                <asp:Button ID="btnCancelRejoin" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RPVWPS">
            <asp:Panel runat="server" ID="pnlWPS" Width="100%" Height="100%">
                <table align="center" runat="server" id="tblWPS"
                    cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                    <tr class="title-bg">
                        <td align="left" colspan="4">Update WPS/DA Transfer Letter Details</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">Business Unit</td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblBsuName" CssClass="field-value" runat="server"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Business Unit's name in WPS/DA Letter</span><span style="color: Red"><i>(if different from BSU name)</i></span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtWPSUnitName" runat="server" MaxLength="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Accounts Officer Name </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAccOffName" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Accounts Officer Email Id </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAccEmailId" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Accounts Officer Mobile Number </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtMobNo" runat="server" MaxLength="30"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Accounts Officer landline Number </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtlandline" runat="server" MaxLength="30"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Routing Code(9 digit)</span> </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtRoutingCode" runat="server" MaxLength="9"></asp:TextBox>

                        </td>
                        <td align="left" width="20%"><span class="field-label">Employer ID(13 digit)</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEstablishmentCode" runat="server" MaxLength="13"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSaveWPS" runat="server" CssClass="button" Text="Update Details" CausesValidation="true"
                                OnClientClick="return window.confirm('These changes would be updated in businessunit master.Please click OK to Confirm!');" />
                            <asp:Button ID="btnCancelWPS" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RPVPaymonth" runat="server">
            <asp:Panel runat="server" ID="pnlPaymonth" Width="100%" Height="100%">
                <div>
                    <table align="center" runat="server" id="tblPaymonth"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Change BSU pay month/year </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Current Pay Month</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblPaymonth" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Current Pay Year</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblPayYear" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trMonth_Year">
                            <td align="left" width="20%"><span class="field-label">Open Month</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlPayMonth" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Open Year</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlPayYear" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Remarks</span><font color="red">*</font> </td>
                            <td align="left">
                                <asp:TextBox ID="txtPaymonthRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnUpdatePaymonth" runat="server" CssClass="button" Text="Open Pay month/Year" CausesValidation="true"
                                    OnClientClick="return window.confirm('You are about to open selected pay month/year for all payroll/finance processes.Please click OK to Confirm!');" />
                                <asp:Button ID="btnCancelPaymonth" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RPVLoan" runat="server">
            <div>
                <asp:Panel ID="pnlLoan" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="tblLoan"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Set employee loan as paid</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtLoanEmpname" runat="server" AutoPostBack="true" OnTextChanged="txtLoanEmpname_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="ImgLoan" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetEMPNameLoan(); return false;" />
                            </td>
                            <td align="left" width="20%"><span class="field-label">Empno</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblLoanEmpno" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:GridView ID="gvLoanDetails" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row" EmptyDataText="No Unpaid Loan Details Available">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <input id="chkLoanall" type="checkbox" onclick="javascript: change_chkLoan_state(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="chkELD_ID" runat="server" value='<%# Bind("ELD_ID") %>' type="checkbox" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ERN_DESCR" ControlStyle-Width="208px" HeaderText="Type" />
                                        <asp:BoundField DataField="ELH_PURPOSE" ControlStyle-Width="208px" HeaderText="Purpose" />
                                        <asp:BoundField DataField="MONTHYEAR" ControlStyle-Width="208px" HeaderText="Month/Year" />
                                        <asp:BoundField DataField="Installment" ControlStyle-Width="208px" HeaderText="Installment No." />
                                        <asp:BoundField DataField="ELD_AMOUNT" ControlStyle-Width="208px" HeaderText="Installment Amount" />
                                        <asp:BoundField DataField="ELD_PAIDAMT" ControlStyle-Width="208px" HeaderText="Paid Amount" />
                                        <asp:TemplateField HeaderText="Amount Settled">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPaidAmt" runat="server" Text='<%# Bind("ELD_AMOUNT") %>'> </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPaidRemarks" MaxLength="80" runat="server" TextMode="MultiLine"> </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_new" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <EmptyDataRowStyle />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSaveLoanPayment" runat="server" CssClass="button" Text="Save Loan payment" CausesValidation="true"
                                    OnClientClick="return window.confirm('You are about to update the selected loan payments.Please click OK to Confirm!');" />
                                <asp:Button ID="btnCancelLoan" runat="server" CssClass="button" Text="Cancel" />

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RPVArrears">
            <div>
                <asp:Panel ID="pnlArrears" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="tblArrears"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="95%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Set Arrears as Paid</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtArrearEmpName" runat="server" AutoPostBack="true" OnTextChanged="txtArrearEmpName_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgArrearSearch" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetEMPName('ERA'); return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtArrearEmpName"
                                    ErrorMessage="Select Employee" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                            <td align="left" width="20%"><span class="field-label">Empno </span></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblArrearEmpNo" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="title-bg" runat="server" id="tr1">
                            <td colspan="4" align="left">Arrears Details
                            </td>
                        </tr>

                        <tr runat="server" id="tr2">
                            <td colspan="4" align="center">
                                <asp:GridView ID="gvArrears" CssClass="table table-bordered table-row" runat="server" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                <input id="chkArrall" type="checkbox" onclick="javascript: change_chkArr_state(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="chkESD_ID" runat="server" value='<%# Bind("ESD_ID") %>' type="checkbox" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="MONTHYEAR" HeaderText="Month/Year" />
                                        <asp:BoundField DataField="AMOUNT" HeaderText="AMOUNT" />

                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_new" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <EmptyDataRowStyle />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Remarks</span><font color="red">*</font> </td>
                            <td align="left">
                                <asp:TextBox ID="txtArrearRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSetArrearsPaid" runat="server" CssClass="button" Text="Set arrears as paid" CausesValidation="true"
                                    OnClientClick="return window.confirm('You are about to set selected arrears as paid.Please click OK to Confirm!');" />
                                <asp:Button ID="btnArrearsCancel" runat="server" CssClass="button" Text="Cancel" />

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>
        <%--Added by vikranth on 22nd Oct 2019--%>
        <telerik:RadPageView ID="RPVUpdateEMPDOJ" runat="server">
            <div>
                <asp:Panel ID="pnlUpdateEMPDOJ" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="Table2"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Update Employee Date of Join</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Employee</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtUpdateEmpName" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:ImageButton ID="ImgUpdteEMPDOJ" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetEMPNameUpdateEMPDOJ(); return false;" />
                            </td>
                            <td align="left" width="20%"><span class="field-label">Empno</span> </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblUpdateEmpNo" CssClass="field-value" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Date of Join</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtEmpDOJ" runat="server"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgEmpDOJ" TargetControlID="txtEmpDOJ">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtEmpDOJ" TargetControlID="txtEmpDOJ">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="imgEmpDOJ" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            </td>
                            <td align="left" width="20%"><span class="field-label">BSU Date of Join</span><font color="red">*</font></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtEmpBsuDOJ" runat="server"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgEmpBSUDOJ" TargetControlID="txtEmpBsuDOJ">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtEmpBsuDOJ" TargetControlID="txtEmpBsuDOJ">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="imgEmpBSUDOJ" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Remarks</span><font color="red">*</font> </td>
                            <td align="left">
                                <asp:TextBox ID="txtUpdateEMPDOJRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnUpdateEMPDOJ" runat="server" CssClass="button" Text="Update Date of Join" CausesValidation="true"
                                    OnClientClick="return window.confirm('You are about to update the selected employee date of join.Please click OK to Confirm!');" />
                                <asp:Button ID="btnupdateEMPDOJCancel" runat="server" CssClass="button" Text="Cancel" />

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>
        <%--Added by vikranth on 23rd Oct 2019--%>
        <telerik:RadPageView ID="RPVUpdateEMPReportsTo" runat="server">
            <div>
                <asp:Panel ID="PnlUpdateEMPReportsTo" runat="server" Width="100%" Height="100%">
                    <table align="center" runat="server" id="Table3"
                        cellpadding="3" cellspacing="0" style="border-collapse: collapse;" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4">Update Employee Reports To</td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Bulk Upload</span><font color="red">*</font></td>
                            <td align="left">
                                <asp:FileUpload ID="flUpExcel" runat="server" />
                                <%--<asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Update" />--%>
                                <%--<asp:LinkButton ID="lnkDownload" Text = "Download" runat="server"></asp:LinkButton>--%>
                            </td>
                        </tr>                        
                        <tr>
                            <td align="left"><span class="field-label">Remarks</span><font color="red">*</font> </td>
                            <td align="left">
                                <asp:TextBox ID="txtUpdateEMPReportsToRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnUpdateEMPReportsTo" runat="server" CssClass="button" Text="Update" CausesValidation="true"
                                    OnClientClick="return window.confirm('You are about to update employee Reports To.Please click OK to Confirm!');" />
                                <asp:Button ID="btnUpdateEMPReportsToCancel" runat="server" CssClass="button" Text="Cancel" />

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <input id="hfBPNo" runat="server" type="hidden" />
    <asp:HiddenField ID="h_Emp_No" runat="server" />
    <asp:HiddenField ID="hfMode" runat="server" />
    <asp:HiddenField ID="hfEmployeeNum" runat="server" />
</asp:Content>
