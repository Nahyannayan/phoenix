<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empPostPayroll.aspx.vb" Inherits="Payroll_empPostPayroll" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_Disable() {

            if (confirm("You are about to close the logged in Business Unit's current Pay Month.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("reports\/aspx\/SelIDDESC.aspx?ID=EMP", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Post Payroll"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" class="matters"  > <span class="field-label"> Business Unit</span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"  ><span class="field-label">Select Month &amp; Year</span></td>
                        <td align="left" class="matters"  >
             <asp:GridView ID="gvMonthyear" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="False">
                 <Columns>
                     <asp:TemplateField>
                         <HeaderTemplate>
                             <input id="chkSelectall" type="checkbox" onclick="change_chk_state('0')" />
                         </HeaderTemplate>
                         <ItemTemplate>
                             &nbsp;<input id="chkMONTH_YEAR" runat="server" type="checkbox" value='<%# Bind("MONTH_YEAR") %>' />
                         </ItemTemplate>
                         <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                     </asp:TemplateField>
                     <asp:BoundField DataField="MONTHYEAR" HeaderText="MONTHYEAR" ReadOnly="True" SortExpression="MONTHYEAR">
                         <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                     </asp:BoundField>
                     <asp:BoundField DataField="TOTAL" HeaderText="TOTAL" ReadOnly="True" SortExpression="TOTAL">
                         <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                     </asp:BoundField>
                     <asp:TemplateField HeaderText="Pay Date">
                         <ItemTemplate>
                             <asp:TextBox ID="txtDate" runat="server"  Text='<%# Bind("PAYDATE") %>'></asp:TextBox>
                             <asp:Label ID="lblIsFFS" runat="server" Text='<%# IIF(Eval("FinalSettlement")="1","(Final Settlement)","") %>'></asp:Label>
                         </ItemTemplate>
                         <ItemStyle VerticalAlign="Middle" />
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" ><span class="field-label">Current Pay Month &amp; Year</span></td>
                        <td align="left" class="matters" >
                            <asp:TextBox ID="txtPayMonth" runat="server" CssClass="inputbox"  
                                MaxLength="100" TabIndex="1" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"  colspan="2">
                            <asp:CheckBox ID="chkClose" runat="server" Text="Close Current Pay Month" CssClass="field-label"
                                AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="2" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Post Payroll" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>


