<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empRecurringDed.aspx.vb" Inherits="Payroll_empRecurringDed" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">

        function GetEMPName() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_emp");
           <%-- result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //document.forms[0].submit();
                return true;
            }--%>
            return false;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');;
                document.getElementById('<%=txtEmpNo.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=h_Emp_No.ClientID%>').value = NameandCode[1];
                  __doPostBack('<%=txtEmpNo.ClientID%>', 'TextChanged');
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-usd mr-3"></i>
            Salary Standard Deduction
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td>
                            <table align="centre" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="error"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left">

                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                                            ValidationGroup="VALEDITSAL" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" align="center">
                                            <tr>
                                                <td width="20%"><span class="field-label">Employee No</span></td>
                                                <td width="30%" align="left">
                                                    <asp:TextBox ID="txtEmpNo" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;
                                        <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="return GetEMPName();" />
                                                    <asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtEmpNo"
                                                        ErrorMessage="Select Employee" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator></td>

                                                <td align="left" width="20%"><span class="field-label">Type</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true">
                                                        <asp:ListItem Value="1" Enabled="False">EARNINGS</asp:ListItem>
                                                        <asp:ListItem Selected="True" Value="0">DEDUCTION</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Deduction/Earning Code</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddSalEarnCode" runat="server" ValidationGroup="VALADDNEWSAL">
                                                    </asp:DropDownList></td>

                                                <td align="left"><span class="field-label">Amount</span></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtSalAmount" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSalAmount"
                                                        ErrorMessage="Enter the Salary Amount" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator></td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">From Date</span></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator
                                                        ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFromDate"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator>
                                                    <ajaxToolkit:CalendarExtender ID="calFrom" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgFrom"
                                                        TargetControlID="txtFromDate">
                                                    </ajaxToolkit:CalendarExtender>

                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                        PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="rfvFromDt" runat="server" ControlToValidate="txtFromDate"
                                                        ErrorMessage="Enter From date" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator>
                                                </td>

                                                <td align="left"><span class="field-label">To Date</span></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtToDate" runat="server" Width="110px"></asp:TextBox>
                                                    <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                        runat="server" ControlToValidate="txtToDate" Display="Dynamic" EnableViewState="False"
                                                        ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                        ValidationGroup="VALSALDET">*</asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                                        ErrorMessage="Enter To date" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator>
                                                    <ajaxToolkit:CalendarExtender ID="calTo" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgTo"
                                                        TargetControlID="txtToDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                        PopupButtonID="txtToDate" TargetControlID="txtToDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Remarks</span></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                                        CssClass="inputbox_multi" Rows="4" SkinID="MultiText"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                        ErrorMessage="Enter  Remarks" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr visible="false" runat="server">
                                                <td align="center" colspan="4">
                                                    <asp:Button ID="btnExtend" runat="server" CssClass="button" Text="Extend"
                                                        ValidationGroup="VALSALDET" />
                                                    <asp:Button ID="btnSalCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnAdd" runat="server" CssClass="button"
                    Text="Add" />
                <div align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button"
                        Text="Save" ValidationGroup="VALEDITSAL" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                </div>
                <table width="100%">
                    <tr>
                        <td class="title-bg">
                            <strong>Salary Standard Deductions</strong><strong> History</strong>
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <asp:GridView CssClass="table table-bordered table-row" ID="gvEmpSalHistory" runat="server" AutoGenerateColumns="False" SkinID="GridViewNormal"
                                Width="98%" AllowPaging="True" EmptyDataText="No Data to display" PageSize="5">
                                <Columns>
                                    <asp:BoundField DataField="ERN_TYP" HeaderText="Type" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="DedTYPe" HeaderText="Type" ReadOnly="True" />
                                    <asp:BoundField DataField="ENR_ID" HeaderText="Code" ReadOnly="True" />


                                    <asp:TemplateField ShowHeader="False" HeaderText="From">
                                        <HeaderTemplate>
                                            From
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            &nbsp;
                                    <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("ERD_STARTDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>&nbsp;
                                  
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False" HeaderText="To"
                                        ConvertEmptyStringToNull="False">
                                        <HeaderTemplate>
                                            To
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            &nbsp;
                                    <asp:Label ID="lblToDate" runat="server" Text='<%# Bind("ERD_ENDDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("ERD_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" ReadOnly="True">

                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:TemplateField ShowHeader="False" HeaderText="Action">
                                        <HeaderTemplate>
                                            Action
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkEdit" Text="Edit/Delete"
                                                OnClick="lnkEdit_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ERD_ID" HeaderText="ERD_ID" ReadOnly="True" />
                                    <asp:BoundField DataField="tableName" HeaderText="tableName" ReadOnly="True" />

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br />
    <asp:HiddenField ID="h_Emp_No" runat="server" />
</asp:Content>
