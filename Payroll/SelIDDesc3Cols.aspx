<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelIDDesc3Cols.aspx.vb" Inherits="SelIDDesc3Cols" %>
<%@ OutputCache Duration="1" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Cheque Selection</title>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <base target="_self" />
    
    
    
   
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
         </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
    <table id="tbl" width="100%" align="center">
                    <tr valign = "top">
                        <td>
                            <asp:GridView ID="gvGroup" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False"
                              SkinID="GridViewView" Width="100%" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                            <asp:TextBox ID="txtCode" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                        <HeaderTemplate>
                                           <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                           <asp:TextBox ID="txtBSUName" runat="server" Width="75%"></asp:TextBox>
                                           <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="CODE" >
                                        <HeaderTemplate>
                                            <asp:Label ID="lblCitCode" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                            <asp:TextBox ID="txtCitCode" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnCitCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Bind("DESCR2") %>'  ID="lblDataCode" ></asp:Label>
                                      
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /></td>
                        
                    </tr> 
                </table>
           
    </form>
</body>
</html>