Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Payroll_empViewEmpMaster
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then

            h_Grid.Value = "top"
            lblError.Text = ""
            ''''' 
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_11.Value = "LI__../Images/operations/like.gif"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or _
            (ViewState("MainMnu_code") <> "P050090") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, _
                ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
                gvEMPDETAILS.Attributes.Add("bordercolor", "#1b80b6")
                ddlFilter.Items.FindByValue("1").Selected = True
                gridbind()
                If Request.QueryString("deleted") <> "" Then
                    lblError.Text = getErrorMessage("520")
                End If
                Dim url As String
                ViewState("datamode") = Encr_decrData.Encrypt("add")
                url = "empMasterDetail.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                hlAddNew.NavigateUrl = url
            End If
        End If
    End Sub

    Public Function returnpath(ByVal p_bPhotos As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_bPhotos)
            If p_bPhotos Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvEMPDETAILS.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvEMPDETAILS.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Protected Sub gvEMPDETAILS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPDETAILS.PageIndexChanging
        gvEMPDETAILS.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvEMPDETAILS_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMPDETAILS.RowDataBound
        Try
            Dim lblEMPID As Label
            lblEMPID = TryCast(e.Row.FindControl("lblEMPID"), Label)
            Dim cmdCol As Integer = gvEMPDETAILS.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblEMPID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                'hlview.NavigateUrl = "empMasterDetail.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                hlview.NavigateUrl = "empMasterDetail.aspx?viewid=" & Encr_decrData.Encrypt(lblEMPID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&FilterMode=" & ddlFilter.SelectedValue & "&Statusid=" & ddlStatus.SelectedValue
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim str_Filter As String = ""

        Dim lstrEmpDOJ As String = String.Empty
        Dim lstrEMPNo As String = String.Empty
        Dim lstrPrevEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty
        Dim lstrPassportNo As String = String.Empty
        Dim lstrDesignation As String = String.Empty
        Dim lstrVisaID As String = String.Empty

        Dim lstrFiltDOJ As String = String.Empty
        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltPrevEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty
        Dim lstrFiltPassportNo As String = String.Empty
        Dim lstrFiltDesignation As String = String.Empty
        Dim lstrFiltVisaID As String = String.Empty

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim ddlgvStatus As DropDownList
        Dim lstrStatus As String = String.Empty


        If gvEMPDETAILS.Rows.Count > 0 Then
            ' --- Initialize The Variables


            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)


            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDOJ")
            lstrEmpDOJ = Trim(txtSearch.Text)
            If (lstrEmpDOJ <> "") Then lstrFiltDOJ = SetCondn(lstrOpr, "EMP_JOINDT", lstrEmpDOJ)

            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EMPNO", lstrEMPNo)


            larrSearchOpr = h_Selected_menu_11.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPrevEmpNo")
            lstrPrevEMPNo = Trim(txtSearch.Text)
            If (lstrPrevEMPNo <> "") Then lstrFiltPrevEMPNo = SetCondn(lstrOpr, "EMP_STAFFNO", lstrPrevEMPNo)



            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPassportNo")
            lstrPassportNo = txtSearch.Text
            If (lstrPassportNo <> "") Then lstrFiltPassportNo = SetCondn(lstrOpr, "EMP_PASSPORT", lstrPassportNo)

            '   -- 5  COLLUN
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDesignation")
            lstrDesignation = txtSearch.Text
            If (lstrDesignation <> "") Then lstrFiltDesignation = SetCondn(lstrOpr, "EMP_DES_DESCR", lstrDesignation)

            larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtVisaID")
            lstrVisaID = txtSearch.Text
            If (lstrVisaID <> "") Then lstrFiltVisaID = SetCondn(lstrOpr, "EMP_VISA_ID", lstrVisaID)


            ''ddlgvStatus = gvEMPDETAILS.HeaderRow.FindControl("ddlgvStatus")
            ''If ddlgvStatus.SelectedItem.Value <> "0" Then
            ''    lstrStatus = " AND EMP_STATUS = '" & ddlgvStatus.SelectedItem.Value & "'"
            ''Else
            ''    lstrStatus = ""
            ''End If


            If ddlStatus.SelectedItem.Value <> "0" Then
                lstrStatus = " AND EMP_STATUS = '" & ddlStatus.SelectedItem.Value & "'"
            Else
                lstrStatus = ""
            End If


        End If

        If ddlFilter.SelectedItem.Value = "1" Then
            str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT, EMP_VISA_ID, " & _
            " EMP_PASSPORT, EMP_STATUS_DESCR,  EMP_DES_DESCR," & _
            " CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 0 ELSE 1 END bPhotoExists,EMP_STAFFNO  " & _
            " FROM vw_OSO_EMPLOYEEMASTER" & _
            " WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' " _
            & str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltPrevEMPNo & lstrFiltEmpName & _
            lstrFiltDesignation & lstrFiltPassportNo & lstrFiltVisaID & lstrStatus & " order by EMP_STATUS_DESCR,EMPNAME"
        ElseIf ddlFilter.SelectedItem.Value = "2" Then
            str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT, EMP_VISA_ID, " & _
                        " EMP_PASSPORT, EMP_STATUS_DESCR,  EMP_DES_DESCR," & _
                        " CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 0 ELSE 1 END bPhotoExists,EMP_STAFFNO  " & _
                        " FROM vw_OSO_EMPLOYEEMASTER" & _
                        " WHERE EMP_VISA_BSU_ID = '" & Session("sBsuid") & "' " _
                        & str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltPrevEMPNo & lstrFiltEmpName & _
                        lstrFiltDesignation & lstrFiltPassportNo & lstrFiltVisaID & lstrStatus & " order by EMP_STATUS_DESCR,EMPNAME"
        Else
            str_Sql = "SELECT EMP_ID, EMPNO, EMPNAME, EMP_JOINDT, EMP_VISA_ID, " & _
                                    " EMP_PASSPORT, EMP_STATUS_DESCR,  EMP_DES_DESCR," & _
                                    " CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 0 ELSE 1 END bPhotoExists,EMP_STAFFNO  " & _
                                    " FROM vw_OSO_EMPLOYEEMASTER" & _
                                    " WHERE (EMP_BSU_ID = '" & Session("sBsuid") & "' OR EMP_VISA_BSU_ID = '" & Session("sBsuid") & "')" _
                                    & str_Filter & lstrFiltDOJ & lstrFiltEMPNo & lstrFiltPrevEMPNo & lstrFiltEmpName & _
                                   lstrFiltDesignation & lstrFiltPassportNo & lstrFiltVisaID & lstrStatus & " order by EMP_STATUS_DESCR,EMPNAME"
        End If




        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPDETAILS.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEMPDETAILS.DataBind()
            Dim columnCount As Integer = gvEMPDETAILS.Rows(0).Cells.Count

            gvEMPDETAILS.Rows(0).Cells.Clear()
            gvEMPDETAILS.Rows(0).Cells.Add(New TableCell)
            gvEMPDETAILS.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEMPDETAILS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEMPDETAILS.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvEMPDETAILS.DataBind()
        End If

        'gvJournal.DataBind()
        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDOJ")
        txtSearch.Text = lstrEmpDOJ

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPrevEmpNo")
        txtSearch.Text = lstrPrevEMPNo

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtPassportNo")
        txtSearch.Text = lstrPassportNo

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtVisaID")
        txtSearch.Text = lstrVisaID

        txtSearch = gvEMPDETAILS.HeaderRow.FindControl("txtDesignation")
        txtSearch.Text = lstrDesignation

        gvEMPDETAILS.SelectedIndex = p_selected_id

        'ddlgvStatus = gvEMPDETAILS.HeaderRow.FindControl("ddlgvStatus")
        'If lstrStatus <> "" And lstrStatus <> "0" Then
        '    ddlgvStatus.SelectedItem.Value = lstrStatus
        ''End If
    End Sub

    'Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
    '    Try
    '        Dim url As String
    '        ViewState("datamode") = "add"
    '        Dim Queryusername As String = Session("sUsr_name")
    '        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
    '        url = "empMasterDetail.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '    End Try
    'End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        gridbind()
    End Sub



    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
End Class
