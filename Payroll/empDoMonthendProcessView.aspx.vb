Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empDoMonthendProcessView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_AbortTransaction(sender As Object, e As EventArgs) Handles Me.AbortTransaction

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empDoMonthendProcessAirfare.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P174015" And ViewState("MainMnu_code") <> "P174010" And ViewState("MainMnu_code") <> "P174005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "P174015"
                            lblHead.Text = "Air fare"
                            ViewState("trantype") = "A"
                        Case "P174010"
                            lblHead.Text = "Gratuity"
                            ViewState("trantype") = "G"
                        Case "P174005"
                            lblHead.Text = "Leave Salary"
                            ViewState("trantype") = "L"
                    End Select
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""


            str_Sql = "SELECT MJH.MJH_DOCDT, MJH.MJH_DOCNO, SUM(JNL.JNL_DEBIT) AS AMOUNT, JNL.JNL_NARRATION , 0 IsFromDAX " _
                & " FROM MONTHLYJV_H AS MJH INNER JOIN JOURNAL_D AS JNL ON MJH.MJH_DOCDT = JNL.JNL_DOCDT " _
                & " AND MJH.MJH_DOCNO = JNL.JNL_DOCNO AND MJH.MJH_DOCTYPE = JNL.JNL_DOCTYPE " _
                & " AND MJH.MJH_BSU_ID = JNL.JNL_BSU_ID" _
                & " GROUP BY MJH.MJH_DOCNO, MJH.MJH_BSU_ID, MJH.MJH_DOCDT, JNL.JNL_NARRATION,MJH.MJH_TYPE" _
                & " HAVING (MJH.MJH_BSU_ID = '" & Session("sBSUID") & "') AND (MJH.MJH_TYPE = '" & ViewState("trantype") & "')"
            '& " ORDER BY MJH.MJH_DOCDT DESC"
            If Session("BSU_IsOnDAX") = 1 Then
                str_Sql = str_Sql & " Union  SELECT MJH.MJH_DOCDT, MJH.MJH_DOCNO, SUM(JNL.JNL_DEBIT) AS AMOUNT, JNL.JNL_NARRATION, 1 IsFromDAX " _
                   & " FROM MONTHLYJV_H AS MJH INNER JOIN OASIS_DAX.FIN.JOURNAL_D AS JNL ON MJH.MJH_DOCDT = JNL.JNL_DOCDT " _
                   & " AND MJH.MJH_DOCNO = JNL.JNL_DOCNO AND MJH.MJH_DOCTYPE = JNL.JNL_DOCTYPE " _
                   & " AND MJH.MJH_BSU_ID = JNL.JNL_BSU_ID" _
                   & " GROUP BY MJH.MJH_DOCNO, MJH.MJH_BSU_ID, MJH.MJH_DOCDT, JNL.JNL_NARRATION,MJH.MJH_TYPE" _
                   & " HAVING (MJH.MJH_BSU_ID = '" & Session("sBSUID") & "') AND (MJH.MJH_TYPE = '" & ViewState("trantype") & "')" _
                   & " ORDER BY MJH.MJH_DOCDT DESC"
            Else
                str_Sql = str_Sql & " ORDER BY MJH.MJH_DOCDT DESC"
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            Dim lblIsDax As New Label, lbVoucher As New LinkButton, lblPosted As New Label
            lblIsDax = TryCast(e.Row.FindControl("lblIsDax"), Label)
            lbVoucher = TryCast(e.Row.FindControl("lbVoucher"), LinkButton)
            lblPosted = TryCast(e.Row.FindControl("lblPosted"), Label)
            If lblIsDax.Text = "1" Then
                lbVoucher.Visible = False
                lblPosted.Visible = True
            Else
                lbVoucher.Visible = True
                lblPosted.Visible = False

            End If
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empLeaveHistory.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Session("ReportSource") = AccountsReports.JournalVouchers(sender.Parent.parent.cells(0).text, String.Format("{0:MM-dd-yyyy}", CDate(sender.Parent.parent.cells(1).text)), Session("SBSUID"), Session("F_YEAR"))
            'Session("ReportSource") = AccountsReports.JournalVouchers(sender.Parent.parent.cells(0).text, String.Format("{0:dd/MMM/yyyy}", CDate(sender.Parent.parent.cells(1).text)), Session("SBSUID"), Session("F_YEAR"))
            Session("ReportSource") = VoucherReports.JournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "JV", sender.Parent.parent.cells(0).text, True)
            ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbBankTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            Dim strFiltBSUID As String = "WHERE     (ESD_BSU_ID = '" & Session("sBsuid") & "') AND (EBT_DATE = CONVERT(DATETIME, '" & sender.Parent.parent.cells(4).text & "', 102))"
            Dim str_Sql As String = "SELECT     * FROM         VW_OSO_SALARYTRANSFERTOBANK " & _
            strFiltBSUID
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("UserName") = Session("sUsr_name")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../payroll/Reports/RPT/rptSALARYTRANSFERTOBANK.rpt"
                Session("ReportSource") = repSource
                ' Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True) 
                ReportLoadSelection()
            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
