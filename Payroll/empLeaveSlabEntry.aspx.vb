Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Telerik.Web.UI
Imports System.Web.UI.HtmlControls.HtmlIframe
Partial Class Payroll_empLeaveSlabEntry
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                BindStatus()
                If Not Request.QueryString("sdate") Is Nothing Then
                    h_SDate.Value = CDate(Request.QueryString("sdate")).ToString("dd/MMM/yyyy")
                End If
                If Not Request.QueryString("empid") Is Nothing Then
                    h_Emp_ID.Value = Request.QueryString("empid")
                End If
                If Not Request.QueryString("eltid") Is Nothing Then
                    h_ELT_ID.Value = Request.QueryString("eltid")
                End If
                If Not Request.QueryString("ShowDocSubmit") Is Nothing Then
                    trDocSubmitted.Visible = IIf(Request.QueryString("ShowDocSubmit") = "1", True, False)
                End If
                BindOtherLeaveSlabs()
                BindLeaveSlabDetails(h_Emp_ID.Value, h_SDate.Value, 0)
            End If
            If h_SelectedId.Value <> "Close" Then
                Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)
                Response.Write("} </script>" & vbCrLf)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindOtherLeaveSlabs()
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT 0 ELR_ID,'----------Select----------' ELR_DESCR   UNION ALL "
        str_Sql &= " SELECT ELR_ID,replace(convert(varchar(20),ELR_DTFROM,106),' ','/')  + ' To ' + replace(convert(varchar(20),ELR_DTTO,106),' ','/') ELR_DESCR FROM EMPLEAVERANGE  "
        str_Sql &= " WHERE ELR_BSU_ID ='" & Session("sBsuid") & "'"
        str_Sql &= "AND ELR_EMP_ID =" & h_Emp_ID.Value

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        RadOtherLeaveSlab.DataSource = ds.Tables(0)
        RadOtherLeaveSlab.DataTextField = "ELR_DESCR"
        RadOtherLeaveSlab.DataValueField = "ELR_ID"
        RadOtherLeaveSlab.DataBind()
        RadOtherLeaveSlab.SelectedIndex = 0
    End Sub
    Private Sub BindStatus()
        Dim str_Sql As String
        Dim ds As New DataSet

        str_Sql = "SELECT DISTINCT ISNULL(EAS_ELT_ID, EAS_DESCR) EAS_ELT_ID, EAS_DESCR FROM EMPATTENDANCE_STATUS INNER JOIN BSU_LEAVESLAB_S ON EAS_ELT_ID=BLS_ELT_ID "
        str_Sql &= " AND BLS_BSU_ID ='" & Session("sBsuid") & "'"
        str_Sql &= " UNION ALL SELECT 'PRESENT','PRESENT'"
        str_Sql &= " UNION ALL SELECT 'HO','Holiday'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        RadLeaveType.DataSource = ds.Tables(0)
        RadLeaveType.DataTextField = "EAS_DESCR"
        RadLeaveType.DataValueField = "EAS_ELT_ID"
        RadLeaveType.DataBind()
    End Sub
    Private Sub BindLeaveSlabDetails(ByVal EMP_ID As String, ByVal sDate As String, ByVal ELR_ID As Integer)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            If ELR_ID = 0 Then
                str_Sql = "select EMP_FNAME + ' ' + EMP_MNAME  + ' ' + EMP_LNAME FROM EMPLOYEE_M WHERE EMP_ID='" & EMP_ID & "'"
                lblEmployeeName.Text = Mainclass.getDataValue(str_Sql, "OASISConnectionString")
                str_Sql = "SELECT ELR_ID,ELR_ELT_ID,ELR_DTFROM,ELR_DTTO,ELR_REMARKS,ELR_DOCSUBMITTED FROM EMPLEAVERANGE "
                str_Sql &= " WHERE ELR_BSU_ID='" & Session("sBSUID") & "' AND ELR_EMP_ID='" & EMP_ID & "'"
                str_Sql &= " AND '" & sDate & "' BETWEEN ELR_DTFROM AND ELR_DTTO"
            Else
                str_Sql = "select EMP_FNAME + ' ' + EMP_MNAME  + ' ' + EMP_LNAME FROM EMPLOYEE_M WHERE EMP_ID='" & EMP_ID & "'"
                lblEmployeeName.Text = Mainclass.getDataValue(str_Sql, "OASISConnectionString")
                str_Sql = "SELECT ELR_ID,ELR_ELT_ID,ELR_DTFROM,ELR_DTTO,ELR_REMARKS,ELR_DOCSUBMITTED FROM EMPLEAVERANGE "
                str_Sql &= " WHERE ELR_ID=" & ELR_ID
            End If

            Dim mTable As New DataTable
            mTable = Mainclass.getDataTable(str_Sql, str_conn)

            If mTable.Rows.Count > 0 Then
                h_ELR_ID.Value = mTable.Rows(0).Item("ELR_ID")
                UsrFromDate.SelectedDate = CDate(mTable.Rows(0).Item("ELR_DTFROM")).ToString("dd/MMM/yyyy")
                UsrToDate.SelectedDate = CDate(mTable.Rows(0).Item("ELR_DTTO")).ToString("dd/MMM/yyyy")
                txtRemarks.Text = mTable.Rows(0).Item("ELR_REMARKS").ToString
                chkDocSubmitted.Checked = mTable.Rows(0).Item("ELR_DOCSUBMITTED")
                Dim lstItem As New RadComboBoxItem
                lstItem = RadLeaveType.Items.FindItemByValue(mTable.Rows(0).Item("ELR_ELT_ID"))
                If Not lstItem Is Nothing Then RadLeaveType.SelectedValue = lstItem.Value
                lstItem = RadOtherLeaveSlab.Items.FindItemByValue(mTable.Rows(0).Item("ELR_ID"))
                If Not lstItem Is Nothing Then RadOtherLeaveSlab.SelectedValue = lstItem.Value
                DisableControls(True)
            Else
                UsrFromDate.SelectedDate = CDate(Now).ToString("dd/MMM/yyyy")
                UsrToDate.SelectedDate = CDate(Now).ToString("dd/MMM/yyyy")
                txtRemarks.Text = ""
                h_ELR_ID.Value = 0
                Dim lstItem As New RadComboBoxItem
                lstItem = RadLeaveType.Items.FindItemByValue(h_ELT_ID.Value)
                If Not lstItem Is Nothing Then RadLeaveType.SelectedValue = lstItem.Value
                DisableControls(False)
            End If
            ViewState("RetValue") = txtRemarks.Text & "___" & chkDocSubmitted.Checked
            txtRemarks.Focus()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub DisableControls(ByVal mDisable As Boolean)
        UsrFromDate.Enabled = Not mDisable
        UsrToDate.Enabled = Not mDisable
        txtRemarks.Enabled = Not mDisable
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        ' RadLeaveType.Enabled = Not mDisable
    End Sub
    Private Sub ClearAll()
        UsrFromDate.SelectedDate = Now.Date
        UsrToDate.SelectedDate = Now.Date
        txtRemarks.Text = ""
        RadOtherLeaveSlab.SelectedIndex = 0
    End Sub
    Private Function SaveEmpLeaveSlab() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(8) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ELR_ID", h_ELR_ID.Value, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ELR_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ELR_EMP_ID", h_Emp_ID.Value, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ELR_ELT_ID", RadLeaveType.SelectedValue, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ELR_DTFROM", Format(UsrFromDate.SelectedDate, "dd/MMM/yyyy"), SqlDbType.DateTime)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ELR_DTTO", Format(UsrToDate.SelectedDate, "dd/MMM/yyyy"), SqlDbType.DateTime)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ELR_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ELR_DOCSUBMITTED", chkDocSubmitted.Checked, SqlDbType.Bit)
            sqlParam(8) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "SaveEmployeeLeaveRange", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(8).Value = "" Then
                SaveEmpLeaveSlab = ""
                h_ELR_ID.Value = sqlParam(0).Value
                DisableControls(True)
                BindOtherLeaveSlabs()
            Else
                SaveEmpLeaveSlab = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim retval As String
            Dim new_elh_id As Integer = 0
            retval = SaveEmpLeaveSlab()
            If (retval = "0" Or retval = "") Then
                lblError.Text = "Data Saved Successfully !!!"
                ViewState("RetValue") = txtRemarks.Text & "___" & LCase(chkDocSubmitted.Checked.ToString)
            Else
                lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
            End If
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & ViewState("RetValue") & "';")
            Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnClose.Click
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("window.returnValue = '" & ViewState("RetValue") & "';")
        Response.Write("window.close();")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"
    End Sub

    Protected Sub RadOtherLeaveSlab_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadOtherLeaveSlab.SelectedIndexChanged
        BindLeaveSlabDetails(h_Emp_ID.Value, h_SDate.Value, RadOtherLeaveSlab.SelectedValue)
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        DisableControls(False)
        btnSave.Visible = True
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        DisableControls(False)
        btnSave.Visible = True
        ClearAll()
    End Sub
End Class

