Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Imports AjaxControlToolkit
Imports Telerik.Web.UI
'Version            Date            Author          Change
'2.0                21-Jul-2019      Vikranth       conditions are cheking in RadEmpAttendance_ItemDataBound() method
Partial Class Payroll_empMarkDailyAttendance
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property EmpAttendanceList() As DataTable
        Get
            Return Session("EmpAttendanceList")
        End Get
        Set(ByVal value As DataTable)
            Session("EmpAttendanceList") = value
        End Set
    End Property
    Private Property TempEMPID() As String
        Get
            Return ViewState("TempEMPID")
        End Get
        Set(ByVal value As String)
            ViewState("TempEMPID") = value
        End Set
    End Property
    Private Property AttendanceDateValid() As Boolean
        Get
            Return ViewState("AttendanceDateValid")
        End Get
        Set(ByVal value As Boolean)
            ViewState("AttendanceDateValid") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ClientScript.RegisterStartupScript([GetType](), "getScreenResolution", "javascript: getScreenResolution(); ", True)
            Page.Title = OASISConstants.Gemstitle
            ' RadEmpAttendance.Attributes.Add("bordercolor", "000000")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim userSuper As Boolean
                userSuper = Session("sBITSupport")
                BindReportTo()
                BindCategory()
                BindStatusDD(RadLeaveType, True)
                'If Session("sroleid").ToString in ("298", "305","340", "663", "681", "772", "791", "821") then

                ShowOrHideMarkAllPresent(chkMarkAllAsPresent.Visible)
                MakeShowAllVisible(Session("EmployeeId"))
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "" Then
                    If ViewState("MainMnu_code") = "P170018" Then
                        RadReportTo.Visible = userSuper
                        trSuperUser.Visible = userSuper
                        RadReportTo.Enabled = userSuper
                        RadAttDate.SelectedDate = Format(CDate(DateTime.Now.Date), OASISConstants.DateFormat)
                        ' pnlProcess.Visible = False
                    ElseIf ViewState("MainMnu_code") = "P170022" Then
                        RadReportTo.Visible = False
                        trSuperUser.Visible = False
                        RadReportTo.Enabled = False
                        ' pnlProcess.Visible = True
                        ViewState("DATE") = Encr_decrData.Decrypt(Request.QueryString("DATE").Replace(" ", "+"))
                        RadAttDate.SelectedDate = Format(CDate(ViewState("DATE")), OASISConstants.DateFormat)
                        Dim CAT_ID As String = Request.QueryString("CAT_ID")
                        If Request.QueryString("TYPE") IsNot Nothing Then
                            ViewState("TYPE") = Request.QueryString("TYPE")
                        Else
                            ViewState("TYPE") = 0
                        End If
                        Select Case ViewState("TYPE")
                            Case "0"
                                lblType.Text = "(All)"
                            Case "1"
                                lblType.Text = "(On Leave)"
                            Case "2"
                                lblType.Text = "(On Approved Leave)"
                            Case "3"
                                lblType.Text = "(Absentees)"
                        End Select
                        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                        Dim sql_str As String = " SELECT ISNULL(ECT_DESCR,'') FROM  EMPCATEGORY_M WHERE ECT_ID = " & CAT_ID
                        RadCategory.SelectedItem.Value = CAT_ID
                        Dim strCategory As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_str)
                        lblType.Text &= " / Category - " & strCategory & " / Date - " & Format(CDate(ViewState("DATE")), "dd/MMM/yyyy")
                        SetSessionDataList(Format(CDate(ViewState("DATE")), "dd/MMM/yyyy"), CAT_ID, ViewState("TYPE"))
                        gridbind()

                    End If
                    setLastTransferDate()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Private Sub ShowOrHideMarkAllPresent(ByVal Show As Boolean)
        If Session("sBsuid") = "900501" Or Session("sBsuid") = "900500" Or Session("sBsuid") = "900510" Then
            chkMarkAllAsPresent.Visible = False
        Else
            chkMarkAllAsPresent.Visible = Show
        End If
    End Sub


    Private Sub setLastTransferDate()
        Dim sql As String, LastTransferDate As String
        sql = "SELECT isnull(convert(varchar(50),MAX(EDA_TIME),100),'') FROM EMPDAILYATTENDANCE WHERE EDA_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(EDA_bBIOMETRIC_DATA,0)=1"
        LastTransferDate = Mainclass.getDataValue(sql, "OASISConnectionString")
        If LastTransferDate.ToString <> "" Then
            lblLastAttTransferDecr.Text = "Last Bio-metric attendance transfered on " & LastTransferDate.ToString & "."
        Else
            lblLastAttTransferDecr.Text = ""
        End If
    End Sub

    Private Sub BindReportTo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_str As String = " Select DISTINCT * FROM"
        sql_str &= "  ("
        sql_str &= "  SELECT EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME EMP_NAME  "
        sql_str &= "  from EMPLOYEE_M"
        sql_str &= "  where EMP_ID IN (SELECT DISTINCT EMP_ATT_Approv_EMP_ID from EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "')  "
        sql_str &= "  UNION ALL"
        sql_str &= "  SELECT EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME EMP_NAME  "
        sql_str &= "  from EMPLOYEE_M"
        sql_str &= "  where EMP_ID IN (SELECT DISTINCT EJD_DEL_EMP_ID FROM dbo.EMPJOBDELEGATE_S "
        sql_str &= "  WHERE EJD_EMP_ID IN ( SELECT DISTINCT EMP_ATT_Approv_EMP_ID from EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "')"
        sql_str &= "  AND GETDATE() BETWEEN EJD_DTFROM AND EJD_DTTO AND ISNULL(EJD_bForward,0)=1 AND ISNULL(EJD_bDisable,0)=0  AND EJD_EJM_ID ='AT'"
        sql_str &= "  )"
        sql_str &= "  ) A"
        sql_str &= "  ORDER BY EMP_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
        RadReportTo.DataSource = ds
        RadReportTo.DataTextField = "EMP_NAME"
        RadReportTo.DataValueField = "EMP_ID"
        RadReportTo.DataBind()
        Dim lstDrp As New RadComboBoxItem
        lstDrp.Value = 0
        lstDrp.Text = ""
        RadReportTo.Items.Insert(0, lstDrp)
        lstDrp = RadReportTo.Items.FindItemByValue(Session("EmployeeId"))
        If Not lstDrp Is Nothing Then
            RadReportTo.SelectedValue = lstDrp.Value
        Else
            RadReportTo.SelectedValue = 0
        End If
    End Sub

    Private Sub BindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim userSuper As Boolean
        userSuper = Session("sBITSupport")
        Dim RepEMPID As String
        If Not RadReportTo.SelectedItem Is Nothing And userSuper Then
            RepEMPID = RadReportTo.SelectedValue
        Else
            RepEMPID = Session("EmployeeId")
        End If
        Dim sql_str As String
        If chkAll.Checked Then
            sql_str = " SELECT '-------All-------' ECT_DESCR,0 EMP_ECT_ID UNION ALL "
            sql_str &= " SELECT   DISTINCT ECT_DESCR, EMP_ECT_ID  FROM EMPLOYEE_M INNER JOIN EMPCATEGORY_M "
            sql_str &= " ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' AND EMP_STATUS <> 4 "
        Else
            'sql_str = " SELECT '-------All-------' ECT_DESCR,0 EMP_ECT_ID UNION ALL SELECT DISTINCT * FROM ("
            'sql_str &= " SELECT   DISTINCT ECT_DESCR, EMP_ECT_ID  FROM EMPLOYEE_M INNER JOIN EMPCATEGORY_M "
            'sql_str &= " ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' AND (EMP_ATT_Approv_EMP_ID = " & RepEMPID & " OR " & RepEMPID & " =0)  AND EMP_STATUS <> 4 "
            'sql_str &= " UNION ALL"
            'sql_str &= " SELECT   DISTINCT ECT_DESCR, EMP_ECT_ID  FROM EMPLOYEE_M INNER JOIN EMPCATEGORY_M "
            'sql_str &= " ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' AND EMP_STATUS <> 4 AND EMP_ATT_Approv_EMP_ID IN("
            'sql_str &= " SELECT DISTINCT EJD_EMP_ID FROM dbo.EMPJOBDELEGATE_S WHERE (EJD_DEL_EMP_ID =" & RepEMPID & " OR " & RepEMPID & " =0)"
            'sql_str &= " AND GETDATE() BETWEEN EJD_DTFROM AND EJD_DTTO AND ISNULL(EJD_bForward,0)=1 AND ISNULL(EJD_bDisable,0)=0  AND EJD_EJM_ID ='AT'"
            'sql_str &= " ))A"


            sql_str = " SELECT '-------All-------' ECT_DESCR,0 EMP_ECT_ID UNION ALL SELECT DISTINCT * FROM ("
            sql_str &= " SELECT   DISTINCT ECT_DESCR, EMP_ECT_ID  FROM EMPLOYEE_M INNER JOIN EMPCATEGORY_M "
            sql_str &= " ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  WHERE EMP_BSU_ID = '" & Session("sBsuid") & "' AND (EMP_ATT_Approv_EMP_ID = " & RepEMPID & " OR " & RepEMPID & " =0)  AND EMP_STATUS <> 4 "
            sql_str &= " UNION ALL"
            sql_str &= " SELECT   DISTINCT ECT_DESCR, EMP_ECT_ID  FROM EMPLOYEE_M INNER JOIN EMPCATEGORY_M "
            sql_str &= " ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  WHERE  EMP_ATT_Approv_EMP_ID IN("
            sql_str &= " SELECT DISTINCT EJD_EMP_ID FROM dbo.EMPJOBDELEGATE_S WHERE "
            sql_str &= "  ISNULL(EJD_bForward,0)=1 AND ISNULL(EJD_bDisable,0)=0  AND EJD_EJM_ID ='AT'"
            sql_str &= " ))A"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)

        RadCategory.DataSource = ds
        RadCategory.DataTextField = "ECT_DESCR"
        RadCategory.DataValueField = "EMP_ECT_ID"
        RadCategory.DataBind()
        RadCategory.SelectedIndex = 0
    End Sub
    Private Sub SetSessionDataList(ByVal vDate As String, ByVal vCAT_ID As Int16, ByVal vType As Int16)
        Try
            ViewState("DATE") = vDate
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim userSuper As Boolean
            userSuper = Session("sBITSupport")
            Dim RepEMPID As String
            If Not RadReportTo.SelectedItem Is Nothing And userSuper Then
                RepEMPID = RadReportTo.SelectedValue
            Else
                RepEMPID = Session("EmployeeId")
            End If
            If RepEMPID = "0" Or RepEMPID = "" Then RepEMPID = Session("EmployeeId")

            Dim param(9) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlClient.SqlParameter("@FROMDT", vDate)
            param(2) = New SqlClient.SqlParameter("@TODT", vDate)
            param(3) = New SqlClient.SqlParameter("@CAT_ID", vCAT_ID)
            param(4) = New SqlClient.SqlParameter("@TYPE", vType)
            param(6) = New SqlClient.SqlParameter("@ELT_ID", RadLeaveType.SelectedValue)
            If ViewState("MainMnu_code") = "P170018" Then
                param(5) = New SqlClient.SqlParameter("@REP_EMP_ID", RepEMPID)
            ElseIf ViewState("MainMnu_code") = "P170022" Then
                param(5) = New SqlClient.SqlParameter("@REP_EMP_ID", 0)
            End If
            If rbnAll.Checked Then
                param(7) = New SqlClient.SqlParameter("@ProcessStatus", 0)
            ElseIf rbnProcessed.Checked Then
                param(7) = New SqlClient.SqlParameter("@ProcessStatus", 1)
            ElseIf rbnNotProcessed.Checked Then
                param(7) = New SqlClient.SqlParameter("@ProcessStatus", 2)
            End If
            param(8) = New SqlClient.SqlParameter("@Summary", 0)
            param(9) = New SqlClient.SqlParameter("@ShowAll", chkAll.Checked)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_EMP_ATT_DASHBOARD_DETAILS_NEW", param)
            If ds.Tables(0).Rows.Count Then
                EmpAttendanceList = ds.Tables(0)
                EmpAttendanceList.AcceptChanges()
            Else
                EmpAttendanceList = Nothing
            End If

            'Add a column named MarkedAsAbsent in EmpAttendacneList data table
            If EmpAttendanceList IsNot Nothing Then
                Dim ColIndex As Integer = EmpAttendanceList.Columns.IndexOf("MarkedAsAbsent")
                If ColIndex = -1 Then
                    EmpAttendanceList.Columns.Add("MarkedAsAbsent", GetType(Integer))
                    EmpAttendanceList.AcceptChanges()
                Else
                    EmpAttendanceList.Columns.Remove("MarkedAsAbsent")
                    EmpAttendanceList.Columns.Add("MarkedAsAbsent", GetType(Integer))
                    EmpAttendanceList.AcceptChanges()
                End If

                'Add a column named ATT_STATUS_FROMDB and set its value to ATT_STATUS
                EmpAttendanceList.Columns.Add("ATT_STATUS_FROMDB", GetType(String))
                EmpAttendanceList.AcceptChanges()
                For Each row As DataRow In EmpAttendanceList.Rows
                    row.Item("ATT_STATUS_FROMDB") = row.Item("ATT_STATUS")
                Next
                'EmpAttendanceList.AcceptChanges()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub gridbind()
        Try
            If EmpAttendanceList IsNot Nothing Then
                EmpAttendanceList.DefaultView.RowFilter = ""
                EmpAttendanceList.DefaultView.RowFilter = "EMP_NO like '%" & txtEmpNo.Text & "%' and EMP_NAME like '%" & txtEmployeeName.Text & "%'"
                RadEmpAttendance.DataSource = EmpAttendanceList
                'RadEmpAttendance.DataSource = EmpAttendanceList.Select("EMP_NO like '%" & txtEmpNo.Text & "%' and EMP_NAME like '%" & txtEmployeeName.Text & "%'")
                RadEmpAttendance.DataBind()

            Else
                RadEmpAttendance.DataSource = Nothing
                RadEmpAttendance.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub RadEmpAttendance_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs)
        Try
            If (e.Item.ItemType = GridItemType.Item) OrElse (e.Item.ItemType = GridItemType.AlternatingItem) Then
                Dim ditem As GridDataItem = CType(e.Item, GridDataItem)
                Dim hdnStatus As HiddenField = DirectCast(e.Item.FindControl("hdnStatus"), HiddenField)
                Dim StatusRadComboBox As RadComboBox = DirectCast(e.Item.FindControl("StatusRadComboBox"), RadComboBox)
                If StatusRadComboBox IsNot Nothing Then
                    BindStatusDD(StatusRadComboBox)
                    StatusRadComboBox.ClearSelection()
                    If Not StatusRadComboBox.Items.FindItemByValue(hdnStatus.Value, True) Is Nothing Then
                        StatusRadComboBox.Items.FindItemByValue(hdnStatus.Value, True).Selected = True
                    End If
                End If
                Dim hdnEMPID As HiddenField = DirectCast(e.Item.FindControl("hdnEMPID"), HiddenField)
                If hdnEMPID Is Nothing Then Exit Sub
                Dim txtRemarks As RadTextBox
                Dim chkDocChecked As CheckBox
                txtRemarks = e.Item.FindControl("RemarksRadTextBox")
                chkDocChecked = e.Item.FindControl("chkDocChecked")
                Dim imgEmpDetailView As ImageButton = DirectCast(e.Item.FindControl("imgEmpDetailView"), ImageButton)
                Dim imgEmpBulkUpd As ImageButton = DirectCast(e.Item.FindControl("imgEmpBulkUpd"), ImageButton)
                Dim imgSelectSlab As ImageButton = DirectCast(e.Item.FindControl("imgSelectSlab"), ImageButton) '
                If Not imgSelectSlab Is Nothing And Not txtRemarks Is Nothing Then
                    imgSelectSlab.Attributes.Add("OnClick", "OpenEmpLeaveEntryScreen('" & hdnEMPID.Value & "','" & StatusRadComboBox.ClientID & "','" & txtRemarks.ClientID & "','" & chkDocChecked.ClientID & "','" & ViewState("DATE") & "','');return false;")
                End If
                If Not imgEmpBulkUpd Is Nothing Then
                    imgEmpBulkUpd.Attributes.Add("OnClick", "OpenBU('" & hdnEMPID.Value & "','" & hdnSelATTDate.Value & "','');return false;")
                End If
                If Not imgEmpDetailView Is Nothing Then
                    imgEmpDetailView.Attributes.Add("OnClick", "OpenEmployeeDetailScreen('" & hdnEMPID.Value & "','" & hdnSelATTDate.Value & "','');return false;")
                End If
                'Added by vikranth on 21st July 2019
                'Dim hdnCheckEMPAttendance As HiddenField = DirectCast(e.Item.FindControl("hdnCheckEMPAttendance"), HiddenField)
                Dim hdnEMP_bOn_ELeave As HiddenField = DirectCast(e.Item.FindControl("hdnEMP_bOn_ELeave"), HiddenField)
                Dim lblATT_STATUS As Label = DirectCast(e.Item.FindControl("lblATT_STATUS"), Label)
                Dim ColMarkAbsent As ImageButton = CType(ditem("ColMarkAbsent").Controls(0), ImageButton)
                'If hdnCheckEMPAttendance.Value IsNot Nothing Then
                If hdnEMP_bOn_ELeave.Value IsNot Nothing Then
                    If hdnEMP_bOn_ELeave.Value = "True" Then
                        'lblATT_STATUS.Text = StatusRadComboBox.SelectedItem.Text
                        lblATT_STATUS.Visible = True
                        'StatusRadComboBox.Enabled = False
                        StatusRadComboBox.Visible = False
                        ColMarkAbsent.Visible = False
                        imgEmpDetailView.Visible = False
                        imgEmpBulkUpd.Visible = False
                        txtRemarks.Enabled = False
                        chkDocChecked.Visible = False
                        imgSelectSlab.Visible = False
                    End If
                End If
                'End If
                'End by vikranth

                Dim hdnLeaveAvailable, hdnLeaveStatus, hdnProcessed, hdnDefStatusSet As HiddenField
                hdnLeaveAvailable = e.Item.FindControl("hdnLeaveAvailable")
                hdnLeaveStatus = e.Item.FindControl("hdnLeaveStatus")
                hdnProcessed = e.Item.FindControl("hdnProcessed")
                hdnDefStatusSet = e.Item.FindControl("hdnDefStatusSet")
                If hdnProcessed IsNot Nothing Then
                    If hdnProcessed.Value = "False" And hdnLeaveAvailable.Value <> "False" Then
                        If hdnDefStatusSet.Value = "True" Then
                            e.Item.ForeColor = Drawing.ColorTranslator.FromHtml("#BBCC00")
                            txtRemarks.ForeColor = Drawing.ColorTranslator.FromHtml("#BBCC00")
                            StatusRadComboBox.ForeColor = Drawing.ColorTranslator.FromHtml("#BBCC00")
                            e.Item.ToolTip &= Trim(IIf(Trim(e.Item.ToolTip) <> "", Environment.NewLine, "") & "Already Applied Leave (Not Processed)")
                        Else
                            e.Item.ForeColor = Drawing.ColorTranslator.FromHtml("#BB7100")
                            txtRemarks.ForeColor = Drawing.ColorTranslator.FromHtml("#BB7100")
                            StatusRadComboBox.ForeColor = Drawing.ColorTranslator.FromHtml("#BB7100")
                            e.Item.ToolTip &= Trim(IIf(Trim(e.Item.ToolTip) <> "", Environment.NewLine, "") & "Attendance not saved for the day.")
                        End If
                    Else
                        If hdnLeaveAvailable IsNot Nothing Then
                            If hdnLeaveAvailable.Value = "False" Then
                                e.Item.ForeColor = Drawing.Color.Red
                                e.Item.ToolTip = hdnLeaveStatus.Value
                                txtRemarks.ForeColor = Drawing.Color.Red
                                StatusRadComboBox.ForeColor = Drawing.Color.Red
                                e.Item.ToolTip &= Trim(IIf(Trim(e.Item.ToolTip) <> "", Environment.NewLine, "") & "No Leave Balance Available.")
                            Else
                                If hdnStatus.Value <> "LOP" And hdnStatus.Value <> "PRESENT" And hdnStatus.Value <> "BT" Then
                                    e.Item.ForeColor = Drawing.Color.Green
                                    txtRemarks.ForeColor = Drawing.Color.Green
                                    StatusRadComboBox.ForeColor = Drawing.Color.Green
                                Else
                                    e.Item.ForeColor = Nothing
                                    txtRemarks.ForeColor = Nothing
                                    StatusRadComboBox.ForeColor = Nothing
                                    e.Item.ToolTip &= Trim(IIf(Trim(e.Item.ToolTip) <> "", Environment.NewLine, ""))
                                End If
                            End If
                        End If
                        If ViewState("MainMnu_code") = "P170022" Then
                            Dim chkDocCheckRequired As CheckBox
                            chkDocCheckRequired = e.Item.FindControl("chkDocCheckRequired")
                            If chkDocCheckRequired.Checked And Not chkDocChecked.Checked Then
                                e.Item.ForeColor = Drawing.Color.Red
                                e.Item.ToolTip &= Trim(IIf(Trim(e.Item.ToolTip) <> "", Environment.NewLine, "") & "Document Not Submitted for leave.")
                            End If
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindStatusDD(ByVal StatusRadComboBox As RadComboBox, Optional ByVal ShowAll As Boolean = False)
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = ""
        str_Sql = "SELECT DISTINCT ISNULL(EAS_ELT_ID, EAS_DESCR) EAS_ELT_ID, EAS_DESCR FROM EMPATTENDANCE_STATUS INNER JOIN BSU_LEAVESLAB_S ON EAS_ELT_ID=BLS_ELT_ID "
        str_Sql &= " AND BLS_BSU_ID ='" & Session("sBsuid") & "'"
        str_Sql &= " UNION ALL SELECT 'HO','Holiday'"
        str_Sql &= " UNION ALL SELECT 'OD','On Duty'"
        str_Sql &= " UNION ALL SELECT 'PRESENT','PRESENT'"
        str_Sql &= " UNION ALL SELECT 'W','Weekend'"
        If ShowAll Or ViewState("StatusList") Is Nothing Then
            If ShowAll Then str_Sql = " SELECT 'All' EAS_ELT_ID,'-------------All--------------' EAS_DESCR  UNION ALL " & str_Sql
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            ViewState("StatusList") = ds
        End If
        StatusRadComboBox.DataTextField = "EAS_DESCR"
        StatusRadComboBox.DataValueField = "EAS_ELT_ID"
        StatusRadComboBox.DataSource = ViewState("StatusList")
        StatusRadComboBox.DataBind()
        If ShowAll Then
            ''commented and added new entry by nahyan on 24feb2015(Error reported new oasis)
            'StatusRadComboBox.SelectedItem.Value = "All"
            StatusRadComboBox.SelectedIndex = StatusRadComboBox.FindItemIndexByText("All")
            StatusRadComboBox.SelectedIndex = 0
        End If
    End Sub

    Private Sub ShowErrorMessage(ByVal strMessage As String)
        lblError.Text = strMessage
        lblErrorFooter.Text = ""
        If RadEmpAttendance.Items.Count > 10 Then
            lblErrorFooter.Text = strMessage
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Private Sub SaveData(Optional ByVal GridItem As GridDataItem = Nothing, Optional ByVal IndividualEmployee As Boolean = False)
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim transaction As SqlTransaction
        transaction = conn.BeginTransaction("SampleTransaction")
        Try
            Dim Failure As Boolean = False
            If ViewState("DATE") > Now.Date Then
                ShowErrorMessage("Cannot process the attendance for future date !!!")
                Exit Sub
            End If
            Dim ErrNo As Integer
            SaveCurrentPageData(GridItem)
            Failure = SaveEMPAttendanceCorrection(conn, transaction, ErrNo, IndividualEmployee)
            If Not Failure Then
                transaction.Commit()
                ' transaction.Rollback()
                If Not IndividualEmployee Then
                    SetSessionDataList(RadAttDate.SelectedDate, RadCategory.SelectedValue, 0)
                    gridbind()
                End If

                ShowErrorMessage("Data Saved Successfully")
            Else
                transaction.Rollback()
                SaveCurrentPageData()
                gridbind()
                ShowErrorMessage(UtilityObj.getErrorMessage(1025))
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                transaction.Rollback()
                conn.Close()
            End If
            ShowErrorMessage(ex.Message)
        Finally
            Me.chkMarkAllAsPresent.Checked = False
        End Try
    End Sub

    Private Function SaveEMPAttendanceCorrection(ByVal conn As SqlConnection, ByVal transaction As SqlTransaction, ByRef ErrNo As Integer, Optional ByVal IndividualEmployee As Boolean = False) As Boolean
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim ELT_EVENT As String
        Dim Failure As Boolean
        Dim mRow, RowToDelete As DataRow
        For Each mRow In EmpAttendanceList.Rows
            If IndividualEmployee Then
                If Not IsDBNull(mRow.Item("MarkedAsAbsent")) Then
                    If mRow.Item("MarkedAsAbsent") = 0 Then
                        Continue For
                    End If
                Else
                    Continue For
                End If
            End If
            ELT_EVENT = ""
            Dim sqlpbFROM_SUPEERVIOSOR As New SqlParameter("@bFROM_SUPEERVIOSOR", SqlDbType.Bit)

            cmd = New SqlCommand("SaveEMP_DAILYATT_CORRECTION_NEW", conn, transaction)
            cmd.CommandType = CommandType.StoredProcedure

            If ViewState("MainMnu_code") = "P170018" Then
                sqlpbFROM_SUPEERVIOSOR.Value = True
                ELT_EVENT = "Supervisor Approval"
            ElseIf ViewState("MainMnu_code") = "P170022" Then
                sqlpbFROM_SUPEERVIOSOR.Value = False
                ELT_EVENT = "Supervisor Correction"
            End If
            cmd.Parameters.Add(sqlpbFROM_SUPEERVIOSOR)
            TempEMPID = mRow("EMP_ID")
            Dim sqlpATT_EMP_ID As New SqlParameter("@ATT_EMP_ID", SqlDbType.BigInt)
            sqlpATT_EMP_ID.Value = mRow("EMP_ID")
            cmd.Parameters.Add(sqlpATT_EMP_ID)

            Dim sqlpATT_EAS_ID As New SqlParameter("@ATT_EAS_ID", SqlDbType.BigInt)
            sqlpATT_EAS_ID.Value = 0
            cmd.Parameters.Add(sqlpATT_EAS_ID)

            Dim sqlpATT_ELT_ID As New SqlParameter("@ATT_ELT_ID", SqlDbType.VarChar)
            'If mRow("CheckEMPAttendance") IsNot Nothing Then
            'If Not String.IsNullOrEmpty(mRow("CheckEMPAttendance")) Then
            'If mRow("EMP_bOn_ELeave") IsNot Nothing Then
            'If Not String.IsNullOrEmpty(mRow("EMP_bOn_ELeave").Value) Then
            If Not IsDBNull(mRow("EMP_bOn_ELeave")) Then
                If mRow("EMP_bOn_ELeave") = "True" Then
                    sqlpATT_ELT_ID.Value = "E-Leave"
                Else sqlpATT_ELT_ID.Value = mRow("ATT_STATUS")
                    End If
                Else sqlpATT_ELT_ID.Value = mRow("ATT_STATUS")
                End If
            'Else sqlpATT_ELT_ID.Value = mRow("ATT_STATUS")
            'End If
            'sqlpATT_ELT_ID.Value = mRow("ATT_STATUS")
            cmd.Parameters.Add(sqlpATT_ELT_ID)

            Dim sqlpATT_EDIT_USR_ID As New SqlParameter("@ATT_EDIT_USR_ID", SqlDbType.VarChar)
            sqlpATT_EDIT_USR_ID.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpATT_EDIT_USR_ID)

            Dim sqlpATT_BSU_ID As New SqlParameter("@ATT_BSU_ID", SqlDbType.VarChar)
            sqlpATT_BSU_ID.Value = Session("sBSUID")
            cmd.Parameters.Add(sqlpATT_BSU_ID)

            Dim sqlpEDP_BDocChecked As New SqlParameter("@EDP_BDocChecked", SqlDbType.Bit)
            sqlpEDP_BDocChecked.Value = mRow("bDocChecked")
            cmd.Parameters.Add(sqlpEDP_BDocChecked)

            Dim sqlpATT_DATE As New SqlParameter("@ATT_DATE", SqlDbType.DateTime)
            sqlpATT_DATE.Value = ViewState("DATE")
            cmd.Parameters.Add(sqlpATT_DATE)

            Dim sqlpATT_REMARKS As New SqlParameter("@ATT_REMARKS", SqlDbType.VarChar)
            sqlpATT_REMARKS.Value = mRow("REMARKS")
            cmd.Parameters.Add(sqlpATT_REMARKS)

            Dim sqlpEDP_EVENT As New SqlParameter("@EDP_EVENT", SqlDbType.VarChar)
            sqlpEDP_EVENT.Value = ELT_EVENT
            cmd.Parameters.Add(sqlpEDP_EVENT)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            Dim retStatusRemarks As New SqlParameter("@StatusRemarks", SqlDbType.VarChar, 200)
            retStatusRemarks.Direction = ParameterDirection.Output
            cmd.Parameters.Add(retStatusRemarks)

            Dim retLeaveAvailable As New SqlParameter("@LeaveAvailable", SqlDbType.Bit)
            retLeaveAvailable.Direction = ParameterDirection.Output
            cmd.Parameters.Add(retLeaveAvailable)
            cmd.ExecuteNonQuery()

            iReturnvalue = retValParam.Value

            RowToDelete = mRow

            If Not retLeaveAvailable.Value Then
                mRow("LeaveAvailable") = False
                Failure = True
            Else
                mRow("LeaveAvailable") = True
            End If

            If iReturnvalue <> 0 Then
                mRow("LeaveStatus") = UtilityObj.getErrorMessage(iReturnvalue)
                Failure = True
            End If

            If IndividualEmployee Then Exit For
        Next
        If RadEmpAttendance.Items.Count > 0 Then
            If iReturnvalue <> 0 Then
                ShowErrorMessage("Error in updating Audit trial")
            End If
        End If

        If Not Failure And IndividualEmployee Then
            If rbnNotProcessed.Checked Then
                EmpAttendanceList.Rows.Remove(RowToDelete)
                EmpAttendanceList.AcceptChanges()
            End If
        End If

        Return Failure
    End Function

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Me.ViewData()
    End Sub

    Private Sub ViewData()
        If RadAttDate.SelectedDate > Now.Date Then
            ShowErrorMessage("Cannot process the attendance for future date !!!")
            RadAttDate.SelectedDate = Now.Date.ToString("dd/MMM/yyyy")
            hdnSelATTDate.Value = RadAttDate.SelectedDate
            gridbind()
        Else
            txtEmpNo.Text = ""
            txtEmployeeName.Text = ""
            SetSessionDataList(RadAttDate.SelectedDate, RadCategory.SelectedValue, 0)
            RadEmpAttendance.CurrentPageIndex = 0
            hdnSelATTDate.Value = RadAttDate.SelectedDate
            gridbind()
            ShowErrorMessage("")
            trSearch.Visible = True
            btnSave.Visible = True
            chkMarkAllAsPresent.Visible = True
        End If
    End Sub

    Public Shared Function GetDesignationIDOfEmployee(ByVal EMPID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "select EMP_DES_ID from employee_m where EMP_ID ='" & EMPID & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetDesignationIDOfEmployee = RetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub MakeShowAllVisible(ByVal EMPID As String)
        Try
            Dim DesID As String
            DesID = GetDesignationIDOfEmployee(EMPID)
            chkAll.Checked = False
            Select Case DesID
                Case "298", "305", "340", "663", "681", "772", "791", "821"
                    Me.chkAll.Visible = True
                Case Else
                    Me.chkAll.Visible = False
            End Select
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub RadReportTo_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadReportTo.SelectedIndexChanged
        Try
            If RadReportTo.SelectedItem Is Nothing Then Exit Sub
            MakeShowAllVisible(RadReportTo.SelectedIndex)
            BindCategory()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SaveCurrentPageData(Optional ByVal GridItem As GridDataItem = Nothing)
        Dim ddlStatus As RadComboBox
        Dim txtRemarks As RadTextBox
        Dim hdnEMPID As HiddenField
        Dim chkDocChecked As CheckBox
        If GridItem Is Nothing Then
            If RadEmpAttendance.Items.Count > 0 Then
                For Each drRow As GridDataItem In RadEmpAttendance.Items
                    ddlStatus = drRow.FindControl("StatusRadComboBox")
                    txtRemarks = drRow.FindControl("RemarksRadTextBox")
                    hdnEMPID = drRow.FindControl("hdnEMPID")
                    chkDocChecked = drRow.FindControl("chkDocChecked")
                    If hdnEMPID Is Nothing OrElse ddlStatus Is Nothing OrElse txtRemarks Is Nothing Then
                        Continue For
                    End If
                    If EmpAttendanceList IsNot Nothing Then
                        Dim mRow As DataRow
                        If EmpAttendanceList.Select("EMP_ID=" & hdnEMPID.Value).Length > 0 Then
                            mRow = EmpAttendanceList.Select("EMP_ID=" & hdnEMPID.Value)(0)
                            mRow("ATT_STATUS") = ddlStatus.SelectedValue
                            mRow("REMARKS") = txtRemarks.Text
                            mRow("bDocChecked") = chkDocChecked.Checked
                        End If
                    End If
                Next
            End If
        Else
            ddlStatus = GridItem.FindControl("StatusRadComboBox")
            txtRemarks = GridItem.FindControl("RemarksRadTextBox")
            hdnEMPID = GridItem.FindControl("hdnEMPID")
            chkDocChecked = GridItem.FindControl("chkDocChecked")
            If hdnEMPID Is Nothing OrElse ddlStatus Is Nothing OrElse txtRemarks Is Nothing Then
                Exit Sub
            End If
            If EmpAttendanceList IsNot Nothing Then
                Dim mRow As DataRow
                If EmpAttendanceList.Select("EMP_ID=" & hdnEMPID.Value).Length > 0 Then
                    mRow = EmpAttendanceList.Select("EMP_ID=" & hdnEMPID.Value)(0)
                    mRow("ATT_STATUS") = ddlStatus.SelectedValue
                    mRow("REMARKS") = txtRemarks.Text
                    mRow("bDocChecked") = chkDocChecked.Checked
                End If
            End If
        End If
    End Sub

    Protected Sub RadEmpAttendance_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadEmpAttendance.ItemCommand
        Dim hdnEMPID As HiddenField
        Dim row As DataRow
        If e.CommandName = "Delete" Then Exit Sub
        If e.CommandName = "Save" Then
            hdnEMPID = e.Item.FindControl("hdnEMPID")
            If hdnEMPID Is Nothing Then Exit Sub
            If EmpAttendanceList.Select("EMP_ID=" & hdnEMPID.Value).Length > 0 Then
                row = EmpAttendanceList.Select("EMP_ID=" & hdnEMPID.Value)(0)
                'mRow("ATT_STATUS") = ddlStatus.SelectedValue
                row.Item("MarkedAsAbsent") = 1
                SaveData(e.Item, True)
                If rbnNotProcessed.Checked Then
                    e.Item.Visible = False
                End If
            End If
        End If
    End Sub


    Protected Sub RadEmpAttendance_PageIndexChanged(ByVal source As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles RadEmpAttendance.PageIndexChanged
        Try
            SaveCurrentPageData()
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request Not Processed"
        End Try
    End Sub

    Protected Sub RadEmpAttendance_PageSizeChanged(ByVal source As Object, ByVal e As Telerik.Web.UI.GridPageSizeChangedEventArgs) Handles RadEmpAttendance.PageSizeChanged
        Try
            SaveCurrentPageData()
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request Not Processed"
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        SaveCurrentPageData()
        gridbind()
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        BindCategory()
    End Sub

    Protected Sub chkMarkAllAsPresent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMarkAllAsPresent.CheckedChanged
        If Not EmpAttendanceList Is Nothing Then
            For Each row As DataRow In EmpAttendanceList.Rows
                If Me.chkMarkAllAsPresent.Checked Then
                    row.Item("ATT_STATUS") = "PRESENT"
                Else
                    'set the value of ATT_STATUS column to the original value we got from the db
                    row.Item("ATT_STATUS") = row.Item("ATT_STATUS_FROMDB")
                End If
            Next
        End If
        gridbind()
    End Sub
End Class
