<%@ Page Language="VB" AutoEventWireup="false" CodeFile="empLeaveSlabEntry.aspx.vb"
    Inherits="Payroll_empLeaveSlabEntry" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <%--<link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/EOSStyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../cssfiles/tabber.js"></script>

    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/RadStyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../cssfiles/tabber.js"></script>--%>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">   
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">    
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>

    <style type="text/css">
        /*.RadInput_Default
        {
            vertical-align: middle;
            font: 12px "segoe ui" ,arial,sans-serif;
        }
        .RadInput_Default
        {
            vertical-align: middle;
            font: 12px "segoe ui" ,arial,sans-serif;
        }
        .RadInput_Default
        {
            vertical-align: middle;
            font: 12px "segoe ui" ,arial,sans-serif;
        }
        .RadInput_Default
        {
            vertical-align: middle;
            font: 12px "segoe ui" ,arial,sans-serif;
        }
        .RadInput_Default
        {
            vertical-align: middle;
            font: 12px "segoe ui" ,arial,sans-serif;
        }*/
         .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: #ffffff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 15px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .RadComboBox_Default {
            width: 100% !important;
        }
        .RadPicker .rcCalPopup, .RadPicker .rcTimePopup {
            width: 30px !important;
            height: 30px !important;
        }
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image:url(../Images/calendar.gif)!important;
            width: 30px;
            height: 30px;
        }
        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active{
            background-image:url(../Images/calendar.gif)!important;
            width: 30px;
            height: 30px;
            background-position: 0;
        }
         .RadComboBox_Default .rcbFocused .rcbReadOnly .rcbInputCellLeft {
            background-color: #ffffff !important;
        }
         .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
             background-position: 0 0 !important;
         }

         .RadPanelQual {
             border: 1px solid rgba(0,0,0,0.16);
         }
         .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft, html body .riSingle .riTextBox[type="text"] {
             width:210px !important;
         }

         table.RadCalendar_Default {
             background-color:#ffffff !important;
         }
    </style>
</head>
<body  topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" class="p-1">
    <form id="form1" runat="server">
    <!--1st drop down menu -->
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600"
        EnablePageMethods="true">
    </ajaxToolkit:ToolkitScriptManager>
    <table align="center" width="100%">
        <tr>
            
        </tr>
        <tr>
            <td align="center">
                <table align="center" width="100%">
                    <tr valign="top">
                        <td align="left" colspan="2" class="title-bg">
                            Employee Leave Entry 
                            <span class="float-right"><asp:ImageButton ID="btnCloseedit" runat="server" Visible="false" ImageUrl="~/Images/close.png" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <span class="field-label">Employee Name</span>
                        </td>
                        
                        <td width="70%" align="left">
                            <asp:Label ID="lblEmployeeName" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Date Ranges</span>
                        </td>
                       
                        <td width="30%" align="left">
                            <telerik:RadComboBox ID="RadOtherLeaveSlab" runat="server" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                           <span class="field-label"> Leave Type</span>
                        </td>
                       
                        <td width="30%" align="left">
                            <telerik:RadComboBox ID="RadLeaveType" runat="server" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">From Date</span>
                        </td>
                        
                        <td width="30%" align="left">
                            <telerik:RadDatePicker ID="UsrFromDate" runat="server"  DateInput-EmptyMessage="MinDate">
                                <DateInput EmptyMessage="MinDate">
                                </DateInput>
                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">To Date</span>
                        </td>
                        
                        <td width="30%" align="left">
                            <telerik:RadDatePicker ID="UsrToDate" runat="server" DateInput-EmptyMessage="MinDate">
                                <DateInput EmptyMessage="MinDate">
                                </DateInput>
                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Remarks</span>
                        </td>
                       
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="68px" TextMode="MultiLine" Width="291px"
                                EnableTheming="False" CssClass="RadLabelText"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trDocSubmitted" runat="server">
                        <td align="left">
                            <span class="field-label">Document Submitted</span>
                        </td>
                       
                        <td width="30%" align="left">
                            <asp:CheckBox ID="chkDocSubmitted" runat="server" CssClass="field-label" Text="Yes" Width="20%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="3">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_ELR_ID" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="h_SDate" runat="server" />
    <asp:HiddenField ID="h_Emp_ID" runat="server" />
    <asp:HiddenField ID="h_ELT_ID" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="h_SelectedId" runat="server" />
    </form>
</body>
</html>
