<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empTerminationView.aspx.vb" Inherits="Payroll_empTerminationView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

        function OpenMasterPopUp(param) {

            var NameandCode;
            var result;
            var strOpen = "empTerminationMasterUpdate.aspx?viewid=" + param

            result = radopen(strOpen, "pop_up1");
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //return false;
        }

        function OpenDAXScreen(EmpId) {

            //alert(param);

            var NameandCode;
            var result;
            var strOpen = "EmpFinalSettlementDAX.aspx?empid=" + EmpId

            result = radopen(strOpen, "pop_up2");
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" EnableViewState="False" ></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td class="matters" valign="top" width="50%" align="left" colspan="">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" style="border-collapse: collapse" align="center"
                    cellpadding="5" cellspacing="0" width="100%">

                    <tr>
                        <td align="right" valign="top">
                            <asp:RadioButton ID="rbOpen" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Open" Checked="True" OnCheckedChanged="RdApprove_CheckedChanged" />
                            <asp:RadioButton ID="rbApprove" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Approved" OnCheckedChanged="RdApprove_CheckedChanged" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="All" OnCheckedChanged="RdApprove_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <br />
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO" CssClass="table table-bordered table-row"
                                EmptyDataText="No Loan Applications found" Width="100%" AllowPaging="True" PageSize="30" EnableModelValidation="True">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Emp No" SortExpression="EMPNO">
                                        <HeaderTemplate>
                                            Emp No<br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp Name" SortExpression="EMP_NAME">
                                        <HeaderTemplate>
                                            Emp Name<br />
                                            <asp:TextBox ID="txtEmpname" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Dt.">
                                        <HeaderTemplate>
                                            Salary<br />
                                            <asp:TextBox ID="txtLType" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EREG_SALARY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EREG_LVSALARY" HeaderText="L.Salary">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="EREG_AIRTICKET" HeaderText="Air Ticket" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREG_TKTCOUNT" HeaderText="Tkt Count">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREG_ARREARS" HeaderText="Arrears">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREG_GRATUITY" HeaderText="Gratuity">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREG_DEDUCTIONS" HeaderText="Deductions">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREG_OTHEARNINGS" HeaderText="Earnings">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREG_VISACANCEL_DT"
                                        HeaderText="Visa Cancellation Date" Visible="False"
                                        DataFormatString="{0:dd/MMM/yyyy}" />
                                    <asp:BoundField DataField="EMP_VISACANCEL_NAME" HeaderText="Visa Cancelled By"
                                        Visible="False" />
                                    <asp:BoundField DataField="EREG_LC_DT" HeaderText="LC Cancellation Date"
                                        Visible="False" DataFormatString="{0:dd/MMM/yyyy}" />
                                    <asp:BoundField DataField="EMP_LCCANCEL_NAME" HeaderText="LC Cancelled By"
                                        Visible="False" />
                                    <asp:BoundField DataField="EREG_EXIT_DT" HeaderText="Employee Exit Date"
                                        Visible="False" DataFormatString="{0:dd/MMM/yyyy}" />
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("EREG_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Remarks<br />
                                            <asp:TextBox ID="txtRemarks" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbPrint" runat="server" OnClick="lbPrint_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ESD_EMP_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblESD_EMP_ID" runat="server" Text='<%# Bind("EREG_EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EREG_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEREG_ID" runat="server" Text='<%# Bind("EREG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EREG_ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Master<br>Update">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgUpdate" runat="server"
                                                ImageUrl="~/Images/ButtonImages/ProfilePic.png" Height="21px" Width="23px" OnClientClick=" <%# &quot;OpenMasterPopUp('&quot; & Container.DataItem(&quot;EREG_ID&quot;) & &quot;');return false;&quot; %>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Remove<br>Processing">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRemove" Text="Remove" runat="server"
                                                OnClick="lnkRemove_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sync To&lt;br&gt;DAX" Visible="False">
                                        <ItemTemplate>
                                            <%--<asp:HyperLink ID="lnkSendToDAX" runat="server" OnClientClick=" <%# &quot;OpenDAXScreen('&quot; & Container.DataItem(&quot;EREG_EMP_ID&quot;) & &quot;');return false;&quot; %>" >Send</asp:HyperLink>--%>
                                            <%--<asp:HyperLink ID="lnkSendToDAX" runat="server" OnClick="lnkSync_Click()" >Send</asp:HyperLink>--%>
                                            <%--<asp:LinkButton ID="lbSendToDAX" runat="server" OnClick="lnkSync_Click">Sync</asp:LinkButton>--%>
                                            <%--<asp:ImageButton ID="imgUpdate" runat="server" 
                                        ImageUrl="~/Images/ButtonImages/ProfilePic.png" Height="21px" Width="23px" onclientclick=" <%# &quot;OpenDAXScreen('&quot; & Container.DataItem(&quot;EREG_EMP_ID&quot;) & &quot;');return false;&quot; %>" 
                                            />--%>
                                            <asp:LinkButton ID="lnkSync" runat="server"
                                                Text="Sync" OnClientClick=" <%# &quot;OpenDAXScreen('&quot; & Container.DataItem(&quot;EREG_EMP_ID&quot;) & &quot;');return false;&quot; %>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

