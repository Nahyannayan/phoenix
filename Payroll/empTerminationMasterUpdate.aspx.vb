﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.Configuration
Imports System.IO
Partial Class Payroll_empTerminationMasterUpdate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version        Author          Date            Change
    '1.1            swapna          6-Nov-2012     Employee resignation - master data update pop up


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' lblCurrentLOPs.Attributes.Add("ReadOnly", "ReadOnly")
        If Not IsPostBack Then

            ViewState("EREG_ID") = Request.QueryString("viewid").ToString
            FillResignationDetails(ViewState("EREG_ID"))

        End If


    End Sub

    Sub FillResignationDetails(ByVal RegId As String)
        Try
            Dim Param(1) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@EREG_ID", RegId)

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetEmpResignationdetails", Param)
            If (ds.Tables(0).Rows.Count > 0) Then
                lblemployeeno.Text = ds.Tables(0).Rows(0).Item("EMPNO").ToString
                lbl_Empname.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
                ddlEmpContractType.SelectedValue = ds.Tables(0).Rows(0).Item("EMP_CONTRACTTYPE").ToString  'V1.2
                If ds.Tables(0).Rows(0).Item("EMP_CONTRACTENDDT") <> "1/jan/1900" Then
                    txtContractEndDt.Text = Format(ds.Tables(0).Rows(0).Item("EMP_CONTRACTENDDT"), OASISConstants.DateFormat)
                Else
                    txtContractEndDt.Text = ""
                End If

                lblLopfirst.Text = "As per system:" & ds.Tables(0).Rows(0).Item("EREG_LOP_First5Yr").ToString
                lblLOPLast.Text = "As per system:" & ds.Tables(0).Rows(0).Item("EREG_LOP_After5Yr").ToString
                txtLOPFirstFiveYr.Text = ds.Tables(0).Rows(0).Item("EREG_LOP_First5Yr").ToString
                txtLOPAfterFiveYr.Text = ds.Tables(0).Rows(0).Item("EREG_LOP_After5Yr").ToString
                lblCurrentLOPs.Text = ds.Tables(0).Rows(0).Item("TotalLOP").ToString

            End If
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsNumeric(txtLOPFirstFiveYr.Text) = False Or IsNumeric(txtLOPAfterFiveYr.Text) = False Then
            lblError.Text = "LOP days must be in numbers."
            Exit Sub
        End If
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
                Dim Param(5) As SqlClient.SqlParameter
                Param(0) = Mainclass.CreateSqlParameter("@ereg_id", ViewState("EREG_ID"), SqlDbType.Int)
                Param(1) = Mainclass.CreateSqlParameter("@ContracttType", ddlEmpContractType.SelectedValue, SqlDbType.Int) 'V1.2

                If txtContractEndDt.Text <> "" Then
                    Param(2) = Mainclass.CreateSqlParameter("@ContractEndDate", txtContractEndDt.Text, SqlDbType.VarChar)
                Else
                    Param(2) = Mainclass.CreateSqlParameter("@ContractEndDate", DBNull.Value, SqlDbType.VarChar)
                End If
                Param(3) = Mainclass.CreateSqlParameter("@LOPFirstFive", txtLOPFirstFiveYr.Text, SqlDbType.VarChar)
                Param(4) = Mainclass.CreateSqlParameter("@LOPAfterFive", txtLOPAfterFiveYr.Text, SqlDbType.VarChar)
                Dim retVal As Integer
                retVal = Mainclass.ExecuteParamQRY(conn, transaction, "UpdateMasterforTermination", Param)

                If retVal = 0 Then
                    transaction.Commit()
                    Response.Write("<script language='javascript'>  window.close(); </script>")
                Else
                    transaction.Rollback()
                End If

            Catch ex As Exception

            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End Using

    End Sub
End Class
