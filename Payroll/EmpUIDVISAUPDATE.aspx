﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="EmpUIDVISAUPDATE.aspx.vb" Inherits="Payroll_EmpUIDVISAUPDATE" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style type="text/css">
         .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
             left: 40%;
            top: 40%;
            position: fixed;
            width: 25%;
        }

    </style>
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Personal Info
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <br />
               <%-- <div id="lblError" runat="server">
                </div>--%>

                <div>
    <pre id="lblError" class="invisible" runat="server"></pre>
</div>
                <br />

                <table width="100%" cellpadding="5" id="tblCategory">


                    <tr id="trBSU" runat="server">
                        <td align="left" width="20%"><span class="field-label">Employee Name</span>
                        </td>

                        <td  align="left" width="30%">

                            <asp:Label ID="lblEmployeeName" runat="server" CssClass="field-value "></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Employee Number</span>
                        </td>

                        <td  align="left" width="30%">
                            <asp:Label ID="lblEmpNo" runat="server" CssClass="field-value "></asp:Label>
                        </td>
                    </tr>
                    <tr id="trEID1" runat="server">
                        <td align="left" width="20%"><span class="field-label">National ID No<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%">
                            <asp:TextBox ID="txtEIDNo" runat="server" TabIndex="18" Visible="false"></asp:TextBox>


                            <telerik:RadTextBox ID="txtEIDNo1" runat="server" EmptyMessage="000" MaxLength="3" Width="15%"></telerik:RadTextBox>
                     <telerik:RadTextBox ID="txtEIDNo2" runat="server" EmptyMessage="0000" MaxLength="4" Width="20%"></telerik:RadTextBox>
                     <telerik:RadTextBox ID="txtEIDNo3" runat="server" EmptyMessage="0000000" Width="30%" MaxLength="7"></telerik:RadTextBox>
                     <telerik:RadTextBox ID="txtEIDNo4" runat="server" EmptyMessage="0" MaxLength="1" Width="8%"></telerik:RadTextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">National ID Expiry Date<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%">
                            <asp:TextBox ID="txtEIDExpryDT" runat="server" TabIndex="22" Enabled="false"></asp:TextBox>
                             <asp:ImageButton ID="ImgCalEIDEXP" runat="server" ImageUrl="~/Images/calendar.gif"
                      />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="ImgCalEIDEXP" TargetControlID="txtEIDExpryDT" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtEIDExpryDT" TargetControlID="txtEIDExpryDT" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                ControlToValidate="txtEIDExpryDT" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter National ID expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator7" runat="server" ControlToValidate="txtEIDExpryDT"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="National ID expiry Date entered is not a valid date"
                                ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                        </td>
                    </tr>

                    <tr id="tr1" runat="server">
                        <td align="left" width="20%"><span class="field-label">Passport No</span><font class="text-danger">*</font>
                        </td>

                        <td  align="left" width="30%">
                            <asp:TextBox ID="txtPassportNo" runat="server" TabIndex="18"></asp:TextBox>

                        </td>
                        <td align="left" width="20%"><span class="field-label">Passport Issued Place</span><font class="text-danger">*</font>
                        </td>

                        <td  align="left" width="30%">
                            <asp:TextBox ID="txtPPIssuedPlace" runat="server" TabIndex="18"></asp:TextBox>

                        </td>
                    </tr>
                    <tr id="tr2" runat="server">

                        <td align="left" width="20%"><span class="field-label">Passport Issued Date</span><font class="text-danger">*</font>
                        </td>

                        <td  align="left" width="30%">
                            <asp:TextBox ID="txtPPIssueDate" runat="server" TabIndex="19" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="imgPPIssue" runat="server" ImageUrl="~/Images/calendar.gif"
                      />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgPPIssue" TargetControlID="txtPPIssueDate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtPPIssueDate" TargetControlID="txtPPIssueDate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtPPIssueDate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Passport issued Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtPPIssueDate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Passport issued Date entered is not a valid date"
                                ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Passport Expiry Date<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%">
                            <asp:TextBox ID="txtPPEXpiry" runat="server" TabIndex="19" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="imgPPEXpiry" runat="server" ImageUrl="~/Images/calendar.gif"
                      />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender7" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgPPEXpiry" TargetControlID="txtPPEXpiry" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtPPEXpiry" TargetControlID="txtPPEXpiry" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                ControlToValidate="txtPPEXpiry" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Passport expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtPPEXpiry"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Passport expiry Date entered is not a valid date"
                                ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>

                        <td align="left" width="20%"><span class="field-label">UID No<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:TextBox ID="txtDepUidno" runat="server" TabIndex="23"></asp:TextBox>
                        </td>


                        <td align="left" width="20%"><span class="field-label">Visa Issued Date<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:TextBox ID="txtDepVissuedate" runat="server" Enabled="false" TabIndex="24"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDepVissuedate" TargetControlID="txtDepVissuedate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                ControlToValidate="txtDepVissuedate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Visa Issued Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator5" runat="server" ControlToValidate="txtDepVissuedate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Issued Date entered is not a valid date"
                                ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Visa Expiry Date<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:TextBox ID="txtDepVexpdate" runat="server" Enabled="false"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtDepVexpdate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                ControlToValidate="txtDepVexpdate" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Visa expiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtDepVexpdate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa expiry Date entered is not a valid date"
                                ForeColor="" ValidationGroup="ValDocDet">*</asp:CustomValidator>
                        </td>


                        <td align="left" width="20%"><span class="field-label">Visa Issued Place<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:TextBox ID="txtDepVissueplace" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Visa Sponsor<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:DropDownList ID="ddlSponsor" runat="server">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Company" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Spouse" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Father/Mother" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Mother" Value="4"></asp:ListItem>

                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="rfSponsor" runat="server" ControlToValidate="ddlSponsor" InitialValue="0" ErrorMessage="Plese select" CssClass="error" ValidationGroup="ValDocDet"></asp:RequiredFieldValidator>

                        </td>
                        <td align="left" width="20%"><span class="field-label">Existing Insurance Card No.<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:TextBox ID="txtInsCardNum" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Residence Emirate<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">
                            <asp:DropDownList ID="ddlEmirate" runat="server" AutoPostBack="true"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEmirate" InitialValue="0" ErrorMessage="Plese select" CssClass="error" ValidationGroup="ValDocDet"></asp:RequiredFieldValidator>

                        </td>


                        <td align="left" width="20%"><span class="field-label">Area<font class="text-danger">*</font></span>
                        </td>

                        <td  align="left" width="30%" nowrap="nowrap">

                            <asp:DropDownList ID="ddlArea" runat="server"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlArea" InitialValue="0" ErrorMessage="Plese select" CssClass="error" ValidationGroup="ValDocDet"></asp:RequiredFieldValidator>
                        </td>
                    </tr>


                    <tr>
                        <td align="left" class="title-bg" colspan="4">Personal Info<font class="text-danger">*</font>
                        </td>
                    </tr>
                    <tr>


                        <td align="left" width="20%"><span class="field-label">Alternate Email <font class="text-danger">*</font></span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAlternateEmail" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvemail" runat="server" ControlToValidate="txtAlternateEmail" ErrorMessage="Please Enter Email" ValidationGroup="ValDocDet"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmailID" runat="server"
                                ControlToValidate="txtAlternateEmail"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Enter Valid Email address</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Phone Number<font class="text-danger">*</font></span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnUpdate" runat="server" Text="UPDATE" OnClick="btnUpdate_Click" ValidationGroup="ValDocDet" CssClass="button" />
                            <asp:Button ID="lnkbackbtn" CssClass="button" runat="server" OnClick="lnkbackbtn_Click" Text="Back"></asp:Button>
                            <asp:HiddenField ID="hdnCTYId" runat="server" />
                        </td>

                    </tr>


                </table>
                <asp:Panel ID="divDrill" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:ImageButton ID="btClose" type="button" runat="server" ImageUrl="../Images/close.png" style="float:right;"></asp:ImageButton>
                            <div class="title-bg p-3 mb-3" width="100%"></div>
                            <div>
                                <div align="center" id="lbluerror" runat="server">
                                    
                                </div>
                                <div align="center">
                                <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="button"></asp:Button>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
</asp:Content>
