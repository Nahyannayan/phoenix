Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Partial Class Payroll_empProcessVacation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    'Version        Author              Date            Change
    '1.1            Swapna              29-Mar-2011     Changes made on addition of LeaveSettled textbox.
    '1.2            Swapna              8-Dec-2011      Adding header to leave salary report
    '1.3            Swapna              12-Jun-2012     Adding Batch selection
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try

            '  TextBox1.Attributes.Add("onmousedown", "showdiv()")
            '   TextBox1.Attributes.Add("onclick", "showdiv()")
            '    TextBox1.Attributes.Add("ondragover", "hidediv()")
            'TextBox1.Attributes.Add("onmouseout", "hidediv()") 

            'ClientScript.RegisterStartupScript(Me.GetType(), _
            '"script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If

            'txtToDate.Attributes.Add("ReadOnly", "Readonly")
            'txtFromDate.Attributes.Add("ReadOnly", "Readonly") 

            FillCATNames(h_CATID.Value)

            FillEmpNames(h_EMPID.Value)

            StoreEMPFilter()

            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                    txtFromDate.Text = Cache("fromDate")
                Else
                    'txtToDate.Text = GetDiplayDate()
                    txtFromDate.Text = GetDiplayDate()
                    txtToDate.Text = ""
                    'txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-3))
                End If
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", Today.Date)
                txtToDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtFromDate.Text).AddMonths(1))
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
                If Request.QueryString("viewid") <> "" Then
                    setViewData()

                    'tr_add.Visible = True
                    'btnAddInst.Enabled = False
                    'txtInst.Attributes.Add("readonly", "readonly")
                    'rbBank.Enabled = False
                    'rbCash.Enabled = False
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    tr_ViewDetailPrint.Visible = True
                    tr_ViewProcessing.Visible = False
                    tr_GridListAll.Visible = False
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    ResetViewData()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM(SELECT ELV.ELV_ID, ELV.ELV_BSU_ID, ELV.ELV_EMP_ID, ELV.ELV_ELT_ID, ELV.ELV_ELA_ID, " _
             & " REPLACE(CONVERT(VARCHAR(11), ELV.ELV_DTFROM ,113), ' ', '/') AS ELV_DTFROM, REPLACE(CONVERT(VARCHAR(11), ELV.ELV_DTTO, 113), ' ', '/') AS ELV_DTTO, ELV.ELV_REMARKS, ELV.ELV_SALARY, ELV.ELV_LVSALARY, " _
             & " ELV.ELV_AIRTICKET, convert(decimal,ELV.ELV_TKTCOUNT) as ELV_TKTCOUNT,convert(decimal,ELV.ELV_ACTUALDAYS) as ELV_ACTUALDAYS, ELV.ELV_LOPDAYS, EM.EMPNO, " _
             & " ISNULL(EM.EMP_FNAME,'') +' '+ ISNULL(EM.EMP_MNAME,'')+' '+ ISNULL(EM.EMP_LNAME,'') AS EMP_NAME,ELV.ELV_bOpening" _
             & " FROM EMPLEAVE_D AS ELV INNER JOIN EMPLOYEE_M AS EM ON ELV.ELV_EMP_ID = EM.EMP_ID) AS  DB" _
             & " WHERE ELV_BSU_ID = '" & Session("SBSUID") & "' AND ELV_bOpening = 0 and ELV_ELT_ID='AL'" _
             & " AND ELV_ID ='" & p_Modifyid & "'"
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                gvJournal.DataSource = ds.Tables(0)
                gvJournal.DataBind()
                Dim dtTemp As DateTime = ds.Tables(0).Rows(0)("ELV_DTFROM")
                Dim dtTo As DateTime = ds.Tables(0).Rows(0)("ELV_DTTO")
                ViewState("emp_id") = ds.Tables(0).Rows(0)("ELV_EMP_ID")
                ViewState("emp_id") = ds.Tables(0).Rows(0)("ELV_EMP_ID")
                ViewState("DTfrom") = dtTemp
                ViewState("DTto") = dtTo
            End If


            str_Sql = "SELECT ELD_ID,  ELH_INSTNO AS ID, ELD_DATE AS DATES , " _
            & " ELD_AMOUNT AS AMOUNT, ISNULL(ELD_PAIDAMT,0)  as PAMOUNT,'' AS Status" _
            & " FROM EMPLOAN_D" _
            & " WHERE ELD_ELH_ID='" & p_Modifyid & "'"
            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Session("dtLoan") = ds.Tables(0)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub setViewData() 'setting controls on view/edit

        'imgFrom.Enabled = False
        'imgEmployee.Enabled = False
        'txtDocdate.Attributes.Add("readonly", "readonly")
        'chkPersonal.Enabled = False
        'txtAmount.Attributes.Add("readonly", "readonly")
        'txtRemarks.Attributes.Add("readonly", "readonly")
        'txtInst.Attributes.Add("readonly", "readonly")
        'ddEarnings.Enabled = False
    End Sub



    Private Sub ResetViewData() 'resetting controls on view/edit
        'imgFrom.Enabled = True
        'imgEmployee.Enabled = True

        'txtDocdate.Attributes.Remove("readonly")
        'chkPersonal.Enabled = True

        'txtAmount.Attributes.Remove("readonly")
        'txtRemarks.Attributes.Remove("readonly")
        'txtInst.Attributes.Remove("readonly")


    End Sub



    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = " AND EMP_BSU_ID='" & Session("SBSUID") & "'"

        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If hfBatch_ID.Value <> "" Then 'V1.3
            str_Filter += " and EMP_ID in " _
            & " (select * from dbo.fnSplitMe((select VACD_EMP_IDs from VacationProcessBatch_D inner join VacationProcessBatch_m on " & _
            " VACD_Vac_Batch_ID=Vac_Batch_ID where Vac_Batch_ID=" + hfBatch_ID.Value + "),','))"
        End If
        Session("EMP_SEL_COND") = str_Filter & " AND EMP_bACTIVE=1 and EMP_STATUS in (1,2) "
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim boolNothingSelected As Boolean = True
        Try

            Dim EMPIDS_AIRFARE As String = ""
            Dim EMPIDS_XML As String = ""
            Dim EMP_AIRFARE As String = ""
            Dim EMP_LEAVESSETTLED As String = ""   'V1.1
            If (CDate(txtFromDate.Text) > CDate(txtToDate.Text)) Then
                lblError.Text = "Invalid dates"
                Exit Sub
            End If
            For i As Integer = 0 To gvEmpDetails.Rows.Count - 1
                Dim lblid As New Label
                Dim chkEmployee As New HtmlInputCheckBox
                Dim chkAirfare As New HtmlInputCheckBox
                Dim chkLSettled As New HtmlInputCheckBox
                Dim txtBox As New TextBox

                Dim K As Integer = True
                chkEmployee = TryCast(gvEmpDetails.Rows(i).FindControl("chkEmployee"), HtmlInputCheckBox)
                chkAirfare = TryCast(gvEmpDetails.Rows(i).FindControl("chkAirfare"), HtmlInputCheckBox)
                chkLSettled = TryCast(gvEmpDetails.Rows(i).FindControl("chkLeavesSettled"), HtmlInputCheckBox) 'V1.1

                If chkEmployee.Checked Then
                    If EMPIDS_AIRFARE = "" Then
                        EMPIDS_AIRFARE = chkEmployee.Value & "__" & IIf(chkAirfare.Checked, 1, 0)
                    Else
                        EMPIDS_AIRFARE = chkEmployee.Value & "__" & IIf(chkAirfare.Checked, 1, 0) & "||" & EMPIDS_AIRFARE
                    End If
                    'V1.1 additional condition
                    If EMP_LEAVESSETTLED = "" Then
                        EMP_LEAVESSETTLED = chkEmployee.Value & "__" & IIf(chkLSettled.Checked, 1, 0)
                    Else
                        EMP_LEAVESSETTLED = chkEmployee.Value & "__" & IIf(chkLSettled.Checked, 1, 0) & "||" & EMP_LEAVESSETTLED
                    End If
                    boolNothingSelected = False
                End If
            Next
            If boolNothingSelected Then
                lblError.Text = "Kindly Select Atleast One Employee"
                Exit Sub
            End If
            ' EMPIDS_XML = GenerateXML(EMPIDS_AIRFARE, XMLType.AIRFARE)
            EMPIDS_XML = GenerateXML(EMP_LEAVESSETTLED, XMLType.LEAVESETTLED)
            If gvEmpDetails.Rows.Count = 1 Then
                For Each item As ListItem In ChkListAirFare.Items
                    If item.Selected = True Then
                        If EMP_AIRFARE = "" Then
                            EMP_AIRFARE = item.Value
                        Else
                            EMP_AIRFARE = EMP_AIRFARE & "," & item.Value
                        End If
                    End If
                Next
            End If

            Dim retval As Integer = PayrollFunctions.ProcessVacation(Session("sBsuid"), txtFromDate.Text, _
            txtToDate.Text, EMPIDS_XML, h_Leaveid.Value, EMP_AIRFARE, stTrans, objConn)
            If retval = 0 Then
                stTrans.Commit()
                'ViewVacationDetails()
                ViewVacationDetails(h_EMPID.Value)
                gvEmpDetails.DataBind()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "Vacation Processing", _
           "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            Else
                stTrans.Rollback()
            End If
            lblError.Text = getErrorMessage(retval)
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        'Dim strEmpIds As String = ""
        'Dim lblEMPID As Label
        'FillEmpNames(h_EMPID.Value)


        'For Each dr As GridViewRow In gvEMPName.Rows
        '    lblEMPID = TryCast(dr.FindControl("lblEMPID"), Label)
        '    strEmpIds = lblEMPID.Text + "||" + strEmpIds
        'Next
        ViewVacationDetails(h_EMPID.Value)
    End Sub

    Sub ViewVacationDetails(Optional ByVal p_empid As String = "")
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If

            Dim docDate As Date = CDate(txtFromDate.Text)
            Dim str_Sql As String
            Dim strBatchEmployees As String = String.Empty
            If hfBatch_ID.Value <> "" And p_empid = "" Then   'V1.3
                strBatchEmployees = FillBatchEmployees(hfBatch_ID.Value)
                p_empid = "" ' p_empid + strBatchEmployees
                h_EMPID.Value = strBatchEmployees
            End If
            If p_empid = "" Then
                str_Sql = "SELECT EMP_ID, EMP_BSU_ID, EMP_NAME, EMPNO, EMP_LASTREJOINDT, DAYSINPERIOD, LOPDAYS, " _
                & " ALLOWEDDAYS, LEAVE_TAKEN,  CARRYFORWARD, SALARY, LEAVE_SALARY, LEAVE_SALARYDAYS_SCHEDULED," _
                & " ECT_DESCR, EMP_JOINDT, TKTCOUNT, TKTAMOUNT ,(ALLOWEDDAYS-LOPDAYS-LEAVE_TAKEN+CARRYFORWARD) AS ELIGIBLE_DAYS" _
                 & " from [fn_GetLeaveSchedule]('" & Session("sbsuid") & "','" & txtFromDate.Text & "','" & p_empid & "')  where 1=1 " & _
                GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
                GetFilter("EMP_ABC", strABC, True) & _
                GetFilter("EMP_ID", h_EMPID.Value, True) '& _
            Else
                str_Sql = "SELECT EMP_ID, EMP_BSU_ID, EMP_NAME, EMPNO, EMP_LASTREJOINDT, DAYSINPERIOD, LOPDAYS, " _
                & " ALLOWEDDAYS, LEAVE_TAKEN,  CARRYFORWARD, SALARY, LEAVE_SALARY, LEAVE_SALARYDAYS_SCHEDULED," _
                & " ECT_DESCR, EMP_JOINDT, TKTCOUNT, TKTAMOUNT ,(ALLOWEDDAYS-LOPDAYS-LEAVE_TAKEN+CARRYFORWARD) AS ELIGIBLE_DAYS" _
                & " from [fn_GetLeaveSchedule]('" & Session("sbsuid") & "','" & txtFromDate.Text & "','" & p_empid & "') where 1=1 " & _
                GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
                GetFilter("EMP_ABC", strABC, True)
                '  where  EMP_ID='" & p_empid & "'"
            End If
            'If hfBatch_ID.Value <> "" Then   'V1.3
            '    str_Sql += " and EMP_ID in " _
            '   & " (select * from dbo.fnSplitMe((select VACD_EMP_IDs from VacationProcessBatch_D where VACD_Vac_Batch_ID=" + hfBatch_ID.Value + "),','))"
            'End If
            str_Sql = str_Sql & " order by EMP_NAME"
            Dim ds As New DataSet

            '   ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Dim cmd As New SqlCommand
            Dim conn As New SqlConnection
            conn.ConnectionString = str_conn
            cmd.CommandType = CommandType.Text
            cmd.Connection = conn
            cmd.CommandTimeout = 0
            cmd.CommandText = str_Sql
            Dim _adapter As New SqlDataAdapter(cmd)
            _adapter.Fill(ds)
            gvEmpDetails.DataSource = ds.Tables(0)
            gvEmpDetails.DataBind()
            Dim fromdt As DateTime = CDate(txtFromDate.Text)
            Dim todt As DateTime = CDate(txtToDate.Text)
            Dim retval As Integer = 0
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                retval = PayrollFunctions.FindLeaveStatus(Session("sbsuid"), 0, fromdt.Month, _
                fromdt.Year, ds.Tables(0).Rows(i)("EMP_ID"), txtFromDate.Text)
                If todt.Year > fromdt.Year And retval = 0 Then
                    retval = PayrollFunctions.FindLeaveStatus(Session("sbsuid"), 0, todt.Month, _
                                   todt.Year, ds.Tables(0).Rows(i)("EMP_ID"), txtFromDate.Text)
                End If
                If retval <> 0 Then
                    lblError.Text = "There is some problem in the leave settings"
                End If
            Next
            If ds.Tables(0).Rows.Count = 1 Then
                tr_airfare.Visible = True
                bind_AirfareList(ds.Tables(0).Rows(0)("EMP_ID"))
            Else
                tr_airfare.Visible = False
            End If
        Catch ex As Exception
            lblError.Text = "There is some problem in the leave settings"
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lbClearBatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClearBatch.Click 'V1.3
        DisableAllControls(False)
        hfBatch_ID.Value = ""
        txtBatchName.Text = ""
        StoreEMPFilter()
        ViewVacationDetails(h_EMPID.Value)
    End Sub

    Sub bind_AirfareList(ByVal p_Emp_id As Integer)
        'SELECT EDD_ID, EDD_NAME FROM EMPDEPENDANTS_D WHERE (EDD_EMP_ID = 4756)


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet

        str_Sql = " SELECT  '-1' AS EDD_ID, 'Self' AS EDD_NAME UNION SELECT EDD_ID, EDD_NAME FROM EMPDEPENDANTS_D inner join employee_m on edd_emp_id=emp_id" & _
        " WHERE (EDD_EMP_ID = " & p_Emp_id & ") and Isnull(emp_TICKETFLAG,0)>1"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ChkListAirFare.DataTextField = "EDD_NAME"
        ChkListAirFare.DataValueField = "EDD_ID"
        If ds.Tables(0).Rows.Count > 1 Then
            ChkListAirFare.DataSource = ds.Tables(0)
            ChkListAirFare.DataBind()
            tr_airfare.Visible = True
        Else
            ds.Tables(0).Rows.Clear()
            ChkListAirFare.DataSource = ds.Tables(0)
            ChkListAirFare.DataBind()
            tr_airfare.Visible = False
        End If


    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function
    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEAIRFARE As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim XMLLEAVESETTLED As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
            Case XMLType.AIRFARE
                ReDim elements(4)
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
                elements(3) = "AIRFARE"
            Case XMLType.LEAVESETTLED    'V1.1
                ReDim elements(5)
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
                elements(3) = "AIRFARE"
                elements(4) = "LEAVESETTLED"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    Dim childElements As String() = IDs(i).Split("__")
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = childElements(0)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)

                    XMLEAIRFARE = xmlDoc.CreateElement(elements(3))
                    XMLEAIRFARE.InnerText = childElements(2)
                    XMLEBSUDetail.AppendChild(XMLEAIRFARE)

                    XMLLEAVESETTLED = xmlDoc.CreateElement(elements(4))  'v1.1
                    XMLLEAVESETTLED.InnerText = childElements(2)
                    XMLEBSUDetail.AppendChild(XMLLEAVESETTLED)

                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Session("EMP_SEL_COND") = ""
        DisableAllControls(False)
        gvEmpDetails.DataBind()
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID,EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE  EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function



    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    'swapna added
    Private Function FillBatchEmployees(ByVal BatchID As String) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet

        str_Sql = "select Replace(VACD_EMP_IDs,',','||') VACD_EMP_IDs from VacationProcessBatch_D inner join VacationProcessBatch_m on  VACD_Vac_Batch_ID=Vac_Batch_ID where Vac_Batch_ID =" & BatchID
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return ""
        Else
            Dim str As String = ""
            For Each drrow As DataRow In ds.Tables(0).Rows
                str = str + "||" + drrow.Item("VACD_EMP_IDs").ToString
            Next
            Return str
        End If
        Return True
    End Function





    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub





    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub



    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub



    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub



    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub



    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
            'Dim str_Sql As String = "select ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE " & _
            'GetFilter("ESD_EMP_ID", h_EMPID.Value, False) & _
            '" AND ESD_MONTH = " & ddlPayMonth.SelectedValue & " AND ESD_YEAR = " & ddlPayYear.SelectedValue

            Dim str_Sql As String = "select DISTINCT ESD_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE ESD_BSU_ID='" & Session("sBsuid") & "' " & _
            " AND ESD_EMP_ID ='" & ViewState("emp_id") & "'and ELV_ID ='" & Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")) & "'" & _
            " AND ESD_bVaccation = 1"
            '" OR (ESD_MONTH = " & ddlPayMonth.SelectedValue - 1 & " AND ESD_YEAR = " & ddlPayYear.SelectedValue & "))" & _
            Dim ESD_IDs As String = String.Empty
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            Dim comma As String = String.Empty
            While (dr.Read())
                ESD_IDs += comma + dr(0).ToString
                comma = ","
            End While
            str_Sql = "select * from dbo.vw_OSO_EMPSALARYHEADERDETAILS WHERE ESD_BSU_ID='" & Session("sBsuid") & "' " & _
            " AND ESD_EMP_ID ='" & ViewState("emp_id") & "' and ELV_ID ='" & Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")) & "'" & _
            " AND ESD_bVaccation = 1"
            'ViewState("emp_id") " OR (ESD_MONTH = " & ddlPayMonth.SelectedValue - 1 & " AND ESD_YEAR = " & ddlPayYear.SelectedValue & "))" & _
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                Dim bMultiMonth As Boolean
                bMultiMonth = True
                'If Session("EMP_MONTH_YEAR_COIN") Is Nothing Then
                '    bMultiMonth = False
                'ElseIf Session("EMP_MONTH_YEAR_COIN").Rows.Count <= 1 Then
                '    bMultiMonth = False
                'Else
                '    bMultiMonth = True
                'End If
                params("bMultiMonth") = bMultiMonth
                Dim repSourceSubRep(1) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubEarn As New SqlCommand
                cmdSubEarn.CommandText = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='D' AND ESS_ESD_ID in(" & ESD_IDs & ")"
                cmdSubEarn.Connection = New SqlConnection(str_conn)
                cmdSubEarn.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubEarn
                repSourceSubRep(1) = New MyReportClass
                Dim cmdSubDed As New SqlCommand
                cmdSubDed.CommandText = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='E' AND ESS_ESD_ID in(" & ESD_IDs & ")"
                cmdSubDed.Connection = New SqlConnection(str_conn)
                cmdSubDed.CommandType = CommandType.Text
                repSourceSubRep(1).Command = cmdSubDed
                repSource.SubReport = repSourceSubRep
                'repSource.IncludeBSUImage = True
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalarySlip_VAC.rpt"
                Session("ReportSource") = repSource
                ' Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()

            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        AddNewCancel()
        gvEmpDetails.DataBind()
    End Sub

    Sub AddNewCancel()
        tr_ViewDetailPrint.Visible = False
        tr_ViewProcessing.Visible = True
        tr_GridListAll.Visible = True
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try


            Dim retval As Integer = PayrollFunctions.DeleteEMPLEAVE_D(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")), stTrans)
            If retval = 0 Then
                stTrans.Commit()
                'ViewVacationDetails()
                gvEmpDetails.DataBind()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "Vacation deleting", _
           "Delete", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

            Else
                stTrans.Rollback()
            End If
            lblError.Text = getErrorMessage(retval)
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        AddNewCancel()
        gvEmpDetails.DataBind()
    End Sub


    Protected Sub imgLeavedetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLeavedetails.Click
        Dim str_sql As String
        If txtLeaveRemarks.Text <> "" Then

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            str_sql = "SELECT ELA.ELA_ID, ELA.ELA_EMP_ID, REPLACE(CONVERT(VARCHAR(11), ELA.ELA_DTFROM, 113), ' ', '/') " _
               & " AS ELA_DTFROM ,  REPLACE(CONVERT(VARCHAR(11), ELA.ELA_DTTO, 113), ' ', '/') AS ELA_DTTO, ELA.ELA_REMARKS, " _
               & " EMP.EMPNO,  ISNULL(EMP.EMP_FNAME,'')+ ' ' +ISNULL(EMP.EMP_MNAME,'') +' ' +ISNULL(EMP.EMP_LNAME,'') AS EMP_NAME" _
               & " FROM EMPLEAVEAPP AS ELA INNER JOIN EMPLOYEE_M AS EMP ON ELA.ELA_EMP_ID = EMP.EMP_ID" _
               & " WHERE (ELA.ELA_APPRSTATUS = 'A') AND (ELA.ELA_ELT_ID = 'AL') AND (ELA.ELA_BSU_ID = '" & Session("sBsuid") & "')" _
               & " AND (ELA.ELA_ID NOT IN (SELECT ISNULL(ELV_ELA_ID, 0) AS ELV_ELA_ID FROM EMPLEAVE_D))" _
               & " AND ELA_ID='" & h_Leaveid.Value & "'"
            Dim ds As New DataSet
            'ELA_ID      ELA_EMP_ID  ELA_DTFROM      ELA_DTTO         ELA_REMARKS EMPNO EMP_NAME

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
            If ds.Tables(0).Rows.Count > 0 Then

                txtFromDate.Text = ds.Tables(0).Rows(0)("ELA_DTFROM")
                txtToDate.Text = ds.Tables(0).Rows(0)("ELA_DTTO")
                ViewVacationDetails(ds.Tables(0).Rows(0)("ELA_EMP_ID"))
            End If
            DisableAllControls(True)


        End If
    End Sub

    Sub DisableAllControls(ByVal p_disable As Boolean)
        If p_disable Then

            txtFromDate.Attributes.Add("readonly", "readonly")
            txtToDate.Attributes.Add("readonly", "readonly")
            txtFromDate.Attributes.Add("readonly", "readonly")
            btnView.Enabled = False
            imgFromDate.Enabled = False
            imgToDate.Enabled = False
        Else
            txtFromDate.Attributes.Remove("readonly")
            txtToDate.Attributes.Remove("readonly")
            txtFromDate.Attributes.Remove("readonly")
            btnView.Enabled = True
            imgFromDate.Enabled = True
            imgToDate.Enabled = True
        End If


    End Sub

    Protected Sub lbClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClear.Click
        DisableAllControls(False)
        h_Leaveid.Value = ""
        txtLeaveRemarks.Text = ""
    End Sub


    Protected Sub gvEMPName_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillEmpNames(h_EMPID.Value)
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub btnLeaveRpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeaveRpt.Click
        GenerateLeaveSalaryDetails()
    End Sub
    Private Sub GenerateLeaveSalaryDetails()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            'If chkEMPABC_A.Checked Then
            '    strABC = "A"
            'End If
            'If chkEMPABC_B.Checked Then
            '    strABC += "||B"
            'End If
            'If chkEMPABC_C.Checked Then
            '    strABC += "||C"
            'End If

            Dim strBsuid As String = " "
            strBsuid = Session("sBsuid").ToString


            Dim strCatIds As String = " "

            Dim strEmpIDs As String = " "

            strEmpIDs = ViewState("emp_id")


            Dim str_Sql As String = "EXEC Emp_LEaveSalaryReportIndividual '" & Format(ViewState("DTfrom"), OASISConstants.DateFormat) & "','" & Format(ViewState("DTto"), OASISConstants.DateFormat) & "','" & strBsuid & "' ,'" & strEmpIDs & "','" & strCatIds & "'"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text
                'V1.2 comments start------------
                'Dim repSource As New MyReportClass
                'Dim params As New Hashtable
                'params("userName") = Session("sUsr_name")
                ''params("@FromDt") = txtFromDate.Text
                ''params("@ToDt") = txtToDate.Text
                ' ''params("decimal") = Session("BSU_ROUNDOFF")
                'repSource.Parameter = params
                'repSource.Command = cmd
                ''If chkViewSummary.Checked Then
                ''    params("reportCaption") = "Leave Details"
                ''    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"
                ''Else
                'params("reportCaption") = "Leave Salary Settlement For the period from " + ViewState("DTfrom") + " to " + ViewState("DTto")
                'repSource.ResourceName = "../../payroll/Reports/RPT/rptIndividualLeaveSalary.rpt"

                ''End If
                'Session("ReportSource") = repSource
                'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                'Comment ends----------------------------------------
                Dim params As New Hashtable
                'params.Add("@IMG_BSU_ID", Session("sbsuid"))
                'params.Add("@IMG_TYPE", "LOGO")
                params.Add("@DTFROM", ViewState("DTfrom"))
                params.Add("@DTTO", ViewState("DTto"))
                params.Add("@CatIds", strCatIds)
                params.Add("@EMP_LIST", strEmpIDs)
                params.Add("@BSU_ID", strBsuid)
                params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
                params.Add("reportCaption", "Leave Salary Settlement For the period from " + ViewState("DTfrom") + " to " + ViewState("DTto"))

                ' params("VoucherName") = labHead.Text.ToUpper().ToString()   'V1.1

                Dim rptClass As New rptClass
                With rptClass
                    .crDatabase = "Oasis"
                    .reportParameters = params
                    .reportPath = Server.MapPath("../Payroll/Reports/Rpt/rptIndividualLeaveSalary.rpt")
                    '.reportPath = "../../PAYROLL/REPORTS/RPT/rptEMPLeaveDetails.rpt"

                    'End If
                End With

                Session("rptClass") = rptClass
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection2()

            Else
                lblError.Text = "No Records with specified condition"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvCat_Unload(ByVal sender As Object, ByVal e As System.EventArgs)
        StoreEMPFilter()
    End Sub

    Protected Sub gvEMPName_Unload(ByVal sender As Object, ByVal e As System.EventArgs)
        StoreEMPFilter()
    End Sub

    Protected Sub txtBatchName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        StoreEMPFilter()
    End Sub

    Protected Sub gvEmpDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpDetails.PageIndexChanging
        gvEmpDetails.PageIndex = e.NewPageIndex
        ViewVacationDetails(h_EMPID.Value)
    End Sub

    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub txtCatName_TextChanged(sender As Object, e As EventArgs)
        txtCatName.Text = ""
        FillCATNames(h_CATID.Value)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelection2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
