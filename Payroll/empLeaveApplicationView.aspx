<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empLeaveApplicationView.aspx.vb" Inherits="Payroll_empLeaveApplicationView"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        function confirm_Edit() {
            if (confirm("You are about to edit a forwarded/approved leave application. Editing requires all the approvers to reapprove the changes. Do you want to proceed ?") == true)
                return true;
            else
                return false;
        }
        function confirm_Delete() {
            if (confirm("You are about to delete a leave application. Are you sure you want to proceed ?") == true)
                return true;
            else
                return false;
        }
        function confirm_Cancellation() {
            if (confirm("You are about to cancel a forwarded leave application. Are you sure you want to proceed ?") == true)
                return true;
            else
                return false;
        }
        function ApplyStartMonth(sender, args) {
            var calendarStart = $find('cxT_FrmDt');
            sender.set_visibleDate(calendarStart._selectedDate);
            sender.set_todaysDate(calendarStart._selectedDate);
            return false;
        }
    </script>
    <style>
        .darkPanel {
            position: absolute;
            left: 0px;
            top: 0px;
            bottom: 0px;
            right: 0px;
            background: url('../images/common/dark.png') 0 0 !important;
            display: block;
            min-width: 100%;
            min-height: 100%;
            z-index: 1;
        }

        .darkPanelTop {
            position: relative;
            background-color: #f0f0f0;
            border: 1px solid #0092d7;
            margin: 90px auto 0px auto; /*top, right, bottom and left */
            width: 552px;
            height: 340px;
            z-index: 200;
            padding: 0px 0px 0px 0px;
            -webkit-box-shadow: 0 15px 10px rgba(6,6,6, 0.7);
            -moz-box-shadow: 0 15px 10px rgba(6,6,6, 0.7);
            box-shadow: 0 15px 10px rgba(6,6,6, 0.7);
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
            -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
        }
       .holderInner {
            overflow: hidden;
            background-color: white;
        }
        .holderInner {
            width: 535px;
            height: 297px;
            border: solid 1px #ccc;
            border-collapse: collapse;
            margin: 6px 6px 6px 6px;
            background-color: #FFFFFF;
            text-align: left;
        }
        .darkPanelFooter {
            padding: 1px 0px 0px 1px;
            margin: 0px; /*top, right, bottom and left */
            width: 99.8%;
            height: 27px;
            color: #fff;
            font-size: 16px;
            position: absolute;
            bottom: 0px;
            border-top: 1px solid #0092d7;
            background: #75a9d1;
            background: -moz-linear-gradient(top, #75a9d1 0%, #4372b0 98%, #75a9d1 99%, #4372b0 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#75a9d1), color-stop(98%,#4372b0), color-stop(99%,#75a9d1), color-stop(100%,#4372b0));
            background: -webkit-linear-gradient(top, #75a9d1 0%,#4372b0 98%,#75a9d1 99%,#4372b0 100%);
            background: -o-linear-gradient(top, #75a9d1 0%,#4372b0 98%,#75a9d1 99%,#4372b0 100%);
            background: -ms-linear-gradient(top, #75a9d1 0%,#4372b0 98%,#75a9d1 99%,#4372b0 100%);
            background: linear-gradient(to bottom, #75a9d1 0%,#4372b0 98%,#75a9d1 99%,#4372b0 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#75a9d1', endColorstr='#4372b0',GradientType=0 );
        }

        .TitlePl {
            position: relative;
            float: left;
            padding-left: 6px;
            margin-top: 5px;
            text-decoration: none;
            font-size: 16px;
            font-weight: normal;
        }
        .closelbtnPl {
            position: relative;
            float: right;
            padding-right: 4px;
            margin-top: 2px;
            text-decoration: none;
            font-size: 18px;
            letter-spacing: 1px;
            border: 0px none;
            color: #a6c4dd;
            text-shadow: 0px 2px 3px #555;
            /*text-shadow: 0px 15px 5px rgba(0,0,0,0.1),

                 10px 20px 5px rgba(0,0,0,0.05),

                 -10px 20px 5px rgba(0,0,0,0.05);*/
        }

        .divBoxFrame {
            float: left;
            /*padding-left:2px;padding-right:2px;*/
            padding-top: 1px;
            padding-bottom: 1px;
            margin-left: 0px;
            margin-right: 0px;
            overflow: hidden;
        }

        .button {
            /*border: 1px solid;
    border-color: #50a3c8 #297cb4 #083f6f;
    -webkit-background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    background-size: 100% 100%;
    background: -moz-linear-gradient(top,white,   #72c6e4 4%,    #0c5fa5    );
    background: -webkit-gradient(linear,left top, left bottom,from(white), to(#0c5fa5),color-stop(0.03, #72c6e4) );

    -moz-border-radius: 0.333em;

    -webkit-border-radius: 0.333em;

    -webkit-background-clip: padding-box;

    border-radius: 0.333em;

    color: white;

    -moz-text-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    -webkit-text-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
    -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
    font-size: 13px;
    height:26px;
    cursor: pointer;
    font-weight: bold;*/
        }

            /* IE class

    .ie button {

        overflow: visible;

    }*/

            /* IE class

    .ie7 button {

        padding-top: 0.357em;

        padding-bottom: 0.214em;

        line-height: 1.143em;

    }*/

            .button:hover {
                /*border-color: #1eafdc #1193d5 #035592;

        background: #057fdb  url(../images/button/btnHover.jpg) repeat-x left top;

        background: -moz-linear-gradient(

            top,

            white,

            #2bcef3 4%,

            #057fdb

        );

        background: -webkit-gradient(

            linear,

            left top, left bottom,

            from(white),

            to(#057fdb),

            color-stop(0.03, #2bcef3)

        );*/
            }



        table.ClassGray { /**/
            font-size: 13px;
            border-left: 1px solid #e6e6e6;
            border-right: 1px solid #e6e6e6;
            border-bottom: 1px solid #e6e6e6;
            padding: 12px;
            padding: 0em;
            margin-bottom: 0em;
            border-collapse: collapse;
            color: #1d4070;
            background-color: #fff;
        }

            table.ClassGray th {
                height: 24px;
                text-align: left;
                color: black;
                font-weight: bold;
                padding-left: 4px;
                -moz-border-radius-topleft: 4px;
                border-top-left-radius: 4px;
                -moz-border-radius-topright: 4px;
                border-top-right-radius: 4px;
                border-bottom: 2px solid #b9b9b9;
                border-left: 1px solid #e6e6e6;
                border-right: 1px solid #e6e6e6;
                border-top: 1px solid #e6e6e6;
                font-size: 13px;
                background-color: #fafafa;
                background: -moz-linear-gradient(top, #fafafa 0%, #e9e9e9 100%, #fafafa 100%, #e9e9e9 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fafafa), color-stop(100%,#e9e9e9), color-stop(100%,#fafafa), color-stop(100%,#e9e9e9));
                background: -webkit-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
                background: -o-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
                background: -ms-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
                background: linear-gradient(to bottom, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#e9e9e9',GradientType=0 );
            }





            table.ClassGray tr {
                border: 0px none !important;
            }

            table.ClassGray td {
                padding-left: 4px;
                padding-top: 4px;
                padding-bottom: 4px;
                vertical-align: middle;
                border: 0px none !important;
                -moz-border-radius: 0;
                font-weight: normal;
            }



        .calPrevMth {
            text-decoration: none;
            font-size: 12px;
            padding-left: 10px;
            padding-right: 6px;
            /*background: transparent url(../Images/common/MthPrev.png) no-repeat -1px -1px;*/
            color: black;
            float: left;
            line-height: 23px;
        }

        .calNxtMth {
            text-decoration: none;
            font-size: 12px;
            /*background: transparent url(../Images/common/MthNext.png) no-repeat 15px 0px;*/
            color: black;
            padding-left: 6px;
            padding-right: 10px;
            /*background: transparent url(../Images/common/MthNext.png) no-repeat 30px -1px;*/
            float: right;
            line-height: 23px;
        }

        .calMthYear {
            margin: 0px;
            width: 100%;
            height: 24px;
            text-align: left;
            color: #1d4070;
            padding-left: 1px;
            padding-top: 2px;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            border-bottom: 1px solid #b9b9b9;
            border-left: 1px solid #e6e6e6;
            border-right: 1px solid #e6e6e6;
            border-top: 1px solid #e6e6e6;
            font-size: 15px;
            background-color: #fafafa;
            background: -moz-linear-gradient(top, #fafafa 0%, #e9e9e9 100%, #fafafa 100%, #e9e9e9 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fafafa), color-stop(100%,#e9e9e9), color-stop(100%,#fafafa), color-stop(100%,#e9e9e9));
            background: -webkit-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -o-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -ms-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: linear-gradient(to bottom, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#e9e9e9',GradientType=0 );
        }

        .calDayStyle {
            border: 1px solid #e6e6e6;
            height: 24px;
            width: 20px;
            text-decoration: none;
            font-size: 14px;
            text-align: center;
            color: #000;
            background-color: #ffffff;
        }



            .calDayStyle:hover {
                color: #000 !important;
                background: #e7fd7e;
                background: -moz-linear-gradient(top, #e7fd7e 0%, #e0fb5e 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e7fd7e), color-stop(100%,#e0fb5e));
                background: -webkit-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: -o-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: -ms-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: linear-gradient(to bottom, #e7fd7e 0%,#e0fb5e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e7fd7e', endColorstr='#e0fb5e',GradientType=0 );
            }





        .calDayToday {
            color: #000;
            font-size: 13px;
            text-decoration: none;
            background: #85b9ff;
            border: none;
        }

            .calDayToday a {
                color: #000 !important;
            }



        .calWeekTitle {
            text-align: center;
            color: #fff;
            font-weight: bold;
            border: 0px none;
            font-size: 13px;
            height: 23px;
            background: #75a9d1;
            background: -moz-linear-gradient(top, #91C252 0%, #91C252 100%, 91C252 100%, #91C252 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#91C252), color-stop(100%,#91C252), color-stop(100%,#91C252), color-stop(100%,#91C252));
            background: -webkit-linear-gradient(top, #91C252 0%,#91C252 100%,#91C252 100%,#91C252 100%);
            background: -o-linear-gradient(top, #91C252 0%,#91C252 100%,#91C252 100%,#91C252 100%);
            background: -ms-linear-gradient(top, #91C252 0%,#91C252 100%,#91C252 100%,#91C252 100%);
            background: linear-gradient(to bottom, #91C252 0%,#91C252 100%,#91C252 100%,#91C252 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#91C252', endColorstr='#91C252',GradientType=0 );
        }

        .calWeekendstyle {
            color: #ffffff !important;
            font-weight: bold;
            border-color: #0c0;
            background: #88e888;
            text-align: center !important;
            vertical-align: middle !important;
            background: -moz-linear-gradient(top, #88e888 0%, #9dff9d 1%, #42fd42 2%, #01ab01 98%, #37ef37 99%, #37ef37 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#88e888), color-stop(1%,#9dff9d), color-stop(2%,#42fd42), color-stop(98%,#01ab01), color-stop(99%,#37ef37), color-stop(100%,#37ef37));
            background: -webkit-linear-gradient(top, #88e888 0%,#9dff9d 1%,#42fd42 2%,#01ab01 98%,#37ef37 99%,#37ef37 100%);
            background: -o-linear-gradient(top, #88e888 0%,#9dff9d 1%,#42fd42 2%,#01ab01 98%,#37ef37 99%,#37ef37 100%);
            background: -ms-linear-gradient(top, #88e888 0%,#9dff9d 1%,#42fd42 2%,#01ab01 98%,#37ef37 99%,#37ef37 100%);
            background: linear-gradient(to bottom, #88e888 0%,#9dff9d 1%,#42fd42 2%,#01ab01 98%,#37ef37 99%,#37ef37 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#88e888', endColorstr='#37ef37',GradientType=0 );
        }

        .calOtherDay {
            border: 1px solid #e6e6e6;
            height: 24px;
            width: 20px;
            text-decoration: none;
            font-size: 14px;
            text-align: center;
            background: #eeeeee;
        }

            .calOtherDay a {
                color: #8c8c8a !important;
            }

        .calCss {
            border-collapse: collapse;
            color: #b9b9b9;
        }



        .tdfields {
            font-weight: 500;
            font-size: 13px;
            color: #084777 !important;
        }

        .tdPadDiv {
            padding-top: 10px;
            padding-bottom: 1px;
        }

        .dropDownList {
            color: #000;
            font-size: 12px;
            height: 21px;
            border: 1px solid #d8d8d8;
        }

        .remark {
            font-size: 11px;
            color: #3f3f3f;
            font-weight: normal;
        }

        .remark_info {
            font-size: 11px;
            color: #3f3f3f;
            font-weight: bold;
            clear: both;
        }

        .multipleText_full {
            color: #000000;
            border: 1px solid #d8d8d8;
            display: block;
            resize: none;
        }



        .inputtext {
            height: 19px;
            color: #000000;
            border: 1px solid #d8d8d8;
            font-size: 12px;
        }



        .calCss a {
            text-decoration: none;
        }

        .calCss table tr {
            border: 0px none;
        }





        .divSub {
            border: 1px solid #e6e6e6;
            cursor: default;
            Height: 30px;
            float: left;
            text-align: center;
            vertical-align: middle;
            color: black;
            font-size: 13px;
            font-weight: bold;
            /**/
            padding: 4px;
            /*color:#d82133;  color:#079adc;*/
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
            background: #ffffff;
            background: -moz-linear-gradient(top, #ffffff 0%, #f0f0f0 100%, #ffffff 100%, #f0f0f0 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f0f0f0), color-stop(100%,#ffffff), color-stop(100%,#f0f0f0));
            background: -webkit-linear-gradient(top, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            background: -o-linear-gradient(top, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            background: -ms-linear-gradient(top, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            background: linear-gradient(to bottom, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f0f0f0',GradientType=0 );
        }

            .divSub div {
                margin-bottom: -5px;
                padding: 3px 3px 2px 3px;
                cursor: default;
            }



        @font-face {
            font-family: 'Conv_Walkway_SemiBold';
            src: url('Fonts/Walkway_SemiBold.eot');
            src: url('Fonts/Walkway_SemiBold.woff') format('woff'), url('Fonts/Walkway_SemiBold.ttf') format('truetype'), url('Fonts/Walkway_SemiBold.svg') format('svg');
            font-weight: normal;
            font-style: normal;
            font-size: 38px;
        }



        .buttonSave {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 22px;
            padding-right: 20px;
            background: transparent url(../Images/common/saveBtn.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            letter-spacing: 1px;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }



            .buttonSave:hover {
                border: 1px solid #fff;
                background-color: #85b9ff;
                color: #fff;
            }



        .buttonCancel {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 26px;
            padding-right: 17px;
            letter-spacing: 1px;
            background: transparent url(../Images/common/CancelBtn.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }

            .buttonCancel:hover {
                border: 1px solid #fff;
                color: #fff;
                background-color: #85b9ff;
            }



        .tblLeaveCal {
            padding: 0px;
            border-collapse: collapse;
        }

        .tdRoundedge {
            border-left: 1px solid #e6e6e6;
            border-right: 1px solid #e6e6e6;
            border-top: 1px solid #e6e6e6;
            border-bottom: 1px solid #e6e6e6;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
        }

        .trLeaveCal_head {
            height: 28px;
            text-align: center;
            padding: 0px;
            margin: 0px;
            border: 0px none;
            color: black;
            font-size: 12px;
            font-weight: bold !important;
            border-collapse: collapse;
            background-color: #fafafa;
            background: -moz-linear-gradient(top, #fafafa 0%, #e9e9e9 100%, #fafafa 100%, #e9e9e9 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fafafa), color-stop(100%,#e9e9e9), color-stop(100%,#fafafa), color-stop(100%,#e9e9e9));
            background: -webkit-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -o-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -ms-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: linear-gradient(to bottom, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#e9e9e9',GradientType=0 );
        }

            .trLeaveCal_head td {
                border: 0px none;
                font-weight: bold !important;
            }

        .trLeaveCal_Row {
            height: 28px;
            border-top: 2px solid #b9b9b9;
            text-align: center;
            color: #222222;
            font-size: 12px;
            font-weight: bold;
        }

            .trLeaveCal_Row td {
                font-weight: bold !important;
            }

        .CssCalMth {
            text-decoration: none;
            font-size: 13px;
            padding-right: 9px;
            color: #1d4070;
            float: left;
            cursor: pointer;
        }

        .divLinkBorder {
            float: left;
            Border-top: 1px solid #e6e6e6;
            padding-top: 4px;
            padding-bottom: 4px;
            margin-left: 4px;
            margin-top: 40px;
            margin-right: 16px;
        }

        .divSeperator {
            border: 1px solid #e6e6e6;
            cursor: default;
            Height: 30px;
            float: left;
            margin-top: -35px;
            text-align: center;
            vertical-align: middle;
            margin-bottom: -4px;
            color: black;
            font-size: 13px;
            font-weight: bold;
            /**/
            /*color:#d82133;  color:#079adc;*/
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
            background: #ffffff;
            background: -moz-linear-gradient(top, #ffffff 0%, #f0f0f0 100%, #ffffff 100%, #f0f0f0 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f0f0f0), color-stop(100%,#ffffff), color-stop(100%,#f0f0f0));
            background: -webkit-linear-gradient(top, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            background: -o-linear-gradient(top, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            background: -ms-linear-gradient(top, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            background: linear-gradient(to bottom, #ffffff 0%,#f0f0f0 100%,#ffffff 100%,#f0f0f0 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f0f0f0',GradientType=0 );
        }

            .divSeperator div {
                margin-bottom: -5px;
                padding: 3px 3px 2px 3px;
                cursor: default;
            }

        .rddlCss {
            padding-top: -10px;
            margin-top: -10px;
        }



        .divBoxScroll {
            float: left;
            height: 198px;
            border: 0px none;
            padding-left: 0px;
            padding-right: 0px;
            padding-top: 1px;
            padding-bottom: 1px;
            margin-left: 0px;
            margin-right: 0px;
            overflow: hidden;
        }

        .divBoxScrollInside {
            overflow: scroll;
            height: 219px;
        }

        .divBoxFrame_Remarks {
            float: left;
            margin-left: 0px;
            border-top: 1px solid #e6e6e6;
            border-right: 1px solid #e6e6e6;
            border-bottom: 1px solid #e6e6e6;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
        }

        .divBoxFrame_Title {
            /*top, right, bottom and left */
            height: 33px !important;
            border-bottom: 2px solid #b9b9b9;
            color: black;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
            text-align: left;
            font-size: 12px;
            background-color: #fafafa;
            background: -moz-linear-gradient(top, #fafafa 0%, #e9e9e9 100%, #fafafa 100%, #e9e9e9 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fafafa), color-stop(100%,#e9e9e9), color-stop(100%,#fafafa), color-stop(100%,#e9e9e9));
            background: -webkit-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -o-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -ms-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: linear-gradient(to bottom, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#e9e9e9',GradientType=0 );
        }

        .spanFrameTitle {
            margin-left: 5px;
            margin-top: 9px;
            float: left;
            vertical-align: middle;
            color: black;
            font-size: 12px;
            font-weight: bold;
        }



        .divMainBox {
            margin-left: 5px;
            border: 1px solid #d1cfcf;
            margin-top: 2px;
            height: 83px;
            width: 111px;
            display: inline-block;
            -moz-border-radius-topleft: 5px;
            border-top-left-radius: 5px;
            -moz-border-radius-topright: 5px;
            border-top-right-radius: 5px;
            -moz-box-shadow: 3px 3px 3px rgba(68,68,68,0.6);
            -webkit-box-shadow: 3px 3px 3px rgba(68,68,68,0.6);
            box-shadow: 3px 3px 3px rgba(68,68,68,0.6);
        }

            .divMainBox .divMainSubBox {
                /* This protects the inner element from being blurred */
                padding: 1px 5px 0px 5px;
                margin-top: 3px;
                width: 110px;
                height: 25px;
                color: #1d4070;
                font-weight: bold;
                border-bottom: 1px solid #b9b9b9;
                text-align: left;
                font-size: 12px;
                /**/
                -moz-border-radius-topleft: 4px;
                border-top-left-radius: 4px;
                -moz-border-radius-topright: 4px;
                border-top-right-radius: 4px;
                /*background-color:#fafafa;

       background: -moz-linear-gradient(top,  #fafafa 0%, #e9e9e9 100%, #fafafa 100%, #e9e9e9 100%) ;

       background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fafafa), color-stop(100%,#e9e9e9), color-stop(100%,#fafafa), color-stop(100%,#e9e9e9));

       background:  -webkit-linear-gradient(top,  #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);

       background: -o-linear-gradient(top,  #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);

       background:  -ms-linear-gradient(top,  #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);

       background: linear-gradient(to bottom,  #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);

       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#e9e9e9',GradientType=0 );*/
            }

        .divMainBoxCenter {
            width: 100%;
            padding-top: 3px;
            padding-left: 45%;
            color: #0076d1;
            text-align: center;
            font-size: 25px;
        }

        .divMainBoxFooter {
            width: 100%;
            text-align: left;
            margin-top: 3px;
            height: 16px;
            border-top: 1px solid #d1cfcf;
            background-color: #fafafa;
            background: -moz-linear-gradient(top, #fafafa 0%, #e9e9e9 100%, #fafafa 100%, #e9e9e9 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fafafa), color-stop(100%,#e9e9e9), color-stop(100%,#fafafa), color-stop(100%,#e9e9e9));
            background: -webkit-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -o-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: -ms-linear-gradient(top, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            background: linear-gradient(to bottom, #fafafa 0%,#e9e9e9 100%,#fafafa 100%,#e9e9e9 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#e9e9e9',GradientType=0 );
        }

        .spanLeftText {
            color: #1d4070;
            float: left;
            display: inline;
            left: 0px;
            position: relative;
            margin: 2px 0px 2px 2px;
            font-size: 10px;
            font-weight: bold;
        }



        .spanRightLink {
            float: right;
            padding-right: 2px;
            font-size: 9px;
            display: inline;
            color: #317abf;
            font-weight: bold;
            text-decoration: none;
            margin: 3px 0px 2px 2px;
        }

        .calEventDay {
            float: left;
            font-size: 11px;
            color: #317abf;
            font-weight: bold;
            text-align: center;
        }

        .calEventName {
            float: left;
            padding-left: 4px;
            padding-bottom: 1px;
            font-size: 12px;
            color: #2b8dd8;
            font-weight: bold;
            letter-spacing: 1px;
        }

        .calEventBoxMain {
            margin-left: 5px;
            margin-top: 7px;
            margin-bottom: 7px;
        }

        .calEventBoxSub {
            border-bottom: 1px solid #e6e6e6;
            width: 100%;
            float: left;
            vertical-align: middle;
        }

        .calEventColor {
            border: 1px solid #fff;
            width: 8px;
            height: 8px;
            display: block;
            float: left;
            margin-top: 1px;
        }

        .calEventDate {
            font-size: 11px;
            color: #222222;
            font-weight: bold;
        }



        .divInfoMsg {
            display: block;
            margin: 2px;
            border: 1px solid #f2fb00;
            border-collapse: collapse;
            text-align: left;
            font-size: 12px;
            font-weight: 600;
            color: #2e2e2e;
            width: 98%;
            min-height: 20px;
            background-position: 5px center;
            padding-left: 20px;
            padding-top: 2pt;
            padding-bottom: 2pt;
            -webkit-box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            -moz-box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
            -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
            background: #feffb3 url(../images/common/info_s.png) no-repeat 2px 3px;
            background: url(../images/common/info_s.png) no-repeat 2px 3px;
            background: url(../images/common/info_s.png) no-repeat 2px 3px,-moz-linear-gradient(top, #feffb3 0%, #fdff8a 98%, #feffb3 99%, #fdff8a 100%);
            background: url(../images/common/info_s.png) no-repeat 2px 3px,-webkit-gradient(linear, left top, left bottom, color-stop(0%,#feffb3), color-stop(98%,#fdff8a), color-stop(99%,#feffb3), color-stop(100%,#fdff8a));
            background: url(../images/common/info_s.png) no-repeat 2px 3px, -webkit-linear-gradient(top, #feffb3 0%,#fdff8a 98%,#feffb3 99%,#fdff8a 100%);
            background: url(../images/common/info_s.png) no-repeat 2px 3px,-o-linear-gradient(top, #feffb3 0%,#fdff8a 98%,#feffb3 99%,#fdff8a 100%);
            background: url(../images/common/info_s.png) no-repeat 2px 3px, -ms-linear-gradient(top, #feffb3 0%,#fdff8a 98%,#feffb3 99%,#fdff8a 100%);
            background: url(../images/common/info_s.png) no-repeat 2px 3px,linear-gradient(to bottom, #feffb3 0%,#fdff8a 98%,#feffb3 99%,#fdff8a 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffb3', endColorstr='#fdff8a',GradientType=0 );
        }



        .divErrorMsg {
            width: 98%;
            display: block;
            margin: 2px;
            border: 1px solid #fb9393;
            border-collapse: collapse;
            text-align: left;
            font-size: 12px;
            font-weight: 600;
            color: #2e2e2e;
            min-height: 20px;
            background-position: 5px center;
            padding-left: 21px;
            padding-top: 2pt;
            padding-bottom: 2pt;
            -webkit-box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            -moz-box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
            -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
            background: #febbbc;
            background: url(../images/common/error_s.png) no-repeat 2px 3px;
            background: url(../images/common/error_s.png) no-repeat 2px 3px,-moz-linear-gradient(top, #febbbc 0%, #fe7a7a 98%, #febbbc 99%, #fe7a7a 100%);
            background: url(../images/common/error_s.png) no-repeat 2px 3px,-webkit-gradient(linear, left top, left bottom, color-stop(0%,#febbbc), color-stop(98%,#fe7a7a), color-stop(99%,#febbbc), color-stop(100%,#fe7a7a));
            background: url(../images/common/error_s.png) no-repeat 2px 3px, -webkit-linear-gradient(top, #febbbc 0%,#fe7a7a 98%,#febbbc 99%,#fe7a7a 100%);
            background: url(../images/common/error_s.png) no-repeat 2px 3px,-o-linear-gradient(top, #febbbc 0%,#fe7a7a 98%,#febbbc 99%,#fe7a7a 100%);
            background: url(../images/common/error_s.png) no-repeat 2px 3px, -ms-linear-gradient(top, #febbbc 0%,#fe7a7a 98%,#febbbc 99%,#fe7a7a 100%);
            background: url(../images/common/error_s.png) no-repeat 2px 3px,linear-gradient(to bottom, #febbbc 0%,#fe7a7a 98%,#febbbc 99%,#fe7a7a 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#febbbc', endColorstr='#fe7a7a',GradientType=0 );
        }









        .divValidMsg {
            display: block;
            margin: 2px;
            border: 1px solid #b1db41;
            text-align: left;
            font-size: 12px;
            color: #2e2e2e;
            font-weight: 600;
            width: 98%;
            min-height: 20px;
            background-position: 5px center;
            background: #d1ff83;
            background: url(../images/common/success_s.png) no-repeat 2px 3px;
            padding-left: 21px;
            padding-top: 2pt;
            padding-bottom: 2pt;
            -webkit-box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            -moz-box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            box-shadow: 0 3px 1px rgba(6,6,6, 0.4);
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
            -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
            background: url(../images/common/success_s.png) no-repeat 2px 3px,-moz-linear-gradient(top, #d1fc72 0%, #ccea89 98%, #d1fc72 99%, #ccea89 100%);
            background: url(../images/common/success_s.png) no-repeat 2px 3px,-webkit-gradient(linear, left top, left bottom, color-stop(0%,#d1fc72), color-stop(98%,#ccea89), color-stop(99%,#d1fc72), color-stop(100%,#ccea89));
            background: url(../images/common/success_s.png) no-repeat 2px 3px, -webkit-linear-gradient(top, #d1fc72 0%,#ccea89 98%,#d1fc72 99%,#ccea89 100%);
            background: url(../images/common/success_s.png) no-repeat 2px 3px,-o-linear-gradient(top, #d1fc72 0%,#ccea89 98%,#d1fc72 99%,#ccea89 100%);
            background: url(../images/common/success_s.png) no-repeat 2px 3px, -ms-linear-gradient(top, #d1fc72 0%,#ccea89 98%,#d1fc72 99%,#ccea89 100%);
            background: url(../images/common/success_s.png) no-repeat 2px 3px,linear-gradient(to bottom, #d1fc72 0%,#ccea89 98%,#d1fc72 99%,#ccea89 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d1fc72', endColorstr='#ccea89',GradientType=0 );
        }





        /* Begining of CSS Style added -- Swapna*/



        table.ClassPlain tr {
            border: 0px none !important;
        }

        table.ClassPlain td {
            padding-left: 4px;
            padding-top: 4px;
            padding-bottom: 4px;
            vertical-align: middle;
            border: 0px none !important;
            -moz-border-radius: 0;
            font-weight: normal;
        }



        .tdProfileHeader {
            font-size: 12px;
            font-weight: 600;
            color: #084777 !important;
        }

        .tdProfileFields {
            font-size: 12px;
            font-weight: 100;
            color: #084777 !important;
        }

        .tdProfileHeaderSmall {
            font-size: 11px;
            font-weight: 600;
            color: #084777 !important;
        }

        .tdProfileFieldsSmall {
            font-size: 11px;
            font-weight: 100;
            color: #084777 !important;
        }

        /* Closing of CSS Style added -- Swapna*/





        /*

      

       Newly added styles Hashmi

 

       */



        .buttonEdit {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 22px;
            padding-right: 20px;
            background: transparent url(../Images/common/Edit.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            letter-spacing: 1px;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }



            .buttonEdit:hover {
                border: 1px solid #fff;
                background-color: #85b9ff;
                color: #fff;
            }



        .PopUpTitle {
            vertical-align: super;
            color: #1d4070;
            font-size: 13px;
            font-weight: bold;
        }



        .buttonClose {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 26px;
            padding-right: 17px;
            letter-spacing: 1px;
            background: transparent url(../Images/common/close-window_off.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }

            .buttonClose:hover {
                border: 1px solid #fff;
                color: #fff;
                background-color: #85b9ff;
            }



        .buttonOverlap {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 22px;
            padding-right: 20px;
            background: url(../Images/common/overlap.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            letter-spacing: 1px;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }



            .buttonOverlap:hover {
                border: 1px solid #fff;
                background-color: #85b9ff;
                color: #fff;
            }



        .buttonDelete {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 26px;
            padding-right: 17px;
            letter-spacing: 1px;
            background: transparent url(../Images/common/cross.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }

            .buttonDelete:hover {
                border: 1px solid #fff;
                color: #fff;
                background-color: #85b9ff;
            }



        .buttonPrint {
            margin-right: 2px;
            margin-left: 2px;
            padding-left: 26px;
            padding-right: 17px;
            letter-spacing: 1px;
            background: transparent url(../Images/common/print.png) no-repeat 4px 5px; /*  left top*/
            background-color: #fff;
            color: #7b7b7b;
            border: 1px solid #c3c3c3;
            font-size: 12px;
            font-weight: bold;
            -moz-border-radius: 0.333em;
            -webkit-border-radius: 0.333em;
            -webkit-background-clip: padding-box;
            border-radius: 0.333em;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
            cursor: pointer;
        }

            .buttonPrint:hover {
                border: 1px solid #fff;
                color: #fff;
                background-color: #85b9ff;
            }

        .linkMenuDeActiveMainLeft {
            color: #222;
            font-size: 11pt;
            letter-spacing: 1px;
            padding-left: 2px;
            padding-right: 18px;
            text-decoration: none;
            font-style: normal;
            font-weight: 100;
        }

        .ajax__calendar_days table tr td,
        .ajax__calendar_months table tr td,
        .ajax__calendar_years table tr td {
            padding: 0 !important;
            margin: 0 !important;
        }

        .textboxWatermark {
            /**/ color: #bebebe;
            font-size: 10pt;
        }

        .RadDropDownList_Default {
            font: inherit !important;
            font-weight: normal !important;
        }


    </style>
    <script>
        function AcceptValues(a) {
            //alert(a)
            location.href = a;
        }

    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Apply Leave 
        </div>
        <div class="card-body">
            <%--<div class="table-responsive">--%>

            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 divBoxFrame">
                    <asp:Label ID="lblmsg" runat="server" CssClass="error" Visible="False" Width="99%"></asp:Label>
                    <table width="100%" class="ClassGray">
                        <tr>
                            <th colspan="4" class="divBoxFrame_Title">Application Form
                                <asp:LinkButton ID="lnkHistory" runat="server" CssClass="float-right" Text="[+] Leave History" OnClientClick="Popup('empLeaveApplicationList.aspx');return false;"></asp:LinkButton>
                            </th>
                        </tr>
                        <tr>
                            <td align="left"><span class="profile-label">Leave Type</span><span class="text-danger">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddMonthstatusPeriodically" runat="server" AutoPostBack="True" ViewStateMode="Enabled">
                                </asp:DropDownList>
                                <asp:ImageButton ID="imgEmployee" runat="server" CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();" Visible="False" />
                            </td>
                            <td align="left" class="tdfields"><span class="profile-label">Employee Since</span></td>
                            <td>
                                <asp:label ID="txtJoinDate" runat="server" CssClass="field-value" ReadOnly="True"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdfields"><span class="profile-label">From Date</span><span class="text-danger">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFrom" AutoPostBack="True" runat="server" Width="110px" CssClass="inputtext" TabIndex="8"
                                    MaxLength="11">

                                </asp:TextBox>

                                <ajaxToolkit:TextBoxWatermarkExtender ID="TBWM1" runat="server" TargetControlID="txtFrom" WatermarkText="dd/mmm/yyyy" />

                                <asp:ImageButton ID="imgBtnT_FrmDt" runat="server" ImageUrl="~/Images/calendar.gif" />

                                <div class="remark" style="padding-top: 4px;">

                                    <%--dd/mmm/yyyy (e.g. 01/Sep/2012)--%>
                                </div>

                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbeDOJ" runat="server" TargetControlID="txtFrom"
                                    FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                                </ajaxToolkit:FilteredTextBoxExtender>

                                <ajaxToolkit:CalendarExtender ID="cxT_FrmDt" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnT_FrmDt"
                                    TargetControlID="txtFrom">
                                </ajaxToolkit:CalendarExtender>

                                <ajaxToolkit:CalendarExtender ID="cxT_FrmDtC" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtFrom"
                                    TargetControlID="txtFrom">
                                </ajaxToolkit:CalendarExtender>
                            </td>

                            <td align="left" class="tdfields"><span class="profile-label">To Date</span><span class="text-danger">*</span>

                            </td>

                            <td>

                                <asp:TextBox ID="txtTo" AutoPostBack="True" runat="server" Width="110px" CssClass="inputtext" TabIndex="8"
                                    MaxLength="11">

                                </asp:TextBox>

                                <ajaxToolkit:TextBoxWatermarkExtender ID="TBWM2" runat="server" TargetControlID="txtTo" WatermarkText="dd/mmm/yyyy" />

                                <asp:ImageButton ID="imgBtnT_ToDt" runat="server" ImageUrl="~/Images/calendar.gif" />

                                <div class="remark" style="padding-top: 4px;">

                                    <%--dd/mmm/yyyy (e.g. 01/Sep/2012)--%>
                                </div>

                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTo"
                                    FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                                </ajaxToolkit:FilteredTextBoxExtender>

                                <ajaxToolkit:CalendarExtender ID="cxT_ToDt" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnT_ToDt"
                                    TargetControlID="txtTo" OnClientShown="ApplyStartMonth" CssClass="MyCalendar">
                                </ajaxToolkit:CalendarExtender>

                                <ajaxToolkit:CalendarExtender ID="cxT_ToDtC" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtTo"
                                    TargetControlID="txtTo" OnClientShown="ApplyStartMonth" CssClass="MyCalendar">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>





                        <tr>

                            <td colspan="4" style="padding-top: 4px; padding-bottom: 4px;"></td>

                        </tr>



                        <tr>

                            <td colspan="4" style="padding-top: 4px; padding-bottom: 4px;">

                                <div style="float: right; width: 99%; border-bottom: 1px solid  #e6e6e6;">

                                    <div class="divSub">

                                        <div>Leave Calculation</div>



                                    </div>

                                </div>



                            </td>

                        </tr>



                        <tr>

                            <td colspan="4" style="font-weight: bold;">

                                <div class="tdRoundedge" style="font-weight: bold;">

                                    <table style="width: 100%; font-weight: bold;" class="tblLeaveCal">

                                        <tr class="trLeaveCal_head">
                                            <td>Leave Type</td>
                                            <td>Date</td>
                                            <td>Return Date</td>
                                            <td>Total Days</td>
                                            <td>Actual Days</td>
                                            <td>Remaining Days</td>
                                        </tr>
                                        <asp:Literal ID="LiteralLeaveCalculation" runat="server" Text="" Visible="true"></asp:Literal>

                                    </table>
                                </div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>&nbsp;</td>
                        </tr>--%>
                        <tr>
                            <td align="left" class="tdfields" colspan="2"><span class="profile-label">Leave Details</span><span class="text-danger">*</span>

                                <asp:TextBox ID="txtRemarks" runat="server" TabIndex="22" TextMode="MultiLine"
                                    Rows="4" CssClass="multipleText_full" Width="310px" Height="30px"></asp:TextBox>

                            </td>
                            <td align="left" class="tdfields" colspan="2"><span class="profile-label">Work will be handed to</span><span class="text-danger">*</span>

                                <asp:TextBox ID="txtHandover" runat="server" TabIndex="22" TextMode="MultiLine"
                                    Rows="4" CssClass="multipleText_full" Width="310px" Height="30px"></asp:TextBox>

                            </td>
                        </tr>
                        <tr id="trUplaod" runat="server" visible="false">

                            <td colspan="4" style="padding-top: 4px; padding-bottom: 4px;"><span class="profile-label">Medical Documents</span>

                                <asp:FileUpload ID="filupload" runat="server" Width="300px" />

                                &nbsp;<asp:Label ID="lblView" runat="server" CssClass="matters_Colln" Display="None" Text="View Uploaded Document :"></asp:Label>

                                <asp:HyperLink ID="imgDoc" runat="server" ImageUrl="../Images/ViewDoc.png" ToolTip="Click To View Document">View</asp:HyperLink>

                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/cross.png" ToolTip="Click To Delete Document" />



                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-top: 4px; padding-bottom: 4px;">

                                <div style="float: right; width: 100%; border-bottom: 1px solid  #e6e6e6;">

                                    <div class="divSub">
                                        <div>Contact Details During Leave</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdfields" colspan="2"><span class="profile-label">Address</span><span class="text-danger">*</span>

                                <asp:TextBox ID="txtAddress" runat="server" TabIndex="22" TextMode="MultiLine" Width="310px"
                                    Rows="3" CssClass="multipleText_full"></asp:TextBox>

                            </td>
                            <td align="left" class="tdfields">

                                <br />

                                <span class="profile-label">Phone</span><br />

                                <br />

                                <span class="profile-label">Mobile</span><br />
                            </td>

                            <td>
                                <br />
                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" Width="180px" CssClass="inputtext">

                                </asp:TextBox>
                                <br />
                                <br />

                                <asp:TextBox ID="txtMobile" runat="server" CssClass="inputtext" MaxLength="50" Width="180px">

                                </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div style="width: 100%; text-align: center;">
                        <asp:Button ID="btnSavePopUp" runat="server" Text="Apply" CssClass="button" />

                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" Visible="False" Width="75px" />

                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="button" Visible="False" />

                        <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="button" Style="display: none;" />

                        <asp:Button ID="btnPrintLeave" runat="server" Text="Print" CssClass="button" Visible="False" />

                        <asp:Button ID="btnCancelLeave" runat="server" CssClass="button" Text="Cancel Leave Application" />

                        <asp:Button ID="lnkbackbtn" runat="server" CssClass="button" OnClick="lnkbackbtn_Click" Text="Back"></asp:Button>
                        <asp:HiddenField ID="h_Emp_No" runat="server" />

                        <asp:HiddenField ID="h_Bsu_Id" runat="server" />

                        <asp:CheckBox ID="chkForward" runat="server" Checked="True" Text="Forward For Approval" CssClass="tdfields" Visible="False" />

                        <asp:HiddenField ID="h_ELA_ID" runat="server" />

                        <asp:HiddenField ID="h_DOC_PCD_ID" runat="server" />

                        <asp:HiddenField ID="h_ID" runat="server" />
                    </div>


                </div>
                <div class="col-lg-6 col-md-12 col-sm-12  divBoxFrame">




                    <div style="width: 50%; float: left;">
                        <%--width: 350px;--%>

                        <table class="calMthYear">

                            <tr>
                                <th>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-3 col-lg-3 col-md-3 col-sm-3 m-0 p-0">
                                                <asp:LinkButton ID="lbtnPreMth" runat="server" Text="Nov" CssClass="calPrevMth" Style="display: none;"></asp:LinkButton>
                                            </div>
                                            <div class="col-6 col-lg-6 col-md-6 col-sm-6 m-0 p-0">
                                                <telerik:RadDropDownList ID="rddlMth" runat="server" BackColor="White" BorderColor="#d8d8d8" Width="53px"
                                                    Font-Size="10px" AutoPostBack="True">
                                                </telerik:RadDropDownList>
                                                &nbsp;

                            <telerik:RadDropDownList ID="rddlYear" runat="server" BackColor="White" BorderColor="#d8d8d8" Width="63px" Font-Size="10px" AutoPostBack="True">
                            </telerik:RadDropDownList>
                                            </div>
                                            <div class="col-3 col-lg-3 col-md-3 col-sm-3 m-0 p-0">
                                                <asp:LinkButton ID="lbtnNxtMth" runat="server" Text="Jan" CssClass="calNxtMth" Style="display: none;"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </th>



                                <%--<th style="text-align: left;  padding-top: 0px; margin-top: 0px;">

                                    </th>

                                <th style="vertical-align:middle;">

                                    

                                </th>

                                <th style="text-align: right;">

                                    &nbsp;</th>--%>
                            </tr>

                        </table>


                        <asp:Calendar ID="CalDatepicker" runat="server" CssClass="calCss"
                            NextPrevFormat="ShortMonth" SelectionMode="Day" ShowTitle="false"
                            ShowGridLines="True" Width="100%" ShowNextPrevMonth="true">

                            <SelectedDayStyle CssClass="calDaySelected" />

                            <SelectorStyle BackColor="#d82133" />

                            <TodayDayStyle CssClass="calDayToday" />

                            <OtherMonthDayStyle CssClass="calOtherDay" />

                            <DayStyle CssClass="calDayStyle" />

                            <DayHeaderStyle CssClass="calWeekTitle" />





                        </asp:Calendar>

                    </div>



                    <div class="divBoxFrame_Remarks" style="width: 49.3%; height: 242px;">

                        <div class="divBoxFrame_Title"><span class="spanFrameTitle">Events</span></div>

                        <div class="divBoxScroll" style="width: 99%;">

                            <div class="divBoxScrollInside" style="width: 106%;">

                                <asp:Literal ID="LiteralCalendarEvents" runat="server">
                                      
                                </asp:Literal>





                            </div>



                        </div>



                    </div>





                    <div style="width: 99%;" class="divLinkBorder">
                        <%--width: 668px;--%>

                        <div class="divSeperator">

                            <div>Leaves Availed</div>



                        </div>

                        <asp:DataList ID="dlLeaveStatus" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" ShowHeader="False" CellPadding="2">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfELT_ID" runat="server" Value='<%# Eval("ELT_ID")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hfElt_BackColor" runat="server" Value='<%# Eval("ELT_BackColor")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hfElt_ForeColor" runat="server" Value='<%# Eval("ELT_ForeColor")%>'></asp:HiddenField>
                                <div class="divMainBox">
                                    <div id="divText" runat="server" class="divMainSubBox">
                                        <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("ELT_DESCR")%>'></asp:Label>
                                    </div>
                                    <divclass class="divMainBoxCenter">
                                            <%# Eval("TOT_TAKEN")%></divclass>
                                    <div class="divMainBoxFooter">
                                        <span class="spanLeftText" id="spLefttext" runat="server" visible='<%# Eval("bSHOW_DETAILS")%>'>Balance <%# Eval("TOT_BALANCE")%></span>
                                        <%--<asp:LinkButton ID="lbtnPushDetails" runat="server" Text="Copy" CssClass="spanRightLink" CommandArgument='<%# Eval("ELT_ID")%>'
                                                 visible='<%# Eval("bPREV_RECORD")%>' ToolTip="copy the last record entered for the selected leave type"></asp:LinkButton>--%>
                                    </div>


                                </div>
                            </ItemTemplate>
                        </asp:DataList>





                    </div>

                    <div style="width: 99%;" class="divLinkBorder">
                        <%--width: 668px;--%>

                        <div class="divSeperator">

                            <div>Leaves Pending for approval (*)</div>



                        </div>
                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                            CssClass="table table-row table-bordered" Width="100%"  >
                            <Columns>

                                <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR"></asp:BoundField>
                                <asp:BoundField DataField="LEAVEDAYS" HeaderText="Leave Days" ReadOnly="True" SortExpression="LEAVEDAYS"></asp:BoundField>
                                <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date From"
                                    HtmlEncode="False" SortExpression="ELA_DTFROM"></asp:BoundField>
                                <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date To"
                                    HtmlEncode="False" SortExpression="ELA_DTTO"></asp:BoundField>
                                <asp:BoundField DataField="ELA_APPRSTATUS" HeaderText="Status" ReadOnly="True" SortExpression="ELA_APPRSTATUS"></asp:BoundField>

                                <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS">
                                    <ItemTemplate>
                                        <asp:Label ID="LinkButton1" runat="server"
                                            Text='<%# Bind("ELA_REMARKS") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="APPRSTATUS" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAPPRSTATUS" runat="server" Text='<%# Bind("APPRSTATUS") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <table style="display: none;">

                        <tr>

                            <td>&nbsp;</td>

                        </tr>

                        <tr>

                            <td align="left" class="tdfields">

                                <asp:Label ID="lblCancelMsg" runat="server" ForeColor="#CC0000"></asp:Label>

                            </td>

                        </tr>

                    </table>


                    <br />




                </div>
            </div>

            <%-- </div>--%>
        </div>




    </div>
      
    <div id="divPopup" class="darkPanel" runat="server" style="width: 100%; height: 100%;" visible="false">
        <div id="divbox-panel" class="darkPanelTop" style="width: 589px; height: 186px;">
            <asp:ImageButton ID="imgclose" runat="server"
                ToolTip="click here to close" ImageUrl="~/images/common/closebox.png" />

            <asp:Label ID="lblPopupTitle" runat="server" CssClass="PopUpTitle" Text="Leave Application - Final Step" Height="20px"></asp:Label>

            <div class="holderInner" runat="server" style="width: 575px; height: 110px;">
                <div>


                    <div style="padding-top: 20px;">

                        <div class="divPassRectext" style="text-align: center">

                            <asp:Label ID="lblPopupMsg" runat="server" CssClass="remark_info" Visible="False"></asp:Label><br />

                            <asp:CheckBox ID="chkSaveAnyway" runat="server" Text="Continue anyway" Checked="False" CssClass="tdfields" Visible="false" />
                            &nbsp;&nbsp;&nbsp;
                  <asp:Button ID="btnSave" runat="server" Text="Save &amp; Send For Approval" CssClass="button" />
                            &nbsp;&nbsp;&nbsp;
                  <asp:Button ID="btnSaveDraft" runat="server" Text="Save As Draft" CssClass="button" />


                        </div>


                    </div>

                </div>
            </div>

        </div>

    </div>


</asp:Content>
