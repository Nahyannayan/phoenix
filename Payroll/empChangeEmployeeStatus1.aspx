<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empChangeEmployeeStatus1.aspx.vb" Inherits="Payroll_empChangeEmployeeStatus" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" EnableViewState="false" >
<script language="javascript" type="text/javascript">
    function getDate(mode) 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
             if (mode==2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt="+document.getElementById('<%=txtFrom.ClientID %>').value,"", sFeatures) 
            if (result=='' || result==undefined)
                {
                return false;
                }
            if (mode==2)
              document.getElementById('<%=txtFrom.ClientID %>').value=result;
              if (document.getElementById('<%=txtTodate.ClientID %>').value=='')
                    document.getElementById('<%=txtTodate.ClientID %>').value=result;   
            return true;
    } 
     function getDate2(mode) 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
             if (mode==2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt="+document.getElementById('<%=txtFrom.ClientID %>').value,"", sFeatures) 
            if (result=='' || result==undefined)
                {
                return false;
                }
            if (mode==2)
              document.getElementById('<%=txtTodate.ClientID %>').value=result; 
            return true;
    } 
    
   function GetEMPName()
       {    
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
       <%--result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN&display=STF_NO","", sFeatures)
            if(result != '' && result != undefined)
            {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1]; 
                document.getElementById('<%=txtEmpNo.ClientID %>').value=NameandCode[0]; 
                //
                return true;
            }
            return false;--%>

       var url = "../Accounts/accShowEmpDetail.aspx?id=EN&display=STF_NO";
       var oWnd = radopen(url, "pop_employee");
        } 
       

    function OnClientClose1(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {

            NameandCode = arg.NameandCode.split('||');
            document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
    }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Change Employee Status
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table align="center" width="100%">
     <tr><td  colspan="4" align="left" width="100%">
                    <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                    </td></tr></table>
 <table align="center" width="100%">            
           
             <tr>
                <td align="left" width="20%"><span class="field-label">Select employee</span></td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                        CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator></td>
                 <td class="20%"><span class="field-label">Current Status</span></td>
                 <td class="30%">
                     <asp:DropDownList ID="ddEstCode" runat="server" OnSelectedIndexChanged="ddEstCode_SelectedIndexChanged" AutoPostBack="True">
             </asp:DropDownList>             
             <asp:CheckBox ID="chkFinalSettled" runat="server" Text="Final Settlement done outside system" Visible="False"  />
                     <div id="divChkMsg" runat="server" visible="false"><br />
                         <div style="width:170px;float:left"></div><div style="float:left"><span class="text-danger"><i>(Removes employee from future leave and gratuity provisionings.)</i></span></div></div>
                 </td>
            </tr>
     
       <tr >
         <td align="left" width="20%">
             <asp:Label id="lblperiod" runat="server" Text="Select a Period" CssClass="field-label">
         </asp:Label></td>
         <td align="left" width="30%">
             <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                 TabIndex="4" />
             <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
             </td>
          <td align="left" width="20%">
             <asp:Label id="lblToDate" runat="server" CssClass="field-label" Text="Last working Date"></asp:Label>
              </td>
           <td align="left" width="30%">
             <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
             <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif"
                  TabIndex="5" />
               <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgToDate" TargetControlID="txtTodate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtTodate" TargetControlID="txtTodate">
                            </ajaxToolkit:CalendarExtender>
           </td> 
     </tr>
     <tr>
                <td align="left">
                    <span class="field-label">Remarks</span></td>
                <td align="left">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox></td>
         <td></td>
         <td></td>
     </tr>
     <tr runat="server" id="tr_Deatailhead"><td colspan="4" align="left" class="title-bg">
         Employee Missing/Suspension History</td></tr>                      
            <tr runat="server" id="tr_Deatails">  
            <td colspan="4" align="center" valign="top" width="100%">
                <asp:GridView ID="gvDetails" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data Found">
                    <Columns>
                        <asp:BoundField DataField="EST_DESCR" HeaderText="Description" InsertVisible="False" ReadOnly="True" >
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EST_DTFROM" HeaderText="From Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EST_DTTO" HeaderText="To Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EST_REMARKS" HeaderText="Remarks" >
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                     <HeaderStyle CssClass="gridheader_pop" />
                    <%--<AlternatingRowStyle CssClass="griditem_alternative"  />--%>
                    <RowStyle CssClass="griditem"  />
                    <SelectedRowStyle BackColor="Aqua" />
                </asp:GridView>
                </td>
            </tr>     
            <tr>
                <td colspan="4" align="center">
                    &nbsp;<asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
        </table>
    <asp:HiddenField ID="h_Emp_No" runat="server" /><asp:HiddenField ID="h_Cat" runat="server" /><asp:HiddenField ID="h_Dpt" runat="server" /><asp:HiddenField ID="h_Grade" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>