﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Math


Imports UtilityObj
Partial Class Payroll_empStaffListWithPhoto
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Catid As String = ""
        If Not Request.QueryString("Catid") Is Nothing Then
            Catid = Request.QueryString("Catid")
        End If

        Dim ABCCat As String = ""
        If Not Request.QueryString("ABCCat") Is Nothing Then
            ABCCat = Request.QueryString("ABCCat")
        End If
        Dim DesID As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        ' Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "exec  rptGetDependanceInsuranceDetails  '" & Session("sBsuid") & "'")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "exec  rptGetStaffDetailsWithPhoto  '" & Session("StaffBSUIds") & "','" & Session("StaffEmpIds") & "','" & Catid & "','" & ABCCat & "','" & Session("StaffDesIds") & "'")
        ' Dim GridView1 As New GridView
        'GridView1.t()
        gvExport.DataSource = ds
        gvExport.DataBind()
        'gvExport.Visible = False
        ' exportnew()
        'Excel_Export()
        'ExporttoExcel(ds, "swapna")


        'gvExport = gvExport
        'Response.ClearContent()
        'Response.AddHeader("content-disposition", "attachment; filename=test.xls")
        'Response.ContentType = "application/excel"
        'Dim sw As New System.IO.StringWriter()
        'Dim htw As New HtmlTextWriter(sw)
        'gvExport.RenderControl(htw)
        'Response.Write(sw.ToString())
        'Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        Return
    End Sub
    Protected Sub gvExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExport.RowDataBound

        'For Each drow As GridViewRow In gvExport.Rows
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lblNum As Label = CType(e.Row.FindControl("lblEmpDepID"), Label)
            Dim imgEmployee As Image = CType(e.Row.FindControl("imgEMployee"), Image)
            'Dim RbDep As Telerik.Web.UI.RadBinaryImage = CType(e.Row.FindControl("RbDep"), Telerik.Web.UI.RadBinaryImage)
            Dim imgFileName As String = "\" + "StaffList_" + Session("sBsuid") + "_" + lblNum.Text + ".jpeg"
            'Dim imgFilePath As String = Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName

            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "/" & imgEmployee.AlternateText
            'imgEmployee.ImageUrl = 
            'strImagePath = strImagePath & "?" & DateTime.Now.Ticks.ToString()


            Dim strPath As String = strImagePath ' GetPhotoPath()
            Try
                If strPath <> "" Then
                    ' System.IO.File.Copy(strPath, imgFilePath)
                End If
            Catch ex As Exception
            End Try

            'imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
            If imgEmployee.AlternateText <> "" Then


                Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                dummyCallBack = New _
                  System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)

                Dim fullSizeImg As System.Drawing.Image
                fullSizeImg = System.Drawing.Image.FromFile(strPath)

                Dim thumbNailImg As System.Drawing.Image
                thumbNailImg = fullSizeImg.GetThumbnailImage(60, 60, _
                                                       dummyCallBack, IntPtr.Zero)
                'If File.Exists(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName) Then
                '    lblerror.Text = "file exists - " + imgFileName
                '    Exit Sub
                'End If

                thumbNailImg.Save(Server.MapPath("~/Payroll/InsurancePhotos") + imgFileName, System.Drawing.Imaging.ImageFormat.Jpeg)
                'System.IO.File.Copy("~/Payroll/InsurancePhotos" + imgFileName, imgEmployee.ImageUrl)
                'imgEmployee = thumbNailImg.
                imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                'Dim imgBytes As Byte()
                'imgBytes = ConvertImageFiletoBytes(strImagePath)

                'RbDep.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                imgEmployee.Visible = True
                thumbNailImg.Dispose()
            End If
        End If

    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Private Sub Excel_Export()

        Response.Clear()

        Response.Buffer = True

        Response.AddHeader("content-disposition", _
    "attachment;filename=GridViewExport.xls")

        Response.Charset = ""

        Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()

        Dim hw As New HtmlTextWriter(sw)

        gvExport.AllowPaging = False

        gvExport.DataBind()

        For i As Integer = 0 To gvExport.Rows.Count - 1

            Dim row As GridViewRow = gvExport.Rows(i)

            'Apply text style to each Row

            row.Attributes.Add("class", "textmode")

        Next

        gvExport.RenderControl(hw)



        'style to format numbers to string

        Dim style As String = "<style> .textmode " & "{ mso-number-format:\@; } </style>"

        Response.Write(style)

        Response.Output.Write(sw.ToString())

        Response.Flush()

        Response.End()

    End Sub
    Protected Sub lnkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExport.Click
        Excel_Export()
    End Sub
End Class
