Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO
Imports UtilityObj
Partial Class empDependantsDetail
    Inherits System.Web.UI.Page
    Dim userSuper As Boolean
    Dim Encr_decrData As New Encryption64
    Dim sqlReader As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If Session("sUsr_name") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If
            If ViewState("MainMnu_code") <> "U000064" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
            hlAddNew.NavigateUrl = "empDependantAdd.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
            BindGrids()
        End If
    End Sub
    Private Sub BindGrids()
        Try
            ViewState("EDD_EMP_ID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            Dim mTable As New DataTable
            mTable = EOS_EmployeeDependant.GetDependantDetailList(ViewState("EDD_EMP_ID"))
            dlDependant.DataSource = mTable
            dlDependant.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    
    Private Sub setImageDisplay(ByVal imgMap As Object, ByVal PhotoBytes As Byte(), Optional ByVal ID As String = "")
        Try
            Dim OC As System.Drawing.Image = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            If Not PhotoBytes Is Nothing Then
                ViewState("imgDependant") = PhotoBytes
                OC = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            Else
                OC = System.Drawing.Image.FromFile(WebConfigurationManager.AppSettings.Item("NoImagePath"))
            End If
            If OC Is Nothing Then Exit Sub
            Dim nameOfImage As String = Session.SessionID & Replace(Now.ToString, ":", "") & ID & "CurrentImage"
            If Not Cache.Get(nameOfImage) Is Nothing Then
                Cache.Remove(nameOfImage)
            End If
            Cache.Insert(nameOfImage, OC, Nothing, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Default, Nothing)
            imgMap.ImageUrl = "empOrgChartImage.aspx?ID=" & nameOfImage
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub dlDependant_ItemDataBound1(sender As Object, e As RepeaterItemEventArgs) Handles dlDependant.ItemDataBound
        Try
            Dim hdnEDDID As New HiddenField
            hdnEDDID = e.Item.FindControl("hdnEDDID")
            If Not hdnEDDID Is Nothing Then

            End If
            Dim lblDepDOB As New Label
            lblDepDOB = e.Item.FindControl("lblDepDOB")
            If Not lblDepDOB Is Nothing Then
                lblDepDOB.Text = Format(CDate(lblDepDOB.Text), OASISConstants.DateFormat)
            End If
            'Dim hdnEDDPHOTO As New HiddenField
            'hdnEDDPHOTO = e.Item.FindControl("hdnEDDPHOTO")
            Dim imgDependant As New ImageButton
            imgDependant = e.Item.FindControl("imgDependant")
            Dim hlView As New HyperLink
            hlView = e.Item.FindControl("hlView")
            Dim NavigateURL As String
            ' NavigateURL = "~/Payroll/empProfileView.aspx?ProfileID=" & Encr_decrData.Encrypt(hdnEDDID.Value) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
            NavigateURL = "~/Payroll/empDependantAdd.aspx?viewid=" & Encr_decrData.Encrypt(hdnEDDID.Value) & "&MainMnu_code=" & Encr_decrData.Encrypt("U000064") & "&datamode=" & Encr_decrData.Encrypt("edit")

            If Not hlView Is Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlView.NavigateUrl = NavigateURL
            End If
            Dim hlDocs As New HyperLink
            hlDocs = e.Item.FindControl("hlDocs")
            Dim hdnEDDDocCount As New HiddenField
            hdnEDDDocCount = e.Item.FindControl("hdnEDDDocCount")
            If Not hlDocs Is Nothing Then
                hlDocs.Text = "Documents(" & hdnEDDDocCount.Value & ")"
                hlDocs.NavigateUrl = "~/Payroll/empDocumentDetail.aspx" & "?ProfileID=" & Encr_decrData.Encrypt(hdnEDDID.Value) & "&MainMnu_code=" & Encr_decrData.Encrypt("U000062") & "&datamode=" & Encr_decrData.Encrypt("view") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
            End If
            If Not imgDependant Is Nothing Then
                imgDependant.ImageUrl = "~/Payroll/ImageHandler.ashx?ID=" + hdnEDDID.Value.ToString & "&TYPE=EDD"
                imgDependant.PostBackUrl = NavigateURL
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = "Unexpected Error !!!"
        End Try
    End Sub

    Protected Sub lnkbackbtn_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("../ESSDashboard.aspx")
        Response.Redirect(URL, False)
    End Sub
End Class
