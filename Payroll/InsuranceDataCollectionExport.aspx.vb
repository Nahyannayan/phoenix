﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Imports GemBox.Spreadsheet
Partial Class Payroll_InsuranceDataCollectionExport
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session.Timeout = 5
            'HiddenPostBack.Value = 1
            Session("Data") = Nothing

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") = Nothing Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If

            BindBsu()

            'Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
            '    While DefReader.Read
            '        Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))


            '    End While
            'End Using

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadExcel)
    End Sub
    Protected Sub btnDownload_Click(sender As Object, e As EventArgs)
        DownloadStaffList()
        ''UpoadEmpPhoto()
    End Sub

    Protected Sub btnDownloadExcel_Click(sender As Object, e As EventArgs)
        DownloadExcelList()
        ''UpoadEmpPhoto()
    End Sub

    Public Sub BindBsu()
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.StoredProcedure, "[dbo].[GetUserBusinessUnits]", pParms)
        ddbsu.DataTextField = "Bsu_Name"
        ddbsu.DataValueField = "BSU_ID"
        ddbsu.DataSource = ds.Tables(0)
        ddbsu.DataBind()
        Dim item As New ListItem("[Select]", "-1")
        ddbsu.Items.Insert(0, item)
        ddbsu.SelectedIndex = 0
    End Sub
    'Public Sub UpoadEmpPhoto()
    '    Dim SourcePhysicalPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString

    '    SourcePhysicalPath = SourcePhysicalPath & "\999998\photo\"
    '    Dim destphysicalpath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\999998\EMPPHOTO\"

    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim ds As DataSet
    '    Dim pParms(0) As SqlClient.SqlParameter
    '    pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
    '    Try
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_EMPLOYEE_MASTERNAHYAN", pParms)
    '        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
    '            For Each row As DataRow In ds.Tables(0).Rows
    '                row.Item("EMP_PHOTO") = SourcePhysicalPath & row.Item("EMP_PHOTO").ToString.Replace("/", "\")

    '                If File.Exists(SourcePhysicalPath & "/" & row.Item("EMPNO") & ".JPG") Then

    '                    'If IO.Directory.Exists(destphysicalpath & "\" & row.Item("EMP_ID")) Then

    '                    '    IO.Directory.CreateDirectory(destphysicalpath & "\" & row.Item("EMP_ID"))
    '                    'End If
    '                    IO.Directory.CreateDirectory(destphysicalpath & "\" & row.Item("EMP_ID"))
    '                    File.Copy(SourcePhysicalPath & "/" & row.Item("EMPNO") & ".JPG", destphysicalpath & "\" & row.Item("EMP_ID") & "\EMPPHOTO.jpg", True)
    '                End If
    '            Next
    '        End If

    '    Catch ex As Exception

    '    End Try


    '        'Dim DestinationPhysicalPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue
    '        'Dim DestinationPhotoPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue & "\Photos"
    'End Sub
    Public Sub DownloadStaffList()

        Me.lblmessage.Text = Nothing

        'Dim SourcePhysicalPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto"
        'Dim SourcePhysicalPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        'Dim DestinationPhysicalPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue
        'Dim DestinationPhotoPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue & "\Photos"
        Dim destphysicafilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder")
        Dim destfilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "PHOTO_S\" & Me.ddbsu.SelectedValue

        ''First delete the Bsu_shortname folder if it already exists. Then create the Bsu_shortname folder
        If IO.Directory.Exists(destfilepath) Then
            IO.Directory.Delete(destfilepath, True)
        End If
        IO.Directory.CreateDirectory(destfilepath)

        'If IO.Directory.Exists(DestinationPhysicalPath) Then
        '    If IO.Directory.Exists(DestinationPhotoPath) Then
        '        IO.Directory.Delete(DestinationPhotoPath, True)
        '    End If
        '    IO.Directory.CreateDirectory(DestinationPhotoPath)
        'End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetInsuranceDataCollection", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                'For Each row As DataRow In ds.Tables(0).Rows
                '    row.Item("EMP_PHOTO") = SourcePhysicalPath & row.Item("EMP_PHOTO").ToString.Replace("/", "\")
                'Next
                ds.AcceptChanges()
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet
                Dim excelDt As New DataTable

                Dim ds1 As New DataSet
                excelDt = ds.Tables(0)
                ds1.Tables.Add(ds.Tables(0).Copy())

                excelDt.Columns.Remove("EDD_Photo")
                excelDt.Columns.Remove("EMP_Photo")
                ef.Worksheets.Add("EMP LIST")
                ws = ef.Worksheets(0)
                ws.InsertDataTable(excelDt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                'AutoFit column widths
                Dim columnCount = ws.CalculateMaxUsedColumns()
                For i As Integer = 0 To columnCount - 1
                    'ws.Columns(i).AutoFitAdvanced(1, ws.Rows(1), ws.Rows(ws.Rows.Count - 1))
                    ws.Columns(i).AutoFit(1, ws.Rows(1), ws.Rows(ws.Rows.Count - 1))
                Next
                'Set header rows style
                Dim ExcelHeaderCellStyle As New GemBox.Spreadsheet.CellStyle()
                ExcelHeaderCellStyle.WrapText = False
                ExcelHeaderCellStyle.ShrinkToFit = False
                ExcelHeaderCellStyle.Font.Weight = ExcelFont.BoldWeight
                ExcelHeaderCellStyle.Font.Color = Drawing.Color.Blue
                'ws.Cells(0, 0).Style = ExcelHeaderCellStyle
                'ws.Cells(0, 1).Style = ExcelHeaderCellStyle

                'ws.Cells.Style = ExcelHeaderCellStyle

                ws.Cells(0, 0).Value = "Sl No"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 2000

                ws.Cells(0, 1).Value = "Employee No"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "Full Name"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 7000

                ws.Cells(0, 3).Value = "First Name"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 5000

                ws.Cells(0, 4).Value = "Second Name"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 5000

                ws.Cells(0, 5).Value = "Family Name"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "Contact No"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 5000

                ws.Cells(0, 7).Value = "DOB"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("G").Width = 5000

                ws.Cells(0, 8).Value = "Occupation / Designation"
                ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("H").Width = 7000

                ws.Cells(0, 9).Value = "Gender"
                ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("I").Width = 5000

                ws.Cells(0, 10).Value = "Nationality"
                ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("J").Width = 7000

                ws.Cells(0, 11).Value = "Passport No"
                ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("K").Width = 5000

                ws.Cells(0, 12).Value = "Marital Status"
                ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("L").Width = 5000


                ws.Cells(0, 13).Value = "Email"
                ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("M").Width = 7000

                ws.Cells(0, 14).Value = "Emirate"
                ws.Cells(0, 14).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 14).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("N").Width = 5000

                ws.Cells(0, 15).Value = "Residential Location"
                ws.Cells(0, 15).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 15).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("O").Width = 7000

                ws.Cells(0, 16).Value = "Work Location"
                ws.Cells(0, 16).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 16).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("P").Width = 5000

                ws.Cells(0, 17).Value = "Salary Band"
                ws.Cells(0, 17).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 17).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Q").Width = 5000


                ws.Cells(0, 18).Value = "Commission"
                ws.Cells(0, 18).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 18).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("R").Width = 7000

                ws.Cells(0, 19).Value = "Emirate ID No / EID application number"
                ws.Cells(0, 19).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 19).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("S").Width = 5000

                ws.Cells(0, 20).Value = "UID No"
                ws.Cells(0, 20).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 20).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("T").Width = 5000


                ws.Cells(0, 21).Value = "Relation"
                ws.Cells(0, 21).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 21).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("U").Width = 5000

                ws.Cells(0, 22).Value = "Entity Type"
                ws.Cells(0, 22).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 22).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("V").Width = 5000


                ws.Cells(0, 23).Value = "Insurance Category"
                ws.Cells(0, 23).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 23).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("W").Width = 4000

                ws.Cells(0, 24).Value = "Visa Sponsorship Details"
                ws.Cells(0, 24).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 24).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("X").Width = 5000

                ws.Cells(0, 25).Value = "Existing medical card number"
                ws.Cells(0, 25).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 25).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Y").Width = 5000

                ws.Cells(0, 26).Value = "Category"
                ws.Cells(0, 26).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 26).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Z").Width = 5000

                ws.Cells(0, 27).Value = "Staff Level"
                ws.Cells(0, 27).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 27).Style.FillPattern.SetSolid(Drawing.Color.Black)

                'new change for adding visa file number
                ws.Cells(0, 28).Value = "Visa file number"
                ws.Cells(0, 28).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 28).Style.FillPattern.SetSolid(Drawing.Color.Black)
                '   ws.Columns("Y").Width = 5000
                ''removing bsu shortname column
                ws.Columns.Remove(29)

                'Delete the 4th column having to EMD_Photo values as it is not required in the excel file
                'For i As Integer = 0 To ef.Worksheets(0).Rows.Count - 1
                '    ws.Cells(i, 3).Value = ""
                'Next

                Dim Filename As String = excelDt.Rows(0)("Bsu_shortname").ToString().Trim(" ") & "_EMP_LIST" & ".xlsx"
                ef.Save(destfilepath & "\" & Filename)
                ef = Nothing
                Dim destPathEmp As String = String.Empty

                'Now copy each employee photo to the destination photo path
                For Each row As DataRow In ds1.Tables(0).Rows

                    If IO.Directory.Exists(destfilepath) Then
                        'If IO.Directory.Exists(DestinationPhotoPath) Then
                        '    IO.Directory.Delete(DestinationPhotoPath, True)
                        'End If
                        '' destPathEmp = destfilepath & "\PHOTO\" & row.Item("Staff ID")
                        destPathEmp = destfilepath & "\PHOTO\"
                        IO.Directory.CreateDirectory(destPathEmp)
                    End If
                    If File.Exists(WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "/" & row.Item("EMP_PHOTO")) Then
                        File.Copy(WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "/" & row.Item("EMP_PHOTO"), destPathEmp & "\" & row.Item("Staff ID") & "_" & row.Item("Member Name").ToString & ".jpg", True)
                    End If


                    If (row.Item("EDD_PHOTO").ToString.Length > 2) Then
                        Try
                            Dim OC As System.Drawing.Image '= EOS_MainClass.ConvertBytesToImage(PhotoBytes)
                            OC = EOS_MainClass.ConvertBytesToImage(row.Item("EDD_PHOTO"))

                            Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                            dummyCallBack = New  _
                              System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                            Dim fullSizeImg As System.Drawing.Image
                            fullSizeImg = OC
                            If OC IsNot Nothing Then
                                Dim thumbNailImg As System.Drawing.Image
                                thumbNailImg = fullSizeImg.GetThumbnailImage(80, 80, _
                                                                       dummyCallBack, IntPtr.Zero)
                                thumbNailImg.Save(destPathEmp & "\" & row.Item("Staff ID").ToString & "_" & row.Item("Member Name").ToString & ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg)

                            End If
                            'imgEmployee.ImageUrl = "~/Payroll/InsurancePhotos" + imgFileName
                            'imgEmployee.Visible = True
                        Catch ex As Exception
                            UtilityObj.Errorlog("ins rowdatabound", ex.Message)
                        End Try
                    End If

                Next

                'Now Zip the entire Bsu_shortname folder
                ''destfilepath
                If File.Exists(destphysicafilepath & ddbsu.SelectedValue & ".zip") Then
                    File.Delete(destphysicafilepath & ddbsu.SelectedValue & ".zip")
                End If
                Dim zip As New Zip.FastZip
                zip.CreateZip(destphysicafilepath & ddbsu.SelectedValue & ".zip", destfilepath, True, Nothing)

                'Download the created zip file
                Dim Content() As Byte = File.ReadAllBytes(destphysicafilepath & ddbsu.SelectedValue & ".zip")
                Response.ContentType = "application/zip"
                Response.AddHeader("content-disposition", "attachment; filename=" + ddbsu.SelectedValue & ".zip")
                Response.BufferOutput = True
                Response.OutputStream.Write(Content, 0, Content.Length)
                Response.Flush()
                Response.End()

            End If
        Catch ex As Exception
            Me.lblmessage.Text = "Error downloading Staff list"
        End Try
        Exit Sub

    End Sub

    Function ThumbnailCallback() As Boolean
        Return False
    End Function

    Public Sub DownloadExcelList()

        Me.lblmessage.Text = Nothing

        'Dim SourcePhysicalPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto"
        'Dim SourcePhysicalPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        'Dim DestinationPhysicalPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue
        'Dim DestinationPhotoPath As String = SourcePhysicalPath & "\" & Me.ddbsu.SelectedValue & "\Photos"
        Dim destphysicafilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder")
        Dim destfilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "PHOTO_S\" & Me.ddbsu.SelectedValue

        ''First delete the Bsu_shortname folder if it already exists. Then create the Bsu_shortname folder
        If IO.Directory.Exists(destfilepath) Then
            IO.Directory.Delete(destfilepath, True)
        End If
        IO.Directory.CreateDirectory(destfilepath)

        'If IO.Directory.Exists(DestinationPhysicalPath) Then
        '    If IO.Directory.Exists(DestinationPhotoPath) Then
        '        IO.Directory.Delete(DestinationPhotoPath, True)
        '    End If
        '    IO.Directory.CreateDirectory(DestinationPhotoPath)
        'End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetInsuranceDataCollection", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                'For Each row As DataRow In ds.Tables(0).Rows
                '    row.Item("EMP_PHOTO") = SourcePhysicalPath & row.Item("EMP_PHOTO").ToString.Replace("/", "\")
                'Next
                ds.AcceptChanges()
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet
                Dim excelDt As New DataTable

                Dim ds1 As New DataSet
                excelDt = ds.Tables(0)
                ds1.Tables.Add(ds.Tables(0).Copy())

                excelDt.Columns.Remove("EDD_Photo")
                excelDt.Columns.Remove("EMP_Photo")
                ef.Worksheets.Add("EMP LIST")
                ws = ef.Worksheets(0)
                ws.InsertDataTable(excelDt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                'AutoFit column widths
                Dim columnCount = ws.CalculateMaxUsedColumns()
                For i As Integer = 0 To columnCount - 1
                    'ws.Columns(i).AutoFitAdvanced(1, ws.Rows(1), ws.Rows(ws.Rows.Count - 1))
                    ws.Columns(i).AutoFit(1, ws.Rows(1), ws.Rows(ws.Rows.Count - 1))
                Next
                'Set header rows style
                Dim ExcelHeaderCellStyle As New GemBox.Spreadsheet.CellStyle()
                ExcelHeaderCellStyle.WrapText = False
                ExcelHeaderCellStyle.ShrinkToFit = False
                ExcelHeaderCellStyle.Font.Weight = ExcelFont.BoldWeight
                ExcelHeaderCellStyle.Font.Color = Drawing.Color.Blue
                'ws.Cells(0, 0).Style = ExcelHeaderCellStyle
                'ws.Cells(0, 1).Style = ExcelHeaderCellStyle

                'ws.Cells.Style = ExcelHeaderCellStyle

                ws.Cells(0, 0).Value = "Sl No"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 2000

                ws.Cells(0, 1).Value = "Employee No"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "Full Name"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 7000

                ws.Cells(0, 3).Value = "First Name"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 5000

                ws.Cells(0, 4).Value = "Second Name"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 5000

                ws.Cells(0, 5).Value = "Family Name"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "Contact No"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 5000

                ws.Cells(0, 7).Value = "DOB"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("G").Width = 5000

                ws.Cells(0, 8).Value = "Occupation / Designation"
                ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("H").Width = 7000

                ws.Cells(0, 9).Value = "Gender"
                ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("I").Width = 5000

                ws.Cells(0, 10).Value = "Nationality"
                ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("J").Width = 7000

                ws.Cells(0, 11).Value = "Passport No"
                ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("K").Width = 5000

                ws.Cells(0, 12).Value = "Marital Status"
                ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("L").Width = 5000


                ws.Cells(0, 13).Value = "Email"
                ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("M").Width = 7000

                ws.Cells(0, 14).Value = "Emirate"
                ws.Cells(0, 14).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 14).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("N").Width = 5000

                ws.Cells(0, 15).Value = "Residential Location"
                ws.Cells(0, 15).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 15).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("O").Width = 7000

                ws.Cells(0, 16).Value = "Work Location"
                ws.Cells(0, 16).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 16).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("P").Width = 5000

                ws.Cells(0, 17).Value = "Salary Band"
                ws.Cells(0, 17).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 17).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Q").Width = 5000


                ws.Cells(0, 18).Value = "Commission"
                ws.Cells(0, 18).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 18).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("R").Width = 7000

                ws.Cells(0, 19).Value = "Emirate ID No / EID application number"
                ws.Cells(0, 19).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 19).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("S").Width = 5000

                ws.Cells(0, 20).Value = "UID No"
                ws.Cells(0, 20).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 20).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("T").Width = 5000


                ws.Cells(0, 21).Value = "Relation"
                ws.Cells(0, 21).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 21).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("U").Width = 5000

                ws.Cells(0, 22).Value = "Entity Type"
                ws.Cells(0, 22).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 22).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("V").Width = 5000


                ws.Cells(0, 23).Value = "Insurance Category"
                ws.Cells(0, 23).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 23).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("W").Width = 4000

                ws.Cells(0, 24).Value = "Visa Sponsorship Details"
                ws.Cells(0, 24).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 24).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("X").Width = 5000

                ws.Cells(0, 25).Value = "Existing medical card number"
                ws.Cells(0, 25).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 25).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Y").Width = 5000

                ws.Cells(0, 26).Value = "Category"
                ws.Cells(0, 26).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 26).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Z").Width = 5000

                ws.Cells(0, 27).Value = "Staff Level"
                ws.Cells(0, 27).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 27).Style.FillPattern.SetSolid(Drawing.Color.Black)

                ws.Cells(0, 28).Value = "Visa file number"
                ws.Cells(0, 28).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 28).Style.FillPattern.SetSolid(Drawing.Color.Black)
                '   ws.Columns("Y").Width = 5000
                ''removing bsu shortname column
                ws.Columns.Remove(29)

                'Delete the 4th column having to EMD_Photo values as it is not required in the excel file
                'For i As Integer = 0 To ef.Worksheets(0).Rows.Count - 1
                '    ws.Cells(i, 3).Value = ""
                'Next

                Dim Filename As String = excelDt.Rows(0)("Bsu_shortname").ToString().Trim(" ") & "_EMP_LIST" & ".xlsx"
                ef.Save(destphysicafilepath & "\" & Filename)


                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Filename)
               

                Dim bytes() As Byte = File.ReadAllBytes(destphysicafilepath & "\" & Filename)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(destphysicafilepath & "\" & Filename))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

            End If
        Catch ex As Exception
            Me.lblmessage.Text = "Please try again.."
        End Try
        Exit Sub

    End Sub

   
End Class
