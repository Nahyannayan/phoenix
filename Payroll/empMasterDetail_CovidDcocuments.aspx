﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empMasterDetail_CovidDcocuments.aspx.vb" Inherits="Payroll_empMasterDetail_CovidDcocuments" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">


        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }
        window.onload = scroll_page;
    </script>
    <style>
        .MyCalendar {
            z-index: 9999;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Employee COVID 19 Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">

                    <tr>
                        <td align="center" valign="top" width="100%">
                            <asp:GridView ID="gvEMPDETAILS" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" AllowPaging="True" SkinID="GridViewView" PageSize="50">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <HeaderTemplate>
                                            EMPLOYEE No.
                                            <br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            EMPLOYEE NAME
                                            <br />
                                            <asp:TextBox ID="txtEmpName" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP DOJ">
                                        <HeaderTemplate>
                                            D.O. Join
                                            <br />
                                            <asp:TextBox ID="txtDOJ" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Bind("EMP_JOINDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="VHH_DOCDT" HeaderText="EMP PASSPORT NO">
                                        <HeaderTemplate>
                                            Passport No
                                            <br />
                                            <asp:TextBox ID="txtPassportNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="btnSearchName_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("EMP_PASSPORT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="EMP DESIGNATION">
                                        <HeaderTemplate>
                                            CATEGORY
                                            <br />
                                            <asp:TextBox ID="txtDesignation" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("EMP_DES_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DO. COVID">
                                        <HeaderTemplate>
                                            D.O. COVID Test                                            
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCovidDate" runat="server" Text='<%# Bind("Test_Done_On") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderTemplate>
                                            COVID Result 
                                           <%-- <br />
                                            <asp:TextBox ID="txtStatus" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearchs" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label61" runat="server" Text='<%# Bind("Result") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Upload" ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">Upload</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="GUID" Visible="False" HeaderText="GUID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="imgDoc" runat="server" ImageUrl="../Images/ViewDoc.png" Visible="false" ToolTip="Click To View Document">View</asp:HyperLink>
                                            <asp:HiddenField runat="server" ID="hfCOVDOC_ID" Value='<%# Eval("DOC_Id")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <a id='detail'></a>
                            <br />
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="ModalPopup_holder">
        <asp:Panel ID="AddNewTaskPopup" runat="server" Style="display: none; width: 100%" CssClass="ModalPopup_Container">
            <div class="ModalPopupExtender action-modal show" id="addNewDocumentModal" tabindex="-1" role="dialog"
                aria-labelledby="addNewDocumentModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header student-profile-modal-header" style="background: rgba(0,0,0,0.04) !important; z-index: 10;">
                            <h5 class="modal-title popupTitle" id="popupTitle">Add COVID 19 Test Details</h5>
                            <button type="button" class="close theme-color" onclick="HideModalPopup('ModalPopupExtender2')">
                                <span class="close_style" aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body ptb20 employee-document-form">
                            <div class="row">
                                <div class="col-md-12 m-b-5" style="padding-bottom: 12px;"><span id="CovidError" class=""></span></div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Date of COVID Test<span class="text-danger">*</span></label>

                                        <%--<input class="form-control datetimepicker1 txtDOCTestDate" type="text" value="" id="txtDOCTestDate" autocomplete="off">--%>
                                        <asp:TextBox ID="txtDOCTestDate" runat="server" CssClass="txtDOCTestDate" Style="border: 1px solid #dee2da!important; box-shadow: 1px 2px 5px rgba(0,0,0,0.1); background-color: #fff !important; padding: 6px;"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" Style="border-radius: 3px !important" />
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton1" TargetControlID="txtDOCTestDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtDOCTestDate" TargetControlID="txtDOCTestDate">
                                        </ajaxToolkit:CalendarExtender>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Test Result<span class="text-danger">*</span></label>
                                        <select class="select form-control slctAddCovidTestResult" id="slctAddCovidTestResult">
                                            <option selected="selected" value="0">--Select--</option>
                                            <option value="NEGATIVE">NEGATIVE</option>
                                            <option value="POSITIVE">POSITIVE</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <label>COVID Test Upload <span class="text-muted" style="font-size: xx-small !important;"><i>Note:File of size max 10 MB</i></span></label>

                                    <div class="form-group input-group">
                                        <input class="upload" type="file" id="fluplodCovidTest" />
                                    </div>
                                    <!-- form-group// -->
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 m-t-5">
                                    <input id="btnAddCovidTest" type="button" class="btn btn-primary" value="Save" onclick="SaveCOVIDData(0);" />
                                    <a onclick="Close_CovidPopUp();" class="btn btn-warning">Close</a>
                                </div>
                            </div>

                        </div>
                        <%-- <div class="modal-footer d-none">

                                    <asp:Button ID="btnDocCancel" runat="server" CssClass="button" Text="Cancel" OnClientClick="closeModal()" />
                                </div>--%>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="Button1"
            PopupControlID="AddNewTaskPopup" DropShadow="true" Enabled="true" BehaviorID="ModalPopupExtender2"
            BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>
    </div>
    <asp:Button Text="text" ID="Button1" CssClass="d-none" runat="server" />
    <input type="hidden" id="h_file_path" />
    <input type="text" id="file_path" style="display: none;" />
    <input type="hidden" id="file_ext" />
    <input type="hidden" id="hfEmpId" class="hfEmpId" />
    <script type="text/javascript">
        function OpenCovidPopup(EmpID) {
            $(".hfEmpId").val(EmpID);
            $find('ModalPopupExtender2').show();
        }
        function ShowModalPopup(id) {
            $find(id).show();
            return false;
        }

         function Close_CovidPopUp() {
           $find('ModalPopupExtender2').hide();
        }
        function HideModalPopup(id) {
            //$('.txtDOCTestDate').val("");            //$('#CovidError').text("");
            //$('#CovidError').attr("class", "");
            //$('#btnAddCovidTest').css("display", "block");
            //$('#h_Action').val('');
            //$('#file_path').val("");
            //$('#file_ext').val("");
            //$('#h_file_path').val("");            //$('select option[value="0"]').attr("selected",true            $find(id).hide();
            return false;
        }

        //added vikranth on Aug 25th 2020 for Covid test
        $(document).on("change", "#fluplodCovidTest", function () {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            convertToBase64Covid();
        })
        //$('#fluplodCovidTest').change(function (event) {
        //    alert("Hi")
        //    var tmppath = URL.createObjectURL(event.target.files[0]);
        //    convertToBase64Covid();
        //    //   $('#h_file_path').val(tmppath);
        //});

        function convertToBase64Covid() {
            //Read File
            var selectedFile = document.getElementById('fluplodCovidTest').files;
            var filename = document.getElementById('fluplodCovidTest').value;

            //Check File is not Empty
            if (selectedFile.length > 0) {
                var file_ext = filename.substr(filename.lastIndexOf('.'), filename.length);
                var filename1 = filename.substr(filename.lastIndexOf('\\') + 1, filename.length);

                $('#file_ext').val(file_ext);
                $('#h_file_path').val(filename1);
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                var base64;
                var returnstring;
                // Onload of file read the file content
                fileReader.onload = function (fileLoadedEvent) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    console.log(base64);
                    $('#file_path').val(base64);
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);

            }
        }

        function SaveCOVIDData(id) {
            var COV_TEST_DATE, slct;
            var e = document.getElementById('slctAddCovidTestResult');

            if ($('.txtDOCTestDate').val() == null || $('.txtDOCTestDate').val() == "") {
                COV_TEST_DATE = "0";
                $('#CovidError').text("Please select date of  COVID test");
                $('#CovidError').attr("class", "alert alert-info");
                return;
            }
            else {
                COV_TEST_DATE = $('.txtDOCTestDate').val();
            }

            if (e.options[e.selectedIndex].value == "0") {
                slct = "0";
                $('#CovidError').text("Please select the test result");
                $('#CovidError').attr("class", "alert alert-info");
                return;
            } else {
                slct = e.options[e.selectedIndex].value;
            }


            //if ($('#file_path').val() == "") {
            //    $('#CovidError').text("Please upload the file"); 
            //    $('#CovidError').attr("class", "alert alert-info");
            //    return;
            //}


            var svObj = {};
            svObj.bsuid = '<%= Session("sbsuid")%>';
            svObj.COV_TEST_DATE = COV_TEST_DATE;
            svObj.COV_RESULT = slct;
            svObj.usrId = $(".hfEmpId").val();
            svObj.filepath = $('#file_path').val();
            svObj.fileext = $('#file_ext').val();
            svObj.filename = $('#h_file_path').val();

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Payroll/empMasterDetail_CovidDcocuments.aspx/SaveCOVIDData") %>',
                //data: '{ ELAID: "' + id + '", bsuid: "' + '<%= Session("sbsuid")%>' + '", fromdate: "' + fromdate + '", todate: "' + todate + '",  leavetype: "' + slct + '", usrname: "' + '<%= Session("sUsr_name")%>' + '", remarks: "' + remarks + '", address: "' + address + '", mobile: "' + mobile + '",  handover: "' + handover + '", filepath: "' + $('#file_path').val() + '", fileext: "' + $('#file_ext').val() + '", filename: "' + $('#h_file_path').val() + '"}',
                data: JSON.stringify(svObj),
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] == '0') {
                        $('#CovidError').text(a[2]);
                        $('#CovidError').attr("class", "alert alert-info");
                        if (id == 0) {
                            $('#btnAddCovidTest').css("display", "none");
                        }
                        $('#h_Action').val('1');
                        $('#file_path').val("");
                        $('#file_ext').val("");
                        $('#h_file_path').val("");
                        window.location.href = "empMasterDetail_CovidDcocuments.aspx?MainMnu_code=BpVJOUMMS2o=&datamode=Zo4HhpVNpXc="
                    } else {
                        //  alert(a[2]);
                        $('#CovidError').text(a[2]);
                        $('#CovidError').attr("class", "alert alert-info");
                        $('#h_Action').val('1');
                    }

                }
            });
        }
    </script>
</asp:Content>

