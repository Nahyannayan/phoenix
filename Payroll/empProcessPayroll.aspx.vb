Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empProcessPayroll
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            rblCategory.DataBind()
            rblCategory.SelectedIndex = 0
            Session("EMP_SEL_COND") = " AND EMP_BSU_ID='" & Session("sBsuid") & "' " _
            & " AND (EMP_bACTIVE = 1 or (month(isnull(EMP_LASTATTDT,getdate()))= month(getdate()) and year( isnull(EMP_LASTATTDT,getdate()))=year(getdate()))) " _
            & " AND EMP_ECT_ID = " & rblCategory.SelectedValue
            gvEMPName.Attributes.Add("bordercolor", "#1b80b6")
            Call Populate_MonthList(Now.Date)
            Call Populate_YearList(Now.Date)
            bind_BusinessUnit()
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130165" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            set_Comboanddate()
            fill_Emp_for_BSU()
        Else
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Sub fill_Emp_for_BSU()
        h_EMPID.Value = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select EMP_ID as ID from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND")
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            If h_EMPID.Value = "" Then
                h_EMPID.Value = dr(0)
            Else
                h_EMPID.Value = h_EMPID.Value & "||" & dr(0)
            End If
        End While
        FillEmpNames(h_EMPID.Value)
    End Sub

    Sub Populate_MonthList(ByVal p_seldt As Date)
        drpCalMonth.Items.Add(New ListItem("January", 1))
        drpCalMonth.Items.Add(New ListItem("February", 2))
        drpCalMonth.Items.Add(New ListItem("March", 3))
        drpCalMonth.Items.Add(New ListItem("April", 4))
        drpCalMonth.Items.Add(New ListItem("May", 5))
        drpCalMonth.Items.Add(New ListItem("June", 6))
        drpCalMonth.Items.Add(New ListItem("July", 7))
        drpCalMonth.Items.Add(New ListItem("August", 8))
        drpCalMonth.Items.Add(New ListItem("September", 9))
        drpCalMonth.Items.Add(New ListItem("October", 10))
        drpCalMonth.Items.Add(New ListItem("November", 11))
        drpCalMonth.Items.Add(New ListItem("December", 12))
    End Sub

    Sub Populate_YearList(ByVal p_seldt As Date)
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 1 To DateTime.Now.Year + 20
            drpCalYear.Items.Add(intYear)
        Next
    End Sub

    Sub bind_BusinessUnit()
        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")
        Dim buser_id As String = Session("sBsuid")
        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else
            Try
                'if user is super admin give access to all the Business Unit
                If tblbUSuper = True Then
                    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        'create a list item to bind records from reader to dropdownlist ddlBunit
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        'check if it return rows or not
                        If BUnitreaderSuper.HasRows = True Then
                            While BUnitreaderSuper.Read
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBUnit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                Else
                    'if user is not super admin get access to the selected  Business Unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        Dim di As ListItem
                        ddlBUnit.Items.Clear()
                        If BUnitreader.HasRows = True Then
                            While BUnitreader.Read
                                di = New ListItem(BUnitreader(2), BUnitreader(0))
                                ddlBUnit.Items.Add(di)
                                For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBUnit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBUnit.SelectedIndex = -1 Then
                        ddlBUnit.SelectedIndex = 0
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        ' ***************** swapna changing code to loop-- commented code ****************
        'Dim streMPIDS As String = UtilityObj.GenerateXML(h_EMPID.Value, XMLType.EMPName)
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim objConn As New SqlConnection(str_conn)
        'objConn.Open()
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        'Try
        '    Dim ela_id As String
        '    Dim edit_bool As Boolean
        '    If ViewState("datamode") = "edit" Then
        '        edit_bool = True
        '        ela_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
        '    Else
        '        edit_bool = False
        '        ela_id = 0
        '    End If
        '    Dim retval As String = "1000"

        'retval = PayrollFunctions.ProcessSalaryWithVaccation(ddlBUnit.SelectedItem.Value, drpCalMonth.SelectedItem.Value, _
        '           drpCalYear.SelectedItem.Value, streMPIDS, objConn, stTrans)
        '******************************************

        Dim IDs As String() = h_EMPID.Value.Split("||")
        Dim condition As String = String.Empty
        Dim retval As String = "0"
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString


        For i As Integer = 0 To IDs.Length - 1
            If IDs(i) <> "" Then

                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction

                Dim streMPIDS As String = UtilityObj.GenerateXML(IDs(i), XMLType.EMPName)


                Dim ela_id As String
                Dim edit_bool As Boolean
                If ViewState("datamode") = "edit" Then
                    edit_bool = True
                    ela_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                Else
                    edit_bool = False
                    ela_id = 0
                End If
                Try
                    retval = PayrollFunctions.ProcessSalaryWithVaccation(ddlBUnit.SelectedItem.Value, drpCalMonth.SelectedItem.Value, _
                    drpCalYear.SelectedItem.Value, streMPIDS, objConn, stTrans)

                    If retval = "0" Then
                        stTrans.Commit()
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value & "-" & drpCalMonth.SelectedItem.Value & "-" & drpCalYear.SelectedItem.Value, _
                             "Insert", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(retval)
                        Exit Sub
                    End If
                Catch ex As Exception
                    lblError.Text = getErrorMessage("1000")
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try
            End If

            'If retval <> "0" Then
            '    Exit For
            'End If


        Next

        If retval = "0" Then
            lblError.Text = "Salary is Processed Successfully ..."

        End If
        'chkNegativesalaryerror()
        clear_All()
    End Sub
    ''' <summary>
    ''' To check for negative salaries generated
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub chkNegativesalaryerror()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = " exec GetNegativeSalaryList '" & h_EMPID.Value & "','" & drpCalMonth.SelectedItem.Value & " ','" & drpCalYear.SelectedItem.Value & "','" & ddlBUnit.SelectedItem.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim strMsg As String = "Error generating salary for Empno: "
        Dim strEmpNos As String = ""
        If ds.Tables(0).Rows.Count <> 0 Then
            For Each dt As DataRow In ds.Tables(0).Rows
                strEmpNos = strMsg & dt.Item(0).ToString & ","
            Next
        End If
        If strEmpNos <> "" Then
            lblError.Text = strMsg & strEmpNos
        End If
    End Sub
    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblEmp_ID"), Label)
        If Not lblACTID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If

    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMPNO as ID,EMP_ID, ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub clear_All()
        h_EMPID.Value = ""
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub gvEMPName_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        set_Comboanddate()
        h_EMPID.Value = ""
        Session("EMP_SEL_COND") = " AND  EMP_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' " _
        & " AND (EMP_bACTIVE = 1 or (month(isnull(EMP_LASTATTDT,getdate()))= month(getdate()) and year( isnull(EMP_LASTATTDT,getdate()))=year(getdate()))) " _
        & " AND EMP_ECT_ID = " & rblCategory.SelectedValue

        fill_Emp_for_BSU()
        FillEmpNames(h_EMPID.Value)
    End Sub

    Sub set_Comboanddate()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & ddlBUnit.SelectedItem.Value & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                drpCalMonth.SelectedIndex = -1
                drpCalYear.SelectedIndex = -1
                drpCalMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                drpCalYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblError.Text = lblError.Text
    End Sub

    Protected Sub rblCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        set_Comboanddate()
        h_EMPID.Value = ""
        Session("EMP_SEL_COND") = " AND  EMP_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' " _
        & " AND (EMP_bACTIVE = 1 or (month(isnull(EMP_LASTATTDT,getdate()))= month(getdate()) and year( isnull(EMP_LASTATTDT,getdate()))=year(getdate()))) " _
        & " AND EMP_ECT_ID = " & rblCategory.SelectedValue

        fill_Emp_for_BSU()
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = h_EMPID.Value.Replace("||", ",")
        FillEmpNames(h_EMPID.Value)
        txtEMPNAME.Text = ""
    End Sub
End Class
