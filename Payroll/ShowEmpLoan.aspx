<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowEmpLoan.aspx.vb" Inherits="Payroll_ShowEmpLoan" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Loan Details</title>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server">
        <table border="0" cellpadding="1" cellspacing="0" width="100%">
            <tr>
              
                <td width="100%">
                    <asp:FormView ID="fvLeave" runat="server" Width="100%">
                        <ItemTemplate>
                            <table style="width: 100%; height: 100%;" align="center" cellpadding="4" cellspacing="0" >
                                <tr class="title-bg">
                                    <td colspan="4">Loan Details</td>
                                </tr>
                                <tr>
                                    <td  align="left" width="20%"><span class="field-label">Emp No</span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblEmpno" runat="server" CssClass="field-value" Text='<%# Bind("EMPNO") %>'></asp:Label></td>
                                     <td  align="left" width="20%"><span class="field-label">Emp Name </span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblEMP_NAME" runat="server" CssClass="field-value" Text='<%# Bind("EMP_NAME") %>'></asp:Label></td>
                                </tr>
                               
                                <tr>
                                    <td  align="left" width="20%"><span class="field-label">Date From</span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblELH_DATE" CssClass="field-value" runat="server" Text='<%# Bind("ELH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label></td>
                                    <td  align="left" width="20%"><span class="field-label">Installments </span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblELH_INSTNO" CssClass="field-value" runat="server" Text='<%# Bind("ELH_INSTNO") %>'></asp:Label></td>
                                </tr>
                               
                                <tr>
                                    <td  align="left" width="20%"><span class="field-label">Purpose </span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblELH_PURPOSE" CssClass="field-value" runat="server" Text='<%# Bind("ELH_PURPOSE") %>'></asp:Label></td>
                                     <td  align="left" width="20%"><span class="field-label">Paid Amount</span> </td>
                                    <td width="30%">
                                        <asp:Label ID="Label1"  CssClass="field-value" runat="server" Text='<%# Bind("ELH_AMOUNT") %>'></asp:Label></td>
                                </tr>
                               
                                <tr>
                                    <td  align="left" width="20%"><span class="field-label">Balance Amount </span> </td>
                                    <td width="30%">
                                        <asp:Label ID="lblELH_BALANCE" CssClass="field-value" runat="server" Text='<%# Bind("ELH_BALANCE") %>'></asp:Label></td>
                                      <td  align="left" width="20%"><span class="field-label">Loan Type </span></td>
                                    <td width="30%">
                                        <asp:Label ID="lblERN_DESCR" CssClass="field-value" runat="server" Text='<%# Bind("ERN_DESCR") %>'></asp:Label></td>
                                </tr>
                            
                            </table>
                        </ItemTemplate>
                    </asp:FormView>
                </td>
               
            </tr>
            <tr>
                <td class="title-bg">
                    Loan History
                </td>
            </tr>
            <tr>
                
                <td align="center" >
                    <asp:GridView ID="gvApprovals" runat="server" AutoGenerateColumns="False" EmptyDataText="History empty" Width="100%" CssClass="table table-bordered table-row">
                    <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                    <RowStyle CssClass="griditem" />
                    <SelectedRowStyle CssClass="griditem_hilight" />
                    <HeaderStyle CssClass="gridheader_pop"  />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <Columns>
                        <asp:BoundField DataField="ELH_INSTNO" HeaderText="Installment" ReadOnly="True">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ELD_AMOUNT" HeaderText="Amount Date" ReadOnly="True">
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ELD_PAIDAMT" HeaderText="Paid Amount">
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ELD_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"
                            HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT ISNULL(EM.EMP_FNAME, '') + ISNULL(EM.EMP_MNAME, '') + ISNULL(EM.EMP_LNAME, '') AS APS_NAME, CASE APS.APS_STATUS WHEN 'A' THEN 'APPROVED' WHEN 'R' THEN 'REJECTED' WHEN 'N' THEN 'NOT APPROVED' WHEN 'H' THEN 'HOLD' END AS APS_STATUS, CASE APS.APS_bREQPPREVAPPROVAL WHEN 0 THEN 'NO' WHEN 1 THEN 'YES' END AS PREV, CASE APS.APS_bREQPHIGHAPPROVAL WHEN 0 THEN 'NO' WHEN 1 THEN 'YES' END AS FINAL, APS.APS_REMARKS, APS.APS_DATE FROM APPROVAL_S AS APS INNER JOIN EMPLOYEE_M AS EM ON APS.APS_USR_ID = EM.EMP_ID WHERE (APS.APS_DOC_ID = 72) ORDER BY APS.APS_ORDERID"></asp:SqlDataSource>
                </td>
                
            </tr>
        </table>

    </form>
</body>
</html>
