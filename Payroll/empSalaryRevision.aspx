<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empSalaryRevision.aspx.vb" Inherits="Payroll_empSalaryRevision" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        function GetPayableFromEligible() {
            var eligibleAmt, FTEamt;
            eligibleAmt = document.getElementById("<%=txtSalAmtEligible.ClientID %>").value;
            FTEamt = document.getElementById("<%=hf_FTE.ClientID %>").value;
            document.getElementById("<%=txtSalAmount.ClientID %>").value = FTEamt * eligibleAmt;
        }

        function AddDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 550px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("empPopupSalarySchedule.aspx?" + url, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=txtGrossSalary.ClientID %>').focus();


            return false;
        }

        <%--function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=ENR", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //document.forms[0].submit();
                return true;
            }
            return false;
        }--%>
    </script>
    <script>
        function GetEMPName() {
            var url;
            url = "../Accounts/accShowEmpDetail.aspx?id=ENR"
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientClose2(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=txtEmpNo.ClientID %>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
        <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Salary Revision"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table   width="100%">
                    <tr>
                        <td align="left" colspan="5">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALEDITSAL" />
                            <asp:Label ID="lblFormError" runat="server" CssClass="error"></asp:Label>
                        <table  width="100%">
                            <tbody>
                                <tr>
                                    <td width="20%"> <span class="field-label">Employee No</span></td>

                                    <td width="30%">
                                        <asp:TextBox ID="txtEmpNo" runat="server" OnTextChanged="txtEmpNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetEMPName(); return false" />
                                        <asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtEmpNo"
                                            ErrorMessage="Select Employee" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator></td>
                                    <td width="20%"> <span class="field-label">Effective From</span></td>

                                    <td width="30%">
                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                            runat="server" ControlToValidate="txtDate" ErrorMessage="Date required" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                ID="revToDate" runat="server" ControlToValidate="txtDate" Display="Dynamic" EnableViewState="False"
                                                ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </table>
                <table id="tblSalDetails" runat="server"   width="100%"  >
                    <tr>
                        <td align="left" class="title-bg" >
                            <strong>Current Salary</strong></td>
                        <td width="50%" class="title-bg">
                            <strong>Salary Revision History</strong>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="left"   style=" vertical-align: top;">
                            <asp:GridView ID="gvEmpSalary" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered" EmptyDataText="No Transaction details added yet." Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("UniqueID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ENR ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblENRID" runat="server" Text='<%# bind("ENR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Earing">
                                        <ItemTemplate>
                                            <asp:Label ID="lblERN_DESCR" runat="server" Text='<%# bind("ERN_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount(Eligible)">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmountEligible" runat="server" Text='<%#AccountFunctions.Round(Container.DataItem("Amount_ELIGIBILITY"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount(Actual)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="bMonthly" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBMonthly" runat="server" Text='<%#bind("bMonthly") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PayTerm" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPayTerm" runat="server" Text='<%# bind("PAYTERM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schedule">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSchedule" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSalEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit" OnClick="lnkSalEdit_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table width="100%">
                                <tr>
                                <td>  <span class="field-label"> Gross Salary</span></td>
                                <td><asp:TextBox ID="txtGrossSalary" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    </tr>
                            </table>
                             
                    
                           </td>
                        <td >
                            <asp:GridView ID="gvEmpSalHistory" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                Width="100%" AllowPaging="True">
                                <Columns>
                                    <asp:BoundField DataField="ENR_ID" HeaderText="Earning" ReadOnly="True" />
                                    <asp:TemplateField ShowHeader="False" HeaderText="Duration">
                                        <HeaderTemplate>
                                            Duration
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            
                                    <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("duration") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr class="title-bg">
                        <td align="left" colspan="2">
                            <asp:Label ID="lblEarnCaption" runat="server" Text="Add New"></asp:Label></td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"> <span class="field-label"> Earning</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddSalEarnCode" runat="server" ValidationGroup="VALADDNEWSAL">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Amount(Eligibility)</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSalAmtEligible" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSalAmtEligible"
                                            ErrorMessage="Enter the Salary Amount" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Amount(Actual)</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSalAmount" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSalAmount"
                                            ErrorMessage="Enter the Salary Amount" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                        <br />
                                        <asp:CheckBox ID="chkSalPayMonthly" runat="server" Text="Per Month" Checked="true" CssClass="field-label" />
                                    </td>
                                    <td align="left"><span class="field-label">Payment Installment(s)</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlayInstallment" runat="server">
                                            <asp:ListItem Value="0">Monthly</asp:ListItem>
                                            <asp:ListItem Value="1">Bimonthly</asp:ListItem>
                                            <asp:ListItem Value="2">Quarterly</asp:ListItem>
                                            <asp:ListItem Value="3">Half-yearly</asp:ListItem>
                                            <asp:ListItem Value="4">Yearly</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                   
                                    <td align="left"><span class="field-label">Pay arrears with</span></td>
                                        <td colspan="2">
                   <asp:DropDownList ID="ddSalPayMonth" runat="server" style="width:15%;min-width: 10% !important;" >
                       <asp:ListItem Value="1">Jan</asp:ListItem>
                       <asp:ListItem Value="2">Feb</asp:ListItem>
                       <asp:ListItem Value="3">Mar</asp:ListItem>
                       <asp:ListItem Value="4">Apr</asp:ListItem>
                       <asp:ListItem Value="5">May</asp:ListItem>
                       <asp:ListItem Value="6">Jun</asp:ListItem>
                       <asp:ListItem Value="7">July</asp:ListItem>
                       <asp:ListItem Value="8">Aug</asp:ListItem>
                       <asp:ListItem Value="9">Sep</asp:ListItem>
                       <asp:ListItem Value="10">Oct</asp:ListItem>
                       <asp:ListItem Value="11">Nov</asp:ListItem>
                       <asp:ListItem Value="12">Dec</asp:ListItem>
                   </asp:DropDownList><asp:DropDownList ID="ddSalPayYear" runat="server" style="width:15%;min-width: 10% !important;" >
                   </asp:DropDownList>
                                       <span class="field-label"> Salary</span></td>
                                    <td align="left">
                                        <asp:CheckBox ID="chksalArreasPaid" runat="server" Text="Arreas Paid" CssClass="field-label"  /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnSalAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALADDNEWSAL" />
                                        <asp:Button ID="btnSalCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                </tr>
                            </table>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
                        </td>
                    </tr>

                </table>
                <table width="100%">
                    <tr>
                        <td align="center">

                            <asp:Button ID="btnSalRevSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALEDITSAL" />
                            <asp:Button ID="btnSalRevCancel" runat="server" CssClass="button" Text="Cancel" /><br />
                            <asp:HiddenField ID="h_Emp_No" runat="server" />
                            <asp:HiddenField ID="hf_FTE" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDate" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtDate" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

