<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empAssignDelegate.aspx.vb" Inherits="Payroll_empAssignDelegate" Title="Asset Location" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function GetEMPName(mode) {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            //var NameandCode;
            //var result;
            <%-- result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                if (ctrlname == "EMP") {
                    NameandCode = result.split('___');
                    document.getElementById('<%=h_EMPID.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=txtEmployee.ClientID %>').value = NameandCode[0];
                }
                else if (ctrlname == "DEL") {
                    NameandCode = result.split('___');
                    document.getElementById('<%=h_DelegateEmpID.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=txtDelegateEmp.ClientID %>').value = NameandCode[0];
                }
                //
            return true;
        }
        return false;--%>

            document.getElementById("<%=hf_SearchMode.ClientID%>").value = mode;
            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var result = radopen(url, "pop_employee");
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var ctrlname = document.getElementById("<%=hf_SearchMode.ClientID%>").value

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                if (ctrlname == "EMP") {
                    document.getElementById('<%=h_EMPID.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=txtEmployee.ClientID %>').value = NameandCode[0];
                    __doPostBack('<%= txtEmployee.ClientID%>', 'TextChanged');
                    //return true;
                    //            PageMethods.setDefaultLeavePolicy();
                }
                else if (ctrlname == "DEL") {
                    document.getElementById('<%=h_DelegateEmpID.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=txtDelegateEmp.ClientID %>').value = NameandCode[0];
                    __doPostBack('<%= txtDelegateEmp.ClientID%>', 'TextChanged');
                }

            }

        }


    function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Job Delegation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBSUName" runat="server" TabIndex="1" MaxLength="100"
                                            CssClass="inputbox" Enabled="False"></asp:TextBox>
                                    </td>
                                     <td align="left" width="20%"><span class="field-label">Employee</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEmployee" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName('EMP'); return false;"
                                            CausesValidation="False" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmployee"
                                            CssClass="error" ErrorMessage="Employee Name:" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="left"><span class="field-label">Job Type</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddJobType" runat="server" AppendDataBoundItems="True">
                                        </asp:DropDownList>
                                    </td>
                                     <td align="left"><span class="field-label">Delegate To</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDelegateEmp" runat="server"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgDelegateEmp" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName('DEL'); return false;"
                                            CausesValidation="False" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDelegateEmp"
                                            CssClass="error" ErrorMessage="Employee Name:" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From Date</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server" AutoPostBack="True" AutoCompleteType="Disabled"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFrom" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="4" />
                                        &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvFromDate" ControlToValidate="txtFromDate"
                                            ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator></td>
                                    <td align="left"><span class="field-label">To Date</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtToDate" runat="server"  AutoPostBack="True" AutoCompleteType="Disabled"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender3" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender4" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvToDate" ControlToValidate="txtToDate"
                                            ErrorMessage="To Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                            EnableTheming="False"></asp:TextBox>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Entered By</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtLoggedInuser" runat="server" CssClass="inputbox"
                                            Enabled="False"></asp:TextBox>                                        
                                    </td>
                                    <td align="left" >
                                        <asp:CheckBox ID="chkForward" runat="server" Text="Enable" />
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkDisable" runat="server" Text="Disable" />
                                    </td>
                                </tr>
                                <tr id="trDisabledDate" runat="server">
                                    <td align="left"><span class="field-label">Disabled Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDisabledDate" runat="server"
                                            AutoPostBack="True" AutoCompleteType="Disabled" Enabled="False"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtDisabledDate_CalendarExtender"
                                            runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFrom" TargetControlID="txtDisabledDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtDisabledDate_CalendarExtender2"
                                            runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtFromDate"
                                            TargetControlID="txtDisabledDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtDisabledDate_CalendarExtender3" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtDisabledDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="txtDisabledDate_CalendarExtender4" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="txtFromDate" TargetControlID="txtDisabledDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="h_EJD_ID" runat="server" />
                            <asp:HiddenField ID="h_BSU_ID" runat="server" />
                            <asp:HiddenField ID="h_EmpID" runat="server" />
                            <asp:HiddenField ID="h_DelegateEmpID" runat="server" />
                            <asp:HiddenField ID="hf_SearchMode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
