<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EMPBookStaffTransfer.aspx.vb" Inherits="Payroll_EMPBookStaffTransfer" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

    <script type="text/javascript">
        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = '../Common/PopUpEmployeesList.aspx?id=EMPLOYEES_TOTRANSFER';
            result = radopen(url, "pop_up");

           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___'); document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');                
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=h_Emp_No.ClientID%>', "ValueChanged");
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>         
     </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="Label4" runat="server" Text="Book Employee Transfer"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALIDATE"></asp:ValidationSummary>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table align="center" cellpadding="5" cellspacing="0"
                    style="border-collapse: collapse; width: 100%;">
                    <%-- <tr class="subheader_img">
            <td align="left" colspan="4" valign="middle">
                <asp:Label ID="Label4" runat="server" Text="Book Employee Transfer"></asp:Label>
            </td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEMPName(); return false;" />
                            <asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtEmpNo"
                                ErrorMessage="Select Employee" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator>
                           <br /> Empno : &nbsp;  
                <asp:Label ID="lblEmpno" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: left" width="20%"><span class="field-label">Transfer Date</span> </td>
                        <td width="30%">
                            <asp:TextBox ID="txtDate" AutoPostBack="true" runat="server" Width="96px"></asp:TextBox>
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate" ErrorMessage="Date required"
                                ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revToDate" runat="server" ControlToValidate="txtDate" Display="Dynamic" EnableViewState="False"
                                    ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="VALIDATE">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Destination Business Unit</span></td>
                        <td style="text-align: left" width="30%">
                            <asp:DropDownList ID="ddlBSUnit" runat="server">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">MOL Profession</span>
                        </td>
                        <td align="left" style="color: red" width="30%">
                            <asp:TextBox ID="txtML" runat="server"></asp:TextBox></td>
                        <td align="left" widt="20%"><span class="field-label">Employee Designation</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtSD" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">MOE Profession</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtME" runat="server"></asp:TextBox></td>
                        <td align="left" wdth="20%"><span class="field-label">Employee Catagory</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCat" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Teaching Grade</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtTGrade" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%"><span class="field-label">Department</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDept" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%"><span class="field-label">Employee Reports To</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtReport" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Vacation Policy</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtVacationPolicy" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20% "><span class="field-label">Leave Approval Policy</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpApprovalPol" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Other Details</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Gratuity</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtgratuity" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtgratuity"
                                ErrorMessage="Please enter Gratuity Amount" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                        <td align="left" width="20%"><span class="field-label">Air Fare</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAirFare" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAirFare"
                                ErrorMessage="Please enter AirFare Amount" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Leave Salary</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLeaveSalary" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLeaveSalary"
                                ErrorMessage="Please enter Leave Salary" ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator></td>
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkConcession" runat="server" Text="Concession" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" ></asp:TextBox></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Book Transfer" ValidationGroup="VALIDATE" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" OnValueChanged="h_Emp_No_ValueChanged" />
                <asp:HiddenField ID="h_Trans_ID" runat="server" />
                <asp:HiddenField ID="hfEmployeeNum" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

