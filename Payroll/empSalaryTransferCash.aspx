<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empSalaryTransferCash.aspx.vb" Inherits="Payroll_empSalaryTransferCash"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function ShowBankDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowBankDetail.aspx?ela_id=" + id + "&filter=" + document.getElementById('<%=hcatFilter.ClientID %>').value + "&EmpId=" + document.getElementById('<%=h_Emp_No.ClientID %>').value +
            "&IsFFS=" + document.getElementById('<%=chkFFS.ClientID %>').checked + "&WPS=0", "", sFeatures)

            return false;
        }

        function UpdateSumBank() {
            var sum = 0.0;
            var dsum = 0.0;
            var chk = 0;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0)
                        if (document.forms[0].elements[i].checked == true)
                        { chk = 1; }
                }
                if (document.forms[0].elements[i].name.search(/txtAmountBank/) > 0) {
                    if (document.forms[0].elements[i].name != '<%=txtAmount.ClientID %>' && chk == 1) {
                        chk = 0;
                        sum += parseFloat(document.forms[0].elements[i].value);
                    }
                }
            }

            document.getElementById('<%=txtAmount.ClientID %>').value = sum;
        }
        function UpdateSumCash() {
            var sum = 0.0;
            var dsum = 0.0;
            var chk = 0;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].name.search(/chkESD_CASH/) > 0)
                        if (document.forms[0].elements[i].checked == true)
                        { chk = 1; }
                }
                if (document.forms[0].elements[i].name.search(/txtAmountCash/) > 0) {
                    if (document.forms[0].elements[i].name != '<%=txtAmount.ClientID %>' && chk == 1) {
                        chk = 0;
                        sum += parseFloat(document.forms[0].elements[i].value);
                    }
                }
            }

            document.getElementById('<%=txtAmount.ClientID %>').value = sum;
        }

        function change_chk_state_Bank(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0 || document.forms[0].elements[i].name.search(/chkSelallBank/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
            UpdateSumBank();
        }
        function change_chk_state_Cash(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkESD_CASH/) > 0 || document.forms[0].elements[i].name.search(/chkSelallCash/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
            UpdateSumCash();
        }

        function get_Bank() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 509px; ";
            //sFeatures += "dialogHeight: 434px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = '../accounts/PopUp.aspx?ShowType=BANK&codeorname=' + document.getElementById('<%=txtBankCode.ClientID %>').value;
            var oWnd = radopen(url, "pop_bank");
            //result = window.showModalDialog("..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "", sFeatures);

           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=txtChqBook.ClientId %>').value = '';
            document.getElementById('<%=hCheqBook.ClientId %>').value = '';
            document.getElementById('<%=txtChqNo.ClientId %>').value = '';--%>
            return false;
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtChqBook.ClientId %>').value = '';
                document.getElementById('<%=hCheqBook.ClientId %>').value = '';
                document.getElementById('<%=txtChqNo.ClientId %>').value = ''
            }
        }
        function get_Cash() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 709px; ";
            //sFeatures += "dialogHeight: 434px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = '../accounts/ShowAccount.aspx?ShowType=CASHONLY&codeorname=' + document.getElementById('<%=txtCashAcc.ClientID %>').value;
            var oWnd = radopen(url, "pop_cash");
            //result = window.showModalDialog("..\/accounts\/ShowAccount.aspx?ShowType=CASHONLY&codeorname=" + document.getElementById('<%=txtCashAcc.ClientID %>').value, "", sFeatures);
          
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtCashAcc.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtCashDescr.ClientID %>').value = NameandCode[1];--%>

            return false;
        }
        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtCashAcc.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCashDescr.ClientID %>').value = NameandCode[1];
            }
        }

        function get_Cheque() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 509px; ";
            //sFeatures += "dialogHeight: 434px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "..\/accounts\/ShowChqs.aspx?ShowType=CHQBOOK_PDC&BankCode=" + document.getElementById('<%= txtBankCode.ClientId %>').value + "&docno=0";

            if (document.getElementById('<%= txtBankCode.ClientId %>').value == "") {
                alert("Please Select The Bank");
                return false;
            }
            var oWnd = radopen(url, "pop_cheq");
            <%--result = window.showModalDialog("..\/accounts\/ShowChqs.aspx?ShowType=CHQBOOK_PDC&BankCode=" + document.getElementById('<%= txtBankCode.ClientId %>').value + "&docno=0", "", sFeatures);
            if (result == '' || result == undefined)
            { return false; }
            lstrVal = result.split('||');
            document.getElementById('<%= txtChqBook.ClientId %>').value = lstrVal[1];
            document.getElementById('<%= hCheqBook.ClientId %>').value = lstrVal[0];
            document.getElementById('<%= txtChqNo.ClientId %>').value = lstrVal[2];--%>

        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hCheqBook.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtChqBook.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtChqNo.ClientID %>').value = NameandCode[1];
            }
        }

        function GetEMPList() {

            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = '../Common/PopUpEmployeesList.aspx?id=EMPLOYEES_ALL';
            var oWnd = radopen(url, "pop_emp");
            //result = window.showModalDialog(url, "", sFeatures)
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___'); document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }--%>
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
            }
        }

    </script>

    <telerik:radwindowmanager id="RadWindowManager1" showcontentduringload="false" visiblestatusbar="false"
        reloadonshow="true" runat="server" enableshadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_cash" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_bank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cheq" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <table width="100%">
                <tr runat="server" id="trCash1">
                    <td><i class="fa fa-usd mr-3"></i>Salary Transfer (Cash)
                    </td>
                </tr>
                <tr runat="server" id="trBank1">
                    <td><i class="fa fa-usd mr-3"></i>Salary Transfer (Bank)
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
                    <tr>
                        <td colspan="4" align="left" style="height: 19px">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Document Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocdate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                        <td width="20%"><span class="field-label">Mode Of Pay</span></td>
                        <td width="30%">
                            <asp:RadioButton ID="rbCashTran" runat="server" Checked="True" GroupName="Transfer"
                                Text="Cash" AutoPostBack="True" />
                            <asp:RadioButton ID="rbBankTran" runat="server" GroupName="Transfer" Text="Bank"
                                AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%" nowrap="nowrap"><span class="field-label">ABC Category</span>
                        </td>
                        <td align="left">
                            <table>
                                <tr>

                                    <td style="width: 50%;">
                                        <asp:CheckBoxList ID="chkABC" runat="server" AutoPostBack="True" RepeatColumns="3"
                                            RepeatDirection="Horizontal" CssClass="checkbox">
                                            <asp:ListItem>A</asp:ListItem>
                                            <asp:ListItem>B</asp:ListItem>
                                            <asp:ListItem>C</asp:ListItem>
                                        </asp:CheckBoxList>&nbsp;
                                    </td>
                                    <td style="width: 50%;" align="left">

                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/new_animated.gif" ToolTip="Click checkbox for final settlement disbursement" />

                                        <asp:Label ID="lblFFS" runat="server" Text=" Final Settlements " Font-Bold="True" Font-Size="X-Small" ForeColor="Maroon" ToolTip="Click checkbox for final settlement disbursement"></asp:Label>&nbsp;<asp:CheckBox ID="chkFFS"
                                            runat="server" Checked="false" AutoPostBack="True" CssClass="checkbox" ToolTip="Click here for final settlement disbursement" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" style="width: 20%"><span class="field-label">Select Employee (Optional)</span></td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtEmpNo" runat="server" Width="299px" OnTextChanged="txtEmpNo_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return GetEMPList()" />

                            &nbsp;&nbsp;&nbsp;
                    
                       <asp:ImageButton ID="imgDelete" runat="server" ToolTip="Click To clear Employee filter"
                           ImageUrl="~/Images/Oasis_Hr/Images/cross.png" Visible="False" OnClick="imgDelete_Click" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Month Year</span>
                        </td>
                        <td align="left">
                            <asp:CheckBoxList ID="clMonthYear" runat="server" AutoPostBack="True" RepeatLayout="Flow" RepeatColumns="4"
                                RepeatDirection="Horizontal" CssClass="checkbox">
                            </asp:CheckBoxList>
                        </td>
                        <td align="left"><span class="field-label">Purpose</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPurpose" runat="server" DataSourceID="SqlDataSource2"
                                DataTextField="PURP_DESCR" DataValueField="PURP_ID">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT PURP_ID, PURP_DESCR FROM PaymentPurpose_M order by PURP_Order"></asp:SqlDataSource>
                        </td>

                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Narration</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNarrn" runat="server" CssClass="inputbox_multi" MaxLength="300"
                                SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                        </td>

                    </tr>
                    <tr style="color: #1b80b6" id="TrCashGrid" runat="server">
                        <td align="left"><span class="field-label">Salary Transfer Details</span>
                        </td>
                        <td align="center" colspan="3">
                            <div visible="false" id="divCashGridhd" runat="server">
                                <table width="100%">
                                    <tr class="title-bg">
                                        <td width="10%" align="center">
                                            <input id="chkSelall2" type="checkbox" onclick="javascript: change_chk_state_Cash(this)" />
                                        </td>
                                        <td width="20%">Employee No
                                        </td>
                                        <td width="40%">Employee Name
                                        </td>
                                        <td width="30%">Amount
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="overflow: auto;" visible="false" id="divCashGrid"
                                runat="server">
                                <asp:GridView ID="gvDetailsCash" CssClass="table table-bordered table-row" runat="server" Width="100%" AutoGenerateColumns="False"
                                    ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                <input id="chkSelallCash" type="checkbox" onclick="javascript: change_chk_state_Cash(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="chkESD_CASH" runat="server" value='<%# Bind("ESD_ID") %>' type="checkbox"
                                                    onclick="UpdateSumCash();" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="EMPNO" HeaderText="Emp No" ItemStyle-Width="20%"></asp:BoundField>
                                        <asp:BoundField DataField="EMP_NAME" HeaderText="Emp Name" ItemStyle-Width="40%"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Amount" ItemStyle-Width="30%">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAmountCash" runat="server" OnPreRender="txtAmount_PreRender" Text='<%# Bind("ESD_EARN_NET") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                    <RowStyle CssClass="griditem" />
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_pop" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </div>
                            <div visible="false" id="divbankHd" runat="server">
                                <table width="100%">
                                    <tr class="title-bg">
                                        <td width="10%" align="center">
                                            <input id="chkSelectAllBanks" type="checkbox" onclick="javascript: change_chk_state_Bank(this)" />
                                        </td>
                                        <td width="50%">Remarks
                                        </td>
                                        <td width="40%">Amount
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="overflow: auto;" visible="false" id="divBank"
                                runat="server">
                                <asp:GridView CssClass="table table-bordered table-row" ID="gvDetailsBank" SkinID="GridViewNormal" runat="server" Width="100%"
                                    AutoGenerateColumns="False" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                <input id="chkSelallBank" type="checkbox" onclick="javascript: change_chk_state_Bank(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input id="chkESD_BANK" runat="server" value='<%# Bind("ESD_BANK") %>' type="checkbox"
                                                    onclick="UpdateSumBank();" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS" ItemStyle-Width="50%">
                                            <ItemTemplate>
                                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="<%# &quot;ShowBankDetail('&quot; & Container.DataItem(&quot;ESD_BANK&quot;) & &quot;');return false;&quot; %>"
                                                    Text='<%# Bind("BNK_DESCRIPTION") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount" ItemStyle-Width="40%">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAmountBank" runat="server" OnPreRender="txtAmount_PreRender" Text='<%# Bind("amount") %>'
                                                    Width="131px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Total Amount Selected</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>

                </table>
                <br />
                <%-- --------Payment Details------------------ --%>
                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" class="title-bg" align="left">Payment Details
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Voucher Type</span></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbCash" CssClass="field-label" runat="server" Checked="True" GroupName="pay" Text="Cash Payment"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbBank" CssClass="field-label" runat="server" GroupName="pay" Text="Bank Payment" AutoPostBack="True" /><br />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="tr_Cash" runat="server">
                        <td align="left"><span class="field-label">Cash A/C</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCashAcc" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgCash" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Cash();return false;" />
                      
                        </td>
                               <td><span class="field-label">Account  Name</span></td>
                        <td> <asp:TextBox ID="txtCashDescr" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr id="tr_Bank" runat="server">
                        <td align="left"><span class="field-label">Bank A/C</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank();return false;" />
                            
                        </td>
                        <td><span class="field-label">Bank Name</span></td>
                        <td><asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr id="tr_ChqRef" runat="server">
                        <td align="left" contenteditable="true"><span class="field-label">Cheque/Refrence</span></td>

                        <td align="left">
                            <asp:RadioButton ID="rbCheque" runat="server" Checked="True" GroupName="chq" Text="Cheque" CssClass="radiobutton field-label" AutoPostBack="True" />
                            &nbsp;
                        <asp:RadioButton ID="rbOthers" runat="server" GroupName="chq" Text="Other Instruments" CssClass="radiobutton field-label" AutoPostBack="True" /></td>
                        <td>
                            <asp:Label ID="lblRef" CssClass="field-label" runat="server" Text="Ref. No"></asp:Label>
                        </td>

                        <td>
                            <asp:TextBox ID="txtrefChequeno" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr id="tr_Cheque" runat="server">
                        <td align="left"><span class="field-label">Date</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChequedate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                        <td align="left"><span class="field-label">Cheque Lot</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqBook" runat="server"></asp:TextBox>
                            <a href="#" onclick="get_Cheque()">
                                <img alt="Cheque" id="IMG1" language="javascript" src="../Images/cal.gif"
                                    runat="server" /></a>
                            <asp:TextBox ID="txtChqNo" runat="server" Width="120px"></asp:TextBox>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT ERN_ID, ERN_DESCR FROM EMPSALCOMPO_M WHERE (ERN_TYP = 0)"></asp:SqlDataSource>
                        </td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr runat="server" id="tr_cheque_Receivedby">
                        <td align="left"><span class="field-label">Received By</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtReceivedby" runat="server"></asp:TextBox></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left"><span class="field-label">Value Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtValueDate" runat="server"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txtValueDate_CalendarExtender" runat="server"
                                CssClass="MyCalendar" PopupPosition="TopRight"
                                Format="dd/MMM/yyyy" PopupButtonID="imgValueDate" TargetControlID="txtValueDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="txtValueDate_CalendarExtender2" runat="server"
                                CssClass="MyCalendar" PopupPosition="TopRight"
                                Format="dd/MMM/yyyy" PopupButtonID="txtValueDate" TargetControlID="txtValueDate">
                            </ajaxToolkit:CalendarExtender>

                            <asp:ImageButton ID="imgValueDate" runat="server"
                                ImageUrl="~/Images/calendar.gif" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="Trbearer" runat="server" visible="false">
                        <td align="left"><span class="field-label">Bearer</span></td>
                        <td align="left">
                            <asp:CheckBox ID="chkBearer" runat="server" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Transfer Salary" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hCheqBook" runat="server" />
    <asp:HiddenField ID="hcatFilter" runat="server" />
    <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgCalendar" TargetControlID="txtDocdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtDocdate" TargetControlID="txtDocdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" PopupPosition="TopRight"
        Format="dd/MMM/yyyy" PopupButtonID="ImageButton4" TargetControlID="txtChequedate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" PopupPosition="TopRight"
        Format="dd/MMM/yyyy" PopupButtonID="txtChequedate" TargetControlID="txtChequedate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="h_Emp_No" runat="server" />
</asp:Content>
