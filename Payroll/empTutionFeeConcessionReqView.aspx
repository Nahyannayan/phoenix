﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empTutionFeeConcessionReqView.aspx.vb" Inherits="Payroll_empTutionFeeConcessionReqView" Theme="General" %>



<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script type="text/javascript" language="javascript">

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }


        function ViewDetail(url, text, w, h) {
            var sFeatures;
            sFeatures = "dialogWidth: 625px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen(url, "pop_up")
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //return false;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-5"></i>

            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server" Visible="False">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr valign="top">
                        <td align="left" valign="middle" width="10%"><span class="field-label">View </span></td>
                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlTopFilter" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"
                                OnSelectedIndexChanged="ddlCount_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="10">Last 10</asp:ListItem>
                                <asp:ListItem Value="25">Last 50</asp:ListItem>
                                <asp:ListItem Value="50">Last 100</asp:ListItem>
                                <asp:ListItem>All</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                        <td align="left" valign="middle" width="8%"><span class="field-label">Academic Year </span></td>
                        <td align="left" valign="middle" width="15%">
                            <asp:DropDownList ID="ddlAcaYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                
                            </asp:DropDownList>
                        </td>



                        <td align="right" width="34%">
                            <asp:RadioButtonList ID="rblFilter" RepeatDirection="Horizontal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblFilter_SelectedIndexChanged">
                                <asp:ListItem Value="PWU">Pending @ Working Unit</asp:ListItem>
                                <asp:ListItem Selected="True" Value="PND">Pending</asp:ListItem>
                                <asp:ListItem Value="APD">Approved</asp:ListItem>
                                <asp:ListItem Value="RJD">Rejected</asp:ListItem>
                                <%--<asp:ListItem Value="ALL">All</asp:ListItem>--%>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="5">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="25" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField HeaderText="" Visible='false'>
                                        <HeaderTemplate>
                                            <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EmpNo">
                                        <HeaderTemplate>
                                            Emp.No
                                            <br />
                                            <asp:TextBox ID="txtEmpno" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEmpSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpno" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EmpName">
                                        <HeaderTemplate>
                                            Emp.Name<br />
                                            <asp:TextBox ID="txtEmpName" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEmpNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp.BSU" Visible ="false" >
                                        <HeaderTemplate>
                                            Emp.BSU
                                            <br />
                                            <asp:TextBox ID="txtEmpBsu" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEmpbsuSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblempbsu" runat="server" Text='<%# Bind("EMP_BSU_SHORTNAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dept.">
                                        <HeaderTemplate>
                                            Department
                                            <br />
                                            <asp:TextBox ID="txtDept" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDeptSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMP_DEPT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <HeaderTemplate>
                                            Designation
                                            <br />
                                            <asp:TextBox ID="txtEmpDesign" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDesigSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesig" runat="server" Text='<%# Bind("EMP_DES_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HAYS Grade">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblDoj" runat="server" Text='<%# Bind("EMP_BSU_JOINDT","{0:dd/MMM/yyyy}") %>'></asp:Label>--%>
                                            <asp:Label ID="lblHaysGrade" runat="server" Text='<%# Bind("HAYS_GRADE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DOJ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJG" runat="server" Text='<%# Bind("EMP_JOINDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                   <%-- <asp:TemplateField HeaderText="Entry.Dt">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSCR_DATE" runat="server" Text='<%# Bind("SCR_DATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="Appr.Dt">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAPPRDT" runat="server" Text='<%# Bind("SCD_APPR_DATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="StudentID">
                                        <HeaderTemplate>
                                            Student#<br />
                                            <asp:TextBox ID="txtStuno" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStunoSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student">
                                        <HeaderTemplate>
                                            Stud.Name
                                            <br />
                                            <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStunameSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuname" runat="server" Text='<%# Bind("STUNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade
                                            <br />
                                            <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="School">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstubsu" runat="server" Text='<%# Bind("STU_BSU_SHORTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Tuition Fee">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgross" runat="server" Text='<%# Bind("FEE_ACTUAL_AMT")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession" Visible ="false" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblconcession" runat="server" Text='<%# Bind("CONC_AMOUNT")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Eligibility as per policy">
                                        <HeaderTemplate>
                                        <span style="color:black;">Eligibility as per policy</span><span style="color:red;"> *</span>                                      
                                        </HeaderTemplate> 
                                      
                                        <ItemTemplate>
                                            
                                            <asp:TextBox ID="txtRemarks"  runat="server" Width="400px" Text='<%# Bind("ELIGIBILITY_NOTE")%>'></asp:TextBox>                                            
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbDetail" OnClick="lbDetail_Click" runat="server">Detail</asp:LinkButton>
                                            <asp:HiddenField ID="hf_EMPID" runat="server" Value='<%# Bind("EMP_ID")%>' />
                                            <asp:HiddenField ID="hf_FCH_BSU_ID" runat="server" Value='<%# Bind("FCH_BSU_ID")%>' />
                                            <asp:HiddenField ID="hf_CACD_ID" runat="server" Value='<%# Bind("FCH_CURR_ACD_ID")%>' />
                                            <asp:HiddenField ID="hf_NEW_ACD_ID" runat="server" Value='<%# Bind("FCH_NEW_ACD_ID")%>' />                                                                                       
                                            <asp:HiddenField ID="hf_STUID" runat="server" Value='<%# Bind("FCD_REF_ID")%>' />                                            
                                            <asp:HiddenField ID="hf_EMP_BSU_ID" runat="server" Value='<%# Bind("EMP_BSU_ID")%>' />
                                            <asp:HiddenField ID="hf_EMPNAME" runat="server" Value='<%# Bind("EMPNAME")%>' />
                                            
<%--                                            <asp:HiddenField ID="hf_STATUS" runat="server" Value='<%# Bind("SCD_APPR_STATUS") %>' />
                                            <asp:HiddenField ID="hf_SSVID" runat="server" Value='<%# Bind("SCD_SSV_ID") %>' />--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Message" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblErrorMessage" runat="server" Text='<%# Bind("ELIGIBILITY_NOTE")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id= "CommentsSection" runat ="server" visible ="false" >
                        <td align="left" valign="top"><span class="field-label">Comments  </span></td>
                        <td align="left">
                            <asp:TextBox ID="txtComments" TextMode="MultiLine" SkinID="MultiText"
                                runat="server"></asp:TextBox>
                        </td>
                        <td></td>

                    </tr>
                    <tr id="alerts" runat ="server" visible ="false" >
                        <td align="left" valign="top" colspan="3">
                            <asp:Label ID="lblAlert" runat="server" CssClass="text-danger"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="3">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                        </td>
                    </tr>
                </table>



                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
