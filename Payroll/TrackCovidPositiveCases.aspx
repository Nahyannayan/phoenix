﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TrackCovidPositiveCases.aspx.vb" Inherits="Payroll_TrackCovidPositiveCases" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">


        function scroll_page() {
            document.location.hash = '<%=h_Grid.Value %>';
        }
        window.onload = scroll_page;
    </script>
    <style>
        .MyCalendar {
            z-index: 9999;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Track COVID Positive Cases
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                        </td>

                    </tr>
                    <tr valign="top">
                        <td valign="top" align="left">&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">

                    <tr>
                        <td align="center" valign="top" width="100%">
                            <asp:GridView ID="gvEMPCOVIDTrackDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" AllowPaging="True" SkinID="GridViewView" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="Staff / Student Ref">
                                        <HeaderTemplate>
                                            BSU
                                            <br />
                                            <asp:TextBox ID="txtBSU" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBSU" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Staff / Student Ref">
                                        <HeaderTemplate>
                                            Staff / Student No
                                            <br />
                                            <asp:TextBox ID="txtStaffStudpNo" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnStaffStudNo" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpStaffNo" runat="server" Text='<%# Bind("[Staff / Student Ref]") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            Staff / Student Name
                                            <br />
                                            <asp:TextBox ID="txtStaffStudName" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnStaffStudName" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("[Ref Name]") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            Type
                                            <br />
                                            <asp:TextBox ID="txtType" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnType" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCaseType" runat="server" Text='<%# Bind("TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            Infected/Close Contact Category
                                            <br />
                                            <asp:TextBox ID="txtCategory" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnCategory" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcategory" runat="server" Text='<%# Bind("CTD_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            Infected/Contact Person
                                            <br />
                                            <asp:TextBox ID="txtContactPerson" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnContactPerson" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcontactPerson" runat="server" Text='<%# Bind("CTD_PERSON_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                           Date of Contact Person
                                            <br />
                                            <asp:TextBox ID="txtDTContactPerson" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDTContactPerson" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDTContactPerson" runat="server" Text='<%# Bind("[CONTACT_PERSON_ON]") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            Isolation/Quarantine Start Date
                                            <br />
                                            <asp:TextBox ID="txtIsolationStartDT" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnISOStartDT" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsoStartDt" runat="server" Text='<%# Bind("CTD_ISOL_DATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                            Isolation/Quarantine End Date
                                            <br />
                                            <asp:TextBox ID="txtIsolationEndDT" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnISOEndDT" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsoEndDt" runat="server" Text='<%# Bind("CTD_DEISOL_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <HeaderTemplate>
                                           Influenza-Like symptoms
                                            <br />
                                            <asp:TextBox ID="txtInfluenza" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnInfluenza" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblInfluenza" runat="server" Text='<%# Bind("[CTD_INFLUENZA]") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Recovered">
                                        <%-- <HeaderTemplate>
                                           Recovered
                                           <br />
                                            <asp:TextBox ID="txtRecovered" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnRecovered" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRecovered" runat="server" Text='<%# Bind("CTD_RECOVERED") %>'></asp:Label>
                                           <asp:LinkButton ID="lbRecovered" runat="server" OnClientClick="if ( !confirm('Are you sure you want to convert the status as recovered?')) return false;" OnClick="lbRecovered_Click">Recovery</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="GUID" Visible="False" HeaderText="GUID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPStudID" runat="server" Text='<%# Bind("Ref_Id") %>'></asp:Label>
                                            <asp:Label ID="lblCTH_ID" runat="server" Text='<%# Bind("CTH_ID") %>'></asp:Label>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("CTH_REF_TYPE") %>'></asp:Label>
                                            <asp:Label ID="lblStaffStudName" runat="server" Text='<%# Bind("STAFF_STUD_NAME") %>'></asp:Label>
                                            <asp:Label ID="lblCTD_ID" runat="server" Text='<%# Bind("CTD_ID") %>'></asp:Label>
                                            <asp:Label ID="lblCTD_INF_CLOSE_CONTACT" runat="server" Text='<%# Bind("CTD_INF_CLOSE_CONTACT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <a id='detail'></a>
                            <br />
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <asp:Button Text="text" ID="Button1" CssClass="d-none" runat="server" />
    <input type="hidden" id="h_file_path" />
    <input type="text" id="file_path" style="display: none;" />
    <input type="hidden" id="file_ext" />
    <input type="hidden" id="hfEmpId" class="hfEmpId" />

</asp:Content>

