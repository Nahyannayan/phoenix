﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class Payroll_EmpReportToAllocation
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If '
            Dim qry As String = "SELECT ISNULL(USR_bITSupport,0) FROM dbo.USERS_M WHERE USR_ID='" & Session("sUsr_id") & "'"
            Dim bITSupport As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
            If bITSupport = True Then
                Me.chkAllEMP.Style.Add("display", "")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If MainMnu_code = "H000083" Then
                Me.chkAllEMP.Style.Add("display", "none")
            Else
            End If
            Page.Title = OASISConstants.Gemstitle
            lblReportCaption.Text = "Change Employee Attendance Reporting To or Reporting To Staff"
        End If

    End Sub


    Protected Sub imgEmp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmp.Click
        FillEMPNames(h_EMP_ID.Value)
    End Sub
    Protected Sub imgAPPEmp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAppEmp.Click
        GetApproverDetail(h_APP_EMP_ID.Value)
    End Sub
    Private Function FillEMPNames(ByVal EMPIDs As String, Optional ByVal bEMPNO As Boolean = False, Optional ByVal bDelete As Boolean = False) As Boolean
        Dim IDs As String() = EMPIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            condition += IIf(condition <> "", ",", "") + "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT A.EMP_ID,A.EMPNO,ISNULL(A.EMP_FNAME,'')+' '+ISNULL(A.EMP_MNAME,'')+' '+ISNULL(A.EMP_LNAME,'') EMP_NAME, " & _
                    "ISNULL(RTO.EMP_FNAME,'')+' '+ISNULL(RTO.EMP_MNAME,'')+' '+ISNULL(RTO.EMP_LNAME,'') RTO_EMP_NAME," & _
                    "ISNULL(AAP.EMP_FNAME,'')+' '+ISNULL(AAP.EMP_MNAME,'')+' '+ISNULL(AAP.EMP_LNAME,'') AAP_EMP_NAME, " & _
                    "B.ECT_DESCR FROM  dbo.EMPLOYEE_M A INNER JOIN EMPCATEGORY_M B ON A.EMP_ECT_ID = B.ECT_ID " & _
                    "LEFT JOIN dbo.EMPLOYEE_M RTO ON A.EMP_REPORTTO_EMP_ID=RTO.EMP_ID " & _
                    "LEFT JOIN dbo.EMPLOYEE_M AAP ON A.EMP_ATT_Approv_EMP_ID=AAP.EMP_ID "

        If bEMPNO = True Then
            str_Sql += "WHERE A.EMPNO IN (" + condition + ") "
        Else
            str_Sql += "WHERE A.EMP_ID IN (" + condition + ") "
        End If
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        If Me.gvEmployee.MasterTableView.Items.Count > 0 And bDelete = False Then
            Try
                Dim mRow As DataRow
                For Each mRow In ds.Tables(0).Rows
                    If DirectCast(ViewState("gvEmployee"), DataTable).Select("EMP_ID=" & mRow("EMP_ID"), "").Length = 0 Then
                        DirectCast(ViewState("gvEmployee"), DataTable).ImportRow(mRow)
                    End If
                Next
            Catch ex As Exception
                ViewState("gvEmployee") = ds.Tables(0)
            End Try
        Else
            ViewState("gvEmployee") = ds.Tables(0)
        End If

        BindGrid()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Sub GetApproverDetail(ByVal EMP_ID As String, Optional ByVal bEMPNO As Boolean = False)
        Dim str_Sql As String
        str_Sql = "SELECT ISNULL(A.EMP_FNAME,'')+' '+ISNULL(A.EMP_MNAME,'')+' '+ISNULL(A.EMP_LNAME,'') EMP_NAME " & _
                    "FROM  dbo.EMPLOYEE_M A  "

        If bEMPNO = False Then
            str_Sql &= "WHERE A.EMP_ID = ('" & EMP_ID & "') "
        Else
            str_Sql &= "WHERE A.EMPNO = ('" & EMP_ID & "') "
        End If
        Me.txtAppEmp.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)

    End Sub
    Sub BindGrid()
        Dim dt As DataTable = ViewState("gvEmployee")
        If dt.Rows.Count > 0 Then
            gvEmployee.Visible = True
            gvEmployee.DataSource = ViewState("gvEmployee")
            gvEmployee.DataBind()
        End If
    End Sub

    Protected Sub gvEmployee_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvEmployee.ItemCommand
        BindGrid()
    End Sub

    Protected Sub gvEmployee_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvEmployee.PageIndexChanged
        Dim index As Int32 = gvEmployee.CurrentPageIndex

        BindGrid()
    End Sub
    Private Function bValidate() As Boolean
        Me.lblError.Text = ""
        bValidate = True
        If Me.gvEmployee.MasterTableView.Items.Count < 1 Then
            bValidate = False
            Me.lblError.Text = "Please select the employees"
        ElseIf h_APP_EMP_ID.Value = "" Then
            bValidate = False
            Me.lblError.Text = "Please select the Approver or Reporting to Employee"
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If bValidate() Then
            Dim flag As Boolean = True
            Dim cmd As New SqlCommand
            cmd.Dispose()
            Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction
            stTrans = objConn.BeginTransaction
            Try
                Dim EMPIDs As String, mRow As DataRow
                EMPIDs = ""
                For Each mRow In DirectCast(ViewState("gvEmployee"), DataTable).Rows
                    EMPIDs &= "|" & mRow("EMP_ID")
                Next

                Dim iReturnvalue As Integer

                Dim pParms(5) As SqlClient.SqlParameter

                pParms(0) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
                pParms(0).Value = Session("sBsuid")

                pParms(1) = New SqlParameter("@APPROVER_ID", SqlDbType.Int)
                pParms(1).Value = Me.h_APP_EMP_ID.Value

                pParms(2) = New SqlParameter("@EMP_IDs", SqlDbType.VarChar)
                pParms(2).Value = EMPIDs 'Me.h_EMP_ID.Value

                pParms(3) = New SqlParameter("@APPROVER_FOR", SqlDbType.VarChar)
                pParms(3).Value = Me.rblAppType.SelectedValue

                pParms(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue

                iReturnvalue = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveEMPLOYEE_REPORTSTO", pParms)
                iReturnvalue = pParms(4).Value

                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    Me.lblError.Text = "Employee details not updated"
                Else
                    stTrans.Commit()
                    FillEMPNames(h_EMP_ID.Value, False, True)
                    Me.txtAppEmp.Text = ""
                    Me.h_APP_EMP_ID.Value = ""
                    Me.lblError.Text = "Employee details updated successfully"
                End If
            Catch
                stTrans.Rollback()
                Me.lblError.Text = "Employee details not updated"
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try


        End If
    End Sub

    Protected Sub lblAddNewEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewEmp.Click
        FillEMPNames(txtEmpName.Text.Trim, True)
    End Sub

    Protected Sub lblAddAppNewEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddAppNewEmp.Click
        GetApproverDetail(Me.txtAppEmp.Text.Trim, True)
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkButton As LinkButton = TryCast(sender, LinkButton)
        Dim row As Telerik.Web.UI.GridDataItem = DirectCast(lnkButton.NamingContainer, Telerik.Web.UI.GridDataItem)
        Dim emp_id As Integer = gvEmployee.MasterTableView.DataKeyValues(row.ItemIndex)("EMP_ID")
        Dim rowcount As Integer = gvEmployee.MasterTableView.Items.Count
        Me.h_EMP_ID.Value = Me.h_EMP_ID.Value.Replace(emp_id & IIf(rowcount = 1, "", "|"), "")
        Me.h_EMP_ID.Value = Me.h_EMP_ID.Value.Replace(IIf(rowcount = 1, "", "|") & emp_id, "")
        Dim mRow As DataRow
        If DirectCast(ViewState("gvEmployee"), DataTable).Select("EMP_ID=" & Val(emp_id), "").Length > 0 Then
            For Each mRow In DirectCast(ViewState("gvEmployee"), DataTable).Select("EMP_ID=" & Val(emp_id), "")
                mRow.Delete()
            Next
        End If
        DirectCast(ViewState("gvEmployee"), DataTable).AcceptChanges()
        BindGrid()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        DirectCast(ViewState("gvEmployee"), DataTable).Clear()
        Me.h_EMP_ID.Value = ""
        BindGrid()
    End Sub

    Protected Sub txtEmpName_TextChanged(sender As Object, e As EventArgs)
        txtEmpName.Text = "Multiple Employees has selected"
        FillEMPNames(h_EMP_ID.Value)
    End Sub
End Class
