<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empAttendanceAdd.aspx.vb" Inherits="Payroll_empAttendanceAdd" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <script language="javascript" type="text/javascript">
        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up")
           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
               document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Employee Attendance
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table cellpadding="5" cellspacing="0" align="center" width="100%">
                    <%--  <tr class="subheader_img">
                        <td colspan="4" style="height: 19px" align="left">Employee Attendance</td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select employee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox><asp:ImageButton
                                ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                                CssClass="error" ErrorMessage="Please Select an Employee"></asp:RequiredFieldValidator></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr runat="server" id="tr_Fromto">
                        <td align="left"><span class="field-label">Leave Period From</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFrom" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                        <td align="left"><span class="field-label">To</span> </td>
                        <td align="left"><asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                       
                    </tr>
                    <tr  runat="server" id="tr_Fromto2">
                         <td align="left"><span class="field-label">Leave Type</span> </td>
                        <td>
             <asp:DropDownList ID="ddMonthstatusPeriodically" runat="server"  DataTextField="ELT_DESCR" DataValueField="ELT_ID">
             </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr runat="server" id="tr_On">
                        <td align="left"><span class="field-label">Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtOn" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td align="left"><span class="field-label">Leave Type </span></td>
                        <td>  <asp:DropDownList ID="ddLeavetypeMonthly" runat="server" DataSourceID="SqlDataSource1" DataTextField="ELT_DESCR" DataValueField="ELT_ID">
             </asp:DropDownList>
                            <asp:DropDownList ID="ddHalfday" runat="server">
                                <asp:ListItem Value="1">Full Day</asp:ListItem>
                                <asp:ListItem Value=".5">Half Day</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Remarks</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                            <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" />
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT [ELT_ID], [ELT_DESCR] FROM [EMPLEAVETYPE_M]&#13;&#10;WHERE     (ELT_bLeave = 1) "></asp:SqlDataSource>

                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="title-bg" runat="server" id="tr_Deatailhead">
                        <td colspan="4" align="left">Leave Details 
         <asp:Label ID="lblWeekend" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>

                    <tr runat="server" id="tr_Deatails">
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvAttendance" runat="server" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Day" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="date" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"
                                        HtmlEncode="False" />
                                    <asp:TemplateField HeaderText="Leave Type">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddMonthstatusgrd" runat="server" DataSourceID="SqlDataSource1" DataTextField="ELT_DESCR" DataValueField="ELT_ID">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblLeave" runat="server" Text='<%# Bind("leavetype") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Bind("remarks") %>' TextMode="MultiLine" ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Days(.5/1)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDays" runat="server" OnPreRender="txtDays_PreRender" Text='<%# Bind("days") %>'
                                               ></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtOn">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtOn" TargetControlID="txtOn">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>
