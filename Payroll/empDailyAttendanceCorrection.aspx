<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empDailyAttendanceCorrection.aspx.vb" Inherits="Payroll_empDailyAttendanceCorrection"
    Title="Daily Attendance View" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>

    <style type="text/css">
        /*.HighLight {
            background-color: #FFCCFF;
        }

        .style1 {
            width: 290px;
        }

        .style2 {
            width: 219px;
        }*/

        #mydiv {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 30em;
            height: 18em;
            margin-top: -49em; /*set to a negative number 1/2 of your height*/
            margin-left: -35em; /*set to a negative number 1/2 of your width*/
            border: 1px solid #ccc;
            background-color: #f3f3f3;
        }


        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 4px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: #ffffff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 10px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .RadComboBox_Default {
            width: 100% !important;
        }
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image:url(../images/calender.gif)!important;
            width: 30px;
            height: 30px;
        }
        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active{
            background-image:url(/images/calender.gif)!important;
            width: 30px;
            height: 30px;
            background-position: 0;
        }

        .RadPicker { width:80% !important;
        }
        table.RadCalendar_Default {
            background:#ffffff !important;
        }

        .RadGrid_Default {
            border: 1px solid rgba(0,0,0,0.16) !important;
        }
        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border : 1px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function OpenBU(EmpID, sdate, mode) {
            var which = $("#<%=pnlViewPopup.ClientID %>");
            var myIFrame = $("#<%=ifrmViewPopup.ClientID %>");
            var hdnEMPID = $("#<%=hdnSelectedEmpID.ClientID %>");
            var hdnHREntry = $("#<%=hdnHREntry.ClientID%>");

            if (mode == "close") {
                which.css("display", "none");
                myIFrame.attr('src', 'about:blank');
                hdnEMPID.val('');
            }
            else {
                hdnEMPID.val(EmpID);
                myIFrame.css("width", "800px");
                myIFrame.css("height", "500px");
                PlaceControlAtCentre(800, 500);
                which.css("display", "block");
                if (hdnHREntry.val == "1") {
                    myIFrame.attr('src', "empLeaveBulkUpdate.aspx?empid=" + EmpID + "&attdate=" + sdate + "&HREntry=" + hdnHREntry.value);
                }
                else {
                    myIFrame.attr('src', "empLeaveBulkUpdate.aspx?empid=" + EmpID + "&attdate=" + sdate);
                }
            }
            return false;
        }
        function OpenEmployeeDetailScreen(EmpID, sdate, mode) {

            var which = $("#<%=pnlViewPopup.ClientID %>");
            var myIFrame = $("#<%=ifrmViewPopup.ClientID %>");
            if (mode == "close") {
                which.css("display", "none");
                myIFrame.attr('src', 'about:blank');
            }
            else {
                myIFrame.css("width", "800px");
                myIFrame.css("height", "500px");
                PlaceControlAtCentre(800, 500);
                which.css("display", "block");
                myIFrame.attr('src', "empViewDetails.aspx?empid=" + EmpID + "&sdate=" + sdate + "&ShowHistory=no");
            }

            return false;
        }
        $(document).ready(function () {
            $(document).click(function (e) {
                var ClickControlId = e.target.id;
                if (ClickControlId.indexOf("pnlViewPopup") >= 0 || ClickControlId.indexOf("pnlLeaveRange") >= 0) {
                    var which = document.getElementById(ClickControlId);
                    if (which.style.display == "block") {
                        var myFrm = document.getElementById('<%=ifrmViewPopup.ClientID %>');
                        myFrm.src = "";
                        which.style.display = "none";
                        var hdnEMPID = document.getElementById('<%=hdnSelectedEmpID.ClientID %>');
                        var hdnFromDT = document.getElementById('<%=hdnFromDT.ClientID %>');
                        var hdnToDT = document.getElementById('<%=hdnToDT.ClientID %>');
                        var hdnType = document.getElementById('<%=hdnType.ClientID %>');
                        var hdnReportTo = document.getElementById('<%=hdnReportTo.ClientID %>');
                        $.ajax({
                            type: "POST",
                            url: "empDailyAttendanceCorrection.aspx/GetDynamicContent",
                            contentType: "application/json; charset=utf-8",
                            data: "{_SelectedEmpID:'" + hdnEMPID.value + "', _FromDT:'" + hdnFromDT.value + "', _ToDT:'" + hdnToDT.value + "', _Type:'" + hdnType.value + "', _ReportTo:'" + hdnReportTo.value + "'}",
                            dataType: "json",
                            success: AjaxSucceeded,
                            error: AjaxFailed
                        });
                    }
                }
                else if (IsValidDate(ClickControlId.replace(/_/g, "-")) && ClickControlId != "") {
                    var grid = $find("<%=RadEmpAttendance.ClientID %>");
                    var masterTable = grid.get_masterTableView();
                    var cell, row, empid;
                    cell = e.target.parentElement;

                    if (cell != null) {
                        row = cell.parentElement;
                        var td = getFirstChild(row);
                        if (td != null) {
                            var item = getFirstChild(td);
                            if (item != null) {
                                if (item.id.indexOf("hdnEMPID") >= 0 && item.type == "hidden") {
                                    OpenBU(item.value, ClickControlId.replace(/_/g, "-"), "")
                                    return false;
                                }

                            }
                        }
                    }
                }
                <%--else if (ClickControlId.indexOf("imgEmpDetailView") >= 0 && ClickControlId != "") {
                    var cell, row, empid;
                    cell = e.target.parentElement;
                    if (cell != null) {
                        row = cell.parentElement;
                        var td = row.firstChild;
                        if (td != null) {
                            var item = getFirstChild(td);
                            if (item != null) {
                                if (item.id.indexOf("hdnEMPID") >= 0 && item.type == "hidden") {
                                    var hdnFromDT = document.getElementById('<%=hdnFromDT.ClientID %>');
                                        OpenEmployeeDetailScreen(item.value, hdnFromDT.value, "")
                                        return false;
                                    }

                                }
                            }
                        }
                    }--%>
            });

            function AjaxSucceeded(result) {
                if (result != null) {
                    var grid = $find("<%=RadEmpAttendance.ClientID %>");
                    var masterTable = grid.get_masterTableView();
                    var row;
                    var ReturnVal = toString(result);
                    var DayLeav;
                    DayLeav = ReturnVal.split('@');
                    var DayStat;
                    var hdnEMPID;
                    var cellNo, rowNo, colCount, EditedCellStr;
                    var hdnEditedCells = document.getElementById('<%=hdnEditedCells.ClientID %>');
                    for (rowNo = 0; rowNo < masterTable.get_dataItems().length; ++rowNo) {
                        row = masterTable.get_dataItems()[rowNo];
                        hdnEMPID = row.findElement("hdnEMPID");
                        if (hdnEMPID != null) {
                            if (hdnEMPID.value == DayLeav[0]) {
                                var gridItemElement = masterTable.get_dataItems()[rowNo].get_element();
                                if (gridItemElement != null) {
                                    colCount = gridItemElement.cells.length;
                                    DayStat = DayLeav[1].split('|');
                                    if (DayStat.length > 0) {
                                        for (cellNo = 0; cellNo < DayStat.length; ++cellNo) {
                                            if (colCount >= cellNo + 3) {
                                                var OldVal;
                                                OldVal = ">" + gridItemElement.cells[cellNo + 3].innerText + "<";
                                                OldVal = OldVal.replace(/ /g, "");
                                                EditedCellStr = '|' + hdnEMPID.value + '@' + (cellNo + 3) + '|'
                                                if (DayStat[cellNo].replace(/ /g, "").indexOf(OldVal) <= 0) {
                                                    hdnEditedCells.value = hdnEditedCells.value + EditedCellStr;
                                                }
                                                gridItemElement.cells[cellNo + 3].innerHTML = DayStat[cellNo];
                                                if (hdnEditedCells.value.indexOf(EditedCellStr) >= 0) {
                                                    gridItemElement.cells[cellNo + 3].className = 'HighLight';
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            function AjaxFailed(result) {
                alert("failed");
            }
            function ResetStatus(result) {
                alert("failed");
            }
        });
        function IsValidDate(myDate) {
            var filter = /^([012]?\d|3[01])-([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][u]l|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][tT]|[Nn][oO][Vv]|[Dd][Ee][Cc])-(19|20)\d\d$/
            return filter.test(myDate);
        }
        function getFirstChild(el) {
            var firstChild = el.firstChild;
            while (firstChild != null && firstChild.nodeType == 3) { // skip TextNodes
                firstChild = firstChild.nextSibling;
            }
            return firstChild;
        }

        function getScreenResolution() {
            var res = $("#<%=hdnResControl.ClientID %>");
            res.val(screen.width + "|" + screen.height);
        }

        function PlaceControlAtCentre(mWidth, mHeight) {
            var myDiv = $("#<%=divViewPopup.ClientID %>");
            var leftMargin, TopMargin, mPaperSizeX, mPaperSizeY, CalcWidth, CalcHeight

            CalcWidth = (90 * screen.width / 100);
            if (mWidth >= CalcWidth)
                mPaperSizeX = CalcWidth;
            else
                mPaperSizeX = mWidth;

            CalcHeight = (90 * screen.height / 100);

            if (mHeight >= CalcHeight)
                mPaperSizeY = CalcHeight;
            else
                mPaperSizeY = mHeight;

            var scrOfY = 0;
            var scrOfX = 0;
            if (typeof (window.pageYOffset) == 'number') {
                //Netscape compliant
                scrOfY = window.pageYOffset;
                scrOfX = window.pageXOffset;
            } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                //DOM compliant
                scrOfY = document.body.scrollTop;
                scrOfX = document.body.scrollLeft;
            } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                //IE6 standards compliant mode
                scrOfY = document.documentElement.scrollTop;
                scrOfX = document.documentElement.scrollLeft;
            }

            leftMargin = (screen.width - mPaperSizeX) / 2;
            TopMargin = (screen.height - mPaperSizeY) / 2;
            myDiv.css("position", "absolute");
            myDiv.css("top", TopMargin + scrOfY - 50);
            myDiv.css("left", leftMargin + scrOfX);
            myDiv.css("width", mWidth);
            myDiv.css("height", mHeight);
            myDiv.css("z-index", "1000");
        }

        
    </script>


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calendar mr-3"></i> <asp:Label ID="lblType" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
               
   <!-- content start -->
    <table align="center" width="100%">
        <tr valign="top">
            <td align="left" valign="middle">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <%--<a href style="border-width: 0px; padding: 0px; margin: 0px; background-color: #CCFFCC; color: #008000; width: 100%; height: 100%;"></a>--%>
            </td>
        </tr>
        
        <tr>
            <td>
                <table width="100%" align="left">
                    <tr id="trSuperUser" runat="server">
                        <td align="left" width="20%"><span class="field-label">Approver</span> 
                        </td>
                        
                        <td align="left" width="25%">
                            <telerik:RadComboBox ID="RadReportTo" runat="server" Width="100%" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False"  AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%"></td>
                        <td width="25%"></td>
                        <td width="10%"></td>
                    </tr>
                    <tr id="trFilter" runat="server">
                        <td align="left"><span class="field-label">From Date</span>
                        </td>
                        
                        <td align="left">
                            <telerik:RadDatePicker ID="RadAttDate" runat="server" DateInput-EmptyMessage="FromDate">
                                <DateInput ID="DateInput1" EmptyMessage="FromDate" DateFormat="dd/MMM/yyyy" runat="server">
                                </DateInput>
                                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                    ViewSelectorText="x" runat="server">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDatePicker>
                        </td>
                        <td align="left"><span class="field-label">To Date</span>
                        </td>
                       
                        <td align="left">
                            <telerik:RadDatePicker ID="RadAttToDate" runat="server" DateInput-EmptyMessage="ToDate">
                                <DateInput ID="DateInput2" EmptyMessage="ToDate" DateFormat="dd/MMM/yyyy" runat="server">
                                </DateInput>
                                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                    ViewSelectorText="x" runat="server">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDatePicker>
                        </td>
                        <td width="10%"></td>
                    </tr>
                    <tr id="trCategory" runat="server">
                        <td align="left"><span class="field-label">Category</span>
                        </td>
                       
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="RadCategory" runat="server" RenderMode="Lightweight" Width="100%" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False">
                            </telerik:RadComboBox>
                        </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" />
                        </td>
                        <td width="10%"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Employee No</span>
                        </td>
                       
                        <td align="left">
                            <asp:TextBox ID="txtEmpNo" runat="server" CssClass="RadLabelText" >
                            </asp:TextBox>
                        </td>
                        <td  align="left"><span class="field-label">Employee Name</span>
                        </td>
                        
                        <td  align="left">
                            <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="RadLabelText"
                                >
                            </asp:TextBox>
                        </td>
                        <td  align="left">
                            <asp:Button ID="btnSearch" runat="server" CssClass="RadButton" SkinID="ButtonNormal"
                                Text="Search" />
                        </td>                        
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <telerik:RadGrid ID="RadEmpAttendance" runat="server" AllowSorting="True" enableajax="True" OnNeedDataSource="RadEmpAttendance_NeedDataSource"
                    ShowStatusBar="True" GroupingEnabled="False" ShowGroupPanel="True" OnItemDataBound="RadEmpAttendance_ItemDataBound"
                    GridLines="None" AllowPaging="True" PageSize="50" Width="100%">
                    <%--<AlternatingItemStyle CssClass="RadGridAltRow" />--%>
                    <ItemStyle CssClass="RadGridRow" />
                    <PagerStyle AlwaysVisible="True" />
                    <MasterTableView GridLines="Both" Width="100%">
                        <ExpandCollapseColumn Visible="False">
                            <HeaderStyle Width="19px" />
                        </ExpandCollapseColumn>
                        <RowIndicatorColumn Visible="False">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <DetailTables>
                        </DetailTables>
                        <ExpandCollapseColumn Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="colImgDetailView">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnEMPID" runat="server" Value='<%# Eval("EMP_ID") %>' />
                                    <asp:ImageButton ID="imgEmpDetailView" runat="server" Height="14px" ImageUrl="~/Images/ButtonImages/PROFILE.png"
                                        ToolTip="Click to view the Leave details." Width="14px" />
                                </ItemTemplate>
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="ImageButton">
                            </EditColumn>
                        </EditFormSettings>
                        <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                    </MasterTableView>
                    <HeaderStyle Font-Bold="True"  HorizontalAlign="left" />
                    <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                    <ClientSettings EnableRowHoverStyle="True" AllowColumnsReorder="True">
                        <Scrolling FrozenColumnsCount="3" SaveScrollPosition="true" />
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <asp:Label ID="lblErrorFooter" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">&nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Go Back" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%">
                    <tr>
                        <td style="vertical-align: top; text-align: left">
                            <asp:Label ID="lblLegandProcessed1" runat="server" CssClass="error" ForeColor="#BB7100"
                                BackColor="#BB7100">AA</asp:Label>
                            <asp:Label ID="lblLegandProcessed" runat="server" CssClass="RadLabel" ForeColor="Black">Attendance Not  Processed</asp:Label>
                            
                            <asp:Label ID="lblLegandProcessed2" runat="server" CssClass="error" ForeColor="#1B80B6"
                                BackColor="#1B80B6">AA</asp:Label>
                            <asp:Label ID="lblLegandProcessed0" runat="server" CssClass="RadLabel" ForeColor="Black">Attendance  Processed</asp:Label>
                            
                            <asp:Label ID="lblLegandProcessed3" runat="server" CssClass="error" ForeColor="Green"
                                BackColor="Green">AA</asp:Label>
                            <asp:Label ID="lblLegandLeaveAvailable" runat="server" CssClass="RadLabel" ForeColor="Black">Leave Available (Processed)</asp:Label>
                            
                            <asp:Label ID="lblLegandProcessed4" runat="server" CssClass="error" ForeColor="Red"
                                BackColor="Red">AA</asp:Label>
                            <asp:Label ID="lblLegandLeaveNotAvailable" runat="server" CssClass="RadLabel" ForeColor="Black">No Balance Leave Available (Processed)</asp:Label>
                        </td>
                        <td align="left" style="vertical-align: top; text-align: left">
                            <asp:Label ID="lblLeaveLegand" runat="server" CssClass="RadLabel" ForeColor="Black">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr id="trCate" runat="server">
                        <td style="vertical-align: top; text-align: left">
                            <table>
                                <tr>
                                    <td><span class="field-label">Other Categories :</span>
                                    </td>
                                    <td>
                                        <asp:DataList ID="dlOtherCategories" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="h_ECTID" runat="server" Value='<%#Container.DataItem("ECT_ID")%>' />
                                                <asp:LinkButton ID="hlECTDESCR" OnClick="lnkView_Click" runat="server" Text='<%#Container.DataItem("ECT_DESCR")%>'></asp:LinkButton>
                                                <asp:Label ID="lblSeperator" runat="server" CssClass="RadLabel" Text="___" ForeColor="White"></asp:Label>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left">&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="100%" align="center">
                <asp:Panel ID="pnlViewPopup" runat="server" CssClass="RadDarkPanlvisible" Style="display: none">
                    <div class="RadPanelQual" runat="server" id="divViewPopup">
                        <table width="100%">
                            <tr>
                                <td class="panel-cover">
                                     <asp:button ID="btClose"  type="button"  runat="server" 
                             style="float:right;margin-top:-1px;margin-right:-1px;font-size:14px;color:white ;border:1px solid red; 
border-radius:10px 10px; background-color :red ; " ForeColor="White"  Text="X" ></asp:button>
                                    <iframe id="ifrmViewPopup" runat="server"></iframe>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnResControl" runat="server" />
    <asp:HiddenField ID="hdnSelectedEmpID" runat="server" />
    <asp:HiddenField ID="hdnFromDT" runat="server" />
    <asp:HiddenField ID="hdnToDT" runat="server" />
    <asp:HiddenField ID="hdnType" runat="server" />
    <asp:HiddenField ID="hdnReportTo" runat="server" />
    <asp:HiddenField ID="hdnCATID" runat="server" />
    <asp:HiddenField ID="hdnLastLoadedID" runat="server" />
    <asp:HiddenField ID="hdnEditedCells" runat="server" />
    <asp:HiddenField ID="hdnHREntry" runat="server" />
</asp:Content>
