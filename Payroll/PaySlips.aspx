<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaySlips.aspx.vb" Inherits="Payroll_PaySlips" MasterPageFile="~/mainMasterPage.master" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script lang="javascript" type="text/javascript">


        var nW, nH, oH, oW;
        function zoomToggle(iWideSmall, iHighSmall, iWideLarge, iHighLarge, whichImage) {
            oW = whichImage.style.width; oH = whichImage.style.height;
            if ((oW == iWideLarge) || (oH == iHighLarge)) {
                nW = iWideSmall; nH = iHighSmall;
            } else {
                nW = iWideLarge; nH = iHighLarge;
            }
            whichImage.style.width = nW; whichImage.style.height = nH;
        }

    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        .PaySlip {
            width: 25%;
            padding: 30px;
        }

        .CalEnabled {
            color: #8DC24C;
            cursor: pointer;
            font-size: 90px;
        }

        .CalDisabled {
            color: gray;
            cursor: not-allowed;
            font-size: 90px;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Generate PaySlip For Required Month
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" style="width: 100%;">
                    <tr align="left">
                        <td>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table runat="server" id="tblMain" width="100%">
                    <tr>
                        <td align="left" style="width: 20%;"><span class="field-label">Select Year</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlPayYear" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td width="30%"></td>
                        <td style="width: 20%;"></td>
                    </tr>
                    <tr>
                        <td colspan="4">Click on any of the month to download payslip.      
                            
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%;" colspan="4">
                            <table style="width: 100%;" align="center">
                                <tr>
                                    <td class="PaySlip" align="center">
                                        <div id="divJan" runat="server">
                                            <asp:LinkButton ID="btnJan" runat="server" CssClass="imageiconlink" ToolTip="Payslip for January">
                                                <i id="iJan" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">JANUARY</span>
                                        </div>
                                        <%--<asp:ImageButton ID="imgJan" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Jan.jpg"></asp:ImageButton>--%>
                                    </td>
                                    <td class="PaySlip" align="center">
                                        <div id="divFeb" runat="server">
                                            <asp:LinkButton ID="btnFeb" runat="server" CssClass="imageiconlink" ToolTip="Payslip for February">
                                                <i id="iFeb" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">FEBRUARY</span>
                                        </div>
                                        <%--<asp:ImageButton ID="ImgFeb" runat="server" class="fa fa-calendar" />--%>
                                    </td>
                                    <td style="width: 20%" align="center">
                                        <div>
                                            <asp:LinkButton ID="btnMar" runat="server" CssClass="imageiconlink" ToolTip="Payslip for March">
                                                <i id="iMar" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">MARCH</span>
                                        </div>
                                        <%--<asp:ImageButton ID="ImgMar" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Mar.jpg" />--%>
                                    </td>
                                    <td class="PaySlip" align="center">
                                        <div>
                                            <asp:LinkButton ID="btnApr" runat="server" CssClass="imageiconlink" ToolTip="Payslip for April">
                                                <i id="iApr" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">APRIL</span>
                                        </div>
                                        <%--<asp:ImageButton ID="ImgApr" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Apr.jpg" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PaySlip" align="center">
                                        <div>
                                            <asp:LinkButton ID="btnMay" runat="server" CssClass="imageiconlink" ToolTip="Payslip for May">
                                                <i id="iMay" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">MAY</span>
                                        </div>
                                        <%--<asp:ImageButton ID="ImgMay" runat="server"
                                            ImageUrl="~/Images/ImageMonths/May.jpg" />--%>
                                    </td>
                                    <td class="PaySlip" align="center">
                                        <%--<asp:ImageButton ID="ImgJun" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Jun.jpg" />--%>
                                        <div>
                                            <asp:LinkButton ID="btnJun" runat="server" CssClass="imageiconlink" ToolTip="Payslip for June">
                                                <i id="iJun" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">JUNE</span>
                                        </div>
                                    </td>
                                    <td class="PaySlip" align="center">
                                        <%--<asp:ImageButton ID="ImgJul" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Jul.jpg" />--%>
                                        <div>
                                            <asp:LinkButton ID="btnJul" runat="server" CssClass="imageiconlink" ToolTip="Payslip for July">
                                                <i id="iJul" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">JULY</span>
                                        </div>
                                    </td>
                                    <td class="PaySlip" align="center">
                                        <%--<asp:ImageButton ID="ImgAug" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Aug.jpg" />--%>
                                        <div>
                                            <asp:LinkButton ID="btnAug" runat="server" CssClass="imageiconlink" ToolTip="Payslip for August">
                                                <i id="iAug" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">AUGUST</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="PaySlip" align="center">
                                        <%-- <asp:ImageButton ID="ImgSep" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Sep.jpg" />--%>
                                        <div>
                                            <asp:LinkButton ID="btnSep" runat="server" CssClass="imageiconlink" ToolTip="Payslip for September">
                                                <i id="iSep" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">SEPTEMBER</span>
                                        </div>
                                    </td>
                                    <td align="center" class="PaySlip">
                                        <%-- <asp:ImageButton ID="ImgOct" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Oct.jpg" />--%>
                                        <div>
                                            <asp:LinkButton ID="btnOct" runat="server" CssClass="imageiconlink" ToolTip="Payslip for October">
                                                <i id="iOct" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">OCTOBER</span>
                                        </div>
                                    </td>
                                    <td align="center" class="PaySlip">
                                        <%--<asp:ImageButton ID="ImgNov" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Nov.jpg" />--%>
                                        <div>
                                            <asp:LinkButton ID="btnNov" runat="server" CssClass="imageiconlink" ToolTip="Payslip for November">
                                                <i id="iNov" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">NOVEMBER</span>
                                        </div>
                                    </td>
                                    <td align="center" class="PaySlip">
                                        <div>
                                            <asp:LinkButton ID="btnDec" runat="server" CssClass="imageiconlink" ToolTip="Payslip for December">
                                                <i id="iDec" runat="server"></i>
                                            </asp:LinkButton><br />
                                            <span class="field-label">DECEMBER</span>
                                        </div>
                                        <%--<asp:ImageButton ID="ImgDec" runat="server"
                                            ImageUrl="~/Images/ImageMonths/Dec.jpg" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Back" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="h_Emp_No" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="h_EmpNoDesc" runat="server"></asp:HiddenField>
    <asp:Label ID="lblSTSMsg" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="10pt"
        ForeColor="Maroon" Text="Please note that Payslip won't be having GEMS logo when printed from this unit."
        Visible="False" Width="458px"></asp:Label>

</asp:Content>
