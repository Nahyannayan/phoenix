<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empAirfare.aspx.vb" Inherits="Payroll_empAirfare" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <script language="javascript" type="text/javascript">
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblHead" runat="server" Text="Air Fare Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Air Fare Class</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddAirfareclass" runat="server">
                                <asp:ListItem Value="E">Economy</asp:ListItem>
                                <asp:ListItem Value="B">Business</asp:ListItem>
                                <asp:ListItem Value="F">First Class</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Age Group</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddAgegroup" runat="server">
                                <asp:ListItem>Infants</asp:ListItem>
                                <asp:ListItem>Adults </asp:ListItem>
                                <asp:ListItem>Child</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Select Date</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFrom" runat="server" Width="112px"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left" class="matters"><span class="field-label">Select  City</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddCity" runat="server" DataSourceID="SqlDataSource1" DataTextField="CIT_DESCR" DataValueField="CIT_ID">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td class="matters" align="left"><span class="field-label">Amount</span></td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtAmount" runat="server" Width="112px"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" />
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT [CIT_ID], [CIT_DESCR] FROM [CITY_M]"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr class="title-bg" runat="server" id="tr_Deatailhead">
                        <td colspan="4" align="left" style="height: 20px">Air Fare Details </td>
                    </tr>
                    <tr runat="server" id="tr_Deatails">
                        <td colspan="4" class="matters" align="center">
                            <asp:GridView ID="gvAttendance" runat="server" CssClass="table table-row table-bordered" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added">
                                <Columns>
                                    <asp:BoundField DataField="Fdate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FareClassname" HeaderText="Fare Class">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AgeGroup" HeaderText="Age Group">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Cityname" HeaderText="City Name">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:TemplateField ShowHeader="False" Visible="False">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                                Text="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button
                                ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" /><asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
