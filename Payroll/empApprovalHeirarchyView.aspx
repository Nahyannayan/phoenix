<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empApprovalHeirarchyView.aspx.vb" Inherits="Payroll_empApprovalHeirarchyView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">


        function ShowLeaveDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowEmpLeave.aspx?ela_id=" + id, "", sFeatures)

            return false;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Approval Heirarchy
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td valign="top"  align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink> 
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
                        <td align="right">&nbsp;</td>
                    </tr>
                </table>
                <a id='top'></a>
                <table runat="server" align="center"
                    cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_img" valign="top">
            <td align="left" colspan="9" valign="middle">Approval Heirarchy</td>
        </tr>--%>
                    <tr>
                        <td align="center" valign="top"  >
                            <asp:GridView ID="gvApprovalPolicy" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <RowStyle CssClass="griditem"  />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <Columns>
                                    <asp:TemplateField HeaderText="LPS_ID" InsertVisible="False" SortExpression="LPS_ID"
                                        Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLPS_ID" runat="server" Text='<%# Bind("LPS_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="LPS_DESCRIPTION" HeaderText="Description" SortExpression="LPS_DESCRIPTION" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


