Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data


Partial Class Payroll_empQualification
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If

        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")

        If Not IsPostBack Then
            ViewState("datamode") = Encr_decrData.Decrypt(IIf(Request.QueryString("datamode") IsNot Nothing, Request.QueryString("datamode").Replace(" ", "+"), String.Empty))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(IIf(Request.QueryString("MainMnu_code") IsNot Nothing, Request.QueryString("MainMnu_code").Replace(" ", "+"), String.Empty))
            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then
                ViewState("Eid") = Encr_decrData.Decrypt(IIf(Request.QueryString("Eid") IsNot Nothing, Request.QueryString("Eid").Replace(" ", "+"), ""))
            End If

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MenuEMPQUALIFICATION_CAT_M _
             And ViewState("MainMnu_code") <> OASISConstants.MenuEMPQUALIFICATION_M _
             And ViewState("MainMnu_code") <> OASISConstants.MenuEMPSUBJECT_M) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MenuEMPQUALIFICATION_CAT_M
                        SetEmpQualification_Cat_M()
                    Case OASISConstants.MenuEMPQUALIFICATION_M
                        SetEmpQualification_M()
                    Case OASISConstants.MenuEMPSUBJECT_M
                        SetSubject_M()
                End Select
            End If
        End If
        'disable the control buttons based on the rights
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        'Make the Controls readOnly if dataMode is View
        If ViewState("datamode") = "view" Then
            MakeControlsReadOnly(True)
        Else
            UtilityObj.beforeLoopingControls(Me.Page)
            MakeControlsReadOnly(False)
        End If

    End Sub

    Private Sub SetSubject_M()
        trQualification.Visible = False
        trSubject.Visible = False
        'trSubDescr.Visible = False
        'txtQDescr.ReadOnly = True
        lblMainDescription.Text = "ID"
        lblSubDescription.Text = "Description"
        lblFormCaption.Text = "Subject Master"
        txtShortDescr.MaxLength = 50

        If ViewState("datamode") <> "add" And ViewState("Eid") IsNot Nothing Then
            SetViewDataEmpSubject_M(ViewState("Eid"))
        Else
            ClearScreen()
        End If
    End Sub

    Private Sub SetViewDataQualification_M(ByVal Eid As Integer)
        'Get the Data Here
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        'Dim str_Sql As String = "SELECT EMPQUALIFICATION_M.QLF_DESCR, EMPQUALIFICATION_CAT_M.QCT_Descr, " & _
        '"SUBJECT_M.SUB_DESCR FROM EMPQUALIFICATION_M LEFT OUTER JOIN EMPQUALIFICATION_CAT_M " & _
        '"ON EMPQUALIFICATION_M.QLF_QCT_ID = EMPQUALIFICATION_CAT_M.QCT_ID LEFT OUTER JOIN " & _
        '"SUBJECT_M ON EMPQUALIFICATION_M.QLF_SUB_ID = SUBJECT_M.SUB_ID WHERE QLF_ID = " & Eid
        Dim str_Sql As String = "SELECT QLF_DESCR, QLF_QCT_ID, QLF_SUB_ID FROM EMPQUALIFICATION_M WHERE QLF_ID = " & Eid
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            txtQDescr.Text = dr("QLF_DESCR")
            ddQualification.SelectedValue = dr("QLF_QCT_ID")
            ddSubject.SelectedItem.Value = dr("QLF_SUB_ID")
        End While
    End Sub

    Private Sub SetEmpQualification_M()
        'trQualification.Visible = True
        'trSubject.Visible = True
        trSubDescr.Visible = False
        lblMainDescription.Text = "Qualification Description"
        lblFormCaption.Text = "Employee Qualification"
        BindQualification()
        BindSubject()

        If ViewState("datamode") <> "add" And ViewState("Eid") IsNot Nothing Then
            SetViewDataQualification_M(ViewState("Eid"))
        Else
            ClearScreen()
        End If
    End Sub

    Private Sub SetViewDataEmpSubject_M(ByVal Eid As String)
        'Get the Data Here
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_Sql As String = "SELECT SBM_ID as ID, SBM_DESCR as DESCR FROM SUBJECT_M WHERE SBM_ID = '" & Eid & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            txtQDescr.Text = dr("ID")
            txtShortDescr.Text = dr("DESCR")
        End While
    End Sub

    Private Sub SetEmpQualification_Cat_M()
        trQualification.Visible = False
        trSubject.Visible = False
        lblMainDescription.Text = "Qualification Description"
        lblSubDescription.Text = "Short Description"
        lblFormCaption.Text = "Employee Qualification Category"
        If ViewState("datamode") <> "add" And ViewState("Eid") IsNot Nothing Then
            SetViewDataEmpQualification_Cat_M(ViewState("Eid"))
        Else
            ClearScreen()
        End If
    End Sub

    Private Sub SetViewDataEmpQualification_Cat_M(ByVal Eid As Integer)
        'Get the Data Here
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT QCT_Descr as DESCR, QCT_SHORT as SEC_DESCR FROM EMPQUALIFICATION_CAT_M WHERE QCT_ID = " & Eid
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr.Read())
            txtQDescr.Text = dr("DESCR")
            txtShortDescr.Text = dr("SEC_DESCR")
        End While
    End Sub

    Private Sub MakeControlsReadOnly(ByVal ctrlReadOnly As Boolean)
        txtQDescr.ReadOnly = ctrlReadOnly
        'If ViewState("MainMnu_code") = OASISConstants.MenuEMPSUBJECT_M Then
        '    txtQDescr.ReadOnly = True
        'End If
        txtShortDescr.ReadOnly = ctrlReadOnly
        ddQualification.Enabled = Not ctrlReadOnly
        ddSubject.Enabled = Not ctrlReadOnly
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            Dim bUpdated As Boolean
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MenuEMPQUALIFICATION_CAT_M
                    lblError.Text = SaveEmpQualification_CAT_M(bUpdated)
                Case OASISConstants.MenuEMPSUBJECT_M
                    lblError.Text = SaveSubject_M(bUpdated)
                Case OASISConstants.MenuEMPQUALIFICATION_M
                    lblError.Text = SaveEMPQUALIFICATION_M(bUpdated)
            End Select
            If bUpdated Then
                ViewState("datamode") = "none"
                MakeControlsReadOnly(False)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ClearScreen()
            End If
        End If
    End Sub

    Private Sub ClearScreen()
        txtQDescr.Text = ""
        txtShortDescr.Text = ""
        'If ViewState("MainMnu_code") = OASISConstants.MenuEMPSUBJECT_M Then
        '    txtQDescr.Text = "AutoGenerated"
        '    txtQDescr.ReadOnly = True
        'End If
        If ViewState("MainMnu_code") = OASISConstants.MenuEMPQUALIFICATION_M Then
            BindQualification()
            BindSubject()
        End If
    End Sub

    Private Sub BindQualification()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT QCT_ID, QCT_SHORT as DESCR FROM EMPQUALIFICATION_CAT_M "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddQualification.DataSource = dr
        ddQualification.DataTextField = "DESCR"
        ddQualification.DataValueField = "QCT_ID"
        ddQualification.DataBind()
    End Sub

    Private Sub BindSubject()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT SBM_ID as SUB_ID,SBM_DESCR as SUB_DESCR  FROM SUBJECT_M "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddSubject.DataSource = dr
        ddSubject.DataTextField = "SUB_DESCR"
        ddSubject.DataValueField = "SUB_ID"
        ddSubject.DataBind()
    End Sub

    Private Function SaveEMPQUALIFICATION_M(ByRef bUpdated As Boolean) As String
        bUpdated = False
        Dim transaction As SqlTransaction
        Try
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("InsertEmpQualification")
                Try
                    Dim cmd As New SqlCommand("Save_EMPQUALIFICATION_M", conn, transaction)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpQLF_ID As New SqlParameter("@QLF_ID", SqlDbType.Int)
                    If ViewState("datamode") = "add" Then
                        sqlpQLF_ID.Value = 0
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpQLF_ID.Value = ViewState("Eid")
                    End If
                    cmd.Parameters.Add(sqlpQLF_ID)

                    Dim sqlpQLF_DESCR As New SqlParameter("@QLF_DESCR", SqlDbType.VarChar, 100)
                    sqlpQLF_DESCR.Value = txtQDescr.Text
                    cmd.Parameters.Add(sqlpQLF_DESCR)

                    Dim sqlpQLF_QCT_ID As New SqlParameter("@QLF_QCT_ID", SqlDbType.VarChar, 100)
                    sqlpQLF_QCT_ID.Value = ddQualification.SelectedValue
                    cmd.Parameters.Add(sqlpQLF_QCT_ID)

                    Dim sqlpQLF_SUB_ID As New SqlParameter("QLF_SUB_ID", SqlDbType.VarChar, 100)
                    sqlpQLF_SUB_ID.Value = ddSubject.SelectedValue
                    cmd.Parameters.Add(sqlpQLF_SUB_ID)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    Dim str_mode As String = String.Empty
                    If ViewState("datamode") = "add" Then
                        sqlpbEdit.Value = False
                        str_mode = "Insert"
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                        str_mode = "Edit"
                    End If

                    cmd.Parameters.Add(sqlpbEdit)

                    If cmd.ExecuteNonQuery() = 1 Then
                        bUpdated = True
                        transaction.Commit()
                        If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), str_mode, Page.User.Identity.Name.ToString, Me.Page) <> 0 Then
                            Return "Data Saved...Failed to Update Audit Trail"
                        End If
                        Return "The Data successfully saved"
                    Else
                        transaction.Rollback()
                        Return "Failed to Save the Data"
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    UtilityObj.Errorlog(ex.Message)
                    Return "Failed to Save the Data"
                End Try
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ex.Message
            Return "Failed to Save the Data"
        End Try
    End Function

    Private Function SaveSubject_M(ByRef bUpdated As Boolean) As String
        bUpdated = False
        Dim transaction As SqlTransaction
        Try
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("InsertEmpSubject")
                Try
                    Dim cmd As New SqlCommand("SP_SUBJECT_M", conn, transaction)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpSUB_ID As New SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
                    If ViewState("datamode") = "add" Then
                        sqlpSUB_ID.Value = 0
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpSUB_ID.Value = ViewState("Eid")
                    End If
                    cmd.Parameters.Add(sqlpSUB_ID)

                    Dim sqlpSUB_DESCR As New SqlParameter("@SUB_DESCR", SqlDbType.VarChar, 50)
                    sqlpSUB_DESCR.Value = txtShortDescr.Text
                    cmd.Parameters.Add(sqlpSUB_DESCR)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    Dim str_mode As String = String.Empty

                    If ViewState("datamode") = "add" Then
                        sqlpbEdit.Value = False
                        str_mode = "Insert"
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                        str_mode = "Edit"
                    End If

                    cmd.Parameters.Add(sqlpbEdit)

                    If cmd.ExecuteNonQuery() = 1 Then
                        bUpdated = True
                        transaction.Commit()
                        If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), str_mode, Page.User.Identity.Name.ToString, Me.Page) <> 0 Then
                            Return "Data Saved...Failed to Update Audit Trail"
                        End If
                        Return "The Data successfully saved"
                    Else
                        transaction.Rollback()
                        Return "Failed to Save the Data"
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    UtilityObj.Errorlog(ex.Message)
                    Return "Failed to Save the Data"
                End Try
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ex.Message
            Return "Failed to Save the Data"
        End Try
    End Function

    Private Function SaveEmpQualification_CAT_M(ByRef bUpdated As Boolean) As String
        bUpdated = False
        Dim transaction As SqlTransaction
        Try
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("InsertEmpQualification")
                Try
                    Dim cmd As New SqlCommand("Save_EMPQUALIFICATION_CAT_M", conn, transaction)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpQCT_ID As New SqlParameter("@QCT_ID", SqlDbType.Int)
                    If ViewState("datamode") = "add" Then
                        sqlpQCT_ID.Value = 0
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpQCT_ID.Value = ViewState("Eid")
                    End If
                    cmd.Parameters.Add(sqlpQCT_ID)

                    Dim sqlpQCT_Descr As New SqlParameter("@QCT_Descr", SqlDbType.VarChar, 100)
                    sqlpQCT_Descr.Value = txtQDescr.Text
                    cmd.Parameters.Add(sqlpQCT_Descr)

                    Dim sqlpQCT_SHORT As New SqlParameter("@QCT_SHORT", SqlDbType.VarChar, 10)
                    sqlpQCT_SHORT.Value = txtShortDescr.Text
                    cmd.Parameters.Add(sqlpQCT_SHORT)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    Dim str_mode As String = String.Empty

                    If ViewState("datamode") = "add" Then
                        sqlpbEdit.Value = False
                        str_mode = "Insert"
                    ElseIf ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                        str_mode = "Edit"
                    End If

                    cmd.Parameters.Add(sqlpbEdit)
                    If cmd.ExecuteNonQuery() = 1 Then
                        bUpdated = True
                        transaction.Commit()
                        'Audit trial
                        If UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), str_mode, Page.User.Identity.Name.ToString, Me.Page) <> 0 Then
                            Return "Data Saved...Failed to Update Audit Trail"
                        End If
                        Return "The Data successfully saved"
                    Else
                        transaction.Rollback()
                        Return "Failed to Save the Data"
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    UtilityObj.Errorlog(ex.Message)
                    Return "Failed to Save the Data"
                End Try
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ex.Message
            Return "Failed to Save the Data"
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If Request.UrlReferrer IsNot Nothing Then
        '    Response.Redirect(Request.UrlReferrer.ToString())
        'Else
        '    Response.Redirect("../Homepage.aspx")
        'End If
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearScreen()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        MakeControlsReadOnly(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        MakeControlsReadOnly(False)
        ClearScreen()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    
End Class
