﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empVisa_LC_Exit_View.aspx.vb" Inherits="Payroll_empVisa_LC_Exit_View" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblTitle" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table align="center" width="100%">
            <tr valign="top" >          
                <td valign="top" width="50%" align="left" >
                <asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink>
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
                    <td align="right">
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    </td>               
            </tr>
        </table>
    <table align="center" width="100%">
            
           
            <tr>
                <td align="center" valign="top" >
                <br />
                    <asp:GridView ID="gvJournal" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                        EmptyDataText="No Loan Applications found" Width="100%" AllowPaging="True" PageSize="30" EnableModelValidation="True">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="Emp No" SortExpression="EMPNO">
                                <HeaderTemplate>
                                    Emp No<br />
                                    <asp:TextBox ID="txtEmpNo" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Emp Name" SortExpression="EMP_NAME">
                                <HeaderTemplate>
                                    Emp Name<br />
                                    <asp:TextBox ID="txtEmpname" runat="server"  Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                          
                             
                         
                            
                            <asp:BoundField DataField="USR_NAME" HeaderText="Cancel By" 
                                 />
                            <asp:BoundField DataField="TRANS_DATE" HeaderText="Cancel Date" 
                                DataFormatString="{0:dd/MMM/yyyy}" />
                          
                           
                            <asp:TemplateField HeaderText="View">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkView" runat="server">View</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="EMP_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblEMP_ID" runat="server" Text='<%# Bind("EMP_ID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                                
                            
                             
                              <asp:TemplateField HeaderText="Remove<br>Processing">
                                <ItemTemplate>
                                   <asp:LinkButton ID="lnkRemove" Text="Remove" runat="server" 
                                        onclick="lnkRemove_Click"  ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                    </asp:GridView> 
                </td>
            </tr>
        </table>
             </div>
            </div>
        </div>

</asp:Content>

