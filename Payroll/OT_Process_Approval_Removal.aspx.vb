﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports UtilityObj
Partial Class Payroll_OT_Process_Approval_Removal
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(),
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                bind_BusinessUnit()
                h_BSUID.Value = Session("sBsuid")
                ddlBSUinit.Enabled = False
                SetTransferDetail()
                LoadOTDetails()
            End If

            'txtFromDate.Text = Format(New Date(Now.Date.Year, Now.Date.Month, 1), OASISConstants.DateFormat)
            'txtToDate.Text = Format(New Date(Now.Date.Year, Now.Date.Month, Date.DaysInMonth(Now.Date.Year, Now.Date.Month)), OASISConstants.DateFormat)
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If MainMnu_code = "P130028" Then
                lblPageTitle.Text = "Approve Processed Overtime"
                btnOTProcess.Visible = True
                btnOTProcessRemoval.Visible = False
            ElseIf MainMnu_code = "P130029" Then
                lblPageTitle.Text = "Remove Processed Overtime"
                btnOTProcess.Visible = False
                btnOTProcessRemoval.Visible = True
            End If
            FillEmpNames(h_EMPID.Value)
            StoreEMPFilter()
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
            End If
            If h_EMPID.Value.ToString <> "" Then
                btnOTDetails.Visible = True
            Else btnOTDetails.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    Private Sub SetTransferDetail()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(0) As SqlClient.SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSUinit.SelectedItem.Value, SqlDbType.VarChar)
        Dim ds As DataSet
        txtFromDate.Enabled = False
        txtToDate.Enabled = False
        imgFromDate.Enabled = False
        imgToDate.Enabled = False
        lblError.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        ds = Mainclass.getDataSet("Get_BSU_OTFreezeDates", param, str_conn)
        trOTSummary.Visible = False
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("ReturnValue").ToString = "" Then
                    If IsDate(ds.Tables(0).Rows(0).Item("TODT")) Then
                        txtToDate.Text = Format(ds.Tables(0).Rows(0).Item("TODT"), OASISConstants.DateFormat)
                    End If
                    If IsDate(ds.Tables(0).Rows(0).Item("FROMDT")) Then
                        txtFromDate.Text = Format(ds.Tables(0).Rows(0).Item("FROMDT"), OASISConstants.DateFormat)
                    End If
                    If IsNumeric(ds.Tables(0).Rows(0).Item("PAYMONTH")) And IsNumeric(ds.Tables(0).Rows(0).Item("PAYYEAR")) Then
                        'LoadSummary(ds.Tables(0).Rows(0).Item("PAYMONTH"), ds.Tables(0).Rows(0).Item("PAYYEAR"))
                    End If
                    If ds.Tables(0).Rows(0).Item("AllowReTransfer") Then
                        btnOTDetails.Enabled = True
                        btnOTProcess.Enabled = True
                        btnOTProcessRemoval.Enabled = True
                        'FillRemSalary()
                    Else
                        btnOTDetails.Enabled = False
                        btnOTProcess.Enabled = False
                        btnOTProcessRemoval.Enabled = False
                    End If
                Else
                    lblError.Text = ds.Tables(0).Rows(0).Item("ReturnValue").ToString
                    If IsDate(ds.Tables(0).Rows(0).Item("TODT")) Then
                        txtToDate.Text = Format(ds.Tables(0).Rows(0).Item("TODT"), OASISConstants.DateFormat)
                    End If
                    If IsDate(ds.Tables(0).Rows(0).Item("FROMDT")) Then
                        txtFromDate.Text = Format(ds.Tables(0).Rows(0).Item("FROMDT"), OASISConstants.DateFormat)
                    End If
                    btnOTDetails.Enabled = False
                    btnOTProcess.Enabled = False
                    btnOTProcessRemoval.Enabled = False
                End If
            End If
        Else
            btnOTDetails.Enabled = False
            btnOTProcess.Enabled = False
            btnOTProcessRemoval.Enabled = False
            lblError.Text = "Payroll freeze data not initiated."
        End If
    End Sub
    Private Sub LoadOTDetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_Id", Session("sBsuid"), SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@FromDate", txtFromDate.Text, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ToDate", txtToDate.Text, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@EMP_Ids", h_EMPID.Value, SqlDbType.VarChar)
            'Dim mDatatable As New DataTable
            Dim ds As New DataSet
            ds = Mainclass.getDataSet("GET_OT_PROCESSED_DATA", sqlParam, str_conn)
            Dim dt As New DataTable
            dt = New DataTable()

            'If gvOTDetails.Columns.Count > 1 Then
            '    Dim K As Integer
            '    K = gvOTDetails.Columns.Count - 1
            '    For i As Integer = 0 To K - 1

            '        gvOTDetails.Columns.RemoveAt(gvOTDetails.Columns.Count - 1)

            '    Next
            'End If
            gvOTDetails.Dispose()


            'mDatatable = Mainclass.getDataTable("OT_Calculation", sqlParam, str_conn)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'Dim tfield As New TemplateField()
                    'tfield.HeaderText = ""
                    'gvOTDetails.Columns.Add(tfield)
                    If gvOTDetails.Columns.Count <= 1 Then
                        Dim bfield As New BoundField()

                        Dim i As Integer = 0
                        For Each column As DataColumn In ds.Tables(0).Columns

                            bfield = New BoundField()
                            bfield.HeaderText = column.ColumnName
                            bfield.DataField = column.ColumnName
                            gvOTDetails.Columns.Add(bfield)
                            i += 1
                        Next
                    End If

                    trOTSummary.Visible = True
                    gvOTDetails.DataSource = ds.Tables(0)
                    gvOTDetails.DataBind()
                    If gvOTDetails.Columns.Count > 0 Then
                        gvOTDetails.Columns(2).Visible = False
                    End If

                    'For Each gvr As GridViewRow In gvOTDetails.Rows
                    '    'Get a programmatic reference to the CheckBox control
                    '    Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
                    '    If cb IsNot Nothing Then
                    '        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    '    End If
                    'Next
                Else trOTSummary.Visible = True
                    If ds.Tables(0).Rows.Count = 0 Then
                        trOTSummary.Visible = False
                        lblOTSummary.Text = "No data is available...."
                    End If
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Sub
    Protected Sub CheckedChaged(sender As Object, e As EventArgs)
        ClientScript.RegisterStartupScript(Me.GetType(),
            "script", "<script language='javascript'>  alert('Hi'); </script>")
        'Me.ClientScript.RegisterClientScriptBlock(GetType(String), "alert", "alert('Hi')", True)
    End Sub
    Protected Sub gvOTDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvOTDetails.RowDataBound
        'If e.Row.RowType = DataControlRowType.Header Then
        '    'For i As Integer = 0 To e.Row.Cells.Count - 1
        '    Dim checkBox As CheckBox = New CheckBox()
        '    checkBox.CssClass = "headerCheckBox"
        '    checkBox.ID = "chkAL"
        '    AddHandler checkBox.CheckedChanged, AddressOf CheckedChaged
        '    checkBox.AutoPostBack = True
        '    e.Row.Cells(0).Controls.Add(checkBox)
        '    'Next
        'End If
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim checkBox As CheckBox = New CheckBox()
        '    checkBox.ID = "chkSub"
        '    checkBox.CssClass = "RowCheckBox"
        '    e.Row.Cells(0).Controls.Add(checkBox)
        'End If
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub bind_BusinessUnit()
        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user access the page directly --will be logged back to the login page
        If tblbUsr_id = "" Then
            Response.Redirect("login.aspx")
        Else

            Try
                'if user is super admin give access to all the Business Unit
                If tblbUSuper = True Then
                    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                        'create a list item to bind records from reader to dropdownlist ddlBunit
                        Dim di As ListItem
                        ddlBSUinit.Items.Clear()
                        'check if it return rows or not
                        If BUnitreaderSuper.HasRows = True Then
                            While BUnitreaderSuper.Read
                                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                                'adding listitems into the dropdownlist
                                ddlBSUinit.Items.Add(di)

                                For ItemTypeCounter = 0 To ddlBSUinit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBSUinit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBSUinit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBSUinit.SelectedIndex = -1 Then
                        ddlBSUinit.SelectedIndex = 0
                    End If
                Else
                    'if user is not super admin get access to the selected  Business Unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        Dim di As ListItem
                        ddlBSUinit.Items.Clear()
                        If BUnitreader.HasRows = True Then
                            While BUnitreader.Read
                                di = New ListItem(BUnitreader(2), BUnitreader(0))
                                ddlBSUinit.Items.Add(di)
                                For ItemTypeCounter = 0 To ddlBSUinit.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlBSUinit.Items(ItemTypeCounter).Value = buser_id Then
                                        ddlBSUinit.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using
                    If ddlBSUinit.SelectedIndex = -1 Then
                        ddlBSUinit.SelectedIndex = 0
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
            End Try
        End If

    End Sub

    Protected Sub btnOTDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOTDetails.Click
        Try
            LoadOTDetails()
        Catch ex As Exception
        Finally
        End Try
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If Request.UrlReferrer IsNot Nothing Then
            'Response.Redirect(Request.UrlReferrer.ToString())
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect("../Homepage.aspx")
        End If
    End Sub

    Protected Sub ddlBSUinit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUinit.SelectedIndexChanged
        SetTransferDetail()
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID,EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub btnOTProcess_Click(sender As Object, e As EventArgs) Handles btnOTProcess.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim count As Integer = 0
        Dim returnVal As Integer
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim str_removed As String = ""
            Dim str_EMP_Ids As String = ""
            For Each row As GridViewRow In gvOTDetails.Rows
                Dim lblid As New Label
                Dim chkESD_ID As New HtmlInputCheckBox
                Dim lblESD_EMP_ID As New Label
                Dim hdnESD_ID As New HiddenField
                Dim K As Integer = True
                Dim cb As CheckBox = row.FindControl("chkSelect")
                'V1.1 changes    
                If cb IsNot Nothing AndAlso cb.Checked Then
                    count += 1
                    If str_EMP_Ids = "" Then
                        str_EMP_Ids = row.Cells(2).Text
                    Else str_EMP_Ids += "||" + row.Cells(2).Text
                    End If
                End If
            Next
            If str_EMP_Ids <> "" Then
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_Id", SqlDbType.VarChar, 20)
                pParms(0).Value = Session("sBsuid")
                pParms(1) = New SqlClient.SqlParameter("@FromDate", SqlDbType.VarChar)
                pParms(1).Value = txtFromDate.Text
                pParms(2) = New SqlClient.SqlParameter("@ToDate", SqlDbType.VarChar)
                pParms(2).Value = txtToDate.Text
                pParms(3) = New SqlClient.SqlParameter("@EMP_Ids", SqlDbType.VarChar)
                pParms(3).Value = h_EMPID.Value
                pParms(4) = New SqlClient.SqlParameter("@OTProcess_Approval", SqlDbType.Bit)
                pParms(4).Value = True
                pParms(5) = New SqlClient.SqlParameter("@OTProcss_Approval_EMPIds", SqlDbType.VarChar)
                pParms(5).Value = str_EMP_Ids
                pParms(6) = New SqlClient.SqlParameter("@OTProcess_ApprovedBy", SqlDbType.VarChar)
                pParms(6).Value = Session("sUsr_name")
                pParms(7) = New SqlClient.SqlParameter("@OTProcess_Removal", SqlDbType.Bit)
                pParms(7).Value = False
                pParms(8) = New SqlClient.SqlParameter("@OTProcess_Removal_EMPIds", SqlDbType.VarChar)
                pParms(8).Value = ""
                pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue
                Dim cmd As New SqlCommand
                cmd.Parameters.Add(pParms(0))
                cmd.Parameters.Add(pParms(1))
                cmd.Parameters.Add(pParms(2))
                cmd.Parameters.Add(pParms(3))
                cmd.Parameters.Add(pParms(4))
                cmd.Parameters.Add(pParms(5))
                cmd.Parameters.Add(pParms(6))
                cmd.Parameters.Add(pParms(7))
                cmd.Parameters.Add(pParms(8))
                cmd.Parameters.Add(pParms(9))

                cmd.Connection = objConn
                cmd.Transaction = stTrans
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "GET_OT_PROCESSED_DATA"
                cmd.CommandTimeout = 0

                returnVal = cmd.ExecuteNonQuery
                returnVal = pParms(9).Value
                If returnVal = 0 Then
                    stTrans.Commit()
                    lblError.Text = "OT Process Approved successfully...."
                    Response.Redirect(Request.RawUrl)
                    'LoadOTDetails()
                Else lblError.Text = getErrorMessage(returnVal)
                    stTrans.Rollback()
                End If
                'Dim sqlParam(9) As SqlParameter
                'sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_Id", Session("sBsuid"), SqlDbType.VarChar)
                'sqlParam(1) = Mainclass.CreateSqlParameter("@FromDate", txtFromDate.Text, SqlDbType.VarChar)
                'sqlParam(2) = Mainclass.CreateSqlParameter("@ToDate", txtToDate.Text, SqlDbType.VarChar)
                'sqlParam(3) = Mainclass.CreateSqlParameter("@EMP_Ids", h_EMPID.Value, SqlDbType.VarChar)
                'sqlParam(6) = Mainclass.CreateSqlParameter("@Process_Flag", True, SqlDbType.Bit)
                'sqlParam(7) = Mainclass.CreateSqlParameter("@ProcessedBy", Session("sUsr_name"), SqlDbType.VarChar)
                'sqlParam(8) = Mainclass.CreateSqlParameter("@Process_EMPIDs", str_EMP_Ids, SqlDbType.VarChar)
                ''Dim mDatatable As New DataTable
                'Dim ds As New DataSet
                'ds = Mainclass.getDataSet("GET_OT_PROCESSED_DATA", sqlParam, str_conn)
                'If ds.Tables.Count > 0 Then
                '    If ds.Tables(2).Rows.Count > 0 Then
                '        gvOTDetails.DataSource = ds.Tables(2)
                '        gvOTDetails.DataBind()
                '        retval = 0
                '    End If
                'End If
            End If

            If count = 0 Then
                lblError.Text = "No records selected."
                retval = 1000
                Exit Sub
            End If
            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = "OT Processed uccessfully...."
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            If returnVal <> 0 Then
                stTrans.Rollback()
            End If
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub btnOTProcessRemoval_Click(sender As Object, e As EventArgs) Handles btnOTProcessRemoval.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim count As Integer = 0
        Dim returnVal As Integer
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim str_removed As String = ""
            Dim str_EMP_Ids As String = ""
            For Each row As GridViewRow In gvOTDetails.Rows
                Dim lblid As New Label
                Dim chkESD_ID As New HtmlInputCheckBox
                Dim lblESD_EMP_ID As New Label
                Dim hdnESD_ID As New HiddenField
                Dim K As Integer = True
                Dim cb As CheckBox = row.FindControl("chkSelect")
                'V1.1 changes    
                If cb IsNot Nothing AndAlso cb.Checked Then
                    count += 1
                    If str_EMP_Ids = "" Then
                        str_EMP_Ids = row.Cells(2).Text
                    Else str_EMP_Ids += "||" + row.Cells(2).Text
                    End If
                End If
            Next
            If str_EMP_Ids <> "" Then
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_Id", SqlDbType.VarChar, 20)
                pParms(0).Value = Session("sBsuid")
                pParms(1) = New SqlClient.SqlParameter("@FromDate", SqlDbType.VarChar)
                pParms(1).Value = txtFromDate.Text
                pParms(2) = New SqlClient.SqlParameter("@ToDate", SqlDbType.VarChar)
                pParms(2).Value = txtToDate.Text
                pParms(3) = New SqlClient.SqlParameter("@EMP_Ids", SqlDbType.VarChar)
                pParms(3).Value = h_EMPID.Value
                pParms(4) = New SqlClient.SqlParameter("@OTProcess_Approval", SqlDbType.Bit)
                pParms(4).Value = False
                pParms(5) = New SqlClient.SqlParameter("@OTProcss_Approval_EMPIds", SqlDbType.VarChar)
                pParms(5).Value = ""
                pParms(6) = New SqlClient.SqlParameter("@OTProcess_ApprovedBy", SqlDbType.VarChar)
                pParms(6).Value = Session("sUsr_name")
                pParms(7) = New SqlClient.SqlParameter("@OTProcess_Removal", SqlDbType.Bit)
                pParms(7).Value = True
                pParms(8) = New SqlClient.SqlParameter("@OTProcess_Removal_EMPIds", SqlDbType.VarChar)
                pParms(8).Value = str_EMP_Ids
                pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue
                Dim cmd As New SqlCommand
                cmd.Parameters.Add(pParms(0))
                cmd.Parameters.Add(pParms(1))
                cmd.Parameters.Add(pParms(2))
                cmd.Parameters.Add(pParms(3))
                cmd.Parameters.Add(pParms(4))
                cmd.Parameters.Add(pParms(5))
                cmd.Parameters.Add(pParms(6))
                cmd.Parameters.Add(pParms(7))
                cmd.Parameters.Add(pParms(8))
                cmd.Parameters.Add(pParms(9))

                cmd.Connection = objConn
                cmd.Transaction = stTrans
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "GET_OT_PROCESSED_DATA"
                cmd.CommandTimeout = 0

                returnVal = cmd.ExecuteNonQuery
                returnVal = pParms(9).Value
                If returnVal = 0 Then
                    stTrans.Commit()
                    lblError.Text = "OT Process Removed successfully...."
                    Response.Redirect(Request.RawUrl)
                    'LoadOTDetails()
                Else lblError.Text = getErrorMessage(returnVal)
                    stTrans.Rollback()
                End If
                'Dim sqlParam(9) As SqlParameter
                'sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_Id", Session("sBsuid"), SqlDbType.VarChar)
                'sqlParam(1) = Mainclass.CreateSqlParameter("@FromDate", txtFromDate.Text, SqlDbType.VarChar)
                'sqlParam(2) = Mainclass.CreateSqlParameter("@ToDate", txtToDate.Text, SqlDbType.VarChar)
                'sqlParam(3) = Mainclass.CreateSqlParameter("@EMP_Ids", h_EMPID.Value, SqlDbType.VarChar)
                'sqlParam(6) = Mainclass.CreateSqlParameter("@Process_Flag", True, SqlDbType.Bit)
                'sqlParam(7) = Mainclass.CreateSqlParameter("@ProcessedBy", Session("sUsr_name"), SqlDbType.VarChar)
                'sqlParam(8) = Mainclass.CreateSqlParameter("@Process_EMPIDs", str_EMP_Ids, SqlDbType.VarChar)
                ''Dim mDatatable As New DataTable
                'Dim ds As New DataSet
                'ds = Mainclass.getDataSet("GET_OT_PROCESSED_DATA", sqlParam, str_conn)
                'If ds.Tables.Count > 0 Then
                '    If ds.Tables(2).Rows.Count > 0 Then
                '        gvOTDetails.DataSource = ds.Tables(2)
                '        gvOTDetails.DataBind()
                '        retval = 0
                '    End If
                'End If
            End If

            If count = 0 Then
                lblError.Text = "No records selected."
                retval = 1000
                Exit Sub
            End If
            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = "OT Processed uccessfully...."
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            If returnVal <> 0 Then
                stTrans.Rollback()
            End If
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
