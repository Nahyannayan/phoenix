﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="OTCalculation.aspx.vb"
    Inherits="Payroll_OTCalculation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
                var chk_state = !chkThis.checked;
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1 && document.forms[0].elements[i].disabled == false) {
                        //if (document.forms[0].elements[i].type=='checkbox' )
                        //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click();//fire the click event of the child element
                    }
                }
            }


        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }


        //$(document).on('change', '[type=checkbox]', function (event) {   
        //    debugger;
        //    if (this.checked) {
        //        var targetId = event.target.id;
        //        if (targetId = "ctl00_cphMasterpage_gvOTDetails_ctl01_chkAL")
        //            alert(targetId);
        //    }
        //})


        function ChangeAllCheckBoxStates(checkState) {
            
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            if (CheckBoxIDs != null) {
                alert('K')
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }

        //function change_chk_state(src) {
        //    var chk_state = (src.checked);

        //    for (i = 0; i < document.forms[0].elements.length; i++) {
        //        if (document.forms[0].elements[i].type == 'checkbox') {
        //            document.forms[0].elements[i].checked = chk_state;
        //        }
        //    }
        //}

        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'none') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
            }
            return false;
        }

        function ToggleSearchDepartment() {
            if (document.getElementById('<%=hfDepartment.ClientID %>').value == 'display') {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusDepartment.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusDepartment.ClientID %>').style.display = '';
                document.getElementById('<%=hfDepartment.ClientID %>').value = 'display';
            }
            return false;
        }

        function ToggleSearchCategory() {
            if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
            }
            return false;
        }

        function HideAll() {
            document.getElementById('<%=trDepartment.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelDepartment.ClientID %>').style.display = '';
            document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
            document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
            document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
        }

        function SearchHide() {
            document.getElementById('trBSUnit').style.display = 'none';
        }


        function GetDeptName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=DEPT", "", sFeatures)
            var oWnd = radopen("Reports\\Aspx\\SelIDDESC.aspx?ID=DEPT", "pop_dept");
            <%-- if (result != '' && result != undefined) {
                 document.getElementById('<%=h_DEPTID.ClientID %>').value = result;//NameandCode[0];
                    document.forms[0].submit();
                }
                else {
                    return false;
                }--%>
        }
        function GetCATName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("SelIDDESC.aspx?ID=CAT", "", sFeatures)
            var oWnd = radopen("Reports\\Aspx\\SelIDDESC.aspx?ID=CAT", "pop_cat");
              <%--  if (result != '' && result != undefined) {
                    document.getElementById('<%=h_CATID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "Reports\\Aspx\\SelIDDESC.aspx?ID=EMP_ALLALLOC"

            var oWnd = radopen(url, "pop_emp2");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode);
                document.getElementById('<%=h_BSUID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_BSUID.ClientID%>', "");
            }
        }
        function OnClientDeptClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_DEPTID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_DEPTID.ClientID%>', "");
            }
        }
        function OnClientCatClose(oWnd, args) {

            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_CATID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_CATID.ClientID%>', "");
            }
        }

        function OnClientEmp2Close(oWnd, args) {

            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=h_EMPID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_EMPID.ClientID%>', "");
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_dept" runat="server" Behaviors="Close,Move" OnClientClose="OnClientDeptClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_cat" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCatClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_emp2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEmp2Close"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblPageTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                </table>
                <table align="center" runat="server" id="tblMain" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_img">
                        <td align="left" colspan="4">
                        </td>
                    </tr>--%>
                    <tr runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" width="80%" colspan="3">
                            <asp:DropDownList ID="ddlBSUinit" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">To Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr id="trSelDepartment">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusDepartment" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                            <span class="field-label">Select Department Filter</span></td>
                    </tr>
                    <tr id="trDepartment" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusDepartment" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" /><span class="field-label">Department</span></td>
                        <td align="left" class="matters" colspan="3" style="text-align: left">(Enter the Department ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtDeptName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddDEPTID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetDeptName(); return false;" /><br />
                            <asp:GridView ID="gvDept" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="DEPT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDEPTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="DEPT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdDeptDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelcategory">
                        <td align="left" class="matters" colspan="4" valign="top">
                            <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                            <span class="field-label">Select Category Filter</span></td>
                    </tr>
                    <tr id="trCategory" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Category</span></td>
                        <td align="left" class="matters" colspan="3" style="text-align: left">(Enter the Category ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtCatName" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetCATName(); return false;" /><br />
                            <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="CAT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelEMPName">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchEMPNames();return false;" CausesValidation="False" />
                            <span class="field-label">Select Employee Name Filter</span></td>
                    </tr>
                    <tr id="trEMPName" style="display: none;">
                        <td align="left">
                            <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchEMPNames();return false;" CausesValidation="False" />
                            <span class="field-label">Employee Name</span></td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtEMPNAME" runat="server" ReadOnly="True"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>' Visible="False"></asp:Label>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Button ID="btnOTCalculation" runat="server" CssClass="button" Text="Calculate Overtime"
                                ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblOTSummary" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trOTSummary">
                        <td colspan="4" width="100%">
                            <asp:GridView ID="gvOTDetails" runat="server" Width="100%"
                                CaptionAlign="Top" PageSize="15" AutoGenerateColumns="false" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                       <%-- <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />--%>
                                                         <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                       Select
                                                                    <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Select one" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnOTProcess" runat="server" CssClass="button" Text="Process Overtime"
                                ValidationGroup="dayBook" />
                            <%--<asp:Button ID="Button2" runat="server" CssClass="button" Text="Cancel" />--%>
                        </td>
                    </tr>
                    
                </table>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
    <asp:HiddenField ID="h_EMPID" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    &nbsp;<asp:HiddenField ID="h_DEPTID" runat="server" />
    <asp:HiddenField ID="h_CATID" runat="server" />
    &nbsp; &nbsp; &nbsp;&nbsp;
    <input id="hfBSU" runat="server" type="hidden" />
    <input id="hfDepartment" runat="server" type="hidden" />
    <input id="hfCategory" runat="server" type="hidden" />
    <input id="hfEmpName" runat="server" type="hidden" /><br />
</asp:Content>

