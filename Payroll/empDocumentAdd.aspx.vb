Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empDocumentAdd
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSave)
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            h_Emp_ID.Value = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "U000062" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("TYPE") <> "" Then
                ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            End If

            If Request.QueryString("TYPE") <> "" Then
                ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            End If
            If ViewState("TYPE") = "EDD" Then
                ViewState("EDD_ID") = Encr_decrData.Decrypt(Request.QueryString("ProfileID").Replace(" ", "+"))
                Master.DependantLoggedIn = "TRUE"
                Master.DependantID = Encr_decrData.Decrypt(Request.QueryString("ProfileID").Replace(" ", "+"))
            Else
                Master.DependantLoggedIn = "FALSE"
                Master.DependantID = "0"
            End If
            UploadDocPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
            filldrp()
            If Request.QueryString("TYPE") <> "" Then
                ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                SetDataMode("add")
                If ViewState("TYPE") = "EMP" Then
                    txtName.Text = EOS_MainClass.GetEmployeeNameFromID(h_Emp_ID.Value)
                ElseIf ViewState("TYPE") = "EDD" Then
                    txtName.Text = EOS_MainClass.GetDepenedantNameFromID(ViewState("EDD_ID"))
                End If
            End If
        End If
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal connStr As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connStr)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub
    Public Sub filldrp()
        Try
            Dim Query As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Query = "SELECT ESD_ID,ESD_DESCR FROM EMPSTATDOCUMENTS_M order BY ESD_ORDER"
            fillDropdown(ddlDocumentType, Query, "ESD_DESCR", "ESD_ID", str_conn, True)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim EDC_Detail As New EOS_EmployeeDocument.DocumentDetail
            EDC_Detail = EOS_EmployeeDocument.GetDocumentDetail(p_Modifyid)
            hdnEDCID.Value = EDC_Detail.EDC_ID
            If IsDate(EDC_Detail.EDC_EXP_DT) Then
                txtExpirydate.Text = Format(CDate(EDC_Detail.EDC_EXP_DT), OASISConstants.DateFormat)
            End If
            h_Emp_ID.Value = EDC_Detail.EDC_EMP_ID
            If ViewState("TYPE") = "EMP" Then
                txtName.Text = EDC_Detail.EDC_EMP_NAME
            Else
                txtName.Text = EDC_Detail.EDC_EDD_NAME
            End If
            Dim lstDrp As New ListItem
            lstDrp = ddlDocumentType.Items.FindByValue(EDC_Detail.EDC_ESD_ID)
            If Not lstDrp Is Nothing Then
                ddlDocumentType.SelectedValue = lstDrp.Value
            Else
                Dim Seltext As String = ""
                Seltext = Mainclass.getDataValue("select isnull(ESD_DESCR,'')  from EMPSTATDOCUMENTS_M where   ESD_ID=" & EDC_Detail.EDC_ESD_ID, "OASISConnectionString")
                ddlDocumentType.SelectedItem.Text = Seltext
            End If

            hdnEDDID.Value = EDC_Detail.EDC_EDD_ID
            txtRemarks.Text = EDC_Detail.EDC_NARRATION
            chkReminder.Checked = EDC_Detail.EDC_bREqReminder
            chkActive.Checked = EDC_Detail.EDC_Bactive
            h_ESDType.Value = EDC_Detail.EDC_TYPE
            h_DocType.Value = EDC_Detail.EDC_CONTENT_TYPE
            setImageDisplay(imgDocument, EDC_Detail.EDC_DOCUMENT)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
            ViewState("imgDocument") = Nothing
        ElseIf mode = "edit" Then
            Dim NoEdit As Boolean
            If Not ddlDocumentType.SelectedItem Is Nothing Then
                NoEdit = (Mainclass.getDataValue("select count(*)  from EMPSTATDOCUMENTS_M where isnull(ESD_BHrOnly,0)=1 and  ESD_ID=" & ddlDocumentType.SelectedValue, "OASISConnectionString") > 0)
            End If
            If NoEdit And ViewState("TYPE") = "EMP" Then
                mDisable = True
            Else
                mDisable = False
            End If
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        ddlDocumentType.Enabled = Not mDisable
        txtName.Enabled = Not mDisable
        txtExpirydate.Enabled = Not mDisable
        imgExpiryDate.Enabled = Not mDisable
        txtRemarks.Enabled = Not mDisable
        chkActive.Enabled = Not mDisable
        chkReminder.Enabled = Not mDisable
        UploadDocPhoto.Enabled = Not mDisable
        chkReminder.Enabled = (mode = "edit" Or mode = "add")
    End Sub
    Sub clear_All()
        txtExpirydate.Text = ""
        imgDocument.ImageUrl = ""
        hdnEDDID.Value = ""
        h_DocumentPath.Value = ""
        ddlDocumentType.SelectedItem.Value = "0"
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ExpiryDate As String = txtExpirydate.Text.Trim
            If Not IsDate(ExpiryDate) Then
                lblError.Text = "Invalid Expiry Date !!!!"
                Exit Sub
            End If
            If ddlDocumentType.SelectedItem Is Nothing Or ddlDocumentType.SelectedValue = 0 Then
                lblError.Text = "Please select the Document Type !!!!"
                Exit Sub
            End If

            Dim EDC_Detail As New EOS_EmployeeDocument.DocumentDetail
            If ViewState("datamode") = "add" Then
                EDC_Detail.EDC_ID = 0
            Else
                EDC_Detail.EDC_ID = hdnEDCID.Value
            End If
            Dim FileError As Boolean
            If UploadDocPhoto.FileName <> "" Then
                Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                If Not Directory.Exists(str_img & "\temp") Then
                    Directory.CreateDirectory(str_img & "\temp")
                End If
                Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName

                Dim strFilepath As String
                strFilepath = str_img & "\temp\" & str_tempfilename
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
                'If hdnAssetImagePath.Value <> "" Then
                If Not IO.File.Exists(strFilepath) Then
                    lblError.Text = "Invalid FilePath!!!!"
                    Exit Sub
                Else
                    If IO.File.Exists(strFilepath) Then
                        Dim bytes As Byte()
                        Dim ContentType As String = ""
                        bytes = ConvertDocumentToBytes(strFilepath, ContentType)
                        If IO.File.Exists(strFilepath) Then
                            IO.File.Delete(strFilepath)
                        End If
                        If Not bytes Is Nothing Then
                            EDC_Detail.EDC_DOCUMENT = bytes
                            EDC_Detail.EDC_CONTENT_TYPE = ContentType
                        Else
                            FileError = True
                        End If
                    Else
                        FileError = True
                    End If
                End If
            ElseIf Not ViewState("imgDocument") Is Nothing Then
                EDC_Detail.EDC_CONTENT_TYPE = h_DocType.Value
                EDC_Detail.EDC_DOCUMENT = ViewState("imgDocument")
            Else
                FileError = True
            End If
            If FileError Then
                lblError.Text = "Invalid File ." & " Upload Image/Word/PDF/Excel formats"
                Exit Sub
            End If
            EDC_Detail.EDC_EXP_DT = ExpiryDate
            EDC_Detail.EDC_EMP_ID = h_Emp_ID.Value
            EDC_Detail.EDC_TYPE = "SS"
            EDC_Detail.EDC_ESD_ID = ddlDocumentType.SelectedItem.Value
            EDC_Detail.EDC_BSU_ID = Session("sBsuid")
            EDC_Detail.EDC_EDD_ID = Val(hdnEDDID.Value)
            EDC_Detail.EDC_NARRATION = txtRemarks.Text
            EDC_Detail.EDC_bREqReminder = chkReminder.Checked
            EDC_Detail.EDC_Bactive = chkActive.Checked
            EDC_Detail.EDC_EDD_ID = ViewState("EDD_ID")
            Dim retval As String
            Dim new_elh_id As Integer = 0
            retval = EOS_EmployeeDocument.SaveDocumentDetail(EDC_Detail)
            If (retval = "0" Or retval = "") Then
                lblError.Text = "Data Saved Successfully !!!"
                hdnEDCID.Value = EDC_Detail.EDC_ID
                setModifyHeader(EDC_Detail.EDC_ID)
                SetDataMode("view")
            Else
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Function ConvertDocumentToBytes(ByVal FilePath As String, Optional ByRef ContentType As String = "") As Byte()
        Try
            ' Dim filePath As String = fuUpload.PostedFile.FileName
            Dim filename As String = Path.GetFileName(filePath)
            Dim ext As String = Path.GetExtension(filename)
            Select Case ext
                Case ".doc"
                    ContentType = "application/vnd.ms-word"
                    Exit Select
                Case ".docx"
                    ContentType = "application/vnd.ms-word"
                    Exit Select
                Case ".xls"
                    ContentType = "application/vnd.ms-excel"
                    Exit Select
                Case ".xlsx"
                    ContentType = "application/vnd.ms-excel"
                    Exit Select
                Case ".jpg"
                    ContentType = "image/jpg"
                    Exit Select
                Case ".png"
                    ContentType = "image/png"
                    Exit Select
                Case ".gif"
                    ContentType = "image/gif"
                    Exit Select
                Case ".pdf"
                    ContentType = "application/pdf"
                    Exit Select
            End Select
            If ContentType <> String.Empty Then
                Dim fs As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
                Dim br As New BinaryReader(fs)
                Dim bytes As Byte() = br.ReadBytes(fs.Length)
                fs.Close()
                ConvertDocumentToBytes = bytes
            Else
                lblError.Text = "File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                ConvertDocumentToBytes = Nothing
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            ConvertDocumentToBytes = Nothing
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setModifyHeader(hdnEDDID.Value)
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            retval = EOS_EmployeeDocument.DeleteDocumentDetail(hdnEDCID.Value)
            If (retval = "0" Or retval = "") Then
                lblError.Text = "Data Deleted Successfully !!!!"
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = retval
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub setImageDisplay(ByVal imgMap As Object, ByVal PhotoBytes As Byte(), Optional ByVal ID As String = "")
        Try
            Dim OC As System.Drawing.Image = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            If Not PhotoBytes Is Nothing Then
                ViewState("imgDocument") = PhotoBytes
                OC = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            Else
                OC = System.Drawing.Image.FromFile("~/Images/NO_PREVIEW.jpg") ' System.Drawing.Image.FromFile(WebConfigurationManager.AppSettings.Item("NoImagePath"))
            End If
            If OC Is Nothing Then Exit Sub
            Dim nameOfImage As String = Session.SessionID & Replace(Now.ToString, ":", "") & ID & "CurrentImage"
            If Not Cache.Get(nameOfImage) Is Nothing Then
                Cache.Remove(nameOfImage)
            End If
            Cache.Insert(nameOfImage, OC, Nothing, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Default, Nothing)
            imgMap.ImageUrl = "empOrgChartImage.aspx?ID=" & nameOfImage
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub ddlDocumentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentType.SelectedIndexChanged
        Try
            Dim NoEdit As Boolean
            If Not ddlDocumentType.SelectedItem Is Nothing Then
                NoEdit = (Mainclass.getDataValue("select count(*)  from EMPSTATDOCUMENTS_M where isnull(ESD_BHrOnly,0)=1 and  ESD_ID=" & ddlDocumentType.SelectedValue, "OASISConnectionString") > 0)
            End If
            If NoEdit And ViewState("TYPE") = "EMP" Then
                ddlDocumentType.SelectedValue = 0
                lblError.Text = "You do not have permission to upload the selected personal document"
            Else
                lblError.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class


