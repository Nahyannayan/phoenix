Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empEarn_Ded_Edit


    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        txtAmtName.Attributes.Add("Readonly", "Readonly")
        txtAmtCode.Attributes.Add("Readonly", "Readonly")
        txtCAmtName.Attributes.Add("Readonly", "Readonly")
        txtCAmtCode.Attributes.Add("Readonly", "Readonly")
        btnPartyAcc.OnClientClick = "getRoleID('" & txtAmtName.ClientID & "', '" & txtAmtCode.ClientID & "','1');return false;"
        btnCPartyAcc.OnClientClick = "getRoleID('" & txtCAmtName.ClientID & "', '" & txtCAmtCode.ClientID & "','2');return false;"

        chkPaywithLeavesal.Visible = False
        lblPaywithLeaveSal.Visible = False
        gvCategory.Attributes.Add("bordercolor", "#1b80b6")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim CurBsUnit As String = Trim(Session("sBsuid") & "")
                Dim USR_NAME As String = Trim(Session("sUsr_name") & "")

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                    txtID.Attributes.Add("Readonly", "Readonly")
                    txtDesc.Attributes.Add("Readonly", "Readonly")
                    btnPartyAcc.Visible = False
                    btnCPartyAcc.Visible = False
                End If
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P050055" And ViewState("MainMnu_code") <> "P050060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("MainMnu_code") = "P050055" Then
                        lblTitle.Text = "Earnings"
                        'rdLopN.Checked = True
                        ddlRules.Enabled = False
                        ViewState("Etype") = "E"
                        Dim Ern_flag As Integer
                        If ViewState("datamode") = "view" Then
                            Using Userreader As SqlDataReader = PayrollFunctions.GetEMPSAL_ID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read
                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))

                                    txtAmtName.Text = Convert.ToString(Userreader("paid"))
                                    Ern_flag = Convert.ToInt16(Userreader("eob"))
                                    txtAmtCode.Text = Convert.ToString(Userreader("Acc_ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    txtCAmtCode.Text = Convert.ToString(Userreader("CAcc_ID"))
                                    txtCAmtName.Text = Convert.ToString(Userreader("CDESCR"))
                                    chkDeductLOP.Checked = Userreader("bLOP")
                                    chkPaywithLeavesal.Checked = Userreader("includeSal")
                                End While
                                'clear of the resource end using
                            End Using
                        Else
                            ddlRules.Enabled = True
                        End If
                        ' str_sql = "Select * from (select ECT_ID as id,ECT_DESCR as descr from EMPCATEGORY_M)a where id<>'' "

                        Using EmpEarnreader As SqlDataReader = AccessRoleUser.GetEmpEarnRules()
                            Dim di As ListItem
                            ddlRules.Items.Clear()
                            If EmpEarnreader.HasRows = True Then
                                While EmpEarnreader.Read
                                    di = New ListItem(Convert.ToString(EmpEarnreader("EMR_Description")), Convert.ToInt64(EmpEarnreader("EMR_ID")))
                                    ddlRules.Items.Add(di)
                                End While
                            End If
                        End Using
                        Dim ItemTypeCounter As Integer = 0
                        For ItemTypeCounter = 0 To ddlRules.Items.Count - 1
                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                            If ddlRules.Items(ItemTypeCounter).Value = Ern_flag Then
                                ddlRules.SelectedIndex = ItemTypeCounter
                            End If
                        Next
                    ElseIf ViewState("MainMnu_code") = "P050060" Then
                        lblTitle.Text = "Deductions"
                        ro4.Visible = False
                        cl6DeductLOP.Visible = False
                        TBCPaywithLeaveSal.Visible = False
                        ViewState("Etype") = "D"
                        If ViewState("datamode") = "view" Then
                            Using Userreader As SqlDataReader = PayrollFunctions.GetEMPSAL_ID(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read
                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("DESCR"))
                                    txtAmtName.Text = Convert.ToString(Userreader("paid"))
                                    txtAmtCode.Text = Convert.ToString(Userreader("Acc_ID"))
                                    txtCAmtCode.Text = Convert.ToString(Userreader("CAcc_ID"))
                                    txtCAmtName.Text = Convert.ToString(Userreader("CDESCR"))
                                End While
                                'clear of the resource end using
                            End Using
                        End If
                    End If

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
            GridBind()
        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        txtDesc.Attributes.Remove("readonly")
        btnPartyAcc.Visible = True
        btnCPartyAcc.Visible = True

        'txtID.Attributes.Remove("readonly")
        ddlRules.Enabled = True
        UtilityObj.beforeLoopingControls(Me.Page)
        'when the edit button is clicked set the datamode to edit
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim btnClicked As String = btnSave.ID
        If Page.IsValid = True Then
            Dim Status As Integer
            Dim Descr As String = String.Empty
            Dim editBit As Boolean
            Dim ERN_ACT_ID As String = String.Empty
            ViewState("Eid") = txtID.Text
            Descr = txtDesc.Text
            ERN_ACT_ID = txtAmtCode.Text
            'check the data mode to do the required operation
            If ViewState("datamode") = "edit" Then
                editBit = True
                Dim transaction As SqlTransaction
                'update  the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        If ViewState("Etype") = "E" Then
                            Dim Ern_flag As Integer
                            Ern_flag = 1 'ddlRules.SelectedValue
                            Status = PayrollFunctions.SaveEMPSALCOMPO_M(txtID.Text, Descr, ViewState("Etype"), _
                            editBit, transaction, True, Ern_flag, ERN_ACT_ID, _
                            chkDeductLOP.Checked, chkPaywithLeavesal.Checked, txtCAmtCode.Text)

                        ElseIf ViewState("Etype") = "D" Then
                            Status = PayrollFunctions.SaveEMPSALCOMPO_M(txtID.Text, Descr, ViewState("Etype"), _
                            editBit, transaction, False, 0, ERN_ACT_ID, chkDeductLOP.Checked, _
                            chkPaywithLeavesal.Checked, txtCAmtCode.Text)
                        End If
                        For Each gvr As GridViewRow In gvCategory.Rows
                            Dim lblECT_ID As Label = CType(gvr.FindControl("lblECT_ID"), Label)
                            Dim txtCode As TextBox = CType(gvr.FindControl("txtCode"), TextBox)
                            Dim txtCCode As TextBox = CType(gvr.FindControl("txtCCode"), TextBox)
                            Dim chkProrata As CheckBox = CType(gvr.FindControl("chkProrata"), CheckBox)
                            '''''''''
                            If txtCode.Text = "" Or txtCode.Text = "" Then
                                transaction.Rollback()
                                lblError.Text = "Please Enter Valid Account"
                                Exit Sub
                            End If
                            If Not lblECT_ID Is Nothing Then
                                Dim iProrata As Integer = 0
                                If chkProrata.Checked Then
                                    iProrata = 1
                                End If
                                Status = PayrollFunctions.SaveEMPSALCOMPO_D(txtID.Text, lblECT_ID.Text, _
                                txtCode.Text, txtCCode.Text, iProrata, transaction)
                            End If
                        Next
                        'If error occured during the process  throw exception and rollback the process

                        If Status = -1 Then
                            Throw New ArgumentException("Record does not exist for updating")
                        ElseIf Status <> 0 Then
                            Throw New ArgumentException("Error while updating the current record")
                        Else
                            'Store the required information into the Audit Trial table when Edited
                            Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                            If Status <> 0 Then
                                Throw New ArgumentException("Could not complete your request")
                            End If
                            ViewState("datamode") = "none"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            transaction.Commit()
                            Call clearMe()

                            lblError.Text = "Record Updated Successfully"
                        End If
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Updated"
                    End Try
                End Using
            ElseIf ViewState("datamode") = "add" Then
                editBit = False
                Dim transaction As SqlTransaction
                'insert the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        If ViewState("Etype") = "E" Then
                            Dim Ern_flag As Integer
                            Ern_flag = ddlRules.SelectedValue
                            Status = PayrollFunctions.SaveEMPSALCOMPO_M(txtID.Text, Descr, ViewState("Etype"), _
                            editBit, transaction, True, Ern_flag, ERN_ACT_ID, chkDeductLOP.Checked, _
                            chkPaywithLeavesal.Checked, txtCAmtCode.Text)

                        ElseIf ViewState("Etype") = "D" Then
                            Status = PayrollFunctions.SaveEMPSALCOMPO_M(txtID.Text, Descr, ViewState("Etype"), _
                            editBit, transaction, False, 0, ERN_ACT_ID, chkDeductLOP.Checked, _
                            chkPaywithLeavesal.Checked, txtCAmtCode.Text)
                        End If
                        For Each gvr As GridViewRow In gvCategory.Rows
                            Dim lblECT_ID As Label = CType(gvr.FindControl("lblECT_ID"), Label)
                            Dim txtCode As TextBox = CType(gvr.FindControl("txtCode"), TextBox)
                            Dim txtCCode As TextBox = CType(gvr.FindControl("txtCCode"), TextBox)
                            Dim chkProrata As CheckBox = CType(gvr.FindControl("chkProrata"), CheckBox)
                            '''''''''
                            If txtCode.Text = "" Or txtCode.Text = "" Then
                                transaction.Rollback()
                                lblError.Text = "Please Enter Valid Account"
                                Exit Sub
                            End If
                            If Not lblECT_ID Is Nothing Then
                                Dim iProrata As Integer = 0
                                If chkProrata.Checked Then
                                    iProrata = 1
                                End If
                                Status = PayrollFunctions.SaveEMPSALCOMPO_D(txtID.Text, lblECT_ID.Text, _
                                txtCode.Text, txtCCode.Text, iProrata, transaction)
                            End If
                        Next

                        If Status = -1 Then
                            Throw New ArgumentException("Record with same ID already exist")
                        ElseIf Status <> 0 Then
                            Throw New ArgumentException("Error while inserting new record")
                        Else
                            'Store the required information into the Audit Trial table when inserted
                            Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "insert", Page.User.Identity.Name.ToString, Me.Page)
                            If Status <> 0 Then
                                Throw New ArgumentException("Could not complete your request")
                            End If
                            ViewState("datamode") = "none"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Inserted Successfully"
                        End If
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Inserted"
                    End Try
                End Using
            End If
        End If
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Call clearMe()
        txtDesc.Attributes.Remove("readonly")
        ddlRules.Enabled = True
        txtID.Attributes.Remove("readonly")
        btnPartyAcc.Visible = True
        btnCPartyAcc.Visible = True

        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click


        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearMe()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user
        txtAmtCode.Attributes.Add("Readonly", "Readonly")
        txtID.Attributes.Add("Readonly", "Readonly")
        txtDesc.Attributes.Add("Readonly", "Readonly")
        txtAmtName.Attributes.Add("Readonly", "Readonly")
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                If ViewState("Etype") = "E" Then
                    Status = AccessRoleUser.DeleteEMPSALCOMPO_M(txtID.Text, ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "D" Then
                    Status = AccessRoleUser.DeleteEMPSALCOMPO_M(txtID.Text, ViewState("Etype"), transaction)
                End If
                If Status = -1 Then
                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record could not be Deleted")
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        Throw New ArgumentException("Could not complete your request")
                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearMe()
                    lblError.Text = "Record Deleted Successfully"
                End If
            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, Page.Title)
                transaction.Rollback()
            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message, Page.Title)
                transaction.Rollback()
            End Try
        End Using
    End Sub


    Sub clearMe()
        txtAmtCode.Text = ""
        txtID.Text = ""
        txtDesc.Text = ""
        txtAmtName.Text = ""
        txtCAmtCode.Text = ""
        txtCAmtName.Text = ""
        chkDeductLOP.Checked = False
        chkPaywithLeavesal.Checked = False
        txtID.Attributes.Add("Readonly", "Readonly")
        txtDesc.Attributes.Add("Readonly", "Readonly")
        GridBind()
        btnPartyAcc.Visible = False
        btnCPartyAcc.Visible = False
        If ViewState("Etype") = "E" Then
            ddlRules.Enabled = False
            ddlRules.SelectedIndex = 0
        End If
    End Sub


    Sub GridBind()
        Try
            Dim str_Sql As String
            'str_Sql = "SELECT DISTINCT ECT.ECT_ID, ECT.ECT_DESCR, ISNULL(ERD.ERD_ACC_ID,'') AS ERD_ACC_ID," _
            '     & " ISNULL(AM.ACT_NAME,'') AS ACT_NAME" _
            '     & " FROM EMPCATEGORY_M AS ECT LEFT OUTER JOIN" _
            '     & " EMPSALCOMPO_D AS ERD ON ECT.ECT_ID = ERD.ERD_ECT_ID  AND ERD.ERD_ERN_ID='" & txtID.Text & "' " _
            '     & " LEFT OUTER  JOIN vw_OSF_ACCOUNTS_M AS AM ON ERD.ERD_ACC_ID = AM.ACT_ID"

            str_Sql = "SELECT DISTINCT ECT.ECT_ID, ECT.ECT_DESCR, ISNULL(ERD.ERD_ACC_ID, '') AS ERD_ACC_ID, " _
            & " ISNULL(AM.ACT_NAME, '') AS ACT_NAME, ERD.ERD_PAYABLE_ACT_ID,  CASE ISNULL(ERD.ERD_CALCTYP,0) WHEN 1 THEN 1 ELSE 0 END AS ERD_CALCTYP, " _
            & " AMP.ACT_NAME AS ERD_PAYABLE_ACT_DESCR" _
            & " FROM EMPCATEGORY_M AS ECT LEFT OUTER JOIN" _
            & " EMPSALCOMPO_D AS ERD ON ECT.ECT_ID = ERD.ERD_ECT_ID AND ERD.ERD_ERN_ID ='" & txtID.Text & "' LEFT OUTER JOIN" _
            & " vw_OSF_ACCOUNTS_M AS AM ON ERD.ERD_ACC_ID = AM.ACT_ID LEFT OUTER JOIN" _
            & " vw_OSF_ACCOUNTS_M AS AMP ON ERD.ERD_PAYABLE_ACT_ID = AMP.ACT_ID"

            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvCategory.DataSource = ds
            gvCategory.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub txtCode_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtCode As New TextBox
        txtCode = sender
        txtCode.Attributes.Add("readonly", "readonly")
    End Sub


    Protected Sub txtDesc_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtDesc As New TextBox
        txtDesc = sender
        txtDesc.Attributes.Add("readonly", "readonly")

    End Sub
    'ts

    Protected Sub gvCategory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCategory.RowDataBound
        Try
            Dim txtDesc As New TextBox
            Dim txtCode As New TextBox
            Dim btnPartyAccGrid As New ImageButton

            Dim txtCDesc As New TextBox
            Dim txtCCode As New TextBox
            Dim btnCPartyAcc As New ImageButton

            txtDesc = e.Row.FindControl("txtDesc")
            txtCode = e.Row.FindControl("txtCode")
            btnPartyAccGrid = e.Row.FindControl("btnPartyAcc")

            txtCDesc = e.Row.FindControl("txtCDesc")
            txtCCode = e.Row.FindControl("txtCCode")
            btnCPartyAcc = e.Row.FindControl("btnCPartyAcc")

            If btnPartyAcc IsNot Nothing Then

                btnPartyAccGrid.OnClientClick = "getRoleID('" & txtDesc.ClientID & "', '" & txtCode.ClientID & "');return false;"
                btnCPartyAcc.OnClientClick = "getRoleID('" & txtCDesc.ClientID & "', '" & txtCCode.ClientID & "');return false;"

                ' btnPartyAcc.OnClientClick = "javascript:AddDetails('vid=" & lblID.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblID.Text) & "');return false;"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


End Class
