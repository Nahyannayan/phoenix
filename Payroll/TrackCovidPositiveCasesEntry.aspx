﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TrackCovidPositiveCasesEntry.aspx.vb" Inherits="Payroll_TrackCovidPositiveCasesEntry" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">
        function GetRDBValue() {
            var radio = $('.rbtnStaffStudent');

            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    alert(radio[i].value);
                    return radio[i].value;
                }
            }
        }
        function GetStaffStudent(rbtnseletecValue) {
            //var rbtnseletecValue = GetRDBValue();
            if (rbtnseletecValue == "Staff") {
                GetEMPName();
            } else {
                GetStudent();
            }
        }

        function GetEMPName() {

            var NameandCode;
            var result;
            //result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN&EMPNO=1", "pop_staff")
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_staff")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                return true;
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=h_StaffNoStudentNo.ClientID%>").value = NameandCode[1];
                document.getElementById("<%=txtStaffNoStudentNo.ClientID%>").value = NameandCode[0];
                if (document.getElementById('<%= txtInfectedPerson.ClientID %>') != null)
                    document.getElementById('<%= txtInfectedPerson.ClientID %>').value = NameandCode[0];
                if (document.getElementById('<%= txtCloseContactPerson.ClientID %>') != null)
                    document.getElementById('<%= txtCloseContactPerson.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtStaffNoStudentNo.ClientID%>', 'TextChanged');
            }
        }

        function GetStudent() {
            var NameandCode;
            var NameandCode;
            var result;
            var STUD_TYP = '0';
            var selType = '';
            var selACD_ID = '';
            var selDATE = '';
            var TYPE;
            if (selDATE == '') { if (STUD_TYP == '0') TYPE = 'stud'; else TYPE = 'TC'; }
            else { if (STUD_TYP == '0') TYPE = 'ENQ_DATE'; else TYPE = 'STUD_DATE'; }
            var url;

            if (STUD_TYP == "0")
                url = '../Students/ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;
            else
                url = '../Students/ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_student");

        }

        function OnClientClose2(oWnd, args) {
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Student.split('||');
                document.getElementById('<%= h_StaffNoStudentNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtStaffNoStudentNo.ClientID %>').value = NameandCode[1];
                if (document.getElementById('<%= txtInfectedPerson.ClientID %>') != null)
                    document.getElementById('<%= txtInfectedPerson.ClientID %>').value = NameandCode[1];
                if (document.getElementById('<%= txtCloseContactPerson.ClientID %>') != null)
                    document.getElementById('<%= txtCloseContactPerson.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtStaffNoStudentNo.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetOtherStaffStudent(rbtnseletecValue) {
            //var rbtnseletecValue = GetRDBValue();
            if (rbtnseletecValue == "Staff") {
                GetOtherEMPName();
            } else {
                GetOtherStudent();
            }
        }

        function GetOtherEMPName() {

            var NameandCode;
            var result;
            //result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN&EMPNO=1", "pop_staff")
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_OherStaff")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                return true;
            }
            return false;--%>
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=h_OtherStaffNoStudentNo.ClientID%>").value = NameandCode[1];
                document.getElementById("<%=txtOtherStaffNoStudentNo.ClientID%>").value = NameandCode[0];
                //__doPostBack('<%= txtOtherStaffNoStudentNo.ClientID%>', 'TextChanged');
            }
        }

        function GetOtherStudent_old() {
            var NameandCode;
            var NameandCode;
            var result;
            var STUD_TYP = '0';
            var selType = '';
            var selACD_ID = '';
            var selDATE = '';
            var TYPE;
            if (selDATE == '') { if (STUD_TYP == '0') TYPE = 'stud'; else TYPE = 'TC'; }
            else { if (STUD_TYP == '0') TYPE = 'ENQ_DATE'; else TYPE = 'STUD_DATE'; }
            var url;

            if (STUD_TYP == "0")
                url = '../Students/ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;
            else
                url = '../Students/ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_OherStudent");

        }

        function OnClientClose4_old(oWnd, args) {
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Student.split('||');
                document.getElementById('<%= h_OtherStaffNoStudentNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtOtherStaffNoStudentNo.ClientID %>').value = NameandCode[1];
                    <%-- document.getElementById('<%= txtInfectedPerson.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtCloseContactPerson.ClientID %>').value = NameandCode[1];--%>
                //__doPostBack('<%= txtOtherStaffNoStudentNo.ClientID%>', 'TextChanged');
            }
        }

        function GetOtherStudent() {

            var NameandCode;
            var result;


            var url = "";

            url = "../Students/BehaviorManagement/bm_MeritDemeritShowStudents.aspx";

            result = radopen(url, "pop_OherStudent");

        }

        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (NameandCode.length > 1) {

                    document.getElementById('<%=h_OtherStaffNoStudentNo.ClientID%>').value = arg.NameandCode;
                    document.getElementById('<%=txtOtherStaffNoStudentNo.ClientID%>').value = "Multiple Students Selected";
                }
                else if (NameandCode.length = 1) {
                    document.getElementById('<%=h_OtherStaffNoStudentNo.ClientID%>').value = NameandCode[0];
                    //document.getElementById('<%= txtOtherStaffNoStudentNo.ClientID %>').value = NameandCode[1];
                }
                __doPostBack('<%= h_OtherStaffNoStudentNo.ClientID%>', 'ValueChanged');
            }
        }




    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_staff" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_OherStaff" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_OherStudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Track COVID Positvie Cases
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary5" runat="server" ValidationGroup="ValDocDet" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="ValCloseDet" />
                        </td>
                        <td style="text-align: right">
                            <asp:Button runat="server" ID="btnBack" CausesValidation="false" Text="Back" CssClass="button" />
                        </td>
                    </tr>

                    <%--<tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" colspan="4">Fields Marked with (<font color="red">*</font>)
           are mandatory</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Staff/Student</span> </td>
                        <td align="left" width="30%">
                            <asp:RadioButtonList ID="rbtnStaffStudent" CssClass="rbtnStaffStudent" RepeatDirection="Horizontal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtnStaffStudent_SelectedIndexChanged">
                                <asp:ListItem Value="Staff" Text="Staff" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Student" Text="Student"></asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                        <td align="left" width="20%"><span class="field-label">Select Staff/Student</span>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtStaffNoStudentNo" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:ImageButton ID="imgsearch" runat="server" ImageUrl="~/Images/forum_search.gif" ClientIDMode="Static" />
                            </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4">COVID-19 Postive Case Information/Details</td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Infected Category</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlStudentCategory" runat="server" Visible="false" AutoPostBack="true">
                                <asp:ListItem Value="SELF" Text="Self"></asp:ListItem>
                                <asp:ListItem Value="PAR" Text="Parent"></asp:ListItem>
                                <asp:ListItem Value="SIB" Text="Siblings"></asp:ListItem>
                                <asp:ListItem Value="OTH" Text="Other"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlStaffCategory" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="SELF" Text="Self"></asp:ListItem>
                                <asp:ListItem Value="DEP" Text="Dependent"></asp:ListItem>
                                <asp:ListItem Value="OTH" Text="Other"></asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtDocDocument"
                                ErrorMessage="Select Document" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><br />--%>
                        </td>
                        <td align="left"><span class="field-label">Infected Person</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlInfectedPerson" runat="server" Visible="false" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtInfectedPerson" runat="server"></asp:TextBox>
                            <br />
                            <asp:TextBox ID="txtInfectedPersonOther" runat="server" Visible="false"></asp:TextBox>
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtInfectedPersonOther"
                                ErrorMessage="Please enter Parent of Other value" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>--%>
                            <%--  <asp:RequiredFieldValidator ID="reqDocNo" runat="server" ControlToValidate="txtDocDocNo"
                                ErrorMessage="Enter Document No " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>--%>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Date of Isolation</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtIsolationDate" runat="server" ValidationGroup="ValDocDet"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtIsolationDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtIsolationDate"
                                ErrorMessage="Please enter Isolation Date " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtIsolationDate"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator></td>
                        <td align="left"><span class="field-label">End Date of Isolation</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtDeIsolationDate" runat="server" ValidationGroup="ValDocDet"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtDeIsolationDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtDeIsolationDate"
                                ErrorMessage="Please enter De-Isolation Date " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtDeIsolationDate"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Upload Medical Clearance Certificate</span><span class="text-muted" style="font-size: xx-small !important;"><i>Note:File of size max 10 MB</i></span>
                        </td>

                        <td align="left">

                            <asp:FileUpload ID="FileUploadDoc" runat="server" />
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Only .pdf,.png,.gif,.jpeg|.jpg files are allowed."
                                ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.png|.gif|.jpeg|.jpg|.PDF|.PNG|.GIF|.JPEG|.JPG)$"
                                ControlToValidate="FileUploadDoc" ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator>--%>
                            <%--.doc|.docx|.pdf|.png|.gif|.jpeg|.jpg|.DOC|.DOCX|.PDF|.PNG|.GIF|.JPEG|.JPG--%>
                            <%--       <ajaxToolkit:AsyncFileUpload ID="AsyncFileUpload1" runat="server" />--%>
                        </td>
                        <td align="left" rowspan="2"><span class="field-label">Remarks</span></td>

                        <td align="left" rowspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Influenza-Like symptoms</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlInfluezaSymptoms" runat="server">
                                <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="No" Text="No" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnDocAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValDocDet" />
                            <asp:Button ID="btnDocCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:GridView ID="gvEMPDocDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                Width="100%">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Unique ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCTDID" runat="server" Text='<%#Bind("CTD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Infected Category" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategory" runat="server" Text='<%#Bind("CTD_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Infected Person">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInfectedPerson" runat="server" Text='<%#Bind("CTD_PERSON_NAME") %>'></asp:Label>
                                            <asp:HiddenField ID="hfInfectedPersonId" runat="server" Value='<%#Bind("CTD_REF_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:BoundField DataField="CTD_CATEGORY" HeaderText="Infected Category" ReadOnly="True" />
                                    <asp:BoundField DataField="CTD_PERSON_NAME" HeaderText="Infected Person" />--%>
                                    <asp:TemplateField HeaderText="Isolation Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSOLDT" runat="server" Text='<%# GetDate(Container.DataItem("CTD_ISOL_DATE")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="De-Isolation Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDEISOLDT" runat="server" Text='<%# GetDate(Container.DataItem("CTD_DEISOL_DATE")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Influenza-Like symptoms" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInflueza" runat="server" Text='<%#Bind("CTD_INFLUENZA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Bind("CTD_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDocEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                OnClick="lnkDocEdit_Click" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            View
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="lnkDocSelect" runat="server" CausesValidation="false" CommandName="Select"
                                                Text="View"></asp:LinkButton>--%>
                                            <%--<asp:HiddenField ID="HF_DocFileMIME" runat="server" Value='<%# Eval("DocFileMIME") %>' />--%>
                                            <asp:HyperLink ID="imgDoc" runat="server" ImageUrl="../Images/ViewDoc.png" Visible="false" ToolTip="Click To View Document">View</asp:HyperLink>
                                            <asp:HiddenField runat="server" ID="hfCOVDOC_ID" Value='<%# Eval("DOC_Id")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:CommandField HeaderText="Edit" ShowEditButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>--%>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center" />
                                    </asp:TemplateField>
                                    <%--<asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>--%>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Close Contacts Information/Details</td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Close Contact Category</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlCloseStudentCategory" runat="server" Visible="false" AutoPostBack="true">
                                <asp:ListItem Value="SELF" Text="Self"></asp:ListItem>
                                <asp:ListItem Value="PAR" Text="Parent"></asp:ListItem>
                                <asp:ListItem Value="SIB" Text="Siblings"></asp:ListItem>
                                <asp:ListItem Value="OTHSTAFF" Text="Other Staff"></asp:ListItem>
                                <asp:ListItem Value="OTHSTUD" Text="Other Student"></asp:ListItem>
                                <asp:ListItem Value="OTH" Text="Other"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlCloseStaffCategory" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="SELF" Text="Self"></asp:ListItem>
                                <asp:ListItem Value="DEP" Text="Dependent"></asp:ListItem>
                                <asp:ListItem Value="OTHSTAFF" Text="Other Staff"></asp:ListItem>
                                <asp:ListItem Value="OTHSTUD" Text="Other Student"></asp:ListItem>
                                <asp:ListItem Value="OTH" Text="Other"></asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtDocDocument"
                                ErrorMessage="Select Document" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><br />--%>
                        </td>
                        <td align="left"><span class="field-label">Close Contact Person</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlCloseContactPerson" runat="server" Visible="false" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtCloseContactPerson" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtOtherStaffNoStudentNo" runat="server" AutoPostBack="true" Visible="false"></asp:TextBox>
                            <asp:ImageButton ID="imgOtherStaffStud" runat="server" ImageUrl="~/Images/forum_search.gif" ClientIDMode="Static" Visible="false" />
                            <br />
                            <asp:TextBox ID="txtCloseContactOther" runat="server" Visible="false"></asp:TextBox>
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCloseContactOther"
                                ErrorMessage="Please enter Parent of Other value" ValidationGroup="ValCloseDet">*</asp:RequiredFieldValidator>--%>
                            <%--  <asp:RequiredFieldValidator ID="reqDocNo" runat="server" ControlToValidate="txtDocDocNo"
                                ErrorMessage="Enter Document No " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>--%>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Mobile No</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtMobileNo" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" rowspan="2"><span class="field-label">Remarks</span></td>

                        <td align="left" rowspan="2">
                            <asp:TextBox ID="txtCloseContactRemarks" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Date of contact with the Positive case </span></td>

                        <td align="left">
                            <asp:TextBox ID="txtDTContactPositivecase" runat="server" ValidationGroup="ValCloseDet"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtDTContactPositivecase" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDTContactPositivecase"
                                ErrorMessage="Please enter Date of contact with the Positive case" ValidationGroup="ValCloseDet">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtDTContactPositivecase"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="ValCloseDet">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Start Date of Quarantine</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtStDTQuarantine" runat="server" ValidationGroup="ValCloseDet"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtStDTQuarantine" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtStDTQuarantine"
                                ErrorMessage="Please enter Start date of Quarantine" ValidationGroup="ValCloseDet">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStDTQuarantine"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="ValCloseDet">*</asp:RegularExpressionValidator></td>
                        <td align="left"><span class="field-label">End Date of Quarantine</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtEndDTQuarantine" runat="server" ValidationGroup="ValCloseDet"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtEndDTQuarantine" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDTQuarantine"
                                ErrorMessage="Please enter End Date of Quarantine" ValidationGroup="ValCloseDet">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDTQuarantine"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="ValCloseDet">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Influenza-Like symptoms</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlCloseInfluenzaSymptoms" runat="server">
                                <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="No" Text="No" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnCloseContactAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValCloseDet" />
                            <asp:Button ID="btnCloseContactCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:GridView ID="gvCLOSEPERSONDETAILS" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                Width="100%">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCTDID" runat="server" Text='<%#Bind("CTD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   <%-- <asp:BoundField DataField="CTD_CATEGORY" HeaderText="Contact Category" ReadOnly="True" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="CTD_PERSON_NAME" HeaderText="Contacted Person" />--%>
                                     <asp:TemplateField HeaderText="Contact Category" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategory" runat="server" Text='<%#Bind("CTD_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contacted Person">
                                        <ItemTemplate>
                                            <asp:Label ID="lblContactedPerson" runat="server" Text='<%#Bind("CTD_PERSON_NAME") %>'></asp:Label>
                                            <asp:HiddenField ID="hfContactedPersonId" runat="server" Value='<%#Bind("CTD_REF_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date of Contact with positive case" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDTofContact" runat="server" Text='<%# GetDate(Container.DataItem("CTD_DATE_OF_CONTACT")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quarantine Start Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuarStDT" runat="server" Text='<%# GetDate(Container.DataItem("CTD_QUARANTINE_START")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quarantine End Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuarEndDT" runat="server" Text='<%# GetDate(Container.DataItem("CTD_QUARANTINE_END")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Influenza-Like symptoms" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInflueza" runat="server" Text='<%#Bind("CTD_INFLUENZA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Bind("CTD_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Mobile" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMobileNo" runat="server" Text='<%#Bind("CTD_MOBILENO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCloseContactEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                OnClick="lnkCloseContactEdit_Click" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>


                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center" />
                                    </asp:TemplateField>
                                    <%--<asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>--%>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <ajaxToolkit:CalendarExtender ID="DocIso" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgtxtIsolationDate" TargetControlID="txtIsolationDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="DocDeIso" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgtxtDeIsolationDate" TargetControlID="txtDeIsolationDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="ContactToPostivecase" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgtxtDTContactPositivecase" TargetControlID="txtDTContactPositivecase">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="StratDateQuarantine" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgtxtStDTQuarantine" TargetControlID="txtStDTQuarantine">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="EndDateQuarantine" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgtxtEndDTQuarantine" TargetControlID="txtEndDTQuarantine">
    </ajaxToolkit:CalendarExtender>

    <asp:HiddenField ID="h_StaffNoStudentNo" runat="server" />
    <asp:HiddenField ID="h_OtherStaffNoStudentNo" runat="server" />
    <asp:HiddenField ID="h_closeContactStaffNoStudNo" runat="server" />
    <asp:HiddenField ID="h_DOC_DOCID" runat="server" />
    <asp:HiddenField ID="h_CTD_ID" runat="server" Value="0" />
</asp:Content>

