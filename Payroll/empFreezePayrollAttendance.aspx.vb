Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Payroll_empFreezePayrollAttendance
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property FreezeDetail() As DataRow
        Get
            If Not ViewState("FreezeDetail") Is Nothing And ViewState("FreezeDetail").rows.count > 0 Then
                Return ViewState("FreezeDetail").rows(0)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataRow)
            ViewState("FreezeDetail") = value.Table
        End Set
    End Property
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                FillPayYearPayMonth()
                HttpContext.Current.Session("divDetail") = divDetail

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P130045") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                    h_BSU_ID.Value = Session("sBsuid")
                    txtBSUName.Text = Mainclass.GetBSUName(h_BSU_ID.Value)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Sub FillPayYearPayMonth()
        Try
            Dim lst(12) As ListItem
            lst(0) = New ListItem("January", 1)
            lst(1) = New ListItem("February", 2)
            lst(2) = New ListItem("March", 3)
            lst(3) = New ListItem("April", 4)
            lst(4) = New ListItem("May", 5)
            lst(5) = New ListItem("June", 6)
            lst(6) = New ListItem("July", 7)
            lst(7) = New ListItem("August", 8)
            lst(8) = New ListItem("September", 9)
            lst(9) = New ListItem("October", 10)
            lst(10) = New ListItem("November", 11)
            lst(11) = New ListItem("December", 12)
            For i As Integer = 0 To 11
                ddlPayMonth.Items.Add(lst(i))
            Next
            Dim iyear As Integer = Session("BSU_PAYYEAR")
            For i As Integer = iyear - 2 To iyear + 2
                ddlPayYear.Items.Add(i.ToString())
            Next
        Catch ex As Exception

        End Try
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            FreezeDetail = GetPayrollFreezeDetail(p_Modifyid)
            If FreezeDetail Is Nothing Then Exit Sub
            h_PFZ_ID.Value = FreezeDetail("PFZ_ID").ToString
            txtBSUName.Text = FreezeDetail("BSU_NAME").ToString
            ddFreezeType.SelectedValue = FreezeDetail("PFZ_TYPE").ToString
            h_BSU_ID.Value = FreezeDetail("PFZ_BSU_ID").ToString
            If IsDate(FreezeDetail("PFZ_TRANDT")) Then
                txtTrDate.Text = Format(FreezeDetail("PFZ_TRANDT"), "dd/MMM/yyyy")
            Else
                txtTrDate.Text = ""
            End If
            ddlPayMonth.SelectedValue = FreezeDetail("PFZ_MONTH").ToString
            ddlPayYear.SelectedValue = FreezeDetail("PFZ_YEAR").ToString
            txtRemarks.Text = FreezeDetail("PFZ_REMARKS").ToString
            txtLoggedInuser.Text = FreezeDetail("PFZ_USER").ToString
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup(); return false;", pce.BehaviorID)
            btnHistory.Attributes.Add("onClick", OnMouseOverScript)
            pce.Enabled = True
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        ddFreezeType.Enabled = Not mDisable
        txtTrDate.Enabled = Not mDisable
        ddlPayMonth.Enabled = Not mDisable
        ddlPayYear.Enabled = Not mDisable
        txtRemarks.Enabled = Not mDisable
    End Sub
    Sub clear_All()
        h_PFZ_ID.Value = ""
        txtBSUName.Text = ""
        ddFreezeType.SelectedValue = Nothing
        h_BSU_ID.Value = ""
        txtTrDate.Text = ""
        ddlPayMonth.SelectedValue = Now.Month
        ddlPayYear.SelectedValue = Now.Year
        txtRemarks.Text = ""
        txtLoggedInuser.Text = Session("sUsr_name")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        h_BSU_ID.Value = Session("sBsuid")
        txtBSUName.Text = Mainclass.GetBSUName(h_BSU_ID.Value)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            retval = DeletePayrollFreezeDetail(h_PFZ_ID.Value)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Deleted Successfully !!!!"
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = retval
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Trandate As String = txtTrDate.Text.Trim
            If Not IsDate(Trandate) Then
                lblError.Text = "Invalid Transaction Date !!!!"
                Exit Sub
            End If
            If CDate(Trandate).Month <> ddlPayMonth.SelectedValue Or CDate(Trandate).Year <> ddlPayYear.SelectedValue Then
                lblError.Text = "Selected Transaction date does not belong to the month !!!!"
                Exit Sub
            End If

            If CheckIfAlreadyFreezed(h_BSU_ID.Value, ddFreezeType.SelectedValue, ddlPayMonth.SelectedValue, ddlPayYear.SelectedValue, Val(h_PFZ_ID.Value)) Then
                lblError.Text = "The Month is already freezed for " & ddFreezeType.SelectedItem.Text
                Exit Sub
            End If

            If FreezeDetail Is Nothing Then Exit Sub
            If ViewState("datamode") = "add" Then
                FreezeDetail("PFZ_ID") = 0
                h_PFZ_ID.Value = 0
            Else
                FreezeDetail("PFZ_ID") = h_PFZ_ID.Value
            End If
            FreezeDetail("PFZ_BSU_ID") = h_BSU_ID.Value
            FreezeDetail("PFZ_USER") = Session("sUsr_name")
            FreezeDetail("PFZ_TYPE") = ddFreezeType.SelectedValue
            FreezeDetail("PFZ_MONTH") = ddlPayMonth.SelectedValue
            FreezeDetail("PFZ_YEAR") = ddlPayYear.SelectedValue
            FreezeDetail("PFZ_REMARKS") = txtRemarks.Text
            FreezeDetail("PFZ_TRANDT") = Trandate
            Dim retval As String
            Dim PFZ_ID As String = ""
            retval = SavePayrollFreezeDetail(FreezeDetail, PFZ_ID)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, h_PFZ_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Saved Successfully !!!"
                h_PFZ_ID.Value = PFZ_ID
                setModifyHeader(PFZ_ID)
                SetDataMode("view")
            Else
                lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Public Shared Function GetPayrollFreezeDetail(ByVal PFZ_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PFZ_ID", PFZ_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("GetPayrollFreezeDetail", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SavePayrollFreezeDetail(ByVal FreezeDetail As DataRow, ByRef PFZ_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(8) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PFZ_ID", FreezeDetail("PFZ_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@PFZ_BSU_ID", FreezeDetail("PFZ_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@PFZ_TYPE", FreezeDetail("PFZ_TYPE"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@PFZ_MONTH", FreezeDetail("PFZ_MONTH"), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@PFZ_YEAR", FreezeDetail("PFZ_YEAR"), SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@PFZ_REMARKS", FreezeDetail("PFZ_REMARKS"), SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@PFZ_TRANDT", FreezeDetail("PFZ_TRANDT"), SqlDbType.DateTime)
            sqlParam(7) = Mainclass.CreateSqlParameter("@PFZ_USER", FreezeDetail("PFZ_USER"), SqlDbType.VarChar)
            sqlParam(8) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "SavePayrollFreezeDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(8).Value = "" Then
                SavePayrollFreezeDetail = ""
                PFZ_ID = sqlParam(0).Value
            Else
                SavePayrollFreezeDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeletePayrollFreezeDetail(ByVal PFZ_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PFZ_ID", PFZ_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "DeletePayrollFreezeDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeletePayrollFreezeDetail = ""
            Else
                DeletePayrollFreezeDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
Public Shared Function GetDynamicContent() As String
        Try
            Dim b As New StringBuilder()
            Dim itemDiv As HtmlControls.HtmlGenericControl
            itemDiv = HttpContext.Current.Session("divDetail")
            Dim sw As New StringWriter()
            Dim pnl As Panel
            pnl = itemDiv.FindControl("pnlDetail")
            BindFreezeHistory(pnl, HttpContext.Current.Session("sBsuid"))
            Dim w As New HtmlTextWriter(sw)
            itemDiv.RenderControl(w)
            Dim s As String = sw.GetStringBuilder().ToString()
            b.Append("<table style='background-color:#f3f3f3; ")
            b.Append("width:500px; ' >")
            b.Append("<tr><td >")
            b.Append(s)
            b.Append("</td></tr>")
            b.Append("</table>")
            Return b.ToString
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Private Shared Sub BindFreezeHistory(ByVal pnl As Panel, ByVal BSU_ID As String)
        Try
            Dim lnkClosePnl As HtmlButton
            lnkClosePnl = pnl.FindControl("lnkClose")
            If Not lnkClosePnl Is Nothing Then
                Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();return false;", "pce_0")
                lnkClosePnl.Attributes.Add("onClick", OnMouseOutScript)
            End If
            Dim rptFreezeHistory As Repeater
            rptFreezeHistory = pnl.FindControl("rptFreezeHistory")
            If Not rptFreezeHistory Is Nothing Then
                Dim mtable As New DataTable
                mtable = GetFreezeHistory(BSU_ID)
                rptFreezeHistory.DataSource = mtable
                rptFreezeHistory.DataBind()
            End If
            Dim btn As HtmlButton
            btn = pnl.FindControl("lnkClose")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Function GetFreezeHistory(ByVal BSU_ID As String) As DataTable
        Try
            Dim mtable As New DataTable
            Dim sql As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            sql = "select PFZ_YEAR,PFZ_MONTH,MONTH,replace(convert(varchar(30),[PAYROLLFREEZE],106),' ','/')PAYROLLFREEZE ,replace(convert(varchar(30),[ATTFREEZE],106),' ','/') ATTFREEZE"
            sql &= " FROM VW_GetFreezeHistory  "
            sql &= "     WHERE PFZ_BSU_ID = '" & BSU_ID & "'"
            sql &= "     ORDER BY PFZ_YEAR DESC,PFZ_MONTH DESC"
            mtable = Mainclass.getDataTable(sql, str_conn)
            GetFreezeHistory = mtable
        Catch ex As Exception
            Errorlog(ex.Message)
            GetFreezeHistory = Nothing
            Throw ex
        End Try
    End Function
    Public Shared Function CheckIfAlreadyFreezed(ByVal BSU_ID As String, ByVal TYPE As String, ByVal Month As String, ByVal Year As String, ByVal CurrID As String) As Boolean
        Try
            Dim sql As String
            sql = "select count(*) from VW_GetFreezeHistory where PFZ_BSU_ID='" & BSU_ID & "' AND PFZ_MONTH='" & Month & "' and PFZ_YEAR='" & Year & "'"
            If TYPE = "A" Then
                sql &= " AND ATTFREEZEID not in (0, " & CurrID & ")"
            ElseIf TYPE = "O" Then
                sql &= " AND OTFREEZEID not in (0, " & CurrID & ")"
            Else
                sql &= " AND PAYROLLFREEZEID not in (0, " & CurrID & ")"
            End If
            If Mainclass.getDataValue(sql, "OASISConnectionString") > 0 Then
                CheckIfAlreadyFreezed = True
            Else
                CheckIfAlreadyFreezed = False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            CheckIfAlreadyFreezed = False
            Throw ex
        End Try
    End Function
End Class
