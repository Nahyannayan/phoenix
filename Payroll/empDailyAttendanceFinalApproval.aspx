<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empDailyAttendanceFinalApproval.aspx.vb" Inherits="Payroll_empDailyAttendanceFinalApproval"
    Title="Daily Attendance View" %>

<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <style type="text/css">
       .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 15px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .RadComboBox_Default {
            width: 100% !important;
        }
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image:url(/images/calender.gif)!important;
            width: 30px;
            height: 30px;
        }
        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active{
            background-image:url(/images/calender.gif)!important;
            width: 30px;
            height: 30px;
            background-position: 0;
        }

        .RadPicker { width:80% !important;
        }
        table.RadCalendar_Default {
            background:#ffffff !important;
        }
        
    </style>
    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calendar mr-3"></i> Employee Daily Attendance Approval
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
    <table align="center"  width="100%">
        
        <tr>
            <td width="20%" align="left">
                <span class="field-label">From Date</span>
            </td>
            
            <td width="30%" align="left">
                <telerik:RadDatePicker ID="UsrFromDate" runat="server" DateInput-EmptyMessage="MinDate">
                    <DateInput EmptyMessage="MinDate">
                    </DateInput>
                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
            </td>
            <td align="left">
                <span class="field-label">To Date</span>
            </td>
            
            <td align="left" valign="top">
                <telerik:RadDatePicker ID="UsrToDate" runat="server" DateInput-EmptyMessage="MinDate">
                    <DateInput EmptyMessage="MinDate">
                    </DateInput>
                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
            </td>
            <td align="left" valign="top">
                <asp:Button ID="btnListData" runat="server" CssClass="button" Text="View Details" />
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" colspan="7">
                <telerik:RadGrid ID="RadEmpAttendanceSummary" CssClass="table table-bordered" runat="server" AllowSorting="True" BorderWidth="0" BorderColor="#dee2da"
                    enableajax="True" ShowStatusBar="True" AutoGenerateColumns="False" ShowGroupPanel="True"
                    Width="100%" GridLines="None" GroupingEnabled="False" OnItemDataBound="RadEmpAttendanceSummary_ItemDataBound">
                    <%--<AlternatingItemStyle CssClass="RadGridAltRow" />--%>
                    <ItemStyle CssClass="RadGridRow" />
                    <MasterTableView GridLines="Both">
                        <ExpandCollapseColumn Visible="False">
                            <HeaderStyle Width="19px" />
                        </ExpandCollapseColumn>
                        <RowIndicatorColumn Visible="False">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <DetailTables>
                        </DetailTables>
                        <ExpandCollapseColumn Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="colAttDate" HeaderText="Date Range">
                                <ItemTemplate>
                                    <asp:Label ID="lblAttDate" runat="server" ForeColor="#1B80B6" Text='<%# Bind("DT","{0:dd-MMM-yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="colAttDetail" HeaderText="Attendance Summary">
                                <ItemTemplate>
                                    <telerik:RadGrid ID="RadEmpAttendanceDetails" CssClass="table table-bordered" runat="server" AllowSorting="True" BorderWidth="0" BorderColor="#dee2da"
                                        enableajax="True" ShowStatusBar="True" AutoGenerateColumns="False" ShowGroupPanel="True"
                                        GroupingEnabled="False" Width="100%" GridLines="None">
                                        <%--<AlternatingItemStyle CssClass="RadGridAltRow" />--%>
                                        <ItemStyle CssClass="RadGridRow" />
                                        <MasterTableView GridLines="Both">
                                            <ExpandCollapseColumn Visible="False">
                                                <HeaderStyle Width="19px" />
                                            </ExpandCollapseColumn>
                                            <RowIndicatorColumn Visible="False">
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                            <DetailTables>
                                            </DetailTables>
                                            <ExpandCollapseColumn Visible="True">
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridTemplateColumn UniqueName="colAttID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCAT_ID" runat="server" Text='<%# Bind("CAT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colCatDescr" HeaderText="Category" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbCAT_DESCR" runat="server" ForeColor="#1B80B6" Text='<%# Bind("CAT_DESCR") %>'
                                                            OnClick="lbCAT_DESCR_NEW_Click" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colEmpCount" HeaderText="Emp Count">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbTOTAL_EMP_COUNT" runat="server" ForeColor="#1B80B6" Text='<%# Bind("TOTAL_EMP_COUNT") %>'
                                                            OnClick="lbCAT_DESCR_NEW_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colPresent" HeaderText="Leaves">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbNO_LEAVES" runat="server" ForeColor="#1B80B6" Text='<%# Bind("NO_LEAVES") %>'
                                                            OnClick="lbCAT_DESCR_NEW_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colLeaves" HeaderText="Appr Leave">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbNO_APPROVED_LEAVES" runat="server" ForeColor="#1B80B6" Text='<%# Bind("NO_APPROVED_LEAVES") %>'
                                                            OnClick="lbCAT_DESCR_NEW_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colApprLeaves" HeaderText="Absentees">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbNO_ABSENTEES" runat="server" ForeColor="#1B80B6" Text='<%# Bind("NO_ABSENTEES") %>'
                                                            OnClick="lbCAT_DESCR_NEW_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colProcessed" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnProcessed" runat="server" Value='<%# Eval("Processed") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn ButtonType="ImageButton">
                                                </EditColumn>
                                            </EditFormSettings>
                                            <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                                        </MasterTableView>
                                        <HeaderStyle HorizontalAlign="left" Font-Bold="true" />
                                        <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                                        <ClientSettings EnableRowHoverStyle="True">
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </ItemTemplate>
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="ImageButton">
                            </EditColumn>
                        </EditFormSettings>
                        <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                    </MasterTableView>
                    <HeaderStyle HorizontalAlign="center" Font-Bold="true"  />
                    <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                    <ClientSettings EnableRowHoverStyle="True">
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" colspan="7">
                <asp:LinkButton ID="lblViewDetails" runat="server" ForeColor="#1B80B6" Text="View Details"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" colspan="7">
                <telerik:RadGrid ID="RadEmpAttendance" runat="server" AllowSorting="True" enableajax="True" BorderWidth="0" BorderColor="#dee2da"
                    ShowStatusBar="True" AutoGenerateColumns="False" ShowGroupPanel="True" Width="100%"
                    GridLines="None" GroupingEnabled="False" OnItemDataBound="RadEmpAttendance_ItemDataBound">
                    <%--<AlternatingItemStyle CssClass="RadGridAltRow" />--%>
                    <ItemStyle CssClass="RadGridRow" />
                    <MasterTableView GridLines="Both">
                        <ExpandCollapseColumn Visible="False">
                            <HeaderStyle Width="19px" />
                        </ExpandCollapseColumn>
                        <RowIndicatorColumn Visible="False">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <DetailTables>
                        </DetailTables>
                        <ExpandCollapseColumn Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="colAttDate" HeaderText="Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblAttDate" runat="server" Text='<%# Bind("DT","{0:dd-MMM-yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="colAttDetail" HeaderText="Attendance Detail">
                                <ItemTemplate>
                                    <telerik:RadGrid ID="RadEmpAttDetail" runat="server" AllowSorting="True" enableajax="True" BorderWidth="0" BorderColor="#dee2da"
                                        ShowStatusBar="True" AutoGenerateColumns="False" ShowGroupPanel="True" GroupingEnabled="False"
                                        Width="100%" GridLines="None">
                                        <AlternatingItemStyle CssClass="RadGridAltRow" />
                                        <ItemStyle CssClass="RadGridRow" />
                                        <MasterTableView GridLines="Both">
                                            <ExpandCollapseColumn Visible="False">
                                                <HeaderStyle Width="19px" />
                                            </ExpandCollapseColumn>
                                            <RowIndicatorColumn Visible="False">
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                            <DetailTables>
                                            </DetailTables>
                                            <ExpandCollapseColumn Visible="True">
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridTemplateColumn UniqueName="colAttID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttCAT_ID" runat="server" Text='<%# Bind("CAT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colCatDescr" HeaderText="Category" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbAttCAT_DESCR" runat="server" Text='<%# Bind("CAT_DESCR") %>'
                                                            OnClick="lbCAT_DESCR_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colEmpCount" HeaderText="Emp Count">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttTOTAL_EMP_COUNT" runat="server" Text='<%# Bind("TOTAL_EMP_COUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colPresent" HeaderText="Present">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblAttEMP_Present" runat="server" Text='<%# Bind("NO_PRESENT") %>'
                                                            OnClick="lbCAT_DESCR_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colLeaves" HeaderText="Leave">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblAttNO_LEAVES" runat="server" Text='<%# Bind("NO_LEAVES") %>'
                                                            OnClick="lbCAT_DESCR_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colApprLeaves" HeaderText="Appr Leave">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttNO_APPROVED_LEAVES" runat="server" Text='<%# Bind("NO_APPROVED_LEAVES") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colAbsentees" HeaderText="Absentees">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttNO_ABSENTEES" runat="server" Text='<%# Bind("NO_ABSENTEES") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colAttdate" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttDetDate" runat="server" Text='<%# Bind("ATT_DATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="colProcessed" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnProcessed" runat="server" Value='<%# Eval("Processed") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn ButtonType="ImageButton">
                                                </EditColumn>
                                            </EditFormSettings>
                                            <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                                        </MasterTableView>
                                        <HeaderStyle HorizontalAlign="left" Font-Bold="true" />
                                        <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                                        <ClientSettings EnableRowHoverStyle="True">
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </ItemTemplate>
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="ImageButton">
                            </EditColumn>
                        </EditFormSettings>
                        <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                    </MasterTableView>
                    <HeaderStyle HorizontalAlign="center" Font-Bold="true" />
                    <GroupHeaderItemStyle CssClass="RadGridGroupHeader" />
                    <ClientSettings EnableRowHoverStyle="True">
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="7" valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" colspan="7">
                <asp:Button ID="btnSave" runat="server" CssClass="button" SkinID="ButtonNormal"
                    Text="Approve" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" />
            </td>
        </tr>
    </table>

            </div>
        </div>
     </div>
</asp:Content>
