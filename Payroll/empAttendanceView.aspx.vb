Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empAttendanceView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version            Date            Author              Change
    '1.1                26-Apr-2011     Swapna              Replaced EAT_ELV_ID in query with EAT_ELA_ID
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empAttendanceAdd.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "P130010" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet

            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""

            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables


                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)


                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

                '   -- 2  txtLType
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ELT_DESCR", lstrCondn3)

                '   -- 3   txtDate

                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MonthYear", lstrCondn4)

                '   -- 5  city

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EAT_REMARKS", lstrCondn5)


            End If
            'V1.1- Replaced EAT_ELV_ID with EAT_ELA_ID
            str_Sql = "SELECT EAT_ELT_ID,EAT_EMP_ID,EAT_BSU_ID,  MonthYear, " _
            & " EAT_ELA_ID, EAT_REMARKS, EMP_NAME, ELT_DESCR, EMPNO,sum( EAT_DAYS) AS Days" _
            & " FROM (SELECT  LEFT(DATENAME(MONTH, EAT_DT),3)+'/'+ " _
            & " LTRIM(RTRIM( DATEPART(YEAR, EAT_DT))) AS MonthYear, EAT_ELT_ID,EAT_EMP_ID,EAT.EAT_ID, EAT.EAT_BSU_ID, EAT.EAT_DT, EAT.EAT_ELA_ID, " _
            & " EAT.EAT_REMARKS, ISNULL(EM.EMP_FNAME, '') + ' ' + ISNULL(EM.EMP_MNAME, '') + ' ' + " _
            & " ISNULL(EM.EMP_LNAME, '') AS EMP_NAME, ELM.ELT_DESCR, EM.EMPNO, EAT.EAT_DAYS" _
            & " FROM          EMPATTENDANCE AS EAT INNER JOIN EMPLOYEE_M AS EM" _
            & " ON EAT.EAT_EMP_ID = EM.EMP_ID INNER JOIN EMPLEAVETYPE_M AS ELM " _
            & " ON EAT.EAT_ELT_ID = ELM.ELT_ID) AS DB WHERE (EAT_BSU_ID = '" & Session("sBSUID") & "')" & str_Filter _
            & " GROUP BY  EAT_ELT_ID,EAT_EMP_ID,EAT_BSU_ID, MonthYear, EAT_ELA_ID, EAT_REMARKS," _
            & " EMP_NAME, ELT_DESCR, EMPNO" _
            & " ,'01/'+CAST(MONTH(EAT_DT) AS varchar(4)) +'/'+ CAST(YEAR(EAT_DT) AS varchar(4))" _
            & " order by cast('01/'+CAST(MONTH(EAT_DT) AS varchar(4)) +'/'+ CAST(YEAR(EAT_DT) AS varchar(4))  as datetime) desc"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtLType")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblEAT_ELT_ID As New Label
            Dim lblEAT_EMP_ID As New Label
            Dim lblMonthYear As New Label

            '
            'lblEAT_ELT_ID 
            'lblEAT_EMP_ID 
            'lblMonthYear 
            lblEAT_ELT_ID = TryCast(e.Row.FindControl("lblEAT_ELT_ID"), Label)
            lblEAT_EMP_ID = TryCast(e.Row.FindControl("lblEAT_EMP_ID"), Label)
            lblMonthYear = TryCast(e.Row.FindControl("lblMonthYear"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblEAT_ELT_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empAttendanceAdd.aspx?viewempid=" & Encr_decrData.Encrypt(lblEAT_EMP_ID.Text) _
                 & "&leave=" & Encr_decrData.Encrypt(lblEAT_ELT_ID.Text) & "&monthyear=" & Encr_decrData.Encrypt(lblMonthYear.Text) _
                & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

End Class
