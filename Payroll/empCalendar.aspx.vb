Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj


Partial Class Payroll_empCalendar
    Inherits System.Web.UI.Page

    Dim dsEvents As New DataSet



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        If Page.IsPostBack = False Then
            bindMonthstatus()
            calDatepicker.FirstDayOfWeek = WebControls.FirstDayOfWeek.Sunday
            chkSunday.Attributes.Add("onclick", "return false;")
            chkMonday.Attributes.Add("onclick", "return false;")
            chkTuesday.Attributes.Add("onclick", "return false;")
            chkWednesday.Attributes.Add("onclick", "return false;")
            chkThursday.Attributes.Add("onclick", "return false;")
            chkFriday.Attributes.Add("onclick", "return false;")
            chkSaturday.Attributes.Add("onclick", "return false;")
            Dim dtselected As Date
            Try
                dtselected = CDate(Request.QueryString("dt"))
            Catch ex As Exception
                dtselected = Now.Date
            End Try
            If Not Page.IsPostBack Then
                calDatepicker.TodaysDate = dtselected
                chkAll.Checked = False
                Call Populate_MonthList(dtselected)
                Call Populate_YearList(dtselected)
                ddlCategory.DataBind()
                chkCategory.DataBind()

                rbOn_CheckedChanged(Nothing, Nothing)
                'chkAll_CheckedChanged(Nothing, Nothing)
            End If
        End If
        bindevents()
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dt1, dt2, dtTemp As Date
            If rbFrom.Checked = True Then
                dt1 = CDate(txtFrom.Text)
                dt2 = CDate(txtTo.Text)
                If (dt1 > dt2) Then
                    lblError.Text = "Choose 'To Date' Greater than 'From Date'"
                    Exit Sub
                End If
            Else
                dt1 = CDate(txtOn.Text)
            End If

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Try
                Dim str_retval As String = ""
                If rbOn.Checked = True Then
                    str_retval = SaveMONTHDEFINE_D(Session("sBsuid"), txtOn.Text, ddMonthstatus.SelectedItem.Value, _
                                  txtRemarks.Text, stTrans)
                Else
                    dtTemp = dt1
                    While dtTemp <= dt2
                        str_retval = SaveMONTHDEFINE_D(Session("sBsuid"), dtTemp, ddMonthstatus.SelectedItem.Value, _
                                                          txtRemarks.Text, stTrans)
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, dtTemp & "-" & ddMonthstatus.SelectedItem.Value, _
                    "Insert", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        dtTemp = dtTemp.AddDays(1)
                        If str_retval <> "0" Then
                            Exit While
                        End If
                    End While

                End If


                If str_retval = "0" Then
                    lblError.Text = getErrorMessage("0")
                    stTrans.Commit()
                Else
                    lblError.Text = getErrorMessage(str_retval)
                    stTrans.Rollback()
                End If
                txtTo.Text = ""
                txtRemarks.Text = ""
                txtFrom.Text = ""
                txtOn.Text = ""

            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If

            End Try
        Catch ex As Exception
            lblError.Text = "Please check the date"
        End Try
    End Sub


    Private Function SaveMONTHDEFINE_D(ByVal p_bsu_id As String, ByVal p_date As String, ByVal p_MNS_ID As String, ByVal p_remarks As String, ByVal p_stTrans As SqlTransaction) As String

        Try
            Dim strCat As String

            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MOH_BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_bsu_id
            pParms(1) = New SqlClient.SqlParameter("@MOD_DATE", SqlDbType.DateTime)
            pParms(1).Value = p_date
            pParms(2) = New SqlClient.SqlParameter("@MOD_MNS_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_MNS_ID
            pParms(3) = New SqlClient.SqlParameter("@MOD_REMARKS", SqlDbType.VarChar, 200)
            pParms(3).Value = p_remarks

            pParms(4) = New SqlClient.SqlParameter("@CatIds", SqlDbType.VarChar, 20)


            pParms(5) = New SqlClient.SqlParameter("@IsHalfDay", SqlDbType.Bit)
            pParms(5).Value = IIf(rblDays.SelectedValue = "0", False, True)


            Dim retval As Integer
            pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue

            For Each item As ListItem In chkCategory.Items
                If item.Selected = True Then
                    '    strCat = strCat & "|" & item.Value
                    pParms(4).Value = item.Value
                    retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveMONTHDEFINE_D", pParms)
                    If pParms(6).Value = "0" Then
                        retval = pParms(6).Value
                    Else
                        retval = "1000"
                        Exit For
                    End If
                End If
            Next


            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            If retval <> "0" Then
                Return "1000"
            End If
            Return 0

        Catch ex As Exception
            Errorlog(ex.Message)
            SaveMONTHDEFINE_D = "1000"
        End Try

    End Function


    Sub bindMonthstatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT MNS_ID, MNS_DESCR, MNS_bHoliday" _
                     & " FROM MONTHSTATUS_M" _
                     & " order by  MNS_ORDER "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddMonthstatus.Items.Clear()
            ddMonthstatus.DataSource = ds.Tables(0)
            ddMonthstatus.DataTextField = "MNS_DESCR"
            ddMonthstatus.DataValueField = "MNS_ID"
            ddMonthstatus.DataBind()
            ddMonthstatus.SelectedIndex = 0
            ds.Tables.Clear()
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                BIND_CHKWEEK(ds.Tables(0).Rows(0)(0).ToString.ToUpper)
                BIND_CHKWEEK(ds.Tables(0).Rows(0)(1).ToString.ToUpper)
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub



    Sub BIND_CHKWEEK(ByVal P_SRC As String)
        Select Case P_SRC
            Case "SUNDAY"
                chkSunday.Checked = True
            Case "MONDAY"
                chkMonday.Checked = True
            Case "TUESDAY"
                chkTuesday.Checked = True
            Case "WEDNESDAY"
                chkWednesday.Checked = True
            Case "THURSDAY"
                chkThursday.Checked = True
            Case "FRIDAY"
                chkFriday.Checked = True
            Case "SATURDAY"
                chkSaturday.Checked = True
        End Select
    End Sub



    Sub bindevents()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT" _
                & " MDH.MOH_BSU_ID, MDD.MOD_DATE, " _
                & " ISNULL(MDD.MOD_REMARKS,'') AS MOD_REMARKS, " _
                & " ISNULL(MSM.MNS_DESCR,'') AS MNS_DESCR," _
                & " MSM.MNS_COLOR" _
                & " FROM MONTHDEFINE_H AS MDH " _
                & " INNER JOIN " _
                & " MONTHDEFINE_D AS MDD " _
                & " ON MDH.MOH_ID = MDD.MOD_MOH_ID" _
                & " LEFT OUTER JOIN " _
                & " MONTHSTATUS_M AS MSM " _
                & " ON MDD.MOD_MNS_ID = MSM.MNS_ID" _
                & " WHERE MDH.MOH_BSU_ID='" & Session("sBSUID") & "' and (isnull(MDH.MOH_ECT_ID,'')='' or MDH.MOH_ECT_ID= " & ddlCategory.SelectedValue & ")"
            dsEvents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetHolidayDetails()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Bsu_ID", Session("sBsuid"))
            pParms(1) = New SqlClient.SqlParameter("@Cat_ID", ddlCategory.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@Date", calDatepicker.SelectedDate.ToString("dd/MMM/yyyy"))


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "getCategoryLeaveData", pParms)

            If dr.HasRows Then
                While dr.Read()
                    txtRemarks.Text = dr("MOD_REMARKS")
                    rblDays.SelectedValue = dr("MOH_IsHalfDay")
                    ddMonthstatus.SelectedValue = dr("MOD_MNS_ID")

                End While

                objConn.Close()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function FIND_DAY(ByVal P_DATE As Date, ByRef P_COLOR As String) As String
        Dim str_events As String = ""
        For i As Integer = 0 To dsEvents.Tables(0).Rows.Count - 1
            If P_DATE = dsEvents.Tables(0).Rows(i)("MOD_DATE") Then
                P_COLOR = dsEvents.Tables(0).Rows(i)("MNS_COLOR")
                If str_events = "" Then
                    str_events = dsEvents.Tables(0).Rows(i)("MOD_REMARKS") & "-" & dsEvents.Tables(0).Rows(i)("MNS_DESCR")
                Else
                    str_events = str_events & "<br>" & dsEvents.Tables(0).Rows(i)("MOD_REMARKS") & "-" & dsEvents.Tables(0).Rows(i)("MNS_DESCR")
                End If
            End If

        Next
        Return str_events


    End Function



    Protected Sub calDatepicker_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calDatepicker.DayRender

        Dim day As CalendarDay = DirectCast(e.Day, CalendarDay)
        Dim cell As TableCell = DirectCast(e.Cell, TableCell)
        Dim str_color As String = ""

        'If Not day.IsOtherMonth Then
        Dim dayStr As String = FIND_DAY(day.Date, str_color)

        Select Case day.Date.DayOfWeek
            Case DayOfWeek.Sunday
                If chkSunday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "SUNDAY" Or ViewState("BSU_WEEKEND2") = "SUNDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If
            Case DayOfWeek.Monday
                If chkMonday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "MONDAY" Or ViewState("BSU_WEEKEND2") = "MONDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If
            Case DayOfWeek.Tuesday
                If chkTuesday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "TUESDAY" Or ViewState("BSU_WEEKEND2") = "TUESDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If
            Case DayOfWeek.Wednesday
                If chkWednesday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "WEDNESDAY" Or ViewState("BSU_WEEKEND2") = "WEDNESDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If
            Case DayOfWeek.Thursday
                If chkThursday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "THURSDAY" Or ViewState("BSU_WEEKEND2") = "THURSDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If
            Case DayOfWeek.Friday
                If chkFriday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "FRIDAY" Or ViewState("BSU_WEEKEND2") = "FRIDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If
            Case DayOfWeek.Saturday
                If chkSaturday.Checked = True Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3E3E")
                End If
                If ViewState("BSU_WEEKEND1") = "SATURDAY" Or ViewState("BSU_WEEKEND2") = "SATURDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EED549") '#E5BC98
                End If


        End Select
        If dayStr <> "" Then
            ' Format the Cell 
            'If day.Date.Day = 15 Then
            cell.BackColor = System.Drawing.ColorTranslator.FromHtml(str_color)
            'Write some description about day 
            cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
            'End If

        End If
        ' End If

    End Sub



    Protected Sub calDatepicker_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calDatepicker.SelectionChanged

        ' Dim dates As DateTime
        txtFrom.Text = calDatepicker.SelectedDates(0).ToString("dd/MMM/yyyy")
        txtTo.Text = calDatepicker.SelectedDates(calDatepicker.SelectedDates.Count - 1).ToString("dd/MMM/yyyy")
        rblDays.SelectedValue = 0
        '    dates = calDatepicker.SelectedDates(0)
        'Next

        Dim str As String = calDatepicker.SelectedDate.ToString("dd/MMM/yyyy")
        txtOn.Text = str
        txtRemarks.Text = ""

        GetHolidayDetails()
        ''If dates > Now.Date And Request.QueryString("nofuture") <> "" Then
        ''    Response.Write("<script language='javascript'> function listen_window(){")
        ''    Response.Write("alert('Future Date is not Allowed');")
        ''    Response.Write("} </script>")
        ''Else
        ''    Dim str As String = dates.ToString("dd/MMM/yyyy")
        ''    'h_SelectedDate.Value = str
        ''    Response.Write("<script language='javascript'> function listen_window(){")
        ''    Response.Write("window.returnValue = '" & str & "';")
        ''    Response.Write("window.close();")
        ''    Response.Write("} </script>")
        ''End If



    End Sub



    Sub Set_Calendar(ByVal Sender As Object, ByVal E As EventArgs) Handles drpCalMonth.SelectedIndexChanged, drpCalYear.SelectedIndexChanged
        'Whenever month or year selection changes display the calendar for that month/year        
        calDatepicker.TodaysDate = CDate(drpCalMonth.SelectedItem.Value & " 1, " & drpCalYear.SelectedItem.Value)
        calDatepicker.DataBind()
    End Sub



    Sub Populate_MonthList(ByVal p_seldt As Date)
        drpCalMonth.Items.Add("January")
        drpCalMonth.Items.Add("February")
        drpCalMonth.Items.Add("March")
        drpCalMonth.Items.Add("April")
        drpCalMonth.Items.Add("May")
        drpCalMonth.Items.Add("June")
        drpCalMonth.Items.Add("July")
        drpCalMonth.Items.Add("August")
        drpCalMonth.Items.Add("September")
        drpCalMonth.Items.Add("October")
        drpCalMonth.Items.Add("November")
        drpCalMonth.Items.Add("December")
        drpCalMonth.Items.FindByValue(MonthName(p_seldt.Month)).Selected = True
    End Sub



    Sub Populate_YearList(ByVal p_seldt As Date)
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 20 To DateTime.Now.Year + 20
            drpCalYear.Items.Add(intYear)
        Next
        drpCalYear.Items.FindByValue(p_seldt.Year).Selected = True
    End Sub



    Protected Sub calDatepicker_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles calDatepicker.VisibleMonthChanged
        drpCalMonth.SelectedIndex = -1
        drpCalMonth.Items.FindByValue(MonthName(calDatepicker.VisibleDate.Month)).Selected = True
        drpCalYear.SelectedIndex = -1
        drpCalYear.Items.FindByValue(calDatepicker.VisibleDate.Year).Selected = True
    End Sub



    Protected Sub chkSunday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSunday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)
    End Sub



    Protected Sub chkMonday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMonday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)

    End Sub



    Protected Sub chkTuesday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTuesday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)

    End Sub



    Protected Sub chkWednesday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkWednesday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)

    End Sub



    Protected Sub chkThursday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkThursday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)

    End Sub



    Protected Sub chkFriday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFriday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)

    End Sub



    Protected Sub chkSaturday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSaturday.CheckedChanged
        calDatepicker.TodaysDate = CDate(calDatepicker.TodaysDate)

    End Sub



    Protected Sub rbOn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOn.CheckedChanged
        tr_On.BgColor = "#e2ffbf" '#FFFFC4
        tr_From.BgColor = "#FFFFFF"

        txtOn.Enabled = True
        CalDTOntext.Enabled = True
        CalDTOnImg.Enabled = True

        txtFrom.Enabled = False
        CalDTFromtext.Enabled = False
        CalDTFromImg.Enabled = False

        txtTo.Enabled = False
        CalDTToText.Enabled = False
        CalDTToImg.Enabled = False

    End Sub

    Protected Sub rbFrom_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFrom.CheckedChanged
        tr_On.BgColor = "#FFFFFF"
        tr_From.BgColor = "#e2ffbf" '#FFFFC4

        txtOn.Enabled = False
        CalDTOntext.Enabled = False
        CalDTOnImg.Enabled = False

        txtFrom.Enabled = True
        CalDTFromtext.Enabled = True
        CalDTFromImg.Enabled = True

        txtTo.Enabled = True
        CalDTToText.Enabled = True
        CalDTToImg.Enabled = True
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each item As ListItem In chkCategory.Items
            If chkAll.Checked = True Then
                item.Selected = True
            Else
                item.Selected = False
            End If
            If item.Value = ddlCategory.SelectedValue Then
                item.Enabled = False
                item.Selected = True
            End If
        Next

    End Sub


    Protected Sub chkCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each item As ListItem In chkCategory.Items
            item.Selected = False
            item.Enabled = True
            If item.Value = ddlCategory.SelectedValue Then
                item.Enabled = False
                item.Selected = True
            End If
        Next
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        chkCategory_DataBound(Nothing, Nothing)
    End Sub
End Class
