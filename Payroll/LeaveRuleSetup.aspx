<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="LeaveRuleSetup.aspx.vb" Inherits="Payroll_LeaveRuleSetup" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
 
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Leave Rule Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" style="border-collapse: collapse;" width="100%">
                               <%-- <tr class="subheader_img">
                                    <td align="left" colspan="2" valign="middle">Leave Rule Setup</td>
                                </tr>--%>
                                <tr>
                                    <td align="left" ><span class="field-label">Employee Category</span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="2" valign="middle">Leave Slab</td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="2">
                                        <asp:GridView ID="gvSlab" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            SkinID="GridViewNormal" DataKeyNames="BLS_ID"
                                            EmptyDataText="&lt;--No Slabs--&gt;">
                                            <Columns>
                                                <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR" />
                                                <asp:TemplateField HeaderText="Cascade Weekend If Exceeds">
                                                    <ItemTemplate>
                                                        <center><asp:TextBox ID="txtiWKD" runat="server" style="text-align:right;" Height="19px" Width="41px" Text='<%# Bind("iWKD") %>'></asp:TextBox></center>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cascade Holiday If Exceeds">
                                                    <ItemTemplate>
                                                        <center><asp:TextBox ID="txtiHOL" runat="server" style="text-align:right;" Height="19px" Width="41px" Text='<%# Bind("iHOL") %>' ></asp:TextBox></center>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cascade If Crosses Weekend">
                                                    <ItemTemplate>
                                                        <center><asp:CheckBox ID="chkWKD" runat="server" Checked='<%# Bind("bWKD") %>' /></center>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cascade  If crosses Holiday">
                                                    <ItemTemplate>
                                                        <center><asp:CheckBox ID="chkHOL" runat="server"  Checked='<%# Bind("bHOL") %>' /></center>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>

                                </tr>
                            </table>
                            </td>
                    </tr>                 
                    <tr>
                        <td  valign="bottom" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                        </td>
                    </tr>                  
                </table>
            </div>
        </div>
    </div>
</asp:Content>

