Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empBankMaster
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim CurBsUnit As String = Trim(Session("sBsuid") & "")
                Dim USR_NAME As String = Trim(Session("sUsr_name") & "")

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or ViewState("MainMnu_code") <> "P050125" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                  
                    If ViewState("datamode") = "view" Then
                        Dim ds As DataSet
                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "SELECT BNK_ID, BNK_DESCRIPTION, BNK_SALTRFNARRATION, " _
                        & " BNK_SHORT, BNK_SHORTNAME, BNK_HEADOFFICE FROM BANK_M where BNK_ID ='" & ViewState("Eid") & "'")
                        If ds.Tables(0).Rows.Count > 0 Then
                            txtBankCode.Text = ds.Tables(0).Rows(0)("BNK_SHORT")
                            txtBankDesc.Text = ds.Tables(0).Rows(0)("BNK_DESCRIPTION")
                            txtBankShort.Text = ds.Tables(0).Rows(0)("BNK_SHORTNAME")
                            txtHeadoffice.Text = ds.Tables(0).Rows(0)("BNK_HEADOFFICE")
                            txtTransferdesc.Text = ds.Tables(0).Rows(0)("BNK_SALTRFNARRATION")
                        End If
                    End If
                End If


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        txtBankDesc.Attributes.Remove("readonly")
        UtilityObj.beforeLoopingControls(Me.Page)
        'when the edit button is clicked set the datamode to edit
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Status As Integer
        Dim editBit As Boolean
        'check the data mode to do the required operation
        If ViewState("datamode") = "edit" Then
            editBit = True
            Dim transaction As SqlTransaction
            'update  the new user
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Status = PayrollFunctions.SaveBANK_M(ViewState("Eid"), txtBankDesc.Text, txtTransferdesc.Text, _
                     txtBankCode.Text, txtBankShort.Text, txtHeadoffice.Text, transaction)
                    'If error occured during the process  throw exception and rollback the process

                    If Status <> 0 Then
                        transaction.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(Status)
                        Exit Sub
                    Else
                        'Store the required information into the Audit Trial table when Edited
                        Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "edit", _
                        Page.User.Identity.Name.ToString, Me.Page)
                         
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        transaction.Commit()
                        Call clearMe()
                        lblError.Text = UtilityObj.getErrorMessage("0")
                    End If

                Catch ex As Exception
                    transaction.Rollback()
                    UtilityObj.Errorlog(ex.Message)
                    lblError.Text = UtilityObj.getErrorMessage(ex.Message)
                End Try
            End Using
        ElseIf ViewState("datamode") = "add" Then
            editBit = False
            Dim transaction As SqlTransaction
            'update  the new user
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try 
                    Status = PayrollFunctions.SaveBANK_M(0, txtBankDesc.Text, txtTransferdesc.Text, _
                     txtBankCode.Text, txtBankShort.Text, txtHeadoffice.Text, transaction)
 
                    'If error occured during the process  throw exception and rollback the process

                    If Status <> 0 Then
                        transaction.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(Status)
                        Exit Sub
                    Else
                        'Store the required information into the Audit Trial table when Edited
                        Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), txtBankCode.Text, _
                        "Insert", Page.User.Identity.Name.ToString, Me.Page)
                       
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        transaction.Commit()
                        Call clearMe()

                        lblError.Text = UtilityObj.getErrorMessage("0")
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    UtilityObj.Errorlog(ex.Message)
                    lblError.Text = UtilityObj.getErrorMessage("1000")
                End Try
            End Using 
        End If 
 
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Call clearMe()
        txtBankDesc.Attributes.Remove("readonly")
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call clearMe()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Status = PayrollFunctions.DeleteBANK_M(ViewState("Eid"), transaction)
                If Status <> 0 Then
                    lblError.Text = UtilityObj.getErrorMessage(Status)
                    transaction.Rollback()
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearMe()
                    lblError.Text = "Record Deleted Successfully"
                End If
            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message, Page.Title)
                transaction.Rollback()
            End Try
        End Using
    End Sub


    Sub clearMe()
        txtBankCode.Text = ""
        txtBankDesc.Text = ""
        txtBankShort.Text = ""
        txtHeadoffice.Text = ""
        txtTransferdesc.Text = ""
    End Sub
 

End Class
