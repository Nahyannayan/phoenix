<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empSalaryHold.aspx.vb" Inherits="Payroll_empSalaryHold" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function ShowBankDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowBankDetail.aspx?ela_id=" + id, "", sFeatures)
            return false;
        }
        function GetEMPNAME() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "reports/\aspx/\SelIDDESC.aspx?ID=EMP";
            var oWnd = radopen(url, "pop_emp");
            // result = window.showModalDialog("reports/\aspx/\SelIDDESC.aspx?ID=EMP", "", sFeatures)
            <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
            return false;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');;
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode[0];
                __doPostBack('<%=h_EMPID.ClientID%>', 'TextChanged');
            }
        }

        function UpdateSum() {
            var sum = 0.0;
            var dsum = 0.0;
            var chk = 0;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0)
                        if (document.forms[0].elements[i].checked == true)
                        { chk = 1; }
                }
                if (document.forms[0].elements[i].name.search(/txtAmount/) > 0) {
                    if (document.forms[0].elements[i].name != '<%=txtAmount.ClientID %>' && chk == 1) {
                        chk = 0;
                        sum += parseFloat(document.forms[0].elements[i].value);
                    }
                }
            }
            document.getElementById('<%=txtAmount.ClientID %>').value = sum;
        }

        function change_chk_state_(chkchecked) {
            var chk_state = chkchecked.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkESD_BANK/) > 0 || document.forms[0].elements[i].name.search(/chkSelall/) > 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        if (document.forms[0].elements[i].disabled == '0') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
            }
            UpdateSum();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <%--  <tr class="subheader_img">
                        <td colspan="2" style="height: 19px" align="left"></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Month</span></td>
                        <td align="left" width="80%">
                            <div class="checkbox-list-full">
                                <asp:RadioButtonList ID="clMonthYear" runat="server" AutoPostBack="True" RepeatColumns="4"
                                    RepeatDirection="Horizontal" Width="98%">
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Narration</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtNarrn" runat="server" MaxLength="300" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr id="Trbank">
                        <td align="left"><span class="field-label">Salary Hold Details</span></td>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td width="100%">
                                        <asp:GridView ID="gvDetails" runat="server" Width="100%" AutoGenerateColumns="False"
                                            EmptyDataText="No Employees Added yet" CssClass="table table-bordered table-row">

                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <input id="chkSelall" runat="server" type="checkbox" onclick="javascript: change_chk_state_(this)" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="chkESD_BANK" runat="server" checked='<%# Bind("hold") %>' value='<%# Bind("ESD_ID") %>' type="checkbox" onclick="UpdateSum();" />

                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="monthyear" HeaderText="Month">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMP_NAME" HeaderText="Emp Name"></asp:BoundField>
                                                <asp:BoundField DataField="EMPNO" HeaderText="Emp No"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAmount" runat="server" OnPreRender="txtAmount_PreRender" Style="text-align: right"
                                                            Text='<%# Bind("amount") %>'></asp:TextBox>

                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="month_year" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmonth_year" runat="server" Text='<%# Bind("month_year") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ESD_EMP_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblESD_EMP_ID" runat="server" Text='<%# Bind("ESD_EMP_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="EHD_RLSDATE" HeaderText="Release Date" DataFormatString="{0:dd/MMM/yyyy}"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtRemark" runat="server" Height="60px" TextMode="MultiLine"
                                                            Width="95%" Text='<%# Bind("EHD_REMARKS") %>' Rows="3"></asp:TextBox>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText=" Locked" Visible="False">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:CheckBox ID="chkLocked" runat="server" Checked='<%# Bind("Locked") %>' />

                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                    <td width="3%">
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetEMPNAME();return false;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Total Amount</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server">
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
