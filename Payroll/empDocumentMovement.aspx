<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDocumentMovement.aspx.vb" Inherits="Payroll_empDocumentMovement" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%--result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtApprovedby.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
            var url = "../Accounts/accShowEmpDetail.aspx?id=EN";
            var oWnd = radopen(url, "pop_empname");            
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID%>').value = NameandCode[1];
                document.getElementById('<%=txtApprovedby.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtApprovedby.ClientID%>', 'TextChanged');
            }
        }


        function GetDocument() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%--result = window.showModalDialog("ShowDocuments.aspx?id=" + document.getElementById('<%=h_Emp_No.ClientID %>').value, "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Docid.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtDocName.ClientID %>').value = NameandCode[1];
                //
                return true;
            }
            return true;--%>
        
            var url = "ShowDocuments.aspx?id=" + document.getElementById('<%=h_Emp_No.ClientID %>').value;
            var oWnd = radopen(url, "pop_getdocument");            
        } 

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Docid.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtDocName.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtDocName.ClientID%>', 'TextChanged');
            }
        }

        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }

        }


        function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;

                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        
        <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getdocument" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Document Movement
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="CLIENTERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table  width="100%" align="center">
                    
                    <tr>
                        <td align="left" width="20%"><span class="field-label"> Employee </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtApprovedby" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgApprovedby" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;" /></td>
                        <td align="left"  width="20%"><span class="field-label">Select Document </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDocName" runat="server" ></asp:TextBox>&nbsp;
                            <asp:ImageButton
                                ID="imgDoc" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetDocument(); return false;" />&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDocName"
                                    CssClass="error" ErrorMessage="Please Select a Document" ValidationGroup="CLIENTERROR">*</asp:RequiredFieldValidator></td>
                    </tr>                  
                    <tr runat="server" id="tr_issue">
                        <td align="left"  valign="middle"><span class="field-label">Issue Date </span></td>
                        <td align="left"   valign="middle">&nbsp;<asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                        <td align="left"   valign="middle"><span class="field-label">Tetative Ret. Date </span></td>
                       
                        <td align="left"  valign="middle">
                            <asp:TextBox ID="txtRetDate" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgRetDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top"><span class="field-label">Approved By </span>
                        </td>
                        <td align="left"   valign="top">
                            <asp:TextBox ID="txtApprovedcomment" runat="server"></asp:TextBox></td>
                         <td  align="left" valign="top"><span class="field-label">Remarks</span></td>
                        <td align="left"  valign="top">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine" ></asp:TextBox></td>
                    </tr>
                   
                    <tr>
                        <td  colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CLIENTERROR" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" />
                <asp:HiddenField ID="h_Docid" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgRetDate" TargetControlID="txtRetDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtRetDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
