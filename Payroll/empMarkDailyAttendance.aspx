<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empMarkDailyAttendance.aspx.vb" Inherits="Payroll_empMarkDailyAttendance"
    Title="Daily Attendance View" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../cssfiles/RadStyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #mydiv {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 30em;
            height: 18em;
            margin-top: -49em; /*set to a negative number 1/2 of your height*/
            margin-left: -35em; /*set to a negative number 1/2 of your width*/
            border: 1px solid #ccc;
            background-color: #f3f3f3;
        }
    


         .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 100%;
            background-image: none !important;
            background-color: #ffffff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 15px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .RadComboBox_Default {
            width: 80% !important;
        }
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image:url(../Images/calendar.gif)!important;
            width: 30px;
            height: 30px;
            z-index:0;
        }
        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active{
            background-image:url(../Images/calendar.gif)!important;
            width: 30px;
            height: 30px;
            background-position: 0;
        }

        .RadPicker { width:80% !important;
        }
        table.RadCalendar_Default {
            background:#ffffff !important;
        }
        
        .auto-style1 {
            height: 56px;
        }
        
        .RadComboBox_Default .rcbFocused .rcbReadOnly .rcbInputCellLeft {
            background-color: #ffffff !important;
        }

        .RadGrid_Default {
            border: 0px !important;
        }
        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border: 1px solid rgba(0,0,0,0.16);
        }
        .RadComboBox table {
            display: inline-table !important;
        }
        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }
        .RadCalendar .rcMainTable {
            background-color: #ffffff !important;
        }

        table td select:disabled, div select:disabled, table td input[type=text]:disabled, table td textarea:disabled, table td input[type=password]:disabled {
    
    padding:0px !important;
    border:none !important;
    box-shadow:none !important;
}

    </style>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script language="javascript" type="text/javascript">
        function OpenEmpLeaveEntryScreen(EmpID, ddLeaveType, txtRemarks, chkDocChecked, sdate, mode) {
            var which = $("#<%=pnlViewPopup.ClientID %>");
            var myIFrame = $("#<%=ifrmViewPopup.ClientID %>");
            var LeaveType = $(ddLeaveType).val();
            if (mode == "close") {
                which.css("display", "none");
                myIFrame.attr("src", "about:blank");
            }
            else {
                myIFrame.css("width", "790px");
                myIFrame.css("height", "440px");
                PlaceControlAtCentre(800, 450);
                which.css("display", "block");
                myIFrame.attr("src", "empLeaveSlabEntry.aspx?sdate=" + sdate + "&empid=" + EmpID + "&eltid=" + LeaveType + "&ShowDocSubmit=0");
            }

            return false;
        }
        function OpenEmployeeDetailScreen(EmpID, sdate, mode) {

            var which = $("#<%=pnlViewPopup.ClientID %>");
            var myIFrame = $("#<%=ifrmViewPopup.ClientID %>");
            if (mode == "close") {
                which.css("display", "none");
                myIFrame.attr('src', 'about:blank');
            }
            else {
                myIFrame.css("width", "795px");
                myIFrame.css("height", "395px");
                PlaceControlAtCentre(800, 400);
                which.css("display", "block");
                myIFrame.attr('src', "empLeaveDetailsPopUp.aspx?empid=" + EmpID + "&sdate=" + sdate);

            }

            return false;
        }
        function OpenBU(EmpID, sdate, mode) {
            var which = $("#<%=pnlViewPopup.ClientID %>");
            var myIFrame = $("#<%=ifrmViewPopup.ClientID %>");
            var hdnEMPID = $("#<%=hdnSelectedEmpID.ClientID %>");
            if (mode == "close") {
                which.css("display", "none");
                myIFrame.attr('src', 'about:blank');
                hdnEMPID.val('');
            }
            else {
                hdnEMPID.val(EmpID);
                myIFrame.css("width", "790px");
                myIFrame.css("height", "390px");
                PlaceControlAtCentre(800, 400);
                which.css("display", "block");
                myIFrame.attr("src", "empLeaveBulkUpdate.aspx?empid=" + EmpID + "&attdate=" + sdate);
            }
            return false;
        }

        $(document).ready(function () {
            $(document).click(function (e) {
                var ClickControlId = e.target.id;
                if (ClickControlId.indexOf("pnlViewPopup") >= 0 || ClickControlId.indexOf("pnlLeaveRange") >= 0) {
                    var which = $(ClickControlId);
                    if (which.is(":visible") == true || which.is(":hidden") == false) {
                        var myFrm = $("#<%=ifrmViewPopup.ClientID %>");
                        myFrm.attr("src", "about:blank");
                        $("#<%=pnlViewPopup.ClientID %>").css("display", "none");
                    }
                }
            });
        });

        function getScreenResolution() {
            var res = $("#<%=hdnResControl.ClientID %>");
            res.val(screen.width + "|" + screen.height);
        }

        function PlaceControlAtCentre(mWidth, mHeight) {
            var myDiv = $("#<%=divViewPopup.ClientID %>");
            var leftMargin, TopMargin, mPaperSizeX, mPaperSizeY, CalcWidth, CalcHeight

            CalcWidth = (90 * screen.width / 100);
            if (mWidth >= CalcWidth)
                mPaperSizeX = CalcWidth;
            else
                mPaperSizeX = mWidth;

            CalcHeight = (90 * screen.height / 100);

            if (mHeight >= CalcHeight)
                mPaperSizeY = CalcHeight;
            else
                mPaperSizeY = mHeight;

            var scrOfY = 0;
            var scrOfX = 0;
            if (typeof (window.pageYOffset) == 'number') {
                //Netscape compliant
                scrOfY = window.pageYOffset;
                scrOfX = window.pageXOffset;
            } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                //DOM compliant
                scrOfY = document.body.scrollTop;
                scrOfX = document.body.scrollLeft;
            } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                //IE6 standards compliant mode
                scrOfY = document.documentElement.scrollTop;
                scrOfX = document.documentElement.scrollLeft;
            }

            leftMargin = (screen.width - mPaperSizeX) / 2;
            TopMargin = (screen.height - mPaperSizeY) / 2;
            myDiv.css("position", "absolute");
            myDiv.css("top", TopMargin + scrOfY - 50);
            myDiv.css("left", leftMargin + scrOfX);
            myDiv.css("width", mWidth);
            myDiv.css("height", mHeight);
            myDiv.css("z-index", "1000");
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> Daily Attendance Marking <asp:Label ID="lblType" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table width="100%">
        
        <tr id="Tr1" runat="server">
            <td align="left" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:Label ID="lblLastAttTransferDecr" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr id="trSuperUser" runat="server">
                        <td align="left" width="20%"><span class="field-label">Approver</span>
                        </td>
                        
                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="RadReportTo" runat="server" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False"  AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr id="trFilter" runat="server">
                        <td align="left" width="20%"><span class="field-label">Date</span>
                        </td>
                       
                        <td align="left" width="30%">
                            <telerik:RadDatePicker ID="RadAttDate" runat="server" DateInput-EmptyMessage="MinDate">
                                <DateInput EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" runat="server">
                                </DateInput>
                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    runat="server">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDatePicker>
                        </td>
                        
                        <td width="20%" align="left"><span class="field-label">Category</span>
                        </td>
                        
                        <td width="30%" align="left">
                            <telerik:RadComboBox ID="RadCategory" runat="server" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False">
                            </telerik:RadComboBox>
                        </td>
                        </tr>

                    <tr>
                        <td width="20%" align="left" class="auto-style1"><span class="field-label">Leave Type</span>
                        </td>
                        
                        <td width="30%" align="left" class="auto-style1">
                            <telerik:RadComboBox ID="RadLeaveType" runat="server" Width="100%" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False">
                            </telerik:RadComboBox>
                        </td>
                       
                        <td width="50%" colspan="2" class="auto-style1">
                            <table width="100%">
                                <tr>
                                    <td float="left" nowrap="nowrap" width="30%">
                                        <asp:Panel ID="pnlProcess" runat="server">
                                            <table>
                                                <tr>
                                                    <td nowrap="nowrap">
                                                        <asp:RadioButton ID="rbnAll" runat="server" CssClass="field-label" Enabled="true" Text="All" GroupName="process" />
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <asp:RadioButton ID="rbnProcessed" runat="server" CssClass="field-label" Enabled="true" Text="Processed"
                                                            GroupName="process" />
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <asp:RadioButton ID="rbnNotProcessed" runat="server" CssClass="field-label" Enabled="true" Text="Not Processed"
                                                            GroupName="process" Checked="True" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td float="left" width="20%">
                                        <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trSearch" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Employee No</span>
                        </td>
                        
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtEmpNo" runat="server" CssClass="RadLabelText" RenderMode="Lightweight">
                            </asp:TextBox>
                        </td>
                        <td width="20%" align="left"><span class="field-label">Employee Name</span>
                        </td>
                        
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="RadLabelText" RenderMode="Lightweight">
                            </asp:TextBox>
                        
                            <asp:Button ID="btnSearch" runat="server" CssClass="button"  Text="Search" />
                        </td>
                        
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" >
                <telerik:RadGrid ID="RadEmpAttendance" runat="server" AllowSorting="True" enableajax="True"
                    ShowStatusBar="True" AutoGenerateColumns="False" GroupingEnabled="False" ShowGroupPanel="True"
                    OnItemDataBound="RadEmpAttendance_ItemDataBound" Width="100%" AllowPaging="True"
                    PageSize="50" GridLines="None">
                    <AlternatingItemStyle CssClass="RadGridAltRow" Font-Size="Small" Font-Bold="false"/>
                    <ItemStyle CssClass="RadGridRow"  Font-Size="Small" Font-Bold="false"/>
                    <MasterTableView GridLines="Both">
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="colImgDetailView">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnEMPID" runat="server" Value='<%# Eval("EMP_ID") %>' />
                                    <asp:ImageButton ID="imgEmpDetailView" runat="server" Height="14px" ImageUrl="~/Images/ButtonImages/PROFILE.png"
                                        ToolTip="Click to view the Leave details." Width="14px" />
                                </ItemTemplate>
                                <HeaderStyle />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="colEmpBulkUpd" Visible="true">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEmpBulkUpd" runat="server" Height="14px" ImageUrl="~/Images/ButtonImages/BULKLEAVE.png"
                                        ToolTip="Click to update the Leave details." Width="14px" />
                                </ItemTemplate>
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="EMP_NO" HeaderText="EMP NO." UniqueName="colEMPNO">
                                <ItemTemplate>
                                    <asp:Label ID="EMP_NOLabel" runat="server" Text='<%# Eval("EMP_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="EMP_NAME" HeaderText="NAME" UniqueName="colEMPNAME"
                                ItemStyle-Wrap="true">
                                <ItemTemplate>
                                    <asp:Label ID="EMP_NAMELabel" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Wrap="True"></ItemStyle>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="DESIGNATION" HeaderText="DESIGNATION" UniqueName="colDesignation">
                                <ItemTemplate>
                                    <asp:Label ID="DESIGNATIONLabel" runat="server" Text='<%# Eval("DESIGNATION") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="ATT_TIME" HeaderText="IN TIME" UniqueName="colInTime">
                                <ItemTemplate>
                                    <asp:Label ID="ATT_TIMELabel" runat="server" Text='<%# Bind("ATT_TIME", "{0:t}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="ATT_STATUS" UniqueName="ColDDStatus" HeaderText="STATUS">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ATT_STATUS")%>' />
                                    <telerik:RadComboBox ID="StatusRadComboBox" runat="server" EmptyMessage="Type here to search..."
                                        EnableScreenBoundaryDetection="False" Width="150px">
                                    </telerik:RadComboBox>
                                    <%--Added by vikranth on 21st July 2019--%>
                                    <asp:Label ID="lblATT_STATUS" Visible="false" ForeColor="Red" runat="server" Text='On E-Leave'></asp:Label>
                                    <%--<asp:HiddenField ID="hdnCheckEMPAttendance" runat="server" Value='<%# Eval("CheckEMPAttendance") %>' />--%>
                                    <asp:HiddenField ID="hdnEMP_bOn_ELeave" runat="server" Value='<%# Eval("EMP_bOn_ELeave") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="" UniqueName="colAttMemo">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelectSlab" runat="server" ImageUrl="~/Images/calendar.gif"
                                        ToolTip="Click to Enter the leave slab." Visible="true" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="REMARKS" UniqueName="colTXTRemarks" HeaderText="REMARKS">
                                <ItemTemplate>
                                    <telerik:RadTextBox ID="RemarksRadTextBox" runat="server" EnableScreenBoundaryDetection="True"
                                        TextMode="MultiLine" Rows="1" Text='<%# Eval("REMARKS") %>' ShouldResetWidthInPixels="False"
                                         CssClass="RadLabelText">
                                    </telerik:RadTextBox>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="bDocChecked" HeaderText="DOC CHECK" UniqueName="colDocCheck">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkDocChecked" runat="server" Checked='<%# Eval("bDocChecked") %>'
                                        Enabled="true" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="bLastUpdateDate" HeaderText="LOG DATE" UniqueName="column"
                                ItemStyle-Wrap="False">
                                <ItemTemplate>
                                    <asp:Label ID="bLastUpdateDateLabel" runat="server" Text='<%# Eval("bLastUpdateDate") %>'>
                                    </asp:Label>
                                    <asp:HiddenField ID="hdnLeaveAvailable" runat="server" Value='<%# Eval("LeaveAvailable") %>' />
                                    <asp:HiddenField ID="hdnLeaveStatus" runat="server" Value='<%# Eval("LeaveStatus") %>' />
                                    <asp:HiddenField ID="hdnProcessed" runat="server" Value='<%# Eval("Processed") %>' />
                                    <asp:HiddenField ID="hdnDefStatusSet" runat="server" Value='<%# Eval("DEFAULT_STATUS_SET") %>' />
                                    <asp:CheckBox ID="chkDocCheckRequired" runat="server" Checked='<%# Bind("DocCheckRequired") %>'
                                        Enabled="false" Visible="false" />
                                </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="SAVE" ImageUrl="~/Images/ButtonImages/SAVE_APPROVE.png"
                                UniqueName="ColMarkAbsent" DataTextField="SaveButton" CommandName="Save">
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridButtonColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="ImageButton">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ClientSettings EnableRowHoverStyle="True">
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td >
                <table width="100%">
                    <tr>
                        <td style="text-align: left;">
                            <asp:Label ID="lblErrorFooter" runat="server" CssClass="error" EnableViewState="False">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:CheckBox ID="chkAll" runat="server" Text="ShowAll" AutoPostBack="true" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:CheckBox ID="chkMarkAllAsPresent" runat="server" CssClass="checkbox field-label" Text="Mark All Present"
                    Visible="False" AutoPostBack="True" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" SkinID="ButtonNormal" Text="Save"
                    Visible="False" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left">
                <asp:Label ID="lblLegandProcessed1" runat="server" CssClass="error" ForeColor="#BB7100"
                    BackColor="#BB7100">AA</asp:Label>
                <asp:Label ID="lblLegandProcessed" runat="server" CssClass="RadLabel" ForeColor="Black">Attendance Not  Processed</asp:Label>
                
                <asp:Label ID="lblLegandProcessed2" runat="server" CssClass="error" ForeColor="#1B80B6"
                    BackColor="#1B80B6">AA</asp:Label>
                <asp:Label ID="lblLegandProcessed0" runat="server" CssClass="RadLabel" ForeColor="Black">Attendance  Processed</asp:Label>
                
                <asp:Label ID="lblLegandProcessed3" runat="server" CssClass="error" ForeColor="Green"
                    BackColor="Green">AA</asp:Label>
                <asp:Label ID="lblLegandLeaveAvailable" runat="server" CssClass="RadLabel" ForeColor="Black">Leave Available (Processed)</asp:Label>
                
                <asp:Label ID="lblLegandProcessed4" runat="server" CssClass="error" ForeColor="Red"
                    BackColor="Red">AA</asp:Label>
                <asp:Label ID="lblLegandLeaveNotAvailable" runat="server" CssClass="RadLabel" ForeColor="Black">No Balance Leave Available (Processed)</asp:Label>
                
                <asp:Label ID="lblAlreadyApliedLeave" runat="server" CssClass="error" ForeColor="#BBCC00"
                    BackColor="#BBCC00">AA</asp:Label>
                <asp:Label ID="Label2" runat="server" CssClass="RadLabel" ForeColor="Black">Already Applied Leave (Not Processed)</asp:Label>
            </td>
        </tr>
    </table>
    <table width="100">
        <tr>
            <td width="100%" align="center">
                <asp:Panel ID="pnlViewPopup" runat="server" CssClass="RadDarkPanlvisible" Style="display: none" Width="100%" Height="1500px">
                    <div class="RadPanelQual" runat="server" id="divViewPopup">
                        <table width="100%">
                            <tr>
                                <td>
                                    <iframe id="ifrmViewPopup" runat="server"></iframe>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnResControl" runat="server" />
    <asp:HiddenField ID="hdnSelATTDate" runat="server" />
    <asp:HiddenField ID="hdnSelectedEmpID" runat="server" />
    <asp:HiddenField ID="hdnOpenType" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>
