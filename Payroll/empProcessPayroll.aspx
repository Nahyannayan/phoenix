<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empProcessPayroll.aspx.vb" Inherits="Payroll_empProcessPayroll" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function show() {
            setTimeout('showGif()', 500);
        }
        function showGif() {
            setTimeout('showGif()', 500);
            document.getElementById('yourImage').style.display = 'block';
        }
        function GetEMPNAME() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "../Common/PopupFormThreeIDHidden.aspx?ID=EMP&multiSelect=true";
            var oWnd = radopen(url, "pop_emp");
            //            result = window.showModalDialog("reports\/aspx\/SelIDDESC.aspx?ID=EMP","", sFeatures)            
            <%--result = window.showModalDialog("../Common/PopupFormThreeIDHidden.aspx?ID=EMP&multiSelect=true", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = NameandCode;
                  __doPostBack('<%=txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }

        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-usd mr-3"></i>
            Process Payroll
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>

                        <td align="left" width="20%">
                            <span class="field-label">Select Month Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="drpCalMonth" runat="Server"
                                Width="103px" SkinID="DropDownListNormal">
                            </asp:DropDownList>&nbsp;
             <asp:DropDownList ID="drpCalYear" runat="Server"
                 Width="97px" SkinID="DropDownListNormal">
             </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Category</span></td>
                        <td align="left" colspan="3">
                            <asp:RadioButtonList ID="rblCategory" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" CssClass="field-label" DataTextField="DESCR" DataValueField="ID" OnSelectedIndexChanged="rblCategory_SelectedIndexChanged" RepeatDirection="Horizontal"></asp:RadioButtonList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M order by ECT_ID"></asp:SqlDataSource>
                        </td>
                        <td></td>
                    </tr>
                    <tr id="Trbank">
                        <td align="left" valign="top">
                            <asp:Label CssClass="field-label" ID="lblEmpName" runat="server" Text="Employee Details"></asp:Label></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtEMPNAME" runat="server" OnTextChanged="txtEMPNAME_TextChanged" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNAME();return false;" /><br />
                            <asp:GridView CssClass="table table-bordered table-row" ID="gvEMPName" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="15" SkinID="GridViewNormal">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMPLOYEE NO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                            <asp:Label ID="lblEmp_ID" runat="server" Text='<%# Bind("EMP_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMPLOYEE NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Timer ID="Timer1" runat="server" Interval="50000">
                            </asp:Timer>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Process Salary" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="h_EMPID" runat="server" />
    <!--  <script> document.getElementById('yourImage').style.display ='none';</script>  -->
</asp:Content>
