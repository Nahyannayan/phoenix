<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empViewQualification.aspx.vb" Inherits="accIUViewJournalVoucher" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Label ID="lblPageCaption" runat="server" Text="Employee Qualification"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" id="tbl">
                    <tr>
                        <td align="center"  valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                        <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
                                        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                                        <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                                        <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"></td>
                                    <asp:HiddenField ID="h_MnuCode" runat="server" />
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvQualDetails" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="GUID" AllowPaging="True" EmptyDataText="No Data Found">
                                <Columns>
                                    <asp:TemplateField SortExpression="GUID" Visible="False" HeaderText="GUID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ID" HeaderText="ID">
                                        <HeaderTemplate>
                                            ID<br />
                                            <asp:TextBox ID="txtID" SkinID="Gridtxt" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnIDSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DESCRIPTION">
                                        <HeaderTemplate>
                                            DESCRIPTION<br />
                                            <asp:TextBox ID="txtDescr" SkinID="Gridtxt" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDescriptoin" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescr" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle  HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SHORT_DESCR">
                                        <EditItemTemplate>
                                            
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeadSecDescr" runat="server" Text="QUALIFICATION CATAGORY"></asp:Label><br />
                                            <asp:TextBox ID="txtSecDescr" SkinID="Gridtxt" runat="server"></asp:TextBox></td>
                                            <asp:ImageButton ID="btnSecDescr" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSecDescr" runat="server" Text='<%# Bind("SEC_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SUBJECT">
                                        <HeaderTemplate>
                                            SUBJECT<br />
                                            <asp:TextBox ID="txtSubject" SkinID="Gridtxt" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSubjectSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SUBJECT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <PagerStyle  HorizontalAlign="left" />
                            </asp:GridView>                            
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

