﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Partial Class Payroll_empLeavePlan
    Inherits System.Web.UI.Page
    '-------------------------------------------------------------------------
    'Author:Swapana.T.V
    'Creation Date:21/Feb/2010
    'Purpose:To show employees leave plan
    '-------------------------------------------------------------------------
    'note: Since version of crystal reports may be different in the server properties for crystal viewer will be
    'different.For disabling group tree the displaypanel property needs to be made "none" in gemsproject
    '
    '
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
       
        ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))

        rs.CacheDuration = 1
        rs.EnableCaching = False
        LoadReports(sender, e)


    End Sub
    Sub LoadReports(ByVal sender As Object, ByVal e As System.EventArgs)
        Try



            Dim rptClass As New rptClass
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

            Try
                newWindow = Request.QueryString("newWindow")
            Catch ex As Exception

            End Try
            Dim rptStr As String = ""
            If Not newWindow Is Nothing Then
                rptStr = "rptClass" + newWindow
            Else
                rptStr = "rptClass"
            End If


            ' rptClass = Session("rptClass")
            rptClass = Session.Item(rptStr)

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass




                'If Not Session("prevClass") Is Nothing And newWindow Is Nothing Then
                '    Session("rptClass") = Session("prevClass")
                '    rptClass = Session("prevClass")
                '    Session("prevClass") = Nothing
                '    Page_Load(sender, e)
                '    Exit Sub
                'End If

                rs.ReportDocument.Load(.reportPath)
                'rs.ReportDocument.DataSourceConnections(0).SetConnection(.crInstanceName, .crDatabase, .crUser, .crPassword) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
                'rs.ReportDocument.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crDatabase, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '
                'rs.ReportDocument.VerifyDatabase()

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If


            End With
        Catch ex As Exception


        End Try
    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            'If vrptClass.Photos IsNot Nothing Then
                            '    SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            'End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next

        myReportDocument.VerifyDatabase()


    End Sub
   
    'Protected Sub EMPLOYEES_List(ByVal value As String)
    '    Try
    '        Dim str_filter As String = String.Empty
    '        Dim str_txtDescr1 As String = String.Empty
    '        Dim str_txtDescr2 As String = String.Empty
    '        Dim str_txtDescr3 As String = String.Empty
    '        Dim str_txtDescr4 As String = String.Empty
    '        Dim str_txtDescr5 As String = String.Empty
    '        Dim str_txtDescr6 As String = String.Empty
    '        Dim str_txtDescr7 As String = String.Empty
    '        Dim str_act, str_doctype As String
    '        str_act = Request.QueryString("act")
    '        str_doctype = Request.QueryString("type")

    '        Dim txtSearch As New TextBox
    '        If gvGroup.Rows.Count > 0 Then
    '            ''code
    '            Dim str_Sid_search() As String
    '            str_Sid_search = h_Selected_menu_1.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
    '            str_txtDescr1 = txtSearch.Text.Trim
    '            str_filter = set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

    '            str_Sid_search = h_selected_menu_2.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
    '            str_txtDescr2 = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

    '            str_Sid_search = h_Selected_menu_3.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
    '            str_txtDescr3 = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR3", str_Sid_search(0), str_txtDescr3)

    '            str_Sid_search = h_Selected_menu_4.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
    '            str_txtDescr4 = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR4", str_Sid_search(0), str_txtDescr4)

    '            str_Sid_search = h_Selected_menu_5.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
    '            str_txtDescr5 = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR5", str_Sid_search(0), str_txtDescr5)



    '        End If
    '        Dim str_Sql As String
    '        Dim str_query_header As String = String.Empty
    '        Dim BSU_ID As String = ""
    '        Dim USR_ID As String = ""
    '        BSU_ID = Session("sBsuid").ToString
    '        USR_ID = Session("sUsr_name").ToString

    '        If (value = "EMPLOYEES_LeavePlan") Then
    '            str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
    '           " SELECT ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2,ELT_DESCR as  DESCR3,  " & _
    '           " dbo.fnFormatDate(ELA_DTFROM,'DD-MON-YYYY')    AS DESCR4,dbo.fnFormatDate(ELA_DTTO,'DD-MON-YYYY')   AS DESCR5 , DATEDIFF(D, ELA_DTFROM, ELA_DTTO) AS DESCR6 " & _
    '           " FROM APPROVAL_S A inner join EMPLEAVEAPP L" & _
    '           " on A.APS_DOC_ID=L.ELA_ID" & _
    '           " inner join EMPLOYEE_M E on" & _
    '           " L.ELA_EMP_ID = E.EMP_ID" & _
    '           " inner join EMPLEAVETYPE_M on" & _
    '           " L.ELA_ELT_ID=EMPLEAVETYPE_M.ELT_ID WHERE  " & _
    '          " APS_DOCTYPE='LEAVE' AND  ELA_APPRSTATUS = 'A' AND " & _
    '          " APS_USR_ID in (select USR_EMP_ID from USERS_M where USR_NAME='" & USR_ID & "')) DB WHERE 1=1 " & str_filter & _
    '           "|" & "Employee ID|Employee Name|Leave Type|From Date|To Date|Days"
    '        End If

    '        str_Sql = str_query_header.Split("||")(0)
    '        Dim str_headers As String()
    '        str_headers = str_query_header.Split("|")
    '        Dim ds As New DataSet
    '        If str_Sql <> "" Then
    '            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & "order by DESCR2")
    '        End If
    '        gvGroup.DataSource = ds
    '        If ds.Tables(0).Rows.Count = 0 Then
    '            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
    '            gvGroup.DataBind()
    '            Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count

    '            gvGroup.Rows(0).Cells.Clear()
    '            gvGroup.Rows(0).Cells.Add(New TableCell)
    '            gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
    '            gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '            gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
    '            ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
    '            'gvGroup.HeaderRow.Visible = True
    '        Else
    '            gvGroup.DataBind()

    '        End If
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
    '        txtSearch.Text = str_txtDescr1
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
    '        txtSearch.Text = str_txtDescr2
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
    '        txtSearch.Text = str_txtDescr3
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
    '        txtSearch.Text = str_txtDescr4
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
    '        txtSearch.Text = str_txtDescr5


    '        Dim lblheader As New Label
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
    '        lblheader.Text = str_headers(1)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
    '        lblheader.Text = str_headers(2)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr3")
    '        lblheader.Text = str_headers(3)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr4")
    '        lblheader.Text = str_headers(4)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr5")
    '        lblheader.Text = str_headers(5)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDESCR6")   'V1.1
    '        lblheader.Text = str_headers(6)


    '        set_Menu_Img()

    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub
    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function
 

    
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
   
   

   
End Class
