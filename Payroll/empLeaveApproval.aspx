<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empLeaveApproval.aspx.vb" Inherits="Payroll_empLeaveApproval" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 10%;
            position: fixed;
            width: 50%;
        }
    </style>
    <script lang="javascript" type="text/javascript">

        function shwPopUp() {
            var url = 'empLeavePlan.aspx?id=EMPLOYEES_LeavePlan';
            bpPopup = window.open(url, "bpPopup", 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=600,height=400,left = 350,top = 262');



        }
        //function ShowLeaveDetail(id) {
        //    var sFeatures;
        //    sFeatures = "dialogWidth: 700px; ";
        //    sFeatures += "dialogHeight: 450px; ";
        //    sFeatures += "help: no; ";
        //    sFeatures += "resizable: no; ";
        //    sFeatures += "scroll: yes; ";
        //    sFeatures += "status: no; ";
        //    sFeatures += "unadorned: no; ";
        //    var NameandCode;
        //    var result;
        //    result = window.showModalDialog("ShowEmpLeave.aspx?ela_id=" + id, "", sFeatures)

        //    return false;
        //}



        function ShowLeaveDetail(Id) {
            Popup('ShowEmpLeave.aspx?ela_id=' + Id)

        }
        function ShowLeaveHistory(Id) {
            Popup('empLeaveApplicationListPopup.aspx?id=' + Id)

        }


        function GoToPage(Id) {

            $.fancybox({
                type: 'iframe',
                maxWidth: 300,
                href: Id,
                maxHeight: 600,
                fitToView: false,
                width: '100%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
            });

        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:radwindowmanager id="RadWindowManager1" showcontentduringload="false" visiblestatusbar="false"
        reloadonshow="true" runat="server" enableshadow="true">
        <Windows>
            <telerik:RadWindow ID="popup" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" width="100%">
                    <tr>
                        <td colspan="3" align="left">
                            <asp:ValidationSummary ID="vsErrors" runat="server" ValidationGroup="valSave"
                                CssClass="error" />
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"
                                CssClass="error" SkinID="Error" Text=""></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <uc1:usrmessagebar runat="server" id="usrMessageBar" />
                        </td>
                    </tr>
                    <tr>

                        <td align="left" colspan="2">
                            <%-- <asp:ImageButton ID="imgAmended" Visible="false" runat="server" Height="20px" ImageUrl="~/Images/animated-news.gif"
                                Width="25px" BorderColor="#CC9900" BorderStyle="Solid" />
                            <asp:LinkButton ID="lblViewAmend" runat="server">View Amendments </asp:LinkButton>--%>
                        </td>
                        <td align="right">
                            <asp:RadioButtonList ID="rblOptions" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                RepeatDirection="Horizontal" Height="16px">
                                <asp:ListItem Selected="True" Value="Open">Open Request</asp:ListItem>
                                <asp:ListItem Value="M">Request for Amendment</asp:ListItem>
                                <asp:ListItem Value="A">Approved</asp:ListItem>
                                <asp:ListItem Value="All">All</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <%-- <asp:Label ID="lblAmended" runat="server" Text='<%#  IIf( Eval("APS_DOCTYPE") ="LEAVE-M", "Amended", "Normal ") %>'></asp:Label>--%>
                    <tr>
                        <td width="15%" align="left">
                            <span class="field-label">Select Employee</span>
                        </td>

                        <td width="25%" align="left">

                            <asp:TextBox ID="txtEmpSearch" runat="server"></asp:TextBox>
                        </td>
                        <td width="25%" align="left">
                            <asp:Button ID="btnFilter" runat="server" CssClass="button" Text="Search" CausesValidation="true" /></td>
                        <td width="35%"></td>
                    </tr>
                    <tr>
                        <td class="" colspan="4" width="100%" align="center">

                            <asp:GridView ID="gvLeaveApproval" runat="server" Width="100%" AutoGenerateColumns="False"
                                DataKeyNames="EMPNO" EmptyDataText="No Leave application pending for approval"
                                AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" Height="16px" ImageUrl="~/Images/cal.gif"
                                                ToolTip="Click to view the Leave details." Width="19px" OnClientClick="return false;" />
                                            <ajaxtoolkit:popupcontrolextender id="imgViewPopupExtender" runat="server" popupcontrolid="Panel2"
                                                targetcontrolid="imgView" dynamiccontextkey='<%# Bind("ACTUAL_ELA_ID") %>' dynamiccontrolid="Panel2"
                                                dynamicservicemethod="GetDynamicContent" position="Bottom" commitproperty="">
                                            </ajaxtoolkit:popupcontrolextender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="EMPNO" HeaderText="Staff ID" ReadOnly="True" SortExpression="EMPNO"
                                        Visible="False" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="img_Photo" runat="server" ImageUrl='<%# Bind("EMD_PHOTO") %>' Width="100px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EMP_NAME" HeaderText="Name" ReadOnly="True" SortExpression="EMP_NAME" />
                                    <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave type" SortExpression="ELT_DESCR"
                                        ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Leave Status" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmpLeaves" runat="server" CausesValidation="False" OnClientClick="return false;">View</asp:LinkButton>
                                            <ajaxtoolkit:popupcontrolextender id="PopupControlExtender1" runat="server" popupcontrolid="Panel2"
                                                targetcontrolid="lnkEmpLeaves" dynamiccontextkey='<%# Bind("APS_DOC_ID") %>' dynamiccontrolid="Panel2"
                                                dynamicservicemethod="GetDynamicContent" position="Bottom" commitproperty="">
                                            </ajaxtoolkit:popupcontrolextender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                        HtmlEncode="False" SortExpression="ELA_DTFROM" ReadOnly="True"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="To Date"
                                        HtmlEncode="False" SortExpression="ELA_DTTO" ReadOnly="True"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="LEAVEDAYS" HeaderText="Leave Days" ReadOnly="True"
                                        SortExpression="LEAVEDAYS" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="APS_STATUS" HeaderText="Status" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Details" SortExpression="APS_REMARKS">
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Bind("ELA_REMARKS") %>'
                                                OnClientClick="<%# &quot;ShowLeaveDetail('&quot; & Container.DataItem(&quot;ID&quot;) & &quot;');return false;&quot; %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>Details</HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ELA_SubmitDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Submit Date"
                                        HtmlEncode="False" SortExpression="ELA_SubmitDate" ReadOnly="True" />
                                    <%--<asp:TemplateField HeaderText="Submit Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubmitDate" runat="server" Text='<%# Eval("ELA_SubmitDate") %>'></asp:Label>
                                            <br />
                                            <asp:ImageButton ID="imgAmended" Visible='<%#  IIf( Eval("APS_DOCTYPE") ="LEAVE-M", true, false) %>' runat="server" ImageUrl="~/Images/amended.gif" Width="100px" />
                                             <asp:Label ID="lblAmended" runat="server" Text='<%#  IIf( Eval("APS_DOCTYPE") ="LEAVE-M", "Amended", "Normal ") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbApprove" runat="server" CausesValidation="False" CommandName="Edit"
                                                Text="Approve/Hold" Visible="False"></asp:LinkButton>
                                            <asp:Button ID="btnApproveHold" runat="server" CausesValidation="true" CommandName="Edit"
                                                CssClass="button" Text="Approve/Hold" />

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMP_ID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APS_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAPS_ID" runat="server" Text='<%# Bind("APS_ID") %>'></asp:Label>
                                            <asp:Label ID="lblActualELA_ID" runat="server" Text='<%# Bind("ACTUAL_ELA_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ELA_ELT_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblELT_ID" runat="server" Text='<%# Bind("ELA_ELT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP_ECT_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblECT_ID" runat="server" Text='<%# Bind("EMP_ECT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCanEdit" runat="server" Text='<%# Bind("CanEdit") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ELA_ELT_ID" HeaderText="ELT_ID" ReadOnly="True" SortExpression="LEAVEDAYS"
                                        Visible="False" />
                                    <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACTUAL_ELA_ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ACTUAL_ELA_ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDoc" runat="server"
                                                AlternateText=' <%#Eval("DOC_ID")%> ' ToolTip="Click To View Document"
                                                ImageUrl="../Images/ViewDoc.png" Visible="false" OnClick="imgDoc_Click" />
                                            &nbsp;
                                     <asp:HyperLink ID="btnImgDoc" runat="server" ImageUrl="../Images/ViewDoc.png">View</asp:HyperLink>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doc Verified">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocChk" runat="server" Text='<%# Bind("ELA_bDocVerified") %>' Visible="false"></asp:Label>
                                            <asp:CheckBox ID="chkDocchk" runat="server" Checked='<%# Eval("ELA_bDocVerified") %>' Enabled="false" Visible='<%# Eval("ELA_bDocVerified") %>'></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave History">
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lnkhistory" runat="server" Text="Leave History"
                                                OnClientClick="<%# &quot;ShowLeaveHistory('&quot; & Container.DataItem(&quot;EMP_ID&quot;) & &quot;');return false;&quot; %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Compare">
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lnkCompareRpt" runat="server" Text="Compare"
                                                OnClick="lnkCompareRpt_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>

                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle BackColor="#DEFFB4" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <table runat="server" id="tblDPTLeave" width="100%">
                    <tr class="title-bg">
                        <td colspan="10">Department Leave Plans
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="10%" style="vertical-align: bottom">
                            <span class="field-label">From</span>
                        </td>
                        <td align="left" width="25%" style="vertical-align: bottom">
                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            <ajaxtoolkit:calendarextender
                                id="DocDate" runat="server" cssclass="MyCalendar" format="dd/MMM/yyyy" popupbuttonid="imgFrom"
                                targetcontrolid="txtFrom">
                            </ajaxtoolkit:calendarextender>
                            <ajaxtoolkit:calendarextender id="CalendarExtender2" runat="server" cssclass="MyCalendar"
                                format="dd/MMM/yyyy" popupbuttonid="txtFrom" targetcontrolid="txtFrom">
                            </ajaxtoolkit:calendarextender>
                        </td>
                        <td align="left" width="10%" style="vertical-align: bottom">
                            <span class="field-label">To</span>
                        </td>
                        <td align="left" width="25%" style="vertical-align: bottom">

                            <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server" cssclass="MyCalendar"
                                format="dd/MMM/yyyy" popupbuttonid="imgto" targetcontrolid="txtTo">
                            </ajaxtoolkit:calendarextender>
                            <ajaxtoolkit:calendarextender id="CalendarExtender3" runat="server" cssclass="MyCalendar"
                                format="dd/MMM/yyyy" popupbuttonid="txtTo" targetcontrolid="txtTo">
                            </ajaxtoolkit:calendarextender>
                        </td>
                        <td align="left" width="30%" style="vertical-align: bottom">
                            <asp:CheckBox ID="chkAll" runat="server" Text="All " ToolTip="Check to view leave plan of all employees in your department"
                                Visible="false" />
                            <%-- <asp:RadioButtonList ID="rblChoice" runat="server" RepeatDirection="Horizontal" CssClass="field-label" >
                            <asp:ListItem Value="0" Text="Employees in date range" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="All"></asp:ListItem>
        </asp:RadioButtonList>--%>

                            <asp:Button ID="lnkLeavePlans" align="right" CssClass="button" runat="server" Text="Compare"></asp:Button>
                        </td>
                    </tr>
                </table>
                <br />
                <div class="text-center">
                    <asp:Button ID="lnkbackbtn" CssClass="button" runat="server" OnClick="lnkbackbtn_Click" Text="Back"></asp:Button>
                </div>
                <asp:Panel ID="pnlLeave" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div id="divPanel" class="panel-cover inner_darkPanlAlumini" runat="server">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; border-radius: 50%; cursor: pointer;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <table runat="server" id="tblApprovalDetails" width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <uc1:usrmessagebar runat="server" id="usrMessageBar2" />
                                        </td>
                                    </tr>
                                    <tr class="title-bg">
                                        <td colspan="4">Approval Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Employee Name</span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblEmpName" runat="server"></asp:Label>
                                        </td>
                                        <td rowspan="4">
                                            <asp:ImageButton ID="imgProfile" CssClass="img-profile" runat="server" Height="117px" Width="100px" ImageUrl="~/Images/Photos/no_image.gif"></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr id="trPrevLeavePeriod" runat="server">
                                        <td align="left">
                                            <span class="field-label">Original Leave Plan</span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPrevLeavePeriod" runat="server" CssClass="text-danger"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <span class="field-label">Leave Type</span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLeaveType" runat="server" CssClass="inputbox" Height="19px"></asp:Label>
                                            <asp:CheckBox ID="chkDocverify" runat="server" Text="Documents verified" CssClass="field-label" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="">
                                            <span class="field-label">Leave Period</span>
                                        </td>

                                        <td class="" align="left">
                                            <asp:Label ID="lblLeavePeriod" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Leave Days</span></td>
                                        <td class="" align="left">
                                            <asp:Label ID="lblLeavedays" runat="server"
                                                Style="text-align: right;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Details</span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDetails" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Remarks </span>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                                                TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%-- <tr id="trChangeStatus" runat="server">
                                        <td align="left" class="">
                                            <span class="field-label">Change Status <span style="color: #c00000">*</span></span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddLeaveStatus" runat="server" CssClass="listbox">
                                                <asp:ListItem Value="A">Approve</asp:ListItem>
                                                <asp:ListItem Value="H">Hold</asp:ListItem>
                                                <asp:ListItem Value="R">Reject</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;
                <asp:LinkButton ID="lnkPlanner" runat="server" ValidationGroup="Datas" Style="display: none;">Leave Planner</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                    </tr>--%>
                                    <tr id="trMsg" runat="server" visible="false">
                                        <td colspan="3" align="left">
                                            <asp:Label ID="lblErrMsg" runat="server" EnableViewState="False" CssClass="text-danger" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">&nbsp; &nbsp; &nbsp; 
                                            <asp:Button ID="btnApprove" runat="server" CssClass="button"
                                                Text="Approve" CausesValidation="true" ValidationGroup="valSave" />&nbsp;<asp:Button ID="btnReject" runat="server" CssClass="button"
                                                    Text="Reject" CausesValidation="true" ValidationGroup="valSave" />&nbsp;
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Back" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:Panel ID="Panel2" runat="server">
    </asp:Panel>
    <asp:HiddenField ID="hfElt_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfEmpId" runat="server"></asp:HiddenField>
</asp:Content>
