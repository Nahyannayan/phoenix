﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empDailyAttendanceBulkUpload.aspx.vb" Inherits="Payroll_empDailyAttendanceBulkUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .error{
            font-size:14px !important;
        }
    </style>
     
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblMonthly" runat="server" Text="Biometric Data Upload"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 98%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary6" runat="server" ValidationGroup="groupM1"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" style="width: 100%">
                            <table align="center" cellpadding="5" cellspacing="0"
                                style="border-collapse: collapse; width: 100%">                                
                                <tr id="trExcelUpload" runat="server">
                                    <td align="left"><span class="field-label">Bulk Upload</span></td>
                                    <td align="left" >
                                        <asp:FileUpload ID="flUpExcel" runat="server" />
                                        <asp:Button ID="btnLoad" runat="server" CssClass="button" Text="Load" />
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvMonthE_D" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPLOYEE_ID" CssClass="table table-bordered table-row"
                                            Width="100%" SkinID="GridViewNormal">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" HorizontalAlign="Center" />
                                            <Columns>
                                                <%--<asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemStyle Width="15%" />
                                                    <HeaderStyle Width="15%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:BoundField DataField="EMPLOYEE_ID" HeaderText="Employee ID" Visible="False">
                                                    <ItemStyle Width="12%" />
                                                    <HeaderStyle Width="12%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMPLOYEE_No" HeaderText="Employee No">
                                                    <ItemStyle Width="12%" />
                                                    <HeaderStyle Width="12%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ATTD_DATE" HeaderText="Date">
                                                    <ItemStyle Width="10%" />
                                                    <HeaderStyle Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PROJECT_NO" HeaderText="Project No">
                                                    <ItemStyle Width="12%" Wrap="True" />
                                                    <HeaderStyle Width="12%" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IN_DATE" HeaderText="IN-DATE"
                                                    ApplyFormatInEditMode="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <ItemStyle Width="10%" Wrap="True" />
                                                    <HeaderStyle Width="10%" Wrap="True" />
                                                </asp:BoundField>
                                            
                                            <asp:BoundField DataField="IN_TIME" HeaderText="IN TIME"
                                                    ApplyFormatInEditMode="True" >
                                                    <ItemStyle Width="10%" Wrap="True" />
                                                    <HeaderStyle Width="10%" Wrap="True" />
                                                </asp:BoundField>
                                          
                                            <asp:BoundField DataField="OUT_DATE" HeaderText="OUT-DATE"
                                                    ApplyFormatInEditMode="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <ItemStyle Width="10%" Wrap="True" />
                                                    <HeaderStyle Width="10%" Wrap="True" />
                                                </asp:BoundField>
                                           
                                            <asp:BoundField DataField="OUT_TIME" HeaderText="OUT TIME">
                                                    <ItemStyle Width="10%" Wrap="True" />
                                                    <HeaderStyle Width="10%" Wrap="True" />
                                                </asp:BoundField>                                            
                                                <asp:BoundField DataField="BREAK_TIME" HeaderText="BREAK TIME">
                                                    <ItemStyle Width="10%" Wrap="True" />
                                                    <HeaderStyle Width="10%" Wrap="True" />
                                                </asp:BoundField>
                                                
                                          </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <%--<asp:Button
                                            ID="btnAdd" runat="server" Text="Add" CssClass="button" />--%>
                                        <asp:Button
                                            ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button
                                            ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EDD_EMP_ID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

