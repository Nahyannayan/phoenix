<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empQualification.aspx.vb" Inherits="Payroll_empQualification" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calender mr-3"></i>
            <asp:Label ID="lblFormCaption" runat="server" Text="Employee Qualification Category"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0" >
                    <tr>
                        <td align="left" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error" ValidationGroup="EMPQUACAT" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td   valign="top">
                            <table align="center"  cellpadding="5" cellspacing="0"
                                style="width: 100%; height: 100%">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="8" valign="middle">
                           </td>
                    </tr>--%>
                                <tr runat="server" id="trMainDescr">
                                    <td align="left" width="20%"  >
                                        <asp:Label ID="lblMainDescription" runat="server" Text="Qualification Description" CssClass="field-label"></asp:Label></td>
                                   
                                    <td align="left" width="30%"  >
                                        <asp:TextBox ID="txtQDescr" runat="server" CssClass="inputbox"  MaxLength="100"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter qualification Description"
                                            ValidationGroup="EMPQUACAT" ControlToValidate="txtQDescr">*</asp:RequiredFieldValidator></td>
                                    <td>

                                    </td>
                                </tr>
                                <tr runat="server" id="trSubDescr">
                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblSubDescription" runat="server" Text="Short Description" CssClass="field-label"></asp:Label></td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtShortDescr" runat="server" CssClass="inputbox" 
                                            MaxLength="10" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Short Description of Qualification"
                                            ValidationGroup="EMPQUACAT" ControlToValidate="txtShortDescr">*</asp:RequiredFieldValidator></td>
                                     <td>

                                    </td>
                                </tr>
                                <tr runat="server" id="trQualification">
                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblQualification" runat="server" Text="Qualification Category" CssClass="field-label"></asp:Label></td>
                                  
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddQualification" runat="server" >
                                        </asp:DropDownList></td>
                                     <td>

                                    </td>
                                </tr>
                                <tr runat="server" id="trSubject">
                                    <td align="left"  width="20%">
                                        <asp:Label ID="lblSubject" runat="server" Text="Subject" CssClass="field-label"></asp:Label></td>
                                
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddSubject" runat="server" >
                                        </asp:DropDownList></td>
                                     <td>

                                    </td>
                                </tr>
                            </table>
                           </td>
                    </tr>
                    <tr>
                        <td   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="EMPQUACAT" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>
                    
                </table>

            </div>
        </div>
    </div>
</asp:Content>

