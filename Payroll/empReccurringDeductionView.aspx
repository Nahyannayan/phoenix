<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empReccurringDeductionView.aspx.vb" Inherits="Accounts_accccViewCreditcardReceipt" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
     }
     window.onload = scroll_page;
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-usd mr-3"></i>
          <asp:Label ID="lblHeader" runat="server">EMPLOYEE MONTHLY DEDUCTION</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" /><input id="h_SelectedId"
                                runat="server" type="hidden" value="-1" /><input id="h_selected_menu_1" runat="server"
                                    type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden"
                                        value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden"
                                value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input
                                id="h_Selected_menu_5" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_6"
                                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                        type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />&nbsp; &nbsp;
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">
                    <tr width="100%">
                        <td align="center" valign="top" width="100%">
                            <asp:GridView ID="gvEMPMONTHLYDEDUCTIONS" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="20">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP NO">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            EMPLOYEE No.<br />
                                            <asp:TextBox ID="txtEmpNo" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP NAME">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            EMPLOYEE NAME<br />
                                            <asp:TextBox ID="txtEmpName" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ERN_DESCR" HeaderText="Earning/Ded. Code" />
                                    <asp:BoundField DataField="ERD_STARTDT" HeaderText="From"
                                        DataFormatString="{0:dd/MMM/yyyy}" ControlStyle-Width="50px">

                                        <ControlStyle Width="50px"></ControlStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                    </asp:BoundField>
                                    <asp:BoundField DataField="ERD_ENDDT" HeaderText="To"
                                        DataFormatString="{0:dd/MMM/yyyy}" ControlStyle-Width="50px">
                                        <ControlStyle Width="50px"></ControlStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="AMOUNT">

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Amount
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ESH_EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ERD_ID" SortExpression="ERD_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblERDID" runat="server" Text='<%# Bind("ERD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False" HeaderText="View">
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>&nbsp;&nbsp;
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <a id='detail'></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;<br />
                            <a id='child'></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        cssdropdown.startchrome("chromemenu1")
        cssdropdown.startchrome("chromemenu3")

    </script>
</asp:Content>
