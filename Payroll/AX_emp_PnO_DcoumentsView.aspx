﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AX_emp_PnO_DcoumentsView.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Payroll_AX_emp_PnO_DcoumentsView" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <%-- <link href="cssfiles/axstyles.css" rel="stylesheet" />--%>


    <script language="javascript" type="text/javascript">       



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            P&O Document Requests
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td  valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>                       
                    </tr>
                </table>
                <table style="border-collapse: collapse;" align="center"  cellpadding="5" cellspacing="0" width="100%">
                   <%-- <tr class="subheader_img" valign="top">
                        <td align="left" valign="middle">P&O Document Requests</td>
                    </tr>--%>
                    <tr>
                        <td align="center" valign="top" class="matters" width="100%">
                            <asp:GridView ID="gvDocuments" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                SkinID="GridViewView" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>

                                    <asp:BoundField DataField="EPnO_AX_RECID" HeaderText="Document ID" SortExpression="ELT_DESCR"></asp:BoundField>
                                    <asp:BoundField DataField="PnO_Doc_Description" HeaderText="Document Type" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="EPnO_LOGDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Applied On"
                                        HtmlEncode="False" SortExpression="EPnO_LOGDT"></asp:BoundField>

                                    <asp:BoundField DataField="PnOStatus_Descr" HeaderText="Status" ReadOnly="True" SortExpression="PnOStatus_Descr"></asp:BoundField>
                                    <asp:BoundField DataField="EPnO_HR_NOTES" HeaderText="HR Notes" ReadOnly="True" SortExpression="EPnO_HR_NOTES"></asp:BoundField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EPnO_REQUEST_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestID" runat="server" Text='<%# Bind("EPnO_REQUEST_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_No" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
