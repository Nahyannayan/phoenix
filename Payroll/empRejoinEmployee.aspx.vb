Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.xml
Partial Class Payroll_empRejoinEmployee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    'Version        Author          Date             Change
    '1.1            Swapna          28/Mar/2011      To enter employee rejoin date for leave applications

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If

            FillCATNames(h_CATID.Value)

            FillEmpNames(h_EMPID.Value)

            StoreEMPFilter()
            'If h_EMPID.Value <> Nothing And h_EMPID.Value <> "" And h_EMPID.Value <> "undefined" Then
            '    FillACTIDs(h_EMPID.Value)
            '    'h_BSUID.Value = ""
            'End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = " AND EMP_BSU_ID='" & Session("SBSUID") & "'"

        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Function GetSalaryProcessedDate(ByVal EmpID As Integer, ByVal Bsu_ID As String, ByVal Rejoindt As Date) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", EmpID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Bsu_ID)
            pParms(2) = New SqlClient.SqlParameter("@RejoinDt", Rejoindt)
            pParms(3) = New SqlClient.SqlParameter("IsProcessed", SqlDbType.Bit)
            pParms(3).Direction = ParameterDirection.ReturnValue

            Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GetSalaryProcessedDate", pParms)
            Return pParms(3).Value
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'If Not IsDate(txtRejoindt.Text) Then
        '    lblError.Text = "Invalid Date!!!"
        '    Exit Sub
        'End If
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim RETVAL As Integer = 1000
        Dim RetvalnMesage As String = ""
        Dim retMsg As String = ""
        Dim FinalMsg As String = ""
        Dim IsSalProcessed As Boolean = False
        Dim boolNothingSelected As Boolean = True
        Dim retMsgSal As String = ""
        Try
            For i As Integer = 0 To gvEmpDetails.Rows.Count - 1
                Dim lblid As New Label
                Dim chkEmployee As New HtmlInputCheckBox
                Dim txtDate As New TextBox '
                Dim separator As String() = New String() {"||"}
                chkEmployee = TryCast(gvEmpDetails.Rows(i).FindControl("chkEmployee"), HtmlInputCheckBox)
                txtDate = TryCast(gvEmpDetails.Rows(i).FindControl("txtDate"), TextBox)
                Dim lblChkEMPID As New Label
                lblChkEMPID = TryCast(gvEmpDetails.Rows(i).FindControl("lblChkEMPID"), Label)

                If chkEmployee.Checked Then
                    If IsDate(txtDate.Text) Then
                        boolNothingSelected = False
                        IsSalProcessed = GetSalaryProcessedDate(lblChkEMPID.Text, Session("sbsuid"), txtDate.Text)

                        FinalMsg = getErrorMessage("0")
                        RetvalnMesage = PayrollFunctions.UpdateLeaveStatus(chkEmployee.Value, txtDate.Text, stTrans, retMsg)
                        retMsg = RetvalnMesage 'myValarr(0)
                        If retMsg = getErrorMessage("0") Then
                            retMsg = ""
                            lblError.Text = getErrorMessage("0")
                            RETVAL = 0
                        End If
                        If RETVAL <> 0 Then
                            lblError.Text = retMsg
                            FinalMsg = ""
                            Exit For
                        End If
                    Else
                        lblError.Text = "Invalid Date on Line :-" & i + 1
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
            Next
            If boolNothingSelected Then
                lblError.Text = "Kindly Select Atleast One Employee"
                Exit Sub
            End If
            If RETVAL = 0 Then
                stTrans.Commit()
                bindRejoinlist()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "Vacation Processing", _
                              "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            Else
                stTrans.Rollback()
            End If

            If FinalMsg = "" Then
                FinalMsg = getErrorMessage("0")
            End If
            FinalMsg.Trim()
            If FinalMsg.StartsWith(" ,") Then
                FinalMsg.Remove(0, 1)
            End If
            lblError.Text = FinalMsg + retMsgSal
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID,EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub txtEMPNAME_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_EMPID.ValueChanged
        FillEmpNames(h_EMPID.Value)
        txtEMPNAME.Text = ""
    End Sub
    Protected Sub txtCatName_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_CATID.ValueChanged
        FillEmpNames(h_CATID.Value)
        txtCatName.Text = ""
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        bindRejoinlist()
    End Sub

    Sub bindRejoinlist()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString

            Dim strABC As String = String.Empty
            If chkEMPABC_A.Checked Then
                strABC = "A"
            End If
            If chkEMPABC_B.Checked Then
                strABC += "||B"
            End If
            If chkEMPABC_C.Checked Then
                strABC += "||C"
            End If
            '----Commenting str_sql for V1.1-----------

            'Dim str_Sql As String = "SELECT     ELV.ELV_ID, ELV.ELV_BSU_ID, ELV.ELV_EMP_ID, ELV.ELV_ELT_ID, ELV.ELV_ELA_ID, " _
            '& " ELV.ELV_DTFROM ,(ELV.ELV_DTTO+1) AS REJOINDT, ELV.ELV_DTTO, ELV.ELV_REMARKS, ELV.ELV_SALARY, ELV.ELV_LVSALARY, " _
            '& " ELV.ELV_AIRTICKET, CONVERT(decimal, ELV.ELV_ACTUALDAYS) AS ELV_ACTUALDAYS, ELV.ELV_LOPDAYS, " _
            '& " EM.EMPNO, ISNULL(EM.EMP_FNAME, '') + ' ' + ISNULL(EM.EMP_MNAME, '') + ' ' + " _
            '& " ISNULL(EM.EMP_LNAME, '') AS EMP_NAME, EM.EMP_DES_ID, " _
            '& " EM.EMP_ECT_ID, EM.EMP_ABC, ELV.ELV_REJOINDT,  ECT.ECT_DESCR FROM EMPLEAVE_D AS ELV INNER JOIN" _
            '& " EMPLOYEE_M AS EM ON ELV.ELV_EMP_ID = EM.EMP_ID" _
            '& " INNER JOIN  EMPCATEGORY_M AS ECT ON EM.EMP_ECT_ID = ECT.ECT_ID" _
            '& " WHERE (ELV.ELV_BSU_ID = '" & Session("sbsuid") & "') AND (ELV.ELV_bOpening = 0) AND (EM.EMP_BSU_ID = '" & Session("sbsuid") & "')" _
            '& " and ELV.ELV_REJOINDT is null " _
            Dim str_Sql As String = "SELECT ELA_ID, ELA_BSU_ID, ELA_EMP_ID, ELA_ELT_ID," _
            & "ELA_DTFROM ,IsNull(ELA_REJOINDT,ELA_DTTO +1) AS REJOINDT,ELA_DTTO, ELA_REMARKS," _
            & " EM.EMPNO, ISNULL(EM.EMP_FNAME, '') + ' ' + ISNULL(EM.EMP_MNAME, '') + ' ' + " _
            & " ISNULL(EM.EMP_LNAME, '') AS EMP_NAME, EM.EMP_DES_ID, " _
            & " EM.EMP_ECT_ID, EM.EMP_ABC, ELA_REJOINDT,  ECT.ECT_DESCR FROM EMPLEAVEAPP INNER JOIN" _
            & " EMPLOYEE_M AS EM ON EMPLEAVEAPP.ELA_EMP_ID = EM.EMP_ID" _
            & " INNER JOIN  EMPCATEGORY_M AS ECT ON EM.EMP_ECT_ID = ECT.ECT_ID" _
            & " INNER JOIN EMPLEAVETYPE_M ON ELT_ID=ELA_ELT_ID " _
            & " WHERE (ELA_BSU_ID = '" & Session("sbsuid") & "')and IsNUll(ELA_REJOINDT,'01/01/1900')= '01/01/1900' and ELA_APPRSTATUS = 'A' and isnull(emp_bactive,0)=1 and ELT_BReqRejoin=1 " _
            & GetFilter("EMP_ECT_ID", h_CATID.Value, True) & _
            GetFilter("EMP_ID", h_EMPID.Value, True) & _
            GetFilter("EMP_ABC", strABC, True) '& _
            ' " AND EMD_EXPDT between '" & docDate & "' AND '" & docDate.AddDays(3) & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & " order by EMPNO ")
            If ds.Tables(0).Rows.Count > 0 Then
                gvEmpDetails.DataSource = ds.Tables(0)
                gvEmpDetails.DataBind()
            Else
                'lblError.Text = "No data to display"
                gvEmpDetails.DataSource = ""
                gvEmpDetails.DataBind()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub txtDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        'txtAmount.Attributes.Add("onBlur", "find_Ototal();")
        txtAmount.Attributes.Add("onFocus", "this.select();")
    End Sub

    Protected Sub gvEmpDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvEmpDetails.PageIndex = e.NewPageIndex
        bindRejoinlist()
    End Sub
End Class
