Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Payroll_empFreezeHR
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property FreezeDetail() As DataRow
        Get
            If Not ViewState("FreezeDetail") Is Nothing And ViewState("FreezeDetail").rows.count > 0 Then
                Return ViewState("FreezeDetail").rows(0)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataRow)
            ViewState("FreezeDetail") = value.Table
        End Set
    End Property
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                HttpContext.Current.Session("divDetail") = divDetail

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P153064") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                h_BSU_ID.Value = Session("sBsuid")
                txtBSUName.Text = Mainclass.GetBSUName(h_BSU_ID.Value)
                setModifyHeader(Session("sBsuid"))
                SetDataMode(h_IsDisabled.Value)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
   
    Private Sub setModifyHeader(ByVal BSU_ID As String) 'setting header data on view/edit
        Try
            FreezeDetail = GetBSUHRFreezeData(BSU_ID)
            If FreezeDetail Is Nothing Then Exit Sub
            txtBSUName.Text = FreezeDetail("BSU_NAME").ToString
            h_BSU_ID.Value = FreezeDetail("HFZ_BSU_ID").ToString
            h_IsDisabled.Value = FreezeDetail("bLockHR")
            txtCurrStatus.Text = IIf(FreezeDetail("bLockHR"), "Freezed", "Unfreezed")
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup(); return false;", pce.BehaviorID)
            btnHistory.Attributes.Add("onClick", OnMouseOverScript)
            pce.Enabled = True
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As Boolean)
        btnEnable.Enabled = mode
        btnDisable.Enabled = Not mode
        txtRemarks.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDisable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisable.Click
        EnableDisableHRData(True)
    End Sub
    Private Sub EnableDisableHRData(ByVal Disable As Boolean)
        Try
            If FreezeDetail Is Nothing Then Exit Sub
            FreezeDetail("HFZ_BSU_ID") = h_BSU_ID.Value
            FreezeDetail("HFZ_LOGUSER") = Session("sUsr_name")
            FreezeDetail("HFZ_REMARKS") = txtRemarks.Text
            FreezeDetail("HFZ_IsDisabled") = Disable
            Dim retval As String
            Dim HFZ_ID As String = ""
            retval = FreezeBSUHRData(FreezeDetail)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, h_HFZ_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Saved Successfully !!!"
                setModifyHeader(h_BSU_ID.Value)
                SetDataMode(h_IsDisabled.Value)
            Else
                lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnEnable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnable.Click
        EnableDisableHRData(False)
    End Sub
    Public Shared Function GetBSUHRFreezeData(ByVal BSU_ID As String) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            mSet = Mainclass.getDataSet("GetBSUHRFreezeData", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function FreezeBSUHRData(ByVal FreezeDetail As DataRow) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(5) As SqlParameter
            sqlParam(1) = Mainclass.CreateSqlParameter("@HFZ_BSU_ID", FreezeDetail("HFZ_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@HFZ_IsDisabled", FreezeDetail("HFZ_IsDisabled"), SqlDbType.Bit)
            sqlParam(3) = Mainclass.CreateSqlParameter("@HFZ_LOGUSER", FreezeDetail("HFZ_LOGUSER"), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@HFZ_REMARKS", FreezeDetail("HFZ_REMARKS"), SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FreezeBSUHRData", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(5).Value = "" Then
                FreezeBSUHRData = ""
            Else
                FreezeBSUHRData = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(5).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
Public Shared Function GetDynamicContent() As String
        Try
            Dim b As New StringBuilder()
            Dim itemDiv As HtmlControls.HtmlGenericControl
            itemDiv = HttpContext.Current.Session("divDetail")
            Dim sw As New StringWriter()
            Dim pnl As Panel
            pnl = itemDiv.FindControl("pnlDetail")
            BindFreezeHistory(pnl, HttpContext.Current.Session("sBsuid"))
            Dim w As New HtmlTextWriter(sw)
            itemDiv.RenderControl(w)
            Dim s As String = sw.GetStringBuilder().ToString()
            b.Append("<table style='background-color:#f3f3f3; ")
            b.Append("width:500px; ' >")
            b.Append("<tr><td >")
            b.Append(s)
            b.Append("</td></tr>")
            b.Append("</table>")
            Return b.ToString
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Private Shared Sub BindFreezeHistory(ByVal pnl As Panel, ByVal BSU_ID As String)
        Try
            Dim lnkClosePnl As HtmlButton
            lnkClosePnl = pnl.FindControl("lnkClose")
            If Not lnkClosePnl Is Nothing Then
                Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();return false;", "pce_0")
                lnkClosePnl.Attributes.Add("onClick", OnMouseOutScript)
            End If
            Dim rptFreezeHistory As Repeater
            rptFreezeHistory = pnl.FindControl("rptFreezeHistory")
            If Not rptFreezeHistory Is Nothing Then
                Dim mtable As New DataTable
                mtable = GetFreezeHistory(BSU_ID)
                rptFreezeHistory.DataSource = mtable
                rptFreezeHistory.DataBind()
            End If
            Dim btn As HtmlButton
            btn = pnl.FindControl("lnkClose")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Function GetFreezeHistory(ByVal BSU_ID As String) As DataTable
        Try
            Dim mtable As New DataTable
            Dim sql As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            sql = "SELECT CASE WHEN ISNULL(HFZ_IsDisabled,0)=1 THEN 'Freezed' ELSE 'Unfreezed' END [Action],REPLACE(CONVERT(VARCHAR(50), HFZ_LOGDT, 13), ' ', ' ') LOGDT,HFZ_LOGUSER,HFZ_REMARKS  "
            sql &= "     FROM dbo.HR_FREEZE_TRN "
            sql &= "     WHERE HFZ_BSU_ID = '" & BSU_ID & "'"
            sql &= "     ORDER BY HFZ_LOGDT DESC"
            mtable = Mainclass.getDataTable(sql, str_conn)
            GetFreezeHistory = mtable
        Catch ex As Exception
            Errorlog(ex.Message)
            GetFreezeHistory = Nothing
            Throw ex
        End Try
    End Function
   
End Class
