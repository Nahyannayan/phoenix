﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empVisa_LC_Exit.aspx.vb" Inherits="Payroll_empVisa_LC_Exit" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">

    function GetEMPName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        <%--result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=VISA_CAN&EMPNO=1", "", sFeatures)
        if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                return true;
            }
            return false;
        }--%>
        var url = "../Accounts/accShowEmpDetail.aspx?id=VISA_CAN&EMPNO=1";
            var oWnd = radopen(url, "pop_empname");

            
        } pop_employee

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();

            if (arg) {                
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
        }

        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
</script>
    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       <Windows>
            <telerik:RadWindow ID="pop_empname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblTitle" runat="server" EnableViewState="False" CssClass="BlueTable"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
 <table width="100%" align="center" >
    <tr>
    <td  colspan="3" align="left">
        <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
    </td>    
    </tr>
 </table>
 <table width="100%" align="center">            
            
             <tr>
                <td align="left" width="20%">
                            <span class="field-label">Select Employee</span></td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;" />
                    </td>
                 <td width="20%">
                     <span class="field-label">Employee Name</span></td>
                 <td width="30%">
                     <asp:Label ID="lbl_Empname" runat="server" CssClass="field-value" ></asp:Label>
                    <asp:linkButton ID="lnkLog" runat="server" style="display:none;"  >Transaction Log</asp:linkButton>
                    <ajaxToolkit:PopupControlExtender
                                        ID="PopupControlExtender1" runat="server" 
                                        DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                        Position="Bottom" TargetControlID="lnkLog">
                                    </ajaxToolkit:PopupControlExtender></td>
            </tr>
             
             <tr>
                <td align="left">
                    <span class="field-label">Designation</span></td>
                <td align="left">
                <asp:Label ID="lbldpt" runat="server" CssClass="field-value"></asp:Label></td>
                <td align="left">
                    <span class="field-label">Department</span></td>
                <td align="left">
                <asp:Label ID="lbldesg" runat="server" CssClass="field-value"></asp:Label></td>
            </tr>
             
             <tr>
                <td align="left">
                    <span class="field-label">Visa Business Unit</span></td>
                <td align="left">
                <asp:Label ID="lblVisaBSU" runat="server" CssClass="field-value"></asp:Label></td>
                <td align="left">
                    <span class="field-label">Working Business Unit</span></td>
                <td align="left">
                <asp:Label ID="lblWorkingBSU" runat="server" CssClass="field-value"></asp:Label></td>
            </tr>
            
             <tr>
                <td align="left">
                <span class="field-label">Date Of Joining the Unit</span></td>
                <td align="left">
                <asp:Label ID="lblDOJBSU" runat="server" CssClass="field-value"></asp:Label></td>
                <td align="left">
                <span class="field-label">Date Of Joining the Group</span></td>
                <td align="left">
                <asp:Label ID="lblDOJGroup" runat="server" CssClass="field-value"></asp:Label></td>
            </tr>
            
             <tr>
                <td align="left">
                    <span class="field-label">Resignation Type</span></td>
                <td align="left">
             <asp:DropDownList ID="DropDownListResignTerm" runat="server" Enabled="False">
                 <asp:ListItem Value="4">Resignation</asp:ListItem>
                 <asp:ListItem Value="5">Termination</asp:ListItem>
             </asp:DropDownList><br />Last Working Date &nbsp;<asp:Label ID="lblLWD" runat="server"></asp:Label>
                 </td>
                 <td></td>
                 <td></td>
            </tr>
             
        </table>
    
    <table id="TBLVISACANCEL" visible="false"  runat="server" width="100%" align="center">
       <tr>
         <td align="left" width="20%">
                    <span class="field-label">Visa Cancelled ?</span></td>
         <td align="left" width="30%">
                         <asp:RadioButton ID="rbVisaYes" runat="server" AutoPostBack="True" CssClass="radiobutton"
                    GroupName="VisaPOST" Text="Yes" Checked="True" 
                         />
                <asp:RadioButton ID="rbVisaNo" runat="server" AutoPostBack="True" CssClass="radiobutton"
                    GroupName="VisaPOST" Text="No" />
                    </td>
           <td width="20%"></td>
           <td width="30%"></td>
     </tr>
       <tr>
         <td align="left">
                    <span class="field-label">Visa Cancellation Date</span></td>
         <td align="left">
             <asp:TextBox ID="txtVisaCancelDt" runat="server" ></asp:TextBox>
             
             <asp:ImageButton ID="imgVisaCancelDt" runat="server" ImageUrl="~/Images/calendar.gif" 
                        TabIndex="4" />
             
             </td>
           <td></td>
           <td></td>
     </tr>
    </table>
        <table id="TBLLCCANCEL" visible="false"  runat="server" width="100%">
       <tr>
         <td align="left" width="20%">
                    <span class="field-label">Labour Card Cancelled ?</span></td>
         <td align="left" width="30%">
                         <asp:RadioButton ID="rbLCYes" runat="server" AutoPostBack="True" CssClass="radiobutton"
                    GroupName="LCPOST" Text="Yes" Checked="True" 
                       />
                <asp:RadioButton ID="rbLCNo" runat="server" AutoPostBack="True" CssClass="radiobutton"
                    GroupName="LCPOST" Text="No"  />
                    </td>
           <td width="20%"></td>
           <td width="30%"></td>
     </tr>
       <tr>
         <td align="left">
                    <span class="field-label">Labour Card Cancellation Date</span></td>
         <td align="left">
             <asp:TextBox ID="txtLCCancelDt" runat="server"></asp:TextBox>
 
             <asp:ImageButton ID="imgLCCancelDt" runat="server" ImageUrl="~/Images/calendar.gif" 
                        TabIndex="4" />
             </td>
           <td></td>
           <td></td>
     </tr>
    </table>
    <table id="TBLEMPEXIT" visible="false"  runat="server" width="100%">
       <tr>
         <td align="left" width="20%">
                    <span class="field-label">Employee Exited from UAE?</span></td>
         <td align="left" width="30%">
                         <asp:RadioButton ID="rbExitYes" runat="server" AutoPostBack="True" CssClass="radiobutton"
                    GroupName="EXTPOST" Text="Yes" Checked="True" 
                         />
                <asp:RadioButton ID="rbExitNo" runat="server" AutoPostBack="True" CssClass="radiobutton"
                    GroupName="EXTPOST" Text="No"  />
                    </td>
           <td width="20%"></td>
           <td width="30%"></td>
     </tr>
       <tr>
         <td align="left" >
                    <span class="field-label">Employee Exit Date</span></td>
         <td align="left">
             <asp:TextBox ID="txtExitDt" runat="server"></asp:TextBox>
 
             <asp:ImageButton ID="imgExitDt" runat="server" ImageUrl="~/Images/calendar.gif" 
                        TabIndex="4" />
             </td>
           <td></td>
           <td></td>
     </tr>
    </table>
    <table  width="100%" align="center">
       <tr>
         <td colspan="4" align="center">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" Visible="False" />
                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" 
                        CausesValidation="False" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Process" />
                    <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" 
                        EnableViewState="False" Visible="False" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
    </table>
    
        <ajaxToolkit:CalendarExtender ID="txtVisaCancelDt_CalendarExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgVisaCancelDt" TargetControlID="txtVisaCancelDt">
    </ajaxToolkit:CalendarExtender>   
            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgLCCancelDt" TargetControlID="txtLCCancelDt">
    </ajaxToolkit:CalendarExtender>    
            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgExitDt" TargetControlID="txtExitDt">
    </ajaxToolkit:CalendarExtender>  
    <asp:HiddenField ID="h_Emp_No" runat="server" />
    <asp:Panel ID="Panel1" CssClass="panel-cover" runat="server"  Width="550px">
    </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>

