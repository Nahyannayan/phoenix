<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empEmp_com.aspx.vb" Inherits="Category_Emp" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
              
                            <table align="center"  width="100%">
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                                </tr>
                                <!-- -->
                                <tr>
                                    <td>
                                        <table align="center" width="100%">
                                            
                                            <tr>
                                                <td align="center" valign="top">
                                                    <asp:GridView ID="gvCommonEmp" runat="server" AutoGenerateColumns="False" HeaderStyle-Height="30" Width="100%" DataKeyNames="ID" AllowPaging="True" CssClass="table table-bordered table-row" PageSize="20" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords.">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" SortExpression="GRP_ID">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblHCode" runat="server">ID</asp:Label><br />
                                                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcode" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Description" SortExpression="DESCR">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtGroupname" runat="server" CssClass="inputbox" MaxLength="100"
                                                                        Text='<%# Bind("DESCR") %>' Width="100%"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblName" runat="server">Description</asp:Label><br />
                                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchpar_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Paid">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("Paid") %>' />
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("Paid") %>' Enabled="false" />
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblPaidH" runat="server" Text="Paid"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="End of service benefit">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Eob") %>' />
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblEof" runat="server">End of Service Benefit</asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Eob") %>' Enabled="false" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                                </tr>

                                <!-- -->
                            </table>
                       
            </div>
        </div>
    </div>
</asp:Content>

