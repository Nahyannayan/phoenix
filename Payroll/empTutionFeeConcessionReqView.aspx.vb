﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empTutionFeeConcessionReqView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim UpdateData As Boolean = True


    Public Property bTRANSPORT() As Boolean
        Get
            Return ViewState("bTRANSPORT")
        End Get
        Set(value As Boolean)
            ViewState("bTRANSPORT") = value
        End Set
    End Property
    Public Property bMatchNames() As Boolean
        Get
            Return ViewState("bMatchNames")
        End Get
        Set(value As Boolean)
            ViewState("bMatchNames") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H200020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "H200020"
                            lblHead.Text = "Approve Tuition Fee Concession Request"
                            bTRANSPORT = False
                            rblFilter.Items(0).Text = ""
                            rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")
                        
                    End Select

                    

                    bMatchNames = True
                    bindDropdown()
                    gridbind()

                    If rblFilter.SelectedValue = "PND" Then
                        Me.btnApprove.Visible = True
                        Me.btnReject.Visible = True
                    Else
                        Me.btnApprove.Visible = False
                        Me.btnReject.Visible = False
                    End If
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Else
            If rblFilter.SelectedValue = "PND" Then
                Me.btnApprove.Visible = True
                Me.btnReject.Visible = True
            Else
                Me.btnApprove.Visible = False
                Me.btnReject.Visible = False
            End If

        End If
    End Sub
    Private Sub SetNameMismatchAlert()
        'If gvJournal.Rows.Count <= 0 Then
        '    Me.lblAlert.Text = ""
        'ElseIf bMatchNames = False Then
        '    Me.lblAlert.Text = "Highlighted are Concession(s) having mismatch in Employee name and Parent name of Child, Please verify before approval"
        '    Me.lblAlert.Visible = False
        'Else
        '    Me.lblAlert.Text = ""
        'End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub rblFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        gridbind()
    End Sub

    Sub bindDropdown()

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_str As String = "select ID,DESCR from OASIS_FEES..VW_TUITIONFEECONCESSION_ACADEMICYEARS order by ID DESC"
        ddlAcaYear.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
        ddlAcaYear.DataTextField = "DESCR"
        ddlAcaYear.DataValueField = "ID"
        ddlAcaYear.DataBind()
        ddlAcaYear.SelectedValue = 30

    End Sub
    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            lstrCondn8 = ""
            str_Filter = ""
            Select Case ViewState("MainMnu_code").ToString
                Case "H200020"
                    rblFilter.Items(0).Text = ""
                    rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")
            End Select

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn1)

                '   -- 2  txtEmpName
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpName")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNAME", lstrCondn2)

                '   -- 3  txtDept
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDept")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DPT_DESCR", lstrCondn3)

                '   -- 4   txtEmpDesign
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpDesign")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DES_DESCR", lstrCondn4)

                '   -- 5  txtStudName
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STUNAME", lstrCondn5)

                '   -- 6  txtGrade
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn6)

                '   -- 7  txtStuno
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn7)
                '   -- 8  txtEmpBsu
                larrSearchOpr = h_selected_menu_8.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtEmpBsu")
                lstrCondn8 = txtSearch.Text
                If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_BSU_SHORTNAME", lstrCondn8)
            End If




            Me.gvJournal.Columns(0).Visible = False
            'Me.gvJournal.Columns(15).Visible = False
            
            Dim STR_RECEIPT As String = "SP_GET_TUITION_FEE_CONCESSION_LIST_FOR_HR_APPROVAL"
            Dim SPParam(4) As SqlParameter

            SPParam(0) = Mainclass.CreateSqlParameter("@EMP_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            SPParam(1) = Mainclass.CreateSqlParameter("@NEW_ACY_iD", ddlAcaYear.SelectedValue, SqlDbType.Int)

            If rblFilter.SelectedValue = "PND" Then
                SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", "P", SqlDbType.VarChar)
                Me.gvJournal.Columns(0).Visible = True
                Me.gvJournal.Columns(15).Visible = True
                CommentsSection.Visible = False


            ElseIf rblFilter.SelectedValue = "APD" Then
                SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", "A", SqlDbType.VarChar)
                CommentsSection.Visible = False
                Me.gvJournal.Columns(15).Visible = True
            ElseIf rblFilter.SelectedValue = "RJD" Then
                SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", "R", SqlDbType.VarChar)
                CommentsSection.Visible = False
                Me.gvJournal.Columns(15).Visible = True
            
            End If
            SPParam(3) = Mainclass.CreateSqlParameter("@DISPLAYRECORD", ddlTopFilter.SelectedItem.Value, SqlDbType.VarChar)
            SPParam(4) = Mainclass.CreateSqlParameter("@str_Filter", str_Filter, SqlDbType.VarChar)


            
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, STR_RECEIPT, SPParam)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case "H200020"
                    'gvJournal.Columns(15).Visible = False
                    'gvJournal.Columns(16).Visible = False
                
            End Select
            bMatchNames = True

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtDept")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpDesign")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn7

            txtSearch = gvJournal.HeaderRow.FindControl("txtEmpBsu")
            txtSearch.Text = lstrCondn8

            If rblFilter.SelectedValue = "PND" Then
                Me.btnApprove.Visible = True
                Me.btnReject.Visible = True
                Me.btnApprove.Enabled = True
                Me.btnReject.Enabled = True
            Else
                Me.btnApprove.Visible = False
                Me.btnReject.Visible = False
                Me.btnApprove.Enabled = False
                Me.btnReject.Enabled = False
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopFilter.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcaYear.SelectedIndexChanged
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbDetail As LinkButton = DirectCast(e.Row.FindControl("lbDetail"), LinkButton)
            Dim lblSCR_DATE As Label = DirectCast(e.Row.FindControl("lblSCR_DATE"), Label)
            Dim hf_EMPID As HiddenField = DirectCast(e.Row.FindControl("hf_EMPID"), HiddenField)
            Dim hf_STUID As HiddenField = DirectCast(e.Row.FindControl("hf_STUID"), HiddenField)
            Dim hf_SCRID As HiddenField = DirectCast(e.Row.FindControl("hf_NEW_ACD_ID"), HiddenField)
            Dim hf_SCDID As HiddenField = DirectCast(e.Row.FindControl("hf_CACD_ID"), HiddenField)
            Dim hf_STATUS As HiddenField = DirectCast(e.Row.FindControl("hf_STATUS"), HiddenField)
            Dim hf_SSVID As HiddenField = DirectCast(e.Row.FindControl("hf_SSVID"), HiddenField)
            Dim txtCnFrom As TextBox = DirectCast(e.Row.FindControl("txtCnFrom"), TextBox)
            Dim txtCnTo As TextBox = DirectCast(e.Row.FindControl("txtCnTo"), TextBox)
            Dim lblErrorMessage As Label = DirectCast(e.Row.FindControl("lblErrorMessage"), Label)
            If Not lblErrorMessage Is Nothing AndAlso lblErrorMessage.Text <> "" Then
            End If
            lbDetail.Attributes.Clear()
            If Not lbDetail Is Nothing AndAlso hf_EMPID.Value <> "" AndAlso hf_STUID.Value <> "" AndAlso hf_SCDID.Value <> "" AndAlso hf_SCRID.Value <> "" Then



                lbDetail.Attributes.Add("onClick", "ViewDetail('empTutionFeeConcessionReqDetail.aspx?EMP_ID=" & Encr_decrData.Encrypt(hf_EMPID.Value) & "&STU_ID=" & Encr_decrData.Encrypt(hf_STUID.Value) & "&SCR_ID=" & Encr_decrData.Encrypt(hf_SCRID.Value) & "&SCD_ID=" & Encr_decrData.Encrypt(hf_SCDID.Value) & "&STATUS=" & Encr_decrData.Encrypt(hf_SCDID.Value) & "&bTRANSPORT=" & Encr_decrData.Encrypt(bTRANSPORT) & "', '', '60%', '80%'); return false;")
                If Not CompareEmpParent(hf_EMPID.Value, hf_STUID.Value) And (Me.rblFilter.SelectedValue = "ALL" Or Me.rblFilter.SelectedValue = "PND") Then 'Returns False if Mismatch and shows message only if pending or All

                    bMatchNames = False
                End If

            Else
                lbDetail.Attributes.Add("onClick", "return false;")
            End If
        End If
    End Sub

    Private Function CompareEmpParent(ByVal EMPID As Int64, ByVal STUID As Int64) As Boolean
        CompareEmpParent = True

        Try
            Dim bMatch As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT dbo.fnCompEmployeeAndParent(" & EMPID & "," & STUID & ")")
            CompareEmpParent = bMatch
        Catch ex As Exception
            CompareEmpParent = False
        End Try


    End Function

    Private Function GetSelectedRows() As String
        GetSelectedRows = ""
        For Each gvr As GridViewRow In gvJournal.Rows
            Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
            Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
            If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                GetSelectedRows &= "|" & hf_SCDID.Value
            End If
        Next
    End Function
    Sub Approve_Reject_Tuition_Fee_request(ByVal action As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim bRolledBack As Boolean = False
        Dim bCommitted As Boolean = False
        Dim Trans As SqlTransaction
        Try
            Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION

            Trans = objConn.BeginTransaction("APVE")
            For Each gvr As GridViewRow In gvJournal.Rows

                Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)


                Dim hf_EMP_BSU_ID As HiddenField = DirectCast(gvr.FindControl("hf_EMP_BSU_ID"), HiddenField)
                Dim hf_EMPID As HiddenField = DirectCast(gvr.FindControl("hf_EMPID"), HiddenField)
                Dim hf_STUID As HiddenField = DirectCast(gvr.FindControl("hf_STUID"), HiddenField)
                Dim hf_FCH_BSU_ID As HiddenField = DirectCast(gvr.FindControl("hf_FCH_BSU_ID"), HiddenField)
                Dim lblgross As Label = DirectCast(gvr.FindControl("lblgross"), Label)
                Dim lblconcession As Label = DirectCast(gvr.FindControl("lblconcession"), Label)
                Dim txtRemarks As TextBox = DirectCast(gvr.FindControl("txtRemarks"), TextBox)
                Dim hf_CACD_ID As HiddenField = DirectCast(gvr.FindControl("hf_CACD_ID"), HiddenField)
                Dim hf_NEW_ACD_ID As HiddenField = DirectCast(gvr.FindControl("hf_NEW_ACD_ID"), HiddenField)
                Dim lblErrorMessage As Label = DirectCast(gvr.FindControl("lblErrorMessage"), Label)
                Dim hf_EMPNAME As HiddenField = DirectCast(gvr.FindControl("hf_EMPNAME"), HiddenField)


                If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then

                    If txtRemarks.Text.Trim <> "" Then


                        Dim retval As String = "1"
                        If 1 = 1 Then

                            If 1 = 0 Then
                                retval = "1"
                                UpdateData = False
                            Else

                                retval = Approve_Employee_Child_TutionFee_ConcessionRequest(objConn, Trans, Session("sBsuid"), hf_EMPID.Value, hf_EMP_BSU_ID.Value, hf_STUID.Value, hf_FCH_BSU_ID.Value, ddlAcaYear.SelectedValue,
                                lblgross.Text, lblconcession.Text, IIf(action = "A", "A", "R"), Session("sUsr_name"), txtRemarks.Text, txtComments.Text, hf_NEW_ACD_ID.Value, hf_CACD_ID.Value)
                                'UpdateData = True
                                If retval <> 0 Then
                                    UpdateData = False
                                    usrMessageBar.ShowNotification("Issue with  Employee Name : " & hf_EMPNAME.Value & " ", UserControls_usrMessageBar.WarningType.Danger)
                                    Exit For
                                End If

                            End If

                        Else
                            bRolledBack = True
                            UpdateData = False
                            gvJournal.Columns(20).Visible = True
                        End If
                    Else
                        usrMessageBar.ShowNotification("Employee : " & hf_EMPNAME.Value & " Elgibility as per policy text is empty,after updating do Approve or Reject", UserControls_usrMessageBar.WarningType.Danger)
                        UpdateData = False
                    End If

                End If
            Next
            If UpdateData = False Then
                bRolledBack = True
                Trans.Rollback()
            Else
                Trans.Commit()
                bCommitted = True
            End If
        Catch ex As Exception
            'Me.lblError.Text = "Unable to approve Request(s)"
            Trans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If bRolledBack = True And bCommitted = True Then
                If action = "A" Then
                    usrMessageBar.ShowNotification("Not all request(s) have been approved, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar.ShowNotification("Not all request(s) have been rejected, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                End If

            ElseIf bCommitted Then

                If action = "A" Then
                    usrMessageBar.ShowNotification("Selected Request(s) have been approved", UserControls_usrMessageBar.WarningType.Success)
                Else
                    usrMessageBar.ShowNotification("Selected Request(s) have been rejected", UserControls_usrMessageBar.WarningType.Success)
                End If

            ElseIf bRolledBack Then
                If action = "A" Then
                    usrMessageBar.ShowNotification("Unable to approve Request(s)", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar.ShowNotification("Unable to reject Request(s)", UserControls_usrMessageBar.WarningType.Danger)

                End If

            End If
            gridbind()
        End Try
        'Else
        'Me.lblError.Text = "Please select one or more concession(s) to be approved"
        'End If

    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click


        Approve_Reject_Tuition_Fee_request("A")
        txtComments.Text = ""


    End Sub

    Public Function Approve_Employee_Child_TutionFee_ConcessionRequest(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction,
ByVal STF_BSU_ID As String, ByVal STF_EMP_ID As Double, ByVal STF_EMP_BSU_ID As String, ByVal STF_STU_ID As Double, ByVal STF_STU_BSU As String, ByVal STF_ACD_ID As Integer, ByVal STF_GROSS_AMT As Double,
ByVal STF_CONSN_AMT As Double, ByVal STF_APPR_STATUS As String, ByVal STF_PROCESS_USER As String, ByVal STF_REMARKS As String, ByVal STF_GENERIC_REMARKS As String, ByVal STF_NEW_ACD_ID As Integer, ByVal STF_CURR_ACD_ID As Integer) As String
        Approve_Employee_Child_TutionFee_ConcessionRequest = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SAVE_STUDENT_TUITION_FEE_CONCESSION_REQUEST", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(14) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@STF_BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = STF_BSU_ID
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@STF_EMP_ID", SqlDbType.VarChar)
            pParms(1).Value = STF_EMP_ID
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@STF_EMP_BSU_ID", SqlDbType.VarChar)
            pParms(2).Value = STF_EMP_BSU_ID
            cmd.Parameters.Add(pParms(2))

            pParms(3) = New SqlClient.SqlParameter("@STF_STU_ID", SqlDbType.VarChar)
            pParms(3).Value = STF_STU_ID
            cmd.Parameters.Add(pParms(3))

            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(4))

            pParms(5) = New SqlClient.SqlParameter("@STF_STU_BSU", SqlDbType.Int)
            pParms(5).Value = STF_STU_BSU
            cmd.Parameters.Add(pParms(5))


            pParms(6) = New SqlClient.SqlParameter("@STF_ACD_ID", SqlDbType.Int)
            pParms(6).Value = STF_ACD_ID
            cmd.Parameters.Add(pParms(6))

            pParms(7) = New SqlClient.SqlParameter("@STF_GROSS_AMT", SqlDbType.Decimal)
            pParms(7).Value = STF_GROSS_AMT
            cmd.Parameters.Add(pParms(7))

            pParms(8) = New SqlClient.SqlParameter("@STF_CONSN_AMT", SqlDbType.Decimal)
            pParms(8).Value = STF_CONSN_AMT
            cmd.Parameters.Add(pParms(8))

            pParms(9) = New SqlClient.SqlParameter("@STF_APPR_STATUS", SqlDbType.VarChar)
            pParms(9).Value = STF_APPR_STATUS
            cmd.Parameters.Add(pParms(9))


            pParms(10) = New SqlClient.SqlParameter("@STF_PROCESS_USER", SqlDbType.VarChar)
            pParms(10).Value = STF_PROCESS_USER
            cmd.Parameters.Add(pParms(10))

            pParms(11) = New SqlClient.SqlParameter("@STF_REMARKS", SqlDbType.VarChar)
            pParms(11).Value = STF_REMARKS
            cmd.Parameters.Add(pParms(11))


            pParms(12) = New SqlClient.SqlParameter("@STF_GENERIC_REMARKS", SqlDbType.VarChar)
            pParms(12).Value = STF_GENERIC_REMARKS
            cmd.Parameters.Add(pParms(12))

            pParms(13) = New SqlClient.SqlParameter("@STF_NEW_ACD_ID", SqlDbType.Int)
            pParms(13).Value = STF_NEW_ACD_ID
            cmd.Parameters.Add(pParms(13))

            pParms(14) = New SqlClient.SqlParameter("@STF_CURR_ACD_ID", SqlDbType.Int)
            pParms(14).Value = STF_CURR_ACD_ID
            cmd.Parameters.Add(pParms(14))


            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            Approve_Employee_Child_TutionFee_ConcessionRequest = pParms(4).Value

        Catch ex As Exception
            Approve_Employee_Child_TutionFee_ConcessionRequest = "1"
            'TRANS.Rollback()
        End Try
    End Function

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Approve_Reject_Tuition_Fee_request("R")
        txtComments.Text = ""
    End Sub

    Protected Sub gvJournal_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvJournal.DataBound
        SetNameMismatchAlert()
    End Sub


End Class
