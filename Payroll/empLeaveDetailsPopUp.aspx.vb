﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports InfosoftGlobal
Imports Telerik.Web.UI
Partial Class Payroll_empLeaveDetailsPopUp
    Inherits System.Web.UI.Page
    Public arr_FCColors(20) As String
    Public FC_ColorCounter As Integer
    Private Sub InitaiteColorArray()
        FC_ColorCounter = 0
        arr_FCColors(0) = "1941A5" 'Dark Blue
        arr_FCColors(1) = "AFD8F8"
        arr_FCColors(2) = "F6BD0F"
        arr_FCColors(3) = "8BBA00"
        arr_FCColors(4) = "A66EDD"
        arr_FCColors(5) = "F984A1"
        arr_FCColors(6) = "CCCC00" 'Chrome Yellow+Green
        arr_FCColors(7) = "999999" 'Grey
        arr_FCColors(8) = "0099CC" 'Blue Shade
        arr_FCColors(9) = "FF0000" 'Bright Red 
        arr_FCColors(10) = "006F00" 'Dark Green
        arr_FCColors(11) = "0099FF" 'Blue (Light)
        arr_FCColors(12) = "FF66CC" 'Dark Pink
        arr_FCColors(13) = "669966" 'Dirty green
        arr_FCColors(14) = "7C7CB4" 'Violet shade of blue
        arr_FCColors(15) = "FF9933" 'Orange
        arr_FCColors(16) = "9900FF" 'Violet
        arr_FCColors(17) = "99FFCC" 'Blue+Green Light
        arr_FCColors(18) = "CCCCFF" 'Light violet
        arr_FCColors(19) = "669900" 'Shade of green
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                InitaiteColorArray()
                If Not Request.QueryString("empid") Is Nothing Then
                    h_Emp_ID.Value = Request.QueryString("empid")
                    If Not Request.QueryString("sdate") Is Nothing Then
                        h_SDate.Value = CDate(Request.QueryString("sdate")).ToString("dd/MMM/yyyy")
                        BindLeaveDetails(h_Emp_ID.Value, h_SDate.Value)
                        Dim showhistory As String = ""
                        If Request.QueryString("showhistory") IsNot Nothing Then
                            showhistory = Request.QueryString("showhistory")
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindLeaveDetails(ByVal EMP_ID As Integer, ByVal sDate As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim mtable As New DataTable
            Dim Param(2) As SqlClient.SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@EMP_ID", EMP_ID, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@BSU_ID", HttpContext.Current.Session("sBsuid"), SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@DTTO", Format(CDate(sDate), "dd/MMM/yyyy"), SqlDbType.DateTime)
            mtable = Mainclass.getDataTable("[dbo].[GetEmployeeBalanceLeaves]", Param, str_conn)
            RadLeaveSummary.DataSource = mtable
            RadLeaveSummary.DataBind()
            RadLeaveSummary.Visible = True
            If mtable.Rows.Count > 0 Then
                '  RadLeaveSummary.Items(0).Selected = True
            End If
            DisplayCharts(mtable)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Function GetLeaveBreakupDetail(ByVal ELT_ID As String) As DataTable
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim mtable As New DataTable
            Dim Param(3) As SqlClient.SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@EMP_ID", h_Emp_ID.Value, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@BSU_ID", HttpContext.Current.Session("sBsuid"), SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@ELT_ID", ELT_ID, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@DTTO", Format(CDate(h_SDate.Value), "dd/MMM/yyyy"), SqlDbType.DateTime)
            mtable = Mainclass.getDataTable("[dbo].[GetEmployeeLeaveDetail]", Param, str_conn)
            GetLeaveBreakupDetail = mtable
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Protected Sub RadLeaveSummary_DetailTableDataBind(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles RadLeaveSummary.DetailTableDataBind
        Dim dataItem As GridDataItem = DirectCast(e.DetailTableView.ParentItem, GridDataItem)
        If e.DetailTableView.Name = "VW_LEAVE_DETAIL" Then
            Dim hdnELTID As HiddenField
            hdnELTID = dataItem.FindControl("hdnELTID")
            e.DetailTableView.DataSource = GetLeaveBreakupDetail(hdnELTID.Value)
        End If
    End Sub

    Protected Sub RadLeaveSummary_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadLeaveSummary.ItemDataBound
        If TypeOf e.Item Is GridCommandItem Then
            Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
            'to hide AddNewRecord button
            cmditm.FindControl("InitInsertButton").Visible = False
            'hide the text
            cmditm.FindControl("AddNewRecordButton").Visible = False
            'hide the image
            'to hide Refresh button
            cmditm.FindControl("RefreshButton").Visible = False
            'hide the text
            'hide the image

            cmditm.FindControl("RebindGridButton").Visible = False
        End If
    End Sub

    Public Sub DisplayCharts(ByVal mtable As DataTable)
        Dim strXML As String
        strXML = ""
        strXML = strXML & "<graph caption='Employee Leave Summary' xAxisName='Leave Type' yAxisName='Leave Taken' decimalPrecision='0' rotateNames='1' formatNumberScale='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC'>"
        strXML = strXML & xml(mtable, "ELT_DESCR", "LEAVE_TAKEN").ToString()
        strXML = strXML & "</graph>"
        FCPieLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Pie3D.swf", "", strXML, "myNext", "300", "200", False)
        FCBarLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "400", "300", False)

        'If mtable.Rows.Count > 0 Then
        '    ColumnChart.Visible = True
        '    ColumnChart.DataSource = mtable
        '    ColumnChart.DataBind()
        'Else
        '    ColumnChart.Visible = False
        'End If
    End Sub
    Public Function xml(ByVal mtable As DataTable, ByVal NameField As String, ByVal ValueField As String) As StringBuilder
        Try
            Dim strXML As New StringBuilder
            Dim mRow As DataRow
            For Each mRow In mtable.Rows
                strXML.Append("<set name='" & mRow(NameField) & "' value='" & mRow(ValueField) & "' color='" & getFCColor() & "' />")
            Next
            Return strXML
        Catch ex As Exception

        End Try
    End Function
    Public Function getFCColor() As String
        'Update index
        FC_ColorCounter = FC_ColorCounter + 1
        'Return color
        Return arr_FCColors(FC_ColorCounter Mod UBound(arr_FCColors))
    End Function

End Class
