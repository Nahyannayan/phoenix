Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Payroll_empBankLoan_View
    Inherits System.Web.UI.Page

    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    'ts
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_2.Value.Split("__")
        ' getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_7.Value.Split("__")
        'getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_9.Value.Split("__")
        getid9(str_Sid_img(2))

    End Sub



    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
     
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid9(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim AdjAddType As String = String.Empty
            If rbActive.Checked = True Then
                AdjAddType = " And a.BActive=1 "
            ElseIf rbInactive.Checked = True Then
                AdjAddType = " And a.BActive=0 "
            End If

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_EmpName As String = String.Empty
            Dim str_filter_LeaveType As String = String.Empty
            Dim str_filter_year As String = String.Empty
            Dim str_filter_Month As String = String.Empty
            Dim str_filter_FromDate As String = String.Empty
            Dim str_filter_ToDate As String = String.Empty
            Dim str_filter_Remark As String = String.Empty

            Dim ds As New DataSet

            str_Sql = "select EBL_ID,BSU_ID,EName,DTFROM,DTTO,BNAME,REMARKS,Branch,Amount,Sal_tran from(SELECT  EMPBANKLOAN_D.EBL_ID as EBL_ID, ISNULL(EMPLOYEE_M.EMP_FNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_LNAME, '') AS EName," & _
            " BANK_M.BNK_DESCRIPTION AS BName, EMPBANKLOAN_D.EBL_REMARKS AS Remarks,EMPBANKLOAN_D.EBL_FROMDT AS DTFrom, EMPBANKLOAN_D.EBL_TODT AS DTTO, EMPBANKLOAN_D.EBL_AMOUNT AS Amount, " & _
" EMPBANKLOAN_D.EBL_bSALARYTRANSFER AS Sal_Tran, EMPBANKLOAN_D.EBL_bActive AS BActive, EMPBANKLOAN_D.EBL_BRANCH AS Branch, " & _
  " BUSINESSUNIT_M.BSU_ID as BSU_ID FROM  EMPLOYEE_M INNER JOIN  EMPBANKLOAN_D ON EMPLOYEE_M.EMP_ID = EMPBANKLOAN_D.EBL_EMP_ID INNER JOIN " & _
  " BANK_M ON EMPBANKLOAN_D.EBL_BNK_ID = BANK_M.BNK_ID INNER JOIN  BUSINESSUNIT_M ON EMPBANKLOAN_D.EBL_BSU_ID = BUSINESSUNIT_M.BSU_ID)a where a.BSU_ID='" & Session("sBsuid") & "' and   a.EBL_ID<>''"

            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox

            Dim str_search As String


            Dim str_EmpName As String = String.Empty
            Dim str_LeaveType As String = String.Empty
            Dim str_Year As String = String.Empty
            Dim str_Month As String = String.Empty

            Dim str_FromDate As String = String.Empty
            Dim str_ToDate As String = String.Empty
            Dim str_Remark As String = String.Empty


            If gvPrePayment.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtEmpName")
                str_EmpName = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_EmpName = " AND isnull(a.EName,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EmpName = " AND isnull(a.EName,'') NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EmpName = " AND isnull(a.EName,'') LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EmpName = " AND isnull(a.EName,'') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EmpName = " AND isnull(a.EName,'') LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EmpName = " AND isnull(a.EName,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                'str_Sid_search = h_Selected_menu_2.Value.Split("__")
                ' str_search = str_Sid_search(0)

                ' txtSearch = gvPrePayment.HeaderRow.FindControl("txtyear")
                ' str_txtYear = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_year = " AND a.FYear LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_year = "  AND  NOT a.FYear LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_year = " AND a.FYear  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_year = " AND a.FYear  NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_year = " AND a.FYear LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_year = " AND a.FYear NOT LIKE '%" & txtSearch.Text & "'"
                'End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtBname")
                str_LeaveType = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_LeaveType = " AND isnull(a.BName,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_LeaveType = "  AND  NOT isnull(a.BName,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_LeaveType = " AND isnull(a.BName,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_LeaveType = " AND isnull(a.BName,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_LeaveType = " AND isnull(a.BName,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_LeaveType = " AND isnull(a.BName,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtFromdate")

                str_FromDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_FromDate = " AND isnull(a.DTFROM,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FromDate = "  AND  NOT isnull(a.DTFROM,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FromDate = " AND isnull(a.DTFROM,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FromDate = " AND isnull(a.DTFROM,'') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FromDate = " AND isnull(a.DTFROM,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FromDate = " AND isnull(a.DTFROM,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtToDate")

                str_ToDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_ToDate = " AND isnull(a.DTTO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ToDate = "  AND  NOT isnull(a.DTTO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ToDate = " AND isnull(a.DTTO,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ToDate = " AND isnull(a.DTTO,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ToDate = " AND isnull(a.DTTO,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ToDate = " AND isnull(a.DTTO,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If




                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtBranch")

                str_Month = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Month = " AND isnull(a.Branch,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Month = "  AND  NOT isnull(a.Branch,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Month = " AND isnull(a.Branch,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Month = " AND isnull(a.Branch,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Month = " AND isnull(a.Branch,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Month = " AND isnull(a.Branch,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If




                'str_Sid_search = h_Selected_menu_7.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvPrePayment.HeaderRow.FindControl("txtCurrency")

                'str_txtCurrency = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_Currency = " AND a.Cur LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_Currency = "  AND  NOT a.Cur LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_Currency = " AND a.Cur  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_Currency = " AND a.Cur  NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_Currency = " AND a.Cur LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_Currency = " AND a.Cur NOT LIKE '%" & txtSearch.Text & "'"
                'End If


                str_Sid_search = h_Selected_menu_8.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtAmount")

                str_Year = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_year = " AND isnull(a.Amount,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_year = "  AND  NOT isnull(a.Amount,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_year = " AND isnull(a.Amount,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_year = " AND isnull(a.Amount,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_year = " AND isnull(a.Amount,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_year = " AND isnull(a.Amount,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_9.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtRemarks")

                str_Remark = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Remark = " AND isnull(a.REMARKS,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Remark = "  AND  NOT isnull(a.REMARKS,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Remark = " AND isnull(a.REMARKS,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Remark = " AND isnull(a.REMARKS,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Remark = " AND isnull(a.REMARKS,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Remark = " AND isnull(a.REMARKS,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If

           
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_EmpName & str_filter_LeaveType & str_filter_FromDate & str_filter_ToDate & str_filter_Month & str_filter_year & str_filter_Remark & AdjAddType & ViewState("SalTran") & "  order by a.Ename desc")

            gvPrePayment.DataSource = ds.Tables(0)



            If ds.Tables(0).Rows.Count > 0 Then


                gvPrePayment.DataBind()
                gvPrePayment.SelectedIndex = p_sindex
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' gvPrePayment.DataBind()

                ds.Tables(0).Rows(0)(9) = True


                gvPrePayment.DataSource = ds.Tables(0)
                Try
                    gvPrePayment.DataBind()
                Catch ex As Exception
                End Try





                Dim columnCount As Integer = gvPrePayment.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPrePayment.Rows(0).Cells.Clear()
                gvPrePayment.Rows(0).Cells.Add(New TableCell)
                gvPrePayment.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPrePayment.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPrePayment.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            
            Dim ddlHeader As New DropDownList
            ddlHeader = gvPrePayment.HeaderRow.FindControl("ddlSalaryTranH")
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlHeader.Items.Count - 1
                'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                If Not ViewState("SalTran_Status") Is Nothing Then
                    If ddlHeader.Items(ItemTypeCounter).Value = ViewState("SalTran_Status") Then
                        ddlHeader.SelectedIndex = ItemTypeCounter
                    End If
                End If

            Next


            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtDocNo")
            'txtSearch.Text = str_txtDocNo
            ''txtSearch = gvPrePayment.HeaderRow.FindControl("txtyear")
            ''txtSearch.Text = str_txtYear
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtCreditor")
            'txtSearch.Text = str_txtCreditor
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtprepaid")
            'txtSearch.Text = str_txtprepaid

            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtExp")
            'txtSearch.Text = str_txtExp
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtDate")
            'txtSearch.Text = str_txtDate
            ''txtSearch = gvPrePayment.HeaderRow.FindControl("txtCurrency")
            ''txtSearch.Text = str_txtCurrency
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtAmount")
            'txtSearch.Text = str_txtAmount

            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtNoInst")
            'txtSearch.Text = str_txtNoInst

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblEBL As New Label
            Dim url As String
            Dim viewid As String

            lblEBL = TryCast(sender.FindControl("lblEBL_ID"), Label)
            viewid = lblEBL.Text


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Payroll\empBankLoan.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                hlAddNew.NavigateUrl = String.Format("~\Payroll\empBankLoan.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                gvPrePayment.Attributes.Add("bordercolor", "#1b80b6")
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "P130053" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    '  h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    ' h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_9.Value = "LI__../Images/operations/like.gif"

                    rbActive.Checked = True

                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub

    Protected Sub rbActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActive.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbInactive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbInactive.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub btnSearchBName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchBranch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchAmount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchRemarks_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvPrePayment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrePayment.PageIndexChanging
        gvPrePayment.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchFromDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddlSalaryTranH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SalTran_Status") = sender.SelectedItem.Value
        If ViewState("SalTran_Status") = "0" Then
            ViewState("SalTran") = " AND a.Sal_tran = 0"
            'sender.SelectedIndex = 2
        ElseIf ViewState("SalTran_Status") = "1" Then
            ViewState("SalTran") = " AND a.Sal_tran = 1"
            'sender.SelectedIndex = 1
        ElseIf ViewState("SalTran_Status") = "2" Then
            ViewState("SalTran") = " AND (a.Sal_tran = 0 or a.Sal_tran = 1)"
            'sender.SelectedIndex = 0
        End If

        gridbind()
    End Sub


   
End Class
