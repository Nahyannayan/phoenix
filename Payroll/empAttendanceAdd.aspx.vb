Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Payroll_empAttendanceAdd
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtEmpNo.Attributes.Add("readonly", "readonly")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ddHalfday.Visible = False
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtFrom.Text = GetDiplayDate()
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130010" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Session("dtAttendance") = CreateDataTableAttendance()
            If Request.QueryString("viewempid") <> "" Then
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewempid").Replace(" ", "+")), _
                Encr_decrData.Decrypt(Request.QueryString("leave").Replace(" ", "+")), Encr_decrData.Decrypt(Request.QueryString("monthyear").Replace(" ", "+")))
                ddHalfday.Visible = True
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
            End If
            ViewState("iID") = 0
            bindevents(Session("sBsuid"), Session("sUsr_name"))

            bindMonthstatus()
            ' ddMonthstatusPeriodically.DataBind()

            gvAttendance.DataBind()
        End If
    End Sub

    Protected Sub ddMonthstatusPeriodically_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMonthstatusPeriodically.DataBound
        If Request.QueryString("viewempid") = "" Or ViewState("datamode") = "add" Then
            ddMonthstatusPeriodically.SelectedValue = "LOP"  'swapna added
        End If

    End Sub


    Private Sub setModifyHeader(ByVal p_viewempid As String, ByVal p_leave As String, ByVal p_monthyear As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim dMonthYear As New Date
            dMonthYear = CDate("1/" & p_monthyear)
            str_Sql = "SELECT EAT_ID AS id, EAT_REMARKS AS Remarks, " _
            & " EAT_DT AS Date, EAT_ELT_ID AS Leavetype,'Normal' as Status  , EAT_DAYS AS days, 1 AS Present" _
            & " FROM EMPATTENDANCE AS EAT" _
            & " WHERE (EAT_BSU_ID = '" & Session("sBsuid") & "') AND (EAT_EMP_ID ='" & p_viewempid & "') " _
            & " AND DATEPART(MONTH,EAT_DT)='" & dMonthYear.Month & "'  AND DATEPART(YEAR,EAT_DT)='" & dMonthYear.Year & "'" _
            & " AND EAT_ELT_ID='" & p_leave & "'" _
            & " order by EAT_DT "
            Dim ds As New DataSet 

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Session("dtAttendance") = ds.Tables(0)
                gridbind()
                str_Sql = "SELECT  top(1) EAT.EAT_EMP_ID, EAT.EAT_ELT_ID,   ISNULL(EM.EMP_FNAME, '')+' ' + " _
                    & " ISNULL(EM.EMP_MNAME, '')+' ' + ISNULL(EM.EMP_LNAME, '') AS EMP_NAME,EAT_REMARKS" _
                    & " FROM         EMPATTENDANCE AS EAT INNER JOIN EMPLOYEE_M AS EM" _
                    & " ON EAT.EAT_EMP_ID = EM.EMP_ID " _
                    & " WHERE (EAT_BSU_ID = '" & Session("sBsuid") & "') AND (EAT_EMP_ID ='" & p_viewempid & "') " _
                    & " AND DATEPART(MONTH,EAT_DT)='" & dMonthYear.Month & "'  AND DATEPART(YEAR,EAT_DT)='" & dMonthYear.Year & "'" _
                    & " AND EAT_ELT_ID='" & p_leave & "'" _
                    & " order by EAT_DT "

                ds.Tables.Clear()
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                ddLeavetypeMonthly.DataBind()
                h_Emp_No.Value = ds.Tables(0).Rows(0)("EAT_EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                ddLeavetypeMonthly.SelectedIndex = -1
                
                ddLeavetypeMonthly.Items.FindByValue(ds.Tables(0).Rows(0)("EAT_ELT_ID").ToString).Selected = True

                ' txtOn.Text = Format(CDate(ds.Tables(0).Rows(0)("EAT_DT")), "dd/MMM/yyyy")
                txtRemarks.Text = ds.Tables(0).Rows(0)("EAT_REMARKS").ToString
            Else

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub




    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        txtTo.Attributes.Add("readonly", "readonly")
        txtOn.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        btnAdddetails.Enabled = False

        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        tr_On.Visible = False
        tr_Fromto.Visible = True

        gvAttendance.Columns(5).Visible = False

        imgFrom.Enabled = False
        imgTo.Enabled = False
        imgEmployee.Enabled = False
        imgCalendar.Enabled = False
        ddMonthstatusPeriodically.Enabled = False
        ddLeavetypeMonthly.Enabled = False
    End Sub




    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        txtTo.Attributes.Remove("readonly")
        txtOn.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        btnAdddetails.Enabled = True

        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        tr_On.Visible = False
        tr_Fromto.Visible = True
        gvAttendance.Columns(5).Visible = True
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
        imgTo.Enabled = True
        imgCalendar.Enabled = True
        ddMonthstatusPeriodically.Enabled = True
        ddLeavetypeMonthly.Enabled = True
    End Sub




    Sub clear_All()
        txtFrom.Text = GetDiplayDate()
        txtTo.Text = ""
        txtTo.Text = ""
        txtOn.Text = ""
        txtRemarks.Text = ""
        txtEmpNo.Text = ""
        Session("dtAttendance").Rows.Clear()
        gridbind()
    End Sub





    Sub bindMonthstatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("strHoliday1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("strHoliday2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper

                'BIND_CHKWEEK(ds.Tables(0).Rows(0)(0).ToString.ToUpper)
                'BIND_CHKWEEK(ds.Tables(0).Rows(0)(1).ToString.ToUpper)
                lblWeekend.Text = "( Weekends : " & ds.Tables(0).Rows(0)(0).ToString.ToUpper & " & " & ds.Tables(0).Rows(0)(1).ToString.ToUpper & " )"

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub





    'Sub bindevents()
    '    Try
    '        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        str_Sql = "SELECT ELT_ID, ELT_DESCR FROM EMPLEAVETYPE_M"
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        ddMonthstatusPeriodically.DataSource = ds.Tables(0)
    '        ddMonthstatusPeriodically.DataBind()
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub


    Sub bindevents(ByVal bsuid As String, ByVal usrname As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "exec GetBSULeaveTypes '" & bsuid & "','" & usrname & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddMonthstatusPeriodically.DataSource = ds.Tables(0)
            ddMonthstatusPeriodically.DataTextField = "ELT_DESCR"
            ddMonthstatusPeriodically.DataValueField = "ELT_ID"
            ddMonthstatusPeriodically.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function get_AlreadyExists(ByVal dtDate As Date) As Boolean
        ' SUNDAY, SATURDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
        For i As Integer = 0 To Session("dtAttendance").Rows.Count - 1
            If Session("dtAttendance").Rows(i)("Date") = dtDate Then
                Return True
            End If
        Next
        Return False
    End Function




    Function get_Isholiday(ByVal dtDate As Date) As Boolean
        Dim isHoliday As Boolean = False
        ' SUNDAY, SATURDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
        Select Case dtDate.DayOfWeek
            Case DayOfWeek.Sunday
                If ViewState("strHoliday1") = "SUNDAY" Or ViewState("strHoliday2") = "SUNDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Monday
                If ViewState("strHoliday1") = "MONDAY" Or ViewState("strHoliday2") = "MONDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Tuesday
                If ViewState("strHoliday1") = "TUESDAY" Or ViewState("strHoliday2") = "TUESDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Wednesday
                If ViewState("strHoliday1") = "WEDNESDAY" Or ViewState("strHoliday2") = "WEDNESDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Thursday
                If ViewState("strHoliday1") = "THURSDAY" Or ViewState("strHoliday2") = "THURSDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Friday
                If ViewState("strHoliday1") = "FRIDAY" Or ViewState("strHoliday2") = "FRIDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Saturday
                If ViewState("strHoliday1") = "SATURDAY" Or ViewState("strHoliday2") = "SATURDAY" Then
                    isHoliday = True
                End If
        End Select
        Return isHoliday
    End Function




    Sub gridbind()

        Dim dv As New DataView(Session("dtAttendance"))
        dv.RowFilter = "Status<>'Deleted' "

        gvAttendance.DataSource = dv
        gvAttendance.DataBind()

        'gvAttendance.DataSource = Session("dtAttendance")
        'gvAttendance.DataBind()
    End Sub

    Function GETbHOLIDYSARELEAVE() As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            If ddMonthstatusPeriodically.SelectedItem.Value = "AL" Then
                str_Sql = "SELECT BLS.BLS_bHolidaysAreLeave FROM EMPLOYEE_M AS EM INNER JOIN " _
                        & " BSU_LEAVESLAB_S AS BLS ON EM.EMP_BLS_ID = BLS.BLS_ID " _
                        & " WHERE     (EM.EMP_BSU_ID = '" & Session("sBSuid") & "') AND (EM.EMP_bACTIVE = 1) AND (EM.EMP_ID ='" & h_Emp_No.Value & "' )"
            Else
                str_Sql = "SELECT BLS.BLS_bHolidaysAreLeave FROM EMPLOYEE_M AS EM INNER JOIN" _
                        & " BSU_LEAVESLAB_S AS BLS ON EM.EMP_ECT_ID = BLS.BLS_ECT_ID" _
                        & " WHERE     (EM.EMP_BSU_ID = '" & Session("sBSuid") & "') AND (EM.EMP_bACTIVE = 1)" _
                        & " AND (EM.EMP_ID = '" & h_Emp_No.Value & "') AND (BLS.BLS_bActive = 1)" _
                        & " AND (BLS.BLS_ELT_ID = '" & ddMonthstatusPeriodically.SelectedItem.Value & "')"
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return Convert.ToBoolean(ds.Tables(0).Rows(0)(0))
            Else
                Return True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return True
        End Try
    End Function


    Sub gridbind_add()
        Try
            Dim dtFrom, dtTo, dtTemp As Date
            Dim isHoliday As Boolean
            Dim bHOLIDYSARELEAVE As Boolean = GETbHOLIDYSARELEAVE()
            dtFrom = CDate(txtFrom.Text)
            dtTo = CDate(txtTo.Text)
            dtTemp = dtFrom
            If dtTo >= dtFrom Then
                While dtTo >= dtTemp
                    Dim dr As DataRow
                    dr = Session("dtAttendance").NewRow
                    dr("Id") = ViewState("iID")
                    ViewState("iID") = ViewState("iID") + 1
                    dr("Present") = True
                    dr("Remarks") = txtRemarks.Text
                    dr("Date") = dtTemp
                    dr("Days") = 1
                    dr("status") = "Normal"
                    If ddMonthstatusPeriodically.SelectedIndex = -1 Then
                        dr("Leavetype") = ddMonthstatusPeriodically.Items(0).Value & ""
                    Else
                        dr("Leavetype") = ddMonthstatusPeriodically.SelectedItem.Value
                    End If
                    If bHOLIDYSARELEAVE Then
                        isHoliday = False
                    Else
                        isHoliday = get_Isholiday(dtTemp)
                    End If
                    If isHoliday = False Then
                        If get_AlreadyExists(dtTemp) = False Then
                            Session("dtAttendance").Rows.Add(dr)
                        End If
                    End If
                    ' session("dtAttendance").Rows.Add(dr)
                    dtTemp = dtTemp.AddDays(1)
                End While
            Else
                lblError.Text = "From Date and To Date"
            End If
            gvAttendance.DataSource = Session("dtAttendance")
            gvAttendance.DataBind()
        Catch ex As Exception
            lblError.Text = "Check Date"
        End Try
    End Sub





    Function CreateDataTableAttendance() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cRemarks As New DataColumn("Remarks", System.Type.GetType("System.String"))
            Dim cPresent As New DataColumn("Present", System.Type.GetType("System.Boolean"))
            Dim cLeavetype As New DataColumn("Leavetype", System.Type.GetType("System.String"))
            Dim cDates As New DataColumn("Date", System.Type.GetType("System.DateTime"))
            Dim cDays As New DataColumn("Days", System.Type.GetType("System.Decimal"))
            'Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            'Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            'System.DateTime 
            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cRemarks)
            dtDt.Columns.Add(cPresent)

            dtDt.Columns.Add(cLeavetype)
            dtDt.Columns.Add(cDates)
            dtDt.Columns.Add(cDays)

            dtDt.Columns.Add(cStatus)
            'dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function




    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Session("dtAttendance").Rows.Count = 0 And ViewState("datamode") <> "edit" Then
            lblError.Text = "There is no data to save"
            Exit Sub
        End If
        AssignRemarks()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"

            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
               
                '  retval = SaveEMPATTENDANCE(eat_id, Session("sbsuid"), h_Emp_No.Value, _
                'ddLeavetypeMonthly.SelectedItem.Value, txtOn.Text, txtRemarks.Text.ToString, _
                'ddHalfday.SelectedItem.Value, edit_bool, stTrans)
                Dim str_opn As String
                For i As Integer = 0 To Session("dtAttendance").Rows.Count - 1

                    If Session("dtAttendance").Rows(i)("Status") <> "Deleted" Then
                        retval = PayrollFunctions.SaveEMPATTENDANCE(Session("dtAttendance").Rows(i)("id"), Session("sbsuid"), h_Emp_No.Value, _
                      Session("dtAttendance").Rows(i)("Leavetype"), _
                      Session("dtAttendance").Rows(i)("Date"), Session("dtAttendance").Rows(i)("Remarks"), _
                      Session("dtAttendance").Rows(i)("Days"), edit_bool, stTrans)
                        str_opn = "Edit"
                    Else
                        retval = PayrollFunctions.DeleteEMPATTENDANCE(Session("dtAttendance").Rows(i)("id"), _
                        Session("sbsuid"), stTrans)
                        str_opn = "Delete"
                    End If

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                    h_Emp_No.Value & "-" & Session("dtAttendance").Rows(i)("Leavetype") & "-" _
                    & Session("dtAttendance").Rows(i)("Date") & "-" & Session("dtAttendance").Rows(i)("Remarks") & "-" & _
                    Session("dtAttendance").Rows(i)("Days"), str_opn, Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If

                    If retval <> "0" Then
                        Exit For
                    End If
                Next
                
            Else
                edit_bool = False
                For i As Integer = 0 To Session("dtAttendance").Rows.Count - 1
                    retval = PayrollFunctions.SaveEMPATTENDANCE(0, Session("sbsuid"), h_Emp_No.Value, _
                    Session("dtAttendance").Rows(i)("Leavetype"), _
                    Session("dtAttendance").Rows(i)("Date"), Session("dtAttendance").Rows(i)("Remarks"), _
                    Session("dtAttendance").Rows(i)("Days"), edit_bool, stTrans)

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                    h_Emp_No.Value & "-" & Session("dtAttendance").Rows(i)("Leavetype") & "-" _
                    & Session("dtAttendance").Rows(i)("Date") & "-" & Session("dtAttendance").Rows(i)("Remarks") & "-" & _
                    Session("dtAttendance").Rows(i)("Days"), "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If

                    If retval <> "0" Then
                        Exit For
                    End If
                Next
            End If

            If retval = "0" Then
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                clear_All()
                gridbind()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
                'lblError.Text = "Cannot save. There is some errors"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


 



    Protected Sub gvAttendance_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAttendance.RowDataBound
        Try
            Dim ddMonthstatusgrd As New DropDownList
            Dim lblId As New Label
            ddMonthstatusgrd = e.Row.FindControl("ddMonthstatusgrd")
            lblId = e.Row.FindControl("lblLeave")
            ddMonthstatusgrd.Items.FindByValue(lblId.Text).Selected = True
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub




    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdddetails.Click
        AssignRemarks()
        gridbind_add()
    End Sub




    Protected Sub gvAttendance_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvAttendance.RowDeleting
        Try
            Dim row As GridViewRow = gvAttendance.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("dtAttendance").Rows.Count - 1
                If ViewState("datamode") = "edit" Then
                    If iIndex = Session("dtAttendance").Rows(iRemove)(0) Then
                        Session("dtAttendance").Rows(iRemove)("Status") = "Deleted"
                        Exit For
                    End If
                Else
                    If iIndex = Session("dtAttendance").Rows(iRemove)(0) Then
                        Session("dtAttendance").Rows(iRemove).Delete()
                        Exit For
                    End If
                End If

            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Record cannot be Deleted"
        End Try
    End Sub



    Private Sub AssignRemarks()
        Try
            For Each gvr As GridViewRow In gvAttendance.Rows
                Dim txtRemarks As TextBox = CType(gvr.FindControl("txtRemarks"), TextBox)
                Dim txtDays As TextBox = CType(gvr.FindControl("txtDays"), TextBox)
                Dim ddMonthstatusgrd As New DropDownList
                Dim lblId As New Label
                ddMonthstatusgrd = gvr.FindControl("ddMonthstatusgrd")
                lblId = gvr.FindControl("lblId")
                For i As Integer = 0 To Session("dtAttendance").Rows.Count - 1
                    If Session("dtAttendance").Rows(i)("id") = lblId.Text Then
                        Session("dtAttendance").Rows(i)("Leavetype") = ddMonthstatusgrd.SelectedItem.Value
                        Session("dtAttendance").Rows(i)("Remarks") = txtRemarks.Text
                        If IsNumeric(txtDays.Text) Then
                            If CDbl(txtDays.Text) = 1 Or CDbl(txtDays.Text) = 0.5 Then
                                Session("dtAttendance").Rows(i)("Days") = txtDays.Text
                            Else
                                Session("dtAttendance").Rows(i)("Days") = 1
                                txtDays.Text = 1
                            End If 
                        Else
                            Session("dtAttendance").Rows(i)("Days") = 1
                            txtDays.Text = 1
                        End If


                        Exit For
                    End If
                Next
            Next
        Catch ex As Exception

        End Try
    End Sub


     


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ddHalfday.Visible = True
    End Sub

    Sub setEditdata()
        txtOn.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        btnAdddetails.Enabled = True
        imgCalendar.Enabled = True
        ddLeavetypeMonthly.Enabled = True
        imgEmployee.Enabled = True
        gvAttendance.Columns(5).Visible = True

        txtFrom.Attributes.Remove("readonly")
        txtTo.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly") 
        imgFrom.Enabled = True
        imgEmployee.Enabled = True
        imgTo.Enabled = True
        imgCalendar.Enabled = True


    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ddHalfday.Visible = False
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Protected Sub txtDays_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("onBlur", "find_Ototal();")
        txtAmount.Attributes.Add("onFocus", "this.select();")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

    End Sub
End Class
