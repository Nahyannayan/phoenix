<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empRejoinEmployee.aspx.vb" Inherits="Payroll_empRejoinEmployee" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox' && (document.forms[0].elements[i].name.search(/chkEmployee/) > 0)) {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures);

            if (result != '' && result != undefined) {
                switch (txtControl) {

                    case 2:

                        break;
                }
            }
            return false;
        }

        function ToggleSearchEMPNames() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'display') {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = '';
                document.getElementById('<%=hfEmpName.ClientID %>').value = 'display';
            }
            return false;
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfEmpName.ClientID %>').value == 'none') {
                 document.getElementById('<%=trEMPName.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelEMPName.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusEMPName.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusEMPName.ClientID %>').style.display = 'none';
             }

             if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
             }
             return false;
         }

         function ToggleSearchCategory() {
             if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
             }
             else {
                 document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
                 document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
                 document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
             }
             return false;
         }

         function HideAll() {


             document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';

             document.getElementById('<%=trEMPName.ClientID %>').style.display = 'none';
             document.getElementById('<%=trSelEMPName.ClientID %>').style.display = '';
         }

         function SearchHide() {
             document.getElementById('trBSUnit').style.display = 'none';
         }



         function GetCATName() {
             var NameandCode;
             var url = "Reports/Aspx/SelIDDESC.aspx?ID=CAT";
             var oWnd = radopen(url, "pop_getcatname");
         }

        function OnClientClose1(oWnd, args) {
            var arg = args.get_argument();

            if (arg) {
                document.getElementById('<%=h_CATID.ClientID%>').value = arg.NameandCode;
                document.getElementById('<%=txtCatName.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%= txtCatName.ClientID%>', 'TextChanged');
            }
        }

            function GetEMPNAME() {
                var NameandCode;                
                var url = "Reports/Aspx/SelIDDESC.aspx?ID=EMP";
                var oWnd = radopen(url, "pop_getempname");
            }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                document.getElementById('<%=h_EMPID.ClientID%>').value = arg.NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = arg.NameandCode;
                __doPostBack('<%= txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getcatname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getempname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Rejoin Employee
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                  <%--  <tr class="subheader_img">
                        <td align="left" colspan="8" valign="middle">Rejoin Employee</td>
                    </tr>--%>
                    <tr id="trSelcategory" runat="server">
                        <td align="left" colspan="2" valign="top">
                            <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../Images/PLUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                            <span class="field-label">Select Category Filter</span></td>
                    </tr>
                    <tr id="trCategory" style="display: none;" runat="server">
                        <td align="left" valign="top">
                            <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../Images/MINUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Category</span></td>
                        
                        <td align="left">(Enter the Category ID you want to Add to the Search and click on Add)<br />
                            <asp:TextBox ID="txtCatName" runat="server" CssClass="inputbox" ></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddCATID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetCATName(); return false;" /><br />
                            <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="CAT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSelEMPName" runat="server">
                        <td align="left" colspan="2" valign="top">
                            <asp:ImageButton ID="imgPlusEmpName" runat="server" ImageUrl="../Images/PLUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" />
                            <span class="field-label">Select Employee Name Filter</span></td>
                    </tr>
                    <tr id="trEMPName" runat="server" style="display: none;">
                        <td align="left" valign="top" >
                            <asp:ImageButton ID="imgMinusEmpName" runat="server" Style="display: none;" ImageUrl="../Images/MINUS.jpg" OnClientClick="ToggleSearchEMPNames();return false;" CausesValidation="False" /><span class="field-label">Employee Name</span></td>                       
                        <td align="left" valign="top">
                            <asp:Label ID="Label1" runat="server" Text="(Enter the Employee ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtEMPNAME" runat="server" CssClass="inputbox"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddEMPID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNAME(); return false;" /><br />
                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMP ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMPNO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="EMPABCCat" runat="server">
                        <td align="left" valign="top"><span class="field-label">Employee Category</span></td>                       
                        <td align="left"  valign="top">
                            <asp:CheckBox ID="chkEMPABC_A" runat="server" Text="A" Checked="True"  CssClass="field-label"/>
                            <asp:CheckBox ID="chkEMPABC_B" runat="server" Text="B" Checked="True" CssClass="field-label" />
                            <asp:CheckBox ID="chkEMPABC_C" runat="server" Text="C" Checked="True" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:GridView ID="gvEmpDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%" EmptyDataText="No Details Found" AllowPaging="True"
                                OnPageIndexChanging="gvEmpDetails_PageIndexChanging" PageSize="20">
                                <RowStyle CssClass="griditem"  />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:BoundField DataField="EMPNO" HeaderText="EmpNo"></asp:BoundField>
                                    <asp:BoundField DataField="EMP_NAME" HeaderText="Name"></asp:BoundField>
                                    <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Vacation From" HtmlEncode="False"></asp:BoundField>
                                    <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Vacation To" HtmlEncode="False"></asp:BoundField>
                                    <asp:BoundField DataField="ECT_DESCR" HeaderText="Category" SortExpression="emp_ECT_id"></asp:BoundField>
                                    <asp:TemplateField HeaderText="REJOINDT">
                                        <ItemTemplate>
                                           <asp:TextBox ID="txtDate" runat="server" OnPreRender="txtDate_PreRender" Text='<%# Bind("REJOINDT", "{0:dd/MMM/yyyy}") %>'
                                                Width="75%"></asp:TextBox><asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgTo" TargetControlID="txtDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="txtDate" TargetControlID="txtDate">
                                            </ajaxToolkit:CalendarExtender>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <input id="chkAll" value='<%# Bind("ELA_EMP_ID") %>' onclick="change_chk_state(this);" type="checkbox" runat="server" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <input id="chkEmployee" value='<%# Bind("ELA_ID") %>' type="checkbox" runat="server" />
                                            <asp:Label ID="lblChkEMPID" runat="server" Text='<%# Bind("ELA_EMP_ID") %>' Visible="false"></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>                           
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="dayBook" />
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Rejoin Employee" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />                           
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_CATID" runat="server" />               
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfDepartment" runat="server" type="hidden" />
                <input id="hfCategory" runat="server" type="hidden" />
                <input id="hfDesignation" runat="server" type="hidden" />
                <input id="hfEmpName" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>

