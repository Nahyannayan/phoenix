Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Payroll_empApprovalHeirarchyView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            hlAddNew.NavigateUrl = "empApprovalHeirarchy.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "P100020" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub


     


    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet




          
            str_Sql = "SELECT     LPS_ID, LPS_DESCRIPTION, LPS_BSU_ID, LPS_bActive " _
            & " FROM APPROVALPOLICY_H " _
            & " WHERE LPS_bActive = 1  AND  LPS_BSU_ID = '" & Session("sBSUID") & "'  "
           

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvApprovalPolicy.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvApprovalPolicy.DataBind()
                Dim columnCount As Integer = gvApprovalPolicy.Rows(0).Cells.Count

                gvApprovalPolicy.Rows(0).Cells.Clear()
                gvApprovalPolicy.Rows(0).Cells.Add(New TableCell)
                gvApprovalPolicy.Rows(0).Cells(0).ColumnSpan = columnCount
                gvApprovalPolicy.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvApprovalPolicy.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvApprovalPolicy.DataBind()
            End If
            'gvApprovalPolicy.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


     


    Protected Sub gvApprovalPolicy_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvApprovalPolicy.PageIndexChanging
        gvApprovalPolicy.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

 

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApprovalPolicy.RowDataBound
        Try

            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblLPS_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                hlEdit.NavigateUrl = "empApprovalHeirarchy.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class


 