Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empDeleteSalary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version        Author          Date            Change
    '1.1            Swapna          10-Apr-2011     To remove WPS processing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ' lblError.Text = "<marquee behavior='alternate'>PANDARAM</marquee>"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            Page.Title = OASISConstants.Gemstitle
            gvSalary.Attributes.Add("bordercolor", "#1b80b6")
            Session("EMP_SEL_COND") = " AND EMP_BSU_ID='" & Session("sBsuid") & "' " _
            & " AND (EMP_bACTIVE = 1 or (month(isnull(emp_resgdt,getdate()))= month(getdate()) and year( isnull(emp_resgdt,getdate()))=year(getdate()))) "
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P130190" And ViewState("MainMnu_code") <> "P130191") Then   'V.1
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            filldrp()
            If ViewState("MainMnu_code") = "P130191" Then
                btnSave.Text = "Remove WPS Processing"
            Else
                btnSave.Text = "Remove Salary"
            End If
            FillSalary()
        End If
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim count As Integer = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim str_removed As String = ""
            For i As Integer = 0 To gvSalary.Rows.Count - 1
                Dim lblid As New Label
                Dim chkESD_ID As New HtmlInputCheckBox
                Dim lblESD_EMP_ID As New Label


                Dim K As Integer = True
                chkESD_ID = TryCast(gvSalary.Rows(i).FindControl("chkESD_ID"), HtmlInputCheckBox)
                lblESD_EMP_ID = TryCast(gvSalary.Rows(i).FindControl("lblESD_EMP_ID"), Label)
                'V1.1 changes    
                If chkESD_ID.Checked Then
                    count = count + 1
                    If ViewState("MainMnu_code") = "P130190" Then

                        retval = PayrollFunctions.DeleteEMPSALARYDATA(chkESD_ID.Value, stTrans)
                        str_removed = str_removed & lblESD_EMP_ID.Text & "_" & gvSalary.Rows(i).Cells(2).Text & ","

                        If retval <> 0 Then
                            Exit For
                        End If

                    ElseIf ViewState("MainMnu_code") = "P130191" Then
                        retval = RemoveWPsProcessing(chkESD_ID.Value, objConn, stTrans)
                        str_removed = str_removed & lblESD_EMP_ID.Text & "_" & gvSalary.Rows(i).Cells(2).Text & ","
                        If retval <> 0 Then
                            Exit For
                        End If
                    End If
                End If
            Next
            If count = 0 Then
                lblError.Text = "No records selected."
                retval = 1000
                Exit Sub
            End If
            If retval = "0" Then
                stTrans.Commit()
                FillSalary()
                'V1.1 changes
                Dim flagAudit As Integer
                Dim err As String = ""
                If ViewState("MainMnu_code") = "P130190" Then

                    flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, str_removed, _
                         "Delete", Page.User.Identity.Name.ToString, Me.Page)
                    err = "Salary is Removed Successfully ..."

                ElseIf ViewState("MainMnu_code") = "P130191" Then

                    flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, str_removed, _
                         "Update", Page.User.Identity.Name.ToString, Me.Page)
                    err = "WPS Processing is Removed Successfully ..."
                End If
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = err
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Function RemoveWPsProcessing(ByVal p_ESD_ID As Integer, ByVal conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String


        Try

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ESD_ID", SqlDbType.Int)
            pParms(0).Value = p_ESD_ID
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "RemoveWPsProcessing", pParms)
            RemoveWPsProcessing = pParms(1).Value

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    Public Sub filldrp()
        Try
            Dim str_Sql As String
            Dim strFil As String = ""
            If ViewState("MainMnu_code") = "P130191" Then
                strFil = "AND  IsNull(ESD_BProcessWPS,0)=1 "
            End If

            str_Sql = "select * from (" _
                      & " Select '' MONTHVALUE,'All' MONTHYEAR,'31/Dec/2500' ESD_DATE " _
                      & " UNION ALL " _
                      & " SELECT distinct dbo.fn_GetMonthFirst3(ESD.ESD_MONTH) + '/' + LTRIM(RTRIM(ESD.ESD_YEAR)) MONTHVALUE,dbo.fn_GetMonthFirst3(ESD.ESD_MONTH) + '/' + LTRIM(RTRIM(ESD.ESD_YEAR)) MONTHYEAR,[dbo].fn_ReturnDate(1,ESD_MONTH,ESD_YEAR) ESD_DATE  " _
                      & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
                      & " EMPLOYEE_M AS EMP ON ESD.ESD_EMP_ID = EMP.EMP_ID" _
                      & " WHERE (ESD.ESD_PAID = 0) AND (ESD.ESD_bPOSTED = 0) AND (ESD.ESD_bVaccation = 0) " _
                      & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "')" & strFil & ") A order by    cast(ESD_DATE as datetime) desc"
            Dim monthDT As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            monthDT = Mainclass.getDataTable(str_Sql, str_conn)
            ddMonth.DataSource = monthDT
            ddMonth.DataTextField = "MONTHYEAR"
            ddMonth.DataValueField = "MONTHVALUE"
            ddMonth.DataBind()
            If monthDT.Rows.Count > 1 Then
                Dim maxValue As String
                maxValue = monthDT.Compute("max(ESD_DATE)", "ESD_DATE <> '31/Dec/2500'")
                ddMonth.SelectedValue = monthDT.Select("ESD_DATE ='" & maxValue & "'", "ESD_DATE desc")(0)("MONTHVALUE")
            Else
                ddMonth.SelectedValue = ""
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Function FillSalary() As Boolean
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        'V1.1

        '--  V1.2 swapna to add paging and filter-----------
        Dim lstrEMPNo As String = String.Empty
        Dim lstrEmpName As String = String.Empty

        Dim lstrFiltEMPNo As String = String.Empty
        Dim lstrFiltEmpName As String = String.Empty


        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox

        If gvSalary.Rows.Count > 0 Then
            ' --- Initialize The Variables



            larrSearchOpr = h_Selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvSalary.HeaderRow.FindControl("txtEmpNo")
            lstrEMPNo = Trim(txtSearch.Text)
            If (lstrEMPNo <> "") Then lstrFiltEMPNo = SetCondn(lstrOpr, "EMPNO", lstrEMPNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvSalary.HeaderRow.FindControl("txtEmpName")
            lstrEmpName = txtSearch.Text
            If (lstrEmpName <> "") Then lstrFiltEmpName = SetCondn(lstrOpr, "EMPNAME", lstrEmpName)
        End If
      

        Dim strFil, strMonth As String
        strFil = "" : strMonth = ""
        If ViewState("MainMnu_code") = "P130191" Then
            strFil = "AND  IsNull(ESD_BProcessWPS,0)=1 "
        End If
        If ddMonth.SelectedValue <> "" Then
            strMonth = " AND dbo.fn_GetMonthFirst3(ESD.ESD_MONTH) + '/' + LTRIM(RTRIM(ESD.ESD_YEAR)) = '" & ddMonth.SelectedValue & "'"
        End If

        str_Sql = "SELECT ESD.ESD_EMP_ID,ESD.ESD_ID, ESD.ESD_BSU_ID, ESD.ESD_EARN_NET, " _
        & " dbo.fn_GetMonthFirst3(ESD.ESD_MONTH) + '/' + LTRIM(RTRIM(ESD.ESD_YEAR)) AS MONTHYEAR, " _
        & " EMPNAME AS EMP_NAME," _
        & " CASE ESD.ESD_MODE WHEN 0 THEN 'Cash' ELSE 'Bank' END AS BANKCASH, EMP.EMPNO" _
        & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
        & " vw_OSO_EMPLOYEEMASTERDETAILS_COMPACT AS EMP ON ESD.ESD_EMP_ID = EMP.EMP_ID" _
        & " WHERE (ESD.ESD_PAID = 0) AND (ESD.ESD_bPOSTED = 0) AND (ESD.ESD_bVaccation = 0) " _
        & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "')" & strFil & strMonth & lstrFiltEMPNo & lstrFiltEmpName & "  order by    [dbo].fn_ReturnDate(1,ESD_MONTH,ESD_YEAR) desc"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvSalary.DataSource = ds
        gvSalary.DataBind()
       

        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        txtSearch = gvSalary.HeaderRow.FindControl("txtEmpNo")
        txtSearch.Text = lstrEMPNo

        txtSearch = gvSalary.HeaderRow.FindControl("txtEmpName")
        txtSearch.Text = lstrEmpName
        Return True

       
    End Function

    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If

        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvSalary.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvSalary.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub ddMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMonth.SelectedIndexChanged
        FillSalary()
    End Sub

    Protected Sub gvSalary_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSalary.PageIndexChanging
        gvSalary.PageIndex = e.NewPageIndex
        FillSalary()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FillSalary()
    End Sub
    Protected Sub btnEnterSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnterSearch.Click
        FillSalary()
    End Sub
End Class
