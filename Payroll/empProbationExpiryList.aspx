<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empProbationExpiryList.aspx.vb" Inherits="Payroll_empProbationExpiryList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Probation Expiry List
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center"
                    cellpadding="3" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left" valign="middle">

                            <asp:Label ID="lblExpiry" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center"  valign="top" width="100%">
                            <asp:GridView ID="gvDocExpiry" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Transaction details" 
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="EMPNO" HeaderText="Empno" ReadOnly="True" SortExpression="EMPNO" />
                                    <asp:BoundField DataField="EMP_NAME" HeaderText="Emp Name" SortExpression="EMP_NAME" />
                                    <asp:BoundField DataField="ECT_DESCR" HeaderText="Category" SortExpression="ECT_DESCR" />
                                    <asp:BoundField DataField="EMP_JOINDT" HeaderText="Join Dt." SortExpression="EMP_JOINDT">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EMP_PROBTILL" HeaderText="Probation Exp." SortExpression="EMP_PROBTILL">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle  />
                            </asp:GridView>
                            <a id="detail"></a>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top" width="100%">&nbsp;<asp:CheckBox ID="chkAll" runat="server" Text="Show All" AutoPostBack="True" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
