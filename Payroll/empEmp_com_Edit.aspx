<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empEmp_com_Edit.aspx.vb" Inherits="CatEdit_del_new" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getEmpID(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Accounts/accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'DT') {
                url = '../Accounts/accShowEmpDetail.aspx?id=ET';
                var oWnd = radopen(url, "pop_employee");
                //  result = window.showModalDialog(url, "", sFeatures);
                <%--if (result == '' || result == undefined)
                { return false; }
                NameandCode = result.split('___');
                document.getElementById("<%=txtType.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfTypeID.ClientID %>").value = NameandCode[1];
            }--%>
            }
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtType.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfTypeID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtType.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="lblTitle" runat="server" Text="Employee Category"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:" CssClass="error" ForeColor="" ValidationGroup="save" />

                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error" SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Table ID="tblComm" runat="server" CellPadding="7" CellSpacing="0" Width="100%" Height="100%">
                                <%-- <asp:TableRow ID="MainHead" runat="server"  HorizontalAlign="Left" VerticalAlign="Middle">
                        <asp:TableCell runat="server" ColumnSpan="3" CssClass="subheader_img"> <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                 </font></asp:TableCell>
                   
                   
                    </asp:TableRow>--%>
                                <asp:TableRow ID="ro1" runat="server" HorizontalAlign="Left">
                                    <asp:TableCell ID="clID" runat="server" Width="20%">
                                        <asp:Label ID="lblID" runat="server" Text="ID" CssClass="field-label"></asp:Label></asp:TableCell>

                                    <asp:TableCell ID="clControl1" runat="server" Width="40%">
                                        <asp:TextBox ID="txtID" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtID"
                                            Display="Dynamic" ErrorMessage="ID required." CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="ro2" runat="server" HorizontalAlign="Left">
                                    <asp:TableCell ID="cl2Desc" runat="server" Width="20%">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="field-label"></asp:Label></asp:TableCell>
                                    <%--  <asp:TableCell ID="clDot2" runat="server" >
                                        <asp:Label ID="lblDot3" runat="server" Text=":"></asp:Label></asp:TableCell>--%>
                                    <asp:TableCell ID="cl2Control2" runat="server" Width="40%">
                                        <asp:TextBox ID="txtDesc" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDesc"
                                            Display="Dynamic" ErrorMessage="Description can not be left empty." CssClass="error" ForeColor="" ValidationGroup="save" >*</asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="ro3" runat="server" HorizontalAlign="Left">
                                    <asp:TableCell ID="cl3Paid" runat="server" Width="20%">
                                        <asp:Label ID="lblBpaid" runat="server" Text="Paid" CssClass="field-label"></asp:Label></asp:TableCell>
                                    <%--<asp:TableCell ID="clDot3" runat="server" >
                                        <asp:Label ID="lblDot" runat="server" Text=":"></asp:Label></asp:TableCell>--%>
                                    <asp:TableCell ID="cl3radio1" runat="server" Width="40%">
                                        <asp:RadioButton ID="rdPaidY" runat="server" Text="Yes" GroupName="Paid" />
                                        <asp:RadioButton ID="rdPaidN" runat="server" Text="No" GroupName="Paid" />
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="ro4" runat="server" HorizontalAlign="Left">
                                    <asp:TableCell ID="cl4Include" runat="server" Width="20%">
                                        <asp:Label ID="lblEob" runat="server" Text="Include in EOS Calculation" CssClass="field-label"></asp:Label></asp:TableCell>
                                    <%--<asp:TableCell ID="clDot4" runat="server" >
                                        <asp:Label ID="lblDot2" runat="server" Text=":"></asp:Label></asp:TableCell>--%>
                                    <asp:TableCell ID="clrd2" runat="server" Width="40%">
                                        <asp:RadioButton ID="rdEobY" runat="server" Text="Yes" GroupName="EOB" />
                                        <asp:RadioButton ID="rdEobN" runat="server" Text="No" GroupName="EOB" />
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="ro6" runat="server">
                                    <asp:TableCell ID="cl6Max" runat="server" HorizontalAlign="Left" Width="20%">
                                        <asp:Label ID="lblMaxAll" runat="server" Text="Maximum Allowed" CssClass="field-label"></asp:Label>
                                    </asp:TableCell>

                                    <%-- <asp:TableCell ID="cl6Dot" runat="server" >
                                        <asp:Label ID="lblDot6" runat="server" Text=":"></asp:Label>

                                    </asp:TableCell>--%>

                                    <asp:TableCell ID="cl6Month" runat="server" HorizontalAlign="Left" Width="40%">
                                        <asp:TextBox ID="txtMonth" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMonth"
                                            Display="Dynamic" ErrorMessage="Month can not be left empty." CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvDouble" runat="server" ControlToValidate="txtMonth"
                                            ErrorMessage="Enter Numeric Values only" Operator="DataTypeCheck" Type="Double" Display="Dynamic" CssClass="error" ForeColor="">*</asp:CompareValidator>
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="ro5" runat="server" HorizontalAlign="Left">
                                    <asp:TableCell ID="cl5Resp" runat="server" Width="20%">
                                        <asp:Label ID="lblRep" runat="server" Text="Job Responsibility" CssClass="field-label"></asp:Label>
                                    </asp:TableCell>
                                    <%-- <asp:TableCell ID="cl5Dot" runat="server" >
                                        <asp:Label ID="lblDot5" runat="server" Text=":"></asp:Label></asp:TableCell>--%>
                                    <asp:TableCell ID="cl5ddl" runat="server" Width="40%">

                                        <asp:DropDownList ID="ddlResp" runat="server">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="tbRowEarningCode" runat="server" HorizontalAlign="Left">
                                    <asp:TableCell ID="TCEarningCode" runat="server" Width="20%">
                                        <asp:Label ID="lblEarningCode" runat="server" Text="Earning Code" CssClass="field-label"></asp:Label>
                                    </asp:TableCell>
                                    <%-- <asp:TableCell ID="TCECDOT" runat="server" >
                                        <asp:Label ID="lblECDOT" runat="server" Text=":"></asp:Label>
                                    </asp:TableCell>--%>
                                    <asp:TableCell ID="TCECTXTBOX" runat="server" Width="40%">

                                        <asp:TextBox ID="txtType" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtType"
                                            Display="Dynamic" ErrorMessage="Earn Type can not be left empty." CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                        <asp:ImageButton ID="btnFixAsset" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getEmpID('DT');return false;" Text="..." />
                                    </asp:TableCell>
                                    <asp:TableCell></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </td>
                    </tr>
                   
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" ValidationGroup="save"  />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" ValidationGroup="save"  />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" ValidationGroup="save" 
                                Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"  
                                Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="hfTypeID" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

