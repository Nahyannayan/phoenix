<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empCalendar.aspx.vb" Inherits="Payroll_empCalendar" Title="Calendar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Monthly calendar settings
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">

                    <tr width="100%">
                        <td align="center" colspan="2" width="40%">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="drpCalMonth" runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" CssClass="listbox" Style="vertical-align: middle; margin: 0px; display: inline" Width="100%"></asp:DropDownList></td>
                                    <td>
                                        <asp:DropDownList ID="drpCalYear" runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" CssClass="listbox" Style="vertical-align: middle; margin: 0px; display: inline" Width="100%"></asp:DropDownList></td>
                                </tr>
                            </table>


                        </td>

                        <td align="center" width="50%">

                            <asp:DropDownList ID="ddlCategory" runat="server" CssClass="listbox"
                                DataSourceID="SqlDataSource1" DataTextField="ECT_DESCR" DataValueField="ECT_ID" Width="60%" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td></td>

                    </tr>
                    <tr>
                        <td colspan="4" align="center" valign="middle" style="height: 412px" width="100%">
                            <asp:Calendar ID="calDatepicker" runat="server" Font-Size="8pt" Height="416px" SelectionMode="DayWeekMonth" Width="80%" SelectMonthText="Select All" BackColor="#E2F3C9" BorderColor="#A8DD99" BorderWidth="1px" DayNameFormat="Shortest" ForeColor="#663399" ShowGridLines="True" NextPrevFormat="ShortMonth">
                                <OtherMonthDayStyle ForeColor="#CC9966" BackColor="#E2F3C9" />
                                <TitleStyle Font-Bold="True"
                                    BackColor="#5C8D22" ForeColor="#E2F3C9" />
                                <NextPrevStyle  ForeColor="#E2F3C9" />
                                <SelectedDayStyle BackColor="#5E5EFF" Font-Bold="True" />
                                <SelectorStyle BackColor="#A8DD99" />
                                <TodayDayStyle BackColor="#A8DD99" ForeColor="White" />
                                <DayHeaderStyle BackColor="#A8DD99" Font-Bold="True" Height="1px" />
                            </asp:Calendar>
                            <span class="radiobutton">Weekend</span>
                            <asp:CheckBox ID="chkSunday" runat="server" CssClass="radiobutton" Text="Sunday" />&nbsp;&nbsp;
                    <asp:CheckBox ID="chkMonday" runat="server" CssClass="radiobutton" Text="Monday" />
                            <asp:CheckBox ID="chkTuesday" runat="server" CssClass="radiobutton" Text="Tuesday" />
                            <asp:CheckBox ID="chkWednesday" runat="server" CssClass="radiobutton" Text="Wednesday" />
                            <asp:CheckBox ID="chkThursday" runat="server" CssClass="radiobutton" Text="Thursday" />
                            <asp:CheckBox ID="chkFriday" runat="server" CssClass="radiobutton" Text="Friday" />
                            <asp:CheckBox ID="chkSaturday" runat="server" CssClass="radiobutton" Text="Saturday" /><br />
                            <table id="table1" width="80%">
                                <tr>
                                    <td align="left" bgcolor="#5e5eff">Selected Day</td>
                                    <td align="left" bgcolor="#c6ffb3">Public Holiday</td>
                                    <td align="left" bgcolor="#EED549">Default Weekend</td>
                                    <td align="left" bgcolor="#ffa8a8">Compensatory Working day</td>
                                    <td align="left" bgcolor="#c69fff">Holiday</td>
                                    <td align="left" bgcolor="#ffffbf">Ramadan Days</td>
                                    <td align="left" bgcolor="#d5ffff">Weekend Holiday</td>
                                    <td align="left" bgcolor="#CCCCBF">Winter Break</td>
                                </tr>
                            </table>
                    </tr>
                </table>
                <table width="100%">
                    <tr runat="server" id="tr_On">
                        <td align="left" width="20%">
                            <asp:RadioButton ID="rbOn" runat="server" CssClass="field-label" Text="On" Checked="True" GroupName="dt" AutoPostBack="True" /></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtOn" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr runat="server" id="tr_From">
                        <td align="left" width="20%">
                            <asp:RadioButton ID="rbFrom" runat="server" CssClass="field-label" Text="From" GroupName="dt" AutoPostBack="True" /></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" /></td>
                        <td align="left" width="20%">
                            <span class="field-label">To</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                    </tr>
                    <tr>
                        <td align="left">

                            <span class="field-label">Status</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddMonthstatus" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Category</span></td>

                        <td align="left" dir="ltr">
                            <asp:CheckBox ID="chkAll" runat="server" Text="All" Font-Overline="False" AutoPostBack="True" OnCheckedChanged="chkAll_CheckedChanged"></asp:CheckBox>
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="chkCategory" runat="server" DataSourceID="SqlDataSource1" DataTextField="ECT_DESCR" DataValueField="ECT_ID" RepeatDirection="Vertical" Font-Overline="False" OnDataBound="chkCategory_DataBound">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Remarks</span></td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText" TextMode="MultiLine" Width="95%"></asp:TextBox>
                        </td>
                    </tr>


                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT ECT_ID,ECT_DESCR from empcategory_m order by ECT_SORT_ID"></asp:SqlDataSource>
                    <tr>
                        <td align="left">
                            <span class="field-label">Days</span></td>
                        <td align="left" colspan="3">
                            <asp:RadioButtonList ID="rblDays" CssClass="field-label" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0">Full Day</asp:ListItem>
                                <asp:ListItem Value="1">Half Day</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False"></asp:Label></td>
                    </tr>


                    <tr>

                        <td colspan="4" align="center">&nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalDTOntext" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtOn" TargetControlID="txtOn">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalDTOnImg" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtOn">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalDTFromtext" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalDTFromImg" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalDTToText" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalDTToImg" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton2" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

