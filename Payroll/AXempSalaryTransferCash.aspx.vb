Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
' ************************************ This screen is meant for AX units only *********************************
'Version        Author          Date            Change
'1.1            Swapna          1-Dec-2015       Bank/Cash salary transfer for AX units.
'2.0            Swapna          16-Mar-2016      Update AX posting staging table with payroll posted data

Public Class Payroll_AXempSalaryTransferCash
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim lstrUsedChqNos As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            txtAmount.Attributes.Add("readonly", "readonly")
            txtDocdate.Text = GetDiplayDate()
            rbCashTran.Checked = True
            rbCash.Checked = True
            rbOthers_CheckedChanged(Nothing, Nothing)
            'bind_BusinessUnit()
            gvDetailsCash.Attributes.Add("bordercolor", "#1b80b6")
            Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            btnPrint.Visible = False
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "P130180" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            setBankorCashTransfer()
            set_Comboanddate()
            Set_CashorBankPayment()
            'fill_Emp_for_BSU()
        Else
            'FillEmpNames(h_EMPID.Value)
        End If
    End Sub

#Region "PostToAXStaging" 'V2.0

    Private Function PostToPayrollAXStaging(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal RefDocNo As String) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            If RefDocNo <> "" Then
                Dim cmd As New SqlCommand("oasis_dax.dbo.SavePayroll_DAX_Postings", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure


                Dim sqlpJHD_DocNo As New SqlParameter("@DOCNO", SqlDbType.VarChar)
                sqlpJHD_DocNo.Value = RefDocNo
                cmd.Parameters.Add(sqlpJHD_DocNo)

                Dim sqlpJHD_Narration As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpJHD_Narration.Value = Session("sBSuid").ToString
                cmd.Parameters.Add(sqlpJHD_Narration)


                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                Return iReturnvalue
            Else
                Return 1000

            End If

        Catch ex As Exception
            lblError.Text = ex.Message
            Return 1000
        End Try

    End Function

#End Region
    Function validate_controls() As String
        Dim str_error As String = ""
        Dim strfDate As String = txtDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            str_error = "Please Enter Valid Document Date <br>"
        Else
            txtDocdate.Text = strfDate
        End If

        'validations----for cash payment
        If rbCash.Checked Then

            If txtCashAcc.Text = "" Then
                str_error = str_error & "Please Select Cash Account <br>"
            End If
            txtCashDescr.Text = AccountFunctions.Validate_Account(txtCashAcc.Text, Session("sbsuid"), "CASHONLY")
            If txtCashDescr.Text = "" Then
                str_error = str_error & "Invalid Cash Account Selected <br>"
            End If
        End If
        'validations----for bank payment
        If rbBank.Checked Then
            If txtBankCode.Text = "" Then
                str_error = str_error & "Please Select Bank <br>"
            End If
            txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
            If txtBankDescr.Text = "" Then
                str_error = str_error & "Invalid bank selected <br>"
            End If
            If txtNarrn.Text = "" Then
                str_error = str_error & "Please Enter Narration <br>"
            End If
            If txtChequedate.Text = "" Then
                str_error = str_error & "Please Enter  Date <br>"
            Else
                If IsDate(txtChequedate.Text) = False Then
                    str_error = str_error & "Please Enter  Date <br>"
                End If
            End If
            If rbCheque.Checked = True Then
                If txtChqBook.Text = "" Then
                    str_error = str_error & "Please Select Cheque <br>"
                End If
            End If
            If rbOthers.Checked = True Then
                If txtrefChequeno.Text = "" Then
                    str_error = str_error & "Please Enter Ref No. <br>"
                End If
            End If
            validate_controls = str_error
        End If
        If txtNarrn.Text = "" Then
            str_error = str_error & "Please Enter Narration <br>"
        End If
        validate_controls = str_error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = ""
        str_err = validate_controls()
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        End If
        If Not IsNumeric(txtAmount.Text) Then
            lblError.Text = "Invalid Amount"
            Exit Sub
        End If
        If CInt(txtAmount.Text) <= 0 Then
            lblError.Text = "Invalid Amount"
            Exit Sub
        End If
        If txtValueDate.Text = "" Then
            lblError.Text = "Please enter Value Date"
            Exit Sub
        ElseIf CDate(txtValueDate.Text) < Today.Date Then
            lblError.Text = "Value date cannot be less than today's date"
            Exit Sub
        End If
        Dim retval As String = "1000"

        If rbCashTran.Checked = True Then
            If rbCash.Checked Then
                retval = AccountFunctions.GetNextDocId("CP", Session("sBsuid"), CType(txtDocdate.Text, Date).Month, CType(txtDocdate.Text, Date).Year)
            ElseIf rbBank.Checked = True Then
                retval = AccountFunctions.GetNextDocId("BP", Session("sBsuid"), CType(txtDocdate.Text, Date).Month, CType(txtDocdate.Text, Date).Year)
            End If
            Save_voucherforCashTransaction()
        ElseIf rbBankTran.Checked = True Then
            retval = AccountFunctions.GetNextDocId("BP", Session("sBsuid"), CType(txtDocdate.Text, Date).Month, CType(txtDocdate.Text, Date).Year)
            Save_voucherBank()
        End If

    End Sub
    'New function for salary transfer Cash
    Protected Sub Save_voucherforCashTransaction()

        Dim lstrNewDocNo As String

        Dim str_connOasisFin As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_connOasisDAX As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_connOasisDAX)

        Dim gdsSysInfo As New DataSet
        Dim str_err As String = ""

        Dim str_Provacc As String = ""
        Dim str_Salaryacc As String = ""
        Dim str_Cashflow As String = ""
        Dim str_Exgrate As Decimal
        Dim str_CorCurid As String = ""
        Dim str_Sql As String
        str_Sql = " SELECT SYS_SALARYPDCPROVACC, SYS_SALARYCASHFLOW,SYS_CUR_ID,SYS_SALARYPAYABLE_ACC_ID From vw_OSF_SYSINFO_S"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasis, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Provacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPDCPROVACC")
            str_Cashflow = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYCASHFLOW")
            str_CorCurid = gdsSysInfo.Tables(0).Rows(0)("SYS_CUR_ID")
            str_Salaryacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPAYABLE_ACC_ID")
        Else
            lblError.TabIndex = "Provision code not set"
            Exit Sub
        End If
        gdsSysInfo.Tables.Clear() 'GET EEXGRATE WRT CORP.OFFICE

        str_Sql = "SELECT EXG_RATE " _
        & " FROM EXGRATE_S WHERE EXG_BSU_ID='" & Session("sBsuid") & "'" _
        & " AND EXG_CUR_ID='" & str_CorCurid & "' AND '" & txtDocdate.Text & "' " _
        & " BETWEEN EXG_FDATE AND ISNULL(EXG_TDATE,GETDATE())"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasisFin, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Exgrate = gdsSysInfo.Tables(0).Rows(0)("EXG_RATE")
        Else
            lblError.TabIndex = "Provision code not set"
            Exit Sub
        End If
        ' No checking for AX account balance
        'If rbCash.Checked = True Then
        '    'If AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtDocdate.Text.Trim) < CDbl(txtAmount.Text) Then
        '    '    lblError.Text = "There is not Enough Balance in the Account"
        '    '    Exit Sub
        '    'End If
        'End If

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim SqlCmd As New SqlCommand("FIN.SaveVOUCHER_H", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
            sqlpGUID.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpGUID)
            SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
            SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
            If rbBank.Checked = True Then
                SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "BP")
                SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChequedate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
                If rbCheque.Checked = True Then
                    SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", Convert.ToInt32(hCheqBook.Value))
                End If
                If rbOthers.Checked = True Then
                    SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", 0)
                End If
            Else
                SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "CP")
                SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtCashAcc.Text)
            End If
            SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_REFNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "P")
            SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtDocdate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAuto", True)
            SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", str_Provacc)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", str_Exgrate)
            SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAdvance", False)

            SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
            'SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", "")
            'V1.2 starts
            If rbCash.Checked = True Then
                SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", "CASH")
            Else
                SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txtReceivedby.Text)
            End If
            'V1.2 ends

            Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
            sqlpJHD_TIMESTAMP.Value = System.DBNull.Value

            SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
            SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
            SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            SqlCmd.Parameters.AddWithValue("@bEdit", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)

            SqlCmd.Parameters.AddWithValue("@VHH_bNoEDit", True) 'V1.2
            If (Trbearer.Visible = True And rbBank.Checked = True) Then
                SqlCmd.Parameters.AddWithValue("@VHH_BBearer", chkBearer.Checked) 'V1.5
            End If
            If rbBank.Checked = True Then

                If chkFFS.Checked = True Then
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "PAY FINAL SET BP")
                Else
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "PAY SAL BANK TRF")
                End If
            Else

                If chkFFS.Checked = True Then
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "PAY FINAL SET CP")
                Else
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "PAY SAL CASH")
                End If
            End If

            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            str_err = CInt(SqlCmd.Parameters("@ReturnValue").Value)

            lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)

            SqlCmd.Parameters.Clear()

            If (str_err = 0) Then

                Dim iBAnkLineno As Integer = 1

                '''''''chq
                'ldrNew("ChqBookId") = lstrChqDet.Split("|")(0) chbid
                'ldrNew("ChqBookLot") = lstrChqDet.Split("|")(1) bklot
                'ldrNew("ChqNo") = lstrChqDet.Split("|")(2) bkno
                Dim lstrChqDet, lstrChqBookId, lstrChqBookLotNo As String
                Dim lintChqs, lintMaxChqs As Integer
                lstrChqBookId = hCheqBook.Value
                Dim gvDetails As New GridView
                If rbBankTran.Checked Then
                    gvDetails = gvDetailsBank
                ElseIf rbCashTran.Checked Then
                    gvDetails = gvDetailsCash

                End If
                Dim cmd As New SqlCommand
                Dim iReturnvalue As Integer

                If (rbBank.Checked = True And rbCheque.Checked = True) Then
                    lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False, stTrans, txtChqNo.Text)
                    lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
                    lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
                    lstrChqBookId = lstrChqDet.Split("|")(2)
                    lstrChqBookLotNo = lstrChqDet.Split("|")(3)

                    lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), iBAnkLineno, False, stTrans, txtChqNo.Text)
                    If lstrChqDet = "" Then
                        lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), iBAnkLineno, True, stTrans, txtChqNo.Text)
                    ElseIf lstrChqDet = "INSUFFICIENT LOT" Then
                        lblError.Text = "Insufficient ChqBook Lot(s)"
                        Exit Sub
                    End If
                    If lstrChqDet = "" Or lstrChqDet = "INSUFFICIENT LOT" Then
                        lblError.Text = "Insufficient ChqBook Lot(s)"
                        Exit Sub
                    End If
                End If

                '----------------------------Save Voucher_D  single row----------------
                cmd = New SqlCommand("FIN.SaveVOUCHER_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@GUID", System.DBNull.Value)
                cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
                cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
                cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
                If rbBank.Checked = True Then
                    cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "BP")
                    cmd.Parameters.AddWithValue("@VHD_CHQDT", txtChequedate.Text)
                    If rbCheque.Checked = True Then
                        cmd.Parameters.AddWithValue("@VHD_CHQID", lstrChqDet.Split("|")(0))
                        cmd.Parameters.AddWithValue("@VHD_CHQNO", lstrChqDet.Split("|")(2))
                        cmd.Parameters.AddWithValue("@VHD_bCheque", True)

                    Else
                        cmd.Parameters.AddWithValue("@VHD_CHQID", 0)
                        cmd.Parameters.AddWithValue("@VHD_CHQNO", txtrefChequeno.Text)
                        cmd.Parameters.AddWithValue("@VHD_bCheque", False)
                    End If
                Else
                    cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "CP")
                    cmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
                    cmd.Parameters.AddWithValue("@VHD_CHQNO", System.DBNull.Value)
                    cmd.Parameters.AddWithValue("@VHD_CHQDT", Today.Date)
                    cmd.Parameters.AddWithValue("@VHD_bCheque", False)
                End If
                cmd.Parameters.AddWithValue("@VHD_DOCNO", lstrNewDocNo)
                cmd.Parameters.AddWithValue("@VHD_LINEID", 1)
                cmd.Parameters.AddWithValue("@VHD_ACT_ID", str_Salaryacc)
                cmd.Parameters.AddWithValue("@VHD_AMOUNT", txtAmount.Text)
                cmd.Parameters.AddWithValue("@VHD_NARRATION", txtNarrn.Text)


                cmd.Parameters.AddWithValue("@VHD_RSS_ID", str_Cashflow)
                cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
                cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
                cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
                cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
                cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)

                cmd.Parameters.AddWithValue("@VHD_COL_ID", 0)
                cmd.Parameters.AddWithValue("@bEdit", False)
                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

                cmd.Parameters.Clear()
                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                    Exit Sub
                End If

                '-----------------------
                For Each gvr As GridViewRow In gvDetails.Rows

                    Dim cb As HtmlInputCheckBox = New HtmlInputCheckBox
                    Dim txtAmount_D As TextBox = New TextBox

                    If rbCashTran.Checked Then
                        cb = CType(gvr.FindControl("chkESD_CASH"), HtmlInputCheckBox)
                        txtAmount_D = CType(gvr.FindControl("txtAmountCash"), TextBox)
                    End If
                    '''''''''
                    If cb.Checked And txtAmount_D.Text <> 0 Then
                        If (rbBank.Checked = True And rbCheque.Checked = True) Then
                            str_err = DoTransactionsBankforCashTransfer(objConn, stTrans, lstrNewDocNo, 1, _
                            str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                            lstrChqDet.Split("|")(0), lstrChqDet.Split("|")(2), str_Salaryacc)
                            If str_err <> 0 Then
                                lblError.Text = getErrorMessage(str_err)
                                stTrans.Rollback()
                                Exit Sub
                            End If
                        End If
                        If (rbBank.Checked = True And rbOthers.Checked = True) Then
                            str_err = DoTransactionsBankforCashTransfer(objConn, stTrans, lstrNewDocNo, 1, _
                                                       str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                                                       "0", txtrefChequeno.Text, str_Salaryacc)
                            If str_err <> 0 Then
                                lblError.Text = getErrorMessage(str_err)
                                stTrans.Rollback()
                                Exit Sub
                            End If
                        End If
                        If rbCash.Checked = True Then
                            str_err = DoTransactionsBankforCashTransfer(objConn, stTrans, lstrNewDocNo, 1, _
                                str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                                 DBNull.Value.ToString, DBNull.Value.ToString, str_Salaryacc)

                            If str_err <> 0 Then
                                lblError.Text = getErrorMessage(str_err)
                                stTrans.Rollback()
                                Exit Sub
                            End If
                        End If
                    End If
                    iBAnkLineno = iBAnkLineno + 1

                Next
                str_err = post_voucher(lstrNewDocNo, stTrans, objConn)
                If str_err = "0" Then   'V1.4
                    If h_Emp_No.Value <> "" Then
                        str_err = CheckJVCreation(objConn, stTrans, lstrNewDocNo)
                    End If

                Else
                    lblError.Text = getErrorMessage(str_err)
                    stTrans.Rollback()
                End If
                If str_err = "0" Then

                    ViewState("strDocDate") = txtDocdate.Text
                    clear_All()
                    btnPrint.Visible = True
                    ViewState("lstrNewDocNo") = lstrNewDocNo
                    str_err = PostToPayrollAXStaging(objConn, stTrans, lstrNewDocNo) 'V2.0
                    If str_err = "0" Then
                        stTrans.Commit()
                    Else
                        lblError.Text = getErrorMessage(str_err)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                    'stTrans.Rollback()
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtDocdate.Text = GetDiplayDate()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", _
                    Page.User.Identity.Name.ToString, Me.Page)
                    lblError.Text = "Data Successfully Saved..." + " Reference Doc No.:" + lstrNewDocNo
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Else
                    lblError.Text = getErrorMessage(str_err)
                    stTrans.Rollback()
                End If
                Else
                    lblError.Text = getErrorMessage(str_err)
                    stTrans.Rollback()
                End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try

    End Sub
    ''' <summary>
    ''' New function to generate JV if final settlement ---- V1.4
    ''' </summary>
    ''' <param name="objConn"></param>
    ''' <param name="stTrans"></param>
    ''' <param name="RefDocNo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckJVCreation(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal RefDocNo As String) As Integer
        Dim iReturnvalue As Integer = 0
        Try

            If h_Emp_No.Value <> "" Then
                Dim cmd As New SqlCommand("oasis.dbo.CheckFFforJVCreation", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpJHD_Month As New SqlParameter("@PAyMonth", SqlDbType.VarChar)
                sqlpJHD_Month.Value = Session("monthselected")
                cmd.Parameters.Add(sqlpJHD_Month)

                Dim sqlpJHD_Year As New SqlParameter("@Payyear", SqlDbType.VarChar)
                sqlpJHD_Year.Value = Session("yearselected")
                cmd.Parameters.Add(sqlpJHD_Year)

                Dim sqlpJHD_BSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                sqlpJHD_BSUID.Value = Session("sBsuid")
                cmd.Parameters.Add(sqlpJHD_BSUID)


                Dim sqlpJHD_DocDate As New SqlParameter("@DOCDT", SqlDbType.DateTime)
                sqlpJHD_DocDate.Value = txtDocdate.Text
                cmd.Parameters.Add(sqlpJHD_DocDate)


                Dim sqlpJHD_EMPID As New SqlParameter("@EMPID", SqlDbType.VarChar, 20)
                sqlpJHD_EMPID.Value = h_Emp_No.Value
                cmd.Parameters.Add(sqlpJHD_EMPID)

                Dim sqlpJHD_DocNo As New SqlParameter("@ESD_REFDOCNO", SqlDbType.VarChar, 20)
                sqlpJHD_DocNo.Value = RefDocNo
                cmd.Parameters.Add(sqlpJHD_DocNo)

                Dim sqlpJHD_Narration As New SqlParameter("@Narration", SqlDbType.VarChar, 200)
                sqlpJHD_Narration.Value = txtNarrn.Text
                cmd.Parameters.Add(sqlpJHD_Narration)


                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
            End If

            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
            Return iReturnvalue
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
            Return 1000
        End Try


    End Function

    ''New function- common for salary transfer Cash-- Cash payment/Bank payment
    Private Function DoTransactionsBankforCashTransfer(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_Exgrate1 As String, ByVal p_Exgrate2 As String, _
      ByVal p_Bankid As String, ByVal p_Lineid As String, _
      ByVal p_Amount As Decimal, ByVal p_Cashflow As String, _
      ByVal p_Chbid As String, ByVal p_Chqno As String, ByVal str_Salaryacc As String) As String

        Dim iReturnvalue As Integer
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim cmdUpdate As New SqlCommand
            Dim str_err As String = ""
            Dim dTotal As Double = 0
            Dim iEbtID As Integer = 0
            If rbBank.Checked = True Then
                iReturnvalue = PayrollFunctions.SaveEMPSALARYTRF_D(p_Bankid, txtBankCode.Text, _
                          p_Chbid, p_Chqno, txtDocdate.Text, p_Amount, p_docno, txtNarrn.Text, _
                          False, iEbtID, "BP", stTrans, Session("sBsuid"), ddlPurpose.SelectedValue, txtValueDate.Text)
            End If

            If rbCash.Checked = True Then
                iReturnvalue = PayrollFunctions.SaveEMPSALARYTRF_D(0, txtCashAcc.Text, _
                          "", "", txtDocdate.Text, p_Amount, p_docno, txtNarrn.Text, _
                          False, iEbtID, "CP", stTrans, Session("sBsuid"), ddlPurpose.SelectedValue, txtValueDate.Text)
            End If

            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
            '''''update empsal_d here 
            Dim strFFS As String = " AND isnull(ESD_ERD_ID,0)=0 "
            If chkFFS.Checked = True Then
                strFFS = " AND isnull(ESD_ERD_ID,0)>0 "
            End If

            Dim str_sql As String = ""
            str_sql = "SELECT ESD.ESD_EMP_ID, ESD.ESD_EARN_NET, " _
                & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+" _
                & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
                & " FROM  " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D AS ESD INNER JOIN " _
                & OASISConstants.dbPayroll & ".DBO.EMPLOYEE_M AS EM ON ESD.ESD_EMP_ID = EM.EMP_ID" _
                & " where ESD.ESD_BSU_ID='" & Session("sBsuid") & "' AND " _
                & " ESD.ESD_MONTH in (" & Session("monthselected") & ") AND " _
                & " ESD.ESD_YEAR in(" & Session("yearselected") & ")" _
                & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM " & OASISConstants.dbPayroll & ".DBO.EMPSALHOLD_D WHERE (EHD_bHold = 1) )" _
                & " AND IsNull(ESD_MODE,0)=0  and ESD_PAid=0 and ESD_ID='" & p_Bankid & "' AND IsNull(ESD.ESD_BProcessWPS,0)=0 " & strFFS


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_sql)


            str_sql = "update " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D set   ESD_REFDOCNO='" & p_docno & "', " _
            & " ESD_DATE ='" & txtDocdate.Text & "', ESD_EBT_ID='" & iEbtID & "', ESD_PAID =1  " _
            & " where ESD_BSU_ID='" & Session("sBsuid") & "' AND " _
            & " ESD_MONTH in (" & Session("monthselected") & ") AND " _
            & " ESD_YEAR in(" & Session("yearselected") & ") " _
            & " AND ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM " & OASISConstants.dbPayroll & ".DBO.EMPSALHOLD_D WHERE (EHD_bHold = 1) )" _
            & " AND IsNull(ESD_MODE,0) = 0 and ESD_PAid=0 and ESD_ID='" & p_Bankid & "'" & strFFS


            cmdUpdate = New SqlCommand(str_sql, objConn, stTrans)
            cmdUpdate.CommandType = CommandType.Text
            cmdUpdate.ExecuteNonQuery()

            Dim str_crdb As String = "DR"
            '''''update empsal_d here


            '______________________Do_transactions_sub_Table()



            'Adding transaction info


            For iVDSIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmd.Dispose()
                cmd = New SqlCommand("FIN.SaveVOUCHER_D_S", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                sqlpGUID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpJDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                sqlpJDS_ID.Value = p_Lineid
                cmd.Parameters.Add(sqlpJDS_ID)

                Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                cmd.Parameters.Add(sqlpJDS_SUB_ID)

                Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpJDS_BSU_ID)

                Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJDS_FYEAR)

                Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                If rbBank.Checked = True Then
                    sqlpJDS_DOCTYPE.Value = "BP"
                Else
                    sqlpJDS_DOCTYPE.Value = "CP"
                End If
                cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpJDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpJDS_DOCNO)

                Dim sqlpJDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                sqlpJDS_DOCDT.Value = txtDocdate.Text & ""
                cmd.Parameters.Add(sqlpJDS_DOCDT)

                Dim sqlpJDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                sqlpJDS_ACT_ID.Value = str_Salaryacc
                cmd.Parameters.Add(sqlpJDS_ACT_ID)

                Dim sqlpbJDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                sqlpbJDS_SLNO.Value = p_Lineid
                cmd.Parameters.Add(sqlpbJDS_SLNO)

                Dim sqlpJDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                sqlpJDS_AMOUNT.Value = ds.Tables(0).Rows(iVDSIndex)("ESD_EARN_NET")
                cmd.Parameters.Add(sqlpJDS_AMOUNT)

                dTotal = dTotal + ds.Tables(0).Rows(iVDSIndex)("ESD_EARN_NET")

                Dim sqlpJDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                'sqlpJDS_CCS_ID.Value = "EMP"       'V1.1
                sqlpJDS_CCS_ID.Value = "0001"       'VDS_CCS_ID for EMPLOYEES
                cmd.Parameters.Add(sqlpJDS_CCS_ID)

                Dim sqlpJDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                sqlpJDS_CODE.Value = ds.Tables(0).Rows(iVDSIndex)("ESD_EMP_ID")
                cmd.Parameters.Add(sqlpJDS_CODE)

                Dim sqlpJDS_Descr As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                sqlpJDS_Descr.Value = ds.Tables(0).Rows(iVDSIndex)("EMP_NAME")
                cmd.Parameters.Add(sqlpJDS_Descr)

                Dim sqlpbJDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                sqlpbJDS_DRCR.Value = str_crdb
                cmd.Parameters.Add(sqlpbJDS_DRCR)

                Dim sqlpJDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                sqlpJDS_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpJDS_bPOSTED)

                Dim sqlpbJDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                sqlpbJDS_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbJDS_BDELETED)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                sqlpbEdit.Value = False
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    Return "1000"
                End If
                cmd.Parameters.Clear()
            Next
            If dTotal <> p_Amount Then
                Return "1000"
            End If
            Return iReturnvalue




            '________________________________


            '' '' 

        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try

    End Function

    Protected Sub Save_voucherBank()

        Dim lstrNewDocNo As String

        Dim str_connOasisFin As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_connOasis As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim objConn As New SqlConnection(str_connOasisFin)
        Dim str_connOasisDAX As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_connOasisDAX)

        Dim gdsSysInfo As New DataSet
        Dim str_err As String = ""

        Dim str_Provacc As String = ""
        Dim str_Salaryacc As String = ""
        Dim str_Cashflow As String = ""
        Dim str_Exgrate As Decimal
        Dim str_CorCurid As String = ""
        Dim str_Sql As String
        str_Sql = " SELECT SYS_SALARYPDCPROVACC, SYS_SALARYCASHFLOW,SYS_CUR_ID,SYS_SALARYPAYABLE_ACC_ID From vw_OSF_SYSINFO_S"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasis, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Provacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPDCPROVACC")
            str_Cashflow = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYCASHFLOW")
            str_CorCurid = gdsSysInfo.Tables(0).Rows(0)("SYS_CUR_ID")
            str_Salaryacc = gdsSysInfo.Tables(0).Rows(0)("SYS_SALARYPAYABLE_ACC_ID")
        Else
            lblError.TabIndex = "Provision code not set"
            Exit Sub
        End If
        gdsSysInfo.Tables.Clear() 'GET EEXGRATE WRT CORP.OFFICE

        str_Sql = "SELECT EXG_RATE " _
        & " FROM EXGRATE_S WHERE EXG_BSU_ID='" & Session("sBsuid") & "'" _
        & " AND EXG_CUR_ID='" & str_CorCurid & "' AND '" & txtDocdate.Text & "' " _
        & " BETWEEN EXG_FDATE AND ISNULL(EXG_TDATE,GETDATE())"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_connOasisFin, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            str_Exgrate = gdsSysInfo.Tables(0).Rows(0)("EXG_RATE")
        Else
            lblError.TabIndex = "Provision code not set"
            Exit Sub
        End If
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim SqlCmd As New SqlCommand("FIN.SaveVOUCHER_H", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
            sqlpGUID.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpGUID)
            SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
            SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
            SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "BP")
            SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_REFNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "P")
            If rbCheque.Checked = True Then
                SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", Convert.ToInt32(hCheqBook.Value))
            End If
            If rbOthers.Checked = True Then
                SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", 0)
            End If

            SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtDocdate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChequedate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAuto", True)
            SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", str_Provacc)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", str_Exgrate)
            SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAdvance", False)

            SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
            SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txtReceivedby.Text)

            Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
            sqlpJHD_TIMESTAMP.Value = System.DBNull.Value

            SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
            SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
            SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            SqlCmd.Parameters.AddWithValue("@bEdit", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)

            SqlCmd.Parameters.AddWithValue("@VHH_bNoEDit", True) 'V1.2
            If (Trbearer.Visible = True And rbBank.Checked = True) Then
                SqlCmd.Parameters.AddWithValue("@VHH_BBearer", chkBearer.Checked) 'V1.5
            End If
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            If chkFFS.Checked = True Then
                SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "PAY FINAL SET BP")
            Else
                SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "PAY SAL BANK TRF")
            End If

            SqlCmd.ExecuteNonQuery()
            str_err = CInt(SqlCmd.Parameters("@ReturnValue").Value)

            lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)

            SqlCmd.Parameters.Clear()

            If (str_err = 0) Then

                Dim iBAnkLineno As Integer = 1

                '''''''chq
                'ldrNew("ChqBookId") = lstrChqDet.Split("|")(0) chbid
                'ldrNew("ChqBookLot") = lstrChqDet.Split("|")(1) bklot
                'ldrNew("ChqNo") = lstrChqDet.Split("|")(2) bkno
                Dim lstrChqDet, lstrChqBookId, lstrChqBookLotNo As String
                Dim lintChqs, lintMaxChqs As Integer
                lstrChqBookId = hCheqBook.Value
                Dim gvDetails As New GridView
                If rbBankTran.Checked Then
                    gvDetails = gvDetailsBank
                ElseIf rbCashTran.Checked Then
                    gvDetails = gvDetailsCash

                End If

                For Each gvr As GridViewRow In gvDetails.Rows

                    Dim cb As HtmlInputCheckBox = New HtmlInputCheckBox
                    Dim txtAmount_D As TextBox = New TextBox
                    If rbBankTran.Checked Then
                        cb = CType(gvr.FindControl("chkESD_BANK"), HtmlInputCheckBox)
                        txtAmount_D = CType(gvr.FindControl("txtAmountBank"), TextBox)
                    End If
                    If rbCashTran.Checked Then
                        cb = CType(gvr.FindControl("chkESD_CASH"), HtmlInputCheckBox)
                        txtAmount_D = CType(gvr.FindControl("txtAmountCash"), TextBox)
                    End If
                    '''''''''

                    If cb.Checked Then
                        If rbCheque.Checked = True Then
                            lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False, stTrans, txtChqNo.Text)
                            lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
                            lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
                            lstrChqBookId = lstrChqDet.Split("|")(2)
                            lstrChqBookLotNo = lstrChqDet.Split("|")(3)

                            lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), iBAnkLineno, False, stTrans, txtChqNo.Text)
                            If lstrChqDet = "" Then
                                lstrChqDet = GetNextChqNo(Convert.ToInt32(hCheqBook.Value), iBAnkLineno, True, stTrans, txtChqNo.Text)
                            ElseIf lstrChqDet = "INSUFFICIENT LOT" Then
                                lblError.Text = "Insufficient ChqBook Lot(s)"
                                Exit Sub
                            End If
                            If lstrChqDet = "" Or lstrChqDet = "INSUFFICIENT LOT" Then
                                lblError.Text = "Insufficient ChqBook Lot(s)"
                                Exit Sub
                            End If



                            If txtAmount_D.Text <> 0 Then
                                str_err = DoTransactionsBank(objConn, stTrans, lstrNewDocNo, 1, _
                                str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                                lstrChqDet.Split("|")(0), lstrChqDet.Split("|")(2), str_Salaryacc)

                                If str_err <> 0 Then
                                    lblError.Text = getErrorMessage(str_err)
                                    stTrans.Rollback()
                                    Exit Sub
                                End If
                            End If
                        End If

                        If rbOthers.Checked = True Then
                            If txtAmount_D.Text <> 0 Then
                                str_err = DoTransactionsBank(objConn, stTrans, lstrNewDocNo, 1, _
                                                           str_Exgrate, cb.Value, iBAnkLineno, txtAmount_D.Text, str_Cashflow, _
                                                           "0", txtrefChequeno.Text, str_Salaryacc)
                            End If
                            If str_err <> 0 Then
                                lblError.Text = getErrorMessage(str_err)
                                stTrans.Rollback()
                                Exit Sub
                            End If
                        End If
                        iBAnkLineno = iBAnkLineno + 1
                    End If

                    If str_err <> 0 Then
                        lblError.Text = getErrorMessage(str_err)
                        stTrans.Rollback()
                        Exit Sub
                    End If

                Next
                If rbCashTran.Checked = True And rbBank.Checked = True Then

                End If
                str_err = post_voucher(lstrNewDocNo, stTrans, objConn)
                If str_err = "0" Then 'V1.4
                    If h_Emp_No.Value <> "" Then
                        str_err = CheckJVCreation(objConn, stTrans, lstrNewDocNo)
                    End If
                Else
                    lblError.Text = getErrorMessage(str_err)
                    stTrans.Rollback()
                End If

                If str_err = "0" Then
                    ViewState("strDocDate") = txtDocdate.Text
                    clear_All()
                    btnPrint.Visible = True
                    ViewState("lstrNewDocNo") = lstrNewDocNo
                    str_err = PostToPayrollAXStaging(objConn, stTrans, lstrNewDocNo) 'V2.0
                    If str_err = "0" Then
                        stTrans.Commit()
                    Else
                        lblError.Text = getErrorMessage(str_err)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                    ' stTrans.Commit()
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    txtDocdate.Text = GetDiplayDate()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", _
                    Page.User.Identity.Name.ToString, Me.Page)
                    lblError.Text = "Data Successfully Saved..." + " Reference Doc No.:" + lstrNewDocNo
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Else
                    lblError.Text = getErrorMessage(str_err)
                    stTrans.Rollback()
                End If
            Else
                lblError.Text = getErrorMessage(str_err)
                stTrans.Rollback()
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
    End Sub
    Private Function DoTransactionsBank(ByVal objConn As SqlConnection, _
       ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
       ByVal p_Exgrate1 As String, ByVal p_Exgrate2 As String, _
       ByVal p_Bankid As String, ByVal p_Lineid As String, _
       ByVal p_Amount As Decimal, ByVal p_Cashflow As String, _
       ByVal p_Chbid As String, ByVal p_Chqno As String, ByVal str_Salaryacc As String) As String

        Dim iReturnvalue As Integer
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim cmdUpdate As New SqlCommand
            Dim str_err As String = ""
            Dim dTotal As Double = 0
            Dim iEbtID As Integer = 0
            If rbBankTran.Checked = True Then
                iReturnvalue = PayrollFunctions.SaveEMPSALARYTRF_D(p_Bankid, txtBankCode.Text, _
                          p_Chbid, p_Chqno, txtDocdate.Text, p_Amount, p_docno, txtNarrn.Text, _
                          False, iEbtID, "BP", stTrans, Session("sBsuid"), ddlPurpose.SelectedValue, txtValueDate.Text)
            End If

            If rbCashTran.Checked Then
                If rbCash.Checked = True Then
                    iReturnvalue = PayrollFunctions.SaveEMPSALARYTRF_D(0, txtCashAcc.Text, _
                              "", "", txtDocdate.Text, p_Amount, p_docno, txtNarrn.Text, _
                              False, iEbtID, "BP", stTrans, Session("sBsuid"), ddlPurpose.SelectedValue, txtValueDate.Text)
                Else
                    iReturnvalue = PayrollFunctions.SaveEMPSALARYTRF_D(0, txtCashAcc.Text, _
                         p_Chbid, p_Chqno, txtDocdate.Text, p_Amount, p_docno, txtNarrn.Text, _
                         False, iEbtID, "BP", stTrans, Session("sBsuid"), ddlPurpose.SelectedValue, txtValueDate.Text)
                End If
            End If

            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If

            Dim str_crdb As String = "DR"
            str_err = DoTransactions_Sub_Table_Bank(objConn, stTrans, p_docno, _
            str_crdb, p_Lineid, str_Salaryacc, p_Amount, txtDocdate.Text, p_Bankid)
            If str_err <> "0" Then
                Return str_err
            End If
            '' '' 

            '''''update empsal_d here 
            Dim str_sql As String = ""
            Dim strFFS As String = " AND isnull(esd.ESD_ERD_ID,0)=0 "
            If chkFFS.Checked = True Then
                strFFS = " AND isnull(esd.ESD_ERD_ID,0)>0 "
            End If
            str_sql = "update  esd  set   ESD_REFDOCNO='" & p_docno & "', " _
            & " ESD_DATE ='" & txtDocdate.Text & "', ESD_EBT_ID='" & iEbtID & "', ESD_PAID =1 from " _
            & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D esd inner join OASIS..employee_m em on em.emp_id=esd.esd_emp_id" _
            & " where ESD_BSU_ID='" & Session("sBsuid") & "' AND " _
            & " ESD_MONTH in (" & Session("monthselected") & ") AND    ESD_PAid=0 and " _
            & " ESD_YEAR in(" & Session("yearselected") & ") " & strFFS _
            & " AND ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM " & OASISConstants.dbPayroll & ".DBO.EMPSALHOLD_D WHERE (EHD_bHold = 1) )"
            If h_Emp_No.Value <> "" Then 'V1.4
                str_sql = str_sql + " AND ESD_EMP_ID= " & h_Emp_No.Value
            End If
            'V1.2 addition
            'If ViewState("MainMnu_code") = "P130170" Then   'v1.1  
            If rbBankTran.Checked = True Then
                str_sql = str_sql & " AND IsNull(ESD_MODE,0)=1 AND ESD_BANK='" & p_Bankid & "'"
            End If
            If rbCashTran.Checked = True Then
                str_sql = str_sql & " AND IsNull(ESD_MODE,0) = 0 and ESD_ID='" & p_Bankid & "'"
            End If
            str_sql = str_sql & "AND IsNull(ESD_BProcessWPS,0)=0  "
            'ElseIf ViewState("MainMnu_code") = "P130171" Then    'Not considering WPS processed.
            'str_sql = str_sql & " AND IsNull(ESD_BProcessWPS,0)=1  "
            'End If
            'Modification for category selection
            If hcatFilter.Value <> "" Then
                str_sql = str_sql & "AND IsNull(EM.EMP_ABC,'0') in (" & hcatFilter.Value & ")"
            End If
            cmdUpdate = New SqlCommand(str_sql, objConn, stTrans)
            cmdUpdate.CommandType = CommandType.Text
            cmdUpdate.ExecuteNonQuery()


            '''''update empsal_d here
            cmd = New SqlCommand("FIN.SaveVOUCHER_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@GUID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
            cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
            cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "BP")
            cmd.Parameters.AddWithValue("@VHD_DOCNO", p_docno)
            cmd.Parameters.AddWithValue("@VHD_LINEID", p_Lineid)
            cmd.Parameters.AddWithValue("@VHD_ACT_ID", str_Salaryacc)
            cmd.Parameters.AddWithValue("@VHD_AMOUNT", p_Amount)
            cmd.Parameters.AddWithValue("@VHD_NARRATION", txtNarrn.Text)
            'Coditions added based on adding cheque or instrument-----------------------
            cmd.Parameters.AddWithValue("@VHD_CHQID", p_Chbid)
            cmd.Parameters.AddWithValue("@VHD_CHQNO", p_Chqno)
            cmd.Parameters.AddWithValue("@VHD_CHQDT", txtChequedate.Text)
            cmd.Parameters.AddWithValue("@VHD_RSS_ID", p_Cashflow)
            cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
            cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
            cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
            cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
            cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
            If rbCheque.Checked = True Then
                cmd.Parameters.AddWithValue("@VHD_bCheque", True)
            End If
            If rbOthers.Checked = True Then
                cmd.Parameters.AddWithValue("@VHD_bCheque", False)
            End If
            cmd.Parameters.AddWithValue("@VHD_COL_ID", 0)
            cmd.Parameters.AddWithValue("@bEdit", False)
            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

            cmd.Parameters.Clear()
            Return iReturnvalue
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function

    Function DoTransactions_Sub_Table_Bank(ByVal objConn As SqlConnection, _
        ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
        ByVal p_crdr As String, ByVal p_slno As Integer, ByVal p_accountid As String, _
        ByVal p_amount As String, ByVal p_date As String, ByVal p_Bankid As String) As String

        Dim iReturnvalue As Integer
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim dTotal As Double = 0
            Dim str_sql As String

            'Adding category filter
            Dim str_filter As String = ""
            Dim strA As String = ""
            Dim strB As String = ""
            Dim strC As String = ""
            If chkABC.Items.FindByValue("A").Selected = True Then
                strA = "A"
            End If
            If chkABC.Items.FindByValue("B").Selected = True Then
                strB = "B"
            End If
            If chkABC.Items.FindByValue("C").Selected = True Then
                strC = "C"
            End If
            If strA <> "" Then
                str_filter = "'" + strA + "'"  '+ strB + "," + strC
            End If
            If strB <> "" Then
                If str_filter <> "" Then
                    str_filter = str_filter + ","
                End If
                str_filter = str_filter + "'" + strB + "'"
            End If
            If strC <> "" Then
                If str_filter <> "" Then
                    str_filter = str_filter + ","
                End If
                str_filter = str_filter + "'" + strC + "'"
            End If
            'str_filter = strA + "," + strB + "," + strC
            hcatFilter.Value = str_filter

            If str_filter <> "" Then
                str_filter = "AND IsNull(EM.EMP_ABC,'0') in (" & str_filter & ")"
            End If
            Dim strFFS As String = " AND isnull(ESD.ESD_ERD_ID,0)=0 "
            If chkFFS.Checked = True Then
                strFFS = " AND isnull(ESD.ESD_ERD_ID,0)>0 "
            End If

            str_sql = "SELECT ESD.ESD_EMP_ID, ESD.ESD_EARN_NET, " _
                & " ISNULL(EM.EMP_FNAME,'')+' '+ ISNULL(EM.EMP_MNAME,'')+' '+" _
                & " ISNULL(EM.EMP_LNAME,'') AS EMP_NAME" _
                & " FROM  " & OASISConstants.dbPayroll & ".DBO.EMPSALARYDATA_D AS ESD INNER JOIN " _
                & OASISConstants.dbPayroll & ".DBO.EMPLOYEE_M AS EM ON ESD.ESD_EMP_ID = EM.EMP_ID" _
                & " where ESD.ESD_BSU_ID='" & Session("sBsuid") & "' AND ESD_PAID=0 and " _
                & " ESD.ESD_MONTH in (" & Session("monthselected") & ") AND " _
                & " ESD.ESD_YEAR in(" & Session("yearselected") & ")" _
                & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM " & OASISConstants.dbPayroll & ".DBO.EMPSALHOLD_D WHERE (EHD_bHold = 1) )" _
                & str_filter & strFFS
            If h_Emp_No.Value <> "" Then  'V1.4
                str_sql = str_sql + " AND ESD_EMP_ID= " & h_Emp_No.Value
            End If
            'V1.2 addition
            If rbBankTran.Checked = True Then
                str_sql = str_sql & " AND IsNull(ESD_MODE,0)=1 AND ESD_BANK='" & p_Bankid & "'"
            End If
            If rbCashTran.Checked = True Then
                str_sql = str_sql & " AND IsNull(ESD_MODE,0)=0 and ESD_ID='" & p_Bankid & "'"
            End If

            ' If ViewState("MainMnu_code") = "P130170" Then       'avoiding WPS records
            str_sql = str_sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=0  "
            'ElseIf ViewState("MainMnu_code") = "P130171" Then      
            'str_sql = str_sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=1  "
            'End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_sql)

            For iVDSIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmd.Dispose()
                cmd = New SqlCommand("FIN.SaveVOUCHER_D_S", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                sqlpGUID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpJDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                sqlpJDS_ID.Value = p_slno
                cmd.Parameters.Add(sqlpJDS_ID)

                Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
                cmd.Parameters.Add(sqlpJDS_SUB_ID)

                Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpJDS_BSU_ID)

                Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJDS_FYEAR)

                Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                sqlpJDS_DOCTYPE.Value = "BP"
                cmd.Parameters.Add(sqlpJDS_DOCTYPE)

                Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpJDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpJDS_DOCNO)

                Dim sqlpJDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                sqlpJDS_DOCDT.Value = txtDocdate.Text & ""
                cmd.Parameters.Add(sqlpJDS_DOCDT)

                Dim sqlpJDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                sqlpJDS_ACT_ID.Value = p_accountid
                cmd.Parameters.Add(sqlpJDS_ACT_ID)

                Dim sqlpbJDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                sqlpbJDS_SLNO.Value = p_slno
                cmd.Parameters.Add(sqlpbJDS_SLNO)

                Dim sqlpJDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                sqlpJDS_AMOUNT.Value = ds.Tables(0).Rows(iVDSIndex)("ESD_EARN_NET")
                cmd.Parameters.Add(sqlpJDS_AMOUNT)

                dTotal = dTotal + ds.Tables(0).Rows(iVDSIndex)("ESD_EARN_NET")

                Dim sqlpJDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                'sqlpJDS_CCS_ID.Value = "EMP"       'V1.1
                sqlpJDS_CCS_ID.Value = "0001"       'VDS_CCS_ID for EMPLOYEES
                cmd.Parameters.Add(sqlpJDS_CCS_ID)

                Dim sqlpJDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                sqlpJDS_CODE.Value = ds.Tables(0).Rows(iVDSIndex)("ESD_EMP_ID")
                cmd.Parameters.Add(sqlpJDS_CODE)

                Dim sqlpJDS_Descr As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                sqlpJDS_Descr.Value = ds.Tables(0).Rows(iVDSIndex)("EMP_NAME")
                cmd.Parameters.Add(sqlpJDS_Descr)

                Dim sqlpbJDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                sqlpbJDS_DRCR.Value = p_crdr
                cmd.Parameters.Add(sqlpbJDS_DRCR)

                Dim sqlpJDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                sqlpJDS_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpJDS_bPOSTED)

                Dim sqlpbJDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                sqlpbJDS_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbJDS_BDELETED)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                sqlpbEdit.Value = False
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    Return "1000"
                End If
                cmd.Parameters.Clear()
            Next
            If dTotal <> p_amount Then
                Return "1000"
            End If
            Return iReturnvalue
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function

    Function post_voucher(ByVal p_Docno As String, ByVal stTrans As SqlTransaction, ByVal objConn As SqlConnection) As Integer
        Try
            Try                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim cmd As New SqlCommand("FIN.POSTVOUCHER", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 0

                Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid")
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = Session("F_YEAR")
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                If rbCash.Checked Then
                    sqlpJHD_DOCTYPE.Value = "CP"
                ElseIf rbBank.Checked Then
                    sqlpJHD_DOCTYPE.Value = "BP"     'V1.1
                End If
                cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                sqlpJHD_DOCNO.Value = p_Docno
                cmd.Parameters.Add(sqlpJHD_DOCNO)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                Return iReturnvalue
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                Errorlog(ex.Message)
                Return 1000
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Protected Sub txtAmount_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("readonly", "readonly")
        txtAmount.Attributes.Add("onFocus", "this.select();")
        txtAmount.Text = AccountFunctions.Round(txtAmount.Text)
        txtAmount.Attributes.CssStyle.Add("TEXT-ALIGN", "right")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub clear_All()
        txtDocdate.Text = GetDiplayDate()
        txtCashAcc.Text = ""
        txtCashDescr.Text = ""
        txtNarrn.Text = ""

        txtAmount.Text = ""

        clMonthYear.DataBind()
        clMonthYear.SelectedIndex = -1
        gvDetailsCash.DataBind()
        divCashGrid.Visible = False
        divCashGridhd.Visible = False
        txtBankCode.Text = ""
        txtBankDescr.Text = ""

        txtChequedate.Text = ""
        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtAmount.Text = ""
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")

        clMonthYear.DataBind()
        clMonthYear.SelectedIndex = -1
        gvDetailsBank.DataBind()
        divBank.Visible = False
        divbankHd.Visible = False
        txtrefChequeno.Text = ""
        txtEmpNo.Text = "" 'V1.4
        h_Emp_No.Value = ""
        imgDelete.Visible = False
        chkBearer.Checked = False
    End Sub

    Sub Set_CashorBankPayment()
        If rbCash.Checked = True Then
            tr_Bank.Visible = False
            tr_Cheque.Visible = False
            tr_ChqRef.Visible = False
            tr_Cash.Visible = True
            tr_cheque_Receivedby.Visible = False
            Trbearer.Visible = False
        End If
        If rbBank.Checked = True Then
            tr_Bank.Visible = True
            tr_Cheque.Visible = True
            tr_Cash.Visible = False
            tr_ChqRef.Visible = True
            txtrefChequeno.Text = ""
            txtrefChequeno.Visible = False
            lblRef.Visible = False
            'rbCheque.Checked = True
            rbOthers.Checked = True
            rbOthers_CheckedChanged(Nothing, Nothing)
            IMG1.Disabled = False
            IMG1.Visible = True
            txtChqBook.ReadOnly = False
            txtChqNo.ReadOnly = False
            tr_cheque_Receivedby.Visible = True
            Trbearer.Visible = True
        End If
    End Sub

    Sub setBankorCashTransfer()
        If rbBankTran.Checked Then
            trCash1.Visible = False
            'trCash2.Visible = False
            gvDetailsCash.Visible = False
            divCashGrid.Visible = False
            divCashGridhd.Visible = False
            'trBank1.Visible = True
            'trBank2.Visible = True
            'trBank3.Visible = True
            'trBank4.Visible = True
            If gvDetailsBank.Rows.Count > 0 Then
                gvDetailsBank.Visible = True
                divBank.Visible = False
                divbankHd.Visible = False

            End If
            trBank1.Visible = True
        End If
        If rbCashTran.Checked Then
            trBank1.Visible = False
            'trBank2.Visible = False
            'trBank3.Visible = False
            'trBank4.Visible = False
            gvDetailsBank.Visible = False
            divBank.Visible = False
            divbankHd.Visible = False
            trCash1.Visible = True
            'trCash1.Visible = True
            'trCash2.Visible = True
            If gvDetailsCash.Rows.Count > 0 Then
                gvDetailsCash.Visible = True
                divCashGrid.Visible = True
                divCashGridhd.Visible = True
            End If
        End If
    End Sub

    Sub set_Comboanddate()
        Dim strFFS As String = " AND isnull(ESD.ESD_ERD_ID,0)=0 "
        If chkFFS.Checked = True Then
            strFFS = " AND isnull(ESD.ESD_ERD_ID,0)>0 "
        End If
        Try
            If rbCashTran.Checked Then   'V1.1  changes to load data based on selected mode
                Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String

                str_Sql = "SELECT DISTINCT CAST(CAST(ESD_MONTH AS varchar(4)) + '/1' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME) as DT," _
                & " CAST(ESD_MONTH AS varchar(4)) + '_' + " _
                & " CAST(ESD_YEAR AS varchar(4)) AS month_year, " _
                & " LEFT( DATENAME(MONTH, CAST(CAST(ESD_MONTH AS varchar(4)) + " _
                & " '/22' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME)), 3)+" _
                & " '/'+CAST(ESD_YEAR AS varchar(4)) AS monthyear" _
                & " FROM EMPSALARYDATA_D AS ESD inner join Employee_m Emp on Emp.EMP_ID=ESD.ESD_EMP_ID" _
                & " WHERE (ESD_MODE = 0) AND (ESD_PAID = 0) and ESD_EARN_NET <>0  " _
                & " AND (ESD_BSU_ID = '" & Session("sBsuid") & "') " & strFFS _
                & " AND ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE (EHD_bHold = 1) ) ORDER BY DT"

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                clMonthYear.DataSource = ds.Tables(0)
                clMonthYear.DataTextField = "monthyear"
                clMonthYear.DataValueField = "month_year"
                clMonthYear.DataBind()
            End If
            If rbBankTran.Checked Then

                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String
                'V1.2 condition added
                str_Sql = "SELECT DISTINCT CAST(CAST(ESD_MONTH AS varchar(4)) + '/1' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME) as DT," _
                & " CAST(ESD_MONTH AS varchar(4)) + '_' + " _
                & " CAST(ESD_YEAR AS varchar(4)) AS month_year, " _
                & " LEFT( DATENAME(MONTH, CAST(CAST(ESD_MONTH AS varchar(4)) + " _
                & " '/22' + '/' + CAST(ESD_YEAR AS varchar(4)) AS DATETIME)), 3)+" _
                & " '/'+CAST(ESD_YEAR AS varchar(4)) AS monthyear, ESD_DTFROM" _
                & " FROM EMPSALARYDATA_D AS  ESD INNER JOIN" _
                & " BANK_M ON ESD.ESD_BANK = BANK_M.BNK_ID" _
                & " WHERE (IsNull(ESD_MODE,0) = 1) AND (IsNull(ESD_PAID,0) = 0) and ESD_EARN_NET <>0 " & strFFS _
                & " AND (ESD_BSU_ID = '" & Session("sBsuid") & "') " _
                & " AND ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE (EHD_bHold = 1)) "
                'V1.2 addition
                str_Sql = str_Sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=0  order by DT"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                clMonthYear.DataSource = ds.Tables(0)
                clMonthYear.DataTextField = "monthyear"
                clMonthYear.DataValueField = "month_year"
                clMonthYear.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub clMonthYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clMonthYear.SelectedIndexChanged
        'V1.1-changes to handle both bank and cash transfers.
        Try
            txtAmount.Text = ""
            Dim str_filter As String = ""
            Dim strA As String = ""
            Dim strB As String = ""
            Dim strC As String = ""
            If chkABC.Items.FindByValue("A").Selected = True Then
                strA = "A"
            End If
            If chkABC.Items.FindByValue("B").Selected = True Then
                strB = "B"
            End If
            If chkABC.Items.FindByValue("C").Selected = True Then
                strC = "C"
            End If
            If strA <> "" Then
                str_filter = "'" + strA + "'"  '+ strB + "," + strC
            End If
            If strB <> "" Then
                If str_filter <> "" Then
                    str_filter = str_filter + ","
                End If
                str_filter = str_filter + "'" + strB + "'"
            End If
            If strC <> "" Then
                If str_filter <> "" Then
                    str_filter = str_filter + ","
                End If
                str_filter = str_filter + "'" + strC + "'"
            End If
            'str_filter = strA + "," + strB + "," + strC
            hcatFilter.Value = str_filter

            Dim strFFS As String = " AND isnull(ESD.ESD_ERD_ID,0)=0 "
            If chkFFS.Checked = True Then
                strFFS = " AND isnull(ESD.ESD_ERD_ID,0)>0 "
            End If

            If rbCashTran.Checked Then
                Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String
                If str_filter <> "" Then
                    str_filter = "AND IsNull(EM.EMP_ABC,'0') in (" & str_filter & ")"
                End If
                If h_Emp_No.Value <> "" Then
                    str_filter = str_filter + " AND ESD_EMP_ID =" & h_Emp_No.Value
                End If
                str_Sql = "SELECT ESD.ESD_ID, ESD.ESD_EARN_NET, EM.EMPNO, " _
                    & " isnull(EM.EMP_FNAME,'')+' '+ isnull(EM.EMP_MNAME,'')+' '+" _
                    & " isnull(EM.EMP_LNAME,'') AS EMP_NAME" _
                    & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
                    & " EMPLOYEE_M AS EM ON ESD.ESD_EMP_ID = EM.EMP_ID" _
                    & " WHERE (IsNull(ESD.ESD_MODE,0) = 0) AND (ESD.ESD_PAID = 0)" _
                    & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "') " & strFFS _
                    & " AND (ESD.ESD_YEAR IN (" & get_Selected_YearMonths(False) & ")) AND (ESD.ESD_MONTH IN (" & get_Selected_YearMonths(True) & "))" _
                    & " AND (ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM  EMPSALHOLD_D WHERE EHD_bHold = 1)) " _
                    & str_filter & " ORDER BY EMP_NAME"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                gvDetailsCash.DataSource = ds.Tables(0)

                gvDetailsCash.DataBind()
                If gvDetailsCash.Rows.Count > 0 Then
                    divCashGrid.Visible = True
                    divCashGridhd.Visible = True
                    gvDetailsCash.Visible = True
                Else
                    divCashGrid.Visible = False
                    divCashGridhd.Visible = False
                    gvDetailsCash.Visible = True
                End If
            End If
            If rbBankTran.Checked Then
                Session("EMP_SEL_COND") = " and EMP_BSU_ID='" & Session("sBsuid") & "' "
                If str_filter <> "" Then
                    str_filter = " AND IsNull(EM.EMP_ABC,'0') in (" & str_filter & ")"
                End If
                If h_Emp_No.Value <> "" Then 'V1.4
                    str_filter = str_filter + " AND ESD_EMP_ID =" & h_Emp_No.Value
                End If
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String
                'V1.2 condition added for bank accounts with/without WPS Processing
                str_Sql = "SELECT DISTINCT " _
                & " BANK_M.BNK_DESCRIPTION, SUM(ESD.ESD_EARN_NET) AS amount,ESD.ESD_BANK" _
                & " FROM EMPSALARYDATA_D AS ESD INNER JOIN" _
                & " BANK_M ON ESD.ESD_BANK = BANK_M.BNK_ID " _
                & "inner join Employee_m EM on EM.EMP_ID=ESD.ESD_EMP_ID" _
                & " WHERE (IsNull(ESD.ESD_MODE,0) = 1) AND (ESD.ESD_PAID = 0) " _
                & " AND (ESD.ESD_BSU_ID = '" & Session("sBsuid") & "')" _
                & " AND ESD.ESD_YEAR in (" & get_Selected_YearMonths(False) & ") and ESD.ESD_MONTH in (" & get_Selected_YearMonths(True) & ")  " _
                & " AND ESD.ESD_ID NOT IN (SELECT EHD_ESD_ID  FROM EMPSALHOLD_D WHERE (EHD_bHold = 1) )"

                'If ViewState("MainMnu_code") = "P130170" Then
                str_Sql = str_Sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=0  " & strFFS & str_filter & ""  'To get records not processed through WPS"
                'ElseIf ViewState("MainMnu_code") = "P130171" Then
                'str_Sql = str_Sql & " AND IsNull(ESD.ESD_BProcessWPS,0)=1  "
                'End If
                str_Sql = str_Sql & " GROUP BY BANK_M.BNK_DESCRIPTION,ESD.ESD_BANK" _
                            & " ORDER BY BNK_DESCRIPTION"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                gvDetailsBank.DataSource = ds.Tables(0)
                gvDetailsBank.DataBind()
                If gvDetailsBank.Rows.Count > 0 Then
                    divbankHd.Visible = True
                    divBank.Visible = True
                    gvDetailsBank.Visible = True
                Else
                    divbankHd.Visible = False
                    divBank.Visible = False
                    gvDetailsBank.Visible = True
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            gvDetailsCash.DataBind()
        End Try
    End Sub

    Private Function get_Selected_YearMonths(ByVal isMonth As Boolean) As String
        Dim str_code As String = ""
        Dim str_narrn As String = ""
        Dim i_yearMonth As Integer = 1 'year
        If isMonth = True Then
            i_yearMonth = 0
        End If
        For Each item As ListItem In clMonthYear.Items
            If (item.Selected) Then
                If str_code <> "" Then
                    str_code = str_code & "," & item.Value.Split("_")(i_yearMonth)
                    str_narrn = str_narrn & "," & item.Text
                Else
                    str_code = item.Value.Split("_")(i_yearMonth)
                    str_narrn = item.Text
                End If
            End If
        Next
        If isMonth = True Then
            If str_code = "" Then
                Session("monthselected") = "1900"
                str_code = "1900"
            Else
                Session("monthselected") = str_code
            End If
        Else
            If str_code = "" Then
                Session("yearselected") = "20"
                str_code = "20"
            Else
                Session("yearselected") = str_code
            End If
            Session("yearselected") = str_code
        End If
        txtNarrn.Text = "Salary Transfer For " & str_narrn
        get_Selected_YearMonths = str_code

    End Function
    'V1.1 addition
    Protected Function GetNextChqNo(ByVal pId As Integer, ByVal pItr As Integer, _
   ByVal pGetNext As Boolean, ByVal stTrans As SqlTransaction, ByVal CHD_NO As String) As String

        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        Dim lstrChqNo As String

        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "SELECT CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If

        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_PREV_CHB_ID='" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If
        If pId = 0 Then
            Return "INSUFFICIENT LOT"
        End If

        Dim str_Sql As String

        'If pItr = 1 Then
        '    str_Sql = "SELECT CHB_Id,CHB_LOTNO,CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_ALLOTED=0 ORDER BY CHD_NO"
        'Else
        '    str_Sql = "SELECT CHB_Id,CHB_LOTNO,CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_ALLOTED=0 AND CHD_NO NOT IN (" & Mid(lstrUsedChqNos, 2) & ") ORDER BY CHD_NO"
        'End If

        If pItr = 1 Then
            str_Sql = "SELECT CHB_Id,CHB_LOTNO,min(CHD_NO)CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_NO>='" & CHD_NO & "' AND ISNULL(CHD_ALLOTED,0)=0 GROUP BY CHB_Id,CHB_LOTNO "
        Else
            str_Sql = "SELECT CHB_Id,CHB_LOTNO,min(CHD_NO)CHD_NO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "'  AND CHD_NO NOT IN (" & Mid(lstrUsedChqNos, 2) & ") AND CHD_NO>='" & CHD_NO & "' AND ISNULL(CHD_ALLOTED,0)=0 GROUP BY CHB_Id,CHB_LOTNO"
        End If


        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lstrChqBookId = ds.Tables(0).Rows(0)(0)
            hCheqBook.Value = lstrChqBookId
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(1)
            lstrChqNo = ds.Tables(0).Rows(0)(2)
            lstrUsedChqNos = lstrUsedChqNos & "," & lstrChqNo
            Return Convert.ToString(lstrChqBookId) & "|" & Convert.ToString(lstrChqBookLotNo) & "|" & lstrChqNo
        Else
            Return ""
        End If
    End Function
    'V1.1 addition
    Protected Function GetChqNos(ByVal pId As Integer, ByVal pGetNext As Boolean, ByVal stTrans As SqlTransaction, ByVal CHD_NO As String) As String
        Dim lintChqs As Integer
        Dim lintMaxChqs As Integer
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = ""
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "select CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If


        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_ID<>'" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If

        'Dim str_Sql As String = "select CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        Dim str_Sql As String = "select Min(CHD_NO) CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' AND CHD_NO>='" & CHD_NO & "' AND ISNULL(CHD_ALLOTED,0)=0 GROUP BY AvlNos,CHB_Id,CHB_LOTNO"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lintChqs = ds.Tables(0).Rows(0)(0)
            lintMaxChqs = ds.Tables(0).Rows(0)(1)
            lstrChqBookId = ds.Tables(0).Rows(0)(2)
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(3)
            Return Convert.ToString(lintChqs) & "|" & Convert.ToString(lintMaxChqs) & "|" & lstrChqBookId & "|" & lstrChqBookLotNo
        Else
            Return ""
        End If
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        btnPrint.Visible = False
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        'If rbBank.Checked = True Then
        Session("ReportSource") = AccountsReports.BankPaymentVoucher(ViewState("lstrNewDocNo"), ViewState("strDocDate"), Session("sBSUID"), Session("F_YEAR"))
        Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
        'End If
        'If rbCash.Checked = True Then
        '    Session("ReportSource") = AccountsReports.CashPaymentVoucher(ViewState("lstrNewDocNo"), txtDocdate.Text, Session("sBSUID"), Session("F_YEAR"))
        '    Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
        'End If
    End Sub

    Protected Sub rbBankTran_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBankTran.CheckedChanged

        setBankorCashTransfer()
        set_Comboanddate()
        clMonthYear_SelectedIndexChanged(Nothing, Nothing)

        rbCash.Checked = False
        rbCash.Enabled = False
        rbBank.Checked = True
        Set_CashorBankPayment()
        chkBearer.Checked = False
        'txtBankCode.Text = Session("CollectBank")
        'txtBankDescr.Text = Session("Collect_name")
    End Sub

    Protected Sub rbCashTran_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCashTran.CheckedChanged
        setBankorCashTransfer()
        set_Comboanddate()
        clMonthYear_SelectedIndexChanged(Nothing, Nothing)
        rbCash.Enabled = True
        chkBearer.Checked = False
    End Sub

    Protected Sub rbBank_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBank.CheckedChanged
        Set_CashorBankPayment()
        Trbearer.Visible = True
        chkBearer.Checked = False
    End Sub

    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged
        Set_CashorBankPayment()
        chkBearer.Checked = False
        Trbearer.Visible = False

    End Sub

    Protected Sub rbCheque_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCheque.CheckedChanged
        lblRef.Visible = False
        txtrefChequeno.Visible = False
        IMG1.Visible = True
        IMG1.Disabled = False
        txtChqBook.ReadOnly = False
        txtChqNo.ReadOnly = False
        txtrefChequeno.ReadOnly = True
    End Sub

    Protected Sub rbOthers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOthers.CheckedChanged
        lblRef.Visible = True
        txtrefChequeno.Visible = True
        IMG1.Disabled = True
        IMG1.Visible = False
        txtChqBook.ReadOnly = True
        txtChqNo.ReadOnly = True
        txtrefChequeno.ReadOnly = False
    End Sub

    Protected Sub chkABC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkABC.SelectedIndexChanged
        clMonthYear_SelectedIndexChanged(Nothing, Nothing)
    End Sub


    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDelete.Click
        txtEmpNo.Text = ""
        h_Emp_No.Value = ""
        clMonthYear_SelectedIndexChanged(Nothing, Nothing)
        imgDelete.Visible = False
    End Sub

    Protected Sub txtEmpNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpNo.TextChanged
        imgDelete.Visible = True
        clMonthYear_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub chkFFS_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFFS.CheckedChanged
        set_Comboanddate()
        clMonthYear_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub rbAXCheque_CheckedChanged(sender As Object, e As EventArgs) Handles rbAXCheque.CheckedChanged
        If rbAXCheque.Checked = True Then
            txtrefChequeno.Enabled = False
            txtrefChequeno.Text = "CHEQUE"
        End If
    End Sub

    Protected Sub rbAXOthers_CheckedChanged(sender As Object, e As EventArgs) Handles rbAXOthers.CheckedChanged
        If rbAXOthers.Checked = True Then
            txtrefChequeno.Enabled = True
            txtrefChequeno.Text = ""
        End If
    End Sub
End Class
