﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="empLeaveDetailsView.aspx.vb" Inherits="Payroll_empLeaveDetailsView" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function ShowLeaveDetail(id) {
            //var sFeatures;
            //sFeatures = "dialogWidth: 559px; ";
            //sFeatures += "dialogHeight: 405px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            //var NameandCode;
            //var result;
            //result = window.showModalDialog("ShowEmpLeave.aspx?ela_id=" + id, "", sFeatures)
            var url = "ShowEmpLeave.aspx?ela_id=" + id;
            //var oWnd = radopen(url, "pop_up");
            //return false;
            Popup(url);
        }

    </script>

    <%-- <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> --%>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Employee Leave Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td valign="top" width="50%" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        <td align="right"></td>
                    </tr>
                </table>
                <a id='top'></a>
                <table style="border-collapse: collapse" align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <table runat="server" id="Table1" cellpadding="5"
                                cellspacing="0" width="100%" align="center">
                                <%-- <tr class="subheader_img">
                <td colspan="7" style="height: 19px" align="left">Employee Leave Details:</td>                
     </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span>  </td>
                                    <td width="30%">

                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                            EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                                    </td>
                                    <td width="20%">
                                        <span class="field-label">To Date  </span></td>
                                    <td width="30%">

                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgToDate" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtToDate" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                            ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:RadioButtonList ID="rblOptions" runat="server" CssClass="radiobutton"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">All</asp:ListItem>
                                            <asp:ListItem Value="A">Approved</asp:ListItem>
                                            <asp:ListItem Value="N">Pending for Approval</asp:ListItem>
                                            <asp:ListItem Value="R">Rejected</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:LinkButton ID="btnExcelto" runat="server" Text="Export to Excel" OnClick="btnExcelto_Click"></asp:LinkButton>
                                        <asp:Button ID="btnView" Text="View" runat="server" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                            <div>
                                <div>
                                    <table width="100%" id="tbl">
                                        <tr>
                                            <td align="center">
                                                <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" CssClass="table table-bordered table-row" AllowPaging="True">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Emp No">
                                                            <HeaderTemplate>
                                                                Emp No.<br />
                                                                <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../images/forum_search.gif" OnClick="ImageButton1_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCode" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <HeaderTemplate>
                                                                Emp Name.
                                                                <br />
                                                                <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../images/forum_search.gif" OnClick="ImageButton1_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From"
                                                            HtmlEncode="False">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="To"
                                                            HtmlEncode="False">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LeaveDays" HeaderText="Days" />

                                                        <asp:TemplateField HeaderText="Leave Type">
                                                            <HeaderTemplate>
                                                                Leave Type
                                                                <br />
                                                                <asp:TextBox ID="txtLeaveType" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnLeaveTypeSearch" runat="server" ImageAlign="Top" ImageUrl="../images/forum_search.gif" OnClick="ImageButton1_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaveType" runat="server" Text='<%# Bind("LeaveType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Status" HeaderText="Status" />

                                                        <asp:TemplateField HeaderText="Department">
                                                            <HeaderTemplate>
                                                                Department
                                                                <br />
                                                                <asp:TextBox ID="txtDept" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnDeptSearch" runat="server" ImageAlign="Top" ImageUrl="../images/forum_search.gif" OnClick="ImageButton1_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDept" runat="server" Text='<%# Bind("EMP_DEPT_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Remarks" SortExpression="Remarks">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkRemarks" runat="server" OnClientClick="<%# &quot;ShowLeaveDetail('&quot; & Container.DataItem(&quot;ELA_ID&quot;) & &quot;');return false;&quot; %>"
                                                                    Text='<%# Bind("Remarks") %>'></asp:LinkButton>
                                                                <asp:Label ID="lblRemarks" Text='<%# Bind("Remarks") %>' runat="server"></asp:Label>
                                                                <asp:Label ID="lblELAID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--Added by vikranth on 29th Oct 2019--%>
                                                        <asp:TemplateField HeaderText="Document">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgDoc" runat="server"
                                                                    AlternateText=' <%#Eval("DOC_ID")%> ' ToolTip="Click To View Document"
                                                                    ImageUrl="../Images/ViewDoc.png" Visible="false" OnClick="imgDoc_Click" />
                                                                &nbsp;
                                     <asp:HyperLink ID="btnImgDoc" runat="server" ImageUrl="../Images/ViewDoc.png">View</asp:HyperLink>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="griditem" />
                                                    <HeaderStyle CssClass="gridheader_pop" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4"><span id="sp_message" class="error" runat="server"></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>

                </table>


            </div>
        </div>
    </div>
</asp:Content>


