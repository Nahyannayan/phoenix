<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="empMasterDetail_Documents.aspx.vb" Inherits="Payroll_empMasterDetail_Documents" Title="Untitled Page" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var oldMenu_HideItems = Menu_HideItems;

        if (oldMenu_HideItems) {
            Menu_HideItems = function (items) {
                if (!items || ((typeof (items.tagName) == "undefined") && (items instanceof Event))) { items = __rootMenuItem; }
                if (items && items.rows && items.rows.length == 0) { items.insertRow(0); }
                return oldMenu_HideItems(items);
            }
        }


        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //var url;

            //url = 'empShowMasterEmp.aspx?id=' + mode;

            document.getElementById("<%=hf_SearchMode.ClientID%>").value = mode;
            var url = "empShowMasterEmp.aspx?id=" + mode;
            var oWnd = radopen(url, "pop_pagecode");
           

        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var SearchMode = document.getElementById("<%=hf_SearchMode.ClientID%>").value

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                if (SearchMode == 'ME') {
                   
                    document.getElementById("<%=txtME.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfME_ID.ClientID %>").value = NameandCode[1];
                }
                else if (SearchMode == 'ML') {
                   
                    document.getElementById("<%=txtML.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfML_ID.ClientID %>").value = NameandCode[1];
                }

                else if (SearchMode == 'IU') {
                    
                    document.getElementById("<%=txtIU.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfIU.ClientID %>").value = NameandCode[1];


                }

                else if (SearchMode == 'WU') {
                    
                    document.getElementById("<%=txtWU.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfWU.ClientID %>").value = NameandCode[1];
                }
            }
        }
function getDocDocument() {
    var sFeatures;
    sFeatures = "dialogWidth: 729px; ";
    sFeatures += "dialogHeight: 560px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    <%--var result;

    result = window.showModalDialog("selIDDesc.aspx?ID=Document", "", sFeatures)
    if (result != '' && result != undefined) {
        NameandCode = result.split('___');
        document.getElementById('<%=h_DOC_DOCID.ClientID %>').value = NameandCode[0];
        document.getElementById('<%=txtDocDocument.ClientID %>').value = NameandCode[1];

    }
    return false;--%>

    var url = "selIDDesc.aspx?ID=Document";
    var oWnd = radopen(url, "pop_document");


}

function OnClientClose2(oWnd, args) {
    //get the transferred arguments

    var arg = args.get_argument();

    if (arg) {
        NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_DOC_DOCID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtDocDocument.ClientID%>').value = NameandCode[1];
                __doPostBack('<%= txtDocDocument.ClientID%>', 'TextChanged');
            }
        }

    

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_pagecode" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_document" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Employee Master
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary5" runat="server" ValidationGroup="ValDocDet" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="ValQualDet" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValExpDet" />
                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Fields Marked with (<font color="red">*</font>)
           are mandatory</td>
                    </tr>
                    <tr>

                        <td>
                            <div  class="card mb-3"> <div class="card-header letter-space">
                            <table align="left" width="100%" cellpadding="3" cellspacing="0" >
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle" style="width: 827px">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"></font></td>
                    </tr>--%>
                               
                                <tr>
                                    <td align="left" rowspan="1">
                                        <table align="left" width="100%">
                                            <tr>
                                                <td valign="top">Employee No</td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtEmpno1" runat="server"></asp:TextBox></td>
                                                <td valign="top">Name<font class="text-danger">*</font></td>

                                                <td style="text-align: left" valign="top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="ddlEmpSalute" runat="server" ReadOnly="true"></asp:TextBox>
                                                                <%-- <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                    <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                    <asp:ListItem Value="Miss">Miss</asp:ListItem>
                                    <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                    <asp:ListItem Value="Dr.">Dr.</asp:ListItem>
                                    <asp:ListItem Value="Prof.">Prof.</asp:ListItem>
                                    <asp:ListItem Value="Rev.">Rev.</asp:ListItem>
                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                </asp:DropDownList>--%>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFname1" runat="server" placeholder="First Name.. "></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMname1" runat="server" placeholder="Middle Name.. "></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtLname1" runat="server" placeholder="Last Name.. "></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <%-- <tr>
                                                <td></td>
                                                <td align="center">(First)</td>
                                                <td align="center">(Middle)</td>
                                                <td align="center">(Last)</td>
                                            </tr>--%>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table></div></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlMain" runat="server">
                                <table align="center" width="100%">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Name as in Passport</span> </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtPassportname" runat="server"></asp:TextBox></td>
                                        <td width="20%"></td>
                                        <td width="30%"></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Visa/ Labour card  status</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlVstatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVstatus_SelectedIndexChanged">
                                                <asp:ListItem Value="0">Applied</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="True">Not Applied</asp:ListItem>
                                                <asp:ListItem Value="2">Visa Holder</asp:ListItem>
                                                <asp:ListItem Value="3">LC Holder</asp:ListItem>
                                            </asp:DropDownList></td>

                                        <td align="left"><span class="field-label">Visa Type</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlVtype" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%-- <asp:SqlDataSource ID="SqlVisaType" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                            SelectCommand="SELECT    EVM_ID, EVM_DESCR&#13;&#10;FROM         EMPVISATYPE_M&#13;&#10;">
                        </asp:SqlDataSource>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Visa Issued Unit</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtIU" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnVIss_unit" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('IU');return false;"
                                                 /></td>
                                        <td align="left"><span class="field-label">Working Unit</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtWU" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnW_unit" runat="server" ImageUrl="~/Images/forum_search.gif" Enabled="false" OnClientClick="getPageCode('WU');return false;"
                                                 /></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">MOL Profession</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtML" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnV_Desig" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('ML');return false;"
                                                 /></td>
                                        <td align="left"><span class="field-label">MOE Profession</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtME" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnMoe_desig" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPageCode('ME');return false;"
                                                 /></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <span><span class="field-label">Visa Prog. ID</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtVisaPrgID" runat="server">
                                            </asp:TextBox></td>
                                        <td align="left"><span class="field-label">Contract Type</span></td>

                                        <td align="left">
                                            <asp:DropDownList ID="ddlEmpContractType" runat="server">
                                                <asp:ListItem Value="0">Limited</asp:ListItem>
                                                <asp:ListItem Value="1">Unlimited</asp:ListItem>
                                                <asp:ListItem Value="3">N/A</asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <span class="field-label">Person ID</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtPersonID" runat="server">
                                            </asp:TextBox></td>
                                        <td align="left"><span class="field-label">ABC Category</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlCategory" runat="server">
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtFromAbc" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgFrom" TargetControlID="txtFromAbc">
                                            </ajaxToolkit:CalendarExtender>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                PopupButtonID="txtFromAbc" TargetControlID="txtFromAbc">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:HiddenField ID="hdnABC" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                           <span class="field-label">UID Number</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtUIDNo" runat="server">
                                            </asp:TextBox></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="title-bg" colspan="4">Document Details</td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Document</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtDocDocument" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgDocDocSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                OnClientClick="getDocDocument();return false;" />
                                            <asp:RequiredFieldValidator ID="rqDocument" runat="server" ControlToValidate="txtDocDocument"
                                                ErrorMessage="Select Document" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><br />
                                            <asp:CheckBox ID="chkDocActive" runat="server" Text="Active" />
                                        </td>
                                        <td align="left"><span class="field-label">Document No</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtDocDocNo" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqDocNo" runat="server" ControlToValidate="txtDocDocNo"
                                                ErrorMessage="Enter Document No " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator></td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="left"><span class="field-label">Issue Place</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtDocIssuePlace" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqIssuePlace" runat="server" ControlToValidate="txtDocIssuePlace"
                                                ErrorMessage="Enter Issue place" ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Issue Date</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtDocIssueDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgDocIssueDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="return getDate(7,0);" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtDocIssueDate"
                                                ErrorMessage="Please enter Issue Date " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDocIssueDate"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator></td>
                                        <td align="left"><span class="field-label">Exp. Date</span></td>

                                        <td align="left">
                                            <asp:TextBox ID="txtDocExpDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgDocExpDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="return getDate(8,1);" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtDocExpDate"
                                                ErrorMessage="Please enter Exp. Date " ValidationGroup="ValDocDet">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                    ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtDocExpDate"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                    ValidationGroup="ValDocDet">*</asp:RegularExpressionValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnDocAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="ValDocDet" />
                                            <asp:Button ID="btnDocCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" valign="top">
                                            <asp:GridView ID="gvEMPDocDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                                Width="100%">
                                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Document" HeaderText="Document" ReadOnly="True" />
                                                    <asp:BoundField DataField="Doc_No" HeaderText="Document No" />
                                                    <asp:BoundField DataField="Doc_IssuePlace" HeaderText="Issue Place" />
                                                    <asp:TemplateField HeaderText="Issue Date" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# GetDate(Container.DataItem("Doc_IssueDate")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Exp. Date" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text='<%# GetDate(Container.DataItem("Doc_ExpDate")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <HeaderTemplate>
                                                            Edit
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDocEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                                OnClick="lnkDocEdit_Click" Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <RowStyle CssClass="griditem" />
                                                <SelectedRowStyle CssClass="griditem_hilight" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>

                    </tr>
                    <tr>

                        <td valign="middle" align="center">
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button
                                ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button
                                ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" />

                        </td>
                    </tr>
                    <tr>

                        <td align="center">
                            <asp:HiddenField ID="hfME_ID" runat="server" />
                            <asp:HiddenField ID="hfML_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="DocIss" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDocIssueDate" TargetControlID="txtDocIssueDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="DocExp" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgDocExpDate" TargetControlID="txtDocExpDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="hfIU" runat="server" />
                            <asp:HiddenField ID="hfAttBy" runat="server" />
                            <asp:HiddenField ID="hfWU" runat="server" />
                            <asp:HiddenField ID="h_DOC_DOCID" runat="server" />
                            <asp:HiddenField ID="hf_InsuID" runat="server" />
                            <asp:HiddenField ID="hf_KnownAs" runat="server" />
                            <asp:HiddenField ID="hf_SearchMode" runat="server" />
                        </td>

                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

