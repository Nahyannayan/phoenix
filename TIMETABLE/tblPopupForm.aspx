<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tblPopupForm.aspx.vb" Inherits="SelBussinessUnit" Theme="General" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
   
       <base target="_self" />
     <!-- Bootstrap core CSS-->
    <link href="/PHOENIXBETAV2/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETAV2/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETAV2/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/PHOENIXBETAV2/cssfiles/sb-admin.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
         </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

 
    <form id="form1" runat="server">
     <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0"> 
         <tr valign="top"  align="center" class="title">
             <td>
                 Select Value</td>
         </tr>
                    <tr valign = "top">
                            <td>
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True" SkinID="GridViewPickDetails" PageSize="15">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <ItemTemplate>
                                            &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSecDescr" runat="server" Text='<%# Bind("DESCR1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu1" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu1">
                                                                                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtCode" runat="server" Width="90px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("DESCR2") %>'></asp:Label>&nbsp;<br />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu2" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu2">
                                                                                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtName" runat="server" Width="120px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblBSUName" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu3" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu3">
                                                                                <img id="mnu_3_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtBSUName" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR2") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                                Width="76px" AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr> 
                        <td align="center" colspan="3" >
                         <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
                    </tr>
                </table>
            <script type="text/javascript">
                cssdropdown.startchrome("chromemenu1");
                if('<%=Request.QueryString("multiSelect") %>' == '' || '<%=Request.QueryString("multiSelect") %>' == 'true')
                { 
                    cssdropdown.startchrome("chromemenu2");
                }
                else
                {
                    cssdropdown.startchrome("chromemenu3");
                }
                
            </script>
    </form>
</body>
</html>