﻿
Partial Class TIMETABLE_tblMast_ProcessStaff
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            TabSelect()
        End If

    End Sub
    Public Sub TabSelect()
        If rbAddteach.Checked = True Then
            ProcStaff_Add_ctr.Visible = True
            ProcStaff_Map_ctr.Visible = False
            callUserControl_Proc(0)
        Else
            ProcStaff_Map_ctr.Visible = True
            ProcStaff_Add_ctr.Visible = False
            callUserControl_Proc(1)

        End If
    End Sub

    Private Sub callUserControl_Proc(ByVal TabIndex As Integer)


        Dim arr As String() = New String(2) {}

        If Not Session("ACD_TYPE_TT") Is Nothing Then
            arr = Session("ACD_TYPE_TT").ToString.Split("|")
        Else
            arr(0) = "0"
            arr(1) = ""
        End If

        If TabIndex = 0 Then
            ProcStaff_Add_ctr.bindStaffData(arr(0), arr(1))
        ElseIf TabIndex = 1 Then
            ProcStaff_Map_ctr.bind_dropdown(arr(0), arr(1))
            ProcStaff_Map_ctr.bindTeacherData_OASIS(arr(0), arr(1))
            'ElseIf TabIndex = 2 Then
            '    ProcStaff_Create_ctr.bindTeacherData()
        End If
    End Sub

    
    Protected Sub rbAddMissing_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddteach.CheckedChanged
        TabSelect()
    End Sub
    Protected Sub rbAddMap_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddMap.CheckedChanged
        TabSelect()
    End Sub

End Class
