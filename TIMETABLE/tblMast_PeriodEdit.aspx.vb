Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studAtt_room_Grade_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page


                    ViewState("grd_arr") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    disable_control()
                    bindCopyGrades()
                    BindWeeks()
                    BINDGRADES()




                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub

    Private Sub BindWeeks()

        chkWeek.Items.Add(New ListItem("Sunday", "Sunday"))
        chkWeek.Items.Add(New ListItem("Monday", "Monday"))
        chkWeek.Items.Add(New ListItem("Tuesday", "Tuesday"))
        chkWeek.Items.Add(New ListItem("Wednesday", "Wednesday"))
        chkWeek.Items.Add(New ListItem("Thursday", "Thursday"))
        chkWeek.Items.Add(New ListItem("Friday", "Friday"))
        chkWeek.Items.Add(New ListItem("Saturday", "Saturday"))

        ddlWk_Name.Items.Add(New ListItem("Sunday", "Sunday"))
        ddlWk_Name.Items.Add(New ListItem("Monday", "Monday"))
        ddlWk_Name.Items.Add(New ListItem("Tuesday", "Tuesday"))
        ddlWk_Name.Items.Add(New ListItem("Wednesday", "Wednesday"))
        ddlWk_Name.Items.Add(New ListItem("Thursday", "Thursday"))
        ddlWk_Name.Items.Add(New ListItem("Friday", "Friday"))
        ddlWk_Name.Items.Add(New ListItem("Saturday", "Saturday"))


    End Sub
    Private Sub bindCopyGrades()
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim str_Sql As String = ""

        Dim str_filter_GRM_DIS As String = String.Empty
        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PM.GETBSU_GRADES", param)

        chkOthGrade.DataSource = ds.Tables(0)
        chkOthGrade.DataTextField = "GRM_DISPLAY"
        chkOthGrade.DataValueField = "GRM_GRD_ID"
        chkOthGrade.DataBind()
    End Sub

    Private Sub BINDGRADES()

        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim str_Sql As String = ""

        Dim str_filter_GRM_DIS As String = String.Empty
        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PM.GETBSU_GRADES", param)

        ddlGrd_id.DataSource = ds.Tables(0)
        ddlGrd_id.DataTextField = "GRM_DISPLAY"
        ddlGrd_id.DataValueField = "GRM_GRD_ID"
        ddlGrd_id.DataBind()
        If ViewState("grd_arr").ToString.Trim <> "" Then

            ddlGrd_id.ClearSelection()

            If Not ddlGrd_id.Items.FindByValue(ViewState("grd_arr").ToString) Is Nothing Then

                ddlGrd_id.Items.FindByValue(ViewState("grd_arr").ToString).Selected = True
            End If
        End If

        ddlGrd_id_SelectedIndexChanged(ddlGrd_id, Nothing)

    End Sub
    Protected Sub ddlGrd_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrd_id.SelectedIndexChanged
        
        bindPeriod_Data()
    End Sub
    Protected Sub ddlWk_Name_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWk_Name.SelectedIndexChanged


       


        bindPeriod_Data()
    End Sub


    Private Sub bindPeriod_Data()

        Try

            For Each item As ListItem In chkOthGrade.Items

                If item.Text = ddlGrd_id.SelectedItem.Text Then
                    item.Selected = True
                    item.Enabled = False
                Else
                    item.Selected = False
                    item.Enabled = True
                End If
            Next
            For Each item As ListItem In chkWeek.Items

                If item.Text = ddlWk_Name.SelectedValue Then
                    item.Selected = True
                    item.Enabled = False
                Else
                    item.Selected = False
                    item.Enabled = True
                End If
            Next


            Dim ds As DataSet
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_filter_GRM_DIS As String = String.Empty

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@GRD_ID", ddlGrd_id.SelectedValue)
            param(2) = New SqlParameter("@WEEK_NAME", ddlWk_Name.SelectedValue)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PM.GETBSU_GRADE_PERIOD", param)

            gvPeriod.DataSource = ds.Tables(0)
            gvPeriod.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            disable_control()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call disable_control()
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim ISCHECKED As Boolean
        For Each R As GridViewRow In gvPeriod.Rows
            Dim chkPer As CheckBox = DirectCast(R.FindControl("chkSelect"), CheckBox)
            If chkPer.Checked = True Then
                ISCHECKED = True
                Exit For

            End If
        Next
        If ISCHECKED = True Then
            str_err = calltransaction(errorMessage)
            If str_err = "0" Then
                bindPeriod_Data()
                lblError.Text = "Record Saved Successfully"
                'reset_state()
                'Call disable_control()
            Else
                lblError.Text = errorMessage
            End If
        Else
            lblError.Text = "Please select the periods allocated to the grade(s) !!! "
        End If


    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean

        Dim GRD_IDs As String = String.Empty
        Dim WeekName As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim str As String = String.Empty
        Dim i As Integer


        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                bEdit = False
                For Each item As ListItem In chkOthGrade.Items
                    If item.Selected = True Then
                        GRD_IDs += item.Value.ToString() + "|"
                    End If
                Next

                For Each item As ListItem In chkWeek.Items
                    If item.Selected = True Then
                        WeekName += item.Value.ToString() + "|"
                    End If
                Next

                For i = 0 To gvPeriod.Rows.Count - 1

                    Dim row As GridViewRow = gvPeriod.Rows(i)

                    Dim chkPer As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim txtPer_Lbl As TextBox = DirectCast(row.FindControl("txtPer_Lbl"), TextBox)
                    Dim lblPM_ID As Label = DirectCast(row.FindControl("lblPM_ID"), Label)
                    Dim txtFrom As TextBox = DirectCast(row.FindControl("txtFrom"), TextBox)
                    Dim txtTo As TextBox = DirectCast(row.FindControl("txtTo"), TextBox)

                    If chkPer.Checked = True Then
                        str += String.Format("<PERIOD PM_ID='{0}' PM_LABEL='{1}' PM_ST_TIME='{2}' PM_END_TIME='{3}' ></PERIOD>", lblPM_ID.Text.Trim, _
                                            txtPer_Lbl.Text.Trim, txtFrom.Text.Trim, txtTo.Text.Trim)
                    End If
                Next
                If str <> "" Then
                    str = "<PERIODS>" + str + "</PERIODS>"
                Else
                    str = "<PERIODS></PERIODS>"
                End If

                Dim PARAM(10) As SqlParameter
                PARAM(0) = New SqlParameter("@BSU_ID", BSU_ID)
                PARAM(1) = New SqlParameter("@GRD_IDS", GRD_IDs)
                PARAM(2) = New SqlParameter("@WEEKNAME", WeekName)
                PARAM(3) = New SqlParameter("@STR_XML", str)              
                PARAM(4) = New SqlParameter("@RETURN_VLAUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PM.SAVEPERIOD_GRADE_WISE", PARAM)

                status = CInt(PARAM(4).Value)


                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If

                ViewState("viewid") = "0"
                ViewState("datamode") = "add"


                calltransaction = "0"


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using



    End Function


    Sub reset_state()

        chkOthGrade.Enabled = False
        gvPeriod.Enabled = False
    End Sub
    Sub disable_control()

        ddlGrd_id.Enabled = False
    End Sub
    Sub enable_control()

        ddlGrd_id.Enabled = True
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

   
    Protected Sub gvPeriod_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPeriod.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim strScript As String = "SelectDeSelectHeader(" + DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try
    End Sub
    
End Class
