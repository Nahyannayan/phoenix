Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class SelBussinessUnit
    Inherits System.Web.UI.Page
    'Shared Session("liUserList") As List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Session("liUserList") = Nothing
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
                Session("liUserList") = Nothing
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        ViewState("multiSel") = IIf(Request.QueryString("multiSelect") = "true", True, False)
        Dim strMultiSel As String = Request.QueryString("multiSelect")
        If strMultiSel <> String.Empty And strMultiSel <> "" Then
            If String.Compare("False", strMultiSel, True) = 0 Then
                gvGroup.Columns(1).Visible = False
                gvGroup.Columns(3).Visible = False
                gvGroup.Columns(4).Visible = True
                'DropDownList1.Visible = False
                btnFinish.Visible = False
                chkSelAll.Visible = False
            Else
                gvGroup.Columns(1).Visible = True
                gvGroup.Columns(3).Visible = True
                gvGroup.Columns(4).Visible = False
                'DropDownList1.Visible = True
                btnFinish.Visible = True
            End If
        Else
            ViewState("multiSel") = True
            gvGroup.Columns(1).Visible = True
            gvGroup.Columns(3).Visible = True
            gvGroup.Columns(4).Visible = False
            'DropDownList1.Visible = True
            btnFinish.Visible = True
        End If
        If Page.IsPostBack = False Then

            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"



            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID")
           

            Case "STUDENT_GRADE"
                Dim strGRD_IDs As String = Request.QueryString("GRD_IDs")
                Dim strSCT_IDs As String = Request.QueryString("SCT_IDs")
                If strGRD_IDs = "0" Then
                    strGRD_IDs = ""
                End If
                If strSCT_IDs = "ALL" Or strSCT_IDs = "0" Then
                    strSCT_IDs = ""
                End If
                GridBindStudentDetails(strGRD_IDs, "GRD_ID", False, strSCT_IDs)
           
        End Select
    End Sub

    

    Sub GridBindStudentDetails(ByVal vIDs As String, ByVal vTYPE As String, ByVal bGETSTU_NO As Boolean, Optional ByVal vSCT_IDs As String = "")
        Try
            Dim strACD_ID As String = Request.QueryString("ACD_ID")
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_Sql As String
            Dim CURRENT_DATE As String = getAcademicYear_date(strACD_ID)
            Dim str_filter_code, str_filter_name, str_Filter_date, strOrderBy As String
            Dim str_mode As String
            Dim str_txtCode, str_txtName, str_BSUName As String
            str_filter_code = ""
            str_filter_name = ""
            str_mode = ""

            str_txtCode = ""
            str_txtName = ""
            str_BSUName = ""
            strOrderBy = ""
            If bGETSTU_NO Then
                str_query_header = "SELECT DISTINCT CAST(STU_ID AS VARCHAR) + '___' + STU_NO  ID, STU_NO DESCR1, STU_NAME DESCR2 "
            Else
                str_query_header = "SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2 "
            End If
            Select Case vTYPE
                'code modified by dhanya

                Case "GRD_ID"
                    str_query_header += " FROM [TT].[vw_STUDENT_LIST_ANYYEAR] " & _
                        " INNER JOIN OASIS..STUDENT_PROMO_S ON STP_STU_ID=STU_ID " & _
                        "WHERE STU_BSU_ID = '" & Session("sBSUID") & "'"
                    If vIDs <> "" Then
                        str_query_header += "AND STP_GRD_ID IN ('" & vIDs.Replace("___", "','") & "')"
                    End If
                    If vSCT_IDs <> "" Then
                        str_query_header += "AND STP_SCT_ID IN ('" & vSCT_IDs.Replace("___", "','") & "')"
                    End If

                    If strACD_ID <> "" Then
                        str_query_header += " AND STP_ACD_ID =" & strACD_ID
                    End If
                    str_query_header += " and stu_currstatus<>'CN' " ' and (stu_leavedate is not null or stu_leavedate>getDate())"

            End Select
            str_Filter_date = " AND (CONVERT(datetime, STU_LASTATTDATE) >= '" & CURRENT_DATE & "' OR " & _
" CONVERT(datetime, STU_LASTATTDATE) IS NULL) " 'AND (STU_DOJ <= CONVERT(datetime, '" & CURRENT_DATE & "')) "
            strOrderBy = " ORDER BY STU_NAME"
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("STU_NO", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If ViewState("multiSel") Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("STU_NAME", str_Sid_search(0), str_txtName)

                ''column1
            End If

            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & str_Filter_date & strOrderBy
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If ViewState("multiSel") Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "Stud. No"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "Student Name"
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Function getAcademicYear_date(ByVal acd_id As String) As String
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim DATE_STRING As String = String.Empty
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@acd_id", acd_id)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "TT.GETACADEMIC_DATE", param)
            If datareader.HasRows = True Then
                While datareader.Read = True
                    DATE_STRING = Convert.ToString(datareader("CURRENT_DATE"))

                End While
            End If
        End Using
        Return DATE_STRING
    End Function



    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        If chkSelAll.Checked Then
            Dim strGRD_IDs As String
            Dim strACD_ID As String
            Dim strSCT_IDs As String
            Select Case ViewState("ID")
                Case "SUBJECT"
                    strGRD_IDs = Request.QueryString("GRD_IDS")
                    strACD_ID = Request.QueryString("ACD_ID")
                Case "STUDENT_GRADE"
                    strGRD_IDs = Request.QueryString("GRD_IDs")
                    strSCT_IDs = Request.QueryString("SCT_IDs")
                    If strSCT_IDs = "ALL" Or strSCT_IDs = "0" Then
                        strSCT_IDs = ""
                    End If
            End Select
            Dim str_query_header As String = " SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2  FROM " & _
       " [TT].[vw_STUDENT_LIST_ANYYEAR] WHERE STU_BSU_ID ='" & Session("sBSUID") & "'"

            If strGRD_IDs <> "" Then
                str_query_header += " AND STU_GRD_ID IN ('" & strGRD_IDs & "')"
            End If
            If strSCT_IDs <> "" Then
                str_query_header += " AND STU_SCT_ID IN ('" & strSCT_IDs & "')"
            End If
            str_query_header += " ORDER BY STU_NO"

            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_TIMETABLEConnectionString, CommandType.Text, str_query_header)
            If drReader.HasRows = True Then
                While (drReader.Read())
                    Session("liUserList").Remove(drReader(0))
                    Session("liUserList").Add(drReader(0))
                End While
            End If
        Else
            SetChk(Me.Page)
            h_SelectedId.Value = ""
        End If
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "___"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")


        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode = '" & h_SelectedId.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub

    Protected Sub gvGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup.Load
        'gvGroup.Columns(2).Visible = False
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim retstring As String = String.Empty
        Dim bGetStudentNo As Boolean = IIf(Request.QueryString("GETSTU_NO") Is Nothing, False, Request.QueryString("GETSTU_NO"))
        If (Not lblcode Is Nothing) Then
            If bGetStudentNo Then
                Dim len As Integer = lblcode.Text.IndexOf("||")
                retstring = lblcode.Text.Insert(len, "||" & lbClose.Text.Replace("'", "\'"))
            Else
                retstring = lblcode.Text & "||" & lbClose.Text.Replace("'", "\'")
            End If
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            ''Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.returnValue = '" & retstring & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")


            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & retstring & "';")
            Response.Write("var oWnd = GetRadWindow('" & retstring & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        If chkSelAll.Checked Then
            Select Case ViewState("ID")

                Case "STUDENT_GRADE"
                    'Dim strGRD_IDs As String = Request.QueryString("GRD_IDs")
                    'SelectAll_Student(strGRD_IDs)

                    Dim strGRD_IDs As String = Request.QueryString("GRD_IDs")
                    Dim strSCT_IDs As String = Request.QueryString("SCT_IDs")
                    If strSCT_IDs = "ALL" Or strSCT_IDs = "0" Then
                        strSCT_IDs = ""
                    End If
                    SelectAll_Student(strGRD_IDs, strSCT_IDs)

            End Select
        Else
            Session("liUserList").Clear()
        End If
        GridBind()
        SetChk(Me.Page)
    End Sub



    Private Sub SelectAll_Student(Optional ByVal vGRD_ID As String = "", Optional ByVal vSCT_ID As String = "")

        Dim str_query_header As String = " SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2  FROM " & _
        " [TT].[vw_STUDENT_LIST_ANYYEAR] WHERE STU_BSU_ID ='" & Session("sBSUID") & "'"

        If vGRD_ID <> "" Then
            str_query_header += " AND STU_GRD_ID IN ('" & vGRD_ID & "')"
        End If
        If vSCT_ID <> "" Then
            str_query_header += " AND STU_SCT_ID IN ('" & vSCT_ID & "')"
        End If
        str_query_header += " ORDER BY STU_NO"

        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_TIMETABLEConnectionString, CommandType.Text, str_query_header)
        If drReader.HasRows = True Then
            While (drReader.Read())
                Session("liUserList").Remove(drReader(0))
                Session("liUserList").Add(drReader(0))
            End While
        End If
        GridBind()

        'For Each Gvrow As GridViewRow In gvGroup.Rows
        '    If Gvrow.RowType = DataControlRowType.DataRow Then
        '        TryCast(Gvrow.FindControl("chkControl"), HtmlInputCheckBox).Checked = True
        '        Session("liUserList").Add(TryCast(Gvrow.FindControl("Label1"), Label).Text)
        '    End If
        'Next

    End Sub


    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow Then
        '        If Session("liUserList").count > 0 Then
        '            TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox).Checked = True
        '        Else
        '            TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox).Checked = False
        '        End If
        '    End If
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        'End Try
    End Sub

End Class

