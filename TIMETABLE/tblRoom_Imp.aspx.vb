Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class tblRoom_Imp
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010502") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                   bindRoom_Data()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

    Sub bindRoom_Data()

        Try

            Dim ds As DataSet
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_filter_GRM_DIS As String = String.Empty

           

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@SHORTNAME", "")
            param(2) = New SqlParameter("@FULLNAME", "")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[RM].[GETROOM_DETAIL_BASIC]", param)
            If ds.Tables(0).Rows.Count > 0 Then
                gvRM.DataSource = ds.Tables(0)
                gvRM.DataBind()
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                'ds.Tables(0).Rows(0)("flag") = False

                gvRM.DataSource = ds.Tables(0)
                Try
                    gvRM.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvRM.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvRM.Rows(0).Cells.Clear()

                gvRM.Rows(0).Cells.Add(New TableCell)
                gvRM.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRM.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRM.Rows(0).Cells(0).Text = "No rooms added"

            End If
          
        Catch ex As Exception

        End Try
    End Sub



   
End Class
