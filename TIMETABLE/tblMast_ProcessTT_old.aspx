<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" 
AutoEventWireup="false" CodeFile="tblMast_ProcessTT_old.aspx.vb" Inherits="tblMast_ProcessTT" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">


 function alertFu()
 {
 document.getElementById("<%=hfDiv.ClientID %>").value='1';
 document.getElementById("228").className += "darkPanlvisible";
 document.getElementById("338").style.display = 'block';
 }
function alertFuHide()
 {
 document.getElementById("228").className =document.getElementById("228").className.replace( /(?:^|\s)darkPanlvisible(?!\S)/g , '' )
 document.getElementById("<%=hfDiv.ClientID %>").value='0';
 document.getElementById("338").style.display = 'none';
 window.location.reload();
 }
</script>
 
 
    
<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
                TIMETABLE PROCESSING</td>
        </tr>
    </table>
    <table  align="center" border="0" cellpadding="0"
        cellspacing="0" width="90%">
        <tr><td align="left" ><asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="Group">
                        </asp:ValidationSummary></td></tr>
        <tr><td class="matters"  valign="bottom">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    class="BlueTable"  width="100%"><tr>
                        <td align="left"  class="matters" >
                            Academic Year<span style="color: #ff0000"></span></td>
                        <td align="center" style="width: 2px;" class="matters" >
                            :</td>
                        <td align="left"  >
                            <asp:DropDownList id="ddlACD_ID" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"  class="matters" >
                           Timegrid</td>
                        <td align="center" style="width: 2px;" class="matters" >
                            :</td>
                        <td align="left"  >
                            <asp:DropDownList ID="ddlUploadFor" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:LinkButton ID="lbtnAddTimeGrid" runat="server">Add / Edit Timegrid</asp:LinkButton>
                        </td>
                    </tr>
                    <tr style="height:100px">
            <td align="left"  style="width:200px; " class="matters" >
                Upload Untis Timetable(timetable.txt)</td>
            <td align="left"  style="width:2px; " class="matters">
                :</td>
            <td align="left" valign="middle"   >
              <iframe id="iframeFileLoad" src="UploadTxt/tbluploadfile_TT.aspx" scrolling="no" frameborder="0"  
               style="text-align:center;vertical-align:middle;border-style:none;margin:0px;width:100%;height:55px"
                 ></iframe>
                <asp:Label id="ltNote" runat="server"></asp:Label>
            </td>
        </tr>
        <tr runat="server" id="hideR1"> <td align="left" style="font-weight: bold; font-size: 8pt; background-image: url(../Images/bgblue.gif);
                                    color: white; font-family: Verdana;" colspan="3"> 
                                    <span style="font-size: 10pt; font-family: Arial">Following Updates Required</span>
                                     </td></tr>
              <tr runat="server" id="hideR2"><td align="left"  class="matters" colspan="3" style="padding:0px;margin:0px;">
              
                <asp:GridView id="gvMaster" runat="server" 
    AutoGenerateColumns="False" BorderColor="#1B80B6"
                              
                Font-Names="Verdana"
                                Font-Size="8pt" ForeColor="#1B80B6" 
                                Width="100%" >
                                <rowstyle cssclass="griditem" height="26px" />
                                <emptydatarowstyle cssclass="gridheader" horizontalalign="Center" wrap="True" />
                                <columns>

                                    <asp:TemplateField HeaderText="Sr.No">
                                        <EditItemTemplate>
                                          
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblr1" runat="server" Text='<%# Bind("r1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Master Data" >
    <ItemTemplate>
<asp:Label id="lblId" runat="server" Text='<%# Bind("DESCR") %>' 
       ></asp:Label> 
</ItemTemplate>
 <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
</asp:TemplateField>
<asp:TemplateField HeaderText="Status">
<ItemTemplate>
    <asp:Image ID="imgStatus" runat="server"  ImageUrl='<%# Bind("FLAG") %>'  AlternateText='<%# Bind("alt") %>' ToolTip='<%# Bind("ALT") %>'/> 
</ItemTemplate>
    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
</asp:TemplateField>

                                    <asp:TemplateField HeaderText="Update">
                                        <EditItemTemplate>
                                           
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                          
                                            <asp:LinkButton ID="lbtnMaster" runat="server" onclick="lbtnMaster_Click">Update Master Data</asp:LinkButton>
                                             
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                      
</columns>
                                <selectedrowstyle backcolor="Khaki" />
                                <headerstyle cssclass="gridheader_new" height="25px" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
              
                          </td></tr>
        
        </table></td></tr>
        <tr runat="server" id="hideR3">
        <td align="left" style="font-weight: bold; font-size: 8pt; background-image: url(../Images/bgblue.gif);
                                    color: white; font-family: Verdana; border:solid 1px #1b80b6;height:25px; border-collapse:collapse;" colspan="3"> 
                                    <span style="font-size: 10pt; font-family: Arial">Timetable Data</span>
                                     </td></tr>
         <tr runat="server" id="hideR4"><td align="center" style="margin:0px;padding:0px;" >
         <asp:GridView id="gvTimetable" runat="server" CssClass="gridstyle" 
                    AutoGenerateColumns="False" EmptyDataText="No Details Added"
                    Width="100%" AllowPaging="True" >
                    <rowstyle cssclass="griditem" height="25px" />
                    <emptydatarowstyle cssclass="matters" />
                    <columns>
                        <asp:TemplateField HeaderText="Class">
                            <HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" class="gridheader_text" width="130px">
                            <tr><td colspan="2" class="matters" align="center" style="font-size:12px;">Class</td></tr>
                            <tr>
                                                            <td align="center">
                                                                <asp:TextBox ID="txtClass" runat="server"  Width="124px"></asp:TextBox>
                                                            </td>
                                                            <td  valign="middle" align="center">
                                                                <asp:ImageButton ID="btnClass_Src" runat="server" OnClick="btnClass_Src_Click"
                                                                    ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" 
                                                                     />
                                                               </td>
                                                        </tr></table>
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblClass" runat="server" Text='<%# Bind("CLASS") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Week Name" HeaderStyle-Font-Size="12px">
                         <HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0"  class ="gridheader_text">
                            <tr><td colspan="2" class="matters" align="center" style="font-size:12px;">Week Name</td></tr>
                            <tr>
                                                            <td align="center">
                                                                <asp:TextBox ID="txtWeek" runat="server"  Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td  valign="middle" align="center">
                                                                <asp:ImageButton ID="btnWeek_Src" runat="server"  OnClick="btnWeek_Src_Click"
                                                                    ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" 
                                                                     />
                                                               </td>
                                                        </tr></table>
                                           
                            </HeaderTemplate>
                        
                            <ItemTemplate>
                                <asp:Label ID="lblDAY_NAME" runat="server" Text='<%# Bind("DAY_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Teacher">
                         <HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0"  class ="gridheader_text">
                            <tr><td colspan="2" class="matters" align="center" style="font-size:12px;">Teacher</td></tr>
                            <tr>
                                                            <td align="center">
                                                                <asp:TextBox ID="txtTeacher" runat="server"  Width="124px"></asp:TextBox>
                                                            </td>
                                                            <td  valign="middle" align="center">
                                                                <asp:ImageButton ID="btnTeacher_Src" runat="server"  OnClick="btnTeacher_Src_Click" 
                                                                    ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" 
                                                                     />
                                                               </td>
                                                        </tr></table>
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStaff_name" runat="server" Text='<%# Bind("STAFF_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                        <HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0"  class ="gridheader_text">
                            <tr><td colspan="2" class="matters" align="center" style="font-size:12px;">Subject</td></tr>
                            <tr>
                                                            <td align="center">
                                                                <asp:TextBox ID="txtSubject" runat="server"  Width="124px"></asp:TextBox>
                                                            </td>
                                                            <td  valign="middle" align="center">
                                                                <asp:ImageButton ID="btnSubject_Src" runat="server"  OnClick="btnSubject_Src_Click" 
                                                                    ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" 
                                                                     />
                                                               </td>
                                                        </tr></table>
                                           
                            </HeaderTemplate>
                        
                        
                            <ItemTemplate>
                                <asp:Label ID="lblSUB" runat="server" Text='<%# Bind("sub") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room" HeaderStyle-Font-Size="12px">
                         <HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0"  class ="gridheader_text">
                            <tr><td colspan="2" class="matters" align="center" style="font-size:12px;">Room</td></tr>
                            <tr>
                                                            <td align="center">
                                                                <asp:TextBox ID="txtRoom" runat="server"  Width="70px"></asp:TextBox>
                                                            </td>
                                                            <td  valign="middle" align="center">
                                                                <asp:ImageButton ID="btnRoom_Src" runat="server"  OnClick="btnRoom_Src_Click" 
                                                                    ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" 
                                                                     />
                                                               </td>
                                                        </tr></table>
                                           
                            </HeaderTemplate>
                       
                        
                            <ItemTemplate>
                                <asp:Label ID="lblTTM_ROOM" runat="server" Text='<%# Bind("TTM_ROOM") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
<asp:TemplateField HeaderText="Period" HeaderStyle-Font-Size="12px">
<HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0"  class ="gridheader_text">
                            <tr><td colspan="2" class="matters" align="center" style="font-size:12px;">Period</td></tr>
                            <tr>
                                                            <td align="center">
                                                                <asp:TextBox ID="txtPeriod" runat="server"  Width="50px"></asp:TextBox>
                                                            </td>
                                                            <td  valign="middle" align="center">
                                                                <asp:ImageButton ID="btnPeriod_Src" runat="server" OnClick="btnPeriod_Src_Click" 
                                                                    ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" 
                                                                     />
                                                               </td>
                                                        </tr></table>
                                           
                            </HeaderTemplate>
<ItemTemplate>
<asp:Label id="lblPeriod" runat="server" Text='<%# Bind("ttm_Period") %>' 
        ></asp:Label>
</ItemTemplate>
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
</asp:TemplateField>
                        <asp:TemplateField HeaderText="Time" HeaderStyle-Font-Size="12px">
                            <ItemTemplate>
                                <asp:Label ID="lblP_TIME" runat="server" Text='<%# bind("P_TIME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
</columns>
                    <selectedrowstyle cssclass="griditem_hilight" />
                     <headerstyle cssclass="gridheader_pop" height="25px" horizontalalign="Center" verticalalign="Middle" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView>
              </td></tr>
        
                </table>
                   <div id="plReopen" runat="server" style="display: block; overflow: visible; border-color: #1b80b6;
                    border-style: solid; border-width: 1px; width: 700px; background-color:White;">
                    <div class="msg_header" style="width: 700px; margin-top: 1px; vertical-align: middle;
                        background-color: White;">
                        <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: left;">
                            <span style="clear: left; display: inline; float: left; visibility: visible;">Add / Edit Timegrid</span> <span style="clear: right; display: inline; float: right; visibility: visible;
                                    margin-top: -4px; vertical-align: top;">
                            
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/closeme.png"
                                        Style="margin-top: -1px; vertical-align: top;" /></span>
                        </div>
                    </div>
                     <asp:Panel ID="Panel1" runat="server" Width="700px" Height="320px" BackColor="White"
                        BorderColor="#1b80b6" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Both">
                        <div>
                        
                       <table  BorderColor="#1b80b6" border="1" CellPadding="6" CellSpacing="2" class="BlueTableView"
                          style="width: 100%">
                   
                    
                    <tr class="matters">
                        <td  align="left" style="height:20px">Timegrid </td>
                        <td >:</td><td align="left" colspan="4">
                        <asp:TextBox ID="txtTGrid" 
                            runat="server" Width="200px"></asp:TextBox></td>
                    </tr>
                    <tr  class="matters">
                        <td  align="left" style="height:20px">Start of the week</td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:DropDownList ID="ddlStartWK" runat="server">
                                <asp:ListItem>Sunday</asp:ListItem>
                                <asp:ListItem>Monday</asp:ListItem>
                                <asp:ListItem>Tuesday</asp:ListItem>
                                <asp:ListItem>Wednesday</asp:ListItem>
                                <asp:ListItem>Thursday</asp:ListItem>
                                <asp:ListItem>Friday</asp:ListItem>
                                <asp:ListItem>Saturday</asp:ListItem>
                            </asp:DropDownList> </td>  
 <td  align="left" style="height:20px">Total week day</td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:DropDownList ID="ddlTotWK" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                            </asp:DropDownList> </td>  
                       
                    </tr>
                    <tr  class="matters">
                        <td align="left" style="height:20px">Show Subject Group Name</td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                           
                            <asp:RadioButton ID="rbYes" runat="server" Text="Yes"  GroupName="GRP" />
                            <asp:RadioButton ID="rbNo" runat="server" Checked="True" Text="No"  GroupName="GRP"/>
                        </td>
                    </tr>
                    </table>
                    <div align="center" style ="padding-top:12px">
                    <asp:Button ID="btnAddSaveGrid" runat="server" Text="Add / Save" CssClass="button" 
                            width="120px" Height="27px" ValidationGroup="time"  ></asp:Button>
                        <asp:Button ID="btnUpdateSaveGrid" runat="server" CssClass="button" 
                            Height="27px" Text="Update / Save" ValidationGroup="time" width="120px" />
                        <asp:Button ID="btnCancelSaveGrid" runat="server" CssClass="button" 
                            Height="27px" Text="Cancel" ValidationGroup="time" width="120px" />
                    </div>
                    <div align="left" style ="padding-top:12px;padding-bottom:12px;">
                    <asp:label ID="ltGridError" runat="server"  ></asp:label>
                    </div>
                    
                    <div>

                <asp:GridView id="gvTime" runat="server" 
    AutoGenerateColumns="False" BorderColor="#1B80B6"
                              
                Font-Names="Verdana"
                                Font-Size="8pt" ForeColor="#1B80B6" 
                                Width="98%" >
                                <rowstyle cssclass="griditem" height="26px" />
                                <emptydatarowstyle cssclass="gridheader" horizontalalign="Center" wrap="True" />
                                <columns>

                                    <asp:TemplateField HeaderText="Timegrid">
                                        <EditItemTemplate>
                                          
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTS_MULTI_TIMEGRID" runat="server" Text='<%# Bind("TS_MULTI_TIMEGRID") %>'></asp:Label>
                                             <asp:Label ID="lblTS_ID" runat="server" Text='<%# Bind("TS_ID") %>' Visible="false"></asp:Label>
                                             <asp:Label ID="lblTS_bSHOWSUBJECT_GRP" runat="server" Text='<%# Bind("TS_bSHOWSUBJECT_GRP") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Start of the week" >
    <ItemTemplate>
<asp:Label id="lblTS_START_WEEK" runat="server" Text='<%# Bind("TS_START_WEEK") %>' 
       ></asp:Label> 
</ItemTemplate>
 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
</asp:TemplateField>
<asp:TemplateField HeaderText="Total day">
<ItemTemplate>
  <asp:Label id="lblTS_TOT_WEEK_DAY" runat="server" Text='<%# Bind("TS_TOT_WEEK_DAY") %>' 
       ></asp:Label>  
</ItemTemplate>
    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
</asp:TemplateField>

                                    <asp:TemplateField HeaderText="Show Group Name">
                                       
                                        <ItemTemplate>
                                          
                                            <asp:Image ID="imgGRP" runat="server"  ImageUrl='<%# Bind("FLAG") %>'   ToolTip='<%# Bind("ALT") %>'/> 
                                             
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                      
                                    <asp:TemplateField HeaderText="Edit">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEditGRP" runat="server" onclick="lbtnEditGRP_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                      
</columns>
                                <selectedrowstyle backcolor="Khaki" />
                                <headerstyle cssclass="gridheader_new" height="25px" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                    </div>
                    </div>
                        </asp:Panel>
                      </div>
            
  <asp:Button ID="btnShowReopen" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mdlReopen" runat="server"
                 TargetControlID="btnShowReopen"
                    PopupControlID="plReopen"  BackgroundCssClass="modalBackground"
                    DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />
                    
                    <asp:HiddenField ID="hfTS_ID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfDiv" runat="server" Value="0" />
                    <div  class="darkPanlvisible" id="228">
                    <div style="height:100px;  margin-top: 200px;margin-left: 200px;">
               
      <div id="338" style="background-color:White;height:125px;width:250px; border:solid 1px gray;font-size:14px;font-weight:bold;padding-top:3px;"><img src="image/progressbar1.gif"  alt="progress" /><div style="padding-top:1px;">Please Wait... </div>
          
     </div></div>
</div>
 
      <script language="javascript" type="text/javascript">
      
var hideFlag;
hideFlag=document.getElementById("<%=hfDiv.ClientID %>").value
//alert(hideFlag);
 if (hideFlag=='0')
 {
 document.getElementById("228").className =document.getElementById("228").className.replace( /(?:^|\s)darkPanlvisible(?!\S)/g , '' )
document.getElementById("338").style.display = 'none';
 
 
 }
 
 else
 {
 document.getElementById("228").className += "darkPanlvisible";
 document.getElementById("338").style.display = 'block';
 }
</script>  
</asp:Content>


