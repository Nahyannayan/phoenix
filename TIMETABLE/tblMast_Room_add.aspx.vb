Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class tblMast_Room_add
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010503") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    btnAddROOM.Visible = True
                    btnUpdateRoom.Visible = False
                    btnCancelROOM.Visible = False
                    bindRoom_Data()
                  
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub
 
    Sub bindRoom_Data()

        Try

            Dim ds As DataSet
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_filter_GRM_DIS As String = String.Empty

            Dim txtSHORT As New TextBox
            Dim txtFull As New TextBox
            If gvRooms.Rows.Count > 0 Then


                If Not gvRooms.HeaderRow.FindControl("txtSHORT") Is Nothing Then
                    txtSHORT = gvRooms.HeaderRow.FindControl("txtSHORT")
                    txtFull = gvRooms.HeaderRow.FindControl("txtFull")
                End If
            End If


            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@SHORTNAME", IIf(txtSHORT.Text = "", System.DBNull.Value, txtSHORT.Text.Trim))
            param(2) = New SqlParameter("@FULLNAME", IIf(txtFull.Text = "", System.DBNull.Value, txtFull.Text.Trim))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[RM].[GETROOM_DETAIL_BASIC]", param)
            If ds.Tables(0).Rows.Count > 0 Then
                gvRooms.DataSource = ds.Tables(0)
                gvRooms.DataBind()
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                'ds.Tables(0).Rows(0)("flag") = False

                gvRooms.DataSource = ds.Tables(0)
                Try
                    gvRooms.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvRooms.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvRooms.Rows(0).Cells.Clear()

                gvRooms.Rows(0).Cells.Add(New TableCell)
                gvRooms.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRooms.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRooms.Rows(0).Cells(0).Text = "No rooms added"

            End If
            txtSHORT.Text = ""
            txtFull.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gvRooms_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRooms.PageIndexChanging
        gvRooms.PageIndex = e.NewPageIndex
        bindRoom_Data()
    End Sub
    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblRM_ID As New Label
            Dim lblVirtual As New Label
            Dim lblRM_SHORTNAME As New Label
            Dim lblRM_FULLNAME As New Label


            lblRM_ID = TryCast(sender.FindControl("lblRM_ID"), Label)
            lblVirtual = TryCast(sender.FindControl("lblVirtual"), Label)
            lblRM_SHORTNAME = TryCast(sender.FindControl("lblRM_SHORTNAME"), Label)
            lblRM_FULLNAME = TryCast(sender.FindControl("lblRM_FULLNAME"), Label)

            txtFullName.Text = lblRM_FULLNAME.Text
            txtShortName.Text = lblRM_SHORTNAME.Text
            hfRM_ID.Value = lblRM_ID.Text

            If lblVirtual.Text = "0" Then
                rbNo.Checked = True
                rbYes.Checked = False
            Else
                rbNo.Checked = False
                rbYes.Checked = True
            End If

            gvRooms.Columns(5).Visible = False
            btnAddROOM.Visible = False
            btnUpdateRoom.Visible = True
            btnCancelROOM.Visible = True
        Catch ex As Exception
            '.Text = "Request could not be processed "
        End Try
    End Sub
  
    Sub reset_state()
        lblError.Text = ""
        gvRooms.Columns(5).Visible = True
        txtFullName.Text = ""
        txtShortName.Text = ""
        rbNo.Checked = True
        rbYes.Checked = False
        hfRM_ID.Value = "0"
        bindRoom_Data()
        btnAddROOM.Visible = True
        btnUpdateRoom.Visible = False
        btnCancelROOM.Visible = False
    End Sub
    
    Protected Sub btnAddROOM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddROOM.Click
        If Page.IsValid = True Then
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty

            str_err = calltransaction(errorMessage)
            If str_err = "0" Then

                reset_state()
                lblError.Text = "Record Saved Successfully"

            Else
                lblError.Text = errorMessage
            End If
        End If



    End Sub

    Protected Sub btnUpdateRoom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateRoom.Click
        If Page.IsValid = True Then
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty

            str_err = calltransaction(errorMessage)
            If str_err = "0" Then

                reset_state()
                lblError.Text = "Record Saved Successfully"
              
            Else
                lblError.Text = errorMessage
            End If
        End If
       

    End Sub

    Protected Sub btnCancelROOM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelROOM.Click
        gvRooms.Columns(5).Visible = True
        btnAddROOM.Visible = True
        btnUpdateRoom.Visible = False
        btnCancelROOM.Visible = False

    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean

        Dim GRD_IDs As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim str As String = String.Empty
      

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                bEdit = False

                Dim PARAM(6) As SqlParameter

                PARAM(0) = New SqlParameter("@BSU_ID", BSU_ID)
                PARAM(1) = New SqlParameter("@FULL_NAME", txtFullName.Text.Trim)
                PARAM(2) = New SqlParameter("@SHORT_NAME", txtShortName.Text.Trim)
                PARAM(3) = New SqlParameter("@bVIRTUAL", IIf(rbNo.Checked = True, 0, 1))
                PARAM(4) = New SqlParameter("@RM_ID", hfRM_ID.Value)
                PARAM(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(5).Direction = ParameterDirection.ReturnValue


                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[RM].[SAVEROOM_M_BASIC]", PARAM)

                status = CInt(PARAM(5).Value)


                If status = -11 Then
                    calltransaction = "1"
                    errorMessage = "Duplicate room short name not allowed !!!"
                    Return "1"
                ElseIf status <> 0 Then
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                    Return "1"
                End If

                ViewState("viewid") = "0"
                ViewState("datamode") = "add"


                calltransaction = "0"


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using



    End Function

    Protected Sub btnFull_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindRoom_Data()
    End Sub
    Protected Sub btnShort_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindRoom_Data()
    End Sub
End Class
