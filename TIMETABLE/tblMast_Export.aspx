﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="tblMast_Export.aspx.vb" Inherits="TIMETABLE_tblMast_Export" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Export to Units"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <iframe id="iframeroomexp" src="ucExport/Masterexp.aspx" scrolling="no" frameborder="0"
                                style="text-align: center; vertical-align: middle; border-style: none; margin: 0px; width: 100%; height: 670px"></iframe>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
