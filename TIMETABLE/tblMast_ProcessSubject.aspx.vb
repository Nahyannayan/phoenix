﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_tblMast_ProcessSubject
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("acd_id") = "0"
                ViewState("SET_TYPE") = "0"
                Dim arr As String() = New String(2) {}
                ViewState("OASIS_SUBJECT_Dataset") = ""
                If Not Session("ACD_TYPE_TT") Is Nothing Then
                    arr = Session("ACD_TYPE_TT").ToString.Split("|")
                    ViewState("acd_id") = arr(0)
                    ViewState("SET_TYPE") = arr(1)
                End If
                callUserControl_Proc()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Private Sub callUserControl_Proc()


        'adding missing subject
        If rbAddSubj.Checked = True Then
            plSubj_Missing.Visible = True
            plSubjectMap.Visible = False
            gvSubjectMap_GRP.Visible = False

            btnSaveAdd.Visible = True
            btnSaveMap.Visible = False
            btnSaveSub_grp_map.Visible = False
            trGrp.Visible = False
            trGrp_filter.Visible = False
            rbNotmap.Checked = True
            txtGrp_Search.Text = ""
            bindSubjectData_ADD(ViewState("acd_id"), ViewState("SET_TYPE"))
        Else
            btnSaveAdd.Visible = False



            plSubj_Missing.Visible = False

            'mapping subject group
            If GETTIMETABLE_GRP() = True Then
                trGrp.Visible = True
                txtGrp_Search.Text = ""

                trGrp_filter.Visible = True
                rbNotmap.Checked = True

                btnSaveMap.Visible = False
                btnSaveSub_grp_map.Visible = True

                gvSubjectMap.Visible = False
                gvSubjectMap_GRP.Visible = True

                plSubjectMap.Visible = False
                gvSubjectMap_GRP.Visible = True

                bind_dropdown(ViewState("acd_id"), ViewState("SET_TYPE"))
                bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))

            Else
                plSubjectMap.Visible = True
                gvSubjectMap_GRP.Visible = False
                trGrp_filter.Visible = False
                rbNotmap.Checked = True

                trGrp.Visible = False
                txtGrp_Search.Text = ""
                btnSaveMap.Visible = True
                btnSaveSub_grp_map.Visible = False

                gvSubjectMap.Visible = True
                gvSubjectMap_GRP.Visible = False
                bindMissingSubject_data(ViewState("acd_id"), ViewState("SET_TYPE"))

            End If

        End If
    End Sub
    Protected Sub rbAddSubj_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddSubj.CheckedChanged
        callUserControl_Proc()
    End Sub
    Protected Sub rbAddMap_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddMap.CheckedChanged
        callUserControl_Proc()
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\TIMETABLE\tblMast_ProcessTT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub
    Private Function GETTIMETABLE_GRP() As Boolean
        Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "TM.GETTIMETABLE_GRP", PARAM)
            If DATAREADER.HasRows = True Then
                GETTIMETABLE_GRP = True
            Else
                GETTIMETABLE_GRP = False
            End If
        End Using
    End Function

#Region "Adding missing Temp subject "
    Private Sub bindSubjectData_ADD(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETMISSING_SUBJECT]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectMissing_ADD.DataSource = ds.Tables(0)
                gvSubjectMissing_ADD.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvSubjectMissing_ADD.DataSource = ds.Tables(0)
                Try
                    gvSubjectMissing_ADD.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectMissing_ADD.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSubjectMissing_ADD.Rows(0).Cells.Clear()
                gvSubjectMissing_ADD.Rows(0).Cells.Add(New TableCell)
                gvSubjectMissing_ADD.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectMissing_ADD.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectMissing_ADD.Rows(0).Cells(0).Text = "No Missing Subject Available !!!"

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvSubjectMissing_ADD_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubjectMissing_ADD.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeader(" + DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub btnSaveAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION_ADD(errorMessage)
        If str_err = "0" Then

            bindSubjectData_ADD(ViewState("acd_id"), ViewState("SET_TYPE"))
            lblErr.Text = "Record Saved Successfully"

        Else
            lblErr.Text = errorMessage
        End If
    End Sub

    Private Function CALLTRANSACTION_ADD(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim chkSelect As CheckBox
        Dim txtTTM_SUBJECT As TextBox
        Dim lblTTM_SUBJECT As Label
        Dim lblTTM_GRD_ID As Label
        Dim STR_XML As New StringBuilder

        For Each row As GridViewRow In gvSubjectMissing_ADD.Rows
            chkSelect = row.FindControl("chkSelect")
            If chkSelect.Checked = True Then
                txtTTM_SUBJECT = row.FindControl("txtTTM_SUBJECT")
                lblTTM_GRD_ID = row.FindControl("lblTTM_GRD_ID")
                lblTTM_SUBJECT = row.FindControl("lblTTM_SUBJECT")
                STR_XML.Append(lblTTM_GRD_ID.Text.Trim + "$" + lblTTM_SUBJECT.Text + "&" + txtTTM_SUBJECT.Text & "|")
            End If

        Next

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("acd_id"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@SUBJECT", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[TM].[SAVESUBJECT_MISSING]", PARAM)
                ReturnFlag = PARAM(4).Value

                If ReturnFlag = 11 Then
                    CALLTRANSACTION_ADD = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION_ADD = 1
                    errorMessage = " Error occured while saving !!!"
                End If




            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION_ADD = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION_ADD <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function
#End Region
#Region "Process subject mapping"
    Sub bindMissingSubject_data(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETMISSING_SUBJECT_TOMAP]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectMap.DataSource = ds.Tables(0)
                gvSubjectMap.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvSubjectMap.DataSource = ds.Tables(0)
                Try
                    gvSubjectMap.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectMap.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSubjectMap.Rows(0).Cells.Clear()
                gvSubjectMap.Rows(0).Cells.Add(New TableCell)
                gvSubjectMap.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectMap.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectMap.Rows(0).Cells(0).Text = "No Records Available !!!"
                Dim imgMap As Image = DirectCast(gvSubjectMap.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvSubjectMap_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlOasis_Subject_Name As DropDownList = DirectCast(e.Row.FindControl("ddlOasis_Subject_Name"), DropDownList)
                Dim lblTTM_SUBJECT As Label = DirectCast(e.Row.FindControl("lblTTM_SUBJECT"), Label)
                Dim lblTTM_STM_ID As Label = DirectCast(e.Row.FindControl("lblTTM_STM_ID"), Label)
                Dim lblTTM_GRD_ID As Label = DirectCast(e.Row.FindControl("lblTTM_GRD_ID"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)
                Dim hidGRD_ID As HiddenField = DirectCast(e.Row.FindControl("hidGRD_ID"), HiddenField)
                Dim hfTTM_SBG_ID As HiddenField = DirectCast(e.Row.FindControl("hfTTM_SBG_ID"), HiddenField)
                Dim DS As DataSet = bindSubjectDdl(hidGRD_ID.Value)
                If Not DS Is Nothing And Not DS.Tables(0) Is Nothing And Not DS.Tables(0).Rows Is Nothing And DS.Tables(0).Rows.Count > 0 Then
                    ddlOasis_Subject_Name.Items.Clear()
                    ddlOasis_Subject_Name.DataSource = DS.Tables(0)
                    ddlOasis_Subject_Name.DataTextField = "SUBJECT"
                    ddlOasis_Subject_Name.DataValueField = "ID"
                    ddlOasis_Subject_Name.DataBind()
                    For Each ITEM As ListItem In ddlOasis_Subject_Name.Items
                        If lblTTM_STM_ID.Text = "Normal" Then
                            'added by vikranth 0n 28th Jan 2020
                            If hfTTM_SBG_ID.Value <> "" Then
                                If hfTTM_SBG_ID.Value.Substring(0, 4).ToLower() = "temp" Then
                                    If ITEM.Text.ToUpper.Contains("(" + lblTTM_SUBJECT.Text.ToUpper + ")") = True Then
                                        ITEM.Selected = True
                                        imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                        Exit For
                                    End If
                                ElseIf ITEM.Value = hfTTM_SBG_ID.Value Then
                                    ITEM.Selected = True
                                    imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                    Exit For
                                End If
                            ElseIf ITEM.Text.ToUpper.Contains("(" + lblTTM_SUBJECT.Text.ToUpper + ")") = True Then
                                ITEM.Selected = True
                                imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                Exit For
                            End If
                            'End by vikranth

                            'Commented by vikranth on 28th Jan 2020
                            'ITEM.Text.ToUpper.Contains("(" + lblTTM_SUBJECT.Text.ToUpper + ")") = True Then
                            '    ITEM.Selected = True
                            'imgMap.ImageUrl = "~/Images/arrowgreen.png"
                            'Exit For
                        Else
                            'Added by vikranth on 28th Jan 2020
                            If hfTTM_SBG_ID.Value <> "" Then
                                If hfTTM_SBG_ID.Value.Substring(0, 4).ToLower() <> "temp" Then
                                    If ITEM.Value = hfTTM_SBG_ID.Value Then
                                        ITEM.Selected = True
                                        imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                        Exit For
                                    End If
                                End If
                            End If
                            'End by vikranth

                            If ITEM.Text.ToUpper.Contains("(" + lblTTM_SUBJECT.Text.ToUpper + ")") = True And ITEM.Text.ToUpper.Contains(lblTTM_STM_ID.Text) Then
                                ITEM.Selected = True
                                imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Function bindSubjectDdl(ByVal grade As String) As DataSet
        Dim ds_subject As DataSet = New DataSet
        Try
            ' [TM].[GETOASIS_SUBJECT]
            Dim str_conn = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ViewState("acd_id"))
            PARAM(2) = New SqlParameter("@GRD_ID", grade)
            PARAM(3) = New SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
            ds_subject = SqlHelper.ExecuteDataset(str_conn, "[TM].[GETOASIS_SUBJECT]", PARAM)
            Return ds_subject
        Catch ex As Exception
            Return ds_subject
        End Try
    End Function
    Protected Sub btnSaveMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMap.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then

            bindMissingSubject_data(ViewState("acd_id"), ViewState("SET_TYPE"))

            lblErr.Text = "Record Saved Successfully"

        Else
            lblErr.Text = errorMessage
        End If
    End Sub
    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim STR_XML As New StringBuilder
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim ddlOasis_Subject_Name As DropDownList
                Dim lblTTM_SUBJECT As Label
                Dim lblTTM_STM_ID As Label
                For Each row As GridViewRow In gvSubjectMap.Rows
                    ddlOasis_Subject_Name = row.FindControl("ddlOasis_Subject_Name")
                    lblTTM_SUBJECT = row.FindControl("lblTTM_SUBJECT")
                    lblTTM_STM_ID = row.FindControl("lblTTM_STM_ID")
                    If ddlOasis_Subject_Name.SelectedValue.ToString.Trim <> "-1" Then
                        STR_XML.Append(lblTTM_SUBJECT.Text & "&" & lblTTM_STM_ID.Text & "%" & ddlOasis_Subject_Name.SelectedValue.ToString.Trim & "|")
                    End If
                Next

                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("acd_id"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@SUB_GRP", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[TM].[SAVESUBJECT_MAPPING]", PARAM)
                ReturnFlag = PARAM(4).Value
                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If
            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function
#End Region

#Region "Mapping subject group "
    Protected Sub rbNotmap_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbNotmap.CheckedChanged
        bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))
    End Sub

    Protected Sub rbMap_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbMap.CheckedChanged
        bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))
    End Sub
    Private Sub bind_dropdown(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Session("TTSubj_map_GRP") = Nothing
        Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim ds As New DataSet
        Dim OASISSUBJECT As New List(Of ListItem)
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
        ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETOASIS_SUBJECT_GRP]", PARAM)
        OASISSUBJECT.Add(New ListItem("Subject Group Not Mapped", "-1"))
        If ds.Tables(0).Rows.Count > 0 Then
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                OASISSUBJECT.Add(New ListItem(ds.Tables(0).Rows(I)("SUB_GRP"), ds.Tables(0).Rows(I)("SBG_ID")))
            Next

        End If
        Session("TTSubj_map_GRP") = OASISSUBJECT
    End Sub
    Private Sub bindSubjectData_OASIS_GRP(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim Info_Type As String = String.Empty
            If rbNotmap.Checked = True Then
                Info_Type = "NotMap"
            ElseIf rbMap.Checked = True Then
                Info_Type = "Map"
            ElseIf rbAll.Checked = True Then
                Info_Type = "All"
            End If
            Dim ds As New DataSet
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            PARAM(3) = New SqlParameter("@UNTIS_GRP", IIf(txtGrp_Search.Text.Trim <> "", txtGrp_Search.Text.Trim, System.DBNull.Value))
            PARAM(4) = New SqlParameter("@INFO_TYPE", Info_Type)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETTIMETABLE_SUBJECT_GRP]", PARAM)
            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectMap_GRP.DataSource = ds.Tables(0)
                gvSubjectMap_GRP.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvSubjectMap_GRP.DataSource = ds.Tables(0)
                Try
                    gvSubjectMap_GRP.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectMap_GRP.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvSubjectMap_GRP.Rows(0).Cells.Clear()
                gvSubjectMap_GRP.Rows(0).Cells.Add(New TableCell)
                gvSubjectMap_GRP.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectMap_GRP.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectMap_GRP.Rows(0).Cells(0).Text = "No Subject Group Available !!!"
                Dim imgMap As Image = DirectCast(gvSubjectMap_GRP.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvSubjectMap_GRP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubjectMap_GRP.PageIndexChanging
        gvSubjectMap_GRP.PageIndex = e.NewPageIndex
        bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))
    End Sub
    Protected Sub gvSubjectMap_GRP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubjectMap_GRP.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlUntis_Subject_Name As DropDownList = DirectCast(e.Row.FindControl("ddlUntis_Subject_Name"), DropDownList)
                Dim lblTTM_SUBJECT As Label = DirectCast(e.Row.FindControl("lblTTM_SUBJECT"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)
                If Not Session("TTSubj_map_GRP") Is Nothing Then
                    ddlUntis_Subject_Name.Items.Clear()
                    ddlUntis_Subject_Name.DataSource = Session("TTSubj_map_GRP")
                    ddlUntis_Subject_Name.DataTextField = "Text"
                    ddlUntis_Subject_Name.DataValueField = "Value"
                    ddlUntis_Subject_Name.DataBind()
                    For Each ITEM As ListItem In ddlUntis_Subject_Name.Items
                        If ITEM.Text.Contains("(" + lblTTM_SUBJECT.Text + ")") = True Then

                            ITEM.Selected = True
                            imgMap.ImageUrl = "~/Images/arrowgreen.png"
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnSaveSub_grp_map_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSub_grp_map.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION_GRP_MAP(errorMessage)
        If str_err = "0" Then
            bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))
            lblErr.Text = "Record Saved Successfully"
        Else
            lblErr.Text = errorMessage
        End If
    End Sub
    Private Function CALLTRANSACTION_GRP_MAP(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim ddlUntis_Subject_Name As DropDownList
        Dim lblTTM_SUBJECT As Label
        Dim STR_XML As New StringBuilder

        For Each row As GridViewRow In gvSubjectMap_GRP.Rows
            ddlUntis_Subject_Name = row.FindControl("ddlUntis_Subject_Name")
            lblTTM_SUBJECT = row.FindControl("lblTTM_SUBJECT")
            If ddlUntis_Subject_Name.SelectedValue.ToString.Trim <> "-1" Then
                STR_XML.Append(lblTTM_SUBJECT.Text & "%" & ddlUntis_Subject_Name.SelectedValue.ToString.Trim & "|")
            End If
        Next

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("acd_id"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@SUB_GRP", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TM.SAVESUBJECT_GROUP_MAPPING", PARAM)
                ReturnFlag = PARAM(4).Value


                If ReturnFlag = 11 Then
                    CALLTRANSACTION_GRP_MAP = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION_GRP_MAP = 1
                    errorMessage = "Error occured while saving !!!"
                End If
            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION_GRP_MAP = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION_GRP_MAP <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
#End Region









End Class
