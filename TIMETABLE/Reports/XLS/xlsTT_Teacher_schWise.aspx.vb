﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Drawing
Partial Class TIMETABLE_xlsTT_Teacher_schWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReport)
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT151005") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                bindAcademic_Year()






            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlACD_ID.Items.Clear()
            ddlAcd_id.DataSource = ds.Tables(0)
            ddlAcd_id.DataTextField = "ACY_DESCR"
            ddlAcd_id.DataValueField = "ACD_ID"
            ddlAcd_id.DataBind()
            ddlAcd_id.ClearSelection()
            ddlAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcd_id_SelectedIndexChanged(ddlAcd_id, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcd_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACD_ID.SelectedIndexChanged
        bindMasterData()
    End Sub

    Sub bindMasterData()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter

            ACD_ID = ddlAcd_id.SelectedValue


            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "TT.RPTGetEmployee_List", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvTT_Teacher.DataSource = ds.Tables(0)
                gvTT_Teacher.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvTT_Teacher.HeaderRow.FindControl("chkSelectAllRM"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvTT_Teacher.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectRM"), CheckBox)

                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next




            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvTT_Teacher.DataSource = ds.Tables(0)
                Try
                    gvTT_Teacher.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTT_Teacher.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvTT_Teacher.Rows(0).Cells.Clear()
                gvTT_Teacher.Rows(0).Cells.Add(New TableCell)
                gvTT_Teacher.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTT_Teacher.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTT_Teacher.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvTT_Teacher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTT_Teacher.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderTR(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Sub CallReport()

        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim EMP_IDS As New StringBuilder

        For Each row As GridViewRow In gvTT_Teacher.Rows
            Dim chkSelectRM = DirectCast(row.FindControl("chkSelectTR"), CheckBox)
            Dim lblEmp_id As Label
            If Not chkSelectRM Is Nothing Then
                If chkSelectRM.Checked = True Then
                    lblEmp_id = DirectCast(row.FindControl("lblEmp_id"), Label)
                    EMP_IDS.Append(lblEmp_id.Text & "|")
                End If
            End If
        Next


        If EMP_IDS.ToString() = "" Then

            lblErr.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1B80B6;padding:5pt;background-color:#eaebec;'>Please select the teacher</div>"
            Exit Sub
        End If


        Dim dsExcel As DataSet
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@IMG_BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", ddlACD_ID.SelectedValue)
        PARAM(2) = New SqlParameter("@EMP_IDS", EMP_IDS.ToString)
     

        dsExcel = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TT.XLS_TEACHER_WISE_SCHOOL_TT", param)




        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        TypicalTableSample(ef.Worksheets.Add("Teachers-Timetable"), dsExcel)

        Dim fileName As String = "SchoolWise-Teachers-Timetable.xlsx"

        'Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        'Dim dts As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
        'Dim stuFilename As String = "student_" & Left(dts, Len(dts) - 2) & ".xls"
        'ws.InsertDataTable(dtEXCEL, "A1", True)
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName)
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()


        Dim pathSave As String = Session("sUsr_id") & "\" & fileName
        If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
            ' Create the directory.
            Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
        End If


        ef.Save(cvVirtualPath & pathSave)
        Dim path = cvVirtualPath & pathSave
        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()
    End Sub

    

    Sub TypicalTableSample(ByVal ws As ExcelWorksheet, ByVal dsExcel As DataSet)

        Dim htcellColorsTop As New Hashtable
        htcellColorsTop.Add("Sunday", "#d6f1b9")
        htcellColorsTop.Add("Monday", "#fbcba3")
        htcellColorsTop.Add("Tuesday", " #f7c5d3")
        htcellColorsTop.Add("Wednesday", "#b8f6f5")
        htcellColorsTop.Add("Thursday", "#fae7a0")
        htcellColorsTop.Add("Friday", "#fffc8c")
        htcellColorsTop.Add("Saturday", "#9fe6b8")

        Dim htcellColorsbottom As New Hashtable
        htcellColorsbottom.Add("Sunday", " #ebf8dc")
        htcellColorsbottom.Add("Monday", "#ffe6cd")
        htcellColorsbottom.Add("Tuesday", " #fbe1e8")
        htcellColorsbottom.Add("Wednesday", "#defafa")
        htcellColorsbottom.Add("Thursday", "#fdf6db")
        htcellColorsbottom.Add("Friday", "#fffed2")
        htcellColorsbottom.Add("Saturday", "#cefcde")

        Dim dtEXCEL As New DataTable()

        Dim headerStyle As CellStyle = New CellStyle
        Dim colHeader As CellStyle = New CellStyle
        Dim CellStyle As CellStyle = New CellStyle
        Dim colTot As Integer = dsExcel.Tables(0).Columns.Count



        Using DATAREADER As SqlDataReader = getBsu_details()
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    headerStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
                    headerStyle.VerticalAlignment = VerticalAlignmentStyle.Center

                    headerStyle.Font.Size = 20 * 20
                    headerStyle.Font.Name = "Verdana"
                    ws.Cells.GetSubrangeAbsolute(0, 0, 0, colTot - 1).Merged = True
                    ws.Cells(0, 0).Value = Convert.ToString(DATAREADER("BSU_NAME"))
                    ws.Cells.GetSubrangeAbsolute(0, 0, 0, colTot - 1).Style = headerStyle
                    ws.Cells(1, 0).Value = "Teacher's Timetable"
                    ws.Cells.GetSubrangeAbsolute(1, 0, 1, colTot - 1).Merged = True
                    ws.Cells.GetSubrangeAbsolute(1, 0, 1, colTot - 1).Style = headerStyle
                     ws.Cells.GetSubrangeAbsolute(1, 0, 1, colTot - 1).Style.Font.Size = 16*16
                End While
            End If
        End Using

        ws.Columns(0).Width = 8 * 256
        ws.Columns(1).Width = 40 * 256
        ws.Columns(2).Width = 15 * 256


        For c As Integer = 3 To colTot - 1
            ws.Columns(c).Width = 16 * 256
        Next
        Dim iCOL As Integer
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "_"
        Dim weekList As New Hashtable()
        Dim tempWeekName As String = String.Empty
        Dim tempMergeClose As Integer = 0
        Dim flag As Boolean
        For Each COL As DataColumn In dsExcel.Tables(0).Columns
            colHeader.HorizontalAlignment = HorizontalAlignmentStyle.Center
            colHeader.VerticalAlignment = VerticalAlignmentStyle.Center
            colHeader.Font.Weight = ExcelFont.BoldWeight
            colHeader.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
            If COL.ColumnName.Contains("_") = True Then

                arInfo = COL.ColumnName.Split(splitter)

                If arInfo(0).ToString = tempWeekName Then
                    If flag = False Then
                        tempMergeClose = iCOL
                        flag = True
                    End If

                Else
                    tempWeekName = arInfo(0)
                    ws.Cells(4, iCOL).Value = arInfo(0)
                    'sunday,monday
                    If tempMergeClose <> iCOL And tempMergeClose <> 0 Then
                        ws.Cells.GetSubrangeAbsolute(4, tempMergeClose - 1, 4, iCOL - 1).Merged = True
                    End If

                    flag = False
                End If
                ws.Cells(4, iCOL).Style = colHeader
                ws.Cells(5, iCOL).Value = arInfo(1)
                ws.Cells(5, iCOL).Style = colHeader
                ws.Cells(5, iCOL).Style.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml(CStr(htcellColorsTop(arInfo(0)))))

                ws.Cells(4, iCOL).Style.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#c5d9f1"))
                ws.Rows(4).Height = 16 * 23
                ws.Rows(5).Height = 15 * 20
            Else


                ws.Cells(4, iCOL).Value = COL.ColumnName
                ws.Cells.GetSubrangeAbsolute(4, iCOL, 5, iCOL).Merged = True
                ws.Cells.GetSubrangeAbsolute(4, iCOL, 5, iCOL).Style = colHeader
                ws.Cells.GetSubrangeAbsolute(4, iCOL, 5, iCOL).Style.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
                ws.Cells.GetSubrangeAbsolute(4, iCOL, 5, iCOL).Style.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#c5d9f1"))
            End If

            iCOL = iCOL + 1
        Next

        If tempMergeClose <> iCOL And tempMergeClose <> 0 Then
            ws.Cells.GetSubrangeAbsolute(4, tempMergeClose - 1, 4, iCOL - 1).Merged = True
        End If


        Dim cellweekName As String = String.Empty
        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 1
                If colLoop = 1 Then
                    CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Left
                Else
                    CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
                End If

                CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center

                If dsExcel.Tables(0).Columns(colLoop).ColumnName.Contains("_") = True Then

                    arInfo = dsExcel.Tables(0).Columns(colLoop).ColumnName.Split(splitter)
                    cellweekName = arInfo(0)
                Else
                    cellweekName = ""
                End If


                If htcellColorsbottom.Contains(cellweekName) Then
                    CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml(CStr(htcellColorsbottom(cellweekName))))
                Else
                    CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#f2f2f2"))
                End If

                CellStyle.Font.Weight = ExcelFont.BoldWeight
                CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#455798")

                If colLoop = 0 Or colLoop = 1 Or colLoop = 2 Then
                    CellStyle.Font.Size = 10 * 20
                Else
                    CellStyle.Font.Size = 10 * 14
                End If

                CellStyle.Font.Name = "Verdana"
                CellStyle.WrapText = True

                CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
                ws.Cells(6 + rowLoop, colLoop).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(6 + rowLoop, colLoop).Style = CellStyle

            Next
        Next






    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        CallReport()
    End Sub

    Private Function getBsu_details() As SqlDataReader
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        getBsu_details = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "TT.GETSchool_detials", PARAM)

    End Function
End Class
