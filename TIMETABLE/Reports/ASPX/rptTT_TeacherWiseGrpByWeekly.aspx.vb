﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_Reports_ASPX_rptTT_TeacherWiseGrpByWeekly
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Shared Function GetDate(ByVal vDate As String) As String
        If vDate = "" OrElse CDate(vDate) = New Date(1900, 1, 1) Then
            Return "-"
        Else
            Return Format(CDate(vDate), OASISConstants.DateFormat)
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            'Session("sbsuid") = "315888"

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Then ' Or (ViewState("MainMnu_code") <> "TT150515") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                Session("EMP_SEL_COND") = " AND  EMP_BSU_ID='" & Session("sBsuid") & "' " _
        & " AND (EMP_bACTIVE = 1 or (month(isnull(EMP_LASTATTDT,getdate()))= month(getdate()) and year( isnull(EMP_LASTATTDT,getdate()))=year(getdate()))) " _
        & " AND EMP_ECT_ID != 2"

                Session("StudentFilter") = Nothing
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                bindAcademic_Year()

            End If
            gvEMPName.Attributes.Add("bordercolor", "#1b80b6")
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " &
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " &
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Private Function GetSelectedStudents_LIST(Optional ByVal vSTU_IDs As String = "") As DataSet
        Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " &
         " FROM  [TT].[vw_STUDENT_LIST_ANYYEAR] "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_TIMETABLEConnectionString, CommandType.Text, str_sql)
    End Function

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim CYCLE As String = Nothing
        If Not Session("StudentFilter") Is Nothing Then
            For iIndex = 0 To Session("StudentFilter").Rows.Count - 1
                If CYCLE Is Nothing Then
                    CYCLE += Session("StudentFilter").Rows(iIndex)("Description") & "_" & Format(Session("StudentFilter").Rows(iIndex)("FromDate"), "dd/MMM/yyyy") & "|" & Format(Session("StudentFilter").Rows(iIndex)("ToDate"), "dd/MMM/yyyy")
                Else : CYCLE += "$" & Session("StudentFilter").Rows(iIndex)("Description") & "_" & Format(Session("StudentFilter").Rows(iIndex)("FromDate"), "dd/MMM/yyyy") & "|" & Format(Session("StudentFilter").Rows(iIndex)("ToDate"), "dd/MMM/yyyy")
                End If
            Next
        End If
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@EMP_IDS", IIf(h_EMPID.Value.Replace("||", "|") = "", System.DBNull.Value, h_EMPID.Value.Replace("||", "|")))
        param.Add("Show_Room", IIf(chkRoom.Checked = True, True, False))
        param.Add("Show_Class", IIf(chkClass.Checked = True, True, False))
        param.Add("Show_Subject", IIf(chkSubject.Checked = True, True, False))
        param.Add("IMG_TYPE", "LOGO")
        param.Add("INFO_TYPE", "STAFF")
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@CYCLE", CYCLE)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_TIMETABLE"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptTT_TeacherWiseGrpByWeekly.rpt")

        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


#Region "Filter Details"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If txtDiscription.Text.Trim <> "" Or txtFromDate.Text.Trim() <> "" Or txtToDate.Text.Trim <> "" Then
            If txtDiscription.Text.Trim <> "" AndAlso txtFromDate.Text.Trim() <> "" AndAlso txtToDate.Text.Trim <> "" Then
                If Session("StudentFilter") Is Nothing Then
                    Session("StudentFilter") = CreateDocDetailsTable()
                End If
                Dim dtFilterDetails As DataTable = Session("StudentFilter")
                Dim status As String = String.Empty
                If txtFromDate.Text.Trim() <> "" AndAlso txtToDate.Text.Trim <> "" Then
                    If CDate(txtFromDate.Text) > CDate(txtToDate.Text) Then
                        lblError.Text = "To Date should be less than From Date"
                        GridBindFilterDetails()
                        Exit Sub
                    End If
                End If

                Dim i As Integer
                For i = 0 To dtFilterDetails.Rows.Count - 1
                    '    If Session("FilterEditID") IsNot Nothing And
                    'Session("FilterEditID") <> dtFilterDetails.Rows(i)("UniqueID") Then
                    If dtFilterDetails.Rows(i)("Description") = txtDiscription.Text And
                     dtFilterDetails.Rows(i)("FromDate") = CDate(txtFromDate.Text.Trim) And
                     dtFilterDetails.Rows(i)("ToDate") = CDate(txtToDate.Text.Trim) Then
                        lblError.Text = "Cannot add details.The entered details are repeating."
                        GridBindFilterDetails()
                        Exit Sub
                    End If
                    'End If
                Next
                If btnAdd.Text = "Add" Then
                    Dim newDR As DataRow = dtFilterDetails.NewRow()
                    newDR("UniqueID") = gvFilterDetails.Rows.Count()
                    newDR("Description") = txtDiscription.Text
                    If txtFromDate.Text <> "" Then
                        newDR("FromDate") = CDate(txtFromDate.Text.Trim)
                    End If
                    If txtToDate.Text <> "" Then
                        newDR("ToDate") = CDate(txtToDate.Text.Trim)
                    End If
                    newDR("Status") = "Insert"
                    status = "Insert"
                    lblError.Text = ""
                    dtFilterDetails.Rows.Add(newDR)
                    btnAdd.Text = "Add"
                ElseIf btnAdd.Text = "Modify" Then
                    Dim iIndex As Integer = 0
                    Dim str_Search As String = Session("FilterEditID")
                    For iIndex = 0 To dtFilterDetails.Rows.Count - 1
                        If str_Search = dtFilterDetails.Rows(iIndex)("UniqueID") And dtFilterDetails.Rows(iIndex)("Status") & "" <> "Deleted" Then
                            dtFilterDetails.Rows(iIndex)("Description") = txtDiscription.Text
                            If txtFromDate.Text.Trim <> "" Then
                                dtFilterDetails.Rows(iIndex)("FromDate") = txtFromDate.Text.Trim
                            End If
                            If txtToDate.Text.Trim <> "" Then
                                dtFilterDetails.Rows(iIndex)("ToDate") = txtToDate.Text.Trim
                            End If
                            If dtFilterDetails.Rows(iIndex)("Status").ToString.ToUpper <> "INSERT" Then
                                dtFilterDetails.Rows(iIndex)("Status") = "Edit"
                            End If
                            Exit For
                        End If
                    Next
                    btnAdd.Text = "Add"

                End If
                ClearFilterDetails()

                'If ViewState("datamode") = "EDIT" Then
                '    If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, status, Page.User.Identity.Name.ToString, Page.Master.FindControl("cphMasterpage")) = 0 Then
                '        ClearFilterDetails()
                '    Else
                '        UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                '    End If
                'End If
                Session("StudentFilter") = dtFilterDetails
                GridBindFilterDetails()
            Else lblError.Text = "Please enter Description, From Date & To Date"
            End If
        Else lblError.Text = "Add atleast one record"
        End If
    End Sub

    Private Function CreateDocDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cDiscription As New DataColumn("Description", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.DateTime"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.DateTime"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(cDiscription)
            dtDt.Columns.Add(cFromDate)
            dtDt.Columns.Add(cToDate)
            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Protected Sub btnDocCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearFilterDetails()
    End Sub

    Sub ClearFilterDetails()
        txtDiscription.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        btnAdd.Text = "Add"
        Session.Remove("StudentFilter")
    End Sub

    Protected Sub lnkDocEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        'h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("StudentFilter").Rows.Count - 1
            If str_Search = Session("StudentFilter").Rows(iIndex)("UniqueID") And Session("StudentFilter").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDiscription.Text = Session("StudentFilter").Rows(iIndex)("Description")
                If Session("StudentFilter").Rows(iIndex)("FromDate") Is Nothing OrElse
                Session("StudentFilter").Rows(iIndex)("FromDate").ToString = "" OrElse
                Session("StudentFilter").Rows(iIndex)("FromDate") = New Date(1900, 1, 1) Then
                    txtFromDate.Text = ""
                Else
                    txtFromDate.Text = Format(Session("StudentFilter").Rows(iIndex)("FromDate"), OASISConstants.DateFormat)
                End If
                If Session("StudentFilter").Rows(iIndex)("ToDate") Is Nothing OrElse
                Session("StudentFilter").Rows(iIndex)("ToDate").ToString = "" OrElse
                Session("StudentFilter").Rows(iIndex)("ToDate") = New Date(1900, 1, 1) Then
                    txtToDate.Text = ""
                Else
                    txtToDate.Text = Format(Session("StudentFilter").Rows(iIndex)("ToDate"), OASISConstants.DateFormat)
                End If
                gvFilterDetails.SelectedIndex = iIndex
                btnAdd.Text = "Modify"
                Session("FilterEditID") = str_Search
                UtilityObj.beforeLoopingControls(Page.Master.FindControl("cphMasterpage"))
                Exit For
            End If
        Next
    End Sub

    Protected Sub gvFilterDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFilterDetails.RowDeleting
        Dim categoryID As Integer = e.RowIndex
        DeleteRowFilterDetails(categoryID)
    End Sub

    Private Sub DeleteRowFilterDetails(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim lblTid As New Label
        lblTid = TryCast(gvFilterDetails.Rows(pId).FindControl("lblId"), Label)
        Dim uID As Integer
        If lblTid IsNot Nothing Then uID = CInt(lblTid.Text) Else Exit Sub
        For iRemove = 0 To Session("StudentFilter").Rows.Count - 1
            If (Session("StudentFilter").Rows(iRemove)("UniqueID") = uID) Then
                Session("StudentFilter").Rows(iRemove)("Status") = "DELETED"
                'If UtilityObj.operOnAudiTable(ViewState("MnuCode"), txtEmpno1.Text, "Delete", Page.User.Identity.Name.ToString, Page.Master.FindControl("cphMasterpage")) = 0 Then
                GridBindFilterDetails()
                'Else
                '    UtilityObj.Errorlog("Could not update ErrorLog in Experiece Tab")
                'End If
                Exit For
            End If
        Next
    End Sub

    Private Sub GridBindFilterDetails()
        If Session("StudentFilter") Is Nothing Then
            gvFilterDetails.DataSource = Nothing
            gvFilterDetails.DataBind()
            Return
        End If
        Dim strColumnName As String = String.Empty
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateDocDetailsTable()
        If Session("StudentFilter").Rows.Count > 0 Then
            For i = 0 To Session("StudentFilter").Rows.Count - 1
                If (Session("StudentFilter").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("StudentFilter").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        ldrTempNew.Item(strColumnName) = Session("StudentFilter").Rows(i)(strColumnName)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        gvFilterDetails.DataSource = dtTempDtl
        gvFilterDetails.DataBind()
    End Sub

#End Region

    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = h_EMPID.Value.Replace("||", ",")
        FillEmpNames(h_EMPID.Value)
        txtEMPNAME.Text = ""
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblEmp_ID"), Label)
        If Not lblACTID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If

    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMPNO as ID,EMP_ID, ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub gvEMPName_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillEmpNames(h_EMPID.Value)
    End Sub
End Class
