﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_rptTT_StudentWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT150515") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                bindAcademic_Year()

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        bindGRADE()
    End Sub
    Sub bindGRADE()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString

            Dim ds As New DataSet
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sbsuid").ToString)
            PARAM(1) = New SqlParameter("@ACD_ID", ddlAca_Year.SelectedValue)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[TM].[GETRPT_GRADES]", PARAM)
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "CLASS"
            ddlGrade.DataValueField = "CLASS_ID"
            ddlGrade.DataBind()
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        bindSECTION()
    End Sub
    Sub bindSECTION()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString

            Dim ds As New DataSet

            If ddlGrade.SelectedValue.Contains("TEMP_") = True Then
                ddlSection.Items.Clear()
                ddlSection.Items.Add(New ListItem("-- Empty ---", "0"))
            Else
                Dim PARAM(2) As SqlParameter
                PARAM(0) = New SqlParameter("@BSU_ID", Session("sbsuid").ToString)
                PARAM(1) = New SqlParameter("@ACD_ID", ddlAca_Year.SelectedValue)
                PARAM(2) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[TM].[GETRPT_SECTION]", PARAM)
                ddlSection.Items.Clear()
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
                ddlSection.Items.Add(New ListItem("-- All ---", "0"))
                ddlSection.ClearSelection()
                ddlSection.Items.FindByValue("0").Selected = True
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = GetSelectedStudents_LIST(vSTU_IDs)
        grdStudent.DataBind()
    End Sub
    Private Function GetSelectedStudents_LIST(Optional ByVal vSTU_IDs As String = "") As DataSet
        Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " & _
         " FROM  [TT].[vw_STUDENT_LIST_ANYYEAR] "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_TIMETABLEConnectionString, CommandType.Text, str_sql)
    End Function
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@SCT_ID", IIf(ddlSection.SelectedValue = "0", System.DBNull.Value, ddlSection.SelectedValue))
        param.Add("@STU_IDS", IIf(h_STU_IDs.Value.Replace("___", "|") = "", System.DBNull.Value, h_STU_IDs.Value.Replace("___", "|")))
        param.Add("Show_Room", IIf(chkRoom.Checked = True, True, False))
        param.Add("Show_Teacher", IIf(chkTeacher.Checked = True, True, False))
        param.Add("Show_Subject", IIf(chkSubject.Checked = True, True, False))
        param.Add("IMG_TYPE", "LOGO")
        param.Add("INFO_TYPE", "STUDENT")
        param.Add("Grade", ddlGrade.SelectedItem.Text)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_TIMETABLE"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptTT_StudentWise.rpt")

        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub h_STU_IDs_ValueChanged(sender As Object, e As EventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
End Class
