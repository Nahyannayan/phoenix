﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="rptTT_Gaps.aspx.vb" Inherits="TIMETABLE_rptTT_Gaps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Timetable Gaps
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table align="center"
                                width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                           
                                    <td align="left" width="20%"> <span class="field-label">Grade   </span>                      
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGRD_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr runat="server" id="tr_SCT" visible="false">
                                    <td align="left" width="20%"><span class="field-label">Section </span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSCT_ID" runat="server">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr runat="server" id="tr_DTL" visible="false">
                                    <td align="left" width="20%"><span class="field-label">Show details </span></td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkTeacher" runat="server" Text="Teacher" CssClass="field-label" />
                                        <asp:CheckBox ID="chkSubject" runat="server" Text="Subject" CssClass="field-label" />
                                        <asp:CheckBox ID="chkRoom" runat="server" Text="Room" CssClass="field-label" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="center" colspan="4">
                                        <asp:Button ID="btnReport"
                                            runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
