﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="rptTT_StudentWise.aspx.vb" Inherits="TIMETABLE_rptTT_StudentWise" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetSTUDENTS() {

            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            result = radopen("../../tblPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "pop_up");
            //if (result != '' && result != undefined) {
            //   //NameandCode[0];
            //}
            //else {
            //    return false;
            //}
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
              
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = arg.NameandCode;
               __doPostBack('<%= h_STU_IDs.ClientID%>', 'ValueChanged');
            }
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Student Timetable
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" align="center">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                   
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Section</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                   
                        <td align="left" width="20%" valign="top"><span class="field-label">Student</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudIDs" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS(); return false;"  OnClick="imgStudent_Click"></asp:ImageButton>
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                PageSize="5" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>


                    <tr>
                        <td align="left" width="20%"><span class="field-label">Show details</span></td>

                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkTeacher" runat="server" Text="Teacher" CssClass="field-label" />
                            <asp:CheckBox ID="chkSubject" runat="server" Text="Subject"  CssClass="field-label"/>
                            <asp:CheckBox ID="chkRoom" runat="server" Text="Room" CssClass="field-label"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server"  />
                <asp:HiddenField ID="h_STU_IDs" runat="server" OnValueChanged="h_STU_IDs_ValueChanged" />
            </div>
        </div>
    </div>
</asp:Content>
