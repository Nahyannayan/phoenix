﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="rptTT_TeacherWise.aspx.vb" Inherits="TIMETABLE_rptTT_TeacherWise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function SelectAllTR(CheckBox) {
            TotalChkBx = parseInt('<%=gvTT_Teacher.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvTT_Teacher.ClientID %>');
            var TargetChildControl = "chkSelectTR";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderTR(CheckBox) {
            TotalChkBx = parseInt('<%=gvTT_Teacher.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvTT_Teacher.ClientID %>');
            var TargetChildControl = "chkSelectTR";
            var TargetHeaderControl = "chkSelectAllTR";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Teachers Timetable
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table align="center"
                                width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Show details</span></td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkSubject" runat="server" Text="Subject" CssClass="field-label" />
                                        <asp:CheckBox ID="chkClass" runat="server" Text="Class" CssClass="field-label" />
                                        <asp:CheckBox ID="chkRoom" runat="server" Text="Room" CssClass="field-label" />
                                    </td>
                                </tr>

                            </table>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server"  Width="100%">
                                            <asp:GridView ID="gvTT_Teacher" runat="server"
                                                AutoGenerateColumns="False"
                                                EmptyDataText="No  room added yet"
                                                CssClass="table table-bordered table-row"
                                                Width="100%" EnableModelValidation="True">
                                                <RowStyle CssClass="griditem" />
                                                <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectTR" runat="server"></asp:CheckBox>

                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAllTR" runat="server" onclick="SelectAllTR(this);"
                                                                ToolTip="Click here to select/deselect all rows" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle Width="30px" />
                                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Sr.No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Short Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSHORTNAME" runat="server"
                                                                Text='<%# BIND("SHORTNAME") %>'></asp:Label>
                                                            <asp:Label ID="lblEmp_id" runat="server"
                                                                Text='<%# BIND("TTM_EMP_ID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Full Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFULLNAME" runat="server" Text='<%# bind("FULLNAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle BackColor="Khaki" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>

                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="center">
                                        <br />
                                        <asp:Button ID="btnReport"
                                            runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
