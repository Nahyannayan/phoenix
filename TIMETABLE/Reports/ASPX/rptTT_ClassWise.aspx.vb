﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_rptTT_ClassWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT150510") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                bindAcademic_Year()






            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlACD_ID.Items.Clear()
            ddlAcd_id.DataSource = ds.Tables(0)
            ddlAcd_id.DataTextField = "ACY_DESCR"
            ddlAcd_id.DataValueField = "ACD_ID"
            ddlAcd_id.DataBind()
            ddlAcd_id.ClearSelection()
            ddlAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcd_id_SelectedIndexChanged(ddlAcd_id, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcd_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcd_id.SelectedIndexChanged
        bindGRADE()
    End Sub
    Sub bindGRADE()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString

            Dim ds As New DataSet
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sbsuid").ToString)
            PARAM(1) = New SqlParameter("@ACD_ID", ddlACD_ID.SelectedValue)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[TM].[GETRPT_CLASS]", PARAM)
            ddlGRD_ID.Items.Clear()
            ddlGRD_ID.DataSource = ds.Tables(0)
            ddlGRD_ID.DataTextField = "CLASS"
            ddlGRD_ID.DataValueField = "CLASS_ID"
            ddlGRD_ID.DataBind()
            ddlGRD_ID.Items.Add(New ListItem("-- All ---", "0"))

            ddlGRD_ID.ClearSelection()
            ddlGRD_ID.Items.FindByValue("0").Selected = True

            ddlGRD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
  
    Protected Sub ddlGRD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGRD_ID.SelectedIndexChanged
        bindSECTION()
    End Sub
    Sub bindSECTION()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString

            Dim ds As New DataSet
           
            If ddlGRD_ID.SelectedValue.Contains("TEMP_") = True Then
                ddlSCT_ID.Items.Clear()
                ddlSCT_ID.Items.Add(New ListItem("-- Empty ---", "0"))
            Else
                Dim PARAM(2) As SqlParameter
                PARAM(0) = New SqlParameter("@BSU_ID", Session("sbsuid").ToString)
                PARAM(1) = New SqlParameter("@ACD_ID", ddlACD_ID.SelectedValue)
                PARAM(2) = New SqlParameter("@GRD_ID", ddlGRD_ID.SelectedValue)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[TM].[GETRPT_SECTION]", PARAM)
                ddlSCT_ID.Items.Clear()
                ddlSCT_ID.DataSource = ds.Tables(0)
                ddlSCT_ID.DataTextField = "SCT_DESCR"
                ddlSCT_ID.DataValueField = "SCT_ID"
                ddlSCT_ID.DataBind()
                ddlSCT_ID.Items.Add(New ListItem("-- All ---", "0"))
                ddlSCT_ID.ClearSelection()
                ddlSCT_ID.Items.FindByValue("0").Selected = True
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        CallReport()
    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@ACD_ID", ddlACD_ID.SelectedValue)
        param.Add("@GRD_ID", IIf(ddlGRD_ID.SelectedValue = "0", System.DBNull.Value, ddlGRD_ID.SelectedValue))
        param.Add("@SCT_ID", IIf(ddlSCT_ID.SelectedValue = "0", System.DBNull.Value, ddlSCT_ID.SelectedValue))
        param.Add("IMG_TYPE", "LOGO")
        param.Add("INFO_TYPE", "CLASS")
        param.Add("Show_Room", IIf(chkRoom.Checked = True, True, False))
        param.Add("Show_Teacher", IIf(chkTeacher.Checked = True, True, False))
        param.Add("Show_Subject", IIf(chkSubject.Checked = True, True, False))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_TIMETABLE"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptTT_ClassWise.rpt")

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
