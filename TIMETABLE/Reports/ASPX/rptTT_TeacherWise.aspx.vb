﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_rptTT_TeacherWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
           
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT150505") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                bindAcademic_Year()






            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

  
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlACD_ID.Items.Clear()
            ddlAcd_id.DataSource = ds.Tables(0)
            ddlAcd_id.DataTextField = "ACY_DESCR"
            ddlAcd_id.DataValueField = "ACD_ID"
            ddlAcd_id.DataBind()
            ddlAcd_id.ClearSelection()
            ddlAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcd_id_SelectedIndexChanged(ddlAcd_id, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcd_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcd_id.SelectedIndexChanged
        bindMasterData()
    End Sub

    Sub bindMasterData()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter
           
                ACD_ID = ddlAcd_id.SelectedValue


            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "TT.RPTGetEmployee_List", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvTT_Teacher.DataSource = ds.Tables(0)
                gvTT_Teacher.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvTT_Teacher.HeaderRow.FindControl("chkSelectAllRM"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvTT_Teacher.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectRM"), CheckBox)

                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next


               

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvTT_Teacher.DataSource = ds.Tables(0)
                Try
                    gvTT_Teacher.DataBind()
                 
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTT_Teacher.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvTT_Teacher.Rows(0).Cells.Clear()
                gvTT_Teacher.Rows(0).Cells.Add(New TableCell)
                gvTT_Teacher.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTT_Teacher.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTT_Teacher.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvTT_Teacher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTT_Teacher.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderTR(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try

    End Sub
   
    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        CallReport()
    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim EMP_IDS As New StringBuilder

        For Each row As GridViewRow In gvTT_Teacher.Rows
            Dim chkSelectTR = DirectCast(row.FindControl("chkSelectTR"), CheckBox)
            Dim lblEmp_id As Label
            If Not chkSelectTR Is Nothing Then
                If chkSelectTR.Checked = True Then
                    lblEmp_id = DirectCast(row.FindControl("lblEmp_id"), Label)
                    EMP_IDS.Append(lblEmp_id.Text & "|")
                End If
            End If
        Next


        If EMP_IDS.ToString() = "" Then

            lblErr.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1B80B6;padding:5pt;background-color:#eaebec;'>Please select the teacher</div>"
            Exit Sub
        End If



        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@ACD_ID", ddlACD_ID.SelectedValue)
        param.Add("@EMP_IDS", EMP_IDS.ToString)
        param.Add("Show_Room", IIf(chkRoom.Checked = True, True, False))
        param.Add("Show_Class", IIf(chkClass.Checked = True, True, False))
        param.Add("Show_Subject", IIf(chkSubject.Checked = True, True, False))
        param.Add("IMG_TYPE", "LOGO")
        param.Add("INFO_TYPE", "STAFF")
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_TIMETABLE"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptTT_TeacherWise.rpt")

        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
