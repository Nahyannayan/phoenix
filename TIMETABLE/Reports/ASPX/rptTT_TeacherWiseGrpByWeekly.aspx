﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTT_TeacherWiseGrpByWeekly.aspx.vb" Inherits="TIMETABLE_Reports_ASPX_rptTT_TeacherWiseGrpByWeekly" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetEMPNAME() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 445px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "../../../Common/PopupFormThreeIDHidden.aspx?ID=EMP&multiSelect=true";
            var oWnd = radopen(url, "pop_emp");
            //            result = window.showModalDialog("reports\/aspx\/SelIDDESC.aspx?ID=EMP","", sFeatures)            
            <%--result = window.showModalDialog("../Common/PopupFormThreeIDHidden.aspx?ID=EMP&multiSelect=true", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMPID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                debugger;
                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_EMPID.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtEMPNAME.ClientID%>').value = NameandCode;
                  __doPostBack('<%=txtEMPNAME.ClientID%>', 'TextChanged');
            }
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Teacher Timetable
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" align="center">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="top"><span class="field-label">Teacher</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEMPNAME" runat="server" OnTextChanged="txtEMPNAME_TextChanged" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPNAME();return false;" /><br />
                            <asp:GridView CssClass="table table-bordered table-row" ID="gvEMPName" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="15" SkinID="GridViewNormal">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMPLOYEE NO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                            <asp:Label ID="lblEmp_ID" runat="server" Text='<%# Bind("EMP_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="EMPLOYEE NAME" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Show details</span></td>

                        <td align="left" width="30%">
                           <asp:CheckBox ID="chkSubject" runat="server" Text="Subject" CssClass="field-label" />
                                        <asp:CheckBox ID="chkClass" runat="server" Text="Class" CssClass="field-label" />
                                        <asp:CheckBox ID="chkRoom" runat="server" Text="Room" CssClass="field-label" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="title-bg" colspan="4">Weekly Cycle</td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Descritpion</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtDiscription" runat="server"></asp:TextBox>

                        </td>

                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">From Date</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtFromDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(7,0);" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFromDate"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left"><span class="field-label">From Date</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgtxtToDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(7,0);" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDate"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                            <br />
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:GridView ID="gvFilterDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet." CssClass="table table-bordered table-row"
                                Width="100%">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Description" HeaderText="Document" />

                                    <%-- <asp:BoundField DataField="FromDate" HeaderText="From Date" />
                                    <asp:BoundField DataField="ToDate" HeaderText="To Date" />--%>
                                    <asp:TemplateField HeaderText="Issue Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# GetDate(Container.DataItem("FromDate")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exp. Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# GetDate(Container.DataItem("ToDate")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDocEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                OnClick="lnkDocEdit_Click" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <ajaxToolkit:CalendarExtender ID="From" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgtxtFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="To" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgtxtToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                        </td>
                    </tr>
                </table>
                 <asp:HiddenField ID="h_EMPID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

