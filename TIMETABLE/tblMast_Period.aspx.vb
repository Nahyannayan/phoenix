Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAtt_room_Grade_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                Dim menu_rights As String
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub


   

    Private Sub gridbind()
        Try



            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_GRM_DIS As String = String.Empty
            Dim ds As New DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PM.GETBSU_GRADES", param)

            If ds.Tables(0).Rows.Count > 0 Then

                gvPeriod.DataSource = ds.Tables(0)
                gvPeriod.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())


                gvPeriod.DataSource = ds.Tables(0)
                Try
                    gvPeriod.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPeriod.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPeriod.Rows(0).Cells.Clear()
                gvPeriod.Rows(0).Cells.Add(New TableCell)
                gvPeriod.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPeriod.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPeriod.Rows(0).Cells(0).Text = "No record available !!!."
            End If




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
   
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblGRD_ID As New Label
            Dim viewid As String
            Dim URL As String = String.Empty
            lblGRD_ID = TryCast(sender.FindControl("lblGRD_ID"), Label)

            viewid = lblGRD_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\TIMETABLE\tblMast_PeriodEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

 

    Protected Sub gvPeriod_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPeriod.RowDataBound
        
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString


            Dim lblGRD_ID As Label = DirectCast(e.Row.FindControl("lblGRD_ID"), Label)
            Dim LTGRD As Literal = DirectCast(e.Row.FindControl("LTGRD"), Literal)
            Dim STR_TBL As String = "<table width='100%'  align='center' border='1' bordercolor='#1b80b6' cellpadding='5' cellspacing='0' Style = 'border-collapse:collapse;'><tr class='matters' align='center'><td >Week Name</td><td>Periods</td><td>Period Label</td></tr>"

      
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@GRD_ID", lblGRD_ID.Text)
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PM.GETGRADE_PERIOD_LABEL", param)
                If datareader.HasRows = True Then

                    While datareader.Read

                        STR_TBL += "<tr class='matters' align='center'><td>" & Convert.ToString(datareader("PGW_WEEKNAME")) & "</td><td> " & Convert.ToString(datareader("PM_DESCR")) & "</td><td>" & Convert.ToString(datareader("PGW_LABEL")) & "</td></tr>"

                    End While
                    LTGRD.Text = STR_TBL + "</table>"

                Else

                    LTGRD.Text = STR_TBL + "<tr><td colspan='3' class='matters' align='center'>No record available !!!</td></tr></table>"
                End If

            End Using


        End If
    End Sub
End Class
