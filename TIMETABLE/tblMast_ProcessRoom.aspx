﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tblMast_ProcessRoom.aspx.vb" Inherits="TIMETABLE_tblMast_ProcessRoom" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="ucMasterProcess/Room/ucProRoom_Add.ascx" TagName="ProcRoom_Add" TagPrefix="ucpRADD" %>
<%@ Register Src="ucMasterProcess/Room/ucProRoom_Map.ascx" TagName="ProcRoom_Map" TagPrefix="ucpRMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function validate(GridView1) {
            var flag = false;
            var dropdowns = new Array(); //Create array to hold all the dropdown lists.
            var imgArr = new Array(); //Create array to hold all the dropdown lists.  
            var gridview = document.getElementById(GridView1); //GridView1 is the id of ur gridview.
            dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
            var imgArr = gridview.getElementsByTagName('img');

            for (var i = 0; i < dropdowns.length; i++) {
                if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                {
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowgreen.png"
                    flag = true;
                }
                else {
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowred.png"

                }
            }

        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="ROOM MASTER"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%--  <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" width="50%">
               </td>
        </tr>
    </table>--%>
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left" colspan="4">                            
                                    <asp:Label ID="lblErr" runat="server" EnableViewState="false" CssClass="error"></asp:Label>                              
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>                             
                        </td>

                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select the option </span></td>
                        <td  align="left" width="30%">
                            <asp:RadioButton ID="rbAddMissing"
                                runat="server" GroupName="room" Text="Add Missing Rooms" Checked="true" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbAddMap"
                                runat="server" GroupName="room" Text="Map Rooms" AutoPostBack="True"  CssClass="field-label"/>
                        </td>
                        <td></td> 
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <ucpRADD:ProcRoom_Add ID="ProcRooms_Add_ctr" runat="server"></ucpRADD:ProcRoom_Add>
                            <ucpRMap:ProcRoom_Map ID="ProcRoom_Map_ctr" runat="server"></ucpRMap:ProcRoom_Map>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

