﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Masterexp.aspx.vb" Inherits="TIMETABLE_ucExport_Masterexp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/CssPageStyle.css" rel="stylesheet" type="text/css" />--%>
    <link href="/PHOENIXBETAV2/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/PHOENIXBETAV2/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function SelectAllRM(CheckBox) {
            TotalChkBx = parseInt('<%=gvRoomExport.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvRoomExport.ClientID %>');
            var TargetChildControl = "chkSelectRM";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderRM(CheckBox) {
            TotalChkBx = parseInt('<%=gvRoomExport.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvRoomExport.ClientID %>');
            var TargetChildControl = "chkSelectRM";
            var TargetHeaderControl = "chkSelectAllRM";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }



        function SelectAllCL(CheckBox) {
            TotalChkBx = parseInt('<%=gvClassExport.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvClassExport.ClientID %>');
            var TargetChildControl = "chkSelectCL";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderCL(CheckBox) {
            TotalChkBx = parseInt('<%=gvClassExport.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvClassExport.ClientID %>');
            var TargetChildControl = "chkSelectCL";
            var TargetHeaderControl = "chkSelectAllCL";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }



        function SelectAllTR(CheckBox) {
            TotalChkBx = parseInt('<%=gvTeacherExport.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvTeacherExport.ClientID %>');
            var TargetChildControl = "chkSelectTR";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderTR(CheckBox) {
            TotalChkBx = parseInt('<%=gvTeacherExport.Rows.Count %>');
              var TargetBaseControl = document.getElementById('<%=gvTeacherExport.ClientID %>');
              var TargetChildControl = "chkSelectTR";
              var TargetHeaderControl = "chkSelectAllTR";
              var Inputs = TargetBaseControl.getElementsByTagName("input");
              var flag = false;
              var HeaderCheckBox;
              for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                  if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                      HeaderCheckBox = Inputs[iCount];
                  if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                      if (CheckBox.checked) {
                          if (!Inputs[iCount].checked) {
                              flag = false;
                              HeaderCheckBox.checked = false;
                              return;
                          }
                          else
                              flag = true;
                      }
                      else if (!CheckBox.checked)
                          HeaderCheckBox.checked = false;
                  }
              }
              if (flag)
                  HeaderCheckBox.checked = CheckBox.checked
          }



          function SelectAllSB(CheckBox) {
              TotalChkBx = parseInt('<%=gvSubjectExport.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvSubjectExport.ClientID %>');
            var TargetChildControl = "chkSelectSB";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderSB(CheckBox) {
            TotalChkBx = parseInt('<%=gvSubjectExport.Rows.Count %>');
     var TargetBaseControl = document.getElementById('<%=gvSubjectExport.ClientID %>');
     var TargetChildControl = "chkSelectSB";
     var TargetHeaderControl = "chkSelectAllSB";
     var Inputs = TargetBaseControl.getElementsByTagName("input");
     var flag = false;
     var HeaderCheckBox;
     for (var iCount = 0; iCount < Inputs.length; ++iCount) {
         if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
             HeaderCheckBox = Inputs[iCount];
         if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
             if (CheckBox.checked) {
                 if (!Inputs[iCount].checked) {
                     flag = false;
                     HeaderCheckBox.checked = false;
                     return;
                 }
                 else
                     flag = true;
             }
             else if (!CheckBox.checked)
                 HeaderCheckBox.checked = false;
         }
     }
     if (flag)
         HeaderCheckBox.checked = CheckBox.checked
 }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellpadding="8" cellspacing="0">
            <tr valign="middle">
                <td align="left" valign="middle" colspan="2">
                    <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal>
                </td>
            </tr>
            <tr valign="top">
                <td align="left" valign="top" width="20%">
                    <span class="field-label">Choose the Master Data </span>
                </td>
                <td width="80%">
                    <asp:RadioButton ID="rbRooms" runat="server" CssClass="field-label"
                        Text="Room" Checked="true" GroupName="grp" AutoPostBack="True" />
                    <asp:RadioButton ID="rbClass" runat="server" Text="Class" GroupName="grp"
                        AutoPostBack="True" CssClass="field-label" />
                    <asp:RadioButton ID="rbTeacher" CssClass="field-label"
                        runat="server" Text="Teacher" GroupName="grp" AutoPostBack="True" />
                    <asp:RadioButton ID="rbSubject" runat="server" Text="Subject" GroupName="grp"
                        AutoPostBack="True" CssClass="field-label" />
                </td>
            </tr>
            <tr valign="top" style="margin: 0px; padding: 0px;">
                <td align="left" valign="top" colspan="2" >
                    <div id="divusrInfo" runat="server" class="panel-cover"></div>
                </td>
            </tr>
            <tr valign="top" id="tbl_acd" runat="server">
                <td align="left" valign="top">
                    <span class="field-label">Academic year</span>
                </td>
                <td>
                    <asp:DropDownList ID="ddlAcd_id" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="plRoom" runat="server"  Width="100%">
                        <asp:GridView ID="gvRoomExport" runat="server"
                            AutoGenerateColumns="False"
                            EmptyDataText="No  room added yet"
                            CssClass="table table-bordered table-row"
                            Width="100%">
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                            <Columns>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelectRM" runat="server"></asp:CheckBox>

                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAllRM" runat="server" onclick="SelectAllRM(this);"
                                            ToolTip="Click here to select/deselect all rows" />
                                    </HeaderTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSHORTNAME" runat="server"
                                            Text='<%# BIND("SHORTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Full Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFULLNAME" runat="server" Text='<%# bind("FULLNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <SelectedRowStyle BackColor="Khaki" />
                            <HeaderStyle CssClass="gridheader_new" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </asp:Panel>

                    <asp:Panel ID="plClass" runat="server"  Width="100%">
                        <asp:GridView ID="gvClassExport" runat="server"
                            AutoGenerateColumns="False"
                            EmptyDataText="No class added yet"
                            Width="100%">
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                            <Columns>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelectCL" runat="server"></asp:CheckBox>

                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAllCL" runat="server" onclick="SelectAllCL(this);"
                                            ToolTip="Click here to select/deselect all rows" />
                                    </HeaderTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Name (Auto Generated)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSHORTNAME" runat="server"
                                            Text='<%# BIND("SHORTNAME") %>'></asp:Label>
                                    </ItemTemplate>

                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Full Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFULLNAME" runat="server" Text='<%# bind("FULLNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="Khaki" />
                            <HeaderStyle CssClass="gridheader_new" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </asp:Panel>

                    <asp:Panel ID="plTeacher" runat="server"  Width="100%">
                        <asp:GridView ID="gvTeacherExport" runat="server"
                            AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                            EmptyDataText="No Subject added yet"
                            Width="100%">
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                            <Columns>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelectTR" runat="server"></asp:CheckBox>

                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAllTR" runat="server" onclick="SelectAllTR(this);"
                                            ToolTip="Click here to select/deselect all rows" />
                                    </HeaderTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSHORTNAME" runat="server"
                                            Text='<%# BIND("SHORTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Full Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFULLNAME" runat="server" Text='<%# bind("FULLNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastCol_R" runat="server" Text='<%# bind("LAST_COL") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="Khaki" />
                            <HeaderStyle CssClass="gridheader_new" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </asp:Panel>

                    <asp:Panel ID="plSubject" runat="server" Width="100%">
                        <asp:GridView ID="gvSubjectExport" runat="server"
                            AutoGenerateColumns="False"
                            EmptyDataText="No Subject added yet"
                            CssClass="table table-bordered table-row"
                            Width="100%">
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                            <Columns>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelectSB" runat="server" Visible='<%# bind("bSHOW") %>'></asp:CheckBox>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAllSB" runat="server" onclick="SelectAllSB(this);"
                                            ToolTip="Click here to select/deselect all rows" />
                                    </HeaderTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSHORTNAME" runat="server"
                                            Text='<%# BIND("SHORTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Full Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFULLNAME" runat="server" Text='<%# bind("FULLNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSUB_DESCRS" runat="server" Text='<%# bind("SUB_DESCRS") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="Khaki" />
                            <HeaderStyle CssClass="gridheader_new" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="bottom" align="center" colspan="2">
                    <br />
                    <asp:Button ID="btnExportRoom" runat="server" CssClass="button" Text="Export" CausesValidation="False" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
