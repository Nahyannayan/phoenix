﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class TIMETABLE_ucExport_Masterexp
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                bindAcademic_Year()
                BINDMASTER_DATA("0")

                tbl_acd.Visible = False
                callDiv_info("Room")

               

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Private Sub callDiv_info(ByVal Infotype As String)

        If Infotype = "Room" Then
            plClass.Visible = False
            plRoom.Visible = True
            plSubject.Visible = False
            plTeacher.Visible = False

            '  divusrInfo.Attributes.Add("class", "divnote")
            divusrInfo.InnerHtml = "<div>&#149; Use Add / Edit Room menu to add rooms if not listed</div><div>&#149; Select rooms that needs to be exported to Untis</div>"

        ElseIf Infotype = "Class" Then
            plClass.Visible = True
            plRoom.Visible = False
            plSubject.Visible = False
            plTeacher.Visible = False


            ' divusrInfo.Attributes.Add("class", "divnote")
            divusrInfo.InnerHtml = "<div>&#149; Select classes that needs to be exported to Untis</div>"

        ElseIf Infotype = "Subject" Then
            plClass.Visible = False
            plRoom.Visible = False
            plSubject.Visible = True
            plTeacher.Visible = False

            ' divusrInfo.Attributes.Add("class", "divnote")
            divusrInfo.InnerHtml = "<div>&#149; Select unique Subject that needs to be exported to Untis</div>"

        ElseIf Infotype = "Teacher" Then
            plClass.Visible = False
            plRoom.Visible = False
            plSubject.Visible = False
            plTeacher.Visible = True
            '  divusrInfo.Attributes.Add("class", "divnote")
            divusrInfo.InnerHtml = "<div>&#149; Select teachers that needs to be exported to Untis</div>"

        End If

    End Sub

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcd_id.Items.Clear()
            ddlAcd_id.DataSource = ds.Tables(0)
            ddlAcd_id.DataTextField = "ACY_DESCR"
            ddlAcd_id.DataValueField = "ACD_ID"
            ddlAcd_id.DataBind()
            ddlAcd_id.ClearSelection()
            ddlAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcd_id_SelectedIndexChanged(ddlAcd_id, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcd_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcd_id.SelectedIndexChanged
        BINDMASTER_DATA("1")
    End Sub

    Private Sub BINDMASTER_DATA(Optional ByVal FLAG As String = "0")
        bindMasterData_CLASS()
        bindMasterData_SUBJECT()
        If FLAG = 0 Then
            bindMasterData_ROOM()
            bindMasterData_TEACHER()
        End If

    End Sub


    Private Sub bindMasterData_ROOM()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter
            If Session("sBsuid") = "125010" And Session("sUsr_name") = "Lijo" Then
                ACD_ID = 592
            Else
                ACD_ID = ddlAcd_id.SelectedValue
            End If
            'If rbRooms.Checked = True Then
            '    INFO_TYPE = "ROOM"
            'ElseIf rbClass.Checked = True Then
            '    INFO_TYPE = "CLASS"
            'ElseIf rbTeacher.Checked = True Then
            '    INFO_TYPE = "TEACHER"
            'ElseIf rbSubject.Checked = True Then
            '    INFO_TYPE = "SUBJECT"
            'End If

            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(3) = New SqlParameter("@INFO_TYPE", "ROOM")
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "EX.EXPORTOASIS_MASTER", PARAM)
            If ds.Tables(0).Rows.Count > 0 Then
                gvRoomExport.DataSource = ds.Tables(0)
                gvRoomExport.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvRoomExport.HeaderRow.FindControl("chkSelectAllRM"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvRoomExport.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectRM"), CheckBox)
                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvRoomExport.DataSource = ds.Tables(0)
                Try
                    gvRoomExport.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvRoomExport.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvRoomExport.Rows(0).Cells.Clear()
                gvRoomExport.Rows(0).Cells.Add(New TableCell)
                gvRoomExport.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRoomExport.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRoomExport.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub bindMasterData_CLASS()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter
            If Session("sBsuid") = "125010" And Session("sUsr_name") = "Lijo" Then
                ACD_ID = 592
            Else
                ACD_ID = ddlAcd_id.SelectedValue
            End If
            'If rbRooms.Checked = True Then
            '    INFO_TYPE = "ROOM"
            'ElseIf rbClass.Checked = True Then
            '    INFO_TYPE = "CLASS"
            'ElseIf rbTeacher.Checked = True Then
            '    INFO_TYPE = "TEACHER"
            'ElseIf rbSubject.Checked = True Then
            '    INFO_TYPE = "SUBJECT"
            'End If

            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(3) = New SqlParameter("@INFO_TYPE", "CLASS")
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "EX.EXPORTOASIS_MASTER", PARAM)
            If ds.Tables(0).Rows.Count > 0 Then
                gvClassExport.DataSource = ds.Tables(0)
                gvClassExport.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvClassExport.HeaderRow.FindControl("chkSelectAllCL"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvClassExport.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectCL"), CheckBox)
                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvClassExport.DataSource = ds.Tables(0)
                Try
                    gvClassExport.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvClassExport.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvClassExport.Rows(0).Cells.Clear()
                gvClassExport.Rows(0).Cells.Add(New TableCell)
                gvClassExport.Rows(0).Cells(0).ColumnSpan = columnCount
                gvClassExport.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvClassExport.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindMasterData_TEACHER()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter
            If Session("sBsuid") = "125010" And Session("sUsr_name") = "Lijo" Then
                ACD_ID = 592
            Else
                ACD_ID = ddlAcd_id.SelectedValue
            End If
            'If rbRooms.Checked = True Then
            '    INFO_TYPE = "ROOM"
            'ElseIf rbClass.Checked = True Then
            '    INFO_TYPE = "CLASS"
            'ElseIf rbTeacher.Checked = True Then
            '    INFO_TYPE = "TEACHER"
            'ElseIf rbSubject.Checked = True Then
            '    INFO_TYPE = "SUBJECT"
            'End If

            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(3) = New SqlParameter("@INFO_TYPE", "TEACHER")
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "EX.EXPORTOASIS_MASTER", PARAM)
            If ds.Tables(0).Rows.Count > 0 Then
                gvTeacherExport.DataSource = ds.Tables(0)
                gvTeacherExport.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvTeacherExport.HeaderRow.FindControl("chkSelectAllTR"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvTeacherExport.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectTR"), CheckBox)
                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvTeacherExport.DataSource = ds.Tables(0)
                Try
                    gvTeacherExport.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTeacherExport.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvTeacherExport.Rows(0).Cells.Clear()
                gvTeacherExport.Rows(0).Cells.Add(New TableCell)
                gvTeacherExport.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTeacherExport.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTeacherExport.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindMasterData_SUBJECT()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter
            If Session("sBsuid") = "125010" And Session("sUsr_name") = "Lijo" Then
                ACD_ID = 592
            Else
                ACD_ID = ddlAcd_id.SelectedValue
            End If
            'If rbRooms.Checked = True Then
            '    INFO_TYPE = "ROOM"
            'ElseIf rbClass.Checked = True Then
            '    INFO_TYPE = "CLASS"
            'ElseIf rbTeacher.Checked = True Then
            '    INFO_TYPE = "TEACHER"
            'ElseIf rbSubject.Checked = True Then
            '    INFO_TYPE = "SUBJECT"
            'End If

            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(3) = New SqlParameter("@INFO_TYPE", "SUBJECT")
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "EX.EXPORTOASIS_MASTER", PARAM)
            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectExport.DataSource = ds.Tables(0)
                gvSubjectExport.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvSubjectExport.HeaderRow.FindControl("chkSelectAllSB"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvSubjectExport.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectSB"), CheckBox)
                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                ds.Tables(0).Rows(0)("bSHOW") = False
                gvSubjectExport.DataSource = ds.Tables(0)
                Try

                    gvSubjectExport.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectExport.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvSubjectExport.Rows(0).Cells.Clear()
                gvSubjectExport.Rows(0).Cells.Add(New TableCell)
                gvSubjectExport.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectExport.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectExport.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvRoomExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRoomExport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderRM(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectRM"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectRM"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub gvClassExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClassExport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderCL(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectCL"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectCL"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvSubjectExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubjectExport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderSB(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectSB"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectSB"), CheckBox).Attributes.Add("onclick", strScript)
                Dim chkSelectSB As CheckBox = DirectCast(e.Row.Cells(0).FindControl("chkSelectSB"), CheckBox)
                If chkSelectSB.Visible = False Then
                    Dim lblSHORTNAME As Label = DirectCast(e.Row.Cells(0).FindControl("lblSHORTNAME"), Label)
                    Dim lblFULLNAME As Label = DirectCast(e.Row.Cells(0).FindControl("lblFULLNAME"), Label)
                    Dim lblSUB_DESCRS As Label = DirectCast(e.Row.Cells(0).FindControl("lblSUB_DESCRS"), Label)
                    Dim lblId As Label = DirectCast(e.Row.Cells(0).FindControl("lblId"), Label)
                    lblSHORTNAME.ForeColor = Drawing.Color.Red
                    lblFULLNAME.ForeColor = Drawing.Color.Red
                    lblSUB_DESCRS.ForeColor = Drawing.Color.Red
                    lblId.ForeColor = Drawing.Color.Red
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvTeacherExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTeacherExport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderTR(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnExportRoom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportRoom.Click

        If Not Session("sBsuid") Is Nothing Then



            Dim DirName As String = "fPath_" & Now.ToString("yyyyMddhhmmss") 'create the unique folder fpath_f2131dsa
            Dim savePath As String = String.Empty
            Dim iflag As Integer = 0
            savePath = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString & "\" & Session("sBsuid") & "\TEMP_TXT"

            'write to physical path C:\inetpub\wwwroot\TIMETABLE\131001\fpath_f2131dsa


            If Not Directory.Exists(savePath) Then


                Directory.CreateDirectory(savePath)
            Else
                'IF DIRECTORY EXIST DELETE ALL THE FILE TO HAVE CLEAN ENTRY OF FILES
                Dim d As New DirectoryInfo(savePath)
                Dim fi() As FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fi.Length > 0 Then
                    For Each f As FileInfo In fi
                        f.Delete()
                    Next
                End If
            End If


            If writeRoom_file(savePath) = "yes" Then
                iflag += 1
            End If
            If writeClass_file(savePath) = "yes" Then
                iflag += 1
            End If
            If writeTeacher_file(savePath) = "yes" Then
                iflag += 1
            End If
            If writeSubject_file(savePath) = "yes" Then
                iflag += 1
            End If

            Try
                If iflag <> 0 Then


                    Dim extr As New Zip.FastZip
                    Dim recurse As Boolean = True
                    Dim filter As String = "\.txt$"
                    Dim ZipFilename As String = savePath & "\MasterFiles.zip"
                    extr.CreateZip(ZipFilename, savePath & "\", recurse, filter)

                    Dim filename As String = "MasterFiles.zip"
                    'Response.Clear()
                    'Response.AppendHeader("content-disposition", "attachment; filename=" + filename)
                    ''Response.AppendHeader("content-disposition", ZipFilename)
                    'Response.ContentType = "application/octet-stream"
                    'Response.TransmitFile(ZipFilename)
                    'HttpContext.Current.ApplicationInstance.CompleteRequest()

                    Response.Clear()

                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename)

                    Response.ContentType = "application/x-zip-compressed"
                    Response.WriteFile(ZipFilename)

                    Response.End()

                End If

            Catch ex As Exception

            End Try


        End If


    End Sub

    Private Function writeRoom_file(ByVal phyFilePath As String) As String
        Dim fs As FileStream = Nothing
        Dim fileLoc As String = phyFilePath & "\Room.txt"
        Dim flagRoom As String = String.Empty
        If (Not File.Exists(fileLoc)) Then
            fs = File.Create(fileLoc)
            Using sw As StreamWriter = New StreamWriter(fs)

                For Each row As GridViewRow In gvRoomExport.Rows
                    Dim chkSelectRM = DirectCast(row.FindControl("chkSelectRM"), CheckBox)
                    Dim lblFULLNAME As Label
                    Dim lblSHORTNAME As Label
                    Dim altRoom As String = String.Empty
                    If Not chkSelectRM Is Nothing Then

                        If chkSelectRM.Checked = True Then
                            lblSHORTNAME = DirectCast(row.FindControl("lblSHORTNAME"), Label)
                            lblFULLNAME = DirectCast(row.FindControl("lblFULLNAME"), Label)

                            sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17}", """" & lblSHORTNAME.Text & """", _
                                                                                               """" & lblFULLNAME.Text & """", """" & altRoom & """", "", _
                                                                                               "", "", "", "", _
                                                                                               "", "", "", _
                                                                                              "", "", "", "", "", "", ""))
                            flagRoom = "yes"
                        End If

                    End If
                Next

            End Using

        End If

        writeRoom_file = flagRoom

    End Function

    Private Function writeClass_file(ByVal phyFilePath As String) As String
        Dim fs As FileStream = Nothing
        Dim fileLoc As String = phyFilePath & "\Class.txt"
        Dim flagClass As String = String.Empty
        If (Not File.Exists(fileLoc)) Then
            fs = File.Create(fileLoc)
            Using sw As StreamWriter = New StreamWriter(fs)

                For Each row As GridViewRow In gvClassExport.Rows
                    Dim chkSelectCL = DirectCast(row.FindControl("chkSelectCL"), CheckBox)
                    Dim lblFULLNAME As Label
                    Dim lblSHORTNAME As Label
                    If Not chkSelectCL Is Nothing Then

                        If chkSelectCL.Checked = True Then
                            lblSHORTNAME = DirectCast(row.FindControl("lblSHORTNAME"), Label)
                            lblFULLNAME = DirectCast(row.FindControl("lblFULLNAME"), Label)


                            sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}," & _
                                                     "{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}," & _
                                                     " {24},{25},{26},{27},{28},{29},{30}", """" & lblSHORTNAME.Text & """", _
"""" & lblFULLNAME.Text & """", "", "", "", "", "", _
"", "", "", "", "", "", "", "", "", _
"", "", "", "", "", "", "", "", "", _
"", "", "", "", "", ""))

                            flagClass = "yes"
                        End If

                    End If
                Next

            End Using

        End If
        writeClass_file = flagClass
    End Function

    Private Function writeTeacher_file(ByVal phyFilePath As String) As String
        Dim fs As FileStream = Nothing
        Dim fileLoc As String = phyFilePath & "\Teacher.txt"
        Dim flagTeacher As String = String.Empty
        If (Not File.Exists(fileLoc)) Then
            fs = File.Create(fileLoc)
            Using sw As StreamWriter = New StreamWriter(fs)

                For Each row As GridViewRow In gvTeacherExport.Rows
                    Dim chkSelectTR = DirectCast(row.FindControl("chkSelectTR"), CheckBox)
                    Dim lblFULLNAME As Label
                    Dim lblSHORTNAME As Label
                    Dim lblLastCol_R As Label
                    If Not chkSelectTR Is Nothing Then

                        If chkSelectTR.Checked = True Then
                            lblSHORTNAME = DirectCast(row.FindControl("lblSHORTNAME"), Label)
                            lblFULLNAME = DirectCast(row.FindControl("lblFULLNAME"), Label)
                            lblLastCol_R = DirectCast(row.FindControl("lblLastCol_R"), Label)

                            sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}," & _
                                                                           "{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}," & _
                                                                           " {24},{25},{26},{27},{28},{29},{30},{31},{32},{33}," & _
                                                                           " {34},{35},{36},{37},{38},{39},{40},{41},{42}", """" & lblSHORTNAME.Text & """", _
                    """" & lblFULLNAME.Text & """", "", "", "", "", "", _
                    "", "", "", "", "", "", "", "", "", _
                    "", "", "", "", "", "", "", "", "", _
                    "", "", "", "", "", "", "", """" & lblLastCol_R.Text & """", "", "", "", "", "", "", "", "", "", ""))

                            flagTeacher = "yes"
                        End If

                    End If
                Next

            End Using

        End If
        writeTeacher_file = flagTeacher
    End Function

    Private Function writeSubject_file(ByVal phyFilePath As String) As String
        Dim fs As FileStream = Nothing
        Dim fileLoc As String = phyFilePath & "\Subject.txt"
        Dim flagSubject As String = String.Empty
        If (Not File.Exists(fileLoc)) Then
            fs = File.Create(fileLoc)
            Using sw As StreamWriter = New StreamWriter(fs)

                For Each row As GridViewRow In gvSubjectExport.Rows
                    Dim chkSelectSB = DirectCast(row.FindControl("chkSelectSB"), CheckBox)
                    Dim lblFULLNAME As Label
                    Dim lblSHORTNAME As Label

                    If Not chkSelectSB Is Nothing Then

                        If chkSelectSB.Checked = True Then
                            lblSHORTNAME = DirectCast(row.FindControl("lblSHORTNAME"), Label)
                            lblFULLNAME = DirectCast(row.FindControl("lblFULLNAME"), Label)


                            sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}," & _
                                                        "{13},{14},{15},{16},{17},{18},{19},{20},{21}", """" & lblSHORTNAME.Text & """", _
 """" & lblFULLNAME.Text & """", "", "", "", "", "", _
 "", "", "", "", "", """" & lblSHORTNAME.Text & """", "", "", "", _
 "", "", "", "", """" & lblSHORTNAME.Text & """", ""))


                            flagSubject = "yes"
                        End If

                    End If
                Next

            End Using

        End If
        writeSubject_file = flagSubject
    End Function
    Protected Sub rbRooms_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRooms.CheckedChanged

        callDiv_info("Room")
        tbl_acd.Visible = False
    End Sub

    Protected Sub rbClass_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbClass.CheckedChanged
        callDiv_info("Class")
        tbl_acd.Visible = True
    End Sub

    Protected Sub rbTeacher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTeacher.CheckedChanged
        callDiv_info("Teacher")
        tbl_acd.Visible = False
    End Sub

    Protected Sub rbSubject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSubject.CheckedChanged
        callDiv_info("Subject")
        tbl_acd.Visible = True
    End Sub


End Class
