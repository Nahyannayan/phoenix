<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="tblMast_ProcessTT.aspx.vb" Inherits="tblMast_ProcessTT" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .darkPanlvisible,
        .darkPanl2,
        .darkPanl {
            max-height: 800px;
            position: fixed;
            left: 0px;
            top: 0px;
            background: url(../Images/dark.png) 0 0 !important;
            display: none;
            width: 100%;
            height: 100%;
            z-index: 10000;
        }

        .darkPanlvisible {
            display: block;
        }

        .panelRef {
            background-color: #FFFFFF;
            border: 3px solid #D8E5F8;
            margin: auto auto auto auto;
            overflow-x: hidden;
            overflow-y: hidden;
            left: 10%;
            top: 20%;
            padding: 10px 10px 10px 10px;
            position: absolute;
            margin-left: -30px;
            margin-top: -50px;
        }

        .panelRef_l {
            background-color: #FFFFFF;
            border: 3px solid #D8E5F8;
            margin: auto auto auto auto;
            overflow-x: hidden;
            overflow-y: hidden;
            left: 50%;
            top: 50%;
            padding: 10px 10px 10px 10px;
            position: absolute;
            margin-left: -30px;
            margin-top: -50px;
        }

        iframe {
            border-width: 0px !important;
            border-style: none !important;
            border-color: none !important;
            border-image: none !important;
        }
    </style>

    <script language="javascript" type="text/javascript">


        function alertFu() {

            document.getElementById("<%=hfDiv.ClientID %>").value = '1';
            document.getElementById("<%=div288.ClientID%>").style.visibility = 'visible';
            document.getElementById("<%=div288.ClientID%>").className += "darkPanlvisible";

            document.getElementById("338").style.display = 'block';
        }
        function alertFuHide() {
            document.getElementById("<%=div288.ClientID%>").style.visibility = 'hidden';
            document.getElementById("<%=div288.ClientID%>").className = document.getElementById("<%=div288.ClientID%>").className.replace(/(?:^|\s)darkPanlvisible(?!\S)/g, '')
            document.getElementById("<%=hfDiv.ClientID %>").value = '0';
            document.getElementById("338").style.display = 'none';
            window.location.reload();
        }

        function hideTimeGrid() {
            document.getElementById("<%=plReopen.ClientID %>").style.display = 'none';
 }
    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Timetable Processing
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="Group"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Timegrid</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlUploadFor" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:LinkButton ID="lbtnAddTimeGrid" runat="server">Add / Edit Timegrid</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="4"><span class="field-label">Upload Untis Timetable(timetable.zip)</span></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="middle" colspan="4">
                                        <iframe id="iframeFileLoad" src="UploadTxt/tbluploadfile_TT.aspx"></iframe>
                                        <asp:Label ID="ltNote" runat="server"></asp:Label>
                                        <div id="div288" runat="server" align="center">
                                            <div class="panelRef_l" align="center">
                                                <div id="338" align="center">
                                                    <img src="../Images/loading1.gif" alt="progress" />
                                                    <%-- <div style="padding-top: 1px;">Please Wait... </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr runat="server" id="hideR1" class="title-bg">
                                    <td align="left"
                                        colspan="4">Following Updates Required
                                    </td>
                                </tr>
                                <tr runat="server" id="hideR2">
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvMaster" runat="server"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row"
                                            Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblr1" runat="server" Text='<%# Bind("r1") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Master Data">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgStatus" runat="server" ImageUrl='<%# Bind("FLAG") %>' AlternateText='<%# Bind("alt") %>' ToolTip='<%# Bind("ALT") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnMaster" runat="server" OnClick="lbtnMaster_Click">Update Master Data</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Khaki" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="hideR3" class="title-bg">
                        <td align="left"
                            colspan="4">Timetable Data
                        </td>
                    </tr>
                    <tr runat="server" id="hideR4">
                        <td align="center" style="margin: 0px; padding: 0px;">
                            <asp:GridView ID="gvTimetable" runat="server" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                Width="100%" AllowPaging="True">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:TemplateField HeaderText="Class">
                                        <HeaderTemplate>
                                            Class
                                            <br />
                                            <asp:TextBox ID="txtClass" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnClass_Src" runat="server" OnClick="btnClass_Src_Click"
                                                ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblClass" runat="server" Text='<%# Bind("CLASS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week Name" HeaderStyle-Font-Size="12px">
                                        <HeaderTemplate>
                                            Week Name
                                            <br />
                                            <asp:TextBox ID="txtWeek" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnWeek_Src" runat="server" OnClick="btnWeek_Src_Click"
                                                ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDAY_NAME" runat="server" Text='<%# Bind("DAY_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Teacher">
                                        <HeaderTemplate>
                                            Teacher
                                            <br />
                                            <asp:TextBox ID="txtTeacher" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnTeacher_Src" runat="server" OnClick="btnTeacher_Src_Click"
                                                ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStaff_name" runat="server" Text='<%# Bind("STAFF_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subject">
                                        <HeaderTemplate>
                                            Subject
                                            <br />
                                            <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSubject_Src" runat="server" OnClick="btnSubject_Src_Click"
                                                ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSUB" runat="server" Text='<%# Bind("sub") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Room" HeaderStyle-Font-Size="12px">
                                        <HeaderTemplate>
                                            Room
                                            <br />
                                            <asp:TextBox ID="txtRoom" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnRoom_Src" runat="server" OnClick="btnRoom_Src_Click"
                                                ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTTM_ROOM" runat="server" Text='<%# Bind("TTM_ROOM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period" HeaderStyle-Font-Size="12px">
                                        <HeaderTemplate>
                                            Period
                                            <br />
                                            <asp:TextBox ID="txtPeriod" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnPeriod_Src" runat="server" OnClick="btnPeriod_Src_Click"
                                                ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Bind("ttm_Period") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time" HeaderStyle-Font-Size="12px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblP_TIME" runat="server" Text='<%# bind("P_TIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SgrID" HeaderStyle-Font-Size="12px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGR_ID" runat="server" Text='<%# Bind("TTM_SGR_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SBGID" HeaderStyle-Font-Size="12px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSBG_ID" runat="server" Text='<%# Bind("TTM_SBG_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="eMPID" HeaderStyle-Font-Size="12px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMP_ID" runat="server" Text='<%# Bind("TTM_EMP_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Days" HeaderStyle-Font-Size="12px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDays" runat="server" Text='<%# Bind("TTM_NO_OF_DAYS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Class" HeaderStyle-Font-Size="12px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUntisClass" runat="server" Text='<%# Bind("TTM_CLASS_UNTIS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group" HeaderStyle-Font-Size="12px" >
                                         <HeaderTemplate>
                                            Group
                                            <br />
                                           <asp:DropDownList ID="ddlFilterGrp" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterGrp_SelectedIndexChanged">
                                               <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                               <asp:ListItem Text="Mapped" Value="1"></asp:ListItem>
                                               <asp:ListItem Text="Un-Mapped" Value="2"></asp:ListItem>
                                           </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropdownList ID="ddlGroup" runat="server" ></asp:DropdownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Update Groups" HeaderStyle-Font-Size="12px" >
                                        <ItemTemplate>
                                            <asp:Button ID="btnUpdateGroup" Text="Update Groups" OnClick="btnUpdateGroups_Click" CssClass="button" runat="server" ></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
                <%--     <div id="plReopen" runat="server" style="display: block; overflow: visible; border-color: #1b80b6;
                    border-style: solid; border-width: 1px; width: 700px; background-color:White;"> </div>--%>
                <asp:Panel ID="plReopen" runat="server" Visible="false" CssClass="darkPanlvisible">
                    <div class="panelRef">
                        <div class="msg_header">
                            <div class="msg">
                                <span class="title-bg">Add / Edit Timegrid</span>
                                <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;">
                                    <asp:ImageButton ID="Imageclose" runat="server" ImageUrl="../Images/closeme.png"
                                        Style="margin-top: -1px; vertical-align: top;" OnClientClick="hideTimeGrid()" /></span>
                            </div>
                        </div>
                        <br />
                        <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover"
                            ScrollBars="Both">
                            <div>
                                <table style="width: 100%">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Timegrid</span> </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtTGrid"
                                                runat="server"></asp:TextBox></td>

                                        <td align="left" width="20%"><span class="field-label">Start of the week</span></td>
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlStartWK" runat="server">
                                                <asp:ListItem>Sunday</asp:ListItem>
                                                <asp:ListItem>Monday</asp:ListItem>
                                                <asp:ListItem>Tuesday</asp:ListItem>
                                                <asp:ListItem>Wednesday</asp:ListItem>
                                                <asp:ListItem>Thursday</asp:ListItem>
                                                <asp:ListItem>Friday</asp:ListItem>
                                                <asp:ListItem>Saturday</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Total week day</span></td>
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlTotWK" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left"><span class="field-label">Show Subject Group Name</span>
                                        </td>
                                        <td align="left">

                                            <asp:RadioButton ID="rbYes" runat="server" Text="Yes" GroupName="GRP" />
                                            <asp:RadioButton ID="rbNo" runat="server" Checked="True" Text="No" GroupName="GRP" />
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">
                                    <asp:Button ID="btnAddSaveGrid" runat="server" Text="Add / Save" CssClass="button"
                                        ValidationGroup="time"></asp:Button>
                                    <asp:Button ID="btnUpdateSaveGrid" runat="server" CssClass="button"
                                        Text="Update / Save" ValidationGroup="time" />
                                    <asp:Button ID="btnCancelSaveGrid" runat="server" CssClass="button"
                                        Text="Cancel" ValidationGroup="time" />
                                </div>
                                <div align="left">
                                    <asp:Label ID="ltGridError" runat="server"></asp:Label>
                                </div>
                                <div>
                                    <asp:GridView ID="gvTime" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row"
                                        Width="100%">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Timegrid">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_MULTI_TIMEGRID" runat="server" Text='<%# Bind("TS_MULTI_TIMEGRID") %>'></asp:Label>
                                                    <asp:Label ID="lblTS_ID" runat="server" Text='<%# Bind("TS_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblTS_bSHOWSUBJECT_GRP" runat="server" Text='<%# Bind("TS_bSHOWSUBJECT_GRP") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start of the week">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_START_WEEK" runat="server" Text='<%# Bind("TS_START_WEEK") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total day">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_TOT_WEEK_DAY" runat="server" Text='<%# Bind("TS_TOT_WEEK_DAY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Show Group Name">
                                                <ItemTemplate>
                                                    <asp:Image ID="imgGRP" runat="server" ImageUrl='<%# Bind("FLAG") %>' ToolTip='<%# Bind("ALT") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEditGRP" runat="server" OnClick="lbtnEditGRP_Click">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle BackColor="Khaki" />
                                        <HeaderStyle CssClass="gridheader_new" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </asp:Panel>
                <asp:Button ID="btnShowReopen" runat="server" Style="display: none" />
                <%-- <ajaxToolkit:ModalPopupExtender ID="mdlReopen" runat="server"
                 TargetControlID="btnShowReopen"
                    PopupControlID="plReopen"  BackgroundCssClass="modalBackground"
                    DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />--%>

                <asp:HiddenField ID="hfTS_ID" runat="server" Value="0" />
                <asp:HiddenField ID="hfDiv" runat="server" Value="0" />

                <script language="javascript" type="text/javascript">

                    var hideFlag;
                    hideFlag = document.getElementById("<%=hfDiv.ClientID %>").value

                    if (hideFlag == '0') {
                        document.getElementById("<%=div288.ClientID%>").style.visibility = 'hidden';
                        document.getElementById("<%=div288.ClientID%>").className = document.getElementById("<%=div288.ClientID%>").className.replace(/(?:^|\s)darkPanlvisible(?!\S)/g, '')
                        document.getElementById("338").style.display = 'none';


                    }

                    else {
                        document.getElementById("<%=div288.ClientID%>").style.visibility = 'visible';
                        document.getElementById("<%=div288.ClientID%>").className += "darkPanlvisible";
                        document.getElementById("338").style.display = 'block';
                    }
                </script>

            </div>
        </div>
    </div>
</asp:Content>


