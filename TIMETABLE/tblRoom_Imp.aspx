<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="tblRoom_Imp.aspx.vb" Inherits="tblRoom_Imp" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Import Rooms From Untis
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False"  ValidationGroup="Group"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <table align="center"  width="100%">
                                <tr class="title-bg">
                                    <td>
                                        Upload Untis Room Master(GPU005.TXT)
                                    </td>
                                </tr>
                                <tr >
                                                                    
                                    <td align="left" valign="middle" width="80%">
                                        <iframe id="iframeFileLoad_room" src="ucMasterProcess/Room/tbluploadfile_Room.aspx" scrolling="no" frameborder="0"
                                            style="text-align: center; vertical-align: middle; border-style: none; margin: 0px; width: 100%;"></iframe>

                                        <asp:Label ID="ltNote" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="hideR3" class="title-bg">
                        <td align="left" 
                            colspan="4">
                           Room Master Data
                        </td>
                    </tr>
                    <tr runat="server" id="hideR4">
                        <td align="center" style="margin: 0px; padding: 0px;">
                            <asp:Panel runat="server" ID="plGvRM" Width="100%"  ScrollBars="Vertical">
                                <asp:GridView ID="gvRM" runat="server" CssClass="table table-bordered table-row"
                                    AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                    Width="100%">
                                    <RowStyle CssClass="griditem"  />
                                    <EmptyDataRowStyle CssClass="matters" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Font-Size="12px">
                                            <ItemTemplate>

                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("Srno") %>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("Srno") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShortName" runat="server" Text='<%# Bind("RM_SHORTNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Name" HeaderStyle-Font-Size="12px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRM_FULLNAME" runat="server"
                                                    Text='<%# Bind("RM_FULLNAME") %>' Width="250px"></asp:Label>

                                            </ItemTemplate>

                                            <HeaderStyle Font-Size="12px"></HeaderStyle>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Virtual Room">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgVirtual" runat="server" ImageUrl='<%# bind("FLAG") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>

                </table>


                <asp:HiddenField ID="hfTS_ID" runat="server" Value="0" />

            </div>
        </div>
    </div>
</asp:Content>


