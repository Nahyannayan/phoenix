﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Drawing

Partial Class TIMETABLE_tblEmp_Update
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReport)
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010501") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                'plEmpExp.Visible = True
                'plempImp.Visible = False
                bindMasterData()






            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


   
    Sub bindMasterData()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim INFO_TYPE As String = String.Empty
            Dim PARAM(3) As SqlParameter

            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "EX.EXPORTEMP_RECORD", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvEmp.DataSource = ds.Tables(0)
                gvEmp.DataBind()
                Dim chkSelectAll As CheckBox = DirectCast(gvEmp.HeaderRow.FindControl("chkSelectAllEMP"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
                For Each row As GridViewRow In gvEmp.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelectEMP"), CheckBox)

                    If Not chkSelect Is Nothing Then
                        If chkSelect.Visible = True Then
                            chkSelect.Checked = True
                        End If
                    End If
                Next




            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvEmp.DataSource = ds.Tables(0)
                Try
                    gvEmp.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvEmp.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvEmp.Rows(0).Cells.Clear()
                gvEmp.Rows(0).Cells.Add(New TableCell)
                gvEmp.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmp.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvTT_Teacher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmp.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderEMP(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectEMP"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectEMP"), CheckBox).Attributes.Add("onclick", strScript)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Sub CallReport()

        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim EMP_IDS As New StringBuilder
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim SrNoCol As DataColumn
        Dim EmpNoCol As DataColumn
        Dim EmpNameCol As DataColumn
        Dim EmpShortNameCol As DataColumn
        Dim i As Integer

        dt = New DataTable()
        SrNoCol = New DataColumn("SRNO", Type.GetType("System.String"))
        EmpNoCol = New DataColumn("EMPNO", Type.GetType("System.String"))
        EmpNameCol = New DataColumn("EMPNAME", Type.GetType("System.String"))
        EmpShortNameCol = New DataColumn("EMPSHORTNAME", Type.GetType("System.String"))

        dt.Columns.Add(SrNoCol)
        dt.Columns.Add(EmpNoCol)
        dt.Columns.Add(EmpNameCol)
        dt.Columns.Add(EmpShortNameCol)

        i = 1
        For Each row As GridViewRow In gvEmp.Rows
            Dim chkSelectEMP = DirectCast(row.FindControl("chkSelectEMP"), CheckBox)
            Dim lblEmp_no = DirectCast(row.FindControl("lblEmp_no"), Label)
            Dim lblEmpName = DirectCast(row.FindControl("lblEmpName"), Label)
            Dim lblEmp_Short = DirectCast(row.FindControl("lblEmp_Short"), Label)

            If Not chkSelectEMP Is Nothing Then
                If chkSelectEMP.Checked = True Then
                    lblEmp_no = DirectCast(row.FindControl("lblEmp_no"), Label)
                    lblEmpName = DirectCast(row.FindControl("lblEmpName"), Label)
                    lblEmp_Short = DirectCast(row.FindControl("lblEmp_Short"), Label)
                    dr = dt.NewRow()
                    dr("SRNO") = i
                    dr("EMPNO") = lblEmp_no.Text
                    dr("EMPNAME") = lblEmpName.Text
                    dr("EMPSHORTNAME") = lblEmp_Short.Text
                    dt.Rows.Add(dr)
                    i = i + 1
                End If
            End If
        Next

        ds.Tables.Add(dt)


        If i = 1 Then

            lblErr.Text = "Please select the employee"
            Exit Sub
        End If


        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        TypicalTableSample(ef.Worksheets.Add("Sheet1"), ds)

        Dim fileName As String = "Staff Short Code Update.xlsx"

      
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()


        Dim pathSave As String = Session("sUsr_id") & "\" & fileName
        If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
            ' Create the directory.
            Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
        End If


        ef.Save(cvVirtualPath & pathSave)
        Dim path = cvVirtualPath & pathSave
        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()
    End Sub



    Sub TypicalTableSample(ByVal ws As ExcelWorksheet, ByVal dsExcel As DataSet)

      

        Dim dtEXCEL As New DataTable()

        Dim headerStyle As CellStyle = New CellStyle
        Dim colHeader As CellStyle = New CellStyle
        Dim CellStyle As CellStyle = New CellStyle
        Dim colTot As Integer = dsExcel.Tables(0).Columns.Count


        headerStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        headerStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        headerStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
        headerStyle.Font.Size = 16 * 16
        headerStyle.Font.Name = "Verdana"
        headerStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#c5d9f1"))
        ws.Rows(0).Height = 16 * 23
        ws.Cells(0, 0).Value = "SrNo"
        ws.Cells(0, 0).Style = headerStyle
        ws.Cells(0, 1).Value = "EmpNo"
        ws.Cells(0, 1).Style = headerStyle
        ws.Cells(0, 2).Value = "Name"
        ws.Cells(0, 2).Style = headerStyle
        ws.Cells(0, 3).Value = "ShortCode"
        ws.Cells(0, 3).Style = headerStyle


        ws.Columns(0).Width = 9 * 256
        ws.Columns(1).Width = 12 * 256
        ws.Columns(2).Width = 45 * 256
        ws.Columns(3).Width = 15 * 256

      
        Dim weekList As New Hashtable()
        Dim tempWeekName As String = String.Empty
        Dim tempMergeClose As Integer = 0

        

        Dim cellweekName As String = String.Empty
        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 1
                If colLoop = 2 Then
                    CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Left
                Else
                    CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
                End If

                CellStyle.Font.Weight = ExcelFont.BoldWeight
                CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#455798")
                CellStyle.Font.Size = 14 * 14

                CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
                CellStyle.Font.Name = "Verdana"
                CellStyle.WrapText = True
                ws.Rows(1 + rowLoop).Height = 16 * 23

                ws.Cells(1 + rowLoop, colLoop).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(1 + rowLoop, colLoop).Style = CellStyle

            Next
        Next

    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        CallReport()
    End Sub

    
    'Protected Sub rbEmp_Exp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEmp_Exp.CheckedChanged
    '    plEmpExp.Visible = True
    '    plempImp.Visible = False
    '    btnReport.Visible = True
    'End Sub

    'Protected Sub rbImp_Exp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbImp_Exp.CheckedChanged
    '    plEmpExp.Visible = False
    '    plempImp.Visible = True
    '    btnReport.Visible = False
    'End Sub
   
    Protected Sub mnuMaster_MenuItemClick(sender As Object, e As MenuEventArgs)

        'MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Dim Iindex As Integer = Int32.Parse(e.Item.Value)
        Dim i As Integer
        'Make the selected menu item reflect the correct imageurl
        For i = 0 To mnuMaster.Items.Count - 1
            Select Case i
                Case 0
                    If i = e.Item.Value Then
                        'mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnMain2.jpg"
                    Else
                        ' mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnMain.jpg"
                    End If
                    mvMaster.ActiveViewIndex = Iindex


                Case 1
                    If i = e.Item.Value Then
                        ' mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnPassport2.jpg"
                    Else
                        '  mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnPassport.jpg"
                    End If
                    mvMaster.ActiveViewIndex = Iindex
                
            End Select
        Next
    End Sub
End Class
