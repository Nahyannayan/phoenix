﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="tblMast_ProcessStaff.aspx.vb"
    Inherits="TIMETABLE_tblMast_ProcessStaff" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="ucMasterProcess/Staff/ucProStaff_Add.ascx" TagName="ProcStaff_Add" TagPrefix="ucpSTADD" %>
<%@ Register Src="ucMasterProcess/Staff/ucProStaff_Map.ascx" TagName="ProcStaff_Map" TagPrefix="ucpSTMap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function validate(gvStaffMap) {
            var flag = false;
            var dropdowns = new Array(); //Create array to hold all the dropdown lists.
            var imgArr = new Array(); //Create array to hold all the dropdown lists.
            var gridview = document.getElementById(gvStaffMap); //GridView1 is the id of ur gridview.
            dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
            var imgArr = gridview.getElementsByTagName('img');

            for (var i = 0; i < dropdowns.length; i++) {
                if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                {
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowgreen.png"
                    flag = true;
                }
                else {
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowred.png"


                }
            }


        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="TEACHER MASTER"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblErr" runat="server" EnableViewState="false"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                        </td>

                    </tr>

                    <tr>
                        <td class="matters" align="left">
                            <span class="field-label">Select the option</span> </td>
                        <td>
                            <asp:RadioButton ID="rbAddteach" CssClass="field-label"
                                runat="server" GroupName="teach" Text="Add Temp Teacher" Checked="true"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbAddMap" CssClass="field-label"
                                runat="server" GroupName="teach" Text="Map Teacher" AutoPostBack="True" />
                        </td>
                        <td colspan="2"></td>

                    </tr>

                    <tr>
                        <td colspan="4">
                            <ucpSTADD:ProcStaff_Add ID="ProcStaff_Add_ctr" runat="server"></ucpSTADD:ProcStaff_Add>
                            <ucpSTMap:ProcStaff_Map ID="ProcStaff_Map_ctr" runat="server"></ucpSTMap:ProcStaff_Map>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

