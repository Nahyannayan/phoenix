﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucProSubject_Map.ascx.vb" 
Inherits="TIMETABLE_ucMasterProcess_ucPrSubject_Map" %>

 <script language="javascript" type="text/javascript">



 
function validate()


{
         var flag = false;
         var dropdowns = new Array(); //Create array to hold all the dropdown lists.
         var imgArr = new Array(); //Create array to hold all the dropdown lists.
         var gridview = document.getElementById('<%=gvSubjectMap_GRP.ClientID %>'); //GridView1 is the id of ur gridview.
         dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
         var imgArr = gridview.getElementsByTagName('img');
 
         for (var i = 0; i < dropdowns.length; i++) {
             if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
             {
                 //imgArr.item(i).style.display="none"
                 imgArr.item(i).src = "../Images/arrowgreen.png"
                 flag = true;
             }
             else {
                 //imgArr.item(i).style.display="none"
                 imgArr.item(i).src = "../Images/arrowred.png"
                 
                 
             }
         }
         
        
     }
 

</script>       
  

        
<table width="100%">
 <tr  valign="bottom">
            <td align="left"   valign="middle">
                        <asp:Literal ID="lblErr" runat="server" EnableViewState="false" ></asp:Literal>
            </td>
        </tr>
<tr>
    <td align="left">
    <asp:Panel ID="plSubjectMap" runat="server" ScrollBars="Vertical" Width="100%" Height="450px" >
         <asp:GridView id="gvSubjectMap" runat="server" AutoGenerateColumns="False"
                                CssClass="gridstyle" Height="100%" 
    Width="100%" >
                                <rowstyle cssclass="griditem" height="25px" />
                                <columns>
                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemTemplate>

    <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("R1") %>' 
      ></asp:Label>  
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>


                                    <asp:TemplateField HeaderText="Class">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTTM_GRD_ID" runat="server" Text='<%# bind("TTM_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
<asp:TemplateField HeaderText="Subject">
    <ItemTemplate>
        <asp:Label ID="lblTTM_SUBJECT" runat="server" Text='<%# bind("TTM_SUBJECT") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate></EditItemTemplate>

<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
<ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
</columns>
                                <selectedrowstyle cssclass="Green" />
                                <headerstyle cssclass="gridheader_pop" height="15px" horizontalalign="Center" 
                                    verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" height="25px" />
                            </asp:GridView>  
       <asp:GridView id="gvSubjectMap_GRP" runat="server" AutoGenerateColumns="False"
                                CssClass="gridstyle" Height="100%"  Width="100%" >
                                <rowstyle cssclass="griditem" height="25px" />
                                <columns>


<asp:TemplateField HeaderText="Untis Uploaded Subject Group">
<ItemTemplate >
    <asp:Label ID="lblTTM_SUBJECT" runat="server" Text='<%# bind("TTM_SUBJECT") %>' 
       ></asp:Label>
         
</ItemTemplate>


<HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>


                                    <asp:TemplateField HeaderText="Map it with OASIS">
                                        <ItemTemplate>
                                            <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

<asp:TemplateField HeaderText="OASIS Available Subject Group"><EditItemTemplate></EditItemTemplate>
<ItemTemplate>
    <asp:DropDownList ID="ddlUntis_Subject_Name" runat="server" onchange="validate()"  CssClass="ddlanswer">
    </asp:DropDownList>
</ItemTemplate>



<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
<ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>


</columns>
                                <selectedrowstyle cssclass="Green" />
                                <headerstyle cssclass="gridheader_pop" height="15px" horizontalalign="Center" verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" height="25px" />
                            </asp:GridView>
       
        </asp:Panel>
                        </td>
    
    </tr>
    <tr><td align="center" style="padding-top:6px;">
    
        <asp:Button ID="btnSaveMap" runat="server" CssClass="button" Text="Process Subject Mapping" 
            Width="170px" Height="25px" />
        <asp:Button ID="btnBackMap" runat="server" CausesValidation="False" 
            CssClass="button" Text="Back" Width="170px" Height="25px" />
            
        </td></tr>
        </table>