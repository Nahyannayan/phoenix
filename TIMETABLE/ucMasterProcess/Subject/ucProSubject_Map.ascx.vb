﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_ucMasterProcess_ucPrSubject_Map
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Page.IsPostBack = False Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("acd_id") = "0"
                ViewState("SET_TYPE") = "0"
                Dim arr As String() = New String(2) {}
                If Not Session("ACD_TYPE_TT") Is Nothing Then
                    arr = Session("ACD_TYPE_TT").ToString.Split("|")
                    ViewState("acd_id") = arr(0)
                    ViewState("SET_TYPE") = arr(1)
                End If
                If GETTIMETABLE_GRP() = True Then
                    gvSubjectMap.Visible = False
                    gvSubjectMap_GRP.Visible = True

                    bind_dropdown(ViewState("acd_id"), ViewState("SET_TYPE"))
                    bindSubjectData_OASIS_GRP(ViewState("acd_id"), ViewState("SET_TYPE"))

                Else
                    gvSubjectMap.Visible = True
                    gvSubjectMap_GRP.Visible = False
                    bindMissingSubject_data(ViewState("acd_id"), ViewState("SET_TYPE"))

                End If





            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Function GETTIMETABLE_GRP() As Boolean
        Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "TM.GETTIMETABLE_GRP", PARAM)
            If DATAREADER.HasRows = True Then
                GETTIMETABLE_GRP = True
            Else
                GETTIMETABLE_GRP = False
            End If
        End Using
    End Function
    Sub bindMissingSubject_data(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETMISSING_SUBJECT]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectMap.DataSource = ds.Tables(0)
                gvSubjectMap.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                gvSubjectMap.DataSource = ds.Tables(0)
                Try
                    gvSubjectMap.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectMap.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSubjectMap.Rows(0).Cells.Clear()
                gvSubjectMap.Rows(0).Cells.Add(New TableCell)
                gvSubjectMap.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectMap.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectMap.Rows(0).Cells(0).Text = "No Records Available !!!"
                Dim imgMap As Image = DirectCast(gvSubjectMap.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnBackMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackMap.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\TIMETABLE\tblMast_ProcessTT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSaveMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMap.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then


            bindMissingSubject_data(ViewState("acd_id"), ViewState("SET_TYPE"))

            lblErr.Text = "<div style='border: 1px solid #1B80B6;width: 97%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#2e2e2e;;padding:5pt;background-color:#d1ff83;'>Record Saved Successfully</div>"

        Else
            lblErr.Text = "<div style='border: 1px solid #1B80B6;width: 97%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#ff0000;padding:5pt;background-color:#ffc7c7;'>" & errorMessage & "</div>"
        End If
    End Sub
    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty

        Dim STR_XML As New StringBuilder



        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("acd_id"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[TM].[SAVESUBJECT_MAPPING]", PARAM)
                ReturnFlag = PARAM(3).Value


                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If




            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function
    Private Sub bind_dropdown(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Session("TTSubj_map_GRP") = Nothing
        Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim ds As New DataSet
        Dim OASISSUBJECT As New List(Of ListItem)
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
        ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETOASIS_SUBJECT_GRP]", PARAM)
        OASISSUBJECT.Add(New ListItem("Subject Group Not Mapped", "-1"))
        If ds.Tables(0).Rows.Count > 0 Then
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                OASISSUBJECT.Add(New ListItem(ds.Tables(0).Rows(I)("SUB_GRP"), ds.Tables(0).Rows(I)("SBG_ID")))
            Next

        End If
        Session("TTSubj_map_GRP") = OASISSUBJECT
    End Sub
    Sub bindSubjectData_OASIS_GRP(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETTIMETABLE_SUBJECT_GRP]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectMap_GRP.DataSource = ds.Tables(0)
                gvSubjectMap_GRP.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                gvSubjectMap_GRP.DataSource = ds.Tables(0)
                Try
                    gvSubjectMap_GRP.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectMap_GRP.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSubjectMap_GRP.Rows(0).Cells.Clear()
                gvSubjectMap_GRP.Rows(0).Cells.Add(New TableCell)
                gvSubjectMap_GRP.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectMap_GRP.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectMap_GRP.Rows(0).Cells(0).Text = "No Subject Group Available !!!"
                Dim imgMap As Image = DirectCast(gvSubjectMap_GRP.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvSubjectMap_GRP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubjectMap_GRP.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlUntis_Subject_Name As DropDownList = DirectCast(e.Row.FindControl("ddlUntis_Subject_Name"), DropDownList)
                Dim lblTTM_SUBJECT As Label = DirectCast(e.Row.FindControl("lblTTM_SUBJECT"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)
                If Not Session("TTSubj_map_GRP") Is Nothing Then
                    ddlUntis_Subject_Name.Items.Clear()
                    ddlUntis_Subject_Name.DataSource = Session("TTSubj_map_GRP")
                    ddlUntis_Subject_Name.DataTextField = "Text"
                    ddlUntis_Subject_Name.DataValueField = "Value"
                    ddlUntis_Subject_Name.DataBind()
                    For Each ITEM As ListItem In ddlUntis_Subject_Name.Items
                        If ITEM.Text.Contains("(" + lblTTM_SUBJECT.Text + ")") = True Then

                            ITEM.Selected = True
                            imgMap.ImageUrl = "~/Images/arrowgreen.png"
                            Exit For
                        End If
                    Next

                    'If Not ddlUntis_Subject_Name.Items.FindByValue(lblTTM_TEACHER.Text) Is Nothing Then
                    '    ddlUntis_Subject_Name.ClearSelection()
                    '    ddlUntis_Subject_Name.Items.FindByValue(lblTTM_TEACHER.Text).Selected = True
                    '    imgMap.ImageUrl = "~/Images/arrowgreen.png"
                    'End If
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub
    
End Class
