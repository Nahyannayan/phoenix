﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucProSubject_Add.ascx.vb" 
Inherits="TIMETABLE_ucMasterProcess_ucProSubject_Add" %>



<script language="javascript" type="text/javascript">
        function SelectAll(CheckBox) {
            TotalChkBx = parseInt('<%=gvSubjectMissing_ADD.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvSubjectMissing_ADD.ClientID %>');
            var TargetChildControl = "chkSelect";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeader(CheckBox) {
            TotalChkBx = parseInt('<%=gvSubjectMissing_ADD.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvSubjectMissing_ADD.ClientID %>');
            var TargetChildControl = "chkSelect";
            var TargetHeaderControl = "chkSelectAll";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }

          
    </script>
<table width="100%">
 <tr  valign="bottom">
            <td align="left"   valign="middle">
                        <asp:Literal ID="lblErr" runat="server" EnableViewState="false" ></asp:Literal>
            </td>
        </tr>
<tr><td>
<asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="100%"  BorderStyle="Solid" BorderColor="#1B80B6" BorderWidth="1px"
        Height="400px" >
      <asp:GridView id="gvSubjectMissing_ADD" runat="server" 
    AutoGenerateColumns="False" BorderColor="#1B80B6"
                              
                EmptyDataText="No  room added yet" Font-Names="Verdana"
                                Font-Size="8pt" ForeColor="#1B80B6" 
                                Width="100%" >
                                <rowstyle cssclass="griditem" height="26px" />
                                <emptydatarowstyle cssclass="gridheader" horizontalalign="Center" wrap="True" />
                                <columns>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server"  ></asp:CheckBox> 

                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                         <asp:CheckBox ID="chkSelectAll" runat="server"  onclick="SelectAll(this);" ToolTip="Click here to select/deselect all rows" />

                                        </HeaderTemplate>
                                        <HeaderStyle Width="30px" />
                                    </asp:TemplateField>
                                      
                                    <asp:TemplateField HeaderText="Sr.No" >
    <ItemTemplate>
<asp:Label id="lblId" runat="server" Text='<%# Bind("R1") %>' 
       ></asp:Label> 
</ItemTemplate>
 <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
  <HeaderStyle Width="50px" />
</asp:TemplateField>
<asp:TemplateField HeaderText="Class">
<ItemTemplate>
<asp:Label id="lblTTM_GRD_ID" runat="server" 
        Text='<%# BIND("TTM_GRD_ID") %>'></asp:Label> 
</ItemTemplate>
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
</asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subject">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTTM_SUBJECT" runat="server" 
                                                Text='<%# BIND("TTM_SUBJECT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
</columns>
                                <selectedrowstyle backcolor="Khaki" />
                                <headerstyle cssclass="gridheader_new" height="25px" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
     
          </asp:Panel>
</td></tr>
       <tr>
            <td  style="height: 7px" valign="bottom" align="center">
                <br />
                <asp:Button id="btnSaveAdd"
                            runat="server" CssClass="button" Text="Save" 
                    ValidationGroup="groupM1"  Height="26px" Width="70px" 
                   />
                <asp:Button id="btnBackAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Back"  Height="26px" 
                    Width="70px" />
            </td>
        </tr>
</table>
