﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class TIMETABLE_tbluploadfile_Room
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function READTIMETABLE_MASTER(ByVal filepath As String) As String
        Try
            Dim errorstring As String = String.Empty
            Dim strbulider As New StringBuilder
            Dim strFileName As String = filepath
            Dim currentRowData() As String

            Dim TRM_SHORTNAME_0 As String = String.Empty
            Dim TRM_FULLNAME_1 As String = String.Empty
            Dim TRM_ALT_NAME_2 As String = String.Empty
            Dim TRM_GROUP_CODE_3 As String = String.Empty
            Dim TRM_BLANK_4 As String = String.Empty
            Dim TRM_OFFSITE_CODE_5 As String = String.Empty
            Dim TRM_WEIGHT_6 As String = String.Empty
            Dim TRM_CAPACITY_7 As String = String.Empty
            Dim TRM_DEPT_NAME_8 As String = String.Empty
            Dim TRM_CORR_9 As String = String.Empty
            Dim TRM_CORR_10 As String = String.Empty
            Dim TRM_TEXT_11 As String = String.Empty
            Dim TRM_DESCR_12 As String = String.Empty
            Dim TRM_FG_COLOUR_13 As String = String.Empty
            Dim TRM_BG_COLOUR_14 As String = String.Empty
            Dim TRM_STATISTICS1_15 As String = String.Empty
            Dim TRM_STATISTICS2_16 As String = String.Empty


            Using txtReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(strFileName)
                txtReader.TextFieldType = FileIO.FieldType.Delimited
                txtReader.SetDelimiters(",")
                txtReader.HasFieldsEnclosedInQuotes = True
                While Not txtReader.EndOfData
                    ' Read one "record's worth" of fields into the 
                    ' "currentRowData" array ...
                    currentRowData = txtReader.ReadFields
                    If UBound(currentRowData) <> 17 Then
                        errorstring = "-1"
                        Exit While
                    End If

                    TRM_SHORTNAME_0 = currentRowData(0).ToString
                    TRM_FULLNAME_1 = currentRowData(1).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                    TRM_ALT_NAME_2 = currentRowData(2).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                    TRM_GROUP_CODE_3 = currentRowData(3).ToString
                    TRM_BLANK_4 = currentRowData(4).ToString
                    TRM_OFFSITE_CODE_5 = currentRowData(5).ToString
                    TRM_WEIGHT_6 = currentRowData(6).ToString
                    TRM_CAPACITY_7 = currentRowData(7).ToString
                    TRM_DEPT_NAME_8 = currentRowData(8).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                    TRM_CORR_9 = currentRowData(9).ToString
                    TRM_CORR_10 = currentRowData(10).ToString
                    TRM_TEXT_11 = currentRowData(11).ToString.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                    TRM_DESCR_12 = currentRowData(12).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                    TRM_FG_COLOUR_13 = currentRowData(13).ToString
                    TRM_BG_COLOUR_14 = currentRowData(14).ToString
                    TRM_STATISTICS1_15 = currentRowData(15).ToString
                    TRM_STATISTICS2_16 = currentRowData(16).ToString

                    strbulider.Append(String.Format("<RM TRM_SHORTNAME='{0}' TRM_FULLNAME='{1}' TRM_ALT_NAME='{2}' TRM_GROUP_CODE='{3}' TRM_BLANK ='{4}' TRM_OFFSITE_CODE ='{5}' TRM_WEIGHT='{6}'" & _
                                                    " TRM_CAPACITY='{7}' TRM_DEPT_NAME ='{8}' TRM_CORR1='{9}' TRM_CORR2='{10}'  " & _
                                                     "   TRM_TEXT='{11}' TRM_DESCR='{12}' TRM_FG_COLOUR='{13}' TRM_BG_COLOUR='{14}' " & _
                                                        " TRM_STATISTICS1='{15}' TRM_STATISTICS2='{16}'  />", TRM_SHORTNAME_0, _
  TRM_FULLNAME_1, TRM_ALT_NAME_2, TRM_GROUP_CODE_3, TRM_BLANK_4, TRM_OFFSITE_CODE_5, TRM_WEIGHT_6, TRM_CAPACITY_7, TRM_DEPT_NAME_8, TRM_CORR_9, _
  TRM_CORR_10, TRM_TEXT_11, TRM_DESCR_12, TRM_FG_COLOUR_13, TRM_BG_COLOUR_14, TRM_STATISTICS1_15, TRM_STATISTICS2_16))
                End While
            End Using

            If errorstring = "-1" Then
                READTIMETABLE_MASTER = "-1"
            Else
                READTIMETABLE_MASTER = strbulider.ToString
            End If


        Catch ex As Exception
            READTIMETABLE_MASTER = ""
        End Try

    End Function
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click


        Try
            Dim fs As New FileInfo(fileload.PostedFile.FileName)
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength ' get the file size
            Dim newFileName As String = "fPath_" & Now.ToString("yyyyMddhhmmss") 'create the unique folder fpath_f2131dsa
            Dim strPostedFileName As String = fs.Name 'get the file name
            Session("TT_Folder_Name") = "\" & Session("sBsuid") & "\TEMP_TXT\"

            If intDocFileLength > 2096000 Then '2MB
                lblErrorLoad.Text = "text file size exceeds the limit of 2Mb."
                Exit Sub
            End If

            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".txt") Then ' if zip file processed

                    Dim savePath As String = String.Empty
                    savePath = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString & Session("TT_Folder_Name")

                    'write to physical path C:\inetpub\wwwroot\TIMETABLE\131001\TEMP_TXT\fpath_f2131dsa.txt

                    If Not Directory.Exists(savePath) Then
                        Directory.CreateDirectory(savePath)

                    End If


                    fileload.PostedFile.SaveAs(savePath & newFileName & ".txt")
                    Dim str_Xml As String = String.Empty
                    str_Xml = READTIMETABLE_MASTER(savePath & newFileName & ".txt")
                    If str_Xml = "-1" Then
                        lblErrorLoad.Text = "File uploaded is not in the required format !!!"
                        File.Delete(savePath & newFileName & ".txt")
                    Else
                        File.Delete(savePath & newFileName & ".txt")
                        Dim str_err As String = String.Empty
                        Dim errorMessage As String = String.Empty
                        str_err = calltransaction(str_Xml)
                        If str_err = "0" Then
                            ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
                            lblErrorLoad.Text = "File uploaded successfully..."
                        Else
                            lblErrorLoad.Text = "Error occured while saving !!!"
                        End If
                        '----after reading the file detelete the file '
                    End If


                Else
                    lblErrorLoad.Text = "Please upload a valid Document with the extenion in : .txt"

                End If
            Else
                lblErrorLoad.Text = "There is no file to Upload."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            lblErrorLoad.Text = "Error while Updating the file !!!"
        End Try
    End Sub

    
    Private Sub createTimetable_file(ByVal savePath As String)
        Dim filepath As String = Session("Folder_Name")
        ' Dim strFilter As String = "*.jpg"
        ' Dim formatarray As String() = {"*.jpg"} 'strFilter.Split(";")
        Dim formatarray As String() = {"*.txt"}
        ' formatarray(0) = "*.jpg"
        Dim str_output As New StringWriter
        For Each FileFormat As String In formatarray
            Dim filelist As String() = Directory.GetFiles(savePath, FileFormat, SearchOption.AllDirectories)
            For Each File As String In filelist
                Dim filename As New FileInfo(File.ToString())

                Dim WriteStatus As String = String.Empty


                Dim str_Xml As String = String.Empty
                str_Xml = READTIMETABLE_MASTER(filename.FullName)
                If str_Xml = "-1" Then
                    lblErrorLoad.Text = "File uploaded is not in the required format !!!"

                Else
                    Dim str_err As String = String.Empty
                    Dim errorMessage As String = String.Empty
                    str_err = calltransaction(str_Xml)
                    If str_err = "0" Then
                        ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
                        lblErrorLoad.Text = "File uploaded successfully..."
                    Else
                        lblErrorLoad.Text = "Error occured while saving !!!"
                    End If
                    '----after reading the file detelete the file '
                End If
            Next
        Next

        lblErrorLoad.Text = Server.HtmlEncode(str_output.ToString).Replace(Environment.NewLine, "<br/>")

    End Sub
    Function calltransaction(ByRef str_Xml As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty
        Dim clientIp As String = Request.UserHostAddress()
        Dim ClientBrw As String = String.Empty
        Dim mcName As String = String.Empty
        Dim MAC_ID As String = IRDataTables.GetMACAddress
        IRDataTables.GetClientInfo(ClientBrw, clientIp, mcName)
  
        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_Xml = "<RM_M>" + str_Xml + "</RM_M>"

                Dim PARAM(12) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@RM_BSU_ID", Session("sBsuid"))
                PARAM(3) = New SqlClient.SqlParameter("@RM_USR_ID", Session("sUsr_id"))
                PARAM(4) = New SqlClient.SqlParameter("@STR_XML", str_Xml.ToString)
                PARAM(5) = New SqlClient.SqlParameter("@RM_OTH_INFO", "CLIENT IP(" & clientIp & ")MAC_ID (" & MAC_ID & ")ClientBrw" & ClientBrw)
                PARAM(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(6).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TT.SAVEROOM_S_IMP", PARAM)
                ReturnFlag = PARAM(6).Value
                If ReturnFlag <> 0 Then
                    calltransaction = 1
                Else
                    calltransaction = 0
                End If
            Catch ex As Exception
                calltransaction = 1
            Finally
                If calltransaction <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
    End Sub

 
End Class
