﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucProRoom_Map.ascx.vb" Inherits="TIMETABLE_ucMasterProcess_ucProRoom_Map" %>

<table width="100%">
    <tr valign="bottom">
        <td align="left" valign="middle">
            <span class="error">  <asp:Literal ID="lblErr" runat="server" EnableViewState="false" ></asp:Literal> </span>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Panel ID="plRoomMap" runat="server" ScrollBars="Vertical" Width="100%" >
                <asp:GridView ID="gvRoomMap" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    Height="100%" Width="100%">
                    <RowStyle CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField HeaderText="Untis Uploaded Rooms">
                            <ItemTemplate>
                                <asp:Label ID="lblTTM_ROOM" runat="server" Text='<%# bind("TTM_ROOM") %>'></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Map it with OASIS">
                            <ItemTemplate>
                                <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="OASIS Available Rooms">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlUntis_RM_Name" runat="server" CssClass="ddlanswer">
                                </asp:DropDownList>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative"  />
                </asp:GridView>
            </asp:Panel>
        </td>

    </tr>
    <tr>
        <td align="center" style="padding-top: 6px;">

            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                />
            <asp:Button ID="btnBack" runat="server" CausesValidation="False"
                CssClass="button" Text="Back"  />

        </td>
    </tr>
</table>
