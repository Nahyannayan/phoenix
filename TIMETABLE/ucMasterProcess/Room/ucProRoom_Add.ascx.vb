﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_ucMasterProcess_ucProRoom_Add
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("acd_id") = "0"
                ViewState("SET_TYPE") = "0"
                Dim arr As String() = New String(2) {}

                If Not Session("ACD_TYPE_TT") Is Nothing Then
                    arr = Session("ACD_TYPE_TT").ToString.Split("|")
                    ViewState("acd_id") = arr(0)
                    ViewState("SET_TYPE") = arr(1)
                End If

                bindRoomData(ViewState("acd_id"), ViewState("SET_TYPE"))


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

    Sub bindRoomData(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "TM.GETMISSING_ROOMS", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvRoomMissing.DataSource = ds.Tables(0)
                gvRoomMissing.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                ds.Tables(0).Rows(0)("bSHOW") = False

                gvRoomMissing.DataSource = ds.Tables(0)
                Try
                    gvRoomMissing.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvRoomMissing.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvRoomMissing.Rows(0).Cells.Clear()
                gvRoomMissing.Rows(0).Cells.Add(New TableCell)
                gvRoomMissing.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRoomMissing.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRoomMissing.Rows(0).Cells(0).Text = "No Missing Rooms Available !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    
    Protected Sub gvRoomMissing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRoomMissing.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeader(" + DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).Attributes.Add("onclick", strScript)
            End If
 Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\TIMETABLE\tblMast_ProcessTT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then

            bindRoomData(ViewState("acd_id"), ViewState("SET_TYPE"))
            lblErr.Text = "Record Saved Successfully"

        ElseIf str_err = "2" Then
            lblErr.Text = "Please select the rooms to be added !!!"
        Else
            lblErr.Text = errorMessage
        End If
    End Sub

    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim chkSelect As CheckBox
        Dim lblROOM_SHORTNAME As Label
        Dim txtFullName As New TextBox
        Dim STR_XML As New StringBuilder
        Dim FULLNAME As String = String.Empty

        For Each row As GridViewRow In gvRoomMissing.Rows
            chkSelect = row.FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblROOM_SHORTNAME = row.FindControl("lblROOM_SHORTNAME")
                txtFullName = row.FindControl("txtFullName")
                FULLNAME = txtFullName.Text.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")

                STR_XML.Append(lblROOM_SHORTNAME.Text & "$" & IIf(FULLNAME.Trim = "", " ", FULLNAME) & "|")
            End If

        Next

        If STR_XML.ToString() = "" Then
            CALLTRANSACTION = 2

            Exit Function
        End If

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("acd_id"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@ROOMS", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TM.SAVEROOM_S_MISSING", PARAM)
                ReturnFlag = PARAM(4).Value


                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If




            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function
End Class
