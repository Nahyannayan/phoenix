﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucProClass_Map.ascx.vb"
    Inherits="TIMETABLE_ucMasterProcess_ucProClass_Map" %>


<table width="100%">
    <tr valign="bottom">
        <td align="left" valign="middle">
           <span class="error"> <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></span>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Panel ID="plClassMap" runat="server" ScrollBars="Vertical" Width="100%">
                <asp:GridView ID="gvClassMap" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row" Height="100%" Width="100%">
                    <RowStyle CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField HeaderText="Untis Uploaded Class">
                            <ItemTemplate>

                                <asp:Label ID="lblTTM_CLASS" runat="server" Text='<%# bind("TTM_CLASS") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblTTM_CLASS_TOMAP" runat="server" Text='<%# Bind("TTM_CLASS_TOMAP")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblTTM_CLASS_DISPLAY" runat="server" Text='<%# bind("TTM_CLASS_DISPLAY") %>'></asp:Label>

                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Map it with OASIS">
                            <ItemTemplate>
                                <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OASIS Available Class">
                            <EditItemTemplate></EditItemTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlUntis_CS_Name" runat="server" onchange="validate()" CssClass="ddlanswer">
                                </asp:DropDownList>

                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative"  />
                </asp:GridView>
            </asp:Panel>
        </td>

    </tr>
    <tr>
        <td align="center" style="padding-top: 6px;">

            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                />
            <asp:Button ID="btnBack" runat="server" CausesValidation="False"
                CssClass="button" Text="Back"  />

        </td>
    </tr>
</table>
