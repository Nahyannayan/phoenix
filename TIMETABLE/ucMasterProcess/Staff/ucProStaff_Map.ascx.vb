﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TIMETABLE_ucMasterProcess_ucProStaff_Map
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Page.IsPostBack = False Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("acd_id") = "0"
                ViewState("SET_TYPE") = "0"
                Dim arr As String() = New String(2) {}

                If Not Session("ACD_TYPE_TT") Is Nothing Then
                    arr = Session("ACD_TYPE_TT").ToString.Split("|")
                    ViewState("acd_id") = arr(0)
                    ViewState("SET_TYPE") = arr(1)
                End If
                bind_dropdown(ViewState("acd_id"), ViewState("SET_TYPE"))
                bindTeacherData_OASIS(ViewState("acd_id"), ViewState("SET_TYPE"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Public Sub bind_dropdown(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Session("TTStaff_map") = Nothing
        Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim ds As New DataSet
        Dim UNTISTEACHER As New List(Of ListItem)
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETOASIS_TEACHER]", PARAM)
        UNTISTEACHER.Add(New ListItem("Teacher Not Mapped", "-1"))
        If ds.Tables(0).Rows.Count > 0 Then
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                UNTISTEACHER.Add(New ListItem(ds.Tables(0).Rows(I)("ENAME"), ds.Tables(0).Rows(I)("SHORTNAME")))
            Next

        End If
        Session("TTStaff_map") = UNTISTEACHER
    End Sub
    Sub bindTeacherData_OASIS(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TM].[GETTIMETABLE_TEACHER]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvStaffMap.DataSource = ds.Tables(0)
                gvStaffMap.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                gvStaffMap.DataSource = ds.Tables(0)
                Try
                    gvStaffMap.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStaffMap.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStaffMap.Rows(0).Cells.Clear()
                gvStaffMap.Rows(0).Cells.Add(New TableCell)
                gvStaffMap.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStaffMap.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStaffMap.Rows(0).Cells(0).Text = "No Staff Available !!!"
                Dim imgMap As Image = DirectCast(gvStaffMap.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvStaffMap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStaffMap.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlUntis_Staff_Name As DropDownList = DirectCast(e.Row.FindControl("ddlUntis_Staff_Name"), DropDownList)
                Dim lblTTM_TEACHER As Label = DirectCast(e.Row.FindControl("lblTTM_TEACHER"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)
                If Not Session("TTStaff_map") Is Nothing Then
                    ddlUntis_Staff_Name.Items.Clear()
                    ddlUntis_Staff_Name.DataSource = Session("TTStaff_map")
                    ddlUntis_Staff_Name.DataTextField = "Text"
                    ddlUntis_Staff_Name.DataValueField = "Value"
                    ddlUntis_Staff_Name.DataBind()

                    ddlUntis_Staff_Name.Attributes.Add("onchange", "validate('" & gvStaffMap.ClientID & "')")
                    For Each ITEM As ListItem In ddlUntis_Staff_Name.Items
                        If ITEM.Text.Contains("(" + lblTTM_TEACHER.Text + ")") = True Then

                            ITEM.Selected = True
                            imgMap.ImageUrl = "~/Images/arrowgreen.png"
                            Exit For
                        End If
                    Next

                    'If Not ddlUntis_Staff_Name.Items.FindByValue(lblTTM_TEACHER.Text) Is Nothing Then
                    '    ddlUntis_Staff_Name.ClearSelection()
                    '    ddlUntis_Staff_Name.Items.FindByValue(lblTTM_TEACHER.Text).Selected = True
                    '    imgMap.ImageUrl = "~/Images/arrowgreen.png"
                    'End If
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\TIMETABLE\tblMast_ProcessTT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then

            bind_dropdown(ViewState("acd_id"), ViewState("SET_TYPE"))
            bindTeacherData_OASIS(ViewState("acd_id"), ViewState("SET_TYPE"))

            lblErr.Text = "Record Saved Successfully"

        Else
            lblErr.Text = errorMessage
        End If
    End Sub

    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim ddlUntis_Staff_Name As DropDownList
        Dim lblTTM_TEACHER As Label
        Dim STR_XML As New StringBuilder

        For Each row As GridViewRow In gvStaffMap.Rows
            ddlUntis_Staff_Name = row.FindControl("ddlUntis_Staff_Name")
            lblTTM_TEACHER = row.FindControl("lblTTM_TEACHER")
            If ddlUntis_Staff_Name.SelectedValue.ToString.Trim <> "-1" Then

                STR_XML.Append(lblTTM_TEACHER.Text & "#" & ddlUntis_Staff_Name.SelectedValue.ToString.Trim & "|")
            End If

        Next

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("acd_id"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@STAFF", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TM.SAVETEACHER_S_MAPPING", PARAM)
                ReturnFlag = PARAM(4).Value


                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If




            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function
End Class
