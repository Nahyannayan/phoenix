﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucProStaff_Add.ascx.vb"
    Inherits="TIMETABLE_ucMasterProcess_ucProStaff_Add" %>
<script language="javascript" type="text/javascript">
    function SelectAll(CheckBox) {
        TotalChkBx = parseInt('<%=gvStaffMissing.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvStaffMissing.ClientID %>');
            var TargetChildControl = "chkSelect";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeader(CheckBox) {
            TotalChkBx = parseInt('<%=gvStaffMissing.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvStaffMissing.ClientID %>');
            var TargetChildControl = "chkSelect";
            var TargetHeaderControl = "chkSelectAll";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }


</script>
<table width="100%">
    <tr valign="bottom">
        <td align="left" valign="middle">
            <span class="error">
                <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></span>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical">
                <asp:GridView ID="gvStaffMissing" runat="server" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="False" EmptyDataText="No staff added yet">
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                    <Columns>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" Visible='<%# Bind("bSHOW") %>'></asp:CheckBox>

                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                            <HeaderStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Short Name">
                            <ItemTemplate>
                                <asp:Label ID="lblTEACHER_SHORTNAME" runat="server"
                                    Text='<%# BIND("TEACHER_SHORTNAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="Khaki" />
                    <HeaderStyle CssClass="gridheader_new"  />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>

            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td  valign="bottom" align="center">
          
            <asp:Button ID="btnSave"
                runat="server" CssClass="button" Text="Save"
                ValidationGroup="groupM1" />
            <asp:Button
                ID="btnBack" runat="server" CausesValidation="False" CssClass="button"
                Text="Back" 
                />
        </td>
    </tr>
</table>
