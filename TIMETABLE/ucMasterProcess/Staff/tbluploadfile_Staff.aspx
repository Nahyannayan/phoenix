﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tbluploadfile_Staff.aspx.vb" Inherits="TIMETABLE_tbluploadfile_Staff" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">



<head id="Head1" runat="server">
    <link href="/PHOENIXBETAV2/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="/PHOENIXBETAV2/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
    <title>Untitled Page</title>
</head>

 
<script language="javascript" type="text/javascript">
 //Trim the input text
  function Trim(input)
  {
    var lre = /^\s*/; 
    var rre = /\s*$/; 
    input = input.replace(lre, ""); 
    input = input.replace(rre, ""); 
    return input; 
   }
 
   // filter the files before Uploading for text file only  
   function CheckUploadedFile() 
   {
        var file = document.getElementById('<%=fileload.ClientID%>');
        var fileName=file.value;    
        //var objFSO = new ActiveXObject("Scripting.FileSystemObject");
//            var files;
//            var size;
//             var path = file.value;    
        //Checking for file browsed or not 
        if (Trim(fileName) =='' )
        {
            alert("Please select a file to upload!!!");
            file.focus();
            return false;
        }
 
       //Setting the extension array for diff. type of text files 
         
       var extType= new Array(".xls",".xlsx");

       //getting the file name
       while (fileName.indexOf("\\") != -1)
         fileName = fileName.slice(fileName.indexOf("\\") + 1);
 
 
       //Getting the file extension                     
       var ext = fileName.slice(fileName.lastIndexOf(".")).toLowerCase();
 
       //matching extension with our given extensions.
    for (var i = 0; i < extType.length; i++) 
        {

         if (extType[i] == ext) 
         { 
           return true;
         }
         
           }  



         alert("Please only upload files that end in types:  "+(extType.join("  ").toUpperCase())  
           +  "\nPlease select a new "
           + "file to upload and submit again.");
           file.focus();
           return false;                
   }  
 

    </script>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FileUpload ID="fileload" runat="server" />
            <asp:Button ID="btnProcess" runat="server"
                Text="Process" OnClientClick="return CheckUploadedFile();" CssClass="button" />

            <div >
                <asp:Literal ID="ltNote" runat="server"></asp:Literal>
                <asp:Label ID="lblErrorLoad" runat="server" Text="" CssClass="error"></asp:Label>
                <asp:HiddenField ID="hf_reload" runat="server" Value="0" />
            </div>
        </div>    
   </form>
</body>
</html>
