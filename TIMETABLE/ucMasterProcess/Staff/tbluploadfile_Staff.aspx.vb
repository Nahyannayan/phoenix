﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports ICSharpCode.SharpZipLib
Imports System.Data.OleDb
Imports System.Data.Common

Partial Class TIMETABLE_tbluploadfile_Staff
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click


        Try
            Dim fs As New FileInfo(fileload.PostedFile.FileName)
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength ' get the file size
            Dim newFileName As String = "fPath_" & Now.ToString("yyyyMddhhmmss") 'create the unique folder fpath_f2131dsa
            Dim strPostedFileName As String = fs.Name 'get the file name
            Session("TT_Folder_Name") = "\" & Session("sBsuid") & "\TEMP_TXT\"
            Dim savePath As String = String.Empty



            Dim StaffDataset As New DataSet()
            savePath = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString & Session("TT_Folder_Name")

            Dim excel_conn As String = String.Empty
            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".xls") Or (strExtn = ".xlsx") Then ' if zip file processed



                    If Not Directory.Exists(savePath) Then
                        Directory.CreateDirectory(savePath)

                    End If
                    savePath += newFileName & strExtn
                    fileload.SaveAs(savePath)

                    If strExtn = ".xls" Then
                        excel_conn = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savePath & ";Extended Properties=""Excel 8.0;"""
                    ElseIf strExtn = ".xlsx" Then
                        excel_conn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savePath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                    End If


                    Try




                        Dim StaffData As New OleDbDataAdapter("SELECT EmpNo,Name,ShortCode FROM [Sheet1$]", excel_conn)
                        StaffData.TableMappings.Add("TableStaff", "ExcelStaff")
                        StaffData.Fill(StaffDataset)

                    Catch ex As Exception

                        Dim StaffData As New OleDbDataAdapter("SELECT EmpNo,Name,ShortCode FROM [Sheet1$]", excel_conn)
                        StaffData.TableMappings.Add("TableStaff", "ExcelStaff")
                        StaffData.Fill(StaffDataset)

                    End Try
                    Dim i = 0
                    Dim EmpNo As String
                    Dim ShortCode As String
                    Dim errorflag As Boolean
                    Dim str_query As String = String.Empty


                    Dim str_Xml As New StringBuilder
                    For i = 0 To StaffDataset.Tables(0).Rows.Count - 1

                        EmpNo = StaffDataset.Tables(0).Rows(i).Item("EMPNO").ToString()
                        ShortCode = StaffDataset.Tables(0).Rows(i).Item("ShortCode").ToString().Replace("'", "").Trim()

                        str_Xml.Append(EmpNo & "~" & ShortCode & "|")


                    Next


                    File.Delete(savePath)


                    Dim str_err As String = String.Empty
                    Dim errorMessage As String = String.Empty
                    str_err = calltransaction(str_Xml.ToString)


                    If str_err = "0" Then
                        ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
                        lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#eaebec;'>File uploaded successfully...</div>"
                    Else
                        lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>Error occured while saving !!!</div>"
                    End If

                Else
                    lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>Please upload a valid Document with the extenion in : .xls or xlsx</div>"

                End If
            Else
                lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>There is no file to Upload.</div>"""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            lblErrorLoad.Text = "Error while Updating the file !!!"
        End Try
    End Sub


    Function calltransaction(ByRef str_Xml As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty
        Dim clientIp As String = Request.UserHostAddress()
        Dim ClientBrw As String = String.Empty
        Dim mcName As String = String.Empty
        Dim MAC_ID As String = IRDataTables.GetMACAddress
        IRDataTables.GetClientInfo(ClientBrw, clientIp, mcName)

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim PARAM(12) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(1) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_id"))
                PARAM(2) = New SqlClient.SqlParameter("@STR_XML", str_Xml.ToString)
                PARAM(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(3).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TT.SAVEEMP_SHORTCODE", PARAM)
                ReturnFlag = PARAM(3).Value
                If ReturnFlag <> 0 Then
                    calltransaction = 1
                Else
                    calltransaction = 0
                End If
            Catch ex As Exception
                calltransaction = 1
            Finally
                If calltransaction <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
    End Sub


End Class
