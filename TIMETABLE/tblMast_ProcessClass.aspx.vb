﻿
Partial Class TIMETABLE_tblMast_ProcessClass
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            TabSelect()
        End If

    End Sub
    Public Sub TabSelect()
        If rbAddClass.Checked = True Then

            ProcClass_Add_ctr.Visible = True
            ProcClass_Map_ctr.Visible = False
            callUserControl_Proc(0)

        Else
            ProcClass_Add_ctr.Visible = False
            ProcClass_Map_ctr.Visible = True
            callUserControl_Proc(1)

        End If
    End Sub

    Private Sub callUserControl_Proc(ByVal TabIndex As Integer)


        Dim arr As String() = New String(2) {}

        If Not Session("ACD_TYPE_TT") Is Nothing Then
            arr = Session("ACD_TYPE_TT").ToString.Split("|")
        Else
            arr(0) = "0"
            arr(1) = ""
        End If

        If TabIndex = 0 Then
            ProcClass_Add_ctr.bindClassData(arr(0), arr(1))
        ElseIf TabIndex = 1 Then
            ProcClass_Map_ctr.bind_dropdown(arr(0), arr(1))
            ProcClass_Map_ctr.bindClassData_OASIS(arr(0), arr(1))
            'ElseIf TabIndex = 2 Then
            '    ProcClass_Create_ctr.bindData_Class()
        End If
    End Sub


    Protected Sub rbAddClass_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddClass.CheckedChanged
        TabSelect()
    End Sub

    Protected Sub rbAddMap_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddMap.CheckedChanged
        TabSelect()
    End Sub
End Class
