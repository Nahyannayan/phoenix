﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="tblEmp_Update.aspx.vb" Inherits="TIMETABLE_tblEmp_Update" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function SelectAllEMP(CheckBox) {
            TotalChkBx = parseInt('<%=gvEmp.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvEmp.ClientID %>');
            var TargetChildControl = "chkSelectEMP";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderEMP(CheckBox) {
            TotalChkBx = parseInt('<%=gvEmp.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvEmp.ClientID %>');
            var TargetChildControl = "chkSelectEMP";
            var TargetHeaderControl = "chkSelectAllEMP";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            Update Staff Short Name
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" colspan="4"><span class="field-label">Employee Records</span></td>
                                    <%-- <td align="left" width="30%">
                                        <asp:RadioButton ID="rbEmp_Exp" runat="server" Text="Export to Excel" AutoPostBack="True" Checked="True" GroupName="EXP_IMP" CssClass="field-label" />
                                        <asp:RadioButton ID="rbImp_Exp" runat="server" Text="Import to PHOENIX" AutoPostBack="True" GroupName="EXP_IMP" CssClass="field-label" />
                                    </td>
                                    <td colspan="2">
                                        
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Menu ID="mnuMaster" Orientation="Horizontal"
                                            runat="server" OnMenuItemClick="mnuMaster_MenuItemClick" CssClass="menu_a">
                                            <Items>


                                                <asp:MenuItem Value="0" Text="Export to Excel" Selected="True" />
                                                <asp:MenuItem Value="1" Text="Import to PHOENIX" ></asp:MenuItem>
                                               
                                            </Items>
                                            <StaticMenuItemStyle CssClass="menuItem" />
                                            <StaticSelectedStyle CssClass="selectedItem" />
                                            <StaticHoverStyle CssClass="hoverItem" />

                                        </asp:Menu>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td colspan="4">
                                        <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">

                                            <asp:View ID="vwMain" runat="server">
                                                <asp:Panel ID="plEmpExp" runat="server" ScrollBars="Vertical" Width="100%" CssClass="panel-cover">
                                                    <table width="100%"> 
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="gvEmp" runat="server" CssClass="table table-bordered table-row"
                                                                    AutoGenerateColumns="False"
                                                                    EmptyDataText="No  room added yet"
                                                                    Width="100%" EnableModelValidation="True">
                                                                    <RowStyle CssClass="griditem" />
                                                                    <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                                                    <Columns>

                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelectEMP" runat="server"></asp:CheckBox>

                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkSelectAllEMP" runat="server" onclick="SelectAllEMP(this);"
                                                                                    ToolTip="Click here to select/deselect all rows" />
                                                                            </HeaderTemplate>
                                                                            <HeaderStyle />
                                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Sr.No">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle />
                                                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Emp No.">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEmp_no" runat="server" Text='<%# bind("Emp_No") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Emp Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEmpName" runat="server"
                                                                                    Text='<%# BIND("EMP_NAME") %>'></asp:Label>
                                                                                <asp:Label ID="lblEmp_id" runat="server"
                                                                                    Text='<%# BIND("EMP_ID") %>' Visible="False"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Short Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEmp_Short" runat="server" Text='<%# bind("Emp_Shortname") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <SelectedRowStyle BackColor="Khaki" />
                                                                    <HeaderStyle CssClass="gridheader_new" />
                                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="bottom" align="center">
                                                                <asp:Button ID="btnReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>                                                                                                         
                                                </asp:Panel>
                                            </asp:View>
                                            <asp:View ID="vwimportToPhnx" runat="server">
                                                <asp:Panel ID="plempImp" runat="server" ScrollBars="NONE" Width="100%" CssClass="panel-cover">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="title-bg">
                                                                 Excel Upload 
                                                            </td>
                                                        </tr>
                                                        <tr>                                                          
                                                            <td align="left" valign="middle">
                                                                <iframe id="iframeFileLoad" src="ucMasterProcess/Staff/tbluploadfile_Staff.aspx" scrolling="no" frameborder="0"
                                                                    style="text-align: center; vertical-align: middle; border-style: none; margin: 0px; width: 100%;"></iframe>
                                                                <asp:Label ID="ltNote" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </asp:Panel>
                                            </asp:View>
                                        </asp:MultiView>
                                    </td>
                                </tr>

                              
                            </table>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
