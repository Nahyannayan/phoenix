<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tblMast_Period.aspx.vb" Inherits="Students_studAtt_room_Grade_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Period Setting"></asp:Literal>
            </div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <table align="center" border="0" width="100%">
                        <tr>
                            <td align="left" valign="top">
                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        </tr>                     
                        <tr>
                            <td align="left" valign="top">
                                <table id="tbl_test" runat="server" align="center" width="100%"
                                    cellpadding="5" cellspacing="0">
                                    <tr >
                                        <td align="left" valign="top">
                                            <asp:GridView ID="gvPeriod" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                 EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                PageSize="20" >
                                                <RowStyle CssClass="griditem"  />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Grade">
                                                        <EditItemTemplate>
                                                            &nbsp; 
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnGrade" runat="server" Text='<%# bind("GRM_DISPLAY") %>'></asp:LinkButton>
                                                            <asp:Label ID="lblGRD_ID" runat="server" Text='<%# bind("GRM_GRD_ID") %>'
                                                                Visible="False"></asp:Label>

                                                            <ajaxToolkit:HoverMenuExtender ID="hmeGrd" runat="server" TargetControlID="lbtnGrade"
                                                                PopupControlID="plGrd" PopupPosition="right" OffsetX="6" PopDelay="25">
                                                            </ajaxToolkit:HoverMenuExtender>

                                                            <div id="plGrd" runat="server" >                                                                
                                                                <asp:Panel ID="Panel1" runat="server" Width="360px" Height="200px" BackColor="White" CssClass="panel-cover"
                                                                    ScrollBars="Vertical">
                                                                    <div class="title-bg">
                                                                        Periods                                                                     
                                                                    </div>
                                                                    <div>
                                                                        <asp:Literal runat="server" ID="LTGRD"></asp:Literal>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <EditItemTemplate>
                                                            &nbsp;
                                                        </EditItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblViewH" runat="server" Text="Edit"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">Edit</asp:LinkButton>
                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle BackColor="Wheat" />
                                                <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                           
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
</asp:Content>

