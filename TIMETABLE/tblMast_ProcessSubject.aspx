﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="true" CodeFile="tblMast_ProcessSubject.aspx.vb"
    Inherits="TIMETABLE_tblMast_ProcessSubject" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function SelectAll(CheckBox) {
            TotalChkBx = parseInt('<%=gvSubjectMissing_ADD.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvSubjectMissing_ADD.ClientID %>');
            var TargetChildControl = "chkSelect";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeader(CheckBox) {
            TotalChkBx = parseInt('<%=gvSubjectMissing_ADD.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvSubjectMissing_ADD.ClientID %>');
            var TargetChildControl = "chkSelect";
            var TargetHeaderControl = "chkSelectAll";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }

        function validate() {
            var flag = false;
            var dropdowns = new Array(); //Create array to hold all the dropdown lists.
            var imgArr = new Array(); //Create array to hold all the dropdown lists.
            var gridview = document.getElementById('<%=gvSubjectMap_GRP.ClientID %>'); //GridView1 is the id of ur gridview.
            dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
            var imgArr = gridview.getElementsByTagName('img');

            for (var i = 0; i < dropdowns.length; i++) {
                
                if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                {
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowgreen.png"
                    flag = true;
                }
                else {
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowred.png"


                }
            }
        }


        function validateSub() {
            var flag = false;
            var dropdowns = new Array(); //Create array to hold all the dropdown lists.
            var imgArr = new Array(); //Create array to hold all the dropdown lists.
            var gridview = document.getElementById('<%=gvSubjectMap.ClientID%>'); //GridView1 is the id of ur gridview.
            dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
            var imgArr = gridview.getElementsByTagName('img');

            for (var i = 0; i < dropdowns.length; i++) {
               
                if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                {
                    
                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowgreen.png"
                    flag = true;
                }
                else {

                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowred.png"


                }
            }
        }



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="SUBJECT MASTER"></asp:Literal>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left" colspan="4">
                         <span class="error">  <asp:Literal ID="lblErr" runat="server" EnableViewState="false" ></asp:Literal></span> 
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td  align="left" width="20%">
                            <span class="field-label">
                            Select the option</span> 
                            </td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbAddSubj"
                                runat="server" GroupName="subj" Text="Add Temp Subject" Checked="true"  CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbAddMap"  CssClass="field-label"
                                runat="server" GroupName="subj" Text="Process Subject Mapping" AutoPostBack="True" />
                        </td>
                        <td colspan="2"></td>
                    </tr>
                   
                    <tr runat="server" id="trGrp_filter">
                        <td align="left" width="20%">
                             <span class="field-label">Filter subject group based on     </span>                      
                            </td>
                         <td align="left" width="30%">
                            <asp:RadioButton ID="rbNotmap" runat="server" GroupName="Map" CssClass="field-label"
                                Text="Not Mapped" AutoPostBack="True" Checked="true" />
                            <asp:RadioButton ID="rbMap" runat="server" GroupName="Map" CssClass="field-label"
                                    Text="Mapped" AutoPostBack="True" />
                            <asp:RadioButton ID="rbAll" runat="server" GroupName="Map" CssClass="field-label"
                                        Text="All" AutoPostBack="True" /><br />

                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr runat="server" id="trGrp">
                        <td align="left" width="20%"><span class="field-label">Search for untis uploaded subject group</span> </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtGrp_Search" runat="server"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False"
                                CssClass="button" Text="Search" /><br />
                        </td>
                        <td colspan="2"></td>
                    </tr>

                    <tr>
                        <td colspan="4" >

                            <asp:Panel ID="plSubj_Missing" runat="server" ScrollBars="Vertical" Width="100%">
                                <asp:GridView ID="gvSubjectMissing_ADD" runat="server"
                                    AutoGenerateColumns="False"
                                    EmptyDataText="No  room added yet"
                                    CssClass="table table-bordered table-row"
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                    <Columns>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll(this);" ToolTip="Click here to select/deselect all rows" />

                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="center" VerticalAlign="Middle"/>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sr.No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            <HeaderStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Class">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_GRD_ID" runat="server"
                                                    Text='<%# BIND("TTM_GRD_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Subject">
                                            <ItemTemplate>                                               
                                                <asp:Label ID="lblTTM_SUBJECT" runat="server"
                                                    Text='<%# Bind("TTM_SUBJECT")%>' Visible="false" ></asp:Label>
                                                <asp:TextBox  ID="txtTTM_SUBJECT" runat="server" Enabled="true"
                                                    Text='<%# BIND("TTM_SUBJECT") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="Khaki" />
                                    <HeaderStyle CssClass="gridheader_new" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>

                            </asp:Panel>
                            <asp:Panel ID="plSubjectMap" runat="server" ScrollBars="Vertical" Width="100%">
                                <asp:GridView ID="gvSubjectMap" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvSubjectMap_RowDataBound"
                                    CssClass="table table-bordered table-row" Height="100%"
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No">
                                            <ItemTemplate>

                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("R1") %>'></asp:Label>
                                            </ItemTemplate>

                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Class">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_GRD_ID" runat="server" Text='<%# bind("TTM_GRD_ID") %>'></asp:Label>
                                                <asp:HiddenField ID="hidGRD_ID" runat="server" Value='<%# Bind("GRD_GROUP")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stream">
                                            <ItemTemplate >
                                                <asp:Label ID="lblTTM_STM_ID" runat="server" Text ='<%# Bind("STM_DESCR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Subject">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_SUBJECT" runat="server" Text='<%# bind("TTM_SUBJECT") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate></EditItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Map it with OASIS">
                                            <ItemTemplate>
                                                <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phoenix Available Subject Group">
                                            <EditItemTemplate></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlOasis_Subject_Name" runat="server" onchange="validateSub()" CssClass="ddlanswer">
                                                </asp:DropDownList>
                                                <%--added by vikranth 0n 27th Jan 2020--%>
                                                <asp:HiddenField ID="hfTTM_SBG_ID" runat="server" Value='<%# Bind("TTM_SBG_ID")%>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" />
                                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center"
                                        VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </asp:Panel>

                            <asp:GridView ID="gvSubjectMap_GRP" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Height="100%" Width="100%"
                                AllowPaging="true" PageSize="20">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Untis Uploaded Subject Group">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTTM_SUBJECT" runat="server" Text='<%# bind("TTM_SUBJECT") %>'></asp:Label>

                                        </ItemTemplate>


                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Map it with OASIS">
                                        <ItemTemplate>
                                            <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OASIS Available Subject Group">
                                        <EditItemTemplate></EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlUntis_Subject_Name" runat="server" onchange="validate()" CssClass="ddlanswer">
                                            </asp:DropDownList>
                                        </ItemTemplate>



                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>


                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>



                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSaveAdd" runat="server" CssClass="button" Text="Save"
                                ValidationGroup="groupM1" />
                            <asp:Button ID="btnSaveMap" runat="server" CssClass="button" Text="Process Mapping" />
                            <asp:Button ID="btnSaveSub_grp_map" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnBack" runat="server" CausesValidation="False"
                                CssClass="button" Text="Back" />
                        </td>


                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

