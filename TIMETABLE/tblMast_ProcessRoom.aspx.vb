﻿
Partial Class TIMETABLE_tblMast_ProcessRoom
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            TabSelect()
        End If

    End Sub
    Public Sub TabSelect()

        If rbAddMissing.Checked = True Then
            ProcRooms_Add_ctr.Visible = True
            ProcRoom_Map_ctr.Visible = False
            callUserControl_Proc(0)

        Else
            ProcRooms_Add_ctr.Visible = False
            ProcRoom_Map_ctr.Visible = True
            callUserControl_Proc(1)

        End If
    End Sub

    Private Sub callUserControl_Proc(ByVal TabIndex As Integer)


        Dim arr As String() = New String(2) {}

        If Not Session("ACD_TYPE_TT") Is Nothing Then
            arr = Session("ACD_TYPE_TT").ToString.Split("|")
        Else
            arr(0) = "0"
            arr(1) = ""
        End If

        If TabIndex = 0 Then
            ProcRooms_Add_ctr.bindRoomData(arr(0), arr(1))
        ElseIf TabIndex = 1 Then
            ProcRoom_Map_ctr.bind_dropdown(arr(0), arr(1))
            ProcRoom_Map_ctr.bindRoomData_OASIS(arr(0), arr(1))
            'ElseIf TabIndex = 2 Then
            '    ProcRoom_Create_ctr.bindRoomData()
        End If
    End Sub
    Protected Sub rbAddMissing_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddMissing.CheckedChanged
        TabSelect()
    End Sub
    Protected Sub rbAddMap_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAddMap.CheckedChanged
        TabSelect()
    End Sub
End Class
