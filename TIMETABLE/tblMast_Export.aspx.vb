﻿
Partial Class TIMETABLE_tblMast_Export
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Private Function isPageExpired() As Boolean
    '    If Session("TT_Export_Tab") Is Nothing OrElse ViewState("TT_Export_Tab") Is Nothing Then
    '        Return False
    '    ElseIf Session("TT_Export_Tab") = ViewState("TT_Export_Tab") Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010525") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ' TabSelect()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

   



End Class
