Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class tblMast_ProcessTT
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                'Session("sBsuid") = "125040"
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "TT010530") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    BindTIMETABLE_SET()
                    GETACADEMICYEAR()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

        If Not Session("TIMETABLE_ERR_MSG") Is Nothing Then
            If Session("TIMETABLE_ERR_MSG") <> "" Then
                lblError.Text = Session("TIMETABLE_ERR_MSG")
                Session("TIMETABLE_ERR_MSG") = ""
            End If
        End If

        If hfDiv.Value = 0 Then

            div288.Attributes.Add("style", "visibility:hidden")

        End If
        ''Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "scriptHide", _
        ''                        "<script language=javascript>Hide_onload();</script>")
        'Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "keyClientBlock", "<script language=javascript>Hide_onload();</script>")
        ''ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> Hide_onload();</script>", False)
    End Sub
    Sub bindMaster()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim STATUS As String = String.Empty
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlACD_ID.SelectedValue)
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@SET_TYPE", ddlUploadFor.SelectedValue)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TT].[GETMISSING_MASTER]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                STATUS = ds.Tables(0).Rows(0)("RECORD")

                If STATUS <> 0 Then
                    hideR1.Visible = True
                    hideR2.Visible = True
                    hideR3.Visible = True
                    hideR4.Visible = True
                Else
                    hideR1.Visible = False
                    hideR2.Visible = False
                    hideR3.Visible = False
                    hideR4.Visible = False
                End If
                gvMaster.DataSource = ds.Tables(0)
                gvMaster.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                'ds.Tables(0).Rows(0)("flag") = False

                gvMaster.DataSource = ds.Tables(0)
                Try
                    hideR1.Visible = False
                    hideR2.Visible = False
                    hideR3.Visible = False
                    hideR4.Visible = False
                    gvMaster.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvMaster.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvMaster.Rows(0).Cells.Clear()
                gvMaster.Rows(0).Cells.Add(New TableCell)
                gvMaster.Rows(0).Cells(0).ColumnSpan = columnCount
                gvMaster.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvMaster.Rows(0).Cells(0).Text = "No timetable file uploaded !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindTIMETABLE_SET()
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim str_Sql As String
        ddlUploadFor.Items.Clear()

        str_Sql = "SELECT TS_ID,TS_MULTI_TIMEGRID FROM dbo.TIMETABLE_SETTINGS WITH(NOLOCK) WHERE TS_BSU_ID='" & Session("sBsuid") & "'"

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then

            ddlUploadFor.DataSource = ds.Tables(0)
            ddlUploadFor.DataTextField = "TS_MULTI_TIMEGRID"
            ddlUploadFor.DataValueField = "TS_ID"
            ddlUploadFor.DataBind()
        Else
            ddlUploadFor.Items.Add(New ListItem("Single Timegrid", "0"))


        End If
        If Not Session("timegrid") Is Nothing Then
            If Not ddlUploadFor.Items.FindByValue(Session("timegrid")) Is Nothing Then
                ddlUploadFor.Items.FindByValue(Session("timegrid")).Selected = True
            End If
        End If
        Session("timegrid") = ddlUploadFor.SelectedValue
    End Sub

    Private Sub GETACADEMICYEAR()
        Dim STR_CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        Dim QUERY As String = String.Empty
        QUERY = "SELECT ACD_ID,ACY_DESCR FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M  ON ACY_ID=ACD_ACY_ID WHERE ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID ='" & Session("CLM") & "' ORDER BY  ACY_DESCR"

        ddlACD_ID.DataSource = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, QUERY)
        ddlACD_ID.DataTextField = "ACY_DESCR"
        ddlACD_ID.DataValueField = "ACD_ID"
        ddlACD_ID.DataBind()

        If Not ddlACD_ID.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
            ddlACD_ID.ClearSelection()
            ddlACD_ID.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        End If
        ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)
    End Sub
    Protected Sub ddlACD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACD_ID.SelectedIndexChanged
        Session("ACD_TYPE_TT") = ddlACD_ID.SelectedValue.ToString() & "|" & ddlUploadFor.SelectedValue.ToString

        bindMaster()
        GRIDBIND()
    End Sub
    Protected Sub ddlUploadFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUploadFor.SelectedIndexChanged
        Session("ACD_TYPE_TT") = ddlACD_ID.SelectedValue.ToString() & "|" & ddlUploadFor.SelectedValue.ToString
        Session("timegrid") = ddlUploadFor.SelectedValue
        bindMaster()
        GRIDBIND()
    End Sub
    Private Sub GRIDBIND()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim STATUS As String = String.Empty
            Dim txtClass As New TextBox
            Dim txtWeek As New TextBox
            Dim txtTeacher As New TextBox
            Dim txtSubject As New TextBox
            Dim txtRoom As New TextBox
            Dim txtPeriod As New TextBox
            Dim ddlFilterGrp As New DropDownList

            Dim strClass As String
            Dim strWeek As String
            Dim strTeacher As String
            Dim strSubject As String
            Dim strRoom As String
            Dim strPeriod As String
            Dim strGrp As String

            If gvTimetable.Rows.Count > 0 Then


                If Not gvTimetable.HeaderRow.FindControl("txtClass") Is Nothing Then
                    txtClass = gvTimetable.HeaderRow.FindControl("txtClass")
                    strClass = txtClass.Text

                    txtWeek = gvTimetable.HeaderRow.FindControl("txtWeek")
                    strWeek = txtWeek.Text

                    txtTeacher = gvTimetable.HeaderRow.FindControl("txtTeacher")
                    strTeacher = txtTeacher.Text

                    txtSubject = gvTimetable.HeaderRow.FindControl("txtSubject")
                    strSubject = txtSubject.Text

                    txtRoom = gvTimetable.HeaderRow.FindControl("txtRoom")
                    strRoom = txtRoom.Text

                    txtPeriod = gvTimetable.HeaderRow.FindControl("txtPeriod")
                    strPeriod = txtPeriod.Text

                    ddlFilterGrp = gvTimetable.HeaderRow.FindControl("ddlFilterGrp")
                    strGrp = ddlFilterGrp.SelectedValue
                End If

            End If
            'txtClass.Text


            Dim ds As New DataSet
            Dim PARAM(10) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlACD_ID.SelectedValue)
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@SET_TYPE", ddlUploadFor.SelectedValue)
            PARAM(3) = New SqlParameter("@Class", IIf(txtClass.Text = "", System.DBNull.Value, txtClass.Text.Trim))
            PARAM(4) = New SqlParameter("@Week", IIf(txtWeek.Text = "", System.DBNull.Value, txtWeek.Text.Trim))
            PARAM(5) = New SqlParameter("@Teach", IIf(txtTeacher.Text = "", System.DBNull.Value, txtTeacher.Text.Trim))
            PARAM(6) = New SqlParameter("@Sub", IIf(txtSubject.Text = "", System.DBNull.Value, txtSubject.Text.Trim))
            PARAM(7) = New SqlParameter("@Room", IIf(txtRoom.Text = "", System.DBNull.Value, txtRoom.Text.Trim))
            PARAM(8) = New SqlParameter("@Period", IIf(txtPeriod.Text = "", System.DBNull.Value, txtPeriod.Text.Trim))


            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TT].[GETTIMETABLE_DETAILS]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then


                If Not String.IsNullOrEmpty(strGrp) Then
                    If strGrp <> "0" Then
                        Dim dataView As DataView = ds.Tables(0).DefaultView
                        If strGrp = "1" Then
                            dataView.RowFilter = "TTM_SGR_ID <> '' AND TTM_SGR_ID <> '0' AND TTM_SGR_ID <> 'NULL' "
                        ElseIf strGrp = "2" Then
                            dataView.RowFilter = "TTM_SGR_ID = '' OR TTM_SGR_ID = '0' OR TTM_SGR_ID = 'NULL' "
                        End If
                        gvTimetable.DataSource = dataView
                        gvTimetable.DataBind()
                    Else gvTimetable.DataSource = ds.Tables(0)
                        gvTimetable.DataBind()
                    End If
                Else gvTimetable.DataSource = ds.Tables(0)
                    gvTimetable.DataBind()
                End If


            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                'ds.Tables(0).Rows(0)("flag") = False

                gvTimetable.DataSource = ds.Tables(0)
                Try
                    gvTimetable.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTimetable.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvTimetable.Rows(0).Cells.Clear()

                gvTimetable.Rows(0).Cells.Add(New TableCell)
                gvTimetable.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTimetable.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTimetable.Rows(0).Cells(0).Text = "No timetable file uploaded !!!"

            End If

            If Not gvTimetable.HeaderRow.FindControl("txtClass") Is Nothing Then
                txtClass = gvTimetable.HeaderRow.FindControl("txtClass")
                txtClass.Text = strClass

                txtWeek = gvTimetable.HeaderRow.FindControl("txtWeek")
                txtWeek.Text = strWeek

                txtTeacher = gvTimetable.HeaderRow.FindControl("txtTeacher")
                txtTeacher.Text = strTeacher

                txtSubject = gvTimetable.HeaderRow.FindControl("txtSubject")
                txtSubject.Text = strSubject

                txtRoom = gvTimetable.HeaderRow.FindControl("txtRoom")
                txtRoom.Text = strRoom

                txtPeriod = gvTimetable.HeaderRow.FindControl("txtPeriod")
                txtPeriod.Text = strPeriod

                ddlFilterGrp = gvTimetable.HeaderRow.FindControl("ddlFilterGrp")
                ddlFilterGrp.SelectedValue = strGrp

            End If

            'txtClass.Text = ""
            'txtWeek.Text = ""
            'txtTeacher.Text = ""
            'txtSubject.Text = ""
            'txtRoom.Text = ""
            'txtPeriod.Text = ""



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvTimetable_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTimetable.PageIndexChanging
        gvTimetable.PageIndex = e.NewPageIndex
        GRIDBIND()

        'div288.Attributes.Add("style", "display:none")
        ' div288.Visible = False
    End Sub

    Protected Sub lbtnMaster_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblr1 As New Label
            Dim url As String
            Dim viewid As String
            lblr1 = TryCast(sender.FindControl("lblr1"), Label)
            viewid = lblr1.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Session("ACD_TYPE_TT") = ddlACD_ID.SelectedValue.ToString() & "|" & ddlUploadFor.SelectedValue.ToString

            If lblr1.Text = "1" Then
                url = String.Format("~\TIMETABLE\tblMast_ProcessRoom.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf lblr1.Text = "2" Then
                url = String.Format("~\TIMETABLE\tblMast_ProcessClass.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf lblr1.Text = "3" Then
                url = String.Format("~\TIMETABLE\tblMast_ProcessStaff.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf lblr1.Text = "4" Then
                url = String.Format("~\TIMETABLE\tblMast_ProcessSubject.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If


            Response.Redirect(url)
        Catch ex As Exception
            '.Text = "Request could not be processed "
        End Try
    End Sub


    Protected Sub btnClass_Src_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GRIDBIND()
    End Sub
    Protected Sub btnWeek_Src_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GRIDBIND()
    End Sub
    Protected Sub btnTeacher_Src_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GRIDBIND()
    End Sub
    Protected Sub btnSubject_Src_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GRIDBIND()
    End Sub
    Protected Sub btnRoom_Src_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GRIDBIND()
    End Sub
    Protected Sub btnPeriod_Src_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GRIDBIND()
    End Sub

    Private Sub GRIDBIND_Time()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim STATUS As String = String.Empty

            Dim ds As New DataSet
            Dim PARAM(1) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[TT].[GETTIMETABLE_SETTINGS]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then


                gvTime.DataSource = ds.Tables(0)
                gvTime.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                'ds.Tables(0).Rows(0)("flag") = False

                gvTime.DataSource = ds.Tables(0)
                Try
                    gvTime.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTime.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvTime.Rows(0).Cells.Clear()

                gvTime.Rows(0).Cells.Add(New TableCell)
                gvTime.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTime.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTime.Rows(0).Cells(0).Text = "No timegrid available !!!"

            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub resettime()
        ltGridError.Text = ""
        hfTS_ID.Value = "0"
        txtTGrid.Text = ""
        ddlStartWK.ClearSelection()
        ddlStartWK.SelectedIndex = 0
        ddlTotWK.ClearSelection()
        ddlTotWK.SelectedIndex = 4
        rbNo.Checked = True
        btnAddSaveGrid.Visible = True
        btnUpdateSaveGrid.Visible = False
        btnCancelSaveGrid.Visible = False
    End Sub

    Protected Sub lbtnAddTimeGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddTimeGrid.Click


        resettime()
        GRIDBIND_Time()
        gvTime.Columns(4).Visible = True

        plReopen.Visible = True
    End Sub
    Protected Sub lbtnEditGRP_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim TS_TOT_WEEK_DAY As String = String.Empty
            Dim TS_START_WEEK As String = String.Empty
            Dim TS_ID As String = String.Empty
            Dim TS_MULTI_TIMEGRID As String = String.Empty
            Dim TS_bSHOWSUBJECT_GRP As Boolean

            hfTS_ID.Value = TryCast(sender.FindControl("lblTS_ID"), Label).Text
            TS_TOT_WEEK_DAY = TryCast(sender.FindControl("lblTS_TOT_WEEK_DAY"), Label).Text
            TS_START_WEEK = TryCast(sender.FindControl("lblTS_START_WEEK"), Label).Text
            txtTGrid.Text = TryCast(sender.FindControl("lblTS_MULTI_TIMEGRID"), Label).Text
            TS_bSHOWSUBJECT_GRP = TryCast(sender.FindControl("lblTS_bSHOWSUBJECT_GRP"), Label).Text
            If Not ddlStartWK.Items.FindByValue(TS_START_WEEK) Is Nothing Then
                ddlStartWK.ClearSelection()
                ddlStartWK.Items.FindByValue(TS_START_WEEK).Selected = True
            End If
            If Not ddlTotWK.Items.FindByValue(TS_TOT_WEEK_DAY) Is Nothing Then
                ddlTotWK.ClearSelection()
                ddlTotWK.Items.FindByValue(TS_TOT_WEEK_DAY).Selected = True
            End If
            If TS_bSHOWSUBJECT_GRP = True Then
                rbYes.Checked = True
                rbNo.Checked = False
            ElseIf TS_bSHOWSUBJECT_GRP = False Then
                rbYes.Checked = True
                rbNo.Checked = False
            End If

            btnAddSaveGrid.Visible = False
            btnUpdateSaveGrid.Visible = True
            btnCancelSaveGrid.Visible = True
            gvTime.Columns(4).Visible = False

            plReopen.Visible = True
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnAddSaveGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSaveGrid.Click
        If Page.IsValid = True Then
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            If txtTGrid.Text.Trim <> "" Then


                str_err = calltransaction_time(errorMessage)
                If str_err = "0" Then
                    resettime()
                    gvTime.Columns(4).Visible = True
                    BindTIMETABLE_SET()
                    GRIDBIND_Time()
                    ltGridError.Text = "Record Saved Successfully"

                Else
                    ltGridError.Text = errorMessage
                End If
            Else
                ltGridError.Text = "Timegrid cannot be left empty"
            End If
        End If

        plReopen.Visible = True

    End Sub

    Protected Sub btnUpdateSaveGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSaveGrid.Click
        If Page.IsValid = True Then
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            If txtTGrid.Text.Trim <> "" Then

                str_err = calltransaction_time(errorMessage)
                If str_err = "0" Then
                    resettime()
                    gvTime.Columns(4).Visible = True
                    BindTIMETABLE_SET()
                    GRIDBIND_Time()
                    ltGridError.Text = "Record Saved Successfully"

                Else
                    ltGridError.Text = errorMessage
                End If
            Else
                ltGridError.Text = "Timegrid cannot be left empty"
            End If
        End If


        plReopen.Visible = True
    End Sub
    Function calltransaction_time(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean

        Dim GRD_IDs As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim str As String = String.Empty


        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                bEdit = False

                Dim PARAM(6) As SqlParameter

                PARAM(0) = New SqlParameter("@TS_BSU_ID", BSU_ID)
                PARAM(1) = New SqlParameter("@TS_MULTI_TIMEGRID", txtTGrid.Text.Trim)
                PARAM(2) = New SqlParameter("@TS_START_WEEK", ddlStartWK.SelectedValue)
                PARAM(3) = New SqlParameter("@TS_TOT_WEEK_DAY", ddlTotWK.SelectedValue)
                PARAM(4) = New SqlParameter("@TS_bSHOWSUBJECT_GRP", IIf(rbNo.Checked = True, 0, 1))
                PARAM(5) = New SqlParameter("@TS_ID", hfTS_ID.Value)
                PARAM(6) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(6).Direction = ParameterDirection.ReturnValue


                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TT.SAVETIMETABLE_SETTING", PARAM)

                status = CInt(PARAM(6).Value)


                If status <> 0 Then
                    calltransaction_time = "1"
                    errorMessage = "Error Occured While Saving."
                    Return "1"
                End If


                calltransaction_time = "0"


            Catch ex As Exception
                calltransaction_time = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction_time <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using



    End Function

    Sub BindTimetableGroups(ddlGroup As DropDownList, sgr_id As String, sbg_id As String, emp_id As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_query As String = "exec getTIMETABLE_GROUPS @SBG_ID='" + sbg_id + "',@EMP_ID='" + emp_id + "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlGroup.DataSource = ds.Tables(0)
            ddlGroup.DataTextField = "SGR_DESCR"
            ddlGroup.DataValueField = "SGR_ID"
            ddlGroup.DataBind()

            Dim Litem As New ListItem
            Litem.Text = "--"
            Litem.Value = "0"
            ddlGroup.Items.Insert(0, Litem)

            If Not ddlGroup.Items.FindByValue(sgr_id) Is Nothing Then
                ddlGroup.Items.FindByValue(sgr_id).Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnUpdateGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim sgr_id As String
            Dim strPeriod As String
            Dim strEmpId As String
            Dim strSbgId As String
            Dim strClass As String
            Dim strDays As String
            strPeriod = TryCast(sender.FindControl("lblPeriod"), Label).Text
            strEmpId = TryCast(sender.FindControl("lblEMP_ID"), Label).Text
            strSbgId = TryCast(sender.FindControl("lblSBG_ID"), Label).Text
            strClass = TryCast(sender.FindControl("lblUntisClass"), Label).Text
            strDays = TryCast(sender.FindControl("lblDays"), Label).Text
            sgr_id = TryCast(sender.FindControl("ddlGroup"), DropDownList).SelectedValue

            Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
            Dim str_query As String = "exec UPDATETIMETABLE_GROUPS " _
                                     & "@ACD_ID='" + ddlACD_ID.SelectedValue.ToString + "'," _
                                     & "@SBG_ID='" + strSbgId + "'," _
                                     & "@EMP_ID='" + strEmpId + "'," _
                                     & "@PERIOD='" + strPeriod + "'," _
                                     & "@DAY='" + strDays + "'," _
                                     & "@CLASS='" + strClass + "'," _
                                     & "@SGR_ID='" + sgr_id + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnCancelSaveGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSaveGrid.Click

        resettime()
        gvTime.Columns(4).Visible = True
        plReopen.Visible = True
    End Sub
    'Protected Sub Imageclose_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Imageclose.Click
    '    'plReopen.Visible = False
    '    Me.mdlReopen.Hide()
    'End Sub

    Protected Sub Imageclose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imageclose.Click
        plReopen.Visible = False
    End Sub

    Protected Sub gvTimetable_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvTimetable.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlGroup As DropDownList = DirectCast(e.Row.FindControl("ddlGroup"), DropDownList)
            Dim lblSGR As Label = DirectCast(e.Row.FindControl("lblSGR_ID"), Label)
            Dim lblSBG_ID As Label = DirectCast(e.Row.FindControl("lblSBG_ID"), Label)
            Dim lblEMP_ID As Label = DirectCast(e.Row.FindControl("lblEMP_ID"), Label)

            BindTimetableGroups(ddlGroup, lblSGR.Text, lblSBG_ID.Text, lblEMP_ID.Text)
        End If
    End Sub
    Protected Sub ddlFilterGrp_SelectedIndexChanged(sender As Object, e As EventArgs)
        GRIDBIND()
    End Sub
End Class

