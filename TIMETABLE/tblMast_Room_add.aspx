<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tblMast_Room_add.aspx.vb" Inherits="tblMast_Room_add" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Add/Edit Rooms"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="bottom">
                            <table align="center" width="100%">
                                <tr class="title-bg">
                                    <td  colspan="4">
                                        <asp:Literal ID="ltLabel" runat="server" Text="Room Master"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Room Full Name</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%"> 
                                        <asp:TextBox ID="txtFullName" runat="server"
                                            ToolTip="Enter room full name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFullName" runat="server"
                                            ControlToValidate="txtFullName" CssClass="error" ErrorMessage="*" ForeColor=""
                                            ValidationGroup="grvir">Required</asp:RequiredFieldValidator>
                                    </td>
                                     
                                    <td align="left" width="20%"><span class="field-label">Short Name(Display Name)</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtShortName" runat="server" MaxLength="20"
                                            ToolTip="Enter room short name of length not more than 20 character long without space."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtShortName" CssClass="error" ErrorMessage="*" ForeColor=""
                                            ValidationGroup="grvir">Required</asp:RequiredFieldValidator>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbTxtShortname" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                            InvalidChars=" " TargetControlID="txtShortName">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Virtual Room</span></td>

                                    <td align="left" width="30%">
                                        <asp:RadioButton ID="rbYes" runat="server" GroupName="vir" Text="Yes" CssClass="field-label" />
                                        <asp:RadioButton ID="rbNo" runat="server" GroupName="vir" Text="No" Checked="true" CssClass="field-label" />

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"
                                        colspan="4">
                                        <asp:Button ID="btnAddROOM" runat="server" CssClass="button"
                                            Text="Add" ValidationGroup="grvir" />
                                        <asp:Button ID="btnUpdateRoom" runat="server" CssClass="button"
                                            Text="Update" ValidationGroup="grvir" />
                                        <asp:Button ID="btnCancelROOM" runat="server" CssClass="button"
                                            Text="Cancel" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvRooms" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="40"
                                            EmptyDataText="No record added yet" CssClass="table table-bordered table-row"
                                            Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="RM_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRM_ID" runat="server" Text='<%# Bind("RM_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblVirtual" runat="server" Text='<%# bind("bVIRTUAL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("srno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Short Name">
                                                    <HeaderTemplate>
                                                        Short Name
                                                        <br />
                                                        <asp:TextBox ID="txtSHORT" runat="server" Width="120px" MaxLength="100"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSHORT" runat="server" OnClick="btnSHORT_Click"
                                                            ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />

                                                    </HeaderTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRM_SHORTNAME" runat="server" Text='<%# Bind("RM_SHORTNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Wrap="false" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Full Name">
                                                    <HeaderTemplate>
                                                        Full Name
                                                        <br />
                                                        <asp:TextBox ID="txtFull" runat="server" Width="180px" MaxLength="200"></asp:TextBox>
                                                        <asp:ImageButton ID="btnFull" runat="server" OnClick="btnFull_Click"
                                                            ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" />
                                                    </HeaderTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRM_FULLNAME" runat="server" Text='<%# bind("RM_FULLNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Virtual Room">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgVirtual" runat="server" ImageUrl='<%# bind("FLAG") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" runat="server" OnClick="lbtnEdit_Click">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="false" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:HiddenField ID="hfRM_ID" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

