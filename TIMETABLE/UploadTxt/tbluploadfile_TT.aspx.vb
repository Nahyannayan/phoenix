﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports ICSharpCode.SharpZipLib
Imports System.Diagnostics
Partial Class TIMETABLE_tbluploadfile_TT
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function READTIMETABLE_MASTER(ByVal filepath As String, ByRef flag_id As String, ByVal RECORD_ID As String) As DataTable

        Try
            'Dim firstread As String = String.Empty
            'Dim secondread As String = String.Empty

            Dim errorstring As String = String.Empty
            Dim Temp_day As String = String.Empty
            Dim Temp_month As String = String.Empty
            Dim Temp_Year As String = String.Empty
            Dim Temp_StartTime As String = String.Empty
            Dim Temp_EndTime As String = String.Empty
            Dim ACD_ID As String = String.Empty
            Dim SET_TYPE As String = String.Empty
            Dim Timetable_dt As DataTable = Create_dataTable()
            Timetable_dt.Rows.Clear()

            Dim ARR As String() = New String(2) {}
            Dim arrMonth As String() = New String(11) {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
            Dim TDate_14 As String = String.Empty
            If Not Session("ACD_TYPE_TT") Is Nothing Then
                ARR = Session("ACD_TYPE_TT").ToString.Split("|")
                ACD_ID = ARR(0)
                SET_TYPE = ARR(1)
            End If


            Dim delimiters As Char() = New Char() {","c}
            Using reader As New StreamReader(filepath)
                'Dim sw As Stopwatch = Stopwatch.StartNew()
                While True


                    Dim line As String = reader.ReadLine()
                    If line Is Nothing Then

                        Exit While
                    End If
                    ' Console.WriteLine("{0} field(s)", parts.Length);
                    Dim RowData As String() = line.Split(delimiters)
                    If Not (UBound(RowData) = 18 Or UBound(RowData) = 17) Then
                        errorstring = "-1"
                        Exit While
                    End If
                    If RowData(18).ToString <> "S" Then

                        Dim rDt As DataRow

                        rDt = Timetable_dt.NewRow

                        Temp_day = IIf(RowData(3).ToString.Trim = "", "0", RowData(3).ToString.Trim)
                        Temp_month = IIf(RowData(2).ToString.Trim = "", "0", RowData(2).ToString.Trim)
                        Temp_Year = IIf(RowData(1).ToString.Trim = "", "0", RowData(1).ToString.Trim)
                        Temp_StartTime = RowData(11).ToString.Replace(".", "").Replace(":", "")
                        Temp_EndTime = RowData(15).ToString.Replace(".", "").Replace(":", "")
                        rDt("TTM_TEACHER") = RowData(0).ToString
                        rDt("TTM_YEAR") = Temp_Year
                        rDt("TTM_MONTH") = Temp_month
                        rDt("TTM_DAY") = Temp_day
                        rDt("TTM_PERIOD") = IIf(RowData(4).ToString.Trim = "", "0", RowData(4).ToString.Trim)
                        rDt("TTM_SUBJECT") = RowData(5).ToString
                        rDt("TTM_CLASS") = RowData(6).ToString
                        rDt("TTM_ROOM") = RowData(7).ToString
                        'TZero_8 = IIf(currentRowData(8).ToString.Trim = "", "0", currentRowData(8).ToString.Trim)
                        ' tBlank_9 = currentRowData(9).ToString
                        rDt("TTM_LESSON_NO") = RowData(10).ToString
                        rDt("TTM_STARTTIME") = IIf(Len(Temp_StartTime) = 3, Temp_StartTime.Insert(1, ":"), IIf(Len(Temp_StartTime) = 4, Temp_StartTime.Insert(2, ":"), Temp_StartTime + ":00"))
                        rDt("TTM_TIMETABLE_MIN") = RowData(12).ToString
                        rDt("TTM_SUBJECT_GROUP") = RowData(13).ToString()
                        rDt("TTM_SUBJECT_ALIAS") = RowData(14).ToString()
                        rDt("TTM_ENDTIME") = IIf(Len(Temp_EndTime) = 3, Temp_EndTime.Insert(1, ":"), IIf(Len(Temp_EndTime) = 4, Temp_EndTime.Insert(2, ":"), Temp_EndTime + ":00"))
                        rDt("TTM_NO_OF_DAYS") = RowData(16).ToString
                        rDt("TTM_PERIOD_LABEL") = RowData(17).ToString
                        rDt("TTM_TIMETABLE_DT") = Temp_day + "/" + arrMonth(IIf(CInt(Temp_month) = 0, 0, CInt(Temp_month) - 1)) + "/" + Temp_Year
                        rDt("TTM_ACD_ID") = ACD_ID
                        rDt("TTM_BSU_ID") = Session("sBsuid")
                        rDt("TTM_SET_TYPE") = SET_TYPE
                        rDt("TTM_USR_ID") = Session("sUsr_id")
                        rDt("TTM_RECORD_ID") = RECORD_ID
                        Timetable_dt.Rows.Add(rDt)
                        ' first_loop += 1

                    End If
                End While
                'sw.[Stop]()
                'firstread = "Time elapsed : " & sw.Elapsed.TotalMilliseconds.ToString() & " ms."
            End Using

            If errorstring = "-1" Then
                flag_id = "-1"
            End If

            Return Timetable_dt


        Catch ex As Exception
            flag_id = "-1"
        End Try

    End Function

    Private Function Create_dataTable() As DataTable
        Dim dtDt As DataTable = New DataTable("Timetable_dt")


        Try
            Dim TTM_LESSON_NO As New DataColumn("TTM_LESSON_NO", System.Type.GetType("System.String"))
            Dim TTM_CLASS As New DataColumn("TTM_CLASS", System.Type.GetType("System.String"))
            Dim TTM_TEACHER As New DataColumn("TTM_TEACHER", System.Type.GetType("System.String"))

            Dim TTM_SUBJECT As New DataColumn("TTM_SUBJECT", System.Type.GetType("System.String"))
            Dim TTM_ROOM As New DataColumn("TTM_ROOM", System.Type.GetType("System.String"))
            Dim TTM_NO_OF_DAYS As New DataColumn("TTM_NO_OF_DAYS", System.Type.GetType("System.String"))
            Dim TTM_PERIOD As New DataColumn("TTM_PERIOD", System.Type.GetType("System.String"))

            Dim TTM_STARTTIME As New DataColumn("TTM_STARTTIME", System.Type.GetType("System.String"))
            Dim TTM_ENDTIME As New DataColumn("TTM_ENDTIME", System.Type.GetType("System.String"))
            Dim TTM_ACD_ID As New DataColumn("TTM_ACD_ID", System.Type.GetType("System.String"))
            Dim TTM_BSU_ID As New DataColumn("TTM_BSU_ID", System.Type.GetType("System.String"))
            Dim TTM_SET_TYPE As New DataColumn("TTM_SET_TYPE", System.Type.GetType("System.String"))
            Dim TTM_USR_ID As New DataColumn("TTM_USR_ID", System.Type.GetType("System.String"))

            Dim TTM_TIMETABLE_DT As New DataColumn("TTM_TIMETABLE_DT", System.Type.GetType("System.String"))
            Dim TTM_TIMETABLE_MIN As New DataColumn("TTM_TIMETABLE_MIN", System.Type.GetType("System.String"))

            Dim TTM_SUBJECT_GROUP As New DataColumn("TTM_SUBJECT_GROUP", System.Type.GetType("System.String"))
            Dim TTM_YEAR As New DataColumn("TTM_YEAR", System.Type.GetType("System.String"))

            Dim TTM_MONTH As New DataColumn("TTM_MONTH", System.Type.GetType("System.String"))
            Dim TTM_DAY As New DataColumn("TTM_DAY", System.Type.GetType("System.String"))
            Dim TTM_PERIOD_LABEL As New DataColumn("TTM_PERIOD_LABEL", System.Type.GetType("System.String"))
            Dim TTM_SUBJECT_ALIAS As New DataColumn("TTM_SUBJECT_ALIAS", System.Type.GetType("System.String"))
            Dim TTM_RECORD_ID As New DataColumn("TTM_RECORD_ID", System.Type.GetType("System.String"))

            dtDt.Columns.Add(TTM_LESSON_NO)
            dtDt.Columns.Add(TTM_CLASS)
            dtDt.Columns.Add(TTM_TEACHER)

            dtDt.Columns.Add(TTM_SUBJECT)
            dtDt.Columns.Add(TTM_ROOM)
            dtDt.Columns.Add(TTM_NO_OF_DAYS)
            dtDt.Columns.Add(TTM_PERIOD)

            dtDt.Columns.Add(TTM_STARTTIME)
            dtDt.Columns.Add(TTM_ENDTIME)
            dtDt.Columns.Add(TTM_ACD_ID)
            dtDt.Columns.Add(TTM_BSU_ID)

            dtDt.Columns.Add(TTM_SET_TYPE)
            dtDt.Columns.Add(TTM_USR_ID)

            dtDt.Columns.Add(TTM_TIMETABLE_DT)
            dtDt.Columns.Add(TTM_TIMETABLE_MIN)

            dtDt.Columns.Add(TTM_SUBJECT_GROUP)
            dtDt.Columns.Add(TTM_YEAR)

            dtDt.Columns.Add(TTM_MONTH)
            dtDt.Columns.Add(TTM_DAY)
            dtDt.Columns.Add(TTM_PERIOD_LABEL)
            dtDt.Columns.Add(TTM_SUBJECT_ALIAS)
            dtDt.Columns.Add(TTM_RECORD_ID)


            Return dtDt
        Catch ex As Exception

            Return dtDt
        End Try

    End Function


    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            Session("TIMETABLE_ERR_MSG") = ""
            Dim fs As New FileInfo(fileload.PostedFile.FileName)
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength ' get the file size
            Dim flag_id As String = Now.ToString("yyyyMddhhmmss")
            Dim DirName As String = "fPath_" & flag_id 'create the unique folder fpath_f2131dsa
            Dim strPostedFileName As String = fs.Name 'get the file name
            Session("TT_Folder_Name") = "\" & Session("sBsuid") & "\"

            If intDocFileLength > 4096000 Then '4MB
                Session("TIMETABLE_ERR_MSG") = "zip file size exceeds the limit of 4Mb."
                Exit Sub
            End If

            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".zip") Then ' if zip file processed

                    Dim savePath As String = String.Empty
                    savePath = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString & Session("TT_Folder_Name") & DirName & "\"

                    'write to physical path C:\inetpub\wwwroot\TIMETABLE\131001\fpath_f2131dsa

                    If Not Directory.Exists(savePath) Then
                        Directory.CreateDirectory(savePath)

                    End If

                    fileload.PostedFile.SaveAs(savePath & System.IO.Path.GetFileName(strPostedFileName))
                    Dim extr As New Zip.FastZip
                    extr.ExtractZip(savePath & System.IO.Path.GetFileName(strPostedFileName), savePath, String.Empty)
                    createTimetable_file(savePath)


                    File.Delete(savePath & System.IO.Path.GetFileName(strPostedFileName))
                    Directory.Delete(savePath, True)

                    ' hf_reload.Value = "1"
                    ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> window.parent.alertFuHide();</script>", False)
                    'Session("TIMETABLE_ERR_MSG") = "File uploaded successfully..."

                Else
                    Session("TIMETABLE_ERR_MSG") = "Please upload a valid Document with the extenion in : .zip"

                End If
            Else
                Session("TIMETABLE_ERR_MSG") = "There is no file to Upload."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            Session("TIMETABLE_ERR_MSG") = "Error while Updating the file !!!"
        End Try
    End Sub

    Private Sub createTimetable_file(ByVal savePath As String)
        Dim filepath As String = Session("Folder_Name")
        Dim dt As New DataTable
        Dim flag_id As String = String.Empty
        Dim formatarray As String() = {"*.txt"}
        ' formatarray(0) = "*.jpg"
        Dim RECORD_ID As String = Session("sBsuid") & DateTime.Now.ToString("ddMMMyyyyHHmmss")
        Dim str_output As New StringWriter
        For Each FileFormat As String In formatarray
            Dim filelist As String() = Directory.GetFiles(savePath, FileFormat, SearchOption.AllDirectories)
            For Each File As String In filelist
                Dim filename As New FileInfo(File.ToString())

                dt = READTIMETABLE_MASTER(filename.FullName, flag_id, RECORD_ID)

                If flag_id <> "" Then
                    Session("TIMETABLE_ERR_MSG") = "File uploaded is not in the required format !!!"

                Else
                    Dim str_err As String = String.Empty
                    Dim errorMessage As String = String.Empty
                    str_err = calltransaction(dt, RECORD_ID)
                    If str_err = "0" Then

                       Session("TIMETABLE_ERR_MSG")="File uploaded successfully..."
                    Else
                        Session("TIMETABLE_ERR_MSG") = "Error occured while saving !"
                    End If
                    '----after reading the file detelete the file '
                End If

            Next
        Next

        ' lblErrorLoad.Text = Server.HtmlEncode(str_output.ToString).Replace(Environment.NewLine, "<br/>")

    End Sub

  
    Function calltransaction(ByVal dt As DataTable, ByVal TTM_RECORD_ID As String) As Integer
        Dim ReturnFlag As Integer
        Dim tran As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim Status As Integer
        Dim clientIp As String = Request.UserHostAddress()
        Dim ClientBrw As String = String.Empty
        Dim mcName As String = String.Empty
        Dim MAC_ID As String = IRDataTables.GetMACAddress
        IRDataTables.GetClientInfo(ClientBrw, clientIp, mcName)
        Dim ACD_ID As String = String.Empty
        Dim SET_TYPE As String = String.Empty
        Dim ARR As String() = New String(2) {}
        If Not Session("ACD_TYPE_TT") Is Nothing Then
            ARR = Session("ACD_TYPE_TT").ToString.Split("|")
            ACD_ID = ARR(0)
            SET_TYPE = ARR(1)
        Else
            Session("TIMETABLE_ERR_MSG") = "Session timeout occurred"
            calltransaction = 1
            Exit Function
        End If
        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            tran = conn.BeginTransaction("SampleTransaction")
            Try

                Dim deleteParam(3) As SqlParameter
                deleteParam(0) = New SqlParameter("@TTM_ACD_ID", ACD_ID)
                deleteParam(1) = New SqlParameter("@TTM_SET_TYPE", SET_TYPE)
                deleteParam(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                deleteParam(2).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "TT.DELETETIMETABLE_M", deleteParam)
                Status = deleteParam(2).Value
               

                If Status = 0 Then

                    Using copy As New SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)
                        'copy.ColumnMappings.Add("Source","Destination--database table")
                        copy.ColumnMappings.Add("TTM_LESSON_NO", "TTM_LESSON_NO")
                        copy.ColumnMappings.Add("TTM_CLASS", "TTM_CLASS")
                        copy.ColumnMappings.Add("TTM_TEACHER", "TTM_TEACHER")
                        copy.ColumnMappings.Add("TTM_SUBJECT", "TTM_SUBJECT")
                        copy.ColumnMappings.Add("TTM_ROOM", "TTM_ROOM")
                        copy.ColumnMappings.Add("TTM_NO_OF_DAYS", "TTM_NO_OF_DAYS")
                        copy.ColumnMappings.Add("TTM_PERIOD", "TTM_PERIOD")
                        copy.ColumnMappings.Add("TTM_STARTTIME", "TTM_STARTTIME")
                        copy.ColumnMappings.Add("TTM_ENDTIME", "TTM_ENDTIME")
                        copy.ColumnMappings.Add("TTM_ACD_ID", "TTM_ACD_ID")
                        copy.ColumnMappings.Add("TTM_BSU_ID", "TTM_BSU_ID")
                        copy.ColumnMappings.Add("TTM_SET_TYPE", "TTM_SET_TYPE")
                        copy.ColumnMappings.Add("TTM_USR_ID", "TTM_USR_ID")
                        copy.ColumnMappings.Add("TTM_TIMETABLE_DT", "TTM_TIMETABLE_DT")
                        copy.ColumnMappings.Add("TTM_TIMETABLE_MIN", "TTM_TIMETABLE_MIN")
                        copy.ColumnMappings.Add("TTM_SUBJECT_GROUP", "TTM_SUBJECT_GROUP")
                        copy.ColumnMappings.Add("TTM_YEAR", "TTM_YEAR")
                        copy.ColumnMappings.Add("TTM_MONTH", "TTM_MONTH")
                        copy.ColumnMappings.Add("TTM_DAY", "TTM_DAY")
                        copy.ColumnMappings.Add("TTM_PERIOD_LABEL", "TTM_PERIOD_LABEL")
                        copy.ColumnMappings.Add("TTM_SUBJECT_ALIAS", "TTM_SUBJECT_ALIAS")
                        copy.ColumnMappings.Add("TTM_RECORD_ID", "TTM_RECORD_ID")
                        copy.DestinationTableName = "TIMETABLE_M"
                        copy.WriteToServer(dt)
                    End Using
                Else

                    Session("TIMETABLE_ERR_MSG") = "Error occurred while clearing pervious records"
                    Status = 1
                End If
            Catch ex As Exception
                Status = 1
            Finally
                If Status <> 0 Then
                    tran.Rollback()
                    Session("TIMETABLE_ERR_MSG") = "Error occured while saving !"
                Else
                    Status = 0
                    tran.Commit()
                End If
            End Try
        End Using


        If Status = 0 Then


            Dim PARAM(12) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@TTM_ACD_ID", ACD_ID)
            PARAM(1) = New SqlClient.SqlParameter("@TTM_BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlClient.SqlParameter("@TTM_SET_TYPE", SET_TYPE)
            PARAM(3) = New SqlClient.SqlParameter("@TTM_USR_ID", Session("sUsr_id"))
            PARAM(4) = New SqlClient.SqlParameter("@TTM_OTH_INFO", "CLIENT IP(" & clientIp & ")MAC_ID (" & MAC_ID & ")ClientBrw" & ClientBrw)
            PARAM(5) = New SqlClient.SqlParameter("@TTM_RECORD_ID", TTM_RECORD_ID)

            PARAM(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            PARAM(6).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "TT.SAVETIMETABLE_M_NEW", PARAM)
            Status = PARAM(6).Value
            If Status <> 0 Then
                Session("TIMETABLE_ERR_MSG") = "Error occured while processing records !"
                calltransaction = 1
            Else
                calltransaction = 0
            End If
        Else
            Session("TIMETABLE_ERR_MSG") = "Error occured while saving !"
            calltransaction = 1
        End If
            


    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
    End Sub

    
End Class
