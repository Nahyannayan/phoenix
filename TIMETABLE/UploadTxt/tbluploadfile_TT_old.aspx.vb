﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class TIMETABLE_tbluploadfile_TT
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function READTIMETABLE_MASTER(ByVal filepath As String) As String

        Try

            Dim errorstring As String = String.Empty

            Dim strbulider As New StringBuilder
            Dim strFileName As String = filepath
            Dim currentRowData() As String
            Dim TTeacher_0 As String = String.Empty
            Dim TYear_1 As String = String.Empty
            Dim TMonth_2 As String = String.Empty
            Dim TDay_3 As String = String.Empty
            Dim TPeriod_4 As String = String.Empty
            Dim TSubject_5 As String = String.Empty
            Dim TClasses_6 As String = String.Empty
            Dim TRoom_7 As String = String.Empty
            Dim TZero_8 As String = String.Empty
            Dim tBlank_9 As String = String.Empty
            Dim TLesson_10 As String = String.Empty
            Dim TStartTime_11 As String = String.Empty
            Dim TMin_12 As String = String.Empty
            Dim TSubjectGRP_13 As String = String.Empty
            Dim TSubjectAlias_14 As String = String.Empty
            Dim TEndTime_15 As String = String.Empty
            Dim TWeekDay_16 As String = String.Empty
            Dim TPeriodLabel_17 As String = String.Empty

            Dim arr As String() = New String(11) {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
            Dim TDate_14 As String = String.Empty

            Using txtReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(strFileName)
                txtReader.TextFieldType = FileIO.FieldType.Delimited
                txtReader.SetDelimiters(",")
                txtReader.HasFieldsEnclosedInQuotes = True
                While Not txtReader.EndOfData
                    ' Read one "record's worth" of fields into the 
                    ' "currentRowData" array ...
                    currentRowData = txtReader.ReadFields
                    If Not (UBound(currentRowData) = 18 Or UBound(currentRowData) = 17) Then
                        errorstring = "-1"
                        Exit While
                        'ElseIf OT IsNumeric(currentRowData(0).ToString) = False Then
                        '    errorstring = "-1"
                        '    Exit While
                    End If
                    If currentRowData(18).ToString <> "S" Then



                        TTeacher_0 = currentRowData(0).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                        TYear_1 = IIf(currentRowData(1).ToString.Trim = "", "0", currentRowData(1).ToString.Trim)
                        TMonth_2 = IIf(currentRowData(2).ToString.Trim = "", "0", currentRowData(2).ToString.Trim)
                        TDay_3 = IIf(currentRowData(3).ToString.Trim = "", "0", currentRowData(3).ToString.Trim)
                        TPeriod_4 = IIf(currentRowData(4).ToString.Trim = "", "0", currentRowData(4).ToString.Trim)
                        TSubject_5 = currentRowData(5).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                        TClasses_6 = currentRowData(6).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                        TRoom_7 = currentRowData(7).ToString
                        TZero_8 = IIf(currentRowData(8).ToString.Trim = "", "0", currentRowData(8).ToString.Trim)
                        tBlank_9 = currentRowData(9).ToString
                        TLesson_10 = currentRowData(10).ToString
                        TStartTime_11 = currentRowData(11).ToString
                        TMin_12 = currentRowData(12).ToString
                        TSubjectGRP_13 = currentRowData(13).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                        TDate_14 = TDay_3 + "/" + arr(IIf(CInt(TMonth_2) = 0, 0, CInt(TMonth_2) - 1)) + "/" + TYear_1

                        TSubjectAlias_14 = currentRowData(14).ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                        TEndTime_15 = currentRowData(15).ToString
                        TWeekDay_16 = currentRowData(16).ToString
                        TPeriodLabel_17 = currentRowData(17).ToString


                        strbulider.Append(String.Format("<TTM TTEACHER='{0}' TYEAR='{1}' TMONTH='{2}' TDAY='{3}' TPERIOD ='{4}' TSUBJECT ='{5}' TCLASS='{6}'" & _
                                                        " TROOM='{7}' TZERO ='{8}' TBLANK='{9}' TLESSON='{10}'  " & _
                                                         "   TSTARTTIME='{11}' TMIN='{12}' TSUBJECTGRP='{13}' TDate='{14}' " & _
                                                            " TSUBJECTALIAS='{15}' TENDTIME='{16}' TNO_OF_DAYS='{17}' TPERIOD_LABEL='{18}' />", TTeacher_0, _
      TYear_1, TMonth_2, TDay_3, TPeriod_4, TSubject_5, TClasses_6, TRoom_7, TZero_8, tBlank_9, TLesson_10, TStartTime_11, _
      TMin_12, TSubjectGRP_13, TDate_14, TSubjectAlias_14, TEndTime_15, TWeekDay_16, TPeriodLabel_17))
                    End If
                End While
            End Using

            If errorstring = "-1" Then
                READTIMETABLE_MASTER = "-1"
            Else
                READTIMETABLE_MASTER = strbulider.ToString
            End If


        Catch ex As Exception
            READTIMETABLE_MASTER = ""
        End Try

    End Function
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click


        Try
            Dim fs As New FileInfo(fileload.PostedFile.FileName)
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength ' get the file size
            Dim newFileName As String = "fPath_" & Now.ToString("yyyyMddhhmmss") 'create the unique folder fpath_f2131dsa
            Dim strPostedFileName As String = fs.Name 'get the file name
            Session("TT_Folder_Name") = "\" & Session("sBsuid") & "\TEMP_TXT\"

            If intDocFileLength > 2096000 Then '2MB
                lblErrorLoad.Text = "text file size exceeds the limit of 2Mb."
                Exit Sub
            End If

            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".txt") Then ' if zip file processed

                    Dim savePath As String = String.Empty
                    savePath = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString & Session("TT_Folder_Name")

                    'write to physical path C:\inetpub\wwwroot\TIMETABLE\131001\TEMP_TXT\fpath_f2131dsa.txt

                    If Not Directory.Exists(savePath) Then
                        Directory.CreateDirectory(savePath)

                    End If


                    fileload.PostedFile.SaveAs(savePath & newFileName & ".txt")
                    Dim str_Xml As String = String.Empty
                    str_Xml = READTIMETABLE_MASTER(savePath & newFileName & ".txt")
                    If str_Xml = "-1" Then
                        lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>File uploaded is not in the required format !!!</div>"
                        File.Delete(savePath & newFileName & ".txt")
                    Else
                        File.Delete(savePath & newFileName & ".txt")
                        Dim str_err As String = String.Empty
                        Dim errorMessage As String = String.Empty
                        str_err = calltransaction(str_Xml)
                        If str_err = "0" Then
                            ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> window.parent.alertFuHide();</script>", False)
                            'ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
                            lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#eaebec;'>File uploaded successfully...</div>"
                        Else
                            lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>Error occured while saving !!!</div>"
                        End If
                        '----after reading the file detelete the file '
                    End If


                Else
                    lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>Please upload a valid Document with the extenion in : .txt</div>"

                End If
            Else
                lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>There is no file to Upload.</div>"""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            lblErrorLoad.Text = "Error while Updating the file !!!"
        End Try
    End Sub

    'Try
    '    Dim fs As New FileInfo(fileload.PostedFile.FileName)
    '    Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength ' get the file size
    '    Dim DirName As String = "fPath_" & Now.ToString("yyyyMddhhmmss") 'create the unique folder fpath_f2131dsa
    '    Dim strPostedFileName As String = fs.Name 'get the file name
    '    Session("TT_Folder_Name") = "\" & Session("sBsuid") & "\TEMP_TXT\"

    '    If intDocFileLength > 4096000 Then '4MB
    '        lblErrorLoad.Text = "zip file size exceeds the limit of 4Mb."
    '        Exit Sub
    '    End If

    '    If (strPostedFileName <> String.Empty) Then
    '        Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

    '        If (strExtn = ".zip") Then ' if zip file processed

    '            Dim savePath As String = String.Empty
    '            savePath = WebConfigurationManager.ConnectionStrings("TIME_TABLE").ConnectionString & Session("TT_Folder_Name") & DirName

    '            'write to physical path C:\inetpub\wwwroot\TIMETABLE\131001\fpath_f2131dsa.txt

    '            If Not Directory.Exists(savePath) Then
    '                Directory.CreateDirectory(savePath)

    '            End If




    '            fileload.PostedFile.SaveAs(savePath & System.IO.Path.GetFileName(strPostedFileName))
    '            Dim extr As New Zip.FastZip
    '            extr.ExtractZip(savePath & System.IO.Path.GetFileName(strPostedFileName), savePath, String.Empty)
    '            createTimetable_file(savePath)


    '            File.Delete(savePath & System.IO.Path.GetFileName(strPostedFileName))
    '            Directory.Delete(savePath, True)

    '            ' hf_reload.Value = "1"
    '            ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
    '            lblErrorLoad.Text = "File uploaded successfully..."


    '        Else
    '            lblErrorLoad.Text = "Please upload a valid Document with the extenion in : .zip"

    '        End If
    '    Else
    '        lblErrorLoad.Text = "There is no file to Upload."
    '    End If
    'Catch ex As Exception
    '    UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


    '    lblErrorLoad.Text = "Error while Updating the file !!!"
    'End Try
    Private Sub createTimetable_file(ByVal savePath As String)
        Dim filepath As String = Session("Folder_Name")
        ' Dim strFilter As String = "*.jpg"
        ' Dim formatarray As String() = {"*.jpg"} 'strFilter.Split(";")
        Dim formatarray As String() = {"*.txt"}
        ' formatarray(0) = "*.jpg"
        Dim str_output As New StringWriter
        For Each FileFormat As String In formatarray
            Dim filelist As String() = Directory.GetFiles(savePath, FileFormat, SearchOption.AllDirectories)
            For Each File As String In filelist
                Dim filename As New FileInfo(File.ToString())

                Dim WriteStatus As String = String.Empty


                Dim str_Xml As String = String.Empty
                str_Xml = READTIMETABLE_MASTER(filename.FullName)
                If str_Xml = "-1" Then
                    lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>File uploaded is not in the required format !!!</div>"

                Else
                    Dim str_err As String = String.Empty
                    Dim errorMessage As String = String.Empty
                    str_err = calltransaction(str_Xml)
                    If str_err = "0" Then
                        ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
                        lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#eaebec;'>File uploaded successfully...</div>"
                    Else
                        lblErrorLoad.Text = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#eaebec;'>Error occured while saving !!!</div>"
                    End If
                    '----after reading the file detelete the file '
                End If
                

              

            Next
        Next

        lblErrorLoad.Text = Server.HtmlEncode(str_output.ToString).Replace(Environment.NewLine, "<br/>")

    End Sub
    Function calltransaction(ByRef str_Xml As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty


        Dim clientIp As String = Request.UserHostAddress()
        Dim ClientBrw As String = String.Empty
        Dim mcName As String = String.Empty
        Dim MAC_ID As String = IRDataTables.GetMACAddress
        IRDataTables.GetClientInfo(ClientBrw, clientIp, mcName)
        Dim ACD_ID As String = String.Empty
        Dim SET_TYPE As String = String.Empty
        Dim ARR As String() = New String(2) {}

        If Not Session("ACD_TYPE_TT") Is Nothing Then
            ARR = Session("ACD_TYPE_TT").ToString.Split("|")
            ACD_ID = ARR(0)
            SET_TYPE = ARR(1)
        Else
            calltransaction = 1
            Exit Function
        End If


        Using conn As SqlConnection = ConnectionManger.GetOASIS_TIMETABLEConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_Xml = "<TTM_M>" + str_Xml + "</TTM_M>"

                Dim PARAM(12) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@TTM_ACD_ID", ACD_ID)
                PARAM(1) = New SqlClient.SqlParameter("@TTM_BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@TTM_SET_TYPE", SET_TYPE)
                PARAM(3) = New SqlClient.SqlParameter("@TTM_USR_ID", Session("sUsr_id"))
                PARAM(4) = New SqlClient.SqlParameter("@STR_XML", str_Xml.ToString)
                PARAM(5) = New SqlClient.SqlParameter("@TTM_OTH_INFO", "CLIENT IP(" & clientIp & ")MAC_ID (" & MAC_ID & ")ClientBrw" & ClientBrw)
                PARAM(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(6).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TT.SAVETIMETABLE_M", PARAM)
                ReturnFlag = PARAM(6).Value
                If ReturnFlag <> 0 Then
                    calltransaction = 1
                Else
                    calltransaction = 0
                End If
            Catch ex As Exception
                calltransaction = 1
            Finally
                If calltransaction <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> parent.location.href = parent.location.href;</script>", False)
    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Try


    '        Dim extr As New Zip.FastZip
    '        Dim recurse As Boolean = True
    '        Dim filter As String = "\.txt$"
    '        Dim ZipFilename As String = "C:\inetpub\wwwroot\TIMETABLE\125010\TEMP_TXT\myzipFile.zip"
    '        extr.CreateZip(ZipFilename, "C:\inetpub\wwwroot\TIMETABLE\125010\TEMP_TXT\", recurse, filter)

    '        Dim filename As String = "myzipFile.zip"
    '        Response.Clear()
    '        Response.AppendHeader("content-disposition", "attachment; filename=" + filename)
    '        'Response.AppendHeader("content-disposition", ZipFilename)
    '        Response.ContentType = "application/octet-stream"
    '        Response.TransmitFile(ZipFilename)
    '        HttpContext.Current.ApplicationInstance.CompleteRequest()

    '    Catch ex As Exception

    '    End Try
    'End Sub
End Class
