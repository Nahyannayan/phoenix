<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tblMast_PeriodEdit.aspx.vb" Inherits="Students_studAtt_room_Grade_edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function SelectAll(CheckBox) {
            TotalChkBx = parseInt('<%=gvPeriod.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvPeriod.ClientID %>');
            var TargetChildControl = "chkSelect";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeader(CheckBox) {
            TotalChkBx = parseInt('<%=gvPeriod.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvPeriod.ClientID %>');
            var TargetChildControl = "chkSelect";
            var TargetHeaderControl = "chkSelectAll";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Period Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">                            
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                       ></asp:Label>
                                </div>                           
                        </td>
                    </tr>

                    <tr>
                        <td align="left"  valign="bottom">
                            <table align="center" width="100%">
                                <tr class="title-bg" >
                                    <td colspan="4" >                                       
                                            <asp:Literal ID="ltLabel" runat="server" Text="Grade Setting"></asp:Literal>
                                    </td>
                                </tr>

                                <tr >
                                    <td align="left" width="15%"><span class="field-label"> Grade</span></td>
                                   
                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlGrd_id" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                
                                    <td align="left" width="20%"><span class="field-label">Copy to Grades </span></td>
                                   
                                    <td align="left" width="40%">
                                        <asp:CheckBoxList ID="chkOthGrade" runat="server" CellPadding="6" CellSpacing="1" RepeatColumns="7" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList></td>
                                </tr>
                                <tr >
                                    <td align="left"><span class="field-label">Week </span></td>
                                   
                                    <td align="left">
                                        <asp:DropDownList ID="ddlWk_Name" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                
                                    <td align="left"><span class="field-label">Copy to Weeks </span></td>
                                    
                                    <td align="left">
                                        <asp:CheckBoxList ID="chkWeek" runat="server" CellPadding="6" CellSpacing="1" RepeatColumns="4" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left"  colspan="4" >Period List <span style="font:small">(<i>Select the period that needs to be saved !!!</i>)</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4" >
                                        <asp:GridView ID="gvPeriod" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No record added yet" 
                                             Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Period_Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPM_ID" runat="server" Text='<%# Bind("PM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll(this);" ToolTip="Click here to select/deselect all rows" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Bind("flag") %>' />

                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Periods">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPM_DESCR" runat="server" Text='<%# Bind("PM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Wrap="false" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Period Label">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPer_Lbl" runat="server" MaxLength="20" Text='<%# Bind("PGW_LABEL") %>'></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbPer_lbl" runat="server" TargetControlID="txtPer_Lbl"
                                                            FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars=" ">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Period From" >
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtFrom" runat="server" Text='<%# Bind("PGW_START_TIME")%>'></asp:TextBox>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Period To">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTo" runat="server" Text='<%# Bind("PGW_END_TIME")%>'></asp:TextBox>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle CssClass="gridheader_new"  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"  Text="Save" ValidationGroup="groupM1"  />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False"  CssClass="button" OnClick="btnCancel_Click" Text="Cancel"  />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

