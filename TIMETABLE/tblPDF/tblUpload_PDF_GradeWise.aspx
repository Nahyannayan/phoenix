﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tblUpload_PDF_GradeWise.aspx.vb" Inherits="Curriculum_CLMStud_uploadEdit_frame" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
   
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/ControlStyle.css" rel="stylesheet" type="text/css" />
   
<script language="javascript" type="text/javascript">
 //Trim the input text
  function Trim(input)
  {
    var lre = /^\s*/; 
    var rre = /\s*$/; 
    input = input.replace(lre, ""); 
    input = input.replace(rre, ""); 
    return input; 
   }
 
   // filter the files before Uploading for text file only  
   function CheckUploadedFile() 
   {
        var file = document.getElementById('<%=fileload.ClientID%>');
        var fileName=file.value;    
        //var objFSO = new ActiveXObject("Scripting.FileSystemObject");
//            var files;
//            var size;
//             var path = file.value;    
        //Checking for file browsed or not 
        if (Trim(fileName) =='' )
        {
            alert("Please select a file to upload!!!");
            file.focus();
            return false;
        }
 
       //Setting the extension array for diff. type of text files 
         
       var extType= new Array(".pdf");

       //getting the file name
       while (fileName.indexOf("\\") != -1)
         fileName = fileName.slice(fileName.indexOf("\\") + 1);
 
 
       //Getting the file extension                     
       var ext = fileName.slice(fileName.lastIndexOf(".")).toLowerCase();
 
       //matching extension with our given extensions.
    for (var i = 0; i < extType.length; i++) 
        {

         if (extType[i] == ext) 
         { 
           return true;
         }
         
           }  

//        //alert(path);

//        files = objFSO.getFile(path);
//      

//        size = files.size ; // This size will be in Bytes

//  // We are converting it to KB as below

//        alert('File Size is : ' + files.size /1024 +' KB'); 

         alert("Please only upload files that end in types:  "+(extType.join("  ").toUpperCase())  
           +  "\nPlease select a new "
           + "file to upload and submit again.");
           file.focus();
           return false;                
   }  
 
    </script>
</head>
<body >
    <form id="form1" runat="server">
    <table style="margin-top:-1px;">
    <tr>
    <td align="center">
     <table align="center" cellpadding="2" cellspacing="0" border="0" style="margin-top:-1px;width:700px; border-collapse:collapse;">
              <tr>
        <td align="left" >
        <br />
         <asp:Label ID="lblErrorLoad" runat="server" CssClass="error" 
            EnableViewState="False"></asp:Label> 
        </td>
        </tr>
    </table>
    
    </td>
    
      
    </tr>
        <tr id="trUploadImg" runat="server" align="center"  style="width:700px;">
    <td  align="left"  style="width:700px;">
   <%--<div  style="overflow: visible; border-color: #1b80b6; border-style:solid;border-width:1px;width:380px;">
         <div class="msg_header" style="height:23px;width: 380px;margin-top:1px;vertical-align:middle;background-color:White;"> <div class="msg"  style="margin-top:0px;vertical-align:middle; text-align:center;">
           Upload Image</div></div>--%>
    <asp:FileUpload ID="fileload" runat="server" CssClass="filebutton" Width="350px" 
            Height="20px" /> &nbsp;&nbsp;
      <asp:Button ID="btnProcess" runat="server"
                    Text="Save"   CssClass="button"  /><br />
                    <div style="padding-top:6px;">
                        <asp:Literal ID="ltNote" runat="server"></asp:Literal></div>
                 
   <%-- </div>--%>

    </td>
    
      
    </tr>
    
    
    </table>
    
   
    
    
   
    
    
    </form>
</body>
</html>
