﻿Imports System
Imports System.Web
Imports System.Web.UI
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Curriculum_CLMStud_uploadEdit_frame
    Inherits System.Web.UI.Page

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Not Page.IsPostBack Then
            Dim display_data() As String = New String(7) {}
            Dim data() As String = New String(4) {}

            Dim splitter As Char = "|"
            Dim str_display_data As String = String.Empty

            ViewState("ImgPath") = "~/Images/Photos/no_image1.gif"
            ViewState("STU_ID") = "0"
            ViewState("RPF_ID") = "0"

            If Not Session("Upload_display_data_TT") Is Nothing Then
                str_display_data = Session("Upload_display_data_TT")
            End If
            If Not Session("Upload_data_TT") Is Nothing Then
                data = Session("Upload_data_TT").Split(splitter)
                ViewState("BSU_ID") = data(0)
                ViewState("ACD_ID") = data(1)
                ViewState("GRD_ID") = data(2)
            End If
        End If

    End Sub
    
   
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click

        Try
            Dim intDocFileLength As Integer = fileload.PostedFile.ContentLength
            Dim strExtnchk As String = System.IO.Path.GetExtension(fileload.PostedFile.FileName).ToLower

            If fileload.HasFile = False Then
                lblErrorLoad.Text = "Select  pdf file to upload !!!"
                Exit Sub
            ElseIf strExtnchk <> ".pdf" Then '4MB
                lblErrorLoad.Text = "Please upload PDF file only !!!"
                Exit Sub
            ElseIf intDocFileLength > 50500 Then '4MB
                lblErrorLoad.Text = "PDF file size exceeds the limit of 50kb !!!"
                Exit Sub
            End If
            'Dim fs As New FileInfo(fileload.PostedFile.FileName)

            ' get the file size
            Dim strPostedFileName As String = fileload.PostedFile.FileName       'get the file name
            Dim str_phyPath As String = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString
            Dim UploadPath As String = str_phyPath & Session("Folder_Name")
            Dim status As String = String.Empty
            Dim startfilepath As String = "\" & ViewState("BSU_ID") & "\" & ViewState("ACD_ID")


            If (strPostedFileName <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower 'gets the file extn

                If (strExtn = ".pdf") Then ' if zip file processed Or (strExtn = ".png") Or (strExtn = ".bmp")


                    'write to physical path C:\inetpub\wwwroot\CURR\fpath_f2131dsa

                    If Not Directory.Exists(UploadPath) Then
                        Directory.CreateDirectory(UploadPath)
                        fileload.PostedFile.SaveAs(UploadPath & ViewState("GRD_ID") & System.IO.Path.GetExtension(strPostedFileName))
                        status = Update_database(startfilepath & ViewState("GRD_ID") & System.IO.Path.GetExtension(strPostedFileName))

                        If status <> "0" Then
                            lblErrorLoad.Text = "Error while saving " & fileload.PostedFile.FileName & " to the destination folder"
                        End If
                    Else
                        fileload.PostedFile.SaveAs(UploadPath & ViewState("GRD_ID") & System.IO.Path.GetExtension(strPostedFileName))
                        status = Update_database(startfilepath & ViewState("GRD_ID") & System.IO.Path.GetExtension(strPostedFileName))

                        If status <> "0" Then
                            lblErrorLoad.Text = "Error while saving " & fileload.PostedFile.FileName & " to the destination folder"
                            Exit Sub
                        End If


                    End If
                   
                    lblErrorLoad.Text = "<font color='green'>File uploaded successfully...</font>"
                Else
                    lblErrorLoad.Text = "Upload pdf file type extension .pdf only !!!"

                End If
            Else
                lblErrorLoad.Text = "There is no file to upload !!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")


            lblErrorLoad.Text = "Error while Updating the file !!!"
        End Try
    End Sub
    Private Function Update_database(ByVal file As String) As String
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(9) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("BSU_ID"))
            pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ViewState("ACD_ID"))
            pParms(3) = New SqlClient.SqlParameter("@GRD_ID", ViewState("GRD_ID"))
            pParms(4) = New SqlClient.SqlParameter("@FILEPATH", file)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "TT.SAVETIMETABLE_PDFUPLOAD", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag




        Catch ex As Exception
            Return -1
        End Try

    End Function

End Class