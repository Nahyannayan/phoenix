Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration
Imports ICSharpCode.SharpZipLib

Partial Class Curriculum_clmStudActivityUpload
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        ' ScriptManager.GetCurrent(Page).RegisterPostBackControl(mdlPopup)
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"

                'check for the usr_name and the menucode are valid otherwise redirect to login page


                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100225") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    GETPRE_DEFINEDSIZE()
                    btnCloseedit.Attributes.Add("onClick", "pagereload();")
                    ltNote.Text = "<div style='width:700px;font-family: Verdana, tahoma, sans-serif;color:#800000;padding:2px;'><b>Note:</b><i>Upload zip folder type extension <b>zip</b> with size less than 4MB.The zip folder must contain only PDF file type extension <b>PDF</b> with each file size less than 50KB and the PDF file name should be in the format given PK,KG1,KG2,G1,G2,...G12,G13 </div>"
                    Call bindAcademic_Year()

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
        gridbind()
    End Sub
    Sub GETPRE_DEFINEDSIZE()
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim STR As String = " select FUM_IMG_WIDTH,FUM_IMG_HEIGHT from CURR.FILEUPLOAD_M where FUM_APPLIED_FOR='CURR' AND FUM_BSU_ID='" & Session("sBsuid") & "'"
        Using IMG_DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, STR)
            If IMG_DATAREADER.HasRows Then
                While IMG_DATAREADER.Read
                    ViewState("IMG_WIDTH") = Convert.ToString(IMG_DATAREADER("FUM_IMG_WIDTH"))
                    ViewState("IMG_HEIGHT") = Convert.ToString(IMG_DATAREADER("FUM_IMG_HEIGHT"))
                End While
            Else
                ViewState("IMG_WIDTH") = "380"
                ViewState("IMG_HEIGHT") = "510"
            End If
        End Using
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "' AND ACD_CLM_ID = '" & Session("CLM") & "'"
            ddlAcdID.Items.Clear()
            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlAcdID.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACD_ID")))
            End While
            reader.Close()
            ddlAcdID.ClearSelection()
            ddlAcdID.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdID_SelectedIndexChanged(ddlAcdID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdID.SelectedIndexChanged
        gridbind()

    End Sub

    

    Sub gridbind()
        Dim strCondition As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strSql As String = String.Empty
        Dim str_PDFphysical As String = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString
        Dim str_PDFvirtual As String = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepathvirtual").ConnectionString
        Dim FULLPATH As String = String.Empty

        Dim dsPDF As DataSet


        Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlAcdID.SelectedValue.ToString & "\"



        strSql = " SELECT ROW_NUMBER() OVER(ORDER BY GRD_DISPLAYORDER) AS SRNO," & _
       " GRD_DISPLAYORDER, TT_ID, GRM_GRD_ID, GRM_DISPLAY,ACTUAL_FILE_PATH, FILE_PATH ,FILE_ALT " & _
" FROM( SELECT DISTINCT GRD_DISPLAYORDER,TT_ID,GRM_GRD_ID,GRM_DISPLAY," & _
" CASE WHEN ISNULL(TT_FILE_PATH,'') <>'' THEN '~/Images/Photos/timetablepdf.png' " & _
 " ELSE  '~/Images/Photos/no_file.png' END FILE_PATH,CASE WHEN ISNULL(TT_FILE_PATH,'') <>'' THEN 'Uploaded PDF Timetable'" & _
 " ELSE  'No File Available' END FILE_ALT, CASE WHEN ISNULL(TT_FILE_PATH,'') <>''  THEN TT_FILE_PATH ELSE ''  END ACTUAL_FILE_PATH " & _
" FROM TT.TIMETABLE_PDF  WITH(NOLOCK) RIGHT OUTER JOIN DBO.GRADE_BSU_M WITH(NOLOCK) ON TT_GRD_ID=GRM_GRD_ID " & _
" AND TT_ACD_ID=GRM_ACD_ID  INNER JOIN DBO.GRADE_M WITH(NOLOCK) ON " & _
" GRD_ID=GRM_GRD_ID  WHERE GRM_ACD_ID= '" & ddlAcdID.SelectedValue & "')A ORDER BY GRD_DISPLAYORDER "

        dsPDF = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If dsPDF.Tables(0).Rows.Count > 0 Then
            gvInfo.DataSource = dsPDF.Tables(0)
            gvInfo.DataBind()
        Else
            dsPDF.Tables(0).Rows.Add(dsPDF.Tables(0).NewRow())
            'start the count from 1 no matter gridcolumn is visible or not
            dsPDF.Tables(0).Rows(0)(4) = True
            gvInfo.DataSource = dsPDF.Tables(0)
            Try
                gvInfo.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvInfo.Rows(0).Cells.Count
            gvInfo.Rows(0).Cells.Clear()
            gvInfo.Rows(0).Cells.Add(New TableCell)
            gvInfo.Rows(0).Cells(0).ColumnSpan = columnCount
            gvInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvInfo.Rows(0).Cells(0).Text = "Not record available."
        End If
        deleteOld_dir()
    End Sub
    Sub deleteOld_dir()
        Try


            Dim str_phyPath As String = WebConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString
            Dim UploadPath As String = str_phyPath & Session("Folder_Name")
            'write to physical path C:\inetpub\wwwroot\CURR\fpath_f2131dsa
            ' Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlGrade.SelectedValue & "_" & Right(ddlAcdID.SelectedItem.Text, 2) & "_" & ddlRptSchedule.SelectedValue & "\"
            If Directory.Exists(UploadPath) Then
                Dim Dirlist As String() = Directory.GetDirectories(UploadPath)
                For Each vdir As String In Dirlist
                    Dim dt As DateTime = DateAdd(DateInterval.Hour, 1, File.GetCreationTime(vdir))
                    If dt < Now() Then
                        Directory.Delete(vdir, True)
                    End If
                Next



            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub imgThumb_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim lblFilePath As New Label
        Dim lblGrade_ID As New Label
        Dim lblGRADE As New Label
        Dim str_PDFphysical As String = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepath").ConnectionString
        Dim str_PDFvirtual As String = WebConfigurationManager.ConnectionStrings("TIMETABLEFilepathvirtual").ConnectionString
        Dim FULLPATH As String = String.Empty
        Session("Folder_Name") = "\" & Session("sBsuid") & "\" & ddlAcdID.SelectedValue.ToString & "\"
        lblFilePath = TryCast(sender.FindControl("lblFilePath"), Label)
        If lblFilePath.Text.Trim <> "" Then
            FULLPATH = str_PDFvirtual + lblFilePath.Text.Trim
            Session("FullPath") = FULLPATH

            ltFrame.Text = "<iframe src='Timetablepdf.aspx'  frameborder='0 style='margin:1px;'  scrolling='no' width='100%' height='400px' id='ifAlert' runat='server' />"


        Else

            ltFrame.Text = "<div style='width:100%;height:100%;padding-top:200px; text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 18px; font-weight: bold; color: #1B80B6;	vertical-align:middle;'>No file available !!!</div>"

        End If


        Dim imgthumb As New ImageButton
        imgthumb = sender

        lblGRADE = TryCast(sender.FindControl("lblGRADE"), Label)
        ltHeader.Text = lblGRADE.Text & " Uploaded PDF Timetable"

        mdlPopup.Show()
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

   

    Protected Sub btnClose_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mdlPopup.Hide()
        'gridbind()
    End Sub
    Protected Sub btnCloseedit_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        mdlPopUpEdit.Hide()
        ' gridbind()
        ' ScriptManager.RegisterStartupScript(Me, GetType(String), "script", "<script type=text/javascript> window.location.href=window.location.href</script>", False)
    End Sub
    'Sub ListZipContent(ByVal sFile As String)
    '    Dim zip As New ZipFile(File.OpenRead(sFile))
    '    Dim str1 As String = String.Empty
    '    For Each entry As ZipEntry In zip
    '        str1 = str1 + entry.Name + ","
    '    Next
    '    lblError.Text = str1
    'End Sub


    'Sub UncompressZip(ByVal sFile As String)
    '    Dim zipin As New ZipInputStream(File.OpenRead(sFile))

    '    Dim entry As ZipEntry
    '    For Each entry In zipin.GetNextEntry()


    '    Next
    '    While (entry = zipin.GetNextEntry())
    '        Dim streamWriter As FileStream = File.Create("C:\Temp\" & entry.Name)
    '        Dim size As Long = entry.Size
    '        Dim data As Byte() = New Byte(size - 1) {}
    '        While (True)
    '            size = zipin.Read(data, 0, data.Length)
    '            If size > 0 Then
    '                streamWriter.Write(data, 0, CInt(size))
    '            Else
    '                Exit While
    '            End If
    '        End While
    '        streamWriter.Close()
    '    End While

    'End Sub

    ''    private static void UncompressZip(string sFile)
    ''{
    ''    ZipInputStream zipIn = new ZipInputStream(File.OpenRead(sFile));
    ''    ZipEntry entry;
    ''    while ((entry = zipIn.GetNextEntry()) != null)
    ''    {
    '        FileStream streamWriter = File.Create(@"C:\Temp\" + entry.Name);
    '        long size = entry.Size;
    '        byte[] data = new byte[size];
    '        while (true)
    '        {
    '            size = zipIn.Read(data, 0, data.Length);
    '            if (size > 0) streamWriter.Write(data, 0, (int) size);
    '            else break;
    '        }
    '        streamWriter.Close();
    ''    }
    ''    Console.WriteLine("Done!!");
    '}  

    '    ZipFile zip = new ZipFile(ZipPath); 
    '//List compressed files 
    'foreach(ZipEntry compfile in zip) { 
    'if (compfile.IsFile) { 
    '//call compfile.Name to ge the file name Notice it includes relative path 
    '} 

    'Protected Sub gvInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInfo.SelectedIndexChanged
    '    Dim lblSname As New Label

    '    Dim gvInfo As GridView = CType(sender, GridView)
    '    Dim row As GridViewRow = gvInfo.SelectedRow


    '    ltHeader.Text = row.Cells(2).Text
    'End Sub

  
    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ACD_ID As String = ddlAcdID.SelectedValue
        Dim lblGrade_ID As New Label
        Dim lblGRADE As New Label


        lblGrade_ID = TryCast(sender.FindControl("lblGrade_ID"), Label)
        lblGRADE = TryCast(sender.FindControl("lblGRADE"), Label)

        Session("Upload_display_data_TT") = lblGRADE.Text
        Session("Upload_data_TT") = Session("sBsuid") & "|" & ACD_ID & "|" & lblGrade_ID.Text
        ltName.Text = "Upload PDF Timetable for " & lblGRADE.Text
        mdlPopUpEdit.Show()
    End Sub
End Class
