<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tblUpload_PDF_view.aspx.vb" Inherits="Curriculum_clmStudActivityUpload" Title="::::GEMS OASIS:::: Online Student Administration System::::" EnableEventValidation="false" ValidateRequest="false" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">

function pagereload()
{
window.location.reload();;
}
</script>

    <asp:Button id="btnShowPopup" runat="server" style="display:none" />
            <ajaxToolkit:ModalPopupExtender  Y="20" 
             ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="plStudAtt" 
             CancelControlID="btnClose" OnCancelScript="cancelClick();"    BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />
   
   <asp:Button id="btnShowEdit" runat="server" style="display:none" />
            <ajaxToolkit:ModalPopupExtender 
             ID="mdlPopUpEdit" runat="server" TargetControlID="btnShowEdit" PopupControlID="divStudEdit" 
             CancelControlID="btnCloseedit"    BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />  
    
             
<TABLE id="Table1" width="100%" border=0><TBODY><TR style="FONT-SIZE: 12pt"><TD class="title" align=left width="50%">
    Upload PDF Timetable</TD></TR>
<tr align="left"><td>
 <asp:Label id="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                </asp:Label>
</td></tr></TBODY></TABLE>
 <table id="tbl_Group" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
        <td>
    <table align="center" cellpadding="5" cellspacing="0" class="BlueTable"  width="80%">
      
        <tr class="subheader_img" valign="top">
            <td align="left" colspan="3" style="height: 18px">
                Upload PDF Timetable</td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 66px">
                Academic Year</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters">
                <asp:DropDownList id="ddlAcdID" runat="server" Width="174px" 
                    OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList>&nbsp;
                </td>
        </tr>
       
        <tr style="height:80px;">
            <td align="left" class="matters" style="width: 66px">
                Upload file</td>
            <td align="left" class="matters" style="font-size: 9pt; ">
                :</td>
            <td align="left" class="matters"   >
              <iframe id="iframeFileLoad" src="tblUpload_PDF_Bulk.aspx" scrolling="no" frameborder="0" 
               style="text-align:center;vertical-align:middle;border-style:none;margin:0px;width:100%;height:35px"
                 ></iframe>
                <asp:Label id="ltNote" runat="server"></asp:label>
            </td>
        </tr>
                
    </table>
    </td>
    </tr>
    <tr>
    <td align="center">
    <table>
    <tr>
    
    <td>
      
                            <asp:GridView id="gvInfo" runat="server" AutoGenerateColumns="False"
                                CssClass="gridstyle" Height="100%" Width="400px" >
                                <rowstyle cssclass="griditem" height="25px" />
                                <columns>
<asp:TemplateField HeaderText="Sl.No"><ItemTemplate>
<asp:Label id="lblsNo" runat="server" Text='<%# Bind("SRNO") %>' 
        __designer:wfdid="w17"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Grade"><EditItemTemplate>
    &nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblGRADE" runat="server" Text='<%# bind("GRM_DISPLAY") %>' 
        __designer:wfdid="w18"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
</asp:TemplateField>
                                    <asp:TemplateField HeaderText="PDF File">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgThumb" runat="server"
                                                ImageUrl='<%# bind("FILE_PATH") %>' onclick="imgThumb_Click" 
                                                style="width: 32px;height:32px;"  AlternateText='<%# bind("FILE_ALT") %>'
                                                 ToolTip='<%# bind("FILE_ALT") %>'/>
                                                 <asp:Label ID="lblFilePath" runat="server" Text='<%# bind("ACTUAL_FILE_PATH") %>' Visible="false" ></asp:Label>
                                                 <asp:Label ID="lblGrade_ID" runat="server" Text='<%# bind("GRM_GRD_ID") %>' Visible="false" ></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
<asp:TemplateField HeaderText="Edit">
<ItemTemplate>
    <asp:LinkButton ID="lbtnEdit" runat="server" onclick="lbtnEdit_Click">Edit</asp:LinkButton>
</ItemTemplate>

    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

</asp:TemplateField>
</columns>
                                <selectedrowstyle cssclass="Green" />
                                <headerstyle cssclass="gridheader_pop" height="15px" horizontalalign="Center" verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" height="25px" />
                            </asp:GridView>
    
    
    </td>
    </tr>
    </table>
    <div id="plStudAtt"  runat="server" style="display: none; overflow: visible; border-color: #1b80b6; border-style:solid;
        border-width:1px;width:900px;Height:400px; background-color:White;">  
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="msg_header" style="width: 900px;margin-top:1px;vertical-align:middle;background-color:White;">
             <div class="msg"  style="margin-top:0px;vertical-align:middle; text-align:left;">
            <asp:Literal ID="ltHeader" runat="server"></asp:Literal>
               <span style="clear: right;display: inline; float: right; visibility: visible;margin-top:-4px; vertical-align:top; ">
               <asp:ImageButton ID="btnClose" runat="server" ImageUrl="~/Images/closeme.png"  
               style="margin-top:-1px;vertical-align:top;padding-bottom:2px;float:right;" /></span> </div>
           </div>
               
   <table cellpadding="0px" cellspacing="0px"  width="100%"  style="height:400px;"  >
     <tr>
  
   <td style="text-align:center;vertical-align:middle;" align="center" valign="middle">

      
      <asp:Panel id="plphoto" runat="server" Width="100%" Height="400px" style="text-align:center;" BackColor="White"  ScrollBars="None"  >
       <asp:Literal ID="ltFrame" runat="server"></asp:Literal>
         </asp:Panel>
   </td>
   
   </tr>
    
   </table>
   
    
  </ContentTemplate>
           
       </asp:UpdatePanel>
     </div>
     
<div id="divStudEdit"  runat="server" style="display: none; overflow: visible; border-color: #1b80b6; 
    border-style:solid;border-width:1px;width:550px;">
<asp:UpdatePanel ID="uppnlPopup" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
         <div class="msg_header" style="width: 550px;margin-top:1px;vertical-align:middle;background-color:White;"> 
         <div class="msg"  style="margin-top:0px;vertical-align:middle; text-align:left;">
            <span style="clear: left;display: inline; float: left; visibility: visible;"> 
            <asp:Literal ID="ltName" runat="server"></asp:Literal></span>
               <span style="clear: right;display: inline; float: right; 
                   visibility: visible;margin-top:-4px; vertical-align:top; ">
                   <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/closeme.png"    
                   style="margin-top:-1px;vertical-align:top;" /></span> </div>
           </div>
 <asp:Panel id="plStudEdit" runat="server" Width="549px" Height="150px" 
 ScrollBars="none" style="text-align:center;margin-top:-1px;" BackColor="White" >
   <div style="float:right; display:inline;text-align:right;">
     </div>
   <table cellpadding="0px" cellspacing="0px" >
   <tr align="center"  >
   <td>
   <iframe id="iframeEdit" src="tblUpload_PDF_Gradewise.aspx" scrolling="no" frameborder="0" 
               style="text-align:center;vertical-align:middle;border-style:none;margin:0px;width:449px;height:150px" ></iframe>
   </td>
   </tr>
   </table>
   
    
    
  
     </asp:Panel> 
  </ContentTemplate>
           
       </asp:UpdatePanel>
</div>
 
 
 
 
 
 
    
    </td>
    
    </tr>
    </table>
  
    </asp:Content>

  
   
    
   
  

