Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studBsu_Service_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050002") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'txtService.Attributes.Add("readonly", "readonly")

                    If ViewState("datamode") = "view" Then

                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                        'Using readerSTU_BSU_RELIGION As SqlDataReader = AccessStudentClass.GetSTU_BSU_RELIGION(ViewState("viewid"))
                        '    While readerSTU_BSU_RELIGION.Read

                        '        'handle the null value returned from the reader incase  convert.tostring

                        '        hfReligion.Value = Convert.ToString(readerSTU_BSU_RELIGION("R_ID"))

                        '        txtService.Text = Convert.ToString(readerSTU_BSU_RELIGION("R_DESCR"))

                        '    End While

                        'End Using
                        GetServiceName()
                    ElseIf ViewState("datamode") = "add" Then


                    End If

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            Call clearRecords()


            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("STU_BSU_RELIGION", ex.Message)
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        UtilityObj.beforeLoopingControls(Me.Page)

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearRecords()

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Sub clearRecords()
        txtService.Text = ""
        hfReligion.Value = 0
        ViewState("viewid") = 0
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        If Page.IsValid = True Then
            str_err = calltransaction(errorMessage)
            If str_err = "0" Then
                lblError.Text = "Record Saved Successfully"
            Else
                lblError.Text = errorMessage
            End If
        End If



    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim SVC_ID As Integer = 0
        'Dim SBR_RLG_ID As String = hfReligion.Value
        Dim SBR_BSU_ID As String = Session("sBsuid")

        Dim bEdit As Boolean

        Dim status As Integer
        Dim transaction As SqlTransaction

        If ViewState("datamode") = "add" Then
            bEdit = False
            'get the max Id to be inserted in audittrial report
            Dim sqlSVC_ID As String = "Select isnull(max(SVC_ID),0)+1  from SERVICES_SYS_M"
            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlSVC_ID, connection)
            Dim temp As String
            Command.CommandType = CommandType.Text
            temp = Command.ExecuteScalar()
            connection.Close()
            connection.Dispose()


            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    'call the class to insert the record into STU_BSU_RELIGION
                    'status = AccessStudentClass.SaveSTU_BSU_RELIGION(SBR_ID, SBR_RLG_ID, SBR_BSU_ID, bEdit, transaction)
                    'throw error if insert to the ACADEMICYEAR_D is not Successfully

                    Dim pParms(4) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@SVC_ID", temp)
                    pParms(1) = New SqlClient.SqlParameter("@SVC_DESCRIPTION", txtService.Text)
                    'pParms(2) = New SqlClient.SqlParameter("@SBR_BSU_ID", SBR_BSU_ID)
                    pParms(2) = New SqlClient.SqlParameter("@bEdit", 0)
                    pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(3).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveSTU_BSU_SERVICE", pParms)
                    Dim ReturnFlag As Integer = pParms(3).Value
                    'Return ReturnFlag
                    'End Using
                    If ReturnFlag <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)
                        Return "1"
                    End If

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), temp, "Insert", Page.User.Identity.Name.ToString)

                    If flagAudit <> 0 Then
                        calltransaction = "1"
                        errorMessage = "Could not process your request."
                        Return "1"
                    End If

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        Call clearRecords()
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using

        ElseIf ViewState("datamode") = "edit" Then


            bEdit = True
            SVC_ID = ViewState("viewid")
            If SVC_ID <> 0 Then

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'status = AccessStudentClass.SaveSTU_BSU_RELIGION(SBR_ID, SBR_RLG_ID, SBR_BSU_ID, bEdit, transaction)
                        Dim pParms(4) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@SVC_ID", SVC_ID)
                        pParms(1) = New SqlClient.SqlParameter("@SVC_DESCRIPTION", txtService.Text)
                        'pParms(2) = New SqlClient.SqlParameter("@SBR_BSU_ID", SBR_BSU_ID)
                        pParms(2) = New SqlClient.SqlParameter("@bEdit", 1)
                        pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(3).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveSTU_BSU_SERVICE", pParms)
                        Dim ReturnFlag As Integer = pParms(3).Value

                        If ReturnFlag <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        End If




                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), SVC_ID, "edit", Page.User.Identity.Name.ToString, Me.Page)

                        If flagAudit <> 0 Then
                            calltransaction = "1"
                            errorMessage = "Could not process audit request.Please contact System Admin."
                            Return "1"
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        ViewState("viewid") = "0"
                        ViewState("datamode") = "none"
                    Catch ex As Exception
                        calltransaction = "1"
                        errorMessage = "Error Occured While Saving."
                    Finally
                        If calltransaction <> "0" Then
                            UtilityObj.Errorlog(errorMessage)
                            transaction.Rollback()
                        Else
                            errorMessage = ""
                            transaction.Commit()
                        End If
                    End Try
                End Using
            Else
                lblError.Text = "Select the record to be edited"
            End If

        End If



    End Function

    Private Function GetServiceName()
        Dim str_sql As String
        Dim ds As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()

        str_sql = "SELECT SVC_DESCRIPTION FROM SERVICES_SYS_M WHERE SVC_ID='" & ViewState("viewid") & "'"
        ds = SqlHelper.ExecuteDataset(connection, CommandType.Text, str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtService.Text = ds.Tables(0).Rows(0)("SVC_DESCRIPTION")
        Else
            txtService.Text = ""
        End If


    End Function

End Class
