﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="encTemplate_M.aspx.vb" Inherits="Students_encTemplate_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
  Email Template </div> 
    <div class="card-body">
        <div class="table-responsive m-auto">
            <table width="100%">
                <tr>
                    <td align="left" colspan="2" >
                        <asp:LinkButton ID="lnkadd" runat="server" Text="Add New"></asp:LinkButton>
                    </td>
                </tr>
                
                <tr align="left">
                    <td><span class="field-label">Business Unit</span> 
                    </td>

                    <td>
                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" align="center">
                        <table id="Table2" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvEmail" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20"  BorderStyle="None">
                                        <Columns>
                                            <asp:TemplateField Visible="False">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEMTID" runat="server" Text='<%# Bind("ETS_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="False">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("ETS_BSU_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="School">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("BSU_SHORTNAME")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Template Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("ETS_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:ButtonField CommandName="view" HeaderText="View" Text="View">
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"  />
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:ButtonField>
                                            <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                        Text="Delete"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                        runat="server">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                        <RowStyle CssClass="griditem"  Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div> 
    </div> 

</asp:Content>

