﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Students_studTC_BlockOnline
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100284") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    BindSegment()
                    BindBusinessunit()

                End If
                gvBsu.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnBlock_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ' #28b828 - Green
        ' #d92626 - Red

        Dim lblBsuId As Label = TryCast(sender.findcontrol("lblBsuId"), Label)
        Dim btnBlock As Button = TryCast(sender.findcontrol("btnBlock"), Button)
        Dim bBlock As String
        If btnBlock.Text.ToLower = "opened" Then
            bBlock = "true"
        Else
            bBlock = "false"
        End If

        SaveData(lblBsuId.Text, bBlock)

        BindBusinessunit()
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSegment()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT BSG_ID,BSG_DESCR FROM [DBO].[BUSSEGMENT_M] " _
                                  & " INNER JOIN BUSINESSUNIT_M ON BSG_ID=BUS_BSG_ID " _
                                  & " WHERE BSU_BONLINETC = 1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        rlSegment.DataSource = ds
        rlSegment.DataTextField = "BSG_DESCR"
        rlSegment.DataValueField = "BSG_ID"
        rlSegment.DataBind()

        rlSegment.ClearSelection()
        If Not rlSegment.Items.FindByText("Premium") Is Nothing Then
            rlSegment.Items.FindByText("Premium").Selected = True
        Else
            rlSegment.Items(0).Selected = True
        End If
    End Sub

    Sub BindBusinessunit()
        ' #28b828 - Green
        ' #d92626 - Red
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_SHORTNAME,BSU_NAME,CASE WHEN ISNULL(BSU_bBLOCKONLINETC,0)=0 THEN 'Opened' Else 'Closed' end as BSU_BLOCK, " _
                                  & "CASE WHEN ISNULL(BSU_bBLOCKONLINETC,0)=0 THEN '#28b828' Else '#d92626' end as BSU_COLOR FROM BUSINESSUNIT_M WHERE BSU_BSCHOOL=1 " _
                                  & " AND BSU_ID IN(SELECT BSU_ID from oasis_fim.[FIM].[BSU_M] WHERE BSU_MENASA='MENASA')" _
                                  & " AND BUS_BSG_ID=" + rlSegment.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvBsu.DataSource = ds
        gvBsu.DataBind()
    End Sub

    Sub SaveData(bsu_id As String, bBlock As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC saveTC_BLOCCKONLINE " _
                                  & "@BSU_ID ='" + bsu_id + "'," _
                                  & "@bBLOCK='" + bBlock + "'," _
                                  & "@USER='" + Session("susr_name") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region

    Protected Sub rlSegment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rlSegment.SelectedIndexChanged
        BindBusinessunit()
    End Sub
End Class
