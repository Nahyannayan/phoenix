﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Covid_Stud_Concession_det.aspx.vb" Inherits="Students_Covid_Stud_Concession_det" %>

<!DOCTYPE html>
 
<html xmlns="http://www.w3.org/1999/xhtml">
   
<head runat="server">
    
   <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>
      <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui-1.10.2.js" type="text/javascript"></script>
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
    <script lang="javascript" type="text/javascript">

         function click_me(e) {
             var dateToday = new Date();
             $("input[id$=txtExpDate]").datepicker({

                 changeMonth: true,

                 changeYear: true,
                 yearRange: '1995:2050',
                 dateFormat: "dd/M/yy",
                 minDate: dateToday

             });
         }
         var dateToday1 = new Date();
             $(function (e) {
                 $("input[id$=txtExpDate]").datepicker({

                     changeMonth: true,
                     changeYear: true,
                     yearRange: '1995:2050',
                     dateFormat: "dd/M/yy",
                     minDate: dateToday1
                 });

             });

             </script>
             <script type="text/javascript">
         function reset_lblComment(e) {

             document.getElementById('<%= lblComment.ClientID%>').innerText = "";


           }
           function reset_lblStatus(e) {

               document.getElementById('<%= lblStatus.ClientID%>').innerText = "";


           }
         function reset_lblExpDate(e) {

               document.getElementById('<%= lblExpDate.ClientID%>').innerText = "";


         }
         </script>  
     
</head>
<body >
    <form id="form1" runat="server">
      
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

                  <table id="tblStud1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>

                            <table id="tblStud" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class=" title-bg" colspan="4">Student Details</td>
                                </tr>
                                 <tr>
                                    <td align="left">
                                        <span class="field-label">Status</span><span class="text-danger font-small"></span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlStatus" runat="server"  onchange="reset_lblStatus(event, this)">
                                        </asp:DropDownList><br />
                                         <asp:Label ID="lblStatus" runat="server" class="text-danger small"></asp:Label>
                                    </td>
                                     </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Expiry Date</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtExpDate" runat="server" autocomplete="off" onkeypress="reset_lblExpDate(event,this)" onfocus="click_me(event,this)" onClick="click_me(event,this)" Onmousedown="click_me(event,this)"></asp:TextBox><br />
                                        <asp:Label ID="lblExpDate" runat="server" class="text-danger small"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Comments</span></td>

                                    <td align="left">
                                       <asp:TextBox runat="server" ID="txtComment" TextMode="MultiLine" Rows="5"  onkeypress="reset_lblComment(event,this)" /><br />
                                        <asp:Label ID="lblComment" runat="server" class="text-danger small"></asp:Label>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                       <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button"  CausesValidation="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan="4">

                                         <asp:GridView ID="gvAdmin" runat="server" AllowPaging="True" AutoGenerateColumns="true"
                                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                PageSize="20" Width="100%" AllowSorting="True">
                                <RowStyle CssClass="griditem" />
                                
                                    </asp:GridView>
                                    </td>
                                </tr>
                                </table>
                            
                          
                            </td>
                        </tr>
                      </table>

                </div>
            </div>
         </div>
                           
    </form>
</body>
</html>
