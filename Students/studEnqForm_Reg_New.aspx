﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnqForm_Reg_New.aspx.vb" Inherits="Students_studEnqForm_Reg_New" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="<%= ResolveUrl("~/cssfiles/EnquiryStyle.css")%>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/vendor/Tabs-To-Accordion-master/css/easy-responsive-tabs.css")%>" rel="stylesheet" />
    <script src="<%= ResolveUrl("~/vendor/Tabs-to-Accordion-master/js/easyResponsiveTabs.js")%>"></script>
    <link href = "<%= ResolveUrl("~/cssfiles/build.css")%>" rel="stylesheet" />
    <ajaxToolkit:ModalPopupExtender ID="mpe"   runat="server"
             Enabled="True" TargetControlID="btnTarget" PopupControlID="plPrev" BackgroundCssClass="modalBackground" x="450" Y="160"/> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Enquiry in School
      
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  text-right">
                  <a class="btn btn-primary btn-sm" href="<%= ResolveUrl("~/Students/EnquiryDashboard.aspx?MainMnu_code=LjcF1BVVHbk%3d&datamode=Zo4HhpVNpXc%3d")%>">Enquiry Managment</a>

                    <%--       <button type="button" class="btn btn-success btn-sm">Follow up</button>--%>
                </div>

            </div>
        </div>

        <div class="card-body">
               <div class="col-12 text-center">
                    <asp:ValidationSummary ID="vsPrim_Contact" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="groupM1"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="vsPrev_Info" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="Prev_Info"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="vsAppl_info" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="App_Info"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
               </div>
            <div class="col-12 text-center">Marked with (<font color="red">*</font>) are mandatory</div>
            <div class="clearfix"></div>

            <div class="row"> 
                <asp:Panel ID="pnlHeader" runat="server" Width="100%">
                <div class="col-md-12">
                    <div class="row">
                           <div class="col-md-8">
                    <div class="form-group">
                        <label class="Ftitle"> Name As In Birth Certificate/Passport 
                             <asp:Label ID="lbl_name" runat="server" ForeColor="Red" Text="*" ></asp:Label>
                        </label>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:TextBox ID="txtFname" runat="server" MaxLength="100" placeholder="First Name" CssClass="form-control"> </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFname_Stud" runat="server" ControlToValidate="txtFname"
                                    Display="Dynamic" ErrorMessage="First name required" ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtMname" runat="server" MaxLength="100" placeholder="Middle Name" CssClass="form-control">  </asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvtxtMname" runat="server" ControlToValidate="txtMname"
                                    Display="Dynamic" ErrorMessage="Middle name required" ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtLname" runat="server" MaxLength="100" placeholder="Last Name" CssClass="form-control"> </asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvtxtLname" runat="server" ControlToValidate="txtLname"
                                    Display="Dynamic" ErrorMessage="Last name required" ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">Gender</label>
                        <div class="RB_Holder">
                            <label class="radio-inline radio radio-success">
                                <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" Text="Male"></asp:RadioButton>
                            </label>
                            <label class="radio-inline radio radio-success">
                                <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female"></asp:RadioButton>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Religion 
                            <asp:Label ID="ltReg" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label></label>
                        <asp:DropDownList ID="ddlReligion" runat="server" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="ddlReligion"
                            Display="Dynamic" ErrorMessage="Please select Religion"
                            InitialValue=" " ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Date Of Birth 
                        <asp:Label ID="ltDOB" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label></label>
                        <div class="Calendar_Holder">
                            <asp:TextBox ID="txtDob" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnDob_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvDatefrom" runat="server" ControlToValidate="txtDob"
                            Display="Dynamic" ErrorMessage="Date of birth cannot be left empty"
                            ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                            PopupButtonID="imgBtnDob_date" TargetControlID="txtDob">
                        </ajaxToolkit:CalendarExtender>
                    </div>
                </div>
                
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                         Place Of Birth 
                           <asp:Label ID="ltPOB" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label></label>
                        <asp:TextBox ID="txtPob" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPob" runat="server" ControlToValidate="txtPob" 
                            Display="Dynamic" ErrorMessage="Place of birth cannot be left empty" ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>


                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Country Of Birth 
                       <asp:Label ID="ltCOB" runat="server" Text="*" ForeColor="Red" Visible="false"></asp:Label></label>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCOB" runat="server" ControlToValidate="ddlCountry"
                            Display="Dynamic" ErrorMessage="Please select Country of Birth"
                            InitialValue=" " ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Nationality 
                            <asp:Label ID="ltNationality" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label></label>
                        <asp:DropDownList ID="ddlNational" runat="server" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="ddlNational"
                            Display="Dynamic" ErrorMessage="Please select Student Nationality"
                            InitialValue=" " ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Ethnicity 
                        </label>
                        <asp:DropDownList ID="ddlEthnicity" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Select" Value="-1" />
                            <asp:ListItem Text="Other" Value="0" />
                        </asp:DropDownList>

                    </div>
                </div>
                <div class="col-md-12" id="ddlEthnicity_Other" style="display: none;">
                    <div class="form-group">
                        <label class="Ftitle">
                            (If you choose other,please specify the ethnicity) 
                        </label>
                        <asp:TextBox ID="txtEthnicity" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Passport No 
                            <asp:Label ID="ltPASSNO" runat="server" ForeColor="Red" Text=""></asp:Label></label>
                        <asp:TextBox ID="txtPassport" runat="server" CssClass="form-control"> </asp:TextBox>
                          <asp:RequiredFieldValidator ID="rfv_txtPassport" runat="server" ControlToValidate="txtPassport"
                            Display="Dynamic" ErrorMessage="Passport No cannot be left empty"
                            InitialValue=" " ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="Ftitle">
                            Emergency Contact No (Country-Area-Number) 
                            <asp:Label ID="lbl_Contact_No" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                        </label>
                        <div class="row">
                            <div class="col-md-3">
                                <asp:TextBox ID="txtStud_Contact_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                            </div>
                            <div class="col-md-3 DT-p-0">
                                <asp:TextBox ID="txtStud_Contact_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtStud_Contact_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                            </div>
                            <asp:CompareValidator ID="cvContact_Country" runat="server" ControlToValidate="txtStud_Contact_Country"
                                Display="Dynamic" ErrorMessage="Enter valid country code" Operator="DataTypeCheck"
                                Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                            <asp:CompareValidator ID="cvContact_Area" runat="server" ControlToValidate="txtStud_Contact_Area"
                                Display="Dynamic" ErrorMessage="Enter valid area code" Operator="DataTypeCheck"
                                Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                            <asp:CompareValidator ID="cvContact_No" runat="server" ControlToValidate="txtStud_Contact_No"
                                Display="Dynamic" ErrorMessage="Enter valid contact no" Operator="DataTypeCheck"
                                Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                            <asp:RegularExpressionValidator ID="revContactNo" runat="server" ControlToValidate="txtStud_Contact_No"
                                Display="Dynamic" ErrorMessage="Enter valid contact no." SetFocusOnError="True"
                                ValidationExpression="^[0-9]{1,10}$" ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                               <asp:RequiredFieldValidator ID="rfvtxtStud_Contact_No" runat="server" ControlToValidate="txtStud_Contact_No"
                            Display="Dynamic" ErrorMessage="Please select Student Emergency Contact No (Country-Area-Number)"
                            InitialValue=" " ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="Ftitle">
                            Mobile No (Country-Area-Number)
                        </label>
                        <div class="row">
                            <div class="col-md-3">
                                <asp:TextBox ID="txtM_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                            </div>
                            <div class="col-md-3 DT-p-0">
                                <asp:TextBox ID="txtM_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtMobile_Pri" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                            </div>
                            <asp:CompareValidator ID="cvM_Country" runat="server" ControlToValidate="txtM_Country"
                                Display="Dynamic" ErrorMessage="Enter valid mobile country code" Operator="DataTypeCheck"
                                Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator><span class="text-info">
                                </span>
                            <asp:CompareValidator ID="cvM_Area" runat="server" ControlToValidate="txtM_Area"
                                Display="Dynamic" ErrorMessage="Enter valid mobile area code" Operator="DataTypeCheck"
                                Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator><span style="color: #1b80b6">
                                </span>
                            <asp:CompareValidator ID="cvM_No" runat="server" ControlToValidate="txtMobile_Pri"
                                Display="Dynamic" ErrorMessage="Enter valid mobile no." Operator="DataTypeCheck"
                                Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                            <asp:RegularExpressionValidator ID="revMobile" runat="server" ControlToValidate="txtMobile_Pri"
                                Display="Dynamic" ErrorMessage="Enter valid mobile no." SetFocusOnError="True"
                                ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="display: none">
                    <div class="form-group">
                        <label class="Ftitle">
                            Set as sibling
                        </label>
                        <asp:RadioButton ID="rdSib" runat="server" GroupName="SetSib"
                            Text="Yes" Checked="true"></asp:RadioButton>

                        <asp:RadioButton ID="rdSib2" runat="server" GroupName="SetSib"
                            Text="No"></asp:RadioButton>
                    </div>
                </div>
                                       <div class="col-md-12">     <hr /></div>
                <div class="col-md-4">
                    <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                        <label class="Ftitle">
                            First Language(Main) 
                        </label>
                        <asp:DropDownList ID="ddlFLang" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                                 </div>
                    <div class="form-group">
                        <label class="Ftitle">
                            Proficiency in English 
                        </label>
                        <div class="row">

                            <div class="col-md-12 mb-2">
                                <label >
                                  <strong style="width: 80px;display: inline-block;">  Reading : </strong>
                                </label>
                                <div style="display:inline-block">

                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRExc" runat="server" GroupName="Read" Text="Excellent" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRGood" runat="server" GroupName="Read" Text="Good" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRFair" runat="server" GroupName="Read" Text="Fair" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRPoor" runat="server" GroupName="Read" Text="Poor" />
                                    </label>
                                </div>
                            </div> 
                            <div class="col-md-12 mb-2">
                                <label >
                                <strong style="width: 80px;display: inline-block;">   Writing : </strong> 
                                </label>
                                     <div style="display:inline-block">

                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWExc" runat="server" GroupName="Write" Text="Excellent" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWGood" runat="server" GroupName="Write" Text="Good" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWFair" runat="server" GroupName="Write" Text="Fair" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWPoor" runat="server" GroupName="Write" Text="Poor" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label >
                                    <strong style="width: 80px;display: inline-block;">
                                    Speaking : </strong>
                                </label>
                                      <div style="display:inline-block">

                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSExc" runat="server" GroupName="Speak" Text="Excellent" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSGood" runat="server" GroupName="Speak" Text="Good" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSFair" runat="server" GroupName="Speak" Text="Fair" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSPoor" runat="server" GroupName="Speak" Text="Poor" />
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="Ftitle">
                            Other Languages(Specify) 
                        </label>
                        <div id="plOLang" runat="server" class="checkbox-list modifay form-control">
                            <div class="checkbox checkbox-success ">
                            <asp:CheckBoxList ID="chkOLang" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="styled">
                            </asp:CheckBoxList>
                                </div>
                        </div>
                    </div>
                </div>
                
                        <div class="col-md-12">     <hr /></div>
                   
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="Ftitle">
                            Primary Contact 
                        </label>
                        <div class="RB_Holder">
                            <label class="radio-inline radio radio-success">
                                <asp:RadioButton ID="rdPri_Father" runat="server" AutoPostBack="True" GroupName="Pri_contact"
                                    Text="Father" ValidationGroup="Pri_contact"></asp:RadioButton>
                            </label>
                            <label class="radio-inline radio radio-success">
                                <asp:RadioButton ID="rbPri_Mother" runat="server" AutoPostBack="True" GroupName="Pri_contact"
                                    Text="Mother" ValidationGroup="Pri_contact"></asp:RadioButton>
                            </label>
                            <label class="radio-inline radio radio-success">
                                <asp:RadioButton ID="rdPri_Guard" runat="server" GroupName="Pri_contact" Text="Guardian"
                                    ValidationGroup="Pri_contact"></asp:RadioButton>
                            </label>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="Ftitle">Applicant Name As In Passport
                             <asp:Label ID="lbl_Fname" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                        </label>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:DropDownList ID="ddlCont_Status" runat="server" CssClass="form-control pull-left ddl_title">
                                    <asp:ListItem>Mr</asp:ListItem>
                                    <asp:ListItem>Mrs</asp:ListItem>
                                    <asp:ListItem>Ms</asp:ListItem>
                                    <asp:ListItem>Dr</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtPri_Fname" runat="server" placeholder="First Name" CssClass="form-control pull-left txt_name">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPri_Fname" runat="server" ControlToValidate="txtPri_Fname"
                                    Display="Dynamic" ErrorMessage="Primary Conact First name required"
                                    ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtPri_Mname" runat="server" MaxLength="70" placeholder="Middle Name" CssClass="form-control">   </asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtPri_Lname" runat="server" MaxLength="70" placeholder="Last Name" CssClass="form-control"> </asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfv_txtPri_Lname" runat="server" ControlToValidate="txtPri_Lname"
                                    Display="Dynamic" ErrorMessage="Primary Conact Last name required"
                                    ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Nationality 
                          <asp:Label ID="ltNational1" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label></label>
                        <asp:DropDownList ID="ddlPri_National1" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvNational1" runat="server" ControlToValidate="ddlPri_National1"
                            Display="Dynamic" ErrorMessage="Please select Primary Contact Nationality"
                            InitialValue=" " ValidationGroup="groupM1" Visible="true">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="Ftitle">
                            Nationality2 
                        </label>
                        <asp:DropDownList ID="ddlPri_National2" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="Ftitle">
                            Email 
                            <asp:Label ID="ltPri_Email" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label></label>
                        <asp:TextBox ID="txtEmail_Pri" runat="server" MaxLength="100" CssClass="form-control">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPri_Email" runat="server" ErrorMessage="E-Mail ID required" Display="None"
                            ValidationGroup="groupM1" ControlToValidate="txtEmail_Pri" Visible="true">*</asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="Ftitle">(Online Enquiry Acknowledgement will be mailed to your E-mail address)</label>
                        <div class="RB_Holder">
                            <label class="checkbox-inline checkbox checkbox-success">
                                <asp:CheckBox ID="chkEmailSend" runat="server" Text="Mail Acknowledgement" Checked="true"></asp:CheckBox>
                            </label>

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="Ftitle">Print Application Form</label>
                        <div class="RB_Holder">
                            <label class="checkbox-inline checkbox checkbox-success">
                                <asp:CheckBox ID="chkPrint" runat="server" Text="Yes" />

                            </label>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="RB_Holder">
                            <label class="checkbox-inline checkbox checkbox-success">
                                <asp:CheckBox ID="chkRef" runat="server"
                                    Text="Were you referred to GEMS by a friend? If ‘YES’ enter the reference code &amp;amp; your reference email address." />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="referred_to_GEMS" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Reference Code(Case sensitive) 
                                </label>
                                <asp:TextBox ID="txtRefCode" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Email 
                                </label>
                                <asp:TextBox ID="txtREFEmail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>


                    </div>
                </div>


                 
             
                      </asp:Panel>



                <div class="clearfix "></div>
                <div id="parentHorizontalTab" class="mt-3">
                    <ul class="resp-tabs-list hor_1">
                        <li>Main</li>
                        <li>Passport / Visa</li>
                        <li>Contact Details</li>
                        <li>Current School</li>
                        <li>Services</li>
                        <li>Other Details</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div>
                            <asp:Panel ID="pnlMain" runat="server" Width="100%">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                School  </label>
                                            <asp:DropDownList ID="ddlGEMSSchool" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Curriculum </label>
                                            <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Academic Year </label>
                                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                <asp:Literal ID="ltStream_lb" runat="server"></asp:Literal><span class="text-info"></span></label>
                                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                <asp:Literal ID="ltGrade_lb" runat="server"></asp:Literal>
                                                </label>
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-2" id="trStm" runat="server">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Meal Plan </label>
                                            <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RadioButton ID="rbMealYes" runat="server" GroupName="meal" Text="Yes" />
                                            <asp:RadioButton ID="rbMealNo" runat="server" GroupName="Meal" Text="No" />
                                        </div>
                                    </div>
                                    <div class="col-md-2"  id="trtrm" runat="server">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Term </label>
                                            <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2"  id="trtjd" runat="server">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Tentative Joining Date  <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtDOJ" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButton1" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="caldoj" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="ImageButton1" TargetControlID="txtDoj">
                                            </ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <asp:Panel ID="pnlPassPort" runat="server" Width="100%">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport Issue Place</label>
                                            <asp:TextBox ID="txtPassportIssue" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport Issue Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtPassIss_date" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnPassIss_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnPassIss_date" TargetControlID="txtPassIss_date">
                                            </ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport Expiry Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtPassExp_Date" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnPassExp_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnPassExp_date" TargetControlID="txtPassExp_Date">
                                            </ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa No</label>
                                            <asp:TextBox ID="txtVisaNo" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa Issue Place</label>
                                            <asp:TextBox ID="txtIss_Place" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa Issue Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtVisaIss_date" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnVisaIss_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnVisaIss_date" TargetControlID="txtVisaIss_date">
                                            </ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa Expiry Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtVisaExp_date" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnVisaExp_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnVisaExp_date" TargetControlID="txtVisaExp_date">
                                            </ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Issuing Authority</label>
                                            <asp:TextBox ID="txtIss_Auth" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                        </div>
                        <div>
                            <asp:Panel ID="pnlParent" runat="server" Width="100%">

                                <div class=" title-bg-Form-small">
                                    Contact Detail
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>Does the Student have one or more Siblings studying in a GEMS school ? </label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkSibling" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-12" id="28" style="display: none">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="text-info">Please give details of any one</span>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Sibling Name</label>
                                                    <asp:TextBox ID="txtSib_Name" runat="server" CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Sibling Fee ID</label>
                                                    <asp:TextBox ID="txtSib_ID" runat="server" AutoPostBack="True" OnTextChanged="txtSib_ID_TextChanged" CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3" id="29" style="display: none">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        GEMS School</label>
                                                    <asp:DropDownList ID="ddlSib_BSU" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Primary Contact Details 
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-12">
                                        <br />
                                        <br />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Permanent Address
                                </div>
                                <div class="row  mb-2">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 1</label>
                                            <asp:TextBox ID="txtAdd1_overSea" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 2</label>
                                            <asp:TextBox ID="txtAdd2_overSea" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address City/State</label>
                                            <asp:TextBox ID="txtOverSeas_Add_City" runat="server" MaxLength="70" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Country</label>
                                            <asp:DropDownList ID="ddlOverSeas_Add_Country" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Phone (Country-Area-Number)
                                            </label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="txtPhone_Oversea_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                </div>
                                                <div class="col-md-3 DT-p-0">
                                                    <asp:TextBox ID="txtPhone_Oversea_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtPhone_Oversea_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvPhone_OS_Country" runat="server" ControlToValidate="txtPhone_Oversea_Country"
                                                    Display="Dynamic" ErrorMessage="Enter valid country code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:CompareValidator ID="cvPhone_OS_Area" runat="server" ControlToValidate="txtPhone_Oversea_Area"
                                                    Display="Dynamic" ErrorMessage="Enter valid  area code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator><span style="font-size: 7pt">
                                                    </span>
                                                <asp:CompareValidator ID="cvPhone_OS_No" runat="server" ControlToValidate="txtPhone_Oversea_No"
                                                    Display="Dynamic" ErrorMessage="Enter valid  no." Operator="DataTypeCheck" Type="Double"
                                                    ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone_Oversea_No"
                                                    Display="Dynamic" ErrorMessage="Enter valid Phone no." SetFocusOnError="True"
                                                    ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box/Zip Code</label>
                                            <asp:TextBox ID="txtPoBox_Pri" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtPoBox_Pri"
                                                Display="Dynamic" ErrorMessage="Enter valid Zip code" Operator="DataTypeCheck"
                                                Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Current Address 
                                </div>
                                <div class="row  mb-2">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Apartment No</label>
                                            <asp:TextBox ID="txtApartNo" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Building</label>
                                            <asp:TextBox ID="txtBldg" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Street</label>
                                            <asp:TextBox ID="txtStreet" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Area</label>
                                            <asp:TextBox ID="txtArea" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/State</label>
                                            <asp:TextBox ID="txtCity_pri" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Country</label>
                                            <asp:DropDownList ID="ddlCountry_Pri" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box</label>
                                            <asp:TextBox ID="txtPoboxLocal" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/Emirate</label>
                                            <asp:DropDownList ID="ddlEmirate" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Home Phone (Country-Area-Number)
                                            </label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="txtHPhone_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                </div>
                                                <div class="col-md-3 DT-p-0">
                                                    <asp:TextBox ID="txtHPhone_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtHPhone" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvUH_Country" runat="server" ControlToValidate="txtHPhone_Country"
                                                    Display="Dynamic" ErrorMessage="Enter valid home phone country code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator><span class="text-info">
                                                    </span>
                                                <asp:CompareValidator ID="cvUH_Area" runat="server" ControlToValidate="txtHPhone_Area"
                                                    Display="Dynamic" ErrorMessage="Enter valid home phone area code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:CompareValidator ID="cvHPhone" runat="server" ControlToValidate="txtHPhone"
                                                    Display="Dynamic" ErrorMessage="Enter valid home phone no." Operator="DataTypeCheck"
                                                    Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtHPhone"
                                                    Display="Dynamic" ErrorMessage="Enter valid Home Phone no." SetFocusOnError="True"
                                                    ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Office Phone (Country-Area-Number)
                                            </label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="txtOPhone_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                </div>
                                                <div class="col-md-3 DT-p-0">
                                                    <asp:TextBox ID="txtOPhone_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtOPhone" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtOPhone_Country"
                                                    Display="Dynamic" ErrorMessage="Enter valid office phone country code" Operator="DataTypeCheck"
                                                    Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtOPhone_Area"
                                                    Display="Dynamic" ErrorMessage="Enter valid office phone area code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtOPhone"
                                                    Display="Dynamic" ErrorMessage="Enter valid office phone no." Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtOPhone"
                                                    Display="Dynamic" ErrorMessage="Enter valid office no." SetFocusOnError="True"
                                                    ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Fax No (Country-Area-Number)
                                            </label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="txtFaxNo_country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                </div>
                                                <div class="col-md-3 DT-p-0">
                                                    <asp:TextBox ID="txtFaxNo_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtFaxNo" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvUFax_country" runat="server" ControlToValidate="txtFaxNo_country"
                                                    Display="Dynamic" ErrorMessage="Enter valid Fax country code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:CompareValidator ID="cvUFax_Area" runat="server" ControlToValidate="txtFaxNo_Area"
                                                    Display="Dynamic" ErrorMessage="Enter valid Fax area code" Operator="DataTypeCheck"
                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:CompareValidator ID="cvUFax_No" runat="server" ControlToValidate="txtFaxNo_country"
                                                    Display="Dynamic" ErrorMessage="Enter valid Fax no." Operator="DataTypeCheck"
                                                    Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtFaxNo"
                                                    Display="Dynamic" ErrorMessage="Enter valid Fax no." SetFocusOnError="True" ValidationExpression="^[0-9]{1,10}$"
                                                    ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Prefered Contact</label>
                                            <asp:DropDownList ID="ddlPref_contact" runat="server" CssClass="form-control">
                                                <asp:ListItem>Email</asp:ListItem>
                                                <asp:ListItem>Home Phone</asp:ListItem>
                                                <asp:ListItem>Office Phone</asp:ListItem>
                                                <asp:ListItem>Mobile</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Fee Sponsor/Who pays the fees</label>
                                            <asp:DropDownList ID="ddlFee_sponsor" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">Father</asp:ListItem>
                                                <asp:ListItem Value="2">Mother</asp:ListItem>
                                                <asp:ListItem Value="3">Guardian</asp:ListItem>
                                                <asp:ListItem Value="4">Father's Company</asp:ListItem>
                                                <asp:ListItem Value="5">Mother's Company</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Occupation</label>
                                            <asp:TextBox ID="txtOccup" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Company</label>
                                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Select" Value="-1" />
                                                <asp:ListItem Text="other" Value="0" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: none;" id="ddlCompany_other">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                (If you choose other,please specify the Company Name) 
                                            </label>
                                            <asp:TextBox ID="txtComp" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>Is any one parent of Student an employee with GEMS ?</label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkStaff_GEMS" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-12" id="30" style="display: none">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Staff Name</label>
                                                    <asp:TextBox ID="txtStaff_Name" runat="server" CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Sibling Fee ID</label>
                                                    <asp:TextBox ID="txtStaffID" runat="server" CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3" id="31" style="display: none">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Business unit</label>
                                                    <asp:DropDownList ID="ddlStaff_BSU" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>Is parent of Student an Ex-Student of a&nbsp; GEMS School?</label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkExStud_Gems" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-12" id="32" style="display: none">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Name</label>
                                                    <asp:TextBox ID="txtExStudName_Gems" runat="server" CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Academic Year</label>
                                                    <asp:DropDownList ID="ddlExYear" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3" id="33" style="display: none">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        GEMS School</label>
                                                    <asp:DropDownList ID="ddlExStud_Gems" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Other Details
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                How did you hear about
                                    <asp:Literal ID="ltAboutUs" runat="server"></asp:Literal>
                                            </label>
                                            <div class="checkbox-list form-control">
                                                    <div class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBoxList ID="chkAboutUs" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                </asp:CheckBoxList>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">Are there any family circumstances of which you feel we should be aware of ? Check as appropriate</label>
                                            <div class="RB_Holder">
                                                <label class="checkbox-inline checkbox checkbox-success">
                                                    <asp:CheckBox ID="chkFam_Par_sep" runat="server" Text="Parents separated" />
                                                </label>
                                                <label class="checkbox-inline checkbox checkbox-success">
                                                    <asp:CheckBox ID="chkFam_Par_div" runat="server" Text="Parents divorced" />
                                                </label>
                                                    <label class="checkbox-inline checkbox checkbox-success">
                                                    <asp:CheckBox ID="chkFam_Fath_Des" runat="server" Text="Father deceased" />
                                                </label>
                                                   <label class="checkbox-inline checkbox checkbox-success">
                                                    <asp:CheckBox ID="chkFam_Moth_Des" runat="server" Text="Mother deceased" />
                                                </label>
                                                     <label class="checkbox-inline checkbox checkbox-success">
                                                    <asp:CheckBox ID="chkFam_Oth" runat="server" Text="Others" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="chkFam_Oth_Others" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                if choosen others please give detail</label>
                                            <asp:TextBox ID="txtFamily_NOTE" runat="server" Rows="2"
                                                SkinID="MultiText" TabIndex="22" TextMode="MultiLine" CssClass="form-control"
                                                MaxLength="255"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If separated or divorced, who has legal custody ? (proof of legal 
                                                                                                 custody/guardianship must be submitted to the school <u>prior</u>
                                                to the first day of attendance)</label>
                                            <asp:TextBox ID="txtStu_Leg" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Who will the student be living with ?</label>
                                            <asp:TextBox ID="txtStu_Living" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                        </div>
                        <div>
                            <asp:Panel ID="pnlCurrent" runat="server" Width="100%">
                                  <div class=" title-bg-Form-small">
                                  <span  id="currPre_SchTitle" runat="server"></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="trSch1" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Name</label>
                                                    <asp:DropDownList ID="ddlPreSchool_Nursery" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Registration No</label>
                                                    <asp:TextBox ID="txtRegNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6" id="trSch11" runat="server">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                School Detail Type</label>
                                            <asp:Label ID="ltSchoolType" runat="server" CssClass="form-control"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="trSch2" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Name</label>
                                                    <asp:DropDownList ID="ddlGemsGr" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Select" Value="-1" />
                                                        <asp:ListItem Text="other" Value="0" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="ddlGemsGr_other" style="display: none;">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        (If you choose other,please specify the School Name)</label>
                                                    <asp:TextBox ID="txtSch_other" runat="server" MaxLength="250" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6" id="trSch3" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Name of the head teacher</label>
                                                    <asp:TextBox ID="txtSchool_head" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Student Id</label>
                                                    <asp:TextBox ID="txtFeeID_GEMS" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="trSch4" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Grade</label>
                                                    <asp:DropDownList ID="ddlPre_Grade" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Curriculum</label>
                                                    <asp:DropDownList ID="ddlPre_Curriculum" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="trSch5" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Language of Instruction</label>
                                                    <asp:TextBox ID="txtLang_Instr" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Address</label>
                                                    <asp:TextBox ID="txtSchAddr" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="trSch6" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        City</label>
                                                    <asp:TextBox ID="txtSchCity" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Country</label>
                                                    <asp:DropDownList ID="ddlPre_Country" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8" id="trSch7" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Phone (Country-Area-Number)
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <asp:TextBox ID="txtSCHPhone_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 DT-p-0">
                                                            <asp:TextBox ID="txtSCHPhone_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtSCHPhone_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Fax (Country-Area-Number)
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <asp:TextBox ID="txtSCHFax_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 DT-p-0">
                                                            <asp:TextBox ID="txtSCHFax_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtSCHFax_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="trSch8" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        From Date
                                                    </label>
                                                    <div class="Calendar_Holder">
                                                        <asp:TextBox ID="txtSchFrom_dt" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                        <asp:ImageButton ID="imgSchFrom_dt" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </div>
                                                    <ajaxToolkit:CalendarExtender ID="SchFrom_dt_CalendarExtender" runat="server"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgSchFrom_dt"
                                                        TargetControlID="txtSchFrom_dt">
                                                    </ajaxToolkit:CalendarExtender>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        To Date
                                                    </label>
                                                    <div class="Calendar_Holder">
                                                        <asp:TextBox ID="txtSchTo_dt" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                        <asp:ImageButton ID="imgtxtSchTo_dt" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </div>
                                                    <ajaxToolkit:CalendarExtender ID="txtSchTo_dt_CalendarExtender" runat="server"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgtxtSchTo_dt"
                                                        TargetControlID="txtSchTo_dt">
                                                    </ajaxToolkit:CalendarExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center" id="trSch9" runat="server">
                                        <asp:Button ID="btnGridAdd" runat="server" CssClass="button"
                                            Text="Add" />
                                        <asp:Button ID="btnGridUpdate" runat="server" CssClass="button"
                                            Text="Update" />
                                    </div>
                                    <div class="col-md-12" id="trSch10" runat="server">
                                        <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False"
                                            DataKeyNames="ID" EmptyDataText="No record added yet"
                                            EnableModelValidation="True" CssClass="table table-bordered table-row"
                                            Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="School Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_NAME" runat="server" Text='<%# Bind("SCH_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Head Teacher">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_HEAD" runat="server" Text='<%# Bind("SCH_HEAD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_GRADE" runat="server" Text='<%# BIND("SCH_GRADE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_FROMDT" runat="server" Text='<%# bind("SCH_FROMDT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSchName" runat="server" Text='<%# bind("SCH_TODT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDt" runat="server" Text='<%# bind("SCH_TODT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="EditBtn" runat="server"
                                                            CommandArgument='<%# Eval("id") %>' CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Khaki" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </asp:Panel>

                        </div>
                        <div>
                            <asp:Panel ID="pnlTrans" runat="server" Width="100%">
                                <div class="row">
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label class="Ftitle "><strong>Transport Details </strong></label>
                                            <label>Transport Required :  </label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkTran_Req" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Main Location</label>
                                            <asp:DropDownList ID="ddlMainLocation" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Sub Location</label>
                                            <asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Pick Up Point</label>
                                            <asp:DropDownList ID="ddlPickup" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                (If your pick up point is unavailable in the list please select the nearest pick
                                            up point and provide details in the box below)</label>
                                            <asp:TextBox ID="txtOthers" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <asp:Panel ID="pnlOther" runat="server" Width="100%">
                                <div class=" title-bg-Form-small">
                                    Health
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Health Card No / Medical Insurance No</label>
                                            <asp:TextBox ID="txtHthNo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Blood Group</label>
                                            <asp:DropDownList ID="ddlBgroup" runat="server" CssClass="form-control">
                                                <asp:ListItem>--</asp:ListItem>
                                                <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                                <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                                <asp:ListItem Value="B+">B+</asp:ListItem>
                                                <asp:ListItem Value="A+">A+</asp:ListItem>
                                                <asp:ListItem Value="B-">B-</asp:ListItem>
                                                <asp:ListItem Value="A-">A-</asp:ListItem>
                                                <asp:ListItem Value="O+">O+</asp:ListItem>
                                                <asp:ListItem Value="O-">O-</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Health Restriction
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Allergies</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthAll_Yes" runat="server" GroupName="Allerg" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthAll_No" runat="server" GroupName="Allerg" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthAll_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Disabled ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthDisabled_YES" runat="server" GroupName="HthDisabled" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthDisabled_No" runat="server" GroupName="HthDisabled" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthDisabled_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Special Medication</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSM_Yes" runat="server" GroupName="SPMed" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSM_No" runat="server" GroupName="SPMed" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthSM_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Physical Education Restrictions</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthPER_Yes" runat="server" GroupName="PhyEd" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthPER_No" runat="server" GroupName="PhyEd" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthPER_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Any other information related to health issue the school should be aware of?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthOther_yes" runat="server" GroupName="HthOther" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthOther_No" runat="server" GroupName="HthOther" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthOth_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Applicant's disciplinary, social,physical or psychological detail
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has the child received any sort of learning support or theraphy?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthLS_Yes" runat="server" GroupName="LearnSp" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthLS_No" runat="server" GroupName="LearnSp" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthLS_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Does the child require any special education needs?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSE_Yes" runat="server" GroupName="SPneed" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSE_No" runat="server" GroupName="SPneed" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthSE_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Does the student require English support as Additional Language program (EAL) ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEAL_Yes" runat="server" GroupName="EAL" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEAL_No" runat="server" GroupName="EAL" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthEAL_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child ever repeated/failed a grade ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbRepGRD_Yes" runat="server" GroupName="RepGRD" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbRepGRD_No" runat="server" GroupName="RepGRD" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        If Yes, which grade ?</label>
                                                    <asp:TextBox ID="txtRep_GRD" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child's behaviour been any cause for concern in previous schools ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthBehv_Yes" runat="server" GroupName="Behv" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthBehv_No" runat="server" GroupName="Behv" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        If yes, please explain and include the name of the school and principal</label>
                                                    <asp:TextBox ID="txtHthBehv_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Any information related to communication & interaction that the school should be aware of ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthComm_Yes" runat="server" GroupName="CommInt" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthComm_No" runat="server" GroupName="CommInt" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthCommInt_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Gifted and talented
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child ever been selected for specific enrichment activities?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEnr_Yes" runat="server" GroupName="Enr" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEnr_No" runat="server" GroupName="Enr" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthEnr_note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Is your child musically proficient?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthMus_Yes" runat="server" GroupName="Music" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthMus_No" runat="server" GroupName="Music" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthMus_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child represented a school or country in sport?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSport_Yes" runat="server" GroupName="Sport" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSport_No" runat="server" GroupName="Sport" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthSport_note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Other Details
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>The application info provided by : </label>
                                            <div class="RB_Holder">
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rbParentF" runat="server" AutoPostBack="True" GroupName="Filled" Text="Parent" />
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rbRelocAgent" runat="server" AutoPostBack="True" GroupName="Filled" Text="Relocation Agent" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="tbReloc" runat="server">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Agent Name</label>
                                                    <asp:TextBox ID="txtAgentName" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAgentName"
                                                        Display="Dynamic" ErrorMessage="Agent Name required" ValidationGroup="Prim_cont" Visible="false">*</asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Mobile (Country-Area-Number)
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <asp:TextBox ID="txtMAgent_country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 DT-p-0">
                                                            <asp:TextBox ID="txtMAgent_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtMAgent_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                        </div>
                                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtMAgent_country"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile country code" Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                        <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtMAgent_Area"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile area code" Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                        <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtMAgent_No"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile no." Operator="DataTypeCheck"
                                                            Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator55" runat="server" ControlToValidate="txtMAgent_No"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile no." SetFocusOnError="True"
                                                            ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkEAdd" runat="server" Text="Subscribe to Newsletters and promotional mails."></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chksms" runat="server" Text="GEMS &nbsp;may use the above Mobile Number to send text messages and alerts."></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkPubl" runat="server" Text="GEMS has permission to include child in publication/promotion photos and videos"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </asp:Panel>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 text-center mt-3">
                     <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add" CausesValidation="False" />
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-success btn-sm" Text="Save"
                                ValidationGroup="groupM1" OnClick="btnSave_Click" />
                            <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="False"
                                     CssClass="btn btn-outline-success btn-sm" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
                 <div class="col-md-12"> 
                     <input id="h_Grade_KG1" runat="server" type="hidden" value="1" />
                            <input id="hd_More_App" type="hidden" runat="server" />
                 <asp:Button Style="display: none" ID="btnTarget" runat="server" Text="Button"></asp:Button>
               </div>
                <div id="plPrev" runat="server" style="display: none; overflow: visible;">
                    <div class="msg_header">
                        <div class="msg" align="center">
                            Confirm
                        </div>
                    </div>

                    <asp:Panel ID="plStudAtt" runat="server" Width="281px" CssClass="panel-cover" >
                        <table>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblErr" runat="server" EnableViewState="False"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table align="center" width="100%">
                            <tr style="padding: 1px;">
                                <td   align="left">
                                    <img src="../Images/alert.png" style="left: 6px; position: relative; top: 10px;" />
                                    <asp:Label ID="lblText" runat="server" Style="position: relative; top: 10px; text-align: left; padding: 4px; left: 2px;"
                                        > Would like to continue with the same Primary Contact info for the next applicant? </asp:Label></td>

                            </tr>

                        </table>
                        <table style="width: 100%;" align="center">
                            <tr>
                                <td ></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnYes2" runat="server" CssClass="button" OnClick="btnYes2_Click"
                                        Text="Yes" />
                                    <asp:Button ID="btnNo2" runat="server" CssClass="button" OnClick="btnNo2_Click" Text="No"
                                        />
                                    <asp:Button ID="btnYes" runat="server" OnClick="btnYes_Click" Text="Yes" CssClass="button"  />&nbsp;<asp:Button
                                        ID="btnNo" runat="server" CssClass="button" Text="No"  OnClick="btnNo_Click" />
                                </td>
                            </tr>                           
                        </table>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    <script type="text/javascript">



        function fill_Sib() {


            if (document.getElementById('<%=chkSibling.ClientID %>').checked == true) {
                styleObj1 = document.getElementById(28).style;
                styleObj2 = document.getElementById(29).style;
                styleObj1.display = '';
                styleObj2.display = '';
            }
            else {

                styleObj1 = document.getElementById(28).style;
                styleObj2 = document.getElementById(29).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }

        function fill_Staff() {


            if (document.getElementById('<%=chkStaff_GEMS.ClientID %>').checked == true) {
                styleObj1 = document.getElementById(30).style;
                styleObj2 = document.getElementById(31).style;
                styleObj1.display = '';
                styleObj2.display = '';
            }
            else {

                styleObj1 = document.getElementById(30).style;
                styleObj2 = document.getElementById(31).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }



        function fill_Stud() {
            if (document.getElementById('<%=chkExStud_Gems.ClientID %>').checked == true) {
                styleObj1 = document.getElementById(32).style;
                styleObj2 = document.getElementById(33).style;
                styleObj1.display = '';
                styleObj2.display = '';
            }
            else {

                styleObj1 = document.getElementById(32).style;
                styleObj2 = document.getElementById(33).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }

        function fillPassport() {
            var passIssu = document.getElementById('<%=txtPassIss_date.ClientID %>')

            if (passIssu.value != "") {
                if (document.getElementById('<%=txtPassExp_Date.ClientID %>').value == "") {
                    document.getElementById('<%=txtPassExp_Date.ClientID %>').value = AddYears(passIssu.value, 5)
                }
                else {
                    document.getElementById('<%=txtPassExp_Date.ClientID %>').value = '';
                    document.getElementById('<%=txtPassExp_Date.ClientID %>').value = AddYears(passIssu.value, 5)
                }
            }



        }

        function fillVisa() {
            var VisaIssu = document.getElementById('<%=txtVisaIss_date.ClientID %>')


            if (VisaIssu.value != "") {
                if (document.getElementById('<%=txtVisaExp_date.ClientID %>').value == "") {
                    document.getElementById('<%=txtVisaExp_date.ClientID %>').value = AddYears(VisaIssu.value, 3)
                }
                else {
                    document.getElementById('<%=txtVisaExp_date.ClientID %>').value = '';
                    document.getElementById('<%=txtVisaExp_date.ClientID %>').value = AddYears(passIssu.value, 3)
                }
            }

        }

        function AddYears(dtStr, num) {
            var dtCh = "/";
            var pos1 = dtStr.indexOf(dtCh)
            var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
            var strMonth = dtStr.substring(pos1 + 1, pos2)
            var tempMonth = dtStr.substring(pos1 + 1, pos2)
            var tempValid = false;
            var strDay = dtStr.substring(0, pos1)
            var strYear = dtStr.substring(pos2 + 1)
            var monthname = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

            if (isNaN(strMonth)) {
                for (var i = 0; i < monthname.length; i++) {
                    if (monthname[i].toLowerCase() == strMonth.toLowerCase()) {
                        strMonth = i + 1;
                        strMonth = strMonth + "";
                        tempValid = true;
                    }
                }
            }
            else if ((parseInt(strMonth) >= 1) && (parseInt(strMonth) <= 12)) {
                tempValid = true;
            }

            if (tempValid == false) {
                return '';
            }


            if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
            if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)


            strMonth = strMonth - 1;

            var myDate = new Date()
            myDate.setFullYear(strYear, strMonth, strDay)
            myDate.setDate(myDate.getDate() - 1)
            myDate.setFullYear(myDate.getFullYear() + num)


            var lstrDay = myDate.getDate()
            var lstrMonth = myDate.getMonth()
            lstrMonth = parseInt(lstrMonth) + 1


            if (parseInt(lstrDay) < 10) {
                lstrDay = "0" + lstrDay
            }


            if (isNaN(tempMonth)) {
                lstrMonth = monthname[lstrMonth - 1]
            }
            else {
                if (parseInt(lstrMonth) < 10) {
                    lstrMonth = "0" + lstrMonth
                }
            }

            return (lstrDay + "/" + lstrMonth + "/" + myDate.getFullYear())
        }

        function GetSelectedItem() {
            var e = document.getElementById("<%= ddlFLang.ClientID%>");
            var fLang = e.options[e.selectedIndex].text;
            var CHK = document.getElementById("<%= chkOLang.ClientID%>");

            var checkbox = CHK.getElementsByTagName("input");

            var label = CHK.getElementsByTagName("label");

            for (var i = 0; i < checkbox.length; i++) {

                if (checkbox[i].checked) {
                    var olang = label[i].innerHTML
                    if (fLang == olang) {
                        checkbox[i].checked = false;
                    }
                }

            }

        }




        function chkFirst_lang() {

            GetSelectedItem();

        }


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

    </script>
    <script>
        $(document).ready(function () {
            $('.Calendar_Holder').on('click', function () {
                var id = $(this).find("input[type=image]").attr('id');
                document.getElementById(id).click()
            });
        });

        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.Calendar_Holder').on('click', function () {
                        var id = $(this).find("input[type=image]").attr('id');
                        document.getElementById(id).click()
                    });
                }
            });
        };
    </script>

    <script>

        function Runfunction() {
            $('#' +'<%=ddlEthnicity.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlEthnicity_Other").show();
                } else {
                    $("#ddlEthnicity_Other").hide();
                }

            });

            $('#' +'<%=chkRef.ClientID %>').change(function () {
                if (this.checked) {
                    $("#referred_to_GEMS").show();
                } else {
                    $("#referred_to_GEMS").hide();
                }

            });
            $('#' +'<%=ddlCompany.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlCompany_other").show();
                } else {
                    $("#ddlCompany_other").hide();
                }

            });
            //ctl00_cphMasterpage_chkFam_Oth
            $('#' +'<%=chkFam_Oth.ClientID %>').change(function () {
                if (this.checked) {
                    $("#chkFam_Oth_Others").show();
                } else {
                    $("#chkFam_Oth_Others").hide();
                }

            });
            $('#' + '<%=ddlGemsGr.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlGemsGr_other").show();
                } else {
                    $("#ddlGemsGr_other").hide();
                }

            });
            $("#ctl00_cphMasterpage_chkSibling").change(function () {
                fill_Sib();

            });
            $("#ctl00_cphMasterpage_chkStaff_GEMS").change(function () {
                fill_Staff();

            });
            $("#ctl00_cphMasterpage_chkExStud_Gems").change(function () {
                fill_Stud();

            });
            $("#ctl00_cphMasterpage_txtPassIss_date").change(function () {
                fillPassport();

            });
            $("#ctl00_cphMasterpage_txtVisaIss_date").change(function () {
                fillVisa();

            });
            $("#ctl00_cphMasterpage_txtPassExp_Date").change(function () {
                fillPassport();

            });
            $("#ctl00_cphMasterpage_txtVisaExp_date").change(function () {
                fillVisa();

            });
            $("#ctl00_cphMasterpage_chkOLang").change(function () {
                GetSelectedItem();

            });
            $("#ctl00_cphMasterpage_ddlFLang").change(function () {
                chkFirst_lang();

            });

        }
        Runfunction();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    Runfunction();

                }
            });
        };
    </script>
    <script type="text/javascript">
        function ResponsiveTabs() {
            $('#parentHorizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                },
            });
        }
        $(document).ready(function () {
            //Horizontal Tab
            ResponsiveTabs();

        });
          //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    ResponsiveTabs();
                }
            });
        };
</script>

</asp:Content>

