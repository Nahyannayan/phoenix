﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports UtilityObj
Imports System
Imports System.Text.RegularExpressions
Imports UserControls_usrMessageBar
Partial Class Students_studEnqForm_Reg_New
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Public Popup_Message As String = ""
    Public Popup_Message_Status As WarningType

    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)

        Try
            'mvMaster.ActiveViewIndex = Int32.Parse(e.Item.Value)

            Dim i As Integer = Int32.Parse(e.Item.Value)


            'mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain.jpg"
            'mnuMaster.Items(1).ImageUrl = "~/Images/Tab_studEdit/btnPassport.jpg"
            'mnuMaster.Items(2).ImageUrl = "~/Images/Tab_studEdit/btnContact.jpg"
            'mnuMaster.Items(3).ImageUrl = "~/Images/Tab_studEdit/btnCurr1.jpg"
            'mnuMaster.Items(4).ImageUrl = "~/Images/Tab_studEdit/btnTransport.jpg"
            'mnuMaster.Items(5).ImageUrl = "~/Images/Tab_studEdit/btnOther.jpg"

            Select Case i
                Case 0
                    'mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain2.jpg"

                Case 1
                    'mnuMaster.Items(1).ImageUrl = "~/Images/Tab_studEdit/btnPassport2.jpg"

                Case 2
                    'mnuMaster.Items(2).ImageUrl = "~/Images/Tab_studEdit/btnContact2.jpg"
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                          "<script language=javascript>fill_Sib();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                           "<script language=javascript>fill_Staff();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                           "<script language=javascript>fill_Stud();</script>")

                Case 3
                    'mnuMaster.Items(3).ImageUrl = "~/Images/Tab_studEdit/btnCurr2.jpg"
                    ''Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                    '                       "<script language=javascript>Sel_Grade_KG1();</script>")

                Case 4
                    'mnuMaster.Items(4).ImageUrl = "~/Images/Tab_studEdit/btnTransport2.jpg"

                Case 5
                    'mnuMaster.Items(5).ImageUrl = "~/Images/Tab_studEdit/btnOther2.jpg"

            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuMaster_MenuItemClick")
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100067") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    check_Visibility()
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    If Session("sBsuid") = "600001" Then
                        trStm.Visible = False
                        trtrm.Visible = False
                        trtjd.Visible = False
                    Else
                        trStm.Visible = True
                        trtrm.Visible = True
                        trtjd.Visible = True
                    End If


                    chkSibling.Attributes.Add("onclick", "fill_Sib()")
                    chkStaff_GEMS.Attributes.Add("onclick", "fill_Staff()")
                    chkExStud_Gems.Attributes.Add("onclick", "fill_Stud()")
                    txtPassIss_date.Attributes.Add("onblur", "fillPassport()")
                    txtVisaIss_date.Attributes.Add("onblur", "fillVisa()")
                    txtPassExp_Date.Attributes.Add("onfocus", "fillPassport()")
                    txtVisaExp_date.Attributes.Add("onfocus", "fillVisa()")
                    chkOLang.Attributes.Add("onclick", "GetSelectedItem()")
                    ddlFLang.Attributes.Add("onChange", "chkFirst_lang()")

                    Call add_keyPress_check()


                    'plPopUp.Visible = False
                    ''plPopUp2.Visible = False

                    'chkSibling.Attributes.Add("onclick", "fill_Sibl(this)")
                    'chkTran_Req.Attributes.Add("onclick", "fill_Tran(this)")
                    chkPubl.Checked = True
                    chksms.Checked = True
                    chkEAdd.Checked = True
                    rdPri_Father.Checked = True
                    rbParentF.Checked = True
                    tbReloc.Visible = False
                    rdMale.Checked = True
                    'rdOther.Checked = True
                    'txtPre_School.Visible = True
                    'ddlGemsGr.Visible = False
                    'rdMisc_No.Checked = True

                    rbHthAll_No.Checked = True
                    rbHthSM_No.Checked = True
                    rbHthPER_No.Checked = True
                    rbHthOther_No.Checked = True
                    rbHthLS_No.Checked = True
                    rbHthSE_No.Checked = True
                    rbHthEAL_No.Checked = True
                    rbHthMus_No.Checked = True
                    rbHthBehv_No.Checked = True
                    rbHthEnr_No.Checked = True
                    rbHthSport_No.Checked = True
                    rbMealNo.Checked = True
                    btnGridUpdate.Visible = False
                    ltSchoolType.Text = "Current School"
                    ViewState("TABLE_School_Pre") = CreateDataTable()
                    ViewState("TABLE_School_Pre").Rows.Clear()
                    ViewState("id") = 1
                    gridbind()



                    ViewState("V_Staff_Check") = 0
                    ViewState("V_Sibl_Check") = 0
                    ViewState("V_Sister_check") = 0

                    Call GEMS_BSU_4_Student()


                    ' txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    ddlGEMSSchool.ClearSelection()
                    If Not ddlGEMSSchool.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                        ddlGEMSSchool.Items.FindByValue(Session("sBsuid")).Selected = True
                    End If
                    Call callAll_fun_OnPageLoad()
                    If Not ddlGemsGr.Items.FindByText("OTHER") Is Nothing Then
                        ddlGemsGr.ClearSelection()
                        ddlGemsGr.Items.FindByText("OTHER").Selected = True
                    End If



                    ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)
                    ' Call ControlTable_row()
                    '  disable_panel()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try

        End If




    End Sub

    Sub add_keyPress_check()



        txtStud_Contact_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtStud_Contact_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtStud_Contact_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMAgent_country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMAgent_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMAgent_No.Attributes.Add("onkeypress", "return isNumberKey(event)")

        txtSCHPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHPhone_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHFax_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHFax_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHFax_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")




    End Sub


    Sub BSU_Enq_validation()

        Try

            Dim Temp_Rfv As New RequiredFieldValidator
            Dim LTSTAR As New Label
            Dim arInfo As String() = New String(2) {}
            Dim rfv As String = String.Empty
            Dim lt As String = String.Empty
            Dim splitter As Char = "|"

            Dim Enq_hash As New Hashtable
            If Not Session("BSU_Enq_Valid") Is Nothing Then
                Enq_hash = Session("BSU_Enq_Valid")
                Dim hashloop As DictionaryEntry
                For Each hashloop In Enq_hash
                    If hashloop.Value <> "" Then
                        arInfo = hashloop.Value.Split(splitter)

                        If arInfo.Length = 2 Then
                            rfv = arInfo(0)
                            lt = arInfo(1)

                        Else
                            rfv = arInfo(0)
                        End If
                        If Not Page.Master.FindControl("cphMasterpage").FindControl(rfv) Is Nothing Then
                            Temp_Rfv = Page.Master.FindControl("cphMasterpage").FindControl(rfv)
                            Temp_Rfv.EnableClientScript = True
                            Temp_Rfv.Enabled = True
                            Temp_Rfv.Visible = True
                        End If
                        If Not Page.FindControl(rfv) Is Nothing Then
                            LTSTAR = Page.FindControl(lt)
                            LTSTAR.Visible = True
                        End If

                    End If
                Next
            End If
            Enq_hash.Clear()
            Session("BSU_Enq_Valid") = Nothing

            Dim BSU_ID As String = ddlGEMSSchool.SelectedValue

            Using Enq_validation_reader As SqlDataReader = AccessStudentClass.GetEnquiry_Validation(BSU_ID, "2")
                While Enq_validation_reader.Read
                    If Enq_validation_reader.HasRows Then
                        Enq_hash.Add(Enq_validation_reader("EQV_CODE"), Enq_validation_reader("CONTROL_ID") & "|" & Enq_validation_reader("STAR_ID"))

                        If Convert.ToString(Enq_validation_reader("CONTROL_ID")) <> "" Then
                            If Not Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("CONTROL_ID")) Is Nothing Then
                                Temp_Rfv = Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("CONTROL_ID"))
                                Temp_Rfv.EnableClientScript = False
                                Temp_Rfv.Enabled = False
                                Temp_Rfv.Visible = False
                            End If
                        End If
                        If Convert.ToString(Enq_validation_reader("STAR_ID")) <> "" Then
                            If Not Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("STAR_ID")) Is Nothing Then
                                LTSTAR = Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("STAR_ID"))
                                LTSTAR.Visible = False

                            End If
                        End If
                    End If


                End While

            End Using

            Session("BSU_Enq_Valid") = Enq_hash

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub
    Function RefCodeIsvalid() As Boolean
        Dim refcode As String = txtRefCode.Text.Trim
        Dim Bsu_id As String = ddlGEMSSchool.SelectedValue
        Dim email As String = txtREFEmail.Text.Trim

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " select TOP 1 D.RFD_ID, M.RFM_CODE,S.RFS_REF_EMAIL from REF.REFERRAL_M as M inner join REF.REFERRAL_S AS S " &
 " on S.RFS_RFM_ID=M.RFM_ID INNER JOIN REF.REFERRAL_D AS D ON D.RFD_RFS_ID=S.RFS_ID " &
 " WHERE RFM_CODE='" & refcode & "' and RFS_REF_EMAIL='" & email & "' and RFD_BSU_ID='" & Bsu_id & "'"

        Using USER_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If USER_reader.HasRows Then


                While USER_reader.Read
                    ViewState("RFD_ID") = Convert.ToString(USER_reader("RFD_ID"))
                End While
                Return True

            Else
                Return False
            End If
        End Using

    End Function
    Sub AboutUs()
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim AboutUSString As String = "select MODE_ID,MODE_DESCR from ENQUIRY_MODE_M"

        Dim ds1 As New DataSet

        ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, AboutUSString)

        conn.Close()
        Dim row As DataRow
        For Each row In ds1.Tables(0).Rows
            chkAboutUs.Items.Add(New ListItem(row("MODE_DESCR"), row("MODE_ID").ToString.Trim))
        Next
    End Sub
#Region "Transaction function"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            If Final_validation() = True Then

                Dim Final_Result As Integer
                Dim Dup_count As Integer

                Dim EQM_APPLPASPRTNO As String = txtPassport.Text
                Dim EQS_ACY_ID As String = ddlAca_Year.SelectedItem.Value
                Dim EQS_BSU_ID As String = ddlGEMSSchool.SelectedItem.Value
                Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
                Dim EQS_SHF_ID As String = ddlShift.SelectedItem.Value
                Dim EQS_STM_ID As String = ddlStream.SelectedItem.Value
                Dim CLM_ID As String = ddlCurri.SelectedItem.Value
                Dim URL_Enquiry As String = String.Empty

                'modfield lijo 9 mar 09 passport no
                'If Trim(txtPassport.Text) <> "" Then
                '    Dup_count = AccessStudentClass.GetDuplicate_Enquiry_M(EQM_APPLPASPRTNO, EQS_SHF_ID, EQS_STM_ID, EQS_BSU_ID, EQS_GRD_ID, EQS_ACY_ID, CLM_ID)
                'Else
                '    Dup_count = 0
                'End If
                Dup_count = AccessStudentClass.GetDuplicate_Enquiry_ByMultipleFields(txtFname.Text, txtMname.Text, txtLname.Text, txtDob.Text, txtEmail_Pri.Text, EQS_BSU_ID, EQS_GRD_ID, CLM_ID, EQS_ACY_ID)



                If Dup_count = 0 Then


                    Final_Result = Enquiry_transaction()


                    If Final_Result = 0 Then
                        Popup_Message = "Record Inserted Successfully"
                        Popup_Message_Status = WarningType.Success

                        ViewState("datamode") = "add"
                        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        If chkEmailSend.Checked = True Then


                            Dim BSC_HOST As String = String.Empty
                            Dim BSC_USERNAME As String = String.Empty
                            Dim BSC_PASSWORD As String = String.Empty
                            Dim BSC_PORT As String = String.Empty
                            Dim BSC_TYPE As String = String.Empty
                            Dim EMAILSTATUS As String = String.Empty
                            Dim EmailFirstPart As String = String.Empty
                            Dim Domain As String = String.Empty
                            Dim MaillTemAsHtml As String = GetMaillTemAsHtml()

                            Using EmailHost_reader As SqlDataReader = AccessStudentClass.GetEMAIL_HOST(Session("TEMP_BSU_ID"))
                                If EmailHost_reader.HasRows Then

                                    While EmailHost_reader.Read

                                        BSC_HOST = Convert.ToString(EmailHost_reader("BSC_HOST"))
                                        BSC_USERNAME = Convert.ToString(EmailHost_reader("BSC_USERNAME"))
                                        BSC_PASSWORD = Convert.ToString(EmailHost_reader("BSC_PASSWORD"))
                                        BSC_PORT = Convert.ToString(EmailHost_reader("BSC_PORT"))
                                        BSC_TYPE = Convert.ToString(EmailHost_reader("BSC_TYPE"))
                                        Dim InputEmail As String = BSC_USERNAME
                                        Dim EmailElements() As String = Split(InputEmail, "@")
                                        If EmailElements.Length = 2 Then
                                            EmailFirstPart = EmailElements(0)
                                            Domain = EmailElements(1)
                                        End If

                                    End While
                                End If


                            End Using

                            EMAILSTATUS = SendRegEmails(Session("sUsr_id"), BSC_USERNAME, BSC_PASSWORD, EmailFirstPart, txtEmail_Pri.Text, "OnlineEnquiry", "Online Enquiry Acknowledgement -" & Session("TEMP_ApplNo"), BSC_HOST, BSC_PORT, MaillTemAsHtml)
                            EMAILSTATUS = SendPDFEmails(BSC_USERNAME, txtEmail_Pri.Text, "Online Enquiry Acknowledgement -" & Session("TEMP_ApplNo"), MaillTemAsHtml, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT)
                            AuditEnquiryEmail(Session("TEMP_BSU_ID"), Session("TEMP_ApplNo"), Session("Enq_ID"), EMAILSTATUS)

                            Session("chkemailSend") = False
                        End If


                        Call disable_panel()

                        btnNo2.Visible = False

                        btnYes2.Visible = False
                        btnNo.Visible = True

                        btnYes.Visible = True
                        lblText.Text = "Would like to continue with the same Primary Contact info for the next applicant? "

                        mpe.Show()
                        'btnAdd.Enabled = False
                        'btnSave.Visible = False
                        'btnCancel.Visible = False
                        'mnuMaster.Enabled = False
                    Else
                        Popup_Message = "Record could not be Inserted"
                        Popup_Message_Status = WarningType.Danger

                    End If
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "scrollTop", " $('html, body').animate({ scrollTop: 0 }, 'slow');", True)

                Else
                    'lblError.Text = "Record already exists"
                    Popup_Message = "Record already exists"
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                End If

            End If
        Catch ex As Exception
            Popup_Message = "Record could not be Inserted"
            Popup_Message_Status = WarningType.Danger
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try

    End Sub
    Private Function SendPDFEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer) As String
        Dim ReturnValue As String = ""
        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage
        Dim mailfrom As New System.Net.Mail.MailAddress(FromEmailId, "Registrar")

        Try

            msg.From = mailfrom
            msg.To.Add(ToEmailId)
            msg.Subject = Subject
            msg.Body = MailBody
            msg.Priority = Net.Mail.MailPriority.High
            msg.IsBodyHtml = True


            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)
            client = Nothing
            msg.Dispose()
            ReturnValue = "Successfully sent to " & ToEmailId
        Catch ex As Exception
            ReturnValue = "Error in Sending Mail :" & Date.Today.ToString & " " & ex.Message
            client = Nothing
            msg.Dispose()
        End Try
        Return ReturnValue
    End Function
    Private Function SendRegEmails(ByVal UserID As String, ByVal Email_From As String, ByVal Email_PASSWORD As String,
                                    ByVal Email_USERNAME As String, ByVal Email_To As String, ByVal LOG_TYPE As String, ByVal Subject As String,
                                   ByVal Host As String, ByVal Port As Integer, ByVal Message As String) As String
        Dim ReturnValue As String = ""
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim Transaction As SqlTransaction
            Transaction = connection.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@UserID", UserID)
                pParms(1) = New SqlClient.SqlParameter("@Email_From", Email_From)
                pParms(2) = New SqlClient.SqlParameter("@Email_PASSWORD", Email_PASSWORD)
                pParms(3) = New SqlClient.SqlParameter("@Email_USERNAME", Email_USERNAME)
                pParms(4) = New SqlClient.SqlParameter("@Email_To", Email_To)
                pParms(5) = New SqlClient.SqlParameter("@LOG_TYPE", LOG_TYPE)
                pParms(6) = New SqlClient.SqlParameter("@Subject", Subject)
                pParms(7) = New SqlClient.SqlParameter("@Message", Message)
                pParms(8) = New SqlClient.SqlParameter("@PORT", Port)
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "Send_mail_EnquiryReg", pParms)
                ReturnValue = "Successfully sent to " & Email_To
                Transaction.Commit()
            Catch ex As Exception
                ReturnValue = "Error in Sending Mail :" & Email_To & " " & ex.Message
                Transaction.Rollback()
            End Try
        End Using
        Return ReturnValue
    End Function
    Public Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New IO.StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml
    Public Function GetMaillTemAsHtml() As String
        If Session("TEMP_ApplNo") Is Nothing Then
            Session("TEMP_ApplNo") = "0"
        End If

        If Session("Enq_ID") Is Nothing Then
            Session("Enq_ID") = "0"
        End If

        Dim result As String = ""
        Dim temp_ID As String = String.Empty
        Dim temp_Grade As String = String.Empty
        Dim temp_AcdYear As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim WardName As String = String.Empty
        Dim ParentName As String = String.Empty
        Dim Email As String = String.Empty
        Dim REGNAME As String = String.Empty
        Dim REGMAIL As String = String.Empty
        Dim html As String = ScreenScrapeHtml(Server.MapPath(ResolveUrl("~\Students\StudEnqMailTempReg.html")))
        Using readerEnquiry_ApplAck As SqlDataReader = GetEnq_ACK(Session("TEMP_ApplNo"), Session("Enq_ID"))
            If readerEnquiry_ApplAck.HasRows = True Then
                While readerEnquiry_ApplAck.Read
                    Session("TEMP_BSU_ID") = Convert.ToString(readerEnquiry_ApplAck("BSU_ID"))
                    temp_ID = Session("TEMP_ApplNo")
                    WardName = Convert.ToString(readerEnquiry_ApplAck("APPLPASSPORTNAME"))
                    ParentName = Convert.ToString(readerEnquiry_ApplAck("FNAME"))
                    temp_AcdYear = Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR"))
                    temp_Grade = Convert.ToString(readerEnquiry_ApplAck("GRM_DISPLAY"))
                    Email = Convert.ToString(readerEnquiry_ApplAck("FEMAIL"))
                    If Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) = "2011-2012" And Session("TEMP_BSU_ID") = "114003" Then
                        BSU_Name = "GEMS American Academy - Khalifa City, Abu Dhabi "
                    ElseIf Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) = "2012-2013" And Session("TEMP_BSU_ID") = "123004" And temp_Grade = "Pre KG" Then
                        BSU_Name = "  Pre-kindergarten at the Modern’s Nursery  in " & Convert.ToString(readerEnquiry_ApplAck("BSU_NAME"))
                    Else
                        BSU_Name = Convert.ToString(readerEnquiry_ApplAck("BSU_NAME"))
                    End If
                    REGNAME = IIf(Session("TEMP_BSU_ID") = "115005", Convert.ToString(readerEnquiry_ApplAck("ENAME")), StrConv(Convert.ToString(readerEnquiry_ApplAck("ENAME")), VbStrConv.ProperCase))
                    REGMAIL = "<a href='mailto:" & Convert.ToString(readerEnquiry_ApplAck("Email")) & "?subject=Online Enquiry Acknowledgement' style='color:black;'>" & Convert.ToString(readerEnquiry_ApplAck("Email")) & "</a><div>" & Convert.ToString(readerEnquiry_ApplAck("REG_INFO")) & "</div>"

                End While
            End If
        End Using
        If Trim(WardName) <> "" Then
            html = html.Replace("@WNAME", UCase(WardName))
        Else
            html = html.Replace("@WNAME", "")
        End If
        If Trim(ParentName) <> "" Then
            html = html.Replace("@PNAME", UCase(ParentName))
        Else
            html = html.Replace("@PNAME", "")
        End If
        If Trim(Email) <> "" Then
            html = html.Replace("@EMAIL", Email)
        Else
            html = html.Replace("@EMAIL", "")
        End If
        html = html.Replace("@CLASS", UCase(temp_Grade))
        html = html.Replace("@SchoolName", UCase(BSU_Name))
        html = html.Replace("@EnquiryNumber", UCase(temp_ID))
        html = html.Replace("@ACYEAR", UCase(temp_AcdYear))
        html = html.Replace("@REGNAME", UCase(REGNAME))
        html = html.Replace("@REGMAIL", UCase(REGMAIL))
        result = html
        Return result
    End Function 'ScreenScrapeHtml
    Private Function GetEnq_ACK(ByVal EQS_APPLNO As String, ByVal EQS_EQM_ENQID As String) As SqlDataReader

        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_APPLNO", EQS_APPLNO)
        pParms(1) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQS_EQM_ENQID)

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ENQ.GETENQUIRY_ACK", pParms)

        SqlConnection.ClearPool(conn)
        Return reader
    End Function
    Public Sub AuditEnquiryEmail(ByVal BSU_ID As String, ByVal ApplNo As String, ByVal EqmEnqId As String,
                                          ByVal EmailStatus As String)
        Try

            Dim conn As String = ConnectionManger.GetOASISAuditConnectionString
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", BSU_ID)
            param(1) = New SqlParameter("@ApplNo", ApplNo)
            param(2) = New SqlParameter("@EqmEnqId", EqmEnqId)
            param(3) = New SqlParameter("@EmailStatus", EmailStatus)

            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "ENQ.SAVEOnlineEnquiryEmailLog", param)

        Catch ex As Exception

        End Try

    End Sub
    Sub ClearPrev()
        txtSch_other.Text = ""
        txtSchool_head.Text = ""
        txtFeeID_GEMS.Text = ""
        If Not ddlGemsGr.Items.FindByText("OTHER") Is Nothing Then
            ddlGemsGr.ClearSelection()
            ddlGemsGr.Items.FindByText("OTHER").Selected = True
        End If
        If Not ddlPre_Grade.Items.FindByText(GetPrevGrade()) Is Nothing Then
            ddlPre_Grade.ClearSelection()
            ddlPre_Grade.Items.FindByText(GetPrevGrade()).Selected = True
        End If
        ddlPre_Country.ClearSelection()
        ddlPre_Country.Items.FindByValue("172").Selected = True
        If Not ddlPre_Curriculum.Items.FindByText("--") Is Nothing Then
            ddlPre_Curriculum.ClearSelection()
            ddlPre_Curriculum.Items.FindByText("--").Selected = True
        End If

        txtLang_Instr.Text = ""
        txtSchCity.Text = ""
        txtSCHPhone_Country.Text = ""
        txtSCHPhone_Area.Text = ""
        txtSCHPhone_No.Text = ""
        txtSCHFax_Country.Text = ""
        txtSCHFax_Area.Text = ""
        txtSCHFax_No.Text = ""
        txtSchFrom_dt.Text = ""
        txtSchTo_dt.Text = ""
        txtSchAddr.Text = ""



        txtPassExp_Date.Text = ""
        txtVisaNo.Text = ""
        txtIss_Place.Text = ""
        txtVisaIss_date.Text = ""
        txtVisaExp_date.Text = ""
        txtIss_Auth.Text = ""
        txtRegNo.Text = ""
        txtSch_other.Text = ""
        txtSchool_head.Text = ""
        txtFeeID_GEMS.Text = ""
        txtLang_Instr.Text = ""
        txtSchAddr.Text = ""
        txtSchCity.Text = ""
        ddlGemsGr.ClearSelection()
        ddlGemsGr.SelectedIndex = 0
        ddlPreSchool_Nursery.ClearSelection()
        ddlPreSchool_Nursery.SelectedIndex = 0
        txtSchFrom_dt.Text = ""
        txtSchTo_dt.Text = ""

        txtHthNo.Text = ""
        ddlBgroup.ClearSelection()
        ddlBgroup.SelectedIndex = 0
        txtHthAll_Note.Text = ""
        txtHthSM_Note.Text = ""
        txtHthPER_Note.Text = ""
        txtHthOth_Note.Text = ""
        txtHthLS_Note.Text = ""
        txtHthSE_Note.Text = ""
        txtHthEAL_Note.Text = ""
        txtHthBehv_Note.Text = ""
        txtHthEnr_note.Text = ""
        txtHthMus_Note.Text = ""
        txtHthSport_note.Text = ""
        rbHthSport_No.Checked = True
        rbHthMus_No.Checked = True
        rbHthEnr_No.Checked = True
        rbHthBehv_No.Checked = True
        rbHthEAL_No.Checked = True
        rbHthSE_No.Checked = True
        rbHthLS_No.Checked = True
        rbHthOther_No.Checked = True
        rbHthPER_No.Checked = True
        rbHthSM_No.Checked = True
        rbHthAll_No.Checked = True
        rbWGood.Checked = False
        rbWExc.Checked = False
        rbWFair.Checked = False
        rbWPoor.Checked = False

        rbSGood.Checked = False
        rbSExc.Checked = False
        rbSFair.Checked = False
        rbSPoor.Checked = False
        rbRGood.Checked = False
        rbRExc.Checked = False
        rbRFair.Checked = False
        rbRPoor.Checked = False
        ' ViewState("TABLE_School_Pre") = CreateDataTable()
        ViewState("TABLE_School_Pre").Rows.Clear()
        ViewState("id") = 1
        gridbind()
    End Sub


    Sub clearAll()
        txtSch_other.Text = ""
        txtSchool_head.Text = ""
        txtFeeID_GEMS.Text = ""
        If Not ddlGemsGr.Items.FindByText("OTHER") Is Nothing Then
            ddlGemsGr.ClearSelection()
            ddlGemsGr.Items.FindByText("OTHER").Selected = True
        End If

        ddlGEMSSchool.ClearSelection()
        If Not ddlGEMSSchool.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlGEMSSchool.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
        ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)

        txtFname.Text = ""
        txtMname.Text = ""
        txtLname.Text = ""
        rdMale.Checked = True
        ddlReligion.ClearSelection()
        ddlReligion.Items.FindByText("")
        txtDob.Text = ""
        txtPob.Text = ""

        ddlCountry.ClearSelection()
        ddlNational.ClearSelection()
        ddlCountry.Items.FindByText("")
        ddlNational.Items.FindByText("")

        txtPassport.Text = ""
        txtStud_Contact_Country.Text = ""
        txtStud_Contact_Area.Text = ""
        txtStud_Contact_No.Text = ""
        txtM_Country.Text = ""
        txtM_Area.Text = ""
        txtMobile_Pri.Text = ""
        txtEmail_Pri.Text = ""
        'txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtPassportIssue.Text = ""
        txtPassportIssue.Text = ""
        txtPassExp_Date.Text = ""
        txtVisaNo.Text = ""
        txtIss_Place.Text = ""
        txtVisaIss_date.Text = ""
        txtVisaExp_date.Text = ""
        txtIss_Auth.Text = ""
        chkSibling.Checked = False

        txtSib_Name.Text = ""
        txtSib_ID.Text = ""
        rdPri_Father.Checked = True
        ddlCont_Status.ClearSelection()
        ddlCont_Status.SelectedIndex = 0
        txtPri_Fname.Text = ""
        txtPri_Mname.Text = ""
        txtPri_Lname.Text = ""
        ddlPri_National1.ClearSelection()
        ddlPri_National2.ClearSelection()
        ddlPri_National1.Items.FindByText("")
        ddlPri_National2.Items.FindByText("")
        txtAdd1_overSea.Text = ""
        txtAdd2_overSea.Text = ""
        txtOverSeas_Add_City.Text = ""
        ddlOverSeas_Add_Country.Text = ""
        txtPhone_Oversea_Country.Text = ""
        txtPhone_Oversea_Area.Text = ""
        txtPhone_Oversea_No.Text = ""
        txtPoBox_Pri.Text = ""
        txtApartNo.Text = ""
        txtBldg.Text = ""
        txtStreet.Text = ""
        txtArea.Text = ""
        txtCity_pri.Text = ""

        ddlCountry_Pri.ClearSelection()
        ddlCountry_Pri.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True
        txtPoboxLocal.Text = ""
        ddlEmirate.ClearSelection()
        ddlEmirate.SelectedIndex = 0
        txtHPhone_Country.Text = ""
        txtHPhone_Area.Text = ""
        txtHPhone.Text = ""
        txtOPhone_Country.Text = ""
        txtOPhone_Area.Text = ""
        txtOPhone.Text = ""
        txtFaxNo_country.Text = ""
        txtFaxNo_Area.Text = ""
        txtFaxNo.Text = ""
        ddlPref_contact.ClearSelection()
        ddlPref_contact.SelectedIndex = 0
        txtOccup.Text = ""
        ddlCompany.ClearSelection()
        ddlCompany.SelectedIndex = 0
        txtComp.Text = ""
        chkStaff_GEMS.Checked = False
        txtStaffID.Text = ""
        txtStaff_Name.Text = ""
        chkExStud_Gems.Checked = False
        txtExStudName_Gems.Text = ""
        ddlExYear.ClearSelection()
        ddlExYear.Items.FindByText(Now.Year)
        ddlExStud_Gems.ClearSelection()
        chkAboutUs.ClearSelection()
        txtRegNo.Text = ""

        chkTran_Req.Checked = False
        ddlMainLocation.ClearSelection()
        ddlMainLocation.SelectedIndex = 0
        txtOthers.Text = ""
        rbParentF.Checked = True
        chkPrint.Checked = False
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True
        chkOLang.ClearSelection()
        chkRef.Checked = False
        txtRefCode.Text = ""
        txtREFEmail.Text = ""

        txtPassExp_Date.Text = ""
        txtVisaNo.Text = ""
        txtIss_Place.Text = ""
        txtVisaIss_date.Text = ""
        txtVisaExp_date.Text = ""
        txtIss_Auth.Text = ""
        txtRegNo.Text = ""
        txtSch_other.Text = ""
        txtSchool_head.Text = ""
        txtFeeID_GEMS.Text = ""
        txtLang_Instr.Text = ""
        txtSchAddr.Text = ""
        txtSchCity.Text = ""
        ddlGemsGr.ClearSelection()
        ddlGemsGr.SelectedIndex = 0
        ddlPreSchool_Nursery.ClearSelection()
        ddlPreSchool_Nursery.SelectedIndex = 0
        txtSchFrom_dt.Text = ""
        txtSchTo_dt.Text = ""

        txtHthNo.Text = ""
        ddlBgroup.ClearSelection()
        ddlBgroup.SelectedIndex = 0
        txtHthAll_Note.Text = ""
        txtHthSM_Note.Text = ""
        txtHthPER_Note.Text = ""
        txtHthOth_Note.Text = ""
        txtHthLS_Note.Text = ""
        txtHthSE_Note.Text = ""
        txtHthEAL_Note.Text = ""
        txtHthBehv_Note.Text = ""
        txtHthEnr_note.Text = ""
        txtHthMus_Note.Text = ""
        txtHthSport_note.Text = ""
        rbHthSport_No.Checked = True
        rbHthMus_No.Checked = True
        rbHthEnr_No.Checked = True
        rbHthBehv_No.Checked = True
        rbHthEAL_No.Checked = True
        rbHthSE_No.Checked = True
        rbHthLS_No.Checked = True
        rbHthOther_No.Checked = True
        rbHthPER_No.Checked = True
        rbHthSM_No.Checked = True
        rbHthAll_No.Checked = True
        rbWGood.Checked = False
        rbWExc.Checked = False
        rbWFair.Checked = False
        rbWPoor.Checked = False

        rbSGood.Checked = False
        rbSExc.Checked = False
        rbSFair.Checked = False
        rbSPoor.Checked = False
        rbRGood.Checked = False
        rbRExc.Checked = False
        rbRFair.Checked = False
        rbRPoor.Checked = False
        ' ViewState("TABLE_School_Pre") = CreateDataTable()
        ViewState("TABLE_School_Pre").Rows.Clear()
        ViewState("id") = 1
        gridbind()

    End Sub
    Sub ClearApplic()
        Try


            'ddlGEMSSchool.ClearSelection()
            'ddlGEMSSchool.Items.FindByValue(Session("sBsuid")).Selected = True
            'ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)

            txtFname.Text = ""
            txtMname.Text = ""
            txtLname.Text = ""
            rdMale.Checked = True
            ' ddlReligion.ClearSelection()
            ' ddlReligion.Items.FindByText("")
            txtDob.Text = ""
            txtPob.Text = ""

            ddlCountry.ClearSelection()
            ddlNational.ClearSelection()
            ' ddlCountry.Items.FindByText("")
            ' ddlNational.Items.FindByText("")

            txtPassport.Text = ""
            'txtStud_Contact_Country.Text = ""
            'txtStud_Contact_Area.Text = ""
            'txtStud_Contact_No.Text = ""
            'txtM_Country.Text = ""
            'txtM_Area.Text = ""
            'txtMobile_Pri.Text = ""
            'txtEmail_Pri.Text = ""
            ' txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            txtPassportIssue.Text = ""
            txtPassportIssue.Text = ""
            txtPassIss_date.Text = ""

            txtPassExp_Date.Text = ""
            txtVisaNo.Text = ""
            txtIss_Place.Text = ""
            txtVisaIss_date.Text = ""
            txtVisaExp_date.Text = ""
            txtIss_Auth.Text = ""
            txtRegNo.Text = ""
            txtSch_other.Text = ""
            txtSchool_head.Text = ""
            txtFeeID_GEMS.Text = ""
            txtLang_Instr.Text = ""
            txtSchAddr.Text = ""
            txtSchCity.Text = ""
            ddlGemsGr.ClearSelection()
            ddlGemsGr.SelectedIndex = 0
            ddlPreSchool_Nursery.ClearSelection()
            ddlPreSchool_Nursery.SelectedIndex = 0
            txtSchFrom_dt.Text = ""
            txtSchTo_dt.Text = ""

            txtHthNo.Text = ""
            ddlBgroup.ClearSelection()
            ddlBgroup.SelectedIndex = 0
            txtHthAll_Note.Text = ""
            txtHthSM_Note.Text = ""
            txtHthPER_Note.Text = ""
            txtHthOth_Note.Text = ""
            txtHthLS_Note.Text = ""
            txtHthSE_Note.Text = ""
            txtHthEAL_Note.Text = ""
            txtHthBehv_Note.Text = ""
            txtHthEnr_note.Text = ""
            txtHthMus_Note.Text = ""
            txtHthSport_note.Text = ""
            rbHthSport_No.Checked = True
            rbHthMus_No.Checked = True
            rbHthEnr_No.Checked = True
            rbHthBehv_No.Checked = True
            rbHthEAL_No.Checked = True
            rbHthSE_No.Checked = True
            rbHthLS_No.Checked = True
            rbHthOther_No.Checked = True
            rbHthPER_No.Checked = True
            rbHthSM_No.Checked = True
            rbHthAll_No.Checked = True
            rbWGood.Checked = False
            rbWExc.Checked = False
            rbWFair.Checked = False
            rbWPoor.Checked = False

            rbSGood.Checked = False
            rbSExc.Checked = False
            rbSFair.Checked = False
            rbSPoor.Checked = False
            rbRGood.Checked = False
            rbRExc.Checked = False
            rbRFair.Checked = False
            rbRPoor.Checked = False
            ' ViewState("TABLE_School_Pre") = CreateDataTable()
            ViewState("TABLE_School_Pre").Rows.Clear()
            ViewState("id") = 1
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "clear appl")
        End Try
    End Sub

    Function Enquiry_transaction() As Integer

        Dim status As String = String.Empty
        Dim TEMP_ApplNo As String = String.Empty
        Dim TEMP_EQM_ENQID As Integer

        Dim EQM_PREVSCHOOL_GRD_ID As String = String.Empty
        Dim EQM_EQP_ID As String = String.Empty
        Dim EQM_EQMID As String = String.Empty
        Dim EQM_PREVSCHOOL_NURSERY As String = String.Empty
        Dim EQM_PREVSCHOOL_REG_ID As String = String.Empty
        Dim EQM_APPLNO As String = String.Empty
        Dim EQM_PREVSCHOOL_BSU_ID As String = String.Empty
        Dim EQM_PREVSCHOOL As String = String.Empty
        Dim EQM_PREVSCHOOL_CITY As String = String.Empty
        Dim EQM_PREVSCHOOL_CTY_ID As String = String.Empty
        Dim EQM_PREVSCHOOL_CLM_ID As String = String.Empty
        Dim EQM_PREVSCHOOL_MEDIUM As String = String.Empty
        Dim EQM_PREVSCHOOL_LASTATTDATE As Date
        Dim EQM_bPREVSCHOOLGEMS As Boolean
        Dim EQM_SIBLINGFEEID As String = String.Empty
        Dim EQM_SIBLINGSCHOOL As String = String.Empty
        Dim EQM_EX_STU_ID As String = String.Empty
        Dim EQM_EXUNIT As String = String.Empty
        Dim EQM_MODE_ID As String = String.Empty
        Dim EQM_ENQTYPE As String = "R"
        Dim EQM_STATUS As String = String.Empty


        Dim EQM_PREFCONTACT As String = String.Empty
        Dim EQM_PRIMARYCONTACT As String = String.Empty
        Dim EQM_STAFFUNIT As String = String.Empty
        Dim EQM_STAFF_EMP_ID As String = String.Empty

        Dim EQM_ENQDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Date.Today)
        Dim EQM_APPLFIRSTNAME As String = Trim(txtFname.Text)
        Dim EQM_APPLMIDNAME As String = Trim(txtMname.Text)
        Dim EQM_APPLLASTNAME As String = Trim(txtLname.Text)
        Dim ENQ_APPLPASSPORTNAME As String = Trim(txtFname.Text) & " " & Trim(txtMname.Text) & " " & Trim(txtLname.Text)
        Dim EQM_APPLDOB As Date = txtDob.Text


        Dim EQM_APPLGENDER As String = String.Empty
        If rdMale.Checked = True Then
            EQM_APPLGENDER = "M"
        ElseIf rdFemale.Checked = True Then
            EQM_APPLGENDER = "F"
        End If

        Dim EQM_REL_ID As String = ddlReligion.SelectedItem.Value
        Dim EQM_APPLNATIONALITY As String = ddlNational.SelectedItem.Value
        Dim EQM_APPLPOB As String = txtPob.Text
        Dim EQM_APPLCOB As String = ddlCountry.SelectedItem.Value
        Dim EQM_SAUDI_ID As String = String.Empty
        Dim EQM_APPLPASPRTNO As String = txtPassport.Text
        Dim EQM_APPLPASPRTISSDATE As Date = Date.MinValue 'txtPassIss_date.Text
        Dim EQM_APPLPASPRTEXPDATE As Date = Date.MinValue ' txtPassExp_Date.Text
        Dim EQM_APPLPASPRTISSPLACE As String = txtPassportIssue.Text
        Dim EQM_APPLVISANO As String = txtVisaNo.Text
        Dim EQM_APPLEMIRATES_ID As String = ""
        Dim EQM_MODE_NOTE As String = "" 'is choosen other
        Dim EQM_APPLVISAISSDATE As Date = Date.MinValue
        Dim EQM_BLOODGROUP As String = ddlBgroup.SelectedValue
        Dim EQM_HEALTHCARDNO As String = txtHthNo.Text.Trim
        Session("PassportNo_Enq") = txtPassport.Text

        If txtPassIss_date.Text <> "" Then
            EQM_APPLPASPRTISSDATE = txtPassIss_date.Text
        End If

        If txtPassExp_Date.Text <> "" Then
            EQM_APPLPASPRTEXPDATE = txtPassExp_Date.Text
        End If


        If txtVisaIss_date.Text <> "" Then
            EQM_APPLVISAISSDATE = txtVisaIss_date.Text
        End If

        Dim EQM_APPLVISAEXPDATE As Date = Date.MinValue
        If txtVisaExp_date.Text <> "" Then
            EQM_APPLVISAEXPDATE = txtVisaExp_date.Text
        End If


        Dim EQM_APPLVISAISSPLACE As String = txtIss_Place.Text
        Dim EQM_APPLVISAISSAUTH As String = txtIss_Auth.Text
        'check 4 is it KG1 or not 
        ' code modified on 22-apr-2008
        'EQM_PREVSCHOOL_NURSERY
        If (ViewState("Table_row") = "KG1") Or (ViewState("Table_row") = "PK") Or (ViewState("Table_row") = "PN1") Then
            EQM_PREVSCHOOL_NURSERY = ddlPreSchool_Nursery.SelectedItem.Value
            EQM_PREVSCHOOL_REG_ID = txtRegNo.Text
        Else
            If ViewState("rowstate") = "2" = True Then
                EQM_PREVSCHOOL_NURSERY = ddlGemsGr.SelectedItem.Value
                EQM_PREVSCHOOL_REG_ID = txtFeeID_GEMS.Text

            End If
            If ViewState("rowstate") = "1" Then
                If ddlGemsGr.SelectedItem.Text = "OTHER" Then
                    EQM_bPREVSCHOOLGEMS = False
                    EQM_PREVSCHOOL = "" '' txtPre_School.Text
                    EQM_PREVSCHOOL_CITY = "" ''txtPre_City.Text
                Else
                    EQM_bPREVSCHOOLGEMS = True
                    EQM_PREVSCHOOL = "" '' txtPre_School.Text
                    EQM_PREVSCHOOL_CITY = "" ''txtPre_City.Text
                End If

            End If
        End If


        Dim EQM_bAPPLSIBLING As Boolean = chkSibling.Checked
        Session("bAPPLSIBLING") = chkSibling.Checked
        Session("SIBLINGFEEID") = ""
        Session("SIBLINGSCHOOL") = ""
        If EQM_bAPPLSIBLING = True Then
            Dim query_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Sib_query As String = "SELECT  STU_FEE_ID  FROM STUDENT_M WHERE (STU_BSU_ID = '" & ddlSib_BSU.SelectedValue & "') AND ((STU_FEE_ID = '" & txtSib_ID.Text & "') OR (STU_NO = '" & txtSib_ID.Text & "'))"

            EQM_SIBLINGFEEID = CStr(SqlHelper.ExecuteScalar(query_conn, CommandType.Text, Sib_query))
            Session("SIBLINGFEEID") = EQM_SIBLINGFEEID
            EQM_SIBLINGSCHOOL = ddlSib_BSU.SelectedItem.Value
            Session("SIBLINGSCHOOL") = EQM_SIBLINGSCHOOL
            Session("Sibling_FeeID") = UCase(txtSib_Name.Text) & "/" & EQM_SIBLINGFEEID

            'EQM_SIBLINGFEEID = txtSib_ID.Text
            'Session("SIBLINGFEEID") = EQM_SIBLINGFEEID
            'EQM_SIBLINGSCHOOL = ddlSib_BSU.SelectedItem.Value
            'Session("SIBLINGSCHOOL") = EQM_SIBLINGSCHOOL
            'Session("Sibling_FeeID") = UCase(txtSib_Name.Text) & "/" & UCase(txtSib_ID.Text)
        Else
            Session("Sibling_FeeID") = ""
        End If
        Session("ExStudName_Gems") = ""
        Dim EQM_bEXSTUDENT As String = chkExStud_Gems.Checked
        If EQM_bEXSTUDENT = True Then
            Session("ExStudName_Gems") = txtExStudName_Gems.Text
            'code modified by removing txtExStudFeeID_Gems.Text
            EQM_EX_STU_ID = "" 'txtExStudFeeID_Gems.Text


            EQM_EXUNIT = ddlExStud_Gems.SelectedItem.Value
        End If
        Session("StaffID_ENQ") = ""
        Session("STAFF_NAME") = ""
        Dim EQM_bSTAFFGEMS As Boolean = chkStaff_GEMS.Checked
        If EQM_bSTAFFGEMS = True Then
            EQM_STAFFUNIT = ddlStaff_BSU.SelectedItem.Value
            Session("STAFFUNIT") = ddlStaff_BSU.SelectedItem.Value
            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtStaffID.Text, ddlStaff_BSU.SelectedItem.Value)
                Session("StaffID_ENQ") = txtStaffID.Text
                Session("STAFF_NAME") = txtStaff_Name.Text
                If ValidStaff_reader.HasRows = True Then

                    While ValidStaff_reader.Read
                        EQM_STAFF_EMP_ID = Convert.ToString(ValidStaff_reader("EMP_ID"))
                    End While

                End If
            End Using

        End If

        Dim EQM_REMARKS As String = ""

        Dim EQM_bRCVSPMEDICATION As Boolean
        Dim EQM_SPMEDICN As String = String.Empty

        Dim EQM_bRCVMAIL As Boolean = chkEAdd.Checked
        Dim EQM_bRCVSMS As Boolean = chksms.Checked

        EQM_PREFCONTACT = ddlPref_contact.SelectedItem.Value

        If rdPri_Father.Checked Then
            EQM_PRIMARYCONTACT = "F"
        ElseIf rbPri_Mother.Checked Then
            EQM_PRIMARYCONTACT = "M"
        ElseIf rdPri_Guard.Checked Then
            EQM_PRIMARYCONTACT = "G"
        End If

        Dim EQM_EMGCONTACT As String = String.Empty
        ' If txtStud_Contact_Country.Text <> "" And txtStud_Contact_Area.Text <> "" And txtStud_Contact_No.Text <> "" Then
        EQM_EMGCONTACT = txtStud_Contact_Country.Text & "-" & txtStud_Contact_Area.Text & "-" & txtStud_Contact_No.Text
        'Else
        ' EQM_EMGCONTACT = ""
        'End If

        Dim EQM_FILLED_BY As String = String.Empty
        Dim EQM_AGENT_NAME As String = String.Empty
        Dim EQM_AGENT_MOB As String = String.Empty
        If rbParentF.Checked = True Then
            EQM_FILLED_BY = "P"

        ElseIf rbRelocAgent.Checked = True Then
            EQM_FILLED_BY = "A"
            EQM_AGENT_NAME = txtAgentName.Text
            EQM_AGENT_MOB = txtMAgent_country.Text & "-" & txtMAgent_Area.Text & "-" & txtMAgent_No.Text
        End If

        Dim EQM_TRM_ID As String = String.Empty
        If ddlTerm.SelectedIndex = -1 Then
            EQM_TRM_ID = ""
        Else
            EQM_TRM_ID = ddlTerm.SelectedItem.Value
        End If
        Dim EQM_FIRSTLANG As String = ddlFLang.SelectedValue
        Dim EQM_OTHLANG As String = String.Empty

        For Each item As ListItem In chkOLang.Items
            If (item.Selected) Then

                EQM_OTHLANG = EQM_OTHLANG + item.Value + "|"
            End If
        Next
        Dim EQM_bRCVPUBL As Boolean = chkPubl.Checked
        Dim EQM_bMEAL As Boolean

        If rbMealYes.Checked = True Then
            EQM_bMEAL = True
        ElseIf rbMealNo.Checked = True Then
            EQM_bMEAL = False
        End If

        Dim trans As SqlTransaction

        'code to check for duplicate entry in the enquiry form
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            trans = conn.BeginTransaction("SampleTransaction")

            Try
                status = AccessStudentClass.SaveStudENQUIRY_M_NEW(EQM_EQMID, UCase(EQM_ENQDATE), UCase(EQM_APPLFIRSTNAME), UCase(EQM_APPLMIDNAME), UCase(EQM_APPLLASTNAME), UCase(ENQ_APPLPASSPORTNAME),
                                       UCase(EQM_APPLDOB), UCase(EQM_APPLGENDER), EQM_REL_ID, UCase(EQM_APPLNATIONALITY), UCase(EQM_APPLPOB), UCase(EQM_APPLCOB), UCase(EQM_APPLPASPRTNO), UCase(EQM_APPLPASPRTISSDATE),
                                         UCase(EQM_APPLPASPRTEXPDATE), UCase(EQM_APPLPASPRTISSPLACE), UCase(EQM_APPLVISANO), UCase(EQM_APPLVISAISSDATE), UCase(EQM_APPLVISAEXPDATE),
                                         UCase(EQM_APPLVISAISSPLACE), UCase(EQM_APPLVISAISSAUTH), EQM_bPREVSCHOOLGEMS, EQM_PREVSCHOOL_BSU_ID, UCase(EQM_PREVSCHOOL_NURSERY), UCase(EQM_PREVSCHOOL_REG_ID), UCase(EQM_PREVSCHOOL),
                                         UCase(EQM_PREVSCHOOL_CITY), EQM_PREVSCHOOL_CTY_ID, UCase(EQM_PREVSCHOOL_GRD_ID), EQM_PREVSCHOOL_CLM_ID, EQM_PREVSCHOOL_MEDIUM, UCase(EQM_PREVSCHOOL_LASTATTDATE),
                                         EQM_bAPPLSIBLING, UCase(EQM_SIBLINGFEEID), UCase(EQM_SIBLINGSCHOOL), EQM_bEXSTUDENT, UCase(EQM_EXUNIT), UCase(EQM_EX_STU_ID),
                                         EQM_bSTAFFGEMS, UCase(EQM_STAFFUNIT), EQM_STAFF_EMP_ID, EQM_bRCVSPMEDICATION, UCase(EQM_SPMEDICN), UCase(EQM_REMARKS),
                                         EQM_EQP_ID, EQM_bRCVSMS, EQM_bRCVMAIL, UCase(EQM_PRIMARYCONTACT), UCase(EQM_PREFCONTACT), UCase(EQM_MODE_ID),
                                        UCase(EQM_ENQTYPE), UCase(EQM_STATUS), EQM_EMGCONTACT, EQM_TRM_ID, EQM_FILLED_BY, UCase(EQM_AGENT_NAME), EQM_AGENT_MOB, EQM_FIRSTLANG, EQM_OTHLANG, EQM_bRCVPUBL, EQM_SAUDI_ID,
                                        EQM_BLOODGROUP, EQM_HEALTHCARDNO, ddlFee_sponsor.SelectedValue, EQM_bMEAL, EQM_APPLEMIRATES_ID, EQM_MODE_NOTE, TEMP_EQM_ENQID, trans)

                If status <> 0 Then
                    Throw New ArgumentException("Record could not be Inserted")

                Else


                    Dim EQP_ID As String = TEMP_EQM_ENQID
                    Dim EQP_FFIRSTNAME As String = String.Empty
                    Dim EQP_FMIDNAME As String = String.Empty
                    Dim EQP_FLASTNAME As String = String.Empty
                    Dim EQP_FNATIONALITY As String = String.Empty
                    Dim EQP_FNATIONALITY2 As String = String.Empty



                    Dim EQP_FCOMSTREET As String = String.Empty
                    Dim EQP_FCOMAREA As String = String.Empty
                    Dim EQP_FCOMBLDG As String = String.Empty
                    Dim EQP_FCOMAPARTNO As String = String.Empty




                    Dim EQP_FCOMPOBOX As String = String.Empty
                    Dim EQP_FCOMCITY As String = String.Empty
                    Dim EQP_FCOMSTATE As String = String.Empty
                    Dim EQP_FCOMCOUNTRY As String = String.Empty
                    Dim EQP_FOFFPHONECODE As String = String.Empty
                    Dim EQP_FOFFPHONE As String = String.Empty
                    Dim EQP_FRESPHONECODE As String = String.Empty
                    Dim EQP_FRESPHONE As String = String.Empty
                    Dim EQP_FFAXCODE As String = String.Empty
                    Dim EQP_FFAX As String = String.Empty
                    Dim EQP_FMOBILECODE As String = String.Empty
                    Dim EQP_FMOBILE As String = String.Empty
                    Dim EQP_FPRMADDR1 As String = String.Empty
                    Dim EQP_FPRMADDR2 As String = String.Empty
                    Dim EQP_FPRMPOBOX As String = String.Empty
                    'field added
                    Dim EQP_FCOMPOBOX_EMIR As String = String.Empty

                    Dim EQP_FPRMCITY As String = String.Empty
                    Dim EQP_FPRMCOUNTRY As String = String.Empty
                    Dim EQP_FPRMPHONE As String = String.Empty
                    Dim EQP_FOCC As String = String.Empty
                    Dim EQP_FCOMPANY As String = String.Empty
                    Dim EQP_FEMAIL As String = String.Empty
                    'FILED ADDED
                    Dim EQP_FACD_YEAR As String = String.Empty
                    Dim EQP_FMODE_ID As String = String.Empty

                    Dim EQP_BSU_ID_STAFF As String = String.Empty
                    Dim EQP_bFGEMSSTAFF As Boolean
                    Dim EQP_MFIRSTNAME As String = String.Empty
                    Dim EQP_MMIDNAME As String = String.Empty
                    Dim EQP_MLASTNAME As String = String.Empty
                    Dim EQP_MNATIONALITY As String = String.Empty
                    Dim EQP_MNATIONALITY2 As String = String.Empty


                    Dim EQP_MCOMSTREET As String = String.Empty
                    Dim EQP_MCOMAREA As String = String.Empty
                    Dim EQP_MCOMBLDG As String = String.Empty
                    Dim EQP_MCOMAPARTNO As String = String.Empty



                    Dim EQP_MCOMPOBOX As String = String.Empty
                    Dim EQP_MCOMCITY As String = String.Empty
                    Dim EQP_MCOMSTATE As String = String.Empty
                    Dim EQP_MCOMCOUNTRY As String = String.Empty
                    Dim EQP_MOFFPHONECODE As String = String.Empty
                    Dim EQP_MOFFPHONE As String = String.Empty
                    Dim EQP_MRESPHONECODE As String = String.Empty
                    Dim EQP_MRESPHONE As String = String.Empty
                    Dim EQP_MFAXCODE As String = String.Empty
                    Dim EQP_MFAX As String = String.Empty
                    Dim EQP_MMOBILECODE As String = String.Empty
                    Dim EQP_MMOBILE As String = String.Empty
                    Dim EQP_MPRMADDR1 As String = String.Empty
                    Dim EQP_MPRMADDR2 As String = String.Empty
                    Dim EQP_MPRMPOBOX As String = String.Empty
                    'field added
                    Dim EQP_MCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_MMODE_ID As String = String.Empty

                    Dim EQP_MPRMCITY As String = String.Empty
                    Dim EQP_MPRMCOUNTRY As String = String.Empty
                    Dim EQP_MPRMPHONE As String = String.Empty
                    Dim EQP_MOCC As String = String.Empty
                    Dim EQP_MCOMPANY As String = String.Empty
                    Dim EQP_MEMAIL As String = String.Empty
                    Dim EQP_MBSU_ID As String = String.Empty
                    'FIELD ADDED
                    Dim EQP_MACD_YEAR As String = String.Empty

                    Dim EQP_bMGEMSSTAFF As Boolean
                    Dim EQP_GFIRSTNAME As String = String.Empty
                    Dim EQP_GMIDNAME As String = String.Empty
                    Dim EQP_GLASTNAME As String = String.Empty
                    Dim EQP_GNATIONALITY As String = String.Empty
                    Dim EQP_GNATIONALITY2 As String = String.Empty


                    Dim EQP_GCOMSTREET As String = String.Empty
                    Dim EQP_GCOMAREA As String = String.Empty
                    Dim EQP_GCOMBLDG As String = String.Empty
                    Dim EQP_GCOMAPARTNO As String = String.Empty

                    Dim EQP_GCOMPOBOX As String = String.Empty
                    Dim EQP_GCOMCITY As String = String.Empty
                    Dim EQP_GCOMSTATE As String = String.Empty
                    Dim EQP_GCOMCOUNTRY As String = String.Empty
                    Dim EQP_GOFFPHONECODE As String = String.Empty
                    Dim EQP_GOFFPHONE As String = String.Empty
                    Dim EQP_GRESPHONECODE As String = String.Empty
                    Dim EQP_GRESPHONE As String = String.Empty
                    Dim EQP_GFAXCODE As String = String.Empty
                    Dim EQP_GFAX As String = String.Empty
                    Dim EQP_GMOBILECODE As String = String.Empty
                    Dim EQP_GMOBILE As String = String.Empty
                    Dim EQP_GPRMADDR1 As String = String.Empty
                    Dim EQP_GPRMADDR2 As String = String.Empty
                    Dim EQP_GPRMPOBOX As String = String.Empty

                    'field added
                    Dim EQP_GCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_GMODE_ID As String = String.Empty

                    Dim EQP_GPRMCITY As String = String.Empty
                    Dim EQP_GPRMCOUNTRY As String = String.Empty
                    Dim EQP_GPRMPHONE As String = String.Empty
                    Dim EQP_GOCC As String = String.Empty
                    Dim EQP_GCOMPANY As String = String.Empty
                    Dim EQP_GEMAIL As String = String.Empty
                    Dim EQP_GBSU_ID As String = String.Empty
                    'FIELD ADDED
                    Dim EQP_GACD_YEAR As String = String.Empty



                    Dim EQP_bGGEMSSTAFF As Boolean
                    Dim EQP_FCOMP_ID As String = String.Empty
                    Dim EQP_MCOMP_ID As String = String.Empty
                    Dim EQP_GCOMP_ID As String = String.Empty

                    Dim EQP_FAMILY_CHK As String = String.Empty
                    If chkFam_Par_sep.Checked = True Then
                        EQP_FAMILY_CHK += "PAR_SEP|"
                    End If
                    If chkFam_Par_div.Checked = True Then
                        EQP_FAMILY_CHK += "PAR_DIV|"
                    End If

                    If chkFam_Fath_Des.Checked = True Then
                        EQP_FAMILY_CHK += "FAT_DES|"
                    End If
                    If chkFam_Moth_Des.Checked = True Then
                        EQP_FAMILY_CHK += "MOT_DES|"
                    End If
                    If chkFam_Oth.Checked = True Then
                        EQP_FAMILY_CHK += "OTH|"
                    End If

                    Dim EQP_FAMILY_NOTE As String = txtFamily_NOTE.Text.Trim 'is choosen other
                    Dim EQP_FAMILY_LEGAL As String = txtStu_Leg.Text.Trim
                    Dim EQP_FAMILY_LIVING As String = txtStu_Living.Text.Trim


                    Dim str_Aboutus As String = String.Empty
                    For Each item As ListItem In chkAboutUs.Items
                        If (item.Selected) Then

                            str_Aboutus = str_Aboutus & item.Value
                            str_Aboutus = str_Aboutus & "|"
                        End If
                    Next




                    Session("str_Aboutus") = str_Aboutus
                    If rdPri_Father.Checked Then


                        EQP_FFIRSTNAME = txtPri_Fname.Text
                        EQP_FMIDNAME = txtPri_Mname.Text
                        EQP_FLASTNAME = txtPri_Lname.Text
                        EQP_FNATIONALITY = ddlPri_National1.SelectedItem.Value
                        EQP_FNATIONALITY2 = ddlPri_National2.SelectedItem.Value

                        'Field added 19/aug/2008

                        EQP_FCOMSTREET = txtStreet.Text
                        EQP_FCOMAREA = txtArea.Text
                        EQP_FCOMBLDG = txtBldg.Text
                        EQP_FCOMAPARTNO = txtApartNo.Text





                        EQP_FCOMPOBOX = txtPoboxLocal.Text
                        'Field added 19/aug/2008
                        EQP_FCOMPOBOX_EMIR = ddlEmirate.SelectedItem.Value

                        EQP_FCOMCITY = txtCity_pri.Text
                        EQP_FCOMSTATE = txtCity_pri.Text
                        EQP_FCOMCOUNTRY = ddlCountry_Pri.SelectedItem.Value
                        EQP_FOFFPHONECODE = txtOPhone_Country.Text


                        ' If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then
                        EQP_FOFFPHONE = txtOPhone_Country.Text & "-" & txtOPhone_Area.Text & "-" & txtOPhone.Text
                        'Else
                        '  EQP_FOFFPHONE = txtOPhone.Text
                        '  End If

                        EQP_FRESPHONECODE = txtHPhone_Country.Text


                        If txtHPhone_Country.Text <> "" And txtHPhone_Area.Text <> "" And txtHPhone.Text <> "" Then
                            EQP_FRESPHONE = txtHPhone_Country.Text & "-" & txtHPhone_Area.Text & "-" & txtHPhone.Text
                        Else
                            EQP_FRESPHONE = txtHPhone.Text
                        End If


                        EQP_FFAXCODE = txtFaxNo_country.Text

                        ' If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then
                        EQP_FFAX = txtFaxNo_country.Text & "-" & txtFaxNo_Area.Text & " " & txtFaxNo.Text
                        'Else
                        '   EQP_FFAX = txtFaxNo.Text
                        'End If

                        EQP_FMOBILECODE = txtM_Country.Text

                        'If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then

                        EQP_FMOBILE = txtM_Country.Text & "-" & txtM_Area.Text & "-" & txtMobile_Pri.Text
                        'Else
                        ' EQP_FMOBILE = txtMobile_Pri.Text
                        ' End If

                        EQP_FPRMADDR1 = txtAdd1_overSea.Text
                        EQP_FPRMADDR2 = txtAdd2_overSea.Text
                        EQP_FPRMPOBOX = txtPoBox_Pri.Text
                        EQP_FPRMCITY = txtOverSeas_Add_City.Text
                        EQP_FPRMCOUNTRY = ddlOverSeas_Add_Country.SelectedItem.Value

                        'If txtPhone_Oversea_Country.Text <> "" And txtPhone_Oversea_Area.Text <> "" And txtPhone_Oversea_No.Text <> "" Then

                        EQP_FPRMPHONE = txtPhone_Oversea_Country.Text & "-" & txtPhone_Oversea_Area.Text & "-" & txtPhone_Oversea_No.Text
                        'Else
                        '  EQP_FPRMPHONE = txtPhone_Oversea_No.Text
                        ' End If


                        EQP_FOCC = txtOccup.Text

                        'code updated 25-may-2008


                        If ddlCompany.SelectedItem.Text = "Other" Then
                            EQP_FCOMP_ID = "0"
                            EQP_FCOMPANY = txtComp.Text
                        Else
                            EQP_FCOMP_ID = ddlCompany.SelectedItem.Value
                            EQP_FCOMPANY = ""
                        End If



                        EQP_FEMAIL = txtEmail_Pri.Text.ToString.ToLower
                        EQP_bFGEMSSTAFF = chkExStud_Gems.Checked
                        If EQP_bFGEMSSTAFF Then
                            EQP_FACD_YEAR = ddlExYear.SelectedItem.Value
                            EQP_BSU_ID_STAFF = ddlExStud_Gems.SelectedItem.Value
                        End If

                        EQP_FMODE_ID = str_Aboutus.ToString

                    ElseIf rbPri_Mother.Checked Then
                        EQP_MFIRSTNAME = txtPri_Fname.Text
                        EQP_MMIDNAME = txtPri_Mname.Text
                        EQP_MLASTNAME = txtPri_Lname.Text
                        EQP_MNATIONALITY = ddlPri_National1.SelectedItem.Value
                        EQP_MNATIONALITY2 = ddlPri_National2.SelectedItem.Value

                        EQP_MCOMSTREET = txtStreet.Text
                        EQP_MCOMAREA = txtArea.Text
                        EQP_MCOMBLDG = txtBldg.Text
                        EQP_MCOMAPARTNO = txtApartNo.Text

                        EQP_MCOMCITY = txtCity_pri.Text
                        EQP_MCOMSTATE = txtCity_pri.Text
                        EQP_MCOMCOUNTRY = ddlCountry_Pri.SelectedItem.Value

                        EQP_MCOMPOBOX = txtPoboxLocal.Text
                        'Field added 19/aug/2008
                        EQP_MCOMPOBOX_EMIR = ddlEmirate.SelectedItem.Value

                        EQP_MOFFPHONECODE = txtOPhone_Country.Text

                        ' If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then

                        EQP_MOFFPHONE = txtOPhone_Country.Text & "-" & txtOPhone_Area.Text & "-" & txtOPhone.Text
                        'Else
                        '    EQP_MOFFPHONE = txtOPhone.Text
                        'End If


                        EQP_MRESPHONECODE = txtHPhone_Country.Text


                        ' If txtHPhone_Country.Text <> "" And txtHPhone_Area.Text <> "" And txtHPhone.Text <> "" Then

                        EQP_MRESPHONE = txtHPhone_Country.Text & "-" & txtHPhone_Area.Text & "-" & txtHPhone.Text
                        'Else
                        '    EQP_MRESPHONE = txtHPhone.Text
                        'End If



                        EQP_MFAXCODE = txtFaxNo_country.Text



                        ' If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then

                        EQP_MFAX = txtFaxNo_country.Text & "-" & txtFaxNo_Area.Text & "-" & txtFaxNo.Text
                        'Else
                        '    EQP_MFAX = txtFaxNo.Text
                        'End If


                        EQP_MMOBILECODE = txtM_Country.Text


                        ' If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then

                        EQP_MMOBILE = txtM_Country.Text & "-" & txtM_Area.Text & "-" & txtMobile_Pri.Text
                        'Else
                        ' EQP_MMOBILE = txtMobile_Pri.Text
                        'End If



                        EQP_MPRMADDR1 = txtAdd1_overSea.Text
                        EQP_MPRMADDR2 = txtAdd2_overSea.Text
                        EQP_MPRMPOBOX = txtPoBox_Pri.Text
                        EQP_MPRMCITY = txtOverSeas_Add_City.Text
                        EQP_MPRMCOUNTRY = ddlOverSeas_Add_Country.SelectedItem.Value


                        If txtPhone_Oversea_Country.Text <> "" And txtPhone_Oversea_Area.Text <> "" And txtPhone_Oversea_No.Text <> "" Then
                            EQP_MPRMPHONE = txtPhone_Oversea_Country.Text & "-" & txtPhone_Oversea_Area.Text & "-" & txtPhone_Oversea_No.Text
                        Else
                            EQP_MPRMPHONE = txtPhone_Oversea_No.Text
                        End If


                        EQP_MOCC = txtOccup.Text

                        'code updated 25-may-2008


                        If ddlCompany.SelectedItem.Text = "Other" Then
                            EQP_MCOMP_ID = ""
                            EQP_MCOMPANY = txtComp.Text
                        Else
                            EQP_MCOMP_ID = ddlCompany.SelectedItem.Value
                            EQP_MCOMPANY = ""
                        End If





                        EQP_MEMAIL = txtEmail_Pri.Text.ToString.ToLower


                        EQP_bMGEMSSTAFF = chkExStud_Gems.Checked
                        If EQP_bMGEMSSTAFF Then
                            EQP_MACD_YEAR = ddlExYear.SelectedItem.Value
                            EQP_BSU_ID_STAFF = ddlExStud_Gems.SelectedItem.Value
                        End If



                        EQP_MMODE_ID = str_Aboutus.ToString
                    ElseIf rdPri_Guard.Checked Then
                        EQP_GFIRSTNAME = txtPri_Fname.Text
                        EQP_GMIDNAME = txtPri_Mname.Text
                        EQP_GLASTNAME = txtPri_Lname.Text
                        EQP_GNATIONALITY = ddlPri_National1.SelectedItem.Value
                        EQP_GNATIONALITY2 = ddlPri_National2.SelectedItem.Value



                        EQP_GCOMSTREET = txtStreet.Text
                        EQP_GCOMAREA = txtArea.Text
                        EQP_GCOMBLDG = txtBldg.Text
                        EQP_GCOMAPARTNO = txtApartNo.Text






                        EQP_GCOMPOBOX = txtPoboxLocal.Text
                        EQP_GCOMCITY = txtCity_pri.Text
                        EQP_GCOMSTATE = txtCity_pri.Text
                        EQP_GCOMCOUNTRY = ddlCountry_Pri.SelectedItem.Value
                        EQP_GOFFPHONECODE = txtOPhone_Country.Text


                        'If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then
                        EQP_GOFFPHONE = txtOPhone_Country.Text & "-" & txtOPhone_Area.Text & "-" & txtOPhone.Text
                        'Else
                        EQP_GOFFPHONE = txtOPhone.Text
                        'End If

                        'EQP_GRESPHONECODE = txtHPhone_Country.Text


                        ' If txtHPhone_Country.Text <> "" And txtHPhone_Area.Text <> "" And txtHPhone.Text <> "" Then

                        EQP_GRESPHONE = txtHPhone_Country.Text & "-" & txtHPhone_Area.Text & "-" & txtHPhone.Text
                        'Else
                        '    EQP_GRESPHONE = txtHPhone.Text
                        'End If


                        EQP_GFAXCODE = txtFaxNo_country.Text




                        'If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then

                        EQP_GFAX = txtFaxNo_country.Text & "-" & txtFaxNo_Area.Text & "-" & txtFaxNo.Text
                        'Else
                        '    EQP_GFAX = txtFaxNo.Text
                        'End If

                        EQP_GMOBILECODE = txtM_Country.Text



                        'If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then
                        EQP_GMOBILE = txtM_Country.Text & "-" & txtM_Area.Text & "-" & txtMobile_Pri.Text
                        'Else
                        '    EQP_GMOBILE = txtMobile_Pri.Text
                        'End If

                        EQP_GPRMADDR1 = txtAdd1_overSea.Text
                        EQP_GPRMADDR2 = txtAdd2_overSea.Text
                        EQP_GPRMPOBOX = txtPoBox_Pri.Text
                        'Field added 19/aug/2008
                        EQP_GCOMPOBOX_EMIR = ddlEmirate.SelectedItem.Value

                        EQP_GPRMCITY = txtOverSeas_Add_City.Text
                        EQP_GPRMCOUNTRY = ddlOverSeas_Add_Country.SelectedItem.Value




                        If txtPhone_Oversea_Country.Text <> "" And txtPhone_Oversea_Area.Text <> "" And txtPhone_Oversea_No.Text <> "" Then
                            EQP_GPRMPHONE = txtPhone_Oversea_Country.Text & " " & txtPhone_Oversea_Area.Text & " " & txtPhone_Oversea_No.Text
                        Else
                            EQP_GPRMPHONE = txtPhone_Oversea_No.Text
                        End If


                        EQP_GOCC = txtOccup.Text



                        'code updated 25-may-2008

                        If ddlCompany.SelectedItem.Text = "Other" Then
                            EQP_GCOMP_ID = ""
                            EQP_GCOMPANY = txtComp.Text
                        Else
                            EQP_GCOMP_ID = ddlCompany.SelectedItem.Value
                            EQP_GCOMPANY = ""
                        End If


                        EQP_GEMAIL = txtEmail_Pri.Text.ToString.ToLower

                        EQP_bGGEMSSTAFF = chkExStud_Gems.Checked
                        If EQP_bGGEMSSTAFF Then
                            EQP_GACD_YEAR = ddlExYear.SelectedItem.Value
                            EQP_BSU_ID_STAFF = ddlExStud_Gems.SelectedItem.Value
                        End If




                        EQP_GMODE_ID = str_Aboutus.ToString
                    End If

                    Dim FSALUT As String = ddlCont_Status.SelectedValue
                    Dim MSALUT As String = ddlCont_Status.SelectedValue
                    Dim GSALUT As String = ddlCont_Status.SelectedValue
                    If rdPri_Father.Checked = True Then
                        MSALUT = ""
                        GSALUT = ""
                    ElseIf rbPri_Mother.Checked = True Then
                        FSALUT = ""
                        GSALUT = ""
                    ElseIf rdPri_Guard.Checked = True Then
                        FSALUT = ""
                        MSALUT = ""


                    End If



                    status = AccessStudentClass.SaveStudENQUIRY_PARENT_M_NEW(TEMP_EQM_ENQID, UCase(EQP_FFIRSTNAME), UCase(EQP_FMIDNAME), UCase(EQP_FLASTNAME), UCase(EQP_FNATIONALITY), UCase(EQP_FNATIONALITY2),
                                                    UCase(EQP_FCOMSTREET), UCase(EQP_FCOMAREA), UCase(EQP_FCOMBLDG), UCase(EQP_FCOMAPARTNO), EQP_FCOMPOBOX, UCase(EQP_FCOMCITY), UCase(EQP_FCOMSTATE), UCase(EQP_FCOMCOUNTRY), EQP_FOFFPHONECODE, EQP_FOFFPHONE,
                                                    EQP_FRESPHONECODE, EQP_FRESPHONE, EQP_FFAXCODE, EQP_FFAX, EQP_FMOBILECODE, EQP_FMOBILE, UCase(EQP_FPRMADDR1), UCase(EQP_FPRMADDR2), EQP_FPRMPOBOX,
                                                    UCase(EQP_FPRMCITY), UCase(EQP_FPRMCOUNTRY), EQP_FPRMPHONE, UCase(EQP_FOCC), UCase(EQP_FCOMPANY), EQP_FEMAIL, EQP_bFGEMSSTAFF, EQP_BSU_ID_STAFF, EQP_FACD_YEAR,
                                                    UCase(EQP_MFIRSTNAME), UCase(EQP_MMIDNAME), UCase(EQP_MLASTNAME), UCase(EQP_MNATIONALITY), UCase(EQP_MNATIONALITY2), UCase(EQP_MCOMSTREET), UCase(EQP_MCOMAREA), UCase(EQP_MCOMBLDG), UCase(EQP_MCOMAPARTNO), EQP_MCOMPOBOX,
                                                    UCase(EQP_MCOMCITY), UCase(EQP_MCOMSTATE), UCase(EQP_MCOMCOUNTRY), EQP_MOFFPHONECODE, EQP_MOFFPHONE, EQP_MRESPHONECODE, EQP_MRESPHONE,
                                                    EQP_MFAXCODE, EQP_MFAX, EQP_MMOBILECODE, EQP_MMOBILE, UCase(EQP_MPRMADDR1), UCase(EQP_MPRMADDR2), EQP_MPRMPOBOX,
                                                    UCase(EQP_MPRMCITY), UCase(EQP_MPRMCOUNTRY), EQP_MPRMPHONE, UCase(EQP_MOCC), UCase(EQP_MCOMPANY), EQP_bMGEMSSTAFF,
                                                    EQP_MBSU_ID, EQP_MACD_YEAR, UCase(EQP_GFIRSTNAME), UCase(EQP_GMIDNAME), UCase(EQP_GLASTNAME), UCase(EQP_GNATIONALITY), UCase(EQP_GNATIONALITY2),
                                                    UCase(EQP_GCOMSTREET), UCase(EQP_GCOMAREA), UCase(EQP_GCOMBLDG), UCase(EQP_GCOMAPARTNO), EQP_GCOMPOBOX, UCase(EQP_GCOMCITY), UCase(EQP_GCOMSTATE), UCase(EQP_GCOMCOUNTRY), EQP_GOFFPHONECODE,
                                                    EQP_GOFFPHONE, EQP_GRESPHONECODE, EQP_GRESPHONE, EQP_GFAXCODE, EQP_GFAX, EQP_GMOBILECODE, EQP_GMOBILE, UCase(EQP_GPRMADDR1),
                                                    UCase(EQP_GPRMADDR2), EQP_GPRMPOBOX, UCase(EQP_GPRMCITY), UCase(EQP_GPRMCOUNTRY), EQP_GPRMPHONE, UCase(EQP_GOCC), UCase(EQP_GCOMPANY), EQP_bGGEMSSTAFF,
                                                    EQP_GBSU_ID, EQP_GACD_YEAR, EQP_GEMAIL, EQP_MEMAIL, UCase(EQP_FCOMP_ID), UCase(EQP_MCOMP_ID), UCase(EQP_GCOMP_ID), EQP_FCOMPOBOX_EMIR, EQP_MCOMPOBOX_EMIR, EQP_GCOMPOBOX_EMIR, EQP_FMODE_ID, EQP_MMODE_ID, EQP_GMODE_ID, EQP_FAMILY_NOTE,
                                                    FSALUT, MSALUT, GSALUT, EQP_FAMILY_CHK, EQP_FAMILY_LEGAL, EQP_FAMILY_LIVING, trans)



                    If status <> 0 Then
                        Throw New ArgumentException("Record could not be Inserted")

                    Else



                        Dim EQS_EQM_ENQID As String = TEMP_EQM_ENQID
                        Dim EQS_ACY_ID As String = ddlAca_Year.SelectedItem.Value
                        Dim EQS_BSU_ID As String = ddlGEMSSchool.SelectedItem.Value
                        Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
                        Dim EQS_SHF_ID As String = ddlShift.SelectedItem.Value
                        Dim EQS_bTPTREQD As Boolean = chkTran_Req.Checked
                        Dim EQS_LOC_ID As String = String.Empty
                        Dim EQS_SBL_ID As String = String.Empty
                        Dim EQS_PNT_ID As String = String.Empty
                        Dim EQS_TPTREMARKS As String = String.Empty
                        Dim EQS_STATUS As String = "NEW"
                        Dim EQS_STM_ID As String = ddlStream.SelectedItem.Value
                        Dim EQS_CLM_ID As Integer = ddlCurri.SelectedItem.Value
                        Dim EQS_DOJ As String = txtDOJ.Text

                        '-----Newly added fields--------------

                        Dim EQS_bALLERGIES As Boolean = IIf(rbHthAll_Yes.Checked = True, True, False)

                        Dim EQS_ALLERGIES As String = txtHthAll_Note.Text.Trim
                        Dim EQS_bRCVSPMEDICATION As Boolean = IIf(rbHthSM_Yes.Checked = True, True, False)
                        Dim EQS_SPMEDICN As String = txtHthSM_Note.Text
                        Dim EQS_bPRESTRICTIONS As Boolean = IIf(rbHthPER_Yes.Checked = True, True, False)
                        Dim EQS_PRESTRICTIONS As String = txtHthPER_Note.Text
                        Dim EQS_bHRESTRICTIONS As Boolean = IIf(rbHthOther_yes.Checked = True, True, False)
                        Dim EQS_HRESTRICTIONS As String = txtHthOth_Note.Text
                        Dim EQS_bTHERAPHY As Boolean = IIf(rbHthLS_Yes.Checked = True, True, False)
                        Dim EQS_THERAPHY As String = txtHthLS_Note.Text
                        Dim EQS_bSPEDUCATION As Boolean = IIf(rbHthSE_Yes.Checked = True, True, False)
                        Dim EQS_SPEDUCATION As String = txtHthSE_Note.Text
                        Dim EQS_bEAL As Boolean = IIf(rbHthEAL_Yes.Checked = True, True, False)
                        Dim EQS_EAL As String = txtHthEAL_Note.Text
                        Dim EQS_bMUSICAL As Boolean = IIf(rbHthMus_Yes.Checked = True, True, False)
                        Dim EQS_MUSICAL As String = txtHthMus_Note.Text
                        Dim EQS_bENRICH As Boolean = IIf(rbHthEnr_Yes.Checked = True, True, False)
                        Dim EQS_ENRICH As String = txtHthEnr_note.Text
                        Dim EQS_bBEHAVIOUR As Boolean = IIf(rbHthBehv_Yes.Checked = True, True, False)
                        Dim EQS_BEHAVIOUR As String = txtHthBehv_Note.Text
                        Dim EQS_bSPORTS As Boolean = IIf(rbHthSport_Yes.Checked = True, True, False)
                        Dim EQS_SPORTS As String = txtHthSport_note.Text
                        Dim EQS_ETHNICITY As String = ddlEthnicity.SelectedValue
                        Dim EQS_ETHNICITY_OTH As String = txtEthnicity.Text.Trim
                        Dim EQS_ENG_READING As String = String.Empty
                        Dim EQS_ENG_WRITING As String = String.Empty
                        Dim EQS_ENG_SPEAKING As String = String.Empty
                        Dim EQS_bREP_GRD As Boolean = IIf(rbRepGRD_Yes.Checked = True, True, False)
                        Dim EQS_REP_GRD As String = txtRep_GRD.Text.Trim

                        Dim EQS_bCommInt As Boolean = IIf(rbHthComm_Yes.Checked = True, True, False)
                        Dim EQS_CommInt As String = txtHthCommInt_Note.Text

                        Dim EQS_bDisabled As Boolean = IIf(rbHthDisabled_YES.Checked = True, True, False)
                        Dim EQS_Disabled As String = txtHthDisabled_Note.Text

                        If rbWExc.Checked = True Then
                            EQS_ENG_WRITING = 1
                        ElseIf rbWGood.Checked = True Then
                            EQS_ENG_WRITING = 2
                        ElseIf rbWFair.Checked = True Then
                            EQS_ENG_WRITING = 3
                        ElseIf rbWPoor.Checked = True Then
                            EQS_ENG_WRITING = 4
                        Else
                            EQS_ENG_WRITING = 0
                        End If


                        If rbSExc.Checked = True Then
                            EQS_ENG_SPEAKING = 1
                        ElseIf rbSGood.Checked = True Then
                            EQS_ENG_SPEAKING = 2
                        ElseIf rbSFair.Checked = True Then
                            EQS_ENG_SPEAKING = 3
                        ElseIf rbSPoor.Checked = True Then
                            EQS_ENG_SPEAKING = 4
                        Else
                            EQS_ENG_SPEAKING = 0
                        End If

                        If rbRExc.Checked = True Then
                            EQS_ENG_READING = 1
                        ElseIf rbRGood.Checked = True Then
                            EQS_ENG_READING = 2
                        ElseIf rbRFair.Checked = True Then
                            EQS_ENG_READING = 3
                        ElseIf rbRPoor.Checked = True Then
                            EQS_ENG_READING = 4
                        Else
                            EQS_ENG_READING = 0
                        End If



                        '------------------previous school Fields
                        Dim Prev_sch_id As String = String.Empty
                        Dim prev_SCH_name As String = String.Empty
                        Dim prev_sch_head As String = String.Empty
                        Dim prev_SCH_FEE_ID As String = String.Empty
                        Dim prev_SCH_GRADE As String = String.Empty
                        Dim prev_SCH_LEARN_INS As String = String.Empty
                        Dim prev_SCH_ADDR As String = String.Empty
                        Dim prev_SCH_CURR As String = String.Empty
                        Dim prev_SCH_CITY As String = String.Empty
                        Dim prev_SCH_COUNTRY As String = String.Empty
                        Dim prev_SCH_PHONE As String = String.Empty
                        Dim prev_SCH_FAX As String = String.Empty
                        Dim prev_SCH_FROMDT As String = String.Empty
                        Dim prev_SCH_TODT As String = String.Empty
                        Dim prev_SCH_TYPE As String = String.Empty





                        '-----------end of newly added field-----------------







                        If EQS_bTPTREQD Then
                            If ddlMainLocation.SelectedIndex <> -1 Then
                                EQS_LOC_ID = ddlMainLocation.SelectedItem.Value
                            End If
                            If ddlSubLocation.SelectedIndex <> -1 Then
                                EQS_SBL_ID = ddlSubLocation.SelectedItem.Value
                            End If
                            If ddlPickup.SelectedIndex <> -1 Then
                                EQS_PNT_ID = ddlPickup.SelectedItem.Value
                            End If


                            EQS_TPTREMARKS = UCase(Trim(txtOthers.Text))

                        End If
                        Dim EQS_RFD_ID As String = String.Empty
                        If chkRef.Checked = True Then
                            EQS_RFD_ID = ViewState("RFD_ID")
                        End If

                        status = AccessStudentClass.SaveStudENQUIRY_SCHOOLPRIO_S_NEW(EQS_EQM_ENQID, EQS_ACY_ID, EQS_BSU_ID, UCase(EQS_GRD_ID), EQS_SHF_ID,
    EQS_bTPTREQD, EQS_LOC_ID, EQS_SBL_ID, EQS_PNT_ID, EQS_STM_ID, EQM_bAPPLSIBLING, UCase(EQM_SIBLINGFEEID),
UCase(EQM_SIBLINGSCHOOL), EQS_TPTREMARKS, UCase(EQS_STATUS), UCase(EQM_REMARKS), EQS_CLM_ID, UCase(EQS_DOJ), EQS_RFD_ID,
EQS_bALLERGIES, UCase(EQS_ALLERGIES), EQS_bRCVSPMEDICATION, UCase(EQS_SPMEDICN), EQS_bPRESTRICTIONS, UCase(EQS_PRESTRICTIONS), EQS_bHRESTRICTIONS,
EQS_HRESTRICTIONS, EQS_bTHERAPHY, EQS_THERAPHY, EQS_bSPEDUCATION,
EQS_SPEDUCATION, EQS_bEAL, EQS_EAL, EQS_bMUSICAL,
EQS_MUSICAL, EQS_bENRICH, EQS_ENRICH, EQS_bBEHAVIOUR,
EQS_BEHAVIOUR, EQS_bSPORTS, EQS_SPORTS, EQS_ETHNICITY,
EQS_ETHNICITY_OTH, EQS_ENG_READING, EQS_ENG_WRITING, EQS_ENG_SPEAKING,
TEMP_ApplNo, EQS_bREP_GRD, EQS_REP_GRD, EQS_bCommInt, EQS_CommInt, EQS_bDisabled, EQS_Disabled, trans)


                        If status <> 0 Then
                            Throw New ArgumentException(getErrorMessage(status))
                        End If




                        If Not ViewState("TABLE_School_Pre") Is Nothing Then
                            If ViewState("Table_row").ToString.Contains("PK|KG1") = False Then

                                Dim str As String = String.Empty
                                If ViewState("TABLE_School_Pre").Rows.Count > 0 Then
                                    For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1


                                        Prev_sch_id = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("sch_id"))
                                        prev_SCH_name = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_NAME"))
                                        prev_sch_head = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_HEAD"))
                                        prev_SCH_FEE_ID = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_FEE_ID"))
                                        prev_SCH_GRADE = ViewState("TABLE_School_Pre").Rows(i)("SCH_GRADE")
                                        prev_SCH_LEARN_INS = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_LEARN_INS"))
                                        prev_SCH_ADDR = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_ADDR"))
                                        prev_SCH_CURR = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_CURR"))
                                        prev_SCH_CITY = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_CITY"))
                                        prev_SCH_COUNTRY = ViewState("TABLE_School_Pre").Rows(i)("SCH_COUNTRY")
                                        prev_SCH_PHONE = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_PHONE"))
                                        prev_SCH_FAX = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_FAX"))
                                        prev_SCH_FROMDT = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_FROMDT"))
                                        prev_SCH_TODT = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_TODT"))
                                        prev_SCH_TYPE = ViewState("TABLE_School_Pre").Rows(i)("SCH_TYPE")







                                        str += String.Format("<School sch_id='{0}' SCH_NAME='{1}' SCH_HEAD='{2}' SCH_FEE_ID='{3}' SCH_GRADE='{4}' SCH_LEARN_INS='{5}' SCH_ADDR='{6}' SCH_CURR='{7}' " &
                                                             " SCH_CITY='{8}' SCH_COUNTRY='{9}' SCH_PHONE='{10}' SCH_FAX='{11}'  SCH_FROMDT='{12}' SCH_TODT='{13}'  SCH_TYPE='{14}' />",
                                                            Prev_sch_id, prev_SCH_name, prev_sch_head,
                                                      prev_SCH_FEE_ID, prev_SCH_GRADE, prev_SCH_LEARN_INS,
                                                      prev_SCH_ADDR, prev_SCH_CURR, prev_SCH_CITY,
                                        prev_SCH_PHONE, prev_SCH_FAX,
                                        prev_SCH_FROMDT, prev_SCH_TODT, prev_SCH_TYPE)
                                    Next

                                    If str <> "" Then
                                        str = "<Schools>" + str + "</Schools>"

                                    End If
                                    Dim pParms(5) As SqlClient.SqlParameter
                                    pParms(0) = New SqlClient.SqlParameter("@STR", str)
                                    pParms(1) = New SqlClient.SqlParameter("@EPS_EQM_ENQID", EQS_EQM_ENQID)
                                    pParms(2) = New SqlClient.SqlParameter("@EPS_BSU_ID", EQS_BSU_ID)
                                    pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                                    pParms(3).Direction = ParameterDirection.ReturnValue
                                    SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveENQUIRY_PREVIOUS_SCHOOL", pParms)
                                    status = pParms(3).Value
                                    If status <> 0 Then
                                        Throw New ArgumentException(getErrorMessage(status))
                                    End If
                                End If

                            End If
                        End If








                    End If

                End If
                'return the ApplNo & Enq_ID
                Session("TEMP_ApplNo") = TEMP_ApplNo
                Session("Enq_ID") = TEMP_EQM_ENQID

                trans.Commit()

                lblErr.Text = "Record Inserted Successfully"

                'Adding transaction info

                Return 0
            Catch myex As ArgumentException
                trans.Rollback()

                UtilityObj.Errorlog(myex.Message)
                lblError.Text = myex.Message
                Return -1
            Catch ex As Exception

                trans.Rollback()

                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Record could not be Inserted"
                Return -1
            Finally

                OnlineEnqAudit_log()

            End Try

        End Using
    End Function
    Private Sub OnlineEnqAudit_log()
        Try
            Dim clientIp1 As String = Request.UserHostAddress()
            Dim ClientBrw As String = String.Empty
            Dim mcName As String = String.Empty
            Dim MAC_ID As String = IRDataTables.GetMACAddress
            IRDataTables.GetClientInfo(ClientBrw, clientIp1, mcName)
            Dim clientIp2 As String = String.Empty
            Dim serverVar As New StringBuilder
            Dim Enq_ID As Integer
            Dim ApplNo As Integer
            Dim OASISUsr As String = String.Empty

            If Not Session("Enq_ID") Is Nothing Then
                Enq_ID = IIf(Session("Enq_ID").ToString.Trim = "", 0, Session("Enq_ID"))
            End If
            If Not Session("TEMP_ApplNo") Is Nothing Then
                ApplNo = IIf(Session("TEMP_ApplNo").ToString.Trim = "", 0, Session("TEMP_ApplNo"))
            End If

            If Not Session("sUsr_id") Is Nothing Then
                If Session("sUsr_id").ToString.Trim <> "" Then
                    OASISUsr = Session("sUsr_id")
                End If

            End If
            clientIp2 = Request.ServerVariables("HTTP_X_FORWARDED_FOR")

            If clientIp2 = "" Or clientIp2 Is Nothing Then
                clientIp2 = Request.ServerVariables("REMOTE_ADDR")
            End If

            For Each item In Request.ServerVariables
                serverVar.Append("<" & item & ">" & Request.ServerVariables(item) & "</" & item & ">")

            Next
            If Enq_ID <> 0 Then



                Dim conn As String = ConnectionManger.GetOASISConnectionString
                Dim param(12) As SqlParameter
                param(0) = New SqlParameter("@OEA_EQS_BSU_ID", ddlGEMSSchool.SelectedItem.Value)
                param(1) = New SqlParameter("@OEA_EQM_ENQID", Enq_ID)
                param(2) = New SqlParameter("@OEA_EQS_APPLNO", ApplNo)
                param(3) = New SqlParameter("@OEA_USR_ID", OASISUsr)
                param(4) = New SqlParameter("@OEA_CLIENTIP1", clientIp1)
                param(5) = New SqlParameter("@OEA_CLIENTIP2", clientIp2)
                param(6) = New SqlParameter("@OEA_CLIENTBRW", ClientBrw)
                param(7) = New SqlParameter("@OEA_MCNAME", mcName)
                param(8) = New SqlParameter("@OEA_MAC_ID", MAC_ID)
                param(9) = New SqlParameter("@OEA_SERVER_VARIABLE", serverVar.ToString)

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "ENQ.OnlineEnquiryAudit", param)

            End If
        Catch ex As Exception

        End Try

    End Sub
#End Region

#Region "User defined functions"
    Function Replace_InvalidXML(ByVal str As String) As String
        str = str.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("""", "&quot;").Replace("'", "&apos;")
        Return str
    End Function
    Sub GetSchoolButton_Enable()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If


        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = ""
        Else
            SHF_ID = ddlShift.SelectedItem.Value
        End If
        If ddlStream.SelectedIndex = -1 Then
            STM_ID = ""
        Else
            STM_ID = ddlStream.SelectedItem.Value
        End If


        If Not (Request.QueryString("MainMnu_code")) Is Nothing Then

            Using EnableButton_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Registrar(Aca_Year, Curri, GEMSSchool)

                If EnableButton_reader.HasRows = True Then

                    ViewState("Acd_Valid") = "1" 'if open for registration

                Else
                    ViewState("Acd_Valid") = "-1" 'if open for registration

                End If

            End Using

        Else
            Using EnableButton_reader As SqlDataReader = AccessStudentClass.getPick_Stream(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID, STM_ID)

                If EnableButton_reader.HasRows = True Then

                    ViewState("Acd_Valid") = "1" 'if open for registration

                Else
                    ViewState("Acd_Valid") = "-1" 'if open for registration

                End If

            End Using


        End If




    End Sub
    Sub GetBusinessUnits_info_student()
        Try
            Dim type As Integer = 1
            If Session("sBsuid") = "600001" Then
                type = 2
            End If

            Using AllStudent_BSU_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student_Enq(type)

                Dim di_Student_BSU As ListItem
                ddlExStud_Gems.Items.Clear()
                ddlSib_BSU.Items.Clear()
                ddlGemsGr.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)

                If AllStudent_BSU_reader.HasRows = True Then
                    While AllStudent_BSU_reader.Read

                        di_Student_BSU = New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id"))
                        ddlExStud_Gems.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                        ddlSib_BSU.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                        ddlGemsGr.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                    End While


                End If
            End Using

            ddlGemsGr.Items.Add(New ListItem("OTHER", ""))
            If Not ddlGemsGr.Items.FindByText("OTHER") Is Nothing Then
                ddlGemsGr.ClearSelection()
                ddlGemsGr.Items.FindByText("OTHER").Selected = True
            End If





        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_student()")
        End Try
    End Sub
    'first GEMS BSU is Called
    Sub GEMS_BSU_4_Student()
        Dim type As Integer = 1
        If Session("sBsuid") = "600001" Then
            type = 2
        End If


        Using BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student_Enq(type)

            Dim di_Grade_BSU As ListItem
            ddlGEMSSchool.Items.Clear()

            If BSU_4_Student_reader.HasRows = True Then
                While BSU_4_Student_reader.Read
                    di_Grade_BSU = New ListItem(BSU_4_Student_reader("bsu_name"), BSU_4_Student_reader("bsu_id"))
                    ddlGEMSSchool.Items.Add(di_Grade_BSU)

                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    Sub CURRICULUM_BSU_4_Student()

        Dim GEMSSchool As String = String.Empty
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If




        Using CURRICULUM_BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_BSU(GEMSSchool)
            Dim di_CURRICULUM_BSU As ListItem
            ddlCurri.Items.Clear()
            If CURRICULUM_BSU_4_Student_reader.HasRows = True Then
                While CURRICULUM_BSU_4_Student_reader.Read
                    di_CURRICULUM_BSU = New ListItem(CURRICULUM_BSU_4_Student_reader("CLM_DESCR"), CURRICULUM_BSU_4_Student_reader("CLM_ID"))
                    ddlCurri.Items.Add(di_CURRICULUM_BSU)
                End While

                ViewState("Acd_Valid") = "1" 'if open for registration
                'ddlCurri.SelectedIndex = 0
                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            Else

                ViewState("Acd_Valid") = "-1" 'if not open for registration



                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    Sub GetAcademicYear_Bsu()
        Try

            Dim Curri As String = String.Empty
            Dim GEMSSchool As String = String.Empty


            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If

            If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
                Using OpenBsu_reader As SqlDataReader = AccessStudentClass.GetActive_BSU_Year_Registrar(GEMSSchool, Curri)
                    Dim di_Aca_Year As ListItem
                    ddlAca_Year.Items.Clear()

                    If OpenBsu_reader.HasRows = True Then
                        While OpenBsu_reader.Read
                            di_Aca_Year = New ListItem(OpenBsu_reader("ACY_DESCR"), OpenBsu_reader("ACY_ID"))
                            ddlAca_Year.Items.Add(di_Aca_Year)

                        End While


                        ViewState("Acd_Valid") = "1" 'if open for registration

                        Call showActive_year()


                        ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)


                    Else

                        ViewState("Acd_Valid") = "-1" 'if not open for registration

                        ' ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
                        ddlAca_Year.Items.Clear()
                        ddlGrade.Items.Clear()
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If
                End Using

            Else

                Using OpenBsu_reader As SqlDataReader = AccessStudentClass.GetActive_BSU_Year(GEMSSchool, Curri)
                    Dim di_Aca_Year As ListItem
                    ddlAca_Year.Items.Clear()

                    If OpenBsu_reader.HasRows = True Then
                        While OpenBsu_reader.Read
                            di_Aca_Year = New ListItem(OpenBsu_reader("ACY_DESCR"), OpenBsu_reader("ACY_ID"))
                            ddlAca_Year.Items.Add(di_Aca_Year)

                        End While

                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)


                    Else

                        ViewState("Acd_Valid") = "-1" 'if not open for registration



                        ddlAca_Year.Items.Clear()
                        ddlGrade.Items.Clear()
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If
                End Using

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAcademicYear_Bsu")
        End Try
    End Sub
    Sub showActive_year()

        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty


        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(GEMSSchool, Curri)
            If readerAcademic_Cutoff.HasRows = True Then
                While readerAcademic_Cutoff.Read
                    ddlAca_Year.ClearSelection()
                    ddlAca_Year.Items.FindByValue(Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))).Selected = True

                    'ddlAca_Year.Items.FindByValue(30).Selected = True
                End While
            End If
        End Using
    End Sub
    Sub GetAllGrade_Bsu()
        Try
            Dim Aca_Year As String = String.Empty
            Dim Curri As String = String.Empty
            Dim GEMSSchool As String = String.Empty
            Dim STM_ID As String = String.Empty

            If ddlAca_Year.SelectedIndex = -1 Then
                Aca_Year = ""
            Else
                Aca_Year = ddlAca_Year.SelectedItem.Value
            End If
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If
            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If

            If ddlStream.SelectedIndex = -1 Then
                STM_ID = ""
            Else
                STM_ID = ddlStream.SelectedItem.Value
            End If


            If Not (Request.QueryString("MainMnu_code")) Is Nothing Then

                Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetAllGrade_Bsu_Registrar(Aca_Year, GEMSSchool, Curri, STM_ID)
                    Dim di_Grade As ListItem
                    ddlGrade.Items.Clear()

                    If AllGrade_reader.HasRows = True Then
                        While AllGrade_reader.Read
                            di_Grade = New ListItem(AllGrade_reader("GRM_Display"), AllGrade_reader("GRD_ID"))
                            ddlGrade.Items.Add(di_Grade)

                        End While

                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                    Else


                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If


                End Using


            Else
                Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetAllGrade_Bsu(Aca_Year, GEMSSchool, Curri)
                    Dim di_Grade As ListItem
                    ddlGrade.Items.Clear()

                    If AllGrade_reader.HasRows = True Then
                        While AllGrade_reader.Read
                            di_Grade = New ListItem(AllGrade_reader("GRM_Display"), AllGrade_reader("GRD_ID"))
                            ddlGrade.Items.Add(di_Grade)

                        End While

                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                    Else



                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If


                End Using
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAllGrade_Bsu")
        End Try
    End Sub
    'Getting all the grade of currently open BSU
    Sub GetAllGrade_Shift()

        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If

        If Not (Request.QueryString("MainMnu_code")) Is Nothing Then

            Using AllGrade_Shift_reader As SqlDataReader = AccessStudentClass.getAllShift_4_Grade_Registrar(Aca_Year, Curri, GEMSSchool, Grade)
                Dim di_Grade_Shift As ListItem
                Dim Shift_count As Integer
                ddlShift.Items.Clear()

                If AllGrade_Shift_reader.HasRows = True Then
                    While AllGrade_Shift_reader.Read
                        di_Grade_Shift = New ListItem(AllGrade_Shift_reader("SHF_DESCR"), AllGrade_Shift_reader("SHF_ID"))
                        ddlShift.Items.Add(di_Grade_Shift)
                        Shift_count += 1
                    End While
                    'code modified on 20th apr-2008
                    'If Shift_count > 1 Then
                    '    ddlShift.Enabled = True
                    'Else
                    '    ddlShift.Enabled = False
                    'End If
                    'end


                    ViewState("Acd_Valid") = "1" 'if open for registration
                    'ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                Else


                    ddlStream.Items.Clear()
                    ddlTerm.Items.Clear()
                End If

            End Using

        Else

            Using AllGrade_Shift_reader As SqlDataReader = AccessStudentClass.getAllShift_4_Grade(Aca_Year, Curri, GEMSSchool, Grade)
                Dim di_Grade_Shift As ListItem
                Dim Shift_count As Integer
                ddlShift.Items.Clear()

                If AllGrade_Shift_reader.HasRows = True Then
                    While AllGrade_Shift_reader.Read
                        di_Grade_Shift = New ListItem(AllGrade_Shift_reader("SHF_DESCR"), AllGrade_Shift_reader("SHF_ID"))
                        ddlShift.Items.Add(di_Grade_Shift)
                        Shift_count += 1
                    End While
                    'code modified on 20th apr-2008
                    'If Shift_count > 1 Then
                    '    ddlShift.Enabled = True
                    'Else
                    '    ddlShift.Enabled = False
                    'End If
                    'end


                    ViewState("Acd_Valid") = "1" 'if open for registration
                    'ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                Else

                    ddlStream.Items.Clear()
                    ddlTerm.Items.Clear()
                End If

            End Using

        End If






    End Sub
    Sub GetStudent_Stream()

        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        Dim SHF_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If


        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If

        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = ""
        Else
            SHF_ID = ddlShift.SelectedItem.Value
        End If

        If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
            Using Student_Stream_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Only_Registrar(Aca_Year, Curri, GEMSSchool)
                Dim di_Stream As ListItem
                Dim Stream_Count As Integer
                ddlStream.Items.Clear()

                If Student_Stream_reader.HasRows = True Then
                    While Student_Stream_reader.Read
                        di_Stream = New ListItem(Student_Stream_reader("STM_DESCR"), Student_Stream_reader("STM_ID"))
                        ddlStream.Items.Add(di_Stream)
                        Stream_Count = Stream_Count + 1
                    End While
                    'Code Modified 20-apr-2008
                    'If Stream_Count > 1 Then
                    '    ddlStream.Enabled = True
                    'Else
                    '    ddlStream.Enabled = False
                    'End If
                    'End

                    ViewState("Acd_Valid") = "1" 'if open for registration
                Else

                    ddlTerm.Items.Clear()
                End If

            End Using


        Else
            Using Student_Stream_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Only(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID)
                Dim di_Stream As ListItem
                Dim Stream_Count As Integer
                ddlStream.Items.Clear()

                If Student_Stream_reader.HasRows = True Then
                    While Student_Stream_reader.Read
                        di_Stream = New ListItem(Student_Stream_reader("STM_DESCR"), Student_Stream_reader("STM_ID"))
                        ddlStream.Items.Add(di_Stream)
                        Stream_Count = Stream_Count + 1
                    End While
                    'Code Modified 20-apr-2008
                    'If Stream_Count > 1 Then
                    '    ddlStream.Enabled = True
                    'Else
                    '    ddlStream.Enabled = False
                    'End If
                    'End
                    ddlStream_SelectedIndexChanged(ddlStream, Nothing)
                    ViewState("Acd_Valid") = "1" 'if open for registration
                Else

                    ddlTerm.Items.Clear()
                End If

            End Using
        End If
    End Sub
    Sub GEMS_BSU_AcadStart()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty

        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If

        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        Using BSU_AcadStart_reader As SqlDataReader = AccessStudentClass.GetBSU_M_AcadStart(GEMSSchool, Aca_Year, Curri)



            While BSU_AcadStart_reader.Read
                txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", BSU_AcadStart_reader("ACD_STARTDT"))
            End While
        End Using

    End Sub
    Sub CheckTermDate()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim TRMID As String = String.Empty

        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value
        End If

        If ddlTerm.SelectedIndex = -1 Then
            TRMID = ""
        Else
            TRMID = ddlTerm.SelectedItem.Value
        End If
        Using Term_4ACD_reader As SqlDataReader = AccessStudentClass.GetTerm_CheckDate(Aca_Year, Curri, GEMSSchool, TRMID)

            If Term_4ACD_reader.HasRows = True Then
                While Term_4ACD_reader.Read

                    If IsDate(Term_4ACD_reader("Trm_startDate")) = True Then
                        ViewState("TRM_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("Trm_startDate")))).Replace("01/Jan/1900", "")
                    End If

                    If IsDate(Term_4ACD_reader("trm_endDate")) = True Then
                        ViewState("TRM_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("trm_endDate")))).Replace("01/Jan/1900", "")
                    End If


                End While


            End If
        End Using

    End Sub
    'getting the term for which currently opened BSU_ID academic year ID
    Sub GetTerm_4ACD()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim termDate()
        Dim i As Integer = 0
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If

        Using Term_4ACD_reader As SqlDataReader = AccessStudentClass.GetTerm_4ACD(Aca_Year, Curri, GEMSSchool)
            Dim di_Term As ListItem
            ddlTerm.Items.Clear()

            If Term_4ACD_reader.HasRows = True Then
                While Term_4ACD_reader.Read
                    di_Term = New ListItem(Term_4ACD_reader("TDescr"), Term_4ACD_reader("TRM_ID"))
                    ddlTerm.Items.Add(di_Term)
                    If IsDate(Term_4ACD_reader("Trm_startDate")) = True Then
                        ViewState("TRM_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("Trm_startDate")))).Replace("01/Jan/1900", "")
                        ReDim Preserve termDate(i)
                        termDate(i) = ViewState("TRM_STARTDT")
                        i = i + 1
                    End If

                    If IsDate(Term_4ACD_reader("trm_endDate")) = True Then


                        ViewState("TRM_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("trm_endDate")))).Replace("01/Jan/1900", "")
                    End If


                End While
            Else


                ViewState("TRM_STARTDT") = ""
                ViewState("TRM_ENDDT") = ""


            End If
        End Using
        Session("termSTDT") = termDate

        ddlTerm_SelectedIndexChanged(ddlTerm, Nothing)
    End Sub


    Sub callAll_fun_OnPageLoad()
        Try
            Call Populate_YearList()
            Call GetBusinessUnits_info_student()
            Call GetPRENURSERY()
            Call GetBusinessUnits_info_staff()
            Call GetReligion_info()
            'Call GetCity_info()
            Call GetCountry_info()
            Call GetNational_info()

            'Call GetTelePhone_info_Mobile("M")
            'Call GetTelePhone_info("L")
            Call GetCurriculum_Info()
            Call GetGrade_info()
            Call GetMain_Loc()
            Call GetSub_Main_Loc()
            Call GetSub_PickUp_point()
            Call GetCompany_Name()
            Call GetEmirate_info()
            Call AboutUs()
            Call bindLanguage_dropDown()
            Call bindETHNICITY()
            'Call GEMS_BSU_AcadStart()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "callAll_fun_OnPageLoad")
        End Try

    End Sub
    Private Sub bindLanguage_dropDown()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT LNG_ID,LNG_DESCR  FROM LANGUAGE_M order by LNG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFLang.DataSource = ds
        ddlFLang.DataTextField = "LNG_DESCR"
        ddlFLang.DataValueField = "LNG_ID"
        ddlFLang.DataBind()
        chkOLang.DataSource = ds
        chkOLang.DataTextField = "LNG_DESCR"
        chkOLang.DataValueField = "LNG_ID"
        chkOLang.DataBind()
        'ddlFLang.Items.Add(New ListItem("-", "0"))
        ddlFLang.Items.Insert(0, New ListItem("", ""))
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True
    End Sub
    Sub GetCompany_Name()
        Try

            Using GetComp_Name_reader As SqlDataReader = AccessStudentClass.GetCompany_Name()

                Dim di_Company As ListItem
                ddlCompany.Items.Clear()
                di_Company = New ListItem("Other", "0")
                ddlCompany.Items.Add(di_Company)
                If GetComp_Name_reader.HasRows = True Then
                    While GetComp_Name_reader.Read

                        di_Company = New ListItem(GetComp_Name_reader("comp_Name"), GetComp_Name_reader("comp_ID"))
                        ddlCompany.Items.Add(di_Company)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCompany_Name()")
        End Try
    End Sub
    'Getting Business Unit info
    Sub GetBusinessUnits_info_staff()
        Try
            Using AllStaff_BSU_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_staff()

                Dim di_Staff_BSU As ListItem
                ddlStaff_BSU.Items.Clear()

                If AllStaff_BSU_reader.HasRows = True Then
                    While AllStaff_BSU_reader.Read

                        di_Staff_BSU = New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id"))
                        ddlStaff_BSU.Items.Add(di_Staff_BSU)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub
    'Getting Religion info
    'code modified on 12/AUG/2008
    Sub GetReligion_info()
        Try
            Dim BSU_ID As String = ddlGEMSSchool.SelectedItem.Value

            Using AllReligion_reader As SqlDataReader = AccessStudentClass.GetStud_BSU_Religion(BSU_ID)

                Dim di_Religion As ListItem
                ddlReligion.Items.Clear()
                ddlReligion.Items.Add(New ListItem("Religion", "-1"))
                If AllReligion_reader.HasRows = True Then
                    While AllReligion_reader.Read

                        di_Religion = New ListItem(AllReligion_reader("RLG_DESCR"), AllReligion_reader("RLG_ID"))
                        ddlReligion.Items.Add(di_Religion)

                    End While
                    ddlReligion.Items.FindByValue("OTH").Selected = True
                Else
                    di_Religion = New ListItem("", "")
                    ddlReligion.Items.Add(di_Religion)
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetReligion_info")
        End Try
    End Sub
    Sub GetCountry_info()
        Try


            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem
                ddlCountry.Items.Clear()
                ddlPre_Country.Items.Clear()
                ddlCountry_Pri.Items.Clear()
                ddlOverSeas_Add_Country.Items.Clear()

                ddlPre_Country.Items.Add(New ListItem("", ""))
                ddlCountry.Items.Add(New ListItem("", ""))
                ddlCountry_Pri.Items.Add(New ListItem("", ""))
                ddlOverSeas_Add_Country.Items.Add(New ListItem("", ""))

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))

                        ddlPre_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlCountry_Pri.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlOverSeas_Add_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                    End While
                    ddlCountry.Items.FindByText("-").Selected = True
                End If

                ddlCountry_Pri.ClearSelection()
                ddlCountry_Pri.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True


            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    'Getting national info
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = AccessStudentClass.GetNational()
                Dim di_National As ListItem
                ddlPri_National1.Items.Clear()
                ddlPri_National2.Items.Clear()
                ddlNational.Items.Clear()



                ddlPri_National1.Items.Add(New ListItem("", ""))
                ddlPri_National2.Items.Add(New ListItem("", ""))
                ddlNational.Items.Add(New ListItem("", ""))


                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()
                        di_National = New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID"))
                        ddlPri_National1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlPri_National2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlNational.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))

                    End While
                    ddlNational.Items.FindByText("-").Selected = True
                    ddlPri_National1.Items.FindByText("-").Selected = True
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info")
        End Try
    End Sub
    ' Getting Emirates info
    Sub GetEmirate_info()
        Try
            Using AllEmirate_reader As SqlDataReader = AccessStudentClass.GetEmirate()
                Dim di_Emirate As ListItem
                ddlEmirate.Items.Clear()

                If AllEmirate_reader.HasRows = True Then
                    While AllEmirate_reader.Read
                        di_Emirate = New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE"))
                        ddlEmirate.Items.Add(di_Emirate)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmirate_info")
        End Try
    End Sub
    Private Sub bindETHNICITY()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select EC_ID,EC_NAME from dbo.ETHNICITY_M order by EC_NAME"
        Dim sqlread As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

        If sqlread.HasRows = True Then
            ddlEthnicity.DataSource = sqlread
            ddlEthnicity.DataTextField = "EC_NAME"
            ddlEthnicity.DataValueField = "EC_ID"
            ddlEthnicity.DataBind()
        End If
        ddlEthnicity.Items.Add(New ListItem("Other", "0"))
        ddlEthnicity.ClearSelection()
        'ddlEthnicity.Items.FindByText("Other").Selected = True
    End Sub
    Sub GetCurriculum_Info()
        Try
            Using AllCurriculum_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M()

                Dim di_Curriculum As ListItem
                ddlPre_Curriculum.Items.Clear()

                If AllCurriculum_reader.HasRows = True Then
                    While AllCurriculum_reader.Read
                        di_Curriculum = New ListItem(AllCurriculum_reader("CLM_DESCR"), AllCurriculum_reader("CLM_ID"))
                        ddlPre_Curriculum.Items.Add(di_Curriculum)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub
    'Getting Grade Info
    Sub GetPRENURSERY()
        'ddlPreSchool_Nursery
        Try
            Dim defaultNursery As String = "--"


            Dim di As ListItem
            Using PRENURSERYreader As SqlDataReader = AccessStudentClass.GetPRENURSERY_M()
                ddlPreSchool_Nursery.Items.Clear()


                If PRENURSERYreader.HasRows = True Then
                    While PRENURSERYreader.Read
                        di = New ListItem(PRENURSERYreader("SCH_DESCR"), PRENURSERYreader("SCH_CODE"))
                        ddlPreSchool_Nursery.Items.Add(di)
                    End While
                End If
            End Using

            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlPreSchool_Nursery.Items.Count - 1
                If ddlPreSchool_Nursery.Items(ItemTypeCounter).Value = "4" Then
                    ddlPreSchool_Nursery.SelectedIndex = ItemTypeCounter
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try





    End Sub
    'SCH_CODE,SCH_DESCR
    Sub GetGrade_info()
        Try


            Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetGrade_M()

                'Dim di_Grade As ListItem
                ddlPre_Grade.Items.Clear()
                If AllGrade_reader.HasRows = True Then
                    While AllGrade_reader.Read
                        ' di_Grade = New ListItem(AllGrade_reader("GRD_ID"), AllGrade_reader("GRD_ID"))
                        ddlPre_Grade.Items.Add(New ListItem(AllGrade_reader("GRD_ID"), AllGrade_reader("GRD_ID")))

                    End While
                End If
            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetGrade_info")
        End Try
    End Sub
    Sub GetMain_Loc()
        Try

            Dim GEMSSchool As String = String.Empty

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If

            Using Main_Loc_reader As SqlDataReader = AccessStudentClass.getMain_Location(GEMSSchool)
                Dim di_Main_Loc As ListItem
                ddlMainLocation.Items.Clear()
                ddlMainLocation.Items.Add(New ListItem("", ""))
                If Main_Loc_reader.HasRows = True Then
                    While Main_Loc_reader.Read
                        di_Main_Loc = New ListItem(Main_Loc_reader("LOC_DESCRIPTION"), Main_Loc_reader("LOC_ID"))
                        ddlMainLocation.Items.Add(di_Main_Loc)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetMain_Loc")

        End Try
    End Sub
    Sub GetSub_Main_Loc()
        Try
            Dim MainLocation As String = String.Empty

            Dim GEMSSchool As String = String.Empty

            If ddlMainLocation.SelectedIndex = -1 Then
                MainLocation = ""
            Else
                MainLocation = ddlMainLocation.SelectedItem.Value
            End If

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            Using Sub_Main_Loc_reader As SqlDataReader = AccessStudentClass.getSubMain_Location(GEMSSchool, MainLocation)
                Dim di_Sub_Main_Loc As ListItem
                ddlSubLocation.Items.Clear()
                ddlSubLocation.Items.Add(New ListItem("", ""))
                If Sub_Main_Loc_reader.HasRows = True Then
                    While Sub_Main_Loc_reader.Read
                        di_Sub_Main_Loc = New ListItem(Sub_Main_Loc_reader("SBL_DESCRIPTION"), Sub_Main_Loc_reader("SBL_ID"))
                        ddlSubLocation.Items.Add(di_Sub_Main_Loc)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetSub_Main_Loc")

        End Try
    End Sub
    Sub GetSub_PickUp_point()
        Try

            Dim SubLocation As String = String.Empty

            Dim GEMSSchool As String = String.Empty

            If ddlSubLocation.SelectedIndex = -1 Then
                SubLocation = ""
            Else
                SubLocation = ddlSubLocation.SelectedItem.Value
            End If

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            Using PickUp_point_reader As SqlDataReader = AccessStudentClass.getPickUP_Points(GEMSSchool, SubLocation)
                Dim di_PickUp As ListItem
                ddlPickup.Items.Clear()
                ' di_PickUp = New ListItem("Other", "")
                ddlPickup.Items.Add(New ListItem("", ""))
                If PickUp_point_reader.HasRows = True Then
                    While PickUp_point_reader.Read
                        di_PickUp = New ListItem(PickUp_point_reader("PNT_DESCRIPTION"), PickUp_point_reader("PNT_ID"))
                        ddlPickup.Items.Add(di_PickUp)
                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Sub_PickUp_point")

        End Try
    End Sub
    Sub ControlTable_row()
        If (ViewState("Table_row") = "KG1") Or (ViewState("Table_row") = "PK") Or (ViewState("Table_row") = "PN1") Then
            h_Grade_KG1.Value = "1"

            ViewState("TABLE_School_Pre").Rows.Clear()
            ltSchoolType.Text = "Current School"
            ViewState("TABLE_School_Pre").Rows.Clear()
            ViewState("id") = 1
            gridbind()
            trSch1.Visible = True
            trSch2.Visible = False
            trSch3.Visible = False
            trSch4.Visible = False
            trSch5.Visible = False
            trSch6.Visible = False
            trSch7.Visible = False
            trSch8.Visible = False
            trSch9.Visible = False
            trSch10.Visible = False
            trSch11.Visible = False
        Else
            '  ddlGemsGr_SelectedIndexChanged(ddlGemsGr, Nothing)
            h_Grade_KG1.Value = "2"
            ddlGemsGr.Visible = True
            'ddlGemsGr.ClearSelection()
            If Not ddlPre_Grade.Items.FindByText(GetPrevGrade()) Is Nothing Then
                ddlPre_Grade.ClearSelection()
                ddlPre_Grade.Items.FindByText(GetPrevGrade()).Selected = True
            End If
            '' txtPre_School.Text = ""
            'txtPre_School.Visible = False
            'rfv_Pre_school.Visible = False



            ViewState("rowstate") = "1"
            trSch1.Visible = False
            trSch2.Visible = True
            trSch3.Visible = True
            trSch4.Visible = True
            trSch5.Visible = True
            trSch6.Visible = True
            trSch7.Visible = True
            trSch8.Visible = True
            trSch9.Visible = True
            trSch10.Visible = True
            trSch11.Visible = True
            '  rfvFeeID_App.Visible = False


        End If

    End Sub




#End Region
#Region "For Selected Index change event"
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ' Call GetTerm_4ACD(ddlAca_Year.SelectedValue, ddlGrade.SelectedValue)
        'check if grade is open or not
        ViewState("Table_row") = ddlGrade.SelectedItem.Value

        If "PK|KG1".Contains(ddlGrade.SelectedValue) Then
            currPre_SchTitle.InnerText = "Current School Details"
        Else
            currPre_SchTitle.InnerText = "Current /Previous School Details"
        End If


        Call ControlTable_row()
        'fifth shift is called
        Call GetAllGrade_Shift()

        ' Call EnquryOpen_for()

        ' Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        Call GetStudent_Stream()
        ''  Call GetSchoolButton_Enable()
        'forth Grade is called
        Call GetAllGrade_Bsu()
        Call GetTerm_4ACD()
        '  Call ControlTable_row()

        Call GEMS_BSU_AcadStart()
    End Sub
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged

        Dim trm_ID As String = String.Empty

        If ddlTerm.SelectedIndex = -1 Then
            trm_ID = ""
        Else
            trm_ID = ddlTerm.SelectedItem.Value
        End If
        Call GEMS_BSU_AcadStart()
        If Not Session("termSTDT") Is Nothing Then
            If ddlTerm.SelectedIndex <> -1 Then
                txtDOJ.Text = Session("termSTDT")(ddlTerm.SelectedIndex)
            End If
        End If

        ' Dim sdate As String = AccessStudentClass.GetTermDate(trm_ID)
        ' txtDOJ.Text = ""
        'txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
    End Sub
#End Region
    Public Function GetPrevGrade() As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select GRD_DISPLAY from GRADE_M where GRD_DISPLAYORDER=(SELECT GRD_DISPLAYORDER FROM GRADE_M where GRD_DISPLAY='" & ddlGrade.SelectedItem.Value & "')-1"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString


    End Function
    Sub SiblingInfo()
        Try
            Dim mobNumber As String()
            Dim hNumber As String()
            Dim oNumber As String()
            Dim fNumber As String()
            Using sibl_reader As SqlDataReader = AccessStudentClass.GetSiblingDetails(txtSib_ID.Text, ddlSib_BSU.SelectedValue)
                While sibl_reader.Read
                    If sibl_reader("STU_PRIMARYCONTACT") = "F" Then

                        rdPri_Father.Checked = True
                        txtPri_Fname.Text = sibl_reader("STS_FFIRSTNAME")
                        txtPri_Mname.Text = sibl_reader("STS_FMIDNAME")
                        txtPri_Lname.Text = sibl_reader("STS_FLASTNAME")
                        If Not ddlPri_National1.Items.FindByValue(sibl_reader("STS_FNATIONALITY")) Is Nothing Then
                            ddlPri_National1.ClearSelection()
                            ddlPri_National1.Items.FindByValue(sibl_reader("STS_FNATIONALITY")).Selected = True
                        End If
                        If Not ddlPri_National2.Items.FindByValue(sibl_reader("STS_FNATIONALITY2")) Is Nothing Then
                            ddlPri_National2.ClearSelection()
                            ddlPri_National2.Items.FindByValue(sibl_reader("STS_FNATIONALITY2")).Selected = True
                        End If
                        mobNumber = sibl_reader("STS_FMOBILE").ToString.Split(" ")
                        If mobNumber.Length = 3 Then
                            txtM_Country.Text = mobNumber(0)
                            txtM_Area.Text = mobNumber(1)
                            txtMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtM_Area.Text = mobNumber(0)
                            txtMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMobile_Pri.Text = mobNumber(0)
                        End If

                        txtPoBox_Pri.Text = sibl_reader("STS_FPRMPOBOX")
                        txtEmail_Pri.Text = sibl_reader("STS_FEMAIL").ToString.ToLower
                        txtAdd1_overSea.Text = sibl_reader("STS_FPRMADDR1")
                        txtAdd2_overSea.Text = sibl_reader("STS_FPRMADDR2")


                        If Not ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_FPRMCOUNTRY")) Is Nothing Then
                            ddlOverSeas_Add_Country.ClearSelection()
                            ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_FPRMCOUNTRY")).Selected = True
                        End If

                        txtStreet.Text = sibl_reader("STS_FCOMADDR1")
                        txtArea.Text = sibl_reader("STS_FCOMADDR2")
                        txtCity_pri.Text = sibl_reader("STS_FCOMCITY")
                        'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_FCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_FCOMCITY")).Selected = True
                        'End If

                        If Not ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_FCOMCOUNTRY")) Is Nothing Then
                            ddlCountry_Pri.ClearSelection()
                            ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_FCOMCOUNTRY")).Selected = True
                        End If

                        hNumber = sibl_reader("STS_FRESPHONE").ToString.Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtHPhone_Country.Text = hNumber(0)
                            txtHPhone_Area.Text = hNumber(1)
                            txtHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtHPhone_Area.Text = hNumber(0)
                            txtHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtHPhone.Text = hNumber(0)
                        End If

                        oNumber = sibl_reader("STS_FOFFPHONE").ToString.Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtOPhone_Country.Text = oNumber(0)
                            txtOPhone_Area.Text = oNumber(1)
                            txtOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtOPhone_Area.Text = oNumber(0)
                            txtOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtOPhone.Text = oNumber(0)
                        End If


                        fNumber = sibl_reader("STS_FFAX").ToString.Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtFaxNo_country.Text = fNumber(0)
                            txtFaxNo_Area.Text = fNumber(1)
                            txtFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFaxNo_Area.Text = fNumber(0)
                            txtFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFaxNo.Text = fNumber(0)
                        End If
                        txtPoboxLocal.Text = sibl_reader("STS_FCOMPOBOX")
                        txtOccup.Text = sibl_reader("STS_FOCC")
                        If Not ddlCompany.Items.FindByValue(sibl_reader("STS_F_COMP_ID")) Is Nothing Then
                            ddlCompany.ClearSelection()
                            ddlCompany.Items.FindByValue(sibl_reader("STS_F_COMP_ID")).Selected = True
                        End If


                    ElseIf sibl_reader("STU_PRIMARYCONTACT") = "M" Then

                        rbPri_Mother.Checked = True
                        txtPri_Fname.Text = sibl_reader("STS_MFIRSTNAME")
                        txtPri_Mname.Text = sibl_reader("STS_MMIDNAME")
                        txtPri_Lname.Text = sibl_reader("STS_MLASTNAME")
                        If Not ddlPri_National1.Items.FindByValue(sibl_reader("STS_MNATIONALITY")) Is Nothing Then
                            ddlPri_National1.ClearSelection()
                            ddlPri_National1.Items.FindByValue(sibl_reader("STS_MNATIONALITY")).Selected = True
                        End If
                        If Not ddlPri_National2.Items.FindByValue(sibl_reader("STS_MNATIONALITY2")) Is Nothing Then
                            ddlPri_National2.ClearSelection()
                            ddlPri_National2.Items.FindByValue(sibl_reader("STS_MNATIONALITY2")).Selected = True
                        End If

                        mobNumber = sibl_reader("STS_MMOBILE").ToString.Split(" ")
                        If mobNumber.Length = 3 Then
                            txtM_Country.Text = mobNumber(0)
                            txtM_Area.Text = mobNumber(1)
                            txtMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtM_Area.Text = mobNumber(0)
                            txtMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMobile_Pri.Text = mobNumber(0)
                        End If

                        txtPoBox_Pri.Text = sibl_reader("STS_MPRMPOBOX")
                        txtEmail_Pri.Text = sibl_reader("STS_MEMAIL").ToString.ToLower
                        txtAdd1_overSea.Text = sibl_reader("STS_MPRMADDR1")
                        txtAdd2_overSea.Text = sibl_reader("STS_MPRMADDR2")

                        If Not ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_MPRMCOUNTRY")) Is Nothing Then
                            ddlOverSeas_Add_Country.ClearSelection()
                            ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_MPRMCOUNTRY")).Selected = True
                        End If
                        txtStreet.Text = sibl_reader("STS_MCOMADDR1")
                        txtArea.Text = sibl_reader("STS_MCOMADDR2")
                        txtCity_pri.Text = Convert.ToString(sibl_reader("STS_MCOMCITY"))

                        'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_MCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_MCOMCITY")).Selected = True
                        'End If

                        If Not ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_MCOMCOUNTRY")) Is Nothing Then
                            ddlCountry_Pri.ClearSelection()
                            ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_MCOMCOUNTRY")).Selected = True
                        End If

                        hNumber = sibl_reader("STS_MRESPHONE").Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtHPhone_Country.Text = hNumber(0)
                            txtHPhone_Area.Text = hNumber(1)
                            txtHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtHPhone_Area.Text = hNumber(0)
                            txtHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtHPhone.Text = hNumber(0)
                        End If

                        oNumber = sibl_reader("STS_MOFFPHONE").Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtOPhone_Country.Text = oNumber(0)
                            txtOPhone_Area.Text = oNumber(1)
                            txtOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtOPhone_Area.Text = oNumber(0)
                            txtOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtOPhone.Text = oNumber(0)
                        End If



                        fNumber = sibl_reader("STS_MFAX").Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtFaxNo_country.Text = fNumber(0)
                            txtFaxNo_Area.Text = fNumber(1)
                            txtFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFaxNo_Area.Text = fNumber(0)
                            txtFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFaxNo.Text = fNumber(0)
                        End If

                        txtPoboxLocal.Text = sibl_reader("STS_MCOMPOBOX")
                        txtOccup.Text = sibl_reader("STS_MOCC")
                        If Not ddlCompany.Items.FindByValue(sibl_reader("STS_M_COMP_ID")) Is Nothing Then
                            ddlCompany.ClearSelection()
                            ddlCompany.Items.FindByValue(sibl_reader("STS_M_COMP_ID")).Selected = True
                        End If

                    ElseIf sibl_reader("STU_PRIMARYCONTACT") = "G" Then

                        rdPri_Guard.Checked = True
                        txtPri_Fname.Text = sibl_reader("STS_GFIRSTNAME")
                        txtPri_Mname.Text = sibl_reader("STS_GMIDNAME")
                        txtPri_Lname.Text = sibl_reader("STS_GLASTNAME")
                        If Not ddlPri_National1.Items.FindByValue(sibl_reader("STS_GNATIONALITY")) Is Nothing Then
                            ddlPri_National1.ClearSelection()
                            ddlPri_National1.Items.FindByValue(sibl_reader("STS_GNATIONALITY")).Selected = True
                        End If
                        If Not ddlPri_National2.Items.FindByValue(sibl_reader("STS_GNATIONALITY2")) Is Nothing Then
                            ddlPri_National2.ClearSelection()
                            ddlPri_National2.Items.FindByValue(sibl_reader("STS_GNATIONALITY2")).Selected = True
                        End If

                        mobNumber = sibl_reader("STS_GMOBILE").ToString.Split(" ")
                        If mobNumber.Length = 3 Then
                            txtM_Country.Text = mobNumber(0)
                            txtM_Area.Text = mobNumber(1)
                            txtMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtM_Area.Text = mobNumber(0)
                            txtMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMobile_Pri.Text = mobNumber(0)
                        End If

                        txtPoBox_Pri.Text = sibl_reader("STS_GCOMPOBOX")
                        txtEmail_Pri.Text = sibl_reader("STS_GEMAIL").ToString.ToLower
                        txtAdd1_overSea.Text = sibl_reader("STS_GPRMADDR1")
                        txtAdd2_overSea.Text = sibl_reader("STS_GPRMADDR2")
                        txtOverSeas_Add_City.Text = sibl_reader("STS_FPRMCITY")

                        If Not ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_GPRMCOUNTRY")) Is Nothing Then
                            ddlOverSeas_Add_Country.ClearSelection()
                            ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_GPRMCOUNTRY")).Selected = True
                        End If

                        txtStreet.Text = sibl_reader("STS_GCOMADDR1")
                        txtArea.Text = sibl_reader("STS_GCOMADDR2")
                        txtCity_pri.Text = sibl_reader("STS_GCOMCITY")
                        'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_GCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_GCOMCITY")).Selected = True
                        'End If

                        If Not ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_GCOMCOUNTRY")) Is Nothing Then
                            ddlCountry_Pri.ClearSelection()
                            ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_GCOMCOUNTRY")).Selected = True
                        End If

                        hNumber = sibl_reader("STS_GRESPHONE").ToString.Split(" ")
                        If hNumber.Length = 3 Then
                            txtHPhone_Country.Text = hNumber(0)
                            txtHPhone_Area.Text = hNumber(1)
                            txtHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtHPhone_Area.Text = hNumber(0)
                            txtHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtHPhone.Text = hNumber(0)
                        End If

                        oNumber = sibl_reader("STS_GOFFPHONE").ToString.Split(" ")
                        If oNumber.Length = 3 Then
                            txtOPhone_Country.Text = oNumber(0)
                            txtOPhone_Area.Text = oNumber(1)
                            txtOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtOPhone_Area.Text = oNumber(0)
                            txtOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtOPhone.Text = oNumber(0)
                        End If

                        fNumber = sibl_reader("STS_GFAX").ToString.Split(" ")
                        If fNumber.Length = 3 Then
                            txtFaxNo_country.Text = fNumber(0)
                            txtFaxNo_Area.Text = fNumber(1)
                            txtFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFaxNo_Area.Text = fNumber(0)
                            txtFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFaxNo.Text = fNumber(0)
                        End If

                        txtPoboxLocal.Text = sibl_reader("STS_GCOMPOBOX")
                        txtOccup.Text = sibl_reader("STS_GOCC")
                        If Not ddlCompany.Items.FindByValue(sibl_reader("STS_G_COMP_ID")) Is Nothing Then
                            ddlCompany.ClearSelection()
                            ddlCompany.Items.FindByValue(sibl_reader("STS_G_COMP_ID")).Selected = True
                        End If

                    End If
                End While

            End Using
        Catch ex As Exception

        End Try
    End Sub



    Function ValidStaff_IDCheck() As Boolean
        Dim BSU_ID As String
        Dim Flag As Boolean
        If ddlStaff_BSU.SelectedIndex = -1 Then
            BSU_ID = ""
        Else
            BSU_ID = ddlStaff_BSU.SelectedItem.Value

        End If


        Using staffIDdatareader As SqlDataReader = AccessStudentClass.CheckStaffID_BSU(BSU_ID)
            While staffIDdatareader.Read
                Flag = Convert.ToBoolean(staffIDdatareader("BSU_bCheck_StaffID"))
            End While
        End Using
        ValidStaff_IDCheck = Flag
    End Function
    'Function Check_KG1() As Boolean
    '    Dim Check_Flag As Boolean = True
    '    Dim ErrorString As String
    '    Dim DateValid_L_Date As Date
    '    ErrorString = "<UL>"
    '    If (ddlGrade.SelectedItem.Value = "KG1" Or ddlGrade.SelectedItem.Value = "PK") Then

    '        If ddlPreSchool_Nursery.SelectedItem.Text = "--" Then
    '            ErrorString = ErrorString & "<LI>Select current school name" & "</UL>"
    '            Check_Flag = False
    '        End If
    '    Else

    '        If ddlPre_Curriculum.SelectedItem.Text = "--" Then
    '            ErrorString = ErrorString & "<LI>Curriculum needs to be selected"
    '            Check_Flag = False
    '        End If
    '        'If txtLast_Att.Text.Trim = "" Then
    '        '    ErrorString = ErrorString & "<LI>Last Attendance Date required"

    '        '    Check_Flag = False
    '        'Else
    '        If txtLast_Att.Text.Trim <> "" Then
    '            If IsDate(txtLast_Att.Text) = False Then
    '                ErrorString = ErrorString & "<LI>Last Attendance Date is not a valid date"

    '                Check_Flag = False
    '            End If
    '        ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = True Then
    '            'code modified 4 date
    '            Dim strfDate As String = txtLast_Att.Text.Trim
    '            Dim str_err As String = DateFunctions.checkdate(strfDate)
    '            If str_err <> "" Then
    '                Check_Flag = False
    '                ErrorString = ErrorString & "<LI>Last Attendance Date is not a valid date"
    '            Else
    '                txtLast_Att.Text = strfDate
    '                DateValid_L_Date = Date.ParseExact(txtLast_Att.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
    '                If IsDate(DateValid_L_Date) = False Then
    '                    ErrorString = ErrorString & "<LI>Last Attendance Date is not a valid date"
    '                    Check_Flag = False
    '                End If
    '            End If
    '        ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = False Then
    '            ErrorString = ErrorString & "<LI>Last Attendance Date format is invalid"

    '            Check_Flag = False
    '        End If
    '    End If
    '    lblError.Text = ErrorString
    '    Check_KG1 = Check_Flag
    'End Function
    Sub OpenStaff_BSU_Populate(ByVal BSU_IDs As String)
        'Dim Bsu As String = Replace(BSU_IDs, "|", ",")
        'Bsu = Bsu.TrimEnd(",")
        'Bsu = Bsu.TrimStart(",")
        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Staff(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then

            ddlStaff_BSU.Items.Clear()
            ddlStaff_BSU.DataSource = ds1.Tables(0)
            ddlStaff_BSU.DataTextField = "BSU_NAME"
            ddlStaff_BSU.DataValueField = "BSU_ID"
            ddlStaff_BSU.DataBind()
        End If
    End Sub
    Sub OpenSib_BSU_Populate(ByVal BSU_IDs As String)

        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Sib(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then
            ddlSib_BSU.Items.Clear()
            ddlSib_BSU.DataSource = ds1.Tables(0)
            ddlSib_BSU.DataTextField = "BSU_NAME"
            ddlSib_BSU.DataValueField = "BSU_ID"
            ddlSib_BSU.DataBind()
        End If

    End Sub
    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Length < 1 Then
            Return False
        Else
            Dim strRegex As String = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" '"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            Dim re As New Regex(strRegex)
            If re.IsMatch(inputEmail) Then
                Return (True)
            Else
                Return (False)
            End If
        End If
    End Function
    Protected Sub ddlGEMSSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGEMSSchool.SelectedIndexChanged
        Call CUSTOM_LABEL()
        Call GetMain_Loc()
        ddlMainLocation_SelectedIndexChanged(ddlMainLocation, Nothing)
        'Second Curriculum 
        Call BSU_Enq_validation()
        Call CURRICULUM_BSU_4_Student()
        ' Call GetTerm_4ACD()
        '  Call GetSchoolButton_Enable()
        Call GetReligion_info()
        'new code added
        If Not ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedItem.Value) Is Nothing Then
            ddlSib_BSU.ClearSelection()
            ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedItem.Value).Selected = True
            ddlSib_BSU.Enabled = False
        Else
            ddlSib_BSU.Enabled = False
        End If
        If Not ddlGemsGr.Items.FindByText("OTHER") Is Nothing Then
            ddlGemsGr.ClearSelection()
            ddlGemsGr.Items.FindByText("OTHER").Selected = True
        End If
        ltAboutUs.Text = StrConv(ddlGEMSSchool.SelectedItem.Text, VbStrConv.ProperCase) & "? [please tick]"
    End Sub
    Private Sub CUSTOM_LABEL()

        Dim BSU_ID As String = ddlGEMSSchool.SelectedValue
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", BSU_ID)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "GETENQUIRY_CUSTOM_LABEL", PARAM)
            If DATAREADER.HasRows Then
                While DATAREADER.Read

                    ltGrade_lb.Text = Convert.ToString(DATAREADER("ECL_GRADE"))
                    ltStream_lb.Text = Convert.ToString(DATAREADER("ECL_STREAM"))
                    If Convert.ToBoolean(DATAREADER("ECL_bSHOWMEAL")) Then
                        trStm.Visible = True
                        ddlShift.Visible = False
                    Else
                        trStm.Visible = False
                        ddlShift.Visible = False
                    End If

                End While

            Else
                ltGrade_lb.Text = "Grade"

                ltStream_lb.Text = "Stream"
                trStm.Visible = False

            End If

        End Using


    End Sub
    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        'third Academic year
        Call GetAcademicYear_Bsu()
        ' Call GetTerm_4ACD()
        'Call GetSchoolButton_Enable()
        'new code added
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        'sixth stream is called
        Call GetStudent_Stream()
        '  Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Call GetAllGrade_Bsu()
        Call GetTerm_4ACD()
        Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlMainLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMainLocation.SelectedIndexChanged
        Call GetSub_Main_Loc()
        Call GetSub_PickUp_point()
    End Sub
    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        Call GetSub_PickUp_point()

        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
        '               "<script language=javascript>fill_Sibls();</script>")
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
        '                            "<script language=javascript>fill_Tran();</script>")


    End Sub
    Sub Populate_YearList()
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 40 To DateTime.Now.Year + 5
            ddlExYear.Items.Add(intYear)
        Next
        ddlExYear.Items.FindByValue(DateTime.Now.Year).Selected = True
    End Sub
    Protected Sub rbRelocAgent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRelocAgent.CheckedChanged
        If rbRelocAgent.Checked = True Then
            tbReloc.Visible = True
        End If

    End Sub
    Protected Sub rbParentF_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbParentF.CheckedChanged
        If rbParentF.Checked = True Then
            tbReloc.Visible = False
        End If

    End Sub
#Region "Radio Button events"
    Protected Sub rdFather_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdPri_Father.CheckedChanged
        ddlCont_Status.SelectedIndex = 0
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                                 "<script language=javascript>fill_Sib();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                               "<script language=javascript>fill_Staff();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                               "<script language=javascript>fill_Stud();</script>")
    End Sub
    Protected Sub rbPri_Mother_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPri_Mother.CheckedChanged
        ddlCont_Status.SelectedIndex = 1
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                                 "<script language=javascript>fill_Sib();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                               "<script language=javascript>fill_Staff();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                               "<script language=javascript>fill_Stud();</script>")
    End Sub
    Protected Sub rdPri_Guard_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdPri_Guard.CheckedChanged
        ddlCont_Status.SelectedIndex = 0
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                                 "<script language=javascript>fill_Sib();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                               "<script language=javascript>fill_Staff();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                               "<script language=javascript>fill_Stud();</script>")
    End Sub

#End Region
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        Call enable_panel()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Sub enable_panel()
        pnlCurrent.Enabled = True
        pnlMain.Enabled = True
        pnlHeader.Enabled = True
        pnlOther.Enabled = True
        pnlParent.Enabled = True
        pnlPassPort.Enabled = True
        pnlTrans.Enabled = True

    End Sub
    Sub disable_panel()
        pnlCurrent.Enabled = False
        pnlMain.Enabled = False
        pnlHeader.Enabled = False
        pnlOther.Enabled = False
        pnlParent.Enabled = False
        pnlPassPort.Enabled = False
        pnlTrans.Enabled = False

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If ViewState("datamode") = "add" Then
            '    'disable_panel()
            '    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            'Else
            Response.Redirect(ResolveUrl("~/Students/EnquiryDashboard.aspx?MainMnu_code=LjcF1BVVHbk%3d&datamode=Zo4HhpVNpXc%3d"))
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub txtSib_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        validate_Sib_ID()
    End Sub
    Sub validate_Sib_ID()
        Dim CommStr As String = String.Empty
        CommStr = "<UL>"
        If chkSibling.Checked = True Then
            If Trim(txtSib_Name.Text) = "" Then
                CommStr = CommStr & "<LI>Sibling name cannot be left empty"
            End If
            If Trim(txtSib_ID.Text) = "" Then
                CommStr = CommStr & "<LI>Sibling Fee ID required"
            Else
                Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)
                    If ValidSibl_reader.HasRows = False Then
                        CommStr = CommStr & "<LI>Sibling ID is not valid for the selected School"
                    End If
                End Using
            End If
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                                            "<script language=javascript>fill_Sib();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                   "<script language=javascript>fill_Staff();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                   "<script language=javascript>fill_Stud();</script>")
            lblError.Text = CommStr & "</UL>"

        End If

    End Sub


    Function Final_validation() As Boolean
        Dim Final_error As Boolean = True
        Dim DateValid_DOB As Date
        Dim DateValid_PIss_D As Date
        Dim DateValid_PExp_D As Date
        'Dim DateValid_VIss_D As Date
        'Dim DateValid_VExp_D As Date

        Dim commString As String = "The following fields needs to be validated: <UL>"

        'Select Schools validation
        Dim ChkCounter As Integer = 0
        If ddlAca_Year.SelectedIndex = -1 Or ddlGrade.SelectedIndex = -1 Or ddlCurri.SelectedIndex = -1 Or ddlShift.SelectedIndex = -1 Then    'And ddlTerm.Items.Count > 0 
            commString = commString & "<LI>Registration is CLOSED for this Search query"
            Final_error = False
        End If
        If txtDOJ.Text.Trim <> "" Then
            Dim strfDate As String = txtDOJ.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                Final_error = False
                commString = commString & "<LI>Invalid tentative date of join"
            Else
                txtDOJ.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtDOJ.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                'check for the leap year date
                If Not IsDate(dateTime1) Then
                    Final_error = False
                    commString = commString & "<LI>Invalid tentative date of join"
                Else
                    Call CheckTermDate()
                    Dim TRM_SDate As Date = ViewState("TRM_STARTDT")
                    Dim TRM_EDate As Date = ViewState("TRM_ENDDT")
                    Dim TENT_date As Date = txtDOJ.Text
                    If Not (TENT_date >= TRM_SDate And TENT_date <= TRM_EDate) Then
                        commString = commString & "<LI>Tentative date of join  must be with in the Term Date"
                        Final_error = False
                    End If
                End If
            End If
        End If

        If Trim(txtFname.Text) <> "" Then
            If Not Regex.Match(txtFname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Student first"
                Final_error = False
            End If
        End If
        If Trim(txtMname.Text) <> "" Then
            If Len(txtMname.Text) = 1 Then
                If Not Regex.Match(txtMname.Text, "^([a-zA-Z])").Success Then
                    commString = commString & "<LI>Invalid Student middle name"
                    Final_error = False
                End If
            Else
                If Not Regex.Match(txtMname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                    commString = commString & "<LI>Invalid Student middle name"
                    Final_error = False
                End If
            End If
        End If


        If Trim(txtLname.Text) <> "" Then
            If Len(txtLname.Text) = 1 Then
                If Not Regex.Match(txtLname.Text, "^([a-zA-Z])").Success Then
                    commString = commString & "<LI>Invalid Student last name"
                    Final_error = False
                End If
            Else
                If Not Regex.Match(txtLname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                    commString = commString & "<LI>Invalid Student last name"
                    Final_error = False
                End If
            End If
        End If


        If chkSibling.Checked = True Then
            If Trim(txtSib_Name.Text) = "" Then
                commString = commString & "<LI>Sibling name cannot be left empty"
            End If
            If Trim(txtSib_ID.Text) = "" Then
                commString = commString & "<LI>Sibling Fee ID required"
            Else
                Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)
                    If ValidSibl_reader.HasRows = False Then
                        commString = commString & "<LI>Sibling ID is not valid for the selected School"
                    End If
                End Using
            End If
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
            '                                                "<script language=javascript>fill_Sib();</script>")
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
            '                       "<script language=javascript>fill_Staff();</script>")
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
            '                       "<script language=javascript>fill_Stud();</script>")
        End If



        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If



        'If Enq_hash.ContainsKey("SFN") = False Then
        '    If txtFname.Text.Trim = "" Then
        '        commString = commString & "<LI>Student name cannot be left empty"

        '        Final_error = False
        '    End If
        'End If


        'If Enq_hash.ContainsKey("SRE") = False Then

        '    If ddlReligion.SelectedItem.Text = "" Then
        '        commString = commString & "<LI>Please Select Religion"
        '        Final_error = False
        '    End If
        'End If
        'If Enq_hash.ContainsKey("COB") = False Then
        '    If ddlCountry.SelectedItem.Text = "" Then
        '        commString = commString & "<LI>Please select Country of Birth"
        '        Final_error = False
        '    End If
        'End If

        'If Enq_hash.ContainsKey("NAT") = False Then

        '    If ddlNational.SelectedItem.Text = "" Then
        '        commString = commString & "<LI>Please select Student Nationality"
        '        Final_error = False
        '    End If
        'End If


        If rfvDatefrom.Visible = True Then
            If txtDob.Text.Trim = "" Then
                commString = commString & "<LI>Student date of birth required"
                Final_error = False

            ElseIf txtDob.Text.Trim <> "" Then
                'code modified 4 date
                Dim strfDate As String = txtDob.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    Final_error = False
                    commString = commString & "<li>Invalid Date of birth"
                Else
                    txtDob.Text = strfDate
                    DateValid_DOB = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    If IsDate(DateValid_DOB) = False Then
                        commString = commString & "<LI>Invalid Date of birth"

                        Final_error = False
                    End If
                End If

            End If
        End If

        'If Enq_hash.ContainsKey("POB") = False Then
        '    If txtPob.Text.Trim = "" Then
        '        commString = commString & "<LI>Student place of birth required"
        '        Final_error = False

        '    End If
        'End If

        'If txtStud_Contact_No.Text.Trim = "" Then
        '    commString = commString & "<LI>Student contact cannot be left empty"
        '    Final_error = False
        'End If

        ' End of select validation of Application Info


        'Validation  for passport/visa  Details

        'If txtPassport.Text.Trim = "" Then
        '    commString = commString & "<LI>Passport No. required"
        '    Final_error = False
        'End If

        'If txtPassportIssue.Text.Trim = "" Then
        '    commString = commString & "<LI>Passport issue place required"
        '    'ViewState("Valid_Pass") = "-1"
        '    ViewState("Valid_Appl") = "-1"
        '    Final_error = False
        'End If

        If txtPassport.Text.Trim <> "" Then
            If txtPassIss_date.Text.Trim <> "" Then
                If IsDate(txtPassIss_date.Text) = False Then
                    commString = commString & "<LI>Passport Issue Date is not a vaild date"
                    Final_error = False
                ElseIf txtPassIss_date.Text.Trim <> "" Then
                    'code modified 4 date
                    Dim strfDate As String = txtPassIss_date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<li>Passport Issue Date format is Invalid"
                    Else
                        txtPassIss_date.Text = strfDate
                        DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        If IsDate(DateValid_PIss_D) = False Then
                            commString = commString & "<LI>Passport Issue Date Invalid"
                            Final_error = False
                        End If
                    End If

                ElseIf txtPassIss_date.Text.Trim <> "" And IsDate(txtPassIss_date.Text) = False Then
                    commString = commString & "<LI>Passport Issue Date format is Invalid"
                    Final_error = False
                ElseIf IsDate(txtPassIss_date.Text) = True Then
                    If IsDate(txtDob.Text) = True Then
                        'code modified 4 date
                        Dim strfDate As String = txtPassIss_date.Text.Trim
                        Dim str_err As String = DateFunctions.checkdate(strfDate)
                        If str_err <> "" Then
                            Final_error = False
                            commString = commString & "<li>Passport Issue Date format is Invalid"
                        Else
                            txtPassIss_date.Text = strfDate
                            Dim strfDate1 As String = txtDob.Text.Trim
                            Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                            If str_err1 <> "" Then
                            Else
                                txtDob.Text = strfDate1
                                DateValid_DOB = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                If DateTime.Compare(DateValid_DOB, DateValid_PIss_D) > 0 Then
                                    commString = commString & "<LI>Passport Issue Date entered is not a valid date and must be greater than Date of Birth"
                                    Final_error = False
                                End If
                            End If
                        End If
                    End If

                End If

            End If


            ' ========================VALIDATE THE PERM ADDRESSS ===================
            'If (txtAdd1_overSea.Text.Trim = "") And (txtAdd2_overSea.Text.Trim = "") Then
            '    commString = commString & "<LI>Overseas address cannot be left empty"
            '    Final_error = False
            'End If


            'If txtOverSeas_Add_City.Text.Trim = "" Then
            '    commString = commString & "<LI>Overseas city cannot be left empty"
            '    Final_error = False
            'End If



            'If ddlOverSeas_Add_Country.SelectedItem.Text = "" Then
            '    commString = commString & "<LI>Overseas country cannot be left empty"
            '    Final_error = False
            'End If


            'If txtPhone_Oversea_Country.Text.Trim = "" Or txtPhone_Oversea_Area.Text.Trim = "" Or txtPhone_Oversea_No.Text.Trim = "" Then
            '    commString = commString & "<LI>Overseas Contact number required"
            '    Final_error = False

            'ElseIf Not Regex.Match(txtPhone_Oversea_Country.Text, "^[0-9]{1,3}$").Success Then
            '    commString = commString & "<LI>Enter valid Overseas country code"
            '    Final_error = False
            'ElseIf Not Regex.Match(txtPhone_Oversea_Area.Text, "^[0-9]{1,4}$").Success Then
            '    commString = commString & "<LI>Enter valid Overseas phone area code"
            '    Final_error = False
            'ElseIf Not Regex.Match(txtPhone_Oversea_No.Text, "^[0-9]{1,10}$").Success Then
            '    commString = commString & "<LI>Enter valid Overseas phone no."
            '    Final_error = False
            'End If


            'If txtPoBox_Pri.Text.Trim = "" Then
            '    commString = commString & "<LI>Overseas PO Box/Zip Code cannot be left empty"
            '    Final_error = False
            'End If









            '==============================================================================













            'validating passport expire  date

            'DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            If txtPassExp_Date.Text.Trim <> "" Then
                If IsDate(txtPassExp_Date.Text) = False Then
                    commString = commString & "<LI>Passport Expiry Date is not a vaild date"
                    Final_error = False
                ElseIf txtPassExp_Date.Text.Trim <> "" And IsDate(txtPassExp_Date.Text) = True Then
                    Dim strfDate As String = txtPassExp_Date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<li>Passport Expiry Date format is Invalid"
                    Else
                        txtPassExp_Date.Text = strfDate
                        DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        If IsDate(DateValid_PExp_D) = False Then
                            commString = commString & "<LI>Date of birth format is invalid"
                            Final_error = False
                        End If
                    End If
                ElseIf IsDate(txtPassExp_Date.Text) = True Then
                    If IsDate(txtPassIss_date.Text) = True Then
                        Dim strfDate As String = txtPassExp_Date.Text.Trim
                        Dim str_err As String = DateFunctions.checkdate(strfDate)
                        If str_err <> "" Then
                            Final_error = False
                            commString = commString & "<li>Passport Expiry Date format is Invalid"
                        Else
                            txtPassExp_Date.Text = strfDate
                            Dim strfDate1 As String = txtPassIss_date.Text.Trim
                            Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                            If str_err1 <> "" Then
                            Else
                                txtPassIss_date.Text = strfDate1
                                DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                If DateTime.Compare(DateValid_PIss_D, DateValid_PExp_D) > 0 Then
                                    commString = commString & "<LI>Passport Expiry  Date entered must be greater than Passport Issue Date"
                                    Final_error = False
                                End If
                            End If
                        End If
                    End If
                End If
            End If

        End If
        If txtVisaIss_date.Text.Trim <> "" Then
            If IsDate(txtVisaIss_date.Text) = False Then
                commString = commString & "<LI>Visa Issue Date is not a vaild date"
                Final_error = False
            Else
                Dim strfDate As String = txtVisaIss_date.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    Final_error = False
                    commString = commString & "<li>Visa Issue Date format is Invalid"
                End If
            End If
        End If

        If txtVisaExp_date.Text.Trim <> "" Then
            ' DateValid_VExp_D = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            If IsDate(txtVisaExp_date.Text) = False Then
                commString = commString & "<LI>Visa Expiry Date is not a vaild date"
                Final_error = False
            ElseIf IsDate(txtVisaExp_date.Text) = True Then
                If IsDate(txtVisaIss_date.Text) = True Then
                    Dim strfDate As String = txtVisaExp_date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<li>Visa Expiry Date format is Invalid"
                    Else
                        txtVisaExp_date.Text = strfDate
                        Dim DateTime2 As Date
                        Dim dateTime1 As Date
                        Dim strfDate1 As String = txtVisaIss_date.Text.Trim
                        Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                        If str_err1 <> "" Then
                        Else
                            txtVisaIss_date.Text = strfDate1
                            DateTime2 = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            dateTime1 = Date.ParseExact(txtVisaIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            'check for the leap year date
                            If IsDate(DateTime2) Then
                                If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                    Final_error = False
                                    commString = commString & "<li>Visa Expiry Date entered  must be greater than Visa Issue Date"
                                End If
                            Else
                                Final_error = False
                                commString = commString & "<li>Invalid Visa Expiry Date"
                            End If
                        End If
                    End If


                End If
            End If
        End If

        ' End for passport/visa  Details



        'Validation  for primary School Details



        If txtPri_Fname.Text <> "" Then
            If Not Regex.Match(txtPri_Fname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Primary contact first name"

                Final_error = False
            End If
        End If

        If txtPri_Mname.Text <> "" Then
            If Not Regex.Match(txtPri_Mname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Primary contact middle name "
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        If txtPri_Lname.Text <> "" Then
            If Not Regex.Match(txtPri_Lname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Primary contact last name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        '^[0-9]{1,3}$


        If ddlPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtHPhone_Country.Text.Trim = "" Or txtHPhone_Area.Text.Trim = "" Or txtHPhone.Text.Trim = "" Then
                commString = commString & "<LI>Home Contact number required"
                Final_error = False
            ElseIf Not Regex.Match(txtHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid home phone country code"
                Final_error = False
            ElseIf Not Regex.Match(txtHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid home phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtHPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid home phone no."
                Final_error = False
            End If

        ElseIf ddlPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtOPhone_Country.Text.Trim = "" Or txtOPhone_Area.Text.Trim = "" Or txtOPhone.Text.Trim = "" Then
                commString = commString & "<LI>Office Contact number required"
                Final_error = False

            ElseIf Not Regex.Match(txtOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid office phone country code"
                Final_error = False
            ElseIf Not Regex.Match(txtOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid office phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtOPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid office phone no."
                Final_error = False
            End If

        ElseIf ddlPref_contact.SelectedItem.Text = "Mobile" Then
            If txtM_Country.Text.Trim = "" Or txtM_Area.Text.Trim = "" Or txtMobile_Pri.Text.Trim = "" Then
                commString = commString & "<LI>Mobile Contact number required"
                Final_error = False

            ElseIf Not Regex.Match(txtM_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid mobile country code"
                Final_error = False
            ElseIf Not Regex.Match(txtM_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid mobile phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid mobile phone no."
                Final_error = False
            End If
        End If

        'If Enq_hash.ContainsKey("PCEID") = False Then
        If txtEmail_Pri.Text.Trim = "" Then
            commString = commString & "<LI>E-mail ID required"
            Final_error = False

        ElseIf isEmail(txtEmail_Pri.Text) = False Then
            commString = commString & "<LI>E-mail entered is not valid"
            Final_error = False
        End If


        'End If


        If chkStaff_GEMS.Checked = True Then
            If Trim(txtStaff_Name.Text) = "" Then
                commString = commString & "<LI>Staff name required"
                Final_error = False
            End If

            If ValidStaff_IDCheck() = True Then
                If Trim(txtStaffID.Text) = "" Then
                    commString = commString & "<LI>Staff ID required"
                    Final_error = False
                Else
                    Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtStaffID.Text, ddlStaff_BSU.SelectedItem.Value)
                        If ValidStaff_reader.HasRows = False Then
                            commString = commString & "<LI>Staff ID is not valid  for the selected Business Unit"
                            Final_error = False
                        End If
                    End Using
                End If

            End If
        End If


        If txtSchFrom_dt.Text.Trim <> "" Then
            If IsDate(txtSchFrom_dt.Text) = False Then
                commString = commString & "<LI>School from date is not a vaild date"
                Final_error = False


            End If
        ElseIf txtSchFrom_dt.Text.Trim <> "" And IsDate(txtSchFrom_dt.Text) = True Then
            'code modified 4 date
            Dim strfDate As String = txtSchFrom_dt.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                commString = commString & "<LI>School from date is not a vaild date"
                Final_error = False
            Else
                txtSchFrom_dt.Text = strfDate
                DateValid_DOB = Date.ParseExact(txtSchFrom_dt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If IsDate(DateValid_DOB) = False Then
                    commString = commString & "<LI>School from date is not a vaild date"
                    Final_error = False
                End If
            End If
        ElseIf txtSchFrom_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = False Then
            commString = commString & "<LI>School from date format is invalid"
            Final_error = False

        End If



        If txtSchTo_dt.Text.Trim <> "" Then
            If IsDate(txtSchTo_dt.Text) = False Then
                commString = commString & "<LI>School to date is not a vaild date"
                Final_error = False
            End If
        ElseIf txtSchTo_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = True Then
            'code modified 4 date
            Dim strfDate As String = txtSchTo_dt.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                commString = commString & "<LI>School to date is not a vaild date"
                Final_error = False
            Else
                txtSchTo_dt.Text = strfDate
                DateValid_DOB = Date.ParseExact(txtSchTo_dt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If IsDate(DateValid_DOB) = False Then
                    commString = commString & "<LI>School to date is not a vaild date"
                    Final_error = False
                End If
            End If
        ElseIf txtSchTo_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = False Then
            commString = commString & "<LI>School to date format is invalid"
            Final_error = False
        End If







        If rbRelocAgent.Checked = True Then
            If Enq_hash.ContainsKey("AGN") = False Then
                If txtAgentName.Text.Trim = "" Then
                    commString = commString & "<LI>Agent contact name cannot be left empty"
                    Final_error = False
                End If
            End If
        End If

        If rbRelocAgent.Checked = True Then
            If txtMAgent_country.Text.Trim = "" Or txtMAgent_Area.Text.Trim = "" Or txtMAgent_No.Text.Trim = "" Then
                commString = commString & "<LI>Agent Mobile Contact number required"
                Final_error = False

            ElseIf Not Regex.Match(txtMAgent_country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile country code"
                Final_error = False
            ElseIf Not Regex.Match(txtMAgent_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtMAgent_No.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile phone no."

                Final_error = False
            End If
        End If

        If chkRef.Checked Then
            If RefCodeIsvalid() = False Then
                commString = commString & "<LI>Please verify the Ref. Code and email you have entered."
                Final_error = False
            End If
        End If

        If Final_error Then
            Final_validation = Final_error
        Else

            lblError.Text = commString & "</UL>"
            Final_validation = Final_error
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                                            "<script language=javascript>fill_Sib();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                   "<script language=javascript>fill_Staff();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC",
                                   "<script language=javascript>fill_Stud();</script>")
        End If


    End Function

    Private Sub check_Visibility()
        Dim i As Integer = 0
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select isnull(EQV_STAR_ID,'') EQV_STAR_ID,isnull(EQV_CONTROL_ID,'') EQV_CONTROL_ID,ISNULL(EVD_bVISIBLE,'FALSE')EVD_bVISIBLE," _
                                      & " ISNULL(EQV_VALID_CONTROL,'')EQV_VALID_CONTROL,ISNULL(EVD_bVALIDATE,'FALSE')EVD_bVALIDATE  from ONLINE_ENQ.ENQUIRY_VISIBLE_M " _
                                      & " INNER JOIN ONLINE_ENQ.ENQUIRY_VISIBLE_D ON EQV_ID=EVD_EQV_ID " _
                                      & " WHERE EVD_BSU_ID='" + Session("sBsuid") + "'and EQV_CONTROL_ID='studEnqForm_Reg_New'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


        If ds.Tables(0).Rows.Count >= 1 Then

            'pnlCurrent.Enabled = True
            'pnlMain.Enabled = True
            'pnlHeader.Enabled = True
            'pnlOther.Enabled = True
            'pnlParent.Enabled = True
            'pnlPassPort.Enabled = True
            'pnlTrans.Enabled = True
            While i < ds.Tables(0).Rows.Count

                Try
                    Dim S As String = ds.Tables(0).Rows(i).Item("EQV_STAR_ID")
                    Dim V As String = ds.Tables(0).Rows(i).Item("EQV_VALID_CONTROL")
                    If S <> "" Then
                        Dim ctrl As Control
                        ctrl = pnlHeader.FindControl(S)
                        If ctrl Is Nothing Then
                            ctrl = pnlCurrent.FindControl(S)
                        ElseIf ctrl Is Nothing Then
                            ctrl = pnlMain.FindControl(S)
                        ElseIf ctrl Is Nothing Then
                            ctrl = pnlOther.FindControl(S)
                        ElseIf ctrl Is Nothing Then
                            ctrl = pnlParent.FindControl(S)
                        ElseIf ctrl Is Nothing Then
                            ctrl = pnlPassPort.FindControl(S)
                        ElseIf ctrl Is Nothing Then
                            ctrl = pnlTrans.FindControl(S)
                        End If
                        ctrl.Visible = ds.Tables(0).Rows(i).Item("EVD_bVISIBLE")
                    End If
                    If V <> "" Then
                        Dim VALID_ctrl As Control
                        VALID_ctrl = pnlHeader.FindControl(V)
                        If VALID_ctrl Is Nothing Then
                            VALID_ctrl = pnlCurrent.FindControl(V)
                        ElseIf VALID_ctrl Is Nothing Then
                            VALID_ctrl = pnlMain.FindControl(V)
                        ElseIf VALID_ctrl Is Nothing Then
                            VALID_ctrl = pnlOther.FindControl(V)
                        ElseIf VALID_ctrl Is Nothing Then
                            VALID_ctrl = pnlParent.FindControl(V)
                        ElseIf VALID_ctrl Is Nothing Then
                            VALID_ctrl = pnlPassPort.FindControl(V)
                        ElseIf VALID_ctrl Is Nothing Then
                            VALID_ctrl = pnlTrans.FindControl(V)
                        End If
                        VALID_ctrl.Visible = ds.Tables(0).Rows(i).Item("EVD_bVALIDATE")
                    End If
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)

                End Try



                i += 1
            End While
        End If


        If rfvReligion.Visible = True Then
            ltReg.Visible = True
        End If
        If rfvDatefrom.Visible = True Then
            ltDOB.Visible = True
        End If
        If rfvPob.Visible = True Then
            ltPOB.Visible = True
        End If
        If rfvCOB.Visible = True Then
            ltCOB.Visible = True
        End If
        If rfvNationality.Visible = True Then
            ltNationality.Visible = True
        End If
        If rfv_txtPassport.Visible = True Then
            ltPASSNO.Visible = True
        End If
        If rfvtxtStud_Contact_No.Visible = True Then
            lbl_Contact_No.Visible = True
        End If
        If rfvPri_Fname.Visible = True Then
            lbl_Fname.Visible = True
        End If
        If rfvNational1.Visible = True Then
            ltNational1.Visible = True
        End If
        If rfvPri_Email.Visible = True Then
            ltPri_Email.Visible = True
        End If

    End Sub










    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Session("SIB_APPLICANT") = ""
            Call clearAll()
            mpe.Hide()

            btnAdd.Enabled = True
            btnSave.Visible = True
            btnCancel.Visible = True
            'mnuMaster.Enabled = True
            enable_panel()
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Session("SIB_APPLICANT") = Session("TEMP_ApplNo")
        lblText.Text = "Would you like to set as sibling?"
        Call ClearApplic()
        btnNo2.Visible = True

        btnYes2.Visible = True
        btnNo.Visible = False

        btnYes.Visible = False


        mpe.Show()
        btnAdd.Enabled = True
        btnSave.Visible = True
        btnCancel.Visible = True
        'mnuMaster.Enabled = True
        enable_panel()

    End Sub

    Protected Sub btnYes2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("SIB_APPLICANT") = Session("TEMP_ApplNo")
        mpe.Hide()
        lblText.Text = "Would like to continue with the same Primary Contact info for the next applicant? "
        btnNo2.Visible = False

        btnYes2.Visible = False
        btnNo.Visible = True

        btnYes.Visible = True

    End Sub

    Protected Sub btnNo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("SIB_APPLICANT") = ""

        mpe.Hide()
        btnNo2.Visible = False
        btnYes2.Visible = False
        btnNo.Visible = True
        btnYes.Visible = True

    End Sub


    Sub gridbind()
        Try


            gvSchool.DataSource = ViewState("TABLE_School_Pre")
            gvSchool.DataBind()

            If Not ViewState("TABLE_School_Pre") Is Nothing Then
                If ViewState("TABLE_School_Pre").Rows.Count > 0 Then
                    ltSchoolType.Text = "Previous School"
                Else
                    ltSchoolType.Text = "Current School"

                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "gridbind")
        End Try
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim SCH_ID As New DataColumn("SCH_ID", System.Type.GetType("System.String"))
            Dim SCH_NAME As New DataColumn("SCH_NAME", System.Type.GetType("System.String"))
            Dim SCH_HEAD As New DataColumn("SCH_HEAD", System.Type.GetType("System.String"))
            Dim SCH_FEE_ID As New DataColumn("SCH_FEE_ID", System.Type.GetType("System.String"))
            Dim SCH_GRADE As New DataColumn("SCH_GRADE", System.Type.GetType("System.String"))
            Dim SCH_LEARN_INS As New DataColumn("SCH_LEARN_INS", System.Type.GetType("System.String"))
            Dim SCH_ADDR As New DataColumn("SCH_ADDR", System.Type.GetType("System.String"))
            Dim SCH_CURR As New DataColumn("SCH_CURR", System.Type.GetType("System.String"))
            Dim SCH_CITY As New DataColumn("SCH_CITY", System.Type.GetType("System.String"))
            Dim SCH_COUNTRY As New DataColumn("SCH_COUNTRY", System.Type.GetType("System.String"))
            Dim SCH_PHONE As New DataColumn("SCH_PHONE", System.Type.GetType("System.String"))
            Dim SCH_FAX As New DataColumn("SCH_FAX", System.Type.GetType("System.String"))
            Dim SCH_FROMDT As New DataColumn("SCH_FROMDT", System.Type.GetType("System.String"))
            Dim SCH_TODT As New DataColumn("SCH_TODT", System.Type.GetType("System.String"))
            Dim SCH_TYPE As New DataColumn("SCH_TYPE", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(SCH_ID)
            dtDt.Columns.Add(SCH_NAME)
            dtDt.Columns.Add(SCH_HEAD)
            dtDt.Columns.Add(SCH_FEE_ID)
            dtDt.Columns.Add(SCH_GRADE)
            dtDt.Columns.Add(SCH_LEARN_INS)
            dtDt.Columns.Add(SCH_ADDR)
            dtDt.Columns.Add(SCH_CURR)
            dtDt.Columns.Add(SCH_CITY)
            dtDt.Columns.Add(SCH_COUNTRY)
            dtDt.Columns.Add(SCH_PHONE)
            dtDt.Columns.Add(SCH_FAX)
            dtDt.Columns.Add(SCH_FROMDT)
            dtDt.Columns.Add(SCH_TODT)
            dtDt.Columns.Add(SCH_TYPE)
            dtDt.Columns.Add(STATUS)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

            Return dtDt
        End Try
    End Function



    Protected Sub btnGridAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGridAdd.Click

        Try
            If valid_date_prev_school() = True Then 'check  for validate date

                Dim Ibsu As Integer = 0
                Dim SCH_NAME As String = String.Empty
                Dim sch_id As String = String.Empty
                If ddlGemsGr.SelectedItem.Text = "OTHER" Then
                    SCH_NAME = txtSch_other.Text
                    sch_id = ""
                Else
                    SCH_NAME = ddlGemsGr.SelectedItem.Text
                    sch_id = ddlGemsGr.SelectedValue
                End If


                If ViewState("TABLE_School_Pre").Rows.Count > 4 Then
                    lblError.Text = "Add school details not more than four !!!"
                    Exit Sub
                End If
                If ViewState("TABLE_School_Pre").Rows.Count > 0 Then
                    For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
                        If ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_NAME"))).Trim = UCase(Trim(SCH_NAME.Trim)))) _
                         And ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_GRADE"))).Trim = UCase(Trim(ddlPre_Grade.SelectedValue.Trim)))) Then

                            lblError.Text = "Duplicate entry not allowed !!!"

                            Exit Sub

                        ElseIf ViewState("TABLE_School_Pre").Rows(i)("SCH_TYPE").Trim = "Current School" Then
                            lblError.Text = "Duplicate current school type entry not allowed !!!"
                            Exit Sub
                        End If
                    Next
                End If



                Dim rDt As DataRow

                rDt = ViewState("TABLE_School_Pre").NewRow
                rDt("ID") = ViewState("id")
                rDt("sch_id") = sch_id
                rDt("SCH_NAME") = SCH_NAME
                rDt("SCH_HEAD") = txtSchool_head.Text.Trim
                rDt("SCH_FEE_ID") = txtFeeID_GEMS.Text.Trim
                rDt("SCH_GRADE") = ddlPre_Grade.SelectedValue.Trim
                rDt("SCH_LEARN_INS") = txtLang_Instr.Text
                rDt("SCH_ADDR") = txtSchAddr.Text.Trim
                rDt("SCH_CURR") = ddlPre_Curriculum.SelectedValue
                rDt("SCH_CITY") = txtSchCity.Text.Trim
                rDt("SCH_COUNTRY") = ddlPre_Country.SelectedValue
                rDt("SCH_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                rDt("SCH_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                rDt("SCH_FROMDT") = txtSchFrom_dt.Text
                rDt("SCH_TODT") = txtSchTo_dt.Text
                rDt("SCH_TYPE") = ltSchoolType.Text
                rDt("STATUS") = "add"
                ViewState("id") = ViewState("id") + 1
                ViewState("TABLE_School_Pre").Rows.Add(rDt)
                gridbind()
                ClearPrev()
            End If

        Catch ex As Exception
            lblError.Text = "Error in adding new record"
        End Try



    End Sub

    Function valid_date_prev_school() As Boolean
        Dim Check_Flag As Boolean = True
        Dim DateValid_L_Date As Date

        If txtSchFrom_dt.Text.Trim <> "" Then
            If IsDate(txtSchFrom_dt.Text) = False Then
                lblError.Text = "School from date is not a vaild date"
                Check_Flag = False

            End If
        ElseIf txtSchFrom_dt.Text.Trim <> "" And IsDate(txtSchFrom_dt.Text) = True Then
            'code modified 4 date
            Dim strfDate As String = txtSchFrom_dt.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                lblError.Text = "School from date is not a vaild date"
                Check_Flag = False
            Else
                txtSchFrom_dt.Text = strfDate
                DateValid_L_Date = Date.ParseExact(txtSchFrom_dt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If IsDate(DateValid_L_Date) = False Then
                    lblError.Text = "School from date is not a vaild date"
                    Check_Flag = False
                End If
            End If
        ElseIf txtSchFrom_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = False Then
            lblError.Text = "School from date format is invalid"
            Check_Flag = False

        End If

        If txtSchTo_dt.Text.Trim <> "" Then
            If IsDate(txtSchTo_dt.Text) = False Then
                lblError.Text = "School to date is not a vaild date"
                Check_Flag = False
            End If
        ElseIf txtSchTo_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = True Then
            'code modified 4 date
            Dim strfDate As String = txtSchTo_dt.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                lblError.Text = "School to date is not a vaild date"
                Check_Flag = False
            Else
                txtSchTo_dt.Text = strfDate
                DateValid_L_Date = Date.ParseExact(txtSchTo_dt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If IsDate(DateValid_L_Date) = False Then
                    lblError.Text = "School to date is not a vaild date"
                    Check_Flag = False
                End If
            End If
        ElseIf txtSchTo_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = False Then
            lblError.Text = "School to date format is invalid"
            Check_Flag = False
        End If
        Return Check_Flag
    End Function


    Sub callAutoAdd()
        Try

            If ViewState("Table_row").ToString.Contains("PK|KG1") = False Then
                Dim Ibsu As Integer = 0
                Dim SCH_NAME As String = String.Empty
                Dim sch_id As String = String.Empty
                If ddlGemsGr.SelectedItem.Text = "OTHER" Then
                    SCH_NAME = txtSch_other.Text
                    sch_id = ""
                Else
                    SCH_NAME = ddlGemsGr.SelectedItem.Text
                    sch_id = ddlGemsGr.SelectedValue
                End If
                If Not ViewState("TABLE_School_Pre") Is Nothing Then
                    If (ViewState("TABLE_School_Pre").Rows.Count > 0) Then
                        For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
                            If ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_NAME"))).Trim = UCase(Trim(SCH_NAME.Trim)))) _
                             And ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_GRADE"))).Trim = UCase(Trim(ddlPre_Grade.SelectedValue.Trim)))) Then
                                clearAll()
                                Exit Sub

                            ElseIf ViewState("TABLE_School_Pre").Rows(i)("SCH_TYPE").Trim = "Current School" Then
                                clearAll()
                                Exit Sub
                            End If
                        Next
                    End If
                End If



                If ViewState("TABLE_School_Pre").Rows.Count > 4 Then
                    Exit Sub
                End If
                Dim rDt As DataRow

                rDt = ViewState("TABLE_School_Pre").NewRow
                rDt("ID") = ViewState("id")
                rDt("sch_id") = sch_id
                rDt("SCH_NAME") = SCH_NAME
                rDt("SCH_HEAD") = txtSchool_head.Text.Trim
                rDt("SCH_FEE_ID") = txtFeeID_GEMS.Text.Trim
                rDt("SCH_GRADE") = ddlPre_Grade.SelectedValue.Trim
                rDt("SCH_LEARN_INS") = txtLang_Instr.Text
                rDt("SCH_ADDR") = txtSchAddr.Text.Trim
                rDt("SCH_CURR") = ddlPre_Curriculum.SelectedValue
                rDt("SCH_CITY") = txtSchCity.Text.Trim
                rDt("SCH_COUNTRY") = ddlPre_Country.SelectedValue
                rDt("SCH_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                rDt("SCH_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                rDt("SCH_FROMDT") = txtSchFrom_dt.Text
                rDt("SCH_TODT") = txtSchTo_dt.Text
                rDt("SCH_TYPE") = ltSchoolType.Text
                rDt("STATUS") = "add"
                ViewState("id") = ViewState("id") + 1
                ViewState("TABLE_School_Pre").Rows.Add(rDt)
                gridbind()
                ClearPrev()
            Else
                ViewState("TABLE_School_Pre").Rows.Clear()
                ViewState("id") = 1
                gridbind()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub gvSchool_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvSchool.RowEditing


        gvSchool.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvSchool.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        Dim arInfoLang() As String
        Dim splitterLang As Char = "-"


        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
            If iIndex = ViewState("TABLE_School_Pre").Rows(iEdit)("ID") Then

                If Not ddlGemsGr.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id")) Is Nothing Then
                    ddlGemsGr.ClearSelection()
                    ddlGemsGr.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id")).Selected = True
                End If

                If ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id") = "" Then
                    txtSch_other.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_NAME")
                Else
                    txtSch_other.Text = ""
                End If

                txtSchool_head.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_HEAD")
                txtFeeID_GEMS.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FEE_ID")

                If Not ddlPre_Grade.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_GRADE")) Is Nothing Then
                    ddlPre_Grade.ClearSelection()
                    ddlPre_Grade.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_GRADE")).Selected = True
                End If
                txtSchCity.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CITY")
                If Not ddlPre_Country.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_COUNTRY")) Is Nothing Then
                    ddlPre_Country.ClearSelection()
                    ddlPre_Country.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_COUNTRY")).Selected = True
                End If

                If Not ddlPre_Curriculum.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CURR")) Is Nothing Then
                    ddlPre_Curriculum.ClearSelection()
                    ddlPre_Curriculum.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CURR")).Selected = True
                End If


                txtSchAddr.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_ADDR")
                arInfoLang = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_PHONE").Trim.Split(splitterLang)
                txtSCHPhone_Country.Text = arInfoLang(0)
                txtSCHPhone_Area.Text = arInfoLang(1)
                txtSCHPhone_No.Text = arInfoLang(2)
                arInfoLang = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FAX").Trim.Split(splitterLang)
                txtSCHFax_Country.Text = arInfoLang(0)
                txtSCHFax_Area.Text = arInfoLang(1)
                txtSCHFax_No.Text = arInfoLang(2)


                txtSchFrom_dt.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FROMDT")
                txtSchTo_dt.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TODT")
                ltSchoolType.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TYPE")

                Exit For
            End If
        Next
        gvSchool.Columns(6).Visible = False
        btnAdd.Visible = False
        btnGridUpdate.Visible = True
        'gridbind()
    End Sub

    Protected Sub btnGridUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGridUpdate.Click
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim row As GridViewRow = gvSchool.Rows(gvSchool.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        iIndex = CInt(idRow.Text)

        Dim SCH_NAME As String = String.Empty
        Dim sch_id As String = String.Empty
        If ddlGemsGr.SelectedItem.Text = "OTHER" Then
            SCH_NAME = txtSch_other.Text
            sch_id = ""
        Else
            SCH_NAME = ddlGemsGr.SelectedItem.Text
            sch_id = ddlGemsGr.SelectedValue
        End If


        For iEdit = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
            If iIndex = ViewState("TABLE_School_Pre").Rows(iEdit)("ID") Then
                ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id") = sch_id
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_NAME") = SCH_NAME
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_HEAD") = txtSchool_head.Text.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FEE_ID") = txtFeeID_GEMS.Text.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_GRADE") = ddlPre_Grade.SelectedValue.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_LEARN_INS") = txtLang_Instr.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_ADDR") = txtSchAddr.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CURR") = ddlPre_Curriculum.SelectedValue
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CITY") = txtSchCity.Text.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_COUNTRY") = ddlPre_Country.SelectedValue
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FROMDT") = txtSchFrom_dt.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TODT") = txtSchTo_dt.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TYPE") = ltSchoolType.Text

                Exit For
            End If
        Next

        btnAdd.Visible = True

        btnGridUpdate.Visible = False
        gvSchool.SelectedIndex = -1
        gridbind()
        gvSchool.Columns(6).Visible = True
        ClearPrev()

    End Sub


    Protected Sub btnSave_Click1(sender As Object, e As EventArgs) Handles btnSave.Click

    End Sub
End Class
