Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class StudContinueCert_Print
    Inherits System.Web.UI.Page

    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200054") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    'h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                   

                    callYEAR_DESCRBind()

                    Call gridbind()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
        'str_Sid_img = h_selected_menu_5.Value.Split("__")
        'getid5(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTCSO_View.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTCSO_View.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTCSO_View.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTCSO_View.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvTCSO_View.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTCSO_View.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvTCSO_View.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTCSO_View.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    'Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
    '    If gvTCSO_View.Rows.Count > 0 Then
    '        Dim s As HtmlControls.HtmlImage
    '        Try

    '            s = gvTCSO_View.HeaderRow.FindControl("mnu_5_img")
    '            If p_imgsrc <> "" Then
    '                s.Src = p_imgsrc
    '            End If
    '            Return s.ClientID

    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End If
    '    Return ""
    'End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_Stu_No As String = String.Empty
            Dim str_filter_Stu_Name As String = String.Empty
            Dim str_filter_Stu_Grade As String = String.Empty
            Dim str_filter_Stu_Section As String = String.Empty
            Dim str_filter_Stu_Status As String = String.Empty
            Dim Stu_Status_rd As String = String.Empty
           
            Dim ds As New DataSet

            str_Sql = " Select Stu_ID,Stu_No,Stu_Name,PRESENTGRADE,SCT_DESCR,STU_LEAVEDATE,STU_CURRSTATUS,STU_ACD_ID from (SELECT DISTINCT " & _
                      " A.STU_ID, ISNULL(A.STU_FIRSTNAME, '') + ' ' + ISNULL(A.STU_MIDNAME, '') + ' ' + ISNULL(A.STU_LASTNAME, '') AS STU_NAME, " & _
                      " A.STU_FIRSTNAMEARABIC AS STU_ARABICNAME, B.SCT_DESCR, A.STU_SCT_ID_JOIN,  " & _
                      "  D.GRM_DISPLAY AS PRESENTGRADE, H.GRD_DISPLAY_ARABIC AS PRESENTGRADEARABIC, F.ACY_DESCR, A.STU_NO, A.STU_BLUEID, " & _
                      "  A.STU_DOB, A.STU_MINDOJ,STU_LEAVEDATE,STU_CURRSTATUS,STU_ACD_ID  " & _
                                        " FROM    STUDENT_M AS A INNER JOIN  SECTION_M AS B ON A.STU_SCT_ID = B.SCT_ID INNER JOIN " & _
                      " GRADE_BSU_M AS D ON A.STU_GRM_ID = D.GRM_ID INNER JOIN " & _
                     "  GRADE_M AS H ON D.GRM_GRD_ID = H.GRD_ID INNER JOIN " & _
                     "  ACADEMICYEAR_D AS E ON A.STU_ACD_ID = E.ACD_ID INNER JOIN " & _
                      " ACADEMICYEAR_M AS F ON E.ACD_ACY_ID = F.ACY_ID  " & _
                     "  WHERE  A.STU_LEAVEDATE IS NULL AND A.STU_CURRSTATUS<>'CN')Z " & _
                    "  WHERE    STU_ACD_ID='" & ddlAca_Year.SelectedItem.Value & "' "


            Dim lblID As New Label
            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_Stu_No As String = String.Empty
            Dim str_Stu_Name As String = String.Empty
            Dim str_Stu_Grade As String = String.Empty
            Dim str_Stu_Section As String = String.Empty
            'Dim str_Stu_Status As String = String.Empty


            If gvTCSO_View.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTCSO_View.HeaderRow.FindControl("txtStu_No")
                str_Stu_No = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Stu_No = " AND isnull(Z.STU_NO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Stu_No = "  AND  NOT isnull(Z.STU_NO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Stu_No = " AND isnull(Z.STU_NO,'') LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Stu_No = " AND isnull(Z.STU_NO,'') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Stu_No = " AND isnull(Z.STU_NO,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Stu_No = " AND isnull(Z.STU_NO,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTCSO_View.HeaderRow.FindControl("txtStu_Name")
                str_Stu_Name = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Stu_Name = " AND Z.Stu_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Stu_Name = "  AND  NOT Z.Stu_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Stu_Name = " AND Z.Stu_Name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Stu_Name = " AND Z.Stu_Name NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Stu_Name = " AND Z.Stu_Name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Stu_Name = " AND Z.Stu_Name NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTCSO_View.HeaderRow.FindControl("txtGrade")

                str_Stu_Grade = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Stu_Grade = " AND isnull(Z.PRESENTGRADE,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Stu_Grade = "  AND  NOT isnull(Z.PRESENTGRADE,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Stu_Grade = " AND isnull(Z.PRESENTGRADE,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Stu_Grade = " AND isnull(Z.PRESENTGRADE'') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Stu_Grade = " AND isnull(Z.PRESENTGRADE,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Stu_Grade = " AND isnull(Z.PRESENTGRADE,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTCSO_View.HeaderRow.FindControl("txtSection")

                str_Stu_Section = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Stu_Section = " AND isnull(Z.SCT_DESCR,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Stu_Section = "  AND  NOT isnull(Z.SCT_DESCR,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Stu_Section = " AND isnull(Z.SCT_DESCR,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Stu_Section = " AND isnull(Z.SCT_DESCR,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Stu_Section = " AND isnull(Z.SCT_DESCR,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Stu_Section = " AND isnull(Z.SCT_DESCR,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                'str_Sid_search = h_selected_menu_4.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvTCSO_View.HeaderRow.FindControl("txtStatus")

                'str_Stu_Status = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_Stu_Status = " AND a.Stu_Status LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_Stu_Status = "  AND  NOT a.Stu_Status LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_Stu_Status = " AND a.Stu_Status  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_Stu_Status = " AND a.Stu_Status  NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_Stu_Status = " AND a.Stu_Status LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_Stu_Status = " AND a.Stu_Status NOT LIKE '%" & txtSearch.Text & "'"
                'End If



            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_Stu_No & str_filter_Stu_Name & str_filter_Stu_Section & str_filter_Stu_Grade & str_filter_Stu_Status & " ORDER BY Z.Stu_Name")


            If ds.Tables(0).Rows.Count > 0 Then

                gvTCSO_View.DataSource = ds.Tables(0)
                gvTCSO_View.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                'ds.Tables(0).Rows(0)(6) = True

                gvTCSO_View.DataSource = ds.Tables(0)
                Try
                    gvTCSO_View.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTCSO_View.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvTCSO_View.Rows(0).Cells.Clear()
                gvTCSO_View.Rows(0).Cells.Add(New TableCell)
                gvTCSO_View.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTCSO_View.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTCSO_View.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvTCSO_View.HeaderRow.FindControl("txtStu_No")
            txtSearch.Text = str_Stu_No
            txtSearch = gvTCSO_View.HeaderRow.FindControl("txtStu_Name")
            txtSearch.Text = str_Stu_Name
            txtSearch = gvTCSO_View.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_Stu_Grade
            txtSearch = gvTCSO_View.HeaderRow.FindControl("txtSection")
            txtSearch.Text = str_Stu_Section
            'txtSearch = gvTCSO_View.HeaderRow.FindControl("txtStatus")
            'txtSearch.Text = str_Stu_Status
            'If Session("Stu_Status_rd") = 1 Then

            '    rbEnroll.Checked = True
            'ElseIf Session("Stu_Status_rd") = 2 Then
            '    rbStrikeOff.Checked = True
            'ElseIf Session("Stu_Status_rd") = 3 Then
            '    rdTC.Checked = True
            'ElseIf Session("Stu_Status_rd") = 4 Then
            '    rdAll.Checked = True
            'End If



            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Function GetCurrentACY_DESCR(ByVal Bsu_id As String, ByVal CLM As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_sql As String = " Select Y_DESCR from(SELECT ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR,ACADEMICYEAR_D.ACD_Current as ACD_Current, " & _
                                    " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.BSU_ID='" & Bsu_id & "'  and a.CLM_ID='" & CLM & "' and a.ACD_Current=1 order by a.Y_DESCR desc"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Public Sub callYEAR_DESCRBind()

        Try
            Dim Active_year As String = GetCurrentACY_DESCR(Session("sBsuid"), Session("CLM"))




            'Using Activereader As SqlDataReader = AccessStudentClass.GetActive_ACD_4_Grade(Session("sBsuid"), Session("CLM"))
            '    While (Activereader.Read())

            '        Active_year = Convert.ToString(Activereader("Y_DESCR"))
            '    End While

            'End Using

            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAca_Year.Items.Clear()
                di = New ListItem("Not Selected", "0")

                ddlAca_Year.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAca_Year.Items.Add(di)


                    End While
                End If
            End Using

            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlAca_Year.Items.Count - 1
                If ddlAca_Year.Items(ItemTypeCounter).Text = Active_year Then
                    ddlAca_Year.SelectedIndex = ItemTypeCounter
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearchStu_No_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Call gridbind()
    End Sub

    Protected Sub btnSearchStu_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Call gridbind()
    End Sub

    Protected Sub btnSearchGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Call gridbind()
    End Sub

    Protected Sub btnSearchSection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Call gridbind()
    End Sub

    'Protected Sub btnSearchStatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Call gridbind()
    'End Sub

    'Protected Sub rbEnroll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnroll.CheckedChanged
    '    Session("Stu_Status_rd") = 1
    '    Call gridbind()

    'End Sub


    Protected Sub gvTCSO_View_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTCSO_View.RowCommand
        If e.CommandName = "View" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvTCSO_View.Rows(index), GridViewRow)
            Dim UserIDLabel As Label

            UserIDLabel = DirectCast(selectedRow.Cells(0).Controls(1), Label)
           

            Dim stu_id As String = UserIDLabel.Text
            Dim lstrRegistrar As String = String.Empty

          

            'KHDA----Start
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            Dim lstrPWD As String = String.Empty


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))

            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_MOE_FILETYPE", pParms)
                While reader.Read
                    lstrPWD = Convert.ToString(reader("BSU_MOE_FILETYPE"))
                End While
            End Using

            ' KHDA----END


            Dim param As New Hashtable

            param.Add("@IMG_BSU_ID", Session("sBsuid"))
            param.Add("@IMG_TYPE", "LOGO")

            param.Add("@STU_ID", stu_id)
            param.Add("@TCSO", "")
          

            If (ViewState("MainMnu_code") = "S200054") Then
                Dim currYear As String = ddlAca_Year.SelectedItem.Text
                Dim year As Integer = Val(ddlAca_Year.SelectedItem.Text.Split("-")(1))
                Dim nextYear As String
                Dim prevYear As String
                nextYear = year.ToString + "-" + (year + 1).ToString
                prevYear = (year - 1).ToString + "-" + year.ToString
                param.Add("nextYear", nextYear)
            End If



            Dim rptClass As New rptClass
            With rptClass
                'If Not (Session("SBsuid") <> 131001 And Session("SBsuid") <> 131002) Then
                '    .Photos = GetPhotoClass(stu_id)
                'End If
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param
                If ViewState("MainMnu_code") = "S200054" Then


                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptContinueCer_KHDA.rpt")
                    ElseIf Session("SBsuid") <> 131001 And Session("SBsuid") <> 131002 Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptContinueCer.rpt")

                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptContinueCer_Sharjah.rpt")
                    End If

                End If

            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()

            'Dim jscript As New StringBuilder()

            'Dim URL As String = "../Reports/ASPX Report/rptReportViewer.aspx"
            'jscript.Append("<script>window.open('")
            'jscript.Append(URL)
            'jscript.Append("');</script>")
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "OpenWindows", jscript.ToString())
            'Response.Write("<Script> window.open('../Reports/ASPX Report/rptReportViewer.aspx') </Script>")
        End If
    End Sub
   
    'Private Sub CallReport_offer()


    '    Dim EQS_IDs As New StringBuilder

    '    For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1

    '        Dim row As GridViewRow = gvStudEnquiry.Rows(i)

    '        Dim ckList As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked

    '        If ckList Then
    '            EQS_IDs.Append((DirectCast(row.FindControl("lblEqsId"), Label).Text) & "_" & (DirectCast(row.FindControl("lblAPL_ID"), Label).Text))
    '            EQS_IDs.Append("|")
    '        End If
    '    Next


    '    Dim param As New Hashtable

    '    param.Add("@IMG_BSU_ID", Session("sBsuid"))
    '    param.Add("@IMG_TYPE", "LOGO")
    '    param.Add("@EQS_IDs", EQS_IDs.ToString)
    '    param.Add("@BSU_ID", Session("sBsuid"))
    '    param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
    '    param.Add("UserName", Session("sUsr_name"))

    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
    '        .reportParameters = param

    '        .reportPath = Server.MapPath("~\Students\Reports\RPT\rptOffer_letter_Group.rpt")


    '    End With
    '    Session("rptClass") = rptClass
    '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")


    'End Sub

    'Function GetEmpName(ByVal designation As String) As String
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
    '                             & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
    '                             & " AND DES_DESCR='" + designation + "'"
    '    Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    '    Return CStr(emp)
    'End Function
    Protected Sub gvTCSO_View_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTCSO_View.PageIndexChanging
        gvTCSO_View.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        gridbind()
    End Sub
    'Function GetPhotoClass(ByVal Stu_Id As Integer) As OASISPhotos
    '    Dim vPhoto As New OASISPhotos
    '    vPhoto.BSU_ID = Session("sBSUID")
    '    vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO

    '    vPhoto.IDs.Add(Stu_Id)

    '    Return vPhoto
    'End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
