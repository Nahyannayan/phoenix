﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuBudgetMaster.aspx.vb" Inherits="Students_stuBudgetMaster" title="Untitled Page" %>

<asp:Content ID="cphMasterpage" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Budget Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">                   
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" width="100%" cellpadding="0" cellspacing="0"
                                >
                                <tr>
                                    <td align="left" width="20%"><span class="field-label"> Academic Year</span>
                                    </td>

                                    <td align="left"  width="20%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                   <td colspan="2" align="right">
                                        <asp:LinkButton ID="lnkCopy" runat="server" Text="Copy to All Months"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"  valign="top">
                                        <asp:GridView ID="gvStudBudg" runat="server" AllowPaging="True"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAcdId" runat="server" Text='<%# Bind("ACD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grd" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrm" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Value">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="lblvalue" runat="server"  CssClass="text-right" ></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle  />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle  CssClass="gridheader_pop" />
                                            <RowStyle CssClass="griditem"  />
                                            <SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div> 


</asp:Content>

