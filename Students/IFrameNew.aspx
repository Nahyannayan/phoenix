<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IFrameNew.aspx.vb" Inherits="IFrameNew" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
<script type="text/javascript" language="javascript">
var getAcrobatInfo = function() {
 
var getBrowserName = function() {
return this.name = this.name || function() {
var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";
 
if(userAgent.indexOf("chrome") > -1) return "chrome";
else if(userAgent.indexOf("safari") > -1) return "safari";
else if(userAgent.indexOf("msie") > -1) return "ie";
else if(userAgent.indexOf("firefox") > -1) return "firefox";
return userAgent;
}();
};
 
var getActiveXObject = function(name) {
try { return new ActiveXObject(name); } catch(e) {}
};
 
var getNavigatorPlugin = function(name) {
for(key in navigator.plugins) {
var plugin = navigator.plugins[key];
if(plugin.name == name) return plugin;
}
};
 
var getPDFPlugin = function() {
return this.plugin = this.plugin || function() {
if(getBrowserName() == 'ie') {
//
// load the activeX control
// AcroPDF.PDF is used by version 7 and later
// PDF.PdfCtrl is used by version 6 and earlier
return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
}
else {
return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
}
}();
};
 
var isAcrobatInstalled = function() {
return !!getPDFPlugin();
};
 
var getAcrobatVersion = function() {
try {
var plugin = getPDFPlugin();
 
if(getBrowserName() == 'ie') {
var versions = plugin.GetVersions().split(',');
var latest = versions[0].split('=');
return parseFloat(latest[1]);
}
 
if(plugin.version) return parseInt(plugin.version);
return plugin.name
}
catch(e) {
return null;
}
}
 
//
// The returned object
//
return {
browser: getBrowserName(),
acrobat: isAcrobatInstalled() ? 'installed' : false,
acrobatVersion: getAcrobatVersion()
};
};

function foo(viewDirect, viewURL) {
   
    var info = getAcrobatInfo();
    document.getElementById("<%=h_browser.ClientID %>").value=info.browser;
    document.getElementById("<%=h_acrobat.ClientID %>").value=info.acrobat;
    document.getElementById("<%=h_version.ClientID %>").value=info.acrobatVersion;
    if (info.acrobat) 
       document.getElementById('iFrame').src=viewDirect;
    else
       document.getElementById('iFrame').src=viewURL;
    }

</script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <iframe id="iFrame" runat="server" scrolling="auto"></iframe>
            <%-- <asp:HyperLink ID="lb" runat="server" Text="Download File" Visible="false"></asp:HyperLink>--%>
        </div>
    <asp:HiddenField ID="h_browser" runat="server" />
    <asp:HiddenField ID="h_acrobat" runat="server" />
    <asp:HiddenField ID="h_version" runat="server" />
    </form>
</body>
</html>
