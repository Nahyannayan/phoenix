<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studSetCalendar.aspx.vb" Inherits="Students_studSetHoliday" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function ShowHDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 370px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var result;


            result = window.showModalDialog(url, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }

            return false;
        }
        function onCancel() {
            // var postBack = new Sys.WebForms.PostBackAction(); 
            //postBack.set_target('CancelButton'); 
            //postBack.set_eventArgument(''); 
            // postBack.performAction();
        }

        function onOk() {
            var postBack = new Sys.WebForms.PostBackAction();
            postBack.set_target('okButton');
            postBack.set_eventArgument('');
            postBack.performAction();
        }



        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }


        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)
            else if (mode == 2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)

        if (result == '' || result == undefined) {
            //            document.getElementById("txtDate").value=''; 
            return false;
        }
        if (mode == 1)
            document.getElementById('<%=txtFrom.ClientID %>').value = result;
        else if (mode == 2)
            document.getElementById('<%=txtTo.ClientID %>').value = result;
       return true;
   }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Attendance Calender</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center"  colspan="4"  >
                                        <table width="100%" >
                                            <tr><td>
                                        <table width="100%" >
                                            <tr>
                                                <td align="center" colspan="4" valign="middle">
                                                    <asp:Calendar ID="calDatepicker" runat="server" BackColor="White" BorderColor="Silver"
                                                        ForeColor="#663399" NextPrevFormat="ShortMonth" SelectionMode="DayWeekMonth"
                                                        SelectMonthText="Select All" ShowGridLines="True" Width="100%"  ShowNextPrevMonth="False">
                                                        <SelectedDayStyle BackColor="#5E5EFF" Font-Bold="True" />
                                                        <SelectorStyle BackColor="#a8dd99" />
                                                        <TodayDayStyle BackColor="#E0E0E0" ForeColor="Black" />
                                                        <OtherMonthDayStyle BackColor="#e2f3c9" ForeColor="#CC9966" />
                                                        <DayStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="False" />
                                                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                                                        <DayHeaderStyle BackColor="#a8dd99" Font-Bold="True" ForeColor="Black" />
                                                        <TitleStyle BackColor="#5c8d22" 
                                                            ForeColor="#FFFFFF" />
                                                    </asp:Calendar>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Year</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlyear" runat="server"   AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    </td>
                                                <td align="left" width="20%"><span class="field-label">Month</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlMonth" runat="server"  AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    </td>
                                            </tr>
                                        </table>
                                                </td></tr>
                                            </table>
                                        <span style="float: left; display: block; left: 0px;">
                                            <div align="left">
                                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Font-Size="10px" />
                                            </div>
                                            <div align="left">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                                    ValidationGroup="AttGroup" DisplayMode="List" Font-Size="10px" ForeColor="" />
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList ></td>
                                    <td align="left" width="20%"><span class="field-label">Weekend</span></td>
                                    <td align="left" width="30%"><span class="field-value">
                                        <asp:Literal ID="ltWeekend" runat="server"></asp:Literal></span></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From Date<font color="red" class="error">*</font></span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            />
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator></td>
                                    
                                    
                                    
                                    
                                    <td align="left"><span class="field-label">To Date<font color="red" class="error">*</font></span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                             />
                                        <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txtTo" Display="Dynamic"
                                            ErrorMessage="To Date required" ValidationGroup="AttGroup" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Grade &amp; Section</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGradeSection" runat="server" CssClass="multitext"
                                            SkinID="MultiText" TextMode="MultiLine">
                                        </asp:TextBox><asp:RequiredFieldValidator ID="rfvGradeSection" runat="server" ControlToValidate="txtGradeSection"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Grade & Section required" ForeColor=""
                                            ValidationGroup="AttGroup">*</asp:RequiredFieldValidator><br />
                                        <asp:LinkButton ID="LinkButton1" runat="server">Please click to select Grade & Section.</asp:LinkButton><br />

                                        <asp:Panel ID="plGrade" runat="server" CssClass="panel-cover">
                                            <div class="msg_header" style="width: 100%; margin-top: -2px; margin-left: -2px; margin-right: -2px">
                                                <div class="title-bg">Select Grade & Section</div>
                                            </div>
                                             <div style="overflow:auto; width:100%; " id="left">
                                            <asp:TreeView ID="tvGrade" runat="server"
                                                ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked();" ExpandDepth="1">
                                                <DataBindings>
                                                    <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand" TextField="Text" ValueField="ValueField"></asp:TreeNodeBinding>
                                                </DataBindings>
                                            </asp:TreeView>
                                                 </div>
                                            <p style="text-align: center;">
                                                <asp:Button ID="okButton" runat="server" Text="OK" CssClass="button" OnClick="okButton_Click" />
                                                <asp:Button ID="CancelButton" runat="server" CssClass="button"
                                                    Text="Cancel" />
                                            </p>

                                        </asp:Panel>
                                    </td>
                                    <td align="left"><span class="field-label">Att Reqd For<font color="red" class="error">*</font></span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAttReq" runat="server">
                                            <asp:ListItem>None</asp:ListItem>
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Set Working Weekend</span></td>
                                    <td align="left" colspan="3">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkWeekend1" runat="server" TextAlign="Left" CssClass="field-label"></asp:CheckBox></td>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:Literal ID="ltWeekend1" runat="server"></asp:Literal></span></td>
                                                <td>
                                                    <asp:CheckBox ID="chkLogBook1" runat="server" TextAlign="Left" CssClass="field-label"></asp:CheckBox></td>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:Literal ID="ltWeekendlog1" runat="server"></asp:Literal></span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkWeekend2" runat="server" TextAlign="Left"></asp:CheckBox></td>
                                                <td><span class="field-label">
                                                    <asp:Literal ID="ltWeekend2" runat="server"></asp:Literal></span></td>
                                                <td>
                                                    <asp:CheckBox ID="chkLogBook2" runat="server" TextAlign="Left"></asp:CheckBox></td>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:Literal ID="ltWeekendlog2" runat="server"></asp:Literal></span></td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Remarks<font color="red" class="error">*</font></span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                            Display="Dynamic" ErrorMessage="Remarks field cannot be left empty" ValidationGroup="AttGroup" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" OnClick="btnAdd_Click" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Edit" OnClick="btnEdit_Click" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                        ValidationGroup="AttGroup" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                            CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /><asp:Button ID="btnDelete" runat="server" CausesValidation="False"
                                                CssClass="button" Text="Delete" OnClick="btnDelete_Click" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="LinkButton1" DropShadow="true"
                    PopupControlID="plGrade" BackgroundCssClass="modalBackground" OnCancelScript="onCancel();" OnOkScript="onOk();" />
                &nbsp; &nbsp;&nbsp;
   
   
            </div>
        </div>
    </div>
         <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar"
        TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
         <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
        TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
    <script type="text/javascript">//<![CDATA[ 
        function resize() {
            var heights = window.innerHeight;
            document.getElementById("left").style.height = (heights - 200) + "px";
        }
        resize();
        window.onresize = function () {
            resize();
        };
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    resize();
                    window.onresize = function () {
                        resize();
                    };
                }
            });
        };
        //]]>  

        </script>

</asp:Content>

