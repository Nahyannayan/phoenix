<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studServices.aspx.vb" Inherits="Students_studServices" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript" >
function getDate(obj) 
          {     
        
            var sFeatures;
            sFeatures="dialogWidth: 227px; ";
            sFeatures+="dialogHeight: 252px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: no; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture="../Accounts/calendar.aspx?nofuture=yes";
           result = window.showModalDialog("../Accounts/calendar.aspx","", sFeatures);
        
      
           var str= obj.getAttribute("id"); 
           var str1=str.replace(/imgDate/,"txtDate")
           document.getElementById(str1).value=result;
          return false;
    }  
    
    function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "input") 
return obj; 
}
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> Student Services
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="left" width="100%">
          <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:"
                    ValidationGroup="groupM1" />
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
             
                </td>
        </tr>
        <tr><td align="center">
     <table id="Table1" runat="server" align="center" width="100%">    
            
         <tr>
             <td align="left">
                 <span class="field-label">Student ID</span></td>
             
             <td align="left">
                 <asp:TextBox ID="txtSen" runat="server" CausesValidation="True" ValidationGroup="groupM1" Width="182px" TabIndex="2" MaxLength="3"></asp:TextBox></td>
             <td align="left">
                 <span class="field-label">Name</span></td>
             
             <td align="left" colspan="2">
                 <asp:TextBox ID="txtName" runat="server" CausesValidation="True" ValidationGroup="groupM1" Width="170px" TabIndex="2" MaxLength="3"></asp:TextBox></td>
         </tr>
           
            <tr>
            <td align="left"  >
            <span class="field-label">Service</span></td>
           
            <td align="left">
            <asp:DropDownList ID="ddlService" runat="server" Width="211px">
            </asp:DropDownList>
            </td>
            <td align="left">
            <span class="field-label">Date</span></td>
           
            <td align="left"  >
            <asp:TextBox ID="txtDate" runat="server" CausesValidation="True" ValidationGroup="groupM1" Width="102px" TabIndex="2" MaxLength="3"></asp:TextBox><asp:ImageButton id="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RegularExpressionValidator
                id="revLastAttendance" runat="server" ControlToValidate="txtDate" CssClass="error"
                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                    id="rfvLast_Attend" runat="server" ControlToValidate="txtDate" CssClass="error"
                    Display="Dynamic" ErrorMessage="Please enter the date" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                    
                   
            <td align="left" >
            <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3" Width="43px" /></td>
            </tr>

            <tr>
            <td align="center" colspan="5" width="100%">
                <asp:LinkButton id="lnkHistory" runat="server" CssClass="text-right">Show History</asp:LinkButton>
            <asp:GridView ID="gvService" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
            PageSize="20" Width="100%"  >
            <RowStyle CssClass="griditem" />
            
            <Columns>
            <asp:TemplateField HeaderText="Service" Visible="False"><ItemTemplate>
            <asp:Label ID="lblSsvId" runat="server" text='<%# Bind("SSV_ID") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Service" Visible="False"><ItemTemplate>
            <asp:Label ID="lblSvcId" runat="server" text='<%# Bind("SSV_SVC_ID") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Service"><ItemTemplate>
            <asp:Label ID="lblService" runat="server" text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="From"><ItemTemplate>
            <asp:Label ID="lblBoard" runat="server" text='<%# Bind("SSV_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="To"><ItemTemplate>
            <asp:Label ID="lblDate" Width="120px" runat="server" text='<%# Bind("SSV_TODATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
            <asp:TextBox id="txtDate" visible="false" tabIndex=2 runat="server" CausesValidation="True" Width="102px" ValidationGroup="groupM1" MaxLength="3">
            </asp:TextBox><asp:ImageButton id="imgDate" visible="false" OnClientClick="getDate(this);" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:ButtonField CommandName="edit" Text="Remove" HeaderText="Remove">
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></ItemStyle>
            </asp:ButtonField>
            
             <asp:TemplateField HeaderText="Service" Visible="False"><ItemTemplate>
            <asp:Label ID="bRemove" runat="server" text='<%# Bind("bRemove") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            
</Columns>
            <SelectedRowStyle CssClass="Green" />
            <HeaderStyle CssClass="gridheader_pop" />
            <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>

            </td>
            </tr>

<tr>
<td align="center" colspan="4">
&nbsp;
<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
<asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
Text="Cancel" UseSubmitBehavior="False" />
</td>
</tr>
</table>
            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
            </ajaxToolkit:CalendarExtender>
            &nbsp;
            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                Format="dd/MMM/yyyy" PopupButtonID="txtDate" TargetControlID="txtDate">
            </ajaxToolkit:CalendarExtender>
            <asp:HiddenField id="hfSTU_ID" runat="server">
            </asp:HiddenField>&nbsp;
            <asp:HiddenField ID="hfACD_ID" runat="server" />
</td></tr>
     
                  
   </table>
                </div>
            </div>
        </div>

</asp:Content>

