<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTc_Student_ChangeReq_M.aspx.vb" Inherits="Students_studTc_Student_ChangeReq_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i><asp:Label ID="lblTitle" runat="server" Text="Student Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">


    
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left" >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" />
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
               <tr>
            <td   valign="bottom">
               
                <table ID="tbTCSO" runat="server" 
                    CellPadding="4" CellSpacing="0"   style="width: 100%">
                    
                    
                    <tr>
                        <td  align="left"><span class="field-label">Student Id(Fees)</span></td>
                       
                        <td align="left">  <asp:TextBox ID="txtStudID_Fee" runat="server"></asp:TextBox></td>
                        <td  align="left"><span class="field-label">Date of Join</span></td>
                        
                        <td  align="left"><asp:TextBox ID="txtDoJ" runat="server" Width="110px"></asp:TextBox></td>
                    </tr>
                    <tr >
                        <td  align="left"><span class="field-label">Grade</span></td>
                        
                        <td  align="left"> <asp:TextBox ID="txtGrade" runat="server" Style="position: relative" Width="34px"></asp:TextBox></td>
                        <td align="left"><span class="field-label">Section</span></td>
                       
                        <td align="left"><asp:TextBox ID="txtSection" runat="server" Width="37px"></asp:TextBox></td>
                    </tr>
                    <tr>
                      <td  align="left" width="20%"><span class="field-label">Name</span></td>
                      
                        <td   align="left">
                            <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>&nbsp;&nbsp; &nbsp;
                        </td>
                    
                    
                    </tr>
                    <tr Class="title-bg">
                                   
                    <td  colspan="5">
                    <span>
                      TC Details </span></td>
                    
                    
                    </tr>
                    <tr >
                   <td align="left"  >
                       <span class="field-label">Tc Ref No</span></td>
                  
                   <td  align="left"><asp:TextBox ID="txtRefNo" runat="server" Width="109px" Enabled="False"></asp:TextBox></td>
                   <td align="left" > <span class="field-label">Tc Apply Date</span></td>
                   
                   <td align="left" ><asp:TextBox ID="txtApplyDate" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                           &nbsp;&nbsp;&nbsp;
                   </td>
                    </tr>
                    <tr >
                    <td  align="left"><span class="field-label">Last Attendance Date</span></td>
                    
                    <td  align="left"> <asp:TextBox ID="txtLast_Attend" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                            &nbsp;&nbsp;
                    </td>
                  <td  align="left"><span class="field-label">Leaving Date</span></td>
                 
                 <td  align="left"><asp:TextBox ID="txtLeave_Date" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                            &nbsp;&nbsp;</td>
                    </tr>
                    
                    <tr >
                        <td  align="left">
                            <span class="field-label">Transfer Type</span></td>
                     
                     <td align="left"> <asp:TextBox ID="txtTCtype" runat="server" Width="151px" Enabled="False"></asp:TextBox></td>
                     <td  align="left"><span class="field-label">Change Reason</span></td>
                     
                     <td align="left"> 
                         <asp:RadioButton ID="rbDataEntry" runat="server" Checked="True" 
                             GroupName="reason" Text="Data Entry Mistake" />
                         <asp:RadioButton ID="rbParentRequest" runat="server" GroupName="reason" 
                             Text="Parent Request" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Requesting For Tc</span><span style="color: #c00000">*</span></td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlTransferType" runat="server" Width="160px">
                            </asp:DropDownList></td>
                        <td align="left" >
                            <span class="field-label">Reason for Change</span></td>
                       
                        <td align="left">
                            <asp:TextBox id="txtReason" runat="server" Height="67px" SkinID="MultiText"
                                TextMode="MultiLine" Width="201px"></asp:TextBox></td>
                    </tr>
                    
                    <tr ID="r12" runat="server" >
                      <td  align="left" width="20%">
                         <span class="field-label"> Request Date</span></td>
                      
                      <td align="left">
                          &nbsp;<asp:TextBox ID="txtReqDate" runat="server" Width="110px"></asp:TextBox>
                          <asp:ImageButton id="imgReqDate" runat="server" ImageUrl="~/Images/calendar.gif">
                          </asp:ImageButton></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td  style="height: 17px; width: 793px;" valign="bottom">
            </td>
        </tr>
        <tr>
            <td align="center" valign="bottom">
                <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtReqDate"
                    Display="None" ErrorMessage="Please enter data in the field Request Date" ValidationGroup="groupM1"
                    Width="23px"></asp:RequiredFieldValidator>
                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" />
                </td>
        </tr>
        <tr>
            <td  style="width: 793px; height: 41px" valign="bottom"><asp:HiddenField ID="hfACD_ID" runat="server" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;&nbsp;
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgReqDate" TargetControlID="txtReqDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtReqDate" TargetControlID="txtReqDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfTCM_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSTU_ID" runat="server" /><asp:HiddenField ID="hfTCT_CODE" runat="server" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</div>
            </div>
        </div>

</asp:Content>

