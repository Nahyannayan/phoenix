Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class Students_studLeave_Entry
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call bindAcademic_Year()
                    Call GetEmpID()

                    Call bindAcademic_Grade()
                    Call bindAcademic_Section()
                    Call bindAcademic_SHIFT()
                    Call bindAcademic_STREAM()

                    txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet

            str_Sql = " SELECT distinct GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER FROM  STAFF_AUTHORIZED_ATT INNER JOIN " & _
 " GRADE_BSU_M ON STAFF_AUTHORIZED_ATT.SAD_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND  STAFF_AUTHORIZED_ATT.SAD_GRD_ID = GRADE_BSU_M.GRM_GRD_ID " & _
" INNER JOIN  EMPLOYEE_M ON STAFF_AUTHORIZED_ATT.SAD_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
" where EMPLOYEE_M.EMP_ID ='" & ViewState("EMP_ID") & "' and STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlGrade.DataSource = ds.Tables(0)
                ddlGrade.DataTextField = "GRM_DISPLAY"
                ddlGrade.DataValueField = "GRD_ID"
                ddlGrade.DataBind()
            End If

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If

            Dim str_Sql As String = " SELECT distinct SECTION_M.SCT_DESCR as SCT_DESCR , STAFF_AUTHORIZED_ATT.SAD_SCT_ID as SCT_ID, SECTION_M.SCT_GRD_ID FROM  STAFF_AUTHORIZED_ATT INNER JOIN " & _
 " SECTION_M ON STAFF_AUTHORIZED_ATT.SAD_ACD_ID = SECTION_M.SCT_ACD_ID AND STAFF_AUTHORIZED_ATT.SAD_SCT_ID = SECTION_M.SCT_ID AND STAFF_AUTHORIZED_ATT.SAD_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN " & _
 " GRADE_BSU_M ON SECTION_M.SCT_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND SECTION_M.SCT_GRD_ID = GRADE_BSU_M.GRM_GRD_ID where STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & ViewState("EMP_ID") & "' and STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' and SECTION_M.SCT_GRD_ID='" & GRD_ID & "' order by SECTION_M.SCT_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub bindAcademic_SHIFT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = "SELECT  distinct   SHIFTS_M.SHF_ID as SHF_ID, SHIFTS_M.SHF_DESCR as  SHF_DESCR " & _
           " FROM  STAFF_AUTHORIZED_ATT INNER JOIN  SHIFTS_M ON STAFF_AUTHORIZED_ATT.SAD_SHF_ID = SHIFTS_M.SHF_ID where STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & ViewState("EMP_ID") & "' " & _
       " and STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' order by  SHIFTS_M.SHF_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlShift.Items.Clear()
            ddlShift.DataSource = ds.Tables(0)
            ddlShift.DataTextField = "SHF_DESCR"
            ddlShift.DataValueField = "SHF_ID"
            ddlShift.DataBind()

            If ViewState("datamode") = "view" Then
                ddlShift.ClearSelection()
                ddlShift.Items.FindByValue(ViewState("SAD_SHF_ID")).Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_STREAM()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = " SELECT distinct  STREAM_M.STM_ID as STM_ID, STREAM_M.STM_DESCR as STM_DESCR FROM STREAM_M INNER JOIN STAFF_AUTHORIZED_ATT ON " & _
" STREAM_M.STM_ID = STAFF_AUTHORIZED_ATT.SAD_STM_ID where STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & ViewState("EMP_ID") & "' and STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' order by STREAM_M.STM_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlStream.Items.Clear()
            ddlStream.DataSource = ds.Tables(0)
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call bindAcademic_Grade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call bindAcademic_Section()
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblStud_ID As New Label
            Dim url As String
            Dim viewid As String
            lblStud_ID = TryCast(sender.FindControl("lblStud_ID"), Label)
            viewid = lblStud_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studLeave_Entry_Edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text <> "" Then
                Dim strfDate As String = txtDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuLeave_Entry")
            Return "-1"
        End Try

    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ValidateDate() = "0" Then


            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

            Dim GRD_ID As String
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SCT_ID As String
            If ddlSection.SelectedIndex = -1 Then
                SCT_ID = ""
            Else
                SCT_ID = ddlSection.SelectedItem.Value
            End If
            Dim STM_ID As String
            If ddlStream.SelectedIndex = -1 Then
                STM_ID = ""
            Else
                STM_ID = ddlStream.SelectedItem.Value
            End If
            Dim SHF_ID As String
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim AttDate As String
            AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = " SELECT ROW_NUMBER() OVER (ORDER BY STUDENT_M.STU_FIRSTNAME)  AS SRNO,    STUDENT_M.STU_ID AS STU_ID, STUDENT_M.STU_NO AS STU_NO," & _
 " ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME),'')  + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),'') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME),'') AS STUDNAME " & _
 " FROM  STUDENT_M INNER JOIN STAFF_AUTHORIZED_ATT ON STUDENT_M.STU_ACD_ID = STAFF_AUTHORIZED_ATT.SAD_ACD_ID AND " & _
 " STUDENT_M.STU_GRD_ID = STAFF_AUTHORIZED_ATT.SAD_GRD_ID And STUDENT_M.STU_SCT_ID = STAFF_AUTHORIZED_ATT.SAD_SCT_ID " & _
" AND  STUDENT_M.STU_STM_ID = STAFF_AUTHORIZED_ATT.SAD_STM_ID And STUDENT_M.STU_SHF_ID = STAFF_AUTHORIZED_ATT.SAD_SHF_ID " & _
" LEFT OUTER JOIN TCM_M  ON TCM_M.TCM_STU_ID=STUDENT_M.STU_ID AND (TCM_M.TCM_CANCELDATE IS NULL)  where  STUDENT_M.STU_CURRSTATUS <> 'CN' AND " & _
"STUDENT_M.STU_ACD_ID='" & ACD_ID & "' and STUDENT_M.STU_GRD_ID='" & GRD_ID & "' and STUDENT_M.STU_SCT_ID='" & SCT_ID & "' and  STUDENT_M.STU_STM_ID='" & STM_ID & "' and STUDENT_M.STU_SHF_ID='" & SHF_ID & "'" & _
" and  ('" & AttDate & "'<=TCM_M.TCM_LASTATTDATE  OR convert(datetime,TCM_M.TCM_LASTATTDATE) is null) AND ('" & AttDate & "' >=SAD_FROMDT  AND '" & AttDate & "' <= ISNULL(SAD_TODT,'" & AttDate & "')) " & _
" and STUDENT_M.STU_DOJ <= convert(datetime,'" & AttDate & "')  and  STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & EMP_ID & "'"

            '" SELECT ROW_NUMBER() OVER (ORDER BY STUDENT_M.STU_FIRSTNAME)  AS SRNO,    STUDENT_M.STU_ID AS STU_ID, STUDENT_M.STU_NO AS STU_NO, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME),'') " & _
            '                        " + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),'') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME),'') AS STUDNAME " & _
            '                 " FROM  STUDENT_M INNER JOIN STAFF_AUTHORIZED_ATT ON STUDENT_M.STU_ACD_ID = STAFF_AUTHORIZED_ATT.SAD_ACD_ID AND " & _
            '                       "  STUDENT_M.STU_GRD_ID = STAFF_AUTHORIZED_ATT.SAD_GRD_ID AND STUDENT_M.STU_SCT_ID = STAFF_AUTHORIZED_ATT.SAD_SCT_ID AND " & _
            '        " STUDENT_M.STU_STM_ID = STAFF_AUTHORIZED_ATT.SAD_STM_ID And STUDENT_M.STU_SHF_ID = STAFF_AUTHORIZED_ATT.SAD_SHF_ID " & _
            '   " where STUDENT_M.STU_CURRSTATUS <> 'CN' AND STUDENT_M.STU_ACD_ID='" & ACD_ID & "' and STUDENT_M.STU_GRD_ID='" & GRD_ID & "' and STUDENT_M.STU_SCT_ID='" & SCT_ID & "' and  STUDENT_M.STU_STM_ID='" & STM_ID & "' and STUDENT_M.STU_SHF_ID='" & SHF_ID & "' " & _
            '   "  and  ('" & AttDate & "'<=STU_LEAVEDATE  OR convert(datetime,STU_LEAVEDATE) is null) AND ('" & AttDate & "' >=SAD_FROMDT  AND '" & AttDate & "' <= ISNULL(SAD_TODT,'" & AttDate & "'))  and STUDENT_M.STU_DOJ <= convert(datetime,'" & AttDate & "') " & _
            '   " and  STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & EMP_ID & "'"

            Dim ds As New DataSet
            Try

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                '& ViewState("str_filter_Year") & str_filter_C_DESCR & str_filter_STARTDT & str_filter_ENDDT & CurrentDatedType & str_filter_OPENONLINE & "  order by  a.Y_DESCR")

                If ds.Tables(0).Rows.Count > 0 Then

                    gvInfo.DataSource = ds.Tables(0)
                    gvInfo.DataBind()

                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    'start the count from 1 no matter gridcolumn is visible or not
                    ' ds.Tables(0).Rows(0)(6) = True

                    gvInfo.DataSource = ds.Tables(0)
                    Try
                        gvInfo.DataBind()
                    Catch ex As Exception
                    End Try

                    Dim columnCount As Integer = gvInfo.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvInfo.Rows(0).Cells.Clear()
                    gvInfo.Rows(0).Cells.Add(New TableCell)
                    gvInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvInfo.Rows(0).Cells(0).Text = "Record not available!!!"
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
End Class
