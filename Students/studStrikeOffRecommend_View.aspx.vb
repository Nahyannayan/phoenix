Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studStrikeOffRecommend_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim str_sql As String = ""

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state
            If ViewState("datamode") = "view" Then

                ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

            End If

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100250" And ViewState("MainMnu_code") <> "S100251" _
            And ViewState("MainMnu_code") <> "S100252" And ViewState("MainMnu_code") <> "S100253" _
            And ViewState("MainMnu_code") <> "S100254") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("datamode") = "add"

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                GridBind()

                Select Case ViewState("MainMnu_code")
                    Case "S100250"
                        lblTitle.Text = "Strike Off Recommendation"
                    Case "S100251"
                        lblTitle.Text = "Strike Off Approval"
                    Case "S100252"
                        lblTitle.Text = "Strike Off"
                    Case "S100253"
                        lblTitle.Text = "Strike Off Cancellation Request"
                    Case "S100254"
                        lblTitle.Text = "Cancel Strike Off"
                End Select

            End If
                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try

        Else



        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim str_query1 As String


        If ViewState("MainMnu_code") <> "S100254" Then
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                       & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " _
                       & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                       & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                       & " WHERE STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + ""
            '& " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "


            If ViewState("MainMnu_code") = "S100250" Then
                'StrikeOff Recommendation
                str_query += " AND STU_ID NOT IN(SELECT STK_STU_ID FROM STRIKEOFF_RECOMMEND_M WHERE STK_bAPPROVED='TRUE' AND STK_CANCELDATE IS NULL)" _
                             & " AND STU_ID NOT IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                           & " AND TCM_CANCELDATE IS NULL)"
            ElseIf ViewState("MainMnu_code") = "S100251" Then
                'Strike Off Approval
                str_query += " AND STU_ID  IN(SELECT STK_STU_ID FROM STRIKEOFF_RECOMMEND_M WHERE STK_bAPPROVED IS NULL AND STK_CANCELDATE IS NULL)" _
                            & " AND STU_ID NOT IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                           & " AND TCM_CANCELDATE IS NULL)"
            ElseIf ViewState("MainMnu_code") = "S100252" Then
                'Strike Off
                str_query += " AND STU_ID IN(SELECT STK_STU_ID FROM STRIKEOFF_RECOMMEND_M WHERE STK_bAPPROVED='TRUE' AND STK_CANCELDATE IS NULL)" _
                           & " AND STU_ID NOT IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                           & " AND TCM_CANCELDATE IS NULL)"
            Else
                'Strike Off Cancellation Request
                str_query += " AND STU_ID IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_TCSO='SO' " _
                            & " AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL)"
            End If
        ElseIf ViewState("MainMnu_code") = "S100254" Then

            ' STRIKE OFF CANCEL
            ' SELECT RECORDS FOR WHICH LEAVE DATE HAS NOT EXCEEDED THE CURRENT DATE AND
            ' IF THE LEAVE DATE HAS EXCEEDED THE CURRENT DATE THEN SELECT THAT RECORD ONLY IF A CANCEL REQUEST IS ENTERED

            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " _
                        & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                        & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID " _
                        & " WHERE TCM_CANCELDATE IS NULL AND STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
                        & " AND  CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) <= CONVERT(datetime,GETDATE()) AND TCM_CANCELREQDATE IS NOT NULL  " _
                        & " AND TCM_TCSO='SO' AND TCM_TC_ID IS NULL"

            str_query += " UNION ALL "
            str_query += "Select STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')), " _
                         & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR from dbo.STRIKEOFF_RECOMMEND_M  A " _
                         & " INNER JOIN Student_M B WITH (NOLOCK) ON STK_STU_ID=STU_ID " _
                         & " INNER JOIN GRADE_BSU_M AS C ON B.STU_GRM_ID=C.GRM_ID  " _
                         & " INNER JOIN SECTION_M AS D ON B.STU_SCT_ID=D.SCT_ID " _
                         & " WHERE STK_bAPPROVED<>'0' AND STK_CANCELDATE IS NULL AND STK_STU_ID NOT IN " _
                         & " (SELECT TCM_STU_ID From TCM_M WHERE TCM_CANCELDATE IS NULL) " _
                        & "  AND STU_ACD_ID='" & ddlAcademicYear.SelectedValue.ToString & "'"

            str_query1 += "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " _
                        & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                        & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID " _
                        & " WHERE TCM_CANCELDATE IS NULL AND STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
                        & " AND  CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND TCM_CANCELREQDATE IS NOT NULL  " _
                        & " AND TCM_TCSO='SO' AND TCM_TC_ID IS NULL"


        End If

        str_query += " AND STU_CURRSTATUS<>'CN'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If
        If ViewState("MainMnu_code") <> "S100254" Then
            str_query += " ORDER BY STU_GRD_ID,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If

        If ViewState("MainMnu_code") = "S100254" Then
            str_query1 += strFilter
            str_query += " UNION ALL " + str_query1
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtSection")
        txtSearch.Text = selectedSection

        set_Menu_Img()


    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

   
    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        If e.CommandName = "view" Then
            Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
            Dim lblStuId As Label
            Dim lblStuName As Label
            Dim lblStuNo As Label
            With selectedRow
                lblStuId = .FindControl("lblStuId")
                lblStuName = .FindControl("lblStuName")
                lblStuNo = .FindControl("lblStuNo")
            End With
            If ViewState("MainMnu_code") = "S100250" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studStrikeOffRecommend_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "S100251" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studStrikeOffApproval_M.aspx?MainMnu_code={0}&datamode={1}&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "S100252" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studStrikeOffRequest_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "S100253" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studStrikeOffCancelRequest_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "S100254" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studStrikeOffCancel_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
        End If
    End Sub
End Class
