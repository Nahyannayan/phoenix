Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studTCEntry_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200058" And ViewState("MainMnu_code") <> "S200057") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))

                    txtDocDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                    PopulateGrade()
                    BindTCReason()
                    BindCountry()
                    BindTCType()
                    BindBsu()
                    hfTCM_ID.Value = 0
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    txtGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    txtSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    txtStudID_Fee.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    txtDoJ.Text = Format(Date.Parse(Encr_decrData.Decrypt(Request.QueryString("doj").Replace(" ", "+"))), "dd/MMM/yyyy")
                    hfACY_DESCR.Value = Encr_decrData.Decrypt(Request.QueryString("Acdyear").Replace(" ", "+"))
                    ViewState("TCSO") = Encr_decrData.Decrypt(Request.QueryString("disState").Replace(" ", "+"))
                    BindData()

                    txtName.Enabled = False
                    txtGrade.Enabled = False
                    txtSection.Enabled = False

                    'If ViewState("MainMnu_code") = "S100085" Then
                    '    trPrint.Visible = False
                    'Else
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))

                    txtIssue.Text = Format(Now.Date, "dd/MMM/yyyy")
                    btnSave.ValidationGroup = False
                    btnCancel.Visible = False
                    imgBtnDate.Visible = False
                    imgBtnDocDate.Visible = False
                    imgbtnLeave_date.Visible = False
                    txtSubject.Enabled = False
                    txtDocDate.Enabled = False
                    '  txtFName.Enabled = False
                    'txtDay_Present.Enabled = False
                    'txtTotWork_Day.Enabled = False
                    txtApplyDate.Enabled = False
                    '    txtConduct.Enabled = False
                    txtDoJ.Enabled = False
                    txtLast_Attend.Enabled = False
                    txtLeave_Date.Enabled = False
                    txtOthers.Enabled = False
                    ' ddlCountry.Enabled = False
                    'ddlPromoted.Enabled = False
                    ' ddlReason.Enabled = False
                    'ddlTrans_School.Enabled = False
                    ddlTransferType.Enabled = False
                    'End If
                End If
            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        End If
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_DOCDATE,TCM_REFNO,TCM_APPLYDATE,TCM_LEAVEDATE," _
                                 & " TCM_LASTATTDATE,ISNULL(TCM_REASON,''),TCM_DAYSP,TCM_DAYST,TCM_TCTYPE,ISNULL(TCM_SUBJECTS,'')," _
                                 & " ISNULL(TCM_RESULT,''),ISNULL(TCM_REMARKS,''),TCM_bTFRGEMS,TCM_GEMS_UNIT,TCM_TFRSCHOOL,TCM_TFRCOUNTRY," _
                                 & " TCM_TFRGRADE, TCM_TFRZONE,TCM_ID,ISNULL(TCM_bCLEARANCE,'FALSE'),ISNULL(TCM_CONDUCT,''),ISNULL(TCM_FATHERNAME,'')" _
                                 & " FROM TCM_M WHERE TCM_STU_ID=" + hfSTU_ID.Value _
                                 & " AND TCM_CANCELDATE IS NULL AND TCM_TCSO='" & ViewState("TCSO") & "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            With reader

                txtDocDate.Text = Format(.GetDateTime(0), "dd/MMM/yyyy")
                txtRefNo.Text = .GetString(1)
                txtApplyDate.Text = Format(.GetDateTime(2), "dd/MMM/yyyy")
                txtLeave_Date.Text = Format(.GetDateTime(3), "dd/MMM/yyyy")
                txtLast_Attend.Text = Format(.GetDateTime(4), "dd/MMM/yyyy")
                ddlReason.ClearSelection()
                ddlReason.Items.FindByValue(.GetString(5)).Selected = True
                txtDay_Present.Text = .GetValue(6)
                txtTotWork_Day.Text = .GetValue(7)
                ddlTransferType.Items.FindByValue(.GetValue(8)).Selected = True
                txtSubject.Text = .GetString(9)
                If Not ddlResult.Items.FindByValue(reader.GetString(10)) Is Nothing Then
                    ddlResult.ClearSelection()
                    ddlResult.Items.FindByValue(reader.GetString(10)).Selected = True
                End If
                txtRemarks.Text = reader.GetString(11)
                If reader.GetBoolean(12) = True Then
                    ddlTrans_School.ClearSelection()
                    ddlTrans_School.Items.FindByValue(.GetString(13)).Selected = True
                Else
                    txtOthers.Text = .GetString(14)
                End If

                ddlCountry.ClearSelection()
                ddlCountry.Items.FindByValue(.GetValue(15)).Selected = True
                ddlPromoted.ClearSelection()
                ddlPromoted.Items.FindByValue(.GetString(16))
                txtTranZone.Text = .GetString(17)
                hfTCM_ID.Value = .GetValue(18)
                If .GetBoolean(19) = True Then
                    btnSave.Enabled = False
                End If
                txtConduct.Text = .GetString(20)
                txtFName.Text = .GetString(21)
            End With
        End While
        reader.Close()

        If hfTCM_ID.Value = 0 Then
            str_query = "SELECT ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') FROM " _
                     & " STUDENT_D WHERE STS_STU_ID=(SELECT STU_SIBLING_ID FROM STUDENT_M WHERE STU_ID=" + hfSTU_ID.Value + ")"
            txtFName.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If

    End Sub
    Sub BindBsu()
        Dim str_query As String = "select bsu_id,bsu_name from businessunit_m where (BUS_BSG_ID<>'4') order by bsu_name"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrans_School.DataSource = ds
        ddlTrans_School.DataTextField = "bsu_name"
        ddlTrans_School.DataValueField = "bsu_id"
        ddlTrans_School.DataBind()
        Dim li As New ListItem
        li.Text = "Others"
        li.Value = "0"
        ddlTrans_School.Items.Insert(0, li)
    End Sub
    Sub PopulateGrade()
        ddlPromoted.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A" _
                                 & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                 & " WHERE GRM_ACD_ID=" + hfACD_ID.Value _
                                 & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPromoted.DataSource = ds
        ddlPromoted.DataTextField = "GRM_DISPLAY"
        ddlPromoted.DataValueField = "GRD_ID"
        ddlPromoted.DataBind()
        Dim li As New ListItem
        li.Text = "NA"
        li.Value = "NA"
        ddlPromoted.Items.Insert(0, li)
    End Sub
    Sub BindTCReason()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT [TCR_CODE],[TCR_DESCR]  FROM [OASIS].[dbo].[TC_REASONS_M] order by [TCR_DESCR]"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "TCR_DESCR"
        ddlReason.DataValueField = "TCR_CODE"
        ddlReason.DataBind()
        ddlReason.Items.FindByValue(1).Selected = True
    End Sub
    Sub BindTCType()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] order by [TCT_DESCR]"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTransferType.DataSource = ds
        ddlTransferType.DataTextField = "TCT_DESCR"
        ddlTransferType.DataValueField = "TCT_CODE"
        ddlTransferType.DataBind()
    End Sub
    Sub BindCountry()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "Select CTY_ID,CTY_DESCR  from Country_m order by CTY_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCountry.DataSource = ds
        ddlCountry.DataTextField = "CTY_DESCR"
        ddlCountry.DataValueField = "CTY_ID"
        ddlCountry.DataBind()
        ddlCountry.Items.FindByValue("5").Selected = True

    End Sub


    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " EXEC [STU].[saveLC_ENTRY]  " _
                                 & hfTCM_ID.Value + "," _
                                 & "'" + Session("SBSUID") + "'," _
                                 & "'" + Format(Date.Parse(txtDocDate.Text), "yyyy-MM-dd") + "'," _
                                 & hfACD_ID.Value + "," _
                                 & hfSTU_ID.Value + "," _
                                 & "'" + txtRefNo.Text.Replace("'", "''") + "'," _
                                 & "'" + Format(Date.Parse(txtApplyDate.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + Format(Date.Parse(txtLeave_Date.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + Format(Date.Parse(txtLast_Attend.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + ddlReason.SelectedValue + "'," _
                                 & txtDay_Present.Text.ToString + "," _
                                 & txtTotWork_Day.Text.ToString + "," _
                                 & "'" + ddlTransferType.SelectedValue.ToString + "'," _
                                 & "'" + txtSubject.Text.Replace("'", "''") + "'," _
                                 & "'" + ddlResult.SelectedValue + "'," _
                                 & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                 & "'" + IIf(ddlTrans_School.SelectedValue = "0", "false", "true") + "'," _
                                 & IIf(ddlTrans_School.SelectedValue = "0", "NULL", "'" + ddlTrans_School.SelectedValue + "'") + "," _
                                 & "'" + IIf(ddlTrans_School.SelectedValue = "0", txtOthers.Text.Replace("'", "''"), "NULL") + "'," _
                                 & "'" + ddlCountry.SelectedValue.ToString + "'," _
                                 & "'" + ddlPromoted.SelectedValue.ToString + "'," _
                                 & "'" + txtTranZone.Text.Replace("'", "''") + "'," _
                                 & "'" + txtConduct.Text.Replace("'", "''") + "'," _
                                 & "'" + txtFName.Text.Replace("'", "''") + "'"

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                hfTCM_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using


    End Function


    Sub PrintTc()
        
        Dim str As String = ""
        Dim lstrRegistrar As String = String.Empty

        Dim param As New Hashtable
        str += "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>"

        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@STU_ID", str)
        param.Add("@TCSO", ViewState("TCSO"))
        param.Add("@TCM_ISSUEDATE", Format(Date.Parse(txtDocDate.Text), "yyyy-MM-dd"))


        param.Add("principal", GetEmpName("PRINCIPAL", "FULL"))
        If (ViewState("MainMnu_code") = "S200058") Then
            Dim currYear As String = hfACY_DESCR.Value
            Dim year As Integer = Val(hfACY_DESCR.Value.Split("-")(1))
            Dim nextYear As String
            Dim prevYear As String
            nextYear = year.ToString + "-" + (year + 1).ToString
            prevYear = (year - 1).ToString + "-" + year.ToString
            param.Add("nextYear", nextYear)
            lstrRegistrar = GetEmpName("REGISTRAR", "FULL")
            If lstrRegistrar Is Nothing Then
                lstrRegistrar = ""
            End If
            param.Add("registrar", lstrRegistrar)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
           
            '.reportPath = Server.MapPath("~\Students\Reports\RPT\rptEnglishLC.rpt")
            If ViewState("MainMnu_code") = "S200056" Then
                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptArabicGCC.rpt")
            ElseIf ViewState("MainMnu_code") = "S200057" Then
                If Session("SBsuid") <> 131001 And Session("SBsuid") <> 131002 Then
                    .reportPath = Server.MapPath("~\Students\Reports\RPT\rptArabicSchoolLeaveCer.rpt")
                Else
                    .reportPath = Server.MapPath("~\Students\Reports\RPT\rptArabicSchoolLeaveCer_Sharjah.rpt")
                End If
            ElseIf ViewState("MainMnu_code") = "S200058" Then
                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptEnglishLC.rpt")


            End If

        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

        'Dim jscript As New StringBuilder()

        'Dim URL As String = "../Reports/ASPX Report/rptReportViewer.aspx"
        'jscript.Append("<script>window.open('")
        'jscript.Append(URL)
        'jscript.Append("');</script>")
        'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
        'Response.Write("<Script> window.open('../Reports/ASPX Report/rptReportViewer.aspx') </Script>")


    End Sub

    Function GetEmpName(ByVal designation As String, ByVal sType As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
        '                         & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
        '                         & " AND DES_DESCR='" + designation + "'"
        'Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If emp = Nothing Then
        '    emp = ""
        'End If
        'Return emp

        Dim emp As String
        Dim pParms(3) As SqlClient.SqlParameter
        emp = ""
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@DES_DESCR", designation)
        Using reader_EQS As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_EMPNAME_DESIG", pParms)
            While reader_EQS.Read
                emp = Convert.ToString(reader_EQS("EMP_NAME"))
            End While
        End Using
        Return emp
    End Function
#End Region
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

  
   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Year(Date.Parse(txtLeave_Date.Text)) = Year(Now.Date) Then
            If Month(Date.Parse(txtLeave_Date.Text)) < Month(Now.Date) Then
                lblError.Text = "Leave date in a previous month is not allowed"
                Exit Sub
            End If
        ElseIf Year(Date.Parse(txtLeave_Date.Text)) < Year(Now.Date) Then
            lblError.Text = "Leave date in a previous month is not allowed"
            Exit Sub
        End If
        SaveData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        SaveData()
        PrintTc()
    End Sub
End Class
