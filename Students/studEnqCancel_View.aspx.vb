Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studRegCancel_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100034") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"


                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try



        Else


        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            'Select records for which registration is completed based on the stage order.
            'If the stage order for offer is higher and if it is completed the the record is not selected

            'Dim str_query As String = "SELECT  EQS_ID,EQS_APPLNO,EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, SHF_DESCR,STM_DESCR, " _
            '                          & " APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, '') " _
            '                         & " FROM   ENQUIRY_M AS A INNER JOIN " _
            '                         & "ENQUIRY_SCHOOLPRIO_S AS B ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN" _
            '                         & " GRADE_BSU_M AS C ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
            '                         & " SHIFTS_M AS D ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
            '                         & " STREAM_M AS E ON B.EQS_STM_ID = E.STM_ID " _
            '                         & " WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
            '                         & " AND EQS_BSU_ID='" + Session("sbsuid") + "' AND " _
            '                         & " (SELECT CASE WHEN F.PRA_STG_ORDER<G.PRA_STG_ORDER AND G.PRA_BCOMPLETED='FALSE'" _
            '                         & " THEN F.PRA_BCOMPLETED WHEN F.PRA_STG_ORDER>G.PRA_STG_ORDER THEN F.PRA_BCOMPLETED  " _
            '                         & " ELSE 'FALSE' END  FROM PROCESSFO_APPLICANT_S AS F " _
            '                         & " INNER JOIN PROCESSFO_APPLICANT_S AS G ON F.PRA_EQS_ID=G.PRA_EQS_ID " _
            '                         & " WHERE F.PRA_EQS_ID=B.EQS_ID AND F.PRA_STG_ID=3 AND G.PRA_STG_ID=6)='TRUE'" _
            '                         & " AND EQS_CANCELDATE IS NULL AND EQS_STATUS<>'ENR' AND EQS_STATUS<>'DEL'"


            Dim str_query As String = " SELECT EQS_CURRSTATUS,EQS_STATUS, EQS_ID,EQS_APPLNO,EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY," _
 & " SHF_DESCR,STM_DESCR,  APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') " _
& " + ' ' + ISNULL(EQM_APPLLASTNAME, '') FROM   ENQUIRY_M AS A INNER JOIN " _
& " ENQUIRY_SCHOOLPRIO_S AS B ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN " _
& " GRADE_BSU_M AS C ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN  SHIFTS_M AS D ON " _
& " B.EQS_SHF_ID = D.SHF_ID INNER JOIN  STREAM_M AS E ON B.EQS_STM_ID = E.STM_ID " _
& " WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND EQS_BSU_ID='" + Session("sbsuid") + "'  AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=2 "


            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String

            Dim ddlgvGrade As New DropDownList
            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList

            Dim selectedGrade As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""


            Dim strApplNo As String
            Dim strDate As String
            Dim strName As String


            If gvStudEnquiry.Rows.Count > 0 Then
                Dim txtSearch As New TextBox


                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("EQS_APPLNO", txtSearch.Text, strSearch)
                strApplNo = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                strDate = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("grm_display", txtSearch.Text, strSearch)
                selectedGrade = txtSearch.Text

                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                If ddlgvShift.Text <> "ALL" Then
                    If strFilter = "" Then
                        strFilter = " and shf_descr='" + ddlgvShift.Text + "'"
                    Else
                        strFilter = strFilter + " and shf_descr='" + ddlgvShift.Text + "'"
                    End If

                    selectedShift = ddlgvShift.Text
                End If


                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
                If ddlgvStream.Text <> "ALL" Then
                    If strFilter = "" Then
                        strFilter = " and stm_descr='" + ddlgvStream.Text + "'"
                    Else
                        strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
                    End If

                    selectedStream = ddlgvStream.Text
                End If


                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                strName = txtSearch.Text

                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If


            End If
            Dim ds As DataSet
            Dim dv As DataView
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & " order by EQS_APPLNO desc ,APPL_NAME ")
            gvStudEnquiry.DataSource = ds

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudEnquiry.DataBind()
                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudEnquiry.DataBind()
            End If

            Dim dt As DataTable = ds.Tables(0)

            If gvStudEnquiry.Rows.Count > 0 Then
                Dim txtSearch As New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                txtSearch.Text = strApplNo

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                txtSearch.Text = strDate

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                txtSearch.Text = strName

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
                txtSearch.Text = selectedGrade

                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

                Dim dr As DataRow

                ddlgvShift.Items.Clear()
                ddlgvShift.Items.Add("ALL")


                ddlgvStream.Items.Clear()
                ddlgvStream.Items.Add("ALL")

                For Each dr In dt.Rows
                    With dr

                        ''check for duplicate values and add to dropdownlist 
                        If ddlgvShift.Items.FindByText(.Item("SHF_DESCR")) Is Nothing Then
                            ddlgvShift.Items.Add(.Item("SHF_DESCR"))
                        End If
                        If ddlgvStream.Items.FindByText(.Item("STM_DESCR")) Is Nothing Then
                            ddlgvStream.Items.Add(.Item("STM_DESCR"))
                        End If
                    End With
                Next

                If selectedGrade <> "" Then
                    ddlgvGrade.Text = selectedGrade
                End If


                If selectedShift <> "" Then
                    ddlgvShift.Text = selectedShift
                End If

                If selectedStream <> "" Then
                    ddlgvStream.Text = selectedStream
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + "  NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function


    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvStudEnquiry.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub lbtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim ACD_ID As String = ddlAcademicYear.SelectedValue.ToString
            Dim MainMnu_code As String = String.Empty
            Dim lblEqsId As New Label
            lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)
            Dim datamode As String = Encr_decrData.Encrypt("view")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            MainMnu_code = Encr_decrData.Encrypt("S100034")
            ACD_ID = Encr_decrData.Encrypt(ACD_ID)
            Dim url As String = String.Format("~\Students\studRegOfferCancel_M.aspx?MainMnu_code={0}&datamode={1}&eqsid={2}&mode={3}&ACD_ID={4}", MainMnu_code, datamode, Encr_decrData.Encrypt(lbleqsid.Text), "ENQ", ACD_ID)
            Response.Redirect(url)

            ' Dim lblEqsId As New Label
            'lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)
            'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim pParms(2) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@EQS_ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            'pParms(1) = New SqlClient.SqlParameter("@EQS_ID", lblEqsId.Text)
            'pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            'pParms(2).Direction = ParameterDirection.ReturnValue
            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[ENQ].[SaveCancel_Enquiry]", pParms)
            'Dim ReturnFlag As Integer = pParms(2).Value
            'If ReturnFlag = 0 Then

            '    lblError.Text = "Applicant enquiry cancelled Sucessfully!!! "
            '    GridBind()
            'Else
            '    lblError.Text = "Request could not be processed "
            'End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
End Class
