Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studTcIssue_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try


                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100087") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"
                    ddlGrade.Items.Insert(0, li)

                    hfMaxGrade.Value = ddlGrade.Items(ddlGrade.Items.Count - 1).Value
                    If hfMaxGrade.Value <> "12" And hfMaxGrade.Value <> "13" And hfMaxGrade.Value <> "08" Then
                        hfMaxGrade.Value = "0"
                    End If

                    BindTCType()
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"


                    tblTC.Rows(5).Visible = False
                    tblTC.Rows(6).Visible = False
                    tblTC.Rows(7).Visible = False
                    tblTC.Rows(8).Visible = False
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else


            studClass.SetChk(gvStud, Session("liUserList"))
        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub lblIssue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStuId As Label = TryCast(sender.FindControl("lblStuId"), Label)
        Dim lblGrade As Label = TryCast(sender.FindControl("lblGrade"), Label)
        Dim lblSection As Label = TryCast(sender.FindControl("lblSection"), Label)
        Dim lblStuName As Label = TryCast(sender.FindControl("lblStuName"), Label)
        Dim lblStuno As Label = TryCast(sender.FindControl("lblStuno"), Label)
        Dim lblDoj As Label = TryCast(sender.FindControl("lblDoj"), Label)
        ViewState("datamode") = "view"

        Dim url As String
        'Encrypt the data that needs to be send through Query String
      
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        url = String.Format("~\Students\studTcEntry_M.aspx?MainMnu_code={0}&datamode={1}&stuid={2}&stuno={3}&stuname={4}&grade={5}&section={6}&doj={7}&acdid={8}&grdid={9}&accyear={10}&maxgrade={11}", _
              ViewState("MainMnu_code"), _
              ViewState("datamode"), _
              Encr_decrData.Encrypt(lblStuId.Text), _
              Encr_decrData.Encrypt(lblStuno.Text), _
              Encr_decrData.Encrypt(lblStuName.Text), _
              Encr_decrData.Encrypt(lblGrade.Text), _
              Encr_decrData.Encrypt(lblSection.Text), _
              Encr_decrData.Encrypt(lblDoj.Text), _
              Encr_decrData.Encrypt(hfACD_ID.Value), _
              Encr_decrData.Encrypt(hfGRD_ID.Value), _
              Encr_decrData.Encrypt(hfACY_DESCR.Value), _
              Encr_decrData.Encrypt(hfMaxGrade.Value))
        Response.Redirect(url)
    End Sub

#Region "Private Methods"

    'Sub BindTCType()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String
    '    If Session("SBSUID") <> "800017" Then
    '        str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] order by [TCT_DESCR]"
    '    Else
    '        str_query = "SELECT [TCT_CODE],[TCT_DESCCR_AQABA]  as TCT_DESCR FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCCR_AQABA IS NOT NULL order by [TCT_DESCR]"
    '    End If
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlTcType.DataSource = ds
    '    ddlTcType.DataTextField = "TCT_DESCR"
    '    ddlTcType.DataValueField = "TCT_CODE"
    '    ddlTcType.DataBind()
    'End Sub
    Sub BindTCType()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String
        'If Session("SBSUID") <> "800017" Then
        '    str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] order by [TCT_DESCR]"
        'Else
        '    str_query = "SELECT [TCT_CODE],[TCT_DESCCR_AQABA]  as TCT_DESCR FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCCR_AQABA IS NOT NULL order by [TCT_DESCR]"
        'End If

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sUsr_name")

        pParms(1) = New SqlClient.SqlParameter("@SBSUID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("SBSUID")


        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.StoredProcedure, "Get_TC_FORMATS", pParms)




        ddlTcType.DataSource = ds
        ddlTcType.DataTextField = "TCT_DESCR"
        ddlTcType.DataValueField = "TCT_CODE"
        ddlTcType.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                    & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID, " _
                    & " TCM_APPLYDATE,(SELECT STUFF((SELECT ','+  CASE WHEN TCR_DESCR LIKE 'OTHER%' THEN STR_OTHER_REASON ELSE TCR_DESCR END " _
                    & " FROM TC_REASONS_M INNER JOIN  TCM_STU_REASON_S ON TCR_CODE=STR_REASON_ID" _
                    & " WHERE STR_TCM_ID=D.TCM_ID AND STR_STU_ID=D.TCM_STU_ID ORDER BY TCR_ORDER for xml path('')),1,1,'')) AS TCR_DESCR,TXTISSUE=CASE WHEN TCM_ISSUEDATE IS NULL THEN 'Issue' ELSE 'Issued' END " _
                    & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                    & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                    & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID " _
                    & " WHERE TCM_CANCELDATE IS NULL AND STU_ACD_ID = " + hfACD_ID.Value _
                    & " AND TCM_TCTYPE=" + hfTcTYPE.Value _
                    & " AND TCM_TCSO='TC' AND TCM_bCLEARANCE='TRUE'"

        If rbNotIssued.Checked = True Then
            str_query = str_query + " AND TCM_ISSUEDATE IS NULL"
        ElseIf rbIssued.Checked = True Then
            str_query = str_query + " AND TCM_ISSUEDATE IS NOT NULL"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If hfGRD_ID.Value <> "0" Then
            str_query += " AND STU_GRD_ID='" + hfGRD_ID.Value + "'"
            'ElseIf ddlTcType.SelectedValue <> 4 Then
            '    str_query += " AND STU_GRD_ID NOT IN ('" + hfMaxGrade.Value + "') "
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If
        If ViewState("MainMnu_code") <> "S100254" Then
            str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If Session("sBsuid") = "131001" Then
            gvStud.Columns(9).Visible = False
        End If

        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtSection")
        txtSearch.Text = selectedSection

        set_Menu_Img()


    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Function GetEmpName(ByVal designation As String, ByVal sType As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
        '                         & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
        '                         & " AND DES_DESCR='" + designation + "'"
        'Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If emp = Nothing Then
        '    emp = ""
        'End If
        'Return emp

        Dim emp As String
        Dim pParms(3) As SqlClient.SqlParameter
        emp = ""
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@DES_DESCR", designation)
        Using reader_EQS As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_EMPNAME_DESIG", pParms)
            While reader_EQS.Read
                emp = Convert.ToString(reader_EQS("EMP_NAME"))
            End While
        End Using
        Return emp
    End Function
    Sub PrintTc()

        Dim lstrPrinci As String
        Dim lstrRegistrar As String
        'If (ddlTransferType.SelectedValue <> 2) And (ddlTransferType.SelectedValue <> 6) Then
        If (ddlTcType.SelectedValue <> 2) And (ddlTcType.SelectedValue <> 6) Then
            lstrPrinci = GetEmpName("ARABIC SECRETARY", "Full")
        Else
            lstrPrinci = GetEmpName("PRINCIPAL", "Full")
        End If

        If lstrPrinci Is Nothing Then
            lstrPrinci = ""
        End If

        'If Session("sbsuid").ToString = "131001" Then
        '    lstrPrinci = "Dr. F WASIL"
        'End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@TCM_ISSUEDATE", txtDate.Text)


        Dim stu_XML As String = getStu_XML()
        If stu_XML = "" Then
            lblError.Text = "No records selected"
            Exit Sub
        End If

        If UpdateStudent_Att(stu_XML) <> 0 Then
            lblError.Text = "Error while updating student attendance"
            Exit Sub
        End If

        Dim rptClass As New rptClass

        'KHDA----Start
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        Dim lstrPWD As String = String.Empty
        Dim lstrSelGrade As String = String.Empty


        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@STU_XML", stu_XML)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_MOE_FILETYPE_V2", pParms)
            While reader.Read
                lstrPWD = Convert.ToString(reader("BSU_MOE_FILETYPE"))
                lstrSelGrade = Convert.ToString(reader("SELGRADE"))
            End While
        End Using

        'If lstrPWD = "13" Then
        '    Response.Write("<Script> window.open('studMOEPrint_Sharjah.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        'ElseIf lstrPWD = "11_1" Then
        '    Response.Write("<Script> window.open('studMOEPrint_AUH.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        'ElseIf lstrPWD = "11" Then
        '    Response.Write("<Script> window.open('studMOEPrint_CHS.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        'ElseIf lstrPWD = "14" Then
        '    Response.Write("<Script> window.open('studMOEPrint_ALN.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        'ElseIf lstrPWD = "12_1" Then
        '    Response.Write("<Script> window.open('studMOEPrint_KHDA.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        'ElseIf lstrPWD = "00" Then
        '    Response.Write("<Script> window.open('studMOEPrint.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")

        'End If
        ' KHDA----END


        Dim TC_TYPE As Integer = 0
        TC_TYPE = ddlTcType.SelectedValue
        If Session("sbsuid") = "131001" Then
            TC_TYPE = 1
        End If

        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            Select Case TC_TYPE 'ddlTcType.SelectedValue
                Case 1 'ARABIC - WITHIN ZONE
                    param.Add("@STU_XML", stu_XML)
                    param.Add("principal", lstrPrinci)

                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        If Session("sbsuid") = "131001" Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_Sharjah_OOS.rpt")
                        Else
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_Sharjah.rpt")
                        End If
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_AUH.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_AUH.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone.rpt")
                    End If

                    'ElseIf Session("SBsuid") <> "131001" And Session("SBsuid") <> "131002" And Session("SBsuid") <> "800017" And Session("SBsuid") <> "114003" And Session("SBsuid") <> "114004" And Session("SBsuid") <> "115002" And Session("SBsuid") <> "111001" Then
                    '.reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone.rpt")
                    'ElseIf Session("SBsuid") = "131001" Then
                    '.reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_Sharjah.rpt")
                    'ElseIf Session("SBsuid") = "131002" Then
                    '.reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_Sharjah.rpt")
                    'ElseIf Session("SBsuid") = "114003" Or Session("SBsuid") = "114004" Or Session("SBsuid") = "115002" Or Session("SBsuid") = "111001" Then
                    '.reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_AUH.rpt")
                    'ElseIf (Session("SBsuid") = "800017") Then
                    '.reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    'End If
                    If Session("sbsuid") = "131001" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_Sharjah_OOS.rpt")
                    End If
                Case 2 'ENGLISH tc
                    param.Add("@STU_XML", stu_XML)

                    Dim currYear As String = hfACY_DESCR.Value
                    Dim year As Integer = Val(hfACY_DESCR.Value.Split("-")(1))
                    Dim nextYear As String
                    Dim prevYear As String

                    nextYear = year.ToString + "-" + (year + 1).ToString
                    prevYear = (year - 1).ToString + "-" + year.ToString
                    param.Add("principal", lstrPrinci)
                    param.Add("nextYear", nextYear)
                    lstrRegistrar = GetEmpName("REGISTRAR", "FULL")
                    If lstrRegistrar Is Nothing Then
                        lstrRegistrar = ""
                    End If
                    param.Add("registrar", lstrRegistrar)

                    'If lstrPWD = "12_1" Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_KHDA.rpt")
                    'ElseIf Session("sBSUID") = "800017" Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_Jordan.rpt")
                    'Else
                    '    If hfGRD_ID.Value = hfMaxGrade.Value Then
                    '        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTCGrade12.rpt")
                    '    Else
                    '        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    '        '.reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTCGrade12.rpt")
                    '    End If
                    'End If


                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_OutsideUAE_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        If (lstrSelGrade = hfMaxGrade.Value Or lstrSelGrade = "10") Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTCGrade12_SHJ.rpt")
                        Else
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_SHJ.rpt")
                        End If
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    ElseIf lstrPWD = "11_2" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_UIS.rpt")
                    ElseIf lstrPWD = "81" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_Qatar.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_Jordan.rpt")
                    ElseIf lstrPWD = "12_2" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_WITHMOE.rpt")
                    ElseIf lstrPWD = "22_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_India.rpt")
                    ElseIf lstrPWD = "22_2" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_MAK.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    End If


                Case 3 'ARABIC- OUTSIDE ZONE
                    param.Add("@STU_XML", stu_XML)

                    param.Add("principal", lstrPrinci)

                    'If lstrPWD = "12_1" Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_KHDA.rpt")
                    'ElseIf Session("SBsuid") <> "131001" And Session("SBsuid") <> "131002" And Session("SBsuid") <> "800017" Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    'ElseIf Session("SBsuid") = "131001" Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone_Sharjah.rpt")
                    'ElseIf Session("SBsuid") = "131002" Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone_Sharjah.rpt")
                    'ElseIf (Session("SBsuid") = "800017") Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    'End If



                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone_Sharjah.rpt")
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    End If


                Case 4 'ARABIC-OUTSIDE UAE

                    param.Add("@STU_XML", stu_XML)

                    param.Add("principal", lstrPrinci)
                    If Session("BSU_COUNTRY_ID") = "217" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_EGYPT.rpt")
                    Else


                        'If lstrPWD = "12_1" Then
                        '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_KHDA.rpt")
                        'ElseIf (Session("SBsuid") <> "800017") Then
                        '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                        'Else
                        '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                        'End If

                        If lstrPWD = "12_1" Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_KHDA.rpt")
                        ElseIf lstrPWD = "13" Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                        ElseIf lstrPWD = "11_1" Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                        ElseIf lstrPWD = "11" Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                        ElseIf lstrPWD = "80" Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                        Else
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                        End If
                    End If


                Case 5 'ARABIC LC'
                    param.Add("@STU_ID", stu_XML)
                    param.Add("@TCSO", "TC")
                    param.Add("@IMG_BSU_ID", Session("sbsuid"))
                    param.Add("@IMG_TYPE", "LOGO")
                    param.Add("@TCM_ISSUEDATE", txtDate.Text)
                    param.Add("principal", lstrPrinci)
                    'If Session("SBsuid") <> 131001 And Session("SBsuid") <> 131002 Then
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    'Else
                    '    .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer_Sharjah.rpt")
                    'End If

                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    ElseIf lstrPWD = "13" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer_Sharjah.rpt")
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    End If
                Case 6 'ENGLISH LC'
                    Dim year As Integer = Val(hfACY_DESCR.Value.Split("-")(1))
                    Dim nextYear As String
                    Dim prevYear As String

                    nextYear = year.ToString + "-" + (year + 1).ToString
                    prevYear = (year - 1).ToString + "-" + year.ToString
                    param.Add("principal", lstrPrinci)
                    param.Add("nextYear", nextYear)
                    lstrRegistrar = GetEmpName("REGISTRAR", "FULL")
                    If lstrRegistrar Is Nothing Then
                        lstrRegistrar = ""
                    End If
                    param.Add("registrar", lstrRegistrar)
                    '.reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishLC.rpt")

                    If lstrPWD = "12_1" Then
                        param.Add("@STU_XML", stu_XML)
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptStudentLeavingCertificateOSeas_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        param.Add("@STU_ID", stu_XML)
                        param.Add("@TCSO", "TC")
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishLC_SHJ.rpt")

                    Else

                        param.Add("@STU_ID", stu_XML)
                        param.Add("@TCSO", "TC")
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishLC.rpt")
                    End If

            End Select
           
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Function getStu_XML()
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim str As String = ""
        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblStuId = gvStud.Rows(i).FindControl("lblStuId")
                str += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID></ID>"
            End If
        Next
        If str <> "" Then
            str = "<IDS>" + str + "</IDS>"
        End If
        Return str
    End Function

    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return emp
    End Function
#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfTcTYPE.Value = ddlTcType.SelectedValue
        hfACY_DESCR.Value = ddlAcademicYear.SelectedItem.Text
        hfSTUNO.Value = txtStuNo.Text
       
        Session("liUserList") = New List(Of String)
        GridBind()
        txtDate.Text = Format(Now.Date, "dd/MMM/yyyy")

        tblTC.Rows(5).Visible = True
        tblTC.Rows(6).Visible = True
        tblTC.Rows(7).Visible = True
        tblTC.Rows(8).Visible = True
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        PrintTc()
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Function UpdateStudent_Att(ByVal stu_XML As String) As Integer
        Dim STATUS As Integer = 0

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try

                transaction = conn.BeginTransaction("SampleTransaction")

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
                pParms(1) = New SqlParameter("@stu_XML", stu_XML)
                pParms(2) = New SqlParameter("@USR_ID", Session("sUsr_id"))

                pParms(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SAVETC_ATT_UPDATE", pParms)


                STATUS = CInt(pParms(3).Value)

            Catch ex As Exception
                STATUS = 1
            Finally
                If STATUS <> 0 Then
                    STATUS = 1
                    transaction.Rollback()
                Else
                    STATUS = 0
                    transaction.Commit()
                End If
            End Try

        End Using

        Return STATUS
    End Function
    Function Get_BSU_COUNTRY_ID() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " select [BSU_COUNTRY_ID] " _
                                 & " FROM [OASIS].[dbo].[BUSINESSUNIT_M] where [BSU_ID] ='" + Session("SBSUID") + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return emp.Trim()
    End Function
End Class
