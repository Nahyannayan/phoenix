<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnq_Setting.aspx.vb" Inherits="Students_studEnq_Setting" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function check(checkbox) {
            var cbl = document.getElementById('<%=chkSchool.ClientID%>').getElementsByTagName("input");
            for (i = 0; i < cbl.length; i++) cbl[i].checked = checkbox.checked;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Online Enquiry Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="groupM1"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="center">Fields Marked with (<span
                            style="color: #800000">*</span>) are mandatory</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td class="title-bg" colspan="4">

                                        <asp:Literal ID="ltLabel" runat="server" Text="Enquiry Setting"></asp:Literal>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span><span style="color: #800000">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Options</span><span style="font-size: 8pt; color: #800000"></span><span style="color: #800000">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlOptions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOptions_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Grade</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">From</span><font color="red">*</font></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server">
                                        </asp:TextBox><asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="getDate(1);return false;"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                                Css Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left">
                                        <span class="field-label">To</span><span style="color: #800000">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server">
                                        </asp:TextBox><asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="getDate(2);return false;"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtTo"
                                            Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Message(Line 1)</span></td>

                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtMsg1" runat="server" Height="45px" TabIndex="6" TextMode="MultiLine"
                                            Width="330px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                            MaxLength="150"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtMsg1" Display="Dynamic"
                                            ErrorMessage="Message field cannot be left empty" ForeColor="#800000"
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                        <br />
                                        Sample--&gt;Online Enquiry is currently open for Sibling only</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Detail (Line 2)</span></td>

                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtMsg2" runat="server" Height="45px"
                                            TabIndex="22" TextMode="MultiLine"
                                            Width="330px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                            MaxLength="255"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtMsg2" Display="Dynamic"
                                            ErrorMessage="Detail field cannot be left empty" ForeColor="#800000"
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator><div>
                                                Sample--> Note: Sibling Fee ID(in Student 
                                            Details) required
                                        </div>
                                    </td>
                                </tr>
                                <tr class="gridheader_pop" runat="server" id="trOpt">
                                    <td align="left" class="title-bg" colspan="4">
                                        <asp:Literal ID="ltOption" runat="server"></asp:Literal></td>
                                </tr>
                                <tr runat="server" id="trOptDetail">
                                    <td colspan="4" valign="top" align="left">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="Panel1" runat="server">
                                                        <input id="Checkbox1" name="chkAL" onclick="check(this);"
                                                            title="Select" type="checkbox" value="Check All" />Select All
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <asp:Panel ID="plChkBUnit" runat="server"  Width="100%">
                                                        <div class="checkbox-list">
                                                        <asp:CheckBoxList ID="chkSchool" runat="server"  CellPadding="2" CellSpacing="0" CssClass="field-value" 
                                                            RepeatColumns="1" RepeatDirection="Horizontal">
                                                        </asp:CheckBoxList></div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                    CssClass="button" OnClick="btnEdit_Click" Text="Edit" /><asp:Button ID="btnSave"
                                        runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" ValidationGroup="groupM1" /><asp:Button
                                            ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click"
                                            Text="Cancel" /><asp:Button ID="btnDelete" runat="server" CausesValidation="False"
                                                CssClass="button" OnClick="btnDelete_Click" Text="Delete" />
                        </td>
                    </tr>
                </table>

                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar"
                    TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calDateText1" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calDateText2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>

</asp:Content>

