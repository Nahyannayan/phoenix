Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudAuthorized_Leave_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    ltLabel.Text = "Leave Approval Permission"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    bindAcademic_Year()
                    bindAcademic_Grade()
                    populateGrade_Edit()
                    Call getStart_EndDate()
                    bindAcademic_STAFF()


                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call Authorized_Staff(ViewState("viewid"))

                        gvInfo.Visible = False
                        plDelete.Visible = False
                        plEdit.Visible = True
                        disablecontrol()
                    ElseIf ViewState("datamode") = "add" Then
                        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtTo.Text = ViewState("ACD_ENDDT")
                        gvInfo.Visible = True
                        plEdit.Visible = False
                        plDelete.Visible = False
                        enablecontrol()
                    End If

                    

                    btnSave.Attributes.Add("onclick", "return Gridvalidate()")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub
    Sub bindAcademic_STAFF()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim str_Sql As String = " SELECT  ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_ID " & _
           " FROM EMPLOYEE_M  where EMP_STATUS in(1,2) and EMP_bACTIVE=1 and EMP_BSU_ID='" & BSU_ID & "' and EMP_DES_ID in(select ATC_DES_ID from AUTHORIZED_CATEGORY where ATC_BSU_ID='" & BSU_ID & "')  order by EMP_NAME"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAuthStaff.Items.Clear()
            ddlAuthStaff.DataSource = ds.Tables(0)
            ddlAuthStaff.DataTextField = "EMP_NAME"
            ddlAuthStaff.DataValueField = "EMP_ID"
            ddlAuthStaff.DataBind()
          
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindAcademic_STAFF")
        End Try

    End Sub
    Sub getStart_EndDate()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
            ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
        Else
            ViewState("ACD_STARTDT") = ""
            ViewState("ACD_ENDDT") = ""

        End If
    End Sub
    Protected Sub gvInfo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cs As ClientScriptManager = Page.ClientScript

        For Each grdrow As GridViewRow In gvInfo.Rows


            Dim chkgrdValidate As CheckBox = DirectCast(grdrow.FindControl("chkG_bAVAILABLE"), CheckBox)

            Dim txtgrdNDays As TextBox = DirectCast(grdrow.FindControl("txtNDays"), TextBox)

            cs.RegisterArrayDeclaration("grd_Cb", [String].Concat("'", chkgrdValidate.ClientID, "'"))

            cs.RegisterArrayDeclaration("grdNDays_Txt", [String].Concat("'", txtgrdNDays.ClientID, "'"))

        Next
    End Sub
    Sub populateGrade_Edit()

        Using Connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

            Dim sqlstring As String = " SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRD_DESCR, GRADE_M.GRD_DISPLAYORDER, GRADE_BSU_M.GRM_ACD_ID, " & _
              " GRADE_BSU_M.GRM_BSU_ID FROM GRADE_BSU_M INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
              " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') order by GRADE_M.GRD_DISPLAYORDER"
            gvInfo.DataSource = SqlHelper.ExecuteDataset(Connection, CommandType.Text, sqlstring)
            gvInfo.DataBind()
        End Using

    End Sub


    Sub Authorized_Staff(ByVal SAL_ID As String)

        Try
            Using readerSTAFF_AUTHORIZED As SqlDataReader = AccessStudentClass.GetSTAFF_AUTHORIZED_LEAVE(ViewState("viewid"))
                If readerSTAFF_AUTHORIZED.HasRows = True Then
                    While readerSTAFF_AUTHORIZED.Read

                        ViewState("SAL_ACD_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAL_ACD_ID"))
                        ViewState("SAL_GRD_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAL_GRD_ID"))
                        ViewState("SAL_EMP_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAL_EMP_ID"))
                        txtNo.Text = Convert.ToString(readerSTAFF_AUTHORIZED("SAL_NDAYS"))
                        'Setting date
                        If IsDate(readerSTAFF_AUTHORIZED("SAL_FROMDT")) = True Then
                            txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerSTAFF_AUTHORIZED("SAL_FROMDT"))))
                        End If
                        If IsDate(readerSTAFF_AUTHORIZED("SAL_TODT")) = True Then
                            txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerSTAFF_AUTHORIZED("SAL_TODT"))))
                        End If

                    End While
                End If
                If ViewState("datamode") = "view" Then
                    If Not ddlAuthStaff.Items.FindByValue(ViewState("SAL_EMP_ID")) Is Nothing Then
                        ddlAuthStaff.ClearSelection()

                        ddlAuthStaff.Items.FindByValue(ViewState("SAL_EMP_ID")).Selected = True
                    End If

                End If

                If ViewState("datamode") = "view" Then
                    If Not ddlAuthGrade.Items.FindByValue(ViewState("SAL_GRD_ID")) Is Nothing Then
                        ddlAuthGrade.ClearSelection()
                        ddlAuthGrade.Items.FindByValue(ViewState("SAL_GRD_ID")).Selected = True
                    End If
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Authorized_Staff")
        End Try
    End Sub
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim str_Sql As String = " SELECT     GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRD_DESCR, GRADE_M.GRD_DISPLAYORDER, GRADE_BSU_M.GRM_ACD_ID, " & _
              " GRADE_BSU_M.GRM_BSU_ID FROM GRADE_BSU_M INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
              " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') order by GRADE_M.GRD_DISPLAYORDER"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAuthGrade.Items.Clear()
            ddlAuthGrade.DataSource = ds.Tables(0)
            ddlAuthGrade.DataTextField = "GRD_DESCR"
            ddlAuthGrade.DataValueField = "GRD_ID"
            ddlAuthGrade.DataBind()
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Authorized_Staff")
        End Try

    End Sub
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If
        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Function validateDateRange() As String
        Dim CommStr As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text


        If (From_date >= Acd_SDate And From_date <= Acd_EDate) Then
            If txtTo.Text.Trim <> "" Then
                Dim To_Date As Date = txtTo.Text
                If Not ((To_Date >= Acd_SDate And To_Date <= Acd_EDate)) Then
                    CommStr = CommStr & "<div>From Date to To Date must be with in the academic year</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date  must be with in the academic year</div>"
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function

    Sub UnCheck_grid()
        For i As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(i)
            DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Checked = False

        Next
    End Sub
    Public Function callAuthorised_Emp() As DataSet

        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim BSU_ID As String = Session("sBsuid")
        Dim str_Sql As String = " SELECT  ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_ID " & _
       " FROM EMPLOYEE_M  where EMP_bACTIVE=1 and EMP_BSU_ID='" & BSU_ID & "' and EMP_DES_ID in(select ATC_DES_ID from AUTHORIZED_CATEGORY where ATC_BSU_ID='" & BSU_ID & "')  order by EMP_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Return ds

    End Function

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

            If ViewState("datamode") = "view" Then
                If Not ddlAcdYear.Items.FindByValue(ViewState("SAL_ACD_ID")) Is Nothing Then

                    ddlAcdYear.ClearSelection()
                    ddlAcdYear.Items.FindByValue(ViewState("SAL_ACD_ID")).Selected = True
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, " bindAcademic_Year")
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then
            If serverDateValidate() = "0" Then
                If validateDateRange() = "0" Then
                    str_err = calltransaction(errorMessage)
                    If str_err = "0" Then
                        lblError.Text = "Record Saved Successfully"
                    Else
                        lblError.Text = errorMessage
                    End If
                End If
            End If
        End If

    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim SAL_ID As String = "0"
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

        Dim BSU_ID As String = Session("sBsuid")
        Dim GRD_ID As String
        Dim FromDT As String = txtFrom.Text
        Dim ToDT As String = txtTo.Text
        Dim SAL_NDAYS As String
        Dim Emp_ID As String
        Dim bAttendance As Boolean = True
        Dim bLEAVE As Boolean = True

        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    For i As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim status As Integer
                        Dim row As GridViewRow = gvInfo.Rows(i)

                        Dim G_bAVAILABLE As Boolean = DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Checked
                        If G_bAVAILABLE Then
                            GRD_ID = DirectCast(row.FindControl("lblGRD_ID"), Label).Text
                            Emp_ID = DirectCast(row.FindControl("ddlEmpName"), DropDownList).SelectedItem.Value
                            SAL_NDAYS = DirectCast(row.FindControl("txtNDays"), TextBox).Text

                            bEdit = False


                            status = AccessStudentClass.SaveSTAFF_AUTHORIZED_LEAVE(SAL_ID, ACD_ID, GRD_ID, _
                        BSU_ID, Emp_ID, FromDT, ToDT, SAL_NDAYS, bEdit, transaction)


                            If status <> 0 Then
                                calltransaction = "1"
                                errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                                Return "1"
                            End If
                        End If
                    Next
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"
                    gvInfo.Enabled = False
                    plEdit.Visible = False
                    reset_state()
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage, "calltransaction")
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    GRD_ID = ddlAuthGrade.SelectedItem.Value
                    Emp_ID = ddlAuthStaff.SelectedItem.Value
                    SAL_ID = ViewState("viewid")
                    SAL_NDAYS = txtNo.Text
                    bEdit = True

                    status = AccessStudentClass.SaveSTAFF_AUTHORIZED_LEAVE(SAL_ID, ACD_ID, GRD_ID, _
                          BSU_ID, Emp_ID, FromDT, ToDT, SAL_NDAYS, bEdit, transaction)

                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"
                    disablecontrol()


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage, "calltransaction")
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function
    Sub reset_state()

        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = ""
        txtNo.Text = ""
        ddlAuthGrade.SelectedIndex = 0
        ddlAuthStaff.SelectedIndex = 0
        bindAcademic_Year()
        populateGrade_Edit()

        UnCheck_grid()
    End Sub
    Sub disablecontrol()
        ddlAcdYear.Enabled = False
        txtFrom.Enabled = False
        txtTo.Enabled = False
        imgCalendar.Enabled = False
        ImageButton1.Enabled = False
        If ViewState("datamode") = "add" Then
            gvInfo.Enabled = False
            plEdit.Visible = False
        ElseIf ViewState("datamode") = "edit" Then
            plEdit.Enabled = False
            gvInfo.Visible = False
        End If
        ddlAuthStaff.Enabled = False
        ddlAuthGrade.Enabled = False
        txtNo.Enabled = False
    End Sub
    Sub enablecontrol()
        ddlAcdYear.Enabled = True
        txtFrom.Enabled = True
        txtTo.Enabled = True
        imgCalendar.Enabled = True
        ImageButton1.Enabled = True
        If ViewState("datamode") = "add" Then
            gvInfo.Enabled = True
            plEdit.Visible = False
        ElseIf ViewState("datamode") = "edit" Then
            plEdit.Enabled = True
            gvInfo.Visible = False
        End If
        ddlAuthStaff.Enabled = True
        ddlAuthGrade.Enabled = True
        txtNo.Enabled = True
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        populateGrade_Edit()
        getStart_EndDate()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        gvInfo.Visible = True
        plEdit.Visible = False
        plDelete.Visible = False
        reset_state()
        txtTo.Text = ViewState("ACD_ENDDT")
        enablecontrol()

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        gvInfo.Visible = False
        plEdit.Visible = True
        plDelete.Visible = False
        enablecontrol()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings

                ' gvInfo.Visible = False
                ' plEdit.Visible = False
                If ViewState("datamode") = "add" Then
                    gvInfo.Visible = True
                    plEdit.Visible = False
                    plDelete.Visible = False
                ElseIf ViewState("datamode") = "edit" Then
                    plEdit.Visible = True
                    gvInfo.Visible = False
                    plDelete.Visible = False
                End If


                disablecontrol()
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("viewid") = 0 Then
            lblError.Text = "No records to delete"
            Exit Sub
        End If
        Dim str_err As String = String.Empty
        Dim DeleteMessage As String = String.Empty
        str_err = callDelete(DeleteMessage)
        If str_err = "0" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            lblError.Text = "Record Deleted Successfully"
            plDelete.Visible = True
            plEdit.Visible = False
            disablecontrol()
        Else
            lblError.Text = DeleteMessage
        End If
    End Sub
    Function callDelete(ByRef DeleteMessage As String) As Integer
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteSTAFF_AUTHORIZED_LEAVE(ViewState("viewid"), transaction)


                If Status = -1 Then
                    callDelete = "1"
                    DeleteMessage = "Record Does Not Exist "
                    Return "1"

                ElseIf Status <> 0 Then
                    callDelete = "1"
                    DeleteMessage = "Error Occured While Deleting."
                    Return "1"
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        callDelete = "1"
                        DeleteMessage = "Could not complete your request"
                        Return "1"

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("viewid") = 0

                End If
            Catch ex As Exception
                callDelete = "1"
                DeleteMessage = "Error Occured While Saving."
            Finally
                If callDelete <> "0" Then
                    UtilityObj.Errorlog(DeleteMessage)
                    transaction.Rollback()
                Else
                    DeleteMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
End Class
