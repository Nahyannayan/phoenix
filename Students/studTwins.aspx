<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"
    CodeFile="studTwins.aspx.vb" Inherits="Students_studTwins" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">

        function getSibling(mode) {

            var url;
            var id = 'SE';
            document.getElementById("<%=h_mode.ClientID%>").value = mode;
            url = '../Students/ShowSiblingInfo.aspx?id=' + id;

            var oWnd = radopen(url, "pop_usr");

        }

        function OnClientClose(oWnd, args) {
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Result.split('___');
                if (document.getElementById("<%=h_mode.ClientID%>").value == 1) {
                    document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];
                    __doPostBack('<%=txtPar_Sib.ClientID%>', 'TextChanged');
                }
                else {
                    document.getElementById("<%=txt_par_NewSibling.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_NewSiblingID.ClientID %>").value = NameandCode[1];
                    __doPostBack('<%=txt_par_NewSibling.ClientID%>', 'TextChanged');
                }
            }
        }



        function setfocs() {
            document.getElementById("<%=txtPar_Sib.ClientID %>").focus()
        }

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID%>");
               if (color == '') {
                   color = getRowColor();
               }
               if (obj.checked) {
                   rowObject.style.backgroundColor = '#f6deb2';
               }
               else {
                   rowObject.style.backgroundColor = '';
                   color = '';
               }
               // private method

               function getRowColor() {
                   if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                   else return rowObject.style.backgroundColor;
               }
           }
           // This method returns the parent row of the object
           function getParentRow(obj) {
               do {
                   obj = obj.parentElement;
               }
               while (obj.tagName != "TR")
               return obj;
           }


           // This method returns the parent row of the object
           function getParentRow(obj) {
               do {
                   obj = obj.parentElement;
               }
               while (obj.tagName != "TR")
               return obj;
           }

           function change_chk_state(chkThis) {
               var chk_state = !chkThis.checked;
               for (i = 0; i < document.forms[0].elements.length; i++) {
                   var currentid = document.forms[0].elements[i].id;
                   if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkPublish") != -1) {
                       //if (document.forms[0].elements[i].type=='checkbox' )
                       //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                       if (document.forms[0].elements[i].disabled == false) {
                           document.forms[0].elements[i].checked = chk_state;
                           document.forms[0].elements[i].click();//fire the click event of the child element
                       }
                   }
               }
           }


           function change_chk_state1(chkThis) {
               var chk_state = !chkThis.checked;
               for (i = 0; i < document.forms[0].elements.length; i++) {
                   var currentid = document.forms[0].elements[i].id;
                   if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkRelease") != -1) {
                       //if (document.forms[0].elements[i].type=='checkbox' )
                       //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                       document.forms[0].elements[i].checked = chk_state;
                       document.forms[0].elements[i].click();//fire the click event of the child element
                   }
               }
           }


           function autoSizeWithCalendar(oWindow) {
               var iframe = oWindow.get_contentFrame();
               var body = iframe.contentWindow.document.body;
               var height = body.scrollHeight;
               var width = body.scrollWidth;
               var iframeBounds = $telerik.getBounds(iframe);
               var heightDelta = height - iframeBounds.height;
               var widthDelta = width - iframeBounds.width;
               if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
               if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
               oWindow.center();
           }

           function confirm_reject() {

               if (confirm("You are about to offer cancel to this data.Do you want to proceed?") == true)
                   return true;
               else
                   return false;

           }

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Set Twins
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left"><span class="field-label">Student No</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPar_Sib" runat="server" OnTextChanged="txtPar_Sib_TextChanged" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnSibling" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getSibling('1');return false;"
                                             OnClick="imgbtnSibling_Click"></asp:ImageButton></td>
                                    <td colspan="2">
                                        <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left">&nbsp; &nbsp;
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Sibling No</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txt_par_NewSibling" runat="server" OnTextChanged="txt_par_NewSibling_TextChanged" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="16px"
                                            ImageUrl="~/Images/forum_search.gif" OnClientClick="getSibling('2');return  false;"
                                            OnClick="ImageButton1_Click"></asp:ImageButton></td>
                                    <td colspan="2">
                                        <asp:Label ID="lblSibling" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Visible="false" Text="Add Twins" /></td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="9" valign="top">
                                        <asp:GridView ID="gvStudEnquiry" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row"  Width="100%" EmptyDataText="No Records Found" HeaderStyle-Height="30">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                         Select 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server" __designer:wfdid="w7"></asp:CheckBox>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Business Unit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsuName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                            <asp:Label ID="stuId" runat="server" Text='<%# bind("Stu_Id") %>' Visible="False"></asp:Label>
                                            <asp:Label ID="lblSTDID" runat="server" Text='<%# bind("STD_ID") %>' Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>

                                                       <asp:Label ID="lblApplName" runat="server" Text="Student Name" CssClass="gridheader_text"></asp:Label>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApplName1" runat="server" CommandName="Select" Text='<%# Bind("STU_FIRSTNAME") %>' Visible="False"></asp:LinkButton>
                                                        <asp:Label ID="lnkApplName" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" Text="Grade" CssClass="gridheader_text"></asp:Label>
                                                        
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                        Class/Section
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" __designer:wfdid="w8" Text='<%# Bind("SECTION") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" __designer:wfdid="w4" Text='<%# Bind("STREAM") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="House">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHouse" runat="server" Text='<%# bind("House") %>' __designer:wfdid="w2"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                     <HeaderTemplate>Cancel</HeaderTemplate>
                     <ItemTemplate> 
                     <asp:LinkButton Text="Cancel" ID="lbReject" runat="server" OnClientClick="return confirm_reject();" CommandName="edit" CommandArgument='<%# Container.DataItemIndex %>'  ></asp:LinkButton> 
                     </ItemTemplate>
                     </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="5">
                                        <asp:Button ID="Button1" runat="server" CssClass="button" OnClientClick="return confirm_enroll();"
                                            TabIndex="4" Text="Save" ValidationGroup="groupM1" SkinID="ButtonNormal" OnClick="Button1_Click" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button"
                                TabIndex="4" Text="Cancel" ValidationGroup="groupM1" SkinID="ButtonNormal"  OnClick="btnCancel_Click" OnClientClick="setfocs()" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_SliblingID" runat="server" />
                            <asp:HiddenField ID="h_NewSiblingID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_StudentId" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_mode" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>

                </div>
            </div>
        </div>

     <telerik:radwindowmanager id="RadWindowManager1" showcontentduringload="false" visiblestatusbar="false"
        reloadonshow="true" runat="server" enableshadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_usr" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
       </telerik:radwindowmanager>

</asp:Content>
