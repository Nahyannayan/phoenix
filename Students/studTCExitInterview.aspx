<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studTCExitInterview.aspx.vb" Inherits="Students_studTCExitInterview"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }


      
    </script>
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Exit Interview
        </div>
        <div class="card-body">
            <div class="table-responsive ">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td >
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center"  style=" width: 100%;" valign="top">
                <table id="tblClm" runat="server" align="center" 
                    cellpadding="4" cellspacing="0" width="100%" >
                    
                    <tr id="trBsu" runat="server">
                        <td align="left"  id="td1" runat="server">
                            <span class="field-label">Business Unit</span>
                        </td>
                        
                        <td align="left"  id="td3" runat="server">
                            <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" Width="408px">
                            </asp:DropDownList>
                        </td>
                        <td align="left"  id="td4" runat="server">
                            <span class="field-label">Curriculum</span>
                        </td>
                        
                        <td align="left"  id="td6" runat="server" colspan="2">
                            <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Academic Year</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Exit Interview Status</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlExitStatus" runat="server" Width="80px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>COMPLETED</asp:ListItem>
                                <asp:ListItem>NOT COMPLETED</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Grade</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" Width="80px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Section</span>
                        </td>
                        
                        <td align="left"  colspan="2">
                            <asp:DropDownList ID="ddlSection" runat="server" Width="80">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Apply Date From</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtApplyStart" runat="server" Width="100px">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgApplyStart" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                        </td>
                        <td align="left" >
                            <span class="field-label">Apply Date To</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtApplyEnd" runat="server" Width="100px">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgApplyEnd" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                        </td>
                        <td colspan="7" >
                            <asp:RadioButton ID="rdOnline" Text="TC Online" Checked="true" runat="server" GroupName="g1" />
                            <asp:RadioButton ID="rdSchool" Text="TC In School" runat="server" GroupName="g1" />
                            <asp:RadioButton ID="rdCancel" Text="Cancelled" runat="server" GroupName="g1" />
                            <asp:RadioButton ID="rdAll" Text="All" runat="server" GroupName="g1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Student ID</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtStudentID" runat="server" Width="150px">
                            </asp:TextBox>
                        </td>
                        <td align="left" >
                            <span class="field-label">Student Name</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtName" runat="server" Width="150px">
                            </asp:TextBox>
                        </td>
                        <td align="left"  colspan="7">
                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="13" align="left">
                            <table border="1" style="border-collapse: collapse;
                                " cellpadding="2" cellspacing="0" width="100%">
                                <tr>
                                    <td rowspan="3" >
                                        <span class="field-label">Status</span>
                                    </td>
                                    <td rowspan="2"  align="center">
                                        <span class="field-label">Online TC</span>
                                    </td>
                                    <td rowspan="2"  align="center">
                                        <span class="field-label">In School</span>
                                    </td>
                                    <td colspan="2"  align="center">
                                        <span class="field-label">Online Interview</span>
                                    </td>
                                    <td colspan="2"  align="center">
                                        <span class="field-label">Principal Interview</span>
                                    </td>
                                    <td colspan="2"  align="center">
                                        <span class="field-label">Corporate Inteview</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td  align="center">
                                        <span class="field-label">Completed</span>
                                    </td>
                                    <td  align="center">
                                        <span class="field-label">Pending</span>
                                    </td>
                                    <td  align="center">
                                        <span class="field-label">Completed</span>
                                    </td>
                                    <td  align="center">
                                        <span class="field-label">Pending</span>
                                    </td>
                                    <td  align="center">
                                        <span class="field-label">Completed</span>
                                    </td>
                                    <td  align="center">
                                        <span class="field-label">Pending</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td  align="center">
                                        <asp:Label ID="lblOnline" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblSchool" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblOnline_C" ForeColor="#008F00" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblOnline_P" ForeColor="#E60000" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblPrincipal_C" ForeColor="#008F00" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblPincipal_P" ForeColor="#E60000" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblCorporate_C" ForeColor="#008F00" runat="server"></asp:Label>
                                    </td>
                                    <td  align="center">
                                        <asp:Label ID="lblCorporate_P" ForeColor="#E60000" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trGrid" runat="server">
                        <td align="center"  colspan="13" valign="top">
                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" Height="25px" />
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTcmId" runat="server" Text='<%# Bind("TCM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("TCM_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOnlineFeedback" runat="server" Text='<%# Bind("TCM_EXITFEEDBACK_PARENT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSchoolFeedback" runat="server" Text='<%# Bind("TCM_EXITFEEDBACK_SCHOOL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCorpFeedback" runat="server" Text='<%# Bind("TCM_EXITFEEDBACK_CORP") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Apply Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplyDate" runat="server" Text='<%# Bind("TCM_APPLYDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" Style="cursor: pointer" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exit Interview">
                                        <HeaderTemplate>
                                            
                                                        Exit Interview<br />
                                                    
                                                        <asp:DropDownList ID="ddlgvExit" runat="server" CssClass="listbox" AutoPostBack="True"
                                                            Width="50px" OnSelectedIndexChanged="ddlgvExit_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>Online</asp:ListItem>
                                                            <asp:ListItem>Principal</asp:ListItem>
                                                            <asp:ListItem>Corporate</asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblExit" runat="server" Text='<%# Bind("TCM_EXITTYPE") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderTemplate>
                                            
                                                        Status<br />
                                                    
                                                        <asp:DropDownList ID="ddlgvStatus" runat="server" CssClass="listbox" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvStatus_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>Completed</asp:ListItem>
                                                            <asp:ListItem>Not Completed</asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="imgExit" runat="server" ImageUrl='<%# getExitImage(Eval("TCM_EXITSTATUS")) %>'>
                                            </asp:Image>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Online">
                                        <HeaderTemplate>
                                           
                                                        Online<br />
                                                    
                                                        <asp:DropDownList ID="ddlgvOnline" runat="server" CssClass="listbox" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvOnline_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>Completed</asp:ListItem>
                                                            <asp:ListItem>Not Completed</asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkOnline" OnClick="lnkOnline_Click" ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"ONLINECOLOR").ToString()) %>'
                                                runat="server" Text="View/Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Principal">
                                        <HeaderTemplate>
                                            
                                                        Principal<br />
                                                    
                                                        <asp:DropDownList ID="ddlgvPrincipal" runat="server" CssClass="listbox" AutoPostBack="True"
                                                             OnSelectedIndexChanged="ddlgvPrincipal_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>Completed</asp:ListItem>
                                                            <asp:ListItem>Not Completed</asp:ListItem>
                                                        </asp:DropDownList>
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrincipal" Width="70px" OnClick="lnkPrincipal_Click" ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"SCHOOLCOLOR").ToString()) %>'
                                                runat="server" Text='<%# getLabel("principal") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Corporate">
                                        <HeaderTemplate>
                                            
                                                        Corporate<br />
                                                    
                                                        <asp:DropDownList ID="ddlgvCorporate" runat="server" CssClass="listbox" AutoPostBack="True"
                                                             OnSelectedIndexChanged="ddlgvCorporate_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>Completed</asp:ListItem>
                                                            <asp:ListItem>Not Completed</asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCorporate" OnClick="lnkCorporate_Click" ForeColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"CORPORATECOLOR").ToString()) %>'
                                                runat="server" Text='<%# getLabel("corporate") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDownload" runat="server"  ForeColor="#000099" OnClick="lnkPrint_Click" Text="Print/Download"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlParentExit" runat="server" Style="background: #fff;height:700px; overflow:scroll; " CssClass="panel-cover">
        <asp:Label ID="lblExitOnlineMsg" runat="server" SkinID="Error" />
        <table >
            <tr>
                <td>
                    <div id="printableArea">
                        <table style="width: 100%; border-collapse: collapse;">
                            <tr class="title-bg">
                                <td colspan="4">
                                    Parent Exit Interview
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  colspan="2">
                                    <span class="field-label">Name of Student :&nbsp;&nbsp;</span>
                                    <asp:Label ID="lblExitStudent" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;
                                    <span class="field-label">Grade & Section :&nbsp;&nbsp;</span>
                                    <asp:Label ID="lblExitClass" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td  align="left">
                                    <span class="field-label">Reason for Withdrawal</span>
                                </td>
                                <td  align="left" width="100%">
                                    <table >
                                        <tr>
                                            <td>
                                                <asp:CheckBoxList ID="chkExitReason" Enabled="false" RepeatLayout="Table" RepeatColumns="2"
                                                    BorderStyle="None" CssClass="dropDownList" runat="server" >
                                                </asp:CheckBoxList>
                                            </td>
                                            <td style="vertical-align: bottom">
                                                <asp:Label ID="lblReason" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:TextBox ID="txtExitFeedback" TextMode="MultiLine" SkinID="MultiText" Width="600px"
                                        runat="server"></asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="wFeedback" runat="server" TargetControlID="txtExitFeedback"
                                        WatermarkText="Please let us have your feedback about our school" WatermarkCssClass="watermarked">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <span class="field-label">Please rate the following :-</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left" width="100%">
                                    <asp:DataList ID="dlExitQ" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                        ShowHeader="False" Width="100%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTqsId" runat="server" Text='<%# BIND("TQS_ID") %>' Visible="false"></asp:Label>
                                            <table border="0">
                                                <tr>
                                                    <td >
                                                        <asp:Label ID="lblSlno" runat="server" Text='<%# BIND("TQS_SLNO") %>'></asp:Label>
                                                        .
                                                    </td>
                                                    <td  align="left">
                                                        <asp:Label ID="lblQuestion" runat="server" Text='<%# BIND("TQS_QUESTION") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td  align="left">
                                                        <asp:RadioButton ID="rdSA" GroupName='<%# BIND("TQS_SLNO") %>' runat="server" Text="Strongly agree" />
                                                        &nbsp;&nbsp;
                                                        <asp:RadioButton ID="rdA" GroupName='<%# BIND("TQS_SLNO") %>' runat="server" Text="Agree" />&nbsp;&nbsp;
                                                        <asp:RadioButton ID="rdDA" GroupName='<%# BIND("TQS_SLNO") %>' runat="server" Text="Disagree" />&nbsp;&nbsp;
                                                        <asp:RadioButton ID="rdSDA" GroupName='<%# BIND("TQS_SLNO") %>' runat="server" Text="Strongly Disagree" />&nbsp;&nbsp;
                                                        <asp:RadioButton ID="rdNA" GroupName='<%# BIND("TQS_SLNO") %>' runat="server" Text="NA" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnExitSave" runat="server" Text="Submit" CssClass="button" Width="60px" />
                   <asp:Button ID="btnExitClose" runat="server" CausesValidation="False" CssClass="button"
                        Text="Close" Width="60px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlSchool" runat="server" Style="background: #fff" CssClass="panel-cover" >
        <asp:Label ID="lblExitPrincipalMsg" runat="server" SkinID="Error" />
        <table  style="width: 65%; border-collapse: collapse;">
            <tr class="title-bg">
                <td colspan="4">
                    Principal Exit Interview
                </td>
            </tr>
            <tr>
                <td align="left"  colspan="2">
                   <span class="field-label"> Name of Student :&nbsp;&nbsp;</span>
                    <asp:Label ID="lblExitStudent_School" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;
                    <span class="field-label">Grade & Section :&nbsp;&nbsp;</span>
                    <asp:Label ID="lblExitGrade_School" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:TextBox ID="txtExitPricipal" TextMode="MultiLine" SkinID="MultiText_Large" Width="600px"
                        runat="server"></asp:TextBox>
                    <ajaxToolkit:TextBoxWatermarkExtender ID="wExitPrincipal" runat="server" TargetControlID="txtExitPricipal"
                        WatermarkText="Please enter your feedback." WatermarkCssClass="watermarked">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnExitSave_Principal" runat="server" Text="Submit" CssClass="button"
                        Width="60px" />
                    <asp:Button ID="btnExitClose_Principal" runat="server" CausesValidation="False" CssClass="button"
                        Text="Close" Width="60px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlCorp" runat="server" Style="background: #fff" CssClass="panel-cover">
        <asp:Label ID="lblExitCorpMsg" runat="server" SkinID="Error" />
        <table  style="width: 65%; border-collapse: collapse;">
            <tr class="title-bg">
                <td colspan="4">
                    Corporate Exit Interview
                </td>
            </tr>
            <tr>
                <td align="left"  colspan="2">
                    <span class="field-label">Name of Student :&nbsp;&nbsp;</span>
                    <asp:Label ID="lblExitStudent_Corp" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;
                   <span class="field-label"> Grade & Section :&nbsp;&nbsp;</span>
                    <asp:Label ID="lblExitGrade_Corp" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:TextBox ID="txtExitCorp" TextMode="MultiLine" SkinID="MultiText_Large" Width="600px"
                        runat="server"></asp:TextBox>
                    <ajaxToolkit:TextBoxWatermarkExtender ID="wExitCorp" runat="server" TargetControlID="txtExitPricipal"
                        WatermarkText="Please enter your feedback." WatermarkCssClass="watermarked">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnExitSave_Corp" runat="server" Text="Submit" CssClass="button"
                        Width="60px" />
                    <asp:Button ID="btnExitClose_Corp" runat="server" CausesValidation="False" CssClass="button"
                        Text="Close" Width="60px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
                </div>
            </div>
         </div>
    <asp:HiddenField ID="hfACD_ID" runat="server" />
    <asp:HiddenField ID="hfSTU_ID" runat="server" />
    <asp:HiddenField ID="hfTCM_ID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgApplyStart" TargetControlID="txtApplyStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtApplyStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgApplyEnd" TargetControlID="txtApplyEnd">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtApplyEnd">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:ModalPopupExtender ID="mdlOnline" runat="server" TargetControlID="hfACD_ID"
        PopupControlID="pnlParentExit" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajaxToolkit:ModalPopupExtender>
    <ajaxToolkit:ModalPopupExtender ID="mdSchool" runat="server" TargetControlID="hfSTU_ID"
        PopupControlID="pnlSchool" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajaxToolkit:ModalPopupExtender>
    <ajaxToolkit:ModalPopupExtender ID="mdCorp" runat="server" TargetControlID="hfTCM_ID"
        PopupControlID="pnlCorp" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajaxToolkit:ModalPopupExtender>
    &nbsp;
     <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
</asp:Content>
