Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class Students_studLeave_Approval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If IsComboBoxVisible() Then
                        tdDDLRemarks.Visible = True
                        tdtxtRemarks.Visible = False
                    Else
                        tdDDLRemarks.Visible = False
                        tdtxtRemarks.Visible = True
                    End If



                    Call bindAcademic_Year()
                    Call GetEmpID()
                    rbPending.Checked = True
                    Call bindAcademic_Grade()
                    Call bindAcademic_Section()
                    ''Call bindAcademic_SHIFT()
                    ''Call bindAcademic_STREAM()
                    Call populateGrade_Edit()

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    Function IsComboBoxVisible() As Boolean
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT CRM_ID FROM COM_REMARK_M WITH(NOLOCK) WHERE CRM_BSU_ID = '" & Session("sBsuid") & "' ORDER BY CRM_REMARK ASC;"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindCommonRemarks")
        End Try
        Return False
    End Function
    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmpID")
        End Try
    End Sub
    Sub GetNDAYS_AUTH()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String

            If ddlAcdYear.SelectedIndex <> -1 Then
                ACD_ID = ddlAcdYear.SelectedItem.Value
            Else
                ACD_ID = ""
            End If

            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If
            str_Sql = "Select SAL_NDAYS from STAFF_AUTHORIZED_LEAVE where SAL_ACD_ID='" & ACD_ID & "' and SAL_GRD_ID='" & GRD_ID & "' and SAL_EMP_ID='" & ViewState("EMP_ID") & "'"
            ViewState("NDAYS") = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNDAYS_AUTH")
        End Try
    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " &
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " &
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindAcademic_Year")
        End Try
    End Sub
    Sub bindCommonRemarks(ByVal COM_FLAG As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT CRM_ID, CRM_REMARK FROM COM_REMARK_M WITH(NOLOCK) WHERE CRM_BSU_ID = '" & Session("sBsuid") & "'  AND CRM_FLAG = '" & COM_FLAG & "' ORDER BY CRM_REMARK ASC;"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlRemarks.Items.Clear()
            ddlRemarks.DataSource = ds.Tables(0)
            ddlRemarks.DataTextField = "CRM_REMARK"
            ddlRemarks.DataValueField = "CRM_ID"
            ddlRemarks.DataBind()
            ddlRemarks.ClearSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindCommonRemarks")
        End Try
    End Sub
    Sub bindAcademic_Grade()
        Try
            '  Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            '            Dim str_Sql As String
            '            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            '            Dim ds As New DataSet

            '            str_Sql = " SELECT DISTINCT  GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER " & _
            '" FROM  GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN " & _
            ' " STAFF_AUTHORIZED_LEAVE ON GRADE_BSU_M.GRM_ACD_ID = STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID AND " & _
            '" GRADE_BSU_M.GRM_GRD_ID = STAFF_AUTHORIZED_LEAVE.SAL_GRD_ID where STAFF_AUTHORIZED_LEAVE.SAL_EMP_ID ='" & ViewState("EMP_ID") & "' " & _
            '" and STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            '            ddlGrade.Items.Clear()
            '            If ds.Tables(0).Rows.Count > 0 Then
            '                ddlGrade.DataSource = ds.Tables(0)
            '                ddlGrade.DataTextField = "GRM_DISPLAY"
            '                ddlGrade.DataValueField = "GRD_ID"
            '                ddlGrade.DataBind()
            '            End If

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim PARAM(6) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedItem.Value)
            PARAM(1) = New SqlParameter("@EMP_ID", ViewState("EMP_ID"))
            PARAM(3) = New SqlParameter("@INFO_TYPE", "GRADE")

            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.BIND_LEAVE_APPR_MASTER", PARAM)
                ddlGrade.Items.Clear()
                ddlGrade.DataSource = DATAREADER
                ddlGrade.DataTextField = "GRM_DISPLAY"
                ddlGrade.DataValueField = "GRD_ID"
                ddlGrade.DataBind()

            End Using




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindAcademic_Grade")
        End Try
    End Sub
    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = "0"
            End If

            '  Dim str_Sql As String = " SELECT DISTINCT GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER, SECTION_M.SCT_ID as SCT_ID, " & _
            '  " SECTION_M.SCT_DESCR as SCT_DESCR FROM  GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN " & _
            '  " STAFF_AUTHORIZED_LEAVE ON GRADE_BSU_M.GRM_ACD_ID = STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID AND GRADE_BSU_M.GRM_GRD_ID = STAFF_AUTHORIZED_LEAVE.SAL_GRD_ID " & _
            ' " INNER Join SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID " & _
            ' " where SECTION_M.SCT_DESCR<>'TEMP' AND STAFF_AUTHORIZED_LEAVE.SAL_EMP_ID ='" & ViewState("EMP_ID") & "' and STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID='" & ACD_ID & "' and " & _
            '" GRADE_BSU_M.GRM_GRD_ID= '" & GRD_ID & "' order by GRADE_M.GRD_DISPLAYORDER,SECTION_M.SCT_DESCR "


            Dim PARAM(6) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedItem.Value)
            PARAM(1) = New SqlParameter("@GRD_ID", GRD_ID)
            PARAM(2) = New SqlParameter("@EMP_ID", ViewState("EMP_ID"))
            PARAM(3) = New SqlParameter("@INFO_TYPE", "SECTION")







            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.BIND_LEAVE_APPR_MASTER", PARAM)
                ddlSection.Items.Clear()
                ddlSection.DataSource = DATAREADER
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End Using

            Using DATAREADER2 As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.BIND_LEAVE_APPR_MASTER", PARAM)
                Dim isTutor As String = ""
                While DATAREADER2.Read
                    isTutor = DATAREADER2("classtutor").ToString

                End While

                If isTutor = "1" Then
                    'ddlSection.Items.Add(New ListItem("ALL", "0"))
                Else
                    ddlSection.Items.Insert(0, New ListItem("ALL", "0"))
                End If

            End Using

            'ddlSection.Items.Clear()
            'If ds.Tables(0).Rows.Count > 0 Then
            '    ddlSection.DataSource = ds.Tables(0)
            '    ddlSection.DataTextField = "SCT_DESCR"
            '    ddlSection.DataValueField = "SCT_ID"
            '    ddlSection.DataBind()
            'End If
            ''Get the total count for authorized
            Call GetNDAYS_AUTH()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindAcademic_Section")
        End Try

    End Sub

    '    Sub bindAcademic_SHIFT()
    '        Try
    '            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '            Dim str_Sql As String
    '            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '            Dim ds As New DataSet
    '            str_Sql = " SELECT DISTINCT SHIFTS_M.SHF_DESCR as SHF_DESCR, SHIFTS_M.SHF_ID as SHF_ID FROM STAFF_AUTHORIZED_LEAVE INNER JOIN GRADE_BSU_M ON " & _
    '            " STAFF_AUTHORIZED_LEAVE.SAL_GRD_ID = GRADE_BSU_M.GRM_GRD_ID And STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
    ' " INNER JOIN  SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID where  STAFF_AUTHORIZED_LEAVE.SAL_EMP_ID='" & ViewState("EMP_ID") & "' and STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID='" & ACD_ID & "' order by SHIFTS_M.SHF_DESCR"

    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '            ddlShift.Items.Clear()
    '            ddlShift.DataSource = ds.Tables(0)
    '            ddlShift.DataTextField = "SHF_DESCR"
    '            ddlShift.DataValueField = "SHF_ID"
    '            ddlShift.DataBind()

    '            If ViewState("datamode") = "view" Then
    '                ddlShift.ClearSelection()
    '                ddlShift.Items.FindByValue(ViewState("SAD_SHF_ID")).Selected = True
    '            End If

    '        Catch ex As Exception
    '            UtilityObj.Errorlog(ex.Message, "bindAcademic_SHIFT")
    '        End Try
    '    End Sub

    '    Sub bindAcademic_STREAM()
    '        Try
    '            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '            Dim str_Sql As String
    '            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '            Dim ds As New DataSet
    '            str_Sql = " SELECT  distinct  STREAM_M.STM_ID as STM_ID , STREAM_M.STM_DESCR as STM_DESCR  FROM STREAM_M INNER JOIN " & _
    '" GRADE_BSU_M ON STREAM_M.STM_ID = GRADE_BSU_M.GRM_STM_ID INNER JOIN  STAFF_AUTHORIZED_LEAVE ON GRADE_BSU_M.GRM_GRD_ID = STAFF_AUTHORIZED_LEAVE.SAL_GRD_ID " & _
    '" AND GRADE_BSU_M.GRM_ACD_ID = STAFF_AUTHORIZED_LEAVE.SAL_ACD_ID where STAFF_AUTHORIZED_LEAVE.SAL_EMP_ID='" & ViewState("EMP_ID") & "' and GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by STREAM_M.STM_DESCR"

    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '            ddlStream.Items.Clear()
    '            ddlStream.DataSource = ds.Tables(0)
    '            ddlStream.DataTextField = "STM_DESCR"
    '            ddlStream.DataValueField = "STM_ID"
    '            ddlStream.DataBind()


    '        Catch ex As Exception
    '            UtilityObj.Errorlog(ex.Message, "bindAcademic_STREAM")
    '        End Try
    '    End Sub    
    Sub populateGrade_Edit()
        Dim GRD_ID As String = String.Empty
        If ddlGrade.SelectedIndex <> -1 Then
            GRD_ID = ddlGrade.SelectedItem.Value
        Else
            GRD_ID = "0"
        End If
        Dim SCT_ID As String = String.Empty
        If ddlSection.SelectedIndex <> -1 Then
            SCT_ID = ddlSection.SelectedItem.Value
        Else
            SCT_ID = "0"
        End If
        Dim App_status As String = String.Empty
        If rbApproved.Checked = True Then
            App_status = "APPROVED"

        ElseIf rbNotApproved.Checked = True Then
            App_status = "NOT APPROVED"
        ElseIf rbPending.Checked = True Then
            App_status = "PENDING"
        ElseIf rbAll.Checked = True Then
            App_status = "ALL"
        End If
        Dim PARAM(6) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedItem.Value)
        PARAM(1) = New SqlParameter("@GRD_ID", GRD_ID)
        PARAM(2) = New SqlParameter("@APPR_STATUS", App_status)
        PARAM(3) = New SqlParameter("@SCT_ID", IIf(SCT_ID = "0", System.DBNull.Value, SCT_ID))
        PARAM(4) = New SqlParameter("@INFO_TYPE", "STUDENT")
        PARAM(5) = New SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        Using Connection As SqlConnection = ConnectionManger.GetOASISConnection()


            '            Dim sqlstring As String = " SELECT  STUDENT_LEAVE_APPROVAL.SLA_ID as ID, STUDENT_M.STU_NO as STUD_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME),'') + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),'') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME),'') AS SNAME , " & _
            ' " STUDENT_LEAVE_APPROVAL.SLA_FROMDT as FROMDT, STUDENT_LEAVE_APPROVAL.SLA_TODT as TODT, STUDENT_LEAVE_APPROVAL.SLA_REMARKS as REMARKS, STUDENT_LEAVE_APPROVAL.SLA_APPRLEAVE as STATUS,STUDENT_LEAVE_APPROVAL.SLA_NDAYS as days FROM  STUDENT_LEAVE_APPROVAL INNER JOIN " & _
            '" STUDENT_M ON STUDENT_LEAVE_APPROVAL.SLA_STU_ID = STUDENT_M.STU_ID where STUDENT_LEAVE_APPROVAL.SLA_ACD_ID='" & ACD_ID & "' and STUDENT_LEAVE_APPROVAL.SLA_GRD_ID='" & GRD_ID & "' and  STUDENT_M.STU_SCT_ID='" & SCT_ID & "'" & _
            '"  and " & App_status & " order by SNAME"


            gvHolidayDetail.DataSource = SqlHelper.ExecuteDataset(Connection, CommandType.StoredProcedure, "ATT.BIND_LEAVE_APPR_MASTER", PARAM)
            gvHolidayDetail.DataBind()
            DisableControls()


        End Using
        '' CREATE PROCEDURE ATT.BIND_LEAVE_APPR_MASTER  
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblSTUD_ID As New Label

            lblSTUD_ID = TryCast(sender.FindControl("lblSTuID"), Label)
            Session("DB_Stu_ID") = lblSTUD_ID.Text

            'Response.Write("<Script> window.open('StudPro_details.aspx') </Script>")
            'url = String.Format("~\Students\StudRecordEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            'Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try




    End Sub
    Sub DisableControls()
        Dim Pending_Count As Integer
        Dim Day_Count As Integer




        For j As Integer = 0 To gvHolidayDetail.Rows.Count - 1

            Dim row As GridViewRow = gvHolidayDetail.Rows(j)

            Dim lblStatus As New Label
            Dim lbDay As Integer = DirectCast(row.FindControl("lblDay"), Label).Text

            lblStatus = DirectCast(row.FindControl("lblStatus"), Label)


            ' Day_Count = DateDiff(DateInterval.Day, lblFROMDT, lblTODT) + 1
            ' Try
            'Day_Count = GETTotal_WorkingDay(ddlAcdYear.SelectedValue, ddlGrade.SelectedValue, ddlSection.SelectedValue, DirectCast(row.FindControl("lblFROMDT"), Label).Text, DirectCast(row.FindControl("lblTODT"), Label).Text, GETWeekEndDay(1), GETWeekEndDay(2))

            ' Catch ex As Exception

            'End Try

            Day_Count = lbDay
            Dim Stud_ID_status As String = lblStatus.Text
            If UCase(Stud_ID_status) = "PENDING" Then
                'lblStatus.ForeColor = Drawing.Color.OrangeRed
                'enabling the check box for the authorised user
                If Day_Count <= ViewState("NDAYS") Then
                    DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Enabled = True
                Else
                    DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Visible = False

                    'row.Style("background-color: #ffffff;border: 1px solid black;filter:alpha(opacity=60);")
                    'row.Style.Add(HtmlTextWriterStyle.BackgroundImage, "..\imgfile\chromebg.gif")
                    '  row.RenderBeginTag()
                    'row.SetRenderMethodDelegate(AddressOf RenderCustom)
                    row.Cells(0).CssClass = "gridRowDenied"
                    'row.CssClass = "gridRowDenied"

                End If
                Pending_Count = Pending_Count + 1
            ElseIf UCase(Stud_ID_status) = "APPROVED" Then
                lblStatus.ForeColor = Drawing.Color.Green
                DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Visible = False
            ElseIf UCase(Stud_ID_status) = "NOT APPROVED" Then
                lblStatus.ForeColor = Drawing.Color.Red
                DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Visible = False
            End If
        Next
        If Pending_Count > 0 Then
            btnApproved.Visible = True
            btnNotApp.Visible = True
        Else
            btnApproved.Visible = False
            btnNotApp.Visible = False
        End If
    End Sub
    Private Function GETTotal_WorkingDay(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String) As String

        Dim sqlString As String = "select count(WDATE)  from fn_TWORKINGDAYS_XLOGBOOK('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "')"

        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CStr(result)

    End Function
    Private Function GETWeekEndDay(ByVal DayOrder As Integer) As String

        Dim sqlString As String = ""
        If DayOrder = 1 Then
            sqlString = "SELECT BSU_WEEKEND1 FROM BUSINESSUNIT_M WHERE BSU_ID=" & Session("sBsuId")
        Else
            sqlString = "SELECT BSU_WEEKEND2 FROM BUSINESSUNIT_M WHERE BSU_ID=" & Session("sBsuId")
        End If

        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CStr(result)

    End Function
    Private Sub RenderCustom(ByVal writer As System.Web.UI.HtmlTextWriter,ByVal container As Control)


        writer.Write("<b>the world</b>")

    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call bindAcademic_Grade()
        Call populateGrade_Edit()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call bindAcademic_Section()
        Call populateGrade_Edit()
    End Sub
    Protected Sub rbPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call populateGrade_Edit()
    End Sub
    Protected Sub rbApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call populateGrade_Edit()
    End Sub
    Protected Sub rbNotApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call populateGrade_Edit()
    End Sub
    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call populateGrade_Edit()
    End Sub
    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call populateGrade_Edit()
    End Sub

    'Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Call populateGrade_Edit()
    'End Sub

    'Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Call populateGrade_Edit()
    'End Sub
    Private Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml
    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim SLA_ID As String = "0"
        Dim EMP_ID As String = ViewState("EMP_ID")
        Dim check_CheckStatus As Integer
        Dim html As String = ScreenScrapeHtml(Server.MapPath("~\Students\ParentTemplate_Att.html"))
        Dim APP_STATUS As String = String.Empty

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i As Integer = 0 To gvHolidayDetail.Rows.Count - 1
                    Dim status As Integer
                    Dim row As GridViewRow = gvHolidayDetail.Rows(i)

                    Dim G_bAVAILABLE As Boolean = DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Checked

                    If G_bAVAILABLE Then

                        SLA_ID = DirectCast(row.FindControl("lblID"), Label).Text

                        If ViewState("APPRLEAVE_TYPE") = "APPROVED" Then
                            APP_STATUS = "<b style='color:green;'>has been approved</b>"
                        Else

                            APP_STATUS = "<b style='color:red;'>has been rejected</b>"
                        End If
                        html = html.Replace("@STATUS", APP_STATUS)


                        status = SAVESTUDENT_LEAVE_APPROVAL_NEW(SLA_ID, ViewState("EMP_ID"), ViewState("APPRLEAVE_TYPE"), html.ToString, transaction)

                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        End If
                        check_CheckStatus = check_CheckStatus + 1
                    End If
                Next
                ViewState("viewid") = "0"
                ViewState("datamode") = "none"

                If check_CheckStatus > 0 Then
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"
                    gvHolidayDetail.Visible = True
                Else
                    calltransaction = "1"
                    errorMessage = "No record as been selected for saving"
                    Return "1"
                End If


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving!!!"
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function
    Private Function SAVESTUDENT_LEAVE_APPROVAL_NEW(ByVal SLA_ID As String, ByVal SLA_EMP_ID_APPR As String, ByVal SLA_APPRLEAVE As String, ByVal htmlmsg As String,
         ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --06/Jul/2008
        'Purpose--To save STUDENT_LEAVE_APPROVAL data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
            pParms(1) = New SqlClient.SqlParameter("@SLA_EMP_ID_APPR", SLA_EMP_ID_APPR)
            pParms(2) = New SqlClient.SqlParameter("@SLA_APPRLEAVE", SLA_APPRLEAVE)
            pParms(3) = New SqlClient.SqlParameter("@htmlmsg", htmlmsg)
            'pParms(4) = New SqlClient.SqlParameter("@APPROVER_REMARKS", txtRemarks.InnerText)
            If IsComboBoxVisible() Then
                pParms(4) = New SqlClient.SqlParameter("@APPROVER_REMARKS", ddlRemarks.SelectedItem.Text.ToString())
            Else
                pParms(4) = New SqlClient.SqlParameter("@APPROVER_REMARKS", txtRemarks.InnerText)
            End If

            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVESTUDENT_LEAVE_APPROVAL", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag

        End Using
    End Function
    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click

        'Dim str_err As String = String.Empty
        'Dim errorMessage As String = String.Empty
        'ViewState("APPRLEAVE_TYPE") = "APPROVED"
        ' '' str_err = calltransaction(errorMessage)
        'If str_err = "0" Then
        '    lblError.Text = "Record Saved Successfully"
        '    Call populateGrade_Edit()
        'Else
        '    lblError.Text = errorMessage
        'End If
        Call bindCommonRemarks("APPROVE")
        pnl_Approve.Visible = True
        btnRejectLEave.Visible = False
        btnApproveLeave.Visible = True

    End Sub
    Protected Sub btnNotApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNotApp.Click
        'Dim str_err As String = String.Empty
        'Dim errorMessage As String = String.Empty
        'ViewState("APPRLEAVE_TYPE") = "NOT APPROVED"
        'str_err = calltransaction(errorMessage)
        'If str_err = "0" Then
        '    lblError.Text = "Record Saved Successfully"
        '    Call populateGrade_Edit()
        'Else
        '    lblError.Text = errorMessage
        'End If
        Call bindCommonRemarks("REJECT")
        pnl_Approve.Visible = True
        btnApproveLeave.Visible = False
        btnRejectLEave.Visible = True

    End Sub
    Protected Sub btnApproveLeave_Click(sender As Object, e As EventArgs)
        pnl_Approve.Visible = False
        'txtRemarks.InnerText = ""

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ViewState("APPRLEAVE_TYPE") = "APPROVED"
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            Call populateGrade_Edit()
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Protected Sub lnkClosePop_Click(sender As Object, e As EventArgs)
        pnl_Approve.Visible = False
    End Sub
    Protected Sub btnRejectLEave_Click(sender As Object, e As EventArgs)
        pnl_Approve.Visible = False
        'txtRemarks.InnerText = ""

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ViewState("APPRLEAVE_TYPE") = "NOT APPROVED"
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            Call populateGrade_Edit()
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvHolidayDetail.PageIndexChanging
        gvHolidayDetail.PageIndex = e.NewPageIndex
        Call populateGrade_Edit()
    End Sub
End Class
