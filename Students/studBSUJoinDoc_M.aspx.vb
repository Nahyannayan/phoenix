Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studBSUJoinDoc_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control buttons based on the rights
              
                lblacademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                hfSTG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stgid").Replace(" ", "+"))
                lblStage.Text = Encr_decrData.Decrypt(Request.QueryString("stage").Replace(" ", "+"))
                BindDocuments()
                ViewState("SelectedRow") = -1
                GetRows()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                BindGrades()
                If chkGrades.Items.Count = 0 Then
                    trGrade.Visible = False
                End If

                ' ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                ' GridBind()

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

#Region "Private methods"



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DCB_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DCB_DOC_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DOC_DESCR"
        dt.Columns.Add(column)
       
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DCB_TYPE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DCB_APPLY"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DCB_COPIES"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grades"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function
    Sub EnableDisableControls(ByVal value As Boolean)
        ddlDocs.Enabled = value
        ddlApply.Enabled = value
        txtCopies.Enabled = value
        rdOrg.Enabled = value
        rdDup.Enabled = value
        gvDocs.Enabled = value
    End Sub

    Sub BindDocuments()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DOC_ID,DOC_DESCR FROM DOCREQD_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDocs.DataSource = ds
        ddlDocs.DataTextField = "DOC_DESCR"
        ddlDocs.DataValueField = "DOC_ID"
        ddlDocs.DataBind()
    End Sub
    Sub BindGrades()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A " _
                                & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID WHERE GRM_ACD_ID=" + hfACD_ID.Value _
                                & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkGrades.DataSource = ds
        chkGrades.DataTextField = "GRM_DISPLAY"
        chkGrades.DataValueField = "GRM_GRD_ID"
        chkGrades.DataBind()
    End Sub
    Sub GetRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
 
        Dim str_query As String = "SELECT DCB_ID,DCB_DOC_ID, DOC_DESCR,DOC_TYPE=CASE DCB_TYPE WHEN 'DUP' THEN 'Duplicate' " _
                                   & " ELSE 'Original' END,DOC_APPLY=CASE DCB_APPLYTO WHEN 'EXP' THEN 'Expats' " _
                                   & " WHEN 'NAT' THEN 'Locals' ELSE 'All' END,DCB_COPIES,DCB_bPROCEED ,GRADES=SUBSTRING(" _
                                   & "(SELECT ',' + GRDJD.DCG_GRD_ID FROM OASIS.DBO.GRADE_JOINDOCUMENTS_M GRDJD WITH (NOLOCK) " _
                                   & " WHERE(GRDJD.DCG_ACD_ID = A.DCB_ACD_ID And GRDJD.DCG_STG_ID = A.DCB_STG_ID And GRDJD.DCG_DOC_ID = B.DOC_ID) " _
                                   & " FOR XML PATH('')),2, 200000)FROM BSU_JOINDOCUMENTS_M AS A" _
                                   & " INNER JOIN DOCREQD_M AS B ON A.DCB_DOC_ID=B.DOC_ID WHERE " _
                                   & " DCB_ACD_ID=" + hfACD_ID.Value + " AND DCB_STG_ID=" + hfSTG_ID.Value _
                                   & " ORDER BY DOC_DESCR"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        dt = SetDataTable()

        While reader.Read
            dr = dt.NewRow
            With dr
                .Item(0) = reader.GetValue(0).ToString
                .Item(1) = reader.GetValue(1).ToString
                .Item(2) = reader.GetString(2)
                .Item(3) = reader.GetString(3)
                .Item(4) = reader.GetString(4)
                .Item(5) = reader.GetValue(5).ToString
                chkProceed.Checked = reader.GetBoolean(6)
                .Item(6) = "update"
                .Item(7) = reader.GetValue(7).ToString
            End With
            dt.Rows.Add(dr)
        End While
        gvDocs.DataSource = dt
        gvDocs.DataBind()

        If dt.Rows.Count <> 0 Then
            btnEdit.Text = "Edit"
            ViewState("datamode") = "view"
            EnableDisableControls(False)
        Else
            btnEdit.Text = "Add"
            ViewState("datamode") = "add"
        End If
        Session("dtDocs") = dt
    End Sub
    Sub AddRows()
        Dim dt As New DataTable
        Dim dr As DataRow
        dt = Session("dtDocs")
        Dim keys As Object()
        ReDim keys(0)

        keys(0) = ddlDocs.SelectedValue.ToString
     
        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This document is already added"
            Exit Sub
        End If
        Dim grades As String = ""
        For Each li As ListItem In chkGrades.Items
            If (li.Selected = True) Then
                grades = IIf(grades = "", "", grades + ",") + li.Value
            End If
        Next
        dr = dt.NewRow
        With dr
            .Item(0) = "0"
            .Item(1) = ddlDocs.SelectedValue.ToString
            .Item(2) = ddlDocs.SelectedItem.Text
            .Item(3) = IIf(rdOrg.Checked = True, "Original", "Dupliacte")
            .Item(4) = ddlApply.SelectedItem.Text
            .Item(5) = txtCopies.Text
            .Item(6) = "add"
            .Item(7) = grades
        End With
        dt.Rows.Add(dr)
        Session("dtDocs") = dt
        GridBind(dt)
        chkGrades.ClearSelection()
    End Sub
    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtDocs")
            Dim index As Integer = ViewState("SelectedRow")
            Dim grades As String = ""
            For Each li As ListItem In chkGrades.Items
                If (li.Selected = True) Then
                    grades = IIf(grades = "", "", grades + ",") + li.Value
                End If
            Next
            With dt.Rows(index)
                .Item(1) = ddlDocs.SelectedValue.ToString
                .Item(2) = ddlDocs.SelectedItem.Text
                .Item(3) = IIf(rdOrg.Checked = True, "Original", "Duplicate")
                .Item(4) = ddlApply.SelectedItem.Text
                .Item(5) = txtCopies.Text
                If .Item(6) = "update" Then
                    .Item(6) = "edit"
                End If
                .Item(7) = grades
            End With

            Session("dtDocs") = dt
            GridBind(dt)
            chkGrades.ClearSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(6) <> "delete" And dt.Rows(i)(6) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvDocs.DataSource = dtTemp
        gvDocs.DataBind()
    End Sub

    Function SaveData() As Boolean

        Dim bSave As Boolean = False
        Dim dt As DataTable
        dt = Session("dtDocs")
        Dim dr As DataRow
        Dim i As Integer
        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        Dim type As String = "All"
                        If .Item(4) = "Expats" Then
                            type = "EXP"
                        ElseIf .Item(4) = "Locals" Then
                            type = "NAT"
                        End If

                        If .Item(6) <> "update" And .Item(6) <> "remove" Then
                            bSave = True
                            If .Item(6) = "edit" Or .Item(6) = "delete" Then
                                UtilityObj.InsertAuditdetails(transaction, .Item(6), "BSU_JOINDOCUMENTS_M", "DCB_ID", "DCB_DOC_ID", "DCB_ID=" + .Item(0).ToString)
                            End If
                            str_query = "exec saveBSUJOINDOCUMENTS " + .Item(0) + "," _
                                        & "'" + Session("sBsuid") + "'," _
                                        & hfACD_ID.Value.ToString + "," _
                                        & hfSTG_ID.Value.ToString + "," _
                                        & .Item(1) + "," _
                                        & "'" + type + "'," _
                                        & .Item(5) + "," _
                                        & "'" + IIf(.Item(3) = "Original", "ORG", "DUP") + "'," _
                                        & "'" + chkProceed.Checked.ToString + "'," _
                                        & "'" + .Item(6) + "'"


                            dr.Item(0) = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)


                            str_query = "exec saveGRADEJOINDOCUMENTS " _
                                                                       & "'" + Session("sBsuid") + "'," _
                                                                       & hfACD_ID.Value.ToString + "," _
                                                                       & hfSTG_ID.Value.ToString + "," _
                                                                       & .Item(1) + "," _
                                                                       & "'" + type + "'," _
                                                                       & .Item(5) + "," _
                                                                       & "'" + IIf(.Item(3) = "Original", "ORG", "DUP") + "'," _
                                                                       & "'" + chkProceed.Checked.ToString + "'," _
                                                                       & "''," _
                                                                       & "'DELETEALL'"
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                            Dim grades As String = .Item(7)
                            For Each grd_Id As String In grades.Split(",")
                                If grd_Id <> "" Then
                                    str_query = "exec saveGRADEJOINDOCUMENTS " _
                                                                       & "'" + Session("sBsuid") + "'," _
                                                                       & hfACD_ID.Value.ToString + "," _
                                                                       & hfSTG_ID.Value.ToString + "," _
                                                                       & .Item(1) + "," _
                                                                       & "'" + type + "'," _
                                                                       & .Item(5) + "," _
                                                                       & "'" + IIf(.Item(3) = "Original", "ORG", "DUP") + "'," _
                                                                       & "'" + chkProceed.Checked.ToString + "'," _
                                                                       & "'" + grd_Id + "'," _
                                                                       & "'ADD'"
                                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                                End If
                            Next


                            'Dim param(10) As SqlClient.SqlParameter
                            'param(0) = New SqlClient.SqlParameter("@DCG_BSU_ID", Session("sBsuid"))
                            'param(1) = New SqlClient.SqlParameter("@DCG_ACD_ID", hfACD_ID.Value.ToString)
                            'param(2) = New SqlClient.SqlParameter("@DCG_STG_ID", hfSTG_ID.Value.ToString)
                            'param(3) = New SqlClient.SqlParameter("@DCG_DOC_ID", .Item(1))
                            'param(4) = New SqlClient.SqlParameter("@DCG_APPLYTO", type.ToString)

                            'param(5) = New SqlClient.SqlParameter("@DCG_COPIES", .Item(5))
                            'param(6) = New SqlClient.SqlParameter("@DCG_TYPE", IIf(.Item(3) = "Original", "ORG", "DUP"))
                            'param(7) = New SqlClient.SqlParameter("@DCG_bPROCEED", chkProceed.Checked.ToString())
                            'param(8) = New SqlClient.SqlParameter("@DCG_GRD_ID", chkGrades.Items(i).Value)
                            'param(9) = New SqlClient.SqlParameter("@MODE", .Item(6))
                            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "DBO.saveGRADEJOINDOCUMENTS", param)


                            ''Updated version


                        End If
                    End With
                Next

                str_query = "UPDATE BSU_JOINDOCUMENTS_M SET DCB_bPROCEED='" + chkProceed.Checked.ToString + "'" _
                           & "  WHERE DCB_ACD_ID=" + hfACD_ID.Value + " AND DCB_STG_ID=" + hfSTG_ID.Value

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()

                If bSave = False Then
                    lblError.Text = "No records to edit"
                Else
                    lblError.Text = "Record Saved Successfully"
                End If

                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function


#End Region

    Protected Sub gvDocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDocs.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim selectedRow As GridViewRow = DirectCast(gvDocs.Rows(index), GridViewRow)
        Dim keys As Object()
        Dim dt As DataTable
        dt = Session("dtDocs")

        Dim lbldocId As Label

        lbldocId = selectedRow.FindControl("lbldocId")
       
        ReDim keys(0)

        keys(0) = lbldocId.Text
      
        index = dt.Rows.IndexOf(dt.Rows.Find(keys))

        If e.CommandName = "Edit" Then
            ddlDocs.Enabled = False
            ViewState("SelectedRow") = index
            btnAddNew.Text = "Update"

            With dt.Rows(index)

                ddlDocs.ClearSelection()
                ddlDocs.Items.FindByValue(lbldocId.Text).Selected = True

                If .Item(3) = "Original" Then
                    rdOrg.Checked = True
                    rdDup.Checked = False
                Else
                    rdDup.Checked = True
                    rdOrg.Checked = False
                End If

                ddlApply.ClearSelection()
                ddlApply.Items.FindByText(.Item(4)).Selected = True

                txtCopies.Text = .Item(5)

                chkGrades.ClearSelection()
                Dim grades As String = .Item(7)
                For Each grd_Id As String In grades.Split(",")
                    If grd_Id <> "" Then
                        chkGrades.Items.FindByValue(grd_Id.Trim()).Selected = True
                    End If
                Next
            End With

            GridBind(dt)

        ElseIf e.CommandName = "Delete" Then
            If dt.Rows(index).Item(6) = "add" Then
                dt.Rows(index).Item(6) = "remove"
            Else
                dt.Rows(index).Item(6) = "delete"
            End If
            Session("dtDocs") = dt
            GridBind(dt)
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        lblError.Text = ""
        If ViewState("SelectedRow") = -1 Then
            AddRows()
        Else
            EditRows()
            ddlDocs.Enabled = True
        End If

        ViewState("SelectedRow") = -1
        btnAddNew.Text = "Add"
    End Sub

    Protected Sub gvDocs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDocs.RowDeleting

    End Sub

    Protected Sub gvDocs_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDocs.RowEditing

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveData() = True Then
            Session("dtDocs") = SetDataTable()
            GetRows()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(False)
        End If
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        lblError.Text = ""
        EnableDisableControls(True)
        ViewState("datamode") = "edit"
          Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim url As String
        Dim MainMnu_code As String
        MainMnu_code = Request.QueryString("MainMnu_code")

        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))


        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            ViewState("datamode") = "none"
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~/Students/studBSUJoinDoc_View.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        End If
    End Sub


End Class
