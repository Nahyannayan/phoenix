﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class Students_ParentPhotoUpload_ParentPhotoUpload
    Inherits System.Web.UI.Page
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session.Timeout = 5
            HiddenPostBack.Value = 1
            Session("Data") = Nothing
            BindBsu()

            Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
                While DefReader.Read
                    Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))


                End While
            End Using


        End If

        'If HiddenPostBack.Value = 0 Then
        '    Session.Timeout = 5
        '    HiddenPostBack.Value = 1
        '    Session("Data") = Nothing
        '    BindBsu()
        'End If

    End Sub

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs)  ' Handles btnupload.Click
        lblmessage.Text = ""
        lblStatus.Text = ""
        Try
            Dim ExtractPath As String = WebConfigurationManager.AppSettings("ExtractPath").ToString()


            Dim DirectoryName As String = "PhotoPath _" & System.Guid.NewGuid().ToString()

            If Not Directory.Exists(ExtractPath & "\" & DirectoryName) Then

                Directory.CreateDirectory(ExtractPath & "\" & DirectoryName)

                Dim FileName As String = FileUploadPhoto.PostedFile.FileName
                Dim filen As String()
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)
                If FileName.Split(".")(1) = "zip" Then
                    FileUploadPhoto.SaveAs(ExtractPath & "\" & DirectoryName & "\" + FileName)

                    Dim c As New Zip.FastZip
                    c.ExtractZip(ExtractPath & "\" & DirectoryName & "\" + FileName, ExtractPath & "\" & DirectoryName & "\", String.Empty)
                    Generate(ExtractPath & "\" & DirectoryName)

                    ''Delete()
                    Directory.Delete(ExtractPath & "\" & DirectoryName, True)
                Else
                    GridUploaded.Controls.Clear()
                    lblmessage.Text = "Please upload Zip File"

                End If


            Else
                lblmessage.Text = "Please Try Again"

            End If
        Catch ex As Exception
            lblmessage.Text = "Error : " & ex.Message
        End Try


    End Sub

    Public Sub Generate(ByVal FolderPath As String)
        Session("Data") = Nothing
        CheckError.Checked = False

        Dim VirtualPath As String = WebConfigurationManager.AppSettings("VirtualPath").ToString()
        Dim Filter As String = WebConfigurationManager.AppSettings("ExtensionFilters").ToString()

        Dim formatarray As String() = Filter.Split(";")
        Dim TotalFiles As Int16 = 0
        Dim VisibleFlag As Int16 = 0
        Dim ErrorFlag As Int16 = 0
        Dim ParentN As String = String.Empty

        If ddlParent.SelectedItem.Value = "1" Then
            ParentN = "FATHER"
        ElseIf ddlParent.SelectedItem.Value = "2" Then
            ParentN = "MOTHER"
        ElseIf ddlParent.SelectedItem.Value = "3" Then
            ParentN = "GUARDIAN"

        End If
        Dim dt As New DataTable
        dt.Columns.Add("File_Name")
        dt.Columns.Add("Image_View")
        dt.Columns.Add("Student_Reference")
        dt.Columns.Add("Student_Id")
        dt.Columns.Add("Status")
        dt.Columns.Add("Student_Name")
        dt.Columns.Add("Parent_Name")
        dt.Columns.Add("Error_Flag")

        For Each FileFormat As String In formatarray

            Dim FilePath As String() = Directory.GetFiles(FolderPath, FileFormat, SearchOption.AllDirectories)


            For Each File As String In FilePath

                TotalFiles = TotalFiles + 1
                VisibleFlag = 1
                Dim filename As New FileInfo(File.ToString())

                Dim referenceno As String

                referenceno = GetData(filename.Name.Split(".")(0), ddbsu.SelectedValue)

                Dim dr As DataRow = dt.NewRow()

                If referenceno.IndexOf("_") > 0 Then
                    Dim filen As String()
                    filen = File.Split("\")
                    Dim filenameDisplay As String = filen(filen.Length - 1)

                    dr.Item("File_Name") = filenameDisplay

                    dr.Item("Student_Reference") = referenceno.Split("_")(0) & " : " & referenceno.Split("_")(2)
                    dr.Item("Student_Id") = referenceno.Split("_")(1)
                    dr.Item("Image_View") = VirtualPath & ddbsu.SelectedValue & "/" & referenceno.Split("_")(1) & "/STUPHOTO/EMIRATESID/" & ParentN & "/" & "PARENTPHOTO." & filenameDisplay.Split(".")(1)
                    dr.Item("Status") = CreateFile(filename.FullName, referenceno.Split("_")(1), filename.Name.Split(".")(1))
                    dr.Item("Student_Name") = GetStudentName(referenceno.Split("_")(1))
                    dr.Item("Parent_Name") = GetParentName(referenceno.Split("_")(1))
                    dr.Item("Error_Flag") = 0
                Else

                    ErrorFlag = ErrorFlag + 1
                    Dim filen As String()
                    filen = File.Split("\")
                    Dim filenameDisplay As String = filen(filen.Length - 1)

                    dr.Item("File_Name") = filenameDisplay
                    dr.Item("Image_View") = ""
                    dr.Item("Student_Reference") = referenceno
                    dr.Item("Student_Id") = referenceno
                    dr.Item("Status") = "File not created.Please check the user inputs."
                    dr.Item("Student_Name") = ""
                    dr.Item("Parent_Name") = ""
                    dr.Item("Error_Flag") = 1
                    CheckError.Visible = True
                End If

                dt.Rows.Add(dr)



            Next


        Next
        ''Datatable in session
        Session("Data") = dt

        GridUploaded.DataSource = dt
        GridUploaded.DataBind()
        lblStatus.Text = "Total Files : " & TotalFiles & "<br>Error in Uploading : " & ErrorFlag
    End Sub
    Public Sub BindBsu()
        Try
            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()


            Dim Sql_Query As String = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
            ddbsu.DataSource = ds.Tables(0)
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
            Dim list As New ListItem
            list.Text = "Select a Business Unit"
            list.Value = "-1"
            ddbsu.items.insert(0, list)

            ddbsu.SelectedValue = Session("sBsuid")

            ddbsu.Enabled = False

        Catch ex As Exception



        End Try


    End Sub

    Public Function GetStudentName(ByVal student_id As String) As String
        Dim returnvalue As String = ""
        Try

            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()

            Dim Sql_Query As String = "SELECT ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') STU_NAME FROM  dbo.STUDENT_M WHERE STU_ID='" & student_id & "'"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = ds.Tables(0).Rows(0).Item("STU_NAME").ToString()
            End If
        Catch ex As Exception
            returnvalue = "Error : " & ex.Message
        End Try



        Return returnvalue
    End Function

    Public Function GetParentName(ByVal student_ID As String) As String
        Dim returnvalue As String = ""
        Try
            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()
            Dim parentType As String = String.Empty

            If ddlParent.SelectedItem.Value = "1" Then
                parentType = "F"
            ElseIf ddlParent.SelectedItem.Value = "2" Then
                parentType = "M"
            ElseIf ddlParent.SelectedItem.Value = "3" Then
                parentType = "G"

            End If
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", student_ID)
            param(1) = New SqlClient.SqlParameter("@PARENT", parentType)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.StoredProcedure, "dbo.GetParentDetailsByStudentID", param)
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = ds.Tables(0).Rows(0).Item("ParentName").ToString()
            End If
        Catch ex As Exception
            returnvalue = "Error : " & ex.Message
        End Try
        Return returnvalue
    End Function


    Public Function GetData(ByVal id As String, ByVal bsu_id As String) As String

        Dim returnvalue As String = ""

        Try

            '' check for fee id for given id

            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()




            Dim Sql_Query As String = "SELECT STU_ID FROM STUDENT_M WHERE STU_FEE_ID='" & id & "' AND STU_BSU_ID='" & bsu_id & "'"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)

            '' data reference fee id
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = "Student Fee Id _" & ds.Tables(0).Rows(0).Item("STU_ID").ToString() & "_" & id
            Else
                '' data reference Student No
                Sql_Query = " SELECT STU_ID FROM STUDENT_M WHERE STU_NO='" & id & "' AND STU_BSU_ID='" & bsu_id & "'"
                ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
                If ds.Tables(0).Rows.Count > 0 Then
                    returnvalue = "Student No _" & ds.Tables(0).Rows(0).Item("STU_ID").ToString() & "_" & id
                Else
                    returnvalue = "No Data Found. Please check the BSU."
                End If
            End If
        Catch ex As Exception
            returnvalue = "Error : " & ex.Message
        End Try

        Return returnvalue

    End Function


#Region "Create"
    Public Function CreateFile(ByVal OriginalPath As String, ByVal student_id As String, ByVal extention As String) As String
        Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()


        Dim ParentNS As String = String.Empty
        Dim ParentD As String = String.Empty
        If ddlParent.SelectedItem.Value = "1" Then
            ParentNS = "FATHER"
            ParentD = "F"
        ElseIf ddlParent.SelectedItem.Value = "2" Then
            ParentNS = "MOTHER"
            ParentD = "M"
        ElseIf ddlParent.SelectedItem.Value = "3" Then
            ParentNS = "GUARDIAN"
            ParentD = "G"

        End If

        Dim status As String = ""
        Dim UploadPath As String = WebConfigurationManager.AppSettings("UploadPath").ToString()

        Try
            If Directory.Exists(UploadPath) Then
                Directory.CreateDirectory(UploadPath & "/" & ddbsu.SelectedValue & "/" & student_id)
                If Not Directory.Exists(UploadPath & "/" & ddbsu.SelectedValue & "/" & student_id & "/STUPHOTO/EMIRATESID/" & ParentNS) Then
                    Directory.CreateDirectory(UploadPath & "/" & ddbsu.SelectedValue & "/" & student_id & "/STUPHOTO/EMIRATESID/" & ParentNS)
                End If
                File.Copy(OriginalPath, UploadPath & "/" & ddbsu.SelectedValue & "/" & student_id & "/STUPHOTO/EMIRATESID/" & ParentNS & "/PARENTPHOTO." & extention, True)
               
                '' Update the database
                Dim path As String = "/" & ddbsu.SelectedValue & "/" & student_id & "/STUPHOTO/EMIRATESID/" & ParentNS & "/PARENTPHOTO." & extention

                'Dim sql_query As String = "UPDATE dbo.STUDENT_M  SET STU_PHOTOPATH='" & path & "' WHERE STU_ID='" & student_id & "'"

                'SqlHelper.ExecuteNonQuery(sql_Connection, CommandType.Text, sql_query)

                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", student_id)
                param(1) = New SqlClient.SqlParameter("@PHOTOPATH", path)
                param(2) = New SqlClient.SqlParameter("@SchoolType", Session("School_Type"))
                param(3) = New SqlClient.SqlParameter("@User", Session("sUsr_name"))
                param(4) = New SqlClient.SqlParameter("@Parent", ParentD)
                status = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_PARENTM_PHOTOPATH", param)


                ''nahayn on 13sep2017
                Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASIS_CRM_INTEGRATIONConnectionString").ConnectionString
                ''Dim imagebase64 As String = Convert.ToBase64String(System.IO.File.ReadAllBytes(UploadPath & "/" & ddbsu.SelectedValue & "/" & student_id & "/STUPHOTO/EMIRATESID/" & ParentNS & "/PARENTPHOTO." & extention))
                Dim photoPathSTU As String = "https://school.gemsoasis.com/OASISPHOTOS/OASIS_HR/ApplicantPhoto" & "/" & ddbsu.SelectedValue & "/" & student_id & "/STUPHOTO/EMIRATESID/" & ParentNS & "/PARENTPHOTO." & extention
                Dim param1(4) As SqlClient.SqlParameter
                param1(0) = New SqlClient.SqlParameter("@SF_STUDENT_ID", student_id)
                param1(1) = New SqlClient.SqlParameter("@PHOTO_BASE64", "")
                param1(2) = New SqlClient.SqlParameter("@PHOTO_PATH", photoPathSTU)

                param1(3) = New SqlClient.SqlParameter("@PHOTO_TYPE", ParentD)
                SqlHelper.ExecuteNonQuery(str_conn2, CommandType.StoredProcedure, "dbo.SAVE_PHOTO_CRM_SF", param1)
                ''nahyan ends 
                status = "Successfully Uploaded"

            Else
                status = "Destination folder does not exists"
            End If
        Catch ex As Exception

            status = ex.Message

        End Try


        Return status

    End Function

#End Region


    Protected Sub GridUploaded_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridUploaded.PageIndexChanging
        GridUploaded.PageIndex = e.NewPageIndex
        Dim dt As New DataTable
        dt = Session("Data")
        If dt.Rows.Count > 0 Then
            BindClientGrid(dt)
        End If

    End Sub


    Public Sub BindClientGrid(ByVal dt As DataTable)


        Dim tempdt As New DataTable

        tempdt = dt
        Dim i As Integer = 0

        For i = dt.Rows.Count - 1 To 0 Step -1

            Dim dr As DataRow = tempdt.Rows(i)
            If CheckError.Checked Then
                If tempdt.Rows(i).Item("Error_Flag").ToString() = "0" Then
                    'tempdt.Rows.Remove(dr)
                    'tempdt.Rows(i).Delete()
                End If
            End If

        Next

        GridUploaded.DataSource = tempdt
        GridUploaded.DataBind()


    End Sub


    Protected Sub CheckError_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckError.CheckedChanged
        Dim dt As New DataTable
        dt = Session("Data")

        If dt.Rows.Count > 0 Then
            If CheckError.Checked Then
                BindClientGrid(dt)

            Else

                GridUploaded.DataSource = dt
                GridUploaded.DataBind()

            End If
        End If


    End Sub
End Class
