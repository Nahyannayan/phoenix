﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ParentPhotoUpload.aspx.vb" Inherits="Students_ParentPhotoUpload_ParentPhotoUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Parent Image Uploading"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div align="center" class="matters">


                    <table width="100%" id="tblParentPhoto">
                        <tr>


                            <td style="width: 10%;"><span class="field-label">Business Unit </span></td>
                            <td style="width: 40%;">
                                <asp:DropDownList ID="ddbsu" runat="server"></asp:DropDownList>
                            </td>
                            <td style="width: 10%;"><span class="field-label">Parent  </span>
                            </td>
                            <td style="width: 40%;">
                                <asp:DropDownList ID="ddlParent" runat="server">
                                    <asp:ListItem Value="0" Text="Please Select"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="FATHER"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="MOTHER"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="GUARDIAN"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvPArent" ValidationGroup="vg1" runat="server" ControlToValidate="ddlParent" InitialValue="0" ErrorMessage="Please select Father/Mother/Guardian" Display="None"></asp:RequiredFieldValidator>
                            </td>

                        </tr>
                        <tr>
                            <td><span class="field-label">Upload Zip File <span style="font-size: smaller">(less than 2 mb)</span></span></td>
                            <td>

                                <asp:FileUpload ID="FileUploadPhoto" runat="server" />

                            </td>
                            <td colspan="2" align="left">
                                <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="button" OnClick="btnupload_Click"  ValidationGroup="vg1"  />

                            </td>

                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblmessage" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:CheckBox ID="CheckError" runat="server" AutoPostBack="True" CssClass="field-label"
                                    Text="Show Error List" Visible="False" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="GridUploaded" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" AllowPaging="True" Width="100%"
                                    PageSize="15">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                File Name
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("File_Name") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                STUDENT
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Student_Name") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                PARENT
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Parent_Name") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Image
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="Image1" ImageUrl='<%# Eval("Image_View") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Referenced
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Student_Reference") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Student ID
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Student_Id") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Status
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Status") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="LightGray" ForeColor="Black" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>





                </div>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select a Business Unit" ValidationGroup="vg1"
                    ControlToValidate="ddbsu" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Upload a Zip File (Student Image)" ValidationGroup="vg1"
                    ControlToValidate="FileUploadPhoto" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadPhoto"
        Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True"
        Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator>--%>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ValidationGroup="vg1"
                    ShowSummary="False" />
                <asp:HiddenField ID="HiddenPostBack" Value="0" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

