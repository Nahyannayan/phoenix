﻿Imports Lesnikowski.Barcode
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Partial Class Students_stuLateBarcode
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass
    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'print in page load
        Create_print()
    End Sub
    Sub Create_print()
        Dim dsSet As New dsImageRpt
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet, i As Integer, j As Integer, str As String
        Dim str_query As String = "SELECT LR_ID,LR_DESCR  FROM STU.STU_LATE_REASONS "



        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        i = 0
        j = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            ''Barcode
            str = ""

            If Len(ds.Tables(0).Rows(i).Item("LR_ID").ToString()) > 1 Then
                str = ds.Tables(0).Rows(i).Item("LR_ID").ToString()
            Else
                str = "0" + ds.Tables(0).Rows(i).Item("LR_ID").ToString()
            End If




            Dim barcode As BaseBarcode
            barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
            barcode.Number = str 'ds.Tables(0).Rows(i).Item("LR_ID").ToString()
            barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")

            barcode.ChecksumAdd = True
            barcode.CustomText = str
            barcode.NarrowBarWidth = 3
            barcode.Height = 300
            barcode.FontHeight = 0.15F
            barcode.ForeColor = Drawing.Color.Black
            Dim b As Byte()
            ReDim b(barcode.Render(ImageType.Png).Length)
            b = barcode.Render(ImageType.Png)


            Dim dr As dsImageRpt.dsLateReasonsRow = dsSet.dsLateReasons.NewdsLateReasonsRow()
            dr.LR_ID = ds.Tables(0).Rows(i).Item("LR_ID").ToString()
            dr.LR_DESCR = ds.Tables(0).Rows(i).Item("LR_DESCR").ToString()
            dr.LR_BARCODE = b
            dsSet.dsLateReasons.Rows.Add(dr)
        Next

        Dim repClassVal As New RepClass
        repClassVal.ResourceName = Server.MapPath("../Students/Reports/Rpt/rptLateReasonBarcode.rpt")
        repClassVal.SetDataSource(dsSet)
        CrystalReportViewer1.ReportSource = repClassVal

        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As Button = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            ' repClassVal.Dispose()

        End Try



    End Sub
    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()
    End Sub

End Class
