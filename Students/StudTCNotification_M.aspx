﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudTCNotification_M.aspx.vb" Inherits="Students_StudTCNotification_M" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

<script language="javascript" type="text/javascript">
    function getSupervisor() {
        var sFeatures;
        sFeatures = "dialogWidth: 445px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../Students/studShowEmpDetailWithEmail.aspx';

        var oWnd = radopen(url, "pop_comment");
        return false;
        <%--result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined) {
            return false;
        }

        NameandCode = result.split('|');
        document.getElementById("<%=txtSupervisor.ClientID%>").value = NameandCode[0];
        document.getElementById("<%=txtSupervisorEmail.ClientID%>").value = NameandCode[2];
        document.getElementById("<%=hfEMP_ID_SUP.ClientID%>").value = NameandCode[1];

        return false;--%>

    }
    function autoSizeWithCalendar(oWindow) {
         var iframe = oWindow.get_contentFrame();
         var body = iframe.contentWindow.document.body;

         var height = body.scrollHeight;
         var width = body.scrollWidth;

         var iframeBounds = $telerik.getBounds(iframe);
         var heightDelta = height - iframeBounds.height;
         var widthDelta = width - iframeBounds.width;

         if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
         if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
         oWindow.center();
     }
       function OnClientClose(oWnd, args) {
               var NameandCode;
            var arg = args.get_argument();
           
            if (arg) {
                NameandCode = arg.Acad.split('||');
              
                
               
                document.getElementById("<%=txtSupervisor.ClientID%>").value = NameandCode[0];
        document.getElementById("<%=txtSupervisorEmail.ClientID%>").value = NameandCode[2];
        document.getElementById("<%=hfEMP_ID_SUP.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtSupervisor.ClientID%>', 'TextChanged');
            }
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Notification Setup
        </div>
        <div class="card-body">
            <div class="table-responsive ">
    <table id="tbl_ShowScreen" runat="server" align="center" 
        cellpadding="5" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" style="width: 100%">
                <table id="Table1" runat="server" align="center"  cellpadding="5" cellspacing="0" style="width: 100%">
                    
                    <tr>
                        <td align="left" width="20%" >
                           <span class="field-label"> Select Grade</span>
                        </td>
                       
                        <td align="left"   >
                            <asp:DropDownList ID="ddlGrade" runat="server" Width="100px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                   
                    <tr>
                        <td align="left" >
                            <span class="field-label">Select Staff</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtSupervisor" runat="server" Width="220px" ></asp:TextBox>
                            <asp:ImageButton ID="imgSub2" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return getSupervisor();"  />
                        </td>
                        <td align="left" >
                            <span class="field-label">Email ID</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtSupervisorEmail" runat="server" Width="220px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" />
                        </td>
                    </tr>



                    <tr id="trGrid" runat="server" >
                        <td align="left" colspan="7"  >
                            <table id="Table4" runat="server" align="center" border="0" 
                                cellpadding="5" cellspacing="0"  width ="100%">
                                <tr>
                                    <td align="center"  >
                                        <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                           CssClass="table table-bordered table-row" EmptyDataText="No records to edit." HeaderStyle-Height="30"
                                            PageSize="20"  BorderStyle="None" Width ="100%">
                                            <Columns>
                                                
                                                <asp:TemplateField HeaderText="Sup_emp_id" Visible="False">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSupEmpId" runat="server" Text='<%# Bind("TNS_EMP_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade" >
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("TNS_GRD_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Staff Name" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSupervisor" Width="250" runat="server"  Text='<%# Bind("EMP_NAME_SUP")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Staff Email">
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblSupervisorEmail"  Width="350" runat="server"   Text='<%# Bind("TNS_EMP_EMAIL")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("UNIQUEID") %>'
                                                            CommandName="delete" Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Details will be deleted permanently.Are you sure you want to continue?"
                                                            runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                        </asp:GridView>
                                       
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                 </div>
            </div>
        </div>
     <asp:HiddenField ID="hfEMP_ID_LAB" runat="server" />
     <asp:HiddenField ID="hfEMP_ID_LIB" runat="server" />
     <asp:HiddenField ID="hfEMP_ID_LAB_ORG" runat="server" />
     <asp:HiddenField ID="hfEMP_ID_LIB_ORG" runat="server" />
    <asp:HiddenField ID="hfEMP_ID_SUP_ORG" runat="server" />
     <asp:HiddenField ID="hfEMP_ID_SUP" runat="server" />
    
</asp:Content>
