<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="UpdateGUSI.aspx.vb" MaintainScrollPositionOnPostback="true" Inherits="UpdateGUSI"
    Title="Manage GUSI" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <script language="javascript" type="text/javascript">
        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var oWnd = radopen("../curriculum/clmPopupForm.aspx?multiselect=false&ID=STUDENT", "RadWindow1");

        }
        function OnClientClose(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[1];
            __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function validateOrFields(source, args) {
            var aa = valDate()
            //      alert(aa)
            if (aa == false) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
            return;
        } 

        function valDate()
          {
            var date=document.getElementById('<%=txtDOB.ClientID %>').value 
            if(date.search('/')==2)
            {
                var len=date.split('/')  
            }       
            else if(date.search('-')==2)
            {
                var len=date.split('-')  
            } 
            else
            {
                alert('Date format is like (eg 01/jan/2010)')
                return false
            }                     
            if(len[1]>12)
            {
                alert('Date format is like (eg 01/jan/2010)')
                return false
            }  
            correctingdate(len)
            return true   
        } 
        function correctingdate(len)
            {
           
                  if(len[1]==01)
                  {
                   document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Jan/'+len[2]     
                   return
                  }  
                  else if(len[1]==02)
                  {
                   document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Feb/'+len[2]     
                   return
                  }  
                  else if(len[1]==03)
                  {
                   document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Mar/'+len[2]     
                   return
                  }
                  else if(len[1]==04)
                  {
                   document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Apr/'+len[2]     
                   return
                  }
                  else if(len[1]==05)
                  {
                   document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/May/'+len[2]     
                   return
                  }
                   else if(len[1]==06)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Jun/'+len[2]     
                  return
                 }
                 else if(len[1]==07)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Jul/'+len[2]     
                  return
                 }
                 else if(len[1]==08)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Aug/'+len[2]     
                  return
                 }
                  else if(len[1]==09)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Sep/'+len[2]     
                  return
                 }
                 else if(len[1]==10)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Oct/'+len[2]     
                  return
                 }
                 else if(len[1]==11)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Nov/'+len[2]     
                  return
                 }
                 else if(len[1]==12)
                 {
                  document.getElementById('<%=txtDOB.ClientID %>').value=len[0]+'/Dec/'+len[2]     
                  return
                 }                      
            }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Manage GUSI"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" class="text-danger font-small">Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <br />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td width="10%"></td>
                                    <td width="20%"></td>
                                    <td width="25%"></td>
                                    <td width="45%"></td>
                                </tr>
                                <tr>
                                    <td>Select Student<span class="text-danger">*</span> : </td>
                                    <td colspan="2">
                                        <asp:HiddenField ID="h_STU_IDs" runat="server" />
                                        <asp:HiddenField ID="h_stu_id_old" runat="server" />
                                        <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;"
                                            OnClick="imgStudent_Click"></asp:ImageButton>
                                        <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            PageSize="5" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <EmptyDataTemplate>
                                                <div class="text-center">No data found!</div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </td>

                                </tr>

                                <tr>
                                    <td>Search By :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSearchBy" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSearchBy_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Text="GUSI" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Student Number" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Student Details" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr runat="server" visible="true" id="tblGUSI">
                                    <td>GUSI Number <span class="text-danger">*</span> :
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtGUSI" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" visible="false" id="tblStuNo">
                                    <td>Student Number <span class="text-danger">*</span> :
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStuNo" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" runat="server" visible="false" id="tblStudentDetails">
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td width="10%">Student First Name <span class="text-danger">*</span>:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                                                </td>
                                                <td>DOB <span class="text-danger">*</span>:
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="70%">
                                                                <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                                                <ajaxToolkit:CalendarExtender ID="txtDOB_CalendarExtender" runat="server"
                                                                    Enabled="True" PopupButtonID="imgCalendar" TargetControlID="txtDOB"
                                                                    Format="dd/MMM/yyyy">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                    ErrorMessage="Not a valid Date" ClientValidationFunction="validateOrFields"
                                                                    ControlToValidate="txtDOB"></asp:CustomValidator></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>Email <span class="text-danger">*</span>:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                </td>
                                                <td>Mobile <span class="text-danger">*</span>:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button" Text="Search" OnClick="btnSearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4">
                            <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No records to edit."
                                            PageSize="20" Width="100%" BorderStyle="None">
                                            <Columns>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="rdStudent"
                                                            onclick="RadioCheck(this);" stuid='<%#Eval("STU_ID")%>'/>
                                                        <asp:HiddenField ID="hdnSTU_ID" runat="server"
                                                            Value='<%#Eval("STU_ID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="School Name">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBSU_NAME" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="GEMS USI">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_GEMS_USI" runat="server" Text='<%# Bind("STU_GEMS_USI") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU NO.">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STUDENT FIRST NAME">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_FIRSTNAME" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DOB">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_DOB" runat="server" Text='<%# Bind("STU_DOB") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MOBILE">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_MOBILE" runat="server" Text='<%# Bind("STU_MOBILE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EMAIL">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_EMAIL" runat="server" Text='<%# Bind("STU_EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <EmptyDataRowStyle />
                                            <EditRowStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" colspan="8">
                            <asp:Button ID="btnSave" runat="server" CausesValidation="False" CssClass="button"
                                Text="Save" Visible="false" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type = "text/javascript"> 
        function RadioCheck(rb) {
            $("#ctl00_cphMasterpage_gvStudent")
                .find("input[type='radio']")
                .each(function (obj) {
                    $(this).prop("checked", false);
            })

            $(rb).prop("checked", true); 
            document.getElementById("<%=h_stu_id_old.ClientID %>").value = $(rb).closest("span").attr("stuid");
        }   
    </script>
</asp:Content>
