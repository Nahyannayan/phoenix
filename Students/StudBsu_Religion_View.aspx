<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudBsu_Religion_View.aspx.vb" Inherits="Students_Stud_Bsu_Religion_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>  <asp:Literal ID="ltHeader" runat="server" Text="Religion"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" cellpadding="0" cellspacing="0" width="100%">
       
        <tr>
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>
        <tr>
            <td align="left">
                <table id="tbl_test" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvReligionRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:BoundField DataField="srno" HeaderText="Sr.No"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Religion">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("SCT_DESCR") %>' __designer:wfdid="w49"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSBR_RLG_ID" runat="server" Text='<%# Bind("RLG_DESCR") %>' __designer:wfdid="w47"></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" __designer:wfdid="w51"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View" __designer:wfdid="w52"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" __designer:wfdid="w50">View</asp:LinkButton>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SBR_ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SBR_ID") %>' __designer:wfdid="w55"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSBR_ID" runat="server" Text='<%# Bind("SBR_ID") %>' __designer:wfdid="w54"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                           
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
        </tr>
    </table>

            </div>
        </div>
    </div>


</asp:Content>

