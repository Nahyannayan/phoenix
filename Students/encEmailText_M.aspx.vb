﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_encEmailText_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S200557") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'GetActive_ACD_4_Grade


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    bind_bsu()
                    bind_accyear()
                    'ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    bind_grade()
                    bind_stream()
                    bind_subject()
                    gridbind()
                End If
            Catch ex As Exception

                ' lblm.Text = "Request could not be processed "
            End Try

        End If

    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub bind_bsu()
        ddlBsu.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
        ddlBsu.Items.FindByValue(Session("sbsuid")).Selected = True

    End Sub

    Private Sub bind_accyear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" & ddlBsu.SelectedValue & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + ddlBsu.SelectedValue + "' "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True

    End Sub
    Private Sub bind_grade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                               & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                           & "  grm_acd_id=" + ddlAcademicYear.SelectedValue + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
        If (Not ddlGrade.Items Is Nothing) AndAlso (ddlGrade.Items.Count > 0) Then
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    
    Private Sub bind_stream()
        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                  & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & " grm_acd_id=" & ddlAcademicYear.SelectedValue & " and grm_grd_id='" & ddlGrade.SelectedValue & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()

        ddlStream.Items.Add(New ListItem("ALL", "0"))
        ddlStream.Items.FindByText("ALL").Selected = True

    End Sub
    Private Sub bind_subject()
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "  SELECT ESM_ID,ESM_SUBJECT  FROM ENC.EMAIL_SUBJ_M "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "ESM_SUBJECT"
        ddlSubject.DataValueField = "ESM_ID"
        ddlSubject.DataBind()
        If (Not ddlSubject.Items Is Nothing) AndAlso (ddlSubject.Items.Count > 0) Then
            ddlSubject.Items.Add(New ListItem("ALL", "0"))
            ddlSubject.Items.FindByText("ALL").Selected = True
        End If

    End Sub
    Private Sub gridbind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        Dim str_query As String = "SELECT EMT_ID,EMT_GRD_ID,STM_DESCR,ESM_SUBJECT   FROM ENC.EMAIL_TEXTS " _
                                  & " INNER JOIN ENC.EMAIL_SUBJ_M ON ESM_ID=EMT_ESM_ID " _
                                  & " INNER JOIN stream_m ON STM_ID=EMT_STM_ID " _
                                  & " WHERE EMT_BSU_ID='" + ddlBsu.SelectedValue + "' AND EMT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString


        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND EMT_GRD_ID='" + ddlGrade.SelectedValue + "'"
        End If

        If ddlStream.SelectedValue <> "0" Then
            str_query += " AND EMT_STM_ID=" + ddlStream.SelectedValue
        End If

        If ddlSubject.SelectedValue <> "0" Then
            str_query += " AND EMT_ESM_ID=" + ddlSubject.SelectedValue
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            gvEmail.DataSource = ds.Tables(0)
            gvEmail.DataBind()

        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEmail.DataSource = ds.Tables(0)
            Try
                gvEmail.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvEmail.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvEmail.Rows(0).Cells.Clear()
            gvEmail.Rows(0).Cells.Add(New TableCell)
            gvEmail.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEmail.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEmail.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        bind_grade()
        bind_stream()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        bind_stream()
        gridbind()
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub gvEmail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmail.PageIndexChanging
        Try
            gvEmail.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Protected Sub lnkadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkadd.Click
        ViewState("bsuid") = ddlBsu.SelectedValue
        ViewState("acdid") = ddlAcademicYear.SelectedValue
        ViewState("grdid") = ddlGrade.SelectedValue
        ViewState("stmid") = ddlStream.SelectedValue
        ViewState("subid") = ddlSubject.SelectedValue
        ViewState("emtid") = 0
        Response.Redirect("~/Students/encEmailText.aspx?bsuid=" + ViewState("bsuid").ToString + "&acdid=" + ViewState("acdid").ToString + "&grdid=" + ViewState("grdid") + "&stmid=" + ViewState("stmid").ToString + "&subid=" + ViewState("subid").ToString + "&emtid=" + ViewState("emtid").ToString, False)
    End Sub

    Protected Sub gvEmail_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvEmail.RowCommand
        If e.CommandName = "view" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvEmail.Rows(index), GridViewRow)

            Dim lblEMTID As Label

            lblEMTID = selectedRow.FindControl("lblEMTID")
            ViewState("bsuid") = ddlBsu.SelectedValue
            ViewState("acdid") = ddlAcademicYear.SelectedValue
            ViewState("grdid") = ddlGrade.SelectedValue
            ViewState("stmid") = ddlStream.SelectedValue
            ViewState("subid") = ddlSubject.SelectedValue
            ViewState("emtid") = lblEMTID.Text
            Response.Redirect("~/Students/encEmailText.aspx?bsuid=" + ViewState("bsuid").ToString + "&acdid=" + ViewState("acdid").ToString + "&grdid=" + ViewState("grdid") + "&stmid=" + ViewState("stmid").ToString + "&subid=" + ViewState("subid").ToString + "&emtid=" + ViewState("emtid").ToString, False)

        End If
    End Sub

    Protected Sub gvEmail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEmail.RowDeleting
        Try
            gvEmail.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = gvEmail.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblEMTID"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM ENC.EMAIL_TEXTS WHERE EMT_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        bind_accyear()
        gridbind()
    End Sub
End Class
