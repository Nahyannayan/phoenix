Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studSetHoilday_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "S059001" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

                    Call bindAcademicYear()
                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try


        Else

            For i As Integer = 0 To gvHolidayRecord.Rows.Count - 1
                'After Postback ID's get lost. Javascript will not work without it, 
                'so we must set them back. 
                gvHolidayRecord.Rows(i).Cells(2).ID = "CellInfo" + i.ToString()
            Next

            'If it is a postback that is not from the grid, we have to expand the rows 
            'the user had expanded before. We have to check first who called this postback 
            'by checking the Event Target. The reason we check this, is because we don't need 
            'to expand if it is changing the page of the datagrid, or sorting, etc... 
            If Request("__EVENTTARGET") IsNot Nothing Then
                Dim strEventTarget As String = Request("__EVENTTARGET").ToString().ToLower()

                'gvHolidayRecord is the name of the grid. If you modify the grid name, 
                'make sure to modify this if statement. 
                If strEventTarget.IndexOf("gvHolidayRecord") = -1 Then
                    If Not Page.ClientScript.IsStartupScriptRegistered("ShowDataJS") Then

                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ShowDataJS", "<script>ShowExpandedDivInfo('" & Me.txtExpandedDivs.ClientID & "','" & Me.gvHolidayRecord.ClientID & "');</script>")

                    End If
                End If

            End If

        End If
        set_Menu_Img()
    End Sub
    Sub bindAcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id in('" & Session("sBsuid") & "') and acd_clm_id='" & Session("CLM") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))


    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvHolidayRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvHolidayRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvHolidayRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvHolidayRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvHolidayRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvHolidayRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvHolidayRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvHolidayRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub gridbind()
        Try

            Dim str_Sql As String = ""
            Dim str_filter_FromDate As String = String.Empty
            Dim str_filter_ToDate As String = String.Empty
            Dim str_filter_Remarks As String = String.Empty
            Dim str_filter_AttReq As String = String.Empty
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            str_Sql = "SELECT  SCH_ID, SCH_DTFROM,SCH_DTTO,SCH_REMARKS,SCH_TYPE FROM SCHOOL_HOLIDAY_M " & _
        "   where SCH_bDeleted ='0' and SCH_BSU_ID='" & Session("sBSUID") & "' and SCH_ACD_ID='" & ACD_ID & "'"


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_fromDate As String = String.Empty
            Dim str_ToDate As String = String.Empty
            Dim str_Remarks As String = String.Empty
            Dim str_AttReq As String = String.Empty

            If gvHolidayRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_DTFROM")
                str_fromDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_FromDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FromDate = "  AND  NOT replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FromDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FromDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FromDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FromDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_DTTO")
                str_ToDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_ToDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ToDate = "  AND  NOT replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ToDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ToDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ToDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ToDate = " AND replace(CONVERT( CHAR(12), SCH_DTFROM, 106 ),' ','/') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_REMARKS")

                str_Remarks = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Remarks = " AND isnull(SCH_REMARKS,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Remarks = "  AND  NOT isnull(SCH_REMARKS,'')  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Remarks = " AND isnull(SCH_REMARKS,'')   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Remarks = " AND isnull(SCH_REMARKS,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Remarks = " AND isnull(SCH_REMARKS,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Remarks = " AND isnull(SCH_REMARKS,'')  NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_TYPE")

                str_AttReq = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_AttReq = " AND isnull(SCH_TYPE,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_AttReq = "  AND  NOT isnull(SCH_TYPE,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_AttReq = " AND isnull(SCH_TYPE,'') LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_AttReq = " AND isnull(SCH_TYPE,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_AttReq = " AND isnull(SCH_TYPE,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_AttReq = " AND isnull(SCH_TYPE,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_FromDate & str_filter_ToDate & str_filter_Remarks & str_filter_AttReq & " ORDER BY SCH_DTFROM")


            If ds.Tables(0).Rows.Count > 0 Then

                gvHolidayRecord.DataSource = ds.Tables(0)
                gvHolidayRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not


                gvHolidayRecord.DataSource = ds.Tables(0)
                Try
                    gvHolidayRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvHolidayRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvHolidayRecord.Rows(0).Cells.Clear()
                gvHolidayRecord.Rows(0).Cells.Add(New TableCell)
                gvHolidayRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvHolidayRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvHolidayRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


            txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_DTFROM")
            txtSearch.Text = str_fromDate
            txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_DTTO")
            txtSearch.Text = str_ToDate
            txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_REMARKS")
            txtSearch.Text = str_Remarks
            txtSearch = gvHolidayRecord.HeaderRow.FindControl("txtSCH_TYPE")
            txtSearch.Text = str_AttReq



            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

   

    Protected Sub btnSearchFromDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchRemark_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchAttReqd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Function getRecordCount(ByVal SCH_ID As String) As Integer


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "SELECT count(SHG_ID)FROM [SCHOOL_HOLIDAY_GRADE] where SHG_SCH_ID='" & SCH_ID & "'"

        getRecordCount = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_query))
        
    End Function
    Protected Sub gvHolidayRecord_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then


                Dim conn As String = ConnectionManger.GetOASISConnectionString
                Dim DetailsQuery As String = String.Empty
                Dim bAllGrade As Boolean = False
                Dim sql_query = "SELECT SCH_bGRADEALL FROM SCHOOL_HOLIDAY_M WHERE SCH_ID ='" & e.Row.Cells(1).Text & "'"

                bAllGrade = CBool(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query))
                Dim ACD_ID as String=ddlAcdYear.SelectedItem.Value
                If getRecordCount(e.Row.Cells(1).Text) > 0 Then



                    DetailsQuery = "select Grade,Section from (SELECT DISTINCT GRADE_BSU_M.GRM_DISPLAY AS GRADE, SECTION_M.SCT_DESCR AS SECTION, GRADE_M.GRD_DISPLAYORDER as DispOrder " & _
                   " FROM  SCHOOL_HOLIDAY_GRADE INNER JOIN   SECTION_M ON SCHOOL_HOLIDAY_GRADE.SHG_SCT_ID = SECTION_M.SCT_ID AND " & _
                   " SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID = SECTION_M.SCT_GRD_ID AND   SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = SECTION_M.SCT_ACD_ID INNER JOIN " & _
                   " GRADE_BSU_M ON SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND   SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                    " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where  SCHOOL_HOLIDAY_GRADE.SHG_SCH_ID='" + e.Row.Cells(1).Text + "' )a order by a.DispOrder ,a.SECTION"

                    'Else
                    '    DetailsQuery = " select Grade,Section from (SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as Grade,SECTION_M.SCT_DESCR as Section, " & _
                    '       " GRADE_M.GRD_DISPLAYORDER as DispOrder  FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
                    '    " GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                    '     " where  GRADE_BSU_M.GRM_ACD_ID='80')a order by a.DispOrder,a.SECTION "

                End If





                'Here I am grabbing the additional data and putting it into mini datagrids... 
                'If you wish to just use labels, or other controls, just bind the data as you 
                'wish, and render to html as I did. 
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, DetailsQuery)

                Dim gvGradeSection As New GridView()
                gvGradeSection.AutoGenerateColumns = True
                gvGradeSection.Width = Unit.Percentage(100)

                gvGradeSection.Font.Size = 8
                'gvGradeSection.Font.Name = "Tahoma"
                gvGradeSection.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                gvGradeSection.RowStyle.HorizontalAlign = HorizontalAlign.Center
                gvGradeSection.RowStyle.VerticalAlign = VerticalAlign.Middle

                gvGradeSection.BorderColor = System.Drawing.Color.Black
                gvGradeSection.BorderStyle = BorderStyle.Solid
                'gvGradeSection.RowStyle.BackColor = System.Drawing.Color.FromArgb(165, 209, 222)
                gvGradeSection.RowStyle.CssClass = "griditem"
                gvGradeSection.HeaderStyle.CssClass = "gridheader_pop"

                gvGradeSection.HeaderStyle.Height = "20"

                If ds Is Nothing OrElse ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 Then
                    gvGradeSection.ShowFooter = True
                End If

                If bAllGrade = False Then
                    gvGradeSection.DataSource = ds
                    gvGradeSection.DataBind()

                Else
                    ds.Tables(0).Clear()
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    ''start the count from 1 no matter gridcolumn is visible or not
                    'ds.Tables(0).Rows(0)(6) = True

                    gvGradeSection.DataSource = ds.Tables(0)
                    Try
                        gvGradeSection.DataBind()
                    Catch ex As Exception
                    End Try

                    Dim columnCount As Integer = gvGradeSection.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvGradeSection.Rows(0).Cells.Clear()
                    gvGradeSection.Rows(0).Cells.Add(New TableCell)
                    gvGradeSection.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvGradeSection.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvGradeSection.Rows(0).Cells(0).Text = "WHOLE SCHOOL"
                End If

                Dim sw As New System.IO.StringWriter()
                Dim htw As New System.Web.UI.HtmlTextWriter(sw)
                gvGradeSection.RenderControl(htw)

                Dim DivStart As String = "<DIV id='uniquename" + e.Row.RowIndex.ToString() + "' style='DISPLAY: none;'>"
                Dim DivBody As String = sw.ToString()
                Dim DivEnd As String = "<td></td></DIV>"
                Dim FullDIV As String = DivStart + DivBody + DivEnd

                Dim LastCellPosition As Integer = e.Row.Cells.Count - 1
                Dim NewCellPosition As Integer = e.Row.Cells.Count - 2

                e.Row.Cells(2).ID = "CellInfo" + e.Row.RowIndex.ToString()

                'Match color of div which we will expand base on row 
                ' If e.Row.RowIndex Mod 2 = 0 Then
                'set to regular row style 
                e.Row.Cells(LastCellPosition).Text = e.Row.Cells(LastCellPosition).Text + "</td><tr><td bgcolor='#fff'></td><td colspan='5'>" + FullDIV
                ' Else
                'set to alternative row style 
                '  e.Row.Cells(LastCellPosition).Text = e.Row.Cells(LastCellPosition).Text + "</td><tr><td bgcolor='#FFFFFF></td><td colspan='" & CStr(NewCellPosition) & "'>" + FullDIV
                ' End If
                e.Row.Cells(2).Attributes("onclick") = "HideShowPanel('uniquename" & e.Row.RowIndex.ToString() & "'); ChangePlusMinusText('" & e.Row.Cells(2).ClientID & "'); SetExpandedDIVInfo('" & e.Row.Cells(2).ClientID & "','" & Me.txtExpandedDivs.ClientID & "', 'uniquename" & e.Row.RowIndex.ToString() & "');"
                e.Row.Cells(2).Attributes("onmouseover") = "this.style.cursor='pointer'"

                e.Row.Cells(2).Attributes("onmouseout") = "this.style.cursor='pointer'"
                'e.Row.Cells(8).Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvHolidayRecord_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header OrElse e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Visible = False
            e.Row.Cells(1).Visible = False
            'e.Row.Cells(8).Visible = False
        End If

    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblSCH_ID As New Label
            Dim url As String
            Dim viewid As String
            lblSCH_ID = TryCast(sender.FindControl("lblSCH_IDH"), Label)
            viewid = lblSCH_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studSetCalendar.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try




      
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studSetCalendar.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvHolidayRecord_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvHolidayRecord.PageIndexChanging
        Try
            gvHolidayRecord.PageIndex = e.NewPageIndex

            gridbind()


        Catch ex As Exception
            lblError.Text = "Request could not be processed"

        End Try
    End Sub
End Class
