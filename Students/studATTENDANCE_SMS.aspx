<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studATTENDANCE_SMS.aspx.vb" Inherits="Students_studGrade_Att" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkControl") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }




<%--        function Grade_chk_state() {
            var chk_state = document.getElementById('<%=ddlGradesAll.ClientID %>').checked;
            var tableBody = document.getElementById('<%=ddlGrdaes.ClientID %>').childNodes[0];
            for (var i = 0; i < tableBody.childNodes.length; i++) {
                var currentTd = tableBody.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = chk_state;

            }
        }--%>




    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="SMS Attendance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


            

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="Message"></asp:ValidationSummary>
                            </div>

                            <asp:Label ID="lblTotalMessage" runat="server" CssClass="error"></asp:Label><asp:Label ID="lblTotalSend"
                                runat="server" CssClass="error"></asp:Label><asp:Label ID="lblSuccess" runat="server" CssClass="error"></asp:Label><asp:Label
                                    ID="lblFailed" runat="server" CssClass="error"></asp:Label><asp:Label ID="lblStartTime" runat="server" CssClass="error"></asp:Label><asp:Label
                                        ID="lblEndTime" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="ltLabel" runat="server" Text="Select Student" class="field-label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year </span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%">
                                        <span class="field-label">Select Attendance Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" OnTextChanged="txtDate_TextChanged"
                                            Width="90px"></asp:TextBox>&nbsp;<asp:ImageButton ID="imgCalendar"
                                                runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Attendance Date required" ForeColor=""
                                            ValidationGroup="Message">*</asp:RequiredFieldValidator></td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Grades </span></td>

                                    <td align="left">
                                        <%--  <div align="left" style="overflow: auto; width: 208px; height: 92px">
                                            &nbsp;<asp:CheckBox ID="ddlGradesAll" runat="server" ForeColor="Red"
                                                Width="185px" onclick="javascript:Grade_chk_state();"
                                                Text="Select All" />
                                            <asp:CheckBoxList ID="ddlGrdaes" runat="server" Width="185px">
                                            </asp:CheckBoxList>
                                        </div>
                                        <br />--%>

                                        <telerik:RadComboBox RenderMode="Lightweight" ID="ddlGrdaes" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="false"
                                            Width="100%">
                                        </telerik:RadComboBox>

                                    </td>
                                    <td align="left">
                                        <span class="field-label">Preview of SMS</span></td>

                                    <td align="left">
                                        <asp:Label ID="ltPrev" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="TR_Attendance_Status" visible="false">
                                     <td align="left" width="20%">
                                        <span class="field-label">Attendance Status </span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddl_Attendance_Status" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_Attendance_Status_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>


                                    <td align="left">
                                        <span class="field-label">Type Message</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="error"
                                            ErrorMessage="Enter the Message to be send" ForeColor="" ValidationGroup="Message" ControlToValidate="txtMessage">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnList" runat="server" OnClick="btnList_Click" Text="List"
                                            CssClass="button" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:GridView ID="gvAlloc" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" DataKeyNames="STU_ID" AllowPaging="True" OnPageIndexChanged="gvAlloc_PageIndexChanged">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblSelectH" runat="server" Text="Select" CssClass="gridheader_text"></asp:Label>
                                            <input id="Checkbox1" name="chkAL" onclick="change_chk_state(this);" type="checkbox"
                                                value="Check All" />




                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("STU_ID") %>' />

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="stud_ID">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblSTU_NOH" runat="server" Text="Student ID"></asp:Label>
                                            <asp:TextBox ID="txtSTU_NO" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSTU_NO" OnClick="btnSearchSTU_NO_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle></HeaderStyle>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="studName">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblStudNameH" runat="server" Text="Student Name"></asp:Label>
                                            <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStudName" OnClick="btnSearchStudName_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("studName") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblGRADEH" runat="server" Text="Grade"></asp:Label>
                                            <asp:TextBox ID="txtGRM_DISPLAY" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchGRM_DISPLAY" OnClick="btnSearchGRM_DISPLAY_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGRM_DISPLAY" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' __designer:wfdid="w41"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle></HeaderStyle>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblSCT_DESCRH" runat="server" Text="Mob No."></asp:Label>
                                            <asp:TextBox ID="txtMOB_NO" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchMOB_NO" OnClick="btnSearchMOB_NO_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="lblMOB_NO" runat="server" Text='<%# Bind("MOB_NO") %>' __designer:wfdid="w37"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle></HeaderStyle>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblAttStatusH" runat="server" Text="Att. Status"></asp:Label>
                                            <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSTATUS" OnClick="btnSearchSTATUS_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="stu_id" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MobNo" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMobNo" runat="server" Text='<%# Bind("MOB_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FNAME" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFname" runat="server" Text='<%# Bind("FNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SMS_Text">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblSMSTEXTH" runat="server" Text="SMS Text"></asp:Label>
                                            <asp:TextBox ID="txtSMS_Text" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSMS_TEXT" OnClick="btnSearchSMS_TEXT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSMS_Text" runat="server" Text='<%# Bind("SMS_Text") %>' __designer:wfdid="w113"></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SMS_STATUS">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblSMSStatusH" runat="server" Text="SMS  Status"></asp:Label>
                                            <asp:TextBox ID="txtSMS_STATUS" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSMS_STATUS" OnClick="btnSearchSMS_STATUS_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSMS_STATUS" runat="server" Text='<%# Bind("SMS_STATUS") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Attempt">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAttempt" runat="server" Text='<%# Bind("Attempt") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("SCT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="chkSelAll" runat="server" AutoPostBack="True"
                                Text="Select All" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Send"
                                OnClick="btnSave_Click" ValidationGroup="Message" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" /></td>
                    </tr>
                    <tr>
                        <td>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />




                            &nbsp;
                
                <asp:HiddenField ID="hiddenStu_IDs" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="calTillDate"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>

                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

