﻿
'Partial Class Students_COVID_Relief_StudentDetail
'    Inherits System.Web.UI.Page

'End Class

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_COVID_Relief_StudentDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Page.Header.DataBind()
        If Page.IsPostBack = False Then
            Try


                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("RRH_ID") = Encr_decrData.Decrypt(Request.QueryString("RRH_ID").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050114") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                'Else



                '    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                '    Call gridbind()


                '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                'End If
                gridbind(ViewState("RRH_ID"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
    End Sub



    Public Sub gridbind(ByRef RRH_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", RRH_ID)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_REQUEST_STUDENT_DETAIL", param)
            If ds.Tables(0).Rows.Count > 0 Then

                gvApproval.DataSource = ds.Tables(0)
                gvApproval.DataBind()
                txtMainRemark.Text = ds.Tables(0).Rows(0)("RRH_REMARKS").ToString
                If ds.Tables(0).Rows(0)("STU_PRIMARYCONTACT").ToString = "F" Then
                    lblContactName.Text = ds.Tables(0).Rows(0)("F_NAME").ToString
                    lblMobile.Text = ds.Tables(0).Rows(0)("F_MOBILE").ToString
                    lblEmail.Text = ds.Tables(0).Rows(0)("F_EMAIL").ToString
                ElseIf ds.Tables(0).Rows(0)("STU_PRIMARYCONTACT").ToString = "M" Then
                    lblContactName.Text = ds.Tables(0).Rows(0)("M_NAME").ToString
                    lblMobile.Text = ds.Tables(0).Rows(0)("M_MOBILE").ToString
                    lblEmail.Text = ds.Tables(0).Rows(0)("M_EMAIL").ToString
                End If

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvApproval.DataSource = ds.Tables(0)
                Try
                    gvApproval.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvApproval.Rows(0).Cells.Count
                gvApproval.Rows(0).Cells.Clear()
                gvApproval.Rows(0).Cells.Add(New TableCell)
                gvApproval.Rows(0).Cells(0).ColumnSpan = columnCount
                gvApproval.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvApproval.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

End Class

