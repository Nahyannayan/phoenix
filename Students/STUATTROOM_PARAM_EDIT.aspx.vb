Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_STUATTROOM_PARAM_EDIT
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C888100" And ViewState("MainMnu_code") <> "S050108") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("ATT_PARAM") = CreateDataTable()
                    Session("ATT_PARAM").Rows.Clear()
                    ViewState("id") = 1

                    ATT_CAT()
                    btnAddPARAM.Visible = True
                    btnAddPARAM.Enabled = False
                    btnUpdate.Visible = False
                    btnCancelPARAM.Visible = False

                    binddata()
                    setcontrol()

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub

    Sub ATT_CAT()
        Dim dsCat As New DataSet
        dsCat = AccessStudentClass.GetATT_PARAM()
        ddlCat.Items.Clear()
        ddlCat.DataSource = dsCat.Tables(0)
        ddlCat.DataTextField = "APM_DESCR"
        ddlCat.DataValueField = "APM_ID"
        ddlCat.DataBind()

    End Sub
    Sub binddata()
        Dim BSU_ID As String = Session("sBsuid")
        Dim gvDataset As DataSet = AccessStudentClass.GetAtt_ROOMPARAM_DETAILS(BSU_ID)

        If gvDataset.Tables(0).Rows.Count > 0 Then

            Session("ATT_PARAM").Rows.Clear()
            ViewState("id") = 1

            For iIndex As Integer = 0 To gvDataset.Tables(0).Rows.Count - 1

                Dim rDt As DataRow
                rDt = Session("ATT_PARAM").NewRow
                rDt("Id") = ViewState("id")
                rDt("APM_DESCR") = gvDataset.Tables(0).Rows(iIndex)("APM_DESCR")
                rDt("RAP_PARAM_DESCR") = gvDataset.Tables(0).Rows(iIndex)("RAP_PARAM_DESCR")
                rDt("APM_ID") = gvDataset.Tables(0).Rows(iIndex)("APM_ID")
                rDt("RAP_ID") = gvDataset.Tables(0).Rows(iIndex)("RAP_ID")
                rDt("RAP_DISP") = gvDataset.Tables(0).Rows(iIndex)("RAP_DISP")
                rDt("STATUS") = gvDataset.Tables(0).Rows(iIndex)("STATUS")
                ViewState("id") = ViewState("id") + 1
                Session("ATT_PARAM").Rows.Add(rDt)
            Next

        End If
        gridbind()
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim APM_DESCR As New DataColumn("APM_DESCR", System.Type.GetType("System.String"))
            Dim RAP_PARAM_DESCR As New DataColumn("RAP_PARAM_DESCR", System.Type.GetType("System.String"))
            Dim APM_ID As New DataColumn("APM_ID", System.Type.GetType("System.String"))
            Dim RAP_ID As New DataColumn("RAP_ID", System.Type.GetType("System.String"))
            Dim RAP_DISP As New DataColumn("RAP_DISP", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(APM_DESCR)
            dtDt.Columns.Add(RAP_PARAM_DESCR)
            dtDt.Columns.Add(APM_ID)
            dtDt.Columns.Add(RAP_ID)
            dtDt.Columns.Add(RAP_DISP)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

            Return dtDt
        End Try
    End Function

    Sub gridbind()
        Try
            Dim dtTempAtt_Param As New DataTable
            dtTempAtt_Param = CreateDataTable()

            If Session("ATT_PARAM").Rows.Count > 0 Then
                For i As Integer = 0 To Session("ATT_PARAM").Rows.Count - 1
                    If Session("ATT_PARAM").Rows(i)("STATUS") & "" <> "DELETED" Then
                        Dim rDt As DataRow
                        rDt = dtTempAtt_Param.NewRow
                        For j As Integer = 0 To Session("ATT_PARAM").Columns.Count - 1
                            rDt.Item(j) = Session("ATT_PARAM").Rows(i)(j)
                        Next

                        dtTempAtt_Param.Rows.Add(rDt)
                    End If
                Next

            End If

            gvAtt_Param.DataSource = dtTempAtt_Param
            gvAtt_Param.DataBind()
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub gvAtt_Param_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gvAtt_Param.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0

        For i = 0 To Session("ATT_PARAM").Rows.Count - 1
            If Session("ATT_PARAM").rows(i)("Id") = COND_ID Then
                If Session("ATT_PARAM").rows(i)("RAP_ID") = "0" Then
                    Session("ATT_PARAM").Rows(i).Delete()
                Else
                    Session("ATT_PARAM").Rows(i)("STATUS") = "DELETED"
                End If
                Exit For
            End If
        Next

        gridbind()
    End Sub

    Protected Sub btnAddPARAM_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Trim(txtParam.Text) = "" Then
                lblError.Text = "Room Attendance Parameter cannot be left empty"
                Exit Sub
            End If
            For i As Integer = 0 To Session("ATT_PARAM").Rows.Count - 1
                If (Session("ATT_PARAM").Rows(i)("RAP_PARAM_DESCR") = UCase(Trim(txtParam.Text))) And (Session("ATT_PARAM").Rows(i)("STATUS") & "" <> "DELETED") Then
                    lblError.Text = "Duplicate parameter entry not allowed"
                    Exit Sub
                End If

            Next

            Dim rDt As DataRow

            rDt = Session("ATT_PARAM").NewRow
            rDt("Id") = ViewState("id")
            rDt("RAP_PARAM_DESCR") = UCase(Trim(txtParam.Text))
            rDt("APM_DESCR") = UCase(Trim(ddlCat.SelectedItem.Text))
            rDt("RAP_ID") = "0"
            rDt("APM_ID") = ddlCat.SelectedItem.Value
            rDt("RAP_DISP") = txtDispChar.Text
            rDt("STATUS") = "ADD"
            ViewState("id") = ViewState("id") + 1
            Session("ATT_PARAM").Rows.Add(rDt)
            gridbind()


        Catch ex As Exception
            lblError.Text = "Error in Adding new parameter"
        End Try
    End Sub
    Protected Sub gvAtt_Param_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)

        btnSave.Enabled = False
        gvAtt_Param.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvAtt_Param.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("ATT_PARAM").Rows.Count - 1
            If iIndex = Session("ATT_PARAM").Rows(iEdit)("ID") Then


                txtParam.Text = Session("ATT_PARAM").Rows(iEdit)("RAP_PARAM_DESCR")
                hfParamID.Value = Session("ATT_PARAM").Rows(iEdit)("RAP_ID")
                ddlCat.ClearSelection()
                ddlCat.Items.FindByValue(Session("ATT_PARAM").Rows(iEdit)("APM_ID")).Selected = True
                txtDispChar.Text = Session("ATT_PARAM").Rows(iEdit)("RAP_DISP")
                Exit For
            End If
        Next

        btnAddPARAM.Visible = False
        btnUpdate.Visible = True
        btnCancelPARAM.Visible = True
        btnSave.Enabled = False
        gridbind()
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim row As GridViewRow = gvAtt_Param.Rows(gvAtt_Param.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        iIndex = CInt(idRow.Text)

        If Trim(txtParam.Text) = "" Then
            lblError.Text = "Attendance Parameter cannot be left empty"
            Exit Sub
        End If
        For iEdit = 0 To Session("ATT_PARAM").Rows.Count - 1

            If iIndex <> Session("ATT_PARAM").Rows(iEdit)("ID") Then
                If (Session("ATT_PARAM").Rows(iEdit)("RAP_PARAM_DESCR") = UCase(Trim(txtParam.Text))) And (Session("ATT_PARAM").Rows(iEdit)("STATUS") & "" <> "DELETED") Then

                    lblError.Text = "Duplicate parameter entry not allowed"
                    Exit Sub
                End If

            End If

        Next
        For iEdit = 0 To Session("ATT_PARAM").Rows.Count - 1
            If iIndex = Session("ATT_PARAM").Rows(iEdit)("ID") Then

                Session("ATT_PARAM").Rows(iEdit)("RAP_PARAM_DESCR") = UCase(Trim(txtParam.Text))
                Session("ATT_PARAM").Rows(iEdit)("APM_DESCR") = UCase(Trim(ddlCat.SelectedItem.Text))
                Session("ATT_PARAM").Rows(iEdit)("APM_ID") = ddlCat.SelectedItem.Value
                Session("ATT_PARAM").Rows(iEdit)("RAP_ID") = hfParamID.Value
                Session("ATT_PARAM").Rows(iEdit)("RAP_DISP") = txtDispChar.Text
                If hfParamID.Value = "" Then
                    Session("ATT_PARAM").Rows(iEdit)("STATUS") = "ADD"
                Else
                    Session("ATT_PARAM").Rows(iEdit)("STATUS") = "UPDATE"
                End If

                Exit For
            End If
        Next

        btnAddPARAM.Visible = True
        btnAddPARAM.Enabled = True
        btnUpdate.Visible = False
        btnCancelPARAM.Visible = False
        btnSave.Enabled = True
        gvAtt_Param.SelectedIndex = -1
        gridbind()

    End Sub

    Sub setcontrol()

        btnAddPARAM.Enabled = False
        ddlCat.Enabled = False
        txtParam.Enabled = False
        txtDispChar.Enabled = False

        gvAtt_Param.Enabled = False
    End Sub

    Sub resetcontrol()
        txtDispChar.Enabled = True
        btnAddPARAM.Enabled = True
        ddlCat.Enabled = True
        txtParam.Enabled = True
        gvAtt_Param.Enabled = True
    End Sub
    Protected Sub btnCancelPARAM_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnAddPARAM.Visible = True
        btnUpdate.Visible = False
        btnCancelPARAM.Visible = False
        btnSave.Enabled = True

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        btnAddPARAM.Visible = True
        btnAddPARAM.Enabled = True
        btnUpdate.Visible = False
        btnCancelPARAM.Visible = False
        binddata()
        resetcontrol()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "add" Then
                setcontrol()
                ViewState("datamode") = "view"
                Session("ATT_PARAM").Rows.Clear()
                ViewState("id") = 1

                ddlCat.SelectedIndex = 0
                txtParam.Text = ""
                binddata()
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            setcontrol()

        Else
            lblError.Text = errorMessage
        End If
    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer



        Dim APD_BSU_ID As String = Session("sBsuid")



        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer



                If Session("ATT_PARAM").Rows.Count <> 0 Then

                    For i As Integer = 0 To Session("ATT_PARAM").Rows.Count - 1




                        'Do While True
                        '    Dim guidResult As String = System.Guid.NewGuid().ToString()
                        '    'Remove the hyphens
                        '    guidResult = guidResult.Replace("-", String.Empty)
                        '    temp_ID = guidResult.Substring(0, 8)
                        '    Dim dupUserID As Integer = AccessRoleUser.checkduplicateUserID(temp_ID)
                        '    'no duplicate id exist
                        '    If dupUserID = 1 Then
                        '        Exit Do
                        '    End If
                        'Loop


                        status = AccessStudentClass.SAVE_ROOMATT_PARAM_SET(Session("ATT_PARAM").Rows(i)("RAP_ID"), APD_BSU_ID, _
                         Session("ATT_PARAM").Rows(i)("RAP_PARAM_DESCR"), Session("ATT_PARAM").Rows(i)("APM_ID"), Session("ATT_PARAM").Rows(i)("RAP_DISP"), Session("ATT_PARAM").Rows(i)("STATUS"), transaction)



                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = "Error while inserting Attendance Parameters"
                            Return "1"
                        End If
                    Next
                End If



                ViewState("viewid") = "0"
                ViewState("datamode") = "view"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function

    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("ATT_PARAM").Rows.Clear()
        ViewState("id") = 1

        binddata()
    End Sub
End Class
