<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Studcardprint_Schools.aspx.vb" Inherits="Students_Studcardprint_Schools" Title="Untitled Page" %>

<%@ Register Src="usercontrol/StudIdcardprint_Schools.ascx" TagName="StudIdcardprint_Schools" TagPrefix="ucschools" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Print Id Card - Customized"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive" >
                <table width="100%">
                    <tr>
                        <td>
                <ucschools:StudIdcardprint_Schools ID="StudIdcardprint_Schools1" runat="server"  />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

