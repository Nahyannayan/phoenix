﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="COVID_Relief_Call_Logs.aspx.vb" Inherits="Students_COVID_Relief_Call_Logs" %>

<html>

<body>
    <form id="frm1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </ajaxToolkit:ToolkitScriptManager>
                <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/cssfiles/sb-admin.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css")%>' rel="stylesheet" />
        <script src='<%= ResolveUrl("~/vendor/jquery/jquery.min.js")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.pack.js?1=2")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.js?1=2")%>'></script>
        <link type="text/css" href='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.css?1=2")%>' rel="stylesheet" />
        <div class="title-bg">Call Log
          </div>
        <div>
            <asp:Label ID="errLbl" runat="server" CssClass="text-danger" ></asp:Label>
        </div>
        <div class="col-12 row">
            <%--<div class="col-4 col-sm-4">
              Log Date:
                  <div>
                      <asp:TextBox runat="server" CssClass="form-control" ID="txtLogDate"></asp:TextBox></div>
            </div>--%>
            <div class="col-4 col-sm-4">
                Communication Mode:
                  <div>
                      <asp:DropDownList runat="server" CssClass="form-control" ID="ddlComunicationMode"> 
                      </asp:DropDownList></div>
            </div>
            <div class="col-4 col-sm-4"> 
            </div>
        </div>
        <div class="col-12 mt-3">
           Comment <span style="color:red">*</span>:
             <div><asp:TextBox ID="txtComment" runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" Rows="4"> 
                  </asp:TextBox></div>
        </div>

        <div class="col-12">
             File:
           <div><asp:FileUpload ID="fileUpload1" runat="server" ViewStateMode="Enabled" /></div> 
        </div>
        <div class="col-12">
           <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Add Call Log" OnClick="btnSave_Click" OnClientClick="return validateSave();" />
        </div> 

        <div class="col-12 badge-light col-12 h5 mt-3">
           Call Log History
        </div>
        <div class="col-12">

             <asp:GridView ID="gvAttachment" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
            PageSize="20" Width="100%">
            <RowStyle CssClass="griditem" />
            <Columns>
                <asp:TemplateField HeaderText="RRC_ID" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblRRC_ID" runat="server" Text='<%# Bind("RRC_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="LOG DATE">
                    <ItemTemplate>
                        <asp:Label ID="lblLOG_DATE" runat="server" Text='<%# Bind("LOG_DATE")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Logged By">
                    <ItemTemplate>
                        <asp:Label ID="lblUSR_DISPLAY_NAME" runat="server" Text='<%# Bind("USR_DISPLAY_NAME")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Communication Mode">
                    <ItemTemplate>
                        <asp:Label ID="lblRRC_COM_MODE" runat="server" Text='<%# Bind("RRC_COM_MODE")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Comment">
                    <ItemTemplate>
                        <asp:Label ID="lblRRC_COMMENTS" runat="server" Text='<%# Bind("RRC_COMMENTS")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="File Name ">
                    <ItemTemplate>
                        <asp:Label ID="lblRRC_FILENAME" runat="server" Text='<%# Bind("RRC_FILENAME")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Download">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" OnClick="lnkDownload_Click"></asp:LinkButton>
                         <asp:HiddenField ID="hf_PhotoPath" runat="server" Value='<%# Bind("RRC_FILEPATH")%>' />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <SelectedRowStyle BackColor="Wheat" />
            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
            <AlternatingRowStyle CssClass="griditem_alternative" />
        </asp:GridView>

        </div>



        <script>
            function validateSave() {
                if ($("#txtComment").val() == "") {
                    alert("Please enter the comment");
                    return false;
                }
                return true;
            }
        </script>
    </form>
</body>
</html>

