<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAtt_room_Grade_View.aspx.vb" Inherits="Students_studAtt_room_Grade_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
                            <asp:Literal ID="ltHeader" runat="server" Text="Grade Setting"></asp:Literal> 
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <%--  <tr>
            <td align="left" colspan="4" style="height: 8px" valign="top">
                <asp:LinkButton id="lbAddNew" runat="server" Font-Bold="True" onclick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>--%>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr >
                                    <td align="left" class="matters" colspan="9" valign="top" style="text-align:center">
                                        <asp:GridView ID="gvOnlineEnq_Ack" runat="server" AllowPaging="FALSE" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No records available !"
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRM_DIS" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' __designer:wfdid="w29"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Periods">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPeriods" runat="server" Text='<%# Bind("period") %>' __designer:wfdid="w27"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View" __designer:wfdid="w24"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" __designer:wfdid="w22">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRAPG_GRD_ID" runat="server" Text='<%# Bind("GRM_GRD_ID") %>' __designer:wfdid="w30"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"   HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

