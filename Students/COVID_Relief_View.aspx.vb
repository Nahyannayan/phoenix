Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Linq
Imports Newtonsoft.Json
Imports System.Security.Cryptography

Partial Class COVID_Relief_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Session("Current_Page") = "View"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Session("sBsuid") = "999998" Then
                    'Session("covid_bBsuId") = "0"
                    Session("covid_bBsuId") = "0"
                Else
                    'Session("covid_bBsuId") = Session("sBsuid")
                    Session("covid_bBsuId") = Session("sBsuid")
                End If

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or Not ((ViewState("MainMnu_code") = "S900052") Or (ViewState("MainMnu_code") = "S900053")) Then
                    'If 1 = 2 Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                    'Call GetDefStatusAndRole()
                    'If ViewState("Def_Status") <> "" And
                    'ViewState("COV_USER_ROLE") <> "" Then
                    '    Call statusDDlbind()
                    '    Call gridbind()
                    'End If

                Else
                    'gvApproval.HeaderRow.TableSection = TableRowSection.TableHeader

                    '   menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call GetDefStatusAndRole()
                    If Session("Def_Status") <> "" And
                    ViewState("COV_USER_ROLE") <> "" Then
                        Call statusDDlbind()
                        Call schoolDDlbind()
                    End If


                    '  Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If
                'gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub

    Public Sub statusDDlbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", 0)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_STATUS_LIST", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ddlStatus.DataSource = ds.Tables(0)
                ddlStatus.DataBind()

                ddlStatus.DataSource = ds
                ddlStatus.DataTextField = "CSM_DESCRIPTION"
                ddlStatus.DataValueField = "CMS_CODE"

                ddlStatus.SelectedValue = Session("Def_Status")
                ddlStatus.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Public Sub schoolDDlbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@USR_ID", Session("sUsr_name"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_SCHOOL_LIST", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ddlSchool.DataSource = ds.Tables(0)
                ddlSchool.DataBind()

                ddlSchool.DataSource = ds
                ddlSchool.DataTextField = "BSU_NAME"
                ddlSchool.DataValueField = "BSU_ID"

                ddlSchool.SelectedValue = Session("covid_bBsuId")
                ddlSchool.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Public Sub GetDefStatusAndRole()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@USR_ID", Session("sUsr_name"))
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))



            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_DEF_STATUS_AND_ROLE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                Session("Def_Status") = ds.Tables(0).Rows(0)("STATUS").ToString()
                ViewState("COV_USER_ROLE") = ds.Tables(0).Rows(0)("ROLE").ToString()
                ViewState("DISABLE_ACTION_STATUS") = ds.Tables(0).Rows(0)("DISABLE_ACTION_STATUS").ToString()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Public Sub statusOnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "SetDataTable_New();", True)
        Session("Def_Status") = sender.SelectedItem.Value

    End Sub

    Public Sub schoolOnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "SetDataTable_New();", True)
        Session("covid_bBsuId") = sender.SelectedItem.Value

    End Sub

    Private Shared Function LoadData() As List(Of clsCOVID_RELIEF)
        Dim Data As List(Of clsCOVID_RELIEF) = New List(Of clsCOVID_RELIEF)()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", HttpContext.Current.Session("covid_bBsuId"))
            param(1) = New SqlParameter("@STATUS", HttpContext.Current.Session("Def_Status"))
            param(2) = New SqlParameter("@USR_ID", HttpContext.Current.Session("sUsr_name"))

            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "QRY.QRY_COVID_RELIEF", param)

            Dim str As String = Newtonsoft.Json.JsonConvert.SerializeObject(ds1.Tables(0))
            Data = JsonConvert.DeserializeObject(Of List(Of clsCOVID_RELIEF))((str.ToString()))

            For Each item As clsCOVID_RELIEF In Data
                item.RRH_ID_ENCR = Encryption64_Page.Encrypt(item.RRH_ID)
            Next
        Catch ex As Exception
            'Console.Write(ex)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return Data
    End Function

    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    Public Shared Function GetData() As Object
        Dim result As DataTablesM = New DataTablesM()

        Try
            Dim search As String = HttpContext.Current.Request.Params("search[value]")
            Dim draw As String = HttpContext.Current.Request.Params("draw")
            Dim order As String = HttpContext.Current.Request.Params("order[0][column]")
            Dim orderDir As String = HttpContext.Current.Request.Params("order[0][dir]")
            Dim PageNo As Integer = Convert.ToInt32(HttpContext.Current.Request.Params("start"))
            Dim pageSize As Integer = Convert.ToInt32(HttpContext.Current.Request.Params("length"))

            If orderDir Is Nothing Then
                orderDir = "ASC"
            End If


            Dim data As List(Of clsCOVID_RELIEF) = COVID_Relief_View.LoadData()
            Dim totalRecords As Integer = data.Count

            If HttpContext.Current.Session("Current_Page") = "SVP" Then
                If Not String.IsNullOrEmpty(search) AndAlso Not String.IsNullOrWhiteSpace(search) Then
                    data = data.Where(Function(p) p.BSU.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherName.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherEmploymentType.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherReliefReason.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherCompanyName.ToString().ToLower().Contains(search.ToLower()) OrElse p.F_TOT_SALARY.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherName.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherEmploymentType.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherReliefReason.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherCompanyName.ToString().ToLower().Contains(search.ToLower()) OrElse p.M_TOT_SALARY.ToString().ToLower().Contains(search.ToLower()) OrElse p.RRH_REMARKS.ToString().ToLower().Contains(search.ToLower()) OrElse p.Due_PreviousTerm.ToString().ToLower().Contains(search.ToLower()) OrElse p.Due_AprJun.ToString().ToLower().Contains(search.ToLower()) OrElse p.APPROVE_OR_REJECT_REASON.ToString().ToLower().Contains(search.ToLower()) OrElse p.FBP_COMMENTS.ToString().ToLower().Contains(search.ToLower())).ToList()
                End If
                If order Is Nothing Then
                    order = "3"
                End If
                data = COVID_Relief_View.SortByColumnWithOrderSVP(order, orderDir, data)
            Else
                If Not String.IsNullOrEmpty(search) AndAlso Not String.IsNullOrWhiteSpace(search) Then
                    data = data.Where(Function(p) p.BSU.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherName.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherEmploymentType.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherReliefReason.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherCompanyName.ToString().ToLower().Contains(search.ToLower()) OrElse p.Father_Salary.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherSalary_Accomodation.ToString().ToLower().Contains(search.ToLower()) OrElse p.FatherSalary_TuitionFee.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherName.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherEmploymentType.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherReliefReason.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherCompanyName.ToString().ToLower().Contains(search.ToLower()) OrElse p.Mother_Salary.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherSalary_Accomodation.ToString().ToLower().Contains(search.ToLower()) OrElse p.MotherSalary_TuitionFee.ToString().ToLower().Contains(search.ToLower()) OrElse p.Due_PreviousTerm.ToString().ToLower().Contains(search.ToLower()) OrElse p.Due_AprJun.ToString().ToLower().Contains(search.ToLower()) OrElse p.ApprovalStatus.ToString().ToLower().Contains(search.ToLower()) OrElse p.Submitted_BY.ToString().ToLower().Contains(search.ToLower())).ToList()
                End If
                If order Is Nothing Then
                    order = "0"
                End If
                data = COVID_Relief_View.SortByColumnWithOrder(order, orderDir, data)
            End If



            Dim recFilter As Integer = data.Count
            data = data.Skip(PageNo).Take(pageSize).ToList()
            result.draw = Convert.ToInt32(draw)
            result.recordsTotal = totalRecords
            result.recordsFiltered = recFilter
            result.data = data
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        Return result
    End Function
    Private Shared Function SortByColumnWithOrderSVP(ByVal order As String, ByVal orderDir As String, ByVal data As List(Of clsCOVID_RELIEF)) As List(Of clsCOVID_RELIEF)
        Dim lst As List(Of clsCOVID_RELIEF) = New List(Of clsCOVID_RELIEF)()
        Try
            Select Case order
                'Case "0"
                    'lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.SlNo).ToList(), data.OrderBy(Function(p) p.SlNo).ToList())
                'Case "1"
                '    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.STU_NO).ToList(), data.OrderBy(Function(p) p.STU_NO).ToList())
                Case "2"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.BSU).ToList(), data.OrderBy(Function(p) p.BSU).ToList())
                Case "3"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherName).ToList(), data.OrderBy(Function(p) p.FatherName).ToList())
                Case "4"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherName).ToList(), data.OrderBy(Function(p) p.MotherName).ToList())
                Case "5"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.RRH_REMARKS).ToList(), data.OrderBy(Function(p) p.RRH_REMARKS).ToList())
                Case "6"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.TermlyFee).ToList(), data.OrderBy(Function(p) p.TermlyFee).ToList())
                Case "7"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.APPROVE_OR_REJECT_REASON).ToList(), data.OrderBy(Function(p) p.APPROVE_OR_REJECT_REASON).ToList())

                Case Else
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherName).ToList(), data.OrderBy(Function(p) p.SlNo).ToList())
            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return lst
    End Function
    Private Shared Function SortByColumnWithOrder(ByVal order As String, ByVal orderDir As String, ByVal data As List(Of clsCOVID_RELIEF)) As List(Of clsCOVID_RELIEF)
        Dim lst As List(Of clsCOVID_RELIEF) = New List(Of clsCOVID_RELIEF)()
        Try
            Select Case order
                Case "0"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.SlNo).ToList(), data.OrderBy(Function(p) p.SlNo).ToList())
                'Case "1"
                '    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.STU_NO).ToList(), data.OrderBy(Function(p) p.STU_NO).ToList())
                Case "2"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.BSU).ToList(), data.OrderBy(Function(p) p.BSU).ToList())
                Case "3"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherName).ToList(), data.OrderBy(Function(p) p.FatherName).ToList())
                Case "4"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherEmploymentType).ToList(), data.OrderBy(Function(p) p.FatherEmploymentType).ToList())
                Case "5"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherReliefReason).ToList(), data.OrderBy(Function(p) p.FatherReliefReason).ToList())
                Case "6"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherCompanyName).ToList(), data.OrderBy(Function(p) p.FatherCompanyName).ToList())
                Case "7"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.Father_Salary).ToList(), data.OrderBy(Function(p) p.Father_Salary).ToList())
                Case "8"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherSalary_Accomodation).ToList(), data.OrderBy(Function(p) p.FatherSalary_Accomodation).ToList())
                Case "9"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.FatherSalary_TuitionFee).ToList(), data.OrderBy(Function(p) p.FatherSalary_TuitionFee).ToList())
                Case "10"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherName).ToList(), data.OrderBy(Function(p) p.MotherName).ToList())
                Case "11"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherEmploymentType).ToList(), data.OrderBy(Function(p) p.MotherEmploymentType).ToList())
                Case "12"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherReliefReason).ToList(), data.OrderBy(Function(p) p.MotherReliefReason).ToList())
                Case "13"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherCompanyName).ToList(), data.OrderBy(Function(p) p.MotherCompanyName).ToList())
                Case "14"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.Mother_Salary).ToList(), data.OrderBy(Function(p) p.Mother_Salary).ToList())
                Case "15"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherSalary_Accomodation).ToList(), data.OrderBy(Function(p) p.MotherSalary_Accomodation).ToList())
                Case "16"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.MotherSalary_TuitionFee).ToList(), data.OrderBy(Function(p) p.MotherSalary_TuitionFee).ToList())
                Case "17"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.Due_PreviousTerm).ToList(), data.OrderBy(Function(p) p.Due_PreviousTerm).ToList())
                Case "18"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.Due_AprJun).ToList(), data.OrderBy(Function(p) p.Due_AprJun).ToList())
                Case "19"
                    lst = If(orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.ApprovalStatus).ToList(), data.OrderBy(Function(p) p.ApprovalStatus).ToList())

                Case Else
                    lst = If(orderDir.Equals("ASC", StringComparison.CurrentCultureIgnoreCase), data.OrderByDescending(Function(p) p.SlNo).ToList(), data.OrderBy(Function(p) p.SlNo).ToList())
            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return lst
    End Function

End Class



Public Class clsCOVID_RELIEF

    Public Property SlNo As Integer
    Public Property RRH_ID As String
    Public Property BSU As String
    Public Property STU_NO As String
    Public Property FatherName As String
    Public Property FatherEmploymentType As String
    Public Property FatherReliefReason As String
    Public Property FatherNationalID As String

    Public Property FatherCompanyName As String
    Public Property Father_Salary As String
    Public Property FatherSalary_Accomodation As String
    Public Property FatherSalary_TuitionFee As String
    Public Property MotherName As String
    Public Property MotherEmploymentType As String


    Public Property MotherReliefReason As String
    Public Property MotherNationalID As String
    Public Property MotherCompanyName As String
    Public Property Mother_Salary As String
    Public Property MotherSalary_Accomodation As String
    Public Property MotherSalary_TuitionFee As String
    Public Property RRH_REMARKS As String
    Public Property Due_PreviousTerm As String
    Public Property Due_AprJun As String
    Public Property FilePaths As String
    Public Property ApprovalStatus As String
    Public Property Submitted_Date As String
    Public Property RRH_STATUS As String
    Public Property Submitted_BY As String
    Public Property F_TOT_SALARY As String
    Public Property M_TOT_SALARY As String
    Public Property TOT_SALARY As String
    Public Property TermlyFee As String
    Public Property Current_Term_Concession As String
    Public Property DiscountPerc As String
    Public Property DiscountAmount As String
    Public Property FBP_COMMENTS As String
    Public Property RRH_ID_ENCR As String
    Public Property APPROVE_OR_REJECT_REASON As String
    Public Property BG_COLOR As String
    Public Property COLOR_DESC As String
End Class

Public Class DataTablesM
    Public Property draw As Integer
    Public Property recordsTotal As Integer
    Public Property recordsFiltered As Integer
    Public Property data As List(Of clsCOVID_RELIEF)
End Class


Public Class Encryption64_Page
    Private Shared key() As Byte = {}
    Private Shared IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

    Public Shared Function Decrypt(ByVal stringToDecrypt As String,
        Optional ByVal sEncryptionKey As String = "!#$a54?W") As String
        Dim inputByteArray(stringToDecrypt.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV),
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

    Public Shared Function Encrypt(ByVal stringToEncrypt As String,
       Optional ByVal SEncryptionKey As String = "!#$a54?W") As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV),
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

End Class
