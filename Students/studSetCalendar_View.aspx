<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studSetCalendar_View.aspx.vb" Inherits="Students_studSetHoilday_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>
    <script language="javascript" src="../chromejs/HGridScript.js" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Attendance Calender</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table  width="100%">
                    <tr>
                        <td align="left" colspan="5" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" valign="middle">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="middle" width="20%">
                            <span class="field-label">Select Academic Year</span></td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" valign="top">
                            <asp:GridView ID="gvHolidayRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" OnRowDataBound="gvHolidayRecord_RowDataBound" OnRowCreated="gvHolidayRecord_RowCreated">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="SCH_ID">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SAL_ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSCH_IDH" runat="server" Text='<%# Bind("SCH_ID") %>' __designer:wfdid="w110"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SCH_ID" HeaderText="SCH_ID"></asp:BoundField>
                                    <asp:HyperLinkField Text="[+]"></asp:HyperLinkField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w90"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblGradeH" runat="server" Text="From Date" CssClass="gridheader_text" __designer:wfdid="w91"></asp:Label><br />
                                            <asp:TextBox ID="txtSCH_DTFROM" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchFromDT" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w93" OnClick="btnSearchFromDT_Click"></asp:ImageButton>&nbsp;
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="lblFromDTH" runat="server" Text='<%# Bind("SCH_DTFROM", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w89"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" __designer:wfdid="w95"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblToDateH" runat="server" Text="To Date" CssClass="gridheader_text" __designer:wfdid="w96"></asp:Label><br />
                                            <asp:TextBox ID="txtSCH_DTTO" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchToDT" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w98" OnClick="btnSearchToDT_Click"></asp:ImageButton>&nbsp;</td>
                                          
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblToDTH" runat="server" Text='<%# Bind("SCH_DTTO", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w94"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("GRD_DESCR") %>' __designer:wfdid="w100"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblRemarksH" runat="server" Text="Remark" CssClass="gridheader_text" __designer:wfdid="w101"></asp:Label><br />
                                            <asp:TextBox ID="txtSCH_REMARKS" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchRemark" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w103" OnClick="btnSearchRemark_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarkH" runat="server" Text='<%# Bind("SCH_REMARKS") %>' __designer:wfdid="w99"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Att. Type">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EMP_Name") %>' __designer:wfdid="w105"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblStud_NameH" runat="server" Text="Att Reqd" CssClass="gridheader_text" __designer:wfdid="w106"></asp:Label><br />
                                                                            <asp:TextBox ID="txtSCH_TYPE" runat="server"></asp:TextBox></td>
                                                                            <asp:ImageButton ID="btnSearchAttReqd" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w108" OnClick="btnSearchAttReqd_Click"></asp:ImageButton>&nbsp;</td>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAttType" runat="server" Text='<%# Bind("SCH_TYPE") %>' __designer:wfdid="w104"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" __designer:wfdid="w112"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View" __designer:wfdid="w113"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblView" runat="server" __designer:wfdid="w111" OnClick="lblView_Click">View</asp:LinkButton>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField></asp:BoundField>
                                </Columns>
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4" valign="middle"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden"
                                        value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="txtExpandedDivs" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

