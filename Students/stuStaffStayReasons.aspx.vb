﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports Lesnikowski.Barcode
Partial Class Students_stuStaffStayReasons
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050139") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'GetActive_ACD_4_Grade


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    gridbind()
                End If
            Catch ex As Exception

                ' lblm.Text = "Request could not be processed "
            End Try

        End If
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub showGrid()
        If rdOptions.Items(0).Selected Then
            tr1.Visible = True
            tr2.Visible = False
            trReasons.Visible = True
            trMenu.Visible = False
        Else
            tr1.Visible = False
            tr2.Visible = True
            trReasons.Visible = False
            trMenu.Visible = True
        End If
    End Sub
    Private Sub gridbind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet

        If rdOptions.Items(0).Selected Then
            Dim str_query As String = "SELECT SBR_ID,SBR_REASON  FROM STAFFSTAYBACKREASON_M "



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                gvReason.DataSource = ds.Tables(0)
                gvReason.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvReason.DataSource = ds.Tables(0)
                Try
                    gvReason.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvReason.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvReason.Rows(0).Cells.Clear()
                gvReason.Rows(0).Cells.Add(New TableCell)
                gvReason.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReason.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReason.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        End If
        If rdOptions.Items(1).Selected Then
            Dim str_query As String = "SELECT SLM_ID,SLM_ITEM  FROM STAFFLUNCHMENU_M "



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                gvMenu.DataSource = ds.Tables(0)
                gvMenu.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvMenu.DataSource = ds.Tables(0)
                Try
                    gvMenu.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvMenu.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvMenu.Rows(0).Cells.Clear()
                gvMenu.Rows(0).Cells.Add(New TableCell)
                gvMenu.Rows(0).Cells(0).ColumnSpan = columnCount
                gvMenu.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvMenu.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
        End If


    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divReason.Visible = False
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divReason.Visible = False
    End Sub

    Protected Sub lnkadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkadd.Click
        divReason.Visible = True
        btnSav.Text = "Save"
        ViewState("LR_ID") = "0"
        txtReason.Text = ""
        If rdOptions.Items(0).Selected Then
            lbl.Text = "Reason"
        Else
            lbl.Text = "Item"
        End If
    End Sub

    Protected Sub gvReason_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReason.RowCommand
        If e.CommandName = "edit" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvReason.Rows(index), GridViewRow)

            Dim lblLRID As Label

            lblLRID = selectedRow.FindControl("lblLRID")
            ViewState("LR_ID") = lblLRID.Text
            divReason.Visible = True
            dispayitems(ViewState("LR_ID"))
            If btnSav.Text = "Save" Then btnSav.Text = "Update"
            lbl.Text = "Reason"
        End If
    End Sub

    Sub dispayitems(ByVal id As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""


            If rdOptions.Items(0).Selected Then
                str_query = "SELECT SBR_ID,SBR_REASON  FROM STAFFSTAYBACKREASON_M" _
                            & " WHERE SBR_ID=" & id
                If str_query <> "" Then
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    If ds.Tables(0).Rows.Count >= 1 Then


                        txtReason.Text = ds.Tables(0).Rows(0).Item("SBR_REASON")




                    End If
                End If
            Else
                str_query = "SELECT SLM_ID,SLM_ITEM  FROM STAFFLUNCHMENU_M" _
                                            & " WHERE SLM_ID=" & id
                If str_query <> "" Then
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    If ds.Tables(0).Rows.Count >= 1 Then


                        txtReason.Text = ds.Tables(0).Rows(0).Item("SLM_ITEM")




                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblUerror.Text = "Request could not be processed"
        End Try
    End Sub

    Sub save_reasons(ByVal id As Integer)
        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                If rdOptions.Items(0).Selected Then
                    param(0) = New SqlParameter("@SBR_ID", id)
                    param(1) = New SqlParameter("@SBR_REASON", txtReason.Text)


                    param(3) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                    param(3).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVE_STAFFSTAYBACKREASON_M", param)
                    Status = param(3).Value
                Else
                    param(0) = New SqlParameter("@SLM_ID", id)
                    param(1) = New SqlParameter("@SLM_ITEM", txtReason.Text)


                    param(3) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                    param(3).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVE_STAFFLUNCHMENU_M", param)
                    Status = param(3).Value
                End If




            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Status <> 0 Then
                    ' UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    transaction.Rollback()
                    lblUerror.Text = "Error "
                Else
                    If btnSav.Text = "save" Then
                        lblUerror.Text = "Saved Successfully."
                    Else
                        lblUerror.Text = "Updated Successfully."
                    End If
                    transaction.Commit()
                    gridbind()
                End If
            End Try

        End Using
    End Sub

    Protected Sub gvReason_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvReason.RowEditing

    End Sub

    Protected Sub btnSav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSav.Click
        save_reasons(ViewState("LR_ID"))
    End Sub




    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        ' Create_print()

        'Dim jscript As New StringBuilder()


        'jscript.Append("<script>window.open('")
        'jscript.Append("~/Students/stuStaffStayBarcode.aspx?type=0")
        'jscript.Append("');</script>")
        '  Page.RegisterStartupScript("OpenWindows", jscript.ToString())


        If rdOptions.Items(0).Selected Then
            'Response.Redirect("~/Students/stuStaffStayBarcode.aspx?type=0", False)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + "/Students/stuStaffStayBarcode.aspx?type=0" + "','_blank');", True)
        Else
            'Response.Redirect("~/Students/stuStaffStayBarcode.aspx?type=1", False)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + "/Students/stuStaffStayBarcode.aspx?type=1" + "','_blank');", True)
        End If
    End Sub

    Protected Sub rdOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOptions.SelectedIndexChanged
        showGrid()
        gridbind()
    End Sub

    Protected Sub gvMenu_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMenu.RowCommand
        If e.CommandName = "edit" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvMenu.Rows(index), GridViewRow)

            Dim lblLRID As Label

            lblLRID = selectedRow.FindControl("lblSLMID")
            ViewState("LR_ID") = lblLRID.Text
            divReason.Visible = True
            dispayitems(ViewState("LR_ID"))
            If btnSav.Text = "Save" Then btnSav.Text = "Update"
            lbl.Text = "Item"
        End If
    End Sub

    Protected Sub gvMenu_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvMenu.RowEditing

    End Sub
End Class
