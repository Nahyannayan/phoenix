Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Globalization

Partial Class Students_studSetHoliday
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Dim Session("dsEvents") As New DataSet

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")



        If Page.IsPostBack = False Then

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050114") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                hfDate.Value = DateTime.Now

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Call bindAcademicYear()

                If ViewState("datamode") = "view" Then

                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    Call RecordBind()
                    Call SetState()



                ElseIf ViewState("datamode") = "add" Then

                    txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    chkActive.Checked = True
                    ddlAcdYear.ClearSelection()
                    ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                    ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)

                    Call ResetState()

                End If



            End If
        End If

    End Sub
    Private Sub bindParam()
        Dim acd_id As String = ddlAcdYear.SelectedValue

        Dim ds As DataSet = AccessStudentClass.GetATTENDANCE_PARAM_D(acd_id, Session("sBsuid"), "Session1")
        ddlAttParam.DataSource = ds
        ddlAttParam.DataTextField = "APD_PARAM_DESCR"
        ddlAttParam.DataValueField = "APD_ID"
        ddlAttParam.DataBind()
    End Sub

    Public Sub BindcheckNodes(ByVal nodeList As TreeNodeCollection, ByVal menuCode As String) 'in view mode bind it to the grid view
        If Not nodeList Is Nothing Then
            For Each nodec As TreeNode In nodeList

                Dim strNodeValue As String = nodec.Value

                If strNodeValue = menuCode Then

                    nodec.Checked = True
                End If
                BindcheckNodes(nodec.ChildNodes, menuCode)
            Next
        End If
    End Sub
    Sub BindTree_GradeSection() 'for orginating tree view for the grade and section
        Dim ACD_ID As String = String.Empty
        If ddlAcdYear.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAcdYear.SelectedItem.Value
        End If

        Dim dtTable As DataTable = AccessStudentClass.PopulateGrade_Section(ACD_ID)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim drGRD_DESCR As DataRow
        For i As Integer = 0 To dtTable.Rows.Count - 1
            drGRD_DESCR = dtTable.Rows(i)
            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
            If contains Then
                Continue For
            End If
            Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
            drGRD_DESCR("GRD_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
        Next
        tvGrade.Nodes.Clear()
        tvGrade.Nodes.Add(trSelectAll)
        tvGrade.DataBind()

    End Sub

   

   
    Sub SetState()
        txtDescr.Enabled = False
        ddlTime.Enabled = False
        chkActive.Enabled = False
        ddlAcdYear.Enabled = False
        ddlAttParam.Enabled = False
        'plGRD_SCT.Enabled = False
        tvGrade.Enabled = False
        txtFrom.Enabled = False
        txtTo.Enabled = False


    End Sub

    Sub ResetState()
        txtDescr.Enabled = True
        ddlTime.Enabled = True
        chkActive.Enabled = True
        ' plGRD_SCT.Enabled = True
        tvGrade.Enabled = True
        ddlAcdYear.Enabled = True
        ddlAttParam.Enabled = True

        txtFrom.Enabled = True
        txtTo.Enabled = True

    End Sub
    Sub clearContent()
        txtDescr.Text = ""
        ddlTime.ClearSelection()
        ddlTime.SelectedIndex = 0
        chkActive.Checked = True

        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        ddlAttParam.ClearSelection()
        ddlAttParam.SelectedIndex = 0
        ddlAcdYear.ClearSelection()
        ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        ClearAllCheck(tvGrade.Nodes)
    End Sub

    Sub bindAcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function validateDateRange() As String
        Dim CommStr As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text


        If (From_date >= Acd_SDate And From_date <= Acd_EDate) Then
            If txtTo.Text.Trim <> "" Then
                Dim To_Date As Date = txtTo.Text
                If Not ((To_Date >= Acd_SDate And To_Date <= Acd_EDate)) Then
                    CommStr = CommStr & "<div>From Date & To Date must be with in the academic year</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date  must be with in the academic year</div>"
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Sub getStart_EndDate()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
            ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
        Else
            ViewState("ACD_STARTDT") = ""
            ViewState("ACD_ENDDT") = ""

        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty
        Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFrom.Text)
        Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
       
        Dim errorMessage As String = String.Empty
        If serverDateValidate() = "0" Then
            If validateDateRange() = "0" Then
                If t1 < t2 Then
                    lblError.Text = "You cannot select a day less than today!"
                    txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Else
                    If tvGrade.CheckedNodes.Count > 0 Then
                        str_err = CallTransaction(errorMessage)

                        If str_err = "0" Then

                            lblError.Text = "Record updated successfully"
                        Else
                            lblError.Text = errorMessage
                        End If
                    Else
                        lblError.Text = "Select Grade & section !!"
                    End If
                End If
            End If

            End If

    End Sub



    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim status As String = String.Empty
        Dim str As String = String.Empty
        Dim AAM_ID As String = String.Empty
        Dim AAM_BSU_ID As String = Session("sBsuid")
        Dim AAM_ACD_ID As String
        If ddlAcdYear.SelectedIndex = -1 Then
            AAM_ACD_ID = ""
        Else
            AAM_ACD_ID = ddlAcdYear.SelectedItem.Value
        End If
        Dim AAM_FROMDT As String = txtFrom.Text
        Dim AAM_TODT As String = txtTo.Text
        Dim AAM_DESCR As String = txtDescr.Text
        Dim ATD_APD_ID As String = ddlAttParam.SelectedItem.Value
        Dim AAM_bACTIVE As Boolean = chkActive.Checked
        Dim AAM_SCHEDULE As String = ddlTime.SelectedValue
        Dim bEdit As Boolean
        'check the data mode to do the required operation

        If ViewState("datamode") = "add" Then
            'only insert if applicable to particular grade & section
           
            Dim transaction As SqlTransaction
            bEdit = False
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    status = ""
                    Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
                    If tvGrade.CheckedNodes.Count > 0 Then
                        For Each node As TreeNode In tvGrade.CheckedNodes
                            If Not node Is Nothing Then
                                If node.Text <> "Select All" Then
                                    If node.Parent.Text <> "Select All" Then
                                        Dim grd_id = node.Parent.Value
                                        Dim sct_id = node.Value
                                        str += String.Format("<AUTOMATE_ATT_D ATD_GRD_ID='{0}'  ATD_SCT_ID='{1}'/>", node.Parent.Value, node.Value)
                                    End If
                                End If
                            End If
                        Next
                    End If
                    If str <> "" Then
                        str = "<AUTOMATE>" + str + "</AUTOMATE>"
                    End If


              
                    Dim pParms(12) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STR", str)
                    pParms(1) = New SqlClient.SqlParameter("@AAM_ID", AAM_ID)
                    pParms(2) = New SqlClient.SqlParameter("@AAM_BSU_ID", AAM_BSU_ID)
                    pParms(3) = New SqlClient.SqlParameter("@AAM_ACD_ID", AAM_ACD_ID)
                    pParms(4) = New SqlClient.SqlParameter("@AAM_FROMDT", AAM_FROMDT)
                    pParms(5) = New SqlClient.SqlParameter("@AAM_TODT", AAM_TODT)
                    pParms(6) = New SqlClient.SqlParameter("@AAM_DESCR", AAM_DESCR)
                    pParms(7) = New SqlClient.SqlParameter("@ATD_APD_ID", ATD_APD_ID)
                    pParms(8) = New SqlClient.SqlParameter("@AAM_bACTIVE", AAM_bACTIVE)
                    pParms(9) = New SqlClient.SqlParameter("@AAM_SCHEDULE", AAM_SCHEDULE)
                    pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)
                    pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(11).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEAUTOMATE_ATT]", pParms)
                    Dim ReturnFlag As Integer = pParms(11).Value


                    If ReturnFlag <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error occured while processing info."
                        status = "1"
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                    Call SetState()
                    Call clearContent()

                Catch ex As Exception

                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        errorMessage = "Error occured while processing info."
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using




        ElseIf ViewState("datamode") = "edit" Then

            Dim transaction As SqlTransaction
            bEdit = True
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    status = ""
                    Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
                    If tvGrade.CheckedNodes.Count > 0 Then
                        For Each node As TreeNode In tvGrade.CheckedNodes
                            If Not node Is Nothing Then
                                If node.Text <> "Select All" Then
                                    If node.Parent.Text <> "Select All" Then
                                        Dim grd_id = node.Parent.Value
                                        Dim sct_id = node.Value
                                        str += String.Format("<AUTOMATE_ATT_D ATD_GRD_ID='{0}'  ATD_SCT_ID='{1}'/>", node.Parent.Value, node.Value)
                                    End If
                                End If
                            End If
                        Next
                    End If
                    If str <> "" Then
                        str = "<AUTOMATE>" + str + "</AUTOMATE>"
                    End If


                 
                    Dim pParms(12) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STR", str)
                    pParms(1) = New SqlClient.SqlParameter("@AAM_ID", AAM_ID)
                    pParms(2) = New SqlClient.SqlParameter("@AAM_BSU_ID", AAM_BSU_ID)
                    pParms(3) = New SqlClient.SqlParameter("@AAM_ACD_ID", AAM_ACD_ID)
                    pParms(4) = New SqlClient.SqlParameter("@AAM_FROMDT", AAM_FROMDT)
                    pParms(5) = New SqlClient.SqlParameter("@AAM_TODT", AAM_TODT)
                    pParms(6) = New SqlClient.SqlParameter("@AAM_DESCR", AAM_DESCR)
                    pParms(7) = New SqlClient.SqlParameter("@ATD_APD_ID", ATD_APD_ID)
                    pParms(8) = New SqlClient.SqlParameter("@AAM_bACTIVE", AAM_bACTIVE)
                    pParms(9) = New SqlClient.SqlParameter("@AAM_SCHEDULE", AAM_SCHEDULE)
                    pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)
                    pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(11).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEAUTOMATE_ATT]", pParms)
                    Dim ReturnFlag As Integer = pParms(11).Value


                    If ReturnFlag <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error occured while processing info."
                        status = "1"
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                    Call SetState()
                    Call clearContent()

                Catch ex As Exception

                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        errorMessage = "Error occured while processing info."
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using


        End If

    End Function


    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdYear.SelectedIndexChanged
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

        getStart_EndDate()
        ClearAllCheck(tvGrade.Nodes)
        Call BindTree_GradeSection()
        bindParam()
    End Sub

  
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            If Not tvGrade Is Nothing Then
                DisableAutoPostBack(tvGrade.Nodes)
            End If
        End If


    End Sub
    Sub DisableAutoPostBack(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub ClearAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = False

            If tn.ChildNodes.Count > 0 Then
                ClearAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub SelectAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = True

            If tn.ChildNodes.Count > 0 Then
                SelectAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    'Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ClearAllCheck(tvGrade.Nodes)
    '    txtGradeSection.Text = ""
    '    'Session("ListOfSection").clear()
    'End Sub
    Protected Sub okButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        populateGrades()
    End Sub
    Public Sub populateGrades()

        Dim ListOfSection As New ArrayList
        ' Session("ListOfSection").clear()
        ' Session("ListOfSection") = ListOfSection

        Dim strAddSection As String = String.Empty
        Dim parentId As String = String.Empty
        Dim tempID As String = String.Empty
        Dim childCount As Integer
        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
        Dim tempCount As Integer
        Dim temp As String = String.Empty
        If tvGrade.CheckedNodes.Count > 0 Then

            'For i As Integer = 0 To tvGrade.CheckedNodes.Count

            'Next

            For Each node As TreeNode In tvGrade.CheckedNodes
                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then

                            Dim popAttGrade As New Attendance(node.Parent.Text, node.Parent.Value, node.Text, node.Value)
                            childCount = node.Parent.ChildNodes.Count
                            ListOfSection.Add(popAttGrade)
                            If tempID = node.Parent.Text Then
                                strAddSection = strAddSection + node.Text + ","
                                ' tempCount = tempCount + 1
                            Else
                                strAddSection = strAddSection + "}" + node.Parent.Text + "{" + node.Text + ","
                                tempID = node.Parent.Text
                                'tempCount = 1
                            End If
                        End If
                    End If

                End If
            Next
            Dim allSection As String = String.Empty
            strAddSection = strAddSection.Replace(",}", "},").TrimStart("}") '.Remove(0, 1)
            allSection = strAddSection.TrimEnd(",") + "}"
            If allSection.Length = 1 Then
                allSection = ""
            End If
         
            If ViewState("nodeCounts") = ListOfSection.Count Then
                ViewState("AllGrades") = 1
            Else
                ViewState("AllGrades") = 0
            End If
        Else

        End If

    End Sub
   


    Sub RecordBind()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_string As String = "SELECT distinct  AAM_DESCR, AAM_ACD_ID,replace(convert(varchar(12), AAM_FROMDT ,106),' ','/') as AAM_FROMDT,replace(convert(varchar(12),  AAM_TODT, 106),' ','/') as  AAM_TODT, isnull(AAM_bACTIVE,0) as AAM_bACTIVE, AAM_SCHEDULE,ATD_APD_ID " & _
" FROM         ATT.AUTOMATE_ATT_M  inner join ATT.AUTOMATE_ATT_D on ATD_AAM_ID=AAM_ID  where  AAM_ID='" & ViewState("viewid") & "'"
        Using readerSql As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.Text, str_string)
            If readerSql.HasRows Then
                While readerSql.Read
                    If Not ddlAcdYear.Items.FindByValue(Convert.ToString(readerSql("AAM_ACD_ID"))) Is Nothing Then
                        ddlAcdYear.ClearSelection()
                        ddlAcdYear.Items.FindByValue(Convert.ToString(readerSql("AAM_ACD_ID"))).Selected = True
                        ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
                    End If
                    txtDescr.Text = Convert.ToString(readerSql("AAM_DESCR"))
                    txtFrom.Text = Convert.ToString(readerSql("AAM_FROMDT"))
                    txtTo.Text = Convert.ToString(readerSql("AAM_TODT"))
                    If Not ddlTime.Items.FindByValue(Convert.ToString(readerSql("AAM_SCHEDULE"))) Is Nothing Then
                        ddlTime.ClearSelection()
                        ddlTime.Items.FindByValue(Convert.ToString(readerSql("AAM_SCHEDULE"))).Selected = True
                    End If

                    If Not ddlAttParam.Items.FindByValue(Convert.ToString(readerSql("ATD_APD_ID"))) Is Nothing Then
                        ddlAttParam.ClearSelection()
                        ddlAttParam.Items.FindByValue(Convert.ToString(readerSql("ATD_APD_ID"))).Selected = True
                    End If
                    chkActive.Checked = Convert.ToBoolean(readerSql("AAM_bACTIVE"))

                End While
            End If
        End Using

        str_string = "select atd_sct_id from ATT.AUTOMATE_ATT_D where atd_aam_id='" & ViewState("viewid") & "'"
        Using readerSql As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.Text, str_string)
            If readerSql.HasRows Then
                While readerSql.Read
                    Call checkme(tvGrade.Nodes, Convert.ToString(readerSql("atd_sct_id")))

                End While
            End If
        End Using

    End Sub

    Public Sub checkme(ByVal nodeList As TreeNodeCollection, ByVal SCTCode As String)

        If Not nodeList Is Nothing Then
            For Each nodec As TreeNode In nodeList
                '  If Not nodec.Parent Is Nothing Then
                Dim strNodeValue As String = nodec.Value
                '   For Each node As TreeNode In nodec.Parent.ChildNodes
                If strNodeValue = SCTCode Then
                    'tvmenu.SelectedNode.Checked
                    ' Label2.Text += nodec.Value & " "
                    nodec.Parent.Expanded = True
                    nodec.Checked = True
                End If
                ' Next
                '  End If
                checkme(nodec.ChildNodes, SCTCode)
            Next
        End If
    End Sub
    
    Public Sub populateGrades_onLoad()

        Dim ListOfSection As New ArrayList
        ' Session("ListOfSection").clear()
        ' Session("ListOfSection") = ListOfSection

        Dim strAddSection As String = String.Empty
        Dim parentId As String = String.Empty
        Dim tempID As String = String.Empty
        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes


        If tvGrade.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGrade.CheckedNodes

                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then
                            Dim popAttGrade As New Attendance(node.Parent.Text, node.Parent.Value, node.Text, node.Value)
                            ListOfSection.Add(popAttGrade)
                            If tempID = node.Parent.Text Then
                                strAddSection = strAddSection + node.Text + ","
                            Else
                                strAddSection = strAddSection + "}" + node.Parent.Text + "{" + node.Text + ","
                                tempID = node.Parent.Text
                            End If
                        End If
                    End If

                End If
            Next
            strAddSection = strAddSection.Replace(",}", "},").TrimStart("}") '.Remove(0, 1)
         




            If ViewState("nodeCounts") = ListOfSection.Count Then
                ViewState("AllGrades") = 1
            Else
                ViewState("AllGrades") = 0
            End If
        Else
            ViewState("AllGrades") = 0
        End If

    End Sub

   

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call SetState()
            Call clearContent()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ResetState()
        clearContent()

        ViewState("datamode") = "add"

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        Call ResetState()
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub




  
End Class
