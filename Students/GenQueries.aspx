<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="GenQueries.aspx.vb" Inherits="GenQueries" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }

        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Customized Queries"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" style="width: 100%;">

                                <tr runat="server" id="tr1">
                                    <td align="left" style="width: 20%"><span class="field-label">Select Query</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:DropDownList ID="ddlQuery" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 50%"></td>
                                </tr>



                                <tr runat="server" id="tr_Acad">
                                    <td align="left" style="width: 20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:DropDownList ID="ddlACD_ID" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 50%"></td>
                                </tr>


                                <tr runat="server" id="tr_Acad_List">
                                    <td align="left" style="width: 20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" style="width: 30%">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstACAD" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td style="width: 50%"></td>
                                </tr>




                                <tr runat="server" id="tr_Grade">
                                    <td align="left" style="width: 20%"><span class="field-label">Grade</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:DropDownList ID="ddlGRD_ID" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 50%"></td>
                                </tr>


                                <tr runat="server" id="tr_Grade_List">
                                    <td align="left" style="width: 20%"><span class="field-label">Grade</span></td>
                                    <td align="left" style="width: 20%">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstGrades" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td style="width: 50%"></td>
                                </tr>

                                <tr runat="server" id="tr_AsOnDate">
                                    <td align="left" style="width: 20%"><span class="field-label">As on/From Date</span></td>
                                    <td align="left" style="text-align: left;width: 30%">
                                        <asp:TextBox ID="txtAsOnDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgAsOnDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                             />
                                    </td>
                                    <td></td>
                                </tr>



                                <tr runat="server" id="tr_ToDate">
                                    <td align="left" style="width: 20%"><span class="field-label">To Date</span></td>
                                    <td align="left" style="text-align: left;width: 30%">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                              />
                                    </td>
                                    <td></td>
                                </tr>
                                     <tr>
                                    <td width="20%">
                                        <asp:CheckBox ID="chkSubscribe" runat="server" CssClass="field-label" Text="Subscribe" OnCheckedChanged="chkSubscribe_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                    <td id="tdSubscribe" runat="server" width="30%">
                                        <asp:RadioButtonList ID="rblSubscribe" runat="server" RepeatDirection="Horizontal" CssClass="field-label"   >
                                            <asp:ListItem Text="Daily" Value="D" ><span class="field-label">Daily</span></asp:ListItem>
                                            <asp:ListItem Text="Weekly" Value="W"><span class="field-label">Weekly</span></asp:ListItem>
                                            <asp:ListItem Text="Monthly" Value="M"><span class="field-label">Monthly</span></asp:ListItem>
                                        </asp:RadioButtonList>
                                        <asp:Button ID="Button1" runat="server" CssClass="button" Text="Subscribe" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export to Excel"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calAsOnDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOnDate" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>



                  <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" X="250" Y="200"
                    CancelControlID="btnCancel" OkControlID="btnOkay"
                    TargetControlID="Button1" PopupControlID="Panel1"
                    PopupDragHandleControlID="PopupHeader" Drag="true">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel ID="Panel1" Style="display: none; width:400px;" runat="server" CssClass="panel-cover">
                    <div  >
                        
                        <div class="title-bg" id="PopupHeader">Confirm Subscribe?
                            <asp:ImageButton ID="ImageButton1" CssClass="float-right align-middle" runat="server" ImageUrl="~/Images/Curriculum/btnCloseblack.png" />
                        </div>
                        <div class="PopupBody m-3">
                            <p>Click on 'Ok' to subscribe this report for the selected criteria(s). </p>
                        </div>
                        <div  align="center" >
                            <asp:Button ID="btnOkay" runat="server" CssClass="button" UseSubmitBehavior="false" Text="Ok" OnClick="btnOkay_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                        </div>
                    </div>
                </asp:Panel>






            </div>
        </div>
    </div>
</asp:Content>

