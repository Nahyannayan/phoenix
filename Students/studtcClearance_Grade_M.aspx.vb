Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studtcClearance_Grade_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050150") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("datamode") <> "add" Then
                        hfTCG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("tcgid").Replace(" ", "+"))
                        Dim li As New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("accid").Replace(" ", "+"))
                        ddlAcademicYear.Items.Add(li)

                        li = New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                        ddlGrade.Items.Add(li)

                        li = New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                        ddlStream.Items.Add(li)

                        PopulateClearance()
                        li = New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("clearance").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("tccid").Replace(" ", "+"))
                        ddlClearance.Items(ddlClearance.Items.IndexOf(li)).Selected = True

                        EnableDisableControls(False)
                    Else
                        hfTCG_ID.Value = 0
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                        PopulateGrade()
                        ddlStream = studClass.PopulateStream(ddlStream)
                        PopulateClearance()
                    End If

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private Methods"


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub EnableDisableControls(ByVal value As Boolean)
        ddlAcademicYear.Enabled = value
        ddlStream.Enabled = value
        ddlGrade.Enabled = value
        ddlClearance.Enabled = value
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "tcc_id"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        keys(0) = column
        column.ColumnName = "tcc_descr"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function


    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If ViewState("datamode") = "edit" Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "TCCLEARANCE_GRADE_M", "TCG_ID", "TCG_ID", "TCG_ID=" + hfTCG_ID.Value.ToString)
                ElseIf ViewState("datamode") = "delete" Then
                    UtilityObj.InsertAuditdetails(transaction, "delete", "TCCLEARANCE_GRADE_M", "TCG_ID", "TCG_ID", "TCG_ID=" + hfTCG_ID.Value.ToString)
                End If

                Dim str_query As String = "exec saveTCCLEARANCE_GRADE_M " _
                                        & hfTCG_ID.Value.ToString + "," _
                                        & "'" + Session("sbsuid") + "'," _
                                        & ddlAcademicYear.SelectedValue.ToString + "," _
                                        & ddlClearance.SelectedValue.ToString + "," _
                                        & "'" + ddlGrade.SelectedValue.ToString + "'," _
                                        & ddlStream.SelectedValue.ToString + "," _
                                        & "'" + ViewState("datamode") + "'"
                hfTCG_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TCC_ID(" + hfTCG_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub


    Sub PopulateGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A" _
                                 & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                 & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub PopulateClearance()
        ddlClearance.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCC_ID,TCC_DESCR FROM TCCLEARANCE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClearance.DataSource = ds
        ddlClearance.DataTextField = "TCC_DESCR"
        ddlClearance.DataValueField = "TCC_ID"
        ddlClearance.DataBind()
    End Sub


#End Region

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            EnableDisableControls(True)
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            EnableDisableControls(False)
            btnCancel.Text = "Back"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            EnableDisableControls(True)
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
            PopulateGrade()
            ddlStream = studClass.PopulateStream(ddlStream)
            PopulateClearance()
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            ViewState("datamode") = "none"
            hfTCG_ID.Value = 0
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            PopulateGrade()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
