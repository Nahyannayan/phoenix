<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="studAtt_Registration.aspx.vb" Inherits="Students_studAtt_Registration" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<%--    <link href="../cssfiles/title.css" rel="stylesheet" />--%>

    <style>

        
/*.red
{
	background-color: White;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	height: 15px;
	vertical-align: middle;
	color: Black;
	border-bottom: red 5px solid;
	background-image: url(../Images/password/red.gif);
	background-repeat: no-repeat;
}
.grey
{
	background-color: White;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	height: 15px;
	vertical-align: middle;
	color: Black;
	border-bottom: blue 5px solid;
	background-image: url(../Images/password/blue.gif);
	background-repeat: no-repeat;
}
.blue
{
	background-color: White;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	height: 15px;
	vertical-align: middle;
	color: Black;
	border-bottom: Maroon 5px solid;
	background-image: url(../Images/password/maroon.gif);
	background-repeat: no-repeat;
}
.yellow
{
	background-color: White;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	height: 15px;
	vertical-align: middle;
	color: Black;
	border-bottom: Lime 5px solid;
	background-image: url(../Images/password/lime.gif);
	background-repeat: no-repeat;
}
.green
{
	background-color: White;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	height: 15px;
	vertical-align: middle;
	color: Black;
	border-bottom: green 5px solid;
	background-image: url(../Images/password/green.gif);
	background-repeat: no-repeat;
}*/

.smallBox
{
display:block;
width:100%;
height:100%;
}
.smallBox_new
{
display:inline;
padding:1px 7px 1px 7px;
}
.divSmallBox
{
font-size:11px;
}
.scannedin
{
color: #FFFFFF !important;
background: #77d42a !important;
background: -moz-linear-gradient(top, #77d42a 0%, #268a16 98%, #77d42a 99%, #268a16 100%) !important;
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#77d42a), color-stop(98%,#268a16), color-stop(99%,#77d42a), color-stop(100%,#268a16))!important;
background: -webkit-linear-gradient(top, #77d42a 0%,#268a16 98%,#77d42a 99%,#268a16 100%) !important;
background: -o-linear-gradient(top, #77d42a 0%,#268a16 98%,#77d42a 99%,#268a16 100%) !important;
background: -ms-linear-gradient(top, #77d42a 0%,#268a16 98%,#77d42a 99%,#268a16 100%)!important;
background: linear-gradient(to bottom, #77d42a 0%,#268a16 98%,#77d42a 99%,#268a16 100%) !important;
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#77d42a', endColorstr='#268a16',GradientType=0 ) !important;
}
.scannedout{
color: #FFFFFF !important;
background: #b29af8 !important;
background: -moz-linear-gradient(top, #b29af8 0%, #9174ed 98%, #b29af8 99%, #9174ed 100%) !important;
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b29af8), color-stop(98%,#9174ed), color-stop(99%,#b29af8), color-stop(100%,#9174ed)) !important;
background: -webkit-linear-gradient(top, #b29af8 0%,#9174ed 98%,#b29af8 99%,#9174ed 100%)!important;
background: -o-linear-gradient(top, #b29af8 0%,#9174ed 98%,#b29af8 99%,#9174ed 100%) !important;
background: -ms-linear-gradient(top, #b29af8 0%,#9174ed 98%,#b29af8 99%,#9174ed 100%) !important;
background: linear-gradient(to bottom, #b29af8 0%,#9174ed 98%,#b29af8 99%,#9174ed 100%) !important;
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b29af8', endColorstr='#9174ed',GradientType=0 )!important;
}
.notscanned{
color: #FFFFFF !important;
background: #f62b2b !important;
background: -moz-linear-gradient(top, #f62b2b 0%, #d20202 98%, #f62b2b 99%, #d20202 100%) !important;
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f62b2b), color-stop(98%,#d20202), color-stop(99%,#f62b2b), color-stop(100%,#d20202)) !important;
background: -webkit-linear-gradient(top, #f62b2b 0%,#d20202 98%,#f62b2b 99%,#d20202 100%) !important;
background: -o-linear-gradient(top, #f62b2b 0%,#d20202 98%,#f62b2b 99%,#d20202 100%) !important;
background: -ms-linear-gradient(top, #f62b2b 0%,#d20202 98%,#f62b2b 99%,#d20202 100%) !important;
background: linear-gradient(to bottom, #f62b2b 0%,#d20202 98%,#f62b2b 99%,#d20202 100%) !important;
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f62b2b', endColorstr='#d20202',GradientType=0 ) !important;
}
.owntransport{
color: #FFFFFF !important;
background: #F68933 !important;
background: -moz-linear-gradient(top, #F68933 0%, #F8AD72 98%, #F68933 99%, #F8AD72 100%) !important;
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F68933), color-stop(98%,#F8AD72), color-stop(99%,#F68933), color-stop(100%,#F8AD72)) !important;
background: -webkit-linear-gradient(top, #F68933 0%,#F8AD72 98%,#F68933 99%,#F8AD72 100%) !important;
background: -o-linear-gradient(top, #F68933 0%,#F8AD72 98%,#F68933 99%,#F8AD72 100%) !important;
background: -ms-linear-gradient(top, #F68933 0%,#F8AD72 98%,#F68933 99%,#F8AD72 100%) !important;
background: linear-gradient(to bottom, #F68933 0%,#F8AD72 98%,#F68933 99%,#F8AD72 100%) !important;
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#F68933', endColorstr='#F8AD72',GradientType=0 ) !important;
}
.latepass{
color: #FFFFFF !important;
background: #EA368E !important;
background: -moz-linear-gradient(top, #EA368E 0%, #F38DBF 98%, #EA368E 99%, #F38DBF 100%) !important;
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#EA368E), color-stop(98%,#F38DBF), color-stop(99%,#EA368E), color-stop(100%,#F38DBF)) !important;
background: -webkit-linear-gradient(top, #EA368E 0%,#F38DBF 98%,#EA368E 99%,#F38DBF 100%) !important;
background: -o-linear-gradient(top, #EA368E 0%,#F38DBF 98%,#EA368E 99%,#F38DBF 100%) !important;
background: -ms-linear-gradient(top, #EA368E 0%,#F38DBF 98%,#EA368E 99%,#F38DBF 100%) !important;
background: linear-gradient(to bottom, #EA368E 0%,#F38DBF 98%,#EA368E 99%,#F38DBF 100%) !important;
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#EA368E', endColorstr='#F38DBF',GradientType=0 ) !important;
}
    </style>
    <script language="javascript" type="text/javascript">


        function checkDate(sender, args) {

            var currenttime = document.getElementById("<%=hfDate.ClientID %>").value;
            var Freezedate = document.getElementById("<%=hfFreezeDate.ClientID %>").value;
            var vGrades = document.getElementById("<%=hfGrades.ClientID %>").value;
            var vGRD = document.getElementById("<%=ddlGrade.ClientID %>").value;
            var DisplayDt = document.getElementById("<%=hfDisplay.ClientID %>").value;
            if (sender._selectedDate > new Date(currenttime)) {
                alert("You cannot select a day greater than today!");
                sender._selectedDate = new Date(currenttime);
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
            else if (vGrades.indexOf(vGRD) != -1) {
                if (sender._selectedDate <= new Date(Freezedate)) {
                    alert("Attendance is closed for this period and therefore cannot be modified upto " + DisplayDt);
                    sender._selectedDate = new Date(currenttime);
                    // set the date back to the current date
                    sender._textbox.set_Value(sender._selectedDate.format(sender._format))
                }

            }
        }


       <%--    function getDate(mode) 
         {     
                  var sFeatures;
                  sFeatures="dialogWidth: 229px; ";
                  sFeatures+="dialogHeight: 234px; ";
                  sFeatures+="help: no; ";
                  sFeatures+="resizable: no; ";
                  sFeatures+="scroll: yes; ";
                  sFeatures+="status: no; ";
                  sFeatures+="unadorned: no; ";
                  var NameandCode;
                  var result;
                  if (mode==1)
                      result = window.showModalDialog("../accounts/calendar.aspx?dt="+document.getElementById('<%=txtDate.ClientID %>').value,"", sFeatures) 
        if (result=='' || result==undefined)
        {
            //            document.getElementById("txtDate").value=''; 
            return false;
        }
        if (mode==1)
            document.getElementById('<%=txtDate.ClientID %>').value=result; 
           return true;
           } 
  --%>

        function hideButton() {
            document.forms[0].submit();
            window.setTimeout("hideButton('" + window.event.srcElement.id + "')", 0);
        }

        function hideButton(buttonID) {
            document.getElementById(buttonID).style.display = 'none';
            document.getElementById('<%=btnSave.ClientID %>').style.display = 'none';
               document.getElementById('<%=btnSave2.ClientID %>').style.display = 'none';
           }

    </script>
    <!--    
<script type="text/javascript">
  function validate_Late()
    {
           for(i=0;i<chkLate.length;i++)
            {
           if(document.getElementById(chkLate[i]).checked ==true)
                {  
                   document.getElementById(chkPresent[i]).checked=true;
             }
           }
                  }
    
 function validate_Present()
    {
           for(i=0;i<chkPresent.length;i++)
            {
           if(document.getElementById(chkPresent[i]).checked ==false)
                {  
                   document.getElementById(chkLate[i]).checked=false;
             }
           }
                  }

   </script>
-->
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Class Wise Attendance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%--   <table id="Table1" width="100%" border="0">
        <tbody>
            <tr >
                <td class="title" align="left" width="50%">
                    <asp:Literal ID="ltHeader" runat="server" Text="CLASS WISE ATTENDANCE"></asp:Literal></td>
            </tr>
        </tbody>
    </table>--%>

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>

                            </div>

                        </td>
                    </tr>
                    <tr>
                        <%-- <td align="center" class="matters" valign="middle">&nbsp;Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory</td>--%>
                        <td align="center" class="text-danger font-small" valign="middle">Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="title-bg-lite" colspan="4">

                                        <asp:Literal ID="ltLabel" runat="server" Text="Student Attendance"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                   <td align="left" class="matters" width="20%"><span class="field-label" >Stream</span></td>
                                     <td align="left" class="matters" width="30%">  <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                  
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Section</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Attendance Date</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAttDate" runat="server" OnSelectedIndexChanged="ddlAttDate_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList><asp:TextBox ID="txtDate" runat="server" Width="90px" AutoPostBack="True" OnTextChanged="txtDate_TextChanged"></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                CssClass="error" Display="Dynamic" ErrorMessage="Attendance Date required" ForeColor=""
                                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                    <td align="left"><span class="field-label">Attendance Type</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAttType" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <br />
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False"
                                CssClass="button" Text="Add" ValidationGroup="AttGroup" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                CssClass="button" Text="Edit" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave2"
                                runat="server" CssClass="button" Text="Save"
                                ValidationGroup="groupM1" />
                            <asp:Button
                                ID="btnCancel2" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" /></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Width="100%" DataKeyNames="SRNO" OnRowCreated="gvInfo_RowCreated">
                        <%--        <RowStyle CssClass="griditem" />--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsNo" runat="server" Text='<%# Bind("SRNO") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StudentID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID1" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStud_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            &nbsp;&nbsp;
                                <asp:LinkButton ID="lbtnStudName" OnClick="lbtnStudName_Click" runat="server" Text='<%# Bind("STUDNAME") %>'></asp:LinkButton>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transport Status">
                                        <ItemTemplate>
                                            <asp:Label ID="divTranStatus" runat="server" CssClass='<%# Bind("TRANSSTATUS_CSS")%>' Text='<%# Bind("TRANSSTATUS_CODE")%>'></asp:Label>
                                            <asp:Label ID="lblTranStatus" runat="server" Text='<%# Bind("TRANSSTATUS")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Remarks">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:TextBox ID="txtRemarks" runat="server" Text='<%# bind("REMARKS") %>' MaxLength="255"></asp:TextBox>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALG_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblALG_ID" runat="server" Text='<%# bind("ALG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GENDER" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGENDER" runat="server" Text='<%# Bind("SGENDER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPLEAVE" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAPPLEAVE" runat="server" Text='<%# Bind("APPLEAVE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MINLIST" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMinList" runat="server" Text='<%# Bind("minList") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SLA_APD_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSLA_APD_ID" runat="server" Text='<%# Bind("SLA_APD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DAY1" HeaderText="DAY1"></asp:BoundField>
                                    <asp:BoundField DataField="DAY2" HeaderText="DAY2"></asp:BoundField>
                                    <asp:BoundField DataField="DAY3" HeaderText="DAY3"></asp:BoundField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                               <%-- <AlternatingRowStyle CssClass="griditem_alternative" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel1" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <div id="pldis" runat="server" style="display: none; overflow: visible;">
                                <div class="title-bg-lite" style="width: 100%; margin-top: 1px; vertical-align: middle; ">
                                    <div>
                                        
                                            <asp:Label ID="lblStudName" runat="server"></asp:Label>
                                        <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;">
                                            <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="../Images/closeme.png" Style="margin-top: -1px; vertical-align: top;" /></span>
                                    </div>
                                </div>
                                <asp:Panel ID="plStudAtt" runat="server" Width="100%" Height="450px" CssClass="panel-cover" ScrollBars="Horizontal">
                                    <table width="100%" align="center">
                                        <tr>
                                            <td align="center" colspan="3"> <span class="field-label">Current Attendance Statics till date( 
                        <asp:Label ID="lblDate" runat="server"></asp:Label>)</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Grade &amp; Section</span></td>
                                            <td>
                                                <asp:Label ID="lblGradeSect" runat="server"></asp:Label>
                                            </td>
                                            <td rowspan="7">
                                                <asp:Image ID="imgStud" runat="server" Height="186px" Width="180px"></asp:Image></td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Session</span></td>
                                            <td>
                                                <asp:Label ID="lblSession" runat="server"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Total Working Days For The Academic Year</span></td>
                                            <td>
                                                <asp:Label ID="lblTotWorking" runat="server"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Total Working Days Till Date</span></td>
                                            <td>
                                                <asp:Label ID="lblTillDateWorking" runat="server"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Total Attendance Marked till Date</span></td>
                                            <td>
                                                <asp:Label ID="lblTotalAttMarked" runat="server"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Days Present</span></td>
                                            <td>
                                                <asp:Label ID="lblDayPres" runat="server"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="left"> <span class="field-label">Days Absent</span></td>
                                            <td>
                                                <asp:Label ID="lblDayAbsent" runat="server">2</asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" valign="bottom">
                                                <asp:Literal ID="FCLiteral" runat="server"></asp:Literal></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender
                                ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
                                BackgroundCssClass="modalBackground" DropShadow="true" RepositionMode="RepositionOnWindowResizeAndScroll" />





                        </td>
                    </tr>
                    <tr id="trStatus" runat="server">
                        <td>
                            <div style="display: block;">
                                <div class="divSmallBox">
                                    <font color="blue">Transport Status color Code</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox_new scannedin">IN</div>
                                    &nbsp;Scanned IN
                    &nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox_new scannedout">OUT</div>
                                    &nbsp;Scanned OUT
                    &nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox_new notscanned">NO SCAN</div>
                                    &nbsp;Not Scanned
                    &nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox_new owntransport">OT</div>
                                    &nbsp;Own transport
                    &nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox_new latepass">LATE</div>
                                    &nbsp;Latepass
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfDay1" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay2" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay3" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay4" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay5" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtDate" OnClientDateSelectionChanged="checkDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hfdate2" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfFlag" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfFreezeDate" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGrades" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hfDisplay" runat="server"></asp:HiddenField>


            </div>
        </div>
    </div>

</asp:Content>

