<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studScreen_D.aspx.vb" Inherits="Students_studScreen_D" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Enquiry Validation Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
    cellspacing="0" width="100%" >
    
    <tr>
    <td align="left">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
     HeaderText="You must enter a value in the following fields:"
    ValidationGroup="groupM1" />
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <asp:CompareValidator ID="cvMarks" runat="server" ErrorMessage="Max marks should be greater than min marks" ValidationGroup="groupM1" ControlToCompare="txtMinMarks" ControlToValidate="txtMaxMarks" ForeColor="Transparent" Height="1px" Operator="GreaterThan" Width="46px" Type="Integer" CssClass="error" Display="None"></asp:CompareValidator></td>
    </tr>
    
    <tr><td align="center">
    <table id="Table1" runat="server" align="center" cellpadding="0"
    cellspacing="0" width="100%">
    <tr>
    <td align="center" colspan="8">
    Fields Marked<span class="text-danger"> </span>with(<span
    class="text-danger">*</span>)are mandatory</td>
    </tr>

    <tr>
    <td align="left" class="title-bg" colspan="8">    
    Screening Set Up</td></tr>
    <tr>
    <td align="left"  >
    <asp:Label ID="lblAccText" runat="server" Text="Academic Year " CssClass="field-label"></asp:Label>
    </td>
    
    <td  align="left" >
    <asp:Label ID="lblacademicYear" runat="server" CssClass="field-value"></asp:Label></td>
    <td align="left" >
    <asp:Label ID="lblgradeText" runat="server" Text="Grade "  CssClass="field-label"></asp:Label>
    </td>
    
    <td align="left">
    <asp:Label ID="lblGrade" runat="server" Text="" CssClass="field-value"></asp:Label></td>
    </tr>
    
    <tr>
    <td align="left">
    <asp:Label ID="lblStreamText" runat="server" Text="Stream"  CssClass="field-label"></asp:Label>
    </td>
    
    <td align="left">
    <asp:Label ID="lblStream" runat="server" Text="" CssClass="field-value"></asp:Label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>

    <tr class="subheader_img">
    <td align="left" colspan="8" class="title-bg">
    Details</td>
    </tr>
    <tr>
    <td align="left">
    <asp:Label ID="lblSubject" runat="server" Text="Subject " CssClass="field-label"></asp:Label>
    </td>
    
    <td align="left">
    <asp:DropDownList ID="ddlSubject" runat="server">
    </asp:DropDownList>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    
    <tr>
     <td >
    <span class="field-label"> Max Marks</span> 
     <span class="text-danger">*</span></td>
    
    <td align="left">
    <asp:TextBox ID="txtMaxMarks" runat="server" CausesValidation="True" ValidationGroup="groupM1" Width="80px" TabIndex="2"></asp:TextBox></td>
       <td align="left">
    <asp:Label ID="lblMinMarks" runat="server" Text="Min Marks " CssClass="field-label"></asp:Label><span
    class="text-danger">*</span></td>
    
    <td align="left">
    <asp:TextBox ID="txtMinMarks" runat="server" CausesValidation="True" ValidationGroup="groupM1" TabIndex="1"></asp:TextBox></td>
     <td align="left" >
    <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3" /></td>


       </tr>
    
    
    <tr><td align="center" colspan="8"  >
    <table id="Table5" runat="server" width="100%" align="center" border="0" cellpadding="0"
    cellspacing="0"  >
    <tr>
    <td align="center" colspan="8" >
    
    
        <asp:GridView ID="gvStudScreen" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
        HeaderStyle-Height="30" PageSize="20" Width="100%"  >
        
<Columns>
            <asp:TemplateField HeaderText="Subject">
            <ItemTemplate>
            <asp:Label ID="lblSubject" runat="server" text='<%# Bind("Subject") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Max Marks" >
            <ItemTemplate>
            <asp:Label ID="lblMax" runat="server" text='<%# Bind("MaxMarks") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Min Marks" >
            <ItemTemplate>
            <asp:Label ID="lblMin" runat="server" text='<%# Bind("MinMarks") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
         
            <asp:ButtonField CommandName="Edit" HeaderText="Edit" Text="Edit"  >
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:ButtonField>
            
            <asp:ButtonField CommandName="Delete" HeaderText="Delete" Text="Delete">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:ButtonField>
</Columns>
<HeaderStyle CssClass="gridheader_pop" />
<RowStyle CssClass="griditem" />
<SelectedRowStyle CssClass="Green" />
<AlternatingRowStyle CssClass="griditem_alternative" />
</asp:GridView>

</td>
</tr>
</table>
</td></tr>
            <tr>
            <td colspan="8" align="center">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
            Text="Add" />
            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
            Text="Edit" />
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
            Text="Cancel" UseSubmitBehavior="False" />
            </td>
            </tr>
</table>
</td></tr>


</table>
<asp:HiddenField ID="hfACD_ID" runat="server" /><asp:HiddenField ID="hfGRD_ID" runat="server" />
<asp:HiddenField ID="hfSTM_ID" runat="server" />

    </div>
    </div>
    </div>

</asp:Content>

