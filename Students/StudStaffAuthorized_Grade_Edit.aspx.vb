Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudStaffAuthorized_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059005" And ViewState("MainMnu_code") <> "S059010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    ltLabel.Text = "Register Permission"

                    bindAcademic_Year()

                   
                    Call getStart_EndDate()
                    bindAcademic_STAFF()




                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call Authorized_Staff(ViewState("viewid"))

                        DisEnable_cont()

                        ' ddlAcdYear.BackColor = Drawing.Color.Black
                        Call populateGrade_Edit()




                        If gvInfo.Rows.Count > 0 Then
                            For i As Integer = 0 To gvInfo.Rows.Count - 1
                                Dim row As GridViewRow = gvInfo.Rows(i)
                                Dim SAD_ID As String = DirectCast(row.FindControl("lblSAD_ID"), Label).Text
                                If SAD_ID = ViewState("viewid") Then
                                    i = i + 1
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "addScript", "<script language=javascript>onGridViewRowSelected('" + i.ToString() + "');</script>")
                                End If
                            Next



                        End If
                    ElseIf ViewState("datamode") = "add" Then


                        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtTo.Text = ViewState("ACD_ENDDT")

                        populateGrade_Edit()
                       
                       
                    End If

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub


    'Sub bindAtt_Type()
    '    Try
    '        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '        Dim GRD_ID As String = ddlGrade.SelectedItem.Value
    '        Dim CheckDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
    '        Dim ATT_Type As String
    '        'Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type(ACD_ID, GRD_ID, CheckDate)
    '        Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type(ACD_ID, CheckDate)
    '            While readerSTUD_ATT_ADD.Read
    '                ATT_Type = Convert.ToString(readerSTUD_ATT_ADD("ATT_TYPE"))
    '            End While
    '        End Using

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub


    Sub Authorized_Staff(ByVal SAD_ID As String)

        Try
            Using readerSTAFF_AUTHORIZED As SqlDataReader = AccessStudentClass.GetSTAFF_AUTHORIZED(ViewState("viewid"))
                If readerSTAFF_AUTHORIZED.HasRows = True Then
                    While readerSTAFF_AUTHORIZED.Read

                        ViewState("SAD_ACD_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAD_ACD_ID"))
                        ViewState("SAD_GRD_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAD_GRD_ID"))
                        ViewState("SAD_SCT_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAD_SCT_ID"))
                        ViewState("SAD_STM_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAD_STM_ID"))
                        ViewState("SAD_SHF_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAD_SHF_ID"))
                        ViewState("SAD_EMP_ID") = Convert.ToString(readerSTAFF_AUTHORIZED("SAD_EMP_ID"))

                        'Setting date



                        If IsDate(readerSTAFF_AUTHORIZED("SAD_FROMDT")) = True Then
                            txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerSTAFF_AUTHORIZED("SAD_FROMDT"))))
                        End If
                        If IsDate(readerSTAFF_AUTHORIZED("SAD_TODT")) = True Then
                            txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerSTAFF_AUTHORIZED("SAD_TODT"))))
                        End If

                    End While


                    
                    For ItemTypeCounter As Integer = 0 To ddlAcdYear.Items.Count - 1
                        'keep loop until you get the School to Not Available into  the SelectedIndex
                        If ddlAcdYear.Items(ItemTypeCounter).Value = Trim(ViewState("SAD_ACD_ID")) Then
                            ddlAcdYear.SelectedIndex = ItemTypeCounter
                            ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
                        End If
                    Next
                    For ItemTypeCounter As Integer = 0 To ddlGrade.Items.Count - 1
                        'keep loop until you get the School to Not Available into  the SelectedIndex
                        If ddlGrade.Items(ItemTypeCounter).Value = Trim(ViewState("SAD_GRD_ID")) Then
                            ddlGrade.SelectedIndex = ItemTypeCounter
                            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                        End If

                    Next

                    For ItemTypeCounter As Integer = 0 To ddlAuthSection.Items.Count - 1
                        'keep loop until you get the School to Not Available into  the SelectedIndex
                        If ddlAuthSection.Items(ItemTypeCounter).Value = Trim(ViewState("SAD_SCT_ID")) Then
                            ddlAuthSection.SelectedIndex = ItemTypeCounter
                            ddlAuthSection_SelectedIndexChanged(ddlAuthSection, Nothing)
                        End If
                    Next

                    For ItemTypeCounter As Integer = 0 To ddlStream.Items.Count - 1
                        'keep loop until you get the School to Not Available into  the SelectedIndex
                        If ddlStream.Items(ItemTypeCounter).Value = Trim(ViewState("SAD_STM_ID")) Then
                            ddlStream.SelectedIndex = ItemTypeCounter
                            ddlStream_SelectedIndexChanged(ddlStream, Nothing)
                        End If
                    Next
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the School to Not Available into  the SelectedIndex
                        If ddlShift.Items(ItemTypeCounter).Value = Trim(ViewState("SAD_SHF_ID")) Then
                            ddlShift.SelectedIndex = ItemTypeCounter
                        End If
                    Next

                    
                   
                    
                    For ItemTypeCounter As Integer = 0 To ddlAuthStaff.Items.Count - 1
                        'keep loop until you get the School to Not Available into  the SelectedIndex
                        If ddlAuthStaff.Items(ItemTypeCounter).Value = Trim(ViewState("SAD_EMP_ID")) Then
                            ddlAuthStaff.SelectedIndex = ItemTypeCounter
                        End If
                    Next

                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub populateGrade_Edit()

        Using Connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String
            Dim GRD_ID As String 
            Dim STM_ID As String
            Dim SHF_ID As String
            Dim SCT_ID As String

            If ddlAcdYear.SelectedIndex <> -1 Then
                ACD_ID = ddlAcdYear.SelectedItem.Value
            Else
                ACD_ID = 0
            End If
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else

                GRD_ID = 0
            End If
            If ddlStream.SelectedIndex <> -1 Then
                STM_ID = ddlStream.SelectedItem.Value
            Else
                STM_ID = 0
            End If
           
            If ddlShift.SelectedIndex <> -1 Then
                SHF_ID = ddlShift.SelectedItem.Value
            Else
                SHF_ID = 0
            End If
            If ddlAuthSection.SelectedIndex <> -1 Then
                SCT_ID = ddlAuthSection.SelectedItem.Value
            Else
                SCT_ID = 0
            End If

            Dim ds As DataSet



            Dim sqlstring As String

            If SCT_ID = "0" Then
                sqlstring = " SELECT   SAD_ID, ISNULL(EMPLOYEE_M.EMP_FNAME, '') + ' ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '')+ ' ' + ISNULL(EMPLOYEE_M.EMP_LNAME, '') AS EMP_Name " & _
  " ,STAFF_AUTHORIZED_ATT.SAD_FROMDT as FROMDT, STAFF_AUTHORIZED_ATT.SAD_TODT as TODT,SCT_DESCR AS section FROM  STAFF_AUTHORIZED_ATT INNER JOIN " & _
 " EMPLOYEE_M ON STAFF_AUTHORIZED_ATT.SAD_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN SECTION_M ON SECTION_M.SCT_ID=STAFF_AUTHORIZED_ATT.SAD_SCT_ID WHERE (STAFF_AUTHORIZED_ATT.SAD_ACD_ID = '" & ACD_ID & "') " & _
" AND (STAFF_AUTHORIZED_ATT.SAD_GRD_ID = '" & GRD_ID & "') AND " & _
" (STAFF_AUTHORIZED_ATT.SAD_STM_ID = '" & STM_ID & "') AND (STAFF_AUTHORIZED_ATT.SAD_SHF_ID = '" & SHF_ID & "')"
            Else
                sqlstring = " SELECT   SAD_ID, ISNULL(EMPLOYEE_M.EMP_FNAME, '') + ' ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '')+ ' ' + ISNULL(EMPLOYEE_M.EMP_LNAME, '') AS EMP_Name " & _
                  " ,STAFF_AUTHORIZED_ATT.SAD_FROMDT as FROMDT, STAFF_AUTHORIZED_ATT.SAD_TODT as TODT,SCT_DESCR AS section FROM  STAFF_AUTHORIZED_ATT INNER JOIN " & _
                 " EMPLOYEE_M ON STAFF_AUTHORIZED_ATT.SAD_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN SECTION_M ON SECTION_M.SCT_ID=STAFF_AUTHORIZED_ATT.SAD_SCT_ID WHERE (STAFF_AUTHORIZED_ATT.SAD_ACD_ID = '" & ACD_ID & "') " & _
               " AND (STAFF_AUTHORIZED_ATT.SAD_GRD_ID = '" & GRD_ID & "') AND  (STAFF_AUTHORIZED_ATT.SAD_SCT_ID = '" & SCT_ID & "') AND " & _
               " (STAFF_AUTHORIZED_ATT.SAD_STM_ID = '" & STM_ID & "') AND (STAFF_AUTHORIZED_ATT.SAD_SHF_ID = '" & SHF_ID & "')"
            End If



            ds = SqlHelper.ExecuteDataset(Connection, CommandType.Text, sqlstring)


            If ds.Tables(0).Rows.Count > 0 Then

                gvInfo.DataSource = ds.Tables(0)
                gvInfo.DataBind()



            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not


                gvInfo.DataSource = ds.Tables(0)
                Try
                    gvInfo.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvInfo.Rows(0).Cells.Clear()
                gvInfo.Rows(0).Cells.Add(New TableCell)
                gvInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvInfo.Rows(0).Cells(0).Text = "No Details Added Yet."
            End If

        End Using

    End Sub

    'Public Function GetCategoryDescriptions() As DataSet
    '    Dim myConnection As New SqlConnection(ConnectionString)
    '    Dim ad As New SqlDataAdapter("SELECT Description FROM Categories", myConnection)
    '    Dim ds As New DataSet()
    '    ad.Fill(ds, "Categories")
    '    Return ds
    'End Function
    Public Function callAuthorised_Emp() As DataSet

        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim BSU_ID As String = Session("sBsuid")
        Dim str_Sql As String = " SELECT  ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_ID " & _
       " FROM EMPLOYEE_M  where EMP_bACTIVE=1 and EMP_BSU_ID='" & BSU_ID & "' and EMP_DES_ID in(select ATC_DES_ID from AUTHORIZED_CATEGORY where ATC_BSU_ID='" & BSU_ID & "')  order by EMP_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Return ds

    End Function

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    '   Sub bindAcademic_Grade()
    '       Try
    '           Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '           Dim str_Sql As String
    '           Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '           Dim ds As New DataSet
    '           str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
    '" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

    '           ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '           ddlGrade.Items.Clear()
    '           ddlGrade.DataSource = ds.Tables(0)
    '           ddlGrade.DataTextField = "GRM_DISPLAY"
    '           ddlGrade.DataValueField = "GRD_ID"
    '           ddlGrade.DataBind()
    '           ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
    '       Catch ex As Exception
    '           UtilityObj.Errorlog(ex.Message)
    '       End Try
    '   End Sub
    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim GRM_ID_Select = "SELECT grm_id FROM grade_bsu_m,stream_m WHERE" _
                                     & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcdYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue + "' and STM_ID= '" + ddlStream.SelectedValue + "'"
            Dim str_Sql As String = " SELECT DISTINCT SECTION_M.SCT_ID as SCT_ID, SECTION_M.SCT_DESCR as SCT_DESCR FROM   GRADE_BSU_M INNER JOIN " &
 " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " &
" WHERE     (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "')" &
              " and  [SCT_GRM_ID] in (" + GRM_ID_Select + ") ORDER BY SCT_DESCR "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAuthSection.Items.Clear()
            ddlAuthSection.DataSource = ds.Tables(0)
            ddlAuthSection.DataTextField = "SCT_DESCR"
            ddlAuthSection.DataValueField = "SCT_ID"
            ddlAuthSection.DataBind()
            If ViewState("datamode") = "add" Then
                ddlAuthSection.Items.Insert(0, New ListItem("All", "0"))
            End If

            ddlAuthSection_SelectedIndexChanged(ddlAuthSection, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub bindAcademic_SHIFT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim SCT_ID As String = ddlAuthSection.SelectedItem.Value
            Dim STM_ID As String = ddlStream.SelectedItem.Value
            Dim ds As New DataSet
            If SCT_ID = "0" Then
                str_Sql = " SELECT DISTINCT SHIFTS_M.SHF_DESCR AS SHF_DESCR, SHIFTS_M.SHF_ID AS SHF_ID " & _
" FROM  GRADE_BSU_M INNER JOIN  SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID  " & _
" WHERE   (GRADE_BSU_M.GRM_STM_ID='" & STM_ID & "') AND   (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') " & _
" ORDER BY SHIFTS_M.SHF_DESCR"
            Else
                str_Sql = " SELECT DISTINCT SHIFTS_M.SHF_DESCR AS SHF_DESCR, SHIFTS_M.SHF_ID AS SHF_ID " & _
               " FROM  GRADE_BSU_M INNER JOIN  SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID INNER JOIN " & _
               " SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID " & _
               " WHERE   (GRADE_BSU_M.GRM_STM_ID='" & STM_ID & "') AND   (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') AND (SECTION_M.SCT_ID='" & SCT_ID & "') " & _
               " ORDER BY SHIFTS_M.SHF_DESCR"
            End If





            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlShift.Items.Clear()
            ddlShift.DataSource = ds.Tables(0)
            ddlShift.DataTextField = "SHF_DESCR"
            ddlShift.DataValueField = "SHF_ID"
            ddlShift.DataBind()



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    '    Sub bindAcademic_STREAM()
    '        Try
    '            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '            Dim str_Sql As String
    '            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
    '            Dim SCT_ID As String = ddlAuthSection.SelectedItem.Value
    '            Dim ds As New DataSet
    '            If SCT_ID = "0" Then
    '                str_Sql = " SELECT DISTINCT STREAM_M.STM_DESCR, STREAM_M.STM_ID " & _
    '" FROM  GRADE_BSU_M INNER JOIN   STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID  " & _
    '" where  GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') " & _
    '" order by STREAM_M.STM_DESCR "
    '            Else
    '                str_Sql = " SELECT DISTINCT STREAM_M.STM_DESCR, STREAM_M.STM_ID, SECTION_M.SCT_ID, SECTION_M.SCT_GRM_ID " & _
    '               " FROM  GRADE_BSU_M INNER JOIN   STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID INNER JOIN " & _
    '               " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
    '               " where  GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') AND (SECTION_M.SCT_ID='" & SCT_ID & "')" & _
    '               " order by STREAM_M.STM_DESCR "
    '            End If


    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '            ddlStream.Items.Clear()
    '            ddlStream.DataSource = ds.Tables(0)
    '            ddlStream.DataTextField = "STM_DESCR"
    '            ddlStream.DataValueField = "STM_ID"
    '            ddlStream.DataBind()

    '            ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    '        Catch ex As Exception
    '            UtilityObj.Errorlog(ex.Message)
    '        End Try
    '    End Sub

    Sub bindAcademic_STAFF()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim str_Sql As String = " SELECT  ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_ID " & _
           " FROM EMPLOYEE_M  where EMP_STATUS NOT IN(4,8,7,5) AND EMP_bACTIVE=1 and EMP_BSU_ID='" & BSU_ID & "' and EMP_DES_ID in(select ATC_DES_ID from AUTHORIZED_CATEGORY where ATC_BSU_ID='" & BSU_ID & "')  order by EMP_NAME"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAuthStaff.Items.Clear()
            ddlAuthStaff.DataSource = ds.Tables(0)
            ddlAuthStaff.DataTextField = "EMP_NAME"
            ddlAuthStaff.DataValueField = "EMP_ID"
            ddlAuthStaff.DataBind()
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub BindGrade()
        Try
            ddlGrade.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            'ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call callStream_Bind()
        Call getStart_EndDate()
        'Call populateGrade_Edit()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'If ViewState("datamode") = "view" Then
        bindAcademic_Section()
        'populateGrade_Edit()
        'End If
    End Sub
    Protected Sub ddlAuthSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'bindAcademic_STREAM()
        bindAcademic_SHIFT()
        populateGrade_Edit()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        BindGrade()
        'bindAcademic_SHIFT()
        'populateGrade_Edit()
        ' End If
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'If ViewState("datamode") = "view" Then
        ' bindAcademic_Section()
        Call populateGrade_Edit()
        ' End If
    End Sub



    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("viewid") = 0 Then
            lblError.Text = "No records to delete"
            Exit Sub
        End If
        Dim str_err As String = String.Empty
        Dim DeleteMessage As String = String.Empty
        str_err = callDelete(DeleteMessage)
        If str_err = "0" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Call populateGrade_Edit()
            DisEnable_cont()
            reset_state()
            lblError.Text = "Record Deleted Successfully"

        Else
            lblError.Text = DeleteMessage
        End If
    End Sub
    Function callDelete(ByRef DeleteMessage As String) As Integer
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteSTAFF_AUTHORIZED_ATTENDANCE(ViewState("viewid"), transaction)


                If Status = -1 Then
                    callDelete = "1"
                    DeleteMessage = "Record Does Not Exist "
                    Return "1"

                ElseIf Status <> 0 Then
                    callDelete = "1"
                    DeleteMessage = "Error Occured While Deleting."
                    Return "1"
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        callDelete = "1"
                        DeleteMessage = "Could not complete your request"
                        Return "1"

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("viewid") = 0
                    ' reset_state()
                End If
            Catch ex As Exception
                callDelete = "1"
                DeleteMessage = "Error Occured While Deleting Record."

            Finally
                If callDelete <> "0" Then
                    UtilityObj.Errorlog(DeleteMessage)
                    transaction.Rollback()
                Else
                    DeleteMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Sub reset_state()

        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = ViewState("ACD_ENDDT")
        ' ViewState("datamode") = "none"
        If ddlAcdYear.SelectedIndex <> -1 Then
            ddlAcdYear.SelectedIndex = 0
        End If
        If ddlGrade.SelectedIndex <> -1 Then
            ddlGrade.SelectedIndex = 0
        End If
        If ddlStream.SelectedIndex <> -1 Then
            ddlGrade.SelectedIndex = 0
        End If
        
        If ddlShift.SelectedIndex <> -1 Then
            ddlShift.SelectedIndex = 0
        End If
        If ddlAuthSection.SelectedIndex <> -1 Then
            ddlAuthSection.SelectedIndex = 0
        End If

        If ddlAuthStaff.SelectedIndex <> -1 Then
            ddlAuthStaff.SelectedIndex = 0
        End If


        'populateGrade_Edit()


        ' UnCheck_grid()
    End Sub

    Sub Enable_cont()
        ddlAcdYear.Enabled = True
        ddlStream.Enabled = True
        ddlShift.Enabled = True
        ddlGrade.Enabled = True

        txtFrom.Enabled = True
        txtTo.Enabled = True
        ddlAuthSection.Enabled = True
        ddlAuthStaff.Enabled = True
        imgCalendar.Enabled = True
        ImageButton1.Enabled = True
    End Sub
    Sub DisEnable_cont()
        ddlAcdYear.Enabled = False
        ddlStream.Enabled = False
        ddlShift.Enabled = False
        ddlGrade.Enabled = False

        txtFrom.Enabled = False
        txtTo.Enabled = False
        ddlAuthSection.Enabled = False
        ddlAuthStaff.Enabled = False
        imgCalendar.Enabled = False
        ImageButton1.Enabled = False
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                ' gvInfo.Visible = True
                ' plEdit.Visible = False
                Call reset_state()
                DisEnable_cont()
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then
            If serverDateValidate() = "0" Then
                If validateDateRange() = "0" Then

                    If ddlAcdYear.SelectedIndex = -1 Then
                        lblError.Text = "Academic Year not selected"

                    ElseIf ddlGrade.SelectedIndex = -1 Then
                        lblError.Text = "Grade not selected"
                    ElseIf ddlAuthSection.SelectedIndex = -1 Then
                        lblError.Text = "Section not selected"
                    ElseIf ddlStream.SelectedIndex = -1 Then
                        lblError.Text = "Stream not selected"
                    ElseIf ddlShift.SelectedIndex = -1 Then
                        lblError.Text = "Shift not selected"
                    ElseIf ddlAuthStaff.SelectedIndex = -1 Then
                        lblError.Text = "Authorized Staff not selected"
                    Else

                        str_err = calltransaction(errorMessage)
                        If str_err = "0" Then


                            DisEnable_cont()
                            populateGrade_Edit()
                            lblError.Text = "Record Saved Successfully"
                        Else
                            lblError.Text = errorMessage
                        End If
                    End If
                End If
                End If
            End If

    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim SAD_ID As String = "0"
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim GRD_ID As String = ddlGrade.SelectedItem.Value

        Dim STM_ID As String = ddlStream.SelectedItem.Value
        Dim SHF_ID As String = ddlShift.SelectedItem.Value
        Dim BSU_ID As String = Session("sBsuid")
        Dim AttType As String = ""
        Dim FromDT As String = txtFrom.Text
        Dim ToDT As String = txtTo.Text
        Dim SCT_ID As String = ddlAuthSection.SelectedItem.Value
        Dim Emp_ID As String = ddlAuthStaff.SelectedItem.Value

        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    'For i As Integer = 0 To gvInfo.Rows.Count - 1
                    Dim status As Integer
                    ' Dim row As GridViewRow = gvInfo.Rows(i)

                    ' Dim G_bAVAILABLE As Boolean = DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Checked
                    ' If G_bAVAILABLE Then
                    ' SCT_ID = DirectCast(row.FindControl("lblSCT_ID"), Label).Text
                    ' Emp_ID = DirectCast(row.FindControl("ddlEmpName"), DropDownList).SelectedItem.Value

                    bEdit = False


                    status = AccessStudentClass.SaveSTAFF_AUTHORIZED_ATTENDANCE(SAD_ID, ACD_ID, GRD_ID, SCT_ID, STM_ID, SHF_ID, _
                BSU_ID, Emp_ID, FromDT, ToDT, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If
                    '  End If
                    ' Next
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"
                    ' gvInfo.Visible = True
                    '  Call populateGrade_Edit()
                    'plEdit.Visible = False

                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    SCT_ID = ddlAuthSection.SelectedItem.Value
                    Emp_ID = ddlAuthStaff.SelectedItem.Value
                    SAD_ID = ViewState("viewid")
                    bEdit = True


                    status = AccessStudentClass.SaveSTAFF_AUTHORIZED_ATTENDANCE(SAD_ID, ACD_ID, GRD_ID, SCT_ID, STM_ID, SHF_ID, _
                BSU_ID, Emp_ID, FromDT, ToDT, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If


                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"
                    'gvInfo.Visible = True
                    ' plEdit.Visible = False

                    ViewState("viewid") = 0
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Enable_cont()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = ViewState("ACD_ENDDT")

        populateGrade_Edit()
        Enable_cont()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    'Sub UnCheck_grid()
    '    For i As Integer = 0 To gvInfo.Rows.Count - 1
    '        Dim row As GridViewRow = gvInfo.Rows(i)
    '        DirectCast(row.FindControl("chkG_bAVAILABLE"), CheckBox).Checked = False

    '    Next
    'End Sub

    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If

        End If



        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Function validateDateRange() As String
        Dim CommStr As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text


        If (From_date >= Acd_SDate And From_date <= Acd_EDate) Then
            If txtTo.Text.Trim <> "" Then
                Dim To_Date As Date = txtTo.Text
                If Not ((To_Date >= Acd_SDate And To_Date <= Acd_EDate)) Then
                    CommStr = CommStr & "<div>From Date to To Date must be with in the academic year</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date  must be with in the academic year</div>"
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function

    Sub getStart_EndDate()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then

            ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
            ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
      
        Else
            ViewState("ACD_STARTDT") = ""
            ViewState("ACD_ENDDT") = ""

        End If
    End Sub

    Protected Sub gvInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvInfo.PageIndex = e.NewPageIndex
        Call populateGrade_Edit()
    End Sub

    Protected Sub ddlAttType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call populateGrade_Edit()
    End Sub

    
End Class
