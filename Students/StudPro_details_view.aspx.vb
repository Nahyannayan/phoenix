Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studChangeGradeSection_M
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100464") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    Call bindAcademic_Year()


                    Dim clm As String = ddlCurri.SelectedValue

                    Session("WEB_SER_VAR") = Session("Current_ACY_ID") & "|" & "0" & "|" & Session("sBsuid") & "|" & clm


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlAcdID.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlAcdID.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACY_ID")))
            End While
            reader.Close()
            ddlAcdID.ClearSelection()
            ddlAcdID.Items.FindByValue(Session("Current_ACY_ID")).Selected = True
            ddlAcdID_SelectedIndexChanged(ddlAcdID, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdID.SelectedIndexChanged
         bindCLM()
    End Sub
    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
   
    Sub bindCLM()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim acd_id As String = ddlAcdID.SelectedValue

        str_query = " select clm_descr,clm_id from dbo.CURRICULUM_M where clm_id in " & _
" (select distinct acd_clm_id from academicyear_d where acd_acy_id='" & acd_id & "' and acd_bsu_id='" & Session("sBsuid") & "')"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurri.DataSource = ds
        ddlCurri.DataTextField = "clm_descr"
        ddlCurri.DataValueField = "clm_id"
        ddlCurri.DataBind()

        If ddlCurri.Items.Count > 1 Then
            'ddlCurri.Items.Add(New ListItem("ALL", "0"))
            'ddlCurri.ClearSelection()
            'ddlCurri.Items.FindByText("ALL").Selected = True
            trCLM.Visible = True
        Else
            trCLM.Visible = False
        End If
        ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
    End Sub



    Sub BindGrade()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACY_ID", ddlAcdID.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", ddlCurri.SelectedValue)
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream_By_ACY_ID", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        'Try
        '    ddlSection.Items.Clear()

        '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        '    Dim PARAM(10) As SqlParameter
        '    PARAM(0) = New SqlParameter("@INFO_TYPE", "GRADE")
        '    PARAM(1) = New SqlParameter("@ACY_ID", ddlAcdID.SelectedValue)
        '    PARAM(2) = New SqlParameter("@CLM_ID", ddlCurri.SelectedValue)
        '    PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        '    PARAM(4) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
        '    PARAM(5) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
        '    PARAM(6) = New SqlParameter("@USR_ID", Session("sUsr_id"))

        '    Dim ds As DataSet
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GETFORM_TUTOR_STUDENTS", PARAM)

        '    ddlGrade.DataSource = ds
        '    ddlGrade.DataTextField = "grm_display"
        '    ddlGrade.DataValueField = "grm_grd_id"
        '    ddlGrade.DataBind()

        'Catch ex As Exception

        'End Try

        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACY_ID", ddlAcdID.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", ddlCurri.SelectedValue)
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data_By_ACY_ID", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub

    Sub BindSection()
        Try
            ddlSection.Items.Clear()
       
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(10) As SqlParameter
            PARAM(0) = New SqlParameter("@INFO_TYPE", "SECTION")
            PARAM(1) = New SqlParameter("@ACY_ID", ddlAcdID.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", ddlCurri.SelectedValue)
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
            PARAM(5) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
            PARAM(6) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            PARAM(7) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GETFORM_TUTOR_STUDENTS", PARAM)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()

        Catch ex As Exception

        End Try

        

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Sub GridBind()

        Dim clm As String = ddlCurri.SelectedValue
        Dim acy_id As String = ddlAcdID.SelectedValue

        Dim strQuery As String = String.Empty


        'If ddlCurri.SelectedValue.ToString <> "0" Then
        '    strQuery += " AND STP_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & clm & "') AND (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "'))"
        'Else
        '    strQuery += " AND STP_ACD_ID IN(SELECT DISTINCT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "'))"
        'End If

        If ddlGrade.SelectedValue.ToString <> "0" Then
            strQuery += " AND STP_GRD_ID='" & ddlGrade.SelectedValue.ToString & "'"
        End If

        If ddlSection.SelectedValue.ToString <> "0" Then
            strQuery += " AND STP_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If txtStuNo.Text <> "" Then
            strQuery += " AND STU_NO LIKE '%" + txtStuNo.Text.Trim + "%'"
        End If

        If txtName.Text <> "" Then
            strQuery += " AND SNAME LIKE '%" + txtName.Text.Trim + "%'"
        End If

        If txtPassNo.Text <> "" Then
            strQuery += " AND STU_PASPRTNO LIKE '%" + txtPassNo.Text.Trim + "%'"
        End If

        If txtEmail.Text <> "" Then
            strQuery += " AND ((STS_FEMAIL LIKE '%" + txtEmail.Text.Trim + "%') OR (STS_MEMAIL LIKE '%" + txtEmail.Text.Trim + "%') OR (STS_MEMAIL LIKE '%" + txtEmail.Text.Trim + "%'))"
        End If

        If txtEmgNo.Text <> "" Then
            strQuery += " AND STU_EMGCONTACT LIKE '%" + txtEmgNo.Text.Trim + "%'"
        End If
        If txtMobNo.Text <> "" Then
            strQuery += " AND ((STS_FMOBILE LIKE '%" + txtMobNo.Text.Trim + "%') OR (STS_MMOBILE LIKE '%" + txtMobNo.Text.Trim + "%') OR (STS_GMOBILE LIKE '%" + txtMobNo.Text.Trim + "%'))"
        End If
       



        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(10) As SqlParameter
        PARAM(0) = New SqlParameter("@ACY_ID", ddlAcdID.SelectedValue)
        PARAM(1) = New SqlParameter("@CLM_ID", ddlCurri.SelectedValue)
        PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(3) = New SqlParameter("@FILTER_COND", strQuery)
        PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GETSTU_PROFILE_LIST", PARAM)
        gvStudChange.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudChange.DataBind()
            Dim columnCount As Integer = gvStudChange.Rows(0).Cells.Count
            gvStudChange.Rows(0).Cells.Clear()
            gvStudChange.Rows(0).Cells.Add(New TableCell)
            gvStudChange.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudChange.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudChange.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudChange.DataBind()
        End If




    End Sub

    

   


    Sub HIDE_ALL()
        'tblStud.Rows(4).Visible = False
        'tblStud.Rows(5).Visible = False
        'tblStud.Rows(6).Visible = False
        'tblStud.Rows(7).Visible = False
        'tblStud.Rows(8).Visible = False
        'tblStud.Rows(9).Visible = False
        lblError.Text = ""
        gvStudChange.Visible = False
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        'tblStud.Rows(4).Visible = True
        'tblStud.Rows(5).Visible = True
        'tblStud.Rows(6).Visible = True
        'tblStud.Rows(7).Visible = True
        'tblStud.Rows(8).Visible = True
        'tblStud.Rows(9).Visible = True
        lblError.Text = ""
        GridBind()
        gvStudChange.Visible = True
        'hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        'hfGRD_ID.Value = ddlGrade.SelectedValue
        'hfSTM_ID.Value = ddlStream.SelectedValue.ToString


      
    End Sub


    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindGrade()
        BindSection()

        'Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString

        HIDE_ALL()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        BindSection()

        'Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString

        HIDE_ALL()
    End Sub


    

    Protected Sub gvStudChange_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudChange.PageIndexChanging
        gvStudChange.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    
    
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblSTUD_ID As New Label
         
            lblSTUD_ID = TryCast(sender.FindControl("lblStu_id"), Label)
            Session("DB_Stu_ID") = lblSTUD_ID.Text

            'Response.Write("<Script> window.open('StudPro_details.aspx') </Script>")
            'url = String.Format("~\Students\StudRecordEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            'Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try




    End Sub

    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        callStream_Bind()
    End Sub

  
End Class
