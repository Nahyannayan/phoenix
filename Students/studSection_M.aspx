<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" CodeFile="studSection_M.aspx.vb" Inherits="Students_studSection_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Section Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfSection" runat="server" ErrorMessage="Please enter the field Section" ControlToValidate="txtSection" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfCap" runat="server" ControlToValidate="txtMaxStrength"
                                Display="None" ErrorMessage="Please enter the field max strength" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regCap" runat="server" ErrorMessage="Please enter a valid data in the field max strength" ControlToValidate="txtMaxStrength" Display="None" ValidationExpression="^\d+$" ValidationGroup="groupM1"></asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />

                        </td>

                    </tr>

                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <%--<td align="center" >
                &nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                    style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>--%>
                                    <td align="center" class="text-danger font-small" colspan="4">Fields Marked with ( * ) are mandatory
                                    </td>
                                </tr>

                                <%--   <tr >
          <td align="left"  >
         <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
             Section Set Up&nbsp;</span></font></td>
                    </tr>--%>

                                <tr>
                                    <td align="left">Select Academic Year<span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                     <td align="left">Select Stream<span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="HF_GRM_Stream" runat="server"/>
                                    </td>
                                </tr>
                                <tr>
 <td align="left">Select Grade<span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left">Select Shift<span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                  
                                </tr>


                                <tr>
                                    <td align="left" colspan="4" class="title-bg">Details
                                    </td>
                                </tr>

                                <tr>

                                    <td align="left">Section Name<span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSection" runat="server"></asp:TextBox></td>

                                    <td align="left">Max Strength<span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMaxStrength" runat="server"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" /></td>
                                </tr>




                    <tr>
                        <td align="left" colspan="4">

                            <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvStudSection" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No records to edit."
                                            PageSize="100" Width="100%" BorderStyle="None">
                                            <Columns>

                                                <asp:TemplateField HeaderText="acd_id" Visible="False">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAcdId" runat="server" Text='<%# Bind("acd_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="AcademicYear">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAcademicYear" runat="server" Text='<%# Bind("acy_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Grade" Visible="False">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrmId" runat="server" Text='<%# Bind("grm_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Shift">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Stream">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="acd_id" Visible="False">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("sct_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Max. Strength">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMaxStrength" runat="server" Text='<%# Bind("sct_maxstrength")  %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>





                                                <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>

                                                <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>

                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <EmptyDataRowStyle />
                                            <EditRowStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>
                    </table>
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                    <asp:HiddenField ID="hfDisplay" runat="server" />
                    <asp:HiddenField ID="hfGrm_MaxCapacity" runat="server" />

                    </td>
                    </tr>

                </table>


            </div>
        </div>
    </div>

</asp:Content>

