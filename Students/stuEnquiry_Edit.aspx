<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="stuEnquiry_Edit.aspx.vb" Inherits="Students_stuEnquiry_Edit" Debug="true" %>

<%@ Register Src="../UserControls/FeeSponsor.ascx" TagName="FeeSponsor" TagPrefix="uc1" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function getDocuments() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfURL.ClientID %>").value

            window.showModalDialog(url, "", sFeatures);
            return false;

        }


        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 650px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;


        }




        function getDocDocument() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


        }

        function getCostUnit() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

        }

        function getCity() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


        }

        function getEMPQUALIFICATION() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


        }
        function getGrade() {

        }

        function AddDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 550px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

        }
        function UploadPhoto() {
            //        document.forms[0].Submit();

        }

        function confirm_cancel() {

            if (confirm("You are about to cancel this enquiry.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }



        function Disable_Text(ddlId) {
            var ControlName = document.getElementById(ddlId.id);



            //   var myVal = document.getElementById('myValidatorClientID');

            if (ControlName.value == '0')  //it depends on which value Selection do u want to hide or show your textbox 
            {

                document.getElementById('<%=txtFCompany.ClientID %>').value = '';
                 document.getElementById('<%=txtFCompany.ClientID %>').readOnly = "";


             }
             else {
                 document.getElementById('<%=txtFCompany.ClientID %>').value = '';
                 document.getElementById('<%=txtFCompany.ClientID %>').readOnly = "readonly";




             }
         }

         function GetSelectedItem() {
             var e = document.getElementById("<%= ddlFLang.ClientID%>");
       var fLang = e.options[e.selectedIndex].text;
       var CHK = document.getElementById("<%= chkOLang.ClientID%>");

var checkbox = CHK.getElementsByTagName("input");

var label = CHK.getElementsByTagName("label");

for (var i = 0; i < checkbox.length; i++) {

    if (checkbox[i].checked) {
        var olang = label[i].innerHTML
        if (fLang == olang) {
            checkbox[i].checked = false;
        }
    }

}

}




function chkFirst_lang() {

    GetSelectedItem();

}


function copyFathertoMother(chkThis) {
    var chk_state = chkThis.checked;
    if (chk_state == true) {
        document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
          document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContCurStreet.ClientID %>').value = document.getElementById('<%=txtFContCurStreet.ClientID %>').value;
          document.getElementById('<%=txtMContPermAddress1.ClientID %>').value = document.getElementById('<%=txtFContPermAddress1.ClientID %>').value;
          document.getElementById('<%=txtMContCurArea.ClientID %>').value = document.getElementById('<%=txtFContCurArea.ClientID %>').value;
          document.getElementById('<%=txtMContPermAddress2.ClientID %>').value = document.getElementById('<%=txtFContPermAddress2.ClientID %>').value;
          document.getElementById('<%=txtMContCurrBldg.ClientID %>').value = document.getElementById('<%=txtFContCurrBldg.ClientID %>').value;
          document.getElementById('<%=txtMContPermCity.ClientID %>').value = document.getElementById('<%=txtFContPermCity.ClientID %>').value
          document.getElementById('<%=txtMContCurrApartNo.ClientID %>').value = document.getElementById('<%=txtFContCurrApartNo.ClientID %>').value;
          document.getElementById('<%=ddlMContPermCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContPermCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContCurrPOBox.ClientID %>').value = document.getElementById('<%=txtFContCurrPOBox.ClientID %>').value;
          document.getElementById('<%=txtMContPermPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContPermPhoneNo.ClientID %>').value;
          document.getElementById('<%=ddlMContCurrPOBOX_EMIR.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrPOBOX_EMIR.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContPermPOBOX.ClientID %>').value = document.getElementById('<%=txtFContPermPOBOX.ClientID %>').value;
          document.getElementById('<%=txtMContCurrCity.ClientID %>').value = document.getElementById('<%=txtFContCurrCity.ClientID %>').value;
          document.getElementById('<%=ddlMContCurrCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContCurrPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContCurrPhoneNo.ClientID %>').value;
          document.getElementById('<%=txtMContCurrMobNo.ClientID %>').value = document.getElementById('<%=txtFContCurrMobNo.ClientID %>').value;

      }
      else {
          document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContCurStreet.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermAddress1.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurArea.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermAddress2.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrBldg.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermCity.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrApartNo.ClientID %>').value = "";
          document.getElementById('<%=ddlMContPermCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContCurrPOBox.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=ddlMContCurrPOBOX_EMIR.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContPermPOBOX.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrCity.ClientID %>').value = "";
          document.getElementById('<%=ddlMContCurrCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContCurrPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrMobNo.ClientID %>').value = "";

      }
      return true;
  }

  function copyFathertoGuardian(chkThis) {
      var chk_state = chkThis.checked;
      if (chk_state == true) {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = document.getElementById('<%=txtFContCurStreet.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = document.getElementById('<%=txtFContPermAddress1.ClientID %>').value;
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = document.getElementById('<%=txtFContCurArea.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').value = document.getElementById('<%=txtFContPermAddress2.ClientID %>').value;
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = document.getElementById('<%=txtFContCurrBldg.ClientID %>').value;
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = document.getElementById('<%=txtFContPermCity.ClientID %>').value
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = document.getElementById('<%=txtFContCurrApartNo.ClientID %>').value;
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContPermCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = document.getElementById('<%=txtFContCurrPOBox.ClientID %>').value;
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContPermPhoneNo.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrPOBOX_EMIR.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = document.getElementById('<%=txtFContPermPOBOX.ClientID %>').value;
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = document.getElementById('<%=txtFContCurrCity.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContCurrPhoneNo.ClientID %>').value;
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').value = document.getElementById('<%=txtFContCurrMobNo.ClientID %>').value;
      }
      else {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').value = "";
      }
      return true;
  }

  function copyMothertoGuardian(chkThis) {
      var chk_state = chkThis.checked;
      if (chk_state == true) {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = document.getElementById('<%=txtMContCurStreet.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = document.getElementById('<%=txtMContPermAddress1.ClientID %>').value;
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = document.getElementById('<%=txtMContCurArea.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').value = document.getElementById('<%=txtMContPermAddress2.ClientID %>').value;
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = document.getElementById('<%=txtMContCurrBldg.ClientID %>').value;
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = document.getElementById('<%=txtMContPermCity.ClientID %>').value
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = document.getElementById('<%=txtMContCurrApartNo.ClientID %>').value;
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlMContPermCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = document.getElementById('<%=txtMContCurrPOBox.ClientID %>').value;
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = document.getElementById('<%=txtMContPermPhoneNo.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = document.getElementById('<%=ddlMContCurrPOBOX_EMIR.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = document.getElementById('<%=txtMContPermPOBOX.ClientID %>').value;
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = document.getElementById('<%=txtMContCurrCity.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlMContCurrCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = document.getElementById('<%=txtMContCurrPhoneNo.ClientID %>').value;
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').selectedIndex = document.getElementById('<%=txtMContCurrMobNo.ClientID %>').selectedIndex;

      }
      else {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').value = "";
      }
      return true;
  }





    </script>

    <style>
        table.menu_a tr td  table tr td a { 
    display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  /*background-color: #b4b2b2 !important;*/
  background-clip: padding-box;
  border: 1px solid #8dc24c;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

      background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%) !important;
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
}

        table.menu_a tr td  table tr td a:hover {
                /*background-color: #ffffff !important;*/
    background: rgb(131,193,50) !important;
    background: -moz-linear-gradient(top, rgba(131,193,50,1) 0%, rgba(144,193,79,1) 56%, rgba(165,224,103,1) 100%) !important;
    background: -webkit-linear-gradient(top, rgba(131,193,50,1) 0%,rgba(144,193,79,1) 56%,rgba(165,224,103,1) 100%) !important;
    background: linear-gradient(to bottom, rgba(131,193,50,1) 0%,rgba(144,193,79,1) 56%,rgba(165,224,103,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#83c132', endColorstr='#a5e067',GradientType=0 ) !important;
            }

      table.menu_a tr td  table tr td a:visited {
            background-color: #b4b2b2 !important;
        }

         table.menu_a tr td  table tr td a:active {
            background-color: #b4b2b2 !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
    Enquiry Main
            </div>
            <div class="card-body">
            <div class="table-responsive m-auto">
    <table align="left" width="100%">
        <tr>
            <td align="left"   >Fields Marked with (<font color="red">*</font>)
           are mandatory</td>
        </tr>
        <tr>
            <td align="left"  >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary ID="vSummary" runat="server" CssClass="error" ForeColor="" ValidationGroup="groupM1"  />
                <asp:ValidationSummary ID="VSummaryPass" runat="server" CssClass="error" ForeColor="" ValidationGroup="groupM2"  />
                <asp:CompareValidator
                    ID="CompareValidator1" runat="server" ControlToValidate="txtACCNO" Display="None"
                    ErrorMessage="Please enter a valid Account No." Operator="GreaterThan" Type="Integer"
                    ValidationGroup="groupM1" ValueToCompare="0"></asp:CompareValidator>

            </td>
        </tr>
        <tr id="trDoc" runat="server">
            <td align="right"  valign="middle" >
                <asp:LinkButton ID="lnkDocs" OnClientClick="javascript:return getDocuments();" class="text-danger" runat="server" Visible="False">Pending Docs</asp:LinkButton>
            </td>
        </tr>
        <tr>
            
            <td>
                <table align="left"   cellpadding="5" cellspacing="0"  width="100%">

                    <tr>
                        <td align="left"  rowspan="1"  text-align: center;">
                            <table align="left"   cellpadding="5" cellspacing="0"
                                style="width: 100%">
                                <tr>
                                    <td  align="left"><span class="field-label">Enquiry No</span><span style="color:red">*</span></td>
                                    
                                    <td  >
                                        <asp:TextBox ID="txtEnqNo" runat="server"  ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" ><span class="field-label">Academic Year</span> <font color="red">*</font></td>
                                    
                                    <td  >
                                        <asp:TextBox ID="txtAcadYear" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>

                                    <td align="left" ><span class="field-label">Grade</span> <font color="red">*</font></td>
                                    
                                    <td  style="text-align: left" >
                                        <asp:TextBox ID="txtGrade" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td  align="left"><span class="field-label">Shift</span> <span style="color: red">*</span></td>
                                    
                                    <td   >
                                        <asp:TextBox ID="txtShift" runat="server" red ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" ><span class="field-label">Stream</span> <font color="red">*</font></td>
                                    
                                    <td  >
                                        <asp:TextBox ID="txtStream" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td align="left" ><span class="field-label">Status</span> <font color="red">*</font></td>
                                    
                                    <td  style="text-align: left" >
                                        <asp:TextBox ID="txtStatus" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr id="TR1" runat="server">
                                    <td align="left" ><span class="field-label">Registration Date</span> <font color="red">*</font></td>
                                    
                                    <td  colspan="0" style="text-align: left">
                                        <asp:TextBox ID="txtregdate" runat="server" red></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" /></td>
                                    <td align="left" ><span class="field-label">Account No.</span> </td>
                                   
                                    <td align="left" >
                                        <asp:TextBox ID="txtACCNO" runat="server" MaxLength="7"></asp:TextBox>

                                        <span class="field-label"></span> <asp:LinkButton ID="lbtnFeeSpon" Style="float: right; vertical-align: middle; display: inline; position: relative; top: -14px;  z-index: 1;" runat="server">Set Fee Sponsor</asp:LinkButton></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
           
        </tr>
        <tr>
           
            <td align="left">
                <div style="border-bottom: 1px solid #dee2e6;">
                <asp:Menu ID="mnuMaster" Orientation="Horizontal"
                    runat="server"   OnMenuItemClick="mnuMaster_MenuItemClick" CssClass="menu_a">
                    <Items>
                        

                        <asp:MenuItem Value="0" Text="Main" Selected="True"  />
                        <asp:MenuItem Text="Passport / Visa" Value="1" ></asp:MenuItem>
                        <asp:MenuItem Text="Previous School" Value="2" ></asp:MenuItem>
                        <asp:MenuItem Text="Contact Details" Value="3" ></asp:MenuItem>
                        <asp:MenuItem Text="Transport" Value="4" ></asp:MenuItem>
                        <asp:MenuItem Text="Health" Value="5" ></asp:MenuItem>
                        <asp:MenuItem Text="Other Details" Value="6" ></asp:MenuItem>
                    </Items>
                   <StaticMenuItemStyle CssClass="menuItem" />
                                    <StaticSelectedStyle CssClass="selectedItem" />
                                    <StaticHoverStyle CssClass="hoverItem" />
                    
                </asp:Menu>
                    </div>
            </td>
           
        </tr>

        <tr>
           
            <td>
                <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">
                    <asp:View ID="vwMain" runat="server">
                        <table id="tblContact" runat="server" align="center" cellpadding="5" cellspacing="0"
                             width="100%">
                            <tr>
                                <td align="left" width="20%" ><span class="field-label">Enquiry Date</span></td>                                
                                <td align="left" width="30%" >
                                    <asp:TextBox ID="txtEnqDate" runat="server" Enabled="false"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnEnqDate" runat="server" Enabled="false"
                                        ImageUrl="../Images/calendar.gif" /></td>
                                <td align="left" width="20%" ><span class="field-label">Tentative Date Of Join</span> 
                                </td>                                
                                <td align="left" width="30%"  >
                                    <asp:TextBox ID="txtTenDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnTenDate" runat="server"
                                        ImageUrl="../Images/calendar.gif" /></td>

                            </tr>
                            <tr>
                                <td align="left" width="20%"   ><span class="field-label">Applicant Full Name As Per Passport</span> <span style="color: red">*</span></td>                                
                                <td align="left"  >

                                    <table>

                                        <tr>
                                            <td>
                                                   <asp:TextBox ID="txtApplFirstName" runat="server"  MaxLength="50"></asp:TextBox>
                                            </td>
                                             <td>
                                                  <asp:TextBox ID="txtApplMidName" runat="server"  MaxLength="50"></asp:TextBox>
                                            </td>
                                             <td>
                                                   <asp:TextBox ID="txtApplLastName" runat="server"  MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                 
                                   
                                  </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"  ><span class="field-label">Name in English (as in National Id Card)</span></td>                               
                                <td align="left" >
                                    <asp:TextBox ID="txtSEmiratesId_ENG" runat="server" ></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>

                            <tr>
                                <td align="left" width="20%"  ><span class="field-label">Name in Arabic (as in National Id Card)</span></td>                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtSEmiratesId_ARB" runat="server" ></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>


                            <tr>
                                <td align="left" width="20%"   ><span class="field-label">Gender</span><font color="red">*</font></td>                                
                                <td align="left" >
                                    <asp:RadioButton ID="rdMale" runat="server" Checked="True" GroupName="Gender" Text="Male" />
                                    <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female" /></td>
                                <td align="left" width="20%"   ><span class="field-label">Religion</span><font color="red">*</font></td>                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlReligion" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="ddlReligion"
                                        Display="Dynamic" ErrorMessage="Please select Religion"
                                        InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>



                            <tr>
                                <td align="left"  width="20%"  ><span class="field-label">Date Of Birth</span><font color="red">*</font></td>                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtDOB" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnDOB" runat="server"
                                        ImageUrl="~/Images/calendar.gif" />
                                </td>
                                <td align="left"  width="20%"  ><span class="field-label">Place Of Birth</span><font color="red">*</font></td>                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPOB" runat="server" Width="163px" MaxLength="50"></asp:TextBox>
                                </td>

                            </tr>

                            <tr>
                                <td align="left" width="20%"  ><span class="field-label">Country Of Birth</span><font color="red">*</font></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlCOB" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvCOB" runat="server" ControlToValidate="ddlCOB"
                                        Display="Dynamic" ErrorMessage="Please select Country of Birth"
                                        InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="20%"  ><span class="field-label">Nationality</span><font color="red">*</font></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlNationality" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="ddlNationality"
                                        Display="Dynamic" ErrorMessage="Please select Student Nationality"
                                        InitialValue=" " ValidationGroup="groupM1">
                        *</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="tr_doc" runat="server" visible="false">
                                <td align="center"  colspan="6">
                                    <table cellpadding="1">
                                        <tr>
                                            <td  >The following documents has to be collected before registration</td>
                                        </tr>
                                        <tr>
                                            <td ><span class="field-label">To Collect</span>
                                    <br />
                                                <asp:ListBox ID="lstNotSubmitted"  runat="server" 
                                                    SelectionMode="Multiple" Style="overflow: auto"></asp:ListBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnRight" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                     OnClick="btnRight_Click" Text=">>" /><br />
                                                <asp:Button ID="btnLeft" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    OnClick="btnLeft_Click" Text="<<" />
                                            </td>
                                            <td ><span class="field-label">Collected</span>
                                    <br />
                                                <asp:ListBox ID="lstSubmitted"  runat="server" 
                                                    SelectionMode="Multiple" Style="overflow: auto" ></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td  align="left"><span class="field-label">First Language(Main)</span></td>                                
                                <td   align="left">
                                    <asp:DropDownList ID="ddlFLang" runat="server">
                                    </asp:DropDownList></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td  align="left"><span class="field-label">Other Languages(Specify)</span></td>                                
                                <td   align="left" colspan="3">
                                    <asp:Panel ID="plOLang" runat="server" >
                                        <div class="checkbox-list">
                                        <asp:CheckBoxList ID="chkOLang" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" >
                                        </asp:CheckBoxList></div>
                                    </asp:Panel>
                                </td>
                                
                            </tr>
                            <tr>
                                <td  rowspan="3"><span class="field-label">Proficiency in English</span></td>
                                <td colspan="3">
                                    <table width="100%">
                                        <tr>
                                             <td  width="15%"  align="left"><span class="field-label">Reading</span> </td><td>
                                    <asp:RadioButton ID="rbRExc" runat="server" GroupName="Read"
                                        Text="Excellent" CssClass="field-label" />
                                    <asp:RadioButton ID="rbRGood" runat="server" GroupName="Read" Text="Good" CssClass="field-label" />
                                    <asp:RadioButton ID="rbRFair" runat="server" GroupName="Read" Text="Fair" CssClass="field-label" />
                                    <asp:RadioButton ID="rbRPoor" runat="server" GroupName="Read" Text="Poor"  CssClass="field-label"/></td>
                                        </tr>
                                        <tr>
                                <td    align="left"><span class="field-label">Writing</span></td><td>
                                    <asp:RadioButton ID="rbWExc" runat="server" GroupName="Write"
                                        Text="Excellent" CssClass="field-label"/>
                                    <asp:RadioButton ID="rbWGood" runat="server" GroupName="Write" Text="Good" CssClass="field-label" />
                                    <asp:RadioButton ID="rbWFair" runat="server" GroupName="Write" Text="Fair" CssClass="field-label" />
                                    <asp:RadioButton ID="rbWPoor" runat="server" GroupName="Write" Text="Poor" CssClass="field-label" />
                                </td>
                            </tr>
                            <tr>
                                <td    align="left"><span class="field-label">Speaking</span></td><td>
                                                                                                <asp:RadioButton ID="rbSExc" runat="server" GroupName="Speak"
                                                                                                    Text="Excellent" CssClass="field-label"/>
                                    <asp:RadioButton ID="rbSGood" runat="server" GroupName="Speak" Text="Good" CssClass="field-label" />
                                    <asp:RadioButton ID="rbSFair" runat="server" GroupName="Speak" Text="Fair" CssClass="field-label" />
                                    <asp:RadioButton ID="rbSPoor" runat="server" GroupName="Speak" Text="Poor" CssClass="field-label" />
                                </td>
                            </tr>
                                    </table>
                                </td>
                               
                            </tr>
                            
                        </table>
                    </asp:View>
                    <asp:View ID="vwAmt" runat="server">
                        <table align="center"   cellpadding="5" cellspacing="0" width="100%"
                            >

                            <tr>
                                <td align="left"  ><span class="field-label">Passport No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtPNo" runat="server"  MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPassNo" runat="server" ControlToValidate="txtPNo"
                                        Display="Dynamic" ErrorMessage="Passport No. required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left"  ><span class="field-label">Passport Issue Place</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPIssPlace" runat="server"  MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPass_Issue" runat="server" ControlToValidate="txtPIssPlace"
                                        Display="Dynamic" ErrorMessage="Passport Issue Place required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>


                            <tr>
                                <td align="left"  ><span class="field-label">Passport Issue Date</span>
                                                                                          
                                </td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPIssDate" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnPIssDate" runat="server"
                                        ImageUrl="~/Images/calendar.gif" />
                                    <asp:RequiredFieldValidator ID="rfvPass_IssDate" runat="server" ControlToValidate="txtPIssDate"
                                        Display="Dynamic" ErrorMessage="Passport Issue Date cannot be left empty"
                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPIssDate"
                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvIss_date" runat="server" ControlToValidate="txtPIssDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                                <td align="left"  ><span class="field-label">Passport Expiry Date</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPExpDate" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnPExpDate" runat="server"
                                        ImageUrl="~/Images/calendar.gif" />
                                    <asp:RequiredFieldValidator ID="rfvPass_ExpDate" runat="server" ControlToValidate="txtPExpDate"
                                        Display="Dynamic" ErrorMessage="Passport Expiry Date cannot be left empty"
                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPExpDate"
                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvExp_date" runat="server" ControlToValidate="txtPExpDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Passport Expiry Date entered is not a valid date and must be greater than Issue Date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                            </tr>


                            <tr>
                                <td align="left"  ><span class="field-label">Visa No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtVNo" runat="server"  MaxLength="50"></asp:TextBox>
                                </td>
                                <td align="left"  ><span class="field-label">Visa Issue Place</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtVIssPlace" runat="server"  MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>


                            <tr>
                                <td align="left"  ><span class="field-label">Visa Issue Date</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtVIssDate" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnVIssDate" runat="server"
                                        ImageUrl="~/Images/calendar.gif" />

                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtVIssDate"
                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtVIssDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                                <td align="left"  ><span class="field-label">Visa Expiry Date</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtVExpDate" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnVExpdate" runat="server"
                                        ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtVExpDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator4" runat="server" ControlToValidate="txtVExpDate"
                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Visa Issue Authority</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtVIssAuth" runat="server"
                                         MaxLength="50"></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">National Id</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtEmiratesId" runat="server" ></asp:TextBox>
                                </td>
                                <td align="left"  ><span class="field-label">Premises Id </span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPremisesId" runat="server" ></asp:TextBox>
                                </td>
                            </tr>

                        </table>
                    </asp:View>
                    <asp:View ID="vwDocuments" runat="server">
                        <table style="border-collapse: collapse" width="100%">

                            <tr>
                                <td align="left">
                                    <table align="left"   cellpadding="5"
                                        cellspacing="0" width="100%">
                                        <tr id="trSch0" runat="server">
                                            <td ><span class="field-label">School Name</span></td>
                                            
                                            <td >
                                                <asp:DropDownList ID="ddlPreSchool_Nursery" runat="server">
                                                </asp:DropDownList>
                                                <br />
                                                (If you choose other,please specify the School Name)<br />
                                                <asp:TextBox ID="txtPreSchool_Nursery" runat="server" MaxLength="250"></asp:TextBox>
                                            </td>
                                            <td ><span class="field-label">Registration No.</span>
                                            </td>
                                           
                                            <td >
                                                <asp:TextBox ID="txtRegNo" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trSch1" runat="server">
                                            <td ><span class="field-label">School Detail Type </span></td>
                                            
                                            <td  >
                                               <span class="field-value"><asp:Literal ID="ltSchoolType" runat="server"></asp:Literal></span> 
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr id="trSch2" runat="server">
                                            <td ><span class="field-label">School Name</span></td>                                            
                                            <td  >
                                                <asp:DropDownList ID="ddlPSchool" runat="server">
                                                </asp:DropDownList>
                                                <br />
                                                (If you choose other,please specify the School Name)<br />
                                                <asp:TextBox ID="txtPSchool" runat="server" MaxLength="250"></asp:TextBox>
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr id="trSch3" runat="server">
                                            <td ><span class="field-label">Name of the head teacher</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtSchool_head" runat="server"></asp:TextBox>
                                            </td>

                                            <td ><span class="field-label">Student Id</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtFeeID_GEMS" runat="server" Width="90px"></asp:TextBox>
                                            </td>

                                        </tr>
                                        <tr id="trSch4" runat="server">
                                            <td ><span class="field-label">Grade</span></td>
                                            
                                            <td >
                                                <asp:DropDownList ID="ddlPGrade" runat="server">
                                                </asp:DropDownList>
                                            </td>

                                            <td ><span class="field-label">Curriculum</span></td>
                                            
                                            <td >
                                                <asp:DropDownList ID="ddlPCurriculum" runat="server">
                                                </asp:DropDownList>
                                            </td>

                                        </tr>


                                        <tr id="trSch5" runat="server">
                                            <td ><span class="field-label">Language of Instruction</span> </td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtLang_Instr" runat="server"></asp:TextBox>
                                            </td>
                                            <td ><span class="field-label">Address</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtSchAddr" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trSch6" runat="server">
                                            <td ><span class="field-label">City</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtPSchCity" runat="server"></asp:TextBox></td>
                                            <td ><span class="field-label">Country</span></td>
                                            
                                            <td >
                                                <asp:DropDownList ID="ddlPCountry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr id="trSch7" runat="server">
                                            <td ><span class="field-label">School Phone</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtSCHPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                -<asp:TextBox ID="txtSCHPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                -<asp:TextBox ID="txtSCHPhone_No" runat="server" MaxLength="10" Width="40%"></asp:TextBox>
                                                <br />
                                                (Country -Area-Number</td>
                                            <td ><span class="field-label">School Fax</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtSCHFax_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                -<asp:TextBox ID="txtSCHFax_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                -<asp:TextBox ID="txtSCHFax_No" runat="server" MaxLength="10" Width="35%"></asp:TextBox>
                                                <br />
                                                (Country -Area-Number)</td>
                                        </tr>
                                        <tr id="trSch8" runat="server">
                                            <td ><span class="field-label">From Date</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtSchFrom_dt" runat="server" Width="110px"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="SchFrom_dt_CalendarExtender" runat="server"
                                                    Format="dd/MMM/yyyy" PopupButtonID="imgSchFrom_dt"
                                                    TargetControlID="txtSchFrom_dt">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:ImageButton ID="imgSchFrom_dt" runat="server"
                                                    ImageUrl="~/Images/calendar.gif" />
                                                <br />
                                               (dd/mmm/yyyy)</td>
                                            <td ><span class="field-label">To Date</span></td>
                                            
                                            <td >
                                                <asp:TextBox ID="txtSchTo_dt" runat="server" Width="110px"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="txtSchTo_dt_CalendarExtender" runat="server"
                                                    Format="dd/MMM/yyyy" PopupButtonID="imgtxtSchTo_dt"
                                                    TargetControlID="txtSchTo_dt">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:ImageButton ID="imgtxtSchTo_dt" runat="server"
                                                    ImageUrl="~/Images/calendar.gif" />
                                                <br />
                                                (dd/mmm/yyyy) </td>
                                        </tr>
                                        <tr id="trSch9" runat="server">
                                            <td  align="center" colspan="6">
                                                <asp:Button ID="btnGridAdd" runat="server" CssClass="button" 
                                                    Text="Add"  />
                                                <asp:Button ID="btnGridUpdate" runat="server" CssClass="button" 
                                                    Text="Update"  />
                                            </td>

                                        </tr>

                                        <tr id="trSch10" runat="server">
                                            <td align="center"  colspan="6">
                                                <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False"
                                                     DataKeyNames="ID" EmptyDataText="No record added yet" CssClass="table table-bordered table-row" 
                                                    EnableModelValidation="True"  
                                                     Width="100%">
                                                    <RowStyle CssClass="griditem"  />
                                                    <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="School Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSCH_NAME" runat="server" Text='<%# Bind("EQM_PREVSCHOOL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"  />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Head Teacher">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSCH_HEAD" runat="server"
                                                                    Text='<%# Bind("EQM_PREVSCHOOL_HEAD_NAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Grade">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSCH_GRADE" runat="server"
                                                                    Text='<%# BIND("EQM_PREVSCHOOL_GRD_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="From Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSCH_FROMDT" runat="server"
                                                                    Text='<%# bind("EQM_PREVSCHOOL_FROMDT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="To Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblToDt" runat="server"
                                                                    Text='<%# bind("EQM_PREVSCHOOL_LASTATTDATE") %>'></asp:Label>
                                                            </ItemTemplate>

                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Edit">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="EditBtn" runat="server" CommandArgument='<%# Eval("id") %>'
                                                                    CommandName="Edit">Edit</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <HeaderStyle  />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="EPS_Id" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEPS_Id" runat="server" Text='<%# bind("EPS_Id") %>'></asp:Label>
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle />
                                                    <HeaderStyle CssClass="gridheader_new"  />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4" ></td>
                            </tr>

                        </table>




                    </asp:View>
                    <asp:View ID="vwConnDet" runat="server">
                        <table align="center"   cellpadding="5" cellspacing="0"
                            width="100%">


                            <tr>
                                <td align="left" width="20%"  ><span class="field-label">Primary Contact</span><font color="red">*</font></td>
                               
                                <td align="left"  >
                                    <asp:RadioButton ID="rdFather" runat="server" GroupName="ContactPrimary" Text="Father" />
                                    <asp:RadioButton ID="rdMother" runat="server" GroupName="ContactPrimary" Text="Mother" />
                                    <asp:RadioButton ID="rdGuardian" runat="server" GroupName="ContactPrimary" Text="Guardian" />

                                </td>
                                <td colspan="2">

                                </td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Fee Sponsor</span> <font color="red">*</font></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFeeSponsor" runat="server" >
                                        <asp:ListItem Value="1">Father</asp:ListItem>
                                        <asp:ListItem Value="2">Mother</asp:ListItem>
                                        <asp:ListItem Value="3">Guardian</asp:ListItem>
                                        <asp:ListItem Value="4">Father's Company</asp:ListItem>
                                        <asp:ListItem Value="5">Mother's Company</asp:ListItem>
                                        <asp:ListItem Value="6">Guardians Company</asp:ListItem>
                                    </asp:DropDownList>
                                   
                                </td>
                                 <td colspan="2"></td>
                            </tr>

                            <tr>
                                <td align="left"  ><span class="field-label">Preferred Contact</span><font color="red">*</font></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlPrefContact" runat="server" Width="109px">
                                        <asp:ListItem Value="HOME PHONE">Home Phone</asp:ListItem>
                                        <asp:ListItem Value="OFFICE PHONE">Office Phone</asp:ListItem>
                                        <asp:ListItem Value="MOBILE">Mobile</asp:ListItem>
                                        <asp:ListItem Value="EMAIL">Email</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td colspan="2"> 

                                </td>
                            </tr>

                            <tr class="title-bg">
                                <td align="left"  colspan="4">
                                   Father's Contact Details</td>
                            </tr>


                            <tr>
                                <td align="left"  ><span class="field-label">Full Name As Per Passport</span> </td>
                                
                                <td align="left"  colspan="3">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtFFirstName" runat="server" MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvFFirstName" runat="server" ControlToValidate="txtFFirstName" Visible="false"
                                                    Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact father's first name required"
                                                    ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                    TargetControlID="txtFFirstName" ValidChars="-\./' ">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFMidName" runat="server" MaxLength="50"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                    TargetControlID="txtFMidName" ValidChars="-\./' ">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFLastName" runat="server" MaxLength="50"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                    TargetControlID="txtFLastName" ValidChars="-\./' ">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td>
                                                <span align="center">First Name</span>
                                            </td>
                                            <td>
                                                <span align="center">Middle Name</span>
                                            </td>
                                            <td>
                                                <span align="center">Last Name</span>
                                            </td>
                                        </tr>
                                    </table>
                                                                                                         
                                </td>                                
                            </tr>

                            <tr>
                                <td align="left" width="20%" ><span class="field-label">National ID</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFEMIRATES_ID" runat="server" ></asp:TextBox></td>
                                <td align="left" width="20%" ><span class="field-label">National ID Expiry Date</span>
                                </td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFEMIRATES_ID_EXPDATE" runat="server" Width="110px" MaxLength="12">
                                    </asp:TextBox><asp:ImageButton ID="imgBtnFEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbFEMIRATES_IDExp_date" runat="server" TargetControlID="txtFEMIRATES_ID_EXPDATE"
                                        FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <ajaxToolkit:CalendarExtender ID="ceFEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                        PopupButtonID="imgBtnFEMIRATES_IDExp_date" TargetControlID="txtFEMIRATES_ID_EXPDATE">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="revFEmir_Exp_date"
                                        runat="server" ControlToValidate="txtFEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                        ErrorMessage="Enter father's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>(dd/mmm/yyyy)
                                </td>
                            </tr>

                            <tr>
                                <td align="left"><span class="field-label">Name in English (as in National Id Card)</span></td>                                
                                <td align="left">
                                    <asp:TextBox ID="txtFEmiratesId_ENG" runat="server" ></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>

                            <tr>
                                <td align="left"><span class="field-label">Name in Arabic (as in National Id Card)</span></td>                               
                                <td align="left">
                                    <asp:TextBox ID="txtFEmiratesId_ARB" runat="server" ></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>

                            <tr>
                                <td align="left"><span class="field-label">Nationalty 1</span></td>
                               
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFNationality1" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="left"  ><span class="field-label">Nationality 2</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFNationality2" runat="server" Height="16px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"  colspan="2" class="title-bg-small">Current Address</td>
                                <td align="center"  colspan="2" class="title-bg-small">Permanent Address</td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Country</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFContCurrCountry" runat="server" AutoPostBack="true">
                                    </asp:DropDownList></td>
                                <td align="left"  ><span class="field-label">Address1</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFContPermAddress1" runat="server"
                                        TabIndex="6" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">City/State</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFCity" runat="server" CssClass="dropDownList" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                        If you choose other,please specify the City/State Name
                                    </div>
                                    <asp:TextBox ID="txtFContCurrCity" runat="server" TabIndex="2"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Address2</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFContPermAddress2" runat="server"
                                        TabIndex="6" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Area</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFArea" runat="server" CssClass="dropDownList">
                                    </asp:DropDownList><div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                        If you choose other,please specify the Area Name
                                    </div>
                                    <asp:TextBox ID="txtFContCurArea" runat="server"  MaxLength="50"></asp:TextBox></td>



                                <td align="left" ><span class="field-label">City</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFContPermCity" runat="server"  TabIndex="8"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Street</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContCurStreet" runat="server" MaxLength="50"></asp:TextBox></td>

                                <td align="left" ><span class="field-label">Country</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFContPermCountry" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Building</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContCurrBldg" runat="server" MaxLength="50" ></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Res. Phone No</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFContPermPhoneNo" runat="server" 
                                        TabIndex="10" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>

                                <td align="left"  ><span class="field-label">Apartment No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContCurrApartNo" runat="server" MaxLength="50"
                                        ></asp:TextBox></td>



                                <td align="left"  ><span class="field-label">P.O Box/Zip Code</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFContPermPOBOX" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">City/Emirate</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFContCurrPOBOX_EMIR" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left"  colspan="2" >
                                </td>

                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">P O Box</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContCurrPOBox" runat="server"  TabIndex="1"
                                        MaxLength="20"></asp:TextBox></td>
                                <td align="left"  colspan="2" >
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Res. Phone No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContCurrPhoneNo" runat="server"  TabIndex="4"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left" colspan="2" ></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Mobile No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContCurrMobNo" runat="server"  TabIndex="5"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left" colspan="2"  >
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="title-bg-small" colspan="4">Communication Details</td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">E-mail</span><font color="red">*</font></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFContEmail" runat="server"  TabIndex="18"
                                        MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPri_Email" runat="server" ControlToValidate="txtFContEmail"
                                        Display="Dynamic" ErrorMessage="E-Mail ID required" ValidationGroup="groupM1" EnableViewState="False">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revPri_Email" runat="server" ControlToValidate="txtFContEmail"
                                        Display="Dynamic" ErrorMessage="E-Mail entered is not valid" ValidationGroup="groupM1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" EnableViewState="False">*</asp:RegularExpressionValidator></td>
                                <td colspan="2">

                                </td>

                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Occupation</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFOcc" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Company</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFCompany" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    (If you choose other,please specify the Company Name)<br />
                                    <asp:TextBox ID="txtFCompany" runat="server" Width="219px" TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Office Phone</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFOfficePhone" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Fax No.</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFFax" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>

                            <tr class="title-bg">
                                <td align="left"  colspan="4">                                    
                                       Mother&#39;s Contact Details
                                       
                                        <span style="float: right;">
                                            <asp:CheckBox ID="chkCopyF_Details" runat="server" Style="clear: left" Text="Copy Father Details" onclick="javascript:return copyFathertoMother(this);" /></span>
                                 
                                </td>

                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Full Name As Per Passport</span> </td>                                
                                <td align="left" colspan="3">                                     
                                     <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtMFirstName" runat="server" MaxLength="50" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvMFirstName" runat="server" ControlToValidate="txtMFirstName" Visible="false"
                                        Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact mother's first name required"
                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                 <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                        TargetControlID="txtMFirstName" ValidChars="-\./' ">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMMidName" runat="server" MaxLength="50" ></asp:TextBox>
                                                  <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                        TargetControlID="txtMMidName" ValidChars="-\./' ">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMLastName" runat="server" MaxLength="50" ></asp:TextBox>
                                                 <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                        TargetControlID="txtMLastName" ValidChars="-\./' ">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>                                                                                                                                                                                                                                                                                
                                        </tr>
                                        <tr>
                                            <td>
                                                <span align="center" >First Name</span>
                                            </td>
                                            <td>
                                                <span align="center" >Middle Name</span>
                                            </td>
                                            <td>
                                                <span align="center">Last Name</span>
                                            </td>
                                        </tr>
                                    </table>                                                                                                                                             
                                </td>
                               
                            </tr>

                            <tr>
                                <td align="left" ><span class="field-label">National ID</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMEMIRATES_ID" runat="server" ></asp:TextBox></td>
                                <td align="left" ><span class="field-label">National ID Expiry Date</span>
                                </td>
                              

                                <td align="left" >
                                    <asp:TextBox ID="txtMEMIRATES_ID_EXPDATE" runat="server" Width="110px" MaxLength="12">
                                    </asp:TextBox><asp:ImageButton ID="imgBtnMEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbMEMIRATES_IDExp_date" runat="server" TargetControlID="txtMEMIRATES_ID_EXPDATE"
                                        FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <ajaxToolkit:CalendarExtender ID="ceMEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                        PopupButtonID="imgBtnMEMIRATES_IDExp_date" TargetControlID="txtMEMIRATES_ID_EXPDATE">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="revMEmir_Exp_date"
                                        runat="server" ControlToValidate="txtMEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                        ErrorMessage="Enter mother's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td>
                            </tr>

                            <tr>
                                <td align="left" ><span class="field-label">Name in English (as in National Id Card)</span></td>                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMEmiratesId_ENG" runat="server" ></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>

                            <tr>
                                <td align="left" ><span class="field-label">Name in Arabic (as in National Id Card)</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMEmiratesId_ARB" runat="server" ></asp:TextBox></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Nationalty 1</span></td>
                               
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMNationality1" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="left"  ><span class="field-label">Nationality 2</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlMNationality2" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="title-bg-small" colspan="2"  >Current Address</td>
                                <td align="center" class="title-bg-small" colspan="2">Permanent Address</td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Country</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMContCurrCountry" runat="server" AutoPostBack="true">
                                    </asp:DropDownList></td>


                                <td align="left"  ><span class="field-label">Address1</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMContPermAddress1" runat="server" 
                                        TabIndex="6" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>

                                <td align="left"  ><span class="field-label">City/State</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMCity" runat="server" CssClass="dropDownList" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                        If you choose other,please specify the City/State Name
                                    </div>
                                    <asp:TextBox ID="txtMContCurrCity" runat="server"  TabIndex="2"
                                        MaxLength="50"></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">Address2</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMContPermAddress2" runat="server"
                                        TabIndex="6" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Area</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMArea" runat="server" CssClass="dropDownList">
                                    </asp:DropDownList>
                                    <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                        If you choose other,please specify the Area Name
                                    </div>
                                    <asp:TextBox ID="txtMContCurArea" runat="server"  MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">City</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMContPermCity" runat="server"  TabIndex="8"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Street</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContCurStreet" runat="server"  MaxLength="50"></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">Country</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlMContPermCountry" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Building</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContCurrBldg" runat="server" MaxLength="50" ></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">Res. Phone No</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMContPermPhoneNo" runat="server" 
                                        TabIndex="10" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Apartment No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContCurrApartNo" runat="server" MaxLength="50"
                                        ></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">P.O Box/Zip Code</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMContPermPOBOX" runat="server" ></asp:TextBox></td>
                            </tr>
                            <tr>

                                <td align="left"  ><span class="field-label">City/Emirate</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMContCurrPOBOX_EMIR" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left" colspan="2"  >
                                </td>
                            </tr>
                            <tr>


                                <td align="left"  ><span class="field-label">P O Box</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContCurrPOBox" runat="server"  TabIndex="1"
                                        MaxLength="20"></asp:TextBox></td>
                                <td align="left" colspan="2" >
                                </td>

                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Res. Phone No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContCurrPhoneNo" runat="server"  TabIndex="4"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  colspan="2" ></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Mobile No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContCurrMobNo" runat="server"  TabIndex="5"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left" colspan="2"  >
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="title-bg-small" colspan="6">Communication Details</td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">E-mail</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMContEmail" runat="server"  TabIndex="18"
                                        MaxLength="50"></asp:TextBox>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Occupation</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMOcc" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Company</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMCompany" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    (If you choose other,please specify the Company Name)<br />
                                    <asp:TextBox ID="txtMCompany" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Office Phone</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMOfficePhone" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Fax No.</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMFax" runat="server" TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>

                            <tr class="title-bg">
                                <td align="center"  colspan="4">
                                    
                                        Guardian&#39;s Contact Details 
                                       
                                        <span style="float: right;">
                                            <asp:CheckBox ID="chkCopyF_Details_Guard" runat="server" Text="Copy Father Details" onclick="javascript:return copyFathertoGuardian(this);" />
                                            <asp:CheckBox ID="chkCopyM_Details_Guard" runat="server" Text="Copy Mother Details" onclick="javascript:return copyMothertoGuardian(this);" /></span>                                 

                                </td>

                            </tr>

                            <tr>
                                <td align="left"  ><span class="field-label">Full Name As Per Passport</span> </td>
                               
                                <td align="left" colspan="3" >

                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtGFirstName" runat="server" MaxLength="50" ></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="rfvGFirstName" runat="server" ControlToValidate="txtGFirstName" Visible="false"
                                        Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact Guardian's first name required"
                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                        TargetControlID="txtGFirstName" ValidChars="-\./' ">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                 <asp:TextBox ID="txtGMidName" runat="server" MaxLength="50" ></asp:TextBox>
                                                  <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                        TargetControlID="txtGMidName" ValidChars="-\./' ">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtGLastName" runat="server" MaxLength="50" ></asp:TextBox>
                                                 <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                        TargetControlID="txtGLastName" ValidChars="-\./' ">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span align="center" >First Name</span>
                                            </td>
                                            <td>
                                                <span align="center" >Middle Name</span>
                                            </td>
                                            <td>
                                                <span align="center" >Last Name</span>
                                            </td>
                                        </tr>
                                    </table>
                                                                                                                                                                                                                                                                                           
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Nationalty 1</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGNationality1" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="left"  ><span class="field-label">Nationality 2</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlGNationality2" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"  class="title-bg-small " colspan="2">Current Address</td>
                                <td align="center"  class="title-bg-small " colspan="2">Permanent Address</td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Country</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGContCurrCountry" runat="server" AutoPostBack="true">
                                    </asp:DropDownList></td>


                                <td align="left"  ><span class="field-label">Address1</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGContPermAddress1" runat="server" 
                                        TabIndex="6" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">City/State</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGCity" runat="server" CssClass="dropDownList" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                        If you choose other,please specify the City/State Name
                                    </div>
                                    <asp:TextBox ID="txtGContCurrCity" runat="server"  TabIndex="2"
                                        MaxLength="50"></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">Address2</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGContPermAddress2" runat="server" 
                                        TabIndex="6" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Area</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGArea" runat="server" CssClass="dropDownList">
                                    </asp:DropDownList>
                                    <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                        If you choose other,please specify the Area Name
                                    </div>
                                    <asp:TextBox ID="txtGContCurArea" runat="server"  MaxLength="50"></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">City</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGContPermCity" runat="server" TabIndex="8"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Street</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGContCurStreet" runat="server"  MaxLength="50"></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">Country</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlGContPermCountry" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>

                                <td align="left"  ><span class="field-label">Building</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGContCurrBldg" runat="server" MaxLength="50" ></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">Res. Phone No</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGContPermPhoneNo" runat="server" 
                                        TabIndex="10" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>


                                <td align="left"  ><span class="field-label">Apartment No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGContCurrApartNo" runat="server" MaxLength="50"
                                        ></asp:TextBox></td>

                                <td align="left"  ><span class="field-label">P.O Box/Zip Code</span>                                 </td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGContPermPOBOX" runat="server" ></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">City/Emirate</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGContCurrPOBOX_EMIR" runat="server">
                                    </asp:DropDownList></td>

                                <td align="left" colspan="2"  >
                                </td>


                            </tr>
                            <tr>


                                <td align="left"  ><span class="field-label">P O Box</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGContCurrPOBox" runat="server"  TabIndex="1"
                                        MaxLength="20"></asp:TextBox></td>

                                <td align="left" colspan="2" >
                                </td>

                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Res. Phone No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGContCurrPhoneNo" runat="server" Width="246px" TabIndex="4"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left" colspan="2"  ></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Mobile No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGContCurrMobNo" runat="server"  TabIndex="5"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left" colspan="2"  >
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="title-bg-small" colspan="4">Communication Details</td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">E-mail</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGContEmail" runat="server"  TabIndex="18"
                                        MaxLength="50"></asp:TextBox>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Occupation</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGOcc" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Company</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGCompany" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    (If you choose other,please specify the Company Name)<br />
                                    <asp:TextBox ID="txtGCompany" runat="server" TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Office Phone</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGOfficePhone" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                                <td align="left"  ><span class="field-label">Fax No.</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGFax" runat="server"  TabIndex="19"
                                        MaxLength="50"></asp:TextBox></td>
                            </tr>

                            <tr class="title-bg">
                                <td align="left"  colspan="4">
                                    Other Info</td>

                            </tr>


                            <tr>
                                <td align="left"  ><span class="field-label">Receive Sms </span></td>
                              
                                <td align="left"  >
                                    <asp:RadioButton ID="rdSmsYes" Text="Yes" GroupName="GroupSms" runat="server" CssClass="field-label" />
                                    <asp:RadioButton ID="rdSmsNo" Text="No" GroupName="GroupSms" runat="server" CssClass="field-label" />
                                </td>
                                <td align="left"  > <span class="field-label">Receive Promotional Emails </span></td>
                               
                                <td align="left"  >
                                    <asp:RadioButton ID="rdEmailYes" Text="Yes" GroupName="GroupEmail" runat="server" CssClass="field-label" />
                                    <asp:RadioButton ID="rdEmailNo" Text="No" GroupName="GroupEmail" runat="server" CssClass="field-label" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  ><span class="field-label">Publication/promotion photos and videos</span></td>
                                
                                <td align="left"  >
                                    <asp:RadioButton ID="rbPubYES" runat="server" GroupName="Pub" Text="Yes" CssClass="field-label" />
                                    <asp:RadioButton ID="rbPubNo" runat="server" GroupName="Pub" Text="No"  CssClass="field-label"/>
                                </td>
                                <td colspan="2">

                                </td>
                            </tr>
                            <tr>
                                <td align="left"  colspan="4" >
                                    <span class="field-label">How did you hear about
                        <asp:Literal ID="ltAboutUs" runat="server"></asp:Literal></span><br />
                                    <asp:CheckBoxList ID="chkAboutUs" runat="server" CellPadding="2" CellSpacing="1"
                                        RepeatColumns="6" RepeatDirection="Horizontal">
                                    </asp:CheckBoxList></td>
                            </tr>
                            <tr>
                                <td  colspan="4" align="left">
                                    <div>
                                       <span class="field-label"> Are there any family circumstances of which you feel we should be aware of?</span>
                                    </div>
                                    <div>
                                      <span class="field-label">  (eg Deceased parent /divorced/separated / adopted /others if so please give 
                                                                                                 detail)</span>
                                    </div>
                                    <br />
                                    <asp:TextBox ID="txtFamily_NOTE" runat="server"  Rows="2"
                                        SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                        MaxLength="255"></asp:TextBox>

                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="vTransport" runat="server">
                        <asp:Panel runat="server" ID="Panel1">
                            <table align="center"   cellpadding="5" cellspacing="0"
                                " width="100%">
                                <tbody>
                                   <%-- <tr>
                                        <td align="left" class="subHeader" colspan="6">Transport Details</td>
                                    </tr>--%>
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Transport Required </span></td>
                                        
                                        <td align="left"  >
                                            <asp:RadioButton ID="rdTYes" Text="Yes" GroupName="GroupTRS" runat="server" AutoPostBack="True" CssClass="field-label" />
                                            <asp:RadioButton ID="rdTNo" Text="No" GroupName="GroupTRS" runat="server" AutoPostBack="True" CssClass="field-label" />
                                        </td>
                                        <td align="left" width="20%" ><span class="field-label">Location</span> </td>
                                        
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td align="left"  ><span class="field-label">Sub Location</span> </td>
                                        
                                        <td align="left"  >
                                            <asp:DropDownList ID="ddlSubLocation" runat="server"  AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left"  ><span class="field-label">Pick Up Point</span> </td>
                                        
                                        <td align="left" >
                                            <asp:DropDownList ID="ddlPickUp" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td align="left"  ><span class="field-label">Remarks </span></td>
                                        
                                        <td align="left" >
                                            <asp:TextBox SkinID="MultiText" ID="txtTptRemarks" runat="server"  TextMode="MultiLine" MaxLength="255"></asp:TextBox>
                                        </td>
                                        <td align="left" ><span class="field-label">Transport Status</span></td>
                                      
                                        <td align="left"  >
                                            <asp:Label ID="lblTptStatus" runat="server" Text="Label" CssClass="field-value"></asp:Label></td>

                                    </tr>

                                </tbody>
                            </table>

                        </asp:Panel>

                        <asp:Panel runat="server" ID="Panel2">
                            <table align="center"   cellpadding="5" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td align="left" class="title-bg" colspan="4"><span class="field-label">Other Services</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"  colspan="5">
                                            <asp:GridView ID="gvServices" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                               CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                HeaderStyle-Height="30"  BorderStyle="None">
                                                <Columns>

                                                    <asp:TemplateField Visible="False">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSvcId" runat="server" Text='<%# Bind("SVC_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Service">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblService" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:RadioButton ID="rdYes" Text="Yes" Checked='<%# Bind("YES") %>' runat="server" GroupName='<%# Eval("SVC_ID") %>' />
                                                            <asp:RadioButton ID="rdNo" Text="No" Checked='<%# Bind("NO") %>' runat="server" GroupName='<%# Eval("SVC_ID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                                <RowStyle CssClass="griditem"  Wrap="False" />
                                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="vOther" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALSALDET" />
                        <table  width="100%">
                            <tr>
                                <td>
                                    <table align="left" width="100%" style="border-collapse: collapse"   cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td   align="left"><span class="field-label">Health Card No / Medical Insurance No</span></td>
                                            
                                            <td   align="left">
                                                <asp:TextBox ID="txtHealtCard" runat="server" MaxLength="50" Width="110px"></asp:TextBox>
                                            </td>
                                            <td   align="left"><span class="field-label">Blood Group</span></td>
                                            
                                            <td    align="left">
                                                <asp:DropDownList ID="ddlBgroup" runat="server">
                                                    <asp:ListItem>--</asp:ListItem>
                                                    <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                                    <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                                    <asp:ListItem Value="B+">B+</asp:ListItem>
                                                    <asp:ListItem Value="A+">A+</asp:ListItem>
                                                    <asp:ListItem Value="B-">B-</asp:ListItem>
                                                    <asp:ListItem Value="A-">A-</asp:ListItem>
                                                    <asp:ListItem Value="O+">O+</asp:ListItem>
                                                    <asp:ListItem Value="O-">O-</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td  colspan="4" class="title-bg">
                                               Health Restriction</td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%" ><span class="field-label">Allergies</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthAll_Yes" runat="server" GroupName="Allerg"
                                                    Text="Yes" CssClass="field-label" />
                                                <asp:RadioButton ID="rbHthAll_No" runat="server" GroupName="Allerg" Text="No" CssClass="field-label" /></td>
                                            <td align="left" width="20%"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthAll_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                                     Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                                    MaxLength="255"></asp:TextBox></td>
                                        </tr>

                                        <tr>
                                            <td align="left"  ><span class="field-label">Disabled ?</span>
                                          
                                            </td>
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthDisabled_YES" runat="server" GroupName="HthDisabled"
                                                    Text="Yes" CssClass="field-label" />
                                                <asp:RadioButton ID="rbHthDisabled_No" runat="server" GroupName="HthDisabled"
                                                    Text="No" CssClass="field-label" />
                                            </td>
                                            <td align="left"  ><span class="field-label">Notes</span>
                                            </td>
                                           
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthDisabled_Note" runat="server" CssClass="inputbox_multi"
                                                     MaxLength="255" Rows="4" SkinID="MultiText" TabIndex="22"
                                                    TextMode="MultiLine" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"  ><span class="field-label">Special Medication</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthSM_Yes" runat="server" GroupName="SPMed"
                                                    Text="Yes" CssClass="field-label" />
                                                <asp:RadioButton ID="rbHthSM_No" runat="server" GroupName="SPMed" Text="No" CssClass="field-label" /></td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthSM_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                                     Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                                    MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" ><span class="field-label">Physical Education Restrictions</span></td>
                                            
                                            <td align="left" >
                                                <asp:RadioButton ID="rbHthPER_Yes" runat="server" GroupName="PhyEd"
                                                    Text="Yes" CssClass="field-label" />
                                                <asp:RadioButton ID="rbHthPER_No" runat="server" GroupName="PhyEd" Text="No" CssClass="field-label" /></td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left" >
                                                <asp:TextBox ID="txtHthPER_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                                     Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                                    MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" ><span class="field-label">Any other information related to health issue the school should be aware of?</span></td>
                                            
                                            <td align="left" >
                                                <asp:RadioButton ID="rbHthOther_yes" runat="server" GroupName="HthOther" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthOther_No" runat="server" GroupName="HthOther" Text="No" CssClass="field-label" /></td>
                                            <td align="left" ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left" >

                                                <asp:TextBox ID="txtHthOth_Note" runat="server" CssClass="inputbox_multi"
                                                     Rows="2" SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                     MaxLength="255"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td  colspan="4" class="title-bg">
                                                Applicant's disciplinary, social, 
                                        physical or psychological detail</td>


                                        </tr>
                                        <tr>
                                            <td 
                                                 align="left"><span class="field-label">Has the child received any sort of learning support or therapy?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthLS_Yes" runat="server" GroupName="LearnSp"
                                                    Text="Yes" CssClass="field-label" />
                                                <asp:RadioButton ID="rbHthLS_No" runat="server" GroupName="LearnSp" Text="No" CssClass="field-label" />
                                            </td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthLS_Note" runat="server" CssClass="inputbox_multi"
                                                     Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                     MaxLength="255"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td align="left"  ><span class="field-label">Does the child require any special education needs?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthSE_Yes" runat="server" GroupName="SPneed" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthSE_No" runat="server" GroupName="SPneed" CssClass="field-label"
                                                    Text="No" />
                                            </td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthSE_Note" runat="server" CssClass="inputbox_multi"
                                                     Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                     MaxLength="255"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"  ><span class="field-label">Does the student require English support as Additional Language program (EAL) ?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthEAL_Yes" runat="server" GroupName="EAL" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthEAL_No" runat="server" GroupName="EAL" CssClass="field-label"
                                                    Text="No" /></td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthEAL_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                                     Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                                    MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left"  ><span class="field-label">Has your child's behaviour been any cause for concern in previous schools ?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthBehv_Yes" runat="server" GroupName="Behv" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthBehv_No" runat="server" GroupName="Behv" CssClass="field-label"
                                                    Text="No" /></td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthBehv_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                                     Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                                    MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left"  ><span class="field-label">Any information related to communication & interaction that the school should be aware of ?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthComm_Yes" runat="server" GroupName="CommInt" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthComm_No" runat="server" GroupName="CommInt" CssClass="field-label"
                                                    Text="No" /></td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthCommInt_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                                     Rows="4" CssClass="inputbox_multi" SkinID="MultiText"
                                                    MaxLength="255"></asp:TextBox></td>
                                        </tr>


                                        <tr>
                                            <td  colspan="4" class="title-bg"
                                               >Gifted and talented</td>
                                        </tr>
                                        <tr>
                                            <td   align="left"><span class="field-label">Has your child ever been selected for specific enrichment activities?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthEnr_Yes" runat="server" GroupName="Enr" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthEnr_No" runat="server" GroupName="Enr" Text="No" CssClass="field-label" />
                                            </td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthEnr_note" runat="server" CssClass="inputbox_multi"
                                                     Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                     MaxLength="255"></asp:TextBox>
                                            </td>
                                        </tr>



                                        <tr>
                                            <td   align="left"><span class="field-label">Is your child musically proficient?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthMus_Yes" runat="server" GroupName="Music" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthMus_No" runat="server" GroupName="Music" Text="No"  CssClass="field-label"/>
                                            </td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthMus_Note" runat="server" CssClass="inputbox_multi"
                                                     Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                     MaxLength="255"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td   align="left"><span class="field-label">Has your child represented a school or country in sport?</span></td>
                                            
                                            <td align="left"  >
                                                <asp:RadioButton ID="rbHthSport_Yes" runat="server" GroupName="Sport" CssClass="field-label"
                                                    Text="Yes" />
                                                <asp:RadioButton ID="rbHthSport_No" runat="server" GroupName="Sport" Text="No" CssClass="field-label" />
                                            </td>
                                            <td align="left"  ><span class="field-label">Notes</span></td>
                                            
                                            <td align="left"  >
                                                <asp:TextBox ID="txtHthSport_note" runat="server" CssClass="inputbox_multi"
                                                     Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                     MaxLength="255"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td align="right"></td>
                            </tr>
                        </table>

                    </asp:View>
                    <asp:View ID="vwSalary" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
                        <table align="center"   cellpadding="5" cellspacing="0"
                            " width="100%">
                            <tbody>

                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Nursery Details(for KG1 Admissions)</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Sister Nursery</span> </td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:CheckBox ID="chkNursery" runat="server" Text="" Checked="false" /></td>
                                    <td align="left" width="20%" ><span class="field-label">Reg No</span></td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtNRegNo" runat="server"  TabIndex="22" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Unit</span> </td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlNUnit" runat="server" >
                                        </asp:DropDownList>


                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Sibling Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Siblings Exist</span> </td>
                                    
                                    <td align="left" width="30%"   >
                                        <asp:CheckBox ID="chkSibling" runat="server" Text="" Checked="false" /></td>
                                    <td align="left"  width="20%"><span class="field-label">Fee Id</span></td>
                                    
                                    <td align="left"  width="30%"  >
                                        <asp:TextBox ID="txtSRegNo" runat="server"  TabIndex="22" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Unit </span></td>
                                    
                                    <td align="left" width="30%"   >
                                        <asp:DropDownList ID="ddlSUnit" runat="server" >
                                            <asp:ListItem Value="0">GEMS ROYAL DUBAi SCHOOL</asp:ListItem>

                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2">

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4">GEMS Staff Details</td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Parent work with GEMS</span> </td>
                                    
                                    <td align="left" width="30%"  >
                                        <asp:CheckBox ID="chkStaffGEMS" runat="server" Text="" Checked="false" /></td>
                                    <td align="left" width="20%" ><span class="field-label">Staff Id</span></td>
                                    
                                    <td align="left"  width="30%" >
                                        <asp:TextBox ID="txtSPayRollId" runat="server"  TabIndex="22" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"  ><span class="field-label">Unit</span> </td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlStaffUnit" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2">

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Ex Student</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Ex Student </span></td>
                                    
                                    <td align="left"  width="30%" >
                                        <asp:CheckBox ID="chkExStudent" runat="server" Text="" Checked="false" /></td>
                                    <td align="left" width="20%"  ><span class="field-label">Fee Id</span></td>
                                    
                                    <td align="left"  width="30%"  >
                                        <asp:TextBox ID="txtERegNo" runat="server"  TabIndex="22" MaxLength="20"></asp:TextBox>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left" width="20%"   ><span class="field-label">Unit </span></td>
                                    
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlEUnit" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>

                            </tbody>
                        </table>
                    </asp:View>


                </asp:MultiView>






            </td>
           
        </tr>

        
        <tr>
           
            <td  valign="middle" align="center" >
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                    ValidationGroup="groupM1" />
                <asp:Button
                    ID="btnCancel" runat="server" CausesValidation="False"
                    CssClass="button" Text="Cancel"  /></td>

           
        </tr>
        <tr>
          
            <td align="center"  >


                <asp:RegularExpressionValidator ID="RegularExpressionValidator113" runat="server" ControlToValidate="txtTenDate"
                    Display="Dynamic" ErrorMessage="Enter the Tendative Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator113" runat="server" ControlToValidate="txtTenDate"
                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Tendative Date of Join entered is not a valid date"
                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>


                <ajaxToolkit:ModalPopupExtender ID="mpxFeeSponsor" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true" PopupControlID="plFeeSP" TargetControlID="lbtnFeeSpon">
                </ajaxToolkit:ModalPopupExtender>
                <asp:HiddenField ID="hfENQNo" runat="server" />
                <asp:HiddenField ID="hfENQ_ID" runat="server" />
                <asp:HiddenField ID="hfEQP_ID" runat="server" />
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfEQS_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSTGREQ" runat="server" />
                <asp:HiddenField ID="hfURL" runat="server" />
                <asp:HiddenField ID="HFEMP_ID" runat="server" />
                <asp:HiddenField ID="hfACCNO" runat="server" />
                <asp:HiddenField ID="hfActual_GRD" runat="server" />
                <asp:Panel ID="plFeeSP" runat="server" Width="541px" Style="display: none" BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px">
                    <uc1:FeeSponsor ID="FeeSponsor1" runat="server" />
                </asp:Panel>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEnqDate"
                    Display="Dynamic" ErrorMessage="Enter the Enquiry Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtEnqDate"
                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="DOB entered is not a valid date"
                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>


                <asp:RequiredFieldValidator ID="rfvDatefrom"
                    runat="server" ControlToValidate="txtDob" Display="Dynamic" EnableViewState="False"
                    ErrorMessage="Date of birth cannot be left empty" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtDOB"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the   Date of Birth in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtDOB"
                    CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Rejoining Date entered is not a valid date"
                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>

                <asp:RequiredFieldValidator ID="rfvPob" runat="server" ControlToValidate="txtPob"
                    Display="Dynamic" ErrorMessage="Place of birth cannot be left empty" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="rfvFname_Stud" runat="server" ControlToValidate="txtApplFirstName"
                    Display="Dynamic" ErrorMessage="First name required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="clExReg_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtregdate">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="clEnq_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnEnqDate" TargetControlID="txtEnqDate">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="clTEN_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnTenDate" TargetControlID="txtTenDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="clDOB_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnDOB" TargetControlID="txtDOB">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="clPIss_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnPIssDate" TargetControlID="txtPIssDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="clPExp_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnPExpDate" TargetControlID="txtPExpDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="clVIss_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnVIssDate" TargetControlID="txtVIssDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="clVEXp_date" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnVExpdate" TargetControlID="txtVExpDate">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="clSChFrom_dt" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgSchFrom_dt" TargetControlID="txtSchFrom_dt">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgtxtSchTo_dt" TargetControlID="txtSchTo_dt">
                </ajaxToolkit:CalendarExtender>

            </td>
           
        </tr>
    </table>

</div> 
                </div> 
        </div> 


</asp:Content>

