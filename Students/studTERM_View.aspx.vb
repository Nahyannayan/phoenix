Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studTERM_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code for Term Master
                If USR_NAME = "" Or MainMnu_code <> "S050041" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ViewState("Y_DESCR") = "All"
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    'Get the Active year
                    Using readerActive_year As SqlDataReader = GetActive_Year(Session("sBsuid"), Session("CLM"))
                        While readerActive_year.Read
                            ViewState("Y_DESCR") = Convert.ToString(readerActive_year("Y_DESCR"))
                        End While
                    End Using
                    callYEAR_DESCRBind(ViewState("Y_DESCR"))
                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String



        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))


    End Sub

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_TERM_DESCR As String = String.Empty
            Dim str_filter_STARTDT As String = String.Empty
            Dim str_filter_ENDDT As String = String.Empty
            Dim ds As New DataSet

            str_Sql = "Select TRM_ID,Y_DESCR,TRM_DESCR,STARTDT,ENDDT from(SELECT TERM_MASTER.TRM_DESCRIPTION as TRM_DESCR , TERM_MASTER.TRM_STARTDATE as STARTDT, TERM_MASTER.TRM_ENDDATE as ENDDT," & _
                        " TERM_MASTER.TRM_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR, TERM_MASTER.TRM_ACD_ID as ACD_ID,TERM_MASTER.TRM_ID as TRM_ID,ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID  FROM ACADEMICYEAR_D " & _
                        " INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                        " TERM_MASTER ON ACADEMICYEAR_D.ACD_ID = TERM_MASTER.TRM_ACD_ID)a where  a.CLM_ID='" & Session("CLM") & "' and a.BSU_ID='" & Session("sBsuid") & "' AND A.ACD_ID<>''"


            Dim lblID As New Label
            Dim txtSearch As New TextBox
            Dim ddlY_DESCRH As New DropDownList
            Dim str_search As String

            Dim str_TRM_DESCR As String = String.Empty
            Dim str_STARTDT As String = String.Empty
            Dim str_ENDDT As String = String.Empty



            If gvAcademic.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtTRM_DESCR")
                str_TRM_DESCR = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_TERM_DESCR = "  AND  NOT a.TRM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtSTARTDT")

                str_STARTDT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STARTDT = " AND a.STARTDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STARTDT = "  AND  NOT a.STARTDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STARTDT = " AND a.STARTDT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STARTDT = " AND a.STARTDT NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STARTDT = " AND a.STARTDT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STARTDT = " AND a.STARTDT NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtENDDT")

                str_ENDDT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_ENDDT = " AND a.ENDDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ENDDT = "  AND  NOT a.ENDDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ENDDT = " AND a.ENDDT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ENDDT = " AND a.ENDDT  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ENDDT = " AND a.ENDDT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ENDDT = " AND a.ENDDT NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_TERM_DESCR & str_filter_STARTDT & str_filter_ENDDT & "  order by  a.Y_DESCR")
            If ds.Tables(0).Rows.Count > 0 Then

                gvAcademic.DataSource = ds.Tables(0)
                gvAcademic.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                gvAcademic.DataSource = ds.Tables(0)
                Try
                    gvAcademic.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAcademic.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAcademic.Rows(0).Cells.Clear()
                gvAcademic.Rows(0).Cells.Add(New TableCell)
                gvAcademic.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAcademic.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAcademic.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = gvAcademic.HeaderRow.FindControl("txtTRM_DESCR")
            txtSearch.Text = str_TRM_DESCR
            txtSearch = gvAcademic.HeaderRow.FindControl("txtSTARTDT")
            txtSearch.Text = str_STARTDT
            txtSearch = gvAcademic.HeaderRow.FindControl("txtENDDT")
            txtSearch.Text = str_ENDDT
            ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH ")
            callYEAR_DESCRBind(ViewState("Y_DESCR"))
            Call ddlYear_state(ddlY_DESCRH.SelectedItem.Text)
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblTRM_ID As New Label
            Dim url As String
            Dim viewid As String

            lblTRM_ID = TryCast(sender.FindControl("lblTRM_ID"), Label)
            viewid = lblTRM_ID.Text

            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studTERM_Master.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studTERM_Master.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Sub ddlYear_state(ByVal selText As String)
        Try

            Dim ItemTypeCounter As Integer
            Dim ddlY_DESCRH As New DropDownList
            ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH")
            For ItemTypeCounter = 0 To ddlY_DESCRH.Items.Count - 1
                'keep loop until you get the counter for default Grade into  the SelectedIndex

                If ddlY_DESCRH.Items(ItemTypeCounter).Text = selText Then
                    ddlY_DESCRH.SelectedIndex = ItemTypeCounter
                End If
            Next
            '  ddlOpenonLineH.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlACY_DESCRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("Y_DESCR") = sender.SelectedItem.Text
        callYEAR_DESCRBind(ViewState("Y_DESCR"))
        gridbind()
    End Sub
    Public Sub callYEAR_DESCRBind(Optional ByVal p_selected As String = "All")

        Try
            Dim ddlY_DESCRH As New DropDownList
            'ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH")

            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = GetYear_DESCR(Session("sBsuid"), Session("CLM"))

                ddlY_DESCRH.Items.Clear()
                di = New ListItem("All", "All")
                ddlY_DESCRH.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACY_ID"))
                        ddlY_DESCRH.Items.Add(di)

                    End While
                End If
            End Using

            If p_selected <> "All" Then
                ViewState("str_filter_Year") = " AND a.Y_DESCR = '" & p_selected & "'"
            Else
                ViewState("str_filter_Year") = " AND a.Y_DESCR <>''"
            End If
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlY_DESCRH.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not ViewState("Y_DESCR") Is Nothing Then
                    If ddlY_DESCRH.Items(ItemTypeCounter).Text = ViewState("Y_DESCR") Then
                        ddlY_DESCRH.SelectedIndex = ItemTypeCounter
                    End If
                End If

            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearchTRM_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSTARTDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchENDDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvAcademic_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAcademic.PageIndexChanging
        gvAcademic.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Public Shared Function GetActive_Year(ByVal Bsu_id As String, ByVal CLM_ID As String) As SqlDataReader
       
        'Purpose--Get active year from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetActive_Year As String = ""

        sqlGetActive_Year = "SELECT   ACADEMICYEAR_M.ACY_ID as ACY_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR , ACADEMICYEAR_D.ACD_ID as ACD_ID " & _
                        " FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
                        " where ACADEMICYEAR_D.ACD_BSU_ID='" & Bsu_id & "'  and ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' and ACADEMICYEAR_D.ACD_CURRENT=1 "


        Dim command As SqlCommand = New SqlCommand(sqlGetActive_Year, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetYear_DESCR(ByVal Bsu_id As String, ByVal CLM As String) As SqlDataReader
       
        'Purpose--Get Grade data from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetYear_DESCR As String = ""

        sqlGetYear_DESCR = "Select  ACY_ID,ACD_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
                     " ACADEMICYEAR_D.ACD_CLM_ID AS CLM_ID, ACADEMICYEAR_D.ACD_ID as ACD_ID FROM ACADEMICYEAR_D INNER JOIN " & _
                     " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetYear_DESCR, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
End Class
