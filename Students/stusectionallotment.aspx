<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" EnableEventValidation="false" CodeFile="stusectionallotment.aspx.vb" Inherits="Students_stusectionallotment" Debug="true" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <script type="text/javascript">

        //function promts(a)
        //{

        //var p = confirm("Shift or Stream is different.Do you want to continue?")
        //if (p)
        //{
        //return true;
        ////window.document.getElementById(a.id).checked = true
        //}
        //else
        //{
        //return false;
        ////window.document.getElementById(a.id).checked = false
        //} 

        //}
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Student Section Allotment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%-- <table width="100%">
            <tr align="left" >
                <td class="title" >
                
                </td>
            </tr>
        </table>--%>

                <table width="100%">
                    <tr>
                        <td style="vertical-align :top;">
                            <table width="100%">
                                <tr>
                                    <td align="left"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddAccYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                  <tr>
                                    <td align="left"><span class="field-label">Stream</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddStream_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Shift</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddShift_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                              
                                <tr>
                                    <td align="left"><span class="field-label">Section</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddSection" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNo" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtStuNo">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student Name</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSAME" runat="server" BehaviorID="AutoCompleteEx2"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNAME" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtNAME">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                                             OnClick="btnSearch_Click" TabIndex="4" Text="List Students" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:CheckBox ID="ChkPrint" runat="server" Text="Print Admit Slip" Font-Size="Larger" ForeColor="Black" CssClass="field-label"></asp:CheckBox></td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                        <td valign="top">
                            <div id="Div1" width="100%">
                                <div class="msg_header">
                                    <div class="title-bg-small" align="center">
                                        Section Detail
                                    </div>
                                </div>
                                <asp:Panel ID="plSection" runat="server"
                                    ScrollBars="Auto" Width="100%" Height="500px">

                                    <asp:GridView ID="grdsectionhistory" EmptyDataText="No Records" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Section">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsct" runat="server" Text='<%#Eval("SCT_DESCR")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Capacity">
                                                <ItemTemplate>
                                                    <%#Eval("SCT_MAXSTRENGTH")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Alloted">
                                                <ItemTemplate>
                                                    <%#Eval("Alloted")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="B">
                                                <ItemTemplate>
                                                    <%#Eval("Alloted_Male")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="G">
                                                <ItemTemplate>
                                                    <%#Eval("Alloted_FeMale")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Stream">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstream" runat="server" Text='<%#Eval("stream")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="Shift">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblshift" runat="server" Text='<%#Eval("shift")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle CssClass="gridheader_pop" />
                                        <RowStyle CssClass="griditem"  />
                                        <SelectedRowStyle CssClass="Green" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>

                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridAllot" CssClass="table table-bordered table-row" EmptyDataText="No Records Found"  AutoGenerateColumns="False" runat="server" AllowPaging="True" OnPageIndexChanging="GridAllot_PageIndexChanging" PageSize="20">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="chk">
                                        <HeaderTemplate>
                                            All
                    <br />
                                            <%--<asp:CheckBox id="checkall" OnCheckedChanged="checkall_CheckedChanged" runat="server" AutoPostBack="true"></asp:CheckBox>--%><asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server" ToolTip="Click here to select/deselect all rows" __designer:wfdid="w54"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ch1" runat="server" Text="" __designer:wfdid="w53"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblno" runat="server" Text="Student ID" CssClass="gridheader_text" __designer:wfdid="w46"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenStudid" runat="server" Value='<%# Eval("STU_ID") %>' __designer:wfdid="w45"></asp:HiddenField>
                                            <%# Eval("STU_NO") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date of Join">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" Text="Date of Join" CssClass="gridheader_text"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblname" runat="server" Text="Student Name" CssClass="gridheader_text" __designer:wfdid="w48"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSname" runat="server" Text='<%# bind("SNAME") %>' __designer:wfdid="w43"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Gender">
                                        <HeaderTemplate>
                                            Gender&nbsp;<br />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGender" runat="server" Text='<%#Eval("STU_GENDER")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Stream">
                                        <HeaderTemplate>
                                            Stream&nbsp;<br />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStreams" runat="server" Text='<%#Eval("STM_DESCR")%>' __designer:wfdid="w49"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle ></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Shift">
                                        <HeaderTemplate>
                                            Shift&nbsp;<br />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblShifts" runat="server" Text='<%#Eval("SHF_DESCR")%>' __designer:wfdid="w50"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle  CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
        <br />


                <asp:HiddenField ID="hfStuIds" runat="server"></asp:HiddenField>
                <asp:Button ID="btnsave" runat="server" Text="Save" Class="button" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Class="button" Visible="False" OnClick="btnCancel_Click" />

            </div>

            <%-- <div id="plPrev"  runat="server" Style="display: none; overflow: visible; border-color: #1b80b6; border-style:solid;border-width:1px;">
         &nbsp;</div>--%>
        </div>
    </div>


</asp:Content>
