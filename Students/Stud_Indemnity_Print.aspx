<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Stud_Indemnity_Print.aspx.vb" Inherits="Students_Indemnity_Print" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style> 
        input {
            vertical-align :middle !important;
        }
    </style>
    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <script language="javascript" type="text/javascript">


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkControl") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

Indemnity Form Print </div>
 <div class="card-body">
            <div class="table-responsive m-auto">

   <%-- <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%; height: 27px;"></td>
        </tr>
    </table>--%>
    <table width="96%">


        <tr>
            <td colspan="2">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td valign="bottom" colspan="2">

                            <div >
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                        ></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="Message"></asp:ValidationSummary>
                                   
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="15%" ><span class="field-label">Select Academic Year</span></td>
                        <td>
                      
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>

                        <td align="right" >
                            <asp:RadioButton ID="rbEnq" runat="server" AutoPostBack="True" GroupName="ENQ" Text="Enquiry" CssClass="field-label"></asp:RadioButton>    
                          <asp:RadioButton ID="rbStud" runat="server" AutoPostBack="True" GroupName="ENQ" Text="Student" CssClass="field-label"></asp:RadioButton>          
                        </td>
                    </tr>
                    
                    <tr>
                        <td  valign="bottom" colspan="3" align="center">

                            <asp:Button ID="btnPrint1" runat="server" CssClass="button"
                                Text="Print"  />

                        </td>
                    </tr>
                   
                    <tr>
                        <td  valign="bottom" colspan="3">

                            <asp:GridView ID="gvAlloc" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" DataKeyNames="STU_ENQ_ID" AllowPaging="True"
                                OnPageIndexChanged="gvAlloc_PageIndexChanged">
                                <RowStyle CssClass="griditem"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSelectH" runat="server" Text="Select" CssClass="gridheader_text" __designer:wfdid="w107"></asp:Label>
                                            <br />
                                            <input id="Checkbox1" name="chkAL" onclick="change_chk_state(this);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("STU_ENQ_ID") %>' />

                                        </ItemTemplate>

                                        <HeaderStyle Width="50px" />

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID">
                                        <EditItemTemplate>
                                             
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSTU_NOH"
                                                runat="server" Text=" ID" CssClass="gridheader_text" __designer:wfdid="w127"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtID" runat="server"  __designer:wfdid="w128"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchID" OnClick="btnSearchID_Click"
                                                runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                __designer:wfdid="w129" ></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'
                                                __designer:wfdid="w126"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="studName">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblNameH"
                                                runat="server" Text="Name" CssClass="gridheader_text" __designer:wfdid="w11"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtName" runat="server" __designer:wfdid="w12"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchName" OnClick="btnSearchName_Click"
                                                runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                __designer:wfdid="w13"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'
                                                __designer:wfdid="w10"></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                             <asp:Label ID="lblGRADEH" runat="server" Text="Grade" CssClass="gridheader_text" __designer:wfdid="w42"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtGRM_DISPLAY" runat="server" Width="69px"
                                                                                __designer:wfdid="w43"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchGRM_DISPLAY" OnClick="btnSearchGRM_DISPLAY_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w44"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGRM_DISPLAY" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' __designer:wfdid="w41"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <EditItemTemplate>
                                             
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSCT_DESCRH"
                                                runat="server" Text="Status" CssClass="gridheader_text" __designer:wfdid="w38"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtstatus" runat="server" Width="87px" __designer:wfdid="w39"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSTATUS" OnClick="btnSearchSTATUS_Click"
                                                runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                __designer:wfdid="w40"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'
                                                __designer:wfdid="w37"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_ENQ_ID" Visible="False">
                                        <EditItemTemplate>
                                             
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_ENQ_ID" runat="server" Text='<%# Bind("STU_ENQ_ID") %>'
                                                __designer:wfdid="w129"></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>

                        </td>
                    </tr>
                   
                    <tr>
                        <td valign="bottom" align="center" colspan="3">
                            <asp:Button ID="btnPrint2" runat="server" CssClass="button" Text="Print"
                                ValidationGroup="Message" 
                                />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom"  colspan="3">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />

                
                <asp:HiddenField ID="hiddenStu_IDs" runat="server" />

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</div>
     </div></div>
</asp:Content>

