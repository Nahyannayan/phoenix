<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBSUJoinDoc_View.aspx.vb" Inherits="Students_studBSUJoinDoc_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Join Documents
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">



                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">


                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center">

                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="right" colspan="2" width="50%"></td>
                                            </tr>

                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:GridView ID="gvStage" runat="server"
                                                        AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                        OnRowDataBound="gvStage_RowDataBound" PageSize="20" Width="100%">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="-" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStgId" runat="server" Text='<%# Bind("PRB_STG_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>



                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                                                                        <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                                    </a>
                                                                </ItemTemplate>
                                                                <AlternatingItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
                                                                        <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                                    </a>
                                                                </AlternatingItemTemplate>
                                                            </asp:TemplateField>





                                                            <asp:TemplateField HeaderText="Stage Description">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStage" runat="server" Text='<%# Bind("PRO_DESCRIPTION") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>





                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>


                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    </td></tr>
             <tr>
                 <td colspan="100%">
                     <div id="div<%# Eval("GUID") %>" style="display: none; position: relative; left: 20px;">
                         <asp:GridView ID="gvDocs" runat="server" Width="100%" CssClass="table table-bordered table-row"
                             AutoGenerateColumns="false" EmptyDataText="No documents for this stage.">

                             <Columns>
                                 <asp:BoundField DataField="DOC_DESCR" HeaderText="Document" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="DOC_TYPE" HeaderText="Doc. Required" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="DOC_APPLY" HeaderText="Applicable To" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="DCB_COPIES" HeaderText="No.Of Copies" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>

                                 <asp:BoundField DataField="GRADES" HeaderText="Grades" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>

                             </Columns>
                             <RowStyle CssClass="griditem" />
                             <HeaderStyle />
                         </asp:GridView>
                     </div>
                 </td>
             </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <RowStyle CssClass="griditem" />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <SelectedRowStyle />
                                                        <PagerStyle HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /></td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>

