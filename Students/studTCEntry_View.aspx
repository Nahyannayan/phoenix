<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studTCEntry_View.aspx.vb" Inherits="Students_studTCEntry_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_ShowScreen" runat="server" width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Academic Year</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rdAll" Text="All" CssClass="field-label" Checked="true" GroupName="G1" runat="server" />
                                        <asp:RadioButton ID="rdNotApplied" CssClass="field-label" Text="Not Applied" GroupName="G1" runat="server" />
                                        <asp:RadioButton ID="rdApplied" CssClass="field-label" Text="Applied" GroupName="G1" runat="server" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Grade</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Select Section</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" Width="71px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Student ID</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Student Name</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    </td>
                                    <%--<td>
                            
                        </td>--%>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="  Withdrawal Entry">
                                                    <HeaderTemplate>
                                                        Withdrawal Entry
                                                   <br />
                                                        <asp:DropDownList ID="ddlgvEntry" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvEntry_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>Online</asp:ListItem>
                                                            <asp:ListItem>School</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEntryType" runat="server" Text='<%# Bind("ENTRYTYPE") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblType" runat="server" OnClick="lblType_Click" Text='<%# Bind("type") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>
