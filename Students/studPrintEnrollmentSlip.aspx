<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studPrintEnrollmentSlip.aspx.vb" Inherits="Students_studPrintEnrollmentSlip" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
           function CheckForPrint() {
               if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';

                   window.open('../Reports/ASPX Report/rptReportViewerModel.aspx?paging=1', "_blank", "toolbar=no,scrollbars=yes, resizable=yes, top=10, left=200, width=800, height=600");
               }
        }
            );

           var color = '';
           function highlight(obj) {
               var rowObject = getParentRow(obj);
               var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {

    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            if (document.forms[0].elements[i].disabled == false) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
            if (chkstate = true) {

            }
        }
    }
}
</script>
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

     <asp:Label ID="lblTitle" runat="server">Print Enrollment Slip</asp:Label> </div>
     <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                  <%--  <tr style="font-size: 12pt;">

                        <td width="50%" align="left" class="title" style="height: 50px"></td>
                    </tr>--%>

                    <tr>
                        <td align="left"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center"  cellpadding="0" cellspacing="0" width="100%">


                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Academic Year</span> </td>
                                   
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                      <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                                  
                                    <td align="left"   >
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                               
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Student ID</span></td>
                                   
                                    <td align="left"  >
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left" width="20%" ><span class="field-label">Student Name</span></td>
                                  
                                    <td align="left"  >
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                                   
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                    
                                </tr>
                                <tr>

                                    <td align="center"  colspan="4" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" >
                                            <RowStyle CssClass="griditem"  />
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Select<br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                        <%--<table>
                                                            <tr>
                                                                <td align="center"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    </td>
                                                            </tr>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuNo" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo"  runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle  CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="8" valign="top">
                                        <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button" TabIndex="4"  /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_print" runat="server" />

                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>            
    </div>
         </div>
    </div>
</asp:Content>

