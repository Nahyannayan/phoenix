<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="StudentLostCard.aspx.vb" Inherits="Students_StudentLostCard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

<%--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--%>

     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        function getStudent() {
            var sFeatures, url;
            sFeatures = "dialogWidth: 800px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var BsuId = '<%= Session("sBSuid") %>'
            var TYPE = 'S';
            var selType = 's';
            //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
            url = "../Fees/ShowStudent.aspx?TYPE=STUD_BSU_SRV&MULTI_SEL=false&bsu=" + BsuId;
            return ShowWindowWithClose(url, 'search', '55%', '85%')
       
            return false;

           <%-- var NameandCode;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=txtStudIDs.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = NameandCode[0];
            }
            return true;--%>
        }

        function SetLostStudentsParentValue(result) {
            NameandCode = result.split('||');
            document.getElementById('<%=txtStudIDs.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STU_IDs.ClientID %>').value = NameandCode[0];        

            CloseFrame();
            $("#<%=lblbtn.ClientID%>").click();
            //return false;
        }

        function CloseFrame() {
            jQuery.fancybox.close();
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Report Lost Card
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%-- <tr>
            <td colspan="6" class="subheader_img">
                Report Lost Card
            </td>
        </tr>--%>
                    <tr align="left">
                        <td width="20%">&nbsp;<span class="field-label">Student</span>
                        </td>

                        <td width="30%">
                            <asp:TextBox ID="txtStudIDs" runat="server" AutoPostBack="True">
                            </asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClick="imgStudent_Click"
                    OnClientClick="getStudent();"></asp:ImageButton>    <asp:Button ID="lblbtn" runat="server" style="display: none" />
                        </td>

                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr align="left" runat="server" id="tr_StudentDet">
                        <td colspan="4">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="20%"><span class="field-label">Student No</span>
                                    </td>

                                    <td width="30%">
                                        <asp:Label ID="lblStdNo" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td width="20%"><span class="field-label">Student Name</span>
                                    </td>
                                    <td width="30%">
                                        <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left">
                                        <asp:Label ID="lblacademicyear" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left"><span class="field-label">Grade</span> &amp; <span class="field-label">Section</span>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-label"></asp:Label>&<asp:Label
                                            ID="lblsect" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">School</span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblschoolname" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td width="20%">&nbsp;<span class="field-label">Remarks</span>
                        </td>

                        <td colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" id="tr_Status" runat="server">
                        <td width="20%"><span class="field-label">Current Status</span>
                        </td>

                        <td width="30%">
                            <asp:Label ID="lblReqStatus" runat="server"></asp:Label>
                            <b>
                                <asp:LinkButton ID="lnkRequestNewCard" CausesValidation="false" runat="server" Visible="False">Request New Card</asp:LinkButton></b>
                        </td>

                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr id="Tr1" visible="false" align="left" runat="server">
                        <td width="20%">&nbsp;<span class="field-label">Request for New card</span>
                        </td>
                       
                        <td  colspan="3">
                           <span class="field-label"> <asp:CheckBox ID="chkRequest" runat="server" /></span>
                            <asp:Label ID="lblNewCardRate" Visible="false" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnReport" runat="server" CausesValidation="False" Text="Report Lost Card"
                                CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">&nbsp;
                <asp:Label ID="lblerror" runat="server" EnableViewState="False" class="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnSLCID" runat="server" />
                <asp:HiddenField ID="hdnStatus" runat="server" />
                <asp:HiddenField ID="hdnRequestNew" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />

            </div>
        </div>
    </div>
    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>
