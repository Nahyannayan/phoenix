<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studGrade_Pending.aspx.vb" Inherits="Students_studGrade_Att" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">


        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtDate.ClientID %>').value, "", sFeatures)


            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }
            if (mode == 1)
                document.getElementById('<%=txtDate.ClientID %>').value = result;
           return true;
       }




       function test1(val) {
           var path;
           if (val == 'LI') {
               path = '../Images/operations/like.gif';
           } else if (val == 'NLI') {
               path = '../Images/operations/notlike.gif';
           } else if (val == 'SW') {
               path = '../Images/operations/startswith.gif';
           } else if (val == 'NSW') {
               path = '../Images/operations/notstartwith.gif';
           } else if (val == 'EW') {
               path = '../Images/operations/endswith.gif';
           } else if (val == 'NEW') {
               path = '../Images/operations/notendswith.gif';
           }
           document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
                }
                function test2(val) {
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
                }




    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Pending Attendance(Grade Wise)"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
              PENDING ATTENDANCE(GRADE WISE)</td>
        </tr>
    </table>--%>

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="6" style="height: 16px">
                            <asp:Literal id="ltLabel" runat="server" Text="Pending Attendance"></asp:Literal></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True" OnTextChanged="txtDate_TextChanged">
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                          OnClientClick="javascript:return false;"   ></asp:ImageButton><%--OnClientClick="getDate(1);return true;"--%>
                                           <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar" CssClass="MyCalendar"
                TargetControlID="txtDate">
            </ajaxToolkit:CalendarExtender>

                                      <%--  <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage=" Date required"
                                            ValidationGroup="AttGroup">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                ID="revDocDate" runat="server" ControlToValidate="txtDate" CssClass="error" Display="Dynamic"
                                                EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="AttGroup" ForeColor="">*</asp:RegularExpressionValidator>--%>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <span class="field-label">Att. Type</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Check For</span></td>

                                    <td align="left" colspan="3">
                                        <asp:RadioButton ID="rbPending" runat="server" AutoPostBack="True" GroupName="Status"
                                            Text="Pending Attendance" OnCheckedChanged="rbPending_CheckedChanged" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbMarked" runat="server" AutoPostBack="True" GroupName="Status"
                                            Text="Attendance Marked" OnCheckedChanged="rbMarked_CheckedChanged" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbFormTutor" runat="server" AutoPostBack="True"
                                            GroupName="Status" Text="Form Tutor Marked" CssClass="field-label" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvAttendance" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records." Width="100%" 
                                UseAccessibleHeader="False" AllowPaging="True" PageSize="10">
                                <RowStyle CssClass="griditem"  />
                                <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox8" runat="server" ></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                           
                                                            <asp:Label ID="lblGradeH" runat="server" Text="Grade" CssClass="gridheader_text"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox9" runat="server" ></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>                                            
                                                            <asp:Label ID="lblCOMP_ADDRH" runat="server" Text="Section" CssClass="gridheader_text" ></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("Section") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Authorized Staff">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" ></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStaff" runat="server" Text='<%# bind("EMP_NAME") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Att. Type">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" ></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>                                          
                                                            <asp:Label ID="lblCityH" runat="server" Text="Att. Type"  ></asp:Label>                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("Att_Type") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle ></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DT_TIME" HeaderText="Created Date &amp; Time">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle  />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                          
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                                type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden"
                                                    value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>



            </div>
        </div>
    </div>

</asp:Content>

