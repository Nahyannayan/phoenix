Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAttendance_M_view
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim menu_rights As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'hardcode the menu code
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "S050100" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    'Dim ddlY_DESCRH As New DropDownList
                    'ddlY_DESCRH = gvATTRecord.HeaderRow.FindControl("ddlY_DESCRH")
                    'ViewState("Y_DESCR") = ddlY_DESCRH.SelectedItem.Text
                    'ddlY_DESCRH.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                    ViewState("str_filter_Year") = ""
                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvATTRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvATTRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvATTRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvATTRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvATTRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvATTRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_FROMDT As String = String.Empty
            Dim str_filter_TODT As String = String.Empty
            Dim str_filter_GRADE As String = String.Empty
            Dim CurBsUnit As String = Session("sBsuid")
            Dim ds As New DataSet

            str_Sql = " select * from(SELECT DISTINCT    ATTENDANCE_M.ATT_ID, ATTENDANCE_M.ATT_ACD_ID AS ACD_ID, ATTENDANCE_M.ATT_GRD_ID AS GRD_ID, ATTENDANCE_M.ATT_FROMDT AS FROMDT, " & _
                     " ATTENDANCE_M.ATT_TODT AS TODT, ATTENDANCE_M.ATT_TYPE, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, ACADEMICYEAR_M.ACY_ID, " & _
                     " ATTENDANCE_M.ATT_BSU_ID AS BSU_ID, GRADE_BSU_M.GRM_DISPLAY AS GRD_DESCR, GRADE_M.GRD_DISPLAYORDER AS GRD_ORDER " & _
                     " FROM  ACADEMICYEAR_D INNER JOIN   ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                     " ATTENDANCE_M ON ACADEMICYEAR_D.ACD_ID = ATTENDANCE_M.ATT_ACD_ID INNER JOIN " & _
                     " GRADE_BSU_M ON ATTENDANCE_M.ATT_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                     " ATTENDANCE_M.ATT_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID)a where a.BSU_ID='" & CurBsUnit & "' and  A.ACD_ID<>''"


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
        
            Dim str_FROMDT As String = String.Empty
            Dim str_TODT As String = String.Empty
            Dim str_GRADE As String = String.Empty
            If gvATTRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String


                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvATTRecord.HeaderRow.FindControl("txtFROMDT")
                str_FROMDT = txtSearch.Text
                Dim FROMDT As String = "  replace(CONVERT( CHAR(12), isnull(a.FROMDT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_FROMDT = " AND " & FROMDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FROMDT = "  AND  NOT " & FROMDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FROMDT = " AND " & FROMDT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvATTRecord.HeaderRow.FindControl("txtTODT")
                str_TODT = txtSearch.Text
                Dim TODT As String = "  replace(CONVERT( CHAR(12), isnull(a.TODT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_TODT = " AND " & TODT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_TODT = "  AND  NOT " & TODT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_TODT = " AND " & TODT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_TODT = " AND " & TODT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_TODT = " AND " & TODT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_TODT = " AND " & TODT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvATTRecord.HeaderRow.FindControl("txtGRD_DESCR")
                str_GRADE = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRADE = " AND isnull(a.GRD_DESCR,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRADE = "  AND  NOT isnull(a.GRD_DESCR,'')  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRADE = " AND isnull(a.GRD_DESCR,'')   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRADE = " AND isnull(a.GRD_DESCR,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRADE = " AND isnull(a.GRD_DESCR,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRADE = " AND isnull(a.GRD_DESCR,'')  NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If
            'Dim ddlY_DESCRH As New DropDownList
            'ddlY_DESCRH = gvATTRecord.HeaderRow.FindControl("ddlY_DESCRH")
            'ViewState("Y_DESCR") = ddlY_DESCRH.SelectedItem.Text

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & ViewState("str_filter_Year") & str_filter_GRADE & str_filter_FROMDT & str_filter_TODT & " ORDER BY a.Y_DESCR desc ,a.GRD_ORDER")
            If ds.Tables(0).Rows.Count > 0 Then
                gvATTRecord.DataSource = ds.Tables(0)
                gvATTRecord.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True
                gvATTRecord.DataSource = ds.Tables(0)
                Try
                    gvATTRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvATTRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvATTRecord.Rows(0).Cells.Clear()
                gvATTRecord.Rows(0).Cells.Add(New TableCell)
                gvATTRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvATTRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvATTRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvATTRecord.HeaderRow.FindControl("txtFROMDT")
            txtSearch.Text = str_FROMDT
            txtSearch = gvATTRecord.HeaderRow.FindControl("txtTODT")
            txtSearch.Text = str_TODT
            txtSearch = gvATTRecord.HeaderRow.FindControl("txtGRD_DESCR")
            txtSearch.Text = str_GRADE
            Call callYEAR_DESCRBind()

            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlACY_DESCRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("Y_DESCR") = sender.SelectedItem.Text
        callYEAR_DESCRBind()
        gridbind()
    End Sub
    Public Sub callYEAR_DESCRBind()

        Try

            Dim ddlY_DESCRH As New DropDownList
            ddlY_DESCRH = gvATTRecord.HeaderRow.FindControl("ddlY_DESCRH")
            'ddlY_DESCRH.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            Dim di As ListItem

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet



            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
         " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
         " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlY_DESCRH.Items.Clear()

            ddlY_DESCRH.DataSource = ds.Tables(0)
            ddlY_DESCRH.DataTextField = "ACY_DESCR"
            ddlY_DESCRH.DataValueField = "ACD_ID"
            ddlY_DESCRH.DataBind()
            ' di = New ListItem("All", "All")
            'ddlY_DESCRH.Items.Add(di)
            Dim p_selected As String = String.Empty

            If ViewState("str_filter_Year") <> "" Then
                ViewState("str_filter_Year") = " AND a.Y_DESCR = '" & ViewState("Y_DESCR") & "'"
            Else
                p_selected = ddlY_DESCRH.SelectedItem.Text
                ViewState("str_filter_Year") = " AND a.Y_DESCR= '" & p_selected & "'"
                ddlY_DESCRH.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlY_DESCRH.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not ViewState("Y_DESCR") Is Nothing Then
                    If ddlY_DESCRH.Items(ItemTypeCounter).Text = ViewState("Y_DESCR") Then
                        ddlY_DESCRH.SelectedIndex = ItemTypeCounter
                    End If
                End If

            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearchFromDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchGRD_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvATTRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvATTRecord.PageIndexChanging
        gvATTRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblATT_ID As New Label
            Dim url As String
            Dim viewid As String
            lblATT_ID = TryCast(sender.FindControl("lblATT_ID"), Label)
            viewid = lblATT_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studAttendance_M_Edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studAttendance_M_Edit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

   
End Class
