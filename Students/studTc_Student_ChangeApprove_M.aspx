<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTc_Student_ChangeApprove_M.aspx.vb" Inherits="Students_studTc_Student_ChangeApprove_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">



        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Student Details             
        </div>
        <div class="card-body">
            <div class="table-responsive">

    
    <table id="tbl_AddGroup" runat="server" align="center" width="100%" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:"  ValidationGroup="groupM1" />
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
        <tr>
            <td>
               
                <table ID="tbTCSO" runat="server" width="100%" CellPadding="4" CellSpacing="0">
                     <tr><td colspan="4" align="center">Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory      
        </td></tr>  
                    
                    
                    <tr>
                        <td align="left"><span class="field-label">Student Id(Fees)</span></td>
                        
                        <td align="left">  <asp:TextBox ID="txtStudID_Fee" runat="server"></asp:TextBox></td>
                        <td align="left"><span class="field-label">Date of Join</span></td>
                      
                        <td align="left"><asp:TextBox ID="txtDoJ" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr >
                        <td align="left"><span class="field-label">Grade</span></td>
                        
                        <td align="left"> <asp:TextBox ID="txtGrade" runat="server" Style="position: relative" Width="34px"></asp:TextBox></td>
                        <td align="left"><span class="field-label">Section</span></td>
                        
                        <td align="left"><asp:TextBox ID="txtSection" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                      <td align="left"><span class="field-label">Name</span></td>
                      
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtName" runat="server" Width="418px"></asp:TextBox>&nbsp;&nbsp; &nbsp;
                        </td>
                        
                        <td></td>
                    </tr>
                    <tr>
                                   
                    <td colspan="4" Class="title-bg">  
                      TC Details 
                    </td>
                    
                    
                    </tr>
                    <tr >
                   <td align="left">
                       <span class="field-label">Tc Ref No</span></td>
                   
                   <td align="left"><asp:TextBox ID="txtRefNo" runat="server" Enabled="False"></asp:TextBox></td>
                   <td align="left"> <span class="field-label">Tc Apply Date</span></td>
                   
                   <td align="left"><asp:TextBox ID="txtApplyDate" runat="server" Enabled="False"></asp:TextBox>
                           &nbsp;&nbsp;&nbsp;
                   </td>
                    </tr>
                    <tr >
                    <td align="left"><span class="field-label">Last Attendance Date</span></td>
                    
                    <td align="left"> <asp:TextBox ID="txtLast_Attend" runat="server" Enabled="False"></asp:TextBox>
                            &nbsp;&nbsp;
                    </td>
                  <td align="left"><span class="field-label">Leaving Date</span></td>
                 
                 <td align="left"><asp:TextBox ID="txtLeave_Date" runat="server" Enabled="False"></asp:TextBox>
                            &nbsp;&nbsp;</td>
                    </tr>
                    
                    <tr >
                        <td align="left">
                            <span class="field-label">Transfer Type</span></td>
                     
                     <td align="left"> <asp:TextBox ID="txtTCtype" runat="server"
                     Enabled="False"></asp:TextBox></td>
                      <td align="left"><span class="field-label">Change Reason</span></td>
                     
                     <td align="left"> 
                         <asp:RadioButton ID="rbDataEntry" runat="server"  
                             GroupName="reason" Text="Data Entry Mistake" CssClass="field-label" Enabled="false" />
                         <asp:RadioButton ID="rbParentRequest" runat="server" CssClass="field-label" GroupName="reason" 
                             Text="Parent Request" Enabled="false"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Requesting For Tc</span><span style="color: #c00000">*</span></td>
                        
                        <td align="left"><asp:TextBox ID="txtTCTypeReq" runat="server" Width="151px" Enabled="False">
                        </asp:TextBox></td>
                        <td align="left" style="width: 486px">
                            <span class="field-label">Reason for Change</span></td>
                        
                        <td align="left">
                            <asp:TextBox id="txtReason" runat="server" Height="67px" SkinID="MultiText"
                                TextMode="MultiLine" Width="201px" ReadOnly="True"></asp:TextBox></td>
                    </tr>
                       <tr ID="r12" runat="server" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="2px">
                      <td align="left">
                          <span class="field-label">Request Date</span></td>
                      
                      <td align="left">
                       <asp:TextBox ID="txtReqDate" runat="server"></asp:TextBox>
                          </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Approval Date</span></td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtAprDate" runat="server" Width="110px"></asp:TextBox>&nbsp;<asp:ImageButton id="imgAprDate" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton></td>
                        <td align="left">
                            <span class="field-label">Remarks</span></td>
                        
                        <td align="left"><asp:TextBox id="txtRemarks" runat="server" Height="67px" SkinID="MultiText"
                                TextMode="MultiLine">
                        </asp:TextBox></td>
                    </tr>
                    
                 
                </table>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtReqDate"
                    Display="None" ErrorMessage="Please enter data in the field Approval Date" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" />
                </td>
        </tr>
        <tr>
            <td><asp:HiddenField ID="hfACD_ID" runat="server" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;&nbsp;
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgAprDate" TargetControlID="txtAprDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtAprDate" TargetControlID="txtAprDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfTCM_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" /><asp:HiddenField ID="hfSTT_ID" runat="server" />
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                <asp:HiddenField ID="hfSCT_ID" runat="server" /><asp:HiddenField ID="hfTCTAPR_CODE" runat="server" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>

                </div>
            </div>
        </div>


</asp:Content>

