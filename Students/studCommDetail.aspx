<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studCommDetail.aspx.vb" Inherits="Students_studCommDetail" title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users"></i>  <asp:Literal id="ltLabel" runat="server" Text="Search"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_AddGroup" runat="server" align="center" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left">
               
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
              
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;Fields Marked with (<span class="text-danger">*</span>) are mandatory</td>
        </tr>
        <tr>
            <td align="center">
                <table align="center"  cellpadding="5" cellspacing="0"  width="100%">
                   
                    <tr>
                        <td align="left">
                            <span class="field-label">Grade</span><span class="text-danger">*</span></td>
                        <td align="left">
                            <span class="field-label">Section</span><span class="text-danger">*</span></td>
                        <td align="left">
                            <span class="field-label">Fee Id</span></td>
                        <td align="left">
                            <span class="field-label">Name</span></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left">
                            <asp:DropDownList id="ddlSection" runat="server">
                            </asp:DropDownList></td>
                        <td align="left">
                            <asp:TextBox id="txtFeeID" runat="server">
                            </asp:TextBox></td>
                        <td align="left">
                            <asp:TextBox id="txtSname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                           <span class="field-label">Select</span><font class="text-danger">*</font><asp:RadioButton id="rbFather" class="field-label" runat="server"
                                GroupName="Parent" Text="Father"></asp:RadioButton>
                                <asp:RadioButton id="rbMother" class="field-label" runat="server" GroupName="Parent" Text="Mother">
                                </asp:RadioButton>
                                <asp:RadioButton id="rbGuardian" class="field-label" runat="server" GroupName="Parent" Text="Guardian">
                                </asp:RadioButton>
                            
                        </td>
                        <td align="left">
                            <asp:RadioButton id="rbStartWith" runat="server" class="field-label" GroupName="SName" Text="Starting With">
                            </asp:RadioButton>
                            <asp:RadioButton id="rbWithin" runat="server"  class="field-label" GroupName="SName" Text="Within">
                            </asp:RadioButton></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <br />
                <asp:Button id="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                    Text="Search" OnClick="btnSearch_Click" /></td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:GridView id="gvInfo" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" Height="100%" Width="100%" EmptyDataText="No record available for the current search" OnRowDataBound="gvInfo_RowDataBound">
                    <rowstyle cssclass="griditem" />
                    <columns>
<asp:TemplateField HeaderText="Fee Id"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblFEE_ID" runat="server" Text='<%# bind("FEE_ID") %>' __designer:wfdid="w3"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student ID" Visible="False"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblStu_ID" runat="server" Text='<%# Bind("STU_ID") %>' __designer:wfdid="w50"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblSname" runat="server" Text='<%# Bind("SNAME") %>' __designer:wfdid="w53"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Pob#"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtPob" runat="server" Text='<%# Bind("Pob") %>' __designer:wfdid="w54" MaxLength="10"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtPob" runat="server" ValidationGroup="groupM1" __designer:wfdid="w55" ErrorMessage="Invalid Pob #" Display="Dynamic" ControlToValidate="txtPob" ValidationExpression="^[0-9]{1,10}$">Invalid Pob #</asp:RegularExpressionValidator> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Emirate"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
&nbsp;<asp:DropDownList id="ddlEmirate" runat="server" __designer:wfdid="w57"></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Res. Country Code"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtResC_Code" runat="server" Text='<%# bind("RCOUNTRY_CODE") %>'  __designer:wfdid="w59" MaxLength="3"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtRCountry_Code" runat="server" ValidationGroup="groupM1" __designer:wfdid="w60" ErrorMessage="Invalid Country Code" Display="Dynamic" ControlToValidate="txtResC_Code" ValidationExpression="^[0-9]{1,3}$">Invalid Country Code</asp:RegularExpressionValidator> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Res. Area Code"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtResA_code" runat="server" Text='<%# Bind("RAREA_CODE") %>'  __designer:wfdid="w62" MaxLength="4"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtRAreaCode" runat="server" ValidationGroup="groupM1" __designer:wfdid="w63" ErrorMessage="Invalid Area Code" Display="Dynamic" ControlToValidate="txtResA_code" ValidationExpression="^[0-9]{1,4}$">Invalid Area Code</asp:RegularExpressionValidator> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Res. Contact No"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtRContact_No" runat="server" Text='<%# Bind("RCONTACT_NO") %>'  __designer:wfdid="w65" MaxLength="10"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtRcontact" runat="server" ValidationGroup="groupM1" __designer:wfdid="w66" ErrorMessage="Invalid Contact No" Display="Dynamic" ControlToValidate="txtRContact_No" ValidationExpression="^[0-9]{1,10}$">Invalid Contact No</asp:RegularExpressionValidator> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mob. Country Code"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtMCountry_Code" runat="server" Text='<%# Bind("MCOUNTRY_CODE") %>' Width="49px" __designer:wfdid="w68" MaxLength="3"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtMCountry_Code" runat="server" ValidationGroup="groupM1" __designer:wfdid="w69" ErrorMessage="Invalid Country Code" Display="Dynamic" ControlToValidate="txtMCountry_Code" ValidationExpression="^[0-9]{1,3}$">Invalid Country Code</asp:RegularExpressionValidator> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mob. Area Code"><EditItemTemplate>
&nbsp;
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtMArea_Code" runat="server" Text='<%# Bind("MAREA_CODE") %>' __designer:wfdid="w71" MaxLength="4"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtMAreaCode" runat="server" ValidationGroup="groupM1" __designer:wfdid="w72" ErrorMessage="Invalid Area Code" Display="Dynamic" ControlToValidate="txtMArea_Code" ValidationExpression="^[0-9]{1,4}$">Invalid Area Code</asp:RegularExpressionValidator> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mob. Contact No"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtMCont_No" runat="server" Text='<%# Bind("MCONTACT_NO") %>' __designer:wfdid="w6" MaxLength="10"></asp:TextBox> <asp:RegularExpressionValidator id="revtxtMcontact" runat="server" ValidationGroup="groupM1" __designer:wfdid="w7" ValidationExpression="^[0-9]{1,10}$" ControlToValidate="txtMCont_No" Display="Dynamic" ErrorMessage="Invalid Contact No">Invalid Contact No</asp:RegularExpressionValidator> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Email"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="txtEmail" runat="server" Text='<%# Bind("Email") %>' __designer:wfdid="w8"></asp:TextBox> <asp:RegularExpressionValidator id="revEmail" runat="server" __designer:dtid="10696049115004929" ValidationGroup="groupM1" __designer:wfdid="w9" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Invalid Email Address">Invalid Email Address</asp:RegularExpressionValidator> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <selectedrowstyle cssclass="Green" />
                    <headerstyle cssclass="gridheader_pop" horizontalalign="Center" verticalalign="Middle" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView></td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button id="btnSave" runat="server" CssClass="button" onclick="btnSave_Click"
                    Text="Save" ValidationGroup="groupM1" />
            </td>
        </tr>
        
    </table>

              </div>
        </div>
    </div>

</asp:Content>

