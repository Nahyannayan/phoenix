<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBB_CheckedBy_M.aspx.vb" Inherits="Students_studBB_CheckedBy_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>Blue Book - Checker
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />

                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  valign="top">
                            <table align="center"width="100%" cellpadding="5" cellspacing="0"
                               >

                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Academic Year</span> </td>
                                   
                                    <td align="left" width="20%" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%" ><span class="field-label">Grade</span></td>
                                   
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%"><span class="field-label">Shift</span></td>

                                
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Stream</span> </td>
                                 
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Section</span> </td>
                                    
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%" ><span class="field-label">Teacher</span> </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlClassTeacher" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <asp:HiddenField ID="hfSEC_ID" runat="server" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

