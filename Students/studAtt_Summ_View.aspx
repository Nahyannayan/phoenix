<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAtt_Summ_View.aspx.vb" Inherits="Students_Reports_ASPX_rptAgeStatistics" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Attendance Summary"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                   <td colspan="2"></td>
                                </tr>
                                <tr>
                                     <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDt" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgFromDt" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="calFromDt" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgFromDt" TargetControlID="txtFromDt">
                                        </ajaxToolkit:CalendarExtender>
                                    </td> 
                                    <td align="left"><span class="field-label">To Date</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTODT" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgTODT" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="calTODT" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgTODT" TargetControlID="txtTODT">
                                        </ajaxToolkit:CalendarExtender>
                                    </td> 
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" >
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="View Summary"
                                        ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFromDT" TargetControlID="txtFROMDT">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgTODT" TargetControlID="txtTODT">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
                

                
            </div>
        </div>
    </div>
</asp:Content>

