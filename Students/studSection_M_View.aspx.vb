Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studSection_M_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Session("SelectedRow") = -1
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "S050040" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    GridBind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    
    'Private Sub GridBind()
    '    Try
    '        Dim ddlgvGrade As New DropDownList
    '        Dim ddlgvSection As New DropDownList

    '        Dim dv As New DataView
    '        Dim selectedGrade As String = ""

    '        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
    '        Dim str_query As String = "SELECT sct_id,sct_grm_id,sct_descr,sct_maxstrength,sct_currstrength FROM" _
    '                                & " section_m,grade_m WHERE section_m.sct_grm_id=grade_m.grd_id and " _
    '                                & " sct_acd_id = " + ViewState("SelectedYear").ToString + " AND sct_bsu_id='" + Session("Sbsuid").ToString + "' ORDER BY grd_displayorder"
    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        dv = ds.Tables(0).DefaultView
    '        Dim selectedSection As String = ""

    '        Dim str_filter As String = ""

    '        If gvStudSection.Rows.Count > 0 Then
    '            ddlgvGrade = gvStudSection.HeaderRow.FindControl("ddlgvGrade")
    '            If ddlgvGrade.Text <> "ALL" Then
    '                '  dv.RowFilter = "sct_grm_id='" + ddlgvGrade.Text + "'"
    '                str_filter = "sct_grm_id='" + ddlgvGrade.Text + "'"
    '                selectedGrade = ddlgvGrade.Text
    '            End If

    '            ddlgvSection = gvStudSection.HeaderRow.FindControl("ddlgvSection")

    '            If ddlgvSection.Text <> "ALL" Then
    '                If str_filter = "" Then
    '                    str_filter = "sct_descr='" + ddlgvSection.Text + "'"
    '                Else
    '                    str_filter = str_filter + " and sct_descr='" + ddlgvSection.Text + "'"
    '                End If

    '                selectedSection = ddlgvSection.Text
    '            End If


    '            If ddlgvGrade.Text = "ALL" And ddlgvSection.Text = "ALL" Then
    '                dv = New DataView
    '                dv = ds.Tables(0).DefaultView
    '            Else
    '                dv.RowFilter = str_filter
    '            End If

    '        End If

    '        gvStudSection.DataSource = dv
    '        gvStudSection.DataBind()
    '        If gvStudSection.Rows.Count > 0 Then
    '            str_query = "SELECT distinct(sct_grm_id),grd_displayorder FROM section_m,grade_m " _
    '                        & " WHERE section_m.sct_grm_id=grade_m.grd_id and sct_acd_id=" + ViewState("SelectedYear").ToString + " AND sct_bsu_id='" + Session("Sbsuid").ToString + "' ORDER BY grd_displayorder"
    '            ddlgvGrade = gvStudSection.HeaderRow.FindControl("ddlgvGrade")
    '            ddlgvGrade.Items.Clear()
    '            ddlgvGrade.Items.Add("ALL")
    '            Dim reader As SqlDataReader
    '            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '            While reader.Read
    '                ddlgvGrade.Items.Add(reader.GetString(0))
    '            End While
    '            reader.Close()
    '            If selectedGrade <> "" Then
    '                ddlgvGrade.Text = selectedGrade
    '            End If
    '            str_query = "SELECT distinct(sct_descr)FROM section_m " _
    '                        & " WHERE  sct_acd_id=" + ViewState("SelectedYear").ToString + " AND sct_bsu_id='" + Session("Sbsuid") + "'"
    '            ddlgvSection = gvStudSection.HeaderRow.FindControl("ddlgvSection")
    '            ddlgvSection.Items.Clear()
    '            ddlgvSection.Items.Add("ALL")
    '            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '            While reader.Read
    '                ddlgvSection.Items.Add(reader.GetString(0))
    '            End While
    '            reader.Close()
    '            If selectedSection <> "" Then
    '                ddlgvSection.Text = selectedSection
    '            End If
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try
    'End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studSection_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudSection_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudSection.PageIndexChanging
        Try
            gvStudSection.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

  
    Protected Sub gvStudSection_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudSection.RowCommand
        Try

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudSection.Rows(index), GridViewRow)
            Dim sct_id As New Label
            Dim url As String
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            Dim lblGrdid As New Label
            Dim lblGrade As New Label
            Dim lblShift As New Label
            Dim lblStream As New Label
            If e.CommandName = "View" Then
                sct_id = selectedRow.Cells(0).FindControl("lblsct_id")
                lblGrdid = selectedRow.FindControl("lblgrdId")
                lblGrade = selectedRow.Cells(2).FindControl("lblGrade")
                lblShift = selectedRow.Cells(3).FindControl("lblShiftID")
                lblStream = selectedRow.Cells(4).FindControl("lblStreamID")
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim editString As String = ddlAcademicYear.SelectedItem.Text + "|" + ddlAcademicYear.SelectedValue.ToString + "|" _
                                             & lblGrade.Text + "|" + lblGrdid.Text + "|" + lblStream.Text + "|" + lblShift.Text
                editString = Encr_decrData.Encrypt(editString)
                url = String.Format("~\Students\studSection_M.aspx?MainMnu_code={0}&datamode={1}&editstring=" + editString, ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ViewState("SelectedYear") = ddlAcademicYear.SelectedValue
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub GridBind()

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvStream As New DropDownList


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedShift As String = ""
        Dim selectedStream As String = ""

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT sct_id,sct_grm_id,sct_descr,grm_display,shf_descr," _
                                & "stm_descr, sct_maxstrength, grd_displayorder,sct_grd_id,STM_ID,SHF_ID " _
                                & "FROM section_m, grade_m, grade_bsu_m, shifts_m, stream_m " _
                                & "WHERE section_m.sct_grm_id = grade_bsu_m.grm_id " _
                                & "AND grade_bsu_m.grm_grd_id=grade_m.grd_id AND " _
                                & "grade_bsu_m.grm_shf_id=shifts_m.shf_id AND  " _
                                & "grade_bsu_m.grm_stm_id = stream_m.stm_id AND " _
                                & "sct_acd_id = " + ddlAcademicYear.SelectedValue + " AND sct_bsu_id='" + Session("Sbsuid").ToString + "'"



        Dim str_filter As String = ""

        If gvStudSection.Rows.Count > 0 Then

            ddlgvGrade = gvStudSection.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" Then
                str_filter = " and grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            ddlgvShift = gvStudSection.HeaderRow.FindControl("ddlgvShift")
            If ddlgvShift.Text <> "ALL" Then

                str_filter = str_filter + " and shf_descr='" + ddlgvShift.Text + "'"


                selectedShift = ddlgvShift.Text
            End If

            ddlgvStream = gvStudSection.HeaderRow.FindControl("ddlgvStream")
            If ddlgvStream.Text <> "ALL" Then

                str_filter = str_filter + " and stm_descr='" + ddlgvStream.Text + "'"
                selectedStream = ddlgvStream.Text
            End If

            ddlgvSection = gvStudSection.HeaderRow.FindControl("ddlgvSection")

            If ddlgvSection.Text <> "ALL" Then

                str_filter = str_filter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
            End If

            str_query += str_filter

        End If

        str_query += " ORDER BY grd_displayorder,sct_descr"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = ds.Tables(0)

        gvStudSection.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudSection.DataBind()
            Dim columnCount As Integer = gvStudSection.Rows(0).Cells.Count
            gvStudSection.Rows(0).Cells.Clear()
            gvStudSection.Rows(0).Cells.Add(New TableCell)
            gvStudSection.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudSection.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudSection.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudSection.DataBind()
        End If

        If gvStudSection.Rows.Count > 0 Then

            ddlgvGrade = gvStudSection.HeaderRow.FindControl("ddlgvGrade")
            ddlgvShift = gvStudSection.HeaderRow.FindControl("ddlgvShift")
            ddlgvStream = gvStudSection.HeaderRow.FindControl("ddlgvStream")
            ddlgvSection = gvStudSection.HeaderRow.FindControl("ddlgvSection")

            Dim dr As DataRow

            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")

            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")

            ddlgvShift.Items.Clear()
            ddlgvShift.Items.Add("ALL")


            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")

            For Each dr In dt.Rows
                With dr

                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    If ddlgvSection.Items.FindByText(.Item(2)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(2))
                    End If
                    If ddlgvGrade.Items.FindByText(.Item(3)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(3))
                    End If
                    If ddlgvShift.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvShift.Items.Add(.Item(4))
                    End If
                    If ddlgvStream.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvStream.Items.Add(.Item(5))
                    End If

                End With
            Next

            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If

            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If

            If selectedShift <> "" Then
                ddlgvShift.Text = selectedShift
            End If

            If selectedStream <> "" Then
                ddlgvStream.Text = selectedStream
            End If
        End If

    End Sub

#End Region

End Class
