﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studAgeCutoff
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050036") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'GetActive_ACD_4_Grade


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    GridBind()
                End If
            Catch ex As Exception

                lblError.Text = "Request could not be processed "
            End Try

        End If

    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gvStudGrade_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade.PageIndexChanging
        Try
            gvStudGrade.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudGrade.RowCommand
        Try

            Dim lblGrdid As New Label
            Dim lblGrade As New Label
            Dim lbldescr As New Label

            Dim lblStream As Label
            Dim lblAgeCutoff As Label
            Dim lblAgeCriteria As Label

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudGrade.Rows(index), GridViewRow)
            lblGrdid = selectedRow.FindControl("lblGrmId")
            lblGrade = selectedRow.FindControl("lblGrade")
            lblStream = selectedRow.FindControl("lblStream")
            lblAgeCutoff = selectedRow.FindControl("lblAgeCutoff")
            lblAgeCriteria = selectedRow.FindControl("lblAgeCriteria")
           If e.CommandName = "edit" Then
                divAge.Visible = True
                lblGradeAge.Text = lblGrade.Text
                lblStreamAge.Text = lblStream.Text
                txtFrom.Text = lblAgeCutoff.Text
                ddlAgeFrom.SelectedValue = IIf(lblAgeCriteria.Text = "", 0, lblAgeCriteria.Text)
                ViewState("GRM_ID") = lblGrdid.Text
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub GridBind()
        Dim ddlgvGrade As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvStream As New DropDownList

        Dim dv As New DataView
        Dim selectedGrade As String = ""
        Dim str_filter As String = ""
        Dim selectedShift As String = ""
        Dim selectedStream As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT GRM_ID, GRM_DISPLAY ,STM_ID, STM_DESCR ,grm_grd_id,  " _
                                  & " replace(CONVERT(VARCHAR(12),ISNULL(GRM_APPL_AGE_CUTOFF,ACD_AGE_CUTOFF),106),' ','/') AS AGE_CUTOFF_DATE ,GRM_AGE_CRITERIA " _
                                  & " FROM Grade_BSU_M A " _
                                  & " INNER JOIN AcademicYear_D B ON GRM_ACD_ID=ACD_ID " _
                                  & " INNER JOIN dbo.STREAM_M C ON GRM_STM_ID=STM_ID " _
                                  & " inner join GRADE_M on GRM_GRD_ID=GRD_ID  " _
                                  & " WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " ORDER BY grd_displayorder"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dv = ds.Tables(0).DefaultView
        If gvStudGrade.Rows.Count > 0 Then
            ddlgvGrade = gvStudGrade.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" Then
                str_filter = "grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            
            ddlgvStream = gvStudGrade.HeaderRow.FindControl("ddlgvStream")

            If ddlgvStream.Text <> "ALL" Then
                If str_filter = "" Then
                    str_filter = "stm_descr='" + ddlgvStream.Text + "'"
                Else
                    str_filter = str_filter + " AND stm_descr='" + ddlgvStream.Text + "'"
                End If

                selectedStream = ddlgvStream.Text
            End If


            If ddlgvGrade.Text = "ALL" And ddlgvShift.Text = "ALL" And ddlgvStream.Text = "ALL" Then
                dv = New DataView
                dv = ds.Tables(0).DefaultView
            Else
                dv.RowFilter = str_filter
            End If
        End If
        gvStudGrade.DataSource = dv
        gvStudGrade.DataBind()

        If gvStudGrade.Rows.Count = 0 Then
            ds.Tables(0).Rows.Clear()
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudGrade.DataSource = ds
            gvStudGrade.DataBind()
            Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
            gvStudGrade.Rows(0).Cells.Clear()
            gvStudGrade.Rows(0).Cells.Add(New TableCell)
            gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudGrade.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If

        If gvStudGrade.Rows.Count > 0 Then

            str_query = "SELECT distinct(grm_display),grd_displayorder FROM grade_bsu_m,grade_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id" _
                       & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " ORDER BY grd_displayorder"

            ddlgvGrade = gvStudGrade.HeaderRow.FindControl("ddlgvGrade")
            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")
            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                ddlgvGrade.Items.Add(reader.GetString(0))
            End While
            reader.Close()
            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If

            

            str_query = "SELECT distinct(stm_descr) FROM grade_bsu_m,stream_m WHERE grade_bsu_m.grm_stm_id=stream_m.stm_id" _
                                 & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + ""

            ddlgvStream = gvStudGrade.HeaderRow.FindControl("ddlgvStream")
            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                ddlgvStream.Items.Add(reader.GetString(0))
            End While
            reader.Close()
            If selectedStream <> "" Then
                ddlgvStream.Text = selectedStream
            End If
        End If
    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False
    End Sub

    Protected Sub gvStudGrade_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudGrade.RowEditing

    End Sub

    Sub update_cutoff_date()



        Dim STU_ID As String = String.Empty




        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                param(0) = New SqlParameter("@GRM_ID", ViewState("GRM_ID"))
                param(1) = New SqlParameter("@CUTOFF_DATE", txtFrom.Text)
                param(2) = New SqlClient.SqlParameter("@AGE", ddlAgeFrom.SelectedValue)

                param(3) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(3).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.UPDATE_AGE_CUTOFF", param)
                Status = param(3).Value





            Catch ex As Exception

                lblError.Text = "Record could not be updated"
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog(lblError.Text)
                    transaction.Rollback()
                Else
                    lblError.Text = "Record Updated Successfully"
                    transaction.Commit()
                End If
            End Try

        End Using

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        update_cutoff_date()
        GridBind()
    End Sub
End Class
