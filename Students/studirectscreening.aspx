<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="studirectscreening.aspx.vb" Inherits="Students_studirectscreening" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

<script type="text/javascript">

    function getDocuments() {
        var sFeatures;
        sFeatures = "dialogWidth: 700px; ";
        sFeatures += "dialogHeight: 400px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        // url='../Students/ShowAcademicInfo.aspx?id='+mode;

        url = document.getElementById("<%=hfURL.ClientID %>").value

            window.showModalDialog(url, "", sFeatures);
            return false;

        }

    </script>
    
<div class="card mb-3">
    <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
    Enquiry Screening
            </div>
     <div class="card-body">
            <div class="table-responsive m-auto">
    <table   cellpadding="5" width="100%">
       <tr id="tr2" runat="server">
                    <td align="right" colspan="4" valign="middle" >
                        <asp:LinkButton ID="lnkDocs" OnClientClick="javascript:return getDocuments();" runat="server" Visible="False">Pending Documents</asp:LinkButton>  
                    </td>
                   </tr>
     
            
                              <tr id="trDoc" runat="server">
                    <td align="center" class="field-label" colspan="4" >
                  <table width="100%" >
                  <tr><td colspan=3 class="">The following documents has to be collected before approval</td></tr>
                            <tr>
                                <td>
                                  <span class="field-label">To Collect</span>
                                    <br />
                                    <div class="checkbox-list">
                                    <asp:ListBox ID="lstNotSubmitted" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="btnRight" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                        Height="15px" OnClick="btnRight_Click" Text=">>" /><br />
                                    <asp:Button ID="btnLeft" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                        Height="15px" OnClick="btnLeft_Click" Text="<<" />
                                </td>
                                <td class="field-label">
                                   <span class="field-label">Collected</span>
                                    <br />
                                    <div class="checkbox-list">
                                    <asp:ListBox ID="lstSubmitted" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <asp:Label id="lblmessage" runat="server" class="error"></asp:Label></td>
                </tr>
                
         <tr>
                  <td  align="left">
                    <span class="field-label">Screening Status</span></td>
                   
                  <td align="left" >
                      <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlApplication" Width="202px"> </asp:DropDownList>
                      </td>
                    <td align="left" ></td>
              <td align="left" ></td>
                     </tr>
                             
                  <tr>
                  <td  align="left" width="20%">
                <span class="field-label">Reason</span></td>
                 
                  <td align="left" width="30%">
                  <asp:DropDownList runat="server" ID="ddlReason"> </asp:DropDownList>
                  </td>
                  <td align="left" width="30%"></td>
                   <td align="left" width="20%"></td>
                  </tr>
        <tr id="tr11" runat="server">
                  <td  align="left">
                 <span class="field-label"> Screening Test Date</span></td>
                
                  <td align="left">
                  <asp:TextBox runat="server" ID="txtScrDate"> </asp:TextBox>
                      <asp:ImageButton id="imgBtnScrDate" runat="server" 
                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                           <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnScrDate" TargetControlID="txtScrDate"> </ajaxToolkit:CalendarExtender>   
                                      <span style="font-size: 7pt">
                                                <br />
                                                (dd/mmm/yyyy)</span>
                  </td>
            <td align="left" ></td>
             <td align="left" ></td>
                  </tr>
         <tr id="tr12" runat="server">
                  <td  align="left">
                 <span class="field-label"> Screening Test Time</span></td>
                 
                  <td align="left" >
                  
                            <telerik:RadTimePicker ID="rdtime" runat="server"></telerik:RadTimePicker><br /><span><span style="font-size: 7pt">
                                                <span style="font-size: 8pt">(hh:mm tt)</span></span></span>
                                       
                  </td>
              <td align="left" ></td>
              <td align="left" ></td>
                  </tr>
       <tr id="tr13" runat="server">
                  <td  align="left">
                 <span class="field-label"> Additional Details</span></td>
                  <%--<td align="center" class=""></td>--%>
                  <td align="left" >
                  <asp:TextBox runat="server" ID="txtDetails"  TextMode="MultiLine" SkinID="MultiText_Large" > </asp:TextBox>
                  </td>
                 <td align="left" ></td>
            <td align="left" ></td>
                  </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button class="button" ID="btnapprove" runat="server" Text="Save" />
                <asp:Button class="button" ID="btnCancel" runat="server" Text="Cancel" /></td>
        </tr>
    </table>
                </div>
         </div>
</div>
 <asp:HiddenField ID="hfURL" runat="server" />
  <asp:HiddenField ID="HiddenEQSID" runat="server" />
</asp:Content>
