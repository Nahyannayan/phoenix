<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studStrikeOffCancelRequest_M.aspx.vb" Inherits="Students_studStrikeOffCancelRequest_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Strike Off"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:" 
                                ValidationGroup="groupM1" />
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>

                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="0" cellspacing="0"
                                style="width: 100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">SEN</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSEN" runat="server" ReadOnly="True"></asp:TextBox></td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%"><span class="field-label">Last Attendance Date</span></td>


                                    <td align="left">
                                        <asp:TextBox ID="txtLast" runat="server" ReadOnly="True">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Recommended Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRec" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRecRemarks" runat="server" TextMode="MultiLine" ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Approval Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtApr" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAprRemarks" runat="server" TextMode="MultiLine" ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Strike Off</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStrike" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStrikeRemarks" runat="server" TextMode="MultiLine" ReadOnly="True"></asp:TextBox></td>
                                </tr>



                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" valign="middle">&nbsp;Details</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Cancel Request Date</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>

                                </tr>



                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="bottom">&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom" style="height: 154px">&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate"
                    Display="None" ErrorMessage="Please enter data in the field Cancel Request  Date" ValidationGroup="groupM1"
                    Width="23px"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfTCM_ID" runat="server" />
                            <asp:HiddenField ID="HF_STK_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtDate" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="hfMode" runat="server" />
                            &nbsp; &nbsp;
                &nbsp;
                &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>

