<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBsu_Service_View.aspx.vb" Inherits="Students_studBsu_Service_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
        
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>  <asp:Literal id="ltHeader" runat="server" Text="Service"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
       
        <tr>
            <td align="left" colspan="2">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:LinkButton id="lbAddNew" runat="server" onclick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table id="tbl_test" runat="server" align="center" width="100%"  cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left"  colspan="2" valign="top">
                            <asp:GridView id="gvServiceRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <rowstyle cssclass="griditem" />
                                <columns>
<asp:BoundField DataField="srno" HeaderText="Sr.No">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Service"><EditItemTemplate>
<asp:TextBox id="TextBox9" runat="server" Text='<%# Bind("SCT_DESCR") %>' __designer:wfdid="w46"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblSVC_DESCR" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>' __designer:wfdid="w45"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" __designer:wfdid="w43"></asp:TextBox> 
</EditItemTemplate>
<HeaderTemplate>
<asp:Label id="lblViewH" runat="server" Text="View" __designer:wfdid="w44"></asp:Label> 
</HeaderTemplate>
<ItemTemplate>
&nbsp;<asp:LinkButton id="lblView" onclick="lblView_Click" runat="server" __designer:wfdid="w42">View</asp:LinkButton> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="SVC_ID" Visible="False"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("SBR_ID") %>' __designer:wfdid="w41"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblSVC_ID" runat="server" Text='<%# Bind("SVC_ID") %>' __designer:wfdid="w40"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                                <selectedrowstyle backcolor="Wheat" />
                                <headerstyle cssclass="gridheader_pop" horizontalalign="Center" verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
        </tr>
    </table>
            </div>
        </div>
    </div>

</asp:Content>

