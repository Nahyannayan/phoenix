﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowEnquiryStudents.aspx.vb" Inherits="Students_ShowEnquiryStudents" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>
    <script language="javascript" type="text/javascript">
        var color = '';

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }



        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method



function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}





    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">
        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
        <div>
            <table align="center" 
                cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                    <td  colspan="2" align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                    </td>
                </tr>
                <tr class="matters">
                    <td align="left">Select Academic Year</td>
                   
                    <td align="left">&nbsp;
                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" Width="108px">
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td align="center" class="matters" colspan="3" valign="top">

                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                            HeaderStyle-Height="30" PageSize="15" Width="100%">
                            <Columns>

                                <%-- <asp:TemplateField HeaderText="Available">
                <EditItemTemplate>
                <asp:CheckBox ID="chkSelect" runat="server"  />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <HeaderStyle Wrap="False" />
                <ItemTemplate>
                <asp:CheckBox ID="chkSelect0" runat="server" 
                        onclick="javascript:highlight(this);" />
                </ItemTemplate>
                <HeaderTemplate>
                <table><tr><td align="center">Select </td></tr><tr><td align="center">
                <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
                ToolTip="Click here to select/deselect all rows" /></td>
                </tr></table></HeaderTemplate>
                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                    <ItemTemplate>

                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegDate" runat="server" Text='<%# Bind("EQS_REGNDATE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("EQM_ENQID") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Enq No">
                                    <HeaderTemplate>
                                         <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No"></asp:Label>
                                                    <br /> <asp:TextBox ID="txtEnqSearch" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnqid_Search_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnqNo" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                        <asp:HiddenField ID="HF_SIBLING_TYPE" runat="server" Value='<%# Bind("EQS_SIBLING_TYPE") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="ACC No">
                                    <HeaderTemplate>
                                        <asp:Label ID="lblAcc" runat="server" CssClass="gridheader_text" Text="Acc No"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtAccSearch" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="btnAcc_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAcc_Search_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAccNo" runat="server" Text='<%# Bind("eqs_accno") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enq Date">
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtEnqDate" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnq_Date_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnqDate0" runat="server"
                                            Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Applicant Name">
                                    <HeaderTemplate>
                                        <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtApplName" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAppl_Name_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkApplName" CommandName="Select" OnClick="lnkApplName_Click" runat="server" Text='<%# Bind("appl_name") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Grade">
                                    <HeaderTemplate>
                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Shift">
                                    <HeaderTemplate>
                                         <asp:Label ID="lblShiftH" runat="server" Text="Shift" CssClass="gridheader_text"></asp:Label>
                                                    <br />
                                                     <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" CssClass="listbox"
                                                        OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" Width="70px">
                                                    </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Stream">
                                    <HeaderTemplate>
                                       <asp:Label ID="lblStreamH" runat="server" Text="Stream" CssClass="gridheader_text"></asp:Label><br />
                                                    <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                        OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" Width="70px">
                                                    </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>



                            </Columns>
                            <HeaderStyle CssClass="gridheader_pop" />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle CssClass="Green" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
               
            </table>
           </div></form>
</body>
</html>
