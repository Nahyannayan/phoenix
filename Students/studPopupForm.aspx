<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studPopupForm.aspx.vb" Inherits="SelBussinessUnit" Theme="General" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet"/>
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet"/>
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet"/>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
     
         </script>

   <%-- <script language="javascript" type="text/javascript">
        function listen_window() {
        }
        function GetRadWindow() {

            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

    </script>--%>
</head>
<body  onload="listen_window();" >

 
    <form id="form1" runat="server">
     <table width="100%" align="center" > 
         <tr valign="top"  align="center" class="title">
             <td>
                 Select Value</td>
         </tr>
                    <tr valign = "top">
                            <td>
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True"  CssClass="table table-bordered table-row" PageSize="15">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <ItemTemplate>
                                            &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSecDescr" runat="server" Text='<%# Bind("DESCR1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label>
                                                        <br /><asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("DESCR2") %>'></asp:Label>&nbsp;<br />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                        <HeaderTemplate>
                                           <asp:Label ID="lblBSUName" runat="server" CssClass="gridheader_text"></asp:Label>
                                                        <br /><asp:TextBox ID="txtBSUName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR2") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                                 AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr> 
                        <td align="center" colspan="3" >
                         <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
                    </tr>
                </table>
          
    </form>
</body>
</html>