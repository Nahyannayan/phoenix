Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports InfoSoftGlobal
Imports Microsoft.Win32
Partial Class Students_studAtt_SummHouse
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim menu_rights As String = String.Empty
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059026") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    Try


                        Dim pageKey As RegistryKey = Registry.CurrentUser.OpenSubKey("software\microsoft\internet explorer\pagesetup", True)
                        Dim header, footer As String
                        'Dim oriFooter As Object = pageKey.GetValue("footerTemp")

                        footer = pageKey.GetValue("footer", 0)
                        header = pageKey.GetValue("header", 0)

                        pageKey.SetValue("footer", "")
                        pageKey.SetValue("header", "")
                        ' pageKey.DeleteValue("footerTemp")

                        pageKey.Close()

                        'ClientScript.RegisterStartupScript(Me.GetType, "error", "<script language='Javascript'>window.close();</script>")

                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message)
                    End Try



                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Dim TODT As String = Session("TO_Dt") ' String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
                    Dim FROMDT As String = Session("FROM_Dt")
                    If FROMDT = TODT Then
                        ltHeader.Text = "House Attendance Summary for the class marked " & TODT
                    Else
                        ltHeader.Text = "House Attendance Summary for the class marked from " & FROMDT & " to " & TODT
                    End If

                 
                    HiddenBsuId.Value = Session("sbsuid")
                    HiddenAcademicyear.Value = Session("ACD_SEL_ATT")
                    BindBsuDetails()
                    CreateCharts_Main()
                    CreateCharts_sub()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                    'Session("TO_Dt") = Nothing
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub
    Public Sub BindBsuDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select BSU_NAME,BSU_ADDRESS,BSU_POBOX,BSU_CITY,BSU_TEL,BSU_FAX,BSU_EMAIL,BSU_MOE_LOGO,BSU_URL from BUSINESSUNIT_M where BSU_ID='" & HiddenBsuId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        BsuName.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
        'bsuAddress.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_ADDRESS").ToString()
        'bsupostbox.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_POBOX").ToString()
        'bsucity.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_CITY").ToString()
        'bsutelephone.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_TEL").ToString()
        'bsufax.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_FAX").ToString()
        'bsuemail.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_EMAIL").ToString()
        'bsuwebsite.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_URL").ToString()
        imglogo.ImageUrl = ds.Tables(0).Rows(0).Item("BSU_MOE_LOGO").ToString()
    End Sub

    Function DateFormat(ByVal fDate As DateTime) As String
        Return Format(fDate, "dd/MMM/yyyy").Replace("01/Jan/1900", "")
    End Function

    Sub CreateCharts_Main()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FROMDT As String = Session("FROM_Dt")
        Dim TODT As String = Session("TO_Dt")
        Dim queryString As String = String.Empty
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("ACD_SEL_ATT"))
        param(1) = New SqlClient.SqlParameter("@TODT", TODT)
        param(2) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
        Dim strXML As String
        Dim group_descr As String = String.Empty
        Dim group_per As String = String.Empty
        Dim VGA_ID As Integer
        'strXML will be used to store the entire XML document generated


        Dim arr() As String
        If Session("sBsuid") = "125018" Then
            arr = New String() {"AFD8F8", "2BCD00", "1F1F1F", "ff2222", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", "e6acef"}
        Else
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", "e6acef"}
        End If

        Dim i As Integer = 0



        strXML = ""
        strXML = strXML & "<graph caption='Attendance House Wise' xAxisName='House Name' yaxismaxvalue='100' yaxisminvalue='0'  yAxisName='Percent' decimalPrecision='2' formatNumberScale='0' rotateNames='0'>"
        'link='javascript:AddMainDetails(" & VGA_ID.ToString & ");' 
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VA.GETHOUSE_ATT_PERC", param)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read

                    queryString = readerStudent_Detail("HOUSE_ID").ToString()
                    group_descr = readerStudent_Detail("HOUSE_DESCR").ToString
                    group_per = readerStudent_Detail("HOUSE_PERC").ToString
                    strXML = strXML & "<set name=" + "'" & group_descr & "' value=" + "'" & group_per & "' color=" + "'" & arr(i) & "' link='javascript:ShowDetails(" & queryString & ");' />"
                    i = i + 1
                End While
                ' Dim strline As String = "<trendlines><line startvalue='75' displayValue='Good-above 75' color='FF0000' thickness='1' isTrendZone='0'/><line startvalue='50' displayValue='Bad-below 50' color='009999' thickness='1' isTrendZone='0'/></trendlines>"
                strXML = strXML & "</graph>"
                ltmain.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "subgroup", "500", "300", False)
            Else
                'lblerror.Text = "No Records Found "

                strXML = ""
                strXML = strXML & "<graph caption='Attendance House Wise' xAxisName='House Name' yAxisName='Percent' decimalPrecision='2' formatNumberScale='0'>"
                strXML = strXML & "</graph>"
                ltmain.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "subgroup", "500", "300", False)
            End If
        End Using




        'strXML will be used to store the entire XML document generated


        'Generate the graph element
        'strXML = "<graph caption='Attendance Group Wise'    decimalPrecision='0' showNames='1' numberSuffix=' %' pieSliceDepth='30' formatNumberScale='0' >"

        '' SQL Query
        'Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART1]", param)

        '    If readerStudent_Detail.HasRows = True Then
        '        While readerStudent_Detail.Read

        '            strXML = strXML & "<set name='" & readerStudent_Detail("GROUP_NAME").ToString & "' value='" & readerStudent_Detail("TOT_PERC").ToString & "'/>"
        '        End While
        '    End If
        'End Using

        '' Open data reader

        ''Iterate through each factory

        ''Finally, close <graph> element
        'strXML = strXML & "</graph>"


        ''Create the chart - Pie 3D Chart with data from strXML
        'Return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "FactorySum", "500", "300", False, False)
    End Sub
    Sub CreateCharts_sub()
        Dim strXML As String
        Dim arr() As String
        arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", _
"e6acef", "efdc07", "73d1f8", "ee735b", "f704dd", "b8f149", "f1cb49", "a849f1", "07daf9", "91b8be", _
"d2cad6", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61", _
"52ed4e", "0a937e", "80269e", "9e266d", "f4e8b0", "8a008c", "288c00", "8e006e", "70e2bb", _
"b3bec3", "9bc101", "9b01c1", "b68db6", "aae6bf", "b9cf75", "d5aff0", "f0baaf", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61"}

        Dim i As Integer = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FROMDT As String = Session("FROM_Dt")
        Dim TODT As String = Session("TO_Dt")
        Dim HOUSE_Code As String = String.Empty
        Dim HOUSE_PERC As String = String.Empty
        Dim queryString As String = String.Empty
        Dim InnerParam(5) As SqlClient.SqlParameter
        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("ACD_SEL_ATT"))
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "VA.GETATT_HOUSE_ID", param)
        dlHouse_att.DataSource = ds.Tables(0)
        dlHouse_att.DataBind()
        For Each row As DataListItem In dlHouse_att.Items
            Dim HOUSE_ID As String = DirectCast(row.FindControl("hfHouse_id"), HiddenField).Value
            Dim House_Descr As String = DirectCast(row.FindControl("hfHouse_Descr"), HiddenField).Value
            Dim ltHouse_Lt As Literal = DirectCast(row.FindControl("ltHouse_Lt"), Literal)

            InnerParam(0) = New SqlClient.SqlParameter("@ACD_ID", Session("ACD_SEL_ATT"))
            InnerParam(1) = New SqlClient.SqlParameter("@TODT", TODT)
            InnerParam(2) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
            InnerParam(3) = New SqlClient.SqlParameter("@HOUSE_ID", HOUSE_ID)

            i = 0
            strXML = ""
            strXML = strXML & "<graph caption=' ' xAxisName=" + "'" & House_Descr & "' yaxismaxvalue='100' yaxisminvalue='0'  yAxisName='Percent' decimalPrecision='2' formatNumberScale='0' rotateNames='1' showLegend='1'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[VA].[GETHOUSE_ATT_PERC_SUB_GRP]", InnerParam)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        HOUSE_Code = readerStudent_Detail("HOUSE_DESCR").ToString
                        HOUSE_PERC = readerStudent_Detail("HOUSE_PERC").ToString

                        strXML = strXML & "<set name=" + "'" & HOUSE_Code & "' value=" + "'" & HOUSE_PERC & "' color=" + "'" & arr(i) & "'  />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"

                    ltHouse_Lt.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "subgrade", "580", "300", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption=' ' xAxisName=' ' yAxisName='Percent' decimalPrecision='0' formatNumberScale='0'>"
                    strXML = strXML & "</graph>"
                    ltHouse_Lt.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "subgrade", "580", "300", False)
                End If
            End Using



        Next




      



       






        ''Generate the graph element
        'strXML = "<graph caption='Attendance Grade Wise' decimalPrecision='0' showNames='1' showLegend='1' numberSuffix=' %' pieSliceDepth='30' formatNumberScale='0' >"

        '' SQL Query
        'Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART2]", param)

        '    If readerStudent_Detail.HasRows = True Then
        '        While readerStudent_Detail.Read

        '            strXML = strXML & "<set name='" & readerStudent_Detail("GRD_DES").ToString & "' value='" & readerStudent_Detail("TOT_PERC").ToString & "'  link='javascript:AddDetails(" & readerStudent_Detail("GRM_ID").ToString & ");'/>"


        '        End While
        '    End If
        'End Using

        '' Open data reader

        ''Iterate through each factory

        ''Finally, close <graph> element
        ''strXML = strXML & "<set name='GRADE 9' value='80' link='" & Server.UrlEncode("Detailed.aspx?FactoryId=9") & "'/>"
        ''strXML = strXML & "<set name='GRADE 10' value='90' link='" & Server.UrlEncode("Detailed.aspx?FactoryId=5") & "'/>"
        'strXML = strXML & "</graph>"
        ''Create the chart - Pie 3D Chart with data from strXML
        'Return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Fact", "500", "300", False, False)
    End Sub
    Protected Sub imgbtnback_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnback.Click

        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        Dim url As String = String.Empty
        url = String.Format("~\Students\studAtt_Summ_view.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)



        'Response.Redirect("~/students/studAtt_Summ_view.aspx")
    End Sub
End Class
