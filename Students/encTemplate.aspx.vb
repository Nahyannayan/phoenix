﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_encTemplate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Bind_school()
                bind_mergetype()
                If Not Request.QueryString("bsuid") Is Nothing Then
                    ddlBSU.SelectedValue = Request.QueryString("bsuid")

                End If
                If Not Request.QueryString("id") Is Nothing Then
                    ViewState("ID") = Request.QueryString("id")

                End If
                Displayitems()
                If ViewState("ID") <> "0" Then
                    txtTemplate.Enabled = False
                    btnAdd.Text = "Update"
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Private Sub Bind_school()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_SCHOOLS", PARAM)
            ddlBSU.DataSource = dsDetails
            ddlBSU.DataTextField = "BSU_NAME"
            ddlBSU.DataValueField = "BSU_ID"
            ddlBSU.DataBind()

            ddlBSU.Items.Add(New ListItem("--All--", ""))
            ddlBSU.Items.FindByText("--All--").Selected = True


        Catch ex As Exception

        End Try
    End Sub
    Sub bind_mergetype()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EMT_ID  ,EMT_DESCR  FROM [ENC].[EMAILMERGE_TYPE_M] "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMergeType.DataSource = ds
        ddlMergeType.DataTextField = "EMT_DESCR"
        ddlMergeType.DataValueField = "EMT_ID"
        ddlMergeType.DataBind()
        ddlMergeType.Items.Add(New ListItem("--Select--", "0"))
        ddlMergeType.Items.FindByText("--Select--").Selected = True
    End Sub
    Sub bind_mergefield()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EMS_ID,EMS_DESCR  FROM [ENC].[EMAILMERGE_TYPE_S] WHERE EMS_EMT_ID=" & ddlMergeType.SelectedValue



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMergeField.DataSource = ds
        ddlMergeField.DataTextField = "EMS_DESCR"
        ddlMergeField.DataValueField = "EMS_ID"
        ddlMergeField.DataBind()
        ddlMergeField.Items.Add(New ListItem("--Select--", "0"))
        ddlMergeField.Items.FindByText("--Select--").Selected = True
    End Sub
    Private Sub SAVE_TEMPLATE(ByVal ID As Integer)
        Dim ReturnFlag As Integer
        Dim con As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@NAME", txtTemplate.Text)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddlBSU.SelectedItem.Value)
            pParms(2) = New SqlClient.SqlParameter("@CONTENT", txtText.Content)
            pParms(3) = New SqlClient.SqlParameter("@USER", Session("sUsr_name"))
            pParms(4) = New SqlClient.SqlParameter("@ID", ID)


            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SAVE_EMAILTEMPLATE", pParms)

            ReturnFlag = pParms(5).Value
        Catch ex As Exception
            lblerror.Text = ex.Message
            lblerror.Focus()

            ReturnFlag = 1


        Finally
            If ReturnFlag = 0 Then
                sqltran.Commit()
                lblerror.Text = "Saved"

            Else
                sqltran.Rollback()
            End If
        End Try
    End Sub
    Private Sub Displayitems()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "SELECT ETS_ID,ETS_CONTENT,ETS_NAME FROM [ENC].[EMAILTEMPLATE_S] " _
                        & " WHERE ETS_ID=" + ViewState("ID")

            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then

                    txtTemplate.Text = ds.Tables(0).Rows(0).Item("ETS_NAME")

                    txtText.Content = ds.Tables(0).Rows(0).Item("ETS_CONTENT")


                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlMergeType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMergeType.SelectedIndexChanged
        bind_mergefield()
    End Sub

    Protected Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click
        txtText.Content = txtText.Content & ddlMergeField.SelectedItem.Text
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        SAVE_TEMPLATE(ViewState("ID"))
    End Sub
End Class
