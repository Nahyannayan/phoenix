Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections

Partial Class Students_studGrade_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050035") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Session("dtGrade") = SetDataTable()
                    ddlShift = studClass.PopulateShift(ddlShift, Session("sbsuid").ToString)
                    ViewState("SelectedRow") = -1
                    ddlStream = studClass.PopulateStream(ddlStream)
                    If ViewState("datamode") = "add" Then
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                        PopulateGradeMaster()
                        'ddlCopyGrade = studClass.PopulateGrade(ddlCopyGrade, ddlAcademicYear.SelectedValue, Session("sbsuid").ToString)
                        PopulateGradeCopy()
                        If ddlGrade.Items.Count <> 0 Then
                            gvStudGrade.DataSource = Session("dtGrade")
                            gvStudGrade.DataBind()
                            txtGradeDisplay.Text = ddlGrade.SelectedItem.Text
                            txtGradeDescr.Text = ddlGrade.SelectedItem.Text
                        End If
                    Else
                        Dim li As New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("academicyear").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                        ddlAcademicYear.Items.Add(li)
                        ddlAcademicYear.Items(0).Selected = True


                        li = New ListItem
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                        li.Text = GradeDisplay(li.Value)

                        ddlGrade.Items.Add(li)
                        ddlGrade.Items(0).Selected = True

                        hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                        GetRows()
                        btnAdd.Visible = False

                        EnableDisableControls(True)
                        ddlCopyGrade.Enabled = False

                        txtGradeDisplay.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                        txtGradeDescr.Text = Encr_decrData.Decrypt(Request.QueryString("descr").Replace(" ", "+"))
                        txtGradeCertificate.Text = Encr_decrData.Decrypt(Request.QueryString("cert").Replace(" ", "+"))
                        hfDisplay.Value = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                        hfDescription.Value = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                        hfCertificate.Value = Encr_decrData.Decrypt(Request.QueryString("cert").Replace(" ", "+"))
                        txtPriority.Text = Encr_decrData.Decrypt(Request.QueryString("prio").Replace(" ", "+"))
                    End If
                    End If


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If
            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
            ddlShift.Enabled = True
            ddlStream.Enabled = True
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            PopulateGradeMaster()
            PopulateGradeCopy()
            If ViewState("datamode") = "add" Then
                txtGradeDisplay.Text = ddlGrade.SelectedItem.Text
                txtGradeDescr.Text = ddlGrade.SelectedItem.Text
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudGrade_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade.PageIndexChanging
        Try
            gvStudGrade.PageIndex = e.NewPageIndex
            gvStudGrade.DataSource = Session("dtGrade")
            gvStudGrade.DataBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudGrade.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudGrade.Rows(index), GridViewRow)
            Dim lblShift As New Label
            Dim lblShiftId As New Label
            Dim lblStream As New Label
            Dim lblStreamId As New Label
            With selectedRow
                lblShift = .Cells(5).FindControl("lblShift")
                lblShiftId = .Cells(6).FindControl("lblShiftId")
                lblStream = .Cells(7).FindControl("lblStream")
                lblStreamId = .Cells(8).FindControl("lblStreamId")
            End With

            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtGrade")

            ReDim keys(3)

            keys(0) = ddlAcademicYear.SelectedValue.ToString
            keys(1) = ddlGrade.SelectedValue.ToString
            keys(2) = lblShiftId.Text
            keys(3) = lblStreamId.Text
            index = dt.Rows.IndexOf(dt.Rows.Find(keys))

            If e.CommandName = "edit" Then

                Dim lblGender As New Label
                Dim lblCapacity As New Label
                Dim lblMgmquota As New Label
                Dim lblOpenOnline As New Label
                Dim lblScreenreqd As New Label
                Dim lblORP As New Label

                With selectedRow
                    lblGender = .Cells(9).FindControl("lblGender")
                    lblCapacity = .Cells(10).FindControl("lblMaxCapacity")
                    lblMgmquota = .Cells(11).FindControl("lblMgmQuota")
                    lblOpenOnline = .Cells(12).FindControl("lblOpenOnline")
                    lblScreenreqd = .Cells(13).FindControl("lblScreenReqd")
                    lblORP = .Cells(17).FindControl("lblORP")
                End With

                Dim li As New ListItem
                li.Text = lblShift.Text
                li.Value = CType(lblShiftId.Text, Integer)
                ddlShift.ClearSelection()
                ddlShift.Items(ddlShift.Items.IndexOf(li)).Selected = True

                li = New ListItem
                li.Text = lblStream.Text
                li.Value = CType(lblStreamId.Text, Integer)
                ddlStream.ClearSelection()
                ddlStream.Items(ddlStream.Items.IndexOf(li)).Selected = True

                ddlGender.ClearSelection()
                If lblGender.Text = "All" Then
                    ddlGender.Items(0).Selected = True
                ElseIf lblGender.Text = "Male" Then
                    ddlGender.Items(1).Selected = True
                Else
                    ddlGender.Items(2).Selected = True
                End If

                txtCapacity.Text = lblCapacity.Text
                txtMgmQuota.Text = lblMgmquota.Text

                If lblOpenOnline.Text = "Yes" Then
                    chkOpen.Checked = True
                Else
                    chkOpen.Checked = False
                End If

                If lblORP.Text = "True" Then
                    chkOORPayment.Checked = True
                Else
                    chkOORPayment.Checked = False
                End If

                If lblScreenreqd.Text = "Yes" Then
                    chkScreenReqd.Checked = True
                Else
                    chkScreenReqd.Checked = False
                End If
                btnAddNew.Text = "Update"

                ViewState("SelectedRow") = index

                ddlShift.Enabled = False
                ddlStream.Enabled = False
            ElseIf e.CommandName = "add" Or e.CommandName = "delete" Then

                If dt.Rows(index).Item(14) = "add" Then
                    dt.Rows(index).Item(14) = "delete"
                ElseIf dt.Rows(index).Item(14) = "edit" Then
                    dt.Rows(index).Item(14) = "remove"
                End If
                Session("dtGrade") = dt
                GridBind(dt)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudGrade_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudGrade.RowDeleting

    End Sub

    Protected Sub gvStudGrade_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudGrade.RowEditing

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            SaveData()
            clearRecords()
            If ViewState("datamode") = "add" Then
                Session("dtGrade") = SetDataTable()
                gvStudGrade.DataSource = Session("dtGrade")
                gvStudGrade.DataBind()
            Else
                Session("dtGrade") = SetDataTable()
                GetRows()
            End If
            '   lblError.Text = "Record saved successfully"
            EnableDisableControls(True)
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Error while saving data"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            hfGRD_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                Call clearRecords()
                EnableDisableControls(True)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Session("dtGrade") = SetDataTable()
                gvStudGrade.DataSource = Session("dtGrade")
                gvStudGrade.DataBind()
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "edit"
            EnableDisableControls(False)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "add"
            EnableDisableControls(False)
            ddlCopyGrade.Enabled = True
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
            PopulateGradeMaster()
            '  ddlCopyGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue, Session("sbsuid").ToString)
            PopulateGradeCopy()
            Session("dtGrade") = SetDataTable()
            gvStudGrade.DataSource = Session("dtGrade")
            gvStudGrade.DataBind()
            gvStudGrade.Columns(1).Visible = True
            lblError.Text = ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                txtGradeDisplay.Text = ddlGrade.SelectedItem.Text
                txtGradeDescr.Text = ddlGrade.SelectedItem.Text
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Try
            CopyFromGrade()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"
    Function GradeDisplay(ByVal grdid As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT grd_display from grade_m where grd_id='" + grdid + "'"
        Dim grade As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return grade
    End Function

    Sub EnableDisableControls(ByVal value As Boolean)
        txtGradeDisplay.ReadOnly = value
        txtGradeDescr.ReadOnly = value
        txtGradeCertificate.ReadOnly = value
        ddlShift.Enabled = Not value
        ddlStream.Enabled = Not value
        txtMgmQuota.ReadOnly = value
        txtCapacity.ReadOnly = value
        chkOpen.Enabled = Not value
        chkOORPayment.Enabled = Not value
        chkScreenReqd.Enabled = Not value
        ddlGender.Enabled = Not value
        gvStudGrade.Enabled = Not value
        btnAddNew.Enabled = Not value
    End Sub
    Sub clearRecords()
        txtMgmQuota.Text = ""
        txtCapacity.Text = ""
        chkOpen.Checked = False
        chkOORPayment.Checked = False
        chkScreenReqd.Checked = False
        ddlShift.ClearSelection()
        ddlShift.SelectedIndex = 0
        ddlStream.ClearSelection()
        ddlStream.SelectedIndex = 0
        ddlGender.ClearSelection()
        ddlGender.SelectedIndex = 0
    End Sub

    Public Sub PopulateGradeMaster()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT grd_display,grd_id,grd_displayorder from grade_m " _
                                  & " where grd_id not in(select grm_grd_id from grade_bsu_m where grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " and grm_bsu_id='" + Session("sbsuid").ToString + "') order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grd_display"
        ddlGrade.DataValueField = "grd_id"
        ddlGrade.DataBind()
    End Sub

    Public Sub PopulateGradeCopy()
        ddlCopyGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                                    & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                                & " grm_bsu_id='" + Session("sbsuid").ToString + "' and grm_acd_id=" + ddlAcademicYear.SelectedValue + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCopyGrade.DataSource = ds
        ddlCopyGrade.DataTextField = "grm_display"
        ddlCopyGrade.DataValueField = "grm_grd_id"
        ddlCopyGrade.DataBind()
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(4)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "acd_id"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "acy_descr"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_grd_id"
        dt.Columns.Add(column)
        keys(1) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_id"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_display"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "shf_descr"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_shf_id"
        dt.Columns.Add(column)
        keys(2) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "stm_descr"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_stm_id"
        dt.Columns.Add(column)
        keys(3) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "gender"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_maxcapacity"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_mgmcapacity"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "openonline"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "screenreqd"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_descr"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_certificate"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_priority"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.Boolean")
        column.ColumnName = "GRM_bOPEN_REG_PAY"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GetRows()
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT grm_id,shf_descr,grm_shf_id,stm_descr,grm_stm_id," _
                                     & "CASE grm_gender WHEN 'A' THEN 'All' WHEN 'M' THEN 'Male' ELSE 'NO' END AS gender," _
                                     & "grm_maxcapacity,grm_mgmcapacity,CASE grm_openonline WHEN 1 THEN 'Yes' ELSE 'No' END AS openonline," _
                                     & "CASE grm_bscreeningreqd WHEN 1 THEN 'Yes' ELSE 'No' END AS screenreqd,isnull(grm_descr,'')," _
                                     & "isnull(grm_certificate,''),isnull(grm_priority,0),grd_displayorder,ISNULL(GRM_bOPEN_REG_PAY,0)GRM_bOPEN_REG_PAY" _
                                     & " FROM grade_bsu_m,grade_m,shifts_m,stream_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id" _
                                     & " AND grade_bsu_m.grm_shf_id = shifts_m.shf_id And grade_bsu_m.grm_stm_id = stream_m.stm_id" _
                                     & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " AND grm_grd_id='" + hfGRD_ID.Value.ToString + "' ORDER BY grd_displayorder"
            dt = Session("dtGrade")
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                dr = dt.NewRow
                dr.Item(0) = ddlAcademicYear.SelectedValue.ToString
                dr.Item(1) = ddlAcademicYear.SelectedItem.Text
                dr.Item(2) = ddlGrade.SelectedValue.ToString
                dr.Item(3) = reader.GetValue(0).ToString
                dr.Item(4) = txtGradeDisplay.Text
                dr.Item(5) = reader.GetString(1)
                dr.Item(6) = reader.GetValue(2).ToString
                dr.Item(7) = reader.GetString(3)
                dr.Item(8) = reader.GetValue(4).ToString
                dr.Item(9) = reader.GetString(5)
                dr.Item(10) = reader.GetValue(6).ToString
                dr.Item(11) = reader.GetValue(7).ToString
                dr.Item(12) = reader.GetString(8)
                dr.Item(13) = reader.GetString(9)
                dr.Item(14) = "edit"
                dr.Item(15) = reader.GetString(10)
                dr.Item(16) = reader.GetString(11)
                dr.Item(17) = reader.GetValue(12).ToString
                dr.Item(18) = reader.GetValue(14).ToString
                dt.Rows.Add(dr)
            End While
            reader.Close()
            gvStudGrade.DataSource = dt
            gvStudGrade.DataBind()

            gvStudGrade.Columns(1).Visible = False
            gvStudGrade.Columns(4).Visible = False

            Session("dtGrade") = dt

            str_query = "SELECT distinct(isnull(grm_display,'')) FROM grade_bsu_m  WHERE grm_grd_id='" + hfGRD_ID.Value.ToString + "'"
            txtGradeDisplay.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            hfDisplay.Value = txtGradeDisplay.Text
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub AddRows()
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            dt = Session("dtGrade")

            ''check if the row is already added
            Dim keys As Object()
            ReDim keys(3)

            keys(0) = ddlAcademicYear.SelectedValue.ToString
            keys(1) = ddlGrade.SelectedValue.ToString
            keys(2) = ddlShift.SelectedValue.ToString
            keys(3) = ddlStream.SelectedValue.ToString
            Dim row As DataRow = dt.Rows.Find(keys)
            If Not row Is Nothing Then
                lblError.Text = "This grade is already added"
                Exit Sub
            End If


            dr = dt.NewRow
            dr.Item(0) = ddlAcademicYear.SelectedValue.ToString
            dr.Item(1) = ddlAcademicYear.SelectedItem.Text
            dr.Item(2) = ddlGrade.SelectedValue.ToString
            dr.Item(3) = 0
            dr.Item(4) = txtGradeDisplay.Text
            dr.Item(5) = ddlShift.SelectedItem.Text
            dr.Item(6) = ddlShift.SelectedValue.ToString
            dr.Item(7) = ddlStream.SelectedItem.Text
            dr.Item(8) = ddlStream.SelectedValue.ToString
            dr.Item(9) = ddlGender.SelectedItem.Text
            dr.Item(10) = txtCapacity.Text.ToString
            dr.Item(11) = txtMgmQuota.Text.ToString
            dr.Item(12) = IIf(chkOpen.Checked, "Yes", "No")
            dr.Item(13) = IIf(chkScreenReqd.Checked, "Yes", "No")
            dr.Item(14) = "add"
            dr.Item(15) = txtGradeDescr.Text
            dr.Item(16) = txtGradeCertificate.Text
            dr.Item(17) = txtPriority.Text
            dr.Item(18) = chkOORPayment.Checked
            dt.Rows.Add(dr)

            Session("dtGrade") = dt


            GridBind(dt)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(14) <> "delete" And dt.Rows(i)(14) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvStudGrade.DataSource = dtTemp
        gvStudGrade.DataBind()
    End Sub

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtGrade")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(10) = txtCapacity.Text.ToString
                .Item(11) = txtMgmQuota.Text.ToString
                .Item(12) = IIf(chkOpen.Checked, "Yes", "No")
                .Item(13) = IIf(chkScreenReqd.Checked, "Yes", "No")
                If .Item(14) = "edit" Then
                    .Item(14) = "update"
                End If
                .Item(18) = chkOORPayment.Checked
            End With

            Session("dtGrade") = dt

            GridBind(dt)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim grmAddIds As String = ""
        Dim grmEditIds As String = ""
        Dim grmDeleteIds As String = ""

        Dim dt As DataTable
        dt = Session("dtGrade")
        Dim dr As DataRow

        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        If .Item(14) <> "delete" And .Item(14) <> "edit" Then
                            If .Item(14) = "update" Then
                                UtilityObj.InsertAuditdetails(transaction, "edit", "GRADE_BSU_M", "GRM_ID", "GRM_ID", "GRM_ID=" + .Item(3).ToString)
                            ElseIf .Item(14) = "remove" Then
                                UtilityObj.InsertAuditdetails(transaction, "delete", "GRADE_BSU_M", "GRM_ID", "GRM_ID", "GRM_ID=" + .Item(3).ToString)
                            End If
                            Dim bORP As Boolean = .Item(18)
                            str_query = "exec studSaveGradeShift " + .Item(3) + ",'" _
                                        & Session("sbsuid").ToString + "'," _
                                        & .Item(0) + ",'" _
                                        & .Item(2) + "','" _
                                        & .Item(4) + "'," _
                                        & .Item(6) + "," _
                                        & .Item(8) + ",'" _
                                        & .Item(9).ToString.Substring(0, 1) + "'," _
                                        & .Item(10) + "," _
                                        & .Item(11) + "," _
                                        & IIf(.Item(12) = "Yes", 1, 0).ToString + "," _
                                        & IIf(.Item(13) = "Yes", 1, 0).ToString + ",'" _
                                        & .Item(14) + "','" _
                                        & .Item(15) + "','" _
                                        & .Item(16) + "','" _
                                        & .Item(17) + "'," _
                                        & bORP & ""
                            dr.Item(3) = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                            If .Item(14) = "add" Then
                                If grmAddIds.Length <> 0 Then
                                    grmAddIds += ","
                                End If
                                grmAddIds += dr.Item(3)
                            End If

                            If .Item(14) = "update" Then
                                If grmEditIds.Length <> 0 Then
                                    grmEditIds += ","
                                End If
                                grmEditIds += dr.Item(3)
                            End If

                            If .Item(14) = "remove" Then
                                If grmDeleteIds.Length <> 0 Then
                                    grmDeleteIds += ","
                                End If
                                grmDeleteIds += dr.Item(3)
                            End If
                        End If
                    End With
                Next

                Dim flagAudit As Integer
                If grmAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_ID(" + grmAddIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If grmEditIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_ID(" + grmEditIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If grmDeleteIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_ID(" + grmDeleteIds + ")", "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If ViewState("datamode") = "edit" Then
                    'Dim dtTemp As New DataTable
                    'dtTemp = SetDataTable()
                    'Dim drTemp As DataRow
                    'Dim i, j As Integer
                    'For i = 0 To dt.Rows.Count - 1
                    '    drTemp = dtTemp.NewRow
                    '    If dt.Rows(i)(14) <> "delete" And dt.Rows(i)(14) <> "remove" Then
                    '        For j = 0 To dt.Columns.Count - 1
                    '            drTemp.Item(j) = dt.Rows(i)(j)
                    '        Next
                    '        dtTemp.Rows.Add(drTemp)
                    '    End If
                    'Next

                    'gvStudGrade.DataSource = dtTemp
                    'gvStudGrade.DataBind()
                    If txtGradeDisplay.Text <> hfDisplay.Value.ToString Or txtGradeDescr.Text <> hfDescription.Value.ToString Or txtGradeCertificate.Text <> hfCertificate.Value.ToString Then
                        UtilityObj.InsertAuditdetails(transaction, "edit", "GRADE_BSU_M", "GRM_ID", "GRM_ID", "GRM_BSU_ID='" + Session("sbsuid") + "' AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "'")

                        str_query = "update grade_bsu_m set grm_display='" + txtGradeDisplay.Text + "',grm_descr='" + txtGradeDescr.Text + "',grm_certificate='" + txtGradeCertificate.Text + "',grm_priority=" + txtPriority.Text + " where grm_bsu_id='" + Session("sbsuid") + "' and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "'" _
                                    & " and grm_acd_id in(select acd_id from academicyear_d where acd_bsu_id='" + Session("sbsuid").ToString + "' and acd_clm_id=" + Session("clm").ToString + ")"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "GRM_BSU_ID(" + Session("sbsuid") + ")/GRM_GRD_ID(" + ddlGrade.SelectedValue + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                    End If

                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub CopyFromGrade()
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT grm_id,shf_descr,grm_shf_id,stm_descr,grm_stm_id," _
                                     & "CASE grm_gender WHEN 'A' THEN 'All' WHEN 'M' THEN 'Male' ELSE 'NO' END AS gender," _
                                     & "grm_maxcapacity,grm_mgmcapacity,CASE grm_openonline WHEN 1 THEN 'Yes' ELSE 'No' END AS openonline," _
                                     & "CASE grm_bscreeningreqd WHEN 1 THEN 'Yes' ELSE 'No' END AS screenreqd,grd_displayorder,ISNULL(GRM_bOPEN_REG_PAY,0)GRM_bOPEN_REG_PAY" _
                                     & " FROM grade_bsu_m,grade_m,shifts_m,stream_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id" _
                                     & " AND grade_bsu_m.grm_shf_id = shifts_m.shf_id And grade_bsu_m.grm_stm_id = stream_m.stm_id" _
                                     & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " AND grm_grd_id='" + ddlCopyGrade.SelectedValue + "' ORDER BY grd_displayorder"
            dt = Session("dtGrade")

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            Dim keys As Object()
            ReDim keys(3)
            Dim row As DataRow
            While reader.Read

                'if data for that grade is already added then remove that grades data and replace it with copied data

                keys(0) = ddlAcademicYear.SelectedValue.ToString
                keys(1) = ddlGrade.SelectedValue.ToString
                keys(2) = reader.GetValue(2).ToString
                keys(3) = reader.GetValue(4).ToString
                row = dt.Rows.Find(keys)
                If Not row Is Nothing Then
                    dt.Rows.RemoveAt(dt.Rows.IndexOf(row))
                End If

                dr = dt.NewRow
                dr.Item(0) = ddlAcademicYear.SelectedValue.ToString
                dr.Item(1) = ddlAcademicYear.SelectedItem.Text
                dr.Item(2) = ddlGrade.SelectedValue.ToString
                dr.Item(3) = 0
                dr.Item(4) = txtGradeDisplay.Text
                dr.Item(5) = reader.GetString(1)
                dr.Item(6) = reader.GetValue(2).ToString
                dr.Item(7) = reader.GetString(3)
                dr.Item(8) = reader.GetValue(4).ToString
                dr.Item(9) = reader.GetString(5)
                dr.Item(10) = reader.GetValue(6).ToString
                dr.Item(11) = reader.GetValue(7).ToString
                dr.Item(12) = reader.GetString(8)
                dr.Item(13) = reader.GetString(9)
                dr.Item(14) = "add"
                dr.Item(15) = txtGradeDescr.Text
                dr.Item(16) = txtGradeCertificate.Text
                dr.Item(17) = txtPriority.Text
                dr.Item(18) = reader.GetString(11).ToString
                dt.Rows.Add(dr)
            End While
            reader.Close()
            gvStudGrade.DataSource = dt
            gvStudGrade.DataBind()
            gvStudGrade.Columns(1).Visible = False
            Session("dtGrade") = dt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#End Region
End Class

