Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studCompMaster_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "S050075" And MainMnu_code <> "S050076" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))


    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCompanyRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCompanyRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvCompanyRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCompanyRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvCompanyRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCompanyRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvCompanyRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvCompanyRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_Comp_Name As String = String.Empty
            Dim str_filter_COMP_ADDR1 As String = String.Empty
            Dim str_filter_COMP_CITY As String = String.Empty
            Dim str_filter_CTY_DESCR As String = String.Empty

            Dim ds As New DataSet
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            str_Sql = "SELECT A.COMP_ID as COMP_ID, A.COMP_NAME  as COMP_NAME, A.COMP_ADDR1 as COMP_ADDR1 , " & _
                       " A.COMP_CITY as COMP_CITY,B.CTY_DESCR as CTY_DESCR, A.COMP_PHONE as COMP_PHONE, " & _
                      " A.COMP_EMAIL FROM COMP_LISTED_M AS A WITH(NOLOCK) INNER JOIN COUNTRY_M AS B WITH(NOLOCK) ON A.COMP_COUNTRY = B.CTY_ID WHERE A.COMP_bACTIVE=1 "
            If MainMnu_code = "S050076" Then 'if GEMS Global
                str_Sql = str_Sql & " AND ISNULL(A.COMP_COUNTRY, 0) NOT IN (172,97,105) " ' UAE,QATAR,SAUDI 
            End If

            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_Comp_Name As String = String.Empty
            Dim str_COMP_ADDR1 As String = String.Empty
            Dim str_COMP_CITY As String = String.Empty
            Dim str_CTY_DESCR As String = String.Empty


            If gvCompanyRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtComp_Name")
                If txtSearch IsNot Nothing Then
                    str_Comp_Name = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_Comp_Name = " AND isnull(A.COMP_NAME,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_Comp_Name = "  AND  NOT isnull(A.COMP_NAME,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_Comp_Name = " AND isnull(A.COMP_NAME,'')  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_Comp_Name = " AND isnull(A.COMP_NAME,'') NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_Comp_Name = " AND isnull(A.COMP_NAME,'') LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_Comp_Name = " AND isnull(A.COMP_NAME,'') NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                End If

                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtCOMP_ADDR1")
                If txtSearch IsNot Nothing Then
                    str_COMP_ADDR1 = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_COMP_ADDR1 = " AND isnull(A.COMP_ADDR1,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_COMP_ADDR1 = "  AND  NOT isnull(A.COMP_ADDR1,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_COMP_ADDR1 = " AND isnull(A.COMP_ADDR1,'')  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_COMP_ADDR1 = " AND isnull(A.COMP_ADDR1,'') NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_COMP_ADDR1 = " AND isnull(A.COMP_ADDR1,'') LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_COMP_ADDR1 = " AND isnull(A.COMP_ADDR1,'') NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                End If
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtCity")
                If txtSearch IsNot Nothing Then
                    str_COMP_CITY = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_COMP_CITY = " AND isnull(A.COMP_CITY,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_COMP_CITY = "  AND  NOT isnull(A.COMP_CITY,'')  LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_COMP_CITY = " AND isnull(A.COMP_CITY,'')   LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_COMP_CITY = " AND isnull(A.COMP_CITY,'')  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_COMP_CITY = " AND isnull(A.COMP_CITY,'') LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_COMP_CITY = " AND isnull(A.COMP_CITY,'')  NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                End If
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtCTY_DESCR")
                If txtSearch IsNot Nothing Then
                    str_CTY_DESCR = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_CTY_DESCR = " AND isnull(B.CTY_DESCR,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_CTY_DESCR = "  AND  NOT isnull(B.CTY_DESCR,'') LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_CTY_DESCR = " AND isnull(B.CTY_DESCR,'') LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_CTY_DESCR = " AND isnull(B.CTY_DESCR,'')  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_CTY_DESCR = " AND isnull(B.CTY_DESCR,'') LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_CTY_DESCR = " AND isnull(B.CTY_DESCR,'') NOT LIKE '%" & txtSearch.Text & "'"
                    End If
                End If
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_Comp_Name & str_filter_COMP_ADDR1 & str_filter_COMP_CITY & str_filter_CTY_DESCR & " ORDER BY A.COMP_NAME")


            If ds.Tables(0).Rows.Count > 0 Then

                gvCompanyRecord.DataSource = ds.Tables(0)
                gvCompanyRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvCompanyRecord.DataSource = ds.Tables(0)
                Try
                    gvCompanyRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCompanyRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCompanyRecord.Rows(0).Cells.Clear()
                gvCompanyRecord.Rows(0).Cells.Add(New TableCell)
                gvCompanyRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCompanyRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCompanyRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


            txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtComp_Name")
            txtSearch.Text = str_Comp_Name
            txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtCOMP_ADDR1")
            txtSearch.Text = str_COMP_ADDR1
            txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtCity")
            txtSearch.Text = str_COMP_CITY
            txtSearch = gvCompanyRecord.HeaderRow.FindControl("txtCTY_DESCR")
            txtSearch.Text = str_CTY_DESCR



            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
   
    Protected Sub gvCompanyRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCompanyRecord.PageIndexChanging
        gvCompanyRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub btnSearchCompName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchAddr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchCity_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchCountry_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblComp_ID As New Label
            Dim url As String
            Dim viewid As String
            lblComp_ID = TryCast(sender.FindControl("lblComp_ID"), Label)
            viewid = lblComp_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studCompMaster_Edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studCompMaster_Edit.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    

   
    Protected Sub gvCompanyRecord_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim hlview As New HyperLink
            Dim lblComp_ID As New Label
            Dim url As String
            Dim viewid As String
            If e.Row.RowType = DataControlRowType.DataRow Then
                hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
                lblComp_ID = TryCast(e.Row.FindControl("lblComp_ID"), Label)


                viewid = lblComp_ID.Text
                'define the datamode to view if view is clicked
                ViewState("datamode") = "view"
                'Encrypt the data that needs to be send through Query String
                MainMnu_code = Request.QueryString("MainMnu_code")
                viewid = Encr_decrData.Encrypt(viewid)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                url = String.Format("~\Students\studCompMaster_Edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
                hlview.NavigateUrl = url
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
End Class
