Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports System.Net.Mail
'Imports System.Web.Mail
Partial Class Students_studPrintOffer
    Inherits System.Web.UI.Page
    Dim encr As New Encryption64
    'Protected Sub lnkOffer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOffer.Click
    '    ''Redirecting to offer Letter Preview.
    '    Dim eqsid As String = encr.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
    '    ''Response.Redirect("~/Students/studOfferLetter_View.aspx?eqsid=" & eqsid)

    '    ' Response.Write("<Script> window.open('studOfferLetter_View.aspx?eqsid=" & eqsid & "') </Script>")

    '    Dim param As New Hashtable


    '    param.Add("@IMG_BSU_ID", Session("sBsuid"))
    '    param.Add("@IMG_TYPE", "LOGO")
    '    param.Add("@EQS_IDs", eqsid)
    '    param.Add("@BSU_ID", Session("sBsuid"))
    '    param.Add("@BAL_APL_ID", GetAPL_ID(eqsid))
    '    param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
    '    param.Add("UserName", Session("sUsr_name"))

    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
    '        .reportParameters = param

    '        .reportPath = Server.MapPath("~\Students\Reports\RPT\rptOffer_letter.rpt")


    '    End With
    '    Session("rptClass") = rptClass
    '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")




    'End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False

    'End Sub
    Protected Sub lnkRegs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRegs.Click
        ''Redirecting to Student Enquiry. 
        Response.Redirect("~/Students/studEnquiryNew.aspx?datamode=Zo4HhpVNpXc=&MainMnu_code=LjcF1BVVHbk=")

    End Sub


    'Function GetAPL_ID(ByVal eqsid As String) As String
    '    Dim sqlString As String = "select  EQS_APL_ID from  ENQUIRY_SCHOOLPRIO_S where EQS_ID = '" & eqsid & "'"

    '    Dim result As Object
    '    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '        Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '        command.CommandType = Data.CommandType.Text
    '        result = command.ExecuteScalar
    '        SqlConnection.ClearPool(connection)
    '    End Using
    '    Return CStr(result)

    'End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    Dim Encr_decrData As New Encryption64()
        '    Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        '    Dim reader As SqlDataReader
        '    Dim str_query As String = ""
        '    Dim email As String = ""
        '    Dim prefer As String = ""
        '    If Not IsPostBack Then
        '        Dim enqid As String = Encr_decrData.Decrypt(Request.QueryString("enqid"))
        '        ''Check the Prefered Contact Option
        '        str_query = "SELECT EQM_PREFCONTACT FROM ENQUIRY_M WHERE EQM_ENQID='" + enqid + "'"

        '        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        '        If (reader.Read()) Then

        '            If reader.GetString(0).ToString.ToUpper = "EMAIL" Then ''If  Prefered Contact Option is Email.

        '                ''Get the Emailid, Firstname Lastname of the parent.
        '                str_query = "SELECT   EQP_FEMAIL, EQP_FFIRSTNAME + '  ' + EQP_FLASTNAME AS NAME" & _
        '                            " FROM ENQUIRY_PARENT_M WHERE EQP_EQM_ENQID='" + enqid + "'"

        '                reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        '                If (reader.Read()) Then

        '                    email = reader.GetString(0) ''Get Email Address
        '                    If email <> "" Then ''Check for empty field
        '                        lnkmail.Text = email

        '                        lblname.Text = reader.GetString(1)
        '                        Td1.Visible = True ''Table Row (Email Display)
        '                    Else
        '                        Td1.Visible = False ''Table Row (Email Display)
        '                    End If
        '                End If
        '            End If
        '        End If

        '    End If
        'Catch ex As Exception

        'End Try



        '************Page Header************'
        'sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        'sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        'sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        'sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        'sb.AppendLine("<BODY>")
        'sb.AppendLine("</BODY></HTML>")

        ''Added by Surya on 25-Jan-2010

        tr_dupli.Visible = False
        clikcoffer.Visible = False
        Try
            Dim Encr_decrData As New Encryption64()
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim str_query As String = ""
            Dim email As String = ""
            Dim prefer As String = ""
            Dim StrenqApplNo As String = ""
            If Not IsPostBack Then
                'Dim enqid As String = Encr_decrData.Decrypt(Request.QueryString("enqid"))
                ''Check the Prefered Contact Option
                'StrenqApplNo = Request.QueryString("enqApplNo")
                'Dim lblenqApplNo As New Label
                StrenqApplNo = Session("enqApplNo")
                str_query = "SELECT EQS_ID FROM  ENQUIRY_SCHOOLPRIO_S WHERE EQS_APPLNO='" + StrenqApplNo + "' AND EQS_BSU_ID=" & Session("sBsuid") & ""

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count > 0 Then
                    Session("EQSID") = ds.Tables(0).Rows(0).Item("EQS_ID")
                    BindOfferType()
                    Offer_letter_Data()
                End If
                'GET_STUDENTSHORTLIST_DUE()
            End If
        Catch ex As Exception

        End Try

        ''Over
    End Sub
    Sub BindOfferType()

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
            pParms(2) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetBSU_OfferLetters", pParms)

            ddlOFFR_TYPE.DataSource = ds
            ddlOFFR_TYPE.DataTextField = "BSO_TITLE"
            ddlOFFR_TYPE.DataValueField = "BSO_TITLE"
            ddlOFFR_TYPE.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GET_STUDENTSHORTLIST_DUE()
        Try
            Dim str_Sql As String = "exec FEES.GET_STUDENTSHORTLIST_DUE  '" & Session("EQSID").ToString & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            
            GridViewShowDetails.EmptyDataText = "No data Found"
            GridViewShowDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    'Protected Sub lnkmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkmail.Click
    '    '   ScreenScrapeHtml("http://localhost/OASIS/Students/studOfferLetter_View.aspx?eqsid=KXfMDxj1hQ0=")
    '    Dim sb As New StringBuilder
    '    sb = CreateOfferLetter(encr.Decrypt(Request.QueryString("eqsid").Replace(" ", "+")))
    '    'SendNewsLetters(lnkmail.Text,lnkmail.Text,"OFFER LETTER",
    'End Sub
    'Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As AlternateView, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String) As String

    '    Dim RetutnValue = ""

    '    Try



    '        Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)



    '        msg.Subject = Subject



    '        msg.AlternateViews.Add(MailBody)



    '        msg.Priority = Net.Mail.MailPriority.High



    '        msg.IsBodyHtml = True





    '        Dim client As New System.Net.Mail.SmtpClient(Host, Port)



    '        If Username <> "" And password <> "" Then

    '            Dim creds As New System.Net.NetworkCredential(Username, password)

    '            client.Credentials = creds

    '        End If





    '        ' for other authentication with local server



    '        'client.Credentials= system.Net.CredentialCache.DefaultCredentials



    '        'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials



    '        ' Async send



    '        'Dim MailToken As String = My.User.Name



    '        'client.SendAsync(msg, emaildid)



    '        'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete



    '        client.Send(msg)



    '        RetutnValue = "Successfully send"



    '    Catch ex As Exception

    '        RetutnValue = "Error : " & ex.Message

    '    End Try



    '    Return RetutnValue



    'End Function


    'Private Function CreateOfferLetter(ByVal eqsid As String) As StringBuilder
    '    Try
    '        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        Dim str_query As String = "SELECT bsu_offerfilepath from businessunit_m where bsu_id='" + Session("sbsuid") + "'"
    '        Dim filedbpath As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    '        Dim filepath As String = Server.MapPath(filedbpath) ''SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    '        str_query = "exec  studGetOfferLetterInfo " + eqsid
    '        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '        Dim fr As System.IO.TextReader
    '        fr = File.OpenText(filepath)
    '        Dim sb As New StringBuilder
    '        Dim sr As String = fr.ReadToEnd
    '        While reader.Read
    '            sr = sr.Replace("%studdate", Format(Now, "dd-MMM-yyyy"))
    '            sr = sr.Replace("%studname", reader.GetString(0))
    '            sr = sr.Replace("%studgrade", reader.GetString(1))
    '            sr = sr.Replace("%studacademicyear", reader.GetString(2))
    '            sr = sr.Replace("%studstart", Format(reader.GetDateTime(3), "MMMM yyyy"))
    '            sr = sr.Replace("%studregistrar", reader.GetString(4).ToUpper)
    '        End While

    '        sb.Append(sr)
    '        Return sb

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try

    'End Function

    'Protected Function GetNavigateUrl() As String
    '    Dim str As String = "javascript:var popup = window.showModalDialog('studJoinDocuments.aspx?', '','dialogHeight:400px;dialogWidth:705px;scroll:yes;resizable:no;');"
    '    Return str
    'End Function
    Private Function GetOffer_DOJDATA() As SqlDataReader
      
        Dim sqlOffer_LetterDATA As String = " Select replace(CONVERT(varchar,EQS_OFR_DOJ,106),' ','/') as EQS_OFR_DOJ,replace(CONVERT(varchar,EQS_OFR_LDATE,106),' ','/') as EQS_OFR_LDATE,EQS_EXTRADOC  From Enquiry_SchoolPrio_S WHERE EQS_ID=" & Session("EQSID") & "  " & _
"  AND EQS_BSU_ID='" & Session("sBsuid") & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlOffer_LetterDATA, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Sub Offer_letter_Data()
        Try
            Using readerOFFER_LETTER As SqlDataReader = GetOffer_DOJDATA()

                If readerOFFER_LETTER.HasRows = True Then
                    While readerOFFER_LETTER.Read
                        txtDoj.Text = readerOFFER_LETTER("EQS_OFR_DOJ")
                        txtLDate.Text = readerOFFER_LETTER("EQS_OFR_LDATE")
                        txtNOTE.Text = readerOFFER_LETTER("EQS_EXTRADOC")




                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblError.Text = ""

        'If Date.Parse(txtDoj.Text) < Now.Date Then
        '    lblError.Text = "Date of Join should not be less than current date"
        '    Exit Sub
        'End If

        If CheckValid_DOJDate() = False Then
            lblError.Text = "Date of Join should be within the academic year"
            Exit Sub
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(13) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@EQS_ID", Session("EQSID"))
        pParms(2) = New SqlClient.SqlParameter("@EQS_OFR_DOJ", txtDoj.Text)
        pParms(3) = New SqlClient.SqlParameter("@EQS_EXTRADOC", Replace(txtNOTE.Text, "'", ""))
        pParms(4) = New SqlClient.SqlParameter("@EQS_OFR_LDATE", txtLDate.Text)
        pParms(5) = New SqlClient.SqlParameter("@EQS_USR", Session("sUsr_name"))
        pParms(6) = New SqlClient.SqlParameter("@EQS_OFR_TYPE", ddlOFFR_TYPE.SelectedItem.Value)



        pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "STU.SaveBSU_OFFER_LETTER_EXTRA", pParms)

        PrintOfferLetter("Preview")

        'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenModalDialog", "<script type=text/javascript>window.showModalDialog('stuOfferLetterPrint.aspx?code=" + txtDoj.ToString() + "', null, 'dialogWidth:850px;dialogHeight:600px;status:no;help: no;resizable: no;scroll: yes;unadorned: no;'); </script>", False)


    End Sub
    Protected Sub btnMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMail.Click
        lblError.Text = ""

        'If Date.Parse(txtDoj.Text) < Now.Date Then
        '    lblError.Text = "Date of Join should not be less than current date"
        '    Exit Sub
        'End If

        If CheckValid_DOJDate() = False Then
            lblError.Text = "Date of Join should be within the academic year"
            Exit Sub
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(13) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@EQS_ID", Session("EQSID"))
        pParms(2) = New SqlClient.SqlParameter("@EQS_OFR_DOJ", txtDoj.Text)
        pParms(3) = New SqlClient.SqlParameter("@EQS_EXTRADOC", Replace(txtNOTE.Text, "'", ""))
        pParms(4) = New SqlClient.SqlParameter("@EQS_OFR_LDATE", txtLDate.Text)
        pParms(5) = New SqlClient.SqlParameter("@EQS_USR", Session("sUsr_name"))

        pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "STU.SaveBSU_OFFER_LETTER_EXTRA", pParms)

        PrintOfferLetter("email")
    End Sub


    Function CheckValid_DOJDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "Select Count(ACD_ID) from Enquiry_SchoolPrio_S A INNER JOIN AcademicYear_D B ON EQS_ACD_ID=ACD_ID WHERE EQS_ID=" & Session("EQSID") & " AND '" & txtDoj.Text & "'  BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function

    Public Sub PrintOfferLetter(ByVal DisplayType As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = String.Empty
            Dim s As String = String.Empty
            Dim tnc As String = String.Empty
            Dim RegistrarEmail As String = String.Empty
            Dim savedPath As String = String.Empty

            Dim sEqsid As String = ""
            sEqsid = Session("EQSID")
            'Session("EQSID") = "383559"
            'Session("sBSUID") = "123004"
            Dim stu_no As String = String.Empty
            Dim MailContent As New StringBuilder
            Dim parentName As String = String.Empty
            Dim LOG_USERNAME As String = String.Empty
            Dim LOG_PASSWORD As String = String.Empty
            Dim LOG_HOST As String = String.Empty
            Dim LOG_PORT As String = String.Empty
            Dim RegistrarName As String = String.Empty
            Dim Parentemail As String = String.Empty
            Dim Email_Subject As String = String.Empty
            Dim EMAILSTATUS As String = String.Empty
            Dim OnBehalf As String
            OnBehalf = "On Behalf of Principal/CEO"

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
            pParms(1) = New SqlClient.SqlParameter("@ROLE", "STAFF")
            pParms(2) = New SqlClient.SqlParameter("@EQS_ID", sEqsid)
            pParms(3) = New SqlClient.SqlParameter("@OFR_TYPE", ddlOFFR_TYPE.SelectedItem.Value)

            Using readerData As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_BSU_OFFER_PRINT_DETAILS", pParms)
                While readerData.Read
                    s = Convert.ToString(readerData("BSU_STUD_OFFRLETTER"))
                    tnc = Convert.ToString(readerData("BSU_OFFER_TNC"))
                    RegistrarEmail = Convert.ToString(readerData("BSU_OFR_REGISTRAR_EMAIL"))
                    s = s.Replace("##DATE##", Convert.ToString(readerData("CURRENTDATE")))
                    s = s.Replace("##AcademicYear##", Convert.ToString(readerData("AcademicYear")))
                    s = s.Replace("##REGISTRAREMAIL##", RegistrarEmail)
                    s = s.Replace("##STUDENTNAME##", Convert.ToString(readerData("StudentName")))
                    s = s.Replace("##STUDENTFEEID##", Convert.ToString(readerData("EQS_STU_NO")))
                    tnc = tnc.Replace("##STUDENTNAME##", Convert.ToString(readerData("StudentName")))
                    tnc = tnc.Replace("##DOB##", Convert.ToString(readerData("EQM_APPLDOB")))
                    s = s.Replace("##GRADE##", Convert.ToString(readerData("Grade")))
                    s = s.Replace("##REGISTRAR##", Convert.ToString(readerData("RegistrarName")))
                    s = s.Replace("##SCHOOLNAME##", Convert.ToString(readerData("School")))
                    tnc = tnc.Replace("##SCHOOLNAME##", Convert.ToString(readerData("School")))
                    s = s.Replace("##Designation##", Convert.ToString(readerData("Designation")))
                    s = s.Replace("##Telephone##", Convert.ToString(readerData("Telephone")))
                    s = s.Replace("##Emirate##", Convert.ToString(readerData("Emirate")))
                    s = s.Replace("##PARENTNAME##", Convert.ToString(readerData("ParentName")))
                    tnc = tnc.Replace("##PARENTNAME##", Convert.ToString(readerData("ParentName")))
                    s = s.Replace("##STARTDATE##", Convert.ToString(readerData("EQS_OFR_DOJ")))
                    s = s.Replace("##EXTRADOCS##", Convert.ToString(readerData("EQS_EXTRADOC")))
                    s = s.Replace("##RETDATE##", Convert.ToString(readerData("RETDATE")))
                    s = s.Replace("##AMOUNT##", Convert.ToString(readerData("ADMIN_AMT")))
                    Parentemail = Convert.ToString(readerData("ParentEmail"))
                    Email_Subject = Convert.ToString(readerData("School")) + " - Offer Letter  " + Convert.ToString(readerData("Grade")) + " " + Convert.ToString(readerData("StudentName"))
                    Session("Offer_letter_BSU_NAME") = Convert.ToString(readerData("BSU_NAME"))
                    Session("Offer_letter_BSU_LOGO") = Convert.ToString(readerData("BSU_MOE_LOGO"))
                    stu_no = Convert.ToString(readerData("EQS_STU_NO"))

                    LOG_USERNAME = Convert.ToString(readerData("LOG_USERNAME"))
                    LOG_PASSWORD = Convert.ToString(readerData("LOG_PASSWORD"))
                    LOG_HOST = Convert.ToString(readerData("LOG_HOST"))
                    LOG_PORT = Convert.ToString(readerData("LOG_PORT"))
                    parentName = Convert.ToString(readerData("ParentName"))
                    RegistrarName = Convert.ToString(readerData("RegistrarName"))
                    OnBehalf = Convert.ToString(readerData("OnBehalf"))

                End While
            End Using

            If s <> "" Then

                If DisplayType = "Preview" Then

                    HTML2PDF_Preview(s, tnc, stu_no)
                Else

                    HTML2PDF_Email(s, tnc, stu_no, savedPath)

                    MailContent.Append("<table border='0' style=' font-family: Verdana;font-size: 12pt;color: #000000;'>")
                    MailContent.Append("<tr><td  >Dear Parent(s) </td></tr>")
                    MailContent.Append("<tr><td ><br />Please find enclosed an Offer Letter from our Principal.<br /><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >Yours sincerely, </td></tr>")
                    MailContent.Append("<tr><td><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >" & RegistrarName & "   (" & RegistrarEmail & ") </td></tr>")
                    MailContent.Append("<tr><td><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >" & OnBehalf & " </td></tr>")

                    MailContent.Append("</table>")

                    ' SendPDFEmails( FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
                    
                    EMAILSTATUS = SendPDFEmails(RegistrarEmail, Parentemail, Email_Subject, MailContent.ToString, LOG_USERNAME, LOG_PASSWORD, LOG_HOST, LOG_PORT, savedPath, True)


                    OFFER_LETTER_PDF_LOG(Parentemail, EMAILSTATUS)
                    If File.Exists(savedPath) Then
                        File.Delete(savedPath)
                    End If

                    If EMAILSTATUS.Contains("Successfully") Then
                        lblError.Text = "Offer Letter succesfully emailed to " + Parentemail
                    Else
                        lblError.Text = "Error while sending email attachment to " + Parentemail
                    End If

                End If
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub HTML2PDF_Email(ByVal HTMLStr As String, ByVal HTMLTERM As String, ByVal stu_no As String, ByRef savedPath As String)
        Try

            Using myMemoryStream As New MemoryStream()
                Dim page As New pdfPage
                Dim document As New Document(PageSize.A4, 10, 10, 10, 10)
                Dim myPDFWriter As PdfWriter = PdfWriter.GetInstance(document, myMemoryStream)
                Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("OfferLetterFilepath").ToString()


                Dim pathSave As String = "Offer Letter-" & stu_no & ".pdf"
                If Not Directory.Exists(PhyPath & "\" & Session("sBSUID") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(PhyPath & "\" & Session("sBSUID") & "\")
                End If

                Dim path = PhyPath & "\" & Session("sBSUID") & "\" & pathSave
                myPDFWriter.PageEvent = page
                document.Open()

                ' Add to content to your PDF here...

                Dim htmlarraylist = HTMLWorker.ParseToList(New StringReader(HTMLStr), Nothing)
                For k As Integer = 0 To htmlarraylist.Count - 1
                    document.Add(DirectCast(htmlarraylist(k), IElement))
                Next
                If HTMLTERM.Trim <> "" Then
                    HTMLTERM = "<div style='page-break-after:always;'></div>" & HTMLTERM

                    Dim htmlarraylist_terms = HTMLWorker.ParseToList(New StringReader(HTMLTERM), Nothing)
                    For i As Integer = 0 To htmlarraylist_terms.Count - 1
                        document.Add(DirectCast(htmlarraylist_terms(i), IElement))

                    Next
                End If



                ' We're done adding stuff to our PDF.
                document.Close()

                Dim content As Byte() = myMemoryStream.ToArray()

                ' Write out PDF from memory stream.
                Using fs As FileStream = File.Create(path)
                    fs.Write(content, 0, CInt(content.Length))
                End Using

                savedPath = path
            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub HTML2PDF_Preview(ByVal HTMLStr As String, ByVal HTMLTERM As String, ByVal stu_no As String)
        Try

            Dim document As New Document(PageSize.A4, 10, 10, 10, 10)
            Dim output = New MemoryStream()
            'Change the content type to PDF
            HttpContext.Current.Response.ContentType = "application/pdf"
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'create an instance of your PDFpage class. This is the class we generated above.
            Dim page As New pdfPage

            'create an instance of the PdfWriter and write to the Response.OutputStream.  directly to the browser
            Dim pdfWriter1 As PdfWriter = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream)

            'set the PageEvent of the pdfWriter instance to the instance of our PDFPage class
            pdfWriter1.PageEvent = page

            'open the document
            document.Open()
            Dim css As New StyleSheet()
            css.LoadTagStyle("HtmlTags.DIV", "HtmlTags.BORDER", "1")




            Dim htmlarraylist = HTMLWorker.ParseToList(New StringReader(HTMLStr), Nothing)
            For k As Integer = 0 To htmlarraylist.Count - 1
                document.Add(DirectCast(htmlarraylist(k), IElement))
            Next
            If HTMLTERM.Trim <> "" Then
                HTMLTERM = "<div style='page-break-after:always;'></div>" & HTMLTERM

                Dim htmlarraylist_terms = HTMLWorker.ParseToList(New StringReader(HTMLTERM), Nothing)
                For i As Integer = 0 To htmlarraylist_terms.Count - 1
                    document.Add(DirectCast(htmlarraylist_terms(i), IElement))

                Next
            End If

            document.Close()
            'Dim bytearray() As Byte
            'bytearray = output.ToArray

            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=Offer Letter-{0}.pdf", stu_no))
            HttpContext.Current.Response.BinaryWrite(output.ToArray)
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.Close()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function SendPDFEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
        Dim ReturnValue As String = ""
        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage
        Dim mailfrom As New System.Net.Mail.MailAddress(FromEmailId, "Registrar")

        Try

            msg.From = mailfrom
            msg.To.Add(ToEmailId)

            If chkEmailCopy.Checked = True Then
                msg.Bcc.Add(FromEmailId)
            End If

            msg.Subject = Subject
            msg.Body = MailBody
            msg.Priority = Net.Mail.MailPriority.High
            msg.IsBodyHtml = True
            '' If Attachments.
            If HasAttachments Then
                Dim attach As New System.Net.Mail.Attachment(Templateid)
                msg.Attachments.Add(attach)

                'If Session("sBSUID") = "900201" Then
                '    Dim attach2 As New System.Net.Mail.Attachment("\\172.16.1.11\oasisphotos\OFFERLETTER\Extras\900201\PEI Student Contract.pdf")
                '    msg.Attachments.Add(attach2)
                'End If
            End If

            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)
            client = Nothing
            msg.Dispose()
            ReturnValue = "Successfully sent to " & ToEmailId
        Catch ex As Exception
            ReturnValue = "Error in Sending Mail :" & Date.Today.ToString & " " & ex.Message
            client = Nothing
            msg.Dispose()
        End Try
        Return ReturnValue
    End Function
    Private Sub OFFER_LETTER_PDF_LOG(ByVal PARENT_EMAIL As String, ByVal STATUS As String)
        Try


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OL_BSU_ID", Session("sBSUID"))
            pParms(1) = New SqlClient.SqlParameter("@OL_EQS_ID", Session("EQSID"))
            pParms(2) = New SqlClient.SqlParameter("@OL_STATUS", STATUS)
            pParms(3) = New SqlClient.SqlParameter("@OL_EMAIL_PAR", PARENT_EMAIL)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "SAVEOFFER_LETTER_EMAIL_LOG", pParms)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    
End Class
Public Class pdfPage
    Inherits iTextSharp.text.pdf.PdfPageEventHelper
    'I create a font object to use within my footer
    'Protected ReadOnly Property footer() As Font
    '    Get
    '        ' create a basecolor to use for the footer font, if needed.
    '        Dim grey As New BaseColor(128, 128, 128)
    '        Dim font__1 As Font = FontFactory.GetFont("Arial", 9, Font.NORMAL, grey)
    '        Return font__1
    '    End Get
    'End Property
    'override the OnStartPage event handler to add our header
    Public Overrides Sub OnStartPage(ByVal writer As PdfWriter, ByVal doc As Document)
        'I use a PdfPtable with 2 column to position my header where I want it
        Dim bsu_name As String = HttpContext.Current.Session("Offer_letter_BSU_NAME")
        Dim col1 As Integer
        Dim col2 As Integer
        Dim headerTbl As New PdfPTable(2)
        If bsu_name.Length <= 30 Then
            col1 = 20
            col2 = 35
        Else
            col1 = 12
            col2 = 36
        End If
        Dim intTblWidth() As Integer = {col1, col2}

        headerTbl.SetWidths(intTblWidth)


        'set the width of the table to be the same as the document
        headerTbl.TotalWidth = doc.PageSize.Width

        'I use an image logo in the header so I need to get an instance of the image to be able to insert it. I believe this is something you couldn't do with older versions of iTextSharp
        Dim logo As Image = Image.GetInstance(HttpContext.Current.Server.MapPath(HttpContext.Current.Session("Offer_letter_BSU_LOGO")))


        logo.ScalePercent(70%)

        'Create a paragraph that contains the footer text
        Dim para As New Paragraph
        para.Add(HttpContext.Current.Session("Offer_letter_BSU_NAME"))
        'add a carriage return
        'para.Add(Environment.NewLine)
        'para.Add("P.O Box :53663,DUBAI")

        'create instance of a table cell to contain the logo
        Dim cell As New PdfPCell(logo)

        'align the logo to the right of the cell
        cell.HorizontalAlignment = Element.ALIGN_LEFT

        'add a bit of padding to bring it away from the right edge
        cell.PaddingLeft = 20

        'remove the border
        cell.Border = 0

        'Add the cell to the table
        headerTbl.AddCell(cell)

        cell = New PdfPCell(para)

        cell.HorizontalAlignment = Element.ALIGN_LEFT
        cell.Border = 0

        headerTbl.AddCell(cell)
        'para = New Paragraph("")
        'cell = New PdfPCell(para)
        'cell.HorizontalAlignment = Element.ALIGN_CENTER
        'cell.Border = 0
        'headerTbl.AddCell(cell)

        'write the rows out to the PDF output stream. I use the height of the document to position the table. Positioning seems quite strange in iTextSharp and caused me the biggest headache.. It almost seems like it starts from the bottom of the page and works up to the top, so you may ned to play around with this.
        headerTbl.WriteSelectedRows(0, -1, 0, (doc.PageSize.Height - 10), writer.DirectContent)
    End Sub

    'override the OnPageEnd event handler to add our footer
    Public Overrides Sub OnEndPage(ByVal writer As PdfWriter, ByVal doc As Document)
        'I use a PdfPtable with 2 columns to position my footer where I want it
        Dim footerTbl As New PdfPTable(1)

        'set the width of the table to be the same as the document
        footerTbl.TotalWidth = doc.PageSize.Width

        'Center the table on the page
        footerTbl.HorizontalAlignment = Element.ALIGN_CENTER



        'I use an i mage logo in the header so I need to get an instance of the image to be able to insert it. I believe this is something you couldn't do with older versions of iTextSharp
        Dim logo As Image = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Images/swoosh_small.png"))

        'I used a large version of the logo to maintain the quality when the size was reduced. I guess you could reduce the size manually and use a smaller version, but I used iTextSharp to reduce the scale. As you can see, I reduced it down to 7% of original size.
        logo.ScalePercent(50.0F)


        'create a cell instance to hold the text
        Dim cell As New PdfPCell(logo)

        'set cell border to 0
        cell.Border = 0
        'align the text to the right of the cell
        cell.HorizontalAlignment = Element.ALIGN_RIGHT
        'add some padding to bring away from the edge
        cell.PaddingRight = 0

        'add cell to table
        footerTbl.AddCell(cell)


        'write the rows out to the PDF output stream.
        footerTbl.WriteSelectedRows(0, -1, 0, (doc.BottomMargin + 170), writer.DirectContent)
    End Sub
End Class