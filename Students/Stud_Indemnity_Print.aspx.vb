Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports SmsService
Partial Class Students_Indemnity_Print
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100434") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

                    rbEnq.Checked = True
                    Call bindAcademic_Year()



                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        For Each gvr As GridViewRow In gvAlloc.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        set_Menu_Img()
        'SetChk(Me.Page)
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))



    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ACD_ID As String = ddlAcademicYear.SelectedValue

            Dim str_Sql As String = ""
            Dim str_filter_ID As String = String.Empty
            Dim str_filter_Name As String = String.Empty
            Dim str_filter_GRM_DISPLAY As String = String.Empty
            Dim str_filter_Status As String = String.Empty

            Dim ds As New DataSet

            If rbEnq.Checked = True Then

                str_Sql = " SELECT STU_ENQ_ID,ID,NAME,GRM_DISPLAY,GRD_DISPLAYORDER,[STATUS]  FROM( " & _
    " select M.EQM_ENQID AS STU_ENQ_ID,S.EQS_APPLNO AS ID,M.EQM_APPLFIRSTNAME + ' ' +ISNULL(M.EQM_APPLMIDNAME,'')+' '+ISNULL(M.EQM_APPLLASTNAME,'') AS NAME, " & _
    " B.GRM_DISPLAY,G.GRD_DISPLAYORDER, CASE WHEN S.EQS_CURRSTATUS=2 THEN 'ENQUIRY'   WHEN S.EQS_CURRSTATUS=3 THEN 'REGISTER' " & _
    " WHEN S.EQS_CURRSTATUS=4 THEN 'SCREENING'   WHEN S.EQS_CURRSTATUS=5 THEN 'APPROVAL'  WHEN S.EQS_CURRSTATUS=6 THEN 'OFFER LETTER'   END  AS [STATUS]  " & _
     " from ENQUIRY_M AS M inner join ENQUIRY_SCHOOLPRIO_S as S ON M.EQM_ENQID=S.EQS_EQM_ENQID  INNER JOIN GRADE_BSU_M AS B ON B.GRM_ACD_ID=S.EQS_ACD_ID " & _
     " AND B.GRM_GRD_ID=S.EQS_GRD_ID INNER JOIN GRADE_M  AS G ON  G.GRD_ID=B.GRM_GRD_ID  WHERE  S.eqs_acd_id='" & ACD_ID & "'" & _
    "  AND S.EQS_CANCELDATE IS NULL AND S.EQS_STATUS<>'DEL' AND S.EQS_STATUS<>'ENR' AND S.EQS_CURRSTATUS>1)A WHERE STU_ENQ_ID<>''"

            ElseIf rbStud.Checked = True Then

                str_Sql = " SELECT STU_ENQ_ID,ID,NAME,GRM_DISPLAY,GRD_DISPLAYORDER,[STATUS]  FROM(" & _
" SELECT STU_ID  AS STU_ENQ_ID,STU_FEE_ID AS ID,STU_FIRSTNAME+' '+ ISNULL(STU_MIDNAME,'') + ' '+ISNULL(STU_LASTNAME,'') AS NAME, B.GRM_DISPLAY" & _
" ,G.GRD_DISPLAYORDER,'ENROLL' AS [STATUS] " & _
" FROM STUDENT_M AS M INNER JOIN GRADE_BSU_M AS B ON B.GRM_ACD_ID=M.STU_ACD_ID AND B.GRM_GRD_ID=M.STU_GRD_ID INNER JOIN GRADE_M  AS G ON" & _
"  G.GRD_ID=B.GRM_GRD_ID  WHERE  (STU_CURRSTATUS ='EN') AND (convert(datetime,STU_LASTATTDATE) >= GETDATE() or " & _
" convert(datetime,STU_LASTATTDATE) is null ) and  STU_DOJ <= GETDATE()  AND STU_ACD_ID='" & ACD_ID & "') A WHERE STU_ENQ_ID<>''"

            End If

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_ID As String = String.Empty
            Dim str_Name As String = String.Empty
            Dim str_GRM_DISPLAY As String = String.Empty
            Dim str_Status As String = String.Empty

            If gvAlloc.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtID")
                str_ID = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_ID = " AND ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ID = "  AND  NOT ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ID = " AND ID  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ID = " AND ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ID = " AND ID LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ID = " AND ID NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtNAME")
                str_Name = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Name = " AND Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Name = "  AND  NOT Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Name = " AND Name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Name = " AND Name NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Name = " AND Name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Name = " AND Name NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtGRM_DISPLAY")
                str_GRM_DISPLAY = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRM_DISPLAY = " AND GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRM_DISPLAY = "  AND  NOT GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRM_DISPLAY = " AND GRM_DISPLAY LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRM_DISPLAY = " AND GRM_DISPLAY NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRM_DISPLAY = " AND GRM_DISPLAY LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRM_DISPLAY = " AND GRM_DISPLAY NOT LIKE '%" & txtSearch.Text & "'"
                End If




                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAlloc.HeaderRow.FindControl("txtStatus")
                str_Status = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Status = " AND [Status] LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Status = "  AND  NOT [Status]  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Status = " AND [Status]   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Status = " AND [Status]  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Status = " AND [Status] LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Status = " AND [Status]  NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_ID & str_filter_Name & str_filter_GRM_DISPLAY & str_filter_Status & " ORDER BY GRD_DISPLAYORDER,NAME")


            If ds.Tables(0).Rows.Count > 0 Then


                gvAlloc.DataSource = ds.Tables(0)
                gvAlloc.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(5) = True

                gvAlloc.DataSource = ds.Tables(0)
                Try
                    gvAlloc.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAlloc.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAlloc.Rows(0).Cells.Clear()
                gvAlloc.Rows(0).Cells.Add(New TableCell)
                gvAlloc.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAlloc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAlloc.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAlloc.HeaderRow.FindControl("txtID")
            txtSearch.Text = str_ID
            txtSearch = gvAlloc.HeaderRow.FindControl("txtNAME")
            txtSearch.Text = str_Name

            txtSearch = gvAlloc.HeaderRow.FindControl("txtGRM_DISPLAY")
            txtSearch.Text = str_GRM_DISPLAY

            txtSearch = gvAlloc.HeaderRow.FindControl("txtStatus")
            txtSearch.Text = str_Status

            'SetChk(Me.Page)
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub bindAcademic_Year()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
            Session("hashCheck_sms") = Nothing
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
    Sub setControl()
        ddlAcademicYear.Enabled = True

    End Sub





    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub gvAlloc_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable
        If Not Session("hashCheck_ids") Is Nothing Then
            hash = Session("hashCheck_ids")
        End If


        Dim chk As HtmlInputCheckBox
        Dim ID As String = String.Empty
        For Each rowItem As GridViewRow In gvAlloc.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkControl")), HtmlInputCheckBox)
            ID = gvAlloc.DataKeys(rowItem.RowIndex)("STU_ENQ_ID").ToString()
            If hash.ContainsValue(ID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next

    End Sub
    Protected Sub gvAlloc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAlloc.PageIndexChanging
        gvAlloc.PageIndex = e.NewPageIndex
        Dim chk As HtmlInputCheckBox
        Dim ID As String = String.Empty

        Dim hash As New Hashtable
        If Not Session("hashCheck_ids") Is Nothing Then
            hash = Session("hashCheck_ids")
        End If

        For Each rowItem As GridViewRow In gvAlloc.Rows
            ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
            chk = DirectCast(rowItem.FindControl("chkControl"), HtmlInputCheckBox)
            ID = DirectCast(rowItem.FindControl("lblSTU_ENQ_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(ID) = False Then
                    hash.Add(ID, ID)
                End If
            Else
                If hash.Contains(ID) = True Then
                    hash.Remove(ID)
                End If
            End If
        Next

        Session("hashCheck_ids") = hash
        gridbind()
    End Sub




    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    If list_add(chk.Value.ToString) = False Then
                        chk.Checked = True
                    End If
                Else

                    If list_exist(chk.Value.ToString) = True Then
                        chk.Checked = True
                    End If

                    list_remove(chk.Value.ToString)


                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal id As String) As Boolean
        If Session("hashCheck_ids").Contains(id) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal id As String) As Boolean
        If Session("hashCheck_ids").Contains(id) Then
            Return False
        Else
            Session("hashCheck_ids").Add(id)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal id As String)
        If Session("hashCheck_ids").Contains(id) Then
            Session("hashCheck_ids").Remove(id)
        End If
    End Sub


    Protected Sub btnPrint1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint1.Click, btnPrint2.Click
        If Page.IsValid = True Then

            Dim chk As HtmlInputCheckBox
            Dim ID As String = String.Empty

            Dim hash As New Hashtable

            If Not Session("hashCheck_ids") Is Nothing Then
                hash = Session("hashCheck_ids")
            End If


            For Each rowItem As GridViewRow In gvAlloc.Rows

                chk = DirectCast(rowItem.FindControl("chkControl"), HtmlInputCheckBox)

                ID = DirectCast(rowItem.FindControl("lblSTU_ENQ_ID"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(ID) = False Then
                        hash.Add(ID, ID)
                    End If
                Else
                    If hash.Contains(ID) = True Then
                        hash.Remove(ID)
                    End If
                End If



            Next




            
            Dim str_ids As String = String.Empty

            Dim hashloop As DictionaryEntry

            For Each hashloop In hash

                str_ids += hashloop.Value.ToString() + "|"
            Next


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim param As New Hashtable
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
            param.Add("@BSU_ID", Session("sbsuid"))
            param.Add("@STU_EQS_IDS", str_ids)
            param.Add("@ISENQUIRY", rbEnq.Checked)
            param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
            param.Add("UserName", Session("sUsr_name"))
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param

                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptINDEMNITY_letter.rpt")

            End With
            Session("rptClass") = rptClass
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()


        End If

    End Sub

    Protected Sub btnSearchID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck_sms") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck_sms") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchGRM_DISPLAY_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck_sms") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSTATUS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck_sms") = Nothing
        gridbind()
    End Sub

    Protected Sub rbStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbStud.CheckedChanged
        Session("hashCheck_sms") = Nothing
        gridbind()
    End Sub

    Protected Sub rbEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnq.CheckedChanged
        Session("hashCheck_sms") = Nothing
        gridbind()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
