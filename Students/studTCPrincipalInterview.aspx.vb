﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class Students_studTCPrincipalInterview
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100282") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfTPS_ID.Value = "0"
                    BindOutcome()
                    BindBSu()
                    BindCurriculum()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
                    If Session("sbsuid") <> "999998" And ddlClm.Items.Count = 1 Then
                        trBsu.Visible = False
                    Else
                       
                    End If
                    BindGrade()
                    BindSection()
                    BindReason()
                    BindTransferSchool()
                    trGrid.Visible = False

                   
                End If

                If ddlInterviewStatus.SelectedItem.Text = "COMPLETED" Then
                    trInterview.Visible = True
                Else
                    trInterview.Visible = False
                End If

                gvStud.Attributes.Add("bordercolor", "#1b80b6")
                hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
                BindOutcomeCount()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBSu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE ISNULL(BSU_bGEMSSCHOOL,0)=1 AND ISNULL(BSU_bSCHOOL,0)=1"

        If Session("sbsuid") <> "999998" Then
            str_query += " AND BSU_ID='" + Session("sbsuid") + "'"
        End If

        str_query += " ORDER BY BSU_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

   

    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT DISTINCT CLM_DESCR,CLM_ID FROM CURRICULUM_M INNER JOIN ACADEMICYEAR_D ON " _
                                & " ACD_CLM_ID=CLM_ID AND ACD_CURRENT=1 AND ACD_BSU_ID='" + ddlBsu.SelectedValue.ToString + "' ORDER BY CLM_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                              & " WHERE GRM_ACD_ID= " + ddlAcademicYear.SelectedValue.ToString _
                              & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub
    Sub BindSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        If ddlGrade.SelectedValue = "0" Then
            ddlSection.Items.Add(li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                    & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindOutcome()
        ddlOutcome.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TPO_ID,TPO_DESCR FROM TC_PRINCIPALINTERVIEW_OUTCOME_M ORDER BY TPO_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlOutcome.DataSource = ds
        ddlOutcome.DataTextField = "TPO_DESCR"
        ddlOutcome.DataValueField = "TPO_ID"
        ddlOutcome.DataBind()
        ddlOutcome.Items.Insert(0, li)

        ddlInterviewOutcome.DataSource = ds
        ddlInterviewOutcome.DataTextField = "TPO_DESCR"
        ddlInterviewOutcome.DataValueField = "TPO_ID"
        ddlInterviewOutcome.DataBind()
        li = New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlInterviewOutcome.Items.Insert(0, li)

    End Sub

    Sub BindReason()
        ddlReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TPR_ID,TPR_DESCR FROM TC_PRINCIPALINTERVIEW_REASONS_M ORDER BY TPR_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReason.DataSource = ds
        ddlReason.DataTextField = "TPR_DESCR"
        ddlReason.DataValueField = "TPR_ID"
        ddlReason.DataBind()
        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlReason.Items.Insert(0, li)
    End Sub

    Sub BindTransferSchool()
        Dim str_query As String = "select bsu_name,BSU_ID from (select BSU_ID+'|GEMS' BSU_ID,bsu_name from businessunit_m where isnull(BSU_bTC_TRANSFER,0)=1 "
        str_query += " union "
        str_query += "SELECT CONVERT(VARCHAR(100),BCC_ID)+'|NONGEMS' ,BCC_NAME FROM BSU_COMPETITORS ) P"
        str_query += " order by bsu_name"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrans_School.DataSource = ds
        ddlTrans_School.DataTextField = "bsu_name"
        ddlTrans_School.DataValueField = "BSU_ID"
        ddlTrans_School.DataBind()
        Dim li As New ListItem
        li.Text = "Others"
        li.Value = "0"
        ddlTrans_School.Items.Insert(0, li)
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME," _
                                  & " GRM_DISPLAY,SCT_DESCR FROM STUDENT_M INNER JOIN " _
                                  & " GRADE_BSU_M ON STU_GRM_ID=GRM_ID INNER JOIN SECTION_M ON STU_SCT_ID=SCT_ID " _
                                  & " WHERE STU_ACD_ID=" + hfACD_ID.Value _
                                  & " AND STU_CURRSTATUS='EN'"
        If ddlGrade.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If

        If ddlSection.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If ddlInterviewStatus.SelectedItem.Text = "COMPLETED" Then
            str_query += " AND STU_ID IN(SELECT TPS_STU_ID FROM TC_PRINCIPALINTERVIEW_STUDENT WHERE TPS_ACd_ID=" + hfACD_ID.Value + ")"
        Else
            str_query += " AND STU_ID NOT IN(SELECT TPS_STU_ID FROM TC_PRINCIPALINTERVIEW_STUDENT WHERE TPS_ACd_ID=" + hfACD_ID.Value + ")"
        End If

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStudentID.Text + "%'"
        End If

        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        str_query += "ORDER BY GRM_DISPLAY,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()

        If ddlInterviewStatus.SelectedItem.Text = "COMPLETED" Then
            gvStud.Columns(5).Visible = True
        Else
            gvStud.Columns(5).Visible = False
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim strGems As String = ""
        Dim strTfrSchool As String = ""
        Dim str As String()
        If ddlTrans_School.SelectedValue <> "0" Then
            str = ddlTrans_School.SelectedValue.Split("|")
            If str(1) = "GEMS" Then
                strGems = "'" + str(0) + "'"
                strTfrSchool = "NULL"
            Else
                strGems = "NULL"
                strTfrSchool = "'" + ddlTrans_School.SelectedItem.Text + "'"
            End If
        Else
            strGems = "NULL"
            strTfrSchool = "'" + txtOthers.Text + "'"
        End If

        Dim str_query As String = "exec saveTC_PRINCIPALINTERVIEW_STUDENT " _
                                  & " @TPS_ID =" + hfTPS_ID.Value + "," _
                                  & " @TPS_STU_ID=" + hfSTU_ID.Value + "," _
                                  & " @TPS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                  & " @TPS_INTERVIEW_DATE='" + txtInterviewDate.Text + "'," _
                                  & " @TPS_INTERVIEW_TIME='" + ddlInterviewTime.SelectedItem.Text + "'," _
                                  & " @TPS_TPO_ID=" + ddlInterviewOutcome.SelectedValue.ToString + "," _
                                  & " @TPS_TPR_ID=" + ddlReason.SelectedValue.ToString + "," _
                                  & " @TPS_bTFRGEMS='" + IIf(strGems = "NULL", "false", "true") + "'," _
                                  & " @TPS_GEMS_SCHOOL=" + strGems + "," _
                                  & " @TPS_TFRSCHOOL=" + strTfrSchool + "," _
                                  & " @TPS_FEEDBACK='" + txtExitFeedback.Text.Replace("'", "''") + "'," _
                                  & " @TPS_bRELEASETC='" + chkProceed.Checked.ToString + "'," _
                                  & " @TPS_USER='" + Session("susr_name") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub


    Sub BindHistoryDates(stu_id As String, gvHistory As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TPS_ID,CONVERT(VARCHAR(100),TPS_INTERVIEW_DATE,106) AS HISTORY_DATE FROM TC_PRINCIPALINTERVIEW_STUDENT WHERE TPS_STU_ID=" + stu_id _
                                  & " ORDER BY TPS_INTERVIEW_DATE"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvHistory.DataSource = ds
        gvHistory.DataBind()
    End Sub

    Sub BindHistoryDataPopup(tps_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME," _
                                  & " GRM_DISPLAY,SCT_DESCR,TPS_INTERVIEW_DATE,TPS_INTERVIEW_TIME,TPS_TPO_ID,TPS_TPR_ID," _
                                  & " TPS_bTFRGEMS,TPS_GEMS_SCHOOL,TPS_TFRSCHOOL,TPS_FEEDBACK,TPS_bRELEASETC,TPS_ACD_ID" _
                                  & " FROM STUDENT_M INNER JOIN " _
                                  & " GRADE_BSU_M ON STU_GRM_ID=GRM_ID INNER JOIN SECTION_M ON STU_SCT_ID=SCT_ID " _
                                  & " INNER JOIN TC_PRINCIPALINTERVIEW_STUDENT ON STU_ID=TPS_STU_ID" _
                                  & " WHERE TPS_ID=" + tps_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                With ds.Tables(0).Rows(0)
                    hfSTU_ID.Value = .Item("STU_ID")
                    lblExitStudent.Text = .Item("STU_NAME")
                    lblExitClass.Text = .Item("GRM_DISPLAY") + " " + .Item("SCT_DESCR")
                    txtInterviewDate.Text = Format(CDate(.Item("TPS_INTERVIEW_DATE")), "dd/MMM/yyyy")
                    If Not ddlInterviewTime.Items.FindByText(.Item("TPS_INTERVIEW_TIME")) Is Nothing Then
                        ddlInterviewTime.ClearSelection()
                        ddlInterviewTime.Items.FindByText(.Item("TPS_INTERVIEW_TIME")).Selected = True
                    End If

                    If .Item("TPS_bTFRGEMS").ToString.ToLower = "true" Then
                        ddlTrans_School.ClearSelection()
                        ddlTrans_School.Items.FindByValue(.Item("TPS_GEMS_SCHOOL") + "|GEMS").Selected = True
                    Else
                        If Not ddlTrans_School.Items.FindByText(.Item("TPS_TFRSCHOOL")) Is Nothing Then
                            ddlTrans_School.ClearSelection()
                            ddlTrans_School.Items.FindByText(.Item("TPS_TFRSCHOOL")).Selected = True
                        Else
                            txtOthers.Text = .Item("TPS_TFRSCHOOL")
                        End If
                    End If
                    txtExitFeedback.Text = .Item("TPS_FEEDBACK")

                    If Not ddlInterviewOutcome.Items.FindByValue(.Item("TPS_TPO_ID")) Is Nothing Then
                        ddlInterviewOutcome.ClearSelection()
                        ddlInterviewOutcome.Items.FindByValue(.Item("TPS_TPO_ID")).Selected = True
                    End If

                    If Not ddlReason.Items.FindByValue(.Item("TPS_TPR_ID")) Is Nothing Then
                        ddlReason.ClearSelection()
                        ddlReason.Items.FindByValue(.Item("TPS_TPR_ID")).Selected = True
                    End If

                    If .Item("TPS_bRELEASETC").ToString.ToLower = "true" Then
                        chkProceed.Checked = True
                    Else
                        chkProceed.Checked = False
                    End If

                    If .Item("TPS_ACD_ID") <> Session("CURRENT_ACD_ID") Then
                        btnExitSave.Visible = False
                    Else
                        btnExitSave.Visible = True
                    End If
                End With
            End If
        End If
    End Sub

    Sub ClearData()
        ddlInterviewOutcome.ClearSelection()
        ddlReason.ClearSelection()
        ddlInterviewTime.ClearSelection()
        ddlTrans_School.ClearSelection()

        txtInterviewDate.Text = ""
        txtExitFeedback.Text = ""
        txtOthers.Text = ""
        lblError.Text = ""
        lblErrorText.Text = ""

        btnExitSave.Visible = True
        chkProceed.Checked = False
        ShowCurrentTime()
    End Sub

    Sub BindOutcomeCount()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TPO_DESCR,COUNT(TPS_ID) as TPO_COUNT FROM TC_PRINCIPALINTERVIEW_OUTCOME_M LEFT OUTER JOIN " _
                                  & " TC_PRINCIPALINTERVIEW_STUDENT ON TPO_ID=TPS_TPO_ID AND TPS_aCD_ID=" + hfACD_ID.Value

        If ddlInterviewStatus.SelectedItem.Text = "COMPLETED" Then
            If txtInterviewStart.Text <> "" Then
                str_query += " AND TPS_INTERVIEW_DATE>='" + txtInterviewStart.Text + "'"
            End If

            If txtInterviewEnd.Text <> "" Then
                str_query += " AND TPS_INTERVIEW_DATE<='" + txtInterviewEnd.Text + "'"
            End If
        End If

        str_query += " GROUP BY TPO_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlCount.DataSource = ds
        dlCount.DataBind()
    End Sub

    Sub ShowCurrentTime()
        Dim i As Integer
        For i = 0 To ddlInterviewTime.Items.Count - 1
            If CInt(ddlInterviewTime.Items(i).Text.Replace(":", "")) >= (Now.Hour * 100 + Now.Minute - 15) And CInt(ddlInterviewTime.Items(i).Text.Replace(":", "")) < (Now.Hour * 100 + Now.Minute) Then
                ddlInterviewTime.ClearSelection()
                ddlInterviewTime.Items(i).Selected = True
                Exit For
            End If
        Next
    End Sub

#End Region

    Protected Sub ddlInterviewStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlInterviewStatus.SelectedIndexChanged
        If ddlInterviewStatus.SelectedItem.Text = "COMPLETED" Then
            trInterview.Visible = True
        Else
            trInterview.Visible = False
        End If
        mdlSchool.Hide()
        trGrid.Visible = False
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        GridBind()
        trGrid.Visible = True
        BindOutcome()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub lnkPrincipal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        Dim lblName As Label = TryCast(sender.findcontrol("lblName"), Label)
        Dim lblGrade As Label = TryCast(sender.findcontrol("lblGrade"), Label)
        Dim lblSection As Label = TryCast(sender.findcontrol("lblSection"), Label)
        Dim chkProceed As CheckBox = TryCast(sender.findcontrol("chkProceed"), CheckBox)
        hfTPS_ID.Value = "0"
        hfSTU_ID.Value = lblStuId.Text
        lblExitStudent.Text = lblName.Text
        lblExitClass.Text = lblGrade.Text + " " + lblSection.Text

        ClearData()
        mdlSchool.Show()
        ' chkProceed.Checked = False
    End Sub
    Protected Sub lnkHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErrorText.Text = ""
        Dim lblTPS As Label = TryCast(sender.findcontrol("lblTPS"), Label)
        hfTPS_ID.Value = lblTPS.Text
        BindHistoryDataPopup(lblTPS.Text)
        mdlSchool.Show()
    End Sub

    Protected Sub btnExitSave_Click(sender As Object, e As EventArgs) Handles btnExitSave.Click
        If txtInterviewDate.Text = "" Then
            lblErrorText.Text = "Please enter the interview date"
            mdlSchool.Show()
            Exit Sub
        End If
        If CDate(txtInterviewDate.Text) > Now.Date Then
            lblErrorText.Text = "Interview date cannot be a future date"
            mdlSchool.Show()
            Exit Sub
        End If
        If CDate(txtInterviewDate.Text) = Now.Date Then
            If (ddlInterviewTime.SelectedItem.Text.Replace(":", "")) > (Now.Hour * 100 + Now.Minute + 15) Then
                lblErrorText.Text = "Interview time cannot be a future time"
                mdlSchool.Show()
                Exit Sub
            End If
        End If
        If ddlReason.SelectedValue.ToString = "0" Then
            lblErrorText.Text = "Please select the reason for leaving"
            mdlSchool.Show()
            Exit Sub
        End If
        If ddlTrans_School.SelectedValue.ToString = "0" And txtOthers.Text = "" Then
            lblErrorText.Text = "Please select the transfer school"
            mdlSchool.Show()
            Exit Sub
        End If
        If ddlInterviewOutcome.SelectedValue.ToString = "0" Then
            lblErrorText.Text = "Please select the interview outcome"
            mdlSchool.Show()
            Exit Sub
        End If
        If txtExitFeedback.Text = "" Then
            lblErrorText.Text = "Please enter the feedback"
            mdlSchool.Show()
            Exit Sub
        End If
        SaveData()
        lblErrorText.Text = "Record Saved Successfully"
        btnExitSave.Visible = False
        mdlSchool.Show()
    End Sub

    Protected Sub gvStud_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStud_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If ddlInterviewStatus.SelectedItem.Text = "COMPLETED" Then
                Dim lblStuId As Label = e.Row.FindControl("lblStuId")
                Dim gvHistory As GridView = e.Row.FindControl("gvHistory")
                BindHistoryDates(lblStuId.Text, gvHistory)
                gvHistory.HeaderRow.Visible = False
                ' gvHistory.Attributes.Add("borderwidth", "0px")
            End If
        End If
    End Sub

    Protected Sub btnExitClose_Click(sender As Object, e As EventArgs) Handles btnExitClose.Click
        GridBind()
        BindOutcomeCount()
    End Sub

    Protected Sub ddlInterviewOutcome_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlInterviewOutcome.SelectedIndexChanged
        If ddlInterviewOutcome.SelectedIndex = 1 Or ddlInterviewOutcome.SelectedIndex = 2 Then
            chkProceed.Checked = False
            chkProceed.Enabled = False
        Else
            chkProceed.Enabled = True
        End If
        mdlSchool.Show()
    End Sub
End Class
