Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudStaffAuthorized_Grade_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059014") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                  

                    callYEAR_DESCRBind()


                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
       


    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
  
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim CurrentDatedType As String = String.Empty



            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_GROUP_NAME As String = String.Empty
            Dim str_filter_EMP_Name As String = String.Empty
            
            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            Dim ds As New DataSet

            str_Sql = "  SELECT AVG_ID,ACD_ID , GROUP_NAME ,EMP_NAME ,STU_COUNT FROM [ATT].[fn_GETATT_VGROUP_VIEW] ( '" & ACD_ID & "') WHERE AVG_ID<>'' "


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_GROUP_NAME As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
           

            If gvAuthorizedRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGROUP_NAME")
                str_GROUP_NAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GROUP_NAME = " AND GROUP_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GROUP_NAME = "  AND  NOT GROUP_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GROUP_NAME = " AND GROUP_NAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GROUP_NAME = " AND GROUP_NAME NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GROUP_NAME = " AND GROUP_NAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GROUP_NAME = " AND GROUP_NAME NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtEMP_Name")
                str_EMP_Name = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_EMP_Name = " AND EMP_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EMP_Name = "  AND  NOT EMP_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EMP_Name = " AND EMP_Name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EMP_Name = " AND EMP_Name NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EMP_Name = " AND EMP_Name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EMP_Name = " AND EMP_Name NOT LIKE '%" & txtSearch.Text & "'"
                End If





            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_GROUP_NAME & str_filter_EMP_Name & " ORDER BY GROUP_NAME")


            If ds.Tables(0).Rows.Count > 0 Then

                gvAuthorizedRecord.DataSource = ds.Tables(0)
                gvAuthorizedRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(4) = True

                gvAuthorizedRecord.DataSource = ds.Tables(0)
                Try
                    gvAuthorizedRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAuthorizedRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAuthorizedRecord.Rows(0).Cells.Clear()
                gvAuthorizedRecord.Rows(0).Cells.Add(New TableCell)
                gvAuthorizedRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAuthorizedRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAuthorizedRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGROUP_NAME")
            txtSearch.Text = str_GROUP_NAME
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtEMP_Name")
            txtSearch.Text = str_EMP_Name
          
           

            'Call callYEAR_DESCRBind()

            'Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)

            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
   

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGROUP_NAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
   

    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAuthorizedRecord.PageIndexChanging
        gvAuthorizedRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    
    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblSAD_ID As New Label
            Dim lblGroupName As New Label
            Dim url As String
            Dim viewid As String
            lblSAD_ID = TryCast(sender.FindControl("lblAVG_ID"), Label)
            lblGroupName = TryCast(sender.FindControl("lblGROUP_NAME"), Label)
            viewid = lblSAD_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            Session("ATT_GP_ACD_ID") = ddlAca_Year.SelectedValue
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\StudVATT_M_GROUP_EDIT.aspx?MainMnu_code={0}&datamode={1}&viewid={2}&gn={3}", ViewState("MainMnu_code"), ViewState("datamode"), viewid, Encr_decrData.Encrypt(lblGroupName.Text))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()

    End Sub


    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\StudVATT_M_GROUP_EDIT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

   
   
End Class
