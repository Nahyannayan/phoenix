<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studScreeningSubjects_View.aspx.vb" Inherits="Students_studScreeningSubjects_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>  Screening Subjects Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">

       

        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>

            <td align="center" colspan="8">
                <table id="Table1" runat="server" align="center" width="100%"  cellpadding="5"  cellspacing="0">
                    <tr>
                        <td colspan="3" align="center" >
                            <table id="Table2" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="center"  colspan="3"  ></td>
                                </tr>

                                <tr>
                                    <td colspan="2" align="left" >
                                        <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:GridView ID="gvSubjects" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />

                                            <Columns>


                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScbId" runat="server" Text='<%# Bind("SCB_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Subject">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSub" runat="server" CssClass="gridheader_text" Text="Subject"></asp:Label>
                                                      <br />
                                                        <asp:TextBox ID="txtSubject" runat="server" Width="40%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSubject_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSubject_Search_Click" />
                                                                       
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SCB_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>

                                            <SelectedRowStyle Wrap="False" />
                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />


                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7"
                                runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>

                </div>
          </div>
    </div>



</asp:Content>

