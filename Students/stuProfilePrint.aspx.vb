Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Partial Class Students_stuProfilePrint
    Inherits System.Web.UI.Page
    Dim enc As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenStudentId.Value = enc.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
            HiddenBsuId.Value = Session("sbsuid")
            BindBsu()
            BindData()
        End If
    End Sub
    Public Sub BindBsu()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "Select BSU_LOGO from BUSINESSUNIT_M where BSU_ID='" & HiddenBsuId.Value & "'"
        Image2.ImageUrl = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString.Replace("..", "~")
    End Sub
   
    Public Sub BindData()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select stu_firstname,sct_descr,grm_display,stm_descr,shf_descr,BSU_SHORTNAME,RLG_DESCR,CTY_NATIONALITY,EQP_FCOMPOBOX_EMIR,EQP_MCOMPOBOX_EMIR,EQP_GCOMPOBOX_EMIR,* from STUDENT_M " & _
                                  " LEFT JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID " & _
                                  " LEFT JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                                  " LEFT JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                                  " LEFT JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                                  " LEFT JOIN STUDENT_D ON STUDENT_D.STS_STU_ID= STUDENT_M.STU_SIBLING_ID " & _
                                  " LEFT JOIN HOUSE_M ON STUDENT_M.STU_HOUSE_ID=HOUSE_M.HOUSE_ID " & _
                                  " LEFT JOIN EMPLOYEE_M ON EMPLOYEE_M.EMP_ID= SECTION_M.SCT_EMP_ID " & _
                                  " LEFT JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID= STUDENT_M.STU_BSU_ID " & _
                                  " LEFT JOIN RELIGION_M ON RELIGION_M.RLG_ID=STUDENT_M.STU_RLG_ID " & _
                                  " LEFT JOIN COUNTRY_M ON COUNTRY_M.CTY_ID= STUDENT_M.STU_NATIONALITY " & _
                                  " LEFT JOIN ENQUIRY_PARENT_M ON ENQUIRY_PARENT_M.EQP_EQM_ENQID=STU_EQM_ENQID " & _
                                  " where STUDENT_M.stu_id='" & HiddenStudentId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ''Main Details
        lblstuname.Text = ds.Tables(0).Rows(0).Item("STU_FIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STU_LASTNAME").ToString()
        lblsen.Text = ds.Tables(0).Rows(0).Item("STU_NO").ToString()
        lblyear.Text = ds.Tables(0).Rows(0).Item("grm_display").ToString()
        lbldiv.Text = ds.Tables(0).Rows(0).Item("sct_descr").ToString()
        lblschool.Text = ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
        lblnationality.Text = ds.Tables(0).Rows(0).Item("CTY_NATIONALITY").ToString()
        lbldateofbirth.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_DOB"))
        lblreligion.Text = ds.Tables(0).Rows(0).Item("RLG_DESCR").ToString()
        lblplaceofbirth.Text = ds.Tables(0).Rows(0).Item("STU_POB").ToString()
        lbldateofjoin.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_DOJ"))
        lblemergencycontact.Text = ds.Tables(0).Rows(0).Item("STU_EMGCONTACT").ToString()

        ''Pasport Details

        lblpassportname.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNAME").ToString()
        lblpassportnumber.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNO").ToString()
        lblpassportissueplace.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTISSPLACE").ToString()
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PASPRTISSDATE")) Then
        Else
            lblpassportissuedate.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_PASPRTISSDATE"))
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PASPRTEXPDATE")) Then
        Else
            lblpassportexpirydate.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_PASPRTEXPDATE"))
        End If


        lblvisanumber.Text = ds.Tables(0).Rows(0).Item("STU_VISANO").ToString()
        lblvisaissueplace.Text = ds.Tables(0).Rows(0).Item("STU_VISAISSPLACE").ToString()
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_VISAISSDATE")) Then
        Else
            lblvisaissuedate.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_VISAISSDATE"))
        End If
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_VISAEXPDATE")) Then
        Else
            lblvisaexpirydate.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_VISAEXPDATE"))
        End If



        ''Parent Details
        ''Father Details
        lblfathername.Text = ds.Tables(0).Rows(0).Item("STS_FFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FLASTNAME").ToString()
        lblfathernationality.Text = Nationality(ds.Tables(0).Rows(0).Item("STS_FNATIONALITY").ToString())
        lblcompanypob.Text = ds.Tables(0).Rows(0).Item("STS_FCOMPOBOX").ToString()
        lblfatheremirate.Text = Emirates(ds.Tables(0).Rows(0).Item("EQP_FCOMPOBOX_EMIR").ToString())
        lblfatherphoneres.Text = ds.Tables(0).Rows(0).Item("STS_FRESPHONE").ToString()
        lblfatherofficephone.Text = ds.Tables(0).Rows(0).Item("STS_FOFFPHONE").ToString()
        lblfathermobile.Text = ds.Tables(0).Rows(0).Item("STS_FMOBILE").ToString()
        lblfatheremail.Text = ds.Tables(0).Rows(0).Item("STS_FEMAIL").ToString()
        lblfatherfax.Text = ds.Tables(0).Rows(0).Item("STS_FFAX").ToString()
        lblfatheroccupation.Text = ds.Tables(0).Rows(0).Item("STS_FOCC").ToString()
        If ds.Tables(0).Rows(0).Item("STS_F_COMP_ID").ToString() <> "" Then
            lblfathercompany.Text = Company(ds.Tables(0).Rows(0).Item("STS_F_COMP_ID").ToString())

        Else
            lblfathercompany.Text = ds.Tables(0).Rows(0).Item("STS_FCOMPANY").ToString()
        End If
       

        ''Mother Details ''Guardian Details
        If ds.Tables(0).Rows(0).Item("STS_MFIRSTNAME").ToString() = "" Then
            ''Show Guardian Information
            lblmgname.Text = ds.Tables(0).Rows(0).Item("STS_GFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GLASTNAME").ToString()
            lblmgnationality.Text = Nationality(ds.Tables(0).Rows(0).Item("STS_GNATIONALITY").ToString())
            lblmgcommpob.Text = ds.Tables(0).Rows(0).Item("STS_GCOMPOBOX").ToString()
            lblmgemirates.Text = Emirates(ds.Tables(0).Rows(0).Item("EQP_GCOMPOBOX_EMIR").ToString())
            lblmgphoneres.Text = ds.Tables(0).Rows(0).Item("STS_GRESPHONE").ToString()
            lblmgofficephone.Text = ds.Tables(0).Rows(0).Item("STS_GOFFPHONE").ToString()
            lblmgmobile.Text = ds.Tables(0).Rows(0).Item("STS_GMOBILE").ToString()
            lblmgemail.Text = ds.Tables(0).Rows(0).Item("STS_GEMAIL").ToString()
            lblmgfax.Text = ds.Tables(0).Rows(0).Item("STS_GFAX").ToString()
            lblmgoccupation.Text = ds.Tables(0).Rows(0).Item("STS_GOCC").ToString()
            If ds.Tables(0).Rows(0).Item("STS_G_COMP_ID").ToString() <> "" Then
                lblfathercompany.Text = Company(ds.Tables(0).Rows(0).Item("STS_G_COMP_ID").ToString())

            Else
                lblfathercompany.Text = ds.Tables(0).Rows(0).Item("STS_GCOMPANY").ToString()
            End If
        Else
            ''Show Mother Information
            lblmgname.Text = ds.Tables(0).Rows(0).Item("STS_MFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MLASTNAME").ToString()
            lblmgnationality.Text = Nationality(ds.Tables(0).Rows(0).Item("STS_MNATIONALITY").ToString())
            lblmgcommpob.Text = ds.Tables(0).Rows(0).Item("STS_MCOMPOBOX").ToString()
            lblmgemirates.Text = Emirates(ds.Tables(0).Rows(0).Item("EQP_MCOMPOBOX_EMIR").ToString())
            lblmgphoneres.Text = ds.Tables(0).Rows(0).Item("STS_MRESPHONE").ToString()
            lblmgofficephone.Text = ds.Tables(0).Rows(0).Item("STS_MOFFPHONE").ToString()
            lblmgmobile.Text = ds.Tables(0).Rows(0).Item("STS_MMOBILE").ToString()
            lblmgemail.Text = ds.Tables(0).Rows(0).Item("STS_MEMAIL").ToString()
            lblmgfax.Text = ds.Tables(0).Rows(0).Item("STS_MFAX").ToString()
            lblmgoccupation.Text = ds.Tables(0).Rows(0).Item("STS_MOCC").ToString()
            If ds.Tables(0).Rows(0).Item("STS_M_COMP_ID").ToString() <> "" Then
                lblfathercompany.Text = Company(ds.Tables(0).Rows(0).Item("STS_M_COMP_ID").ToString())

            Else
                lblfathercompany.Text = ds.Tables(0).Rows(0).Item("STS_MCOMPANY").ToString()
            End If
        End If

        ''Primary Contact
        If ds.Tables(0).Rows(0).Item("STU_PRIMARYCONTACT").ToString() = "F" Then
            lblprimaryaddress.Text = ds.Tables(0).Rows(0).Item("STS_FPRMADDR1").ToString()
            lblprimarycitystate.Text = ds.Tables(0).Rows(0).Item("STS_FPRMCITY").ToString()
            lblprimarycountry.Text = Country(ds.Tables(0).Rows(0).Item("STS_FPRMCOUNTRY").ToString())
        ElseIf ds.Tables(0).Rows(0).Item("STU_PRIMARYCONTACT").ToString() = "M" Then
            lblprimaryaddress.Text = ds.Tables(0).Rows(0).Item("STS_MPRMADDR1").ToString()
            lblprimarycitystate.Text = ds.Tables(0).Rows(0).Item("STS_MPRMCITY").ToString()
            lblprimarycountry.Text = Country(ds.Tables(0).Rows(0).Item("STS_MPRMCOUNTRY").ToString())
        Else
            lblprimaryaddress.Text = ds.Tables(0).Rows(0).Item("STS_GPRMADDR1").ToString()
            lblprimarycitystate.Text = ds.Tables(0).Rows(0).Item("STS_GPRMCITY").ToString()
            lblprimarycountry.Text = Country(ds.Tables(0).Rows(0).Item("STS_GPRMCOUNTRY").ToString())
        End If

        ''Previous School Details
        lblpevsname1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI").ToString()
        lblpevsname2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII").ToString()
        lblpevsname3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII").ToString()

        lblpevsclass1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_GRADE").ToString()
        lblpevsclass2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII_GRADE").ToString()
        lblpevsclass3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_GRADE").ToString()

        lblpevsprogram1.Text = Curriculum(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CLM").ToString())
        lblpevsprogram2.Text = Curriculum(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CLM").ToString())
        lblpevsprogram3.Text = Curriculum(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CLM").ToString())

        lblpevscitycountry1.Text = Country(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_COUNTRY").ToString())
        lblpevscitycountry2.Text = Country(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_COUNTRY").ToString())
        lblpevscitycountry3.Text = Country(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_COUNTRY").ToString())

        If ds.Tables(0).Rows(0).Item("STU_PREVSCHI_LASTATTDATE").ToString().Trim() <> "" Then
            lblpevstodate1.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_LASTATTDATE"))
        End If

        If ds.Tables(0).Rows(0).Item("STU_PREVSCHII_LASTATTDATE").ToString().Trim() <> "" Then
            lblpevstodate1.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_LASTATTDATE"))
        End If

        If ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_LASTATTDATE").ToString().Trim() <> "" Then
            lblpevstodate1.Text = DateFormat(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_LASTATTDATE"))
        End If

        ''Transport Details- Pick Up

        lblbusno.Text = "Pick Up : " & ds.Tables(0).Rows(0).Item("STU_PICKUP_BUSNO").ToString() & "<br> Drop Off : " & ds.Tables(0).Rows(0).Item("STU_DROPOFF_BUSNO").ToString()
       

        ''Medical Details History

        lblhealthcardno.Text = ds.Tables(0).Rows(0).Item("STU_HCNO").ToString()
        lblspecialmedication.Text = ds.Tables(0).Rows(0).Item("STU_SPMEDICATION").ToString()
        lblbloodgroup.Text = ds.Tables(0).Rows(0).Item("STU_BLOODGROUP").ToString()
        'lblsportsrestriction.Text = ds.Tables(0).Rows(0).Item("").ToString()
        lblhealthproblems.Text = ds.Tables(0).Rows(0).Item("STU_HEALTH").ToString() & " " & ds.Tables(0).Rows(0).Item("STU_PHYSICAL").ToString()

        ''Current Academic year details
        lblhouse.Text = ds.Tables(0).Rows(0).Item("HOUSE_DESCRIPTION").ToString()
        lblarabicname.Text = ds.Tables(0).Rows(0).Item("STU_FIRSTNAMEARABIC").ToString() & " " & ds.Tables(0).Rows(0).Item("STU_LASTNAMEARABIC").ToString()
        lblformtutor.Text = ds.Tables(0).Rows(0).Item("EMP_FNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("EMP_LNAME").ToString()
        lblmoeregno.Text = ds.Tables(0).Rows(0).Item("STU_BLUEID").ToString()

        '' Image Path
        If ds.Tables(0).Rows(0).Item("STU_PHOTOPATH").ToString() <> "" Then
            Image1.ImageUrl = ds.Tables(0).Rows(0).Item("STU_PHOTOPATH").ToString()
        Else
            Image1.ImageUrl = "~/Images/stuimage.gif"
        End If

        ''Sibling Details
        str_query = "select stu_id,STU_SIBLING_ID,STU_FIRSTNAME,STU_LASTNAME, " & _
                                  " (case STU_GENDER when 'F' then 'Female' else 'Male' end ) as relation, " & _
                                  " (select BSU_SHORTNAME from BUSINESSUNIT_M where stu_bsu_id=bsu_id) as school,STU_NO " & _
                                  " from student_m  where STU_SIBLING_ID=(select STU_SIBLING_ID from student_m where stu_id='" & HiddenStudentId.Value & "') and stu_id !='" & HiddenStudentId.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridSiblings.DataSource = ds
        GridSiblings.DataBind()

        ''Transport Details - Route and Stage 
        str_query = "SELECT B.TRP_DESCR AS ONWARDROUTE,C.TRP_DESCR AS RETURNROUTE,D.PNT_DESCRIPTION AS ONWARDSTAGE, " & _
                    " E.PNT_DESCRIPTION AS RETURNSTAGE FROM STUDENT_M AS A " & _
                    " INNER JOIN TRANSPORT.VV_TRIPS_M AS B ON A.STU_PICKUP_TRP_ID=B.TRP_ID " & _
                    " INNER JOIN TRANSPORT.VV_TRIPS_M AS C ON A.STU_DROPOFF_TRP_ID=C.TRP_ID " & _
                    " INNER JOIN TRANSPORT.VV_PICKUPOINTS_M AS D ON A.STU_PICKUP=D.PNT_ID " & _
                    " INNER JOIN TRANSPORT.VV_PICKUPOINTS_M AS E ON A.STU_PICKUP=E.PNT_ID " & _
                    " WHERE STU_ID='" & HiddenStudentId.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then

            lblroute.Text = "Onward Route : " & ds.Tables(0).Rows(0).Item("ONWARDROUTE").ToString() & "<br> Return Route : " & ds.Tables(0).Rows(0).Item("RETURNROUTE").ToString()
            lblstage.Text = "Onward Stage : " & ds.Tables(0).Rows(0).Item("ONWARDSTAGE").ToString() & "<br> Return Stage : " & ds.Tables(0).Rows(0).Item("RETURNSTAGE").ToString()

        End If

    End Sub
    Function DateFormat(ByVal fDate As DateTime) As String
        Dim returnval As String = ""
        Return Format(fDate, "dd/MMM/yyyy").Replace("01/Jan/1900", "")
    End Function
    Public Function Company(ByVal com_id As String) As String
        Dim returnval As String
        If com_id <> "" And com_id <> "0" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "Select COMP_NAME from COMP_LISTED_M where COMP_ID='" & com_id & "'"
            returnval = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        Else
            returnval = ""
        End If
        Return returnval
    End Function
    Public Function Country(ByVal Countryid As String) As String
        Dim returnval As String
        If Countryid <> "" And Countryid <> "--" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "Select CTY_DESCR from COUNTRY_M where CTY_ID='" & Countryid & "'"
            returnval = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        Else
            returnval = ""
        End If
        Return returnval
    End Function
    Public Function Nationality(ByVal Countryid As String) As String
        Dim returnval As String

        If Countryid <> "" And Countryid <> "--" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "Select CTY_NATIONALITY from COUNTRY_M where CTY_ID='" & Countryid & "'"
            returnval = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        Else
            returnval = ""
        End If
        Return returnval

    End Function
    Public Function Emirates(ByVal EMR_CODE As String) As String
        Dim returnval As String
        If EMR_CODE <> "" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "Select EMR_DESCR from EMIRATE_M where EMR_CODE='" & EMR_CODE & "'"
            returnval = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        Else
            returnval = ""
        End If
        Return returnval
    End Function
    Public Function Curriculum(ByVal clmid As String) As String
        Dim returnval As String
        If clmid <> "" And clmid <> "0" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "Select CLM_DESCR from CURRICULUM_M where CLM_ID ='" & clmid & "'"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        Else
            returnval = ""
        End If
        Return returnval

    End Function
End Class
