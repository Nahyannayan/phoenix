<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stud_offer_create.aspx.vb" Inherits="Students_stud_offer_create" title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Offer Letter Template"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="Table2" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr id="tr_Old" runat="server">
                        <td colspan="2">
                            <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                                cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <div align="left">
                                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                        </div>
                                        <div align="left">
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                                EnableViewState="False" ForeColor="" ValidationGroup="Offer"></asp:ValidationSummary>
                                        </div>

                                    </td>
                                </tr>
                                <tr valign="bottom">
                                    <td align="center" valign="middle">
                                        <span class="field-label">Fields Marked with (</span><span style="font-size: 8pt; color: red">*</span><span class="field-label">)
                are mandatory</span></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom">
                                        <table align="left" width="100%" style="border-collapse: collapse" cellpadding="5" cellspacing="0">
                                            <tr>
                                                <td class="title-bg" colspan="3">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Indemnity Form Setting"></asp:Literal></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Matter</span><span style="font-size: 7pt; color: #800000">*</span></td>
                                              
                                                <td align="left" valign="middle">
                                                    <table >
                                                        <tr>
                                                            <td rowspan="2"  valign="top">
                                                                <asp:TextBox ID="txtMatter" runat="server" Height="141px" TextMode="MultiLine"  SkinID="MultiText_Large"></asp:TextBox></td>
                                                            <td align="left" valign="top">
                                                                <div>
                                                                    <span class="field-label">Set Line Spacing</span><asp:RadioButtonList ID="rbMatter" GroupName="Mat" runat="server">
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">Insert Applicant Info or the selected format
                                                     <asp:DropDownList ID="ddlMatter" runat="server">
                                                     </asp:DropDownList>&nbsp;
                                                                                  <asp:Button ID="btnMatter" runat="server" CssClass="button" Text="Insert" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMatter"
                                                                    CssClass="error" Display="Dynamic" ErrorMessage="Matter cannot be left empty" ForeColor=""
                                                                    ValidationGroup="Offer">*</asp:RequiredFieldValidator></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Remarks</span></td>
                                              
                                                <td align="left" valign="middle">
                                                    <table >
                                                        <tr>
                                                            <td rowspan="2"  valign="top">

                                                                <asp:TextBox ID="txtRemark" runat="server"  SkinID="MultiText_Large" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                            <td align="left" valign="top">
                                                                <div>
                                                                    <span class="field-label">Set Line Spacing</span> <asp:RadioButtonList ID="rbRemark" runat="server" GroupName="Remark">
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><span class="field-label">Insert  the selected format</span> 
                         
                         <asp:DropDownList ID="ddlRemark" runat="server">
                         </asp:DropDownList>&nbsp;
                                        <asp:Button ID="btnRemark" runat="server" CssClass="button"
                                            Text="Insert" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td align="left" >Signature                       
                                                </td>
                                               
                                                <td align="left" valign="middle">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtSign" runat="server" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <div>Insert  the selected format</div>
                                                                <asp:DropDownList ID="ddlSign" runat="server">
                                                                </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnSign" runat="server" CssClass="button"
                                Text="Insert" />
                                                            </td>

                                                        </tr>

                                                    </table>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">&nbsp;Acknowledgement</td>
                                                
                                                <td align="left" valign="middle">
                                                    <table >
                                                        <tr>
                                                            <td rowspan="2"  valign="top">

                                                                <asp:TextBox ID="txtAck" runat="server"  TextMode="MultiLine"  SkinID="MultiText_Large"></asp:TextBox>
                                                            </td>
                                                            <td align="left" valign="top">
                                                                <div><span class="field-label">Set Line Spacing</span></div>
                                                                <asp:RadioButtonList ID="rbParent" GroupName="Ack" runat="server">
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <div><span class="field-label">Insert  the selected format</span> </div>
                                                                <asp:DropDownList ID="ddlParent" runat="server">
                                                                </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnParent" runat="server" CssClass="button"
                                Text="Insert" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td  valign="bottom"></td>
                                </tr>
                                <tr>
                                    <td  valign="bottom" align="center">
                                        <asp:Button ID="btnSave"
                                            runat="server" CssClass="button" Text="Save" ValidationGroup="Offer" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                       
                        <td  width="15%" valign="bottom" align="left">
                            <span class="field-label">Content Type</span> 
                            </td>
                        <td>
                            <asp:RadioButton ID="rbOffer" runat="server" CssClass="field-label"
                                Text="Offer letter" GroupName="offer" AutoPostBack="true" />
                            <asp:RadioButton ID="rbTerms" runat="server" CssClass="field-label"
                                Text="Terms and Condition" GroupName="offer" AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr>

                        <td width="15%"><span class="field-label">Template</span>
                        </td>
                        <td>
                            <telerik:RadEditor ID="txtOfferText" runat="server" EditModes="All" Height="700%" Width="100%"
                                > 
                            </telerik:RadEditor>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"  valign="bottom" align="center" style="padding-top:50px">
                            <asp:Button ID="btnSave2" runat="server" CssClass="button" Text="Save" ValidationGroup="Offer" />
                        </td>
                    </tr>


                </table>
               <%--Height="750px" Width="750px" --%>
            </div>
        </div>
    </div>

</asp:Content>

