<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnq_Setting_View.aspx.vb" Inherits="Students_stuEnquiry_Setting_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Online Enquiry Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="25%"><span class="field-label">Select Academic year</span></td>

                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server"
                                AutoPostBack="True" CssClass="listbox" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <table id="tbl_test" runat="server" align="center" Width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="left" colspan="4" valign="top">
                                        <asp:GridView ID="gvOnlineEnq" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Curriculum ">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       
                                                                        <asp:Label ID="lblGRADE_H" runat="server" Text="GRADE" CssClass="gridheader_text" __designer:wfdid="w51"></asp:Label>
                                                                        <br />
                                                                        <asp:TextBox ID="txtGRM_DESCR" runat="server" Width="75%" __designer:wfdid="w7"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchGRD" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w8" OnClick="btnSearchGRD_Click"></asp:ImageButton>
                                                                    
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRM_DESCR" runat="server" Text='<%# Bind("GRM_DESCR") %>' __designer:wfdid="w4"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <EditItemTemplate>
                                                       
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                              <asp:Label ID="lblEQO_DESCH" runat="server" Text="Description" CssClass="gridheader_text" __designer:wfdid="w19"></asp:Label>
                                                                <br />
                                                              <asp:TextBox ID="txtEQO_DESC" runat="server" Width="75%" __designer:wfdid="w38"></asp:TextBox>
                                                              <asp:ImageButton ID="btnSearchEQO_DESC" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w39" OnClick="btnSearchEQO_DESC_Click"></asp:ImageButton>
                                                        
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQO_DESC" runat="server" Text='<%# Bind("EQO_DESC") %>' __designer:wfdid="w17"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                     <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                             <asp:Label ID="lblGradeH" runat="server" Text="From Date" CssClass="gridheader_text" __designer:wfdid="w42"></asp:Label>
                                                             <br />               
                                                             <asp:TextBox ID="txtFromDT" runat="server" Width="75%" __designer:wfdid="w43"></asp:TextBox>
                                                             <asp:ImageButton ID="btnSearchFromDT" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w44"></asp:ImageButton>
                                                         
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <asp:Label ID="lblFromDTH" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                     <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                         <asp:Label ID="lblToDateH" runat="server" Text="To Date" CssClass="gridheader_text" __designer:wfdid="w47"></asp:Label>
                                                                <br />
                                                         <asp:TextBox ID="txtToDT" runat="server" Width="75%" __designer:wfdid="w48"></asp:TextBox>
                                                         <asp:ImageButton ID="btnSearchToDT" OnClick="btnSearchToDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w49"></asp:ImageButton>
                                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDTH" runat="server" Text='<%# Bind("ToDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w45"></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EQO_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQS_ID" runat="server" Text='<%# Bind("EQS_ID") %>' __designer:wfdid="w54"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

