<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="STUBSU_APPLIC_EDIT.aspx.vb" Inherits="Students_StudEnq_Setting_Ack_edit" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 <script language="javascript">
 function Subj()
   {
     var ctrl=document.getElementById('<%= txtSub.clientID %>');
    
     var saveText = ctrl.value;                    
     ctrl.focus();
     var range = document.selection.createRange(); 
      var specialchar = String.fromCharCode(1);
    
     range.text = specialchar;
     var pos = ctrl.value.indexOf(specialchar);
         ctrl.value = saveText;
     range = ctrl.createTextRange();
      range.move('character', pos);
         range.select();
        var e = document.getElementById('<%= ddlSub.clientID %>');
          range.text= e.options[e.selectedIndex].value; 
  
     document.getElementById('<%= txtSub.clientID %>').focus();
     window.event.returnValue=false;  
   }
   function Matter()
   {
     var ctrl=document.getElementById('<%= txtMatter.clientID %>');
    
     var saveText = ctrl.value;                    
     ctrl.focus();
     var range = document.selection.createRange(); 
      var specialchar = String.fromCharCode(1);
    
     range.text = specialchar;
     var pos = ctrl.value.indexOf(specialchar);
         ctrl.value = saveText;
     range = ctrl.createTextRange();
      range.move('character', pos);
         range.select();
        var e = document.getElementById('<%= ddlMatter.clientID %>');
          range.text= e.options[e.selectedIndex].value; 
  
     document.getElementById('<%= txtMatter.clientID %>').focus();
     window.event.returnValue=false;  
   }
 function Remark()
   {
     var ctrl=document.getElementById('<%= txtRemark.clientID %>');
    
     var saveText = ctrl.value;                    
     ctrl.focus();
     var range = document.selection.createRange(); 
      var specialchar = String.fromCharCode(1);
    
     range.text = specialchar;
     var pos = ctrl.value.indexOf(specialchar);
         ctrl.value = saveText;
     range = ctrl.createTextRange();
      range.move('character', pos);
         range.select();
        var e = document.getElementById('<%= ddlRemark.clientID %>');
          range.text= e.options[e.selectedIndex].value; 
  
     document.getElementById('<%= txtRemark.clientID %>').focus();
     window.event.returnValue=false;  
   }
    function Signature()
   {
     var ctrl=document.getElementById('<%= txtSign.clientID %>');
    
     var saveText = ctrl.value;                    
     ctrl.focus();
     var range = document.selection.createRange(); 
      var specialchar = String.fromCharCode(1);
    
     range.text = specialchar;
     var pos = ctrl.value.indexOf(specialchar);
         ctrl.value = saveText;
     range = ctrl.createTextRange();
      range.move('character', pos);
         range.select();
        var e = document.getElementById('<%= ddlSign.clientID %>');
          range.text= e.options[e.selectedIndex].value; 
  
     document.getElementById('<%= txtSign.clientID %>').focus();
     window.event.returnValue=false;  
   }
    function Ack_parent()
   {
     var ctrl=document.getElementById('<%= txtAck.clientID %>');
    
     var saveText = ctrl.value;                    
     ctrl.focus();
     var range = document.selection.createRange(); 
      var specialchar = String.fromCharCode(1);
    
     range.text = specialchar;
     var pos = ctrl.value.indexOf(specialchar);
         ctrl.value = saveText;
     range = ctrl.createTextRange();
      range.move('character', pos);
         range.select();
        var e = document.getElementById('<%= ddlParent.clientID %>');
          range.text= e.options[e.selectedIndex].value; 
  
     document.getElementById('<%= txtAck.clientID %>').focus();
     window.event.returnValue=false;  
   }
	</script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Offer Letter Setting"></asp:Literal>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ForeColor="" ValidationGroup="Offer"></asp:ValidationSummary>
                                <span style="font-size: 7pt; color: #800000"></span>
                            </div>
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="center" valign="middle">Fields Marked with (<span style="font-size: 7pt; color: red">*</span>)are mandatory 
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  valign="bottom">
                            <table align="left" width="100%" cellpadding="5" cellspacing="0">
                               
                                <tr class="border">
                                    <td align="left" ><span class="field-label" >Application decision</span> </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlOfferLetter" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" ><span class="field-label" >Set Default Offer Letter</span> </td>

                                    <td align="left">
                                        <asp:CheckBox ID="chkYes" runat="server" Text="YES" CssClass="field-label"></asp:CheckBox></td>
                                </tr>
                                <tr class="border">
                                    <td align="left" ><span class="field-label" >Subject</span><span style="font-size: 7pt; color: red">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSub" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSub"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Subject Required" ForeColor=""
                                            ValidationGroup="Offer">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" >
                                        <div><span class="field-label">Insert  the selected format</span></div>
                                        </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSub" runat="server">
                                        </asp:DropDownList>
                            <asp:Button ID="btnSub" runat="server" CssClass="button"
                                Text="Insert" />
                                    </td>
                                    <%--<td align="left" colspan="3">

                                        <table width="100%">
                                            <tr>
                                                

                                            </tr>

                                        </table>
                                    </td>--%>
                                </tr>
                                <tr class="border">
                                    <td align="left" ><span class="field-label" >Matter</span> <span style="font-size: 7pt; color: red">*</span></td>

                                    <td align="left" colspan="3" valign="middle">
                                        <table width="100%">
                                            <tr>
                                                <td rowspan="2" valign="top" width="31%">
                                                    <asp:TextBox ID="txtMatter" runat="server" TextMode="MultiLine" Height="200px"></asp:TextBox></td>
                                                <td align="left" valign="top">
                                                    <div>
                                                        <span class="field-label" >Set Line Spacing</span> <asp:RadioButtonList ID="rbMatter" GroupName="Mat" runat="server" CssClass="field-label" Font-Bold="true" >
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" ><span class="field-label" >Insert Applicant Info or the <br />selected format</span> </td> <td>
                            <asp:DropDownList ID="ddlMatter" runat="server">
                            </asp:DropDownList>
                                        <asp:Button ID="btnMatter" runat="server" CssClass="button" Text="Insert" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMatter"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Matter cannot be left empty" ForeColor=""
                                            ValidationGroup="Offer">*</asp:RequiredFieldValidator></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="border">
                                    <td align="left" ><span class="field-label" >Remarks</span> </td>

                                    <td align="left" colspan="3" valign="middle">
                                        <table width="100%">
                                            <tr>
                                                <td rowspan="2" valign="top" width="31%">

                                                    <asp:TextBox ID="txtRemark" runat="server" Height="200px" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top">
                                                    <div>
                                                        <span class="field-label" >Set Line Spacing</span> <asp:RadioButtonList ID="rbRemark" runat="server" GroupName="Remark" CssClass="field-label" Font-Bold="true">
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><span class="field-label" >Insert  the selected format</span> </td> <td>
                         
                         <asp:DropDownList ID="ddlRemark" runat="server">
                         </asp:DropDownList>
                                        <asp:Button ID="btnRemark" runat="server" CssClass="button"
                                            Text="Insert" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="border">
                                    <td align="left" ><span class="field-label">Signature</span></td>
                                    <td>
                                        <asp:TextBox ID="txtSign" runat="server" TextMode="MultiLine"  ></asp:TextBox>
                                    </td>
                                    <td >
                                        <div><span class="field-label">Insert  the selected format</span> </div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSign" runat="server">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnSign" runat="server" CssClass="button"
                                            Text="Insert" />
                                    </td>
                                    <%--<td align="left" colspan="3" valign="middle">
                                        <table>
                                            <tr>
                                                

                                            </tr>

                                        </table>

                                    </td>--%>
                                </tr>
                                <tr class="border">
                                    <td align="left" ><span class="field-label" >Parent Acknowledgement</span> </td>

                                    <td align="left" colspan="3" valign="middle">
                                        <table width="100%">
                                            <tr>
                                                <td rowspan="2" valign="top" width="31%">

                                                    <asp:TextBox ID="txtAck" runat="server" TextMode="MultiLine" Height="200px"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top" >
                                                    <div><span class="field-label" >Set Line Spacing</span> </div>
                                                    <asp:RadioButtonList ID="rbParent" GroupName="Ack" runat="server" CssClass="field-label" Font-Bold="true">
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" >
                                                    <div><span class="field-label" >Insert  the selected format</span> </div> </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlParent" runat="server">
                                                    </asp:DropDownList>
                            <asp:Button ID="btnParent" runat="server" CssClass="button"
                                Text="Insert" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom"></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" OnClick="btnEdit_Click" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" ValidationGroup="Offer" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click" Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" OnClick="btnDelete_Click" Text="Delete" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

