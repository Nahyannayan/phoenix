<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studSection_M_View.aspx.vb" Inherits="Students_studSection_M_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Section Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>




                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr >
                                    <td align="left" colspan="4">
                                        </td>
                                </tr>

                                <tr>
                                    <td align="left" Width="20%">
                                        <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year" CssClass="field-label"></asp:Label></td>
                                
                                    <td align="left" Width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                     <td align="left" colspan="2" Width="50%">
                                        </td>

                                </tr>

                                <tr>
                                    <td colspan="4" align="center" >
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvStudSection" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSct_id" runat="server" Text='<%# Bind("sct_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>



                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrmId" runat="server" Text='<%# Bind("sct_grm_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                   Grade<br />
                                                                                <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" 
                                                                                    OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged" >
                                                                                </asp:DropDownList>
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Shift">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                                        <asp:Label ID="lblShiftID" runat="server" Text='<%# Bind("SHF_ID") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Shift<br />
                                                                                <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" 
                                                                                    OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" >
                                                                                </asp:DropDownList>
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Stream">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                                       <asp:Label ID="lblStreamID" runat="server" Text='<%# Bind("STM_ID") %>' Visible="false" ></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Stream<br />
                                                                                <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" 
                                                                                    OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" >
                                                                                </asp:DropDownList>
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Section">
                                                                <HeaderTemplate>
                                                                    Section<br />
                                                                                <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" 
                                                                                    OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged" >
                                                                                </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Max Strength">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMaxStrength" runat="server" Text='<%# Bind("sct_maxstrength") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="SCT_GRD_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgrdid" runat="server" Text='<%# Bind("sct_grd_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                        </Columns>
                                                        <HeaderStyle  />
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                                                        <EmptyDataRowStyle  />
                                                        <EditRowStyle  />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

