Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studReport_Setting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" '"Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050103") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    BSU_YEAR()
                    GRIDBIND()
                    disable_control()
               
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub

    Sub BSU_YEAR()
        Dim CLM_ID As String = Session("CLM")
        Dim dsYear As New DataSet
        dsYear = AccessStudentClass.GetACD_Year_Enquiry(Session("sBsuid"), CLM_ID)
        ddlAcdYear.Items.Clear()
        ddlAcdYear.DataSource = dsYear.Tables(0)
        ddlAcdYear.DataTextField = "ACY_DESCR"
        ddlAcdYear.DataValueField = "ACD_ID"
        ddlAcdYear.DataBind()
    End Sub
    Sub gridbind()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim BSU_ID As String = Session("sBsuid")
        Dim gvDataset As DataSet = AccessStudentClass.GetAtt_PARAM_DISPLAY(ACD_ID, BSU_ID)

        If gvDataset.Tables(0).Rows.Count > 0 Then
            gvInfo.DataSource = gvDataset.Tables(0)
            gvInfo.DataBind()
            If gvInfo.Rows.Count > 0 Then
                For i As Integer = 0 To gvInfo.Rows.Count - 1
                    Dim row As GridViewRow = gvInfo.Rows(i)
                    Dim lblDESCR As Label = DirectCast(row.FindControl("lblAPD_PARAM_DESCR"), Label)
                    Dim txtARP_DISP As TextBox = DirectCast(row.FindControl("txtARP_DISP"), TextBox)

                    If lblDESCR.Text = "PRESENT2" Then
                        lblDESCR.Text = "PRESENT SESSION 2"
                        txtARP_DISP.MaxLength = 1
                    ElseIf lblDESCR.Text = "PRESENT" Then
                        lblDESCR.Text = "PRESENT SESSION 1"
                        txtARP_DISP.MaxLength = 1
                    ElseIf lblDESCR.Text = "SESSION1" Or lblDESCR.Text = "SESSION2" Then
                        txtARP_DISP.MaxLength = 2
                    Else
                        txtARP_DISP.MaxLength = 1
                    End If
                Next
            End If


        End If

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "add" Then

                ViewState("datamode") = "view"
              
                ddlAcdYear.SelectedIndex = 0
                disable_control()
                gridbind()

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
   
    Sub disable_control()
        ddlAcdYear.Enabled = True
      
        gvInfo.Enabled = False
      
    End Sub
    Sub enable_control()
        ddlAcdYear.Enabled = False

        gvInfo.Enabled = True

    End Sub
   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"

            Call disable_control()
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim ARP_ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim ARP_BSU_ID As String = Session("sBsuid")


        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer

                  
                    status = AccessStudentClass.DeleteATTENDANCE_REPORT_PARAM(ARP_ACD_ID, ARP_BSU_ID, transaction)

                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    For i As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(i)
                        Dim lblAPD_ID As String = DirectCast(row.FindControl("lblAPD_ID"), Label).Text
                        Dim txtARP_DISP As String = DirectCast(row.FindControl("txtARP_DISP"), TextBox).Text
                        Dim txtARP_DESCR As String = DirectCast(row.FindControl("txtARP_DESCR"), TextBox).Text

                     
                        status = AccessStudentClass.SaveATTENDANCE_REPORT_PARAM(ARP_ACD_ID, ARP_BSU_ID, lblAPD_ID, txtARP_DESCR, txtARP_DISP, transaction)
                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        End If
                      
                    Next

                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
    
        gridbind()
        enable_control()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

   
End Class
