<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBsu_Service_M.aspx.vb" Inherits="Students_studBsu_Service_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
    
          
      </script>


      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>  <asp:Literal id="ltLabel" runat="server" Text="Service Setup"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    
    <table id="tbl_AddGroup" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0" >
        <tr>
            <td align="left">
                
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                           ></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                </span>
            </td>
        </tr>
        <tr>
            <td align="center"  valign="middle" >
                Fields Marked with(<span class="text-danger">*</span>) are mandatory</td>
        </tr>
        <tr>
            <td   valign="top">
                <table align="center" width="100%" cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="subheader_img" colspan="4">
                            
                                

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label">Service Name</span> <span class="text-danger">*</span></td>
                        
                        <td align="left">
                            <asp:TextBox id="txtService" runat="server"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator id="rfvReligion" runat="server" ControlToValidate="txtService"
                                CssClass="error" Display="Dynamic" ErrorMessage="Please select Service Name."
                                 ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
               
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" /><asp:Button id="btnEdit" runat="server" CausesValidation="False"
                        CssClass="button" onclick="btnEdit_Click" Text="Edit" /><asp:Button id="btnSave"
                            runat="server" CssClass="button" onclick="btnSave_Click" Text="Save" ValidationGroup="groupM1" /><asp:Button
                                id="btnCancel" runat="server" CausesValidation="False" CssClass="button" onclick="btnCancel_Click"
                                Text="Cancel" />
            </td>
        </tr>
    </table>
    <asp:HiddenField id="hfReligion" runat="server">
    </asp:HiddenField>

                 </div>
        </div>
    </div>

</asp:Content>

