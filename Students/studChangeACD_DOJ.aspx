<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studChangeACD_DOJ.aspx.vb" Inherits="Students_studChangeACD_DOJ" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudChange.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }


        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "../Accounts/calendar.aspx?nofuture=yes";
            if (val == 0) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }


            if (val == 1) {
                document.getElementById('<%=txtDoj.ClientID %>').value = result;
            }




            return false;
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            CHANGE ACADEMIC YEAR AND DATE OF JOIN
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblStud1" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" Style="vertical-align: middle" Height="35px" Width="558px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tblStud" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" Width="100px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Shift</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSHF_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Stream</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select Section</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNo" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtStuNo">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSAME" runat="server" BehaviorID="AutoCompleteEx2"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNAME" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtNAME">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" CausesValidation="False" />
                                    </td>

                                </tr>




                                <tr>
                                    <td colspan="4">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" width="20%">
                                                    <asp:CheckBox runat="server" ID="chkAcademicYear" Text="Change AcademicYear" AutoPostBack="True" Enabled="False"></asp:CheckBox>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:CheckBox runat="server" ID="chkJoin" Text="Update Join Details"></asp:CheckBox></td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%">
                                                    <asp:DropDownList ID="ddlCAcademicYear" runat="server" AutoPostBack="True" Enabled="False">
                                                    </asp:DropDownList>

                                                </td>
                                                <td align="left" width="30%">&nbsp;<asp:TextBox ID="txtDoj" runat="server" />
                                                    <asp:ImageButton ID="imgDOJ" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Button ID="btnUpdate2" runat="server" Text="Update" CssClass="button" ValidationGroup="groupM1" TabIndex="4" /></td>
                                </tr>

                                <tr>
                                    <td align="center" class="matters" colspan="4" valign="top">
                                        <asp:GridView ID="gvStudChange" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="gridstyle" EmptyDataText="No Records Found"
                                            HeaderStyle-Height="30" PageSize="20" Width="100%" AllowSorting="True">
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Select </br>
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldoj" runat="server" Text='<%# Bind("STU_DOJ") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STP_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStpId" runat="server" Text='<%# Bind("STP_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SL.No" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text="<%# getSerialNo() %>"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStHeader" runat="server" Text="Stud. No"></asp:Label></br>
                                                                                        <asp:TextBox ID="txtStuNoSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStuNo_Search" OnClick="btnStuNo_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label>
                                                        </br>
                                                         <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" OnClick="btnStudName_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsctId" runat="server" Text='<%# Bind("STP_SCT_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStm_descr" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STM_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTM_ID" runat="server" Text='<%# Bind("STM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Academic Year">
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%# Bind("ACY_DESCR") %>' ID="TextBox2"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# Bind("ACY_DESCR") %>' ID="Label2"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date of Join">
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%# Bind("STU_DOJ") %>' ID="TextBox1"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# Bind("STU_DOJ") %>' ID="Label1"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Button ID="btnChange" runat="server" Text="Update" CssClass="button" ValidationGroup="groupM1" TabIndex="4" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>



                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server"
                    type="hidden" value="=" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSCT_ID" runat="server" />
                <asp:HiddenField ID="hfSTM_ID" runat="server" />


                <ajaxToolkit:CalendarExtender ID="calDoJ" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDOJ"
                    TargetControlID="txtDOJ">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

