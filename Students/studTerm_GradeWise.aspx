<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTerm_GradeWise.aspx.vb" Inherits="Students_studTerm_GradeWise" Title=" " %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Term Wise Grades"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" width="100%">

                   <%-- <tr>
                        <td align="left">
                            <table id="Table3" width="100%">
                            </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>

                        <td align="center">
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left" width="100%">

                                        <asp:GridView ID="gvTerms" runat="server" CssClass="table table-bordered table-row"
                                            AutoGenerateColumns="False"
                                            PageSize="20" AllowPaging="True" OnPageIndexChanging="gvTerms_PageIndexChanging" OnRowCommand="gvTerms_RowCommand" OnRowDataBound="gvTerms_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="-">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTermId" runat="server" Text='<%# Bind("TRM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="10%">
                                                    <AlternatingItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("TRM_ID") %>', 'alt');">
                                                            <img id="img1" alt="Click to show/hide " src="../Images/expand_button_white_alt.jpg" border="0" />
                                                        </a>
                                                    </AlternatingItemTemplate>
                                                    <ItemTemplate>
                                                        <a href="javascript:switchViews('div<%# Eval("TRM_ID") %>', 'one');">
                                                            <img id='imgdiv<%# Eval("TRM_ID") %>' alt="Click to show/hide " src="../Images/expand_button_white.jpg" border="0" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        Term&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlgvTerm" runat="server" CssClass="listbox" AutoPostBack="True" __designer:wfdid="w88" OnSelectedIndexChanged="ddlgvTerm_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTerm" runat="server" Text='<%# Bind("TRM_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div style="display: none; position: relative;" id='div<%# Eval("TRM_ID") %>' width="100%">
                                                            <asp:GridView ID="gvGrade" runat="server" Width="100%" AutoGenerateColumns="False" 
                                                                CssClass="table table-bordered table-row" OnRowDataBound="gvGrade_RowDataBound" __designer:wfdid="w101" EmptyDataText="No subjets for this grade.">
                                                                <Columns>
                                                                    <asp:BoundField HtmlEncode="False" DataField="GRD_ID" Visible="False" HeaderText="GrdId">
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField HtmlEncode="False" DataField="GRM_DISPLAY" HeaderText="Grade">
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="StartDt">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtStartDt" runat="server" Text='<%# Bind("TRM_STR_DT") %>'></asp:TextBox>
                                                                            <ajaxToolkit:CalendarExtender ID="Cal1" runat="server" TargetControlID="txtStartDt" Format="dd-MMM-yyyy" PopupPosition="TopLeft" ></ajaxToolkit:CalendarExtender>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="EndDt">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtendDt" runat="server" Text='<%# Bind("TRM_END_DT") %>'></asp:TextBox>
                                                                            <ajaxToolkit:CalendarExtender ID="Cal2" runat="server" TargetControlID="txtendDt" Format="dd-MMM-yyyy" PopupPosition="TopLeft"></ajaxToolkit:CalendarExtender>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="TotalDays">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtHrs" runat="server" Text='<%# Bind("TRM_TOT_DAYS") %>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                </Columns>

                                                                <RowStyle CssClass="griditem"></RowStyle>

                                                                <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                                            </asp:GridView>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle BackColor="Aqua" />
                                            <PagerStyle HorizontalAlign="Left" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                                            Text="Save" ValidationGroup="MAINERROR" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" OnClick="btnDelete_Click"
                                            Text="Delete" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnCancel_Click" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                </table>

<%--                <script type="text/javascript">
                    cssdropdown.startchrome("Div1")
                    cssdropdown.startchrome("Div2")
                </script>--%>
            </div>
        </div>
    </div>
</asp:Content>

