﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuStaffStayReasons.aspx.vb" Inherits="Students_stuStaffStayReasons" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Stay Back Reason Setting
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkadd" runat="server" Text="Add New"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="rdOptions" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="field-label">
                                <asp:ListItem Text="Stay Back reasons" Value="0" Selected="True" ></asp:ListItem>
                                <asp:ListItem Text="Staff Lunch Menu" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="tr1" runat="server">
                        <td>
                            <span class="field-label">Stay Back Reasons</span>
                        </td>
                    </tr>
                    <tr id="tr2" runat="server" visible="false">
                        <td>
                            <span class="field-label">Staff Lunch Menu</span>
                        </td>
                    </tr>

                    <tr id="trReasons" runat="server">
                        <td>

                            <asp:GridView ID="gvReason" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" BorderStyle="None">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblLRID" runat="server" Text='<%# Bind("SBR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReason" runat="server" Text='<%# Bind("SBR_REASON") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />

                            </asp:GridView>

                        </td>
                    </tr>
                    <tr id="trMenu" runat="server" visible="false">
                        <td>

                            <asp:GridView ID="gvMenu" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" BorderStyle="None">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSLMID" runat="server" Text='<%# Bind("SLM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Item">
                                        <ItemTemplate>
                                            <asp:Label ID="lblItem" runat="server" Text='<%# Bind("SLM_ITEM") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />

                            </asp:GridView>

                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button" />
                        </td>
                    </tr>

                </table>
                <div id="divReason" runat="server" class="darkPanlAlumini" visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <asp:Button ID="btClose" type="button" runat="server"
                            Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X"></asp:Button>
                        <div class="holderInner">
                            <div align="center">
                                <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <br>
                            <br>
                            <table align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lbl" runat="server" Text="Reason" CssClass="field-label"></asp:Label>
                                    </td>
                                 
                                    <td align="left">
                                        <asp:TextBox ID="txtReason" runat="server" ></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td  align="center" colspan="2">
                                        <asp:Button ID="btnSav" Text="Save" runat="server"  CssClass="button" />
                                        <asp:Button ID="btnUClose" Text="Close" runat="server"  CssClass="button" />
                                    </td>
                                </tr>

                            </table>
                        </div>

                    </div>
                </div>
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                    HasCrystalLogo="False" Height="50px" PrintMode="ActiveX"
                    Width="350px"></CR:CrystalReportViewer>
                <asp:HiddenField ID="h_print" runat="server" />


            </div>
        </div>
    </div>
</asp:Content>

