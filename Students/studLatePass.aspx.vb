﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.IO
Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Drawing
Imports System.Drawing.Printing


Partial Class Students_studLatePass
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Dim STR As String = Encr_decrData.Decrypt("wr92h2HbWq4=")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S101301") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    txtStudNo.Focus()
                    txtreason.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    txtTime.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    txtdesc.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
           

        Else


        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSav)



    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub txtStudNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStudNo.TextChanged
        lblError.Text = ""
        If txtStudNo.Text <> "" Then
            divLate.Visible = True
            Displayitems()
            bindReasons()
            BIND_TIME()

            txtreason.Focus()

            'Dim manager As ScriptManager = ScriptManager.GetCurrent(Me)
            'manager.SetFocus(ddlreason)
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "SelectItem", "document.getElementById(""" + ddlreason.ClientID.ToString() + """).focus();", True)

            'txtdesc.Focus()
            ' ddlreason.Focus()
            'ddlreason.Focus()
            ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowLeaveDetails", "setfocus();", True)
            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "satrtup", "setfocus();")
            'Dim smScriptManager As New ScriptManager
            ' smScriptManager = Master.FindControl("ScriptManager1")
            ' smScriptManager.SetFocus(ddlreason)


        Else
            divLate.Visible = False
        End If

    End Sub

    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblError.Text = ""
        If txtStudNo.Text <> "" Then
            divLate.Visible = True
            Displayitems()
            bindReasons()
            BIND_TIME()
            txtreason.Focus()
            'ddlreason1.Focus()
            'Dim manager As ScriptManager = ScriptManager.GetCurrent(Me)
            'manager.SetFocus(ddlreason)
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "SelectItem", "document.getElementById(""" + ddlreason.ClientID.ToString() + """).focus();", True)
            'txtdesc.Focus()
            'ddlreason.Focus()
            ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowLeaveDetails", "setfocus();", True)
            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "satrtup", "setfocus();")
            'Dim smScriptManager As New ScriptManager
            'smScriptManager = Master.FindControl("ScriptManager1")
            ' smScriptManager.SetFocus(ddlreason)


        Else
            divLate.Visible = False
        End If
    End Sub

    Public Sub Displayitems()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@TYPE", "STUD")
            param(1) = New SqlParameter("@STU_NO", txtStudNo.Text)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_LATEPASS", param)


            If ds.Tables(0).Rows.Count >= 1 Then

                lblStudname.Text = ds.Tables(0).Rows(0).Item("STU_NAME")
                lblAcyear.Text = ds.Tables(0).Rows(0).Item("ACY_DESCR")
                lblGrade.Text = ds.Tables(0).Rows(0).Item("GRM_DISPLAY")
                lblParentEmail.Text = ds.Tables(0).Rows(0).Item("FATHER_EMAIL")
                lblParentMob.Text = ds.Tables(0).Rows(0).Item("FATHER_MOBILE")
                lblParentName.Text = ds.Tables(0).Rows(0).Item("FATHER")
                imgStud.ImageUrl = ds.Tables(0).Rows(0).Item("STU_PHOTOPATH")
                ViewState("STU_ID") = ds.Tables(0).Rows(0).Item("STU_ID")
                ViewState("GRD_ID") = ds.Tables(0).Rows(0).Item("GRD_ID")
                ViewState("SCT_ID") = ds.Tables(0).Rows(0).Item("SCT_ID")
                ViewState("ACD_ID") = ds.Tables(0).Rows(0).Item("ACD_ID")

                lblPickupBus.Text = ds.Tables(0).Rows(0).Item("PICKUPBUS")
                lblDropBus.Text = ds.Tables(0).Rows(0).Item("DROPBUS")
                lblPickupArea.Text = ds.Tables(0).Rows(0).Item("PICKUPAREA")
                lblDropArea.Text = ds.Tables(0).Rows(0).Item("DROPAREA")
                lblPickupPoint.Text = ds.Tables(0).Rows(0).Item("PICKUPPOINT")
                lblDropPoint.Text = ds.Tables(0).Rows(0).Item("DROPPOINT")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub

    Protected Sub hrefReason_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles hrefReason.ServerClick
        divReason.Visible = True
        divHistory.Visible = False
    End Sub

    Protected Sub hrefHistory_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles hrefHistory.ServerClick
        divReason.Visible = False
        divHistory.Visible = True
        gridbind()
    End Sub

    Public Sub bindReasons()
        'ddlreason.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT LR_ID,LR_DESCR  FROM STU.STU_LATE_REASONS"
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'ddlreason.DataSource = ds
        'ddlreason.DataTextField = "LR_DESCR"
        'ddlreason.DataValueField = "LR_ID"
        'ddlreason.DataBind()

        'If (Not ddlreason.Items Is Nothing) AndAlso (ddlreason.Items.Count > 1) Then
        '    ddlreason.Items.Add(New ListItem("--", "0"))
        '    ddlreason.Items.FindByText("--").Selected = True
        'End If
    End Sub

    Protected Sub lnkp_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        'CallReport(i)
        Response.Write("<Script> window.open('studLatePrint.aspx?SLS_ID=" & i & "') </Script>")

    End Sub


 

    Protected Sub gvLate_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLate.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblAlert As Label
            Dim lnkp As LinkButton = e.Row.FindControl("lnkp")
            lblAlert = e.Row.FindControl("lblAlert")
            If lblAlert.Text = "True" Then
                Dim ib As ImageButton = e.Row.Cells(6).Controls(0)
                ib.ImageUrl = "~\images\tick.gif"
            Else
                Dim ib As ImageButton = e.Row.Cells(6).Controls(0)
                ib.ImageUrl = "~\images\cross.gif"


            End If
            Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
            ScriptManager1.RegisterPostBackControl(lnkp)

        End If
    End Sub
    Sub gridbind()


        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@TYPE", "DETAIL")
            param(1) = New SqlParameter("@STU_NO", txtStudNo.Text)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_LATEPASS", param)

            gvLate.DataSource = ds
            gvLate.DataBind()
           

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub
    Sub save_late()

    End Sub

    Protected Sub btnSav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSav.Click
        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction
        Dim sls_id As Integer

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                param(0) = New SqlParameter("@SLS_STU_ID", ViewState("STU_ID"))
                param(1) = New SqlParameter("@SLS_ACD_ID", ViewState("ACD_ID"))
                param(2) = New SqlParameter("@SLS_GRD_ID", ViewState("GRD_ID"))
                param(3) = New SqlParameter("@SLS_SCT_ID", ViewState("SCT_ID"))
                param(4) = New SqlParameter("@SLS_LR_ID", txtreason.Text) 'ddlreason.SelectedValue)
                param(5) = New SqlParameter("@SLS_DESCR", IIf(txtdesc.Text = "", txtdesc.Text, "-"))
                param(6) = New SqlParameter("@SLS_bALERT", cbAlert.Checked)
                param(7) = New SqlParameter("@SLS_TIME", txtTime.Text)

                param(8) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(8).Direction = ParameterDirection.ReturnValue
                param(9) = New SqlClient.SqlParameter("@SLS_ID", SqlDbType.VarChar, 200)
                param(9).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVE_STU_LATE", param)
                Status = param(8).Value

                sls_id = param(9).Value



            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Status <> 0 Then
                    ' UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    transaction.Rollback()
                    ' lblError.Text = "Do not allow adding more than one with the same date & time "
                Else
                    lblError.Text = "Saved Successfully."
                    transaction.Commit()


                    If cbPrint.Checked Then
                        'CallReport(sls_id)
                        ' Response.Write("<Script> window.open('studLatePrint.aspx?ID=" & sls_id & "&TYPE=1') </Script>")
                        print_pass(sls_id)

                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "CallPrint", "CallPrint();", True)
                        
                    End If
                    ' fill_div(sls_id)
                    ' Create_file(sls_id)
                    clear()
                    txtStudNo.Text = ""
                    txtStudNo.Focus()
                End If
                clear()
                txtStudNo.Text = ""
                txtStudNo.Focus()
            End Try
            
        End Using
    End Sub
    Sub clear()
        'ddlreason.SelectedValue = "0"
        txtreason.Text = ""
        txtdesc.Text = ""
        cbAlert.Checked = False
    End Sub
    Sub CallReport(ByVal ID As Integer)
        ''Dim param As New Hashtable


        ''param.Add("@SLS_ID", ID)


        ''Dim rptClass As New rptClass
        ''With rptClass
        ''    .crDatabase = "oasis"
        ''    .reportParameters = param

        ''    .reportPath = Server.MapPath("../Students/Reports/Rpt/rptLatePassPRINT.rpt")


        ''End With
        ''LoadReports(rptClass, rs)





        Dim crystalReport As New ReportDocument()
        crystalReport.Load(Server.MapPath("../Students/Reports/Rpt/rptLatePassPRINT.rpt"))


        Dim dsCustomers As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim strformat As String = ""

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SLS_ID", ID)
        dsCustomers = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_STU_LATEPASS_PRINT", param)
        If dsCustomers.Tables(0).Rows.Count >= 1 Then




            ' Dim dsCustomers As DataSet = GetData("select * from visitor_details where  id ='" + Session("sessionvid") + "' and  plant ='" + Session("sessionplant") + "'")

            Dim dataTable As DataTable = dsCustomers.Tables(0)
            crystalReport.Database.Tables(0).SetDataSource(DirectCast(dataTable, DataTable))
            CrystalReportViewer2.ReportSource = crystalReport
            CrystalReportViewer2.Zoom(100)
            'crystalReportViewer1.ExportReport() ;
            CrystalReportViewer2.RefreshReport()


            Dim printeInstaled = System.Drawing.Printing.PrinterSettings.InstalledPrinters

            Dim printerName As String = ""

            Dim item1 As String
            For Each item1 In printeInstaled

                printerName = item1.ToString()
            Next
            Dim myPrinter As New PrintDocument()
            Dim PrinterName__1 As String = myPrinter.PrinterSettings.PrinterName.ToString()
            crystalReport.PrintOptions.PrinterName = PrinterName__1

            'crystalReport.PrintOptions.PrinterName = p.PrinterSettings.PrinterName 'GetDefaultPrinter()
            crystalReport.PrintToPrinter(1, False, 0, 0)

            'CrystalReportViewer2.PrintMode = CrystalDecisions.Web.PrintMode.ActiveX


        End If



        'Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Public Function GetDefaultPrinter() As String
        Dim ps As New System.Drawing.Printing.PrinterSettings()

        Dim defaultPrinterName As String = ps.PrinterName
        GetDefaultPrinter = defaultPrinterName
    End Function


    Public Sub print_pass(ByVal id As Integer)
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim strformat As String = ""

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SLS_ID", id)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_STU_LATEPASS_PRINT", param)
        If ds.Tables(0).Rows.Count >= 1 Then
            strformat = "<div style='border:1 solid;  height:200px;width:400px;'> " _
                        & " <div style='color:white;font-size:14px;font-weight:bold;'>LATE PASS</div><br> " _
                        & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("SLS_TIME") + " , " + ds.Tables(0).Rows(0).Item("SLS_DATE") + "</div><br> " _
                        & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("DESCR") + "</div><br><br> " _
                         & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("STU_NAME") + "</div><br> " _
                       & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("GRADE") + "</div> " _
                      & " </div>"


        End If
        divPrint.InnerHtml = strformat
    End Sub

    Public Sub print_pass_file(ByVal id As Integer)
        Dim sw As StreamWriter = Nothing
        If File.Exists("C:\Users\rajesh.kumar\Desktop\Test.txt") Then
            File.Delete("C:\Users\rajesh.kumar\Desktop\Test.txt")
        End If
        sw = File.CreateText("C:\Users\rajesh.kumar\Desktop\Test.txt")

        Dim line As [String] = New String("-"c, 50)

        sw.WriteLine(PrintHeaderAtCenter("Late Pass", 50))
        'sw.WriteLine(dt.Columns(0).ColumnName.PadRight(5) + dt.Columns(1).ColumnName.PadRight(20))
        sw.WriteLine(line)
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim strformat As String = ""

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SLS_ID", id)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_STU_LATEPASS_PRINT", param)
        If ds.Tables(0).Rows.Count >= 1 Then
            'strformat = "<div style='border:1 solid;  height:200px;width:400px;'> " _
            '            & " <div style='color:white;font-size:14px;font-weight:bold;'>LATE PASS</div><br> " _
            '            & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("SLS_TIME") + " , " + ds.Tables(0).Rows(0).Item("SLS_DATE") + "</div><br> " _
            '            & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("DESCR") + "</div><br><br> " _
            '             & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("STU_NAME") + "</div><br> " _
            '           & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("GRADE") + "</div> " _
            '          & " </div>"
            sw.WriteLine(" " + ds.Tables(0).Rows(0).Item("SLS_TIME").ToString().PadRight(3) + ds.Tables(0).Rows(0).Item("SLS_DATE").ToString().PadRight(20))
            sw.WriteLine(" " + ds.Tables(0).Rows(0).Item("DESCR").ToString().PadRight(3))
            sw.WriteLine(" " + ds.Tables(0).Rows(0).Item("STU_NAME").ToString().PadRight(3))
            sw.WriteLine(" " + ds.Tables(0).Rows(0).Item("GRADE").ToString().PadRight(3))

        End If

        

        ' To find sum of Sno(if you want you can use it)
        'object sdfds = dt.Compute("Sum(Sno)", "");
        'sw.WriteLine(sdfds);

        sw.WriteLine(line)
        sw.Close()

        ' Here only the data will be sent to the printer.
        System.Diagnostics.Process.Start("PRINT", "C:\Users\rajesh.kumar\Desktop\Test.txt")
    End Sub

    Private Function PrintHeaderAtCenter(ByVal TextToBePrinted As String, ByVal size As Double) As String
        Dim midPoint As Double = Math.Ceiling(size / 2)
        Dim strLength As Double = Math.Ceiling(Convert.ToDouble(TextToBePrinted.Length) / 2)
        Dim startWith As Double = midPoint - strLength

        Return " ".PadLeft(Convert.ToInt32(startWith)) & TextToBePrinted
    End Function

    'Sub Create_file(ByVal id As Integer)
    '    Dim fname As String = ""
    '    Dim sw As StreamWriter
    '    Try
    '        sw = File.CreateText(Server.MapPath(".") + "\Download\" + "MyHtml.html")
    '        sw.WriteLine(print_pass(id))
    '        sw.Close()
    '        fname = Server.MapPath(".") + "\Download\" + "MyHtml.html"
    '        SendToPrinter(fname)
    '    Catch ex As Exception
    '        Response.Write("File Creation Failed Due to " + ex.Message.ToString())
    '    End Try
    'End Sub
    Private Sub SendToPrinter(ByVal filename As String)
        Dim info As New ProcessStartInfo()
        info.Verb = "print"
        info.FileName = filename
        info.CreateNoWindow = True
        info.WindowStyle = ProcessWindowStyle.Hidden

        Dim p As New Process()
        p.StartInfo = info
        p.Start()

        Dim ticks As Long = -1
        While ticks <> p.TotalProcessorTime.Ticks
            ticks = p.TotalProcessorTime.Ticks
            Thread.Sleep(1000)
        End While

        If False = p.CloseMainWindow() Then
            p.Kill()
        End If
    End Sub



    'Sub fill_div(ByVal id As Integer)
    '    maindiv.Visible = True
    '    Dim str As String = print_pass(id)
    '    btnPrint.Attributes.Add("onClick", (Convert.ToString("CallPrint('maindiv','") & str) + "')")
    '    ' maindiv.Visible = False
    'End Sub


    Public Sub BIND_TIME()
        Try
            'Dim ds As DataSet
            'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = ""

            'Dim param(3) As SqlParameter
            'param(0) = New SqlParameter("@TYPE", "HOUR")


            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.STU_LATE_TIME", param)



            'ddlHr.DataSource = ds
            'ddlHr.DataTextField = "ID"
            'ddlHr.DataValueField = "ID"
            'ddlHr.DataBind()

            Dim d As DateTime
            d = Now()
            txtTime.Text = d.ToString("hh:mm tt")

            'ddlHr.Text = d.Hour

            'Dim paramS(3) As SqlParameter
            'paramS(0) = New SqlParameter("@TYPE", "MIN")


            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.STU_LATE_TIME", paramS)

            'ddlMin.DataSource = ds
            'ddlMin.DataTextField = "ID"
            'ddlMin.DataValueField = "ID"
            'ddlMin.DataBind()

            'ddlMin.Text = d.Minute

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub

#Region "Reports"
    Sub LoadReports(ByVal rptClass As rptClass, ByVal rs As CrystalDecisions.Web.CrystalReportSource)
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.ReportDocument.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword



                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If
                '   Response.ClearContent()
                ' Response.ClearHeaders()
                ' Response.ContentType = "application/pdf"
                ' rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                exportReport(rs.ReportDocument, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                rs.ReportDocument.Close()
                rs.Dispose()
                ' Response.Flush()
                'Response.Close()
            End With
        Catch ex As Exception
            rs.Dispose()
        End Try
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = HttpContext.Current.Server.MapPath("~/Curriculum/ReportDownloads/")
        Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        ' HttpContext.Current.Response.ContentType = "application/octect-stream"
        ' HttpContext.Current.Response.ContentType = "application/pdf"
        ' HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        ' HttpContext.Current.Response.Clear()
        ' HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()
        SendToPrinter(tempFileNameUsed)
        System.IO.File.Delete(tempFileNameUsed)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub
#End Region

    Protected Sub txtreason_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtreason.TextChanged
        txtreason.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
        btnSav.Focus()
        btnSav.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
        btnSav_Click(btnSav, Nothing)

    End Sub


   
End Class
