<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studReport_Setting.aspx.vb" Inherits="Students_studReport_Setting" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Attendance Report Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server"  width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                         ></asp:Label><span style=" color: #800000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                                    <span style=" color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr style=" color: #800000" valign="bottom">
                        <td align="center" class="matters"   valign="middle">&nbsp;Fields Marked with (<span
                            style=" color: #800000">*</span>) are mandatory</td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"   valign="bottom">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year <span style=" color: #800000">*</span></span></td>
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                      <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><br /><br /></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" class="matters" colspan="4"  >Parameter Settings
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"   colspan="4" valign="top">
                                        <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            DataKeyNames="APD_ID" Height="100%" Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ParaID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" __designer:wfdid="w46"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPD_ID" runat="server" Text='<%# Bind("APD_ID") %>' __designer:wfdid="w51"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Parameter" ItemStyle-Width="40%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPD_PARAM_DESCR" runat="server" Text='<%# Bind("PARAM_DESCR") %>' __designer:wfdid="w41" Width="50%"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="left"  ></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description"  ItemStyle-Width="40%">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:TextBox ID="txtARP_DESCR" runat="server" Text='<%# bind("ARP_DESCR") %>'  ></asp:TextBox>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Character"  ItemStyle-Width="20%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox5" runat="server" __designer:wfdid="w50"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:TextBox ID="txtARP_DISP" runat="server" Text='<%# bind("ARP_DISP") %>'  ></asp:TextBox>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center"  ></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop"   HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative"   />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="matters"  valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add/Edit" /><asp:Button ID="btnSave"
                                    runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" ValidationGroup="groupM1" /><asp:Button
                                        ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click"
                                        Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

