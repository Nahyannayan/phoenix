<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studDocuments.aspx.vb" Debug="true" Inherits="Students_studDocuments"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
 <base target="_self"/>
    <title>Untitled Page</title>
            <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<%--  <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />

    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
    
    <script language="javascript" type="text/javascript">
        function confirmDocCollect() {         
            return confirm('You are going to change document collected status');           
        }
    
    </script>  
    
     <style type="text/css">
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style> 
</head>
<body>

    <form id="form1" runat="server">
     <div> 
         <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
         </ajaxToolkit:ToolkitScriptManager>
                 <table align="center" cellpadding="5" cellspacing="0" width="100%" >
                            <tr  class="title-bg-lite" >
                                <td align="left"   >
                                    Documents Uploaded</td>
                            </tr>
                            <tr><td align="center">
                            
                             <telerik:RadGrid ID="gv_StuDocuments" runat="server" CssClass="table table-bordered table-row"
                                            AutoGenerateColumns="False" CellSpacing="0"  EnableTheming="False" 
                                             GridLines="None" >
                    <MasterTableView>
                    <CommandItemSettings ></CommandItemSettings>

                    <RowIndicatorColumn Visible="True" >
                    <HeaderStyle ></HeaderStyle>
                    </RowIndicatorColumn>

                    <ExpandCollapseColumn Visible="True" >
                    <HeaderStyle ></HeaderStyle>
                    </ExpandCollapseColumn>

                        <Columns>
                          <telerik:GridBoundColumn  
                                HeaderText="Date Uploaded" UniqueName="column" DataField="SDU_CREATED_DATE"  DataFormatString="{0:dd/MMM/yyyy hh:mm tt}">
                          </telerik:GridBoundColumn>
                          <telerik:GridTemplateColumn 
                                HeaderText="Document Type" UniqueName="TemplateColumn">
                              <ItemTemplate>       
                                  <asp:HiddenField ID="HF_SDU_ID" runat="server" Value='<%# Bind("SDU_ID") %>' />  
                                  <asp:Label ID="lblDOC_DESCR" runat="server" Text='<%# Bind("DOC_DESCR") %>' />                                                      
                                </ItemTemplate>
                          </telerik:GridTemplateColumn>
                          <telerik:GridTemplateColumn HeaderText="Verified" UniqueName="TemplateColumn_VERIFIED">
                                <ItemTemplate>
                                    <asp:Image id="imgDocVerified" runat="server" ImageUrl='<%# BIND("VERIFIED_SATUS") %>' HorizontalAlign="Center"></asp:Image> 
                                    <asp:LinkButton ID="lnkDoc_Verify" runat="server" ToolTip="Click here to change the document as verified one" Visible='<%# BIND("SDU_BVERIFIED") %>' OnClick="lnkDoc_Verify_Click">Verify</asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_Doc_Verify" ConfirmText="You are going to change this file as a verified one"
                                    TargetControlID="lnkDoc_Verify" runat="server">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>  
                          </telerik:GridTemplateColumn>
                          <telerik:GridTemplateColumn HeaderText="View" UniqueName="TemplateColumn_View">
                             <ItemTemplate>
                                      <a href='<%# Bind("SDU_FILEURL") %>' target="_blank" runat="server" id="hrefDoc" title="Click here to view the document">View</a>                               
                            </ItemTemplate>
                         </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn  
                                HeaderText="Update document" UniqueName="TemplateColumn1">
                                 <ItemTemplate>       
                                  <asp:HiddenField ID="hf_doc_id" runat="server" Value='<%# Bind("SDU_DOC_ID") %>' />   
                                  <asp:HiddenField ID="hf_DOC_DESCR" runat="server" Value='<%# Bind("DOC_DESCR") %>' />                                                  
                                    <asp:LinkButton ID="lnkchangeDoc" runat="server" onclick="lnkchangeDoc_Click">Upload</asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                       </Columns>

                    <EditFormSettings>
                    <EditColumn ></EditColumn>
                    </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left"   /> 
                    <ItemStyle Font-Bold="true" HorizontalAlign="Left" />   
                    <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                    <FilterMenu ></FilterMenu>
                    </telerik:RadGrid>                           
                </td>
            </tr>
        </table>
        <table align="center"  border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr  class="title-bg-lite">
                <td align="left" >
                    Documents Pending/Collected
                </td>
            </tr>
            <tr>
                <td align="center">
                       <telerik:RadGrid ID="gv_StuPendingDoc" runat="server" CssClass="table table-bordered table-row"
                                            AutoGenerateColumns="False" CellSpacing="0"  EnableTheming="False"  
                           GridLines="None" >
                    <MasterTableView>
                    <CommandItemSettings ></CommandItemSettings>

                    <RowIndicatorColumn Visible="True" >
                    <HeaderStyle ></HeaderStyle>
                    </RowIndicatorColumn>

                    <ExpandCollapseColumn Visible="True" >
                    <HeaderStyle ></HeaderStyle>
                    </ExpandCollapseColumn>

                        <Columns>
                            <telerik:GridBoundColumn  
                                HeaderText="Document Type" UniqueName="column" DataField="DOC_DESCR" >
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn 
                                HeaderText="Collected" UniqueName="TemplateColumn1">
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="cb_collected" runat="server" onclick="return confirmDocCollect();" AutoPostBack="true" OnCheckedChanged="cb_collected_CheckedChanged"/>--%>
                                    <asp:ImageButton id="imgDocCollected" runat="server" 
                                        ImageUrl='<%# BIND("COLLECTED_STATUS") %>' HorizontalAlign="Center" 
                                        ToolTip="Click here to change the document collect status" 
                                        onclick="imgDocCollected_Click" ></asp:ImageButton>                                     
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_cb_collected" ConfirmText="You are going to change document collected status"
                                    TargetControlID="imgDocCollected" runat="server">
                                     </ajaxToolkit:ConfirmButtonExtender>  
                                    <asp:HiddenField ID="HF_SDU_ID" runat="server" Value='<%# Bind("SDU_ID") %>' />  
                                    <asp:HiddenField ID="HF_SDU_BCOLLECTED" runat="server" Value='<%# Bind("SDU_BCOLLECTED") %>' />  
                                                                 
                                </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn 
                                HeaderText="Upload" UniqueName="TemplateColumn" >
                              <ItemTemplate>       
                                  <asp:HiddenField ID="hf_doc_id" runat="server" Value='<%# Bind("DOC_ID") %>' />  
                                  <asp:HiddenField ID="hf_DOC_DESCR" runat="server" Value='<%# Bind("DOC_DESCR") %>' />                     
                                    <asp:LinkButton ID="lnkUpload" runat="server" OnClick="lnkUpload_Click">Upload</asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>

                    <EditFormSettings>
                    <EditColumn ></EditColumn>
                    </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle  HorizontalAlign="Left"/> 
                    <ItemStyle  HorizontalAlign="Left" />   
                     <AlternatingItemStyle  HorizontalAlign="Left" />
                    <FilterMenu ></FilterMenu>
                    </telerik:RadGrid>
     
                </td>
            </tr>
        </table>
    </div>
    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
    <asp:HiddenField ID="HF_stuid" runat="server" />
    <asp:Panel ID="pnl_UploadDoc" runat="server" class="darkPanlAlumini" >
        <div class="panel-cover inner_darkPanlAlumini" >
            <div >
                <div style="float: left; width: 88%;">
                    <b>Upload Document</b> 
                    
                </div>
                <div style="float: right; vertical-align: top;">
                    <asp:LinkButton ForeColor="red" ID="lbtnUploadDocClose" ToolTip="click here to close"
                        CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false">X</asp:LinkButton>
                </div>
            </div>
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tr >
                  <td><span class="field-label">Document</span></td>
         
                    <td  ><asp:Label ID="lblDoc" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr >
                    <td ><span class="field-label">Upload Document</span></td>
                   
                    <td>
                        <asp:FileUpload ID="FileUpload_stud" runat="server" />
                    </td>
                </tr>
            
                <tr >
                <td ><span class="field-label">Verified Document</span></td>    
                 
                <td>
                    <asp:CheckBox ID="chb_bverified" Checked="true" runat="server" /> </td>
                </tr> 
                <tr >
                    <td align="center" colspan="3">
                        <asp:Label ID="lbluploadError" runat="server" Text="" EnableViewState="false"
                            ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Button ID="btnuploadDoc" runat="server" Text="Upload" CausesValidation="true" Visible="false"  
                            ValidationGroup="Upload" CssClass="button" />
                              <asp:Button ID="btnuploadChangeDoc" runat="server" Text="Upload new document" CausesValidation="true" Visible="false"  
                            ValidationGroup="Upload" CssClass="button" />
                        <asp:Button ID="btnCancelupload" runat="server" Text="Close" CausesValidation="false"
                             CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
