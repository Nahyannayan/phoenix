<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studStrikeOffRecommend_M.aspx.vb" Inherits="Students_studStrikeOffRecommend_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_remove() {

            if (confirm("You are about to remove the strike off recommendation.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Strike Off Recommendation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" style="width: 100%">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLast"
                                Display="None" ErrorMessage="Please enter the last attendance date" ValidationGroup="groupM1"
                                Width="23px"></asp:RequiredFieldValidator>
                            &nbsp;
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="center" valign="bottom" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Width="475px"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td   valign="top">
                            <table align="center"  cellpadding="5" cellspacing="0"
                                style="width: 100%" >
                               
                                <tr>
                                    <td align="left"  ><span class="field-label">Student Name</span></td>
                                    
                                    <td align="left" >
                                        <asp:TextBox ID="txtName" runat="server" Enabled="False"></asp:TextBox></td>
                                    <td align="left"  ><span class="field-label">SEN</span></td>
                                   
                                    <td align="left"  >
                                        <asp:TextBox ID="txtSEN" runat="server" Enabled="False"></asp:TextBox></td>
                                </tr>


                                <tr>
                                    <td align="left"  ><span class="field-label">Recommended Date</span><span style="font-size: 8pt; color: #800000">*</span></td>
                                   
                                    <td align="left"  >
                                        <asp:TextBox ID="txtRec" runat="server" Width="99px">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgRec" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="revRec" runat="server" ControlToValidate="txtRec"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Recommended Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>

                                    <td align="left"  ><span class="field-label">Last Attendance Date</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                   
                                    <td align="left"  >
                                        <asp:TextBox ID="txtLast" runat="server" Width="99px"></asp:TextBox>
                                        <asp:ImageButton ID="imgLast" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="revLast" runat="server" ControlToValidate="txtLast"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Last Attendance  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>


                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Remarks</span><span style="font-size: 8pt; color: #800000">*</span></td>
                                    
                                    <td align="left"   colspan="4">
                                        <asp:TextBox ID="txtRemarks" runat="server" Width="238px" TextMode="MultiLine" Height="67px" SkinID="MultiText"></asp:TextBox>
                                    </td>

                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td  align="center" valign="bottom">&nbsp;
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" OnClick="btnSave_Click" />
                            <asp:Button ID="btnRemove" OnClientClick="return confirm_remove();" runat="server" CssClass="button" Text="Remove" Visible="False" ValidationGroup="groupM1" OnClick="btnRemove_Click" />&nbsp;
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                    <tr>
                        <td  valign="bottom" style="width: 481px; height: 178px;">&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                            Display="None" ErrorMessage="Please enter data in the field remarks" ValidationGroup="groupM1"
                            Width="23px">
                        </asp:RequiredFieldValidator>&nbsp;
                    <asp:HiddenField ID="hfSTK_ID" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRec"
                                Display="None" ErrorMessage="Please enter the recommended date" ValidationGroup="groupM1"
                                Width="23px"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender
                                ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtLast" TargetControlID="txtLast">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgLast" TargetControlID="txtLast">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;&nbsp;
                    <asp:HiddenField ID="hfMode" runat="server" />
                            <ajaxToolkit:CalendarExtender
                                ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtRec" TargetControlID="txtRec">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgRec" TargetControlID="txtRec">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                    &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

