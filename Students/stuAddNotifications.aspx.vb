﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Students_stuAddNotifications
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0
    Private Property VSgridNotifications() As DataTable
        Get
            Return ViewState("Notifications")
        End Get
        Set(ByVal value As DataTable)
            ViewState("Notifications") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Else
                        ViewState("ViewId") = ""
                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                    ShowMessage("", True)
                    fillTranAcdY()
                    viewidUpdate.Value = 0
                    'disable the control based on the rights 
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
                    ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))
                    PopulateGrade() 
                    populateDdllCategory()
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "diverrorPopUp"
            Else
                lblError.CssClass = "divvalidPopUp"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
    Sub fillTranAcdY()
        Try
            Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
            ddlACDYear.DataSource = dtACD
            ddlACDYear.DataTextField = "ACY_DESCR"
            ddlACDYear.DataValueField = "ACD_ID"
            ddlACDYear.DataBind()
            For Each rowACD As DataRow In dtACD.Rows
                If rowACD("ACD_CURRENT") Then
                    ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                    Exit For
                End If
            Next            
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillTranAcdY :" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Function PopulateGradeandSections(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataTable
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ALL_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGradeandSections" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Function
    Protected Function selectedGradesAndSections(ByVal viewid As Integer) As DataTable
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", viewid, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.Get_PHOENIXMISC_Connection
                Dim sql_query As String = "[DBO].[GET_SELECTED_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From selectedGradesAndSections: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Function
    Private Sub SelectedTreeView(ByVal viewid As Integer)
        'For Each node As TreeNode In nodes
        '    node.Checked = selAll
        '    If node.ChildNodes.Count > 0 Then
        '        SelectAllTreeView(node.ChildNodes, selAll)
        '    End If
        'Next
        Try
            Dim dtTable As DataTable = PopulateGradeandSections(ddlACDYear.SelectedValue, Session("sbsuid"))
            Dim dtTble_selected As DataTable = selectedGradesAndSections(viewid)
            ' PROCESS Filter
            Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "GRM_GRD_ID", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("Select All", "ALL")
            Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
            Dim drTRM_DESCRIPTION As DataRowView
            While (ienumTRM_DESCRIPTION.MoveNext())
                'Processes List
                drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("GRM_GRD_ID") Then
                        contains = True
                    End If
                End While
                Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("GRM_GRD_ID"), drTRM_DESCRIPTION("GRM_GRD_ID"))
                If contains Then
                    Continue While
                End If
                Dim strAMS_GRADE As String = "GRM_GRD_ID = '" & _
                drTRM_DESCRIPTION("GRM_GRD_ID") & "'"
                Dim dvAMS_GRADE As New DataView(dtTable, strAMS_GRADE, "GRM_GRD_ID", DataViewRowState.OriginalRows)
                Dim ienumAMS_GRADE As IEnumerator = dvAMS_GRADE.GetEnumerator
                While (ienumAMS_GRADE.MoveNext())
                    Dim drSCT_DESCR As DataRowView = ienumAMS_GRADE.Current
                    Dim trNodeSCT_DESCR As New TreeNode(drSCT_DESCR("SCT_DESCR"), (drSCT_DESCR("SCT_ID")))
                    trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeSCT_DESCR)
                    For Each row In dtTble_selected.Rows
                        If (row("NGD_SCT_ID").ToString = drSCT_DESCR("SCT_ID").ToString) Then
                            trNodeSCT_DESCR.Checked = True
                        End If
                    Next
                End While
                trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
            End While
            trGrades.Nodes.Clear()
            trGrades.Nodes.Add(trSelectAll)
            trGrades.DataBind()
            trGrades.CollapseAll()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGrade: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Private Sub PopulateGrade()
        Try
            Dim dtTable As DataTable = PopulateGradeandSections(ddlACDYear.SelectedValue, Session("sbsuid"))
            ' PROCESS Filter
            Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "GRM_GRD_ID", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("Select All", "ALL")
            Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
            Dim drTRM_DESCRIPTION As DataRowView
            While (ienumTRM_DESCRIPTION.MoveNext())
                'Processes List
                drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("GRM_GRD_ID") Then
                        contains = True
                    End If
                End While
                Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("GRM_GRD_ID"), drTRM_DESCRIPTION("GRM_GRD_ID"))
                If contains Then
                    Continue While
                End If
                Dim strAMS_MONTH As String = "GRM_GRD_ID = '" & _
                drTRM_DESCRIPTION("GRM_GRD_ID") & "'"
                Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "GRM_GRD_ID", DataViewRowState.OriginalRows)
                Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
                While (ienumAMS_MONTH.MoveNext())
                    Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                    Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("SCT_DESCR"), (drMONTH_DESCR("SCT_ID")))
                    trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
                End While
                trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
            End While
            trGrades.Nodes.Clear()
            trGrades.Nodes.Add(trSelectAll)
            trGrades.DataBind()
            trGrades.CollapseAll()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGrade: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Sub PopulateGradetoShowonUpdate()

    End Sub
    Protected Sub ddlACDYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            PopulateGrade()
            populateDdllCategory()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From ddlACDYear_SelectedIndexChanged: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    'Protected Sub gridAllNotificationsList_RowCommand(sender As Object, e As GridViewCommandEventArgs)

    '    If e.CommandName = "btnUpdateCommand" Then
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim gvRow As GridViewRow = gridAllNotificationsList.Rows(index)

    '        Dim viewid As Integer = Convert.ToInt64(DirectCast(gvRow.FindControl("lblViewId"), Label).Text)

    '        clearall()
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim pParam(0) As SqlParameter
    '        Try
    '            pParam(0) = Mainclass.CreateSqlParameter("@VIEWID", viewid, SqlDbType.BigInt)
    '            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[dbo].[GET_NOTIFICATION_ONUPDATE]", pParam)
    '            If Not ds Is Nothing And ds.Tables.Count > 0 Then
    '                Dim dt As DataTable = ds.Tables(0)
    '                txtNotificationText.InnerText = dt.Rows(0)("NLD_NOTIFICATION_CONTENT").ToString
    '                ddlACDYear.ClearSelection()
    '                ddlACDYear.Items.FindByText(dt.Rows(0)("ACY_DESCR").ToString).Selected = True
    '                ddlCategory.ClearSelection()
    '                ddlCategory.Items.FindByText(dt.Rows(0)("NCM_DESCR").ToString).Selected = True
    '                Dim Array() As String = dt.Rows(0)("NLD_NOTIFICATION_DATE").ToString.Split(" ")
    '                txtNotificationDt.Text = Array(0)

    '                txtNotificationTitle.InnerText = dt.Rows(0)("NLD_NOTIFICATION_TITLE").ToString
    '                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
    '                btnSave.Text = "Update"
    '                viewidUpdate.Value = viewid
    '                SelectedTreeView(viewid)
    '            End If
    '        Catch ex As Exception

    '        End Try
    '    ElseIf e.CommandName = "btnDeleteCommand" Then
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim gvRow As GridViewRow = gridAllNotificationsList.Rows(index)
    '        Dim viewid As Integer = Convert.ToInt64(DirectCast(gvRow.FindControl("lblViewId"), Label).Text)

    '        Dim strans As SqlTransaction = Nothing
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim pParam(0) As SqlParameter
    '        Try
    '            Dim objConn As New SqlConnection(str_conn)
    '            objConn.Open()
    '            strans = objConn.BeginTransaction
    '            pParam(0) = Mainclass.CreateSqlParameter("@VIEWID", viewid, SqlDbType.BigInt)
    '            SqlHelper.ExecuteNonQuery(strans, "[dbo].[DELETE_NOTIFICATIONS]", pParam)
    '            strans.Commit()
    '            ShowMessage("Notification got deleted", False)
    '            FillGridView()
    '        Catch ex As Exception
    '            strans.Rollback()
    '        End Try
    '    Else

    '    End If
    'End Sub
    Protected Sub InsertNotifications_OLD()
        Dim Today As DateTime = DateTime.Today
        Dim NotificationDateTime As DateTime
        Dim strans As SqlTransaction = Nothing
        Dim str_conn As String = ConnectionManger.Get_PHOENIXMISC_ConnectionString
        Dim pParams(7) As SqlParameter
        Dim Params(5) As SqlParameter
        Dim qry1 = "[DBO].[INSERT_NOTIFICATIONLIST_ONCREATION]"
        Dim qry2 = "[DBO].[INSERT_NOTIFICATIONGRADES_ONCREATION]"
        Dim GET_SELECTED_SECTIONS As String = ""
        Dim GET_SELECTED_GRADES As String = ""
        Dim GET_SELECTED_SEC_DESCR As String = ""
        Try
            NotificationDateTime = DateTime.Parse((txtNotificationDt.Text).Trim())
            If trGrades.CheckedNodes.Count = 0 Then
                Message = getErrorMessage("4015")
                ShowMessage(Message, True)
            ElseIf txtNotificationDt.Text = "" Or txtNotificationDt Is Nothing Or txtNotificationText.InnerText = "" Or txtNotificationText.InnerText Is Nothing Then
                Message = "Please Enter Date"
                ShowMessage(Message, True)
            ElseIf NotificationDateTime < Today Then
                Message = "Selected Date And Time Is Expired. Please Select Valid Date"
                ShowMessage(Message, True)
            Else
                Try
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.Int)
                    pParams(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
                    pParams(2) = Mainclass.CreateSqlParameter("@NOTIFICATION_CONTENT", txtNotificationText.InnerText, SqlDbType.VarChar)
                    pParams(3) = Mainclass.CreateSqlParameter("@NOTIFICATION_DATETIME", NotificationDateTime, SqlDbType.DateTime)
                    pParams(4) = Mainclass.CreateSqlParameter("@CREATOR", ViewState("EmployeeID").ToString, SqlDbType.VarChar)
                    pParams(5) = Mainclass.CreateSqlParameter("@CATEGORY", ddlCategory.SelectedValue, SqlDbType.BigInt)
                    pParams(6) = Mainclass.CreateSqlParameter("@NLD_NOTIFICATION_TITLE", txtNotificationTitle.InnerText, SqlDbType.NVarChar)

                    Latest_refid = SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry1, pParams)
                    For Each node As TreeNode In trGrades.CheckedNodes
                        If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                            Continue For
                        End If
                        If node.Value <> "ALL" Then
                            GET_SELECTED_SEC_DESCR = node.Text
                            GET_SELECTED_SECTIONS = node.Value
                            GET_SELECTED_GRADES = node.Parent.Value
                            'Dim A As String = node
                            Params(0) = Mainclass.CreateSqlParameter("@REF_ID", Latest_refid, SqlDbType.Int)
                            Params(1) = Mainclass.CreateSqlParameter("@GRD_ID", GET_SELECTED_GRADES, SqlDbType.VarChar)
                            Params(2) = Mainclass.CreateSqlParameter("@SCT_ID", GET_SELECTED_SECTIONS, SqlDbType.VarChar)
                            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
                        End If
                    Next
                    strans.Commit()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                    ShowMessage("Notification details added successfully", False)
                Catch ex As Exception
                    strans.Rollback()
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub InsertNotifications_STudents()
        Dim Today As DateTime = DateTime.Today
        Dim NotificationDateTime As DateTime
        Dim strans As SqlTransaction = Nothing
        Dim str_conn As String = ConnectionManger.Get_PHOENIXMISC_ConnectionString

        Dim pParams(12) As SqlParameter
        Dim Params(5) As SqlParameter
        Dim qry1 = "[DBO].[INSERT_NOTIFICATIONLIST_ONCREATION_GENERIC]"
        Dim GET_SELECTED_SECTIONS As String = ""
        Dim GET_SELECTED_GRADES As String = ""
        Dim GET_SELECTED_SEC_DESCR As String = ""
        Try
            NotificationDateTime = DateTime.Parse((txtNotificationDt.Text).Trim())
            If txtNotificationDt.Text = "" Or txtNotificationDt Is Nothing Or txtNotificationText.InnerText = "" Or txtNotificationText.InnerText Is Nothing Then
                Message = "Please Enter Date"
                ShowMessage(Message, True)
            ElseIf NotificationDateTime < Today Then
                Message = "Selected Date And Time Is Expired. Please Select Valid Date"
                ShowMessage(Message, True)
            Else
                Try

                    Dim str_Stu_ids As String = h_Stu_Ids.Value.Substring(1)
                    str_Stu_ids = str_Stu_ids.Replace(",", "|")

                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.Int)
                    pParams(1) = Mainclass.CreateSqlParameter("@NOTIFICATION_CONTENT", txtNotificationText.InnerText, SqlDbType.NVarChar)
                    pParams(2) = Mainclass.CreateSqlParameter("@NOTIFICATION_DATETIME", NotificationDateTime, SqlDbType.DateTime)


                    pParams(3) = Mainclass.CreateSqlParameter("@CREATOR", ViewState("EmployeeID").ToString, SqlDbType.VarChar)
                    pParams(4) = Mainclass.CreateSqlParameter("@CATEGORY", ddlCategory.SelectedValue, SqlDbType.BigInt)
                    pParams(5) = Mainclass.CreateSqlParameter("@GRADES", "", SqlDbType.VarChar)

                    pParams(6) = Mainclass.CreateSqlParameter("@STU_IDs", str_Stu_ids, SqlDbType.NVarChar)
                    pParams(7) = Mainclass.CreateSqlParameter("@MESSAGE_TYPE", "", SqlDbType.NVarChar)
                    pParams(8) = Mainclass.CreateSqlParameter("@NOTIFICATION_TITLE", txtNotificationTitle.InnerText, SqlDbType.NVarChar)


                    SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry1, pParams)

                    strans.Commit()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                    ShowMessage("Notification details added successfully", False)
                Catch ex As Exception
                    strans.Rollback()
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub InsertNotifications_New()
        Dim Today As DateTime = DateTime.Today
        Dim NotificationDateTime As DateTime
        Dim strans As SqlTransaction = Nothing
        ''nahyan commented this to misc connection string with manoj ion 4jan2021
        Dim str_conn As String = ConnectionManger.Get_PHOENIXMISC_ConnectionString
        Dim pParams(12) As SqlParameter
        Dim Params(5) As SqlParameter
        Dim qry1 = "[DBO].[INSERT_NOTIFICATIONLIST_ONCREATION_GENERIC]"
        Dim GET_SELECTED_SECTIONS As String = ""
        Dim GET_SELECTED_GRADES As String = ""
        Dim GET_SELECTED_SEC_DESCR As String = ""
        Try
            NotificationDateTime = DateTime.Parse((txtNotificationDt.Text).Trim())
            If trGrades.CheckedNodes.Count = 0 Then
                Message = getErrorMessage("4015")
                ShowMessage(Message, True)
            ElseIf txtNotificationDt.Text = "" Or txtNotificationDt Is Nothing Or txtNotificationText.InnerText = "" Or txtNotificationText.InnerText Is Nothing Then
                Message = "Please Enter Date"
                ShowMessage(Message, True)
            ElseIf NotificationDateTime < Today Then
                Message = "Selected Date And Time Is Expired. Please Select Valid Date"
                ShowMessage(Message, True)
            Else
                Try

                    Dim grades As New StringBuilder()
                    For Each node As TreeNode In trGrades.CheckedNodes
                        If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                            Continue For
                        End If
                        If node.Value IsNot Nothing AndAlso node.Parent.Value IsNot Nothing Then
                            If node.Value <> "ALL" Then
                                grades.Append("|")
                                grades.Append(node.Parent.Value.ToString())
                                grades.Append("#")
                                grades.Append(node.Value.ToString())
                            End If
                        End If

                    Next



                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.Int)
                    pParams(1) = Mainclass.CreateSqlParameter("@NOTIFICATION_CONTENT", txtNotificationText.InnerText, SqlDbType.NVarChar)
                    pParams(2) = Mainclass.CreateSqlParameter("@NOTIFICATION_DATETIME", NotificationDateTime, SqlDbType.DateTime)


                    pParams(3) = Mainclass.CreateSqlParameter("@CREATOR", ViewState("EmployeeID").ToString, SqlDbType.VarChar)
                    pParams(4) = Mainclass.CreateSqlParameter("@CATEGORY", ddlCategory.SelectedValue, SqlDbType.BigInt)
                    pParams(5) = Mainclass.CreateSqlParameter("@GRADES", grades.ToString().Substring(1), SqlDbType.VarChar)

                    pParams(6) = Mainclass.CreateSqlParameter("@STU_IDs", "", SqlDbType.NVarChar)
                    pParams(7) = Mainclass.CreateSqlParameter("@MESSAGE_TYPE", "", SqlDbType.NVarChar)
                    pParams(8) = Mainclass.CreateSqlParameter("@NOTIFICATION_TITLE", txtNotificationTitle.InnerText, SqlDbType.NVarChar)


                    SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry1, pParams)

                    strans.Commit()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                    ShowMessage("Notification details added successfully", False)
                Catch ex As Exception
                    strans.Rollback()
                End Try
            End If
        Catch ex As Exception
            Dim str As String = ex.Message

        End Try
    End Sub
    Protected Sub UpdateNotifications(ByVal viewid As Integer)
        Dim NotificationDateTime As DateTime
        Dim strans As SqlTransaction = Nothing
        Dim Today As DateTime = DateTime.Today
        Dim str_conn As String = ConnectionManger.Get_PHOENIXMISC_ConnectionString
        Dim Params(4) As SqlParameter
        Dim pParams(6) As SqlParameter
        Dim GET_SELECTED_SECTIONS As String = ""
        Dim GET_SELECTED_GRADES As String = ""
        Dim GET_SELECTED_SEC_DESCR As String = ""
        Dim qry1 = "[DBO].[UPDATE_NOTIFICATIONS]"
        Dim qry2 = "[DBO].[INSERT_NOTIFICATIONGRADES_ONCREATION]"
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            NotificationDateTime = DateTime.Parse((txtNotificationDt.Text).Trim())
            pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.BigInt)
            pParams(1) = Mainclass.CreateSqlParameter("@VIEWID", viewid, SqlDbType.BigInt)
            pParams(2) = Mainclass.CreateSqlParameter("@NOTIFICATION_CONTENT", txtNotificationText.InnerText, SqlDbType.NVarChar)
            pParams(3) = Mainclass.CreateSqlParameter("@NOTIFICATION_DATE", NotificationDateTime, SqlDbType.DateTime)
            pParams(4) = Mainclass.CreateSqlParameter("@CATEGORY", ddlCategory.SelectedValue, SqlDbType.BigInt)
            pParams(5) = Mainclass.CreateSqlParameter("@NLD_NOTIFICATION_TITLE", txtNotificationTitle.InnerText, SqlDbType.NVarChar)
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry1, pParams)
            For Each node As TreeNode In trGrades.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If node.Value <> "ALL" Then
                    GET_SELECTED_SEC_DESCR = node.Text
                    GET_SELECTED_SECTIONS = node.Value
                    GET_SELECTED_GRADES = node.Parent.Value
                    'Dim A As String = node
                    Params(0) = Mainclass.CreateSqlParameter("@REF_ID", viewid, SqlDbType.Int)
                    Params(1) = Mainclass.CreateSqlParameter("@GRD_ID", GET_SELECTED_GRADES, SqlDbType.VarChar)
                    Params(2) = Mainclass.CreateSqlParameter("@SCT_ID", GET_SELECTED_SECTIONS, SqlDbType.VarChar)
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                    ShowMessage("Notification details updated successfully", False)
                End If
            Next
            strans.Commit()

        Catch ex As Exception
            strans.Rollback()
        End Try
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If (btnSave.Text = "Submit") Then
            If chkAdhoc.Checked = True Then
                InsertNotifications_STudents()
            Else
                InsertNotifications_New()
            End If

        ElseIf (btnSave.Text = "Update") Then
            UpdateNotifications(Convert.ToInt32(viewidUpdate.Value))
        End If

    End Sub
    Protected Sub clearall()
        ShowMessage("", False)
        fillTranAcdY()
        txtNotificationDt.Text = ""
        txtNotificationText.InnerText = ""
        'txtNotificationTime.Text = ""
        PopulateGrade()
        h_Stu_Ids.Value = ""
        Session("STU_SELECTION") = Nothing
        chkAdhoc.Checked = False
        tblStudentView.Visible = False
        tblTreeView.Visible = True
        gvStudents.DataSource = Nothing
        gvStudents.DataBind()
        txtNotificationTitle.InnerText = ""
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Try
            clearall()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        Dim URL As String = Nothing
        Try
            ' Response.Redirect("stuAddNotifications.aspx?MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")))
            URL = String.Format("stuAddViewNotification.aspx?MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")))
            Response.Redirect(URL)
        Catch ex As Exception

        End Try
    End Sub




    Protected Sub populateDdllCategory()
        '
        Try
            Dim str_conn As String = ConnectionManger.Get_PHOENIXMISC_ConnectionString
            Dim dtCategory As DataSet
            dtCategory = SqlHelper.ExecuteDataset(str_conn, "[DBO].[GET_NOTIFICATION_CATEGORY]")
            ddlCategory.DataSource = dtCategory.Tables(0)
            ddlCategory.DataTextField = "DESCR"
            ddlCategory.DataValueField = "ID"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, New ListItem("SELECT", ""))
            ddlCategory.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From populateDdllCategory :" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    
    Protected Sub txtSTU_IDs_TextChanged(sender As Object, e As EventArgs) Handles txtSTU_IDs.TextChanged
        txtSTU_IDs.Text = ""
        If h_Stu_Ids.Value <> "" Then
            gvStudents.Visible = True
            FillTrainerNames(h_Stu_Ids.Value)
        Else
            gvStudents.Visible = False
        End If
    End Sub
    Private Function FillTrainerNames(ByVal stu_ids As String) As Boolean

        Dim IDs As String() = stu_ids.Split(",")
        Dim condition As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ds As DataSet, dt As DataTable
        Dim param(3) As SqlClient.SqlParameter

        If stu_ids <> "" Then
            param(0) = New SqlParameter("@stu_ids", stu_ids)
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(2) = New SqlParameter("@FILTERCONDITION", " AND 1=1")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GET_STUDENT_LIST_NOTIFICATION]", param)


            gvStudents.DataSource = ds
            Session("STU_SELECTION") = ds
            gvStudents.DataBind()

            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
        Else
            Return False
        End If



        Return True
    End Function
    Protected Sub gvStudents_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudents.RowDeleting
        Try
            Dim stu_id As Integer = CType(Me.gvStudents.Rows(e.RowIndex).FindControl("lblId"), Label).Text
            Dim ds As DataSet = Session("STU_SELECTION")
            If Not ds Is Nothing Then
                ds.Tables(0).Rows.Remove(ds.Tables(0).Select("stu_id = " & stu_id)(0))
                ds.AcceptChanges()
                Me.gvStudents.DataSource = ds
                Session("STU_SELECTION") = ds
                Dim trainerID As String = h_Stu_Ids.Value
                Dim newsd As String = String.Empty
                Dim newsd1 As String = String.Empty
                If trainerID <> "" Then
                    newsd = trainerID.Replace(id, "")
                    newsd1 = newsd.Replace(", ,", ",")
                    newsd1.TrimEnd(", ")
                    h_Stu_Ids.Value = newsd1.TrimEnd()
                    h_Stu_Ids.Value = newsd1.TrimStart()

                    If h_Stu_Ids.Value.EndsWith(",") Then
                        Dim subE As String = h_Stu_Ids.Value.Substring(0, h_Stu_Ids.Value.Length - 1) 
                        h_Stu_Ids.Value = subE 
                    End If
                    If h_Stu_Ids.Value.StartsWith(",") Then
                        Dim SubS = h_Stu_Ids.Value.Substring(1, h_Stu_Ids.Value.Length - 1)
                        h_Stu_Ids.Value = SubS
                    End If

                End If
                Me.gvStudents.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    
    Protected Sub chkAdhoc_CheckedChanged(sender As Object, e As EventArgs)
        If chkAdhoc.Checked = True Then
            tblStudentView.Visible = True
            tblTreeView.Visible = False
        Else
            tblStudentView.Visible = False
            tblTreeView.Visible = True
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If chkAdhoc.Checked = True Then
            tblStudentView.Visible = True
            tblTreeView.Visible = False
        Else
            tblStudentView.Visible = False
            tblTreeView.Visible = True
        End If
    End Sub
End Class
