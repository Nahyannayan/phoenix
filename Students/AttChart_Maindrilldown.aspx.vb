Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports InfoSoftGlobal
Partial Class Students_AttChart_Maindrilldown
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ltDrilldownMain.Text = CreateCharts_Main()
    End Sub
    'Public Function CreateCharts_Main() As String

    '    'In this example, we plot a multi series chart from data contained
    '    'in an array. The array will have three columns - first one for data label (product)
    '    'and the next two for data values. The first data value column would store sales information
    '    'for current year and the second one for previous year.

    '    'Let's store the sales data for 6 products in our array. We also store
    '    'the name of products. 
    '    Dim arrData(6, 3) As String
    '    'Store Name of Products
    '    arrData(0, 1) = "Product A"
    '    arrData(1, 1) = "Product B"
    '    arrData(2, 1) = "Product C"
    '    arrData(3, 1) = "Product D"
    '    arrData(4, 1) = "Product E"
    '    arrData(5, 1) = "Product F"
    '    'Store sales data for current year
    '    arrData(0, 2) = "567500"
    '    arrData(1, 2) = "815300"
    '    arrData(2, 2) = "556800"
    '    arrData(3, 2) = "734500"
    '    arrData(4, 2) = "676800"
    '    arrData(5, 2) = "648500"
    '    'Store sales data for previous year
    '    arrData(0, 3) = "547300"
    '    arrData(1, 3) = "584500"
    '    arrData(2, 3) = "754000"
    '    arrData(3, 3) = "456300"
    '    arrData(4, 3) = "754500"
    '    arrData(5, 3) = "437600"

    '    'Now, we need to convert this data into multi-series XML. 
    '    'We convert using string concatenation.
    '    'strXML - Stores the entire XML
    '    'strCategories - Stores XML for the <categories> and child <category> elements
    '    'strDataCurr - Stores XML for current year's sales
    '    'strDataPrev - Stores XML for previous year's sales
    '    Dim strXML As String, strCategories As String, strDataCurr As String, strDataPrev As String, i As Integer

    '    'Initialize <graph> element
    '    strXML = "<graph caption='Sales by Product' numberPrefix='$' decimalPrecision='0' >"

    '    'Initialize <categories> element - necessary to generate a multi-series chart
    '    strCategories = "<categories>"

    '    'Initiate <dataset> elements
    '    strDataCurr = "<dataset seriesName='Current Year' color='AFD8F8'>"
    '    strDataPrev = "<dataset seriesName='Previous Year' color='F6BD0F'>"

    '    'Iterate through the data 
    '    For i = 0 To UBound(arrData) - 1
    '        'Append <category name='...' /> to strCategories
    '        strCategories = strCategories & "<category name='" & arrData(i, 1) & "' />"
    '        'Add <set value='...' /> to both the datasets
    '        strDataCurr = strDataCurr & "<set value='" & arrData(i, 2) & "' />"
    '        strDataPrev = strDataPrev & "<set value='" & arrData(i, 3) & "' />"
    '    Next

    '    'Close <categories> element
    '    strCategories = strCategories & "</categories>"

    '    'Close <dataset> elements
    '    strDataCurr = strDataCurr & "</dataset>"
    '    strDataPrev = strDataPrev & "</dataset>"

    '    'Assemble the entire XML now
    '    strXML = strXML & strCategories & strDataCurr & strDataPrev & "</graph>"

    '    'Create the chart - MS Column 3D Chart with data contained in strXML
    '    Return FusionCharts.RenderChart("../FusionCharts/FCF_MSColumn2D.swf", "", strXML, "productSales", "600", "300", False, False)

    'End Function



    'Public Function CreateCharts_Main() As String

    '    'This page demonstrates the ease of generating charts using FusionCharts.
    '    'For this chart, we've used a string variable to contain our entire XML data.

    '    'Ideally, you would generate XML data documents at run-time, after interfacing with
    '    'forms or databases etc.Such examples are also present.
    '    'Here, we've kept this example very simple.

    '    'Create an XML data document in a string variable

    '    Dim strXML As String
    '    strXML = ""
    '    strXML = strXML & "<graph caption='Monthly Unit Sales' xAxisName='Month' yAxisName='Units' decimalPrecision='0' formatNumberScale='0'>"
    '    strXML = strXML & "<set name='Jan' value='462' color='AFD8F8' />"
    '    strXML = strXML & "<set name='Feb' value='857' color='F6BD0F' />"
    '    strXML = strXML & "<set name='Mar' value='671' color='8BBA00' />"
    '    strXML = strXML & "<set name='Apr' value='494' color='FF8E46'/>"
    '    strXML = strXML & "<set name='May' value='761' color='008E8E'/>"
    '    strXML = strXML & "<set name='Jun' value='960' color='D64646'/>"
    '    strXML = strXML & "<set name='Jul' value='629' color='8E468E'/>"
    '    strXML = strXML & "<set name='Aug' value='622' color='588526'/>"
    '    strXML = strXML & "<set name='Sep' value='376' color='B3AA00'/>"
    '    strXML = strXML & "<set name='Oct' value='494' color='008ED6'/>"
    '    strXML = strXML & "<set name='Nov' value='761' color='9D080D'/>"
    '    strXML = strXML & "<set name='Dec' value='960' color='A186BE'/>"
    '    strXML = strXML & "</graph>"

    '    'Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    '    Return FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext1", "600", "300", False)

    'End Function
    Public Function CreateCharts_Main() As String
        'In this example, we plot a multi series chart from data contained
        'in an array. The array will have three columns - first one for data label (product)
        'and the next two for data values. The first data value column would store sales information
        'for current year and the second one for previous year.
        'Lets store the sales data for 6 products in our array. We also store
        'the name of products. 
        Dim arrData(6, 4)
        'Store Name of Products
        arrData(0, 0) = "Product A"
        arrData(1, 0) = "Product B"
        arrData(2, 0) = "Product C"
        arrData(3, 0) = "Product D"
        arrData(4, 0) = "Product E"
        arrData(5, 0) = "Product F"
        'Store sales data for current year
        arrData(0, 1) = 567500
        arrData(1, 1) = 815300
        arrData(2, 1) = 76800
        arrData(3, 1) = 734500
        arrData(4, 1) = 676800
        arrData(5, 1) = 648500
        'Store sales data for previous year
        arrData(0, 2) = 547300
        arrData(1, 2) = 584500
        arrData(2, 2) = 0
        arrData(3, 2) = 456300
        arrData(4, 2) = 754500
        arrData(5, 2) = 437600
        arrData(0, 3) = 547300
        arrData(1, 3) = 584500
        arrData(2, 3) = 0
        arrData(3, 3) = 456300
        arrData(4, 3) = 754500
        arrData(5, 3) = 437600
        'Now, we need to convert this data into multi-series XML. 
        'We convert using string concatenation.
        'xmlData - Stores the entire XML
        'strCategories - Stores XML for the <categories> and child <category> elements
        'strDataCurr - Stores XML for current year's sales
        'strDataPrev - Stores XML for previous year's sales
        Dim xmlData As String
        Dim categories As String
        Dim currentYear As String
        Dim previousYear As String
        Dim OtherYear As String
        'Initialize <chart> element
        xmlData = "<graph caption='Group Attendance' numberPrefix='$' yaxisminvalue='0' showhovercap='0' showLegend='0'  zeroPlaneShowBorder='0' decimalPrecision='0' >"
        'Initialize <categories> element - necessary to generate a multi-series chart
        categories = "<categories >"
        'Initiate <dataset> elements
        currentYear = "<dataset seriesName='Current Year' >"
        previousYear = "<dataset seriesName='Previous Year'>"
        OtherYear = "<dataset seriesName='other Year'>"
        'Iterate through the data 
        Dim i As Integer = 0
        Do While (i < arrData.GetUpperBound(0))
            'Append <category name='...' /> to strCategories
            categories = (categories & ("<category name='" _
             & (arrData(i, 0) & "' />")))
            'Add <set value='...' /> to both the datasets
            currentYear = (currentYear & ("<set value='" _
            & (arrData(i, 1) & "' />")))
            previousYear = (previousYear & ("<set value='" _
            & (arrData(i, 2) & "' />")))
            OtherYear = (OtherYear & ("<set value='" _
           & (arrData(i, 3) & "' />")))
            i = (i + 1)
        Loop
        'Close <categories> element
        categories = (categories & "</categories>")
        'Close <dataset> elements
        currentYear = (currentYear & "</dataset>")
        previousYear = (previousYear & "</dataset>")
        OtherYear = (OtherYear & "</dataset>")
        'Assemble the entire XML now
        xmlData = (xmlData _
         & (categories _
          & (currentYear _
          & (previousYear _
          & (OtherYear & "</graph>")))))
        'Create the chart - MS Column 3D Chart with data contained in strXML
        Return InfosoftGlobal.FusionCharts.RenderChart("../FusionCharts/FCF_MSColumn2D.swf", "", xmlData, "productSales", "600", "300", False, False)
    End Function


    'Private Function CreateCharts_Main() As String
    '    'In this example, we show how to connect FusionCharts to a database.
    '    'For the sake of ease, we've used an Access database which is present in
    '    '../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
    '    'other. 

    '    'Database Objects - Initialization
    '    Dim VGA_ID As String = Request.QueryString("vid")
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
    '    Dim param(10) As SqlClient.SqlParameter
    '    param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
    '    param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
    '    param(2) = New SqlClient.SqlParameter("@TODT", TODT)
    '    param(3) = New SqlClient.SqlParameter("@VGA_ID", VGA_ID)

    '    Dim strXML As String, strCategories As String, strDataCurr As String, strDataPrev As String, i As Integer

    '    Dim sct_descr As String = String.Empty
    '    Dim sct_per As String = String.Empty
    '    'strXML will be used to store the entire XML document generated


    '    Dim arr() As String
    '    arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", "e6acef"}
    '    'Dim i As Integer = 0



    '    strXML = ""
    '    'strXML = strXML & "<graph caption='Section Wise' xAxisName='Section' yaxismaxvalue='100' yaxisminvalue='0'  yAxisName='Percent' decimalPrecision='0' formatNumberScale='0' rotateNames='0'>"

    '    'Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART1_DRILL]", param)

    '    '    If readerStudent_Detail.HasRows = True Then
    '    '        While readerStudent_Detail.Read


    '    '            sct_descr = readerStudent_Detail("SCT_DES").ToString
    '    '            sct_per = readerStudent_Detail("PERC").ToString

    '    '            strXML = strXML & "<set name=" + "'" & sct_descr & "' value=" + "'" & sct_per & "' color=" + "'" & arr(i) & "' />"
    '    '            i = i + 1
    '    '        End While
    '    '        'Dim strline As String = "<trendlines><line startvalue='75' displayValue='Good-above 75' color='FF0000' thickness='1' isTrendZone='0'/><line startvalue='50' displayValue='Bad-below 50' color='009999' thickness='1' isTrendZone='0'/></trendlines>"
    '    '        strXML = strXML & "</graph>"
    '    '        ltDrilldown.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "MAINSUBGROUP", "600", "300", False)
    '    '    Else
    '    '        'lblerror.Text = "No Records Found "

    '    '        strXML = ""
    '    '        strXML = strXML & "<graph caption='Attendance Section Wise' xAxisName='Section' yAxisName='Percent' decimalPrecision='0' formatNumberScale='0'>"
    '    '        strXML = strXML & "</graph>"
    '    '        ltDrilldown.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "MAINSUBGROUP", "550", "400", False)


    '    '    End If
    '    'End Using




    '    'Initialize <graph> element
    '    strXML = "<graph caption='Group Attendance' numberPrefix='$' decimalPrecision='0' >"

    '    'Initialize <categories> element - necessary to generate a multi-series chart
    '    'strCategories = "<categories><category name='Grade 01'><dataset seriesName='S1' color='AFD8F8'><set value='50' /></dataset> <dataset seriesName='S2' color='AFD8F8'><set value='60' /></dataset><dataset seriesName='S3' color='AFD8F8'><set value='50' /></dataset></category><category name='Grade 02'><dataset seriesName='S9' color='AFD8F8'><set value='50' /></dataset></category><category name='Grade 03'><dataset seriesName='S39' color='AFD8F8'><set value='70' /></dataset></category><category name='Grade 04'><dataset seriesName='S88' color='AFD8F8'><set value='30' /></dataset></category></categories>"
    '    strCategories = "<categories><category name='Grade 01'><dataset seriesName='S1' color='AFD8F8'><set value='50' /></dataset> <dataset seriesName='S2' color='AFD8F8'><set value='60' /></dataset><dataset seriesName='S3' color='AFD8F8'><set value='50' /></dataset></category></categories>"

    '    'Initiate <dataset> elements
    '    'strDataCurr = "<dataset seriesName='Current Year' color='AFD8F8'><set value='" & arrData(i, 2) & "' />"
    '    'strDataPrev = "<dataset seriesName='Previous Year' color='F6BD0F'>"

    '    ''Iterate through the data 
    '    'For i = 0 To UBound(arrData) - 1
    '    '    'Append <category name='...' /> to strCategories
    '    '    strCategories = strCategories & "<category name='" & arrData(i, 1) & "' />"
    '    '    'Add <set value='...' /> to both the datasets
    '    '    strDataCurr = strDataCurr & "<set value='" & arrData(i, 2) & "' />"
    '    '    strDataPrev = strDataPrev & "<set value='" & arrData(i, 3) & "' />"
    '    'Next

    '    ''Close <categories> element
    '    'strCategories = strCategories & "</categories>"

    '    ''Close <dataset> elements
    '    'strDataCurr = strDataCurr & "</dataset>"
    '    'strDataPrev = strDataPrev & "</dataset>"

    '    'Assemble the entire XML now
    '    strXML = strXML & strCategories & "</graph>"

    '    'Create the chart - MS Column 3D Chart with data contained in strXML
    '    'Return FusionCharts.RenderChart("../FusionCharts/FCF_MSColumn3D.swf", "", strXML, "productSales", "600", "300", False, False)
    '    Return FusionCharts.RenderChart("../FusionCharts/FCF_MSColumn3D.swf", "", strXML, "productSales", "600", "300", False, False)


    'End Function
End Class



