<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studConductCert.aspx.vb" Inherits="Students_studConductCert" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudCondCert.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }






        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }

        function test2(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid2()%>").src = path;
            document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }


        function test3(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid3()%>").src = path;
            document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
        }


        function test4(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid4()%>").src = path;
    document.getElementById("<%=h_selected_menu_4.ClientID %>").value = val + '__' + path;
}



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Conduct Certificate"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False" ValidationGroup="groupM1"
                                HeaderText="You must enter a value in the following fields:" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img" style="height: 30px">
                                    <td align="center" colspan="6" style="height: 1px; width: 500px" valign="middle">
                                        <div align="left">
                                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                    </font>&nbsp;
                                        </div>
                                    </td>
                                </tr>--%>

                                <tr>
                                    <td align="left"><span class="field-label">Academic Year</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Shift</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShift_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Stream</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>

                                    <td align="left">

                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Section</span></td>

                                    <td align="left">

                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr id="OptSub" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Optional Subjects</span></td>

                                    <td align="left" colspan="3">
                                        <asp:CheckBoxList ID="chkbxStream" runat="server" checked="true" AutoPostBack="False">
                                        </asp:CheckBoxList></td>
                                </tr>

                                <tr id="ComSub" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Common Subjects</span></td>

                                    <td align="left" colspan="3">
                                        <asp:CheckBoxList ID="chkbxSubjects" runat="server" checked="true">
                                        </asp:CheckBoxList>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Exam Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtExamDate" runat="server">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvExamID" runat="server" ControlToValidate="txtExamDate" ValidationGroup="groupM1"
                                            CssClass="error" ErrorMessage="Exam Date required." ForeColor="Red">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"><span class="field-label">Result Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtResultDate" runat="server">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvResult" runat="server" ControlToValidate="txtResultDate" ValidationGroup="groupM1"
                                            CssClass="error" ErrorMessage="Result Date required." ForeColor="Red">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Student Character</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCharacter" runat="server">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvCharacter" runat="server" ControlToValidate="txtCharacter" ValidationGroup="groupM1"
                                            CssClass="error" ErrorMessage="Student Character required." ForeColor="Red">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" colspan="2"></td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:GridView ID="gvStudCondCert" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                PageSize="40" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Available">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />

                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                ToolTip="Click here to select/deselect all rows" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />

                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student No">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblStHeader" runat="server" Text="Student No"></asp:Label><br />
                                            <asp:TextBox ID="txtStuNoSearch" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label><br />
                                            <asp:TextBox ID="txtStuNameSearch" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblH12" runat="server" Text="Grade"></asp:Label><br />
                                            <asp:TextBox ID="txtGradeSearch" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblH123" runat="server" Text="Section"></asp:Label><br />
                                            <asp:TextBox ID="txtSectionSearch" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_DOB" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuDOB" runat="server" Text='<%# Bind("STU_DOB") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Shift" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSHFDESCR" runat="server" Text='<%# Bind("SHF_DESCR") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stream" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTMDESCR" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button" TabIndex="4" ValidationGroup="groupM1" /></td>
                    </tr>
                </table>






                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />

                <asp:HiddenField ID="hfSCT_ID" runat="server" />
                <asp:HiddenField ID="hfACY_DESCR" runat="server" />
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfMaxGrade" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <br />
                <asp:HiddenField ID="hfTcTYPE" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>


            </div>
        </div>
    </div>

</asp:Content>


