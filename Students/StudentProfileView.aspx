﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentProfileView.aspx.vb" Inherits="Students_StudentProfileView" EnableEventValidation="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%--  <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />   
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> --%>

    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>




    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../cssfiles/BSUstyles.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" />

    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css" />

    <title></title>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

            .switch input {
                opacity: 0;
                width: 0;
                height: 0;
            }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

            .slider.round:before {
                border-radius: 50%;
            }
    </style>


    <script type="text/javascript">

        function showDocument(filename, contenttype) {

            result = radopen("IFrameNew.aspx?filename=" + filename + "&contenttype=" + contenttype, "popup1");
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function listen_window() { }
        function GetRadWindow() {

            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);

            }
        }


        function OnTreeClick1(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }


        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }


        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }
                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }



    </script>
</head>
<telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
    ReloadOnShow="true" runat="server" EnableShadow="true">
    <Windows>
        <telerik:RadWindow ID="popup1" runat="server" Behaviors="Close,Move"
            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="500px" Height="500px">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

<script>
    function UnCheckAll() {
        var oTree = document.getElementById("<%=tvReport.ClientId %>");
        childChkBoxes = oTree.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = false;
        }

        return true;
    }
</script>
<body onload="listen_window();">
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <table align="center" width="100%" id="trStuProfile" runat="server" visible="false">


            <tr>
                <td class="title-bg">Personal Details </td>
            </tr>
            <tr>
                <td colspan="6" rowspan="4" valign="top">
                    <table width="100%">
                        <tr>
                            <td>
                                <span class="field-label">Date of Birth</span></td>

                            <td>
                                <asp:Literal ID="txtDob" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Place of Birth</span></td>

                            <td>
                                <asp:Literal ID="LTpob" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Country of Birth</span></td>

                            <td>
                                <asp:Literal ID="LTcunbirth" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Nationality</span></td>

                            <td>
                                <asp:Literal ID="LTnationality" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Gender</span></td>

                            <td>
                                <asp:Literal ID="LTgender" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Religion</span></td>

                            <td>
                                <asp:Literal ID="LTreligion" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td colspan="6" class="title-bg">Join Details</td>
                        </tr>


                        <tr>
                            <td>
                                <span class="field-label">Fee ID</span></td>

                            <td>
                                <asp:Literal ID="txtFee_ID" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">MOE Reg#</span></td>

                            <td>
                                <asp:Literal ID="txtMOE_No" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">GEMS USI</span></td>

                            <td>
                                <asp:Literal ID="txtGEMS_USI" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Date of Join(Ministry)</span></td>

                            <td>
                                <asp:Literal ID="txtMINDOJ" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Date of Join</span></td>

                            <td>
                                <asp:Literal ID="txtDOJ" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Join Academic Year</span></td>

                            <td>
                                <asp:Literal ID="txtACD_ID_Join" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Tranfer Type</span></font></td>

                            <td>
                                <asp:Literal ID="TLtftype" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Join Section</span></td>

                            <td>
                                <asp:Literal ID="txtSCT_ID_JOIN" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Join Grade</span></td>

                            <td>
                                <asp:Literal ID="txtGRD_ID_Join" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Join Stream</span></td>

                            <td>

                                <asp:Literal ID="txtJoin_Stream" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Join Shift</span></td>

                            <td>

                                <asp:Literal ID="txtJoin_Shift" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Ministry List Type</span></td>

                            <td>
                                <asp:Literal ID="LTminlistType" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Ministry List</span></td>

                            <td>
                                <asp:Literal ID="ltminlist" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Fee Sponsor</span></td>

                            <td>
                                <asp:Literal ID="txtFee_Spon" runat="server"></asp:Literal></td>
                        </tr>



                        <tr>
                            <td>
                                <span class="field-label">Emergency Contact</span></td>

                            <td>
                                <asp:Literal ID="LTemgcon" runat="server"></asp:Literal></td>
                            <td></td>

                            <td></td>
                        </tr>

                        <tr>
                            <td colspan="6" class="title-bg">Passport/Visa Details</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Passport No</span></td>

                            <td>
                                <asp:Literal ID="txtPNo" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Passport Issue Place</span></td>

                            <td>
                                <asp:Literal ID="txtPIssPlace" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Passport Issue Date</span></td>

                            <td>
                                <asp:Literal ID="txtPIssDate" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Passport Expiry Date</span></td>

                            <td>

                                <asp:Literal ID="txtPExpDate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Visa No</span></td>

                            <td>
                                <asp:Literal ID="txtVNo" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Visa Issue Place</span></td>

                            <td>
                                <asp:Literal ID="txtVIssPlace" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Visa Issue Date</span></td>

                            <td>

                                <asp:Literal ID="txtVIssDate" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Visa Expiry Date</span></td>

                            <td>

                                <asp:Literal ID="txtVExpDate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Issuing Authority</span></td>

                            <td colspan="4">

                                <asp:Literal ID="txtVIssAuth" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Emirates ID</span></td>

                            <td>

                                <asp:Literal ID="ltEmiratesID" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Premises Id</span></td>

                            <td>

                                <asp:Literal ID="ltPremisesId" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td colspan="6" class="title-bg">Language Details </td>
                        </tr>

                        <tr>
                            <td>
                                <span class="field-label">First Language</span></td>

                            <td>
                                <asp:Literal ID="ltFirstLang" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Other Languages</span></td>

                            <td>
                                <asp:Literal ID="ltOthLang" runat="server"></asp:Literal></td>
                        </tr>

                        <tr>
                            <td>
                                <span class="field-label">Proficiency in English</span></td>

                            <td colspan="4">

                                <div style="width: 100%">
                                    <div>
                                        Reading :
                                        <asp:Literal ID="ltProEng_R" runat="server"></asp:Literal>
                                        Writing :
                                        <asp:Literal ID="ltProEng_W" runat="server"></asp:Literal>
                                        Speaking :
                                        <asp:Literal ID="ltProEng_S" runat="server"></asp:Literal>
                                    </div>



                                </div>


                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="title-bg">Other Info  </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Info</span></td>

                            <td colspan="3">

                                <div style="width: 100%">
                                    <div>
                                        <asp:Literal ID="lit_Other_Info" runat="server"></asp:Literal>
                                    </div>

                                </div>


                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Receive SMS</span></td>

                            <td>
                                <asp:Literal ID="ltRecevSMS" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Receive Mail</span></td>

                            <td>
                                <asp:Literal ID="ltRecevEmail" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Publication/promotion photos and videos</span></td>

                            <td>
                                <asp:Literal ID="ltPublicpromotion" runat="server"></asp:Literal></td>
                            <td>
                                <span class="field-label">Comment</span></td>

                            <td>
                                <asp:Literal ID="ltcommentstud" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">House</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlHouse_BSU" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%" id="trParentDetails" runat="server" visible="false">
            <tr>
                <td class="title-bg">Contact Details</td>
            </tr>
            <tr>
                <td colspan="6" valign="top">
                    <table id="table1" runat="server" width="100%" class="table table-bordered table-row">
                        <tr class="title-bg">
                            <td></td>
                            <td><strong>Father</strong></td>
                            <td><strong>Mother</strong></td>
                            <td><strong>Guardian</strong></td>

                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Name</span></td>

                            <td>
                                <asp:Literal ID="Ltl_Fname" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_Mname" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_Gname" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Nationality</span></td>

                            <td>
                                <asp:Literal ID="Ltl_Fnationality" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_Mnationality" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_Gnationality" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Comm.POB</span></td>

                            <td>
                                <asp:Literal ID="Ltl_Fpob" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_Mpob" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_Gpob" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">City/Emirate</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FEmirate" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MEmirate" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GEmirate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Phone Res</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FPhoneRes" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MPhoneRes" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GPhoneRes" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Office Phone</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FOfficePhone" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MOfficePhone" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GOfficePhone" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Mobile</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FMobile" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MMobile" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GMobile" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Email</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FEmail" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MEmail" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GEmail" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Fax</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FFax" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MFax" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GFax" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Occupation</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FOccupation" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MOccupation" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GOccupation" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>

                            <td>
                                <span class="field-label">Company</span></td>

                            <td>
                                <asp:Literal ID="Ltl_FCompany" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_MCompany" runat="server"></asp:Literal></td>
                            <td>
                                <asp:Literal ID="Ltl_GCompany" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table id="tblAttendance" runat="server" visible="false" width="100%">

            <tr class="title-bg">
                <td>Attendance Details</td>
            </tr>
            <tr>
                <td colspan="6" rowspan="4" valign="top">
                    <table id="table2" runat="server" width="100%" class="table table-bordered table-row">
                        <tr>
                            <td>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-2 mt-2"><span class="field-label">Academic Year</span></div>
                                        <div class="col-lg-2">
                                            <asp:DropDownList ID="ddlAttAcd_id" runat="server" AutoPostBack="true" Width="20%"></asp:DropDownList>
                                        </div>
                                        <div class="col-lg-8"></div>
                                    </div>
                                </div>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="ltTitleAcd" runat="server"></asp:Literal></td>

                            <td colspan="2">
                                <asp:Literal ID="ltAcdWorkingDays" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="ltTotTilldate" runat="server"></asp:Literal></td>

                            <td colspan="2">
                                <asp:Literal ID="ltTotWorkTilldate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="ltMrkTilldate" runat="server"></asp:Literal></td>

                            <td colspan="2">
                                <asp:Literal ID="ltAttMarkTilldate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Days Present</span></td>

                            <td colspan="2">
                                <asp:Literal ID="ltDayPresent" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Days Absent</span></td>

                            <td colspan="2">
                                <asp:Literal ID="ltDayAbsent" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Days on Leave</span></td>

                            <td colspan="3">
                                <asp:Literal ID="ltDayLeave" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Percentage of Attendance</span></td>

                            <td colspan="3">
                                <asp:Literal ID="ltPerAtt" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
        <table width="100%" id="tblAttendanceChart" runat="server" visible="false">
            <tr>

                <td align="left" colspan="2" width="100%">
                    <telerik:RadHtmlChart runat="server" ID="radAttendanceChart" Width="900px" Skin="Metro">
                        <PlotArea>
                            <Series>
                                <telerik:ColumnSeries Name="TMONTH" DataFieldY="TOT_ATT">
                                    <TooltipsAppearance />
                                    <LabelsAppearance Visible="false" />
                                </telerik:ColumnSeries>
                            </Series>
                            <XAxis DataLabelsField="TMONTH">
                            </XAxis>
                            <YAxis>
                                <LabelsAppearance />
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="false" />
                        </Legend>
                        <ChartTitle Text="Attendance By Month">
                        </ChartTitle>
                    </telerik:RadHtmlChart>
                </td>

            </tr>
            <tr>
                <td align="left" colspan="2" width="100%">
                    <telerik:RadHtmlChart runat="server" ID="radAttendanceABSChart" Height="250" Skin="Metro">
                        <PlotArea>
                            <Series>
                                <telerik:ColumnSeries Name="TWEEK" DataFieldY="TOT_ABSCOUNT">
                                    <TooltipsAppearance />
                                    <LabelsAppearance Visible="false" />
                                </telerik:ColumnSeries>
                            </Series>
                            <XAxis DataLabelsField="TWEEK">
                            </XAxis>
                            <YAxis MinValue="0" MaxValue="10">
                                <LabelsAppearance Step="1" />
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="false" />
                        </Legend>
                        <ChartTitle Text="Weekly Absent Pattern">
                        </ChartTitle>
                    </telerik:RadHtmlChart>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" width="100%">
                    <telerik:RadHtmlChart runat="server" ID="radAttendanceLateChart" Height="250" Skin="Metro">
                        <PlotArea>
                            <Series>
                                <telerik:ColumnSeries Name="TWEEK" DataFieldY="TOT_LCOUNT">
                                    <TooltipsAppearance />
                                    <LabelsAppearance Visible="false" />
                                </telerik:ColumnSeries>
                            </Series>
                            <XAxis DataLabelsField="TWEEK">
                            </XAxis>
                            <YAxis MinValue="0" MaxValue="10">

                                <LabelsAppearance Step="1" />
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="false" />
                        </Legend>
                        <ChartTitle Text="Weekly Late Pattern">
                        </ChartTitle>
                    </telerik:RadHtmlChart>
                </td>

            </tr>
        </table>

        <table id="tblFeeDetails" runat="server" visible="false" width="100%">

            <tr class="title-bg">
                <td>Fee Details</td>
            </tr>
            <tr>
                <td width="100%">
                    <asp:GridView ID="gvPaymentHist" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                        Width="100%" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="15" OnPageIndexChanging="gvPaymentHist_PageIndexChanging">

                        <Columns>

                            <asp:BoundField DataField="Sl_No" HeaderText="Sl No" ReadOnly="True" />
                            <asp:TemplateField SortExpression="FCL_DATE">
                                <HeaderTemplate>
                                    Rct. Date
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <%--    <asp:Label ID="lblFCL_DATE" runat="server" Text='<%# Bind("FCL_DATE") %>'></asp:Label>--%>
                                    <asp:Label ID="lblFCL_DATE" runat="server" Text='<%# Bind("DOC_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="FCL_DATE">
                                <HeaderTemplate>
                                    Fee Type
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <%--    <asp:Label ID="lblFCL_DATE" runat="server" Text='<%# Bind("FCL_DATE") %>'></asp:Label>--%>
                                    <asp:Label ID="lblFEE" runat="server" Text='<%# Bind("FEE")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField SortExpression="FCL_RECNO">
                                <HeaderTemplate>
                                    Rct No
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>
                                    <%-- <asp:Label ID="lblFCL_RECNO" runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:Label>--%>
                                    <asp:Label ID="lblFCL_RECNO" runat="server" Text='<%# Bind("REF_NO")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField SortExpression="FCL_NARRATION">
                                <HeaderTemplate>
                                    Narration
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblFCL_NARRATION" runat="server" Text='<%# Bind("FCL_NARRATION") %>'></asp:Label>--%>
                                    <asp:Label ID="lblFCL_NARRATION" runat="server" Text='<%# Bind("TRANSACTION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField SortExpression="FCL_AMOUNT">
                                <HeaderTemplate>
                                    Debit
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>

                                    <asp:Label ID="lbl_Debit" runat="server" Text='<%# Bind("DEBIT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="FCL_AMOUNT">
                                <HeaderTemplate>
                                    Credit
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>

                                    <asp:Label ID="lbl_Credit" runat="server" Text='<%# Bind("CREDIT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField SortExpression="FCL_AMOUNT">
                                <HeaderTemplate>
                                    Amount
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblFCL_AMOUNT" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>--%>
                                    <asp:Label ID="lblFCL_AMOUNT" runat="server" Text='<%# Bind("NET_AMOUNT")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_new" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnPayFee" runat="server" Text="Pay Fees" OnClick="btnPayFee_Click" CssClass="button" Visible="false" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                </td>
            </tr>
        </table>

        <table id="tblCurriculum" runat="server" visible="false" width="100%">

            <tr class="title-bg">
                <td>Subject List</td>
            </tr>
            <tr>
                <td colspan="7" rowspan="4" valign="top">
                    <table width="100%">


                        <tr>
                            <td align="left" colspan="7" rowspan="4" valign="top">
                                <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                    Width="100%" CssClass="table table-bordered table-row">

                                    <Columns>

                                        <asp:TemplateField HeaderText="Subject">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Subject">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Subject">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTutor" runat="server" Text='<%# Bind("TEACHERS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="griditem" Height="25px" />
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblCurriculum2" runat="server" visible="false" width="100%">
            <tr class="title-bg">
                <td>Progress Report</td>
            </tr>
            <tr>
                <td colspan="6" rowspan="4" valign="top">
                    <table width="100%">


                        <tr>
                            <td colspan="6" rowspan="4">
                                <asp:GridView ID="gvProgress" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                    Width="100%" CssClass="table table-bordered table-row">

                                    <Columns>

                                        <asp:TemplateField HeaderText="Report Type">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPrinted" OnClick="lnkPrinted_Click" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField Visible="FALSE">
                                            <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRpfId" runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField Visible="FALSE">
                                            <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRSMId" runat="server" Text='<%# Bind("RPF_RSM_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="griditem" Height="25px" />
                                    <SelectedRowStyle CssClass="griditem_hilight" />
                                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblPer" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" class="title-bg">STUDENT PERFORMANCE TRACKING</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                    <table align="center" border="0" cellspacing="0" width="100%">


                        <tr>
                            <td align="center" colspan="6" valign="top" style="height: 212px">&nbsp;
                        <asp:TreeView ID="tvReport" runat="server" BorderStyle="Solid" BorderWidth="1px"
                            ExpandDepth="1" Height="206px" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                            PopulateNodesFromClient="False" ShowCheckBoxes="All" Style="overflow: auto; text-align: left"
                            Width="293px">
                            <ParentNodeStyle Font-Bold="False" />
                            <HoverNodeStyle Font-Underline="True" />
                            <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Underline="False"
                                HorizontalPadding="3px" VerticalPadding="1px" />
                            <NodeStyle HorizontalPadding="5px"
                                NodeSpacing="1px" VerticalPadding="2px" />
                        </asp:TreeView>
                            </td>

                        </tr>
                        <tr>
                            <td align="center" colspan="6">
                                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                    Text="Generate Report" ValidationGroup="groupM1" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table id="tblBehaviour" runat="server" visible="false" width="100%">
            <tr>
                <td>
                    <!--Accordion wrapper-->
                    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                        <!-- Accordion card -->
                        <div class="">

                            <!-- Card header -->
                            <div class="card-header" role="tab" id="headingOne1">
                                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="false"
                                    aria-controls="collapseOne1">
                                    <h10 class="mb-0">
          Add New
        </h10>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" runat="server"
                                data-parent="#accordionEx">
                                <div class="card-body">
                                    <table class="table table-bordered table-row">
                                        <tr>
                                            <th width="20%">Date</th>
                                            <th width="20%">Merit/Demerit</th>
                                            <th width="20%">Category</th>
                                            <th width="5%">Showable</th>
                                            <th width="20%">Comments</th>
                                            <th width="15%">Upload</th>
                                        </tr>
                                        <tr>
                                            <td width="20%">
                                                <asp:TextBox ID="txtincidentdate" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="imgincidentdate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                    OnClientClick="javascript:return false;" />
                                            </td>
                                            <td width="20%">
                                                <asp:DropDownList ID="ddlMerit" runat="server" OnSelectedIndexChanged="ddlMerit_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="Achieves" Value="1" Selected="true"></asp:ListItem>
                                                    <asp:ListItem Text="Merit" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="De-Merit" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="20%">
                                                <asp:DropDownList ID="ddlcat" runat="server"></asp:DropDownList>
                                            </td>
                                            <td width="5%">
                                                <label class="switch">
                                                    <asp:CheckBox ID="chk_bshow" runat="server" Checked="true" />
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td width="20%">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td width="15%">
                                                <asp:UpdatePanel ID="updpnl" runat="server">
                                                    <ContentTemplate>
                                                        <asp:FileUpload ID="upload" runat="server" />
                                                        <asp:Button ID="saveupload" runat="server" Text="Upload" OnClick="saveupload_Click" CssClass="button" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="saveupload" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <asp:LinkButton ID="lnkShowFile" runat="server" Visible="false" Text=""></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="right">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button m-0" OnClick="btnAdd_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- Accordion wrapper -->
                    <asp:Label ID="lblIncMssg" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <%-- <tr class="title-bg">
                <td>Behavioural Merits</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gvStudGrade_POS" runat="server" AllowPaging="True"
                        PageSize="15" EmptyDataText="No Records Available"
                        AutoGenerateColumns="false"
                        CssClass="table table-bordered table-row" BorderStyle="None" BorderWidth="0px" Width="100%"
                        HeaderStyle-HorizontalAlign="Center">
                        <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                        <Columns>
                            <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("BM_ID") %>'></asp:Label>
                                    <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STU_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("BM_ENTRY_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Staff Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("BM_CATEGORYNAME")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label
                                        ID="T12lblview2" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label><asp:LinkButton
                                            ID="lblDetail2" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>

                                    <div id="plR_loc2" runat="server" class="gridMsg" visible='<%# BIND("bShow") %>'>
                                        <asp:Literal ID="ltRemarks2" runat="server" Text='<%# Bind("BM_REPORT_ON_INCIDENT")%>'>
                                        </asp:Literal>
                                    </div>
                                    <%-- &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            Width="35px">View</asp:LinkButton>&nbsp;--%>
            <%--   </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>


                    </asp:GridView>
                </td>
            </tr>
            <tr class="title-bg">
                <td>Behavioural De Merits</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        EmptyDataText="No Records Available"
                        PageSize="15" Width="100%"
                        CssClass="table table-bordered table-row" BorderStyle="None" BorderWidth="0px"
                        HeaderStyle-HorizontalAlign="Center">
                        <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                        <Columns>
                            <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("BM_ID") %>'></asp:Label>
                                    <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STU_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("BM_ENTRY_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Staff Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("BM_CATEGORYNAME")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label
                                        ID="T12lblview3" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label><asp:LinkButton
                                            ID="lblDetail3" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>

                                    <div id="plR_loc3" runat="server" class="gridMsg" visible='<%# BIND("bShow") %>'>
                                        <asp:Literal ID="ltRemarks3" runat="server" Text='<%# Bind("BM_REPORT_ON_INCIDENT")%>'>
                                        </asp:Literal>
                                    </div>
                                    <%--  &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            Width="35px">View</asp:LinkButton>&nbsp;--%>
            <%--  </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </td>
            </tr> --%>
            <tr class="title-bg">
                <td>Merit/De-Merit
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvStudTPT_Merit" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnRowCreated="gvStudTPT_Merit_RowCreated"
                        CssClass="table table-bordered table-row" OnRowEditing="OnRowEditing" OnRowUpdating="OnRowUpdating" OnRowCancelingEdit="OnRowCancelingEdit" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                        PageSize="20" Width="100%" OnRowDataBound="gvStudTPT_Merit_RowDataBound">
                        <Columns>

                            <asp:TemplateField HeaderText="Log Date">
                                <HeaderTemplate>
                                    Log Date<br />
                                    <asp:TextBox ID="txtLogDateMerit" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_LogDateMerit" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_ClickM" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:HiddenField ID="hidid" Value='<%# Bind("MRT_ID")%>' runat="server" />
                                    <asp:Label ID="lblEntry_DateMerit" runat="server" Text='<%# Bind("MRT_ENTRY_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Date">
                                <HeaderTemplate>
                                    Date<br />
                                    <asp:TextBox ID="txtInciDateMerit" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_InciDateMerit" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_ClickM" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblInci_DateMerit" runat="server" Text='<%# Bind("MRT_INCDNT_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Log Date">
                                <HeaderTemplate>
                                    Merit / De-Merit<br />
                                    <asp:TextBox ID="txtMeritMerit" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchMerit" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_ClickM" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblStu_NoMerit" runat="server" Text='<%# Bind("MRT_INCDNT_TYPE") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DdlMeritMerit" runat="server">
                                        <asp:ListItem Enabled="true" Value="Merit"></asp:ListItem>
                                        <asp:ListItem Value="De-merit"></asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                <HeaderTemplate>
                                    Remarks<br />
                                    <asp:TextBox ID="txtRemarksMerit" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_Remarks" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_ClickM" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblStu_NameMerit" runat="server" Text='<%# Bind("MRT_INCDNT_REMARKS") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEditRemarks" runat="server" Width="140"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Show In Parentportal">
                                <ItemTemplate>
                                    <label class="switch">
                                        <asp:CheckBox ID="chk_bshow_merit" runat="server" Checked='<%# Bind("bShow")%>' />
                                        <span class="slider round"></span>
                                    </label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ButtonType="Link" HeaderText="Edit" ShowEditButton="true" />

                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdeleteMerit" OnCommand="lnkdeleteMerit_Command" CommandArgument='<%# Bind("MRT_ID")%>' runat="server" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Attachements">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidDocNameMerit" Value='<%# Bind("MRG_FILENAME")%>' runat="server" />
                                    <asp:ImageButton ID="lnkAttbtnMerit" runat="server" ImageUrl="~/Images/email-attachment.png" Visible="false" />
                                    <%--<asp:LinkButton ID="lnkAttDwnldbtn" runat="server"  Visible="false" text ="Download" OnCommand="lnkAttDwnldbtn_Command" CommandArgument='<%# Bind("MRG_FILENAME")%>'></asp:LinkButton>--%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:UpdatePanel ID="updpnl2" runat="server">
                                        <ContentTemplate>
                                            <asp:FileUpload ID="upload2" runat="server" />
                                            <asp:Button ID="saveupload2" runat="server" Text="Upload" OnClick="saveupload_Click2" CssClass="button" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="saveupload2" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:LinkButton ID="lnkShowFile2" runat="server" Visible="false" Text=""></asp:LinkButton>
                                    <asp:HiddenField ID="hidGrpId2" Value='<%# Bind("MRGID")%>' runat="server" />
                                </EditItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="title-bg">
                <td>Achievements
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvStudTPT" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnRowDataBound="gvStudTPT_RowDataBound"
                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                        PageSize="20" Width="100%">
                        <Columns>

                            <asp:TemplateField HeaderText="Log Date" HeaderStyle-Width="15%">
                                <HeaderTemplate>
                                    Log Date<br />
                                    <asp:TextBox ID="txtLogDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_LogDate" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_Click" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblEntry_Date" runat="server" Text='<%# Bind("MRT_ENTRY_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="15%">
                                <HeaderTemplate>
                                    Date<br />
                                    <asp:TextBox ID="txtInciDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_InciDate" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_Click" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblInci_Date" runat="server" Text='<%# Bind("MRT_INCDNT_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Log Date" HeaderStyle-Width="32.5%">
                                <HeaderTemplate>
                                    Achievement<br />
                                    <asp:TextBox ID="txtMerit" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_Merit" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_Click" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("BM_CATEGORYNAME") %>'></asp:Label>
                                    <asp:DropDownList ID="ddl_Categoryname" runat="server" Visible="false"></asp:DropDownList>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30%"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR" HeaderStyle-Width="32.5%">
                                <HeaderTemplate>
                                    Remarks<br />
                                    <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch_Remarks" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_Click" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("MRT_INCDNT_REMARKS") %>'></asp:Label>
                                    <asp:TextBox ID="txtStu_REMARKS" runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70%"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Show In Parentportal">
                                <ItemTemplate>
                                    <label class="switch">
                                        <asp:CheckBox ID="chk_bshow" runat="server" Checked='<%# IIf(Eval("bShow").ToString().Equals("Y"), False, Eval("bShow"))%>' />
                                        <span class="slider round"></span>
                                    </label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEdit" runat="server" OnCommand="btnEdit_Command" Text="Edit" CommandArgument='<%# Bind("MRT_ID")%>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" OnCommand="lnkdelete_Command" CommandArgument='<%# Bind("MRT_ID")%>' runat="server" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Attachements">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidDocName" Value='<%# Bind("MRG_FILENAME")%>' runat="server" />
                                    <asp:ImageButton ID="lnkAttbtn" runat="server" ImageUrl="~/Images/email-attachment.png" Visible="false" />
                                    <div id="divUpload" runat="server" visible="false">
                                        <asp:UpdatePanel ID="updpnl1" runat="server">
                                            <ContentTemplate>
                                                <asp:FileUpload ID="upload1" runat="server" />
                                                <asp:Button ID="saveupload1" runat="server" Text="Upload" OnClick="saveupload_Click1" CssClass="button" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="saveupload1" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:LinkButton ID="lnkShowFile1" runat="server" Visible="false" Text=""></asp:LinkButton>
                                        <asp:HiddenField ID="hidGrpId" Value='<%# Bind("MRGID")%>' runat="server" />
                                    </div>
                                    <%--<asp:LinkButton ID="lnkAttDwnldbtn" runat="server"  Visible="false" text ="Download" OnCommand="lnkAttDwnldbtn_Command" CommandArgument='<%# Bind("MRG_FILENAME")%>'></asp:LinkButton>--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="title-bg">
                <td>Behavior Details
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="grdBehavior" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                        PageSize="20" Width="100%">
                        <Columns>

                            <asp:TemplateField HeaderText="Entry Date" HeaderStyle-Width="15%">

                                <ItemTemplate>
                                    <asp:Label ID="lblEntry_Date" runat="server" Text='<%# Bind("Entry_date", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Incident Date" HeaderStyle-Width="15%">

                                <ItemTemplate>
                                    <asp:Label ID="lblInci_Date" runat="server" Text='<%# Bind("Incident_Date", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Behavior Remarks" HeaderStyle-Width="32.5%">

                                <ItemTemplate>
                                    <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("REMARKS")%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30%"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Reporting Staff" SortExpression="DESCR" HeaderStyle-Width="32.5%">
                                <ItemTemplate>
                                    <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("Reporting_To")%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70%"></ItemStyle>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:Label ID="lblCat_Name" runat="server" Text='<%# Bind("Category")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
        </table>


        <table width="100%" id="tblMed" runat="server" visible="false">


            <tr class="title-bg">
                <td colspan="6">Health Details</td>
            </tr>
            <tr>
                <td>
                    <span class="field-label">Health Card No / Medical Insurance No</span></td>

                <td>
                    <asp:Literal ID="ltHth_No" runat="server"></asp:Literal>&nbsp;</td>
                <td>
                    <span class="field-label">Blood Group</span></td>

                <td>
                    <asp:Literal ID="ltB_grp" runat="server"></asp:Literal>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6" class="title-bg">Health Restriction</td>
            </tr>
            <tr>
                <td colspan="6">
                    <span class="field-label">Does your child have any allergies? &nbsp;</span><asp:Literal ID="ltAlg_Detail"
                        runat="server" EnableViewState="False"></asp:Literal>
                    <div id="divAlg" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <span class="field-label">Does your child have any prescribed Special Medication? </span>
                    <asp:Literal ID="ltSp_med" runat="server"></asp:Literal>
                    <div id="divSp_med" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>


            <tr>
                <td colspan="6">
                    <span class="field-label">Is there any Physical Education Restrictions for your child?  </span>
                    <asp:Literal ID="ltPhy_edu" runat="server"></asp:Literal>&nbsp;
         <div id="divPhy_edu" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="6">
                    <span class="field-label">Any other information related to health issue of your child the school should be aware of?</span>
                    <asp:Literal ID="ltHth_Info" runat="server"></asp:Literal>&nbsp;
        <div id="divHth_Info" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="6" class="title-bg">
                    <span class="field-label">Applicant's disciplinary, social, physical or psychological detail</span></td>
            </tr>

            <tr>
                <td colspan="6">
                    <span class="field-label">Has the child received any sort of learning support or theraphy?</span>
                    <asp:Literal ID="ltLS_Ther" runat="server"></asp:Literal>&nbsp;
        <div id="divLS_Ther" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="6">
                    <span class="field-label">Does the child require any special education needs?</span>
                    <asp:Literal ID="ltSEN" runat="server"></asp:Literal>&nbsp;
         <div id="divSEN" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="6">
                    <span class="field-label">Does the student require English support as Additional Language program (EAL) ?</span>
                    <asp:Literal ID="ltEAL" runat="server"></asp:Literal>&nbsp;
          <div id="divEAL" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="6">
                    <span class="field-label">Has your child's behaviour been any cause for concern in previous schools ?</span>
                    <asp:Literal ID="ltPrev_sch" runat="server"></asp:Literal>
                    <div id="divPrev_sch" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                </td>

            </tr>



        </table>


        <table width="100%" id="tbllibrary1" runat="server" visible="false">
            <tr class="title-bg">
                <td colspan="6">Library Membership(s)</td>
            </tr>
            <tr>
                <td colspan="6">


                    <asp:GridView ID="GridMemberships" CssClass="table table-bordered table-row"
                        runat="server" Width="100%" EmptyDataText="Membership not assigned for this user." AutoGenerateColumns="false">
                        <Columns>

                            <asp:TemplateField HeaderText="Library Divisions">
                                <HeaderTemplate>
                                    Library Divisions
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Library Code">
                                <HeaderTemplate>
                                    Library Membership
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <%#Eval("MEMBERSHIP_DES")%>
                                        </center>
                                </ItemTemplate>

                            </asp:TemplateField>



                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <EmptyDataRowStyle />
                        <SelectedRowStyle />
                        <HeaderStyle />
                        <EditRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>



                </td>
            </tr>
        </table>


        <table width="100%" id="tbllibrary2" runat="server" visible="false">
            <tr class="title-bg">
                <td colspan="6">Library Usage</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table align="center" border="0" cellspacing="0" width="100%">


                        <tr>
                            <td align="left" colspan="6" rowspan="4" valign="top">
                                <br />
                                <asp:GridView ID="GrdUserTransaction" runat="server" AllowPaging="True" EmptyDataText="No transactions done yet." AutoGenerateColumns="false"
                                    CssClass="table table-bordered table-row" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Stock ID">
                                            <HeaderTemplate>
                                                Accession No
                                                           
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                        <%#Eval("ACCESSION_NO")%>
                                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item Image">
                                            <HeaderTemplate>
                                                Item Image
                                                           
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                        <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item Name">
                                            <HeaderTemplate>
                                                Item Name
                                                           
                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <%#Eval("ITEM_TITLE")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Library">
                                            <HeaderTemplate>
                                                Library
                                                          
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                        <%#Eval("LIBRARY_DIVISION_DES")%>
                                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Issue Period">
                                            <HeaderTemplate>
                                                Issue Date
                                                           
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center><%#Eval("ITEM_TAKEN_DATE")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tentative Period">
                                            <HeaderTemplate>
                                                Tentative Date
                                                            
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center><%#Eval("ITEM_RETURN_DATE")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Return Date">
                                            <HeaderTemplate>
                                                Return Date
                                                         
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center><%#Eval("RTN_DATE")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Fine">
                                            <HeaderTemplate>
                                                Fine
                                                           
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                        <%#Eval("FINE_AMOUNT") %>
                                                        <%#Eval("CURRENCY_ID") %>
                                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <EmptyDataRowStyle />
                                    <SelectedRowStyle />
                                    <HeaderStyle />
                                    <EditRowStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


        <table width="100%" id="tbllibrary3" runat="server" visible="false">
            <tr class="title-bg">
                <td colspan="6">Item(s) Due</td>
            </tr>
            <tr>
                <td colspan="6">

                    <br />

                    <asp:GridView ID="GridDue" runat="server" AllowPaging="True" EmptyDataText="No Items on Due." AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Stock ID">
                                <HeaderTemplate>
                                    Accession No
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <%#Eval("ACCESSION_NO")%>
                                        </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Image">
                                <HeaderTemplate>
                                    Item Image
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                                        </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name">
                                <HeaderTemplate>
                                    Item Name
                                               
                                </HeaderTemplate>
                                <ItemTemplate>

                                    <%#Eval("ITEM_TITLE")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Library">
                                <HeaderTemplate>
                                    Library
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Issue Period">
                                <HeaderTemplate>
                                    Issue Date
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center><%#Eval("ITEM_TAKEN_DATE")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tentative Period">
                                <HeaderTemplate>
                                    Tentative Date
                                              
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center><%#Eval("ITEM_RETURN_DATE")%> </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <EmptyDataRowStyle />
                        <SelectedRowStyle />
                        <HeaderStyle />
                        <EditRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>

                    <br />

                </td>
            </tr>
        </table>



        <table width="100%" id="tbllibrary4" runat="server" visible="false">
            <tr class="title-bg">
                <td colspan="6">Reservations</td>
            </tr>
            <tr>
                <td colspan="6">

                    <br />

                    <asp:GridView ID="GridReservations" runat="server" AllowPaging="True" EmptyDataText="No Items Reserved." AutoGenerateColumns="false"
                        CssClass="table table-bordered table-row" Width="100%">
                        <Columns>

                            <asp:TemplateField HeaderText="Item Image">
                                <HeaderTemplate>
                                    Item Image
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                                        </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name">
                                <HeaderTemplate>
                                    Item Name
                                              
                                </HeaderTemplate>
                                <ItemTemplate>

                                    <%#Eval("ITEM_TITLE")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Library">
                                <HeaderTemplate>
                                    Library
                                              
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Reserved Date">
                                <HeaderTemplate>
                                    Reserved Date
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center><%#Eval("ENTRY_DATE")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Period">
                                <HeaderTemplate>
                                    Period Reserved
                                               
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center><%#Eval("RESERVE_START_DATE")%> - <%#Eval("RESERVE_END_DATE")%> </center>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                    Status
                                              
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center><%#Eval("CANCEL_RESERVATION")%> </center>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <EmptyDataRowStyle />
                        <SelectedRowStyle />
                        <HeaderStyle />
                        <EditRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>

                    <br />

                </td>
            </tr>
        </table>


        <table width="100%" id="tblcommunication1" runat="server" visible="false">
            <tr class="title-bg">
                <td colspan="6">SMS Sent
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" Width="100%" runat="server" CssClass="table table-bordered table-row"
                        AllowPaging="True" EmptyDataText="No SMS sent"
                        OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <HeaderTemplate>
                                    ID
                                   
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("LOG_CMS_ID")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sms Text">
                                <HeaderTemplate>
                                    Sms Text
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                    <asp:Panel ID="T12Panel1" runat="server">
                                        <%#Eval("cms_sms_text")%>
                                    </asp:Panel>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                        ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                        TextLabelID="T12lblview">
                                    </ajaxToolkit:CollapsiblePanelExtender>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Mobile Number">
                                <HeaderTemplate>
                                    Mobile Number
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("LOG_MOBILE_NUMBER")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Sent Date">
                                <HeaderTemplate>
                                    Sent Date
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                    Status
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("status")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <EmptyDataRowStyle />
                        <EditRowStyle />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgincidentdate"
            TargetControlID="txtincidentdate">
        </ajaxToolkit:CalendarExtender>
        <table width="100%" id="tblcommunication2" runat="server" visible="false">
            <tr class="title-bg">
                <td colspan="6">Newsletter and Plain Text Emails
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:GridView ID="GridEmails" AutoGenerateColumns="false" Width="100%" runat="server" CssClass="table table-bordered table-row"
                        AllowPaging="True" EmptyDataText="No Emails Sent"
                        PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <HeaderTemplate>
                                    ID
                                   
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("LOG_EML_ID")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sms Text">
                                <HeaderTemplate>
                                    Email Title
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("EML_TITLE")%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Type">
                                <HeaderTemplate>
                                    Type
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("EmailType")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Email ID">
                                <HeaderTemplate>
                                    Email ID
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("LOG_EMAIL_ID")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Sent Date">
                                <HeaderTemplate>
                                    Sent Date
                                  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                    Status
                                   
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <%#Eval("status")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <EmptyDataRowStyle />
                        <EditRowStyle />
                    </asp:GridView>

                </td>
            </tr>
        </table>
        <asp:HiddenField ID="HF_stuid" runat="server" />
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfCBSESchool" runat="server" />
        <asp:HiddenField ID="h_remark_edit" runat="server" Value="" />


        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
        </CR:CrystalReportSource>
    </form>
</body>
</html>
