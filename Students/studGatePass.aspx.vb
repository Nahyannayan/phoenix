﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.IO
Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Drawing
Imports System.Drawing.Printing
Partial Class Students_studGatePass
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Dim STR As String = Encr_decrData.Decrypt("wr92h2HbWq4=")

        If Page.IsPostBack = False Then

            setMetaTag()

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S101303") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    txtStudNo.Focus()
                    ddlreason.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    txtTime.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    txtdesc.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    hrefReason.Attributes.Add("class", "cssStepBtnActive")
                    hrefHistory.Attributes.Add("class", "cssStepBtnInActive")
                    
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try


        Else


        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSav)



    End Sub
    Sub setMetaTag()
        Dim htMeta As New HtmlMeta()
        htMeta.HttpEquiv = "X-UA-Compatible"
        htMeta.Content = "IE=Edge,chrome=1"
        Me.Page.Header.Controls.Add(htMeta)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub txtStudNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStudNo.TextChanged
        lblError.Text = ""
        If txtStudNo.Text <> "" Then
            divLate.Visible = True
            divReason.Visible = True
            trdets.Visible = True
            Displayitems()
            bindReasons()
            BIND_TIME()
            check_safeword()
            'ddlreason.Focus()
            ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowLeaveDetails", "setfocus();", True)
            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "satrtup", "setfocus();")
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")
            smScriptManager.SetFocus(ddlreason)


        Else
            divLate.Visible = False
            divReason.Visible = False
            trdets.Visible = False
        End If

    End Sub

    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblError.Text = ""
        If txtStudNo.Text <> "" Then
            divLate.Visible = True
            divReason.Visible = True
            trdets.Visible = True
            Displayitems()
            bindReasons()
            BIND_TIME()
            check_safeword()
            'ddlreason.Focus()
            ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowLeaveDetails", "setfocus();", True)
            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "satrtup", "setfocus();")
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")
            smScriptManager.SetFocus(ddlreason)


        Else
            divLate.Visible = False
            divReason.Visible = False
            trdets.Visible = False
        End If
    End Sub

    Public Sub Displayitems()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@TYPE", "STUD")
            param(1) = New SqlParameter("@STU_NO", txtStudNo.Text)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_GATEPASS", param)


            If ds.Tables(0).Rows.Count >= 1 Then

                lblStudname.Text = ds.Tables(0).Rows(0).Item("STU_NAME")
                lblAcyear.Text = ds.Tables(0).Rows(0).Item("ACY_DESCR")
                lblGrade.Text = ds.Tables(0).Rows(0).Item("GRM_DISPLAY")
                lblParentEmail.Text = ds.Tables(0).Rows(0).Item("FATHER_EMAIL")
                lblParentMob.Text = ds.Tables(0).Rows(0).Item("FATHER_MOBILE")
                lblParentName.Text = ds.Tables(0).Rows(0).Item("FATHER")
                imgStud.ImageUrl = ds.Tables(0).Rows(0).Item("STU_PHOTOPATH")
                ViewState("STU_ID") = ds.Tables(0).Rows(0).Item("STU_ID")
                ViewState("GRD_ID") = ds.Tables(0).Rows(0).Item("GRD_ID")
                ViewState("SCT_ID") = ds.Tables(0).Rows(0).Item("SCT_ID")
                ViewState("ACD_ID") = ds.Tables(0).Rows(0).Item("ACD_ID")

                lblPickupBus.Text = ds.Tables(0).Rows(0).Item("PICKUPBUS")
                lblDropBus.Text = ds.Tables(0).Rows(0).Item("DROPBUS")
                lblPickupArea.Text = ds.Tables(0).Rows(0).Item("PICKUPAREA")
                lblDropArea.Text = ds.Tables(0).Rows(0).Item("DROPAREA")
                lblPickupPoint.Text = ds.Tables(0).Rows(0).Item("PICKUPPOINT")
                lblDropPoint.Text = ds.Tables(0).Rows(0).Item("DROPPOINT")

                If ds.Tables(0).Rows(0).Item("CON_bCONTRACT") = "True" Then
                    cbAlert.Checked = True
                    rd1.Items(0).Selected = True
                    cbAlert.Enabled = False
                    rd1.Enabled = False
                Else
                    cbAlert.Checked = False
                    rd1.Items(0).Selected = False
                    cbAlert.Enabled = True
                    rd1.Enabled = True
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub

    Protected Sub hrefReason_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles hrefReason.Click
        hrefReason.Attributes.Add("class", "cssStepBtnActive")
        hrefHistory.Attributes.Add("class", "cssStepBtnInActive")

        divReason.Visible = True
        divHistory.Visible = False
    End Sub

    Protected Sub hrefHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles hrefHistory.Click

        hrefReason.Attributes.Add("class", "cssStepBtnInActive")
        hrefHistory.Attributes.Add("class", "cssStepBtnActive")

        divReason.Visible = False
        divHistory.Visible = True
        gridbind()
    End Sub

    Public Sub bindReasons()
        ddlreason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT GPR_ID,GPR_DESCR  FROM STU.STU_GATEPASS_REASONS"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlreason.DataSource = ds
        ddlreason.DataTextField = "GPR_DESCR"
        ddlreason.DataValueField = "GPR_ID"
        ddlreason.DataBind()
        If (Not ddlreason.Items Is Nothing) AndAlso (ddlreason.Items.Count > 1) Then
            ddlreason.Items.Add(New ListItem("--", "0"))
            ddlreason.Items.FindByText("--").Selected = True
        End If
    End Sub

    Protected Sub lnkp_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)
        print_pass(i)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "CallPrint", "CallPrint();", True)
        'CallReport(i)
        ' Response.Write("<Script> window.open('studLatePrint.aspx?SLS_ID=" & i & "') </Script>")

    End Sub




    Protected Sub gvLate_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLate.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblAlert As Label
            Dim lnkp As LinkButton = e.Row.FindControl("lnkp")
            lblAlert = e.Row.FindControl("lblAlert")
            If lblAlert.Text = "True" Then
                Dim ib As ImageButton = e.Row.Cells(6).Controls(0)
                ib.ImageUrl = "~\images\tick.gif"
            Else
                Dim ib As ImageButton = e.Row.Cells(6).Controls(0)
                ib.ImageUrl = "~\images\cross.gif"


            End If
            Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
            ScriptManager1.RegisterPostBackControl(lnkp)

        End If
    End Sub
    Sub gridbind()


        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@TYPE", "DETAIL")
            param(1) = New SqlParameter("@STU_NO", txtStudNo.Text)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_GATEPASS", param)

            gvLate.DataSource = ds
            gvLate.DataBind()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub


    Protected Sub btnSav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSav.Click


        'If cbAlert.Checked = False Then
        '    check_safeword()
        '    If ViewState("IsSAFEWORD") <> "1" Then
        '        lblError.Text = "Safe Word is not matching.. Please check "
        '        Exit Sub
        '    Else
        '        rd1.Items(0).Selected = True
        '    End If

        'End If



        If rd1.Items(0).Selected = True Then


            Dim Status As Integer
            Dim param(14) As SqlClient.SqlParameter
            Dim transaction As SqlTransaction
            Dim sls_id As Integer

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try


                    param(0) = New SqlParameter("@GPS_STU_ID", ViewState("STU_ID"))
                    param(1) = New SqlParameter("@GPS_ACD_ID", ViewState("ACD_ID"))
                    param(2) = New SqlParameter("@GPS_GRD_ID", ViewState("GRD_ID"))
                    param(3) = New SqlParameter("@GPS_SCT_ID", ViewState("SCT_ID"))
                    param(4) = New SqlParameter("@GPS_GPR_ID", ddlreason.SelectedValue)
                    param(5) = New SqlParameter("@GPS_DESCR", txtdesc.Text)
                    param(6) = New SqlParameter("@GPS_bCONTRACT", IIf(rd1.Items(0).Selected, 1, 0))
                    param(7) = New SqlParameter("@GPS_bALERT", cbAlert.Checked)
                    param(8) = New SqlParameter("@GPS_TIME", txtTime.Text)

                    param(9) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                    param(9).Direction = ParameterDirection.ReturnValue
                    param(10) = New SqlClient.SqlParameter("@GPS_ID", SqlDbType.VarChar, 200)
                    param(10).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVE_STU_GATEPASS", param)
                    Status = param(9).Value

                    sls_id = param(10).Value



                Catch ex As Exception

                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Finally
                    If Status <> 0 Then
                        ' UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        transaction.Rollback()
                        lblError.Text = "Do not allow adding more than one with the same date & time "
                    Else
                        lblError.Text = "Saved Successfully."
                        transaction.Commit()



                        'CallReport(sls_id)
                        ' Response.Write("<Script> window.open('studLatePrint.aspx?ID=" & sls_id & "&TYPE=1') </Script>")

                        If rd1.Items(0).Selected = True Then
                            ViewState("smsText") = "Your student " + lblStudname.Text + " left the school at " + String.Format("{0:dd/MMM/yyyy}", Now.Date) + " " + txtTime.Text + " for " + ddlreason.SelectedItem.Text
                            print_pass(sls_id)
                            ' callSend_SMS_ALERT(sls_id)
                        End If
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "CallPrint", "CallPrint();", True)

                        ' fill_div(sls_id)
                        ' Create_file(sls_id)
                        clear()
                        txtStudNo.Text = ""
                        txtStudNo.Focus()
                    End If
                End Try

            End Using
        End If
    End Sub
    Sub clear()
        ddlreason.SelectedValue = "0"
        txtdesc.Text = ""
        cbAlert.Checked = False
        txtSafeword.Text = ""
    End Sub
    Private Sub callSend_SMS_ALERT(ByVal GPS_ID As Integer)
        Try
            Dim status As String
            Dim password As String = ConfigurationManager.AppSettings("smspwd").ToString
            Dim usrname As String = ConfigurationManager.AppSettings("smsUsername").ToString

            lblParentMob.Text = "971" + Right(Convert.ToString(lblParentMob.Text).Replace("-", ""), 9) '"971566175152"

            status = SmsService.sms.SendMessage(lblParentMob.Text, ViewState("smsText"), "5124", usrname, password)
            SaveLog(GPS_ID, ViewState("smsText"), status)



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub
    Private Sub SaveLog(ByVal GPS_ID As String, ByVal MSG As String, ByVal STATUS As String)
        Try

            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@GPS_ID", GPS_ID)
            pParms(1) = New SqlClient.SqlParameter("@MOBNO", lblParentMob.Text)
            pParms(2) = New SqlClient.SqlParameter("@MSG", MSG)
            pParms(3) = New SqlClient.SqlParameter("@STATUS", STATUS)
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "STU.SAVESMS_LOG", pParms)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub

    Public Sub print_pass(ByVal id As Integer)
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim strformat As String = ""

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@GPS_ID", id)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_STU_GATEPASS_PRINT", param)
        If ds.Tables(0).Rows.Count >= 1 Then
            strformat = "<div style='border:1 solid;  height:200px;width:400px;'> " _
                        & " <div style='color:white;font-size:14px;font-weight:bold;'>GATE PASS</div><br> " _
                        & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("GPS_TIME") + " , " + ds.Tables(0).Rows(0).Item("GPS_DATE") + "</div><br> " _
                        & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("DESCR") + "</div><br><br> " _
                         & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("STU_NAME") + "</div><br> " _
                       & " <div style='color:white;'>" + ds.Tables(0).Rows(0).Item("GRADE") + "</div> " _
                      & " </div>"


        End If
        divPrint.InnerHtml = strformat
    End Sub



    Public Sub BIND_TIME()
        Try


            Dim d As DateTime
            d = Now()
            txtTime.Text = d.ToString("hh:mm tt")



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub
    Public Sub check_safeword()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", ViewState("STU_ID"))
        param(1) = New SqlParameter("@TEXT", txtSafeword.Text)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_SAFEWORD", param)


        If ds.Tables(0).Rows.Count >= 1 Then
            ViewState("IsSAFEWORD") = ds.Tables(0).Rows(0).Item("VALID")
            txtSafeword.Text = ds.Tables(0).Rows(0).Item("SAFE_WORD")
        End If

    End Sub


    Protected Sub ddlreason_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlreason.SelectedIndexChanged
        ddlreason.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
        btnSav.Focus()
        btnSav.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
    End Sub

    Protected Sub cbAlert_CheckedChanged(sender As Object, e As EventArgs) Handles cbAlert.CheckedChanged
        If cbAlert.Checked Then
            divsafe.Visible = False
            divsafe1.Visible = False

            rd1.Items(0).Selected = True
        Else
            divsafe.Visible = True
            divsafe1.Visible = True
            check_safeword()
            rd1.Items(0).Selected = False
            rd1.Items(1).Selected = False
        End If
    End Sub
End Class
