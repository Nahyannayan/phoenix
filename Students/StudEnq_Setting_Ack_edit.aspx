<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"  ValidateRequest="false"
CodeFile="StudEnq_Setting_Ack_edit.aspx.vb" Inherits="Students_StudEnq_Setting_Ack_edit" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 
    <script language="javascript" type="text/javascript">
    
        
    function ShowOthGrade(rowState)
    {
   
	if (rowState==1)
	{
  styleObj1=document.getElementById(21).style;
	 styleObj1.display='';
	 	 }
	 else
	{
	  styleObj1=document.getElementById(21).style;
	styleObj1.display='none';
	 
		}

       }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal id="ltHeader" runat="server" Text="Enquiry Acknowledgement Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    
    <table id="tbl_AddGroup" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
               
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" ValidationGroup="List_valid">
                        </asp:ValidationSummary></div>
                
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory</td>
        </tr>
        <tr>
            <td align="left">
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    
                    <tr>
                        <td align="left" width="25%">
                            <span class="field-label">Academic Year</span><span class="text-danger">*</span></td>
                        
                        <td align="left" width="40%">
                            <asp:DropDownList id="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Grade</span><span class="text-danger">*</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlGrade" runat="server" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr id="21" style="display: none">
                        <td align="left">
                            <span class="field-label">Copy to Grade(s)</span></td>
                        
                        <td align="left" colspan="3">
                            <asp:CheckBoxList id="chkOthGrade" runat="server" CellPadding="8" CellSpacing="2" RepeatColumns="6" RepeatDirection="Horizontal">
                            </asp:CheckBoxList></td>
                       
                    </tr>
                    <tr class="gridheader_pop">
                        <td align="left"  colspan="4" class="title-bg">
                            Message
                            <span  style="float:right;">
                                   <asp:CheckBox ID="chkMessage" runat="server" style="clear: left" Text="Show"  />
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left" style="height: 21px">
                            <span class="field-label">Matter</span><span class="text-danger">*</span></td>
                        
                        <td align="left">
                            <asp:TextBox id="txtMessage" runat="server"  TextMode="MultiLine" Width="315px" SkinID="MultiText"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                     <tr class="gridheader_pop">
                        <td align="left" colspan="4" class="title-bg">
                             List of Documents to show in the Enquiry Acknowledgement
                            <span  style="float:right;">
                                   <asp:CheckBox ID="chkDoc"  CssClass="field-label" runat="server" style="clear: left" Text="Show" /></span></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Show list of Documents</span></td>
                        
                        <td align="left">
                            <asp:CheckBox id="chkBulletPoints" CssClass="field-label" runat ="server" Text="Bullet Points">
                            </asp:CheckBox></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Matter</span><span class="text-danger">*</span></td>
                        
                        <td align="left">
                            <asp:TextBox id="txtDoc" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Display Order</span><span class="text-danger">*</span></td>
                        
                        <td align="left">
                            <asp:TextBox id="txtNo" runat="server" MaxLength="2"></asp:TextBox>&nbsp;
                        </td>
                        <td align="left" colspan="2">
                            <asp:Button id="btnAddDoc" runat="server" CssClass="button" Text="Add" OnClick="btnAddDoc_Click" ValidationGroup="List_valid" />&nbsp;
                            <asp:Button id="btnUpdate" runat="server" CssClass="button" Text="Update" OnClick="btnUpdate_Click" ValidationGroup="List_valid" />
                            <asp:Button id="btnCancelDoc" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancelDoc_Click" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:GridView id="gvDoc" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" 
                                DataKeyNames="id" EmptyDataText="No document added yet" 
                                 OnRowDeleting="gvDoc_RowDeleting" Width="100%" OnRowEditing="gvDoc_RowEditing">
                                <rowstyle cssclass="griditem" height="23px" />
                                <emptydatarowstyle cssclass="gridheader" horizontalalign="Center" wrap="True" />
                                <columns>
<asp:TemplateField HeaderText="Id" Visible="False"><ItemTemplate>
<asp:Label id="lblId" runat="server" Text='<%# Bind("id") %>' __designer:wfdid="w80"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DIS_ORDER" HeaderText="Display Order"></asp:BoundField>
<asp:BoundField DataField="DOCLIST" HeaderText="Document Required"></asp:BoundField>
<asp:TemplateField HeaderText="Edit"><ItemTemplate>
<asp:LinkButton id="EditBtn" runat="server" CausesValidation="False" __designer:wfdid="w79" CommandName="Edit" CommandArgument='<%# Eval("id") %>'>           Edit </asp:LinkButton> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete"><ItemTemplate>
<asp:LinkButton id="DeleteBtn" runat="server" __designer:wfdid="w65" CommandName="Delete" CommandArgument='<%# Eval("id") %>'>
         Delete</asp:LinkButton> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
</asp:TemplateField>
</columns>
                                <selectedrowstyle backcolor="Khaki" />
                                <headerstyle cssclass="gridheader_new" height="25px" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" /><asp:Button id="btnEdit" runat="server" CausesValidation="False"
                        CssClass="button" onclick="btnEdit_Click" Text="Edit" /><asp:Button id="btnSave"
                            runat="server" CssClass="button" onclick="btnSave_Click" Text="Save" ValidationGroup="groupM1"  /><asp:Button
                                id="btnCancel" runat="server" CausesValidation="False" CssClass="button" onclick="btnCancel_Click"
                                Text="Cancel" /><asp:Button id="btnDelete" runat="server" CausesValidation="False"
                                    CssClass="button" onclick="btnDelete_Click" Text="Delete" />
            </td>
        </tr>
    </table>

                </div>
            </div>
        </div>

</asp:Content>

