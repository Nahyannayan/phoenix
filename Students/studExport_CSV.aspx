<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studExport_CSV.aspx.vb" Inherits="studExport_CSV" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkControl") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }




        function Grade_chk_state() {
            var chk_state = document.getElementById('<%=ddlGradesAll.ClientID %>').checked;
            var tableBody = document.getElementById('<%=ddlGrdaes.ClientID %>').childNodes[0];
            for (var i = 0; i < tableBody.childNodes.length; i++) {
                var currentTd = tableBody.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = chk_state;

            }
        }


        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 1:
                        document.getElementById('<%=txtAcdDate_From.ClientID %>').value = result;
                          break;
                      case 2:
                          document.getElementById('<%=txtAcdDate_To.ClientID %>').value = result;
                      break;
                  case 3:
                      document.getElementById('<%=txtTC_SO_Cutoff.ClientID %>').value = result;
                       break;
               }
           }
           return false;
       }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Student Export
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <%--  <td align="left" >
                            Regular/Observer</td>
                        <td  style="width: 7px; color: #1b80b6">
                            :</td>
                        <td align="left"  colspan="10">
                           <asp:DropDownList ID="ddlMinList" runat="server"  Width="108px">
                            <asp:ListItem Value="ALL">ALL</asp:ListItem>   
                             <asp:ListItem Value="REGULAR">REGULAR</asp:ListItem>     
                             <asp:ListItem Value="OBSERVER">OBSERVER</asp:ListItem>                                                   
                                                      
                          </asp:DropDownList>       
                            
                            
                            </td>--%>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>

                                    <td align="left" width="30%">
                                        <%--<asp:DropDownList ID="ddlGrade" runat="server" Style="position: relative" AutoPostBack="True">
                            </asp:DropDownList>--%>

                                        <div align="left">
                                            <asp:CheckBox ID="ddlGradesAll" runat="server"
                                                onclick="javascript:Grade_chk_state();" CssClass="field-label"
                                                Text="Select All" />
                                            <div class="checkbox-list">
                                                <asp:CheckBoxList ID="ddlGrdaes" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>

                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">DOJ From</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAcdDate_From" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgShowDate_From" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgShowDate_From" TargetControlID="txtAcdDate_From">
                                        </ajaxToolkit:CalendarExtender>
                                        <br />
                                        <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">DOJ To</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAcdDate_To" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgShowDate_To" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(550, 310, 2)" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgShowDate_To" TargetControlID="txtAcdDate_To">
                                        </ajaxToolkit:CalendarExtender>
                                        <br />
                                        <span style="font-size: 7pt">(dd/mmm/yyyy)</span>


                                    </td>
                                </tr>


                                <tr runat="server" id="tr_TCSO">
                                    <td align="left" width="20%"><span class="field-label">Include TC/SO</span></td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="consider_tc" runat="server"
                                            Text="TC" />
                                        <asp:CheckBox ID="consider_so" runat="server"
                                            Text="SO" />

                                    </td>
                                    <td align="left" width="20%"><span class="field-label">LDA Cutoff</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTC_SO_Cutoff" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgShowDate_TC" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(550, 310, 3)" />

                                        <br />
                                        <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Format Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTransferType" runat="server">
                                            <%-- <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem Value="I">Internal</asp:ListItem>
                                <asp:ListItem Value="N">New</asp:ListItem>
                                <asp:ListItem Value="O">Overseas</asp:ListItem>
                                <asp:ListItem Value="R">Re-Admit</asp:ListItem>--%>
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Exported</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlExportType" runat="server">
                                            <asp:ListItem Value="A">All</asp:ListItem>
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                </tr>


                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export Data" />
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

</asp:Content>

