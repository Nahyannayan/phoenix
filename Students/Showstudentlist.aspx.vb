Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Showstudentlist
    Inherits System.Web.UI.Page
    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'SearchMode = "SI" 'Request.QueryString("id")
                SearchMode = Request.QueryString("id")
                If SearchMode = "SI" Then
                    Page.Title = "Student Info"
                End If

                h_Selected_menu_0.Value = "LI__../Images/operations/like.gif"
                '' h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
                '' h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                gridbind()
                gradebind()

            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_0.Value.Split("__")
        getid0(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_1.Value.Split("__")
        ' getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        'str_Sid_img = h_Selected_menu_3.Value.Split("__")
        'getid3(str_Sid_img(2))

    End Sub

    Public Function getid0(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_0_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub gradebind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_qry = "select distinct GRD_ID,GRM_DISPLAY,ACD_ID,GRD_DISPLAYORDER  FROM GRADE_M JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_GRD_ID=GRADE_M.GRD_ID JOIN ACADEMICYEAR_D ON ACD_ID=GRM_ACD_ID WHERE ACD_CURRENT=1 AND GRM_BSU_ID='" & Session("sBsuid") & "' AND GRM_ACD_ID=ACD_ID AND ACD_CLM_ID=" & Session("clm") & " order by GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_qry)
        Dim ddl As DropDownList = TryCast(gvSiblingInfo.HeaderRow.FindControl("ddlgrade"), DropDownList)
        ddl.DataSource = ds
        ddl.DataValueField = "GRD_ID"
        ddl.DataTextField = "GRM_DISPLAY"
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("ALL", 0))
        ''If (ViewState("grd_id") > 0) Then
        ddl.SelectedValue = ViewState("grd_id")
        ''End If
        sectionbind(ddl.SelectedValue)
    End Sub
    Sub sectionbind(ByVal grd_id As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_qry = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M join ACADEMICYEAR_D on acd_id=sct_acd_id WHERE acd_current=1 and acd_BSU_ID='" & Session("sBsuid") & "'  and acd_clm_id=" & Session("clm") & " AND SCT_GRD_ID='" & grd_id & "'order by sct_descr"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_qry)
        Dim ddl As DropDownList = TryCast(gvSiblingInfo.HeaderRow.FindControl("ddlsection"), DropDownList)
        ddl.DataSource = ds
        ddl.DataValueField = "SCT_ID"
        ddl.DataTextField = "SCT_DESCR"

        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("ALL", 0))
        ''If (ViewState("sct_id") > 0) Then
        ddl.SelectedValue = ViewState("sct_id")
        ''End If
   


    End Sub
    Private Sub gridbind()
        Try
            '' gradebind()

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_FeeID As String
            Dim str_filter_StudName As String
            Dim str_filter_StudNo As String
            Dim str_filter_SchoolName As String
            Dim str_filter_grdId As String
            Dim str_filter_sctId As String

            Dim temp_name As String = String.Empty
            Dim ds As New DataSet

            Dim lblFeeID As New Label
            Dim lblStudName As New Label
            Dim lblStudNo As New Label
            Dim lblSchoolName As New Label

            Dim txtSearch As New TextBox

            Dim str_txtFeeID, str_txtStudName, str_txtStudNo, str_txtSchoolName As String

            Dim str_search As String
            str_txtFeeID = ""
            str_txtStudName = ""
            str_txtStudNo = ""
            str_txtSchoolName = ""

            str_filter_FeeID = ""
            str_filter_StudName = ""
            str_filter_StudNo = ""
            str_filter_SchoolName = ""
            str_filter_grdId = ""
            str_filter_sctId = ""

            Using Userreader As SqlDataReader = AccessRoleUser.GetBUnitImage(Session("sBsuid"))
                While Userreader.Read
                    temp_name = Convert.ToString(Userreader("BSU_NAME"))
                End While
            End Using


            str_filter_SchoolName = " AND a.SchoolName LIKE '%" & temp_name & "%'"



            Dim CurBsUnit As String = Session("sBsuid").ToString & ""
            SearchMode = Request.QueryString("id")

            If SearchMode = "SI" Then 'get student detail
                str_Sql = "Select SIBLING_ID,FeeID,StudNo,StudName, SchoolName,'' as Sibling,STU_GRD_ID,GRM_DISPLAY,SCT_DESCR,stu_SCT_ID,GRD_DISPLAYORDER from(SELECT distinct STUDENT_M.STU_NO as StudNo,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '')AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, STUDENT_M.STU_SIBLING_ID as SIBLING_ID,STU_GRD_ID,GRM_DISPLAY,SCT_DESCR,stu_SCT_ID ,GRADE_M.GRD_DISPLAYORDER FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN GRADE_M ON STUDENT_M.STU_GRD_ID=GRADE_M.GRD_ID INNER JOIN GRADE_BSU_M ON GRM_GRD_ID=GRD_ID INNER JOIN SECTION_M ON SECTION_M.SCT_ID=STUDENT_M.STU_SCT_ID INNER JOIN ACADEMICYEAR_D ON ACD_ID=STU_ACD_ID INNER JOIN  BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID and(isnull(STUDENT_M.stu_leavedate,getdate())>=getdate())and STU_CURRSTATUS<>'cn' AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "' AND ACD_CURRENT=1 AND ACD_CLM_ID=" & Session("CLM") & " AND GRM_ACD_ID=ACD_ID)a where a.SIBLING_ID<>''"


                'str_Sql = " Select SIBLING_ID,FeeID,StudNo,StudName, SchoolName,'' as Sibling from(SELECT  STUDENT_M.STU_NO as StudNo,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') " & _
                '     " AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, STUDENT_M.STU_SIBLING_ID as SIBLING_ID " & _
                '    " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN " & _
                '     " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID and STUDENT_M.STU_bActive=1 AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "')a where a.SIBLING_ID<>'' "
            End If

            If SearchMode = "SE" Then 'get student detail
                str_Sql = "SELECT SIBLING_ID,FeeID,StudNo,StudName, SchoolName, stu_sibling_id as Sibling from(SELECT  distinct STUDENT_M.STU_NO as StudNo,STUDENT_M.stu_sibling_id,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') " & _
                     " AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, STUDENT_M.STU_SIBLING_ID as SIBLING_ID " & _
                     " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN " & _
                     " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "' AND stu_sibling_id IN(" & _
                     " SELECT stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>=1))a WHERE a.StudNo <> ''"
            End If



            If gvSiblingInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                If ViewState("grd_id_flag") = 1 Then
                    Dim grd_id As String = TryCast(gvSiblingInfo.HeaderRow.FindControl("ddlgrade"), DropDownList).SelectedValue
                    ViewState("grd_id") = grd_id
                    If grd_id <> "0" Then
                        str_filter_grdId = "and STU_GRD_ID='" & grd_id & "'"
                    End If

                End If
                If ViewState("sct_id_flag") = 1 Then

                    Dim sct_id As String = TryCast(gvSiblingInfo.HeaderRow.FindControl("ddlsection"), DropDownList).SelectedValue
                    ViewState("sct_id") = sct_id
                    If sct_id <> "0" Then
                        str_filter_sctId = "and STU_SCT_ID='" & sct_id & "'"
                    End If
                End If

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_0.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtFeeID")
                str_txtFeeID = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                'str_Sid_search = h_selected_menu_1.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudNo")
                'str_filter_StudNo = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_StudNo = " AND a.StudNo LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_StudNo = "  AND  NOT a.StudNo LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_StudNo = " AND a.StudNo  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_StudNo = " AND a.StudNo  NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_StudNo = " AND a.StudNo LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_StudNo = " AND a.StudNo NOT LIKE '%" & txtSearch.Text & "'"
                'End If


                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudName")
                str_txtStudName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_StudName = " AND a.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_StudName = "  AND  NOT a.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_StudName = " AND a.StudName  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_StudName = " AND a.StudName  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_StudName = " AND a.StudName LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_StudName = " AND a.StudName NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                'str_Sid_search = h_Selected_menu_3.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtSchoolName")
                'str_txtSchoolName = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_SchoolName = " AND a.SchoolName LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_SchoolName = "  AND  NOT a.SchoolName LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_SchoolName = " AND a.SchoolName  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_SchoolName = " AND a.SchoolName  NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_SchoolName = " AND a.SchoolName LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_SchoolName = " AND a.SchoolName NOT LIKE '%" & txtSearch.Text & "'"
                'End If

            End If

            If SearchMode = "SE" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_StudNo & str_filter_StudName)
            Else
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_FeeID & str_filter_StudNo & str_filter_StudName & str_filter_SchoolName & str_filter_grdId & str_filter_sctId & "ORDER BY A.GRD_DISPLAYORDER,A.SCT_DESCR,A.STUDNAME")

            End If
            ' ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_FeeID & str_filter_StudNo & str_filter_StudName & str_filter_SchoolName & "order by a.StudName")



            gvSiblingInfo.DataSource = ds.Tables(0)
            ' gvSiblingInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvSiblingInfo.DataBind()
                Dim columnCount As Integer = gvSiblingInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvSiblingInfo.Rows(0).Cells.Clear()
                gvSiblingInfo.Rows(0).Cells.Add(New TableCell)
                gvSiblingInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSiblingInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSiblingInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvSiblingInfo.HeaderRow.Visible = True
            Else
                gvSiblingInfo.DataBind()


            End If
            gradebind()
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtFeeID")
            txtSearch.Text = str_txtFeeID
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = str_txtStudName
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = str_txtStudNo
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtSchoolName")
            txtSearch.Text = str_txtSchoolName

            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            UtilityObj.Errorlog("Error In collecting sibling info")
        End Try





    End Sub

    'Protected Sub lblStudName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblStudNo As New Label
    '    Dim lblStudID As New Label
    '    Dim lbClose As New LinkButton


    '    lbClose = sender

    '    lblStudNo = sender.Parent.FindControl("lblStudNo")
    '    lblStudID = sender.parent.findcontrol("lblStudID")

    '    ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
    '    Dim l_Str_Msg As String = lblStudNo.Text & "___" & lblStudID.Text
    '    l_Str_Msg = l_Str_Msg.Replace("'", "\'")

    '    If (Not lblStudNo Is Nothing) Then
    '        '   Response.Write(lblcode.Text)
    '        Response.Write("<script language='javascript'> function listen_window(){")
    '        Response.Write("window.returnValue = '" & l_Str_Msg & "';")


    '        Response.Write("window.close();")
    '        Response.Write("} </script>")
    '        h_SelectedId.Value = "Close"
    '    End If
    'End Sub
    Protected Sub gvSiblingInfo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSiblingInfo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lblStudID As Label = DirectCast(e.Row.FindControl("lblStudID"), Label)
            Dim lblStudName As LinkButton = DirectCast(e.Row.FindControl("lblStudName"), LinkButton)
            Dim l_Str_Msg As String = lblStudName.Text & "___" & lblStudID.Text
            l_Str_Msg = l_Str_Msg.Replace("'", "\'")
            If (Not lblStudID Is Nothing) Then

                lblStudName.Attributes.Add("onClick", "return SetValuetoParent('" & l_Str_Msg & "');")
            End If

        End If
    End Sub


    Protected Sub btnSearchFeeID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStudNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSchoolName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub gvSiblingInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSiblingInfo.PageIndexChanging
        gvSiblingInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvSiblingInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSiblingInfo.SelectedIndexChanged

    End Sub

    Protected Sub ddlsection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ViewState("sct_id_flag") = 1
        gridbind()
    End Sub

    Protected Sub ddlgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("sct_id_flag") = 0
        ViewState("grd_id_flag") = 1
        gridbind()
    End Sub
End Class
