<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studApplicationDecision_View.aspx.vb" Inherits="Students_studApplicationDecision_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
      
function switchViews(obj,row)
        {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            
            if (div.style.display=="none")
                {
                    div.style.display = "inline";
                    if (row=='alt')
                       {
                           img.src="../Images/expand_button_white_alt_down.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_Button_white_Down.jpg" ;
                       }
                   img.alt = "Click to close";
               }
           else
               {
                   div.style.display = "none";
                   if (row=='alt')
                       {
                           img.src="../Images/Expand_button_white_alt.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_button_white.jpg" ;
                       }
                   img.alt = "Click to expand";
               }
       }

</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Application Decision Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%-- <table id="tbl_ShowScreen" runat="server" align="center" 
                    cellpadding="5" cellspacing="0" width="100%">                                  

                    <tr>
                        <td align="center"  >--%>

                <%--<table id="Table1" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" >
                                <tr>
                                    <td colspan="3" align="center" class="matters">--%>

                <table id="Table2" runat="server" align="center" width="100%"
                    cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvAppl" runat="server"
                                AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                OnRowDataBound="gvAppl_RowDataBound" PageSize="20" AllowPaging="True" AllowSorting="True">
                                <Columns>

                                    <asp:TemplateField HeaderText="-" Visible="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAplId" runat="server" Text='<%# Bind("APL_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                                                <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                            </a>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
                                                <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                            </a>
                                        </AlternatingItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplication" runat="server" Text='<%# Bind("APL_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="APL Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTypes" runat="server" Text='<%# Bind("APL_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Disable Next Stage">
                                        <ItemTemplate>
                                            <asp:Image ID="imgDIS" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("IMGDIS") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>



                                    <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                    <asp:TemplateField>

                                        <ItemTemplate>
                                            </td>
                                            </tr>
                                                                    <tr>
                                                                        <td colspan="100%">
                                                                            <div id="div<%# Eval("GUID") %>" style="display: none; position: relative; left: 20px;">
                                                                                <asp:GridView ID="gvReason" runat="server" Width="98%"
                                                                                    AutoGenerateColumns="false" EmptyDataText="No reasons added">

                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="APR_DESCR" HeaderText="Reason" HtmlEncode="False">
                                                                                            <ItemStyle HorizontalAlign="left" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                    <RowStyle CssClass="griditem"   />
                                                                                    <HeaderStyle CssClass="gridheader_pop" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle />
                                <PagerStyle  HorizontalAlign="Left" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <%--  </td>
                                </tr>
                            </table>--%>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <%--</td>
                    </tr>
                </table>--%>
            </div>
        </div>
    </div> 





</asp:Content>

