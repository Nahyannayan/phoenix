﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Students_studCardNumber
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200219") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights




                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    BindSection()

                    tr1.Visible = False
                    tr2.Visible = False
                    tr3.Visible = False
                    tr4.Visible = False

                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                   & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                   & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                                   & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                                   & " where grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                   & " order by grd_displayorder"
        Else
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                  & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                  & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                                  & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                                  & " inner join section_m on grm_id=sct_grm_id" _
                                  & " where grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                                    & " order by grd_displayorder"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()


    End Sub

    Sub BindSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                        & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                        & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'" _
                        & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'" _
                       & " ORDER BY SCT_DESCR"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String




        str_query = "SELECT STU_ID,STU_NO,ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) AS STU_NAME," _
                         & " ISNULL(STU_TCPOS_CARDNUM,'') STU_TCPOS_CARDNUM" _
                         & " FROM STUDENT_M AS A  " _
                         & " WHERE STU_ACD_ID=" + hACD_ID.Value _
                         & " AND STU_SCT_ID=" + hSCT_ID.Value _
                         & " AND STU_CURRSTATUS<>'CN'" _
                         & " ORDER BY STU_PASPRTNAME"

      

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvStud.DataSource = ds
        gvStud.DataBind()

        Dim i As Integer

       


    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        Dim i As Integer

        Dim lblStuID As Label
        Dim txtHeight As TextBox
        Dim txtWeight As TextBox
        Dim ddlbloodgroup As DropDownList
        Dim txtVisionL As TextBox
        Dim txtVisionR As TextBox
        Dim txtDental As TextBox
        Dim txtRollNo As TextBox
        Dim txtFather As TextBox
        Dim txtMother As TextBox
        Dim txtRegNo As TextBox

        For i = 0 To gvStud.Rows.Count - 1

            With gvStud.Rows(i)
                lblStuID = .FindControl("lblStuID")
             
                txtRollNo = .FindControl("txtRollNo")
               
            End With

            str_query = "exec saveSTUDENT_TCPOS_CARDNUM " _
                      & "@STH_STU_ID=" + lblStuID.Text + "," _
                      & "@STH_ACD_ID=" + hACD_ID.Value + "," _
                      & "@STH_GRD_ID='" + hGRD_ID.Value + "'," _
                      & "@STH_ROLLNO='" + txtRollNo.Text + "' ," _
                      & "@USR_NAME='" + Session("sUsr_name") + "'"
                    
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next


        lblError.Text = "Record Saved Successfully"
    End Sub
#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        tr1.visible = True
        tr2.visible = True
        tr3.visible = True
        tr4.visible = True


        hSCT_ID.Value = ddlSection.SelectedValue.ToString
        hACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hGRD_ID.Value = ddlSection.SelectedValue.ToString
        GridBind()

        lblError.Text = ""
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSection()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
    End Sub
End Class
