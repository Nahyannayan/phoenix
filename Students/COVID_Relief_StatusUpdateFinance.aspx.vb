﻿
'Partial Class Students_COVID_Relief_StatusUpdateFinance
'    Inherits System.Web.UI.Page

'End Class


'Partial Class Students_COVID_Relief_StatusUpdate
'    Inherits System.Web.UI.Page

'End Class

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_COVID_Relief_StatusUpdateFinance
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                ViewState("RRH_ID") = Encr_decrData.Decrypt(Request.QueryString("RRH_ID").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                gridbind(ViewState("RRH_ID"))
                GetUpdateStatusData()
                getReliefQualifyData()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
    End Sub



    Public Sub gridbind(ByRef RRH_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", RRH_ID)
            param(1) = New SqlParameter("@IS_CURR_STATUS_REQ", False)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_STATUS_LIST", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ddlStatus.DataSource = ds.Tables(0)
                ddlStatus.DataBind()

                ddlStatus.DataSource = ds
                ddlStatus.DataTextField = "CSM_DESCRIPTION"
                ddlStatus.DataValueField = "CMS_CODE"
                ddlStatus.SelectedValue = 11
                ddlStatus.DataBind()

                For i = 0 To ds.Tables(0).Rows.Count - 1

                    If ds.Tables(0).Rows(i)("SELECTED").ToString() = "1" Then
                        ddlStatus.SelectedIndex = i
                    End If


                Next
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Public Sub btnSave_Click(sender As Object, e As EventArgs)
        Dim id As Integer
        Dim RQM_ID As Integer

        Dim discount As String = ""
        Dim qal As Boolean = False

        If (radioBtnList.SelectedValue = "Yes") Then
            qal = True
            RQM_ID = ddlStatusRelQualYes.SelectedValue
        Else
            RQM_ID = ddlStatusRelQualNo.SelectedValue
        End If


        'If qal And Not Int32.TryParse(txtDiscount.Text, id) Then

        '    errLbl.Text = "Please enter the vaild discount"

        'Else

        If (txtComments.Text <> "") Then
            If (qal And ddlStatusRelQualYes.SelectedIndex <= 0) Then
                errLbl.Text = "Please select the Relief Qualified Under"
            ElseIf (Not qal And ddlStatusRelQualNo.SelectedIndex <= 0) Then

                errLbl.Text = "Please select the Rejection reason"

            ElseIf qal And (txtDiscount.Text = "" Or txtDiscount.Text = "0") Then
                errLbl.Text = "Please enter the vaild discount"
            Else

                If (radioBtnList.SelectedValue = "Yes") Then
                    discount = txtDiscount.Text
                End If

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim objConn As New SqlConnection(str_conn) '
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try


                    Dim retVal As Int64


                    Dim pParms(9) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int)
                    pParms(0).Value = ViewState("RRH_ID")
                    pParms(1) = New SqlClient.SqlParameter("@RRH_STATUS", SqlDbType.VarChar)
                    pParms(1).Value = ddlStatus.SelectedValue
                    pParms(2) = New SqlClient.SqlParameter("@RRC_LOG_USR_ID", SqlDbType.VarChar)
                    pParms(2).Value = Session("sUsr_name")
                    pParms(3) = New SqlClient.SqlParameter("@RRC_COMMENTS", SqlDbType.VarChar)
                    pParms(3).Value = txtComments.Text
                    pParms(4) = New SqlClient.SqlParameter("@IS_QUALIFIED", SqlDbType.Bit)
                    pParms(4).Value = qal
                    pParms(5) = New SqlClient.SqlParameter("@RQM_ID", SqlDbType.Int)
                    pParms(5).Value = RQM_ID
                    pParms(6) = New SqlClient.SqlParameter("@RRH_DISCOUNT_PERCENT", SqlDbType.VarChar)
                    pParms(6).Value = discount
                    pParms(7) = New SqlClient.SqlParameter("@RetVal", SqlDbType.BigInt)
                    pParms(7).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "COV.UPDATE_RELIEF_FINANCE_STATUS", pParms)
                    retVal = pParms(7).Value

                    If (retVal = 0) Then
                        stTrans.Commit()

                        errLbl.Text = "Data Saved Sucessfully"
                    Else
                        stTrans.Rollback()

                        errLbl.Text = "Unable to save the data"
                    End If


                    If (radioBtnList.SelectedValue = "Yes") Then
                        ddlStatusRelQualYes_SelectedIndexChanged(Nothing, Nothing)
                    End If

                Catch ex As Exception
                    stTrans.Rollback()

                    errLbl.Text = "Unable to save the data"

                Finally

                End Try

            End If
        Else
            errLbl.Text = "Please enter the comments"

        End If
        ' End If

    End Sub

    Public Sub GetUpdateStatusData()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", ViewState("RRH_ID"))


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_UPDATE_STATUS_FINANCE_DATA", param)
            If ds.Tables(0).Rows.Count > 0 Then
                FeeLbl.Text = ds.Tables(0).Rows(0)("FEES").ToString()
                ConTlbl.Text = ds.Tables(0).Rows(0)("CON_TYPE").ToString()
                ConAmtlbl.Text = ds.Tables(0).Rows(0)("CON_AMT").ToString()
                PreComments.Text = ds.Tables(0).Rows(0)("PRE_COMMENTS").ToString()
                CurrStatusLbl.Text = ds.Tables(0).Rows(0)("CURR_STATUS").ToString()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub getReliefQualifyData()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@Type", "Yes")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_QUALIFY_M", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ddlStatusRelQualYes.DataSource = ds.Tables(0)
                ddlStatusRelQualYes.DataBind()

                ddlStatusRelQualYes.DataSource = ds
                ddlStatusRelQualYes.DataTextField = "RQM_DESCR"
                ddlStatusRelQualYes.DataValueField = "RQM_ID"
                ddlStatusRelQualYes.SelectedIndex = 0
                ddlStatusRelQualYes.DataBind()

                ViewState("ddlStatusRelQualYes") = ds.Tables(0)
            End If

            Dim ds1 As DataSet
            Dim param1(1) As SqlParameter
            param1(0) = New SqlParameter("@Type", "No")
            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_QUALIFY_M", param1)
            If ds1.Tables(0).Rows.Count > 0 Then

                ddlStatusRelQualNo.DataSource = ds1.Tables(0)
                ddlStatusRelQualNo.DataBind()

                ddlStatusRelQualNo.DataSource = ds1
                ddlStatusRelQualNo.DataTextField = "RQM_DESCR"
                ddlStatusRelQualNo.DataValueField = "RQM_ID"
                ddlStatusRelQualNo.SelectedIndex = 0
                ddlStatusRelQualNo.DataBind()

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub radioBtnList_SelectedIndexChanged(sender As Object, e As EventArgs)
        If radioBtnList.SelectedValue = "Yes" Then
            ddlStatus.SelectedValue = 10
            trRelQalYes.Visible = True
            trRelQalNo.Visible = False
            trDis.Visible = True
            trDiscountAmt.Visible = True
        Else
            ddlStatus.SelectedValue = 11
            trRelQalYes.Visible = False
            trRelQalNo.Visible = True
            trDis.Visible = False
            trDiscountAmt.Visible = False
        End If
    End Sub
    Protected Sub ddlStatusRelQualYes_SelectedIndexChanged(sender As Object, e As EventArgs)
        'Try




        '    txtDiscount.Text = ViewState("ddlStatusRelQualYes").Rows(ddlStatusRelQualYes.SelectedIndex)("DISCOUNT").ToString()

        'Catch ex As Exception
        '    txtDiscount.Text = "0"
        'End Try

        'Try
        '    txtDiscountAmount.Text = ((Decimal.Parse(FeeLbl.Text) - Decimal.Parse(ConAmtlbl.Text)) * Decimal.Parse(txtDiscount.Text) / 100).ToString()
        'Catch ex As Exception
        '    txtDiscountAmount.Text = "0.00"
        'End Try


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim ds As DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@RRH_ID ", ViewState("RRH_ID"))
        param(1) = New SqlParameter("@RQM_ID", ddlStatusRelQualYes.SelectedValue)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_DISCOUNT_APPLICABLE", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtDiscount.Text = ds.Tables(0).Rows(0)("DISC_TEXT")
            txtDiscountAmount.Text = ds.Tables(0).Rows(0)("DISC_AMOUNT")
        End If

        ''ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "GetDiscount();", True)

    End Sub
End Class


