Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studSection_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Session("SelectedRow") = -1
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "S050040" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("dtSection") = SetDataTable()
                    ViewState("SelectedRow") = -1

                    If ViewState("datamode") = "add" Then
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                        callStream_Bind()
                        'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                        'If ddlGrade.Items.Count <> 0 Then
                        '    ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                        'End If
                        'If ddlShift.Items.Count <> 0 Then
                        '    ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
                        '    GetMaxGradeStength()
                        'End If

                    Else
                        Dim editstring As String = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+"))
                        Dim editstrings() As String = editstring.Split("|")

                        ddlGrade.Items.Clear()
                        Dim li As New ListItem
                        li.Text = editstrings(0)
                        li.Value = editstrings(1)
                        ddlAcademicYear.Items.Add(li)
                        Dim liStream = New ListItem
                        liStream.Text = editstrings(4)
                        liStream.Value = editstrings(4)
                        callStream_Bind(liStream.Value)
                        Dim liGrade = New ListItem
                        liGrade.Text = editstrings(2)
                        liGrade.Value = editstrings(3)
                        ddlGrade.SelectedValue = liGrade.Value
                        Dim liShift = New ListItem
                        liShift.Text = editstrings(5)
                        liShift.Value = editstrings(5)
                        ddlShift.SelectedValue = liShift.Value



                        If ddlGrade.Items.Count <> 0 Then
                            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                            HF_GRM_Stream.Value = Get_GRMIDBy_GradeStream(ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue, ddlShift.SelectedValue)
                        End If
                        If ddlShift.Items.Count <> 0 Then
                            'ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)

                            GetMaxGradeStength()
                        End If

                        ' ddlShift.Items.Add(editstrings(4))

                        'li = New ListItem
                        'li.Text = editstrings(5)
                        'li.Value = editstrings(3)
                        'ddlStream.Items.Add(li)

                        GetRows()
                        EnableDisableControls(True)

                        GetMaxGradeStength()
                    End If

                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub

    Private Sub GetMaxGradeStength()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select GRM_MAXCAPACITY from grade_bsu_m where GRM_ID=" + HF_GRM_Stream.Value + " "
        hfGrm_MaxCapacity.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub


    Private Function AvailabeStrength() As Integer
        Dim dt As DataTable = Session("dtSection")
        Dim dr As DataRow
        Dim strength As Integer = 0
        Dim aStrength As Integer = 0
        For Each dr In dt.Rows
            If dr.Item(2) = HF_GRM_Stream.Value And dr.Item(9) <> "delete" And dr.Item(9) <> "remove" Then
                strength += CType(dr.Item(8), Integer)
            End If
        Next
        aStrength = hfGrm_MaxCapacity.Value - strength
        Return aStrength
    End Function


    ''grades for which sections are already added are not populated
    'Public Sub PopulateGrade()
    '    ddlGrade.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder" _
    '                             & " FROM grade_bsu_m,grade_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id " _
    '                             & "AND grm_acd_id=" + ddlAcademicYear.SelectedValue + " AND " _
    '                             & "grm_grd_id NOT IN (SELECT grm_grd_id FROM grade_bsu_m WHERE grm_id " _
    '                            & " IN (SELECT sct_grm_id FROM section_m) and grm_acd_id=" + ddlAcademicYear.SelectedValue + ") ORDER BY grd_displayorder"


    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '    ddlGrade.DataSource = ds
    '    ddlGrade.DataTextField = "grm_display"
    '    ddlGrade.DataValueField = "grm_grd_id"
    '    ddlGrade.DataBind()
    'End Sub
    Sub EnableDisableControls(ByVal value As Boolean)
        txtMaxStrength.ReadOnly = value
        gvStudSection.Enabled = Not value
    End Sub


    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(2)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "acd_id"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "acy_descr"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_id"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "grm_display"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "shf_descr"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "stm_descr"
        dt.Columns.Add(column)
       
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sct_id"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sct_descr"
        dt.Columns.Add(column)
        keys(1) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sct_maxstrength"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "datamode"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function


    Sub AddRows()
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            dt = Session("dtSection")

            ''check if the row is already added
            Dim keys As Object()
            ReDim keys(1)
            keys(0) = HF_GRM_Stream.Value
            keys(1) = txtSection.Text

            'If AvailabeStrength() < Val(txtMaxStrength.Text) Then
            '    lblError.Text = "Max strength exceeds the available Grade capacity"
            '    Exit Sub
            'End If
            Dim row As DataRow = dt.Rows.Find(keys)
            If Not row Is Nothing Then
                lblError.Text = "This section is already added"
                Exit Sub
            End If

            dr = dt.NewRow
            dr.Item(0) = ddlAcademicYear.SelectedValue.ToString
            dr.Item(1) = ddlAcademicYear.SelectedItem.Text
            dr.Item(2) = HF_GRM_Stream.Value
            dr.Item(3) = ddlGrade.SelectedItem.Text
            dr.Item(4) = ddlShift.SelectedItem.Text
            dr.Item(5) = ddlStream.SelectedItem.Text
            dr.Item(6) = 0
            dr.Item(7) = txtSection.Text
            dr.Item(8) = txtMaxStrength.Text.ToString
            dr.Item(9) = "add"
            dt.Rows.Add(dr)

            Session("dtSection") = dt
            Dim dtTemp As New DataTable
            dtTemp = SetDataTable()
            Dim drTemp As DataRow
            Dim i, j As Integer
            For i = 0 To dt.Rows.Count - 1
                drTemp = dtTemp.NewRow
                If dt.Rows(i)(9) <> "delete" And dt.Rows(i)(9) <> "remove" Then
                    For j = 0 To dt.Columns.Count - 1
                        drTemp.Item(j) = dt.Rows(i)(j)
                    Next
                    dtTemp.Rows.Add(drTemp)
                End If
            Next

            gvStudSection.DataSource = dtTemp
            gvStudSection.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub GetRows()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT sct_grm_id,grm_display,shf_descr," _
        '                        & "stm_descr,sct_id,sct_descr, sct_maxstrength, grd_displayorder " _
        '                        & "FROM section_m, grade_m, grade_bsu_m, shifts_m, stream_m " _
        '                        & "WHERE section_m.sct_grm_id = grade_bsu_m.grm_id " _
        '                        & "AND grade_bsu_m.grm_grd_id=grade_m.grd_id AND " _
        '                        & "grade_bsu_m.grm_shf_id=shifts_m.shf_id AND  " _
        '                        & "grade_bsu_m.grm_stm_id = stream_m.stm_id AND " _
        '                        & "grm_id = " + hfGRM_ID.Value + ""

        Dim str_query As String = "SELECT sct_grm_id,grm_display,shf_descr," _
                        & "stm_descr,sct_id,sct_descr, sct_maxstrength, grd_displayorder " _
                        & "FROM section_m, grade_m, grade_bsu_m, shifts_m, stream_m " _
                        & "WHERE section_m.sct_grm_id = grade_bsu_m.grm_id " _
                        & "AND grade_bsu_m.grm_grd_id=grade_m.grd_id AND " _
                        & "grade_bsu_m.grm_shf_id=shifts_m.shf_id AND  " _
                        & "grade_bsu_m.grm_stm_id = stream_m.stm_id AND " _
                        & "sct_grd_id = '" + ddlGrade.SelectedValue.ToString + "' and sct_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_STM_ID = " + ddlStream.SelectedValue.ToString _
                        & " and sct_descr<>'TEMP'"


        Dim dt As DataTable
        Dim dr As DataRow
        dt = Session("dtSection")

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            dr = dt.NewRow
            With dr
                .Item(0) = ddlAcademicYear.SelectedValue.ToString
                .Item(1) = ddlAcademicYear.SelectedItem.Text
                .Item(2) = reader.GetValue(0).ToString
                .Item(3) = reader.GetString(1)
                .Item(4) = reader.GetString(2)
                .Item(5) = reader.GetString(3)
                .Item(6) = reader.GetValue(4).ToString
                .Item(7) = reader.GetString(5).ToString
                .Item(8) = reader.GetValue(6).ToString
                .Item(9) = "edit"
                dt.Rows.Add(dr)
            End With
        End While
        reader.Close()

        gvStudSection.DataSource = dt
        gvStudSection.DataBind()
        Session("dtSection") = dt

        'gvStudSection.Columns(1).Visible = False
        'gvStudSection.Columns(3).Visible = False
        'gvStudSection.Columns(4).Visible = False
        'gvStudSection.Columns(5).Visible = False


    End Sub
    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtSection")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                'If (AvailabeStrength() + Val(.Item(8)) - Val(txtMaxStrength.Text)) < 0 Then
                '    lblError.Text = "Max strength exceeds the available Grade capacity"
                '    Exit Sub
                'End If
                'Session("AvailableGradeStrength") += Val(.Item(8)) - Val(txtMaxStrength.Text)
                .Item(7) = txtSection.Text.ToString
                .Item(8) = txtMaxStrength.Text.ToString
                If .Item(9) = "edit" Then
                    .Item(9) = "update"
                End If
            End With

            Session("dtSection") = dt
            Dim dtTemp As New DataTable
            dtTemp = SetDataTable()
            Dim drTemp As DataRow
            Dim i, j As Integer
            For i = 0 To dt.Rows.Count - 1
                drTemp = dtTemp.NewRow
                If dt.Rows(i)(9) <> "delete" And dt.Rows(i)(9) <> "remove" Then
                    For j = 0 To dt.Columns.Count - 1
                        drTemp.Item(j) = dt.Rows(i)(j)
                    Next
                    dtTemp.Rows.Add(drTemp)
                End If
            Next

            gvStudSection.DataSource = dtTemp
            gvStudSection.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub SaveData()
        ' Try
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim dt As DataTable
        dt = Session("dtSection")
        Dim dr As DataRow
        Dim str_query As String

        Dim sctAddIds As String = ""
        Dim sctEditIds As String = ""
        Dim sctDeleteIds As String = ""


        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                For Each dr In dt.Rows
                    With dr
                        If .Item(9).ToString <> "remove" And .Item(9).ToString <> "edit" Then
                            If .Item(9) = "update" Then
                                UtilityObj.InsertAuditdetails(transaction, "edit", "SECTION_M", "SCT_ID", "SCT_GRM_ID", "SCT_ID=" + .Item(6).ToString)
                            ElseIf .Item(9) = "remove" Then
                                UtilityObj.InsertAuditdetails(transaction, "delete", "SECTION_M", "SCT_ID", "SCT_GRM_ID", "SCT_ID=" + .Item(6).ToString)
                            End If
                            str_query = "exec SaveSTUDSECTION  '" + Session("sbsuid").ToString + "'," _
                                        & .Item(0).ToString + "," _
                                        & "'" + ddlGrade.SelectedValue.ToString + "'," _
                                        & .Item(2).ToString + "," _
                                        & .Item(6).ToString + ",'" _
                                        & .Item(7).ToString + "'," _
                                        & .Item(8).ToString + ",'" _
                                        & .Item(9) + "'"
                            dr.Item(6) = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                            If .Item(9) = "add" Then
                                If sctAddIds.Length <> 0 Then
                                    sctAddIds += ","
                                End If
                                sctAddIds += dr.Item(6)
                            End If

                            If .Item(9) = "update" Then
                                If sctEditIds.Length <> 0 Then
                                    sctEditIds += ","
                                End If
                                sctEditIds += dr.Item(6)
                            End If

                            If .Item(9) = "delete" Then
                                If sctDeleteIds.Length <> 0 Then
                                    sctDeleteIds += ","
                                End If
                                sctDeleteIds += dr.Item(6)
                            End If
                        End If
                    End With
                Next

                Dim flagAudit As Integer
                If sctAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SCT_ID(" + sctAddIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If sctEditIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SCT_ID(" + sctEditIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If sctDeleteIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SCT_ID(" + sctDeleteIds + ")", "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If ViewState("datamode") = "edit" Then
                    Session("dtSection") = dt
                    Dim dtTemp As New DataTable
                    dtTemp = SetDataTable()
                    Dim drTemp As DataRow
                    Dim i, j As Integer
                    For i = 0 To dt.Rows.Count - 1
                        drTemp = dtTemp.NewRow
                        If dt.Rows(i)(9) <> "delete" And dt.Rows(i)(9) <> "remove" Then
                            For j = 0 To dt.Columns.Count - 1
                                drTemp.Item(j) = dt.Rows(i)(j)
                            Next
                            dtTemp.Rows.Add(drTemp)
                        End If
                    Next

                    gvStudSection.DataSource = dtTemp
                    gvStudSection.DataBind()
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If ddlGrade.Items.Count <> 0 Then
            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            HF_GRM_Stream.Value = Get_GRMIDBy_GradeStream(ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue, ddlShift.SelectedValue)
        End If
        If ddlShift.Items.Count <> 0 Then

            'ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
            GetMaxGradeStength()
        End If

    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        If ddlShift.Items.Count <> 0 Then
            'ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
            HF_GRM_Stream.Value = Get_GRMIDBy_GradeStream(ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue, ddlShift.SelectedValue)
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If
            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
            ddlShift.Enabled = True
            ddlStream.Enabled = True
            txtSection.Text = ""
            txtMaxStrength.Text = ""
            txtSection.ReadOnly = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

   

    Protected Sub gvStudSection_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudSection.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim selectedRow As GridViewRow = DirectCast(gvStudSection.Rows(index), GridViewRow)
        Dim lblSctId As New Label
        Dim lblSection As New Label
        Dim lblmaxStrength As New Label
        Dim lblGrmId As New Label
        lblSection = selectedRow.FindControl("lblSection")
        lblGrmId = selectedRow.FindControl("lblGrmId")
        lblError.Text = ""
        Dim keys As Object()
        Dim dt As DataTable
        dt = Session("dtSection")

        ReDim keys(1)

        keys(0) = lblGrmId.Text
        keys(1) = lblSection.Text.ToString
        index = dt.Rows.IndexOf(dt.Rows.Find(keys))

        If e.CommandName = "edit" Then
            With selectedRow
                lblmaxStrength = .Cells(8).FindControl("lblMaxStrength")
            End With
            txtSection.Text = lblSection.Text
            txtMaxStrength.Text = lblmaxStrength.Text
            ViewState("SelectedRow") = index
            btnAddNew.Text = "Update"
            'txtSection.ReadOnly = True
        Else
            If dt.Rows(index).Item(9) = "add" Then
                dt.Rows(index).Item(9) = "remove"
            ElseIf dt.Rows(index).Item(9) = "edit" Then
                dt.Rows(index).Item(9) = "delete"
            End If
            Session("dtSection") = dt
            Dim dtTemp As New DataTable
            dtTemp = SetDataTable()
            Dim drTemp As DataRow
            Dim i, j As Integer
            For i = 0 To dt.Rows.Count - 1
                drTemp = dtTemp.NewRow
                If dt.Rows(i)(9) <> "delete" And dt.Rows(i)(9) <> "remove" Then
                    For j = 0 To dt.Columns.Count - 1
                        drTemp.Item(j) = dt.Rows(i)(j)
                    Next
                    dtTemp.Rows.Add(drTemp)
                End If
            Next

            gvStudSection.DataSource = dtTemp
            gvStudSection.DataBind()
        End If

    End Sub

    Protected Sub gvStudSection_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudSection.RowDeleting

    End Sub

    Protected Sub gvStudSection_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudSection.RowEditing

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ViewState("datamode") = "add"
        EnableDisableControls(False)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
        'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        'ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
        'ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
        callStream_Bind()
        txtSection.Text = ""
        txtMaxStrength.Text = ""
        Session("dtSection") = SetDataTable()
        gvStudSection.DataSource = Session("dtSection")
        gvStudSection.DataBind()
        'gvStudSection.Columns(1).Visible = False
        'gvStudSection.Columns(3).Visible = False
        'gvStudSection.Columns(4).Visible = False
        'gvStudSection.Columns(5).Visible = False

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "edit"
            EnableDisableControls(False)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            hfGRD_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                EnableDisableControls(True)
                txtSection.Text = ""
                txtMaxStrength.Text = ""
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Session("dtSection") = SetDataTable()
                gvStudSection.DataSource = Session("dtGrade")
                gvStudSection.DataBind()
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callStream_Bind()
        'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        'If ddlGrade.Items.Count <> 0 Then
        '    ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
        'End If
        'If ddlShift.Items.Count <> 0 Then
        '    ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
        'End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            SaveData()
            txtSection.Text = ""
            txtMaxStrength.Text = ""
            If ViewState("datamode") = "add" Then
                Session("dtSection") = SetDataTable()
                gvStudSection.DataSource = Session("dtSection")
                gvStudSection.DataBind()
            Else
                Session("dtSection") = SetDataTable()
                GetRows()
            End If

            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Error while saving data"
        End Try
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        'if the selected grade is the maximum grade of the school then disable section
        Try
            BindGrade()
            If ddlGrade.Items.Count <> 0 Then
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'If ddlGrade.SelectedIndex <> ddlGrade.Items.Count - 1 Then
        '    ddlPromoteSection.Enabled = True
        '    PopulatePromotedSection()
        'Else
        '    ddlPromoteSection.Enabled = False
        '    ddlPromoteSection.Items.Clear()
        'End If
    End Sub
    Public Sub callStream_Bind(ByVal Optional Stream_ID As String = "-1")
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                If Stream_ID = "-1" Then
                    ddlStream.SelectedValue = "1"
                Else
                    ddlStream.SelectedValue = Stream_ID
                End If

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub BindGrade()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            'ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
    Function Get_GRMIDBy_GradeStream(ByVal grdid As String, ByVal acdid As String, ByVal shfid As String) As String
        Dim GRM_ID As String = "0"
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT grm_id FROM grade_bsu_m,stream_m WHERE" _
                                     & " grade_bsu_m.grm_stm_id=" + ddlStream.SelectedValue + " and " _
                                 & " grm_acd_id=" + acdid + " and grm_grd_id='" + grdid + "' and grm_shf_id=" + shfid
            Dim val = SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_query)
            GRM_ID = val
        Catch ex As Exception

        End Try

        Return GRM_ID
    End Function
End Class
