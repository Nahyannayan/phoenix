Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.IO
Partial Class Students_studBB_CheckedBy_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100125") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim mode As String = Request.QueryString("mode")
                    If mode = "edit" Then

                        Dim editstring As String = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+"))
                        Dim editstrings As String() = editstring.Split("|")

                        hfSEC_ID.Value = editstrings(0)
                        Dim li As New ListItem

                        li.Text = editstrings(1)
                        li.Value = CType(editstrings(2), Integer)
                        ddlAcademicYear.Items.Add(li)
                        ddlAcademicYear.Items(0).Selected = True


                        ddlGrade.Items.Add(editstrings(3))


                        li = New ListItem
                        li.Text = editstrings(4).Trim
                        li.Value = CType(editstrings(5).Trim, Integer)
                        ddlSection.Items.Add(li)
                        ddlSection.Items(0).Selected = True

                        PopulateClassTeacher()

                        li = New ListItem
                        li.Text = editstrings(6).Trim
                        li.Value = editstrings(7).Trim

                        'ddlClassTeacher.Items(ddlClassTeacher.Items.IndexOf(li)).Selected = True

                        For ItemTypeCounter As Integer = 0 To ddlClassTeacher.Items.Count - 1
                            'keep loop until you get the School to Not Available into  the SelectedIndex
                            If UCase(ddlClassTeacher.Items(ItemTypeCounter).Value) = li.Value Then
                                ddlClassTeacher.SelectedIndex = ItemTypeCounter
                            End If
                        Next

                        ddlShift.Items.Add(editstrings(8))

                        li = New ListItem
                        li.Text = editstrings(9).Trim
                        li.Value = editstrings(10).Trim
                        ddlStream.Items.Add(li)
                        ddlStream.Items(0).Selected = True
                        EnableDisableControls(False)
                    ElseIf mode = "add" Then
                        lblError.Text = ""
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                        If ddlAcademicYear.Items.Count <> 0 Then
                            PopulateGrade()
                        End If
                        If ddlGrade.Items.Count <> 0 Then
                            ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                        End If
                        If ddlShift.Items.Count <> 0 Then
                            ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
                        End If
                        If ddlStream.Items.Count <> 0 Then
                            PopulateSection()
                        End If

                        PopulateClassTeacher()
                    End If


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            If ddlGrade.Items.Count <> 0 Then
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            End If
            If ddlShift.Items.Count <> 0 Then
                ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
            End If
            If ddlStream.Items.Count <> 0 Then
                PopulateSection()
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            If ddlShift.Items.Count <> 0 Then
                ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
            End If
            If ddlStream.Items.Count <> 0 Then
                PopulateSection()
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(True)
            lblError.Text = ""
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
            PopulateClassTeacher()
            PopulateGrade()
            If ddlGrade.Items.Count <> 0 Then
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            End If
            If ddlShift.Items.Count <> 0 Then
                ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
            End If
            PopulateSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            SaveData()
            ViewState("datamode") = "none"

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(False)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ViewState("datamode") = "edit"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
            EnableDisableControls(True)
            lblError.Text = ""
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfSEC_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                EnableDisableControls(False)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Try
            If ddlStream.Items.Count <> 0 Then
                PopulateSection()
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            If ddlAcademicYear.Items.Count <> 0 Then
                PopulateGrade()
            End If
            If ddlGrade.Items.Count <> 0 Then
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
            End If
            If ddlShift.Items.Count <> 0 Then
                ddlStream = studClass.PopulateGradeStream(ddlStream, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
            End If
            If ddlStream.Items.Count <> 0 Then
                PopulateSection()
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "Private Method"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub PopulateGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder" _
                                  & " FROM grade_bsu_m,grade_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id " _
                                  & "AND grm_acd_id=" + ddlAcademicYear.SelectedValue + " ORDER BY grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub


    Sub PopulateSection()
        ddlSection.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT sct_descr,sct_id FROM section_m " _
                    & " WHERE sct_descr<>'TEMP' and sct_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " AND sct_bsu_id='" + Session("Sbsuid") + "'" _
                    & " AND sct_grm_id=" + ddlStream.SelectedValue + " and sct_id not in(select sec_sct_id from SECTION_BlueBook_Checker    )" _
                    & " order by sct_descr"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "sct_descr"
        ddlSection.DataValueField = "sct_id"
        ddlSection.DataBind()
    End Sub

    Sub PopulateClassTeacher()
        ddlClassTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                 & "emp_id FROM employee_m WHERE emp_status<>4 AND emp_bactive=1 AND emp_ect_id NOT IN (2,8) and emp_bsu_id='" + Session("sBsuid") + "' order by emp_fname,emp_mname,emp_lname"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClassTeacher.DataSource = ds
        ddlClassTeacher.DataTextField = "emp_name"
        ddlClassTeacher.DataValueField = "emp_id"
        ddlClassTeacher.DataBind()
    End Sub

    Private Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim transaction As SqlTransaction
        Dim prevId As Integer = Val(hfSEC_ID.Value)
        Dim flagAudit As Integer
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If ViewState("datamode") = "add" Then
                    str_query = "exec saveSTUDBB_CHECKER " + ddlSection.SelectedValue.ToString + "," + ddlClassTeacher.SelectedValue.ToString
                ElseIf ViewState("datamode") = "edit" Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "SECTION_TUTOR_D", "SEC_ID", "SEC_SCT_ID", "SEC_ID=" + hfSEC_ID.Value.ToString)
                    str_query = "exec saveSTUDBB_CHECKER " + ddlSection.SelectedValue.ToString + "," + ddlClassTeacher.SelectedValue.ToString
                End If
                hfSEC_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                If ViewState("datamode") = "add" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SEC_ID(" + hfSEC_ID.Value.ToString + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                ElseIf ViewState("datamode") = "edit" Then
                    If prevId <> 0 Then
                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SEC_ID(" + prevId.ToString + ")", "Edit", Page.User.Identity.Name.ToString, Me.Page)
                    End If
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SEC_ID(" + hfSEC_ID.Value.ToString + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using

    End Sub

    Sub EnableDisableControls(ByVal Value As Boolean)
        ddlAcademicYear.Enabled = Value
        ddlGrade.Enabled = Value
        ddlSection.Enabled = Value
        ddlClassTeacher.Enabled = Value
        ddlStream.Enabled = Value
        ddlShift.Enabled = Value
    End Sub

    Private Function isTutorAdded() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If ViewState("datamode") = "edit" Then

            str_query = "SELECT count(sec_id) FROM SECTION_BlueBook_Checker WHERE sec_grm_id='" + ddlStream.SelectedValue.ToString + "'" _
                      & "AND sec_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and sec_bsu_id='" + Session("sbsuid") + "'" _
                      & "AND sec_emp_id = " + ddlClassTeacher.SelectedValue.ToString + " and sec_id=" + hfSEC_ID.Value
        Else
            str_query = "SELECT count(sec_id) FROM SECTION_BlueBook_Checker WHERE sec_grm_id='" + ddlStream.SelectedValue.ToString + "'" _
                     & "AND sec_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and sec_bsu_id='" + Session("sbsuid") + "'" _
                     & "and sec_sct_id=" + ddlSection.SelectedValue
        End If

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            Return False
        End If

        Return True

    End Function
#End Region

End Class


