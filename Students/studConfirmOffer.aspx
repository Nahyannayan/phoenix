<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="studConfirmOffer.aspx.vb" Inherits="Students_studConfirmOffer" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="lblTitle" runat="server">Confirm</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table  width="100%" >
                    <tr id="tr2" runat="server">
                        <td>
                            <asp:LinkButton ID="lnkDocs" OnClientClick="javascript:return getDocuments();" ForeColor="red" runat="server" Visible="False">Pending Documents</asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="trDoc" runat="server">
                        <td>
                            <table>
                                <tr>
                                    <td colspan="3" >The following documents has to be collected before offer</td>
                                </tr>
                                <tr>
                                    <td ><span class="field-label"> To Collect</span>
                                    <br />
                                        <asp:ListBox ID="lstNotSubmitted" CssClass="matters" Font-Bold="true" runat="server" Font-Size="xx-Small"
                                            SelectionMode="Multiple" Style="overflow: auto"></asp:ListBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnRight" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                            OnClick="btnRight_Click" Text=">>" /><br />
                                        <asp:Button ID="btnLeft" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                            OnClick="btnLeft_Click" Text="<<" />
                                    </td>
                                    <td class="matters">Collected
                                    <br />

                                        <asp:ListBox ID="lstSubmitted" runat="server" CssClass="matters"
                                            SelectionMode="Multiple" Style="overflow: auto"></asp:ListBox>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label"> Possible Duplicate Records</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">

                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Data Found">
                                <PagerStyle BorderStyle="Solid" />
                                <HeaderStyle BorderStyle="Solid" />
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>

                    <tr>
                        <td  align="center" colspan="2" style="color: Maroon;">You are about to offer admission to this candidate.Do you want to proceed?
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <br />
                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CausesValidation="False"></asp:Button>
                            <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CausesValidation="False"></asp:Button>
                            <asp:HiddenField ID="hfURL" runat="server" />
                            <asp:HiddenField ID="HiddenEQSID" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
