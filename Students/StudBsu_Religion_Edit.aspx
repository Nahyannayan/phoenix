<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudBsu_Religion_Edit.aspx.vb" Inherits="Students_StudBsu_Religion_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
        function getReligion(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/ShowAcademicInfo.aspx?id=' + mode;

            if (mode == 'R') {
                //result = window.showModalDialog(url,"", sFeatures);
                var oWnd = radopen(url, "pop_rel");
                <%--if (result=='' || result==undefined)
            {   
             return false; 
            }   
             NameandCode = result.split('___');  
             document.getElementById("<%=txtReligion.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfReligion.ClientID %>").value=NameandCode[1];
             }--%>
            }
        }

          function OnClientClose(oWnd, args) {
          
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Acad.split('||');
                      document.getElementById("<%=txtReligion.ClientID %>").value=NameandCode[0];
                      document.getElementById("<%=hfReligion.ClientID %>").value=NameandCode[1];
                    __doPostBack('<%= txtReligion.ClientID%>', 'TextChanged');
            }
         }

 

            function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        } 
          
      </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user"></i>  <asp:Literal id="ltLabel" runat="server" Text="Set Religion"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    

    <table id="tbl_AddGroup" runat="server" width="100%" align="center" cellpadding="0" cellspacing="0" >
        <tr>
            <td align="left">
                
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                </span>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" >
                Fields Marked with(<span class="text-danger">*</span>) are mandatory</td>
        </tr>
        <tr>
            <td   valign="top">
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Religion</span>  <font class="text-danger" color="red">*</font></td>
                       
                        <td align="left" width="50%">
                            <asp:TextBox id="txtReligion" runat="server" ></asp:TextBox>
                            <asp:ImageButton ImageUrl="~/Images/forum_search.gif" id="btnReligion" ImageAlign="Middle" runat="server" OnClientClick="getReligion('R');return false;"
                                Text="..." />
                            <asp:RequiredFieldValidator id="rfvReligion" runat="server" ControlToValidate="txtReligion"
                                CssClass="error" Display="Dynamic" ErrorMessage="Please select religion."
                                 ValidationGroup="groupM1">*</asp:RequiredFieldValidator>&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
               
            </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" /><asp:Button id="btnEdit" runat="server" CausesValidation="False"
                        CssClass="button" onclick="btnEdit_Click" Text="Edit" /><asp:Button id="btnSave"
                            runat="server" CssClass="button" onclick="btnSave_Click" Text="Save" ValidationGroup="groupM1" /><asp:Button
                                id="btnCancel" runat="server" CausesValidation="False" CssClass="button" onclick="btnCancel_Click"
                                Text="Cancel" />
            </td>
        </tr>
    </table>

    <asp:HiddenField id="hfReligion" runat="server">
    </asp:HiddenField>

                </div>
            </div>
        </div>

        <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
       </telerik:RadWindowManager>

</asp:Content>

