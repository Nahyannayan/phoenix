﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="stuAddViewNotification.aspx.vb" Inherits="Students_stuAddNotifications" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../Scripts/jquery.ui.timepicker.css" rel="stylesheet" />
     <script type="text/javascript" src="../Scripts/jquery.ui.timepicker.js"></script>
     <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
      <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script> 
    <style>
        .ui-timepicker-table td a{
          width: fit-content!important;
        }

        .ui-widget-header {
            border: 1px solid buttonface!important;
            background: #8DC24C 50% 50% repeat-x!important;
            color: White!important;
            border-style: Solid!important;
        }
        .ui-timepicker {
        background-color:white!important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblheading" runat="server" Text="View Notification" /> </div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <asp:Label ID="lblError" runat="server" />
                    <asp:HiddenField ID="viewidUpdate" runat="server" />
                    <table width="100%">
                        <tbody>
                           
                            <tr>
                                <td colspan="4" align="left">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="button" OnClick="LinkButton1_Click" >Add New</asp:LinkButton> 
                                 </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="title-bg">
                                    <asp:Label ID="lblgridviewheader" runat="server" Text="Logged Activities" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="gridAllNotificationsList" runat="server" PageSize="10" AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Data" CssClass="table table-bordered table-row"
                                        OnPageIndexChanging="gridAllNotificationsList_PageIndexChanging"  OnRowDataBound="gridAllNotificationsList_RowDataBound">
                                        <Columns>
                                           <%-- <asp:BoundField DataField="ViewId" ControlStyle-CssClass="hidefield" ItemStyle-CssClass="hidefield" FooterStyle-CssClass="hidefield" HeaderStyle-CssClass="hidefield"></asp:BoundField>
                                            --%>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblViewId" runat="server" Text='<%#Eval("ViewId") %>' Visible="false" /> 
                                                    <asp:Label ID="lblNLD_ISREAD_FLAG" runat="server" Text='<%#Eval("NLD_ISREAD_FLAG") %>' Visible="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="SlNo" HeaderText="Sr.No.">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AcademicYear" HeaderText="Academic Year">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NotificationText" HeaderText="Notification Text">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Grades" HeaderText="Grades">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NotificationDate" HeaderText="Notification Date">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CATEGORY" HeaderText="Category">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:ButtonField ButtonType="Link" runat="server" CommandName="btnUpdateCommand" Text="Update" Visible="false"  />
                                            <asp:ButtonField ButtonType="Link" runat="server" CommandName="btnDeleteCommand" Text="Delete"  Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
      
    </div>
    
</asp:Content>
