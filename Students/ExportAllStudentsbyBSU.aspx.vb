﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Students_ExportAllStudentsbyBSU
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try
                If Session("sUsr_name") Is Nothing OrElse Session("sUsr_name").ToString = "" Then
                    Response.Redirect("~\noAccess.aspx")
                End If

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Dim isPD_SuperUser As Boolean = Check_PD_SuperUser()

                    'If Not isPD_SuperUser Then
                    '    tblNoAccess.Visible = True
                    '    tblData.Visible = False
                    '    'tblGridView.Visible = False
                    '    Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                    '    lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                    'End If

                    bindAcademic_Year_Filter()



                End If

                'show all requests by default


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        Try
            DownloadStudInfoExcel()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub
    Private Sub bindAcademic_Year_Filter()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@INFO_TYPE", "ACD_CURR_PREVIOUS")
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@CLM_ID", Session("CLM"))


            Using YEAR_DESCRreader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "SM.GETACADEMIC_DATABIND", PARAM)
                ddlAcdYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then

                    ddlAcdYear.DataSource = YEAR_DESCRreader
                    ddlAcdYear.DataTextField = "ACY_DESCR"
                    ddlAcdYear.DataValueField = "ACD_ID"
                    ddlAcdYear.DataBind()


                End If
            End Using

            If Not Session("Current_ACD_ID") Is Nothing Then
                If Not ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                    ddlAcdYear.ClearSelection()
                    ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            End If
            'ddlAcdYearr_SelectedIndexChanged(ddlFilterYear, Nothing)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub DownloadStudInfoExcel()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
           
            Dim param(4) As SqlClient.SqlParameter
            Dim ds As DataSet



           


            param(0) = New SqlClient.SqlParameter("@ACDID", ddlAcdYear.SelectedItem.Value)
            param(1) = New SqlClient.SqlParameter("@userId", Session("sUsr_name"))
            param(2) = New SqlClient.SqlParameter("@BSUID", Session("sBsuid"))
            param(3) = New SqlClient.SqlParameter("@LASTDATE", txtDate.Text)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[STU].[GET_ALL_STUDENTS_BY_ACD_AND_DATE]", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else


                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)


                Dim datesel As DateTime

                datesel = Convert.ToDateTime(txtDate.Text)
                Dim excelfiledate As String = datesel.ToShortDateString().ToString().Replace("/", "_").Replace(" ", "").Replace(":", "")
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")

                'Dim dt As String = txtDate.Text.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim bsu As String = Session("BSU_Name").ToString.Replace(" ", "")
                Dim stuFilename As String = bsu & excelfiledate & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                ''removing unwanted columns
                ''  ws.Columns("A").Delete()
                ''ws.Columns("C").Delete()



              

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
         

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
