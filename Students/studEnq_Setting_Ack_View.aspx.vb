Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studEnq_Setting_Ack_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100064") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"

                    callYEAR_DESCRBind()
                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvOnlineEnq_Ack.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvOnlineEnq_Ack.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
   
    Public Function returnpath(ByVal p_posted) As String
        Try
            If p_posted = True Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function
    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id in('" & Session("sBsuid") & "') and acd_clm_id in('" & Session("CLM") & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try



            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_GRM_DIS As String = String.Empty
          
            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            Dim ds As New DataSet

            str_Sql = " Select * from(SELECT distinct ENQUIRY_ACK_SETTING.EAS_ID, ENQUIRY_ACK_SETTING.[EAS_ACD_ID],  ENQUIRY_ACK_SETTING.EAS_MAIN_bSHOW as Main_bShow," & _
 " SUBSTRING(ENQUIRY_ACK_SETTING.EAS_TEXT_MAIN, 1, 40)   + '...' AS EAS_TEXT_MAIN,  ENQUIRY_ACK_SETTING.EAS_SUB_bSHOW as Sub_bShow, " & _
" (SELECT  COUNT(EAD_ID) AS Doc_list  FROM  ENQUIRY_ACK_DOC   WHERE  EAD_EAS_ID = ENQUIRY_ACK_SETTING.EAS_ID) AS DOC_Count, " & _
" GRADE_BSU_M.GRM_DISPLAY as GRM_DIS, GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER FROM ENQUIRY_ACK_SETTING INNER JOIN  GRADE_BSU_M " & _
" ON ENQUIRY_ACK_SETTING.EAS_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND ENQUIRY_ACK_SETTING.EAS_ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
"  INNER JOIN   GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID) A where A.[EAS_ACD_ID]='" & ACD_ID & "'"



            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String

            Dim str_GRM_DIS As String = String.Empty
          
            If gvOnlineEnq_Ack.Rows.Count > 0 Then

                Dim str_Sid_search() As String



                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvOnlineEnq_Ack.HeaderRow.FindControl("txtGRM_DIS")
                str_GRM_DIS = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRM_DIS = "  AND  NOT a.GRM_DIS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS NOT LIKE '%" & txtSearch.Text & "'"
                End If


              




            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_GRM_DIS & " ORDER BY a.GRD_DISPLAYORDER")

            If ds.Tables(0).Rows.Count > 0 Then

                gvOnlineEnq_Ack.DataSource = ds.Tables(0)
                gvOnlineEnq_Ack.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvOnlineEnq_Ack.DataSource = ds.Tables(0)
                Try
                    gvOnlineEnq_Ack.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvOnlineEnq_Ack.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvOnlineEnq_Ack.Rows(0).Cells.Clear()
                gvOnlineEnq_Ack.Rows(0).Cells.Add(New TableCell)
                gvOnlineEnq_Ack.Rows(0).Cells(0).ColumnSpan = columnCount
                gvOnlineEnq_Ack.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvOnlineEnq_Ack.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            'txtSearch = gvOnlineEnq_Ack.HeaderRow.FindControl("txtACY_DESCR")
            'txtSearch.Text = str_ACY_DESCR

            txtSearch = gvOnlineEnq_Ack.HeaderRow.FindControl("txtGRM_DIS")
            txtSearch.Text = str_GRM_DIS
       

            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
   
   
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\StudEnq_Setting_Ack_edit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblEAS_ID As New Label
            Dim url As String
            Dim viewid As String
            lblEAS_ID = TryCast(sender.FindControl("lblEAS_ID"), Label)
            viewid = lblEAS_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\StudEnq_Setting_Ack_edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGRD_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

 

    
End Class
