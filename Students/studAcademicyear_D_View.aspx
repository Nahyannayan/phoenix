<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAcademicyear_D_View.aspx.vb" Inherits="Students_studAcademicyear_D_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <style>
        input{
            vertical-align:middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Academic"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" colspan="2"  valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr >
                        <td align="left" valign="middle">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" >Add New</asp:LinkButton></td>
                        <td align="right" colspan="1" style="text-decoration: underline" valign="middle">
                            <asp:RadioButton ID="rbCurrent" runat="server" CssClass="field-label" AutoPostBack="True" 
                                GroupName="Current" Text="Current" />
                            <asp:RadioButton ID="rbDated" runat="server" CssClass="field-label" AutoPostBack="True" 
                                GroupName="Current" Text="Dated" />
                            <asp:RadioButton ID="rbAll" runat="server"
                                    AutoPostBack="True"  CssClass="field-label" GroupName="Current" Text="All" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="middle">
                            <asp:GridView ID="gvAcademic" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="ACD_ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("ACD_ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblACD_ID" runat="server" Text='<%# Bind("ACD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Y_DESCR">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Y_DESCR") %>' ></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                             Academic Year
                                                        <asp:DropDownList ID="ddlY_DESCRH" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlACY_DESCRH_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                            <%--<table style="width: 100%">
                                                <tr>
                                                    <td align="center"></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                       </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblY_DESCR" runat="server" Text='<%# Bind("Y_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C_DESCR">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("C_DESCR") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblC_DESCRH" runat="server" CssClass="gridheader_text" Text="Curriculum"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtC_DESCR" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchC_DESCR" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchC_DESCR_Click"  />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        <HeaderStyle Wrap="False" VerticalAlign="Middle"/>
                                        <ItemTemplate>
                                            <asp:Label ID="lblC_DESCR" runat="server" Text='<%# Bind("C_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STARTDT">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("STARTDT") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSTARTDTH" runat="server" CssClass="gridheader_text"
                                                Text="Start Date"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtSTARTDT" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSTARTDT" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchSTARTDT_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        <HeaderStyle Wrap="False" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTARTDT" runat="server" Text='<%# Bind("STARTDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ENDDT">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ENDDT") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblENDDTH" runat="server" CssClass="gridheader_text" Text="End Date"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtENDDT" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchENDDT" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchENDDT_Click" />
                                        </HeaderTemplate >
                                        <ControlStyle Width="45%" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblENDDT" runat="server" Text='<%# Bind("ENDDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OPENONLINE">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("OPENONLINE") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Online Reg<br />
                                                        <asp:DropDownList ID="ddlOPENONLINEH" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlOPENONLINEH_SelectedIndexChanged" >
                                                            <asp:ListItem>All</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                        </asp:DropDownList>
                                           <%-- <table style="width: 100%">
                                                <tr>
                                                    <td align="center"></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            &nbsp;<asp:CheckBox ID="chkOPENONLINE" runat="server" Checked='<%# Bind("OPENONLINE") %>'
                                                Enabled="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

