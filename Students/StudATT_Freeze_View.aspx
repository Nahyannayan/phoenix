<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudATT_Freeze_View.aspx.vb" Inherits="Students_StudATT_Freeze_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Freeze Attendance Date "></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"  >
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr  >
                                    <td align="left" class="matters" colspan="9" valign="top">
                                        <asp:GridView ID="gvAutoATT" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Academic Year">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblDescription_H" runat="server" Text="Academic Year"
                                                                            CssClass="gridheader_text" __designer:wfdid="w51"></asp:Label> <br />
                                                                                        <asp:TextBox ID="txtACY_DESCR" runat="server"  
                                                                                            __designer:wfdid="w7"></asp:TextBox> 
                                                                                        <asp:ImageButton ID="btnSearchACY_DESCR" runat="server"
                                                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                                                            __designer:wfdid="w8" OnClick="btnSearchACY_DESCR_Click"></asp:ImageButton> 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACY_DESCR" runat="server" Text='<%# Bind("ACY_DESCR") %>'
                                                            ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle   />

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Month">
                                                    <EditItemTemplate>
                                                   
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblGradeH"
                                                                            runat="server" Text="Month" CssClass="gridheader_text" __designer:wfdid="w42"></asp:Label> <br />
                                                                                        <asp:TextBox ID="txtMth" runat="server"   __designer:wfdid="w43"></asp:TextBox> 
                                                                                        <asp:ImageButton ID="btnSearchMth" OnClick="btnSearchMth_Click" runat="server"
                                                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w44"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblMth" runat="server" Text='<%# Bind("Mth") %>'
                                                            __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" VerticalAlign="Middle"
                                                    ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Freeze Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblToDateH"
                                                                            runat="server" Text="Freeze Date" CssClass="gridheader_text"
                                                                           ></asp:Label><br />
                                                                                        <asp:TextBox ID="txtFAD_DT" runat="server"    ></asp:TextBox> 
                                                                                        <asp:ImageButton ID="btnSearchFAD_DT" OnClick="btnSearchFAD_DT_Click"
                                                                                            runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                                                            ></asp:ImageButton> 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFAD_DT" runat="server" Text='<%# Bind("FAD_DT") %>'
                                                            __designer:wfdid="w45"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle   />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grades">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrades" runat="server" Text='<%# Bind("Grades") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle   />
                                                    <ItemStyle  />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="FAD_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFAD_ID" runat="server" Text='<%# Bind("FAD_ID") %>'
                                                            __designer:wfdid="w54"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

