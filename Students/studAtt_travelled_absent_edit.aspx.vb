﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Globalization
Partial Class Students_studAtt_travelled_absent_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")



        If Page.IsPostBack = False Then

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050117") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                bindSchool()
                ViewState("MPA_ID") = "0"
                Session("NOTIFY_MAIL") = CreateDataTable()
                Session("NOTIFY_MAIL").Rows.Clear()
                ViewState("id") = 1


                If ViewState("datamode") = "view" Then

                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    ddlBSU_ID.ClearSelection()
                    ddlBSU_ID.Items.FindByValue(ViewState("viewid")).Selected = True
                    ddlBSU_ID_SelectedIndexChanged(ddlBSU_ID, Nothing)

                    Call SetState()
                    btnAddGrd.Visible = True
                    btnAddGrd.Enabled = False

                    gvEMP_NAME.Enabled = False
                ElseIf ViewState("datamode") = "add" Then

                    txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                    ddlBSU_ID.ClearSelection()
                    ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True
                    ddlBSU_ID_SelectedIndexChanged(ddlBSU_ID, Nothing)

                    btnAddGrd.Visible = True
                    btnAddGrd.Enabled = True
                    Call ResetState()
                    gvEMP_NAME.Enabled = True
                    clearContent()
                End If

                btnUpdateGrd.Visible = False
                btnCancelGRD.Visible = False

            End If
        End If

    End Sub
    Sub bindMain()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = " select MTA_ID,MTA_BSU_ID,replace(convert(varchar(12),MTA_FROMDT, 106),' ','/') as FROMDT," & _
" replace(convert(varchar(12),MTA_TODT ,106),' ','/') as TODT,MTA_SCHEDULE from ATT.MAIL_TRAVEL_ABSENT_ATT where MTA_BSU_ID='" & ViewState("viewid") & "'"


        Using mailreader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            If mailreader.HasRows Then

                While mailreader.Read
                    ViewState("MPA_ID") = Convert.ToString(mailreader("MTA_ID"))
                    'ddlBSU_ID.ClearSelection()
                    'ddlBSU_ID.Items.FindByValue(Convert.ToString(mailreader("MPA_BSU_ID"))).Selected = True
                    'ddlBSU_ID_SelectedIndexChanged(ddlBSU_ID, Nothing)
                    txtFrom.Text = Convert.ToString(mailreader("FROMDT"))
                    txtTo.Text = Convert.ToString(mailreader("TODT"))
                    ddlSCHEDULE.ClearSelection()
                    If Not ddlSCHEDULE.Items.FindByValue(Convert.ToString(mailreader("MTA_SCHEDULE"))) Is Nothing Then
                        ddlSCHEDULE.Items.FindByValue(Convert.ToString(mailreader("MTA_SCHEDULE"))).Selected = True
                    End If


                End While
            Else
                ViewState("MPA_ID") = 0

            End If

        End Using
    End Sub

    Sub binddata()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_sql As String = " SELECT     N.NMTA_ID, N.NMTA_MTA_ID, N.NMTA_EMP_ID, E.EMP_FNAME + ' '+ isnull(E.EMP_LNAME,' ' )+ ' '+isnull(E.EMP_MNAME, '') AS NMTA_EMP_NAME,'OLD' AS STATUS " & _
" FROM ATT.NOTIFY_TRAVEL_ABSENT_ATT AS N INNER JOIN EMPLOYEE_M AS E ON N.NMTA_EMP_ID = E.EMP_ID where N.NMTA_MTA_ID='" & ViewState("MPA_ID") & "'"

        Dim gvDataset As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)


        If gvDataset.Tables(0).Rows.Count > 0 Then

            Session("NOTIFY_MAIL").Rows.Clear()
            ViewState("id") = 1

            For iIndex As Integer = 0 To gvDataset.Tables(0).Rows.Count - 1

                Dim rDt As DataRow
                rDt = Session("NOTIFY_MAIL").NewRow
                rDt("ID") = ViewState("id")
                rDt("NMTA_ID") = gvDataset.Tables(0).Rows(iIndex)("NMTA_ID")
                rDt("NMTA_MTA_ID") = gvDataset.Tables(0).Rows(iIndex)("NMTA_MTA_ID")
                rDt("NMTA_EMP_ID") = gvDataset.Tables(0).Rows(iIndex)("NMTA_EMP_ID")
                rDt("NMTA_EMP_NAME") = gvDataset.Tables(0).Rows(iIndex)("NMTA_EMP_NAME")
                rDt("STATUS") = gvDataset.Tables(0).Rows(iIndex)("STATUS")
                ViewState("id") = ViewState("id") + 1
                Session("NOTIFY_MAIL").Rows.Add(rDt)
            Next
        End If
        gridbind()
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim NMTA_ID As New DataColumn("NMTA_ID", System.Type.GetType("System.String"))
            Dim NMTA_MTA_ID As New DataColumn("NMTA_MTA_ID", System.Type.GetType("System.String"))
            Dim NMTA_EMP_ID As New DataColumn("NMTA_EMP_ID", System.Type.GetType("System.String"))
            Dim NMTA_EMP_NAME As New DataColumn("NMTA_EMP_NAME", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(NMTA_ID)
            dtDt.Columns.Add(NMTA_MTA_ID)
            dtDt.Columns.Add(NMTA_EMP_ID)
            dtDt.Columns.Add(NMTA_EMP_NAME)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

            Return dtDt
        End Try
    End Function
    Sub gridbind()
        Try


            Dim dtTempROOM_SET As New DataTable
            dtTempROOM_SET = CreateDataTable()
            ViewState("id") = 1
            If Session("NOTIFY_MAIL").Rows.Count > 0 Then
                For i As Integer = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
                    If Session("NOTIFY_MAIL").Rows(i)("STATUS") & "" <> "DELETED" Then
                        Dim rDt As DataRow
                        rDt = dtTempROOM_SET.NewRow
                        rDt("ID") = ViewState("id")
                        rDt("NMTA_ID") = Session("NOTIFY_MAIL").Rows(i)("NMTA_ID")
                        rDt("NMTA_MTA_ID") = Session("NOTIFY_MAIL").Rows(i)("NMTA_MTA_ID")
                        rDt("NMTA_EMP_ID") = Session("NOTIFY_MAIL").Rows(i)("NMTA_EMP_ID")
                        rDt("NMTA_EMP_NAME") = Session("NOTIFY_MAIL").Rows(i)("NMTA_EMP_NAME")
                        rDt("STATUS") = Session("NOTIFY_MAIL").Rows(i)("STATUS")


                        dtTempROOM_SET.Rows.Add(rDt)
                        ViewState("id") = ViewState("id") + 1
                    End If
                Next

            End If

            gvEMP_NAME.DataSource = dtTempROOM_SET
            gvEMP_NAME.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvEMP_NAME_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEMP_NAME.RowDeleting
        Dim COND_ID As Integer = CInt(gvEMP_NAME.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0

        For i = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
            If Session("NOTIFY_MAIL").rows(i)("ID") = COND_ID Then
                If Session("NOTIFY_MAIL").rows(i)("NMTA_ID") = "0" Then
                    Session("NOTIFY_MAIL").Rows(i).Delete()
                Else
                    Session("NOTIFY_MAIL").Rows(i)("STATUS") = "DELETED"
                End If
                Exit For
            End If
        Next

        gridbind()
    End Sub

    Protected Sub btnAddGRD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGrd.Click

        Try
            For i As Integer = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
                If (Trim(Session("NOTIFY_MAIL").Rows(i)("NMTA_EMP_ID")) = ddlEMP.SelectedValue) And (Session("NOTIFY_MAIL").Rows(i)("STATUS") & "" <> "DELETED") Then
                    lblError_grid.Text = "Duplicate entry not allowed !!!"
                    Exit Sub
                End If
            Next
            Dim rDt As DataRow
            rDt = Session("NOTIFY_MAIL").NewRow
            rDt("ID") = ViewState("id")
            rDt("NMTA_ID") = ""
            rDt("NMTA_MTA_ID") = ViewState("MPA_ID")
            rDt("NMTA_EMP_ID") = ddlEMP.SelectedValue
            rDt("NMTA_EMP_NAME") = ddlEMP.SelectedItem.Text

            rDt("STATUS") = "ADD"
            ViewState("id") = ViewState("id") + 1
            Session("NOTIFY_MAIL").Rows.Add(rDt)
            gridbind()

        Catch ex As Exception
            lblError_grid.Text = "Error in adding new record"
        End Try


    End Sub

    Protected Sub btnUpdateGRDGRD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateGrd.Click
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim row As GridViewRow = gvEMP_NAME.Rows(gvEMP_NAME.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        iIndex = CInt(idRow.Text)

        For iEdit = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
            If iIndex <> Session("NOTIFY_MAIL").Rows(iEdit)("ID") Then
                If (Trim(Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_EMP_ID")) = ddlEMP.SelectedValue) And (Session("NOTIFY_MAIL").Rows(iEdit)("STATUS") & "" <> "DELETED") Then
                    lblError_grid.Text = "Duplicate entry not allowed !!!"
                    Exit Sub
                End If
            End If
        Next

        For iEdit = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
            If iIndex = Session("NOTIFY_MAIL").Rows(iEdit)("ID") Then
                Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_ID") = hfNMPA_ID.Value
                Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_MTA_ID") = ViewState("MPA_ID")
                Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_EMP_ID") = ddlEMP.SelectedValue
                Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_EMP_NAME") = ddlEMP.SelectedItem.Text



                If hfNMPA_ID.Value = "" Then
                    Session("NOTIFY_MAIL").Rows(iEdit)("STATUS") = "ADD"
                Else
                    Session("NOTIFY_MAIL").Rows(iEdit)("STATUS") = "UPDATE"
                End If

                Exit For
            End If
        Next

        btnAddGrd.Visible = True
        btnAddGrd.Enabled = True
        btnUpdateGrd.Visible = False
        btnCancelGRD.Visible = False
        btnSave.Enabled = True
        gvEMP_NAME.SelectedIndex = -1
        gridbind()
        gvEMP_NAME.Columns(2).Visible = True
        gvEMP_NAME.Columns(3).Visible = True



    End Sub

    Protected Sub gvEMP_NAME_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEMP_NAME.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DeleteBtn As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
            DeleteBtn.OnClientClick = "if(!confirm('Are you sure you want to delete this?')) return false;"
        End If
    End Sub

    Protected Sub gvEMP_NAME_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvEMP_NAME.RowEditing
        btnSave.Enabled = False
        gvEMP_NAME.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvEMP_NAME.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
            If iIndex = Session("NOTIFY_MAIL").Rows(iEdit)("ID") Then
                hfNMPA_ID.Value = Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_ID")

                If Not ddlEMP.Items.FindByValue(Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_EMP_ID")) Is Nothing Then
                    ddlEMP.ClearSelection()
                    ddlEMP.Items.FindByValue(Session("NOTIFY_MAIL").Rows(iEdit)("NMTA_EMP_ID")).Selected = True
                End If

                Exit For
            End If
        Next
        gvEMP_NAME.Columns(2).Visible = False
        gvEMP_NAME.Columns(3).Visible = False
        btnAddGrd.Visible = False
        btnUpdateGrd.Visible = True
        btnCancelGRD.Visible = True
        btnSave.Enabled = False
        gridbind()

    End Sub

    Private Sub bindSchool()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_sql As String = "select bsu_Id,bsu_name from businessunit_M where  (BUS_BSG_ID NOT IN (4, 6, 8, 10, 9)) order by bsu_name"

        Using sqlBSUreader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)

            If sqlBSUreader.HasRows Then
                ddlBSU_ID.DataSource = sqlBSUreader
                ddlBSU_ID.DataTextField = "bsu_name"
                ddlBSU_ID.DataValueField = "bsu_id"
                ddlBSU_ID.DataBind()
            End If

        End Using

    End Sub

    Private Sub bindStaff()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim BSU_id As String = ddlBSU_ID.SelectedValue
        Dim str_sql As String = "SELECT  ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_ID " & _
" FROM EMPLOYEE_M  where EMP_STATUS NOT IN(4,8,7,5) AND EMP_bACTIVE=1 and EMP_BSU_ID='" & BSU_ID & "' and EMP_DES_ID in(select ATC_DES_ID from AUTHORIZED_CATEGORY where ATC_BSU_ID='" & BSU_ID & "')  order by EMP_NAME "

        Using sqlstaffreader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)

            If sqlstaffreader.HasRows Then
                ddlEMP.DataSource = sqlstaffreader
                ddlEMP.DataTextField = "EMP_NAME"
                ddlEMP.DataValueField = "EMP_ID"
                ddlEMP.DataBind()
                btnAddGrd.Enabled = True
            Else
                ddlEMP.Items.Clear()
                btnAddGrd.Enabled = False
            End If

        End Using

    End Sub

    Sub SetState()

        ddlSCHEDULE.Enabled = False

        ddlBSU_ID.Enabled = False

        gvEMP_NAME.Enabled = False
        txtFrom.Enabled = False
        txtTo.Enabled = False


    End Sub

    Sub ResetState()

        ddlSCHEDULE.Enabled = True

        ddlBSU_ID.Enabled = True


        txtFrom.Enabled = True
        txtTo.Enabled = True

    End Sub
    Sub clearContent()

        ddlSCHEDULE.ClearSelection()
        ddlSCHEDULE.SelectedIndex = 2
        Session("NOTIFY_MAIL").Rows.Clear()
        gridbind()
        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

        ddlBSU_ID.ClearSelection()
        ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True

    End Sub

    Sub bindAcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlBSU_ID.Items.Clear()
            ddlBSU_ID.DataSource = ds.Tables(0)
            ddlBSU_ID.DataTextField = "ACY_DESCR"
            ddlBSU_ID.DataValueField = "ACD_ID"
            ddlBSU_ID.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty
        Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFrom.Text)
        Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Dim errorMessage As String = String.Empty
        If serverDateValidate() = "0" Then

            If t1 < t2 Then
                lblError.Text = "You cannot select a day less than today!"
                txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Else

                str_err = CallTransaction(errorMessage)

                If str_err = "0" Then

                    lblError.Text = "Record updated successfully"
                Else
                    lblError.Text = "Error while updating record!!!"
                End If

            End If



        End If

    End Sub



    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim status As String = String.Empty
        Dim str As String = String.Empty



        Dim MPA_FROMDT As String = txtFrom.Text
        Dim MPA_TODT As String = txtTo.Text
        Dim MPA_SCHEDULE As String = ddlSCHEDULE.SelectedValue
        Dim bEdit As Boolean
        'check the data mode to do the required operation

        If ViewState("datamode") = "add" Then
            'only insert if applicable to particular grade & section

            Dim transaction As SqlTransaction
            bEdit = False
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    status = ""

                    For i As Integer = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
                        If (Session("NOTIFY_MAIL").Rows(i)("STATUS") = "ADD") Or (Session("NOTIFY_MAIL").Rows(i)("STATUS") = "UPDATE") Then


                            str += String.Format("<PENDING NMTA_ID='{0}' NMTA_EMP_ID='{1}' STATUS='{2}' />", Session("NOTIFY_MAIL").Rows(i)("NMTA_ID"), _
                                             Session("NOTIFY_MAIL").Rows(i)("NMTA_EMP_ID"), _
                                           Session("NOTIFY_MAIL").Rows(i)("STATUS"))

                        End If
                    Next

                    If str <> "" Then
                        str = "<MAIL_PENDING>" + str + "</MAIL_PENDING>"
                    End If


                    Dim pParms(12) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STR", str)
                    pParms(1) = New SqlClient.SqlParameter("@MTA_ID", ViewState("MPA_ID"))
                    pParms(2) = New SqlClient.SqlParameter("@MTA_BSU_ID", ddlBSU_ID.SelectedValue)
                    pParms(3) = New SqlClient.SqlParameter("@MTA_FROMDT", MPA_FROMDT)
                    pParms(4) = New SqlClient.SqlParameter("@MTA_TODT", MPA_TODT)
                    pParms(5) = New SqlClient.SqlParameter("@MTA_SCHEDULE", MPA_SCHEDULE)
                    pParms(6) = New SqlClient.SqlParameter("@MTA_EDITBY", Session("sUsr_name"))


                    pParms(7) = New SqlClient.SqlParameter("@bEdit", bEdit)
                    pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(8).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEMAIL_TRAVEL_ABSENT_ATT]", pParms)
                    Dim ReturnFlag As Integer = pParms(8).Value




                    If ReturnFlag <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error occured while processing info."
                        status = "1"
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                    Call SetState()
                    Call clearContent()

                Catch ex As Exception

                    errorMessage = ex.Message
                Finally
                    If CallTransaction <> "0" Then
                        errorMessage = "Error occured while processing info."
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using




        ElseIf ViewState("datamode") = "edit" Then

            Dim transaction As SqlTransaction
            bEdit = True
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    status = ""

                    For i As Integer = 0 To Session("NOTIFY_MAIL").Rows.Count - 1
                        If (Session("NOTIFY_MAIL").Rows(i)("STATUS") = "ADD") Or (Session("NOTIFY_MAIL").Rows(i)("STATUS") = "UPDATE") Then




                            str += String.Format("<PENDING NMTA_ID='{0}' NMTA_EMP_ID='{1}' STATUS='{2}' />", Session("NOTIFY_MAIL").Rows(i)("NMTA_ID"), _
                                             Session("NOTIFY_MAIL").Rows(i)("NMTA_EMP_ID"), _
                                           Session("NOTIFY_MAIL").Rows(i)("STATUS"))

                        ElseIf (Session("NOTIFY_MAIL").rows(i)("NMTA_ID") <> "0") And (Session("NOTIFY_MAIL").Rows(i)("STATUS") = "DELETED") Then


                            str += String.Format("<PENDING NMTA_ID='{0}' NMTA_EMP_ID='{1}' STATUS='{2}' />", Session("NOTIFY_MAIL").Rows(i)("NMTA_ID"), _
                                             Session("NOTIFY_MAIL").Rows(i)("NMTA_EMP_ID"), _
                                           Session("NOTIFY_MAIL").Rows(i)("STATUS"))
                        End If
                    Next

                    If str <> "" Then
                        str = "<MAIL_PENDING>" + str + "</MAIL_PENDING>"
                    End If


                    Dim pParms(12) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STR", str)
                    pParms(1) = New SqlClient.SqlParameter("@MTA_ID", ViewState("MPA_ID"))
                    pParms(2) = New SqlClient.SqlParameter("@MTA_BSU_ID", ddlBSU_ID.SelectedValue)
                    pParms(3) = New SqlClient.SqlParameter("@MTA_FROMDT", MPA_FROMDT)
                    pParms(4) = New SqlClient.SqlParameter("@MTA_TODT", MPA_TODT)
                    pParms(5) = New SqlClient.SqlParameter("@MTA_SCHEDULE", MPA_SCHEDULE)
                    pParms(6) = New SqlClient.SqlParameter("@MTA_EDITBY", Session("sUsr_name"))


                    pParms(7) = New SqlClient.SqlParameter("@bEdit", bEdit)
                    pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(8).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEMAIL_TRAVEL_ABSENT_ATT]", pParms)
                    Dim ReturnFlag As Integer = pParms(8).Value

                    If ReturnFlag <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error occured while processing info."
                        status = "1"
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                    Call SetState()
                    Call clearContent()

                Catch ex As Exception

                    errorMessage = ex.Message
                Finally
                    If CallTransaction <> "0" Then
                        errorMessage = "Error occured while processing info."
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using


        End If

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Call SetState()
            Call clearContent()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Call ResetState()
        clearContent()
        gvEMP_NAME.Enabled = True
        ViewState("datamode") = "add"
        btnAddGrd.Enabled = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        gvEMP_NAME.Enabled = True
        btnAddGrd.Enabled = True
        Call ResetState()
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub ddlBSU_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU_ID.SelectedIndexChanged
        ViewState("viewid") = ddlBSU_ID.SelectedValue
        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Session("NOTIFY_MAIL").Rows.Clear()
        gridbind()
        bindStaff()
        bindMain()
        binddata()

        If Session("NOTIFY_MAIL").Rows.Count > 0 Then
            ViewState("datamode") = "edit"
        Else
            ViewState("datamode") = "add"
        End If

    End Sub
End Class
