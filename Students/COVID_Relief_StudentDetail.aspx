﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="COVID_Relief_StudentDetail.aspx.vb" Inherits="Students_COVID_Relief_StudentDetail" %>

<html>

<body>
    <form id="frm1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </ajaxToolkit:ToolkitScriptManager>
                <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/cssfiles/sb-admin.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css")%>' rel="stylesheet" />
        <script src='<%= ResolveUrl("~/vendor/jquery/jquery.min.js")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.pack.js?1=2")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.js?1=2")%>'></script>
        <link type="text/css" href='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.css?1=2")%>' rel="stylesheet" />
        <div class="col-12 badge-light col-12 h5">
            Primary Contact Details
        </div>
        <div class="col-12 row">
            <div class="col-4 col-sm-4">
                Contact Person Name:
                  <div>
                      <asp:Label runat="server" CssClass="form-control" ID="lblContactName"></asp:Label></div>
            </div>
            <div class="col-4 col-sm-4">
                Mobile:
                  <div>
                      <asp:Label runat="server" CssClass="form-control" ID="lblMobile"></asp:Label></div>
            </div>
            <div class="col-4 col-sm-4">
                Email:
                  <div>
                      <asp:Label runat="server" CssClass="form-control" ID="lblEmail"></asp:Label></div>
            </div>
        </div>
        <div class="col-12 badge-light col-12 h5 mt-3">
            Student Details
        </div>
        <div class="col-12" style="overflow-x:scroll;">
        <asp:GridView ID="gvApproval" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
            PageSize="20" Width="100%">
            <RowStyle CssClass="griditem" />
            <Columns>
                <asp:TemplateField HeaderText="School Code" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblSchoolCode" runat="server" Text='<%# Bind("BSU_SHORTNAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="School Name">
                    <ItemTemplate>
                        <asp:Label ID="lblSchoolName" runat="server" Text='<%# Bind("BSU_NAME")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Student Id">
                    <ItemTemplate>
                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_FULLNAME")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date of Join">
                    <ItemTemplate>
                        <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("STU_DOJ")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                    <asp:TemplateField HeaderText="Academic Year">
                    <ItemTemplate>
                        <asp:Label ID="lblACD" runat="server" Text='<%# Bind("ACY_DESCR")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                    <asp:TemplateField HeaderText="Grade">
                    <ItemTemplate>
                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("STU_GRADE")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                 
                    <asp:TemplateField HeaderText="Section">
                    <ItemTemplate>
                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("STU_SECTION")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                   <asp:TemplateField HeaderText="Prev. term due">
                    <ItemTemplate>
                        <asp:Label ID="lblPTDue" runat="server" Text='<%# Bind("RRD_PREV_TRM_DUE")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Current term due">
                    <ItemTemplate>
                        <asp:Label ID="lblCTDue" runat="server" Text='<%# Bind("RRD_CURR_TRM_DUE")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                  <asp:TemplateField HeaderText="Current term concession">
                    <ItemTemplate>
                        <asp:Label ID="lblCTConc" runat="server" Text='<%# Bind("RRD_CURR_TRM_CONC")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Concession type">
                    <ItemTemplate>
                        <asp:Label ID="lblConcType" runat="server" Text='<%# Bind("RRD_CONC_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fee Structure">
                    <ItemTemplate>
                        <asp:Label ID="lblFCM_DESC" runat="server" Text='<%# Bind("FCM_DESCR")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Discount %">
                    <ItemTemplate>
                        <asp:Label ID="lblRRH_DISCOUNT_PERCENT" runat="server" Text='<%# Bind("RRH_DISCOUNT_PERCENT")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Discount Amount">
                    <ItemTemplate>
                        <asp:Label ID="lblDiscountAmount" runat="server" Text='<%# Bind("DiscountAmount")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                 <%--<asp:TemplateField HeaderText="Remarks">
                    <ItemTemplate>
                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("RRD_REMARKS")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>--%>
                
            </Columns>
            <SelectedRowStyle BackColor="Wheat" />
            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
            <AlternatingRowStyle CssClass="griditem_alternative" />
        </asp:GridView>
        </div>
        <div class="col-12 badge-light col-12 h5 mt-3">
            <span>Remark</span>
            <asp:TextBox ID="txtMainRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" Rows="4" disabled></asp:TextBox>
        </div>

    </form>
</body>
</html>

