Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studTCEntry_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            pnlSTBooking.Visible = False

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100085" And ViewState("MainMnu_code") <> "S100086" And ViewState("MainMnu_code") <> "S100087" And ViewState("MainMnu_code") <> "S100480") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    txtDocDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                    PopulateGrade()
                    BindTCReason()
                    BindCountry()
                    BindTCType()
                    BindBsu()
                    hfTCM_ID.Value = 0
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    txtGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    txtSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    txtStudID_Fee.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    txtDoJ.Text = Format(Date.Parse(Encr_decrData.Decrypt(Request.QueryString("doj").Replace(" ", "+"))), "dd/MMM/yyyy")
                    BindData()

                    txtName.Enabled = False
                    txtGrade.Enabled = False
                    txtSection.Enabled = False

                    If ViewState("MainMnu_code") = "S100085" Or ViewState("MainMnu_code") = "S100086" Then
                        trPrint.Visible = False
                    Else
                        '    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                        If ViewState("MainMnu_code") <> "S100480" Then
                            hfMaxGrade.Value = Encr_decrData.Decrypt(Request.QueryString("maxgrade").Replace(" ", "+"))
                            hfACY_DESCR.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                            txtIssue.Text = Format(Now.Date, "dd/MMM/yyyy")
                            ddlCountry.Enabled = False
                            ddlReason.Enabled = True
                            ddlTrans_School.Enabled = False
                            ddlTransferType.Enabled = False
                            txtOthers.Enabled = False
                        Else
                            trPrint.Visible = False
                        End If

                        btnSave.ValidationGroup = False
                        btnCancel.Visible = False
                        imgBtnDate.Visible = False
                        imgBtnDocDate.Visible = False
                        imgbtnLeave_date.Visible = False
                        imgBtnLast_date.Visible = False
                        txtDocDate.Enabled = False
                        txtApplyDate.Enabled = False
                        txtDoJ.Enabled = False
                        txtLast_Attend.Enabled = False
                        txtLeave_Date.Enabled = False


                        txtStudID_Fee.Enabled = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindData()
        Dim TcFlag_convert As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_DOCDATE,TCM_REFNO,TCM_APPLYDATE,TCM_LEAVEDATE," _
                                 & " TCM_LASTATTDATE,ISNULL(TCM_REASON,''),TCM_DAYSP,TCM_DAYST,TCM_TCTYPE,ISNULL(TCM_SUBJECTS,'')," _
                                 & " ISNULL(TCM_RESULT,''),ISNULL(TCM_REMARKS,''),TCM_bTFRGEMS,TCM_GEMS_UNIT,TCM_TFRSCHOOL,TCM_TFRCOUNTRY," _
                                 & " TCM_TFRGRADE, isNULL(TCM_TFRZONE,'') as TCM_TFRZONE,TCM_ID,ISNULL(TCM_bPREAPPROVED,'FALSE'),ISNULL(TCM_CONDUCT,''),ISNULL(TCM_FATHERNAME,''),ISNULL(STU_GRD_ID,'') , ISNULL(STU_JOIN_RESULT,'NEW') as STU_JOIN_RESULT" _
                                 & " FROM TCM_M AS A INNER JOIN Student_M C ON TCM_STU_ID=STU_ID LEFT OUTER JOIN GRADE_BSU_M AS B ON A.TCM_GRADE=B.GRM_ID WHERE TCM_STU_ID=" + hfSTU_ID.Value _
                                 & " AND TCM_CANCELDATE IS NULL "
        If ViewState("MainMnu_code") = "S100480" Then
            str_query += " AND TCM_TCSO='SO' AND TCM_TC_ID IS NULL "
            TcFlag_convert = True
        Else
            str_query += " AND TCM_TCSO='TC'"

        End If
        Call getLast_AttDT()

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            If reader.HasRows = True Then
                If TcFlag_convert = True Then
                    ddlTransferType.Enabled = True
                Else
                    ddlTransferType.Enabled = False
                End If

                While reader.Read
                    With reader

                        txtDocDate.Text = Format(.GetDateTime(0), "dd/MMM/yyyy")
                        txtRefNo.Text = .GetString(1)
                        txtApplyDate.Text = Format(.GetDateTime(2), "dd/MMM/yyyy")
                        txtLeave_Date.Text = Format(.GetDateTime(3), "dd/MMM/yyyy")
                        txtLast_Attend.Text = Format(.GetDateTime(4), "dd/MMM/yyyy")
                        If ViewState("MainMnu_code") <> "S100480" Then
                            'ddlReason.ClearSelection()
                            'ddlReason.Items.FindByValue(.GetString(5)).Selected = True
                            txtDay_Present.Text = .GetValue(6)
                            txtTotWork_Day.Text = .GetValue(7)
                            ddlTransferType.Items.FindByValue(.GetValue(8)).Selected = True
                            txtSubject.Text = .GetString(9)
                            If Not ddlResult.Items.FindByValue(reader.GetString(10)) Is Nothing Then
                                ddlResult.ClearSelection()
                                ddlResult.Items.FindByValue(reader.GetString(10)).Selected = True
                            End If
                            txtRemarks.Text = reader.GetString(11)
                            If reader.GetBoolean(12) = True Then
                                ddlTrans_School.ClearSelection()
                                ddlTrans_School.Items.FindByValue(.GetString(13) + "|GEMS").Selected = True
                            Else
                                If Not ddlTrans_School.Items.FindByText(.GetString(14)) Is Nothing Then
                                    ddlTrans_School.Items.FindByText(.GetString(14)).Selected = True
                                Else
                                    txtOthers.Text = .GetString(14)
                                End If
                            End If

                            ddlCountry.ClearSelection()
                            ddlCountry.Items.FindByValue(.GetValue(15)).Selected = True
                            ddlPromoted.ClearSelection()
                            If Not ddlPromoted.Items.FindByValue(.GetString(16)) Is Nothing Then
                                ddlPromoted.Items.FindByValue(.GetString(16)).Selected = True
                            End If
                            txtTranZone.Text = .GetString(17)
                            hfTCM_ID.Value = .GetValue(18)
                            If .GetBoolean(19) = True Then
                                btnSave.Enabled = False
                            End If
                            txtConduct.Text = .GetString(20)
                        End If
                        txtFName.Text = .GetString(21).TrimEnd
                        hfGRD_ID.Value = .GetString(22)

                        If Not ddlJoinResult.Items.FindByValue(Convert.ToString(reader("STU_JOIN_RESULT"))) Is Nothing Then
                            ddlJoinResult.ClearSelection()
                            ddlJoinResult.Items.FindByValue(Convert.ToString(reader("STU_JOIN_RESULT"))).Selected = True
                        End If

                    End With
                End While
            Else
                ddlTransferType.Enabled = True
            End If
        End Using

        If txtFName.Text = "" Then
            str_query = "SELECT ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') FROM " _
                     & " STUDENT_D WHERE STS_STU_ID=(SELECT STU_SIBLING_ID FROM STUDENT_M WHERE STU_ID=" + hfSTU_ID.Value + ")"
            txtFName.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If

        GetTCReason()

    End Sub



    Sub getLast_AttDT()
        Try
            Dim dt As String = String.Empty

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", hfACD_ID.Value)
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", hfSTU_ID.Value)
            If txtLast_Attend.Text <> "" Then
                dt = Format(Date.Parse(txtLast_Attend.Text), "dd/MMM/yyyy")
            End If
            If dt <> "" Then
                '    pParms(2) = New SqlClient.SqlParameter("@LASTDT", DBNull.Value)
                'Else
                pParms(2) = New SqlClient.SqlParameter("@LASTDT", dt)
            End If


            Dim str_date As String = String.Empty
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_ATTDetail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETLAST_ATT_DT_LASTYR]", pParms)

                If readerStudent_ATTDetail.HasRows = True Then
                    While readerStudent_ATTDetail.Read
                        str_date = Convert.ToString(readerStudent_ATTDetail("AVAIL_DATE"))
                        txtDay_Present.Text = Convert.ToString(readerStudent_ATTDetail("tot_prs_days"))
                        txtTotWork_Day.Text = Convert.ToString(readerStudent_ATTDetail("tot_days"))
                    End While
                End If
            End Using
            If txtLast_Attend.Text.Trim = "" Then
                If IsDate(str_date) = True Then

                    txtLast_Attend.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((str_date))).Replace("01/Jan/1900", "")


                Else
                    txtLast_Attend.Text = ""

                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "TC WithDraw")

        End Try

    End Sub
    Protected Sub txtLast_Attend_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLast_Attend.TextChanged
        getLast_AttDT()
    End Sub

    Sub BindBsu()
        Dim str_query As String = "select bsu_name,BSU_ID from (select BSU_ID+'|GEMS' BSU_ID,bsu_name from businessunit_m where (BUS_BSG_ID<>'4') and bsu_id<>'" + Session("sbsu_id") + "' AND BSU_ENQ_GROUP=1 "
        str_query += " union "
        str_query += "SELECT CONVERT(VARCHAR(100),BCC_ID)+'|NONGEMS' ,BCC_NAME FROM BSU_COMPETITORS ) P"
        str_query += " order by bsu_name"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrans_School.DataSource = ds
        ddlTrans_School.DataTextField = "bsu_name"
        ddlTrans_School.DataValueField = "BSU_ID"
        ddlTrans_School.DataBind()
        Dim li As New ListItem
        li.Text = "Others"
        li.Value = "0"
        ddlTrans_School.Items.Insert(0, li)
    End Sub
    Sub PopulateGrade()
        ddlPromoted.Items.Clear()
        Dim acd_ID As String = String.Empty
        If Not Session("next_ACD_ID") Is Nothing Then
            If Session("next_ACD_ID") <> "" Then
                acd_ID = Session("next_ACD_ID")
            Else
                acd_ID = hfACD_ID.Value
            End If

        Else
            acd_ID = hfACD_ID.Value
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A" _
                                 & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                 & " WHERE a.grm_bsu_id='" + Session("sBsuid") + "'" _
                                 & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPromoted.DataSource = ds
        ddlPromoted.DataTextField = "GRM_DISPLAY"
        ddlPromoted.DataValueField = "GRD_ID"
        ddlPromoted.DataBind()
        Dim li As New ListItem
        li.Text = "NA"
        li.Value = "NA"
        ddlPromoted.Items.Insert(0, li)
    End Sub
    Sub BindTCReason()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT [TCR_CODE],[TCR_DESCR]  FROM [OASIS].[dbo].[TC_REASONS_M] order by [TCR_ORDER]"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "TCR_DESCR"
        ddlReason.DataValueField = "TCR_CODE"
        ddlReason.DataBind()
        'ddlReason.Items.FindByValue(1).Selected = True
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlReason.Items.Insert(0, li)
    End Sub
    Sub GetTCReason()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT STR_REASON_ID,ISNULL(STR_OTHER_REASON,'') FROM TCM_STU_REASON_S WHERE STR_STU_ID=" + hfSTU_ID.Value _
              & " AND STR_TCM_ID=" + hfTCM_ID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            If Not ddlReason.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)) Is Nothing Then
                ddlReason.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)).Selected = True
                'If ds.Tables(0).Rows(i).Item(1) <> "" Then
                '    ddlReason.Text = ds.Tables(0).Rows(i).Item(1)
                '    If ds.Tables(0).Rows(i).Item(1) <> "" Then
                '        txtOtherReason.Text = ds.Tables(0).Rows(i).Item(1)
                '    End If
                'End If
                Exit For
            End If
        Next


    End Sub
    Sub BindTCType()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String
        'If Session("SBSUID") <> "800017" Then
        '    str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] order by [TCT_DESCR]"
        'Else
        '    str_query = "SELECT [TCT_CODE],[TCT_DESCCR_AQABA]  as TCT_DESCR FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCCR_AQABA IS NOT NULL order by [TCT_DESCR]"
        'End If

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sUsr_name")

        pParms(1) = New SqlClient.SqlParameter("@SBSUID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("SBSUID")


        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.StoredProcedure, "Get_TC_FORMATS", pParms)




        ddlTransferType.DataSource = ds
        ddlTransferType.DataTextField = "TCT_DESCR"
        ddlTransferType.DataValueField = "TCT_CODE"
        ddlTransferType.DataBind()
    End Sub
    Sub BindCountry()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "Select CTY_ID,CTY_DESCR  from Country_m order by CTY_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCountry.DataSource = ds
        ddlCountry.DataTextField = "CTY_DESCR"
        ddlCountry.DataValueField = "CTY_ID"
        ddlCountry.DataBind()
        ddlCountry.Items.FindByValue("5").Selected = True

    End Sub


    Function SaveData() As Boolean

        Dim i As Integer
        Dim strReason As String = ddlReason.SelectedValue.ToString
        'For i = 0 To ddlReason.Items.Count - 1
        '    If ddlReason.Items(i).Selected = True Then
        '        If strReason <> "" Then
        '            strReason += "|"
        '        End If
        '        strReason += ddlReason.Items(i).Value
        '    End If
        'Next


        Dim lintTCMID As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        lintTCMID = hfTCM_ID.Value

        Dim strGems As String = ""
        Dim strTfrSchool As String = ""
        Dim str As String()
        If ddlTrans_School.SelectedValue <> "0" Then
            str = ddlTrans_School.SelectedValue.Split("|")
            If str(1) = "GEMS" Then
                strGems = "'" + str(0) + "'"
                strTfrSchool = "NULL"
            Else
                strGems = "NULL"
                strTfrSchool = "'" + ddlTrans_School.SelectedItem.Text + "'"
            End If
        Else
            strGems = "NULL"
            strTfrSchool = "'" + txtOthers.Text + "'"
        End If

        Dim str_query As String = " EXEC saveTCENTRY " _
                         & "@TCM_ID=" + hfTCM_ID.Value + "," _
                         & "@TCM_BSU_ID='" + Session("SBSUID") + "'," _
                         & "@TCM_DOCDATE='" + Format(Now, "yyyy-MM-dd") + "'," _
                         & "@TCM_ACD_ID='" + hfACD_ID.Value + "'," _
                         & "@TCM_STU_ID=" + hfSTU_ID.Value + "," _
                         & "@TCM_REFNO='" + txtRefNo.Text.Replace("'", "''") + "'," _
                         & "@TCM_APPLYDATE='" + Format(Date.Parse(txtApplyDate.Text), "yyyy-MM-dd") + "'," _
                         & "@TCM_LEAVEDATE='" + Format(Date.Parse(txtLeave_Date.Text), "yyyy-MM-dd") + "'," _
                         & "@TCM_LASTATTDATE='" + Format(Date.Parse(txtLast_Attend.Text), "yyyy-MM-dd") + "'," _
                         & "@TCM_REASON='" + strReason + "'," _
                         & "@TCM_DAYSP=" + txtDay_Present.Text.ToString + "," _
                         & "@TCM_DAYST=" + txtTotWork_Day.Text.ToString + "," _
                         & "@TCM_TCTYPE='" + ddlTransferType.SelectedValue.ToString + "'," _
                         & "@TCM_SUBJECTS='" + txtSubject.Text.Replace("'", "''") + "'," _
                         & "@TCM_RESULT='" + ddlResult.SelectedValue + "'," _
                         & "@TCM_REMARKS='" + txtRemarks.Text.Replace("'", "''") + "'," _
                         & "@TCM_bTFRGEMS='" + IIf(strGems = "NULL", "false", "true") + "'," _
                         & "@TCM_GEMS_UNIT=" + strGems + "," _
                        & "@TCM_TFRSCHOOL='" + strTfrSchool.Replace("'", "''") + "'," _
                         & "@TCM_TFRCOUNTRY='" + ddlCountry.SelectedValue.ToString + "'," _
                         & "@TCM_TFRGRADE='" + ddlPromoted.SelectedValue.ToString + "'," _
                         & "@TCM_TFRZONE=N'" + txtTranZone.Text.Replace("'", "''") + "'," _
                         & "@TCM_CONDUCT='" + txtConduct.Text.Replace("'", "''") + "'," _
                         & "@TCM_FATHERNAME='" + txtFName.Text.Replace("'", "''") + "'," _
                         & "@TCM_ENTRY_TYPE='SCHOOL'," _
                         & "@USR_ID='" + Session("sUsr_id") + "'," _
                         & "@STU_JOIN_RESULT='" + ddlJoinResult.SelectedItem.Value + "'," _
                         & "@TCM_OTHERREASON='" + txtOtherReason.Text + "'"

        'Dim str_query As String = " EXEC saveTCENTRY " _
        '                         & hfTCM_ID.Value + "," _
        '                         & "'" + Session("SBSUID") + "'," _
        '                         & "'" + Format(Date.Parse(txtDocDate.Text), "yyyy-MM-dd") + "'," _
        '                         & hfACD_ID.Value + "," _
        '                         & hfSTU_ID.Value + "," _
        '                         & "'" + txtRefNo.Text.Replace("'", "''") + "'," _
        '                         & "'" + Format(Date.Parse(txtApplyDate.Text), "yyyy-MM-dd") + "'," _
        '                         & "'" + Format(Date.Parse(txtLeave_Date.Text), "yyyy-MM-dd") + "'," _
        '                         & "'" + Format(Date.Parse(txtLast_Attend.Text), "yyyy-MM-dd") + "'," _
        '                         & "'" + strReason + "'," _
        '                         & txtDay_Present.Text.ToString + "," _
        '                         & txtTotWork_Day.Text.ToString + "," _
        '                         & "'" + ddlTransferType.SelectedValue.ToString + "'," _
        '                         & "'" + txtSubject.Text.Replace("'", "''") + "'," _
        '                         & "'" + ddlResult.SelectedValue + "'," _
        '                         & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
        '                         & "'" + IIf(ddlTrans_School.SelectedValue = "0", "false", "true") + "'," _
        '                         & IIf(ddlTrans_School.SelectedValue = "0", "NULL", "'" + ddlTrans_School.SelectedValue + "'") + "," _
        '                         & "'" + IIf(ddlTrans_School.SelectedValue = "0", txtOthers.Text.Replace("'", "''"), "NULL") + "'," _
        '                         & "'" + ddlCountry.SelectedValue.ToString + "'," _
        '                         & "'" + ddlPromoted.SelectedValue.ToString + "'," _
        '                         & "N'" + txtTranZone.Text.Replace("'", "''") + "'," _
        '                         & "'" + txtConduct.Text.Replace("'", "''") + "'," _
        '                         & "'" + txtFName.Text.Replace("'", "''") + "','SCHOOL', " _
        '& "'" + Session("sUsr_id") + "','" + ddlJoinResult.SelectedItem.Value + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If hfTCM_ID.Value <> 0 Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                    UtilityObj.InsertAuditdetails(transaction, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString, "TCM_M_EDIT")
                End If
                hfTCM_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                If lintTCMID = 0 Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                    If ViewState("MainMnu_code") = "S100480" Then
                        UtilityObj.InsertAuditdetails(transaction, "add", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString, "TCM_M_ADD_CONVERT_SO")
                    Else
                        UtilityObj.InsertAuditdetails(transaction, "add", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString, "TCM_M_ADD")
                    End If
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using


    End Function


    Sub PrintTc()
        Dim lstrPrinci As String
        Dim lstrRegistrar As String

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        If (ddlTransferType.SelectedValue <> 2) And (ddlTransferType.SelectedValue <> 6) Then
            lstrPrinci = GetEmpName("ARABIC SECRETARY", "Full")
        Else
            lstrPrinci = GetEmpName("PRINCIPAL", "Full")
        End If

        If lstrPrinci Is Nothing Then
            lstrPrinci = ""
        End If
        'If Session("sbsuid").ToString = "131001" And ((ddlTransferType.SelectedValue = 2) Or (ddlTransferType.SelectedValue = 6)) Then
        '    lstrPrinci = "Dr. F WASIL"
        'End If


        param.Add("principal", lstrPrinci)

        param.Add("@TCM_ISSUEDATE", txtIssue.Text)



        'KHDA----Start
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        Dim lstrPWD As String = String.Empty
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_MOE_FILETYPE", pParms)
            While reader.Read
                lstrPWD = Convert.ToString(reader("BSU_MOE_FILETYPE"))
            End While
        End Using
        ' KHDA----END


        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            Select Case ddlTransferType.SelectedValue
                Case 1 'ARABIC - WITHIN ZONE
                    param.Add("@STU_XML", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")

                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_Sharjah.rpt")
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_AUH.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_AUH.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone.rpt")
                    End If

                Case 2 'ENGLISH tc
                    param.Add("@STU_XML", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")


                    Dim currYear As String = hfACY_DESCR.Value
                    Dim year As Integer = Val(hfACY_DESCR.Value.Split("-")(1))
                    Dim nextYear As String
                    Dim prevYear As String

                    nextYear = year.ToString + "-" + (year + 1).ToString
                    prevYear = (year - 1).ToString + "-" + year.ToString

                    param.Add("nextYear", nextYear)
                    lstrRegistrar = GetEmpName("REGISTRAR", "FULL")
                    If lstrRegistrar Is Nothing Then
                        lstrRegistrar = ""
                    End If
                    param.Add("registrar", lstrRegistrar)
                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_OutsideUAE_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        If (hfGRD_ID.Value = hfMaxGrade.Value Or hfGRD_ID.Value = "10") Then
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTCGrade12_SHJ.rpt")
                        Else
                            .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_SHJ.rpt")
                        End If
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    ElseIf lstrPWD = "11_2" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_UIS.rpt")
                    ElseIf lstrPWD = "81" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_Qatar.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_Jordan.rpt")
                    ElseIf lstrPWD = "12_2" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_WITHMOE.rpt")
                    ElseIf lstrPWD = "22_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_INDIA.rpt")
                    ElseIf lstrPWD = "22_2" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_MAK.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC.rpt")
                    End If
                Case 3 'ARABIC- OUTSIDE ZONE
                    param.Add("@STU_XML", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")


                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcWithinZone_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone_Sharjah.rpt")
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideZone.rpt")
                    End If
                Case 4 'ARABIC-OUTSIDE UAE
                    param.Add("@STU_XML", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")

                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishTC_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTC_Jordan.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                    End If


                Case 5 'ARABIC LC'
                    param.Add("@STU_ID", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")
                    param.Add("@TCSO", "TC")
                    If lstrPWD = "12_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    ElseIf lstrPWD = "13" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer_Sharjah.rpt")
                    ElseIf lstrPWD = "11_1" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicTcOutsideUAE.rpt")
                    ElseIf lstrPWD = "11" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    ElseIf lstrPWD = "80" Then
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    Else
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptArabicSchoolLeaveCer.rpt")
                    End If
                Case 6 'ENGLISH LC'
                    'param.Add("@STU_ID", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")
                    'param.Add("@TCSO", "TC")
                    Dim year As Integer = Val(hfACY_DESCR.Value.Split("-")(1))
                    Dim nextYear As String
                    Dim prevYear As String

                    nextYear = year.ToString + "-" + (year + 1).ToString
                    prevYear = (year - 1).ToString + "-" + year.ToString
                    param.Add("nextYear", nextYear)
                    lstrRegistrar = GetEmpName("REGISTRAR", "FULL")
                    If lstrRegistrar Is Nothing Then
                        lstrRegistrar = ""
                    End If
                    param.Add("registrar", lstrRegistrar)

                    If lstrPWD = "12_1" Then
                        param.Add("@STU_XML", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptStudentLeavingCertificateOSeas_KHDA.rpt")
                    ElseIf lstrPWD = "13" Then
                        param.Add("@STU_ID", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")
                        param.Add("@TCSO", "TC")
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishLC_SJH.rpt")
                    Else
                        param.Add("@STU_ID", "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>")
                        param.Add("@TCSO", "TC")
                        .reportPath = Server.MapPath("../Students/Reports/RPT/rptEnglishLC.rpt")
                    End If
            End Select

            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Function GetEmpName(ByVal designation As String, ByVal sType As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
        '                         & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
        '                         & " AND DES_DESCR='" + designation + "'"
        'Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If emp = Nothing Then
        '    emp = ""
        'End If
        'Return emp

        Dim emp As String
        Dim pParms(3) As SqlClient.SqlParameter
        emp = ""
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@DES_DESCR", designation)
        Using reader_EQS As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_EMPNAME_DESIG", pParms)
            While reader_EQS.Read
                emp = Convert.ToString(reader_EQS("EMP_NAME"))
            End While
        End Using
        Return emp
    End Function

    Function CheckStrikeOff() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(TCM_LEAVEDATE,'01/01/1900') FROM TCM_M WHERE TCM_STU_ID=" + hfSTU_ID.Value _
                                & " AND TCM_TCSO='SO' AND TCM_CANCELDATE IS NULL"
        Dim strikeDate As String = ""
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            strikeDate = reader.GetDateTime(0).ToString
        End While
        reader.Close()
        Return strikeDate
    End Function

    Function CheckDate() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACD_ENDDT FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " "
        Dim currdate As Date = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Try

            'code modified to apply leave date not less than the current date
            Dim leavedate As Date = Date.Parse(txtLeave_Date.Text)


            If DateTime.Compare(leavedate, currdate) > 0 Then
                lblError.Text = "The service start date should be within the academic year"
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try

        'If count = 0 Then
        '    lblError.Text = "The service start date should be within the academic year"
        '    Return False
        'Else
        '    Return True
        'End If

    End Function

    Function CheckValid_ACDDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND GetDate()>= ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function



#End Region
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'if the student is struck off the leave date has to be within the strike off month
        '

        Dim strikeDate As String = CheckStrikeOff()
        If strikeDate <> "" Then
            If Year(Date.Parse(txtLeave_Date.Text)) <> Year(Date.Parse(strikeDate)) Then
                lblError.Text = "The student is struck off on " + Format(Date.Parse(strikeDate), "dd/MMM/yyyy") + " .Therefore the leave date has to be on the same month as the strike off date"
                Exit Sub
            End If
            If Month(Date.Parse(txtLeave_Date.Text)) <> Month(strikeDate) Then
                lblError.Text = "The student is struck off on " + Format(Date.Parse(strikeDate), "dd/MMM/yyyy") + " .Therefore the leave date has to be on the same month as the strike off date"
                Exit Sub
            End If
        End If

        'if the student is not in the current academicyear then leavedate hasto be within the academicyear
        'if the student is in the current academicyear then leavedate cannot be in the previous month
        If ViewState("MainMnu_code") <> "S100480" Then
            If hfACD_ID.Value <> Session("Current_ACD_ID") Then
                If CheckValid_ACDDate() = False Then
                    lblError.Text = "Leave date should not be less than current date"
                    Exit Sub
                End If
                'Else
                'If CheckDate() = False Then
                '        lblError.Text = "Leave date should not be less than current date"
                '        Exit Sub
                '    End If
            End If
        End If

        If ViewState("MainMnu_code") <> "S100480" Then
            If hfACD_ID.Value <> Session("Current_ACD_ID") Then
                If CheckDate() = False Then
                    lblError.Text = "Leave date should be within the academic year"
                    Exit Sub
                End If
                'Else
                'If CheckDate() = False Then
                '        lblError.Text = "Leave date should not be less than current date"
                '        Exit Sub
                '    End If
            End If
        End If



        If ViewState("MainMnu_code") <> "S100480" Then
            If hfACD_ID.Value = Session("Current_ACD_ID") Then
                If Date.Parse(txtLeave_Date.Text) < Now.Date Then
                    If CheckValid_ACDDate() = False Then
                        lblError.Text = "Leave date should not be less than current date"
                        Exit Sub
                    End If

                End If
            End If
        End If


        If Date.Parse(txtLast_Attend.Text) > Date.Parse(txtLeave_Date.Text) Then
            lblError.Text = "Last date of attendance cannot be higher  than the leave date"
            Exit Sub
        End If


        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TCM_STU_ID", hfSTU_ID.Value)
        pParms(1) = New SqlClient.SqlParameter("@TCM_LASTATTDATE", Format(Date.Parse(txtLast_Attend.Text), "yyyy-MM-dd"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TCSO_LastAttDate_Validate", pParms)
            While reader.Read
                Dim lstrRet = Convert.ToString(reader("Ret"))
                If lstrRet = "0" Then
                    lblError.Text = "Last date of attendance cannot be less  than the actual attended date"
                    Exit Sub
                End If
            End While
        End Using

        If ddlReason.SelectedValue = "0" Then
            lblError.Text = "Please select the reason for withdrawal"
            Exit Sub
        End If

        SaveData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim flag As String = String.Empty
        SaveData()
        CheckStudent_Att(flag)
        If flag = "NO" Then
            PrintTc()
        Else
            pnlSTBooking.Visible = True
        End If
    End Sub

    Private Sub CheckStudent_Att(ByRef flag As String)
        Try
            Dim STATUS As Integer = 0
            Dim stu_xml As String = "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>"

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
            pParms(1) = New SqlParameter("@stu_XML", stu_xml)
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.CHECKTC_ATT_UPDATE", pParms)
                If datareader.HasRows = True Then


                    While datareader.Read

                        divTCTot.InnerHtml = Convert.ToString(datareader("OLD_DAYST"))
                        divActualTot.InnerHtml = Convert.ToString(datareader("NEW_DAYST"))
                        divTCPRS.InnerHtml = Convert.ToString(datareader("OLD_DAYSP"))
                        divActualPRS.InnerHtml = Convert.ToString(datareader("NEW_DAYSP"))
                        flag = Convert.ToString(datareader("DISCR"))
                    End While
                Else
                    flag = "No"

                End If

            End Using



        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lbtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnClose.Click
        pnlSTBooking.Visible = False
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        pnlSTBooking.Visible = False
        PrintTc()
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        pnlSTBooking.Visible = False
        If UpdateStudent_Att() <> 0 Then
            lblError.Text = "Error while updating student attendance"
            Exit Sub
        Else
            PrintTc()
        End If

    End Sub
    Private Function UpdateStudent_Att() As Integer
        Dim STATUS As Integer = 0

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try

                transaction = conn.BeginTransaction("SampleTransaction")


                Dim stu_xml As String = "<IDS><ID><STU_ID>" + hfSTU_ID.Value + "</STU_ID></ID></IDS>"

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
                pParms(1) = New SqlParameter("@stu_XML", stu_xml)
                pParms(2) = New SqlParameter("@USR_ID", Session("sUsr_id"))

                pParms(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SAVETC_ATT_UPDATE", pParms)


                STATUS = CInt(pParms(3).Value)

            Catch ex As Exception
                Status = 1
            Finally
                If STATUS <> 0 Then
                    STATUS = 1
                    transaction.Rollback()
                Else
                    STATUS = 0
                    transaction.Commit()
                End If
            End Try

        End Using

        UpdateStudent_Att = STATUS
    End Function
End Class
