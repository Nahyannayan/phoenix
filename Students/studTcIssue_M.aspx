<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="True"
    CodeFile="studTcIssue_M.aspx.vb" Inherits="Students_studTcIssue_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
       
          var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvStud.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}


    function change_chk_state(chkThis)
         {
                  
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (  document.forms[0].elements[i].disabled==false)
                    {
                      document.forms[0].elements[i].checked=chk_state;
                      document.forms[0].elements[i].click();//fire the click event of the child element
                      }
                     if (chkstate=true)
                     {
                     
                     }
                 }
              }
          }
           
                      
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Issue TC
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTC" runat="server" align="center" width="100%"
                    cellpadding="0" cellspacing="0">
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Select Academic Year</span></td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Show</span></td>
                        
                        <td align="left">
                            <asp:RadioButton ID="rbNotIssued" runat="server" CssClass="field-label" GroupName="SHOW" Text="Not Issued"
                                Checked="true"></asp:RadioButton>
                            <asp:RadioButton ID="rbIssued" runat="server" CssClass="field-label" GroupName="SHOW" Text="Issued"></asp:RadioButton>
                            <asp:RadioButton ID="rbAll" runat="server" CssClass="field-label" GroupName="SHOW" Text="All"></asp:RadioButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Select Grade</span></td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server">
                            </asp:DropDownList></td>
                        <td align="left">
                            <span class="field-label">Select Tc Type</span></td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlTcType" runat="server" Width="178px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Student ID</span></td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtStuNo" runat="server">
                            </asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Student Name</span></td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                        
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                              OnClick="btnSearch_Click" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" >
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Issue Date</span></td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtDate" runat="server">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4">
                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Available">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                                Select
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                               
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <HeaderTemplate>
                                           
                                                <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label>
                                                <br />
                                                <asp:TextBox ID="txtStuNo" runat="server" Width="75%"></asp:TextBox>
                                                 
                                                <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnSearchStuNo_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                        <HeaderTemplate>
                                         
                                                <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                <br />
                                                <asp:TextBox ID="txtStuName" runat="server" Width="75%"></asp:TextBox>
                                                <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnSearchStuName_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                           
                                               <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                               <br />
                                               <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                               <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnGrade_Search_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>
                                           <asp:Label ID="lblH123" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label>
                                           <br />
                                            <asp:TextBox ID="txtSection" runat="server" Width="75%"></asp:TextBox>
                                           <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnSection_Search_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tc Apply Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplyDate" runat="server" Text='<%# Bind("TCM_APPLYDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason for Tc">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReason" runat="server" Text='<%# Bind("TCR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Issue">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblIssue" Text='<%# Bind("TXTISSUE") %>' runat="server"
                                                OnClick="lblIssue_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="8">
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" ValidationGroup="groupM1"
                                Width="58px" /></td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>
    
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtDate" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMaxGrade" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfShow" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfACY_DESCR" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfSTU_ID" runat="server" />
    <br />
    <asp:HiddenField ID="hfTcTYPE" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
     <asp:HiddenField ID="hfCountry" runat="server"></asp:HiddenField>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    
                

            </div>
        </div>
    </div>



</asp:Content>
