<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnqDocProceed_View.aspx.vb" Inherits="Students_studEnqDocProceed_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        input {
            vertical-align :middle !important;
        }
    </style>
    <script language="javascript" type="text/javascript">


        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {

    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            if (document.forms[0].elements[i].disabled == false) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
            if (chkstate = true) {

            }
        }
    }
}

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
              Enquiry Document Stages
            </div> 
        <div class="card-body">
            <div class="table-responsive m-auto">  
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="center"  valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table align="center"  cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%"> <span class="field-label">Academic Year</span></td>
                     
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                            </asp:DropDownList>                           
                        </td>
                         <td></td>
                         <td></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4"  valign="top">
                            <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                HeaderStyle-Height="30" PageSize="20" >
                                <RowStyle CssClass="griditem" />
                                <Columns>

                                    <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("EQM_ENQID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Enq No">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnqid_Search_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnqNo" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>

                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Enq Date">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtEnqDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnq_Date_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>

                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <%--<tbody>
                                                    <tr>
                                                        <td align="center">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            </td>
                                                    </tr>
                                                </tbody>
                                                    
                                            </table>--%>
                                             Grade
                                                            <br />
                                                            <asp:DropDownList ID="ddlgvGrade" runat="server" CssClass="listbox" AutoPostBack="True" OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Shift">
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        </td>
                                                </tr>
                                            </table>--%>
                                            Shift
                                                        <br />
                                                        <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>

                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stream">
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                       </td>
                                                </tr>
                                            </table>--%>
                                            Stream <br />
                                                         <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>

                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Applicant Name">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAppl_Name_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lnkApplName" runat="server" Text='<%# Bind("appl_name") %>'></asp:Label>

                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="View" Text="View" HeaderText="Document Stages">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:ButtonField>

                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>                 
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                        type="hidden" value="=" />
                <input id="lstValues" runat="server" name="lstValues" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px"
                    type="hidden" />
            </td>
        </tr>
    </table>
                </div> 
            </div> 
        </div> 

</asp:Content>

