Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Students_Reports_ASPX_rptAgeStatistics
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            If ValidateDate() = "" Then
                Dim todt As DateTime = txtTODT.Text
                Dim fromdt As DateTime = txtFromDt.Text

                Dim startdt As DateTime = ViewState("startDt")
                Dim enddt As DateTime = ViewState("endDt")

                If ((fromdt >= startdt) And (fromdt <= enddt)) And ((todt >= startdt) And (todt <= enddt)) Then
                    CallReport()
                Else
                    lblError.Text = "Date must be within the academic year!!!"
                End If

            End If

        End If
    End Sub
    Sub CallReport()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            If ViewState("MainMnu_code") = "S059026" Then 'House attendance summary


                Dim Ason_Date As String = ""
                Dim i As Integer = 0
                Session("TO_Dt") = txtTODT.Text
                Session("FROM_Dt") = txtFromDt.Text
                Session("ACD_SEL_ATT") = ddlAcademicYear.SelectedValue
                ViewState("datamode") = "add"
                'Encrypt the data that needs to be send through Query String
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                ViewState("datamode") = Encr_decrData.Encrypt("add")
                Dim url As String = String.Empty
                url = String.Format("~\Students\studAtt_SummHouse.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            ElseIf ViewState("MainMnu_code") = "S059023" Then


                Dim Ason_Date As String = ""
                Dim i As Integer = 0
                Session("TO_Dt") = txtTODT.Text
                Session("FROM_Dt") = txtFromDt.Text
                Session("ACD_SEL_ATT") = ddlAcademicYear.SelectedValue
                ViewState("datamode") = "add"
                'Encrypt the data that needs to be send through Query String
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                ViewState("datamode") = Encr_decrData.Encrypt("add")
                Dim url As String = String.Empty
                url = String.Format("~\Students\studAtt_Summ.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            ElseIf ViewState("MainMnu_code") = "S059024" Then


                Dim FROMDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDt.Text) '"25/NOV/2008" '
                Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtTODT.Text) '"25/NOV/2008" '
                Dim param As New Hashtable
                param.Add("@IMG_BSU_ID", Session("sbsuid"))
                param.Add("@IMG_TYPE", "LOGO")
                param.Add("@BSU_ID", Session("sbsuid"))
                param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
                param.Add("@FROMDT", Format(Date.Parse(FROMDT), "dd/MMM/yyyy"))
                param.Add("@TODT", Format(Date.Parse(TODT), "dd/MMM/yyyy"))
                param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
                param.Add("fromdt", Format(Date.Parse(FROMDT), "dd/MMM/yyyy"))
                param.Add("todt", Format(Date.Parse(TODT), "dd/MMM/yyyy"))
                param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                param.Add("UserName", Session("sUsr_name"))

                Dim rptClass As New rptClass
                With rptClass
                    .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                    .reportParameters = param
                    .reportPath = Server.MapPath("~\Students\Reports\RPT\rptAtt_KHDA.rpt")

                End With
                Session("rptClass") = rptClass
                '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If


        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059023" And ViewState("MainMnu_code") <> "S059024" And ViewState("MainMnu_code") <> "S059026") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                hfDate.Value = DateTime.Now
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call bindAcademic_Year()
                'getACDstart_dt()
                txtTODT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub getACDstart_dt()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim acd_id As String = ddlAcademicYear.SelectedValue
            Dim sqlString As String = String.Empty
            sqlString = "select acd_startdt,acd_enddt from academicyear_d where acd_id='" & acd_id & "' "




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlString)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ViewState("startDt") = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(0))
                        ViewState("endDt") = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(1))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"





            If txtFromDt.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDt.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDt.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"

                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtTODT.Text.Trim <> "" Then

                Dim strfDate As String = txtTODT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtTODT.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDt.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtTODT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where  acd_clm_id='" & Session("CLM").ToString & "'  and  acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcademicYear.Items.Clear()
            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            'get the start date and end date
            getACDstart_dt()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        getACDstart_dt()
    End Sub

End Class
