﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports UtilityObj
Partial Class Students_studentAchView
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Dim Message As String = ""

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Page.Form.Attributes.Add("enctype", "multipart/form-data")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                Session("MERIT_Stu_ID") = Request.QueryString("STU_ID")
                Session("MERIT_ACD_ID") = Request.QueryString("ACD_ID")
                StudHeader(Session("MERIT_Stu_ID"))
                gridbind()


                ' End If
                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvStudTPT.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtLogDate")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_ENTRY_DATE", lstrCondn1)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtInciDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_DATE", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtMerit")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_TYPE", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtRemarks")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_REMARKS", lstrCondn4)

            End If

            str_Sql = "Select isnull(MRG_ID ,'') MRGID,isnull(MRT_bShow,0) bShow,* From BM.Merits_M A" _
                   & " INNER JOIN BM.MERITS_D B ON A.MRT_ID=B.MRS_MRT_ID left outer join BM.BM_CATEGORY C on C.BM_CATEGORYID=A.MRT_BM_SUB_CATEGORY_ID " _
                   & " LEFT join BM.MERITS_G  G ON G.MRG_ID = A .MRT_MRG_ID " _
                   & " WHERE MRT_INCDNT_TYPE='Achieves' AND ISNULL(MRT_DELETED,0) = 0 AND MRS_STU_ID='" & Session("MERIT_STU_ID") & "' AND MRT_ACD_ID='" & Session("MERIT_ACD_ID") & "' " & str_Filter


            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            gvStudTPT.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudTPT.DataBind()
                Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
                gvStudTPT.Rows(0).Cells.Clear()
                gvStudTPT.Rows(0).Cells.Add(New TableCell)
                gvStudTPT.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudTPT.DataBind()
            End If

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtLogDate")
            txtSearch.Text = lstrCondn1

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtInciDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtMerit")
            txtSearch.Text = lstrCondn3

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn4



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvStudTPT.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub StudHeader(ByVal Stu_ID As String)

        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlString As String = "SELECT  ISNULL(STUDENT_M.STU_PHOTOPATH,'') AS STU_PHOTOPATH,student_M.stu_acd_id,  STUDENT_M.STU_NO, GRADE_BSU_M.GRM_DISPLAY, SECTION_M.SCT_DESCR, " &
            "ISNULL(STUDENT_D.STS_FFIRSTNAME, '') + ' ' + ISNULL(STUDENT_D.STS_FMIDNAME, '') + ' ' + " &
            "ISNULL(STUDENT_D.STS_FLASTNAME, '') AS Fname, ISNULL(STUDENT_D.STS_MFIRSTNAME, '') + ' ' + " &
            "ISNULL(STUDENT_D.STS_MMIDNAME, '') + ' ' + ISNULL(STUDENT_D.STS_MLASTNAME, '') AS Mname, " &
            "ISNULL(STUDENT_D.STS_FUSR_NAME, ISNULL(STUDENT_D.STS_MUSR_NAME,ISNULL(STUDENT_D.STS_GUSR_NAME,''))) AS ParUserName, " &
            "case when (select count(*) from tcm_m where tcm_stu_id=stu_id)>=1	then   case when (select top(1)tcm_tcso from tcm_m where tcm_stu_id=stu_id and TCM_bpreApproved=0 and TCM_bCANCELLED=0 order by tcm_id desc)='TC'  then 'ACTIVE - But applied for TC' when (select top(1)tcm_tcso from tcm_m where tcm_stu_id=stu_id and TCM_bpreApproved=1 and TCM_bCANCELLED=0 order by tcm_id desc)='TC'  then 'INACTIVE - Issued TC' when (select top(1)tcm_tcso from tcm_m join STRIKEOFF_RECOMMEND_M ON STK_STU_ID=tcm_stu_id where tcm_stu_id=stu_id 	and STK_bAPPROVED=0 and TCM_bCANCELLED=0 AND tcm_tcso='so' order by tcm_id desc)='SO'  then 'ACTIVE - Recommended for Strike Off' when (select top(1)tcm_tcso from tcm_m join STRIKEOFF_RECOMMEND_M ON STK_STU_ID=tcm_stu_id  where tcm_stu_id=stu_id	 and STK_bAPPROVED=1 AND tcm_tcso='so' and TCM_bCANCELLED=0 order by tcm_id desc)='SO'  then 'INACTIVE - Strike Off' else case STU_CURRSTATUS when 'SO' then 'INACTIVE -Strike Off' when 'EN' then 'ACTIVE' when 'TC' then 'INACTIVE - TC' when 'CN' then 'CANCELLED' end  End 	else case STU_CURRSTATUS when 'EN' then 'ACTIVE' when 'SO' then 'INACTIVE -Strike Off' when 'TC' then 'INACTIVE - TC' 	when 'CN' then 'CANCELLED' end End STU_CURRSTATUS, CASE WHEN  (SELECT     isnull(BSU_bSTUD_DISPLAYPASSPRTNAME, 0) " &
            "FROM  businessunit_m  WHERE      bsu_id = STUDENT_M.STU_BSU_ID) = 1 THEN STUDENT_M.STU_PASPRTNAME " &
            "ELSE STUDENT_M.STU_FIRSTNAME + ' ' + isnull(STUDENT_M.STU_MIDNAME, '')  + ' ' + isnull(STUDENT_M.STU_LASTNAME, '') " &
            " END AS sname, case when (select count(distinct grm_shf_id) from grade_bsu_m " &
            " where grm_grd_id=student_m.stu_grd_id and grm_acd_id=student_M.stu_acd_id)>1 then " &
            "(select shf_descr from shifts_m where shf_id=student_m.stu_shf_id) else '' end as shf," &
            " case when (select count( distinct grm_stm_id) from grade_bsu_m " &
            "where grm_grd_id=student_m.stu_grd_id and grm_acd_id=student_M.stu_acd_id)>1 then " &
            " (select stm_descr from stream_m where stm_id=student_m.stu_stm_id) else '' end as stm, " &
            " case when (select count(distinct acd_clm_id) from academicyear_d where acd_acy_id= " &
            "SECTION_M.SCT_ACY_ID and acd_bsu_id=STUDENT_M.STU_BSU_ID)>1 then " &
            " (select clm_descr from dbo.CURRICULUM_M where clm_id=(select acd_clm_id from academicyear_d " &
            " where acd_id=STUDENT_M.STU_ACD_ID)) else '' end  as clm ,STU_MINLIST,STU_MINLISTTYPE,isNULL(STU_USR_NAME,'') as StudUserName,STU_LastAttDate,STU_LEAVEDATE" &
            " FROM         STUDENT_M INNER JOIN   GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN " &
            " SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID INNER JOIN " &
             " STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID " &
            " WHERE     STUDENT_M.STU_ID = '" & Stu_ID & "'"


        Try


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlString)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ltStudName.Text = Convert.ToString(readerStudent_Detail("Sname"))
                        ltStudId.Text = Convert.ToString(readerStudent_Detail("stu_no"))
                        ltCLM.Text = Convert.ToString(readerStudent_Detail("clm"))
                        ltGrd.Text = Convert.ToString(readerStudent_Detail("grm_display"))
                        ltSct.Text = Convert.ToString(readerStudent_Detail("sct_descr"))

                        ltStatus.Text = Convert.ToString(readerStudent_Detail("stu_currstatus"))




                        If Trim(ltCLM.Text) = "" Then
                            trCurr.Visible = False
                        Else
                            trCurr.Visible = True
                        End If



                        Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                        Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                        Dim strImagePath As String = String.Empty
                        If strPath <> "" Then
                            strImagePath = connPath & strPath
                            imgEmpImage.ImageUrl = strImagePath
                        End If

                    End While

                End If

            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnEdit_Command(sender As Object, e As CommandEventArgs)
        Dim st_conn = ConnectionManger.GetOASISConnectionString

        Dim btnEdit As Button = DirectCast(sender, Button)
        Dim meritid As Integer = Convert.ToInt16(e.CommandArgument)
        Dim lblStu_Name As Label = DirectCast(sender.parent.findcontrol("lblStu_Name"), Label)
        Dim lblStu_No As Label = DirectCast(sender.parent.findcontrol("lblStu_No"), Label)
        Dim ddl_Categoryname As DropDownList = DirectCast(sender.parent.findcontrol("ddl_Categoryname"), DropDownList)
        Dim txtStu_REMARKS As TextBox = DirectCast(sender.parent.findcontrol("txtStu_REMARKS"), TextBox)
        Dim GroupId As Integer = (TryCast(sender.parent.FindControl("hidGrpId"), HiddenField)).Value
        Dim lnkAttbtn As ImageButton = DirectCast(sender.parent.FindControl("lnkAttbtn"), ImageButton)
        Dim divUpload As HtmlGenericControl = DirectCast(sender.parent.FindControl("divUpload"), HtmlGenericControl)
        Dim chk_bshow As CheckBox = DirectCast(sender.parent.FindControl("chk_bshow"), CheckBox)

        If (btnEdit.Text = "Edit") Then
          
            lblStu_Name.Visible = False
            lblStu_No.Visible = False
            lnkAttbtn.Visible = False
            divUpload.Visible = True

            Dim PARAM(0) As SqlParameter
            PARAM(0) = Mainclass.CreateSqlParameter("@MRTID", meritid, SqlDbType.BigInt)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(st_conn, "[BM].[GET_ACHEIVMENT_CATEGORIES]", PARAM)
            If (ds.Tables.Count > 0) Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    ddl_Categoryname.DataSource = ds.Tables(0)
                    ddl_Categoryname.DataValueField = "ID"
                    ddl_Categoryname.DataTextField = "NAME"
                    ddl_Categoryname.DataBind()
                    ddl_Categoryname.Visible = True
                    If Not lblStu_No.Text Is Nothing And lblStu_No.Text <> "" Then
                        If Not (ddl_Categoryname.Items.FindByText(lblStu_No.Text).Text) Is Nothing Then
                            ddl_Categoryname.Items.FindByText(lblStu_No.Text).Selected = True
                            'ddl_Categoryname.Visible = True
                        End If
                    End If
                End If
            End If
            'ddl_Categoryname.Items.Insert(0, New ListItem("SELECT", 0))
            'ddl_Categoryname.Items.FindByText("SELECT").Selected = True
            '[BM].[GET_ACHEIVMENT_CATEGORIES]
            txtStu_REMARKS.Visible = True
            txtStu_REMARKS.Text = lblStu_Name.Text

            btnEdit.Text = "Update"
        ElseIf (btnEdit.Text = "Update") Then
            Dim strans As SqlTransaction = Nothing
            Try
                Dim objConn As New SqlConnection(st_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                '[BM].[UPDATE_ACHEIVMENT_DETAILS] 
                Dim params(4) As SqlParameter
                params(0) = Mainclass.CreateSqlParameter("@PAGE", "A", SqlDbType.Char)
                params(1) = Mainclass.CreateSqlParameter("@MERITID", meritid, SqlDbType.BigInt)
                params(2) = Mainclass.CreateSqlParameter("@REMARKS", txtStu_REMARKS.Text, SqlDbType.VarChar)
                params(3) = Mainclass.CreateSqlParameter("@BM_CATID", ddl_Categoryname.SelectedValue, SqlDbType.BigInt)
                If chk_bshow.Checked Then
                    params(4) = New SqlClient.SqlParameter("@bShow", 1)
                Else
                    params(4) = New SqlClient.SqlParameter("@bShow", 0)
                End If
                SqlHelper.ExecuteNonQuery(strans, "[BM].[UPDATE_ACHEIVMENT_DETAILS]", params)
                If ViewState("ACTIVITYFILEPATH") <> "" And Not ViewState("ACTIVITYFILEPATH") Is Nothing Then
                    callSavefile(upload, Session("sbsuid"), ddl_Categoryname.SelectedItem.Text, GroupId, strans)
                End If

                strans.Commit()
                ShowMessage("Record got updated", False)
                gridbind()
            Catch ex As Exception
                strans.Rollback()
            Finally

            End Try

        End If

    End Sub
    Protected Sub lnkdelete_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            '[BM].[DELETE_MERIT_DETAILS] '
            Dim params(0) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@MRT_ID", e.CommandArgument, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, "[BM].[DELETE_MERIT_DETAILS]", params)
            strans.Commit()
            ShowMessage("Record got deleted", False)
            gridbind()
        Catch EX As Exception
        End Try
    End Sub

    'Protected Sub lnkAttDwnldbtn_Command(sender As Object, e As CommandEventArgs)
    '    Dim filepath As String = e.CommandArgument
    '    Response.Clear()
    '    Response.ContentType = "application/pdf"
    '    Response.AppendHeader("Content-Disposition", "attachment; filename=" & filepath)
    '    Response.BinaryWrite(System.IO.File.ReadAllBytes(filepath))
    '    Response.End()
    'End Sub

    Protected Sub gvStudTPT_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Dim index As String = gvStudTPT.EditIndex
        Dim hidDocName As HiddenField = DirectCast(e.Row.FindControl("hidDocName"), HiddenField)
        Dim hidid As HiddenField = DirectCast(e.Row.FindControl("hidid"), HiddenField)
        Dim lnkAttbtn As ImageButton = DirectCast(e.Row.FindControl("lnkAttbtn"), ImageButton)
        'Dim lnkAttDwnldbtn As LinkButton = DirectCast(e.Row.FindControl("lnkAttDwnldbtn"), LinkButton)
        If Not hidDocName Is Nothing Then
            If (hidDocName.Value <> "") Then
                lnkAttbtn.Visible = True
                ' lnkAttDwnldbtn.Visible = True
                hidDocName.Value = "Final\" + hidDocName.Value
                hidDocName.Value = hidDocName.Value.Replace("\", "\\")
                Dim strExtn As String = System.IO.Path.GetExtension(hidDocName.Value).ToLower
                lnkAttbtn.Attributes.Add("OnClick", "javascript: return showDocument('" & hidDocName.Value & "','" & strExtn & "')")
            Else
                'lnkAttDwnldbtn.Visible = False
                lnkAttbtn.Visible = False
            End If
        End If
    End Sub

#Region "FileUpload_Edit"
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Function ContainsSpecialChars(s As String) As Boolean
        Return s.IndexOfAny("[~`!@#$%^&*()-+=|{}':;,<>/?]".ToCharArray) <> -1
    End Function
    Private Function GetExtension(ByVal FileName As String) As String
        Dim Extension As String
        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)
        Return Extension
    End Function
    Public Function CurrentDate() As String
        'Dim lastDate As String = Day(DateTime.Now).ToString() & "/" & MonthName(Month(DateTime.Now), True) & "/" & Year(DateTime.Now).ToString()
        'lastDate = Convert.ToDateTime(lastDate).ToString("ddMMMyyyy")
        'Return lastDate
        Dim lastdate As String = DateTime.Now.Ticks.ToString
        Return lastdate
    End Function

    Function callSavefile(ByVal upload As FileUpload, ByVal bsuid As String, ByVal merit As String, ByVal hidGrpId As Integer, ByRef strans As SqlTransaction) As Integer
        callSavefile = 1000
        'Try
        Dim returnvalue As Boolean = False
        Dim str_error As String = ""
        Dim status As Integer = 0
        Dim STU_BSU_ID As String = bsuid
        Dim PHOTO_PATH As String = String.Empty
        Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathFinal").ToString
        Dim path As String
        Dim fileCount As Integer = 0
        Dim errCount As Integer = 0
        If Not Directory.Exists(ConFigPath & bsuid & "\" & hidGrpId & "\") Then
            Directory.CreateDirectory(ConFigPath & bsuid & "\" & hidGrpId & "\")
            path = ConFigPath & bsuid & "\" & hidGrpId & "\"
        Else
            Dim dold As New DirectoryInfo(ConFigPath & bsuid & "\" & hidGrpId & "\")
            path = ConFigPath & bsuid & "\" & hidGrpId & "\"
            Dim fiold() As System.IO.FileInfo
            fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            If fiold.Length > 0 Then '' If Having Attachments
                For Each f As System.IO.FileInfo In fiold
                    f.Delete()
                Next
            End If
        End If


        Dim d As New DirectoryInfo(ViewState("ACTIVITYFILEPATH"))

        Dim fi() As System.IO.FileInfo
        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
        If fi.Length = 1 Then '' If Having Attachments
            For Each f As System.IO.FileInfo In fi
                f.MoveTo(path & f.Name)
                Dim Str_fullpath As String = bsuid & "\" & hidGrpId & "\" & "" & f.Name
                If path <> "" Then
                    status = callDocSave(merit, strans, Str_fullpath, "U", hidGrpId)
                Else
                    callSavefile = 1000
                    strans.Rollback()
                    Exit Function
                End If
            Next
        End If
        d.Delete()
        ViewState("ACTIVITYFILEPATH") = ""

        callSavefile = status
        'Catch ex As Exception
        '    UtilityObj.Errorlog("From callSavefile: " + ex.Message, "OASIS ACTIVITY SERVICES")
        '    strans.Rollback()
        '    callSavefile = 1000
        'End Try
    End Function
    Function callDocSave(ByVal incident_type As String, ByRef trans As SqlTransaction, ByVal DocPath As String, ByVal type As Char, ByVal mrg_id As Integer) As Integer
        Dim ReturnFlag As Int16
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim params(8) As SqlClient.SqlParameter
        params(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
        params(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        params(2) = New SqlClient.SqlParameter("@INC_TYPE", incident_type)
        params(3) = New SqlClient.SqlParameter("@INC_DATE", "")
        params(4) = New SqlClient.SqlParameter("@TYPE", type)
        params(5) = New SqlClient.SqlParameter("@DOCName", DocPath)
        params(6) = New SqlClient.SqlParameter("@MRG_ID", mrg_id)
        params(7) = New SqlClient.SqlParameter("@NewMRg_ID", SqlDbType.BigInt)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        params(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[BM].[SaveDocMerit_M]", params)
        ReturnFlag = params(8).Value.ToString
        Return ReturnFlag
    End Function

    Protected Sub saveupload_Click(sender As Object, e As EventArgs)
        Dim DdlMerit As DropDownList = DirectCast(sender.parent.findcontrol("ddl_Categoryname"), DropDownList)
        Dim updpnl As UpdatePanel = DirectCast(sender.parent.findcontrol("updpnl"), UpdatePanel)
        Dim lnkShowFile As LinkButton = DirectCast(sender.parent.findcontrol("lnkShowFile"), LinkButton)
        Dim upload As FileUpload = DirectCast(sender.parent.findcontrol("upload"), FileUpload)

        Try
            Dim str_error As String = ""
            Dim obj As Object = sender.parent
            Dim BSUID = Session("sbsuid")
            Dim errCount As Integer = 0
            Dim fileCount As Integer = 0
            Dim returnvalue As Boolean = False
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathTemp").ToString
            Dim Tempath As String = ConFigPath & BSUID & "\Achievement\"

            'to remove exising files in temp path
            Dim dold As New DirectoryInfo(Tempath)
            Dim fiold() As System.IO.FileInfo
            If dold.Exists Then
                fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fiold.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fiold
                        f.Delete()
                    Next
                End If
            End If
            Dim UploadfileName As String = ""
            ' end of deletion of exising files
            ' For Each rptr As RepeaterItem In repdoc.Items
            ' Dim lblfile As Label = CType(rptr.FindControl("lblfile"), Label)
            ' Dim Fileupload As FileUpload = CType(rptr.FindControl("Fileupload"), FileUpload)
            ' Dim hffile As HiddenField = CType(rptr.FindControl("hffile"), HiddenField)
            If upload.HasFile Then
                Try
                    fileCount = fileCount + 1
                    Dim strPostedFileName As String = upload.FileName 'get the file name
                    Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                    If strPostedFileName <> "" And ConFigPath <> "" Then
                        Dim intDocFileLength As Integer = upload.FileContent.Length ' get the file size
                        Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                        Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
                        Dim FileNameLength As Integer = strPostedFileName.Length
                        UploadfileName = "Achievement" & "_" & CurrentDate() & strExtn
                        Dim s As String = strPostedFileName
                        Dim result() As String
                        result = s.Split(".")
                        Dim fileext = GetExtension(strExtn)
                        'Checking file extension
                        If (strPostedFileName <> String.Empty) And fileext <> "" Then
                            If Not (fileext.ToUpper = "PDF") And Not (fileext.ToUpper = "PNG") And Not (fileext.ToUpper = "JPG") Then 'Or fileext.ToUpper = "PNG"

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Upload PDF Files Only!!"
                                Else
                                    str_error = str_error & "Upload PDF Files Only!!"
                                End If
                                returnvalue = False
                                '  Exit Sub
                                'Checking file extension length
                            ElseIf fileext.Length = 0 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "file with out extension not allowed...!!"
                                Else
                                    str_error = str_error & "file with out extension not allowed...!!"
                                End If
                                returnvalue = False
                                '   Exit Sub
                                'Checking Special Characters in file name

                            ElseIf ContainsSplChar = True Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                Else
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub
                                'Checking FileName length

                            ElseIf FileNameLength = 0 Or FileNameLength > 1500 Then '255
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Error with file Name Length..!!"
                                Else
                                    str_error = str_error & "Error with file Name Length..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub

                            ElseIf result.Length > 2 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Invalid file Name..!!"
                                Else
                                    str_error = str_error & "Invalid file Name..!!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            ElseIf upload.FileContent.Length > 5000000 Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                Else
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                End If

                                returnvalue = False
                                '' Exit Sub

                            ElseIf Not (strExtn = ".pdf") And Not strExtn = ".jpg" And Not strExtn = ".png" Then 'exten type

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File type is different than allowed!"
                                Else
                                    str_error = str_error & "File type is different than allowed!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            Else
                                str_error = ""

                                If Not Directory.Exists(Tempath) Then
                                    Directory.CreateDirectory(Tempath)
                                End If
                                If errCount = 0 Then
                                    upload.SaveAs(Tempath & UploadfileName)
                                End If
                                ViewState("ACTIVITYFILEPATH") = ConFigPath & BSUID & "\Achievement"
                            End If
                        End If
                    End If

                Catch ex As Exception
                    ' returnvalue = False
                    '  Return returnvalue
                    ShowMessage(ex.Message, True)
                    Exit Sub
                End Try
            End If
            'Next

            If str_error <> "" Then
                ShowMessage(str_error, True)
                ' returnvalue = False
                ' Return returnvalue
                Exit Sub
            ElseIf fileCount <> 1 Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please select file!!!"
                Else
                    str_error = str_error & "Please select file!!!"
                End If
                '  returnvalue = False
                ' Return returnvalue
                ShowMessage(str_error, True)
                Exit Sub
            Else
                If Not (errCount = 0 And fileCount = 1) Then
                    ' Dim status As Integer = callSave(BSUID, ActivityID, strans)
                    ' If status = 1000 Then
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "Please upload file!"
                    Else
                        str_error = str_error & "Please upload file!"
                    End If
                    ShowMessage(str_error, True)
                    'returnvalue = False
                Else
                    'If str_error <> "" Then
                    '    str_error = str_error & "<br /><br />"
                    '    str_error = str_error & "File saved successfully!"
                    'Else
                    '    str_error = str_error & "File saved successfully!"
                    '    ShowMessage(str_error, True)
                    'End If
                    '   returnvalue = True
                    updpnl.Visible = False

                    lnkShowFile.Visible = True
                    lnkShowFile.Text = UploadfileName
                    Dim uploadpath As String = ("Temp\" & BSUID & "\" & DdlMerit.SelectedItem.Text & "\" & UploadfileName)
                    uploadpath = uploadpath.Replace("\", "\\")
                    Dim strExtn As String = System.IO.Path.GetExtension(UploadfileName).ToLower
                    lnkShowFile.Attributes.Add("OnClick", "javascript: return showDocument('" & uploadpath & "','" & strExtn & "')")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From saveupload_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region
End Class

