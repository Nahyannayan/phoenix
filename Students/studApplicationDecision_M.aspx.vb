Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studApplicationDecision_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S000210") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("SelectedRow") = -1

                    If ViewState("datamode") = "add" Then
                        hfAPL_ID.Value = 0
                        Session("dtReason") = SetDataTable()
                    Else

                        hfAPL_ID.Value = Encr_decrData.Decrypt(Request.QueryString("apl_id").Replace(" ", "+"))
                        txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("apl_descr").Replace(" ", "+"))
                        GeRows()
                        EnableDisableControls(False)
                    End If

                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            lblError.Text = "Records saved successfully"
            ViewState("datamode") = "none"
            EnableDisableControls(False)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvReason_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReason.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvReason.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtReason")

            Dim lblAplId As Label
            Dim lblReason As Label

            lblAplId = selectedRow.FindControl("lblAplId")
            lblReason = selectedRow.FindControl("lblReason")

            ReDim keys(0)

            keys(0) = lblReason.Text

            index = dt.Rows.IndexOf(dt.Rows.Find(keys))

            If e.CommandName = "Edit" Then
                ViewState("SelectedRow") = index
                btnAddNew.Text = "Update"

                With dt.Rows(index)
                    txtReason.Text = .Item(1)
                End With

                GridBind(dt)

            ElseIf e.CommandName = "Delete" Then
                If dt.Rows(index).Item(2) = "add" Then
                    dt.Rows(index).Item(2) = "remove"
                Else
                    dt.Rows(index).Item(2) = "delete"
                End If
                Session("dtReason") = dt
                GridBind(dt)
            End If

        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblError.Text = ""
            ClearControls()
            EnableDisableControls(True)
            ViewState("datamode") = "add"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            EnableDisableControls(True)
            ViewState("datamode") = "edit"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If

            txtReason.Text = ""
            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvReason_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReason.RowDeleting

    End Sub

    Protected Sub gvReason_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvReason.RowEditing

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""

            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                txtDescr.Text = ""
                EnableDisableControls(False)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "add" Then
                    ClearControls()
                End If
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            SaveData()
            ClearControls()
            ViewState("datamode") = "none"
            lblError.Text = ""
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function SetDataTable() As DataTable

        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "apr_id"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "apr_descr"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub GeRows()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dt As DataTable
        dt = SetDataTable()
        Dim dr As DataRow
        Dim str_query As String = "SELECT APR_ID,APR_DESCR FROM APPLDESC_REASON_M WHERE APR_APL_ID=" + hfAPL_ID.Value
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            dr = dt.NewRow
            dr.Item(0) = reader.GetValue(0).ToString
            dr.Item(1) = reader.GetString(1)
            dr.Item(2) = "edit"
            dt.Rows.Add(dr)
        End While

        gvReason.DataSource = dt
        gvReason.DataBind()
        Session("dtReason") = dt



        Dim lstrQuery As String = "SELECT APL_TYPE,APL_DISABLE FROM APPLICATIONDECISION_M WHERE APL_ID=" + hfAPL_ID.Value
        Using reader2 As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, lstrQuery)
            While reader2.Read
                If reader2("APL_TYPE") = "SCR" Then
                    ddlType.SelectedIndex = 0
                Else
                    ddlType.SelectedIndex = 1
                End If
                chkProceed.Checked = reader2("APL_DISABLE")

            End While
        End Using



    End Sub

    Sub AddRows()

        Dim dr As DataRow
        Dim dt As New DataTable
        dt = Session("dtReason")

        Dim keys As Object()
        ReDim keys(0)

        keys(0) = txtReason.Text

        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This record is already added"
            Exit Sub
        End If

        dr = dt.NewRow
        dr.Item(0) = 0
        dr.Item(1) = txtReason.Text
        dr.Item(2) = "add"
        dt.Rows.Add(dr)

        Session("dtReason") = dt

        GridBind(dt)
    End Sub

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtReason")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(1) = txtReason.Text
                If .Item(2) = "update" Then
                    .Item(2) = "edit"
                End If
            End With

            Session("dtReason") = dt

            GridBind(dt)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(2) <> "delete" And dt.Rows(i)(2) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvReason.DataSource = dtTemp
        gvReason.DataBind()
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection

            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = "exec SaveAPPLICATIONDECISION  " _
                                        & hfAPL_ID.Value + "," _
                                        & "'" + Session("sBsuid") + "'," _
                                        & "'" + txtDescr.Text + "'," _
                                        & "'" + chkProceed.Checked.ToString + "'," _
                                        & "'" + ViewState("datamode") + "'," _
                                        & "'" + ddlType.SelectedItem.Value + "'"

                hfAPL_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                If ViewState("datamode") = "delete" Then
                    transaction.Commit()
                    Exit Sub
                End If
                Dim dt As DataTable
                dt = Session("dtReason")
                Dim dr As DataRow

                For Each dr In dt.Rows
                    With dr
                        If .Item(2) <> "remove" And .Item(2) <> "update" Then
                            str_query = "exec SaveAPPLDESCREASON " _
                                      & hfAPL_ID.Value + "," _
                                      & .Item(0) + "," _
                                      & "'" + .Item(1) + "'," _
                                      & "'" + .Item(2) + "'"

                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                        End If
                    End With
                Next

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try

        End Using

    End Sub

    Sub EnableDisableControls(ByVal value As Boolean)
        txtDescr.Enabled = value
        txtReason.Enabled = value
        btnAddNew.Enabled = value
        chkProceed.Enabled = value
        gvReason.Enabled = value
    End Sub

    Sub ClearControls()
        txtDescr.Text = ""
        txtReason.Text = ""
        chkProceed.Checked = False
        Session("dtReason") = SetDataTable()
        gvReason.DataSource = Session("dtReason")
        gvReason.DataBind()
        hfAPL_ID.Value = 0
    End Sub
#End Region
End Class
