<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAcademicyear_D.aspx.vb" Inherits="Student_studAcademicyear_D" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <script language="javascript" type="text/javascript">


        function getAcademicAY() {

            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = 'ShowAcademicInfo.aspx?id=AY';
            var oWnd = radopen(url, "pop_acd");
            //if (mode=='AY')
            // {          
     <%--result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false; 
                 }   
             NameandCode = result.split('___');  
             document.getElementById("<%=txtAcadYear.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfACY_ID.ClientID %>").value=NameandCode[1];
             }--%>
             
             <%--else if(mode=='CU')
             {
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {   
             return false; 
                 } 
            
            NameandCode = result.split('___');  
            document.getElementById("<%=txtCurr.ClientID %>").value=NameandCode[0];
            document.getElementById("<%=hfCLM_ID.ClientID %>").value=NameandCode[1];
             }
            --%>

 }

        function OnClientClose(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.Acad.split('||');

                document.getElementById("<%=txtAcadYear.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfACY_ID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtAcadYear.ClientID%>', 'TextChanged');
            }
        }

        function getAcademicCU() {
            var NameandCode;
            var result;
            var url;
            url = 'ShowAcademicInfo.aspx?id=CU'
            var oWnd = radopen(url, "pop_curr");

        }
        function OnClientClose2(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Acad.split('||');
                document.getElementById("<%=txtCurr.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfCLM_ID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtCurr.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //alert("hello")
            result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures)
            if (result == '' || result == undefined) {

                return false;
            }

            if (val == 1) {
                document.getElementById('<%=txtFrom_date.ClientID %>').value = result;
            }
            else if (val == 2) {
                document.getElementById('<%=txtTo_date.ClientID %>').value = result;
        }

        else if (val == 3) {
            document.getElementById('<%=txtAttDate.ClientID %>').value = result;
   }

   else if (val == 4) {
       document.getElementById('<%=txtBrownBookDate.ClientID %>').value = result;
     }
}

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_acd" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_curr" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Academic Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0"
                                width="100%">

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Academic Year</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAcadYear" runat="server"></asp:TextBox>
                                        <%-- <asp:Button ID="btnAcademic" runat="server" CssClass="button" OnClientClick="getAcademicAY();return false;"
                                            Text="..." />--%>
                                        <asp:ImageButton ID="btnAcademic" runat="server" ImageUrl="../Images/cal.gif"
                                            OnClientClick="getAcademicAY();return false;" TabIndex="18"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvAcadYear" runat="server" ControlToValidate="txtAcadYear"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Academic Year is mandatory" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left">
                                        <span class="field-label">Curriculum</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCurr" runat="server"></asp:TextBox>
                                        <%-- <asp:Button
                                            ID="btnCurr" runat="server" CssClass="button" OnClientClick="getAcademicCU();return false;"
                                            Text="..." />--%>
                                        <asp:ImageButton ID="btnCurr" runat="server" ImageUrl="../Images/cal.gif"
                                            OnClientClick="getAcademicCU();return false;" TabIndex="18"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtCurr" CssClass="error" Display="Dynamic" ErrorMessage="Curriculum entry required"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Start Date</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFrom_date" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnFrom_date" TargetControlID="txtFrom_date">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Start  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Start Date entered is not a valid date" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvFrom_Date" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Start Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left">
                                        <span class="field-label">End Date</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTo_date" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnTo_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgbtnTo_date" TargetControlID="txtTo_date">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTo_date"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter End  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvTodate" runat="server" CssClass="error" Display="Dynamic"
                                            EnableViewState="False" ErrorMessage="End Date entered is not a valid date and must be greater than or equal to Start date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="End Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Attendance Closing date</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAttDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton1" TargetControlID="txtAttDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAttDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Attendance  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Start Date entered is not a valid date" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAttDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Start Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>--%>

                                    </td>
                                    <td align="left"><span class="field-label">Brown Book Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtBrownBookDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton2" TargetControlID="txtBrownBookDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtBrownBookDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Bronbook Date  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" CssClass="error" Display="Dynamic"
                                            EnableViewState="False" ErrorMessage="BrownBook Date entered is not a valid date and must be greater than or equal to Start date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtBrownBookDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="End Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>--%>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Current Academic Year</span> </td>

                                    <td align="left">
                                        <asp:CheckBox ID="chkCurrent" runat="server" /></td>
                                    <td align="left">
                                        <span class="field-label">Open for Online Registration</span> </td>

                                    <td align="left">
                                        <asp:CheckBox ID="chkOpenOnline" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Board
                            Affiliation No</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAffNo" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">MOE Affiliation No</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMoeAffNo" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">MOE
                            License No</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtLicense" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom"></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">&nbsp;<asp:HiddenField ID="hfACY_ID" runat="server" />
                            <asp:HiddenField ID="hfCLM_ID" runat="server" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

