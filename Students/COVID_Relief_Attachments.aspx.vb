Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net

Partial Class COVID_Relief_Attachments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try


                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("RRH_ID") = Encr_decrData.Decrypt(Request.QueryString("RRH_ID").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050114") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                'Else



                '    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                '    Call gridbind()


                '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                'End If
                gridbind(ViewState("RRH_ID"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
    End Sub



    Public Sub gridbind(ByRef RRH_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", RRH_ID)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GetAttachmentList", param)
            If ds.Tables(0).Rows.Count > 0 Then

                gvAttachment.DataSource = ds.Tables(0)
                gvAttachment.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvAttachment.DataSource = ds.Tables(0)
                Try
                    gvAttachment.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvAttachment.Rows(0).Cells.Count
                gvAttachment.Rows(0).Cells.Clear()
                gvAttachment.Rows(0).Cells.Add(New TableCell)
                gvAttachment.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAttachment.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAttachment.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lnkDownload_Click(sender As Object, e As EventArgs)
        Dim FilePath As String = TryCast(sender.FindControl("hf_PhotoPath"), HiddenField).Value
        DownloadFile(FilePath)
    End Sub
    'Sub DownloadFile(filename As String)
    '    Dim tempDir As String = Convert.ToString(ConfigurationManager.AppSettings("covidfiles"))
    '    Dim tempFileName As String = filename
    '    Dim tempFileNameUsed As String = tempDir + tempFileName

    '    Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)

    '    Response.Clear()
    '    Response.ClearHeaders()
    '    Response.ContentType = "application/octect-stream"
    '    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
    '    Response.BinaryWrite(bytes)
    '    Response.Flush()
    '    Response.End()
    'End Sub

    Sub DownloadFile(filename As String)
        Dim tempDir As String = Convert.ToString(ConfigurationManager.AppSettings("covidfiles"))
        Dim tempFileName As String = filename
        Dim tempFileNameUsed As String = tempDir + tempFileName


       
        Dim myWebClient As New WebClient
        ''myWebClient.DownloadFile(tempFileNameUsed, filename)

        Dim data As Byte() = myWebClient.DownloadData(tempFileNameUsed)
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(data)
        Response.Flush()


       
    End Sub
End Class
