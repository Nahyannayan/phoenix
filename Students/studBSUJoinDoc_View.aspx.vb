Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studBSUJoinDoc_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                GridBind()

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

    Protected Sub gvStage_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStage.RowCommand
        If e.CommandName = "View" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStage.Rows(index), GridViewRow)

            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            Dim lblStgId As Label
            Dim lblStage As Label
         
            With selectedRow
                lblStage = .FindControl("lblStage")
                lblStgId = .FindControl("lblStgId")
            End With

            Dim url As String
            ViewState("datamode") = Encr_decrData.Encrypt("none")
            url = String.Format("~\Students\studBSUJoinDoc_M.aspx?MainMnu_code={0}&datamode={1}&stgid=" + Encr_decrData.Encrypt(lblStgId.Text) _
                 & "&stage=" + Encr_decrData.Encrypt(lblStage.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                 & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        End If
    End Sub
    Protected Sub gvStage_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStage.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStgid As New Label
          
            lblStgid = e.Row.FindControl("lblStgId")

            Dim gvDocs As New GridView
            gvDocs = e.Row.FindControl("gvDocs")
            BindDocuments(lblStgid.Text, gvDocs)
        End If
    End Sub
#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT B.PRB_ID AS GUID,CASE PRO_DESCRIPTION WHEN 'APPLICANT' " _
                               & " THEN 'SHORTLISTED' ELSE PRO_DESCRIPTION END AS PRO_DESCRIPTION,PRB_STG_ID FROM PROCESSFO_SYS_M AS A " _
                               & " INNER JOIN PROCESSFO_BSU_M AS B ON A.PRO_ID=B.PRB_STG_ID" _
                               & " WHERE PRB_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "  AND" _
                               & " PRB_bREQUIRED='TRUE' ORDER BY PRB_STG_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvStage.DataSource = ds
        gvStage.DataBind()
    End Sub

    Private Sub BindDocuments(ByVal StgId As String, ByVal gvDocs As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT DOC_DESCR,DOC_TYPE=CASE DCB_TYPE WHEN 'DUP' THEN 'Duplicate' " _
        '                         & " ELSE 'Original' END,DOC_APPLY=CASE DCB_APPLYTO WHEN 'EXP' THEN 'Expatriate' " _
        '                         & " WHEN 'NAT' THEN 'Local' ELSE 'All' END,DCB_COPIES FROM BSU_JOINDOCUMENTS_M AS A" _
        '                         & " INNER JOIN DOCREQD_M AS B ON A.DCB_DOC_ID=B.DOC_ID WHERE " _
        '                         & " DCB_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND DCB_STG_ID=" + StgId _
        '                         & " ORDER BY DOC_DESCR"

        ''Updated version
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OPTION", 1)
        param(1) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param(2) = New SqlClient.SqlParameter("@STG_ID", StgId)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GETJOIN_DOCUMENTS", param)

        gvDocs.DataSource = ds
        gvDocs.DataBind()
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub
End Class
