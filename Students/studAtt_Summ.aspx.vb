Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports InfoSoftGlobal
Imports Microsoft.Win32
Partial Class Students_stuEnquiryPrint
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim menu_rights As String = String.Empty
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059023") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    Try


                        Dim pageKey As RegistryKey = Registry.CurrentUser.OpenSubKey("software\microsoft\internet explorer\pagesetup", True)
                        Dim header, footer As String
                        'Dim oriFooter As Object = pageKey.GetValue("footerTemp")

                        footer = pageKey.GetValue("footer", 0)
                        header = pageKey.GetValue("header", 0)

                        pageKey.SetValue("footer", "")
                        pageKey.SetValue("header", "")
                        ' pageKey.DeleteValue("footerTemp")

                        pageKey.Close()

                        'ClientScript.RegisterStartupScript(Me.GetType, "error", "<script language='Javascript'>window.close();</script>")

                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message)
                    End Try



                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Dim TODT As String = Session("TO_Dt") ' String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
                    Dim FROMDT As String = Session("FROM_Dt")
                    If FROMDT = TODT Then
                        ltHeader.Text = "Attendance Summary for the class marked " & TODT
                    Else
                        ltHeader.Text = "Attendance Summary for the class marked from " & FROMDT & " to " & TODT
                    End If

                    Dim ATT_PER As Double = GETVALID_ATTDay()
                    ltSchoolPer.Text = "Overall attendance % for the classes marked " & Math.Round(ATT_PER, 2) & "% "
                    HiddenBsuId.Value = Session("sbsuid")
                    HiddenAcademicyear.Value = Session("ACD_SEL_ATT")
                    BindBsuDetails()
                    CreateCharts_Main()
                    CreateCharts_sub()
                    AttDASHBOARD()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                    'Session("TO_Dt") = Nothing
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub
    Private Function GETVALID_ATTDay() As Double
        Dim ACD_ID As String = Session("ACD_SEL_ATT")
        Dim BSU_ID As String = Session("sBsuid")
        Dim FROMDT As String = Session("FROM_Dt")
        Dim TODT As String = Session("TO_Dt")
        'Dim attday As Double
        Try



            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@TODT", TODT)
            pParms(3) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_ATTDetail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GETSCHOOL_ATT_PER", pParms)

                If readerStudent_ATTDetail.HasRows = True Then
                    While readerStudent_ATTDetail.Read
                        GETVALID_ATTDay = readerStudent_ATTDetail("ATT_PER")
                    End While
                End If
            End Using
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Sub BindBsuDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select BSU_NAME,BSU_ADDRESS,BSU_POBOX,BSU_CITY,BSU_TEL,BSU_FAX,BSU_EMAIL,BSU_MOE_LOGO,BSU_URL from BUSINESSUNIT_M where BSU_ID='" & HiddenBsuId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        BsuName.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
        'bsuAddress.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_ADDRESS").ToString()
        'bsupostbox.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_POBOX").ToString()
        'bsucity.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_CITY").ToString()
        'bsutelephone.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_TEL").ToString()
        'bsufax.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_FAX").ToString()
        'bsuemail.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_EMAIL").ToString()
        'bsuwebsite.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_URL").ToString()
        imglogo.ImageUrl = ds.Tables(0).Rows(0).Item("BSU_MOE_LOGO").ToString()
    End Sub

    Function DateFormat(ByVal fDate As DateTime) As String
        Return Format(fDate, "dd/MMM/yyyy").Replace("01/Jan/1900", "")
    End Function

    Sub CreateCharts_Main()
        'In this example, we show how to connect FusionCharts to a database.
        'For the sake of ease, we've used an Access database which is present in
        '../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
        'other. 

        'Database Objects - Initialization

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FROMDT As String = Session("FROM_Dt")
        Dim TODT As String = Session("TO_Dt")

        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("ACD_SEL_ATT"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlClient.SqlParameter("@TODT", TODT)
        param(3) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
        Dim strXML As String
        Dim group_descr As String = String.Empty
        Dim group_per As String = String.Empty
        Dim VGA_ID As Integer
        'strXML will be used to store the entire XML document generated


        Dim arr() As String
        arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", "e6acef"}
        Dim i As Integer = 0



        strXML = ""
        strXML = strXML & "<graph caption='Attendance Group Wise' xAxisName='Group' yaxismaxvalue='100' yaxisminvalue='0'  yAxisName='Percent' decimalPrecision='0' formatNumberScale='0' rotateNames='0' canvasbordercolor='f8f9fa' canvasBorderThickness='0'>"
        'link='javascript:AddMainDetails(" & VGA_ID.ToString & ");' 
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART1]", param)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read


                    group_descr = readerStudent_Detail("GROUP_NAME").ToString
                    group_per = readerStudent_Detail("TOT_PERC").ToString
                    VGA_ID = readerStudent_Detail("VGA_ID").ToString
                    strXML = strXML & "<set name=" + "'" & group_descr & "' value=" + "'" & group_per & "' color=" + "'" & arr(i) & "'  />"
                    i = i + 1
                End While
                ' Dim strline As String = "<trendlines><line startvalue='75' displayValue='Good-above 75' color='FF0000' thickness='1' isTrendZone='0'/><line startvalue='50' displayValue='Bad-below 50' color='009999' thickness='1' isTrendZone='0'/></trendlines>"
                strXML = strXML & "</graph>"
                ltmain.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column2D.swf", "", strXML, "subgroup", "550", "300", False)
            Else
                'lblerror.Text = "No Records Found "

                strXML = ""
                strXML = strXML & "<graph caption='Attendance Group Wise' xAxisName='Group' yAxisName='Percent' decimalPrecision='0' formatNumberScale='0'>"
                strXML = strXML & "</graph>"
                ltmain.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column2D.swf", "", strXML, "subgroup", "550", "300", False)
            End If
        End Using




        'strXML will be used to store the entire XML document generated


        'Generate the graph element
        'strXML = "<graph caption='Attendance Group Wise'    decimalPrecision='0' showNames='1' numberSuffix=' %' pieSliceDepth='30' formatNumberScale='0' >"

        '' SQL Query
        'Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART1]", param)

        '    If readerStudent_Detail.HasRows = True Then
        '        While readerStudent_Detail.Read

        '            strXML = strXML & "<set name='" & readerStudent_Detail("GROUP_NAME").ToString & "' value='" & readerStudent_Detail("TOT_PERC").ToString & "'/>"
        '        End While
        '    End If
        'End Using

        '' Open data reader

        ''Iterate through each factory

        ''Finally, close <graph> element
        'strXML = strXML & "</graph>"


        ''Create the chart - Pie 3D Chart with data from strXML
        'Return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "FactorySum", "500", "300", False, False)
    End Sub
    Sub CreateCharts_sub()


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FROMDT As String = Session("FROM_Dt")
        Dim TODT As String = Session("TO_Dt")

        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("ACD_SEL_ATT"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlClient.SqlParameter("@TODT", TODT)
        param(3) = New SqlClient.SqlParameter("@FROMDT", FROMDT)

        Dim strXML As String
        Dim d1 As String = "2"
        Dim dAmt As String = "4"
        'strXML will be used to store the entire XML document generated
        Dim grd_descr As String = String.Empty
        Dim grd_per As String = String.Empty
        'strXML will be used to store the entire XML document generated


        Dim arr() As String
        arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", _
"e6acef", "efdc07", "73d1f8", "ee735b", "f704dd", "b8f149", "f1cb49", "a849f1", "07daf9", "91b8be", _
"d2cad6", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61", _
"52ed4e", "0a937e", "80269e", "9e266d", "f4e8b0", "8a008c", "288c00", "8e006e", "70e2bb", _
"b3bec3", "9bc101", "9b01c1", "b68db6", "aae6bf", "b9cf75", "d5aff0", "f0baaf", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61"}
        Dim i As Integer = 0



        strXML = ""
        strXML = strXML & "<graph caption='Attendance Grade Wise' xAxisName='Grade' yaxismaxvalue='100' yaxisminvalue='0'  yAxisName='Percent' decimalPrecision='0' formatNumberScale='0' rotateNames='1' showLegend='1' canvasbordercolor='f8f9fa' canvasBorderThickness='0'>"

        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART2]", param)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read


                    grd_descr = readerStudent_Detail("GRD_DES").ToString
                    grd_per = readerStudent_Detail("TOT_PERC").ToString

                    strXML = strXML & "<set name=" + "'" & grd_descr & "' value=" + "'" & grd_per & "' color=" + "'" & arr(i) & "' link='javascript:AddDetails(" & readerStudent_Detail("GRM_ID").ToString & ");' />"
                    i = i + 1
                End While
                'Dim strline As String = "<trendlines><line startvalue='75' displayValue='Good-above 75' color='FF0000' thickness='1' isTrendZone='0'/><line startvalue='50' displayValue='Bad-below 50' color='009999' thickness='1' isTrendZone='0'/></trendlines>"
                strXML = strXML & "</graph>"

                ltsub.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column2D.swf", "", strXML, "subgrade", "450", "300", False)
            Else
                'lblerror.Text = "No Records Found "

                strXML = ""
                strXML = strXML & "<graph caption='Attendance Grade Wise' xAxisName='Section' yAxisName='Percent' decimalPrecision='0' formatNumberScale='0'>"
                strXML = strXML & "</graph>"
                ltsub.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column2D.swf", "", strXML, "subgrade", "450", "300", False)
            End If
        End Using







        ''Generate the graph element
        'strXML = "<graph caption='Attendance Grade Wise' decimalPrecision='0' showNames='1' showLegend='1' numberSuffix=' %' pieSliceDepth='30' formatNumberScale='0' >"

        '' SQL Query
        'Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART2]", param)

        '    If readerStudent_Detail.HasRows = True Then
        '        While readerStudent_Detail.Read

        '            strXML = strXML & "<set name='" & readerStudent_Detail("GRD_DES").ToString & "' value='" & readerStudent_Detail("TOT_PERC").ToString & "'  link='javascript:AddDetails(" & readerStudent_Detail("GRM_ID").ToString & ");'/>"


        '        End While
        '    End If
        'End Using

        '' Open data reader

        ''Iterate through each factory

        ''Finally, close <graph> element
        ''strXML = strXML & "<set name='GRADE 9' value='80' link='" & Server.UrlEncode("Detailed.aspx?FactoryId=9") & "'/>"
        ''strXML = strXML & "<set name='GRADE 10' value='90' link='" & Server.UrlEncode("Detailed.aspx?FactoryId=5") & "'/>"
        'strXML = strXML & "</graph>"
        ''Create the chart - Pie 3D Chart with data from strXML
        'Return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Fact", "500", "300", False, False)
    End Sub

    Protected Sub btnPrint0_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ACD_ID As String = Session("ACD_SEL_ATT")
        Dim BSU_ID As String = Session("sBsuid")
        Dim TODT As String = Session("TO_Dt") 'String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
        Dim FROMDT As String = Session("FROM_Dt")

        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@FROMDT", FROMDT)
        param.Add("@TODT", TODT)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ReportHeader", ltSchoolPer.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptAtt_School_Grade_Detail.rpt")
            '.reportPath = Server.MapPath("/Students/Reports/RPT/rptAtt_School_Grade_Detail.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        Session("ReportSel") = ""
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub AttDASHBOARD()

        Dim ACD_ID As String = Session("ACD_SEL_ATT")
        Dim BSU_ID As String = Session("sBsuid")
        Dim TODT As String = Session("TO_Dt") 'String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
        Dim FROMDT As String = Session("FROM_Dt")

        Dim var1 As String = String.Empty
        Try


            Dim ds As New DataSet
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
            pParms(3) = New SqlClient.SqlParameter("@TODT", TODT)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD]", pParms)




            If ds.Tables(0).Rows.Count > 0 Then

                gvAttQuick.DataSource = ds.Tables(0)
                gvAttQuick.DataBind()
                If Session("TO_Dt") <> Session("FROM_Dt") Then
                    gvAttQuick.Columns(3).Visible = False
                    gvAttQuick.Columns(4).Visible = False
                    gvAttQuick.Columns(5).Visible = False
                End If
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvAttQuick.DataSource = ds.Tables(0)
                Try
                    gvAttQuick.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAttQuick.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAttQuick.Rows(0).Cells.Clear()
                gvAttQuick.Rows(0).Cells.Add(New TableCell)
                gvAttQuick.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAttQuick.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAttQuick.Rows(0).Cells(0).Text = "No Record Is Available"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub


    Protected Sub gvAttQuick_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)


        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lbl As Label = DirectCast(e.Row.FindControl("lblGroup"), Label)
            If lbl.Text.Trim = "" Then

                lbl.Visible = False
            Else

               
                If Session("TO_Dt") = Session("FROM_Dt") Then
                    e.Row.Cells(0).ColumnSpan = 5
                    e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left

                    e.Row.Cells(1).Visible = False
                    e.Row.Cells(2).Visible = False
                    e.Row.Cells(3).Visible = False
                    e.Row.Cells(4).Visible = False
                    e.Row.Cells(5).Visible = False
                    ' e.Row.Cells(6).Visible = False


                    lbl.Visible = True

                    If lbl.Text.Trim = "Overall School % for the classes marked" Then
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#c6d2e2")
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#c6d2e2")
                        e.Row.Cells(0).ForeColor = Drawing.Color.White
                        'e.Row.Cells(0).Font.Size = 8
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#7497c5")
                        e.Row.Cells(6).ForeColor = Drawing.Color.White
                        ' e.Row.Cells(6).Font.Size = 8
                        e.Row.Cells(6).Font.Bold = True
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#7497c5")
                    ElseIf lbl.Text.Contains("Single Group") = True Then
                        e.Row.Visible = False
                    ElseIf lbl.Text.Contains("#overall") = True Then
                        lbl.Text = lbl.Text.Replace("#", "")
                        e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#a2b3ca")
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#a2b3ca")
                        e.Row.Cells(0).ForeColor = Drawing.Color.White
                        'e.Row.Cells(0).Font.Size = 8
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(6).ForeColor = Drawing.Color.White
                        'e.Row.Cells(6).Font.Size = 8
                        e.Row.Cells(6).Font.Bold = True
                    Else
                        ' e.Row.Cells(0).Font.Size = 8
                        e.Row.Cells(0).Font.Bold = True
                        'e.Row.Cells(6).Font.Size = 8
                        e.Row.Cells(6).Font.Bold = True
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#c6d2e2")
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#c6d2e2")
                    End If




                Else

                    e.Row.Cells(0).ColumnSpan = 2
                    e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left

                    e.Row.Cells(1).Visible = False
                    e.Row.Cells(2).Visible = False
                    e.Row.Cells(3).Visible = False
                    e.Row.Cells(4).Visible = False
                    e.Row.Cells(5).Visible = False

                    lbl.Visible = True

                    If lbl.Text.Trim = "Overall School % for the classes marked" Then
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#c6d2e2")
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#c6d2e2")
                        e.Row.Cells(0).ForeColor = Drawing.Color.White
                        'e.Row.Cells(0).Font.Size = 8
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#7497c5")
                        e.Row.Cells(6).ForeColor = Drawing.Color.White
                        'e.Row.Cells(6).Font.Size = 8
                        e.Row.Cells(6).Font.Bold = True
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#7497c5")
                    ElseIf lbl.Text.Contains("Single Group") = True Then
                        e.Row.Visible = False
                    ElseIf lbl.Text.Contains("#overall") = True Then
                        lbl.Text = lbl.Text.Replace("#", "")
                        e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#a2b3ca")
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#a2b3ca")
                        e.Row.Cells(0).ForeColor = Drawing.Color.White
                        'e.Row.Cells(0).Font.Size = 8
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(6).ForeColor = Drawing.Color.White
                        ' e.Row.Cells(6).Font.Size = 8
                        e.Row.Cells(6).Font.Bold = True
                    Else
                        'e.Row.Cells(0).Font.Size = 8
                        e.Row.Cells(0).Font.Bold = True
                        'e.Row.Cells(6).Font.Size = 8
                        e.Row.Cells(6).Font.Bold = True
                        e.Row.Cells(0).BackColor = Drawing.Color.FromName("#c6d2e2")
                        e.Row.Cells(6).BackColor = Drawing.Color.FromName("#c6d2e2")
                    End If
                End If
               

               
            End If
        End If




    End Sub

    Protected Sub gvAttQuick_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAttQuick.PageIndexChanging
        gvAttQuick.PageIndex = e.NewPageIndex
        Call AttDASHBOARD()

    End Sub

    Protected Sub imgbtnback_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnback.Click

        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        Dim url As String = String.Empty
        url = String.Format("~\Students\studAtt_Summ_view.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)



        'Response.Redirect("~/students/studAtt_Summ_view.aspx")
    End Sub
End Class
