Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studEnquiryTransferReq_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                 Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100100") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "view"
                    hfEQS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfEQS_ACY_ID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsacyid").Replace(" ", "+"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
            BindShift()
            BindServices()
            BindCurriculum()
            rdTNo.Checked = True
            ddlPickUp.Enabled = False
            ddlLocation.Enabled = False
            ddlSubLocation.Enabled = False
        Else
        End If
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        SaveData()

    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub SaveData()
        Dim str_query As String

        Dim transaction As SqlTransaction
        Dim bSave As Boolean = False
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SCHOOLPRIO_S", "EQS_ID", "EQS_EQM_ENQID", "EQS_ID=" + hfEQS_ID.Value.ToString)
                If rdTYes.Checked = True Then
                    str_query = "exec studEnquiryTransferAccept  " + hfEQS_ID.Value.ToString + "," _
                                                                & ddlCurriculum.SelectedValue.ToString + "," _
                                                                & hfEQS_ACY_ID.Value + "," _
                                                                & ddlShift.SelectedValue + "," _
                                                                & "1," _
                                                                & ddlLocation.SelectedValue.ToString + "," _
                                                                & ddlSubLocation.SelectedValue.ToString + "," _
                                                                & ddlPickUp.SelectedValue.ToString + ",'" _
                                                                & txtTptRemarks.Text.Trim + "'"

                Else
                    str_query = "exec studEnquiryTransferAccept  " + hfEQS_ID.Value.ToString + "," _
                                                               & ddlCurriculum.SelectedValue.ToString + "," _
                                                               & hfEQS_ACY_ID.Value + "," _
                                                               & ddlShift.SelectedValue + "," _
                                                               & "0,NULL,NULL,NULL,NULL"
                End If
                Dim reader As SqlDataReader = SqlHelper.ExecuteReader(transaction, CommandType.Text, str_query)
                While reader.Read
                    If reader.GetValue(0) = 0 Or reader.GetValue(0) = -1 Then
                        lblError.Text = "Please contact school"
                        reader.Close()
                        Exit Sub
                    End If
                    hfEQS_TRANSFERID.Value = reader.GetValue(0)
                    hfACD_TRANSFERID.Value = reader.GetValue(1)
                End While
                reader.Close()


                SaveServices(transaction)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EQS_ID(" + hfEQS_ID.Value.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EQS_ID(" + hfEQS_TRANSFERID.Value.ToString + ")", "insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If


                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

                bSave = True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

            If bSave = True Then
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End Using
    End Sub

    Sub BindShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct SHF_DESCR,SHF_ID FROM SHIFTS_M AS A INNER JOIN " _
                                & " GRADE_BSU_M AS B ON A.SHF_ID=B.GRM_SHF_ID WHERE GRM_GRD_ID='" + hfGRD_ID.Value.ToString + "' AND SHF_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
        If ddlShift.Items.Count > 0 Then
            ddlShift.Items(0).Selected = True
        End If
        If ddlShift.Items.Count = 1 Then
            lblText.Text = "Shift"
            ddlShift.Visible = False
            lblShift.Text = ddlShift.Items(0).Text
        Else
            lblText.Text = "Select shift"
            lblShift.Visible = False
        End If
    End Sub

    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT CLM_ID,CLM_DESCR FROM CURRICULUM_M AS A INNER JOIN " _
                               & " ACADEMICYEAR_D AS B ON A.CLM_ID=B.ACD_CLM_ID WHERE " _
                               & " ACD_ACY_ID=" + hfEQS_ACY_ID.Value + " AND ACD_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurriculum.DataSource = ds
        ddlCurriculum.DataTextField = "CLM_DESCR"
        ddlCurriculum.DataValueField = "CLM_ID"
        ddlCurriculum.DataBind()
        If ddlCurriculum.Items.Count = 1 Then
            lblCrText.Text = "Curriculum"
            ddlCurriculum.Visible = False
            lblCurr.Text = ddlCurriculum.Items(0).Text
        Else
            lblCrText.Text = "Select Curriculum"
            lblCurr.Visible = False
        End If
    End Sub
    Sub BindServices()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SVC_ID,SVC_DESCRIPTION FROM SERVICES_SYS_M AS A " _
                                & " INNER JOIN SERVICES_BSU_M AS B ON A.SVC_ID=B.SVB_SVC_ID WHERE " _
                                & " SVC_ID<>1 AND SVB_BSU_ID='" + Session("sbsuid") + "' AND SVB_BAVAILABLE=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvServices.DataSource = ds
        gvServices.DataBind()
    End Sub

    Sub BindLocation()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT loc_description,loc_id FROM transport.vv_location_m WHERE loc_bsu_id='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "loc_description"
        ddlLocation.DataValueField = "loc_id"
        ddlLocation.DataBind()
    End Sub

    Sub BindSubLocation()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT sbl_description,sbl_id FROM transport.vv_sublocation_m WHERE sbl_bsu_id='" + Session("sbsuid") + "' and sbl_loc_id=" + ddlLocation.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlSubLocation.DataSource = ds
        ddlSubLocation.DataTextField = "sbl_description"
        ddlSubLocation.DataValueField = "sbl_id"
        ddlSubLocation.DataBind()
    End Sub

    Sub BindPickUp()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT pnt_description,pnt_id FROM transport.vv_pickupoints_m WHERE pnt_bsu_id='" + Session("sbsuid") + "' and pnt_sbl_id=" + ddlSubLocation.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPickUp.DataSource = ds
        ddlPickUp.DataTextField = "pnt_description"
        ddlPickUp.DataValueField = "pnt_id"
        ddlPickUp.DataBind()
    End Sub
   
   
    Sub SaveServices(ByVal transaction As SqlTransaction)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim lblSvcId As Label
        Dim rdY As RadioButton
        Dim status As Integer
        If rdTYes.Checked = True Then
            str_query = "exec studSAVEENQSERVICES '" + Session("sbsuid").ToString + "'," + hfACD_TRANSFERID.Value + "," + hfEQS_TRANSFERID.Value.ToString + ",1," + ddlLocation.SelectedValue.ToString + "," + ddlSubLocation.SelectedValue.ToString + "," + ddlPickUp.SelectedValue.ToString + "," + rdTYes.Checked.ToString
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
        End If
        With gvServices
            For i = 0 To .Rows.Count - 1
                lblSvcId = .Rows(i).FindControl("lblSvcId")
                rdY = .Rows(i).FindControl("rdYes")
                If rdY.Checked = True Then
                    status = 1
                Else
                    status = 0
                End If
                str_query = "exec studSAVEENQSERVICES '" + Session("sbsuid").ToString + "'," + hfACD_TRANSFERID.Value + "," + hfEQS_TRANSFERID.Value.ToString + "," + lblSvcId.Text.ToString + ",0,0,0," + rdY.Checked.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
            Next
        End With
    End Sub


#End Region

    Protected Sub rdTYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdTYes.CheckedChanged
        ddlPickUp.Enabled = True
        ddlLocation.Enabled = True
        ddlSubLocation.Enabled = True
        BindLocation()
        BindSubLocation()
        BindPickUp()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindSubLocation()
        BindPickUp()
    End Sub

    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        BindPickUp()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
