Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class studCompEdit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state
                'Dim ss As String = Request.UrlReferrer.ToString()
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050075" And ViewState("MainMnu_code") <> "S050076") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    'get the menucode to confirm the user is accessing the valid page
                    ' InitialiseComponents()
                    Session("Comp_FeeSetup") = CreateDataTable()

                    btnUpdate.Visible = False
                    btnChildCancel.Visible = False

                    If ViewState("datamode") = "view" Then
                        btnFill.Visible = False
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call COMP_LISTED_M(ViewState("viewid"))
                        setModifyHeader(ViewState("viewid"))
                        '    Call set_State()
                    ElseIf ViewState("datamode") = "add" Then
                        btnFill.Visible = True
                        '    Call clearMe()
                        '    Call Reset_State()
                    End If
                    rdYes.Checked = True
                    GetCountry_info()
                    GetEmirate_info()
                    GetFee_info()
                    ViewState("iID") = 0

                    gvFeeDetail.DataBind()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

#Region "Private function"
    Sub COMP_LISTED_M(ByVal COMP_ID As String)


        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        'arInfo = info.Split(splitter)
        Try


            Using readerCOMP_LISTED_M As SqlDataReader = AccessStudentClass.GetCOMP_LISTED_M(ViewState("viewid"))
                If readerCOMP_LISTED_M.HasRows = True Then
                    While readerCOMP_LISTED_M.Read

                        'handle the null value returned from the reader incase  convert.tostring

                        txtComp.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_NAME"))
                        txtADDR1.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_ADDR1"))
                        txtADDR2.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_ADDR2"))
                        txtCity.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_CITY"))

                        txtPOBOX.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_POBOX"))

                        txtEmail.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_EMAIL"))
                        txtURL.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_URL"))
                        txtContactPer.Text = Convert.ToString(readerCOMP_LISTED_M("COMP_CONTACT"))

                        ViewState("CTY_ID") = Convert.ToString(readerCOMP_LISTED_M("COMP_COUNTRY"))
                        ViewState("EMR_CODE") = Convert.ToString(readerCOMP_LISTED_M("COMP_EMIRATE"))

                        Temp_Phone_Split = Convert.ToString(readerCOMP_LISTED_M("COMP_PHONE"))

                        arInfo = Temp_Phone_Split.Split(splitter)

                        If arInfo.Length > 2 Then
                            txtOffPhone_Country.Text = arInfo(0)
                            txtOffPhone_Area.Text = arInfo(1)
                            txtOffPhone_No.Text = arInfo(2)

                        End If

                        Temp_Phone_Split = Convert.ToString(readerCOMP_LISTED_M("COMP_FAX"))

                        arInfo = Temp_Phone_Split.Split(splitter)

                        If arInfo.Length > 2 Then
                            txtFax_Country.Text = arInfo(0)
                            txtFax_Area.Text = arInfo(1)
                            txtFax_No.Text = arInfo(2)
                        End If

                        Temp_Phone_Split = Convert.ToString(readerCOMP_LISTED_M("COMP_CONTACT_MOBILE"))

                        arInfo = Temp_Phone_Split.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMobile_Country.Text = arInfo(0)
                            txtMobile_Area.Text = arInfo(1)
                            txtMobile_No.Text = arInfo(2)
                        End If

                    End While

                Else
                End If

            End Using
        Catch ex As Exception
            lblError.Text = "Error while populating company record"
        End Try
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT COMP_FEECOMPO_S.CIS_ID as Id,COMP_FEECOMPO_S.CIS_ID as CIS_ID,COMP_FEECOMPO_S.CIS_COMP_ID as CIS_COMP_ID, COMP_FEECOMPO_S.CIS_FEE_ID as CIS_FEE_ID," & _
" FEES.FEES_M.FEE_DESCR as CIS_FEE_Descr,case COMP_FEECOMPO_S.CIS_bPreformaInvoice when 1 then 'Yes' else 'No' end as CIS_bPreformaInvoice," & _
" COMP_FEECOMPO_S.CIS_DiscPerct as CIS_DiscPerct,'Normal' AS Status  FROM  COMP_FEECOMPO_S " & _
" INNER JOIN    FEES.FEES_M ON COMP_FEECOMPO_S.CIS_FEE_ID = FEES.FEES_M.FEE_ID where  CIS_COMP_ID='" & p_Modifyid & "'"




            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Session("Comp_FeeSetup") = ds.Tables(0)
            gridbind()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub set_State()
        txtComp.Attributes.Add("Readonly", "Readonly")
        txtADDR1.Attributes.Add("Readonly", "Readonly")
        txtADDR2.Attributes.Add("Readonly", "Readonly")
        txtCity.Attributes.Add("Readonly", "Readonly")
        txtPOBOX.Attributes.Add("Readonly", "Readonly")
        txtOffPhone_Country.Attributes.Add("Readonly", "Readonly")
        txtOffPhone_Area.Attributes.Add("Readonly", "Readonly")
        txtOffPhone_No.Attributes.Add("Readonly", "Readonly")
        txtFax_Country.Attributes.Add("Readonly", "Readonly")
        txtFax_Area.Attributes.Add("Readonly", "Readonly")
        txtFax_No.Attributes.Add("Readonly", "Readonly")
        txtEmail.Attributes.Add("Readonly", "Readonly")
        txtURL.Attributes.Add("Readonly", "Readonly")
        txtContactPer.Attributes.Add("Readonly", "Readonly")
        txtMobile_Country.Attributes.Add("Readonly", "Readonly")
        txtMobile_Area.Attributes.Add("Readonly", "Readonly")
        txtMobile_No.Attributes.Add("Readonly", "Readonly")

        If ViewState("datamode") = "edit" Or ViewState("datamode") = "none" Or ViewState("datamode") = "view" Then

            ddlCountry.Enabled = False
            ddlEmirate.Enabled = False


        ElseIf ViewState("datamode") = "add" Then
            ddlCountry.SelectedIndex = 0
            ddlEmirate.SelectedIndex = 0

            ddlCountry.Enabled = False
            ddlEmirate.Enabled = False



        End If

    End Sub
    Sub Reset_State()
        txtComp.Attributes.Remove("Readonly")
        txtADDR1.Attributes.Remove("Readonly")
        txtADDR2.Attributes.Remove("Readonly")
        txtCity.Attributes.Remove("Readonly")
        txtPOBOX.Attributes.Remove("Readonly")
        txtOffPhone_Country.Attributes.Remove("Readonly")
        txtOffPhone_Area.Attributes.Remove("Readonly")
        txtOffPhone_No.Attributes.Remove("Readonly")
        txtFax_Country.Attributes.Remove("Readonly")
        txtFax_Area.Attributes.Remove("Readonly")
        txtFax_No.Attributes.Remove("Readonly")
        txtEmail.Attributes.Remove("Readonly")
        txtURL.Attributes.Remove("Readonly")
        txtContactPer.Attributes.Remove("Readonly")
        txtMobile_Country.Attributes.Remove("Readonly")
        txtMobile_Area.Attributes.Remove("Readonly")
        txtMobile_No.Attributes.Remove("Readonly")

        If ViewState("datamode") = "edit" Or ViewState("datamode") = "none" Or ViewState("datamode") = "view" Then

            ddlCountry.Enabled = True
            ddlEmirate.Enabled = True


        ElseIf ViewState("datamode") = "add" Then
            ddlCountry.SelectedIndex = 0
            ddlEmirate.SelectedIndex = 0

            ddlCountry.Enabled = True
            ddlEmirate.Enabled = True



        End If

    End Sub
    Sub clearMe()
        txtComp.Text = ""
        txtADDR1.Text = ""
        txtADDR2.Text = ""
        txtCity.Text = ""
        ddlCountry.SelectedIndex = 0
        txtPOBOX.Text = ""
        ddlEmirate.SelectedIndex = 0
        txtOffPhone_Country.Text = ""
        txtOffPhone_Area.Text = ""
        txtOffPhone_No.Text = ""
        txtFax_Country.Text = ""
        txtFax_Area.Text = ""
        txtFax_No.Text = ""
        txtEmail.Text = ""
        txtURL.Text = ""
        txtContactPer.Text = ""
        txtMobile_Country.Text = ""
        txtMobile_Area.Text = ""
        txtMobile_No.Text = ""

    End Sub

    Sub GetCountry_info()
        Try
            ddlCountry.Items.Clear()
            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        ddlCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                    End While
                End If
            End Using
            If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then
                For ItemTypeCounter As Integer = 0 To ddlCountry.Items.Count - 1
                    If ddlCountry.Items(ItemTypeCounter).Value = ViewState("CTY_ID") Then
                        ddlCountry.SelectedIndex = ItemTypeCounter
                    End If
                Next
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    Sub GetFee_info()
        Try
            ddlFeeType.Items.Clear()

            Using FeeType_reader As SqlDataReader = AccessStudentClass.GetFee_Type()
                If FeeType_reader.HasRows = True Then
                    While FeeType_reader.Read
                        ddlFeeType.Items.Add(New ListItem(FeeType_reader("FEE_DESCR"), FeeType_reader("FEE_ID")))
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetFEE_info")
        End Try
    End Sub
    Public Shared Function GetEmirate(Optional ByVal ENQ_GROUP As Integer = 1, Optional ByVal EMR_CTY_ID As Int16 = 0) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get Emirate data from Emirate_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmirate As String = "", SQLCTY As String = ""
        If EMR_CTY_ID <> 0 Then
            SQLCTY = " OR EMR_CTY_ID =" & EMR_CTY_ID
        End If
        If HttpContext.Current.Session("sBSUID") = "315001" Then
            sqlGetEmirate = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='4' " & SQLCTY & " order by EMR_DESCR"
        ElseIf HttpContext.Current.Session("sBSUID") = "800017" Then
            sqlGetEmirate = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='3' " & SQLCTY & "  order by EMR_DESCR"
        Else
            sqlGetEmirate = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='" & ENQ_GROUP & "' " & SQLCTY & "  order by EMR_DESCR"
        End If


        Dim command As SqlCommand = New SqlCommand(sqlGetEmirate, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Sub GetEmirate_info()
        Try
            ddlEmirate.Items.Clear()
            ddlEmirate.Items.Add(New ListItem("Not Available", "0"))
            'Using AllEmirate_reader As SqlDataReader = AccessStudentClass.GetEmirate()
            Using AllEmirate_reader As SqlDataReader = GetEmirate(1, ddlCountry.SelectedItem.Value)
                If AllEmirate_reader.HasRows = True Then
                    While AllEmirate_reader.Read
                        ddlEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                    End While
                End If
            End Using
            If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then
                For ItemTypeCounter As Integer = 0 To ddlEmirate.Items.Count - 1
                    If ddlEmirate.Items(ItemTypeCounter).Value = ViewState("EMR_CODE") Then
                        ddlEmirate.SelectedIndex = ItemTypeCounter
                    End If
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmirate_info")
        End Try
    End Sub

#End Region
#Region "Button events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim Status As Integer
        Dim RecordStatus As String = String.Empty
        Dim bEdit As Boolean
        Dim Comp_ID As Integer = 0
        Dim offPhone As String
        Dim FaxNo As String
        Dim MobPhone As String
        Dim NEW_COMP_ID As String = "0"

        If txtOffPhone_Country.Text.Trim = "" Or txtOffPhone_Area.Text.Trim = "" Or txtOffPhone_No.Text.Trim = "" Then
            offPhone = ""
        Else
            offPhone = txtOffPhone_Country.Text & "-" & txtOffPhone_Area.Text & "-" & txtOffPhone_No.Text
        End If

        If txtFax_Country.Text.Trim = "" Or txtFax_Area.Text.Trim = "" Or txtFax_No.Text.Trim = "" Then
            FaxNo = ""
        Else
            FaxNo = txtFax_Country.Text & "-" & txtFax_Area.Text & "-" & txtFax_No.Text
        End If

        If txtMobile_Country.Text.Trim = "" Or txtMobile_Area.Text.Trim = "" Or txtMobile_No.Text.Trim = "" Then
            MobPhone = ""
        Else
            MobPhone = txtMobile_Country.Text & "-" & txtMobile_Area.Text & "-" & txtMobile_No.Text
        End If

        'check the data mode to do the required operation

        If txtComp.Text.Trim = "" Then
            lblError.Text = "Company Name cannot be empty"
            Exit Sub
        End If
        If txtADDR1.Text.Trim = "" Then
            lblError.Text = "Address cannot be empty"
            Exit Sub
        End If
        If txtCity.Text.Trim = "" Then
            lblError.Text = "City cannot be empty"
            Exit Sub
        End If

        If ddlCountry.SelectedValue.Trim = "0" Then
            lblError.Text = "Please select Country"
            Exit Sub
        End If

        'If ddlEmirate.SelectedValue.Trim = "-" Then
        '    lblError.Text = "Please select City/Emirate"
        '    Exit Sub
        'End If
        If txtOffPhone_Country.Text.Trim = "" Then
            lblError.Text = "Phone Country Code cannot be empty"
            Exit Sub
        End If
        If txtOffPhone_Area.Text.Trim = "" Then
            lblError.Text = "Phone Area Code cannot be empty"
            Exit Sub
        End If
        If txtOffPhone_No.Text.Trim = "" Then
            lblError.Text = "Phone No cannot be empty"
            Exit Sub
        End If
        If txtEmail.Text.Trim = "" Then
            lblError.Text = "Email cannot be empty"
            Exit Sub
        End If
        If txtContactPer.Text.Trim = "" Then
            lblError.Text = "Contact Person cannot be empty"
            Exit Sub
        End If

        If ViewState("datamode") = "edit" Then
            bEdit = True
            Comp_ID = ViewState("viewid")
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Status = AccessStudentClass.SaveSTUDCOMP_LISTED_M(Comp_ID, txtComp.Text, txtADDR1.Text, txtADDR2.Text, txtCity.Text, ddlCountry.SelectedItem.Value, txtPOBOX.Text, ddlEmirate.SelectedItem.Value, offPhone, FaxNo, MobPhone, txtEmail.Text, txtURL.Text, txtContactPer.Text, NEW_COMP_ID, bEdit, transaction)
                    'If error occured during the process  throw exception and rollback the process
                    If Status <> 0 Then
                        Throw New ArgumentException("Error while updating company record")
                    Else
                        'Store the required information into the Audit Trial table when Edited
                        Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                        If Status <> 0 Then
                            Throw New ArgumentException("Could not complete your request")
                        End If
                        If Session("Comp_FeeSetup").Rows.Count <> 0 Then

                            For i As Integer = 0 To Session("Comp_FeeSetup").Rows.Count - 1
                                If Session("Comp_FeeSetup").Rows(i)("Status").ToString = "updated" Or Session("Comp_FeeSetup").Rows(i)("Status").ToString = "Inserted" Or Session("Comp_FeeSetup").Rows(i)("Status").ToString = "DELETED" Then
                                    Dim Proforma As Boolean = False
                                    If Session("Comp_FeeSetup").Rows(i)("CIS_bPreformaInvoice") = "Yes" Then
                                        Proforma = True
                                    End If
                                    If Session("Comp_FeeSetup").Rows(i)("Status").ToString = "updated" And Session("Comp_FeeSetup").Rows(i)("CIS_ID") = "0" Then
                                        RecordStatus = "1" ' "Inserted"
                                    ElseIf Session("Comp_FeeSetup").Rows(i)("Status").ToString = "updated" And Session("Comp_FeeSetup").Rows(i)("CIS_ID") <> "0" Then
                                        RecordStatus = "2" '"updated"
                                    ElseIf Session("Comp_FeeSetup").Rows(i)("Status").ToString = "Inserted" Then
                                        RecordStatus = "1" '"Inserted"
                                    ElseIf Session("Comp_FeeSetup").Rows(i)("Status").ToString = "DELETED" And Session("Comp_FeeSetup").Rows(i)("CIS_ID") <> "0" Then
                                        RecordStatus = "0" '"DELETED"
                                    End If
                                    Status = AccessStudentClass.SaveSTUDCOMP_FEECOMPO_S(Session("Comp_FeeSetup").Rows(i)("CIS_ID"), _
                                          Comp_ID, Session("Comp_FeeSetup").Rows(i)("CIS_FEE_ID"), Proforma, _
                                          Session("Comp_FeeSetup").Rows(i)("CIS_DiscPerct"), RecordStatus, bEdit, transaction)
                                    If Status <> 0 Then
                                        Throw New ArgumentException("Error while updating company fee Component")
                                    Else
                                        Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                                        If Status <> 0 Then
                                            Throw New ArgumentException("Could not complete your request")
                                        End If
                                    End If
                                End If
                            Next
                        End If
                        ViewState("viewid") = "0"
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        transaction.Commit()
                        Call clearMe()
                        Call set_State()
                        clear_All()
                        gridbind()
                        lblError.Text = "Record Updated Successfully"
                    End If
                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Updated"
                End Try
            End Using
        ElseIf ViewState("datamode") = "add" Then
            bEdit = False
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Status = AccessStudentClass.SaveSTUDCOMP_LISTED_M(Comp_ID, txtComp.Text, txtADDR1.Text, txtADDR2.Text, txtCity.Text, ddlCountry.SelectedItem.Value, txtPOBOX.Text, ddlEmirate.SelectedItem.Value, offPhone, FaxNo, MobPhone, txtEmail.Text, txtURL.Text, txtContactPer.Text, NEW_COMP_ID, bEdit, transaction)
                    If Status = -111 Then
                        Throw New ArgumentException("Duplicate Company Name!!!!")
                    ElseIf Status <> 0 Then
                        Throw New ArgumentException("Error while inserting new record")
                    Else
                        'Store the required information into the Audit Trial table when inserted
                        Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), NEW_COMP_ID, "insert", Page.User.Identity.Name.ToString, Me.Page)
                        If Status <> 0 Then
                            Throw New ArgumentException("Could not complete your request")
                        End If
                        If Session("Comp_FeeSetup").Rows.Count <> 0 Then

                            For i As Integer = 0 To Session("Comp_FeeSetup").Rows.Count - 1
                                If Session("Comp_FeeSetup").Rows(i)("Status").ToString = "updated" Or Session("Comp_FeeSetup").Rows(i)("Status").ToString = "Inserted" Then
                                    Dim Proforma As Boolean = False
                                    If Session("Comp_FeeSetup").Rows(i)("CIS_bPreformaInvoice") = "Yes" Then
                                        Proforma = True
                                    End If
                                    RecordStatus = "1" 'Inserted
                                    Status = AccessStudentClass.SaveSTUDCOMP_FEECOMPO_S(Session("Comp_FeeSetup").Rows(i)("CIS_ID"), _
                                          NEW_COMP_ID, Session("Comp_FeeSetup").Rows(i)("CIS_FEE_ID"), Proforma, _
                                          Session("Comp_FeeSetup").Rows(i)("CIS_DiscPerct"), RecordStatus, bEdit, transaction)
                                    If Status <> 0 Then
                                        Throw New ArgumentException("Error while updating company fee Component")
                                    Else
                                        Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                                        If Status <> 0 Then
                                            Throw New ArgumentException("Could not complete your request")
                                        End If
                                    End If
                                End If
                            Next
                        End If

                        ViewState("viewid") = "0"
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        transaction.Commit()
                        Call clearMe()
                        Call set_State()
                        clear_All()
                        gridbind()
                        lblError.Text = "Record Inserted Successfully"
                    End If
                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Inserted"
                End Try

            End Using

        End If

    End Sub

    'If Page.IsValid = True Then
    '    Dim Status As Integer

    '    Dim bEdit As Boolean
    '    Dim Comp_ID As Integer = 0
    '    Dim offPhone As String
    '    Dim FaxNo As String
    '    Dim MobPhone As String

    '    If txtOffPhone_Country.Text.Trim <> "" And txtOffPhone_Area.Text.Trim <> "" And txtOffPhone_No.Text.Trim <> "" Then
    '        offPhone = txtOffPhone_Country.Text & "-" & txtOffPhone_Area.Text & "-" & txtOffPhone_No.Text
    '    End If

    '    If txtFax_Country.Text.Trim <> "" And txtFax_Area.Text.Trim <> "" And txtFax_No.Text.Trim <> "" Then
    '        FaxNo = txtFax_Country.Text & "-" & txtFax_Area.Text & "-" & txtFax_No.Text
    '    End If

    '    If txtMobile_Country.Text.Trim <> "" And txtMobile_Area.Text.Trim <> "" And txtMobile_No.Text.Trim <> "" Then
    '        MobPhone = txtMobile_Country.Text & "-" & txtMobile_Area.Text & "-" & txtMobile_No.Text
    '    End If

    '    'check the data mode to do the required operation

    '    If ViewState("datamode") = "edit" Then
    '        bEdit = True
    '        Comp_ID = ViewState("viewid")
    '        Dim transaction As SqlTransaction

    '        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
    '            transaction = conn.BeginTransaction("SampleTransaction")
    '            Try
    '                Status = AccessStudentClass.SaveSTUDCOMP_LISTED_M(Comp_ID, txtComp.Text, txtADDR1.Text, txtADDR2.Text, txtCity.Text, ddlCountry.SelectedItem.Value, txtPOBOX.Text, ddlEmirate.SelectedItem.Value, offPhone, FaxNo, MobPhone, txtEmail.Text, txtURL.Text, txtContactPer.Text, bEdit, transaction)
    '                'If error occured during the process  throw exception and rollback the process
    '                If Status <> 0 Then
    '                    Throw New ArgumentException("Error while updating the current record")
    '                Else
    '                    'Store the required information into the Audit Trial table when Edited
    '                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "edit", Page.User.Identity.Name.ToString, Me.Page)
    '                    If Status <> 0 Then
    '                        Throw New ArgumentException("Could not complete your request")
    '                    End If
    '                    ViewState("datamode") = "none"
    '                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                    transaction.Commit()
    '                    Call clearMe()
    '                    Call set_State()
    '                    lblError.Text = "Record Updated Successfully"
    '                End If
    '            Catch myex As ArgumentException
    '                transaction.Rollback()
    '                lblError.Text = myex.Message
    '            Catch ex As Exception
    '                transaction.Rollback()
    '                lblError.Text = "Record could not be Updated"
    '            End Try
    '        End Using

    '    ElseIf ViewState("datamode") = "add" Then
    '        bEdit = False
    '        Dim transaction As SqlTransaction
    '        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
    '            transaction = conn.BeginTransaction("SampleTransaction")
    '            Try
    '                Status = AccessStudentClass.SaveSTUDCOMP_LISTED_M(Comp_ID, txtComp.Text, txtADDR1.Text, txtADDR2.Text, txtCity.Text, ddlCountry.SelectedItem.Value, txtPOBOX.Text, ddlEmirate.SelectedItem.Value, offPhone, FaxNo, MobPhone, txtEmail.Text, txtURL.Text, txtContactPer.Text, bEdit, transaction)
    '                If Status = -111 Then
    '                    Throw New ArgumentException("Duplicate Company Name!!!!")
    '                ElseIf Status <> 0 Then
    '                    Throw New ArgumentException("Error while inserting new record")
    '                Else
    '                    'Store the required information into the Audit Trial table when inserted
    '                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "insert", Page.User.Identity.Name.ToString, Me.Page)
    '                    If Status <> 0 Then
    '                        Throw New ArgumentException("Could not complete your request")
    '                    End If
    '                    ViewState("datamode") = "none"
    '                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                    transaction.Commit()
    '                    Call clearMe()
    '                    Call set_State()
    '                    lblError.Text = "Record Inserted Successfully"
    '                End If
    '            Catch myex As ArgumentException
    '                transaction.Rollback()
    '                lblError.Text = myex.Message
    '            Catch ex As Exception
    '                transaction.Rollback()
    '                lblError.Text = "Record could not be Inserted"
    '            End Try

    '        End Using

    '    End If

    'End If
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteSTUDCOMP_LISTED_M(ViewState("viewid"), transaction)
                If Status = -1 Then
                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record could not be Deleted")
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        Throw New ArgumentException("Could not complete your request")
                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearMe()
                    Call set_State()
                    clear_All()
                    gridbind()
                    ViewState("viewid") = 0
                    lblError.Text = "Record Deleted Successfully"

                End If

            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message)
                transaction.Rollback()

            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try


            ViewState("datamode") = "add"
            btnFill.Visible = True
            Call clearMe()

            Call Reset_State()


            clear_All()
            gridbind()

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("Company", ex.Message)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ViewState("datamode") = "edit"
        btnFill.Visible = True
        Call Reset_State()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then


            ViewState("datamode") = "none"
            btnFill.Visible = False
            Call clearMe()
            clear_All()
            gridbind()
            Call set_State()

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
#End Region

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim fId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim fCIS_ID As New DataColumn("CIS_ID", System.Type.GetType("System.String"))
            Dim fCIS_COMP_ID As New DataColumn("CIS_COMP_ID", System.Type.GetType("System.String"))
            Dim fCIS_FEE_ID As New DataColumn("CIS_FEE_ID", System.Type.GetType("System.String"))
            Dim fCIS_FEE_Descr As New DataColumn("CIS_FEE_Descr", System.Type.GetType("System.String"))
            Dim fCIS_bPreformaInvoice As New DataColumn("CIS_bPreformaInvoice", System.Type.GetType("System.String"))
            Dim fCIS_DiscPerc As New DataColumn("CIS_DiscPerct", System.Type.GetType("System.Decimal"))
            Dim fStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            dtDt.Columns.Add(fId)
            dtDt.Columns.Add(fCIS_ID)
            dtDt.Columns.Add(fCIS_COMP_ID)
            dtDt.Columns.Add(fCIS_FEE_ID)
            dtDt.Columns.Add(fCIS_FEE_Descr)
            dtDt.Columns.Add(fCIS_bPreformaInvoice)
            dtDt.Columns.Add(fCIS_DiscPerc)
            dtDt.Columns.Add(fStatus)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Protected Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        gridbind_add()
    End Sub

    Sub gridbind_add()
        Try
            Dim iInc As Integer
            Dim j As Integer = 1
            If Not IsNumeric(txtDiscPer.Text) Then
                lblError.Text = "Invalid Discount Amount"
                Exit Sub
            End If
            Dim arrID As New ArrayList
            For iInc = 0 To Session("Comp_FeeSetup").Rows.Count - 1
                If arrID.Contains(Session("Comp_FeeSetup").Rows(iInc)("ID")) = False Then
                    arrID.Add(Session("Comp_FeeSetup").Rows(iInc)("ID"))
                End If

            Next
            For Each DROW As DataRow In Session("Comp_FeeSetup").Rows
                If DROW("CIS_FEE_ID").ToString = ddlFeeType.SelectedValue Then
                    lblError.Text = ddlFeeType.SelectedItem.Text & " already Exists!"
                    Exit Sub
                End If
            Next

            Dim dr As DataRow
            dr = Session("Comp_FeeSetup").NewRow


            Do While j > 0
                If arrID.Contains(j) = False Then
                    arrID.Add(j)
                    ViewState("iID") = j
                    Exit Do
                End If
                j = j + 1

            Loop

            dr("Id") = ViewState("iID")

            dr("CIS_ID") = 0
            dr("CIS_FEE_ID") = ddlFeeType.SelectedItem.Value
            If ViewState("datamode") = "edit" Then
                dr("CIS_COMP_ID") = ViewState("viewid")
            Else
                dr("CIS_COMP_ID") = "0"
            End If

            dr("CIS_FEE_Descr") = ddlFeeType.SelectedItem.Text
            If rdYes.Checked = True Then
                dr("CIS_bPreformaInvoice") = "Yes"
            ElseIf rdNo.Checked = True Then
                dr("CIS_bPreformaInvoice") = "No"
            End If
            dr("CIS_DiscPerct") = txtDiscPer.Text
            dr("Status") = "Inserted"

            Session("Comp_FeeSetup").Rows.Add(dr)
            gvFeeDetail.DataSource = Session("Comp_FeeSetup")
            gvFeeDetail.DataBind()
        Catch ex As Exception
            lblError.Text = "Check Amount"
        End Try
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        btnSave.Enabled = True
        Dim row As GridViewRow = gvFeeDetail.Rows(gvFeeDetail.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("Comp_FeeSetup").Rows.Count - 1
            If iIndex = Session("Comp_FeeSetup").Rows(iEdit)(0) Then

                If ViewState("datamode") = "edit" Then
                    Session("Comp_FeeSetup").Rows(iEdit)("CIS_COMP_ID") = ViewState("viewid")
                Else
                    Session("Comp_FeeSetup").Rows(iEdit)("CIS_COMP_ID") = "0"
                End If


                Session("Comp_FeeSetup").Rows(iEdit)("CIS_FEE_ID") = ddlFeeType.SelectedItem.Value
                Session("Comp_FeeSetup").Rows(iEdit)("CIS_FEE_Descr") = ddlFeeType.SelectedItem.Text
                If rdYes.Checked = True Then
                    Session("Comp_FeeSetup").Rows(iEdit)("CIS_bPreformaInvoice") = "Yes"
                ElseIf rdNo.Checked = False Then
                    Session("Comp_FeeSetup").Rows(iEdit)("CIS_bPreformaInvoice") = "No"
                End If
                Session("Comp_FeeSetup").Rows(iEdit)("CIS_DiscPerct") = txtDiscPer.Text
                If Session("Comp_FeeSetup").Rows(iEdit)("Status").ToString <> "inserted" Then
                    Session("Comp_FeeSetup").Rows(iEdit)("Status") = "updated"
                End If


                Exit For
            End If
        Next
        btnFill.Visible = True
        btnUpdate.Visible = False
        btnChildCancel.Visible = False
        gvFeeDetail.Columns(3).Visible = True
        gvFeeDetail.SelectedIndex = -1
        gvFeeDetail.Columns(4).Visible = True
        gridbind()
    End Sub


    Protected Sub btnChildCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChildCancel.Click
        btnFill.Visible = True
        btnUpdate.Visible = False
        btnChildCancel.Visible = False
        btnSave.Enabled = True
        gvFeeDetail.Columns(3).Visible = True
        gvFeeDetail.SelectedIndex = -1
        gvFeeDetail.Columns(4).Visible = True
        gridbind()
    End Sub
    Protected Sub gvFeeDetail_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvFeeDetail.RowEditing
        btnSave.Enabled = False
        gvFeeDetail.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvFeeDetail.Rows(e.NewEditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("Comp_FeeSetup").Rows.Count - 1
            If iIndex = Session("Comp_FeeSetup").Rows(iEdit)(0) Then
                'txtAmount.Text = Session("Comp_FeeSetup").Rows(iEdit)("Amount")

                'ID, Acd_year, Acd_id, Fee_id, Fee_descr, Grd_id, FDate, TDate, Amount, Status
                '  , Join, Discontinue, Active ,Percentage,LateAmount,LateDays ,Collection

                ddlFeeType.ClearSelection()
                ddlFeeType.Items.FindByValue(Session("Comp_FeeSetup").Rows(iEdit)("CIS_FEE_ID").ToString).Selected = True

                txtDiscPer.Text = Format(Session("Comp_FeeSetup").Rows(iEdit)("CIS_DiscPerct"), "0.00")

                If Session("Comp_FeeSetup").Rows(iEdit)("CIS_bPreformaInvoice") = "Yes" Then
                    rdYes.Checked = True
                    rdNo.Checked = False
                ElseIf Session("Comp_FeeSetup").Rows(iEdit)("CIS_bPreformaInvoice") = "No" Then
                    rdYes.Checked = False
                    rdNo.Checked = True
                End If
                Exit For
            End If
        Next
        btnFill.Visible = False
        btnUpdate.Visible = True
        btnChildCancel.Visible = True
        gvFeeDetail.Columns(3).Visible = False
        gvFeeDetail.Columns(4).Visible = False
        gridbind()
    End Sub


    Protected Sub gvFeeDetail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFeeDetail.RowDeleting
        Try
            Dim row As GridViewRow = gvFeeDetail.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("Comp_FeeSetup").Rows.Count - 1
                If iIndex = Session("Comp_FeeSetup").Rows(iRemove)(0) Then
                    'Dim dv As New DataView(Session("gdtSub"))
                    'dv.RowFilter = "Status<>'DELETED' "

                    'gvJournal.DataSource = dv
                    'gvJournal.DataBind()
                    Session("Comp_FeeSetup").Rows(iRemove)("Status") = "DELETED"
                    ' Session("Comp_FeeSetup").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Record cannot be Deleted"
        End Try
    End Sub
    Sub clear_All()

        Session("Comp_FeeSetup").Rows.Clear()
    End Sub


    Sub gridbind()
        Dim dv As New DataView(Session("Comp_FeeSetup"))
        dv.RowFilter = "Status<>'DELETED'"


        gvFeeDetail.DataSource = dv
        gvFeeDetail.DataBind()

        'gvFeeDetail.DataSource = Session("Comp_FeeSetup")
        'gvFeeDetail.DataBind()
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        GetEmirate_info()
    End Sub
End Class
