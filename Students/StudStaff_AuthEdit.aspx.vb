Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports Telerik.Web.UI
Partial Class Students_StudStaff_AuthEdit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim temp_ID As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
           

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = "view"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050095") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling page right class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                btnCancel1.Visible = False

                Dim conn As String = ConnectionManger.GetOASISConnectionString
                ''commentd by nahyan on 28apr 2019 -adv charan to show only desig with school
                ''  Dim EMPDESIGN As String = "SELECT DES_ID, DES_DESCR FROM EMPDESIGNATION_M WHERE (DES_FLAG = 'SD') AND (DES_DESCR NOT LIKE '-%')ORDER BY DES_DESCR"
                Dim EMPDESIGN As String = "select distinct  DES_ID, DES_DESCR FROM EMPDESIGNATION_M inner join employee_m on EMP_DES_ID= des_id and EMP_BSU_ID='" & Session("sBsuid") & "' WHERE (DES_FLAG = 'SD') AND (DES_DESCR NOT LIKE '-%')ORDER BY DES_DESCR"
                Dim STAFF_AUTH As String = "SELECT ATC_DES_ID FROM AUTHORIZED_CATEGORY WHERE ATC_BSU_ID ='" & Session("sBsuid") & "'"

                Dim ds1 As New DataSet
                Dim ds2 As New DataSet


                ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, EMPDESIGN)
                ds2 = SqlHelper.ExecuteDataset(conn, CommandType.Text, STAFF_AUTH)

                disablecontrol()
                Dim row As DataRow
                For Each row In ds1.Tables(0).Rows

                    Dim str1 As String = row("DES_DESCR")
                    Dim str2 As String = row("DES_ID")
                    chkCategory.Items.Add(New RadComboBoxItem(str1, str2))
                    Dim row2 As DataRow
                    For Each row2 In ds2.Tables(0).Rows
                        Dim str3 As String = row2("ATC_DES_ID")
                        If str2 = str3 Then
                            chkCategory.Items.FindItemByText(str1).Checked = True

                        End If
                    Next
                Next

            End If

            End If
    End Sub


    Sub clearall()

        For Each item As RadComboBoxItem In chkCategory.Items
            item.Selected = False
        Next

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim CountChk As Integer = 0
        Dim collection As IList(Of RadComboBoxItem) = chkCategory.CheckedItems
        'For Each item As RadComboBoxItem In collection
        '    If (item.Selected) Then
        '        CountChk = CountChk + 1
        '    End If
        'Next
        CountChk = collection.Count()
        If CountChk > 0 Then
            str_err = callTrans(errorMessage)

            If str_err = "0" Then
                Call disablecontrol()
                lblError.Text = "Record updated successfully"
            Else
                lblError.Text = errorMessage
            End If
        Else
            lblError.Text = "Category needs to be selected"
        End If

    End Sub

    Function callTrans(ByRef errorMessage As String) As String

        Dim transaction As SqlTransaction
        Dim Status As Integer
        'insert the new user
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Status = AccessStudentClass.DeleteStaffAuth_BSU(Session("sBsuid"), transaction)
                Dim collection As IList(Of RadComboBoxItem) = chkCategory.CheckedItems
                If Status = 0 Then
                    For Each item As RadComboBoxItem In collection
                        'If (item.Selected) Then
                        'loop through the each checkbox and insert it
                        Status = AccessStudentClass.SaveSTAFF_AUTHORIZED_M(Session("sBsuid"), item.Value, transaction)
                        If Status <> 0 Then
                            callTrans = "1"
                            errorMessage = "Error while inserting records"
                            Return "1"
                        End If
                        '  End If
                    Next
                Else
                    callTrans = "1"
                    errorMessage = "Error while inserting records"
                    Return "1"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), temp_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    callTrans = "1"
                    errorMessage = "Could not process audit request"
                    Return "1"
                End If


                ViewState("viewid") = "0"
                ViewState("datamode") = "none"

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                callTrans = "0"
                ' clearall()
                btnAdd.Visible = True
                btnCancel1.Visible = False
            Catch ex As Exception
                callTrans = "1"
                errorMessage = "Record could not be Updated"
            Finally
                If callTrans <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                disablecontrol()
                btnAdd.Visible = True
                btnCancel1.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub resetcontrol()
        chkCategory.Enabled = True

    End Sub
    Sub disablecontrol()
        chkCategory.Enabled = False

    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        resetcontrol()
        btnCancel1.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
End Class
