Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studShifts_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Session("SelectedRow") = -1
            Try
            
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
           

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "S050019" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                If ViewState("datamode") = "view" Then
                    hfLNG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("LNGID").Replace(" ", "+"))

                    txtLangDescr.Text = Encr_decrData.Decrypt(Request.QueryString("LNGDESCR").Replace(" ", "+"))
                Else
                    hfLNG_ID.Value = 0

                    txtLangDescr.ReadOnly = False
                End If



            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub SaveData()
        Dim datamode As String
        If ViewState("datamode") = "none" Then
            datamode = "delete"
        Else
            datamode = ViewState("datamode")
        End If
        Dim flagAudit As Integer
        Dim LNGId As Integer
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                If datamode <> "add" Then
                    UtilityObj.InsertAuditdetails(transaction, datamode, "Language_M", "LNG_ID", "LNG_ID", "LNG_ID=" + hfLNG_ID.Value.ToString)
                End If
                Dim str_query As String = "exec studSaveLang " + hfLNG_ID.Value.ToString + ",'" + txtLangDescr.Text + " ','" + datamode + "'"
                LNGId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "LNG_DESCR(" + LNGId.ToString + ")", IIf(datamode = "add", "Insert", datamode), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using

    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"

        txtLangDescr.ReadOnly = False
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"


        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        txtLangDescr.ReadOnly = False
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If hfLNG_ID.Value = 0 Then
                lblError.Text = "No records to delete"
                Exit Sub
            End If
            ViewState("datamode") = "delete"
            SaveData()

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            ViewState("datamode") = "none"
            txtLangDescr.Text = ""
            lblError.Text = "Record Deleted Successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                If isLANGAdded() > 0 Then
                    lblError.Text = "This Language is already created"
                    Exit Sub
                End If
            End If
            SaveData()

            txtLangDescr.Text = ""

            txtLangDescr.ReadOnly = True
            ViewState("datamode") = "none"
            lblError.Text = "Record saved successfully"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Error while saving data"
        End Try
    End Sub
    Private Function isLANGAdded() As Integer
        Try
            Dim count As Integer
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Dim str_query As String

            str_query = "Select  count(lng_id) from Language_m where " _
                                     & " LNG_DESC ='" + txtLangDescr.Text + "'"


            count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            Return count
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfLNG_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then


                txtLangDescr.Text = ""

                txtLangDescr.ReadOnly = True
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
